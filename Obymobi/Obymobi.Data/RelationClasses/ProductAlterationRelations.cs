﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ProductAlteration. </summary>
	public partial class ProductAlterationRelations
	{
		/// <summary>CTor</summary>
		public ProductAlterationRelations()
		{
		}

		/// <summary>Gets all relations of the ProductAlterationEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AlterationEntityUsingAlterationId);
			toReturn.Add(this.PosproductPosalterationEntityUsingPosproductPosalterationId);
			toReturn.Add(this.ProductEntityUsingProductId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between ProductAlterationEntity and AlterationEntity over the m:1 relation they have, using the relation between the fields:
		/// ProductAlteration.AlterationId - Alteration.AlterationId
		/// </summary>
		public virtual IEntityRelation AlterationEntityUsingAlterationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AlterationEntity", false);
				relation.AddEntityFieldPair(AlterationFields.AlterationId, ProductAlterationFields.AlterationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductAlterationEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ProductAlterationEntity and PosproductPosalterationEntity over the m:1 relation they have, using the relation between the fields:
		/// ProductAlteration.PosproductPosalterationId - PosproductPosalteration.PosproductPosalterationId
		/// </summary>
		public virtual IEntityRelation PosproductPosalterationEntityUsingPosproductPosalterationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PosproductPosalterationEntity", false);
				relation.AddEntityFieldPair(PosproductPosalterationFields.PosproductPosalterationId, ProductAlterationFields.PosproductPosalterationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PosproductPosalterationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductAlterationEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ProductAlterationEntity and ProductEntity over the m:1 relation they have, using the relation between the fields:
		/// ProductAlteration.ProductId - Product.ProductId
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ProductEntity", false);
				relation.AddEntityFieldPair(ProductFields.ProductId, ProductAlterationFields.ProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductAlterationEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticProductAlterationRelations
	{
		internal static readonly IEntityRelation AlterationEntityUsingAlterationIdStatic = new ProductAlterationRelations().AlterationEntityUsingAlterationId;
		internal static readonly IEntityRelation PosproductPosalterationEntityUsingPosproductPosalterationIdStatic = new ProductAlterationRelations().PosproductPosalterationEntityUsingPosproductPosalterationId;
		internal static readonly IEntityRelation ProductEntityUsingProductIdStatic = new ProductAlterationRelations().ProductEntityUsingProductId;

		/// <summary>CTor</summary>
		static StaticProductAlterationRelations()
		{
		}
	}
}
