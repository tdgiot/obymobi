﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: SurveyQuestionLanguage. </summary>
	public partial class SurveyQuestionLanguageRelations
	{
		/// <summary>CTor</summary>
		public SurveyQuestionLanguageRelations()
		{
		}

		/// <summary>Gets all relations of the SurveyQuestionLanguageEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.LanguageEntityUsingLanguageId);
			toReturn.Add(this.SurveyQuestionEntityUsingSurveyQuestionId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between SurveyQuestionLanguageEntity and LanguageEntity over the m:1 relation they have, using the relation between the fields:
		/// SurveyQuestionLanguage.LanguageId - Language.LanguageId
		/// </summary>
		public virtual IEntityRelation LanguageEntityUsingLanguageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "LanguageEntity", false);
				relation.AddEntityFieldPair(LanguageFields.LanguageId, SurveyQuestionLanguageFields.LanguageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyQuestionLanguageEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between SurveyQuestionLanguageEntity and SurveyQuestionEntity over the m:1 relation they have, using the relation between the fields:
		/// SurveyQuestionLanguage.SurveyQuestionId - SurveyQuestion.SurveyQuestionId
		/// </summary>
		public virtual IEntityRelation SurveyQuestionEntityUsingSurveyQuestionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SurveyQuestionEntity", false);
				relation.AddEntityFieldPair(SurveyQuestionFields.SurveyQuestionId, SurveyQuestionLanguageFields.SurveyQuestionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyQuestionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyQuestionLanguageEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticSurveyQuestionLanguageRelations
	{
		internal static readonly IEntityRelation LanguageEntityUsingLanguageIdStatic = new SurveyQuestionLanguageRelations().LanguageEntityUsingLanguageId;
		internal static readonly IEntityRelation SurveyQuestionEntityUsingSurveyQuestionIdStatic = new SurveyQuestionLanguageRelations().SurveyQuestionEntityUsingSurveyQuestionId;

		/// <summary>CTor</summary>
		static StaticSurveyQuestionLanguageRelations()
		{
		}
	}
}
