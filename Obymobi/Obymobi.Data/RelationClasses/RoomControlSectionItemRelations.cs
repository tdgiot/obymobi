﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: RoomControlSectionItem. </summary>
	public partial class RoomControlSectionItemRelations
	{
		/// <summary>CTor</summary>
		public RoomControlSectionItemRelations()
		{
		}

		/// <summary>Gets all relations of the RoomControlSectionItemEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CustomTextEntityUsingRoomControlSectionItemId);
			toReturn.Add(this.MediaEntityUsingRoomControlSectionItemId);
			toReturn.Add(this.RoomControlSectionItemLanguageEntityUsingRoomControlSectionItemId);
			toReturn.Add(this.RoomControlWidgetEntityUsingRoomControlSectionItemId);
			toReturn.Add(this.InfraredConfigurationEntityUsingInfraredConfigurationId);
			toReturn.Add(this.RoomControlSectionEntityUsingRoomControlSectionId);
			toReturn.Add(this.StationListEntityUsingStationListId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between RoomControlSectionItemEntity and CustomTextEntity over the 1:n relation they have, using the relation between the fields:
		/// RoomControlSectionItem.RoomControlSectionItemId - CustomText.RoomControlSectionItemId
		/// </summary>
		public virtual IEntityRelation CustomTextEntityUsingRoomControlSectionItemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomTextCollection" , true);
				relation.AddEntityFieldPair(RoomControlSectionItemFields.RoomControlSectionItemId, CustomTextFields.RoomControlSectionItemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlSectionItemEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RoomControlSectionItemEntity and MediaEntity over the 1:n relation they have, using the relation between the fields:
		/// RoomControlSectionItem.RoomControlSectionItemId - Media.RoomControlSectionItemId
		/// </summary>
		public virtual IEntityRelation MediaEntityUsingRoomControlSectionItemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MediaCollection" , true);
				relation.AddEntityFieldPair(RoomControlSectionItemFields.RoomControlSectionItemId, MediaFields.RoomControlSectionItemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlSectionItemEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RoomControlSectionItemEntity and RoomControlSectionItemLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// RoomControlSectionItem.RoomControlSectionItemId - RoomControlSectionItemLanguage.RoomControlSectionItemId
		/// </summary>
		public virtual IEntityRelation RoomControlSectionItemLanguageEntityUsingRoomControlSectionItemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RoomControlSectionItemLanguageCollection" , true);
				relation.AddEntityFieldPair(RoomControlSectionItemFields.RoomControlSectionItemId, RoomControlSectionItemLanguageFields.RoomControlSectionItemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlSectionItemEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlSectionItemLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RoomControlSectionItemEntity and RoomControlWidgetEntity over the 1:n relation they have, using the relation between the fields:
		/// RoomControlSectionItem.RoomControlSectionItemId - RoomControlWidget.RoomControlSectionItemId
		/// </summary>
		public virtual IEntityRelation RoomControlWidgetEntityUsingRoomControlSectionItemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RoomControlWidgetCollection" , true);
				relation.AddEntityFieldPair(RoomControlSectionItemFields.RoomControlSectionItemId, RoomControlWidgetFields.RoomControlSectionItemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlSectionItemEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlWidgetEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between RoomControlSectionItemEntity and InfraredConfigurationEntity over the m:1 relation they have, using the relation between the fields:
		/// RoomControlSectionItem.InfraredConfigurationId - InfraredConfiguration.InfraredConfigurationId
		/// </summary>
		public virtual IEntityRelation InfraredConfigurationEntityUsingInfraredConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "InfraredConfigurationEntity", false);
				relation.AddEntityFieldPair(InfraredConfigurationFields.InfraredConfigurationId, RoomControlSectionItemFields.InfraredConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InfraredConfigurationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlSectionItemEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RoomControlSectionItemEntity and RoomControlSectionEntity over the m:1 relation they have, using the relation between the fields:
		/// RoomControlSectionItem.RoomControlSectionId - RoomControlSection.RoomControlSectionId
		/// </summary>
		public virtual IEntityRelation RoomControlSectionEntityUsingRoomControlSectionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RoomControlSectionEntity", false);
				relation.AddEntityFieldPair(RoomControlSectionFields.RoomControlSectionId, RoomControlSectionItemFields.RoomControlSectionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlSectionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlSectionItemEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RoomControlSectionItemEntity and StationListEntity over the m:1 relation they have, using the relation between the fields:
		/// RoomControlSectionItem.StationListId - StationList.StationListId
		/// </summary>
		public virtual IEntityRelation StationListEntityUsingStationListId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "StationListEntity", false);
				relation.AddEntityFieldPair(StationListFields.StationListId, RoomControlSectionItemFields.StationListId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("StationListEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlSectionItemEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticRoomControlSectionItemRelations
	{
		internal static readonly IEntityRelation CustomTextEntityUsingRoomControlSectionItemIdStatic = new RoomControlSectionItemRelations().CustomTextEntityUsingRoomControlSectionItemId;
		internal static readonly IEntityRelation MediaEntityUsingRoomControlSectionItemIdStatic = new RoomControlSectionItemRelations().MediaEntityUsingRoomControlSectionItemId;
		internal static readonly IEntityRelation RoomControlSectionItemLanguageEntityUsingRoomControlSectionItemIdStatic = new RoomControlSectionItemRelations().RoomControlSectionItemLanguageEntityUsingRoomControlSectionItemId;
		internal static readonly IEntityRelation RoomControlWidgetEntityUsingRoomControlSectionItemIdStatic = new RoomControlSectionItemRelations().RoomControlWidgetEntityUsingRoomControlSectionItemId;
		internal static readonly IEntityRelation InfraredConfigurationEntityUsingInfraredConfigurationIdStatic = new RoomControlSectionItemRelations().InfraredConfigurationEntityUsingInfraredConfigurationId;
		internal static readonly IEntityRelation RoomControlSectionEntityUsingRoomControlSectionIdStatic = new RoomControlSectionItemRelations().RoomControlSectionEntityUsingRoomControlSectionId;
		internal static readonly IEntityRelation StationListEntityUsingStationListIdStatic = new RoomControlSectionItemRelations().StationListEntityUsingStationListId;

		/// <summary>CTor</summary>
		static StaticRoomControlSectionItemRelations()
		{
		}
	}
}
