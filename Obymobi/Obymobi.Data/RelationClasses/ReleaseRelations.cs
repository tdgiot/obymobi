﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Release. </summary>
	public partial class ReleaseRelations
	{
		/// <summary>CTor</summary>
		public ReleaseRelations()
		{
		}

		/// <summary>Gets all relations of the ReleaseEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ApplicationEntityUsingReleaseId);
			toReturn.Add(this.CloudProcessingTaskEntityUsingReleaseId);
			toReturn.Add(this.CompanyReleaseEntityUsingReleaseId);
			toReturn.Add(this.ReleaseEntityUsingIntermediateReleaseId);
			toReturn.Add(this.ApplicationEntityUsingApplicationId);
			toReturn.Add(this.ReleaseEntityUsingReleaseIdIntermediateReleaseId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ReleaseEntity and ApplicationEntity over the 1:n relation they have, using the relation between the fields:
		/// Release.ReleaseId - Application.ReleaseId
		/// </summary>
		public virtual IEntityRelation ApplicationEntityUsingReleaseId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ApplicationCollection" , true);
				relation.AddEntityFieldPair(ReleaseFields.ReleaseId, ApplicationFields.ReleaseId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReleaseEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ApplicationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ReleaseEntity and CloudProcessingTaskEntity over the 1:n relation they have, using the relation between the fields:
		/// Release.ReleaseId - CloudProcessingTask.ReleaseId
		/// </summary>
		public virtual IEntityRelation CloudProcessingTaskEntityUsingReleaseId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CloudProcessingTaskCollection" , true);
				relation.AddEntityFieldPair(ReleaseFields.ReleaseId, CloudProcessingTaskFields.ReleaseId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReleaseEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CloudProcessingTaskEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ReleaseEntity and CompanyReleaseEntity over the 1:n relation they have, using the relation between the fields:
		/// Release.ReleaseId - CompanyRelease.ReleaseId
		/// </summary>
		public virtual IEntityRelation CompanyReleaseEntityUsingReleaseId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CompanyReleaseCollection" , true);
				relation.AddEntityFieldPair(ReleaseFields.ReleaseId, CompanyReleaseFields.ReleaseId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReleaseEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyReleaseEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ReleaseEntity and ReleaseEntity over the 1:n relation they have, using the relation between the fields:
		/// Release.ReleaseId - Release.IntermediateReleaseId
		/// </summary>
		public virtual IEntityRelation ReleaseEntityUsingIntermediateReleaseId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "IntermediateReleaseCollection" , true);
				relation.AddEntityFieldPair(ReleaseFields.ReleaseId, ReleaseFields.IntermediateReleaseId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReleaseEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReleaseEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between ReleaseEntity and ApplicationEntity over the m:1 relation they have, using the relation between the fields:
		/// Release.ApplicationId - Application.ApplicationId
		/// </summary>
		public virtual IEntityRelation ApplicationEntityUsingApplicationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ApplicationEntity", false);
				relation.AddEntityFieldPair(ApplicationFields.ApplicationId, ReleaseFields.ApplicationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ApplicationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReleaseEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ReleaseEntity and ReleaseEntity over the m:1 relation they have, using the relation between the fields:
		/// Release.IntermediateReleaseId - Release.ReleaseId
		/// </summary>
		public virtual IEntityRelation ReleaseEntityUsingReleaseIdIntermediateReleaseId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "IntermedateReleaseEntity", false);
				relation.AddEntityFieldPair(ReleaseFields.ReleaseId, ReleaseFields.IntermediateReleaseId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReleaseEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReleaseEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticReleaseRelations
	{
		internal static readonly IEntityRelation ApplicationEntityUsingReleaseIdStatic = new ReleaseRelations().ApplicationEntityUsingReleaseId;
		internal static readonly IEntityRelation CloudProcessingTaskEntityUsingReleaseIdStatic = new ReleaseRelations().CloudProcessingTaskEntityUsingReleaseId;
		internal static readonly IEntityRelation CompanyReleaseEntityUsingReleaseIdStatic = new ReleaseRelations().CompanyReleaseEntityUsingReleaseId;
		internal static readonly IEntityRelation ReleaseEntityUsingIntermediateReleaseIdStatic = new ReleaseRelations().ReleaseEntityUsingIntermediateReleaseId;
		internal static readonly IEntityRelation ApplicationEntityUsingApplicationIdStatic = new ReleaseRelations().ApplicationEntityUsingApplicationId;
		internal static readonly IEntityRelation ReleaseEntityUsingReleaseIdIntermediateReleaseIdStatic = new ReleaseRelations().ReleaseEntityUsingReleaseIdIntermediateReleaseId;

		/// <summary>CTor</summary>
		static StaticReleaseRelations()
		{
		}
	}
}
