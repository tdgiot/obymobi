﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: AdyenPaymentMethod. </summary>
	public partial class AdyenPaymentMethodRelations
	{
		/// <summary>CTor</summary>
		public AdyenPaymentMethodRelations()
		{
		}

		/// <summary>Gets all relations of the AdyenPaymentMethodEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AdyenPaymentMethodBrandEntityUsingAdyenPaymentMethodId);
			toReturn.Add(this.PaymentIntegrationConfigurationEntityUsingPaymentIntegrationConfigurationId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between AdyenPaymentMethodEntity and AdyenPaymentMethodBrandEntity over the 1:n relation they have, using the relation between the fields:
		/// AdyenPaymentMethod.AdyenPaymentMethodId - AdyenPaymentMethodBrand.AdyenPaymentMethodId
		/// </summary>
		public virtual IEntityRelation AdyenPaymentMethodBrandEntityUsingAdyenPaymentMethodId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AdyenPaymentMethodBrandCollection" , true);
				relation.AddEntityFieldPair(AdyenPaymentMethodFields.AdyenPaymentMethodId, AdyenPaymentMethodBrandFields.AdyenPaymentMethodId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdyenPaymentMethodEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdyenPaymentMethodBrandEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between AdyenPaymentMethodEntity and PaymentIntegrationConfigurationEntity over the m:1 relation they have, using the relation between the fields:
		/// AdyenPaymentMethod.PaymentIntegrationConfigurationId - PaymentIntegrationConfiguration.PaymentIntegrationConfigurationId
		/// </summary>
		public virtual IEntityRelation PaymentIntegrationConfigurationEntityUsingPaymentIntegrationConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PaymentIntegrationConfigurationEntity", false);
				relation.AddEntityFieldPair(PaymentIntegrationConfigurationFields.PaymentIntegrationConfigurationId, AdyenPaymentMethodFields.PaymentIntegrationConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentIntegrationConfigurationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdyenPaymentMethodEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticAdyenPaymentMethodRelations
	{
		internal static readonly IEntityRelation AdyenPaymentMethodBrandEntityUsingAdyenPaymentMethodIdStatic = new AdyenPaymentMethodRelations().AdyenPaymentMethodBrandEntityUsingAdyenPaymentMethodId;
		internal static readonly IEntityRelation PaymentIntegrationConfigurationEntityUsingPaymentIntegrationConfigurationIdStatic = new AdyenPaymentMethodRelations().PaymentIntegrationConfigurationEntityUsingPaymentIntegrationConfigurationId;

		/// <summary>CTor</summary>
		static StaticAdyenPaymentMethodRelations()
		{
		}
	}
}
