﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: AlterationitemAlteration. </summary>
	public partial class AlterationitemAlterationRelations
	{
		/// <summary>CTor</summary>
		public AlterationitemAlterationRelations()
		{
		}

		/// <summary>Gets all relations of the AlterationitemAlterationEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AlterationEntityUsingAlterationId);
			toReturn.Add(this.AlterationitemEntityUsingAlterationitemId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between AlterationitemAlterationEntity and AlterationEntity over the m:1 relation they have, using the relation between the fields:
		/// AlterationitemAlteration.AlterationId - Alteration.AlterationId
		/// </summary>
		public virtual IEntityRelation AlterationEntityUsingAlterationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AlterationEntity", false);
				relation.AddEntityFieldPair(AlterationFields.AlterationId, AlterationitemAlterationFields.AlterationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationitemAlterationEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AlterationitemAlterationEntity and AlterationitemEntity over the m:1 relation they have, using the relation between the fields:
		/// AlterationitemAlteration.AlterationitemId - Alterationitem.AlterationitemId
		/// </summary>
		public virtual IEntityRelation AlterationitemEntityUsingAlterationitemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AlterationitemEntity", false);
				relation.AddEntityFieldPair(AlterationitemFields.AlterationitemId, AlterationitemAlterationFields.AlterationitemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationitemEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationitemAlterationEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticAlterationitemAlterationRelations
	{
		internal static readonly IEntityRelation AlterationEntityUsingAlterationIdStatic = new AlterationitemAlterationRelations().AlterationEntityUsingAlterationId;
		internal static readonly IEntityRelation AlterationitemEntityUsingAlterationitemIdStatic = new AlterationitemAlterationRelations().AlterationitemEntityUsingAlterationitemId;

		/// <summary>CTor</summary>
		static StaticAlterationitemAlterationRelations()
		{
		}
	}
}
