﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: RoomControlArea. </summary>
	public partial class RoomControlAreaRelations
	{
		/// <summary>CTor</summary>
		public RoomControlAreaRelations()
		{
		}

		/// <summary>Gets all relations of the RoomControlAreaEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ClientEntityUsingRoomControlAreaId);
			toReturn.Add(this.CustomTextEntityUsingRoomControlAreaId);
			toReturn.Add(this.DeliverypointEntityUsingRoomControlAreaId);
			toReturn.Add(this.RoomControlAreaLanguageEntityUsingRoomControlAreaId);
			toReturn.Add(this.RoomControlSectionEntityUsingRoomControlAreaId);
			toReturn.Add(this.UIWidgetEntityUsingRoomControlAreaId);
			toReturn.Add(this.RoomControlConfigurationEntityUsingRoomControlConfigurationId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between RoomControlAreaEntity and ClientEntity over the 1:n relation they have, using the relation between the fields:
		/// RoomControlArea.RoomControlAreaId - Client.RoomControlAreaId
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingRoomControlAreaId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ClientCollection" , true);
				relation.AddEntityFieldPair(RoomControlAreaFields.RoomControlAreaId, ClientFields.RoomControlAreaId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlAreaEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RoomControlAreaEntity and CustomTextEntity over the 1:n relation they have, using the relation between the fields:
		/// RoomControlArea.RoomControlAreaId - CustomText.RoomControlAreaId
		/// </summary>
		public virtual IEntityRelation CustomTextEntityUsingRoomControlAreaId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomTextCollection" , true);
				relation.AddEntityFieldPair(RoomControlAreaFields.RoomControlAreaId, CustomTextFields.RoomControlAreaId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlAreaEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RoomControlAreaEntity and DeliverypointEntity over the 1:n relation they have, using the relation between the fields:
		/// RoomControlArea.RoomControlAreaId - Deliverypoint.RoomControlAreaId
		/// </summary>
		public virtual IEntityRelation DeliverypointEntityUsingRoomControlAreaId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeliverypointCollection" , true);
				relation.AddEntityFieldPair(RoomControlAreaFields.RoomControlAreaId, DeliverypointFields.RoomControlAreaId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlAreaEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RoomControlAreaEntity and RoomControlAreaLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// RoomControlArea.RoomControlAreaId - RoomControlAreaLanguage.RoomControlAreaId
		/// </summary>
		public virtual IEntityRelation RoomControlAreaLanguageEntityUsingRoomControlAreaId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RoomControlAreaLanguageCollection" , true);
				relation.AddEntityFieldPair(RoomControlAreaFields.RoomControlAreaId, RoomControlAreaLanguageFields.RoomControlAreaId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlAreaEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlAreaLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RoomControlAreaEntity and RoomControlSectionEntity over the 1:n relation they have, using the relation between the fields:
		/// RoomControlArea.RoomControlAreaId - RoomControlSection.RoomControlAreaId
		/// </summary>
		public virtual IEntityRelation RoomControlSectionEntityUsingRoomControlAreaId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RoomControlSectionCollection" , true);
				relation.AddEntityFieldPair(RoomControlAreaFields.RoomControlAreaId, RoomControlSectionFields.RoomControlAreaId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlAreaEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlSectionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RoomControlAreaEntity and UIWidgetEntity over the 1:n relation they have, using the relation between the fields:
		/// RoomControlArea.RoomControlAreaId - UIWidget.RoomControlAreaId
		/// </summary>
		public virtual IEntityRelation UIWidgetEntityUsingRoomControlAreaId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UIWidgetCollection" , true);
				relation.AddEntityFieldPair(RoomControlAreaFields.RoomControlAreaId, UIWidgetFields.RoomControlAreaId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlAreaEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIWidgetEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between RoomControlAreaEntity and RoomControlConfigurationEntity over the m:1 relation they have, using the relation between the fields:
		/// RoomControlArea.RoomControlConfigurationId - RoomControlConfiguration.RoomControlConfigurationId
		/// </summary>
		public virtual IEntityRelation RoomControlConfigurationEntityUsingRoomControlConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RoomControlConfigurationEntity", false);
				relation.AddEntityFieldPair(RoomControlConfigurationFields.RoomControlConfigurationId, RoomControlAreaFields.RoomControlConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlConfigurationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlAreaEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticRoomControlAreaRelations
	{
		internal static readonly IEntityRelation ClientEntityUsingRoomControlAreaIdStatic = new RoomControlAreaRelations().ClientEntityUsingRoomControlAreaId;
		internal static readonly IEntityRelation CustomTextEntityUsingRoomControlAreaIdStatic = new RoomControlAreaRelations().CustomTextEntityUsingRoomControlAreaId;
		internal static readonly IEntityRelation DeliverypointEntityUsingRoomControlAreaIdStatic = new RoomControlAreaRelations().DeliverypointEntityUsingRoomControlAreaId;
		internal static readonly IEntityRelation RoomControlAreaLanguageEntityUsingRoomControlAreaIdStatic = new RoomControlAreaRelations().RoomControlAreaLanguageEntityUsingRoomControlAreaId;
		internal static readonly IEntityRelation RoomControlSectionEntityUsingRoomControlAreaIdStatic = new RoomControlAreaRelations().RoomControlSectionEntityUsingRoomControlAreaId;
		internal static readonly IEntityRelation UIWidgetEntityUsingRoomControlAreaIdStatic = new RoomControlAreaRelations().UIWidgetEntityUsingRoomControlAreaId;
		internal static readonly IEntityRelation RoomControlConfigurationEntityUsingRoomControlConfigurationIdStatic = new RoomControlAreaRelations().RoomControlConfigurationEntityUsingRoomControlConfigurationId;

		/// <summary>CTor</summary>
		static StaticRoomControlAreaRelations()
		{
		}
	}
}
