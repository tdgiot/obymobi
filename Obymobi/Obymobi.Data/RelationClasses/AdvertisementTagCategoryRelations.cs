﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: AdvertisementTagCategory. </summary>
	public partial class AdvertisementTagCategoryRelations
	{
		/// <summary>CTor</summary>
		public AdvertisementTagCategoryRelations()
		{
		}

		/// <summary>Gets all relations of the AdvertisementTagCategoryEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AdvertisementTagEntityUsingAdvertisementTagId);
			toReturn.Add(this.CategoryEntityUsingCategoryId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between AdvertisementTagCategoryEntity and AdvertisementTagEntity over the m:1 relation they have, using the relation between the fields:
		/// AdvertisementTagCategory.AdvertisementTagId - AdvertisementTag.AdvertisementTagId
		/// </summary>
		public virtual IEntityRelation AdvertisementTagEntityUsingAdvertisementTagId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AdvertisementTagEntity", false);
				relation.AddEntityFieldPair(AdvertisementTagFields.AdvertisementTagId, AdvertisementTagCategoryFields.AdvertisementTagId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementTagEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementTagCategoryEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AdvertisementTagCategoryEntity and CategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// AdvertisementTagCategory.CategoryId - Category.CategoryId
		/// </summary>
		public virtual IEntityRelation CategoryEntityUsingCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CategoryEntity", false);
				relation.AddEntityFieldPair(CategoryFields.CategoryId, AdvertisementTagCategoryFields.CategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementTagCategoryEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticAdvertisementTagCategoryRelations
	{
		internal static readonly IEntityRelation AdvertisementTagEntityUsingAdvertisementTagIdStatic = new AdvertisementTagCategoryRelations().AdvertisementTagEntityUsingAdvertisementTagId;
		internal static readonly IEntityRelation CategoryEntityUsingCategoryIdStatic = new AdvertisementTagCategoryRelations().CategoryEntityUsingCategoryId;

		/// <summary>CTor</summary>
		static StaticAdvertisementTagCategoryRelations()
		{
		}
	}
}
