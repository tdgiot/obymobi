﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: PriceLevel. </summary>
	public partial class PriceLevelRelations
	{
		/// <summary>CTor</summary>
		public PriceLevelRelations()
		{
		}

		/// <summary>Gets all relations of the PriceLevelEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.PriceLevelItemEntityUsingPriceLevelId);
			toReturn.Add(this.PriceScheduleItemEntityUsingPriceLevelId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between PriceLevelEntity and PriceLevelItemEntity over the 1:n relation they have, using the relation between the fields:
		/// PriceLevel.PriceLevelId - PriceLevelItem.PriceLevelId
		/// </summary>
		public virtual IEntityRelation PriceLevelItemEntityUsingPriceLevelId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PriceLevelItemCollection" , true);
				relation.AddEntityFieldPair(PriceLevelFields.PriceLevelId, PriceLevelItemFields.PriceLevelId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PriceLevelEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PriceLevelItemEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PriceLevelEntity and PriceScheduleItemEntity over the 1:n relation they have, using the relation between the fields:
		/// PriceLevel.PriceLevelId - PriceScheduleItem.PriceLevelId
		/// </summary>
		public virtual IEntityRelation PriceScheduleItemEntityUsingPriceLevelId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PriceScheduleItemCollection" , true);
				relation.AddEntityFieldPair(PriceLevelFields.PriceLevelId, PriceScheduleItemFields.PriceLevelId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PriceLevelEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PriceScheduleItemEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between PriceLevelEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// PriceLevel.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, PriceLevelFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PriceLevelEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPriceLevelRelations
	{
		internal static readonly IEntityRelation PriceLevelItemEntityUsingPriceLevelIdStatic = new PriceLevelRelations().PriceLevelItemEntityUsingPriceLevelId;
		internal static readonly IEntityRelation PriceScheduleItemEntityUsingPriceLevelIdStatic = new PriceLevelRelations().PriceScheduleItemEntityUsingPriceLevelId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new PriceLevelRelations().CompanyEntityUsingCompanyId;

		/// <summary>CTor</summary>
		static StaticPriceLevelRelations()
		{
		}
	}
}
