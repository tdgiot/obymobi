﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: PriceSchedule. </summary>
	public partial class PriceScheduleRelations
	{
		/// <summary>CTor</summary>
		public PriceScheduleRelations()
		{
		}

		/// <summary>Gets all relations of the PriceScheduleEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ClientConfigurationEntityUsingPriceScheduleId);
			toReturn.Add(this.DeliverypointgroupEntityUsingPriceScheduleId);
			toReturn.Add(this.PriceScheduleItemEntityUsingPriceScheduleId);
			toReturn.Add(this.TimestampEntityUsingPriceScheduleId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between PriceScheduleEntity and ClientConfigurationEntity over the 1:n relation they have, using the relation between the fields:
		/// PriceSchedule.PriceScheduleId - ClientConfiguration.PriceScheduleId
		/// </summary>
		public virtual IEntityRelation ClientConfigurationEntityUsingPriceScheduleId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ClientConfigurationCollection" , true);
				relation.AddEntityFieldPair(PriceScheduleFields.PriceScheduleId, ClientConfigurationFields.PriceScheduleId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PriceScheduleEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientConfigurationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PriceScheduleEntity and DeliverypointgroupEntity over the 1:n relation they have, using the relation between the fields:
		/// PriceSchedule.PriceScheduleId - Deliverypointgroup.PriceScheduleId
		/// </summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingPriceScheduleId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeliverypointgroupCollection" , true);
				relation.AddEntityFieldPair(PriceScheduleFields.PriceScheduleId, DeliverypointgroupFields.PriceScheduleId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PriceScheduleEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PriceScheduleEntity and PriceScheduleItemEntity over the 1:n relation they have, using the relation between the fields:
		/// PriceSchedule.PriceScheduleId - PriceScheduleItem.PriceScheduleId
		/// </summary>
		public virtual IEntityRelation PriceScheduleItemEntityUsingPriceScheduleId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PriceScheduleItemCollection" , true);
				relation.AddEntityFieldPair(PriceScheduleFields.PriceScheduleId, PriceScheduleItemFields.PriceScheduleId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PriceScheduleEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PriceScheduleItemEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PriceScheduleEntity and TimestampEntity over the 1:1 relation they have, using the relation between the fields:
		/// PriceSchedule.PriceScheduleId - Timestamp.PriceScheduleId
		/// </summary>
		public virtual IEntityRelation TimestampEntityUsingPriceScheduleId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "TimestampCollection", true);


				relation.AddEntityFieldPair(PriceScheduleFields.PriceScheduleId, TimestampFields.PriceScheduleId);


				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PriceScheduleEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TimestampEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PriceScheduleEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// PriceSchedule.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, PriceScheduleFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PriceScheduleEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPriceScheduleRelations
	{
		internal static readonly IEntityRelation ClientConfigurationEntityUsingPriceScheduleIdStatic = new PriceScheduleRelations().ClientConfigurationEntityUsingPriceScheduleId;
		internal static readonly IEntityRelation DeliverypointgroupEntityUsingPriceScheduleIdStatic = new PriceScheduleRelations().DeliverypointgroupEntityUsingPriceScheduleId;
		internal static readonly IEntityRelation PriceScheduleItemEntityUsingPriceScheduleIdStatic = new PriceScheduleRelations().PriceScheduleItemEntityUsingPriceScheduleId;
		internal static readonly IEntityRelation TimestampEntityUsingPriceScheduleIdStatic = new PriceScheduleRelations().TimestampEntityUsingPriceScheduleId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new PriceScheduleRelations().CompanyEntityUsingCompanyId;

		/// <summary>CTor</summary>
		static StaticPriceScheduleRelations()
		{
		}
	}
}
