﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: AdvertisementTag. </summary>
	public partial class AdvertisementTagRelations
	{
		/// <summary>CTor</summary>
		public AdvertisementTagRelations()
		{
		}

		/// <summary>Gets all relations of the AdvertisementTagEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AdvertisementTagAdvertisementEntityUsingAdvertisementTagId);
			toReturn.Add(this.AdvertisementTagCategoryEntityUsingAdvertisementTagId);
			toReturn.Add(this.AdvertisementTagEntertainmentEntityUsingAdvertisementTagId);
			toReturn.Add(this.AdvertisementTagGenericproductEntityUsingAdvertisementTagId);
			toReturn.Add(this.AdvertisementTagLanguageEntityUsingAdvertisementTagId);
			toReturn.Add(this.AdvertisementTagProductEntityUsingAdvertisementTagId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between AdvertisementTagEntity and AdvertisementTagAdvertisementEntity over the 1:n relation they have, using the relation between the fields:
		/// AdvertisementTag.AdvertisementTagId - AdvertisementTagAdvertisement.AdvertisementTagId
		/// </summary>
		public virtual IEntityRelation AdvertisementTagAdvertisementEntityUsingAdvertisementTagId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AdvertisementTagAdvertisementCollection" , true);
				relation.AddEntityFieldPair(AdvertisementTagFields.AdvertisementTagId, AdvertisementTagAdvertisementFields.AdvertisementTagId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementTagEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementTagAdvertisementEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AdvertisementTagEntity and AdvertisementTagCategoryEntity over the 1:n relation they have, using the relation between the fields:
		/// AdvertisementTag.AdvertisementTagId - AdvertisementTagCategory.AdvertisementTagId
		/// </summary>
		public virtual IEntityRelation AdvertisementTagCategoryEntityUsingAdvertisementTagId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AdvertisementTagCategoryCollection" , true);
				relation.AddEntityFieldPair(AdvertisementTagFields.AdvertisementTagId, AdvertisementTagCategoryFields.AdvertisementTagId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementTagEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementTagCategoryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AdvertisementTagEntity and AdvertisementTagEntertainmentEntity over the 1:n relation they have, using the relation between the fields:
		/// AdvertisementTag.AdvertisementTagId - AdvertisementTagEntertainment.AdvertisementTagId
		/// </summary>
		public virtual IEntityRelation AdvertisementTagEntertainmentEntityUsingAdvertisementTagId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AdvertisementTagEntertainmentCollection" , true);
				relation.AddEntityFieldPair(AdvertisementTagFields.AdvertisementTagId, AdvertisementTagEntertainmentFields.AdvertisementTagId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementTagEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementTagEntertainmentEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AdvertisementTagEntity and AdvertisementTagGenericproductEntity over the 1:n relation they have, using the relation between the fields:
		/// AdvertisementTag.AdvertisementTagId - AdvertisementTagGenericproduct.AdvertisementTagId
		/// </summary>
		public virtual IEntityRelation AdvertisementTagGenericproductEntityUsingAdvertisementTagId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AdvertisementTagGenericproductCollection" , true);
				relation.AddEntityFieldPair(AdvertisementTagFields.AdvertisementTagId, AdvertisementTagGenericproductFields.AdvertisementTagId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementTagEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementTagGenericproductEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AdvertisementTagEntity and AdvertisementTagLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// AdvertisementTag.AdvertisementTagId - AdvertisementTagLanguage.AdvertisementTagId
		/// </summary>
		public virtual IEntityRelation AdvertisementTagLanguageEntityUsingAdvertisementTagId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AdvertisementTagLanguageCollection" , true);
				relation.AddEntityFieldPair(AdvertisementTagFields.AdvertisementTagId, AdvertisementTagLanguageFields.AdvertisementTagId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementTagEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementTagLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AdvertisementTagEntity and AdvertisementTagProductEntity over the 1:n relation they have, using the relation between the fields:
		/// AdvertisementTag.AdvertisementTagId - AdvertisementTagProduct.AdvertisementTagId
		/// </summary>
		public virtual IEntityRelation AdvertisementTagProductEntityUsingAdvertisementTagId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AdvertisementTagProductCollection" , true);
				relation.AddEntityFieldPair(AdvertisementTagFields.AdvertisementTagId, AdvertisementTagProductFields.AdvertisementTagId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementTagEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementTagProductEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticAdvertisementTagRelations
	{
		internal static readonly IEntityRelation AdvertisementTagAdvertisementEntityUsingAdvertisementTagIdStatic = new AdvertisementTagRelations().AdvertisementTagAdvertisementEntityUsingAdvertisementTagId;
		internal static readonly IEntityRelation AdvertisementTagCategoryEntityUsingAdvertisementTagIdStatic = new AdvertisementTagRelations().AdvertisementTagCategoryEntityUsingAdvertisementTagId;
		internal static readonly IEntityRelation AdvertisementTagEntertainmentEntityUsingAdvertisementTagIdStatic = new AdvertisementTagRelations().AdvertisementTagEntertainmentEntityUsingAdvertisementTagId;
		internal static readonly IEntityRelation AdvertisementTagGenericproductEntityUsingAdvertisementTagIdStatic = new AdvertisementTagRelations().AdvertisementTagGenericproductEntityUsingAdvertisementTagId;
		internal static readonly IEntityRelation AdvertisementTagLanguageEntityUsingAdvertisementTagIdStatic = new AdvertisementTagRelations().AdvertisementTagLanguageEntityUsingAdvertisementTagId;
		internal static readonly IEntityRelation AdvertisementTagProductEntityUsingAdvertisementTagIdStatic = new AdvertisementTagRelations().AdvertisementTagProductEntityUsingAdvertisementTagId;

		/// <summary>CTor</summary>
		static StaticAdvertisementTagRelations()
		{
		}
	}
}
