﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Map. </summary>
	public partial class MapRelations
	{
		/// <summary>CTor</summary>
		public MapRelations()
		{
		}

		/// <summary>Gets all relations of the MapEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.MapPointOfInterestEntityUsingMapId);
			toReturn.Add(this.UITabEntityUsingMapId);
			toReturn.Add(this.TimestampEntityUsingMapId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between MapEntity and MapPointOfInterestEntity over the 1:n relation they have, using the relation between the fields:
		/// Map.MapId - MapPointOfInterest.MapId
		/// </summary>
		public virtual IEntityRelation MapPointOfInterestEntityUsingMapId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MapPointOfInterestCollection" , true);
				relation.AddEntityFieldPair(MapFields.MapId, MapPointOfInterestFields.MapId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MapEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MapPointOfInterestEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between MapEntity and UITabEntity over the 1:n relation they have, using the relation between the fields:
		/// Map.MapId - UITab.MapId
		/// </summary>
		public virtual IEntityRelation UITabEntityUsingMapId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UITabCollection" , true);
				relation.AddEntityFieldPair(MapFields.MapId, UITabFields.MapId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MapEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UITabEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between MapEntity and TimestampEntity over the 1:1 relation they have, using the relation between the fields:
		/// Map.MapId - Timestamp.MapId
		/// </summary>
		public virtual IEntityRelation TimestampEntityUsingMapId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "TimestampCollection", true);


				relation.AddEntityFieldPair(MapFields.MapId, TimestampFields.MapId);


				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MapEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TimestampEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between MapEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// Map.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, MapFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MapEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticMapRelations
	{
		internal static readonly IEntityRelation MapPointOfInterestEntityUsingMapIdStatic = new MapRelations().MapPointOfInterestEntityUsingMapId;
		internal static readonly IEntityRelation UITabEntityUsingMapIdStatic = new MapRelations().UITabEntityUsingMapId;
		internal static readonly IEntityRelation TimestampEntityUsingMapIdStatic = new MapRelations().TimestampEntityUsingMapId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new MapRelations().CompanyEntityUsingCompanyId;

		/// <summary>CTor</summary>
		static StaticMapRelations()
		{
		}
	}
}
