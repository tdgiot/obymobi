﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: PointOfInterestVenueCategory. </summary>
	public partial class PointOfInterestVenueCategoryRelations
	{
		/// <summary>CTor</summary>
		public PointOfInterestVenueCategoryRelations()
		{
		}

		/// <summary>Gets all relations of the PointOfInterestVenueCategoryEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.PointOfInterestEntityUsingPointOfInterestId);
			toReturn.Add(this.VenueCategoryEntityUsingVenueCategoryId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between PointOfInterestVenueCategoryEntity and PointOfInterestEntity over the m:1 relation they have, using the relation between the fields:
		/// PointOfInterestVenueCategory.PointOfInterestId - PointOfInterest.PointOfInterestId
		/// </summary>
		public virtual IEntityRelation PointOfInterestEntityUsingPointOfInterestId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PointOfInterestEntity", false);
				relation.AddEntityFieldPair(PointOfInterestFields.PointOfInterestId, PointOfInterestVenueCategoryFields.PointOfInterestId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PointOfInterestEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PointOfInterestVenueCategoryEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between PointOfInterestVenueCategoryEntity and VenueCategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// PointOfInterestVenueCategory.VenueCategoryId - VenueCategory.VenueCategoryId
		/// </summary>
		public virtual IEntityRelation VenueCategoryEntityUsingVenueCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "VenueCategoryEntity", false);
				relation.AddEntityFieldPair(VenueCategoryFields.VenueCategoryId, PointOfInterestVenueCategoryFields.VenueCategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VenueCategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PointOfInterestVenueCategoryEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPointOfInterestVenueCategoryRelations
	{
		internal static readonly IEntityRelation PointOfInterestEntityUsingPointOfInterestIdStatic = new PointOfInterestVenueCategoryRelations().PointOfInterestEntityUsingPointOfInterestId;
		internal static readonly IEntityRelation VenueCategoryEntityUsingVenueCategoryIdStatic = new PointOfInterestVenueCategoryRelations().VenueCategoryEntityUsingVenueCategoryId;

		/// <summary>CTor</summary>
		static StaticPointOfInterestVenueCategoryRelations()
		{
		}
	}
}
