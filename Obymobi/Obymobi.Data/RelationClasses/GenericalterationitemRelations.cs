﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Genericalterationitem. </summary>
	public partial class GenericalterationitemRelations
	{
		/// <summary>CTor</summary>
		public GenericalterationitemRelations()
		{
		}

		/// <summary>Gets all relations of the GenericalterationitemEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.GenericalterationEntityUsingGenericalterationId);
			toReturn.Add(this.GenericalterationoptionEntityUsingGenericalterationoptionId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between GenericalterationitemEntity and GenericalterationEntity over the m:1 relation they have, using the relation between the fields:
		/// Genericalterationitem.GenericalterationId - Genericalteration.GenericalterationId
		/// </summary>
		public virtual IEntityRelation GenericalterationEntityUsingGenericalterationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "GenericalterationEntity", false);
				relation.AddEntityFieldPair(GenericalterationFields.GenericalterationId, GenericalterationitemFields.GenericalterationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericalterationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericalterationitemEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between GenericalterationitemEntity and GenericalterationoptionEntity over the m:1 relation they have, using the relation between the fields:
		/// Genericalterationitem.GenericalterationoptionId - Genericalterationoption.GenericalterationoptionId
		/// </summary>
		public virtual IEntityRelation GenericalterationoptionEntityUsingGenericalterationoptionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "GenericalterationoptionEntity", false);
				relation.AddEntityFieldPair(GenericalterationoptionFields.GenericalterationoptionId, GenericalterationitemFields.GenericalterationoptionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericalterationoptionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericalterationitemEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticGenericalterationitemRelations
	{
		internal static readonly IEntityRelation GenericalterationEntityUsingGenericalterationIdStatic = new GenericalterationitemRelations().GenericalterationEntityUsingGenericalterationId;
		internal static readonly IEntityRelation GenericalterationoptionEntityUsingGenericalterationoptionIdStatic = new GenericalterationitemRelations().GenericalterationoptionEntityUsingGenericalterationoptionId;

		/// <summary>CTor</summary>
		static StaticGenericalterationitemRelations()
		{
		}
	}
}
