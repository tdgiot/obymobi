﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: TerminalConfiguration. </summary>
	public partial class TerminalConfigurationRelations
	{
		/// <summary>CTor</summary>
		public TerminalConfigurationRelations()
		{
		}

		/// <summary>Gets all relations of the TerminalConfigurationEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.TerminalEntityUsingTerminalConfigurationId);
			toReturn.Add(this.TimestampEntityUsingTerminalConfigurationId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			toReturn.Add(this.TerminalEntityUsingTerminalId);
			toReturn.Add(this.UIModeEntityUsingUIModeId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between TerminalConfigurationEntity and TerminalEntity over the 1:n relation they have, using the relation between the fields:
		/// TerminalConfiguration.TerminalConfigurationId - Terminal.TerminalConfigurationId
		/// </summary>
		public virtual IEntityRelation TerminalEntityUsingTerminalConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TerminalCollection" , true);
				relation.AddEntityFieldPair(TerminalConfigurationFields.TerminalConfigurationId, TerminalFields.TerminalConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalConfigurationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TerminalConfigurationEntity and TimestampEntity over the 1:1 relation they have, using the relation between the fields:
		/// TerminalConfiguration.TerminalConfigurationId - Timestamp.TerminalConfigurationId
		/// </summary>
		public virtual IEntityRelation TimestampEntityUsingTerminalConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "TimestampCollection", true);


				relation.AddEntityFieldPair(TerminalConfigurationFields.TerminalConfigurationId, TimestampFields.TerminalConfigurationId);


				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalConfigurationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TimestampEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TerminalConfigurationEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// TerminalConfiguration.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, TerminalConfigurationFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalConfigurationEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TerminalConfigurationEntity and TerminalEntity over the m:1 relation they have, using the relation between the fields:
		/// TerminalConfiguration.TerminalId - Terminal.TerminalId
		/// </summary>
		public virtual IEntityRelation TerminalEntityUsingTerminalId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TerminalEntity", false);
				relation.AddEntityFieldPair(TerminalFields.TerminalId, TerminalConfigurationFields.TerminalId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalConfigurationEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TerminalConfigurationEntity and UIModeEntity over the m:1 relation they have, using the relation between the fields:
		/// TerminalConfiguration.UIModeId - UIMode.UIModeId
		/// </summary>
		public virtual IEntityRelation UIModeEntityUsingUIModeId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UIModeEntity", false);
				relation.AddEntityFieldPair(UIModeFields.UIModeId, TerminalConfigurationFields.UIModeId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIModeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalConfigurationEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticTerminalConfigurationRelations
	{
		internal static readonly IEntityRelation TerminalEntityUsingTerminalConfigurationIdStatic = new TerminalConfigurationRelations().TerminalEntityUsingTerminalConfigurationId;
		internal static readonly IEntityRelation TimestampEntityUsingTerminalConfigurationIdStatic = new TerminalConfigurationRelations().TimestampEntityUsingTerminalConfigurationId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new TerminalConfigurationRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation TerminalEntityUsingTerminalIdStatic = new TerminalConfigurationRelations().TerminalEntityUsingTerminalId;
		internal static readonly IEntityRelation UIModeEntityUsingUIModeIdStatic = new TerminalConfigurationRelations().UIModeEntityUsingUIModeId;

		/// <summary>CTor</summary>
		static StaticTerminalConfigurationRelations()
		{
		}
	}
}
