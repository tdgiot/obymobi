﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: CarouselItem. </summary>
	public partial class CarouselItemRelations
	{
		/// <summary>CTor</summary>
		public CarouselItemRelations()
		{
		}

		/// <summary>Gets all relations of the CarouselItemEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CustomTextEntityUsingCarouselItemId);
			toReturn.Add(this.MediaEntityUsingCarouselItemId);
			toReturn.Add(this.WidgetActionButtonEntityUsingWidgetActionButtonId);
			toReturn.Add(this.WidgetCarouselEntityUsingWidgetCarouselId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between CarouselItemEntity and CustomTextEntity over the 1:n relation they have, using the relation between the fields:
		/// CarouselItem.CarouselItemId - CustomText.CarouselItemId
		/// </summary>
		public virtual IEntityRelation CustomTextEntityUsingCarouselItemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomTextCollection" , true);
				relation.AddEntityFieldPair(CarouselItemFields.CarouselItemId, CustomTextFields.CarouselItemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CarouselItemEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CarouselItemEntity and MediaEntity over the 1:n relation they have, using the relation between the fields:
		/// CarouselItem.CarouselItemId - Media.CarouselItemId
		/// </summary>
		public virtual IEntityRelation MediaEntityUsingCarouselItemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MediaCollection" , true);
				relation.AddEntityFieldPair(CarouselItemFields.CarouselItemId, MediaFields.CarouselItemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CarouselItemEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between CarouselItemEntity and WidgetActionButtonEntity over the m:1 relation they have, using the relation between the fields:
		/// CarouselItem.WidgetActionButtonId - WidgetActionButton.WidgetId
		/// </summary>
		public virtual IEntityRelation WidgetActionButtonEntityUsingWidgetActionButtonId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "WidgetActionButtonEntity", false);
				relation.AddEntityFieldPair(WidgetActionButtonFields.WidgetId, CarouselItemFields.WidgetActionButtonId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WidgetActionButtonEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CarouselItemEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CarouselItemEntity and WidgetCarouselEntity over the m:1 relation they have, using the relation between the fields:
		/// CarouselItem.WidgetCarouselId - WidgetCarousel.WidgetId
		/// </summary>
		public virtual IEntityRelation WidgetCarouselEntityUsingWidgetCarouselId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "WidgetCarouselEntity", false);
				relation.AddEntityFieldPair(WidgetCarouselFields.WidgetId, CarouselItemFields.WidgetCarouselId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WidgetCarouselEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CarouselItemEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCarouselItemRelations
	{
		internal static readonly IEntityRelation CustomTextEntityUsingCarouselItemIdStatic = new CarouselItemRelations().CustomTextEntityUsingCarouselItemId;
		internal static readonly IEntityRelation MediaEntityUsingCarouselItemIdStatic = new CarouselItemRelations().MediaEntityUsingCarouselItemId;
		internal static readonly IEntityRelation WidgetActionButtonEntityUsingWidgetActionButtonIdStatic = new CarouselItemRelations().WidgetActionButtonEntityUsingWidgetActionButtonId;
		internal static readonly IEntityRelation WidgetCarouselEntityUsingWidgetCarouselIdStatic = new CarouselItemRelations().WidgetCarouselEntityUsingWidgetCarouselId;

		/// <summary>CTor</summary>
		static StaticCarouselItemRelations()
		{
		}
	}
}
