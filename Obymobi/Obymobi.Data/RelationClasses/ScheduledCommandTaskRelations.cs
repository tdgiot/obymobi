﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ScheduledCommandTask. </summary>
	public partial class ScheduledCommandTaskRelations
	{
		/// <summary>CTor</summary>
		public ScheduledCommandTaskRelations()
		{
		}

		/// <summary>Gets all relations of the ScheduledCommandTaskEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ScheduledCommandEntityUsingScheduledCommandTaskId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ScheduledCommandTaskEntity and ScheduledCommandEntity over the 1:n relation they have, using the relation between the fields:
		/// ScheduledCommandTask.ScheduledCommandTaskId - ScheduledCommand.ScheduledCommandTaskId
		/// </summary>
		public virtual IEntityRelation ScheduledCommandEntityUsingScheduledCommandTaskId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ScheduledCommandCollection" , true);
				relation.AddEntityFieldPair(ScheduledCommandTaskFields.ScheduledCommandTaskId, ScheduledCommandFields.ScheduledCommandTaskId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduledCommandTaskEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduledCommandEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between ScheduledCommandTaskEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// ScheduledCommandTask.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, ScheduledCommandTaskFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduledCommandTaskEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticScheduledCommandTaskRelations
	{
		internal static readonly IEntityRelation ScheduledCommandEntityUsingScheduledCommandTaskIdStatic = new ScheduledCommandTaskRelations().ScheduledCommandEntityUsingScheduledCommandTaskId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new ScheduledCommandTaskRelations().CompanyEntityUsingCompanyId;

		/// <summary>CTor</summary>
		static StaticScheduledCommandTaskRelations()
		{
		}
	}
}
