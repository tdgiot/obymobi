﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Survey. </summary>
	public partial class SurveyRelations
	{
		/// <summary>CTor</summary>
		public SurveyRelations()
		{
		}

		/// <summary>Gets all relations of the SurveyEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.MediaEntityUsingSurveyId);
			toReturn.Add(this.SurveyLanguageEntityUsingSurveyId);
			toReturn.Add(this.SurveyPageEntityUsingSurveyId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between SurveyEntity and MediaEntity over the 1:n relation they have, using the relation between the fields:
		/// Survey.SurveyId - Media.SurveyId
		/// </summary>
		public virtual IEntityRelation MediaEntityUsingSurveyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MediaCollection" , true);
				relation.AddEntityFieldPair(SurveyFields.SurveyId, MediaFields.SurveyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SurveyEntity and SurveyLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Survey.SurveyId - SurveyLanguage.SurveyId
		/// </summary>
		public virtual IEntityRelation SurveyLanguageEntityUsingSurveyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SurveyLanguageCollection" , true);
				relation.AddEntityFieldPair(SurveyFields.SurveyId, SurveyLanguageFields.SurveyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SurveyEntity and SurveyPageEntity over the 1:n relation they have, using the relation between the fields:
		/// Survey.SurveyId - SurveyPage.SurveyId
		/// </summary>
		public virtual IEntityRelation SurveyPageEntityUsingSurveyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SurveyPageCollection" , true);
				relation.AddEntityFieldPair(SurveyFields.SurveyId, SurveyPageFields.SurveyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyPageEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between SurveyEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// Survey.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, SurveyFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticSurveyRelations
	{
		internal static readonly IEntityRelation MediaEntityUsingSurveyIdStatic = new SurveyRelations().MediaEntityUsingSurveyId;
		internal static readonly IEntityRelation SurveyLanguageEntityUsingSurveyIdStatic = new SurveyRelations().SurveyLanguageEntityUsingSurveyId;
		internal static readonly IEntityRelation SurveyPageEntityUsingSurveyIdStatic = new SurveyRelations().SurveyPageEntityUsingSurveyId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new SurveyRelations().CompanyEntityUsingCompanyId;

		/// <summary>CTor</summary>
		static StaticSurveyRelations()
		{
		}
	}
}
