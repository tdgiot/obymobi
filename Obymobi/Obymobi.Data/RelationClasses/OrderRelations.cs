﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Order. </summary>
	public partial class OrderRelations
	{
		/// <summary>CTor</summary>
		public OrderRelations()
		{
		}

		/// <summary>Gets all relations of the OrderEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AnalyticsProcessingTaskEntityUsingOrderId);
			toReturn.Add(this.ExternalSystemLogEntityUsingOrderId);
			toReturn.Add(this.MessageEntityUsingOrderId);
			toReturn.Add(this.OrderEntityUsingMasterOrderId);
			toReturn.Add(this.OrderitemEntityUsingOrderId);
			toReturn.Add(this.OrderNotificationLogEntityUsingOrderId);
			toReturn.Add(this.OrderRoutestephandlerEntityUsingOrderId);
			toReturn.Add(this.OrderRoutestephandlerHistoryEntityUsingOrderId);
			toReturn.Add(this.PaymentTransactionEntityUsingOrderId);
			toReturn.Add(this.ReceiptEntityUsingOrderId);
			toReturn.Add(this.TerminalLogEntityUsingOrderId);
			toReturn.Add(this.CheckoutMethodEntityUsingCheckoutMethodId);
			toReturn.Add(this.ClientEntityUsingClientId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			toReturn.Add(this.CurrencyEntityUsingCurrencyId);
			toReturn.Add(this.CustomerEntityUsingCustomerId);
			toReturn.Add(this.DeliveryInformationEntityUsingDeliveryInformationId);
			toReturn.Add(this.DeliverypointEntityUsingDeliverypointId);
			toReturn.Add(this.DeliverypointEntityUsingChargeToDeliverypointId);
			toReturn.Add(this.OrderEntityUsingOrderIdMasterOrderId);
			toReturn.Add(this.OutletEntityUsingOutletId);
			toReturn.Add(this.ServiceMethodEntityUsingServiceMethodId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between OrderEntity and AnalyticsProcessingTaskEntity over the 1:n relation they have, using the relation between the fields:
		/// Order.OrderId - AnalyticsProcessingTask.OrderId
		/// </summary>
		public virtual IEntityRelation AnalyticsProcessingTaskEntityUsingOrderId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AnalyticsProcessingTaskCollection" , true);
				relation.AddEntityFieldPair(OrderFields.OrderId, AnalyticsProcessingTaskFields.OrderId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AnalyticsProcessingTaskEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between OrderEntity and ExternalSystemLogEntity over the 1:n relation they have, using the relation between the fields:
		/// Order.OrderId - ExternalSystemLog.OrderId
		/// </summary>
		public virtual IEntityRelation ExternalSystemLogEntityUsingOrderId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ExternalSystemLogCollection" , true);
				relation.AddEntityFieldPair(OrderFields.OrderId, ExternalSystemLogFields.OrderId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalSystemLogEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between OrderEntity and MessageEntity over the 1:n relation they have, using the relation between the fields:
		/// Order.OrderId - Message.OrderId
		/// </summary>
		public virtual IEntityRelation MessageEntityUsingOrderId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MessageCollection" , true);
				relation.AddEntityFieldPair(OrderFields.OrderId, MessageFields.OrderId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between OrderEntity and OrderEntity over the 1:n relation they have, using the relation between the fields:
		/// Order.OrderId - Order.MasterOrderId
		/// </summary>
		public virtual IEntityRelation OrderEntityUsingMasterOrderId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrderCollection" , true);
				relation.AddEntityFieldPair(OrderFields.OrderId, OrderFields.MasterOrderId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between OrderEntity and OrderitemEntity over the 1:n relation they have, using the relation between the fields:
		/// Order.OrderId - Orderitem.OrderId
		/// </summary>
		public virtual IEntityRelation OrderitemEntityUsingOrderId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrderitemCollection" , true);
				relation.AddEntityFieldPair(OrderFields.OrderId, OrderitemFields.OrderId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderitemEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between OrderEntity and OrderNotificationLogEntity over the 1:n relation they have, using the relation between the fields:
		/// Order.OrderId - OrderNotificationLog.OrderId
		/// </summary>
		public virtual IEntityRelation OrderNotificationLogEntityUsingOrderId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrderNotificationLogCollection" , true);
				relation.AddEntityFieldPair(OrderFields.OrderId, OrderNotificationLogFields.OrderId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderNotificationLogEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between OrderEntity and OrderRoutestephandlerEntity over the 1:n relation they have, using the relation between the fields:
		/// Order.OrderId - OrderRoutestephandler.OrderId
		/// </summary>
		public virtual IEntityRelation OrderRoutestephandlerEntityUsingOrderId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrderRoutestephandlerCollection" , true);
				relation.AddEntityFieldPair(OrderFields.OrderId, OrderRoutestephandlerFields.OrderId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderRoutestephandlerEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between OrderEntity and OrderRoutestephandlerHistoryEntity over the 1:n relation they have, using the relation between the fields:
		/// Order.OrderId - OrderRoutestephandlerHistory.OrderId
		/// </summary>
		public virtual IEntityRelation OrderRoutestephandlerHistoryEntityUsingOrderId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrderRoutestephandlerHistoryCollection" , true);
				relation.AddEntityFieldPair(OrderFields.OrderId, OrderRoutestephandlerHistoryFields.OrderId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderRoutestephandlerHistoryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between OrderEntity and PaymentTransactionEntity over the 1:n relation they have, using the relation between the fields:
		/// Order.OrderId - PaymentTransaction.OrderId
		/// </summary>
		public virtual IEntityRelation PaymentTransactionEntityUsingOrderId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PaymentTransactionCollection" , true);
				relation.AddEntityFieldPair(OrderFields.OrderId, PaymentTransactionFields.OrderId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentTransactionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between OrderEntity and ReceiptEntity over the 1:n relation they have, using the relation between the fields:
		/// Order.OrderId - Receipt.OrderId
		/// </summary>
		public virtual IEntityRelation ReceiptEntityUsingOrderId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ReceiptCollection" , true);
				relation.AddEntityFieldPair(OrderFields.OrderId, ReceiptFields.OrderId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReceiptEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between OrderEntity and TerminalLogEntity over the 1:n relation they have, using the relation between the fields:
		/// Order.OrderId - TerminalLog.OrderId
		/// </summary>
		public virtual IEntityRelation TerminalLogEntityUsingOrderId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TerminalLogCollection" , true);
				relation.AddEntityFieldPair(OrderFields.OrderId, TerminalLogFields.OrderId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalLogEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between OrderEntity and CheckoutMethodEntity over the m:1 relation they have, using the relation between the fields:
		/// Order.CheckoutMethodId - CheckoutMethod.CheckoutMethodId
		/// </summary>
		public virtual IEntityRelation CheckoutMethodEntityUsingCheckoutMethodId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CheckoutMethodEntity", false);
				relation.AddEntityFieldPair(CheckoutMethodFields.CheckoutMethodId, OrderFields.CheckoutMethodId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CheckoutMethodEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OrderEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// Order.ClientId - Client.ClientId
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingClientId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ClientEntity", false);
				relation.AddEntityFieldPair(ClientFields.ClientId, OrderFields.ClientId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OrderEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// Order.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, OrderFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OrderEntity and CurrencyEntity over the m:1 relation they have, using the relation between the fields:
		/// Order.CurrencyId - Currency.CurrencyId
		/// </summary>
		public virtual IEntityRelation CurrencyEntityUsingCurrencyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CurrencyEntity", false);
				relation.AddEntityFieldPair(CurrencyFields.CurrencyId, OrderFields.CurrencyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CurrencyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OrderEntity and CustomerEntity over the m:1 relation they have, using the relation between the fields:
		/// Order.CustomerId - Customer.CustomerId
		/// </summary>
		public virtual IEntityRelation CustomerEntityUsingCustomerId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CustomerEntity", false);
				relation.AddEntityFieldPair(CustomerFields.CustomerId, OrderFields.CustomerId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomerEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OrderEntity and DeliveryInformationEntity over the m:1 relation they have, using the relation between the fields:
		/// Order.DeliveryInformationId - DeliveryInformation.DeliveryInformationId
		/// </summary>
		public virtual IEntityRelation DeliveryInformationEntityUsingDeliveryInformationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DeliveryInformationEntity", false);
				relation.AddEntityFieldPair(DeliveryInformationFields.DeliveryInformationId, OrderFields.DeliveryInformationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliveryInformationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OrderEntity and DeliverypointEntity over the m:1 relation they have, using the relation between the fields:
		/// Order.DeliverypointId - Deliverypoint.DeliverypointId
		/// </summary>
		public virtual IEntityRelation DeliverypointEntityUsingDeliverypointId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DeliverypointEntity", false);
				relation.AddEntityFieldPair(DeliverypointFields.DeliverypointId, OrderFields.DeliverypointId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OrderEntity and DeliverypointEntity over the m:1 relation they have, using the relation between the fields:
		/// Order.ChargeToDeliverypointId - Deliverypoint.DeliverypointId
		/// </summary>
		public virtual IEntityRelation DeliverypointEntityUsingChargeToDeliverypointId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DeliverypointEntity_", false);
				relation.AddEntityFieldPair(DeliverypointFields.DeliverypointId, OrderFields.ChargeToDeliverypointId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OrderEntity and OrderEntity over the m:1 relation they have, using the relation between the fields:
		/// Order.MasterOrderId - Order.OrderId
		/// </summary>
		public virtual IEntityRelation OrderEntityUsingOrderIdMasterOrderId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "OrderEntity", false);
				relation.AddEntityFieldPair(OrderFields.OrderId, OrderFields.MasterOrderId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OrderEntity and OutletEntity over the m:1 relation they have, using the relation between the fields:
		/// Order.OutletId - Outlet.OutletId
		/// </summary>
		public virtual IEntityRelation OutletEntityUsingOutletId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "OutletEntity", false);
				relation.AddEntityFieldPair(OutletFields.OutletId, OrderFields.OutletId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OutletEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OrderEntity and ServiceMethodEntity over the m:1 relation they have, using the relation between the fields:
		/// Order.ServiceMethodId - ServiceMethod.ServiceMethodId
		/// </summary>
		public virtual IEntityRelation ServiceMethodEntityUsingServiceMethodId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ServiceMethodEntity", false);
				relation.AddEntityFieldPair(ServiceMethodFields.ServiceMethodId, OrderFields.ServiceMethodId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ServiceMethodEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticOrderRelations
	{
		internal static readonly IEntityRelation AnalyticsProcessingTaskEntityUsingOrderIdStatic = new OrderRelations().AnalyticsProcessingTaskEntityUsingOrderId;
		internal static readonly IEntityRelation ExternalSystemLogEntityUsingOrderIdStatic = new OrderRelations().ExternalSystemLogEntityUsingOrderId;
		internal static readonly IEntityRelation MessageEntityUsingOrderIdStatic = new OrderRelations().MessageEntityUsingOrderId;
		internal static readonly IEntityRelation OrderEntityUsingMasterOrderIdStatic = new OrderRelations().OrderEntityUsingMasterOrderId;
		internal static readonly IEntityRelation OrderitemEntityUsingOrderIdStatic = new OrderRelations().OrderitemEntityUsingOrderId;
		internal static readonly IEntityRelation OrderNotificationLogEntityUsingOrderIdStatic = new OrderRelations().OrderNotificationLogEntityUsingOrderId;
		internal static readonly IEntityRelation OrderRoutestephandlerEntityUsingOrderIdStatic = new OrderRelations().OrderRoutestephandlerEntityUsingOrderId;
		internal static readonly IEntityRelation OrderRoutestephandlerHistoryEntityUsingOrderIdStatic = new OrderRelations().OrderRoutestephandlerHistoryEntityUsingOrderId;
		internal static readonly IEntityRelation PaymentTransactionEntityUsingOrderIdStatic = new OrderRelations().PaymentTransactionEntityUsingOrderId;
		internal static readonly IEntityRelation ReceiptEntityUsingOrderIdStatic = new OrderRelations().ReceiptEntityUsingOrderId;
		internal static readonly IEntityRelation TerminalLogEntityUsingOrderIdStatic = new OrderRelations().TerminalLogEntityUsingOrderId;
		internal static readonly IEntityRelation CheckoutMethodEntityUsingCheckoutMethodIdStatic = new OrderRelations().CheckoutMethodEntityUsingCheckoutMethodId;
		internal static readonly IEntityRelation ClientEntityUsingClientIdStatic = new OrderRelations().ClientEntityUsingClientId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new OrderRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation CurrencyEntityUsingCurrencyIdStatic = new OrderRelations().CurrencyEntityUsingCurrencyId;
		internal static readonly IEntityRelation CustomerEntityUsingCustomerIdStatic = new OrderRelations().CustomerEntityUsingCustomerId;
		internal static readonly IEntityRelation DeliveryInformationEntityUsingDeliveryInformationIdStatic = new OrderRelations().DeliveryInformationEntityUsingDeliveryInformationId;
		internal static readonly IEntityRelation DeliverypointEntityUsingDeliverypointIdStatic = new OrderRelations().DeliverypointEntityUsingDeliverypointId;
		internal static readonly IEntityRelation DeliverypointEntityUsingChargeToDeliverypointIdStatic = new OrderRelations().DeliverypointEntityUsingChargeToDeliverypointId;
		internal static readonly IEntityRelation OrderEntityUsingOrderIdMasterOrderIdStatic = new OrderRelations().OrderEntityUsingOrderIdMasterOrderId;
		internal static readonly IEntityRelation OutletEntityUsingOutletIdStatic = new OrderRelations().OutletEntityUsingOutletId;
		internal static readonly IEntityRelation ServiceMethodEntityUsingServiceMethodIdStatic = new OrderRelations().ServiceMethodEntityUsingServiceMethodId;

		/// <summary>CTor</summary>
		static StaticOrderRelations()
		{
		}
	}
}
