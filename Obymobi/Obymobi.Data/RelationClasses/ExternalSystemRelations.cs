﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ExternalSystem. </summary>
	public partial class ExternalSystemRelations
	{
		/// <summary>CTor</summary>
		public ExternalSystemRelations()
		{
		}

		/// <summary>Gets all relations of the ExternalSystemEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ExternalDeliverypointEntityUsingExternalSystemId);
			toReturn.Add(this.ExternalMenuEntityUsingExternalSystemId);
			toReturn.Add(this.ExternalProductEntityUsingExternalSystemId);
			toReturn.Add(this.ExternalSystemLogEntityUsingExternalSystemId);
			toReturn.Add(this.OrderRoutestephandlerEntityUsingExternalSystemId);
			toReturn.Add(this.OrderRoutestephandlerHistoryEntityUsingExternalSystemId);
			toReturn.Add(this.RoutestephandlerEntityUsingExternalSystemId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ExternalSystemEntity and ExternalDeliverypointEntity over the 1:n relation they have, using the relation between the fields:
		/// ExternalSystem.ExternalSystemId - ExternalDeliverypoint.ExternalSystemId
		/// </summary>
		public virtual IEntityRelation ExternalDeliverypointEntityUsingExternalSystemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ExternalDeliverypointCollection" , true);
				relation.AddEntityFieldPair(ExternalSystemFields.ExternalSystemId, ExternalDeliverypointFields.ExternalSystemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalSystemEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalDeliverypointEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ExternalSystemEntity and ExternalMenuEntity over the 1:n relation they have, using the relation between the fields:
		/// ExternalSystem.ExternalSystemId - ExternalMenu.ExternalSystemId
		/// </summary>
		public virtual IEntityRelation ExternalMenuEntityUsingExternalSystemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ExternalMenuCollection" , true);
				relation.AddEntityFieldPair(ExternalSystemFields.ExternalSystemId, ExternalMenuFields.ExternalSystemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalSystemEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalMenuEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ExternalSystemEntity and ExternalProductEntity over the 1:n relation they have, using the relation between the fields:
		/// ExternalSystem.ExternalSystemId - ExternalProduct.ExternalSystemId
		/// </summary>
		public virtual IEntityRelation ExternalProductEntityUsingExternalSystemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ExternalProductCollection" , true);
				relation.AddEntityFieldPair(ExternalSystemFields.ExternalSystemId, ExternalProductFields.ExternalSystemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalSystemEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalProductEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ExternalSystemEntity and ExternalSystemLogEntity over the 1:n relation they have, using the relation between the fields:
		/// ExternalSystem.ExternalSystemId - ExternalSystemLog.ExternalSystemId
		/// </summary>
		public virtual IEntityRelation ExternalSystemLogEntityUsingExternalSystemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ExternalSystemLogCollection" , true);
				relation.AddEntityFieldPair(ExternalSystemFields.ExternalSystemId, ExternalSystemLogFields.ExternalSystemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalSystemEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalSystemLogEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ExternalSystemEntity and OrderRoutestephandlerEntity over the 1:n relation they have, using the relation between the fields:
		/// ExternalSystem.ExternalSystemId - OrderRoutestephandler.ExternalSystemId
		/// </summary>
		public virtual IEntityRelation OrderRoutestephandlerEntityUsingExternalSystemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrderRoutestephandlerCollection" , true);
				relation.AddEntityFieldPair(ExternalSystemFields.ExternalSystemId, OrderRoutestephandlerFields.ExternalSystemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalSystemEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderRoutestephandlerEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ExternalSystemEntity and OrderRoutestephandlerHistoryEntity over the 1:n relation they have, using the relation between the fields:
		/// ExternalSystem.ExternalSystemId - OrderRoutestephandlerHistory.ExternalSystemId
		/// </summary>
		public virtual IEntityRelation OrderRoutestephandlerHistoryEntityUsingExternalSystemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrderRoutestephandlerHistoryCollection" , true);
				relation.AddEntityFieldPair(ExternalSystemFields.ExternalSystemId, OrderRoutestephandlerHistoryFields.ExternalSystemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalSystemEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderRoutestephandlerHistoryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ExternalSystemEntity and RoutestephandlerEntity over the 1:n relation they have, using the relation between the fields:
		/// ExternalSystem.ExternalSystemId - Routestephandler.ExternalSystemId
		/// </summary>
		public virtual IEntityRelation RoutestephandlerEntityUsingExternalSystemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RoutestephandlerCollection" , true);
				relation.AddEntityFieldPair(ExternalSystemFields.ExternalSystemId, RoutestephandlerFields.ExternalSystemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalSystemEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoutestephandlerEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between ExternalSystemEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// ExternalSystem.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, ExternalSystemFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalSystemEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticExternalSystemRelations
	{
		internal static readonly IEntityRelation ExternalDeliverypointEntityUsingExternalSystemIdStatic = new ExternalSystemRelations().ExternalDeliverypointEntityUsingExternalSystemId;
		internal static readonly IEntityRelation ExternalMenuEntityUsingExternalSystemIdStatic = new ExternalSystemRelations().ExternalMenuEntityUsingExternalSystemId;
		internal static readonly IEntityRelation ExternalProductEntityUsingExternalSystemIdStatic = new ExternalSystemRelations().ExternalProductEntityUsingExternalSystemId;
		internal static readonly IEntityRelation ExternalSystemLogEntityUsingExternalSystemIdStatic = new ExternalSystemRelations().ExternalSystemLogEntityUsingExternalSystemId;
		internal static readonly IEntityRelation OrderRoutestephandlerEntityUsingExternalSystemIdStatic = new ExternalSystemRelations().OrderRoutestephandlerEntityUsingExternalSystemId;
		internal static readonly IEntityRelation OrderRoutestephandlerHistoryEntityUsingExternalSystemIdStatic = new ExternalSystemRelations().OrderRoutestephandlerHistoryEntityUsingExternalSystemId;
		internal static readonly IEntityRelation RoutestephandlerEntityUsingExternalSystemIdStatic = new ExternalSystemRelations().RoutestephandlerEntityUsingExternalSystemId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new ExternalSystemRelations().CompanyEntityUsingCompanyId;

		/// <summary>CTor</summary>
		static StaticExternalSystemRelations()
		{
		}
	}
}
