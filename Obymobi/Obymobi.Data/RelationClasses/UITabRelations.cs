﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: UITab. </summary>
	public partial class UITabRelations
	{
		/// <summary>CTor</summary>
		public UITabRelations()
		{
		}

		/// <summary>Gets all relations of the UITabEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CustomTextEntityUsingUITabId);
			toReturn.Add(this.UIModeEntityUsingDefaultUITabId);
			toReturn.Add(this.UITabLanguageEntityUsingUITabId);
			toReturn.Add(this.UIWidgetEntityUsingUITabId);
			toReturn.Add(this.CategoryEntityUsingCategoryId);
			toReturn.Add(this.EntertainmentEntityUsingEntertainmentId);
			toReturn.Add(this.MapEntityUsingMapId);
			toReturn.Add(this.SiteEntityUsingSiteId);
			toReturn.Add(this.UIModeEntityUsingUIModeId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between UITabEntity and CustomTextEntity over the 1:n relation they have, using the relation between the fields:
		/// UITab.UITabId - CustomText.UITabId
		/// </summary>
		public virtual IEntityRelation CustomTextEntityUsingUITabId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomTextCollection" , true);
				relation.AddEntityFieldPair(UITabFields.UITabId, CustomTextFields.UITabId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UITabEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UITabEntity and UIModeEntity over the 1:n relation they have, using the relation between the fields:
		/// UITab.UITabId - UIMode.DefaultUITabId
		/// </summary>
		public virtual IEntityRelation UIModeEntityUsingDefaultUITabId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UIModeCollection" , true);
				relation.AddEntityFieldPair(UITabFields.UITabId, UIModeFields.DefaultUITabId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UITabEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIModeEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UITabEntity and UITabLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// UITab.UITabId - UITabLanguage.UITabId
		/// </summary>
		public virtual IEntityRelation UITabLanguageEntityUsingUITabId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UITabLanguageCollection" , true);
				relation.AddEntityFieldPair(UITabFields.UITabId, UITabLanguageFields.UITabId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UITabEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UITabLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UITabEntity and UIWidgetEntity over the 1:n relation they have, using the relation between the fields:
		/// UITab.UITabId - UIWidget.UITabId
		/// </summary>
		public virtual IEntityRelation UIWidgetEntityUsingUITabId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UIWidgetCollection" , true);
				relation.AddEntityFieldPair(UITabFields.UITabId, UIWidgetFields.UITabId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UITabEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIWidgetEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between UITabEntity and CategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// UITab.CategoryId - Category.CategoryId
		/// </summary>
		public virtual IEntityRelation CategoryEntityUsingCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CategoryEntity", false);
				relation.AddEntityFieldPair(CategoryFields.CategoryId, UITabFields.CategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UITabEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between UITabEntity and EntertainmentEntity over the m:1 relation they have, using the relation between the fields:
		/// UITab.EntertainmentId - Entertainment.EntertainmentId
		/// </summary>
		public virtual IEntityRelation EntertainmentEntityUsingEntertainmentId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "EntertainmentEntity", false);
				relation.AddEntityFieldPair(EntertainmentFields.EntertainmentId, UITabFields.EntertainmentId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UITabEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between UITabEntity and MapEntity over the m:1 relation they have, using the relation between the fields:
		/// UITab.MapId - Map.MapId
		/// </summary>
		public virtual IEntityRelation MapEntityUsingMapId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "MapEntity", false);
				relation.AddEntityFieldPair(MapFields.MapId, UITabFields.MapId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MapEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UITabEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between UITabEntity and SiteEntity over the m:1 relation they have, using the relation between the fields:
		/// UITab.SiteId - Site.SiteId
		/// </summary>
		public virtual IEntityRelation SiteEntityUsingSiteId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SiteEntity", false);
				relation.AddEntityFieldPair(SiteFields.SiteId, UITabFields.SiteId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SiteEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UITabEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between UITabEntity and UIModeEntity over the m:1 relation they have, using the relation between the fields:
		/// UITab.UIModeId - UIMode.UIModeId
		/// </summary>
		public virtual IEntityRelation UIModeEntityUsingUIModeId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UIModeEntity", false);
				relation.AddEntityFieldPair(UIModeFields.UIModeId, UITabFields.UIModeId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIModeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UITabEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticUITabRelations
	{
		internal static readonly IEntityRelation CustomTextEntityUsingUITabIdStatic = new UITabRelations().CustomTextEntityUsingUITabId;
		internal static readonly IEntityRelation UIModeEntityUsingDefaultUITabIdStatic = new UITabRelations().UIModeEntityUsingDefaultUITabId;
		internal static readonly IEntityRelation UITabLanguageEntityUsingUITabIdStatic = new UITabRelations().UITabLanguageEntityUsingUITabId;
		internal static readonly IEntityRelation UIWidgetEntityUsingUITabIdStatic = new UITabRelations().UIWidgetEntityUsingUITabId;
		internal static readonly IEntityRelation CategoryEntityUsingCategoryIdStatic = new UITabRelations().CategoryEntityUsingCategoryId;
		internal static readonly IEntityRelation EntertainmentEntityUsingEntertainmentIdStatic = new UITabRelations().EntertainmentEntityUsingEntertainmentId;
		internal static readonly IEntityRelation MapEntityUsingMapIdStatic = new UITabRelations().MapEntityUsingMapId;
		internal static readonly IEntityRelation SiteEntityUsingSiteIdStatic = new UITabRelations().SiteEntityUsingSiteId;
		internal static readonly IEntityRelation UIModeEntityUsingUIModeIdStatic = new UITabRelations().UIModeEntityUsingUIModeId;

		/// <summary>CTor</summary>
		static StaticUITabRelations()
		{
		}
	}
}
