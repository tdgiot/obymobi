﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: UISchedule. </summary>
	public partial class UIScheduleRelations
	{
		/// <summary>CTor</summary>
		public UIScheduleRelations()
		{
		}

		/// <summary>Gets all relations of the UIScheduleEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ClientConfigurationEntityUsingUIScheduleId);
			toReturn.Add(this.DeliverypointgroupEntityUsingUIScheduleId);
			toReturn.Add(this.TimestampEntityUsingUIScheduleId);
			toReturn.Add(this.UIScheduleItemEntityUsingUIScheduleId);
			toReturn.Add(this.UIScheduleItemOccurrenceEntityUsingUIScheduleId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			toReturn.Add(this.DeliverypointgroupEntityUsingDeliverypointgroupId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between UIScheduleEntity and ClientConfigurationEntity over the 1:n relation they have, using the relation between the fields:
		/// UISchedule.UIScheduleId - ClientConfiguration.UIScheduleId
		/// </summary>
		public virtual IEntityRelation ClientConfigurationEntityUsingUIScheduleId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ClientConfigurationCollection" , true);
				relation.AddEntityFieldPair(UIScheduleFields.UIScheduleId, ClientConfigurationFields.UIScheduleId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIScheduleEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientConfigurationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UIScheduleEntity and DeliverypointgroupEntity over the 1:n relation they have, using the relation between the fields:
		/// UISchedule.UIScheduleId - Deliverypointgroup.UIScheduleId
		/// </summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingUIScheduleId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeliverypointgroupCollection" , true);
				relation.AddEntityFieldPair(UIScheduleFields.UIScheduleId, DeliverypointgroupFields.UIScheduleId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIScheduleEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UIScheduleEntity and TimestampEntity over the 1:n relation they have, using the relation between the fields:
		/// UISchedule.UIScheduleId - Timestamp.UIScheduleId
		/// </summary>
		public virtual IEntityRelation TimestampEntityUsingUIScheduleId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TimestampCollection" , true);
				relation.AddEntityFieldPair(UIScheduleFields.UIScheduleId, TimestampFields.UIScheduleId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIScheduleEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TimestampEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UIScheduleEntity and UIScheduleItemEntity over the 1:n relation they have, using the relation between the fields:
		/// UISchedule.UIScheduleId - UIScheduleItem.UIScheduleId
		/// </summary>
		public virtual IEntityRelation UIScheduleItemEntityUsingUIScheduleId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UIScheduleItemCollection" , true);
				relation.AddEntityFieldPair(UIScheduleFields.UIScheduleId, UIScheduleItemFields.UIScheduleId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIScheduleEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIScheduleItemEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UIScheduleEntity and UIScheduleItemOccurrenceEntity over the 1:n relation they have, using the relation between the fields:
		/// UISchedule.UIScheduleId - UIScheduleItemOccurrence.UIScheduleId
		/// </summary>
		public virtual IEntityRelation UIScheduleItemOccurrenceEntityUsingUIScheduleId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UIScheduleItemOccurrenceCollection" , true);
				relation.AddEntityFieldPair(UIScheduleFields.UIScheduleId, UIScheduleItemOccurrenceFields.UIScheduleId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIScheduleEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIScheduleItemOccurrenceEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between UIScheduleEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// UISchedule.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, UIScheduleFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIScheduleEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between UIScheduleEntity and DeliverypointgroupEntity over the m:1 relation they have, using the relation between the fields:
		/// UISchedule.DeliverypointgroupId - Deliverypointgroup.DeliverypointgroupId
		/// </summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingDeliverypointgroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DeliverypointgroupEntity", false);
				relation.AddEntityFieldPair(DeliverypointgroupFields.DeliverypointgroupId, UIScheduleFields.DeliverypointgroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIScheduleEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticUIScheduleRelations
	{
		internal static readonly IEntityRelation ClientConfigurationEntityUsingUIScheduleIdStatic = new UIScheduleRelations().ClientConfigurationEntityUsingUIScheduleId;
		internal static readonly IEntityRelation DeliverypointgroupEntityUsingUIScheduleIdStatic = new UIScheduleRelations().DeliverypointgroupEntityUsingUIScheduleId;
		internal static readonly IEntityRelation TimestampEntityUsingUIScheduleIdStatic = new UIScheduleRelations().TimestampEntityUsingUIScheduleId;
		internal static readonly IEntityRelation UIScheduleItemEntityUsingUIScheduleIdStatic = new UIScheduleRelations().UIScheduleItemEntityUsingUIScheduleId;
		internal static readonly IEntityRelation UIScheduleItemOccurrenceEntityUsingUIScheduleIdStatic = new UIScheduleRelations().UIScheduleItemOccurrenceEntityUsingUIScheduleId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new UIScheduleRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation DeliverypointgroupEntityUsingDeliverypointgroupIdStatic = new UIScheduleRelations().DeliverypointgroupEntityUsingDeliverypointgroupId;

		/// <summary>CTor</summary>
		static StaticUIScheduleRelations()
		{
		}
	}
}
