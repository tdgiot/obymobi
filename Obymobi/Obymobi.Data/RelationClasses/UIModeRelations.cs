﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: UIMode. </summary>
	public partial class UIModeRelations
	{
		/// <summary>CTor</summary>
		public UIModeRelations()
		{
		}

		/// <summary>Gets all relations of the UIModeEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ClientConfigurationEntityUsingUIModeId);
			toReturn.Add(this.DeliverypointgroupEntityUsingMobileUIModeId);
			toReturn.Add(this.DeliverypointgroupEntityUsingTabletUIModeId);
			toReturn.Add(this.DeliverypointgroupEntityUsingUIModeId);
			toReturn.Add(this.TerminalEntityUsingUIModeId);
			toReturn.Add(this.TerminalConfigurationEntityUsingUIModeId);
			toReturn.Add(this.TimestampEntityUsingUIModeId);
			toReturn.Add(this.UIFooterItemEntityUsingUIModeId);
			toReturn.Add(this.UITabEntityUsingUIModeId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			toReturn.Add(this.PointOfInterestEntityUsingPointOfInterestId);
			toReturn.Add(this.UITabEntityUsingDefaultUITabId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between UIModeEntity and ClientConfigurationEntity over the 1:n relation they have, using the relation between the fields:
		/// UIMode.UIModeId - ClientConfiguration.UIModeId
		/// </summary>
		public virtual IEntityRelation ClientConfigurationEntityUsingUIModeId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ClientConfigurationCollection" , true);
				relation.AddEntityFieldPair(UIModeFields.UIModeId, ClientConfigurationFields.UIModeId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIModeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientConfigurationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UIModeEntity and DeliverypointgroupEntity over the 1:n relation they have, using the relation between the fields:
		/// UIMode.UIModeId - Deliverypointgroup.MobileUIModeId
		/// </summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingMobileUIModeId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeliverypointgroupCollection_" , true);
				relation.AddEntityFieldPair(UIModeFields.UIModeId, DeliverypointgroupFields.MobileUIModeId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIModeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UIModeEntity and DeliverypointgroupEntity over the 1:n relation they have, using the relation between the fields:
		/// UIMode.UIModeId - Deliverypointgroup.TabletUIModeId
		/// </summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingTabletUIModeId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeliverypointgroupCollection__" , true);
				relation.AddEntityFieldPair(UIModeFields.UIModeId, DeliverypointgroupFields.TabletUIModeId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIModeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UIModeEntity and DeliverypointgroupEntity over the 1:n relation they have, using the relation between the fields:
		/// UIMode.UIModeId - Deliverypointgroup.UIModeId
		/// </summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingUIModeId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeliverypointgroupCollection" , true);
				relation.AddEntityFieldPair(UIModeFields.UIModeId, DeliverypointgroupFields.UIModeId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIModeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UIModeEntity and TerminalEntity over the 1:n relation they have, using the relation between the fields:
		/// UIMode.UIModeId - Terminal.UIModeId
		/// </summary>
		public virtual IEntityRelation TerminalEntityUsingUIModeId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TerminalCollection" , true);
				relation.AddEntityFieldPair(UIModeFields.UIModeId, TerminalFields.UIModeId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIModeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UIModeEntity and TerminalConfigurationEntity over the 1:n relation they have, using the relation between the fields:
		/// UIMode.UIModeId - TerminalConfiguration.UIModeId
		/// </summary>
		public virtual IEntityRelation TerminalConfigurationEntityUsingUIModeId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TerminalConfigurationCollection" , true);
				relation.AddEntityFieldPair(UIModeFields.UIModeId, TerminalConfigurationFields.UIModeId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIModeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalConfigurationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UIModeEntity and TimestampEntity over the 1:n relation they have, using the relation between the fields:
		/// UIMode.UIModeId - Timestamp.UIModeId
		/// </summary>
		public virtual IEntityRelation TimestampEntityUsingUIModeId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TimestampCollection" , true);
				relation.AddEntityFieldPair(UIModeFields.UIModeId, TimestampFields.UIModeId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIModeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TimestampEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UIModeEntity and UIFooterItemEntity over the 1:n relation they have, using the relation between the fields:
		/// UIMode.UIModeId - UIFooterItem.UIModeId
		/// </summary>
		public virtual IEntityRelation UIFooterItemEntityUsingUIModeId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UIFooterItemCollection" , true);
				relation.AddEntityFieldPair(UIModeFields.UIModeId, UIFooterItemFields.UIModeId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIModeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIFooterItemEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UIModeEntity and UITabEntity over the 1:n relation they have, using the relation between the fields:
		/// UIMode.UIModeId - UITab.UIModeId
		/// </summary>
		public virtual IEntityRelation UITabEntityUsingUIModeId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UITabCollection" , true);
				relation.AddEntityFieldPair(UIModeFields.UIModeId, UITabFields.UIModeId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIModeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UITabEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between UIModeEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// UIMode.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, UIModeFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIModeEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between UIModeEntity and PointOfInterestEntity over the m:1 relation they have, using the relation between the fields:
		/// UIMode.PointOfInterestId - PointOfInterest.PointOfInterestId
		/// </summary>
		public virtual IEntityRelation PointOfInterestEntityUsingPointOfInterestId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PointOfInterestEntity", false);
				relation.AddEntityFieldPair(PointOfInterestFields.PointOfInterestId, UIModeFields.PointOfInterestId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PointOfInterestEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIModeEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between UIModeEntity and UITabEntity over the m:1 relation they have, using the relation between the fields:
		/// UIMode.DefaultUITabId - UITab.UITabId
		/// </summary>
		public virtual IEntityRelation UITabEntityUsingDefaultUITabId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DefaultUITabEntity", false);
				relation.AddEntityFieldPair(UITabFields.UITabId, UIModeFields.DefaultUITabId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UITabEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIModeEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticUIModeRelations
	{
		internal static readonly IEntityRelation ClientConfigurationEntityUsingUIModeIdStatic = new UIModeRelations().ClientConfigurationEntityUsingUIModeId;
		internal static readonly IEntityRelation DeliverypointgroupEntityUsingMobileUIModeIdStatic = new UIModeRelations().DeliverypointgroupEntityUsingMobileUIModeId;
		internal static readonly IEntityRelation DeliverypointgroupEntityUsingTabletUIModeIdStatic = new UIModeRelations().DeliverypointgroupEntityUsingTabletUIModeId;
		internal static readonly IEntityRelation DeliverypointgroupEntityUsingUIModeIdStatic = new UIModeRelations().DeliverypointgroupEntityUsingUIModeId;
		internal static readonly IEntityRelation TerminalEntityUsingUIModeIdStatic = new UIModeRelations().TerminalEntityUsingUIModeId;
		internal static readonly IEntityRelation TerminalConfigurationEntityUsingUIModeIdStatic = new UIModeRelations().TerminalConfigurationEntityUsingUIModeId;
		internal static readonly IEntityRelation TimestampEntityUsingUIModeIdStatic = new UIModeRelations().TimestampEntityUsingUIModeId;
		internal static readonly IEntityRelation UIFooterItemEntityUsingUIModeIdStatic = new UIModeRelations().UIFooterItemEntityUsingUIModeId;
		internal static readonly IEntityRelation UITabEntityUsingUIModeIdStatic = new UIModeRelations().UITabEntityUsingUIModeId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new UIModeRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation PointOfInterestEntityUsingPointOfInterestIdStatic = new UIModeRelations().PointOfInterestEntityUsingPointOfInterestId;
		internal static readonly IEntityRelation UITabEntityUsingDefaultUITabIdStatic = new UIModeRelations().UITabEntityUsingDefaultUITabId;

		/// <summary>CTor</summary>
		static StaticUIModeRelations()
		{
		}
	}
}
