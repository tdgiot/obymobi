﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: PmsReportColumn. </summary>
	public partial class PmsReportColumnRelations
	{
		/// <summary>CTor</summary>
		public PmsReportColumnRelations()
		{
		}

		/// <summary>Gets all relations of the PmsReportColumnEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.PmsReportConfigurationEntityUsingRoomNumberColumnId);
			toReturn.Add(this.PmsRuleEntityUsingPmsReportColumnId);
			toReturn.Add(this.PmsReportConfigurationEntityUsingPmsReportConfigurationId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between PmsReportColumnEntity and PmsReportConfigurationEntity over the 1:n relation they have, using the relation between the fields:
		/// PmsReportColumn.PmsReportColumnId - PmsReportConfiguration.RoomNumberColumnId
		/// </summary>
		public virtual IEntityRelation PmsReportConfigurationEntityUsingRoomNumberColumnId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PmsReportConfigurationCollection" , true);
				relation.AddEntityFieldPair(PmsReportColumnFields.PmsReportColumnId, PmsReportConfigurationFields.RoomNumberColumnId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PmsReportColumnEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PmsReportConfigurationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PmsReportColumnEntity and PmsRuleEntity over the 1:n relation they have, using the relation between the fields:
		/// PmsReportColumn.PmsReportColumnId - PmsRule.PmsReportColumnId
		/// </summary>
		public virtual IEntityRelation PmsRuleEntityUsingPmsReportColumnId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PmsRuleCollection" , true);
				relation.AddEntityFieldPair(PmsReportColumnFields.PmsReportColumnId, PmsRuleFields.PmsReportColumnId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PmsReportColumnEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PmsRuleEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between PmsReportColumnEntity and PmsReportConfigurationEntity over the m:1 relation they have, using the relation between the fields:
		/// PmsReportColumn.PmsReportConfigurationId - PmsReportConfiguration.PmsReportConfigurationId
		/// </summary>
		public virtual IEntityRelation PmsReportConfigurationEntityUsingPmsReportConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PmsReportConfigurationEntity", false);
				relation.AddEntityFieldPair(PmsReportConfigurationFields.PmsReportConfigurationId, PmsReportColumnFields.PmsReportConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PmsReportConfigurationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PmsReportColumnEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPmsReportColumnRelations
	{
		internal static readonly IEntityRelation PmsReportConfigurationEntityUsingRoomNumberColumnIdStatic = new PmsReportColumnRelations().PmsReportConfigurationEntityUsingRoomNumberColumnId;
		internal static readonly IEntityRelation PmsRuleEntityUsingPmsReportColumnIdStatic = new PmsReportColumnRelations().PmsRuleEntityUsingPmsReportColumnId;
		internal static readonly IEntityRelation PmsReportConfigurationEntityUsingPmsReportConfigurationIdStatic = new PmsReportColumnRelations().PmsReportConfigurationEntityUsingPmsReportConfigurationId;

		/// <summary>CTor</summary>
		static StaticPmsReportColumnRelations()
		{
		}
	}
}
