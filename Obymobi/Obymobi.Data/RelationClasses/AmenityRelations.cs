﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Amenity. </summary>
	public partial class AmenityRelations
	{
		/// <summary>CTor</summary>
		public AmenityRelations()
		{
		}

		/// <summary>Gets all relations of the AmenityEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AmenityLanguageEntityUsingAmenityId);
			toReturn.Add(this.CompanyAmenityEntityUsingAmenityId);
			toReturn.Add(this.CustomTextEntityUsingAmenityId);
			toReturn.Add(this.PointOfInterestAmenityEntityUsingAmenityId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between AmenityEntity and AmenityLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Amenity.AmenityId - AmenityLanguage.AmenityId
		/// </summary>
		public virtual IEntityRelation AmenityLanguageEntityUsingAmenityId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AmenityLanguageCollection" , true);
				relation.AddEntityFieldPair(AmenityFields.AmenityId, AmenityLanguageFields.AmenityId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AmenityEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AmenityLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AmenityEntity and CompanyAmenityEntity over the 1:n relation they have, using the relation between the fields:
		/// Amenity.AmenityId - CompanyAmenity.AmenityId
		/// </summary>
		public virtual IEntityRelation CompanyAmenityEntityUsingAmenityId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CompanyAmenityCollection" , true);
				relation.AddEntityFieldPair(AmenityFields.AmenityId, CompanyAmenityFields.AmenityId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AmenityEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyAmenityEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AmenityEntity and CustomTextEntity over the 1:n relation they have, using the relation between the fields:
		/// Amenity.AmenityId - CustomText.AmenityId
		/// </summary>
		public virtual IEntityRelation CustomTextEntityUsingAmenityId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomTextCollection" , true);
				relation.AddEntityFieldPair(AmenityFields.AmenityId, CustomTextFields.AmenityId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AmenityEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AmenityEntity and PointOfInterestAmenityEntity over the 1:n relation they have, using the relation between the fields:
		/// Amenity.AmenityId - PointOfInterestAmenity.AmenityId
		/// </summary>
		public virtual IEntityRelation PointOfInterestAmenityEntityUsingAmenityId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PointOfInterestAmenityCollection" , true);
				relation.AddEntityFieldPair(AmenityFields.AmenityId, PointOfInterestAmenityFields.AmenityId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AmenityEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PointOfInterestAmenityEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticAmenityRelations
	{
		internal static readonly IEntityRelation AmenityLanguageEntityUsingAmenityIdStatic = new AmenityRelations().AmenityLanguageEntityUsingAmenityId;
		internal static readonly IEntityRelation CompanyAmenityEntityUsingAmenityIdStatic = new AmenityRelations().CompanyAmenityEntityUsingAmenityId;
		internal static readonly IEntityRelation CustomTextEntityUsingAmenityIdStatic = new AmenityRelations().CustomTextEntityUsingAmenityId;
		internal static readonly IEntityRelation PointOfInterestAmenityEntityUsingAmenityIdStatic = new AmenityRelations().PointOfInterestAmenityEntityUsingAmenityId;

		/// <summary>CTor</summary>
		static StaticAmenityRelations()
		{
		}
	}
}
