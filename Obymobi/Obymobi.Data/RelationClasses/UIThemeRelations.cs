﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: UITheme. </summary>
	public partial class UIThemeRelations
	{
		/// <summary>CTor</summary>
		public UIThemeRelations()
		{
		}

		/// <summary>Gets all relations of the UIThemeEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ClientConfigurationEntityUsingUIThemeId);
			toReturn.Add(this.DeliverypointgroupEntityUsingUIThemeId);
			toReturn.Add(this.MediaEntityUsingUIThemeId);
			toReturn.Add(this.TimestampEntityUsingUIThemeId);
			toReturn.Add(this.UIThemeColorEntityUsingUIThemeId);
			toReturn.Add(this.UIThemeTextSizeEntityUsingUIThemeId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between UIThemeEntity and ClientConfigurationEntity over the 1:n relation they have, using the relation between the fields:
		/// UITheme.UIThemeId - ClientConfiguration.UIThemeId
		/// </summary>
		public virtual IEntityRelation ClientConfigurationEntityUsingUIThemeId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ClientConfigurationCollection" , true);
				relation.AddEntityFieldPair(UIThemeFields.UIThemeId, ClientConfigurationFields.UIThemeId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIThemeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientConfigurationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UIThemeEntity and DeliverypointgroupEntity over the 1:n relation they have, using the relation between the fields:
		/// UITheme.UIThemeId - Deliverypointgroup.UIThemeId
		/// </summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingUIThemeId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeliverypointgroupCollection" , true);
				relation.AddEntityFieldPair(UIThemeFields.UIThemeId, DeliverypointgroupFields.UIThemeId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIThemeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UIThemeEntity and MediaEntity over the 1:n relation they have, using the relation between the fields:
		/// UITheme.UIThemeId - Media.UIThemeId
		/// </summary>
		public virtual IEntityRelation MediaEntityUsingUIThemeId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MediaCollection" , true);
				relation.AddEntityFieldPair(UIThemeFields.UIThemeId, MediaFields.UIThemeId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIThemeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UIThemeEntity and TimestampEntity over the 1:n relation they have, using the relation between the fields:
		/// UITheme.UIThemeId - Timestamp.UIThemeId
		/// </summary>
		public virtual IEntityRelation TimestampEntityUsingUIThemeId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TimestampCollection" , true);
				relation.AddEntityFieldPair(UIThemeFields.UIThemeId, TimestampFields.UIThemeId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIThemeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TimestampEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UIThemeEntity and UIThemeColorEntity over the 1:n relation they have, using the relation between the fields:
		/// UITheme.UIThemeId - UIThemeColor.UIThemeId
		/// </summary>
		public virtual IEntityRelation UIThemeColorEntityUsingUIThemeId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UIThemeColorCollection" , true);
				relation.AddEntityFieldPair(UIThemeFields.UIThemeId, UIThemeColorFields.UIThemeId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIThemeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIThemeColorEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UIThemeEntity and UIThemeTextSizeEntity over the 1:n relation they have, using the relation between the fields:
		/// UITheme.UIThemeId - UIThemeTextSize.UIThemeId
		/// </summary>
		public virtual IEntityRelation UIThemeTextSizeEntityUsingUIThemeId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UIThemeTextSizeCollection" , true);
				relation.AddEntityFieldPair(UIThemeFields.UIThemeId, UIThemeTextSizeFields.UIThemeId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIThemeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIThemeTextSizeEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between UIThemeEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// UITheme.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, UIThemeFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIThemeEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticUIThemeRelations
	{
		internal static readonly IEntityRelation ClientConfigurationEntityUsingUIThemeIdStatic = new UIThemeRelations().ClientConfigurationEntityUsingUIThemeId;
		internal static readonly IEntityRelation DeliverypointgroupEntityUsingUIThemeIdStatic = new UIThemeRelations().DeliverypointgroupEntityUsingUIThemeId;
		internal static readonly IEntityRelation MediaEntityUsingUIThemeIdStatic = new UIThemeRelations().MediaEntityUsingUIThemeId;
		internal static readonly IEntityRelation TimestampEntityUsingUIThemeIdStatic = new UIThemeRelations().TimestampEntityUsingUIThemeId;
		internal static readonly IEntityRelation UIThemeColorEntityUsingUIThemeIdStatic = new UIThemeRelations().UIThemeColorEntityUsingUIThemeId;
		internal static readonly IEntityRelation UIThemeTextSizeEntityUsingUIThemeIdStatic = new UIThemeRelations().UIThemeTextSizeEntityUsingUIThemeId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new UIThemeRelations().CompanyEntityUsingCompanyId;

		/// <summary>CTor</summary>
		static StaticUIThemeRelations()
		{
		}
	}
}
