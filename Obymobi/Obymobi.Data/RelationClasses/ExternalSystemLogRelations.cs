﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ExternalSystemLog. </summary>
	public partial class ExternalSystemLogRelations
	{
		/// <summary>CTor</summary>
		public ExternalSystemLogRelations()
		{
		}

		/// <summary>Gets all relations of the ExternalSystemLogEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ExternalSystemEntityUsingExternalSystemId);
			toReturn.Add(this.OrderEntityUsingOrderId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between ExternalSystemLogEntity and ExternalSystemEntity over the m:1 relation they have, using the relation between the fields:
		/// ExternalSystemLog.ExternalSystemId - ExternalSystem.ExternalSystemId
		/// </summary>
		public virtual IEntityRelation ExternalSystemEntityUsingExternalSystemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ExternalSystemEntity", false);
				relation.AddEntityFieldPair(ExternalSystemFields.ExternalSystemId, ExternalSystemLogFields.ExternalSystemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalSystemEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalSystemLogEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ExternalSystemLogEntity and OrderEntity over the m:1 relation they have, using the relation between the fields:
		/// ExternalSystemLog.OrderId - Order.OrderId
		/// </summary>
		public virtual IEntityRelation OrderEntityUsingOrderId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "OrderEntity", false);
				relation.AddEntityFieldPair(OrderFields.OrderId, ExternalSystemLogFields.OrderId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalSystemLogEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticExternalSystemLogRelations
	{
		internal static readonly IEntityRelation ExternalSystemEntityUsingExternalSystemIdStatic = new ExternalSystemLogRelations().ExternalSystemEntityUsingExternalSystemId;
		internal static readonly IEntityRelation OrderEntityUsingOrderIdStatic = new ExternalSystemLogRelations().OrderEntityUsingOrderId;

		/// <summary>CTor</summary>
		static StaticExternalSystemLogRelations()
		{
		}
	}
}
