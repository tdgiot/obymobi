﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: PointOfInterest. </summary>
	public partial class PointOfInterestRelations
	{
		/// <summary>CTor</summary>
		public PointOfInterestRelations()
		{
		}

		/// <summary>Gets all relations of the PointOfInterestEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AccessCodePointOfInterestEntityUsingPointOfInterestId);
			toReturn.Add(this.BusinesshoursEntityUsingPointOfInterestId);
			toReturn.Add(this.CompanyManagementTaskEntityUsingPointOfInterestId);
			toReturn.Add(this.CustomTextEntityUsingPointOfInterestId);
			toReturn.Add(this.MapPointOfInterestEntityUsingPointOfInterestId);
			toReturn.Add(this.MediaEntityUsingPointOfInterestId);
			toReturn.Add(this.PointOfInterestAmenityEntityUsingPointOfInterestId);
			toReturn.Add(this.PointOfInterestLanguageEntityUsingPointOfInterestId);
			toReturn.Add(this.PointOfInterestVenueCategoryEntityUsingPointOfInterestId);
			toReturn.Add(this.ProductEntityUsingPointOfInterestId);
			toReturn.Add(this.UIModeEntityUsingPointOfInterestId);
			toReturn.Add(this.TimestampEntityUsingPointOfInterestId);
			toReturn.Add(this.ActionButtonEntityUsingActionButtonId);
			toReturn.Add(this.CountryEntityUsingCountryId);
			toReturn.Add(this.CurrencyEntityUsingCurrencyId);
			toReturn.Add(this.TimeZoneEntityUsingTimeZoneId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between PointOfInterestEntity and AccessCodePointOfInterestEntity over the 1:n relation they have, using the relation between the fields:
		/// PointOfInterest.PointOfInterestId - AccessCodePointOfInterest.PointOfInterestId
		/// </summary>
		public virtual IEntityRelation AccessCodePointOfInterestEntityUsingPointOfInterestId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AccessCodePointOfInterestCollection" , true);
				relation.AddEntityFieldPair(PointOfInterestFields.PointOfInterestId, AccessCodePointOfInterestFields.PointOfInterestId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PointOfInterestEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AccessCodePointOfInterestEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PointOfInterestEntity and BusinesshoursEntity over the 1:n relation they have, using the relation between the fields:
		/// PointOfInterest.PointOfInterestId - Businesshours.PointOfInterestId
		/// </summary>
		public virtual IEntityRelation BusinesshoursEntityUsingPointOfInterestId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "BusinesshoursCollection" , true);
				relation.AddEntityFieldPair(PointOfInterestFields.PointOfInterestId, BusinesshoursFields.PointOfInterestId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PointOfInterestEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinesshoursEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PointOfInterestEntity and CompanyManagementTaskEntity over the 1:n relation they have, using the relation between the fields:
		/// PointOfInterest.PointOfInterestId - CompanyManagementTask.PointOfInterestId
		/// </summary>
		public virtual IEntityRelation CompanyManagementTaskEntityUsingPointOfInterestId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CompanyManagementTaskCollection" , true);
				relation.AddEntityFieldPair(PointOfInterestFields.PointOfInterestId, CompanyManagementTaskFields.PointOfInterestId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PointOfInterestEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyManagementTaskEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PointOfInterestEntity and CustomTextEntity over the 1:n relation they have, using the relation between the fields:
		/// PointOfInterest.PointOfInterestId - CustomText.PointOfInterestId
		/// </summary>
		public virtual IEntityRelation CustomTextEntityUsingPointOfInterestId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomTextCollection" , true);
				relation.AddEntityFieldPair(PointOfInterestFields.PointOfInterestId, CustomTextFields.PointOfInterestId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PointOfInterestEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PointOfInterestEntity and MapPointOfInterestEntity over the 1:n relation they have, using the relation between the fields:
		/// PointOfInterest.PointOfInterestId - MapPointOfInterest.PointOfInterestId
		/// </summary>
		public virtual IEntityRelation MapPointOfInterestEntityUsingPointOfInterestId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MapPointOfInterestCollection" , true);
				relation.AddEntityFieldPair(PointOfInterestFields.PointOfInterestId, MapPointOfInterestFields.PointOfInterestId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PointOfInterestEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MapPointOfInterestEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PointOfInterestEntity and MediaEntity over the 1:n relation they have, using the relation between the fields:
		/// PointOfInterest.PointOfInterestId - Media.PointOfInterestId
		/// </summary>
		public virtual IEntityRelation MediaEntityUsingPointOfInterestId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MediaCollection" , true);
				relation.AddEntityFieldPair(PointOfInterestFields.PointOfInterestId, MediaFields.PointOfInterestId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PointOfInterestEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PointOfInterestEntity and PointOfInterestAmenityEntity over the 1:n relation they have, using the relation between the fields:
		/// PointOfInterest.PointOfInterestId - PointOfInterestAmenity.PointOfInterestId
		/// </summary>
		public virtual IEntityRelation PointOfInterestAmenityEntityUsingPointOfInterestId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PointOfInterestAmenityCollection" , true);
				relation.AddEntityFieldPair(PointOfInterestFields.PointOfInterestId, PointOfInterestAmenityFields.PointOfInterestId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PointOfInterestEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PointOfInterestAmenityEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PointOfInterestEntity and PointOfInterestLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// PointOfInterest.PointOfInterestId - PointOfInterestLanguage.PointOfInterestId
		/// </summary>
		public virtual IEntityRelation PointOfInterestLanguageEntityUsingPointOfInterestId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PointOfInterestLanguageCollection" , true);
				relation.AddEntityFieldPair(PointOfInterestFields.PointOfInterestId, PointOfInterestLanguageFields.PointOfInterestId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PointOfInterestEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PointOfInterestLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PointOfInterestEntity and PointOfInterestVenueCategoryEntity over the 1:n relation they have, using the relation between the fields:
		/// PointOfInterest.PointOfInterestId - PointOfInterestVenueCategory.PointOfInterestId
		/// </summary>
		public virtual IEntityRelation PointOfInterestVenueCategoryEntityUsingPointOfInterestId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PointOfInterestVenueCategoryCollection" , true);
				relation.AddEntityFieldPair(PointOfInterestFields.PointOfInterestId, PointOfInterestVenueCategoryFields.PointOfInterestId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PointOfInterestEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PointOfInterestVenueCategoryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PointOfInterestEntity and ProductEntity over the 1:n relation they have, using the relation between the fields:
		/// PointOfInterest.PointOfInterestId - Product.PointOfInterestId
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingPointOfInterestId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ProductCollection" , true);
				relation.AddEntityFieldPair(PointOfInterestFields.PointOfInterestId, ProductFields.PointOfInterestId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PointOfInterestEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PointOfInterestEntity and UIModeEntity over the 1:n relation they have, using the relation between the fields:
		/// PointOfInterest.PointOfInterestId - UIMode.PointOfInterestId
		/// </summary>
		public virtual IEntityRelation UIModeEntityUsingPointOfInterestId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UIModeCollection" , true);
				relation.AddEntityFieldPair(PointOfInterestFields.PointOfInterestId, UIModeFields.PointOfInterestId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PointOfInterestEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIModeEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PointOfInterestEntity and TimestampEntity over the 1:1 relation they have, using the relation between the fields:
		/// PointOfInterest.PointOfInterestId - Timestamp.PointOfInterestId
		/// </summary>
		public virtual IEntityRelation TimestampEntityUsingPointOfInterestId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "TimestampCollection", true);


				relation.AddEntityFieldPair(PointOfInterestFields.PointOfInterestId, TimestampFields.PointOfInterestId);


				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PointOfInterestEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TimestampEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PointOfInterestEntity and ActionButtonEntity over the m:1 relation they have, using the relation between the fields:
		/// PointOfInterest.ActionButtonId - ActionButton.ActionButtonId
		/// </summary>
		public virtual IEntityRelation ActionButtonEntityUsingActionButtonId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ActionButtonEntity", false);
				relation.AddEntityFieldPair(ActionButtonFields.ActionButtonId, PointOfInterestFields.ActionButtonId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ActionButtonEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PointOfInterestEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between PointOfInterestEntity and CountryEntity over the m:1 relation they have, using the relation between the fields:
		/// PointOfInterest.CountryId - Country.CountryId
		/// </summary>
		public virtual IEntityRelation CountryEntityUsingCountryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CountryEntity", false);
				relation.AddEntityFieldPair(CountryFields.CountryId, PointOfInterestFields.CountryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CountryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PointOfInterestEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between PointOfInterestEntity and CurrencyEntity over the m:1 relation they have, using the relation between the fields:
		/// PointOfInterest.CurrencyId - Currency.CurrencyId
		/// </summary>
		public virtual IEntityRelation CurrencyEntityUsingCurrencyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CurrencyEntity", false);
				relation.AddEntityFieldPair(CurrencyFields.CurrencyId, PointOfInterestFields.CurrencyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CurrencyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PointOfInterestEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between PointOfInterestEntity and TimeZoneEntity over the m:1 relation they have, using the relation between the fields:
		/// PointOfInterest.TimeZoneId - TimeZone.TimeZoneId
		/// </summary>
		public virtual IEntityRelation TimeZoneEntityUsingTimeZoneId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TimeZoneEntity", false);
				relation.AddEntityFieldPair(TimeZoneFields.TimeZoneId, PointOfInterestFields.TimeZoneId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TimeZoneEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PointOfInterestEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPointOfInterestRelations
	{
		internal static readonly IEntityRelation AccessCodePointOfInterestEntityUsingPointOfInterestIdStatic = new PointOfInterestRelations().AccessCodePointOfInterestEntityUsingPointOfInterestId;
		internal static readonly IEntityRelation BusinesshoursEntityUsingPointOfInterestIdStatic = new PointOfInterestRelations().BusinesshoursEntityUsingPointOfInterestId;
		internal static readonly IEntityRelation CompanyManagementTaskEntityUsingPointOfInterestIdStatic = new PointOfInterestRelations().CompanyManagementTaskEntityUsingPointOfInterestId;
		internal static readonly IEntityRelation CustomTextEntityUsingPointOfInterestIdStatic = new PointOfInterestRelations().CustomTextEntityUsingPointOfInterestId;
		internal static readonly IEntityRelation MapPointOfInterestEntityUsingPointOfInterestIdStatic = new PointOfInterestRelations().MapPointOfInterestEntityUsingPointOfInterestId;
		internal static readonly IEntityRelation MediaEntityUsingPointOfInterestIdStatic = new PointOfInterestRelations().MediaEntityUsingPointOfInterestId;
		internal static readonly IEntityRelation PointOfInterestAmenityEntityUsingPointOfInterestIdStatic = new PointOfInterestRelations().PointOfInterestAmenityEntityUsingPointOfInterestId;
		internal static readonly IEntityRelation PointOfInterestLanguageEntityUsingPointOfInterestIdStatic = new PointOfInterestRelations().PointOfInterestLanguageEntityUsingPointOfInterestId;
		internal static readonly IEntityRelation PointOfInterestVenueCategoryEntityUsingPointOfInterestIdStatic = new PointOfInterestRelations().PointOfInterestVenueCategoryEntityUsingPointOfInterestId;
		internal static readonly IEntityRelation ProductEntityUsingPointOfInterestIdStatic = new PointOfInterestRelations().ProductEntityUsingPointOfInterestId;
		internal static readonly IEntityRelation UIModeEntityUsingPointOfInterestIdStatic = new PointOfInterestRelations().UIModeEntityUsingPointOfInterestId;
		internal static readonly IEntityRelation TimestampEntityUsingPointOfInterestIdStatic = new PointOfInterestRelations().TimestampEntityUsingPointOfInterestId;
		internal static readonly IEntityRelation ActionButtonEntityUsingActionButtonIdStatic = new PointOfInterestRelations().ActionButtonEntityUsingActionButtonId;
		internal static readonly IEntityRelation CountryEntityUsingCountryIdStatic = new PointOfInterestRelations().CountryEntityUsingCountryId;
		internal static readonly IEntityRelation CurrencyEntityUsingCurrencyIdStatic = new PointOfInterestRelations().CurrencyEntityUsingCurrencyId;
		internal static readonly IEntityRelation TimeZoneEntityUsingTimeZoneIdStatic = new PointOfInterestRelations().TimeZoneEntityUsingTimeZoneId;

		/// <summary>CTor</summary>
		static StaticPointOfInterestRelations()
		{
		}
	}
}
