﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: RoomControlSection. </summary>
	public partial class RoomControlSectionRelations
	{
		/// <summary>CTor</summary>
		public RoomControlSectionRelations()
		{
		}

		/// <summary>Gets all relations of the RoomControlSectionEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CustomTextEntityUsingRoomControlSectionId);
			toReturn.Add(this.MediaEntityUsingRoomControlSectionId);
			toReturn.Add(this.RoomControlComponentEntityUsingRoomControlSectionId);
			toReturn.Add(this.RoomControlSectionItemEntityUsingRoomControlSectionId);
			toReturn.Add(this.RoomControlSectionLanguageEntityUsingRoomControlSectionId);
			toReturn.Add(this.RoomControlWidgetEntityUsingRoomControlSectionId);
			toReturn.Add(this.UIWidgetEntityUsingRoomControlSectionId);
			toReturn.Add(this.RoomControlAreaEntityUsingRoomControlAreaId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between RoomControlSectionEntity and CustomTextEntity over the 1:n relation they have, using the relation between the fields:
		/// RoomControlSection.RoomControlSectionId - CustomText.RoomControlSectionId
		/// </summary>
		public virtual IEntityRelation CustomTextEntityUsingRoomControlSectionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomTextCollection" , true);
				relation.AddEntityFieldPair(RoomControlSectionFields.RoomControlSectionId, CustomTextFields.RoomControlSectionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlSectionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RoomControlSectionEntity and MediaEntity over the 1:n relation they have, using the relation between the fields:
		/// RoomControlSection.RoomControlSectionId - Media.RoomControlSectionId
		/// </summary>
		public virtual IEntityRelation MediaEntityUsingRoomControlSectionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MediaCollection" , true);
				relation.AddEntityFieldPair(RoomControlSectionFields.RoomControlSectionId, MediaFields.RoomControlSectionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlSectionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RoomControlSectionEntity and RoomControlComponentEntity over the 1:n relation they have, using the relation between the fields:
		/// RoomControlSection.RoomControlSectionId - RoomControlComponent.RoomControlSectionId
		/// </summary>
		public virtual IEntityRelation RoomControlComponentEntityUsingRoomControlSectionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RoomControlComponentCollection" , true);
				relation.AddEntityFieldPair(RoomControlSectionFields.RoomControlSectionId, RoomControlComponentFields.RoomControlSectionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlSectionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlComponentEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RoomControlSectionEntity and RoomControlSectionItemEntity over the 1:n relation they have, using the relation between the fields:
		/// RoomControlSection.RoomControlSectionId - RoomControlSectionItem.RoomControlSectionId
		/// </summary>
		public virtual IEntityRelation RoomControlSectionItemEntityUsingRoomControlSectionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RoomControlSectionItemCollection" , true);
				relation.AddEntityFieldPair(RoomControlSectionFields.RoomControlSectionId, RoomControlSectionItemFields.RoomControlSectionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlSectionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlSectionItemEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RoomControlSectionEntity and RoomControlSectionLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// RoomControlSection.RoomControlSectionId - RoomControlSectionLanguage.RoomControlSectionId
		/// </summary>
		public virtual IEntityRelation RoomControlSectionLanguageEntityUsingRoomControlSectionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RoomControlSectionLanguageCollection" , true);
				relation.AddEntityFieldPair(RoomControlSectionFields.RoomControlSectionId, RoomControlSectionLanguageFields.RoomControlSectionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlSectionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlSectionLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RoomControlSectionEntity and RoomControlWidgetEntity over the 1:n relation they have, using the relation between the fields:
		/// RoomControlSection.RoomControlSectionId - RoomControlWidget.RoomControlSectionId
		/// </summary>
		public virtual IEntityRelation RoomControlWidgetEntityUsingRoomControlSectionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RoomControlWidgetCollection" , true);
				relation.AddEntityFieldPair(RoomControlSectionFields.RoomControlSectionId, RoomControlWidgetFields.RoomControlSectionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlSectionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlWidgetEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RoomControlSectionEntity and UIWidgetEntity over the 1:n relation they have, using the relation between the fields:
		/// RoomControlSection.RoomControlSectionId - UIWidget.RoomControlSectionId
		/// </summary>
		public virtual IEntityRelation UIWidgetEntityUsingRoomControlSectionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UIWidgetCollection" , true);
				relation.AddEntityFieldPair(RoomControlSectionFields.RoomControlSectionId, UIWidgetFields.RoomControlSectionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlSectionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIWidgetEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between RoomControlSectionEntity and RoomControlAreaEntity over the m:1 relation they have, using the relation between the fields:
		/// RoomControlSection.RoomControlAreaId - RoomControlArea.RoomControlAreaId
		/// </summary>
		public virtual IEntityRelation RoomControlAreaEntityUsingRoomControlAreaId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RoomControlAreaEntity", false);
				relation.AddEntityFieldPair(RoomControlAreaFields.RoomControlAreaId, RoomControlSectionFields.RoomControlAreaId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlAreaEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlSectionEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticRoomControlSectionRelations
	{
		internal static readonly IEntityRelation CustomTextEntityUsingRoomControlSectionIdStatic = new RoomControlSectionRelations().CustomTextEntityUsingRoomControlSectionId;
		internal static readonly IEntityRelation MediaEntityUsingRoomControlSectionIdStatic = new RoomControlSectionRelations().MediaEntityUsingRoomControlSectionId;
		internal static readonly IEntityRelation RoomControlComponentEntityUsingRoomControlSectionIdStatic = new RoomControlSectionRelations().RoomControlComponentEntityUsingRoomControlSectionId;
		internal static readonly IEntityRelation RoomControlSectionItemEntityUsingRoomControlSectionIdStatic = new RoomControlSectionRelations().RoomControlSectionItemEntityUsingRoomControlSectionId;
		internal static readonly IEntityRelation RoomControlSectionLanguageEntityUsingRoomControlSectionIdStatic = new RoomControlSectionRelations().RoomControlSectionLanguageEntityUsingRoomControlSectionId;
		internal static readonly IEntityRelation RoomControlWidgetEntityUsingRoomControlSectionIdStatic = new RoomControlSectionRelations().RoomControlWidgetEntityUsingRoomControlSectionId;
		internal static readonly IEntityRelation UIWidgetEntityUsingRoomControlSectionIdStatic = new RoomControlSectionRelations().UIWidgetEntityUsingRoomControlSectionId;
		internal static readonly IEntityRelation RoomControlAreaEntityUsingRoomControlAreaIdStatic = new RoomControlSectionRelations().RoomControlAreaEntityUsingRoomControlAreaId;

		/// <summary>CTor</summary>
		static StaticRoomControlSectionRelations()
		{
		}
	}
}
