﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Terminal. </summary>
	public partial class TerminalRelations
	{
		/// <summary>CTor</summary>
		public TerminalRelations()
		{
		}

		/// <summary>Gets all relations of the TerminalEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.DeliverypointgroupEntityUsingXTerminalId);
			toReturn.Add(this.IcrtouchprintermappingEntityUsingTerminalId);
			toReturn.Add(this.NetmessageEntityUsingReceiverTerminalId);
			toReturn.Add(this.NetmessageEntityUsingSenderTerminalId);
			toReturn.Add(this.OrderRoutestephandlerEntityUsingForwardedFromTerminalId);
			toReturn.Add(this.OrderRoutestephandlerEntityUsingTerminalId);
			toReturn.Add(this.OrderRoutestephandlerHistoryEntityUsingForwardedFromTerminalId);
			toReturn.Add(this.OrderRoutestephandlerHistoryEntityUsingTerminalId);
			toReturn.Add(this.RoutestephandlerEntityUsingTerminalId);
			toReturn.Add(this.ScheduledCommandEntityUsingTerminalId);
			toReturn.Add(this.TerminalEntityUsingForwardToTerminalId);
			toReturn.Add(this.TerminalConfigurationEntityUsingTerminalId);
			toReturn.Add(this.TerminalLogEntityUsingTerminalId);
			toReturn.Add(this.TerminalMessageTemplateCategoryEntityUsingTerminalId);
			toReturn.Add(this.TerminalStateEntityUsingTerminalId);
			toReturn.Add(this.TimestampEntityUsingTerminalId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			toReturn.Add(this.DeliverypointEntityUsingAltSystemMessagesDeliverypointId);
			toReturn.Add(this.DeliverypointEntityUsingSystemMessagesDeliverypointId);
			toReturn.Add(this.DeliverypointgroupEntityUsingDeliverypointgroupId);
			toReturn.Add(this.DeviceEntityUsingDeviceId);
			toReturn.Add(this.EntertainmentEntityUsingBrowser1);
			toReturn.Add(this.EntertainmentEntityUsingBrowser2);
			toReturn.Add(this.EntertainmentEntityUsingCmsPage);
			toReturn.Add(this.IcrtouchprintermappingEntityUsingIcrtouchprintermappingId);
			toReturn.Add(this.ProductEntityUsingBatteryLowProductId);
			toReturn.Add(this.ProductEntityUsingClientDisconnectedProductId);
			toReturn.Add(this.ProductEntityUsingOrderFailedProductId);
			toReturn.Add(this.ProductEntityUsingUnlockDeliverypointProductId);
			toReturn.Add(this.TerminalEntityUsingTerminalIdForwardToTerminalId);
			toReturn.Add(this.TerminalConfigurationEntityUsingTerminalConfigurationId);
			toReturn.Add(this.UIModeEntityUsingUIModeId);
			toReturn.Add(this.UserEntityUsingAutomaticSignOnUserId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and DeliverypointgroupEntity over the 1:n relation they have, using the relation between the fields:
		/// Terminal.TerminalId - Deliverypointgroup.XTerminalId
		/// </summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingXTerminalId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeliverypointgroupCollection" , true);
				relation.AddEntityFieldPair(TerminalFields.TerminalId, DeliverypointgroupFields.XTerminalId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and IcrtouchprintermappingEntity over the 1:n relation they have, using the relation between the fields:
		/// Terminal.TerminalId - Icrtouchprintermapping.TerminalId
		/// </summary>
		public virtual IEntityRelation IcrtouchprintermappingEntityUsingTerminalId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "IcrtouchprintermappingCollection" , true);
				relation.AddEntityFieldPair(TerminalFields.TerminalId, IcrtouchprintermappingFields.TerminalId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("IcrtouchprintermappingEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and NetmessageEntity over the 1:n relation they have, using the relation between the fields:
		/// Terminal.TerminalId - Netmessage.ReceiverTerminalId
		/// </summary>
		public virtual IEntityRelation NetmessageEntityUsingReceiverTerminalId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ReceivedNetmessageCollection" , true);
				relation.AddEntityFieldPair(TerminalFields.TerminalId, NetmessageFields.ReceiverTerminalId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NetmessageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and NetmessageEntity over the 1:n relation they have, using the relation between the fields:
		/// Terminal.TerminalId - Netmessage.SenderTerminalId
		/// </summary>
		public virtual IEntityRelation NetmessageEntityUsingSenderTerminalId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SentNetmessageCollection" , true);
				relation.AddEntityFieldPair(TerminalFields.TerminalId, NetmessageFields.SenderTerminalId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NetmessageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and OrderRoutestephandlerEntity over the 1:n relation they have, using the relation between the fields:
		/// Terminal.TerminalId - OrderRoutestephandler.ForwardedFromTerminalId
		/// </summary>
		public virtual IEntityRelation OrderRoutestephandlerEntityUsingForwardedFromTerminalId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrderRoutestephandlerCollection_" , true);
				relation.AddEntityFieldPair(TerminalFields.TerminalId, OrderRoutestephandlerFields.ForwardedFromTerminalId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderRoutestephandlerEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and OrderRoutestephandlerEntity over the 1:n relation they have, using the relation between the fields:
		/// Terminal.TerminalId - OrderRoutestephandler.TerminalId
		/// </summary>
		public virtual IEntityRelation OrderRoutestephandlerEntityUsingTerminalId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrderRoutestephandlerCollection" , true);
				relation.AddEntityFieldPair(TerminalFields.TerminalId, OrderRoutestephandlerFields.TerminalId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderRoutestephandlerEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and OrderRoutestephandlerHistoryEntity over the 1:n relation they have, using the relation between the fields:
		/// Terminal.TerminalId - OrderRoutestephandlerHistory.ForwardedFromTerminalId
		/// </summary>
		public virtual IEntityRelation OrderRoutestephandlerHistoryEntityUsingForwardedFromTerminalId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrderRoutestephandlerHistoryCollection_" , true);
				relation.AddEntityFieldPair(TerminalFields.TerminalId, OrderRoutestephandlerHistoryFields.ForwardedFromTerminalId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderRoutestephandlerHistoryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and OrderRoutestephandlerHistoryEntity over the 1:n relation they have, using the relation between the fields:
		/// Terminal.TerminalId - OrderRoutestephandlerHistory.TerminalId
		/// </summary>
		public virtual IEntityRelation OrderRoutestephandlerHistoryEntityUsingTerminalId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrderRoutestephandlerHistoryCollection" , true);
				relation.AddEntityFieldPair(TerminalFields.TerminalId, OrderRoutestephandlerHistoryFields.TerminalId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderRoutestephandlerHistoryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and RoutestephandlerEntity over the 1:n relation they have, using the relation between the fields:
		/// Terminal.TerminalId - Routestephandler.TerminalId
		/// </summary>
		public virtual IEntityRelation RoutestephandlerEntityUsingTerminalId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RoutestephandlerCollection" , true);
				relation.AddEntityFieldPair(TerminalFields.TerminalId, RoutestephandlerFields.TerminalId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoutestephandlerEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and ScheduledCommandEntity over the 1:n relation they have, using the relation between the fields:
		/// Terminal.TerminalId - ScheduledCommand.TerminalId
		/// </summary>
		public virtual IEntityRelation ScheduledCommandEntityUsingTerminalId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ScheduledCommandCollection" , true);
				relation.AddEntityFieldPair(TerminalFields.TerminalId, ScheduledCommandFields.TerminalId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduledCommandEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and TerminalEntity over the 1:n relation they have, using the relation between the fields:
		/// Terminal.TerminalId - Terminal.ForwardToTerminalId
		/// </summary>
		public virtual IEntityRelation TerminalEntityUsingForwardToTerminalId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ForwardedFromTerminalCollection" , true);
				relation.AddEntityFieldPair(TerminalFields.TerminalId, TerminalFields.ForwardToTerminalId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and TerminalConfigurationEntity over the 1:n relation they have, using the relation between the fields:
		/// Terminal.TerminalId - TerminalConfiguration.TerminalId
		/// </summary>
		public virtual IEntityRelation TerminalConfigurationEntityUsingTerminalId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TerminalConfigurationCollection" , true);
				relation.AddEntityFieldPair(TerminalFields.TerminalId, TerminalConfigurationFields.TerminalId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalConfigurationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and TerminalLogEntity over the 1:n relation they have, using the relation between the fields:
		/// Terminal.TerminalId - TerminalLog.TerminalId
		/// </summary>
		public virtual IEntityRelation TerminalLogEntityUsingTerminalId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TerminalLogCollection" , true);
				relation.AddEntityFieldPair(TerminalFields.TerminalId, TerminalLogFields.TerminalId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalLogEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and TerminalMessageTemplateCategoryEntity over the 1:n relation they have, using the relation between the fields:
		/// Terminal.TerminalId - TerminalMessageTemplateCategory.TerminalId
		/// </summary>
		public virtual IEntityRelation TerminalMessageTemplateCategoryEntityUsingTerminalId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TerminalMessageTemplateCategoryCollection" , true);
				relation.AddEntityFieldPair(TerminalFields.TerminalId, TerminalMessageTemplateCategoryFields.TerminalId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalMessageTemplateCategoryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and TerminalStateEntity over the 1:n relation they have, using the relation between the fields:
		/// Terminal.TerminalId - TerminalState.TerminalId
		/// </summary>
		public virtual IEntityRelation TerminalStateEntityUsingTerminalId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TerminalStateCollection" , true);
				relation.AddEntityFieldPair(TerminalFields.TerminalId, TerminalStateFields.TerminalId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalStateEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and TimestampEntity over the 1:n relation they have, using the relation between the fields:
		/// Terminal.TerminalId - Timestamp.TerminalId
		/// </summary>
		public virtual IEntityRelation TimestampEntityUsingTerminalId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TimestampCollection" , true);
				relation.AddEntityFieldPair(TerminalFields.TerminalId, TimestampFields.TerminalId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TimestampEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// Terminal.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, TerminalFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and DeliverypointEntity over the m:1 relation they have, using the relation between the fields:
		/// Terminal.AltSystemMessagesDeliverypointId - Deliverypoint.DeliverypointId
		/// </summary>
		public virtual IEntityRelation DeliverypointEntityUsingAltSystemMessagesDeliverypointId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AltSystemMessagesDeliverypointEntity", false);
				relation.AddEntityFieldPair(DeliverypointFields.DeliverypointId, TerminalFields.AltSystemMessagesDeliverypointId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and DeliverypointEntity over the m:1 relation they have, using the relation between the fields:
		/// Terminal.SystemMessagesDeliverypointId - Deliverypoint.DeliverypointId
		/// </summary>
		public virtual IEntityRelation DeliverypointEntityUsingSystemMessagesDeliverypointId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SystemMessagesDeliverypointEntity", false);
				relation.AddEntityFieldPair(DeliverypointFields.DeliverypointId, TerminalFields.SystemMessagesDeliverypointId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and DeliverypointgroupEntity over the m:1 relation they have, using the relation between the fields:
		/// Terminal.DeliverypointgroupId - Deliverypointgroup.DeliverypointgroupId
		/// </summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingDeliverypointgroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DeliverypointgroupEntity", false);
				relation.AddEntityFieldPair(DeliverypointgroupFields.DeliverypointgroupId, TerminalFields.DeliverypointgroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and DeviceEntity over the m:1 relation they have, using the relation between the fields:
		/// Terminal.DeviceId - Device.DeviceId
		/// </summary>
		public virtual IEntityRelation DeviceEntityUsingDeviceId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DeviceEntity", false);
				relation.AddEntityFieldPair(DeviceFields.DeviceId, TerminalFields.DeviceId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and EntertainmentEntity over the m:1 relation they have, using the relation between the fields:
		/// Terminal.Browser1 - Entertainment.EntertainmentId
		/// </summary>
		public virtual IEntityRelation EntertainmentEntityUsingBrowser1
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Browser1EntertainmentEntity", false);
				relation.AddEntityFieldPair(EntertainmentFields.EntertainmentId, TerminalFields.Browser1);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and EntertainmentEntity over the m:1 relation they have, using the relation between the fields:
		/// Terminal.Browser2 - Entertainment.EntertainmentId
		/// </summary>
		public virtual IEntityRelation EntertainmentEntityUsingBrowser2
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Browser2EntertainmentEntity", false);
				relation.AddEntityFieldPair(EntertainmentFields.EntertainmentId, TerminalFields.Browser2);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and EntertainmentEntity over the m:1 relation they have, using the relation between the fields:
		/// Terminal.CmsPage - Entertainment.EntertainmentId
		/// </summary>
		public virtual IEntityRelation EntertainmentEntityUsingCmsPage
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CmsPageEntertainmentEntity", false);
				relation.AddEntityFieldPair(EntertainmentFields.EntertainmentId, TerminalFields.CmsPage);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and IcrtouchprintermappingEntity over the m:1 relation they have, using the relation between the fields:
		/// Terminal.IcrtouchprintermappingId - Icrtouchprintermapping.IcrtouchprintermappingId
		/// </summary>
		public virtual IEntityRelation IcrtouchprintermappingEntityUsingIcrtouchprintermappingId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "IcrtouchprintermappingEntity", false);
				relation.AddEntityFieldPair(IcrtouchprintermappingFields.IcrtouchprintermappingId, TerminalFields.IcrtouchprintermappingId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("IcrtouchprintermappingEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and ProductEntity over the m:1 relation they have, using the relation between the fields:
		/// Terminal.BatteryLowProductId - Product.ProductId
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingBatteryLowProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "BatteryLowProductEntity", false);
				relation.AddEntityFieldPair(ProductFields.ProductId, TerminalFields.BatteryLowProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and ProductEntity over the m:1 relation they have, using the relation between the fields:
		/// Terminal.ClientDisconnectedProductId - Product.ProductId
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingClientDisconnectedProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ClientDisconnectedProductEntity", false);
				relation.AddEntityFieldPair(ProductFields.ProductId, TerminalFields.ClientDisconnectedProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and ProductEntity over the m:1 relation they have, using the relation between the fields:
		/// Terminal.OrderFailedProductId - Product.ProductId
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingOrderFailedProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ProductEntity", false);
				relation.AddEntityFieldPair(ProductFields.ProductId, TerminalFields.OrderFailedProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and ProductEntity over the m:1 relation they have, using the relation between the fields:
		/// Terminal.UnlockDeliverypointProductId - Product.ProductId
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingUnlockDeliverypointProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UnlockDeliverypointProductEntity", false);
				relation.AddEntityFieldPair(ProductFields.ProductId, TerminalFields.UnlockDeliverypointProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and TerminalEntity over the m:1 relation they have, using the relation between the fields:
		/// Terminal.ForwardToTerminalId - Terminal.TerminalId
		/// </summary>
		public virtual IEntityRelation TerminalEntityUsingTerminalIdForwardToTerminalId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ForwardToTerminalEntity", false);
				relation.AddEntityFieldPair(TerminalFields.TerminalId, TerminalFields.ForwardToTerminalId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and TerminalConfigurationEntity over the m:1 relation they have, using the relation between the fields:
		/// Terminal.TerminalConfigurationId - TerminalConfiguration.TerminalConfigurationId
		/// </summary>
		public virtual IEntityRelation TerminalConfigurationEntityUsingTerminalConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TerminalConfigurationEntity", false);
				relation.AddEntityFieldPair(TerminalConfigurationFields.TerminalConfigurationId, TerminalFields.TerminalConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalConfigurationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and UIModeEntity over the m:1 relation they have, using the relation between the fields:
		/// Terminal.UIModeId - UIMode.UIModeId
		/// </summary>
		public virtual IEntityRelation UIModeEntityUsingUIModeId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UIModeEntity", false);
				relation.AddEntityFieldPair(UIModeFields.UIModeId, TerminalFields.UIModeId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIModeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TerminalEntity and UserEntity over the m:1 relation they have, using the relation between the fields:
		/// Terminal.AutomaticSignOnUserId - User.UserId
		/// </summary>
		public virtual IEntityRelation UserEntityUsingAutomaticSignOnUserId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AutomaticSignOnUserEntity", false);
				relation.AddEntityFieldPair(UserFields.UserId, TerminalFields.AutomaticSignOnUserId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticTerminalRelations
	{
		internal static readonly IEntityRelation DeliverypointgroupEntityUsingXTerminalIdStatic = new TerminalRelations().DeliverypointgroupEntityUsingXTerminalId;
		internal static readonly IEntityRelation IcrtouchprintermappingEntityUsingTerminalIdStatic = new TerminalRelations().IcrtouchprintermappingEntityUsingTerminalId;
		internal static readonly IEntityRelation NetmessageEntityUsingReceiverTerminalIdStatic = new TerminalRelations().NetmessageEntityUsingReceiverTerminalId;
		internal static readonly IEntityRelation NetmessageEntityUsingSenderTerminalIdStatic = new TerminalRelations().NetmessageEntityUsingSenderTerminalId;
		internal static readonly IEntityRelation OrderRoutestephandlerEntityUsingForwardedFromTerminalIdStatic = new TerminalRelations().OrderRoutestephandlerEntityUsingForwardedFromTerminalId;
		internal static readonly IEntityRelation OrderRoutestephandlerEntityUsingTerminalIdStatic = new TerminalRelations().OrderRoutestephandlerEntityUsingTerminalId;
		internal static readonly IEntityRelation OrderRoutestephandlerHistoryEntityUsingForwardedFromTerminalIdStatic = new TerminalRelations().OrderRoutestephandlerHistoryEntityUsingForwardedFromTerminalId;
		internal static readonly IEntityRelation OrderRoutestephandlerHistoryEntityUsingTerminalIdStatic = new TerminalRelations().OrderRoutestephandlerHistoryEntityUsingTerminalId;
		internal static readonly IEntityRelation RoutestephandlerEntityUsingTerminalIdStatic = new TerminalRelations().RoutestephandlerEntityUsingTerminalId;
		internal static readonly IEntityRelation ScheduledCommandEntityUsingTerminalIdStatic = new TerminalRelations().ScheduledCommandEntityUsingTerminalId;
		internal static readonly IEntityRelation TerminalEntityUsingForwardToTerminalIdStatic = new TerminalRelations().TerminalEntityUsingForwardToTerminalId;
		internal static readonly IEntityRelation TerminalConfigurationEntityUsingTerminalIdStatic = new TerminalRelations().TerminalConfigurationEntityUsingTerminalId;
		internal static readonly IEntityRelation TerminalLogEntityUsingTerminalIdStatic = new TerminalRelations().TerminalLogEntityUsingTerminalId;
		internal static readonly IEntityRelation TerminalMessageTemplateCategoryEntityUsingTerminalIdStatic = new TerminalRelations().TerminalMessageTemplateCategoryEntityUsingTerminalId;
		internal static readonly IEntityRelation TerminalStateEntityUsingTerminalIdStatic = new TerminalRelations().TerminalStateEntityUsingTerminalId;
		internal static readonly IEntityRelation TimestampEntityUsingTerminalIdStatic = new TerminalRelations().TimestampEntityUsingTerminalId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new TerminalRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation DeliverypointEntityUsingAltSystemMessagesDeliverypointIdStatic = new TerminalRelations().DeliverypointEntityUsingAltSystemMessagesDeliverypointId;
		internal static readonly IEntityRelation DeliverypointEntityUsingSystemMessagesDeliverypointIdStatic = new TerminalRelations().DeliverypointEntityUsingSystemMessagesDeliverypointId;
		internal static readonly IEntityRelation DeliverypointgroupEntityUsingDeliverypointgroupIdStatic = new TerminalRelations().DeliverypointgroupEntityUsingDeliverypointgroupId;
		internal static readonly IEntityRelation DeviceEntityUsingDeviceIdStatic = new TerminalRelations().DeviceEntityUsingDeviceId;
		internal static readonly IEntityRelation EntertainmentEntityUsingBrowser1Static = new TerminalRelations().EntertainmentEntityUsingBrowser1;
		internal static readonly IEntityRelation EntertainmentEntityUsingBrowser2Static = new TerminalRelations().EntertainmentEntityUsingBrowser2;
		internal static readonly IEntityRelation EntertainmentEntityUsingCmsPageStatic = new TerminalRelations().EntertainmentEntityUsingCmsPage;
		internal static readonly IEntityRelation IcrtouchprintermappingEntityUsingIcrtouchprintermappingIdStatic = new TerminalRelations().IcrtouchprintermappingEntityUsingIcrtouchprintermappingId;
		internal static readonly IEntityRelation ProductEntityUsingBatteryLowProductIdStatic = new TerminalRelations().ProductEntityUsingBatteryLowProductId;
		internal static readonly IEntityRelation ProductEntityUsingClientDisconnectedProductIdStatic = new TerminalRelations().ProductEntityUsingClientDisconnectedProductId;
		internal static readonly IEntityRelation ProductEntityUsingOrderFailedProductIdStatic = new TerminalRelations().ProductEntityUsingOrderFailedProductId;
		internal static readonly IEntityRelation ProductEntityUsingUnlockDeliverypointProductIdStatic = new TerminalRelations().ProductEntityUsingUnlockDeliverypointProductId;
		internal static readonly IEntityRelation TerminalEntityUsingTerminalIdForwardToTerminalIdStatic = new TerminalRelations().TerminalEntityUsingTerminalIdForwardToTerminalId;
		internal static readonly IEntityRelation TerminalConfigurationEntityUsingTerminalConfigurationIdStatic = new TerminalRelations().TerminalConfigurationEntityUsingTerminalConfigurationId;
		internal static readonly IEntityRelation UIModeEntityUsingUIModeIdStatic = new TerminalRelations().UIModeEntityUsingUIModeId;
		internal static readonly IEntityRelation UserEntityUsingAutomaticSignOnUserIdStatic = new TerminalRelations().UserEntityUsingAutomaticSignOnUserId;

		/// <summary>CTor</summary>
		static StaticTerminalRelations()
		{
		}
	}
}
