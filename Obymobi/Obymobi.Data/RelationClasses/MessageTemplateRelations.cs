﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: MessageTemplate. </summary>
	public partial class MessageTemplateRelations
	{
		/// <summary>CTor</summary>
		public MessageTemplateRelations()
		{
		}

		/// <summary>Gets all relations of the MessageTemplateEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.MessageTemplateCategoryMessageTemplateEntityUsingMessageTemplateId);
			toReturn.Add(this.ScheduledMessageEntityUsingMessageTemplateId);
			toReturn.Add(this.CategoryEntityUsingCategoryId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			toReturn.Add(this.EntertainmentEntityUsingEntertainmentId);
			toReturn.Add(this.MediaEntityUsingMediaId);
			toReturn.Add(this.PageEntityUsingPageId);
			toReturn.Add(this.ProductEntityUsingProductId);
			toReturn.Add(this.ProductCategoryEntityUsingProductCategoryId);
			toReturn.Add(this.SiteEntityUsingSiteId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between MessageTemplateEntity and MessageTemplateCategoryMessageTemplateEntity over the 1:n relation they have, using the relation between the fields:
		/// MessageTemplate.MessageTemplateId - MessageTemplateCategoryMessageTemplate.MessageTemplateId
		/// </summary>
		public virtual IEntityRelation MessageTemplateCategoryMessageTemplateEntityUsingMessageTemplateId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MessageTemplateCategoryMessageTemplateCollection" , true);
				relation.AddEntityFieldPair(MessageTemplateFields.MessageTemplateId, MessageTemplateCategoryMessageTemplateFields.MessageTemplateId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageTemplateEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageTemplateCategoryMessageTemplateEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between MessageTemplateEntity and ScheduledMessageEntity over the 1:n relation they have, using the relation between the fields:
		/// MessageTemplate.MessageTemplateId - ScheduledMessage.MessageTemplateId
		/// </summary>
		public virtual IEntityRelation ScheduledMessageEntityUsingMessageTemplateId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ScheduledMessageCollection" , true);
				relation.AddEntityFieldPair(MessageTemplateFields.MessageTemplateId, ScheduledMessageFields.MessageTemplateId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageTemplateEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduledMessageEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between MessageTemplateEntity and CategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// MessageTemplate.CategoryId - Category.CategoryId
		/// </summary>
		public virtual IEntityRelation CategoryEntityUsingCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CategoryEntity", false);
				relation.AddEntityFieldPair(CategoryFields.CategoryId, MessageTemplateFields.CategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageTemplateEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MessageTemplateEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// MessageTemplate.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, MessageTemplateFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageTemplateEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MessageTemplateEntity and EntertainmentEntity over the m:1 relation they have, using the relation between the fields:
		/// MessageTemplate.EntertainmentId - Entertainment.EntertainmentId
		/// </summary>
		public virtual IEntityRelation EntertainmentEntityUsingEntertainmentId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "EntertainmentEntity", false);
				relation.AddEntityFieldPair(EntertainmentFields.EntertainmentId, MessageTemplateFields.EntertainmentId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageTemplateEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MessageTemplateEntity and MediaEntity over the m:1 relation they have, using the relation between the fields:
		/// MessageTemplate.MediaId - Media.MediaId
		/// </summary>
		public virtual IEntityRelation MediaEntityUsingMediaId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "MediaEntity", false);
				relation.AddEntityFieldPair(MediaFields.MediaId, MessageTemplateFields.MediaId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageTemplateEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MessageTemplateEntity and PageEntity over the m:1 relation they have, using the relation between the fields:
		/// MessageTemplate.PageId - Page.PageId
		/// </summary>
		public virtual IEntityRelation PageEntityUsingPageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PageEntity", false);
				relation.AddEntityFieldPair(PageFields.PageId, MessageTemplateFields.PageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageTemplateEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MessageTemplateEntity and ProductEntity over the m:1 relation they have, using the relation between the fields:
		/// MessageTemplate.ProductId - Product.ProductId
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ProductEntity", false);
				relation.AddEntityFieldPair(ProductFields.ProductId, MessageTemplateFields.ProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageTemplateEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MessageTemplateEntity and ProductCategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// MessageTemplate.ProductCategoryId - ProductCategory.ProductCategoryId
		/// </summary>
		public virtual IEntityRelation ProductCategoryEntityUsingProductCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ProductCategoryEntity", false);
				relation.AddEntityFieldPair(ProductCategoryFields.ProductCategoryId, MessageTemplateFields.ProductCategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductCategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageTemplateEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MessageTemplateEntity and SiteEntity over the m:1 relation they have, using the relation between the fields:
		/// MessageTemplate.SiteId - Site.SiteId
		/// </summary>
		public virtual IEntityRelation SiteEntityUsingSiteId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SiteEntity", false);
				relation.AddEntityFieldPair(SiteFields.SiteId, MessageTemplateFields.SiteId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SiteEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageTemplateEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticMessageTemplateRelations
	{
		internal static readonly IEntityRelation MessageTemplateCategoryMessageTemplateEntityUsingMessageTemplateIdStatic = new MessageTemplateRelations().MessageTemplateCategoryMessageTemplateEntityUsingMessageTemplateId;
		internal static readonly IEntityRelation ScheduledMessageEntityUsingMessageTemplateIdStatic = new MessageTemplateRelations().ScheduledMessageEntityUsingMessageTemplateId;
		internal static readonly IEntityRelation CategoryEntityUsingCategoryIdStatic = new MessageTemplateRelations().CategoryEntityUsingCategoryId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new MessageTemplateRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation EntertainmentEntityUsingEntertainmentIdStatic = new MessageTemplateRelations().EntertainmentEntityUsingEntertainmentId;
		internal static readonly IEntityRelation MediaEntityUsingMediaIdStatic = new MessageTemplateRelations().MediaEntityUsingMediaId;
		internal static readonly IEntityRelation PageEntityUsingPageIdStatic = new MessageTemplateRelations().PageEntityUsingPageId;
		internal static readonly IEntityRelation ProductEntityUsingProductIdStatic = new MessageTemplateRelations().ProductEntityUsingProductId;
		internal static readonly IEntityRelation ProductCategoryEntityUsingProductCategoryIdStatic = new MessageTemplateRelations().ProductCategoryEntityUsingProductCategoryId;
		internal static readonly IEntityRelation SiteEntityUsingSiteIdStatic = new MessageTemplateRelations().SiteEntityUsingSiteId;

		/// <summary>CTor</summary>
		static StaticMessageTemplateRelations()
		{
		}
	}
}
