﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Routestep. </summary>
	public partial class RoutestepRelations
	{
		/// <summary>CTor</summary>
		public RoutestepRelations()
		{
		}

		/// <summary>Gets all relations of the RoutestepEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.RoutestephandlerEntityUsingRoutestepId);
			toReturn.Add(this.RouteEntityUsingRouteId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between RoutestepEntity and RoutestephandlerEntity over the 1:n relation they have, using the relation between the fields:
		/// Routestep.RoutestepId - Routestephandler.RoutestepId
		/// </summary>
		public virtual IEntityRelation RoutestephandlerEntityUsingRoutestepId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RoutestephandlerCollection" , true);
				relation.AddEntityFieldPair(RoutestepFields.RoutestepId, RoutestephandlerFields.RoutestepId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoutestepEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoutestephandlerEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between RoutestepEntity and RouteEntity over the m:1 relation they have, using the relation between the fields:
		/// Routestep.RouteId - Route.RouteId
		/// </summary>
		public virtual IEntityRelation RouteEntityUsingRouteId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RouteEntity", false);
				relation.AddEntityFieldPair(RouteFields.RouteId, RoutestepFields.RouteId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RouteEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoutestepEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticRoutestepRelations
	{
		internal static readonly IEntityRelation RoutestephandlerEntityUsingRoutestepIdStatic = new RoutestepRelations().RoutestephandlerEntityUsingRoutestepId;
		internal static readonly IEntityRelation RouteEntityUsingRouteIdStatic = new RoutestepRelations().RouteEntityUsingRouteId;

		/// <summary>CTor</summary>
		static StaticRoutestepRelations()
		{
		}
	}
}
