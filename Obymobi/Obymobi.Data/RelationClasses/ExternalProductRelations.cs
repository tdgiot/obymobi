﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ExternalProduct. </summary>
	public partial class ExternalProductRelations
	{
		/// <summary>CTor</summary>
		public ExternalProductRelations()
		{
		}

		/// <summary>Gets all relations of the ExternalProductEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AlterationEntityUsingExternalProductId);
			toReturn.Add(this.AlterationoptionEntityUsingExternalProductId);
			toReturn.Add(this.ExternalSubProductEntityUsingExternalProductId);
			toReturn.Add(this.ExternalSubProductEntityUsingExternalSubProductId);
			toReturn.Add(this.ProductEntityUsingExternalProductId);
			toReturn.Add(this.ExternalMenuEntityUsingExternalMenuId);
			toReturn.Add(this.ExternalSystemEntityUsingExternalSystemId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ExternalProductEntity and AlterationEntity over the 1:n relation they have, using the relation between the fields:
		/// ExternalProduct.ExternalProductId - Alteration.ExternalProductId
		/// </summary>
		public virtual IEntityRelation AlterationEntityUsingExternalProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AlterationCollection" , true);
				relation.AddEntityFieldPair(ExternalProductFields.ExternalProductId, AlterationFields.ExternalProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ExternalProductEntity and AlterationoptionEntity over the 1:n relation they have, using the relation between the fields:
		/// ExternalProduct.ExternalProductId - Alterationoption.ExternalProductId
		/// </summary>
		public virtual IEntityRelation AlterationoptionEntityUsingExternalProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AlterationoptionCollection" , true);
				relation.AddEntityFieldPair(ExternalProductFields.ExternalProductId, AlterationoptionFields.ExternalProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationoptionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ExternalProductEntity and ExternalSubProductEntity over the 1:n relation they have, using the relation between the fields:
		/// ExternalProduct.ExternalProductId - ExternalSubProduct.ExternalProductId
		/// </summary>
		public virtual IEntityRelation ExternalSubProductEntityUsingExternalProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ExternalSubProductCollection" , true);
				relation.AddEntityFieldPair(ExternalProductFields.ExternalProductId, ExternalSubProductFields.ExternalProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalSubProductEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ExternalProductEntity and ExternalSubProductEntity over the 1:n relation they have, using the relation between the fields:
		/// ExternalProduct.ExternalProductId - ExternalSubProduct.ExternalSubProductId
		/// </summary>
		public virtual IEntityRelation ExternalSubProductEntityUsingExternalSubProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ExternalSubProductCollection1" , true);
				relation.AddEntityFieldPair(ExternalProductFields.ExternalProductId, ExternalSubProductFields.ExternalSubProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalSubProductEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ExternalProductEntity and ProductEntity over the 1:n relation they have, using the relation between the fields:
		/// ExternalProduct.ExternalProductId - Product.ExternalProductId
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingExternalProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ProductCollection" , true);
				relation.AddEntityFieldPair(ExternalProductFields.ExternalProductId, ProductFields.ExternalProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between ExternalProductEntity and ExternalMenuEntity over the m:1 relation they have, using the relation between the fields:
		/// ExternalProduct.ExternalMenuId - ExternalMenu.ExternalMenuId
		/// </summary>
		public virtual IEntityRelation ExternalMenuEntityUsingExternalMenuId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ExternalMenuEntity", false);
				relation.AddEntityFieldPair(ExternalMenuFields.ExternalMenuId, ExternalProductFields.ExternalMenuId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalMenuEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalProductEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ExternalProductEntity and ExternalSystemEntity over the m:1 relation they have, using the relation between the fields:
		/// ExternalProduct.ExternalSystemId - ExternalSystem.ExternalSystemId
		/// </summary>
		public virtual IEntityRelation ExternalSystemEntityUsingExternalSystemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ExternalSystemEntity", false);
				relation.AddEntityFieldPair(ExternalSystemFields.ExternalSystemId, ExternalProductFields.ExternalSystemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalSystemEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalProductEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticExternalProductRelations
	{
		internal static readonly IEntityRelation AlterationEntityUsingExternalProductIdStatic = new ExternalProductRelations().AlterationEntityUsingExternalProductId;
		internal static readonly IEntityRelation AlterationoptionEntityUsingExternalProductIdStatic = new ExternalProductRelations().AlterationoptionEntityUsingExternalProductId;
		internal static readonly IEntityRelation ExternalSubProductEntityUsingExternalProductIdStatic = new ExternalProductRelations().ExternalSubProductEntityUsingExternalProductId;
		internal static readonly IEntityRelation ExternalSubProductEntityUsingExternalSubProductIdStatic = new ExternalProductRelations().ExternalSubProductEntityUsingExternalSubProductId;
		internal static readonly IEntityRelation ProductEntityUsingExternalProductIdStatic = new ExternalProductRelations().ProductEntityUsingExternalProductId;
		internal static readonly IEntityRelation ExternalMenuEntityUsingExternalMenuIdStatic = new ExternalProductRelations().ExternalMenuEntityUsingExternalMenuId;
		internal static readonly IEntityRelation ExternalSystemEntityUsingExternalSystemIdStatic = new ExternalProductRelations().ExternalSystemEntityUsingExternalSystemId;

		/// <summary>CTor</summary>
		static StaticExternalProductRelations()
		{
		}
	}
}
