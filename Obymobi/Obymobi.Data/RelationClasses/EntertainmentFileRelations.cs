﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: EntertainmentFile. </summary>
	public partial class EntertainmentFileRelations
	{
		/// <summary>CTor</summary>
		public EntertainmentFileRelations()
		{
		}

		/// <summary>Gets all relations of the EntertainmentFileEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CloudProcessingTaskEntityUsingEntertainmentFileId);
			toReturn.Add(this.EntertainmentEntityUsingEntertainmentFileId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between EntertainmentFileEntity and CloudProcessingTaskEntity over the 1:n relation they have, using the relation between the fields:
		/// EntertainmentFile.EntertainmentFileId - CloudProcessingTask.EntertainmentFileId
		/// </summary>
		public virtual IEntityRelation CloudProcessingTaskEntityUsingEntertainmentFileId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CloudProcessingTaskCollection" , true);
				relation.AddEntityFieldPair(EntertainmentFileFields.EntertainmentFileId, CloudProcessingTaskFields.EntertainmentFileId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentFileEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CloudProcessingTaskEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between EntertainmentFileEntity and EntertainmentEntity over the 1:n relation they have, using the relation between the fields:
		/// EntertainmentFile.EntertainmentFileId - Entertainment.EntertainmentFileId
		/// </summary>
		public virtual IEntityRelation EntertainmentEntityUsingEntertainmentFileId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "EntertainmentCollection" , true);
				relation.AddEntityFieldPair(EntertainmentFileFields.EntertainmentFileId, EntertainmentFields.EntertainmentFileId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentFileEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticEntertainmentFileRelations
	{
		internal static readonly IEntityRelation CloudProcessingTaskEntityUsingEntertainmentFileIdStatic = new EntertainmentFileRelations().CloudProcessingTaskEntityUsingEntertainmentFileId;
		internal static readonly IEntityRelation EntertainmentEntityUsingEntertainmentFileIdStatic = new EntertainmentFileRelations().EntertainmentEntityUsingEntertainmentFileId;

		/// <summary>CTor</summary>
		static StaticEntertainmentFileRelations()
		{
		}
	}
}
