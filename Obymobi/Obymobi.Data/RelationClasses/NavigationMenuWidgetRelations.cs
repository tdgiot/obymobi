﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: NavigationMenuWidget. </summary>
	public partial class NavigationMenuWidgetRelations
	{
		/// <summary>CTor</summary>
		public NavigationMenuWidgetRelations()
		{
		}

		/// <summary>Gets all relations of the NavigationMenuWidgetEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.NavigationMenuEntityUsingNavigationMenuId);
			toReturn.Add(this.WidgetEntityUsingWidgetId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between NavigationMenuWidgetEntity and NavigationMenuEntity over the m:1 relation they have, using the relation between the fields:
		/// NavigationMenuWidget.NavigationMenuId - NavigationMenu.NavigationMenuId
		/// </summary>
		public virtual IEntityRelation NavigationMenuEntityUsingNavigationMenuId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "NavigationMenuEntity", false);
				relation.AddEntityFieldPair(NavigationMenuFields.NavigationMenuId, NavigationMenuWidgetFields.NavigationMenuId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NavigationMenuEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NavigationMenuWidgetEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between NavigationMenuWidgetEntity and WidgetEntity over the m:1 relation they have, using the relation between the fields:
		/// NavigationMenuWidget.WidgetId - Widget.WidgetId
		/// </summary>
		public virtual IEntityRelation WidgetEntityUsingWidgetId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "WidgetEntity", false);
				relation.AddEntityFieldPair(WidgetFields.WidgetId, NavigationMenuWidgetFields.WidgetId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WidgetEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NavigationMenuWidgetEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticNavigationMenuWidgetRelations
	{
		internal static readonly IEntityRelation NavigationMenuEntityUsingNavigationMenuIdStatic = new NavigationMenuWidgetRelations().NavigationMenuEntityUsingNavigationMenuId;
		internal static readonly IEntityRelation WidgetEntityUsingWidgetIdStatic = new NavigationMenuWidgetRelations().WidgetEntityUsingWidgetId;

		/// <summary>CTor</summary>
		static StaticNavigationMenuWidgetRelations()
		{
		}
	}
}
