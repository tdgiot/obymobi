﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ReportProcessingTaskTemplate. </summary>
	public partial class ReportProcessingTaskTemplateRelations
	{
		/// <summary>CTor</summary>
		public ReportProcessingTaskTemplateRelations()
		{
		}

		/// <summary>Gets all relations of the ReportProcessingTaskTemplateEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.UIScheduleItemEntityUsingReportProcessingTaskTemplateId);
			toReturn.Add(this.UIScheduleItemOccurrenceEntityUsingReportProcessingTaskTemplateId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			toReturn.Add(this.ReportProcessingTaskEntityUsingReportProcessingTaskId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ReportProcessingTaskTemplateEntity and UIScheduleItemEntity over the 1:n relation they have, using the relation between the fields:
		/// ReportProcessingTaskTemplate.ReportProcessingTaskTemplateId - UIScheduleItem.ReportProcessingTaskTemplateId
		/// </summary>
		public virtual IEntityRelation UIScheduleItemEntityUsingReportProcessingTaskTemplateId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UIScheduleItemCollection" , true);
				relation.AddEntityFieldPair(ReportProcessingTaskTemplateFields.ReportProcessingTaskTemplateId, UIScheduleItemFields.ReportProcessingTaskTemplateId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportProcessingTaskTemplateEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIScheduleItemEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ReportProcessingTaskTemplateEntity and UIScheduleItemOccurrenceEntity over the 1:n relation they have, using the relation between the fields:
		/// ReportProcessingTaskTemplate.ReportProcessingTaskTemplateId - UIScheduleItemOccurrence.ReportProcessingTaskTemplateId
		/// </summary>
		public virtual IEntityRelation UIScheduleItemOccurrenceEntityUsingReportProcessingTaskTemplateId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UIScheduleItemOccurrenceCollection" , true);
				relation.AddEntityFieldPair(ReportProcessingTaskTemplateFields.ReportProcessingTaskTemplateId, UIScheduleItemOccurrenceFields.ReportProcessingTaskTemplateId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportProcessingTaskTemplateEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIScheduleItemOccurrenceEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between ReportProcessingTaskTemplateEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// ReportProcessingTaskTemplate.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, ReportProcessingTaskTemplateFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportProcessingTaskTemplateEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ReportProcessingTaskTemplateEntity and ReportProcessingTaskEntity over the m:1 relation they have, using the relation between the fields:
		/// ReportProcessingTaskTemplate.ReportProcessingTaskId - ReportProcessingTask.ReportProcessingTaskId
		/// </summary>
		public virtual IEntityRelation ReportProcessingTaskEntityUsingReportProcessingTaskId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ReportProcessingTaskEntity", false);
				relation.AddEntityFieldPair(ReportProcessingTaskFields.ReportProcessingTaskId, ReportProcessingTaskTemplateFields.ReportProcessingTaskId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportProcessingTaskEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportProcessingTaskTemplateEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticReportProcessingTaskTemplateRelations
	{
		internal static readonly IEntityRelation UIScheduleItemEntityUsingReportProcessingTaskTemplateIdStatic = new ReportProcessingTaskTemplateRelations().UIScheduleItemEntityUsingReportProcessingTaskTemplateId;
		internal static readonly IEntityRelation UIScheduleItemOccurrenceEntityUsingReportProcessingTaskTemplateIdStatic = new ReportProcessingTaskTemplateRelations().UIScheduleItemOccurrenceEntityUsingReportProcessingTaskTemplateId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new ReportProcessingTaskTemplateRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation ReportProcessingTaskEntityUsingReportProcessingTaskIdStatic = new ReportProcessingTaskTemplateRelations().ReportProcessingTaskEntityUsingReportProcessingTaskId;

		/// <summary>CTor</summary>
		static StaticReportProcessingTaskTemplateRelations()
		{
		}
	}
}
