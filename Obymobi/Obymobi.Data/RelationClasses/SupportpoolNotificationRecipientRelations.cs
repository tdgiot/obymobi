﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: SupportpoolNotificationRecipient. </summary>
	public partial class SupportpoolNotificationRecipientRelations
	{
		/// <summary>CTor</summary>
		public SupportpoolNotificationRecipientRelations()
		{
		}

		/// <summary>Gets all relations of the SupportpoolNotificationRecipientEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.SupportpoolEntityUsingSupportpoolId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between SupportpoolNotificationRecipientEntity and SupportpoolEntity over the m:1 relation they have, using the relation between the fields:
		/// SupportpoolNotificationRecipient.SupportpoolId - Supportpool.SupportpoolId
		/// </summary>
		public virtual IEntityRelation SupportpoolEntityUsingSupportpoolId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SupportpoolEntity", false);
				relation.AddEntityFieldPair(SupportpoolFields.SupportpoolId, SupportpoolNotificationRecipientFields.SupportpoolId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SupportpoolEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SupportpoolNotificationRecipientEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticSupportpoolNotificationRecipientRelations
	{
		internal static readonly IEntityRelation SupportpoolEntityUsingSupportpoolIdStatic = new SupportpoolNotificationRecipientRelations().SupportpoolEntityUsingSupportpoolId;

		/// <summary>CTor</summary>
		static StaticSupportpoolNotificationRecipientRelations()
		{
		}
	}
}
