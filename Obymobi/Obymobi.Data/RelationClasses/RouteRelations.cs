﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Route. </summary>
	public partial class RouteRelations
	{
		/// <summary>CTor</summary>
		public RouteRelations()
		{
		}

		/// <summary>Gets all relations of the RouteEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CategoryEntityUsingRouteId);
			toReturn.Add(this.ClientConfigurationRouteEntityUsingRouteId);
			toReturn.Add(this.CompanyEntityUsingRouteId);
			toReturn.Add(this.DeliverypointgroupEntityUsingEmailDocumentRouteId);
			toReturn.Add(this.DeliverypointgroupEntityUsingRouteId);
			toReturn.Add(this.DeliverypointgroupEntityUsingOrderNotesRouteId);
			toReturn.Add(this.DeliverypointgroupEntityUsingSystemMessageRouteId);
			toReturn.Add(this.ProductEntityUsingRouteId);
			toReturn.Add(this.RouteEntityUsingEscalationRouteId);
			toReturn.Add(this.RoutestepEntityUsingRouteId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			toReturn.Add(this.RouteEntityUsingRouteIdEscalationRouteId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between RouteEntity and CategoryEntity over the 1:n relation they have, using the relation between the fields:
		/// Route.RouteId - Category.RouteId
		/// </summary>
		public virtual IEntityRelation CategoryEntityUsingRouteId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CategoryCollection" , true);
				relation.AddEntityFieldPair(RouteFields.RouteId, CategoryFields.RouteId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RouteEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RouteEntity and ClientConfigurationRouteEntity over the 1:n relation they have, using the relation between the fields:
		/// Route.RouteId - ClientConfigurationRoute.RouteId
		/// </summary>
		public virtual IEntityRelation ClientConfigurationRouteEntityUsingRouteId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ClientConfigurationRouteCollection" , true);
				relation.AddEntityFieldPair(RouteFields.RouteId, ClientConfigurationRouteFields.RouteId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RouteEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientConfigurationRouteEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RouteEntity and CompanyEntity over the 1:n relation they have, using the relation between the fields:
		/// Route.RouteId - Company.RouteId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingRouteId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CompanyCollection" , true);
				relation.AddEntityFieldPair(RouteFields.RouteId, CompanyFields.RouteId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RouteEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RouteEntity and DeliverypointgroupEntity over the 1:n relation they have, using the relation between the fields:
		/// Route.RouteId - Deliverypointgroup.EmailDocumentRouteId
		/// </summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingEmailDocumentRouteId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeliverypointgroupCollection__" , true);
				relation.AddEntityFieldPair(RouteFields.RouteId, DeliverypointgroupFields.EmailDocumentRouteId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RouteEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RouteEntity and DeliverypointgroupEntity over the 1:n relation they have, using the relation between the fields:
		/// Route.RouteId - Deliverypointgroup.RouteId
		/// </summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingRouteId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeliverypointgroupCollection" , true);
				relation.AddEntityFieldPair(RouteFields.RouteId, DeliverypointgroupFields.RouteId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RouteEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RouteEntity and DeliverypointgroupEntity over the 1:n relation they have, using the relation between the fields:
		/// Route.RouteId - Deliverypointgroup.OrderNotesRouteId
		/// </summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingOrderNotesRouteId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeliverypointgroupCollection___" , true);
				relation.AddEntityFieldPair(RouteFields.RouteId, DeliverypointgroupFields.OrderNotesRouteId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RouteEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RouteEntity and DeliverypointgroupEntity over the 1:n relation they have, using the relation between the fields:
		/// Route.RouteId - Deliverypointgroup.SystemMessageRouteId
		/// </summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingSystemMessageRouteId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeliverypointgroupCollection_" , true);
				relation.AddEntityFieldPair(RouteFields.RouteId, DeliverypointgroupFields.SystemMessageRouteId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RouteEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RouteEntity and ProductEntity over the 1:n relation they have, using the relation between the fields:
		/// Route.RouteId - Product.RouteId
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingRouteId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ProductCollection" , true);
				relation.AddEntityFieldPair(RouteFields.RouteId, ProductFields.RouteId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RouteEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RouteEntity and RouteEntity over the 1:n relation they have, using the relation between the fields:
		/// Route.RouteId - Route.EscalationRouteId
		/// </summary>
		public virtual IEntityRelation RouteEntityUsingEscalationRouteId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "EscalationFromRouteCollection" , true);
				relation.AddEntityFieldPair(RouteFields.RouteId, RouteFields.EscalationRouteId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RouteEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RouteEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RouteEntity and RoutestepEntity over the 1:n relation they have, using the relation between the fields:
		/// Route.RouteId - Routestep.RouteId
		/// </summary>
		public virtual IEntityRelation RoutestepEntityUsingRouteId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RoutestepCollection" , true);
				relation.AddEntityFieldPair(RouteFields.RouteId, RoutestepFields.RouteId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RouteEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoutestepEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between RouteEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// Route.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, RouteFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RouteEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RouteEntity and RouteEntity over the m:1 relation they have, using the relation between the fields:
		/// Route.EscalationRouteId - Route.RouteId
		/// </summary>
		public virtual IEntityRelation RouteEntityUsingRouteIdEscalationRouteId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "EscalationRouteEntity", false);
				relation.AddEntityFieldPair(RouteFields.RouteId, RouteFields.EscalationRouteId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RouteEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RouteEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticRouteRelations
	{
		internal static readonly IEntityRelation CategoryEntityUsingRouteIdStatic = new RouteRelations().CategoryEntityUsingRouteId;
		internal static readonly IEntityRelation ClientConfigurationRouteEntityUsingRouteIdStatic = new RouteRelations().ClientConfigurationRouteEntityUsingRouteId;
		internal static readonly IEntityRelation CompanyEntityUsingRouteIdStatic = new RouteRelations().CompanyEntityUsingRouteId;
		internal static readonly IEntityRelation DeliverypointgroupEntityUsingEmailDocumentRouteIdStatic = new RouteRelations().DeliverypointgroupEntityUsingEmailDocumentRouteId;
		internal static readonly IEntityRelation DeliverypointgroupEntityUsingRouteIdStatic = new RouteRelations().DeliverypointgroupEntityUsingRouteId;
		internal static readonly IEntityRelation DeliverypointgroupEntityUsingOrderNotesRouteIdStatic = new RouteRelations().DeliverypointgroupEntityUsingOrderNotesRouteId;
		internal static readonly IEntityRelation DeliverypointgroupEntityUsingSystemMessageRouteIdStatic = new RouteRelations().DeliverypointgroupEntityUsingSystemMessageRouteId;
		internal static readonly IEntityRelation ProductEntityUsingRouteIdStatic = new RouteRelations().ProductEntityUsingRouteId;
		internal static readonly IEntityRelation RouteEntityUsingEscalationRouteIdStatic = new RouteRelations().RouteEntityUsingEscalationRouteId;
		internal static readonly IEntityRelation RoutestepEntityUsingRouteIdStatic = new RouteRelations().RoutestepEntityUsingRouteId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new RouteRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation RouteEntityUsingRouteIdEscalationRouteIdStatic = new RouteRelations().RouteEntityUsingRouteIdEscalationRouteId;

		/// <summary>CTor</summary>
		static StaticRouteRelations()
		{
		}
	}
}
