﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: PmsActionRule. </summary>
	public partial class PmsActionRuleRelations
	{
		/// <summary>CTor</summary>
		public PmsActionRuleRelations()
		{
		}

		/// <summary>Gets all relations of the PmsActionRuleEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.PmsRuleEntityUsingPmsActionRuleId);
			toReturn.Add(this.ClientConfigurationEntityUsingClientConfigurationId);
			toReturn.Add(this.MessagegroupEntityUsingMessagegroupId);
			toReturn.Add(this.PmsReportConfigurationEntityUsingPmsReportConfigurationId);
			toReturn.Add(this.ScheduledMessageEntityUsingScheduledMessageId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between PmsActionRuleEntity and PmsRuleEntity over the 1:n relation they have, using the relation between the fields:
		/// PmsActionRule.PmsActionRuleId - PmsRule.PmsActionRuleId
		/// </summary>
		public virtual IEntityRelation PmsRuleEntityUsingPmsActionRuleId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PmsRuleCollection" , true);
				relation.AddEntityFieldPair(PmsActionRuleFields.PmsActionRuleId, PmsRuleFields.PmsActionRuleId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PmsActionRuleEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PmsRuleEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between PmsActionRuleEntity and ClientConfigurationEntity over the m:1 relation they have, using the relation between the fields:
		/// PmsActionRule.ClientConfigurationId - ClientConfiguration.ClientConfigurationId
		/// </summary>
		public virtual IEntityRelation ClientConfigurationEntityUsingClientConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ClientConfigurationEntity", false);
				relation.AddEntityFieldPair(ClientConfigurationFields.ClientConfigurationId, PmsActionRuleFields.ClientConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientConfigurationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PmsActionRuleEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between PmsActionRuleEntity and MessagegroupEntity over the m:1 relation they have, using the relation between the fields:
		/// PmsActionRule.MessagegroupId - Messagegroup.MessagegroupId
		/// </summary>
		public virtual IEntityRelation MessagegroupEntityUsingMessagegroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "MessagegroupEntity", false);
				relation.AddEntityFieldPair(MessagegroupFields.MessagegroupId, PmsActionRuleFields.MessagegroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessagegroupEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PmsActionRuleEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between PmsActionRuleEntity and PmsReportConfigurationEntity over the m:1 relation they have, using the relation between the fields:
		/// PmsActionRule.PmsReportConfigurationId - PmsReportConfiguration.PmsReportConfigurationId
		/// </summary>
		public virtual IEntityRelation PmsReportConfigurationEntityUsingPmsReportConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PmsReportConfigurationEntity", false);
				relation.AddEntityFieldPair(PmsReportConfigurationFields.PmsReportConfigurationId, PmsActionRuleFields.PmsReportConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PmsReportConfigurationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PmsActionRuleEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between PmsActionRuleEntity and ScheduledMessageEntity over the m:1 relation they have, using the relation between the fields:
		/// PmsActionRule.ScheduledMessageId - ScheduledMessage.ScheduledMessageId
		/// </summary>
		public virtual IEntityRelation ScheduledMessageEntityUsingScheduledMessageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ScheduledMessageEntity", false);
				relation.AddEntityFieldPair(ScheduledMessageFields.ScheduledMessageId, PmsActionRuleFields.ScheduledMessageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduledMessageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PmsActionRuleEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPmsActionRuleRelations
	{
		internal static readonly IEntityRelation PmsRuleEntityUsingPmsActionRuleIdStatic = new PmsActionRuleRelations().PmsRuleEntityUsingPmsActionRuleId;
		internal static readonly IEntityRelation ClientConfigurationEntityUsingClientConfigurationIdStatic = new PmsActionRuleRelations().ClientConfigurationEntityUsingClientConfigurationId;
		internal static readonly IEntityRelation MessagegroupEntityUsingMessagegroupIdStatic = new PmsActionRuleRelations().MessagegroupEntityUsingMessagegroupId;
		internal static readonly IEntityRelation PmsReportConfigurationEntityUsingPmsReportConfigurationIdStatic = new PmsActionRuleRelations().PmsReportConfigurationEntityUsingPmsReportConfigurationId;
		internal static readonly IEntityRelation ScheduledMessageEntityUsingScheduledMessageIdStatic = new PmsActionRuleRelations().ScheduledMessageEntityUsingScheduledMessageId;

		/// <summary>CTor</summary>
		static StaticPmsActionRuleRelations()
		{
		}
	}
}
