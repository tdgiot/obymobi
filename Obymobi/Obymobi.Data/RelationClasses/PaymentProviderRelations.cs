﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: PaymentProvider. </summary>
	public partial class PaymentProviderRelations
	{
		/// <summary>CTor</summary>
		public PaymentProviderRelations()
		{
		}

		/// <summary>Gets all relations of the PaymentProviderEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.PaymentIntegrationConfigurationEntityUsingPaymentProviderId);
			toReturn.Add(this.PaymentProviderCompanyEntityUsingPaymentProviderId);
			toReturn.Add(this.PaymentTransactionEntityUsingPaymentProviderId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between PaymentProviderEntity and PaymentIntegrationConfigurationEntity over the 1:n relation they have, using the relation between the fields:
		/// PaymentProvider.PaymentProviderId - PaymentIntegrationConfiguration.PaymentProviderId
		/// </summary>
		public virtual IEntityRelation PaymentIntegrationConfigurationEntityUsingPaymentProviderId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PaymentIntegrationConfigurationCollection" , true);
				relation.AddEntityFieldPair(PaymentProviderFields.PaymentProviderId, PaymentIntegrationConfigurationFields.PaymentProviderId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentProviderEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentIntegrationConfigurationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PaymentProviderEntity and PaymentProviderCompanyEntity over the 1:n relation they have, using the relation between the fields:
		/// PaymentProvider.PaymentProviderId - PaymentProviderCompany.PaymentProviderId
		/// </summary>
		public virtual IEntityRelation PaymentProviderCompanyEntityUsingPaymentProviderId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PaymentProviderCompanyCollection" , true);
				relation.AddEntityFieldPair(PaymentProviderFields.PaymentProviderId, PaymentProviderCompanyFields.PaymentProviderId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentProviderEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentProviderCompanyEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PaymentProviderEntity and PaymentTransactionEntity over the 1:n relation they have, using the relation between the fields:
		/// PaymentProvider.PaymentProviderId - PaymentTransaction.PaymentProviderId
		/// </summary>
		public virtual IEntityRelation PaymentTransactionEntityUsingPaymentProviderId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PaymentTransactionCollection" , true);
				relation.AddEntityFieldPair(PaymentProviderFields.PaymentProviderId, PaymentTransactionFields.PaymentProviderId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentProviderEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentTransactionEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between PaymentProviderEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// PaymentProvider.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, PaymentProviderFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentProviderEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPaymentProviderRelations
	{
		internal static readonly IEntityRelation PaymentIntegrationConfigurationEntityUsingPaymentProviderIdStatic = new PaymentProviderRelations().PaymentIntegrationConfigurationEntityUsingPaymentProviderId;
		internal static readonly IEntityRelation PaymentProviderCompanyEntityUsingPaymentProviderIdStatic = new PaymentProviderRelations().PaymentProviderCompanyEntityUsingPaymentProviderId;
		internal static readonly IEntityRelation PaymentTransactionEntityUsingPaymentProviderIdStatic = new PaymentProviderRelations().PaymentTransactionEntityUsingPaymentProviderId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new PaymentProviderRelations().CompanyEntityUsingCompanyId;

		/// <summary>CTor</summary>
		static StaticPaymentProviderRelations()
		{
		}
	}
}
