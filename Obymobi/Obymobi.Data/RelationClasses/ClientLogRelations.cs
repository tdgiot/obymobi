﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ClientLog. </summary>
	public partial class ClientLogRelations
	{
		/// <summary>CTor</summary>
		public ClientLogRelations()
		{
		}

		/// <summary>Gets all relations of the ClientLogEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ClientEntityUsingClientId);
			toReturn.Add(this.ClientLogFileEntityUsingClientLogFileId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between ClientLogEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// ClientLog.ClientId - Client.ClientId
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingClientId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ClientEntity", false);
				relation.AddEntityFieldPair(ClientFields.ClientId, ClientLogFields.ClientId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientLogEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ClientLogEntity and ClientLogFileEntity over the m:1 relation they have, using the relation between the fields:
		/// ClientLog.ClientLogFileId - ClientLogFile.ClientLogFileId
		/// </summary>
		public virtual IEntityRelation ClientLogFileEntityUsingClientLogFileId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ClientLogFileEntity", false);
				relation.AddEntityFieldPair(ClientLogFileFields.ClientLogFileId, ClientLogFields.ClientLogFileId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientLogFileEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientLogEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticClientLogRelations
	{
		internal static readonly IEntityRelation ClientEntityUsingClientIdStatic = new ClientLogRelations().ClientEntityUsingClientId;
		internal static readonly IEntityRelation ClientLogFileEntityUsingClientLogFileIdStatic = new ClientLogRelations().ClientLogFileEntityUsingClientLogFileId;

		/// <summary>CTor</summary>
		static StaticClientLogRelations()
		{
		}
	}
}
