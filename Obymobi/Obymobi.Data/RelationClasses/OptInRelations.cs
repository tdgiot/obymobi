﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: OptIn. </summary>
	public partial class OptInRelations
	{
		/// <summary>CTor</summary>
		public OptInRelations()
		{
		}

		/// <summary>Gets all relations of the OptInEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CompanyEntityUsingParentCompanyId);
			toReturn.Add(this.OutletEntityUsingOutletId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between OptInEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// OptIn.ParentCompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingParentCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, OptInFields.ParentCompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OptInEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OptInEntity and OutletEntity over the m:1 relation they have, using the relation between the fields:
		/// OptIn.OutletId - Outlet.OutletId
		/// </summary>
		public virtual IEntityRelation OutletEntityUsingOutletId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "OutletEntity", false);
				relation.AddEntityFieldPair(OutletFields.OutletId, OptInFields.OutletId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OutletEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OptInEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticOptInRelations
	{
		internal static readonly IEntityRelation CompanyEntityUsingParentCompanyIdStatic = new OptInRelations().CompanyEntityUsingParentCompanyId;
		internal static readonly IEntityRelation OutletEntityUsingOutletIdStatic = new OptInRelations().OutletEntityUsingOutletId;

		/// <summary>CTor</summary>
		static StaticOptInRelations()
		{
		}
	}
}
