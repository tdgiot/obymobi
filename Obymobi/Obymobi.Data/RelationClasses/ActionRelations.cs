﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Action. </summary>
	public partial class ActionRelations
	{
		/// <summary>CTor</summary>
		public ActionRelations()
		{
		}

		/// <summary>Gets all relations of the ActionEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.NavigationMenuItemEntityUsingActionId);
			toReturn.Add(this.WidgetEntityUsingActionId);
			toReturn.Add(this.ApplicationConfigurationEntityUsingApplicationConfigurationId);
			toReturn.Add(this.LandingPageEntityUsingLandingPageId);
			toReturn.Add(this.CategoryEntityUsingCategoryId);
			toReturn.Add(this.ProductEntityUsingProductId);
			toReturn.Add(this.ProductCategoryEntityUsingProductCategoryId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ActionEntity and NavigationMenuItemEntity over the 1:n relation they have, using the relation between the fields:
		/// Action.ActionId - NavigationMenuItem.ActionId
		/// </summary>
		public virtual IEntityRelation NavigationMenuItemEntityUsingActionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "NavigationMenuItemCollection" , true);
				relation.AddEntityFieldPair(ActionFields.ActionId, NavigationMenuItemFields.ActionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ActionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NavigationMenuItemEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ActionEntity and WidgetEntity over the 1:n relation they have, using the relation between the fields:
		/// Action.ActionId - Widget.ActionId
		/// </summary>
		public virtual IEntityRelation WidgetEntityUsingActionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "WidgetCollection" , true);
				relation.AddEntityFieldPair(ActionFields.ActionId, WidgetFields.ActionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ActionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WidgetEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between ActionEntity and ApplicationConfigurationEntity over the m:1 relation they have, using the relation between the fields:
		/// Action.ApplicationConfigurationId - ApplicationConfiguration.ApplicationConfigurationId
		/// </summary>
		public virtual IEntityRelation ApplicationConfigurationEntityUsingApplicationConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ApplicationConfigurationEntity", false);
				relation.AddEntityFieldPair(ApplicationConfigurationFields.ApplicationConfigurationId, ActionFields.ApplicationConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ApplicationConfigurationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ActionEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ActionEntity and LandingPageEntity over the m:1 relation they have, using the relation between the fields:
		/// Action.LandingPageId - LandingPage.LandingPageId
		/// </summary>
		public virtual IEntityRelation LandingPageEntityUsingLandingPageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "LandingPageEntity", false);
				relation.AddEntityFieldPair(LandingPageFields.LandingPageId, ActionFields.LandingPageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LandingPageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ActionEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ActionEntity and CategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// Action.CategoryId - Category.CategoryId
		/// </summary>
		public virtual IEntityRelation CategoryEntityUsingCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CategoryEntity", false);
				relation.AddEntityFieldPair(CategoryFields.CategoryId, ActionFields.CategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ActionEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ActionEntity and ProductEntity over the m:1 relation they have, using the relation between the fields:
		/// Action.ProductId - Product.ProductId
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ProductEntity", false);
				relation.AddEntityFieldPair(ProductFields.ProductId, ActionFields.ProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ActionEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ActionEntity and ProductCategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// Action.ProductCategoryId - ProductCategory.ProductCategoryId
		/// </summary>
		public virtual IEntityRelation ProductCategoryEntityUsingProductCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ProductCategoryEntity", false);
				relation.AddEntityFieldPair(ProductCategoryFields.ProductCategoryId, ActionFields.ProductCategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductCategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ActionEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticActionRelations
	{
		internal static readonly IEntityRelation NavigationMenuItemEntityUsingActionIdStatic = new ActionRelations().NavigationMenuItemEntityUsingActionId;
		internal static readonly IEntityRelation WidgetEntityUsingActionIdStatic = new ActionRelations().WidgetEntityUsingActionId;
		internal static readonly IEntityRelation ApplicationConfigurationEntityUsingApplicationConfigurationIdStatic = new ActionRelations().ApplicationConfigurationEntityUsingApplicationConfigurationId;
		internal static readonly IEntityRelation LandingPageEntityUsingLandingPageIdStatic = new ActionRelations().LandingPageEntityUsingLandingPageId;
		internal static readonly IEntityRelation CategoryEntityUsingCategoryIdStatic = new ActionRelations().CategoryEntityUsingCategoryId;
		internal static readonly IEntityRelation ProductEntityUsingProductIdStatic = new ActionRelations().ProductEntityUsingProductId;
		internal static readonly IEntityRelation ProductCategoryEntityUsingProductCategoryIdStatic = new ActionRelations().ProductCategoryEntityUsingProductCategoryId;

		/// <summary>CTor</summary>
		static StaticActionRelations()
		{
		}
	}
}
