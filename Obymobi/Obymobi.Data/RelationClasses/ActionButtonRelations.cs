﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ActionButton. </summary>
	public partial class ActionButtonRelations
	{
		/// <summary>CTor</summary>
		public ActionButtonRelations()
		{
		}

		/// <summary>Gets all relations of the ActionButtonEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ActionButtonLanguageEntityUsingActionButtonId);
			toReturn.Add(this.CompanyEntityUsingActionButtonId);
			toReturn.Add(this.CustomTextEntityUsingActionButtonId);
			toReturn.Add(this.PointOfInterestEntityUsingActionButtonId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ActionButtonEntity and ActionButtonLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// ActionButton.ActionButtonId - ActionButtonLanguage.ActionButtonId
		/// </summary>
		public virtual IEntityRelation ActionButtonLanguageEntityUsingActionButtonId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ActionButtonLanguageCollection" , true);
				relation.AddEntityFieldPair(ActionButtonFields.ActionButtonId, ActionButtonLanguageFields.ActionButtonId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ActionButtonEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ActionButtonLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ActionButtonEntity and CompanyEntity over the 1:n relation they have, using the relation between the fields:
		/// ActionButton.ActionButtonId - Company.ActionButtonId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingActionButtonId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CompanyCollection" , true);
				relation.AddEntityFieldPair(ActionButtonFields.ActionButtonId, CompanyFields.ActionButtonId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ActionButtonEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ActionButtonEntity and CustomTextEntity over the 1:n relation they have, using the relation between the fields:
		/// ActionButton.ActionButtonId - CustomText.ActionButtonId
		/// </summary>
		public virtual IEntityRelation CustomTextEntityUsingActionButtonId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomTextCollection" , true);
				relation.AddEntityFieldPair(ActionButtonFields.ActionButtonId, CustomTextFields.ActionButtonId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ActionButtonEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ActionButtonEntity and PointOfInterestEntity over the 1:n relation they have, using the relation between the fields:
		/// ActionButton.ActionButtonId - PointOfInterest.ActionButtonId
		/// </summary>
		public virtual IEntityRelation PointOfInterestEntityUsingActionButtonId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PointOfInterestCollection" , true);
				relation.AddEntityFieldPair(ActionButtonFields.ActionButtonId, PointOfInterestFields.ActionButtonId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ActionButtonEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PointOfInterestEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticActionButtonRelations
	{
		internal static readonly IEntityRelation ActionButtonLanguageEntityUsingActionButtonIdStatic = new ActionButtonRelations().ActionButtonLanguageEntityUsingActionButtonId;
		internal static readonly IEntityRelation CompanyEntityUsingActionButtonIdStatic = new ActionButtonRelations().CompanyEntityUsingActionButtonId;
		internal static readonly IEntityRelation CustomTextEntityUsingActionButtonIdStatic = new ActionButtonRelations().CustomTextEntityUsingActionButtonId;
		internal static readonly IEntityRelation PointOfInterestEntityUsingActionButtonIdStatic = new ActionButtonRelations().PointOfInterestEntityUsingActionButtonId;

		/// <summary>CTor</summary>
		static StaticActionButtonRelations()
		{
		}
	}
}
