﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Menu. </summary>
	public partial class MenuRelations
	{
		/// <summary>CTor</summary>
		public MenuRelations()
		{
		}

		/// <summary>Gets all relations of the MenuEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CategoryEntityUsingMenuId);
			toReturn.Add(this.ClientConfigurationEntityUsingMenuId);
			toReturn.Add(this.DeliverypointgroupEntityUsingMenuId);
			toReturn.Add(this.TimestampEntityUsingMenuId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between MenuEntity and CategoryEntity over the 1:n relation they have, using the relation between the fields:
		/// Menu.MenuId - Category.MenuId
		/// </summary>
		public virtual IEntityRelation CategoryEntityUsingMenuId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CategoryCollection" , true);
				relation.AddEntityFieldPair(MenuFields.MenuId, CategoryFields.MenuId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MenuEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between MenuEntity and ClientConfigurationEntity over the 1:n relation they have, using the relation between the fields:
		/// Menu.MenuId - ClientConfiguration.MenuId
		/// </summary>
		public virtual IEntityRelation ClientConfigurationEntityUsingMenuId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ClientConfigurationCollection" , true);
				relation.AddEntityFieldPair(MenuFields.MenuId, ClientConfigurationFields.MenuId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MenuEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientConfigurationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between MenuEntity and DeliverypointgroupEntity over the 1:n relation they have, using the relation between the fields:
		/// Menu.MenuId - Deliverypointgroup.MenuId
		/// </summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingMenuId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeliverypointgroupCollection" , true);
				relation.AddEntityFieldPair(MenuFields.MenuId, DeliverypointgroupFields.MenuId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MenuEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between MenuEntity and TimestampEntity over the 1:1 relation they have, using the relation between the fields:
		/// Menu.MenuId - Timestamp.MenuId
		/// </summary>
		public virtual IEntityRelation TimestampEntityUsingMenuId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "TimestampCollection", true);


				relation.AddEntityFieldPair(MenuFields.MenuId, TimestampFields.MenuId);


				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MenuEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TimestampEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between MenuEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// Menu.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, MenuFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MenuEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticMenuRelations
	{
		internal static readonly IEntityRelation CategoryEntityUsingMenuIdStatic = new MenuRelations().CategoryEntityUsingMenuId;
		internal static readonly IEntityRelation ClientConfigurationEntityUsingMenuIdStatic = new MenuRelations().ClientConfigurationEntityUsingMenuId;
		internal static readonly IEntityRelation DeliverypointgroupEntityUsingMenuIdStatic = new MenuRelations().DeliverypointgroupEntityUsingMenuId;
		internal static readonly IEntityRelation TimestampEntityUsingMenuIdStatic = new MenuRelations().TimestampEntityUsingMenuId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new MenuRelations().CompanyEntityUsingCompanyId;

		/// <summary>CTor</summary>
		static StaticMenuRelations()
		{
		}
	}
}
