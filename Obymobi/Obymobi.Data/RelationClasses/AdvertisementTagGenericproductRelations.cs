﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: AdvertisementTagGenericproduct. </summary>
	public partial class AdvertisementTagGenericproductRelations
	{
		/// <summary>CTor</summary>
		public AdvertisementTagGenericproductRelations()
		{
		}

		/// <summary>Gets all relations of the AdvertisementTagGenericproductEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AdvertisementTagEntityUsingAdvertisementTagId);
			toReturn.Add(this.GenericproductEntityUsingGenericproductId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between AdvertisementTagGenericproductEntity and AdvertisementTagEntity over the m:1 relation they have, using the relation between the fields:
		/// AdvertisementTagGenericproduct.AdvertisementTagId - AdvertisementTag.AdvertisementTagId
		/// </summary>
		public virtual IEntityRelation AdvertisementTagEntityUsingAdvertisementTagId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AdvertisementTagEntity", false);
				relation.AddEntityFieldPair(AdvertisementTagFields.AdvertisementTagId, AdvertisementTagGenericproductFields.AdvertisementTagId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementTagEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementTagGenericproductEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AdvertisementTagGenericproductEntity and GenericproductEntity over the m:1 relation they have, using the relation between the fields:
		/// AdvertisementTagGenericproduct.GenericproductId - Genericproduct.GenericproductId
		/// </summary>
		public virtual IEntityRelation GenericproductEntityUsingGenericproductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "GenericproductEntity", false);
				relation.AddEntityFieldPair(GenericproductFields.GenericproductId, AdvertisementTagGenericproductFields.GenericproductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericproductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementTagGenericproductEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticAdvertisementTagGenericproductRelations
	{
		internal static readonly IEntityRelation AdvertisementTagEntityUsingAdvertisementTagIdStatic = new AdvertisementTagGenericproductRelations().AdvertisementTagEntityUsingAdvertisementTagId;
		internal static readonly IEntityRelation GenericproductEntityUsingGenericproductIdStatic = new AdvertisementTagGenericproductRelations().GenericproductEntityUsingGenericproductId;

		/// <summary>CTor</summary>
		static StaticAdvertisementTagGenericproductRelations()
		{
		}
	}
}
