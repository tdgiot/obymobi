﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Widget. </summary>
	public partial class WidgetRelations : IRelationFactory
	{
		/// <summary>CTor</summary>
		public WidgetRelations()
		{
		}

		/// <summary>Gets all relations of the WidgetEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.LandingPageWidgetEntityUsingWidgetId);
			toReturn.Add(this.NavigationMenuWidgetEntityUsingWidgetId);
			toReturn.Add(this.WidgetGroupWidgetEntityUsingWidgetId);
			toReturn.Add(this.CustomTextEntityUsingWidgetId);
			toReturn.Add(this.ActionEntityUsingActionId);
			toReturn.Add(this.ApplicationConfigurationEntityUsingApplicationConfigurationId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between WidgetEntity and LandingPageWidgetEntity over the 1:n relation they have, using the relation between the fields:
		/// Widget.WidgetId - LandingPageWidget.WidgetId
		/// </summary>
		public virtual IEntityRelation LandingPageWidgetEntityUsingWidgetId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "LandingPageWidgetCollection" , true);
				relation.AddEntityFieldPair(WidgetFields.WidgetId, LandingPageWidgetFields.WidgetId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WidgetEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LandingPageWidgetEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between WidgetEntity and NavigationMenuWidgetEntity over the 1:n relation they have, using the relation between the fields:
		/// Widget.WidgetId - NavigationMenuWidget.WidgetId
		/// </summary>
		public virtual IEntityRelation NavigationMenuWidgetEntityUsingWidgetId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "NavigationMenuWidgetCollection" , true);
				relation.AddEntityFieldPair(WidgetFields.WidgetId, NavigationMenuWidgetFields.WidgetId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WidgetEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NavigationMenuWidgetEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between WidgetEntity and WidgetGroupWidgetEntity over the 1:n relation they have, using the relation between the fields:
		/// Widget.WidgetId - WidgetGroupWidget.WidgetId
		/// </summary>
		public virtual IEntityRelation WidgetGroupWidgetEntityUsingWidgetId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ChildWidgetCollection" , true);
				relation.AddEntityFieldPair(WidgetFields.WidgetId, WidgetGroupWidgetFields.WidgetId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WidgetEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WidgetGroupWidgetEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between WidgetEntity and CustomTextEntity over the 1:n relation they have, using the relation between the fields:
		/// Widget.WidgetId - CustomText.WidgetId
		/// </summary>
		public virtual IEntityRelation CustomTextEntityUsingWidgetId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomTextCollection" , true);
				relation.AddEntityFieldPair(WidgetFields.WidgetId, CustomTextFields.WidgetId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WidgetEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between WidgetEntity and ActionEntity over the m:1 relation they have, using the relation between the fields:
		/// Widget.ActionId - Action.ActionId
		/// </summary>
		public virtual IEntityRelation ActionEntityUsingActionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ActionEntity", false);
				relation.AddEntityFieldPair(ActionFields.ActionId, WidgetFields.ActionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ActionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WidgetEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between WidgetEntity and ApplicationConfigurationEntity over the m:1 relation they have, using the relation between the fields:
		/// Widget.ApplicationConfigurationId - ApplicationConfiguration.ApplicationConfigurationId
		/// </summary>
		public virtual IEntityRelation ApplicationConfigurationEntityUsingApplicationConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ApplicationConfigurationEntity", false);
				relation.AddEntityFieldPair(ApplicationConfigurationFields.ApplicationConfigurationId, WidgetFields.ApplicationConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ApplicationConfigurationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WidgetEntity", true);
				return relation;
			}
		}



		/// <summary>Returns a new IEntityRelation object, between WidgetEntity and WidgetActionBannerEntity over the 1:1 relation they have, which is used to build a target per entity hierarchy</summary>		
		internal IEntityRelation RelationToSubTypeWidgetActionBannerEntity
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, true);
				relation.AddEntityFieldPair(WidgetFields.WidgetId, WidgetActionBannerFields.WidgetId);
				relation.IsHierarchyRelation=true;
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between WidgetEntity and WidgetActionButtonEntity over the 1:1 relation they have, which is used to build a target per entity hierarchy</summary>		
		internal IEntityRelation RelationToSubTypeWidgetActionButtonEntity
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, true);
				relation.AddEntityFieldPair(WidgetFields.WidgetId, WidgetActionButtonFields.WidgetId);
				relation.IsHierarchyRelation=true;
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between WidgetEntity and WidgetCarouselEntity over the 1:1 relation they have, which is used to build a target per entity hierarchy</summary>		
		internal IEntityRelation RelationToSubTypeWidgetCarouselEntity
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, true);
				relation.AddEntityFieldPair(WidgetFields.WidgetId, WidgetCarouselFields.WidgetId);
				relation.IsHierarchyRelation=true;
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between WidgetEntity and WidgetGroupEntity over the 1:1 relation they have, which is used to build a target per entity hierarchy</summary>		
		internal IEntityRelation RelationToSubTypeWidgetGroupEntity
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, true);
				relation.AddEntityFieldPair(WidgetFields.WidgetId, WidgetGroupFields.WidgetId);
				relation.IsHierarchyRelation=true;
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between WidgetEntity and WidgetHeroEntity over the 1:1 relation they have, which is used to build a target per entity hierarchy</summary>		
		internal IEntityRelation RelationToSubTypeWidgetHeroEntity
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, true);
				relation.AddEntityFieldPair(WidgetFields.WidgetId, WidgetHeroFields.WidgetId);
				relation.IsHierarchyRelation=true;
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between WidgetEntity and WidgetLanguageSwitcherEntity over the 1:1 relation they have, which is used to build a target per entity hierarchy</summary>		
		internal IEntityRelation RelationToSubTypeWidgetLanguageSwitcherEntity
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, true);
				relation.AddEntityFieldPair(WidgetFields.WidgetId, WidgetLanguageSwitcherFields.WidgetId);
				relation.IsHierarchyRelation=true;
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between WidgetEntity and WidgetMarkdownEntity over the 1:1 relation they have, which is used to build a target per entity hierarchy</summary>		
		internal IEntityRelation RelationToSubTypeWidgetMarkdownEntity
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, true);
				relation.AddEntityFieldPair(WidgetFields.WidgetId, WidgetMarkdownFields.WidgetId);
				relation.IsHierarchyRelation=true;
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between WidgetEntity and WidgetOpeningTimeEntity over the 1:1 relation they have, which is used to build a target per entity hierarchy</summary>		
		internal IEntityRelation RelationToSubTypeWidgetOpeningTimeEntity
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, true);
				relation.AddEntityFieldPair(WidgetFields.WidgetId, WidgetOpeningTimeFields.WidgetId);
				relation.IsHierarchyRelation=true;
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between WidgetEntity and WidgetPageTitleEntity over the 1:1 relation they have, which is used to build a target per entity hierarchy</summary>		
		internal IEntityRelation RelationToSubTypeWidgetPageTitleEntity
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, true);
				relation.AddEntityFieldPair(WidgetFields.WidgetId, WidgetPageTitleFields.WidgetId);
				relation.IsHierarchyRelation=true;
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between WidgetEntity and WidgetWaitTimeEntity over the 1:1 relation they have, which is used to build a target per entity hierarchy</summary>		
		internal IEntityRelation RelationToSubTypeWidgetWaitTimeEntity
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, true);
				relation.AddEntityFieldPair(WidgetFields.WidgetId, WidgetWaitTimeFields.WidgetId);
				relation.IsHierarchyRelation=true;
				return relation;
			}
		}
		
		/// <summary>Returns the relation object the entity, to which this relation factory belongs, has with the subtype with the specified name</summary>
		/// <param name="subTypeEntityName">name of direct subtype which is a subtype of the current entity through the relation to return.</param>
		/// <returns>relation which makes the current entity a supertype of the subtype entity with the name specified, or null if not applicable/found</returns>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName)
		{
			switch(subTypeEntityName)
			{
				case "WidgetActionBannerEntity":
					return this.RelationToSubTypeWidgetActionBannerEntity;
				case "WidgetActionButtonEntity":
					return this.RelationToSubTypeWidgetActionButtonEntity;
				case "WidgetCarouselEntity":
					return this.RelationToSubTypeWidgetCarouselEntity;
				case "WidgetGroupEntity":
					return this.RelationToSubTypeWidgetGroupEntity;
				case "WidgetHeroEntity":
					return this.RelationToSubTypeWidgetHeroEntity;
				case "WidgetLanguageSwitcherEntity":
					return this.RelationToSubTypeWidgetLanguageSwitcherEntity;
				case "WidgetMarkdownEntity":
					return this.RelationToSubTypeWidgetMarkdownEntity;
				case "WidgetOpeningTimeEntity":
					return this.RelationToSubTypeWidgetOpeningTimeEntity;
				case "WidgetPageTitleEntity":
					return this.RelationToSubTypeWidgetPageTitleEntity;
				case "WidgetWaitTimeEntity":
					return this.RelationToSubTypeWidgetWaitTimeEntity;
				default:
					return null;
			}
		}
		
		/// <summary>Returns the relation object the entity, to which this relation factory belongs, has with its supertype, if applicable.</summary>
		/// <returns>relation which makes the current entity a subtype of its supertype entity or null if not applicable/found</returns>
		public virtual IEntityRelation GetSuperTypeRelation()
		{
			return null;
		}

		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticWidgetRelations
	{
		internal static readonly IEntityRelation LandingPageWidgetEntityUsingWidgetIdStatic = new WidgetRelations().LandingPageWidgetEntityUsingWidgetId;
		internal static readonly IEntityRelation NavigationMenuWidgetEntityUsingWidgetIdStatic = new WidgetRelations().NavigationMenuWidgetEntityUsingWidgetId;
		internal static readonly IEntityRelation WidgetGroupWidgetEntityUsingWidgetIdStatic = new WidgetRelations().WidgetGroupWidgetEntityUsingWidgetId;
		internal static readonly IEntityRelation CustomTextEntityUsingWidgetIdStatic = new WidgetRelations().CustomTextEntityUsingWidgetId;
		internal static readonly IEntityRelation ActionEntityUsingActionIdStatic = new WidgetRelations().ActionEntityUsingActionId;
		internal static readonly IEntityRelation ApplicationConfigurationEntityUsingApplicationConfigurationIdStatic = new WidgetRelations().ApplicationConfigurationEntityUsingApplicationConfigurationId;

		/// <summary>CTor</summary>
		static StaticWidgetRelations()
		{
		}
	}
}
