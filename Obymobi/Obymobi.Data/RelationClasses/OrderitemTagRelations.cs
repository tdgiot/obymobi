﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: OrderitemTag. </summary>
	public partial class OrderitemTagRelations
	{
		/// <summary>CTor</summary>
		public OrderitemTagRelations()
		{
		}

		/// <summary>Gets all relations of the OrderitemTagEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.OrderitemEntityUsingOrderitemId);
			toReturn.Add(this.ProductEntityUsingProductId);
			toReturn.Add(this.ProductCategoryTagEntityUsingProductCategoryTagId);
			toReturn.Add(this.TagEntityUsingTagId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between OrderitemTagEntity and OrderitemEntity over the m:1 relation they have, using the relation between the fields:
		/// OrderitemTag.OrderitemId - Orderitem.OrderitemId
		/// </summary>
		public virtual IEntityRelation OrderitemEntityUsingOrderitemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "OrderitemEntity", false);
				relation.AddEntityFieldPair(OrderitemFields.OrderitemId, OrderitemTagFields.OrderitemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderitemEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderitemTagEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OrderitemTagEntity and ProductEntity over the m:1 relation they have, using the relation between the fields:
		/// OrderitemTag.ProductId - Product.ProductId
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ProductEntity", false);
				relation.AddEntityFieldPair(ProductFields.ProductId, OrderitemTagFields.ProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderitemTagEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OrderitemTagEntity and ProductCategoryTagEntity over the m:1 relation they have, using the relation between the fields:
		/// OrderitemTag.ProductCategoryTagId - ProductCategoryTag.ProductCategoryTagId
		/// </summary>
		public virtual IEntityRelation ProductCategoryTagEntityUsingProductCategoryTagId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ProductCategoryTagEntity", false);
				relation.AddEntityFieldPair(ProductCategoryTagFields.ProductCategoryTagId, OrderitemTagFields.ProductCategoryTagId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductCategoryTagEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderitemTagEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OrderitemTagEntity and TagEntity over the m:1 relation they have, using the relation between the fields:
		/// OrderitemTag.TagId - Tag.TagId
		/// </summary>
		public virtual IEntityRelation TagEntityUsingTagId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TagEntity", false);
				relation.AddEntityFieldPair(TagFields.TagId, OrderitemTagFields.TagId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TagEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderitemTagEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticOrderitemTagRelations
	{
		internal static readonly IEntityRelation OrderitemEntityUsingOrderitemIdStatic = new OrderitemTagRelations().OrderitemEntityUsingOrderitemId;
		internal static readonly IEntityRelation ProductEntityUsingProductIdStatic = new OrderitemTagRelations().ProductEntityUsingProductId;
		internal static readonly IEntityRelation ProductCategoryTagEntityUsingProductCategoryTagIdStatic = new OrderitemTagRelations().ProductCategoryTagEntityUsingProductCategoryTagId;
		internal static readonly IEntityRelation TagEntityUsingTagIdStatic = new OrderitemTagRelations().TagEntityUsingTagId;

		/// <summary>CTor</summary>
		static StaticOrderitemTagRelations()
		{
		}
	}
}
