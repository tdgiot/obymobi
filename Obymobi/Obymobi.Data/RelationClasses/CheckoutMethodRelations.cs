﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: CheckoutMethod. </summary>
	public partial class CheckoutMethodRelations
	{
		/// <summary>CTor</summary>
		public CheckoutMethodRelations()
		{
		}

		/// <summary>Gets all relations of the CheckoutMethodEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CheckoutMethodDeliverypointgroupEntityUsingCheckoutMethodId);
			toReturn.Add(this.CustomTextEntityUsingCheckoutMethodId);
			toReturn.Add(this.OrderEntityUsingCheckoutMethodId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			toReturn.Add(this.OutletEntityUsingOutletId);
			toReturn.Add(this.PaymentIntegrationConfigurationEntityUsingPaymentIntegrationConfigurationId);
			toReturn.Add(this.ReceiptTemplateEntityUsingReceiptTemplateId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between CheckoutMethodEntity and CheckoutMethodDeliverypointgroupEntity over the 1:n relation they have, using the relation between the fields:
		/// CheckoutMethod.CheckoutMethodId - CheckoutMethodDeliverypointgroup.CheckoutMethodId
		/// </summary>
		public virtual IEntityRelation CheckoutMethodDeliverypointgroupEntityUsingCheckoutMethodId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CheckoutMethodDeliverypointgroupCollection" , true);
				relation.AddEntityFieldPair(CheckoutMethodFields.CheckoutMethodId, CheckoutMethodDeliverypointgroupFields.CheckoutMethodId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CheckoutMethodEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CheckoutMethodDeliverypointgroupEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CheckoutMethodEntity and CustomTextEntity over the 1:n relation they have, using the relation between the fields:
		/// CheckoutMethod.CheckoutMethodId - CustomText.CheckoutMethodId
		/// </summary>
		public virtual IEntityRelation CustomTextEntityUsingCheckoutMethodId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomTextCollection" , true);
				relation.AddEntityFieldPair(CheckoutMethodFields.CheckoutMethodId, CustomTextFields.CheckoutMethodId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CheckoutMethodEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CheckoutMethodEntity and OrderEntity over the 1:n relation they have, using the relation between the fields:
		/// CheckoutMethod.CheckoutMethodId - Order.CheckoutMethodId
		/// </summary>
		public virtual IEntityRelation OrderEntityUsingCheckoutMethodId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrderCollection" , true);
				relation.AddEntityFieldPair(CheckoutMethodFields.CheckoutMethodId, OrderFields.CheckoutMethodId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CheckoutMethodEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between CheckoutMethodEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// CheckoutMethod.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, CheckoutMethodFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CheckoutMethodEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CheckoutMethodEntity and OutletEntity over the m:1 relation they have, using the relation between the fields:
		/// CheckoutMethod.OutletId - Outlet.OutletId
		/// </summary>
		public virtual IEntityRelation OutletEntityUsingOutletId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "OutletEntity", false);
				relation.AddEntityFieldPair(OutletFields.OutletId, CheckoutMethodFields.OutletId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OutletEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CheckoutMethodEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CheckoutMethodEntity and PaymentIntegrationConfigurationEntity over the m:1 relation they have, using the relation between the fields:
		/// CheckoutMethod.PaymentIntegrationConfigurationId - PaymentIntegrationConfiguration.PaymentIntegrationConfigurationId
		/// </summary>
		public virtual IEntityRelation PaymentIntegrationConfigurationEntityUsingPaymentIntegrationConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PaymentIntegrationConfigurationEntity", false);
				relation.AddEntityFieldPair(PaymentIntegrationConfigurationFields.PaymentIntegrationConfigurationId, CheckoutMethodFields.PaymentIntegrationConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentIntegrationConfigurationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CheckoutMethodEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CheckoutMethodEntity and ReceiptTemplateEntity over the m:1 relation they have, using the relation between the fields:
		/// CheckoutMethod.ReceiptTemplateId - ReceiptTemplate.ReceiptTemplateId
		/// </summary>
		public virtual IEntityRelation ReceiptTemplateEntityUsingReceiptTemplateId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ReceiptTemplateEntity", false);
				relation.AddEntityFieldPair(ReceiptTemplateFields.ReceiptTemplateId, CheckoutMethodFields.ReceiptTemplateId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReceiptTemplateEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CheckoutMethodEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCheckoutMethodRelations
	{
		internal static readonly IEntityRelation CheckoutMethodDeliverypointgroupEntityUsingCheckoutMethodIdStatic = new CheckoutMethodRelations().CheckoutMethodDeliverypointgroupEntityUsingCheckoutMethodId;
		internal static readonly IEntityRelation CustomTextEntityUsingCheckoutMethodIdStatic = new CheckoutMethodRelations().CustomTextEntityUsingCheckoutMethodId;
		internal static readonly IEntityRelation OrderEntityUsingCheckoutMethodIdStatic = new CheckoutMethodRelations().OrderEntityUsingCheckoutMethodId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new CheckoutMethodRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation OutletEntityUsingOutletIdStatic = new CheckoutMethodRelations().OutletEntityUsingOutletId;
		internal static readonly IEntityRelation PaymentIntegrationConfigurationEntityUsingPaymentIntegrationConfigurationIdStatic = new CheckoutMethodRelations().PaymentIntegrationConfigurationEntityUsingPaymentIntegrationConfigurationId;
		internal static readonly IEntityRelation ReceiptTemplateEntityUsingReceiptTemplateIdStatic = new CheckoutMethodRelations().ReceiptTemplateEntityUsingReceiptTemplateId;

		/// <summary>CTor</summary>
		static StaticCheckoutMethodRelations()
		{
		}
	}
}
