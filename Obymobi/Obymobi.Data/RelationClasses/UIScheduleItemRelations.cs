﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: UIScheduleItem. </summary>
	public partial class UIScheduleItemRelations
	{
		/// <summary>CTor</summary>
		public UIScheduleItemRelations()
		{
		}

		/// <summary>Gets all relations of the UIScheduleItemEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.UIScheduleItemOccurrenceEntityUsingUIScheduleItemId);
			toReturn.Add(this.MediaEntityUsingMediaId);
			toReturn.Add(this.ReportProcessingTaskTemplateEntityUsingReportProcessingTaskTemplateId);
			toReturn.Add(this.ScheduledMessageEntityUsingScheduledMessageId);
			toReturn.Add(this.UIScheduleEntityUsingUIScheduleId);
			toReturn.Add(this.UIWidgetEntityUsingUIWidgetId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between UIScheduleItemEntity and UIScheduleItemOccurrenceEntity over the 1:n relation they have, using the relation between the fields:
		/// UIScheduleItem.UIScheduleItemId - UIScheduleItemOccurrence.UIScheduleItemId
		/// </summary>
		public virtual IEntityRelation UIScheduleItemOccurrenceEntityUsingUIScheduleItemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UIScheduleItemOccurrenceCollection" , true);
				relation.AddEntityFieldPair(UIScheduleItemFields.UIScheduleItemId, UIScheduleItemOccurrenceFields.UIScheduleItemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIScheduleItemEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIScheduleItemOccurrenceEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between UIScheduleItemEntity and MediaEntity over the m:1 relation they have, using the relation between the fields:
		/// UIScheduleItem.MediaId - Media.MediaId
		/// </summary>
		public virtual IEntityRelation MediaEntityUsingMediaId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "MediaEntity", false);
				relation.AddEntityFieldPair(MediaFields.MediaId, UIScheduleItemFields.MediaId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIScheduleItemEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between UIScheduleItemEntity and ReportProcessingTaskTemplateEntity over the m:1 relation they have, using the relation between the fields:
		/// UIScheduleItem.ReportProcessingTaskTemplateId - ReportProcessingTaskTemplate.ReportProcessingTaskTemplateId
		/// </summary>
		public virtual IEntityRelation ReportProcessingTaskTemplateEntityUsingReportProcessingTaskTemplateId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ReportProcessingTaskTemplateEntity", false);
				relation.AddEntityFieldPair(ReportProcessingTaskTemplateFields.ReportProcessingTaskTemplateId, UIScheduleItemFields.ReportProcessingTaskTemplateId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportProcessingTaskTemplateEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIScheduleItemEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between UIScheduleItemEntity and ScheduledMessageEntity over the m:1 relation they have, using the relation between the fields:
		/// UIScheduleItem.ScheduledMessageId - ScheduledMessage.ScheduledMessageId
		/// </summary>
		public virtual IEntityRelation ScheduledMessageEntityUsingScheduledMessageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ScheduledMessageEntity", false);
				relation.AddEntityFieldPair(ScheduledMessageFields.ScheduledMessageId, UIScheduleItemFields.ScheduledMessageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduledMessageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIScheduleItemEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between UIScheduleItemEntity and UIScheduleEntity over the m:1 relation they have, using the relation between the fields:
		/// UIScheduleItem.UIScheduleId - UISchedule.UIScheduleId
		/// </summary>
		public virtual IEntityRelation UIScheduleEntityUsingUIScheduleId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UIScheduleEntity", false);
				relation.AddEntityFieldPair(UIScheduleFields.UIScheduleId, UIScheduleItemFields.UIScheduleId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIScheduleEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIScheduleItemEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between UIScheduleItemEntity and UIWidgetEntity over the m:1 relation they have, using the relation between the fields:
		/// UIScheduleItem.UIWidgetId - UIWidget.UIWidgetId
		/// </summary>
		public virtual IEntityRelation UIWidgetEntityUsingUIWidgetId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UIWidgetEntity", false);
				relation.AddEntityFieldPair(UIWidgetFields.UIWidgetId, UIScheduleItemFields.UIWidgetId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIWidgetEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIScheduleItemEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticUIScheduleItemRelations
	{
		internal static readonly IEntityRelation UIScheduleItemOccurrenceEntityUsingUIScheduleItemIdStatic = new UIScheduleItemRelations().UIScheduleItemOccurrenceEntityUsingUIScheduleItemId;
		internal static readonly IEntityRelation MediaEntityUsingMediaIdStatic = new UIScheduleItemRelations().MediaEntityUsingMediaId;
		internal static readonly IEntityRelation ReportProcessingTaskTemplateEntityUsingReportProcessingTaskTemplateIdStatic = new UIScheduleItemRelations().ReportProcessingTaskTemplateEntityUsingReportProcessingTaskTemplateId;
		internal static readonly IEntityRelation ScheduledMessageEntityUsingScheduledMessageIdStatic = new UIScheduleItemRelations().ScheduledMessageEntityUsingScheduledMessageId;
		internal static readonly IEntityRelation UIScheduleEntityUsingUIScheduleIdStatic = new UIScheduleItemRelations().UIScheduleEntityUsingUIScheduleId;
		internal static readonly IEntityRelation UIWidgetEntityUsingUIWidgetIdStatic = new UIScheduleItemRelations().UIWidgetEntityUsingUIWidgetId;

		/// <summary>CTor</summary>
		static StaticUIScheduleItemRelations()
		{
		}
	}
}
