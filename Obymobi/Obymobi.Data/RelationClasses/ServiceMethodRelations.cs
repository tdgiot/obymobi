﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ServiceMethod. </summary>
	public partial class ServiceMethodRelations
	{
		/// <summary>CTor</summary>
		public ServiceMethodRelations()
		{
		}

		/// <summary>Gets all relations of the ServiceMethodEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CustomTextEntityUsingServiceMethodId);
			toReturn.Add(this.DeliveryDistanceEntityUsingServiceMethodId);
			toReturn.Add(this.OrderEntityUsingServiceMethodId);
			toReturn.Add(this.ServiceMethodDeliverypointgroupEntityUsingServiceMethodId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			toReturn.Add(this.OutletEntityUsingOutletId);
			toReturn.Add(this.ProductEntityUsingServiceChargeProductId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ServiceMethodEntity and CustomTextEntity over the 1:n relation they have, using the relation between the fields:
		/// ServiceMethod.ServiceMethodId - CustomText.ServiceMethodId
		/// </summary>
		public virtual IEntityRelation CustomTextEntityUsingServiceMethodId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomTextCollection" , true);
				relation.AddEntityFieldPair(ServiceMethodFields.ServiceMethodId, CustomTextFields.ServiceMethodId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ServiceMethodEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ServiceMethodEntity and DeliveryDistanceEntity over the 1:n relation they have, using the relation between the fields:
		/// ServiceMethod.ServiceMethodId - DeliveryDistance.ServiceMethodId
		/// </summary>
		public virtual IEntityRelation DeliveryDistanceEntityUsingServiceMethodId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeliveryDistanceCollection" , true);
				relation.AddEntityFieldPair(ServiceMethodFields.ServiceMethodId, DeliveryDistanceFields.ServiceMethodId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ServiceMethodEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliveryDistanceEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ServiceMethodEntity and OrderEntity over the 1:n relation they have, using the relation between the fields:
		/// ServiceMethod.ServiceMethodId - Order.ServiceMethodId
		/// </summary>
		public virtual IEntityRelation OrderEntityUsingServiceMethodId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrderCollection" , true);
				relation.AddEntityFieldPair(ServiceMethodFields.ServiceMethodId, OrderFields.ServiceMethodId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ServiceMethodEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ServiceMethodEntity and ServiceMethodDeliverypointgroupEntity over the 1:n relation they have, using the relation between the fields:
		/// ServiceMethod.ServiceMethodId - ServiceMethodDeliverypointgroup.ServiceMethodId
		/// </summary>
		public virtual IEntityRelation ServiceMethodDeliverypointgroupEntityUsingServiceMethodId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ServiceMethodDeliverypointgroupCollection" , true);
				relation.AddEntityFieldPair(ServiceMethodFields.ServiceMethodId, ServiceMethodDeliverypointgroupFields.ServiceMethodId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ServiceMethodEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ServiceMethodDeliverypointgroupEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between ServiceMethodEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// ServiceMethod.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, ServiceMethodFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ServiceMethodEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ServiceMethodEntity and OutletEntity over the m:1 relation they have, using the relation between the fields:
		/// ServiceMethod.OutletId - Outlet.OutletId
		/// </summary>
		public virtual IEntityRelation OutletEntityUsingOutletId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "OutletEntity", false);
				relation.AddEntityFieldPair(OutletFields.OutletId, ServiceMethodFields.OutletId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OutletEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ServiceMethodEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ServiceMethodEntity and ProductEntity over the m:1 relation they have, using the relation between the fields:
		/// ServiceMethod.ServiceChargeProductId - Product.ProductId
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingServiceChargeProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ProductEntity", false);
				relation.AddEntityFieldPair(ProductFields.ProductId, ServiceMethodFields.ServiceChargeProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ServiceMethodEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticServiceMethodRelations
	{
		internal static readonly IEntityRelation CustomTextEntityUsingServiceMethodIdStatic = new ServiceMethodRelations().CustomTextEntityUsingServiceMethodId;
		internal static readonly IEntityRelation DeliveryDistanceEntityUsingServiceMethodIdStatic = new ServiceMethodRelations().DeliveryDistanceEntityUsingServiceMethodId;
		internal static readonly IEntityRelation OrderEntityUsingServiceMethodIdStatic = new ServiceMethodRelations().OrderEntityUsingServiceMethodId;
		internal static readonly IEntityRelation ServiceMethodDeliverypointgroupEntityUsingServiceMethodIdStatic = new ServiceMethodRelations().ServiceMethodDeliverypointgroupEntityUsingServiceMethodId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new ServiceMethodRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation OutletEntityUsingOutletIdStatic = new ServiceMethodRelations().OutletEntityUsingOutletId;
		internal static readonly IEntityRelation ProductEntityUsingServiceChargeProductIdStatic = new ServiceMethodRelations().ProductEntityUsingServiceChargeProductId;

		/// <summary>CTor</summary>
		static StaticServiceMethodRelations()
		{
		}
	}
}
