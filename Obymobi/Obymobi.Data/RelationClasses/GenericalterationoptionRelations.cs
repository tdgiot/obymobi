﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Genericalterationoption. </summary>
	public partial class GenericalterationoptionRelations
	{
		/// <summary>CTor</summary>
		public GenericalterationoptionRelations()
		{
		}

		/// <summary>Gets all relations of the GenericalterationoptionEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AlterationoptionEntityUsingGenericalterationoptionId);
			toReturn.Add(this.GenericalterationitemEntityUsingGenericalterationoptionId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between GenericalterationoptionEntity and AlterationoptionEntity over the 1:n relation they have, using the relation between the fields:
		/// Genericalterationoption.GenericalterationoptionId - Alterationoption.GenericalterationoptionId
		/// </summary>
		public virtual IEntityRelation AlterationoptionEntityUsingGenericalterationoptionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AlterationoptionCollection" , true);
				relation.AddEntityFieldPair(GenericalterationoptionFields.GenericalterationoptionId, AlterationoptionFields.GenericalterationoptionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericalterationoptionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationoptionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between GenericalterationoptionEntity and GenericalterationitemEntity over the 1:n relation they have, using the relation between the fields:
		/// Genericalterationoption.GenericalterationoptionId - Genericalterationitem.GenericalterationoptionId
		/// </summary>
		public virtual IEntityRelation GenericalterationitemEntityUsingGenericalterationoptionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "GenericalterationitemCollection" , true);
				relation.AddEntityFieldPair(GenericalterationoptionFields.GenericalterationoptionId, GenericalterationitemFields.GenericalterationoptionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericalterationoptionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericalterationitemEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticGenericalterationoptionRelations
	{
		internal static readonly IEntityRelation AlterationoptionEntityUsingGenericalterationoptionIdStatic = new GenericalterationoptionRelations().AlterationoptionEntityUsingGenericalterationoptionId;
		internal static readonly IEntityRelation GenericalterationitemEntityUsingGenericalterationoptionIdStatic = new GenericalterationoptionRelations().GenericalterationitemEntityUsingGenericalterationoptionId;

		/// <summary>CTor</summary>
		static StaticGenericalterationoptionRelations()
		{
		}
	}
}
