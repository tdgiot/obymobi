﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: TaxTariff. </summary>
	public partial class TaxTariffRelations
	{
		/// <summary>CTor</summary>
		public TaxTariffRelations()
		{
		}

		/// <summary>Gets all relations of the TaxTariffEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AlterationoptionEntityUsingTaxTariffId);
			toReturn.Add(this.OrderitemEntityUsingTaxTariffId);
			toReturn.Add(this.OrderitemAlterationitemEntityUsingTaxTariffId);
			toReturn.Add(this.ProductEntityUsingTaxTariffId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between TaxTariffEntity and AlterationoptionEntity over the 1:n relation they have, using the relation between the fields:
		/// TaxTariff.TaxTariffId - Alterationoption.TaxTariffId
		/// </summary>
		public virtual IEntityRelation AlterationoptionEntityUsingTaxTariffId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AlterationoptionCollection" , true);
				relation.AddEntityFieldPair(TaxTariffFields.TaxTariffId, AlterationoptionFields.TaxTariffId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TaxTariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationoptionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TaxTariffEntity and OrderitemEntity over the 1:n relation they have, using the relation between the fields:
		/// TaxTariff.TaxTariffId - Orderitem.TaxTariffId
		/// </summary>
		public virtual IEntityRelation OrderitemEntityUsingTaxTariffId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrderitemCollection" , true);
				relation.AddEntityFieldPair(TaxTariffFields.TaxTariffId, OrderitemFields.TaxTariffId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TaxTariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderitemEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TaxTariffEntity and OrderitemAlterationitemEntity over the 1:n relation they have, using the relation between the fields:
		/// TaxTariff.TaxTariffId - OrderitemAlterationitem.TaxTariffId
		/// </summary>
		public virtual IEntityRelation OrderitemAlterationitemEntityUsingTaxTariffId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrderitemAlterationitemCollection" , true);
				relation.AddEntityFieldPair(TaxTariffFields.TaxTariffId, OrderitemAlterationitemFields.TaxTariffId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TaxTariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderitemAlterationitemEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TaxTariffEntity and ProductEntity over the 1:n relation they have, using the relation between the fields:
		/// TaxTariff.TaxTariffId - Product.TaxTariffId
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingTaxTariffId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ProductCollection" , true);
				relation.AddEntityFieldPair(TaxTariffFields.TaxTariffId, ProductFields.TaxTariffId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TaxTariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between TaxTariffEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// TaxTariff.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, TaxTariffFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TaxTariffEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticTaxTariffRelations
	{
		internal static readonly IEntityRelation AlterationoptionEntityUsingTaxTariffIdStatic = new TaxTariffRelations().AlterationoptionEntityUsingTaxTariffId;
		internal static readonly IEntityRelation OrderitemEntityUsingTaxTariffIdStatic = new TaxTariffRelations().OrderitemEntityUsingTaxTariffId;
		internal static readonly IEntityRelation OrderitemAlterationitemEntityUsingTaxTariffIdStatic = new TaxTariffRelations().OrderitemAlterationitemEntityUsingTaxTariffId;
		internal static readonly IEntityRelation ProductEntityUsingTaxTariffIdStatic = new TaxTariffRelations().ProductEntityUsingTaxTariffId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new TaxTariffRelations().CompanyEntityUsingCompanyId;

		/// <summary>CTor</summary>
		static StaticTaxTariffRelations()
		{
		}
	}
}
