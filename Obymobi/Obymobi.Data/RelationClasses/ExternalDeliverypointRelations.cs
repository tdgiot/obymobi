﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ExternalDeliverypoint. </summary>
	public partial class ExternalDeliverypointRelations
	{
		/// <summary>CTor</summary>
		public ExternalDeliverypointRelations()
		{
		}

		/// <summary>Gets all relations of the ExternalDeliverypointEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.DeliverypointExternalDeliverypointEntityUsingExternalDeliverypointId);
			toReturn.Add(this.ExternalSystemEntityUsingExternalSystemId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ExternalDeliverypointEntity and DeliverypointExternalDeliverypointEntity over the 1:n relation they have, using the relation between the fields:
		/// ExternalDeliverypoint.ExternalDeliverypointId - DeliverypointExternalDeliverypoint.ExternalDeliverypointId
		/// </summary>
		public virtual IEntityRelation DeliverypointExternalDeliverypointEntityUsingExternalDeliverypointId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeliverypointExternalDeliverypointCollection" , true);
				relation.AddEntityFieldPair(ExternalDeliverypointFields.ExternalDeliverypointId, DeliverypointExternalDeliverypointFields.ExternalDeliverypointId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalDeliverypointEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointExternalDeliverypointEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between ExternalDeliverypointEntity and ExternalSystemEntity over the m:1 relation they have, using the relation between the fields:
		/// ExternalDeliverypoint.ExternalSystemId - ExternalSystem.ExternalSystemId
		/// </summary>
		public virtual IEntityRelation ExternalSystemEntityUsingExternalSystemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ExternalSystemEntity", false);
				relation.AddEntityFieldPair(ExternalSystemFields.ExternalSystemId, ExternalDeliverypointFields.ExternalSystemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalSystemEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalDeliverypointEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticExternalDeliverypointRelations
	{
		internal static readonly IEntityRelation DeliverypointExternalDeliverypointEntityUsingExternalDeliverypointIdStatic = new ExternalDeliverypointRelations().DeliverypointExternalDeliverypointEntityUsingExternalDeliverypointId;
		internal static readonly IEntityRelation ExternalSystemEntityUsingExternalSystemIdStatic = new ExternalDeliverypointRelations().ExternalSystemEntityUsingExternalSystemId;

		/// <summary>CTor</summary>
		static StaticExternalDeliverypointRelations()
		{
		}
	}
}
