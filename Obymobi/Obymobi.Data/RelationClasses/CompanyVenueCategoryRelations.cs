﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: CompanyVenueCategory. </summary>
	public partial class CompanyVenueCategoryRelations
	{
		/// <summary>CTor</summary>
		public CompanyVenueCategoryRelations()
		{
		}

		/// <summary>Gets all relations of the CompanyVenueCategoryEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			toReturn.Add(this.VenueCategoryEntityUsingVenueCategoryId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between CompanyVenueCategoryEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// CompanyVenueCategory.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, CompanyVenueCategoryFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyVenueCategoryEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CompanyVenueCategoryEntity and VenueCategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// CompanyVenueCategory.VenueCategoryId - VenueCategory.VenueCategoryId
		/// </summary>
		public virtual IEntityRelation VenueCategoryEntityUsingVenueCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "VenueCategoryEntity", false);
				relation.AddEntityFieldPair(VenueCategoryFields.VenueCategoryId, CompanyVenueCategoryFields.VenueCategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VenueCategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyVenueCategoryEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCompanyVenueCategoryRelations
	{
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new CompanyVenueCategoryRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation VenueCategoryEntityUsingVenueCategoryIdStatic = new CompanyVenueCategoryRelations().VenueCategoryEntityUsingVenueCategoryId;

		/// <summary>CTor</summary>
		static StaticCompanyVenueCategoryRelations()
		{
		}
	}
}
