﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: AlterationoptionTag. </summary>
	public partial class AlterationoptionTagRelations
	{
		/// <summary>CTor</summary>
		public AlterationoptionTagRelations()
		{
		}

		/// <summary>Gets all relations of the AlterationoptionTagEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AlterationoptionEntityUsingAlterationoptionId);
			toReturn.Add(this.TagEntityUsingTagId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between AlterationoptionTagEntity and AlterationoptionEntity over the m:1 relation they have, using the relation between the fields:
		/// AlterationoptionTag.AlterationoptionId - Alterationoption.AlterationoptionId
		/// </summary>
		public virtual IEntityRelation AlterationoptionEntityUsingAlterationoptionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AlterationoptionEntity", false);
				relation.AddEntityFieldPair(AlterationoptionFields.AlterationoptionId, AlterationoptionTagFields.AlterationoptionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationoptionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationoptionTagEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AlterationoptionTagEntity and TagEntity over the m:1 relation they have, using the relation between the fields:
		/// AlterationoptionTag.TagId - Tag.TagId
		/// </summary>
		public virtual IEntityRelation TagEntityUsingTagId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TagEntity", false);
				relation.AddEntityFieldPair(TagFields.TagId, AlterationoptionTagFields.TagId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TagEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationoptionTagEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticAlterationoptionTagRelations
	{
		internal static readonly IEntityRelation AlterationoptionEntityUsingAlterationoptionIdStatic = new AlterationoptionTagRelations().AlterationoptionEntityUsingAlterationoptionId;
		internal static readonly IEntityRelation TagEntityUsingTagIdStatic = new AlterationoptionTagRelations().TagEntityUsingTagId;

		/// <summary>CTor</summary>
		static StaticAlterationoptionTagRelations()
		{
		}
	}
}
