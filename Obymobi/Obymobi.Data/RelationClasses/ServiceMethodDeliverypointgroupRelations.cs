﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ServiceMethodDeliverypointgroup. </summary>
	public partial class ServiceMethodDeliverypointgroupRelations
	{
		/// <summary>CTor</summary>
		public ServiceMethodDeliverypointgroupRelations()
		{
		}

		/// <summary>Gets all relations of the ServiceMethodDeliverypointgroupEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CompanyEntityUsingParentCompanyId);
			toReturn.Add(this.DeliverypointgroupEntityUsingDeliverypointgroupId);
			toReturn.Add(this.ServiceMethodEntityUsingServiceMethodId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between ServiceMethodDeliverypointgroupEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// ServiceMethodDeliverypointgroup.ParentCompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingParentCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, ServiceMethodDeliverypointgroupFields.ParentCompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ServiceMethodDeliverypointgroupEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ServiceMethodDeliverypointgroupEntity and DeliverypointgroupEntity over the m:1 relation they have, using the relation between the fields:
		/// ServiceMethodDeliverypointgroup.DeliverypointgroupId - Deliverypointgroup.DeliverypointgroupId
		/// </summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingDeliverypointgroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DeliverypointgroupEntity", false);
				relation.AddEntityFieldPair(DeliverypointgroupFields.DeliverypointgroupId, ServiceMethodDeliverypointgroupFields.DeliverypointgroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ServiceMethodDeliverypointgroupEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ServiceMethodDeliverypointgroupEntity and ServiceMethodEntity over the m:1 relation they have, using the relation between the fields:
		/// ServiceMethodDeliverypointgroup.ServiceMethodId - ServiceMethod.ServiceMethodId
		/// </summary>
		public virtual IEntityRelation ServiceMethodEntityUsingServiceMethodId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ServiceMethodEntity", false);
				relation.AddEntityFieldPair(ServiceMethodFields.ServiceMethodId, ServiceMethodDeliverypointgroupFields.ServiceMethodId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ServiceMethodEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ServiceMethodDeliverypointgroupEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticServiceMethodDeliverypointgroupRelations
	{
		internal static readonly IEntityRelation CompanyEntityUsingParentCompanyIdStatic = new ServiceMethodDeliverypointgroupRelations().CompanyEntityUsingParentCompanyId;
		internal static readonly IEntityRelation DeliverypointgroupEntityUsingDeliverypointgroupIdStatic = new ServiceMethodDeliverypointgroupRelations().DeliverypointgroupEntityUsingDeliverypointgroupId;
		internal static readonly IEntityRelation ServiceMethodEntityUsingServiceMethodIdStatic = new ServiceMethodDeliverypointgroupRelations().ServiceMethodEntityUsingServiceMethodId;

		/// <summary>CTor</summary>
		static StaticServiceMethodDeliverypointgroupRelations()
		{
		}
	}
}
