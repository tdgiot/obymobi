﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: SurveyResult. </summary>
	public partial class SurveyResultRelations
	{
		/// <summary>CTor</summary>
		public SurveyResultRelations()
		{
		}

		/// <summary>Gets all relations of the SurveyResultEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ClientEntityUsingClientId);
			toReturn.Add(this.CustomerEntityUsingCustomerId);
			toReturn.Add(this.SurveyAnswerEntityUsingSurveyAnswerId);
			toReturn.Add(this.SurveyQuestionEntityUsingSurveyQuestionId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between SurveyResultEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// SurveyResult.ClientId - Client.ClientId
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingClientId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ClientEntity", false);
				relation.AddEntityFieldPair(ClientFields.ClientId, SurveyResultFields.ClientId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyResultEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between SurveyResultEntity and CustomerEntity over the m:1 relation they have, using the relation between the fields:
		/// SurveyResult.CustomerId - Customer.CustomerId
		/// </summary>
		public virtual IEntityRelation CustomerEntityUsingCustomerId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CustomerEntity", false);
				relation.AddEntityFieldPair(CustomerFields.CustomerId, SurveyResultFields.CustomerId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomerEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyResultEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between SurveyResultEntity and SurveyAnswerEntity over the m:1 relation they have, using the relation between the fields:
		/// SurveyResult.SurveyAnswerId - SurveyAnswer.SurveyAnswerId
		/// </summary>
		public virtual IEntityRelation SurveyAnswerEntityUsingSurveyAnswerId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SurveyAnswerEntity", false);
				relation.AddEntityFieldPair(SurveyAnswerFields.SurveyAnswerId, SurveyResultFields.SurveyAnswerId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyAnswerEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyResultEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between SurveyResultEntity and SurveyQuestionEntity over the m:1 relation they have, using the relation between the fields:
		/// SurveyResult.SurveyQuestionId - SurveyQuestion.SurveyQuestionId
		/// </summary>
		public virtual IEntityRelation SurveyQuestionEntityUsingSurveyQuestionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SurveyQuestionEntity", false);
				relation.AddEntityFieldPair(SurveyQuestionFields.SurveyQuestionId, SurveyResultFields.SurveyQuestionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyQuestionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyResultEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticSurveyResultRelations
	{
		internal static readonly IEntityRelation ClientEntityUsingClientIdStatic = new SurveyResultRelations().ClientEntityUsingClientId;
		internal static readonly IEntityRelation CustomerEntityUsingCustomerIdStatic = new SurveyResultRelations().CustomerEntityUsingCustomerId;
		internal static readonly IEntityRelation SurveyAnswerEntityUsingSurveyAnswerIdStatic = new SurveyResultRelations().SurveyAnswerEntityUsingSurveyAnswerId;
		internal static readonly IEntityRelation SurveyQuestionEntityUsingSurveyQuestionIdStatic = new SurveyResultRelations().SurveyQuestionEntityUsingSurveyQuestionId;

		/// <summary>CTor</summary>
		static StaticSurveyResultRelations()
		{
		}
	}
}
