﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: PriceScheduleItem. </summary>
	public partial class PriceScheduleItemRelations
	{
		/// <summary>CTor</summary>
		public PriceScheduleItemRelations()
		{
		}

		/// <summary>Gets all relations of the PriceScheduleItemEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.PriceScheduleItemOccurrenceEntityUsingPriceScheduleItemId);
			toReturn.Add(this.PriceLevelEntityUsingPriceLevelId);
			toReturn.Add(this.PriceScheduleEntityUsingPriceScheduleId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between PriceScheduleItemEntity and PriceScheduleItemOccurrenceEntity over the 1:n relation they have, using the relation between the fields:
		/// PriceScheduleItem.PriceScheduleItemId - PriceScheduleItemOccurrence.PriceScheduleItemId
		/// </summary>
		public virtual IEntityRelation PriceScheduleItemOccurrenceEntityUsingPriceScheduleItemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PriceScheduleItemOccurrenceCollection" , true);
				relation.AddEntityFieldPair(PriceScheduleItemFields.PriceScheduleItemId, PriceScheduleItemOccurrenceFields.PriceScheduleItemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PriceScheduleItemEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PriceScheduleItemOccurrenceEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between PriceScheduleItemEntity and PriceLevelEntity over the m:1 relation they have, using the relation between the fields:
		/// PriceScheduleItem.PriceLevelId - PriceLevel.PriceLevelId
		/// </summary>
		public virtual IEntityRelation PriceLevelEntityUsingPriceLevelId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PriceLevelEntity", false);
				relation.AddEntityFieldPair(PriceLevelFields.PriceLevelId, PriceScheduleItemFields.PriceLevelId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PriceLevelEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PriceScheduleItemEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between PriceScheduleItemEntity and PriceScheduleEntity over the m:1 relation they have, using the relation between the fields:
		/// PriceScheduleItem.PriceScheduleId - PriceSchedule.PriceScheduleId
		/// </summary>
		public virtual IEntityRelation PriceScheduleEntityUsingPriceScheduleId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PriceScheduleEntity", false);
				relation.AddEntityFieldPair(PriceScheduleFields.PriceScheduleId, PriceScheduleItemFields.PriceScheduleId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PriceScheduleEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PriceScheduleItemEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPriceScheduleItemRelations
	{
		internal static readonly IEntityRelation PriceScheduleItemOccurrenceEntityUsingPriceScheduleItemIdStatic = new PriceScheduleItemRelations().PriceScheduleItemOccurrenceEntityUsingPriceScheduleItemId;
		internal static readonly IEntityRelation PriceLevelEntityUsingPriceLevelIdStatic = new PriceScheduleItemRelations().PriceLevelEntityUsingPriceLevelId;
		internal static readonly IEntityRelation PriceScheduleEntityUsingPriceScheduleIdStatic = new PriceScheduleItemRelations().PriceScheduleEntityUsingPriceScheduleId;

		/// <summary>CTor</summary>
		static StaticPriceScheduleItemRelations()
		{
		}
	}
}
