﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Genericcategory. </summary>
	public partial class GenericcategoryRelations
	{
		/// <summary>CTor</summary>
		public GenericcategoryRelations()
		{
		}

		/// <summary>Gets all relations of the GenericcategoryEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CategoryEntityUsingGenericcategoryId);
			toReturn.Add(this.CustomTextEntityUsingGenericcategoryId);
			toReturn.Add(this.GenericcategoryEntityUsingParentGenericcategoryId);
			toReturn.Add(this.GenericcategoryLanguageEntityUsingGenericcategoryId);
			toReturn.Add(this.GenericproductEntityUsingGenericcategoryId);
			toReturn.Add(this.MediaEntityUsingGenericcategoryId);
			toReturn.Add(this.BrandEntityUsingBrandId);
			toReturn.Add(this.GenericcategoryEntityUsingGenericcategoryIdParentGenericcategoryId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between GenericcategoryEntity and CategoryEntity over the 1:n relation they have, using the relation between the fields:
		/// Genericcategory.GenericcategoryId - Category.GenericcategoryId
		/// </summary>
		public virtual IEntityRelation CategoryEntityUsingGenericcategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CategoryCollection" , true);
				relation.AddEntityFieldPair(GenericcategoryFields.GenericcategoryId, CategoryFields.GenericcategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericcategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between GenericcategoryEntity and CustomTextEntity over the 1:n relation they have, using the relation between the fields:
		/// Genericcategory.GenericcategoryId - CustomText.GenericcategoryId
		/// </summary>
		public virtual IEntityRelation CustomTextEntityUsingGenericcategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomTextCollection" , true);
				relation.AddEntityFieldPair(GenericcategoryFields.GenericcategoryId, CustomTextFields.GenericcategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericcategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between GenericcategoryEntity and GenericcategoryEntity over the 1:n relation they have, using the relation between the fields:
		/// Genericcategory.GenericcategoryId - Genericcategory.ParentGenericcategoryId
		/// </summary>
		public virtual IEntityRelation GenericcategoryEntityUsingParentGenericcategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "GenericcategoryCollection" , true);
				relation.AddEntityFieldPair(GenericcategoryFields.GenericcategoryId, GenericcategoryFields.ParentGenericcategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericcategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericcategoryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between GenericcategoryEntity and GenericcategoryLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Genericcategory.GenericcategoryId - GenericcategoryLanguage.GenericcategoryId
		/// </summary>
		public virtual IEntityRelation GenericcategoryLanguageEntityUsingGenericcategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "GenericcategoryLanguageCollection" , true);
				relation.AddEntityFieldPair(GenericcategoryFields.GenericcategoryId, GenericcategoryLanguageFields.GenericcategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericcategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericcategoryLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between GenericcategoryEntity and GenericproductEntity over the 1:n relation they have, using the relation between the fields:
		/// Genericcategory.GenericcategoryId - Genericproduct.GenericcategoryId
		/// </summary>
		public virtual IEntityRelation GenericproductEntityUsingGenericcategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "GenericproductCollection" , true);
				relation.AddEntityFieldPair(GenericcategoryFields.GenericcategoryId, GenericproductFields.GenericcategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericcategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericproductEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between GenericcategoryEntity and MediaEntity over the 1:n relation they have, using the relation between the fields:
		/// Genericcategory.GenericcategoryId - Media.GenericcategoryId
		/// </summary>
		public virtual IEntityRelation MediaEntityUsingGenericcategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MediaCollection" , true);
				relation.AddEntityFieldPair(GenericcategoryFields.GenericcategoryId, MediaFields.GenericcategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericcategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between GenericcategoryEntity and BrandEntity over the m:1 relation they have, using the relation between the fields:
		/// Genericcategory.BrandId - Brand.BrandId
		/// </summary>
		public virtual IEntityRelation BrandEntityUsingBrandId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "BrandEntity", false);
				relation.AddEntityFieldPair(BrandFields.BrandId, GenericcategoryFields.BrandId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BrandEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericcategoryEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between GenericcategoryEntity and GenericcategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// Genericcategory.ParentGenericcategoryId - Genericcategory.GenericcategoryId
		/// </summary>
		public virtual IEntityRelation GenericcategoryEntityUsingGenericcategoryIdParentGenericcategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "GenericcategoryEntity", false);
				relation.AddEntityFieldPair(GenericcategoryFields.GenericcategoryId, GenericcategoryFields.ParentGenericcategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericcategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericcategoryEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticGenericcategoryRelations
	{
		internal static readonly IEntityRelation CategoryEntityUsingGenericcategoryIdStatic = new GenericcategoryRelations().CategoryEntityUsingGenericcategoryId;
		internal static readonly IEntityRelation CustomTextEntityUsingGenericcategoryIdStatic = new GenericcategoryRelations().CustomTextEntityUsingGenericcategoryId;
		internal static readonly IEntityRelation GenericcategoryEntityUsingParentGenericcategoryIdStatic = new GenericcategoryRelations().GenericcategoryEntityUsingParentGenericcategoryId;
		internal static readonly IEntityRelation GenericcategoryLanguageEntityUsingGenericcategoryIdStatic = new GenericcategoryRelations().GenericcategoryLanguageEntityUsingGenericcategoryId;
		internal static readonly IEntityRelation GenericproductEntityUsingGenericcategoryIdStatic = new GenericcategoryRelations().GenericproductEntityUsingGenericcategoryId;
		internal static readonly IEntityRelation MediaEntityUsingGenericcategoryIdStatic = new GenericcategoryRelations().MediaEntityUsingGenericcategoryId;
		internal static readonly IEntityRelation BrandEntityUsingBrandIdStatic = new GenericcategoryRelations().BrandEntityUsingBrandId;
		internal static readonly IEntityRelation GenericcategoryEntityUsingGenericcategoryIdParentGenericcategoryIdStatic = new GenericcategoryRelations().GenericcategoryEntityUsingGenericcategoryIdParentGenericcategoryId;

		/// <summary>CTor</summary>
		static StaticGenericcategoryRelations()
		{
		}
	}
}
