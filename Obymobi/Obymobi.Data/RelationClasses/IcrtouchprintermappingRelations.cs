﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Icrtouchprintermapping. </summary>
	public partial class IcrtouchprintermappingRelations
	{
		/// <summary>CTor</summary>
		public IcrtouchprintermappingRelations()
		{
		}

		/// <summary>Gets all relations of the IcrtouchprintermappingEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.IcrtouchprintermappingDeliverypointEntityUsingIcrtouchprintermappingId);
			toReturn.Add(this.TerminalEntityUsingIcrtouchprintermappingId);
			toReturn.Add(this.TerminalEntityUsingTerminalId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between IcrtouchprintermappingEntity and IcrtouchprintermappingDeliverypointEntity over the 1:n relation they have, using the relation between the fields:
		/// Icrtouchprintermapping.IcrtouchprintermappingId - IcrtouchprintermappingDeliverypoint.IcrtouchprintermappingId
		/// </summary>
		public virtual IEntityRelation IcrtouchprintermappingDeliverypointEntityUsingIcrtouchprintermappingId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "IcrtouchprintermappingDeliverypointCollection" , true);
				relation.AddEntityFieldPair(IcrtouchprintermappingFields.IcrtouchprintermappingId, IcrtouchprintermappingDeliverypointFields.IcrtouchprintermappingId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("IcrtouchprintermappingEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("IcrtouchprintermappingDeliverypointEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between IcrtouchprintermappingEntity and TerminalEntity over the 1:n relation they have, using the relation between the fields:
		/// Icrtouchprintermapping.IcrtouchprintermappingId - Terminal.IcrtouchprintermappingId
		/// </summary>
		public virtual IEntityRelation TerminalEntityUsingIcrtouchprintermappingId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TerminalCollection" , true);
				relation.AddEntityFieldPair(IcrtouchprintermappingFields.IcrtouchprintermappingId, TerminalFields.IcrtouchprintermappingId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("IcrtouchprintermappingEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between IcrtouchprintermappingEntity and TerminalEntity over the m:1 relation they have, using the relation between the fields:
		/// Icrtouchprintermapping.TerminalId - Terminal.TerminalId
		/// </summary>
		public virtual IEntityRelation TerminalEntityUsingTerminalId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TerminalEntity", false);
				relation.AddEntityFieldPair(TerminalFields.TerminalId, IcrtouchprintermappingFields.TerminalId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("IcrtouchprintermappingEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticIcrtouchprintermappingRelations
	{
		internal static readonly IEntityRelation IcrtouchprintermappingDeliverypointEntityUsingIcrtouchprintermappingIdStatic = new IcrtouchprintermappingRelations().IcrtouchprintermappingDeliverypointEntityUsingIcrtouchprintermappingId;
		internal static readonly IEntityRelation TerminalEntityUsingIcrtouchprintermappingIdStatic = new IcrtouchprintermappingRelations().TerminalEntityUsingIcrtouchprintermappingId;
		internal static readonly IEntityRelation TerminalEntityUsingTerminalIdStatic = new IcrtouchprintermappingRelations().TerminalEntityUsingTerminalId;

		/// <summary>CTor</summary>
		static StaticIcrtouchprintermappingRelations()
		{
		}
	}
}
