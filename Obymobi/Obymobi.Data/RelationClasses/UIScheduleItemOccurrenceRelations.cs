﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: UIScheduleItemOccurrence. </summary>
	public partial class UIScheduleItemOccurrenceRelations
	{
		/// <summary>CTor</summary>
		public UIScheduleItemOccurrenceRelations()
		{
		}

		/// <summary>Gets all relations of the UIScheduleItemOccurrenceEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ScheduledMessageHistoryEntityUsingUIScheduleItemOccurrenceId);
			toReturn.Add(this.UIScheduleItemOccurrenceEntityUsingParentUIScheduleItemOccurrenceId);
			toReturn.Add(this.MessagegroupEntityUsingMessagegroupId);
			toReturn.Add(this.ReportProcessingTaskTemplateEntityUsingReportProcessingTaskTemplateId);
			toReturn.Add(this.UIScheduleEntityUsingUIScheduleId);
			toReturn.Add(this.UIScheduleItemEntityUsingUIScheduleItemId);
			toReturn.Add(this.UIScheduleItemOccurrenceEntityUsingUIScheduleItemOccurrenceIdParentUIScheduleItemOccurrenceId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between UIScheduleItemOccurrenceEntity and ScheduledMessageHistoryEntity over the 1:n relation they have, using the relation between the fields:
		/// UIScheduleItemOccurrence.UIScheduleItemOccurrenceId - ScheduledMessageHistory.UIScheduleItemOccurrenceId
		/// </summary>
		public virtual IEntityRelation ScheduledMessageHistoryEntityUsingUIScheduleItemOccurrenceId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ScheduledMessageHistoryCollection" , true);
				relation.AddEntityFieldPair(UIScheduleItemOccurrenceFields.UIScheduleItemOccurrenceId, ScheduledMessageHistoryFields.UIScheduleItemOccurrenceId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIScheduleItemOccurrenceEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduledMessageHistoryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UIScheduleItemOccurrenceEntity and UIScheduleItemOccurrenceEntity over the 1:n relation they have, using the relation between the fields:
		/// UIScheduleItemOccurrence.UIScheduleItemOccurrenceId - UIScheduleItemOccurrence.ParentUIScheduleItemOccurrenceId
		/// </summary>
		public virtual IEntityRelation UIScheduleItemOccurrenceEntityUsingParentUIScheduleItemOccurrenceId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UIScheduleItemOccurrenceCollection" , true);
				relation.AddEntityFieldPair(UIScheduleItemOccurrenceFields.UIScheduleItemOccurrenceId, UIScheduleItemOccurrenceFields.ParentUIScheduleItemOccurrenceId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIScheduleItemOccurrenceEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIScheduleItemOccurrenceEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between UIScheduleItemOccurrenceEntity and MessagegroupEntity over the m:1 relation they have, using the relation between the fields:
		/// UIScheduleItemOccurrence.MessagegroupId - Messagegroup.MessagegroupId
		/// </summary>
		public virtual IEntityRelation MessagegroupEntityUsingMessagegroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "MessagegroupEntity", false);
				relation.AddEntityFieldPair(MessagegroupFields.MessagegroupId, UIScheduleItemOccurrenceFields.MessagegroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessagegroupEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIScheduleItemOccurrenceEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between UIScheduleItemOccurrenceEntity and ReportProcessingTaskTemplateEntity over the m:1 relation they have, using the relation between the fields:
		/// UIScheduleItemOccurrence.ReportProcessingTaskTemplateId - ReportProcessingTaskTemplate.ReportProcessingTaskTemplateId
		/// </summary>
		public virtual IEntityRelation ReportProcessingTaskTemplateEntityUsingReportProcessingTaskTemplateId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ReportProcessingTaskTemplateEntity", false);
				relation.AddEntityFieldPair(ReportProcessingTaskTemplateFields.ReportProcessingTaskTemplateId, UIScheduleItemOccurrenceFields.ReportProcessingTaskTemplateId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportProcessingTaskTemplateEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIScheduleItemOccurrenceEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between UIScheduleItemOccurrenceEntity and UIScheduleEntity over the m:1 relation they have, using the relation between the fields:
		/// UIScheduleItemOccurrence.UIScheduleId - UISchedule.UIScheduleId
		/// </summary>
		public virtual IEntityRelation UIScheduleEntityUsingUIScheduleId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UIScheduleEntity", false);
				relation.AddEntityFieldPair(UIScheduleFields.UIScheduleId, UIScheduleItemOccurrenceFields.UIScheduleId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIScheduleEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIScheduleItemOccurrenceEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between UIScheduleItemOccurrenceEntity and UIScheduleItemEntity over the m:1 relation they have, using the relation between the fields:
		/// UIScheduleItemOccurrence.UIScheduleItemId - UIScheduleItem.UIScheduleItemId
		/// </summary>
		public virtual IEntityRelation UIScheduleItemEntityUsingUIScheduleItemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UIScheduleItemEntity", false);
				relation.AddEntityFieldPair(UIScheduleItemFields.UIScheduleItemId, UIScheduleItemOccurrenceFields.UIScheduleItemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIScheduleItemEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIScheduleItemOccurrenceEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between UIScheduleItemOccurrenceEntity and UIScheduleItemOccurrenceEntity over the m:1 relation they have, using the relation between the fields:
		/// UIScheduleItemOccurrence.ParentUIScheduleItemOccurrenceId - UIScheduleItemOccurrence.UIScheduleItemOccurrenceId
		/// </summary>
		public virtual IEntityRelation UIScheduleItemOccurrenceEntityUsingUIScheduleItemOccurrenceIdParentUIScheduleItemOccurrenceId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ParentUIScheduleItemOccurrenceEntity", false);
				relation.AddEntityFieldPair(UIScheduleItemOccurrenceFields.UIScheduleItemOccurrenceId, UIScheduleItemOccurrenceFields.ParentUIScheduleItemOccurrenceId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIScheduleItemOccurrenceEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIScheduleItemOccurrenceEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticUIScheduleItemOccurrenceRelations
	{
		internal static readonly IEntityRelation ScheduledMessageHistoryEntityUsingUIScheduleItemOccurrenceIdStatic = new UIScheduleItemOccurrenceRelations().ScheduledMessageHistoryEntityUsingUIScheduleItemOccurrenceId;
		internal static readonly IEntityRelation UIScheduleItemOccurrenceEntityUsingParentUIScheduleItemOccurrenceIdStatic = new UIScheduleItemOccurrenceRelations().UIScheduleItemOccurrenceEntityUsingParentUIScheduleItemOccurrenceId;
		internal static readonly IEntityRelation MessagegroupEntityUsingMessagegroupIdStatic = new UIScheduleItemOccurrenceRelations().MessagegroupEntityUsingMessagegroupId;
		internal static readonly IEntityRelation ReportProcessingTaskTemplateEntityUsingReportProcessingTaskTemplateIdStatic = new UIScheduleItemOccurrenceRelations().ReportProcessingTaskTemplateEntityUsingReportProcessingTaskTemplateId;
		internal static readonly IEntityRelation UIScheduleEntityUsingUIScheduleIdStatic = new UIScheduleItemOccurrenceRelations().UIScheduleEntityUsingUIScheduleId;
		internal static readonly IEntityRelation UIScheduleItemEntityUsingUIScheduleItemIdStatic = new UIScheduleItemOccurrenceRelations().UIScheduleItemEntityUsingUIScheduleItemId;
		internal static readonly IEntityRelation UIScheduleItemOccurrenceEntityUsingUIScheduleItemOccurrenceIdParentUIScheduleItemOccurrenceIdStatic = new UIScheduleItemOccurrenceRelations().UIScheduleItemOccurrenceEntityUsingUIScheduleItemOccurrenceIdParentUIScheduleItemOccurrenceId;

		/// <summary>CTor</summary>
		static StaticUIScheduleItemOccurrenceRelations()
		{
		}
	}
}
