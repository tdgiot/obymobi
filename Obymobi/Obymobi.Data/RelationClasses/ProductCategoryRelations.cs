﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ProductCategory. </summary>
	public partial class ProductCategoryRelations
	{
		/// <summary>CTor</summary>
		public ProductCategoryRelations()
		{
		}

		/// <summary>Gets all relations of the ProductCategoryEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AdvertisementEntityUsingProductCategoryId);
			toReturn.Add(this.ActionEntityUsingProductCategoryId);
			toReturn.Add(this.AvailabilityEntityUsingActionProductCategoryId);
			toReturn.Add(this.CategorySuggestionEntityUsingProductCategoryId);
			toReturn.Add(this.MessageEntityUsingProductCategoryId);
			toReturn.Add(this.MessageTemplateEntityUsingProductCategoryId);
			toReturn.Add(this.ProductCategorySuggestionEntityUsingProductCategoryId);
			toReturn.Add(this.ProductCategorySuggestionEntityUsingSuggestedProductCategoryId);
			toReturn.Add(this.ProductCategoryTagEntityUsingProductCategoryId);
			toReturn.Add(this.ScheduledMessageEntityUsingProductCategoryId);
			toReturn.Add(this.UIWidgetEntityUsingProductCategoryId);
			toReturn.Add(this.CategoryEntityUsingCategoryId);
			toReturn.Add(this.ProductEntityUsingProductId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ProductCategoryEntity and AdvertisementEntity over the 1:n relation they have, using the relation between the fields:
		/// ProductCategory.ProductCategoryId - Advertisement.ProductCategoryId
		/// </summary>
		public virtual IEntityRelation AdvertisementEntityUsingProductCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AdvertisementCollection" , true);
				relation.AddEntityFieldPair(ProductCategoryFields.ProductCategoryId, AdvertisementFields.ProductCategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductCategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductCategoryEntity and ActionEntity over the 1:n relation they have, using the relation between the fields:
		/// ProductCategory.ProductCategoryId - Action.ProductCategoryId
		/// </summary>
		public virtual IEntityRelation ActionEntityUsingProductCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ActionCollection" , true);
				relation.AddEntityFieldPair(ProductCategoryFields.ProductCategoryId, ActionFields.ProductCategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductCategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ActionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductCategoryEntity and AvailabilityEntity over the 1:n relation they have, using the relation between the fields:
		/// ProductCategory.ProductCategoryId - Availability.ActionProductCategoryId
		/// </summary>
		public virtual IEntityRelation AvailabilityEntityUsingActionProductCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AvailabilityCollection" , true);
				relation.AddEntityFieldPair(ProductCategoryFields.ProductCategoryId, AvailabilityFields.ActionProductCategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductCategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AvailabilityEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductCategoryEntity and CategorySuggestionEntity over the 1:n relation they have, using the relation between the fields:
		/// ProductCategory.ProductCategoryId - CategorySuggestion.ProductCategoryId
		/// </summary>
		public virtual IEntityRelation CategorySuggestionEntityUsingProductCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CategorySuggestionCollection" , true);
				relation.AddEntityFieldPair(ProductCategoryFields.ProductCategoryId, CategorySuggestionFields.ProductCategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductCategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategorySuggestionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductCategoryEntity and MessageEntity over the 1:n relation they have, using the relation between the fields:
		/// ProductCategory.ProductCategoryId - Message.ProductCategoryId
		/// </summary>
		public virtual IEntityRelation MessageEntityUsingProductCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MessageCollection" , true);
				relation.AddEntityFieldPair(ProductCategoryFields.ProductCategoryId, MessageFields.ProductCategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductCategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductCategoryEntity and MessageTemplateEntity over the 1:n relation they have, using the relation between the fields:
		/// ProductCategory.ProductCategoryId - MessageTemplate.ProductCategoryId
		/// </summary>
		public virtual IEntityRelation MessageTemplateEntityUsingProductCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MessageTemplateCollection" , true);
				relation.AddEntityFieldPair(ProductCategoryFields.ProductCategoryId, MessageTemplateFields.ProductCategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductCategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageTemplateEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductCategoryEntity and ProductCategorySuggestionEntity over the 1:n relation they have, using the relation between the fields:
		/// ProductCategory.ProductCategoryId - ProductCategorySuggestion.ProductCategoryId
		/// </summary>
		public virtual IEntityRelation ProductCategorySuggestionEntityUsingProductCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ProductCategorySuggestionCollection" , true);
				relation.AddEntityFieldPair(ProductCategoryFields.ProductCategoryId, ProductCategorySuggestionFields.ProductCategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductCategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductCategorySuggestionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductCategoryEntity and ProductCategorySuggestionEntity over the 1:n relation they have, using the relation between the fields:
		/// ProductCategory.ProductCategoryId - ProductCategorySuggestion.SuggestedProductCategoryId
		/// </summary>
		public virtual IEntityRelation ProductCategorySuggestionEntityUsingSuggestedProductCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SuggestedProductCategorySuggestionCollection" , true);
				relation.AddEntityFieldPair(ProductCategoryFields.ProductCategoryId, ProductCategorySuggestionFields.SuggestedProductCategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductCategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductCategorySuggestionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductCategoryEntity and ProductCategoryTagEntity over the 1:n relation they have, using the relation between the fields:
		/// ProductCategory.ProductCategoryId - ProductCategoryTag.ProductCategoryId
		/// </summary>
		public virtual IEntityRelation ProductCategoryTagEntityUsingProductCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ProductCategoryTagCollection" , true);
				relation.AddEntityFieldPair(ProductCategoryFields.ProductCategoryId, ProductCategoryTagFields.ProductCategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductCategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductCategoryTagEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductCategoryEntity and ScheduledMessageEntity over the 1:n relation they have, using the relation between the fields:
		/// ProductCategory.ProductCategoryId - ScheduledMessage.ProductCategoryId
		/// </summary>
		public virtual IEntityRelation ScheduledMessageEntityUsingProductCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ScheduledMessageCollection" , true);
				relation.AddEntityFieldPair(ProductCategoryFields.ProductCategoryId, ScheduledMessageFields.ProductCategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductCategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduledMessageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductCategoryEntity and UIWidgetEntity over the 1:n relation they have, using the relation between the fields:
		/// ProductCategory.ProductCategoryId - UIWidget.ProductCategoryId
		/// </summary>
		public virtual IEntityRelation UIWidgetEntityUsingProductCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UIWidgetCollection" , true);
				relation.AddEntityFieldPair(ProductCategoryFields.ProductCategoryId, UIWidgetFields.ProductCategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductCategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIWidgetEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between ProductCategoryEntity and CategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// ProductCategory.CategoryId - Category.CategoryId
		/// </summary>
		public virtual IEntityRelation CategoryEntityUsingCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CategoryEntity", false);
				relation.AddEntityFieldPair(CategoryFields.CategoryId, ProductCategoryFields.CategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductCategoryEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ProductCategoryEntity and ProductEntity over the m:1 relation they have, using the relation between the fields:
		/// ProductCategory.ProductId - Product.ProductId
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ProductEntity", false);
				relation.AddEntityFieldPair(ProductFields.ProductId, ProductCategoryFields.ProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductCategoryEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticProductCategoryRelations
	{
		internal static readonly IEntityRelation AdvertisementEntityUsingProductCategoryIdStatic = new ProductCategoryRelations().AdvertisementEntityUsingProductCategoryId;
		internal static readonly IEntityRelation ActionEntityUsingProductCategoryIdStatic = new ProductCategoryRelations().ActionEntityUsingProductCategoryId;
		internal static readonly IEntityRelation AvailabilityEntityUsingActionProductCategoryIdStatic = new ProductCategoryRelations().AvailabilityEntityUsingActionProductCategoryId;
		internal static readonly IEntityRelation CategorySuggestionEntityUsingProductCategoryIdStatic = new ProductCategoryRelations().CategorySuggestionEntityUsingProductCategoryId;
		internal static readonly IEntityRelation MessageEntityUsingProductCategoryIdStatic = new ProductCategoryRelations().MessageEntityUsingProductCategoryId;
		internal static readonly IEntityRelation MessageTemplateEntityUsingProductCategoryIdStatic = new ProductCategoryRelations().MessageTemplateEntityUsingProductCategoryId;
		internal static readonly IEntityRelation ProductCategorySuggestionEntityUsingProductCategoryIdStatic = new ProductCategoryRelations().ProductCategorySuggestionEntityUsingProductCategoryId;
		internal static readonly IEntityRelation ProductCategorySuggestionEntityUsingSuggestedProductCategoryIdStatic = new ProductCategoryRelations().ProductCategorySuggestionEntityUsingSuggestedProductCategoryId;
		internal static readonly IEntityRelation ProductCategoryTagEntityUsingProductCategoryIdStatic = new ProductCategoryRelations().ProductCategoryTagEntityUsingProductCategoryId;
		internal static readonly IEntityRelation ScheduledMessageEntityUsingProductCategoryIdStatic = new ProductCategoryRelations().ScheduledMessageEntityUsingProductCategoryId;
		internal static readonly IEntityRelation UIWidgetEntityUsingProductCategoryIdStatic = new ProductCategoryRelations().UIWidgetEntityUsingProductCategoryId;
		internal static readonly IEntityRelation CategoryEntityUsingCategoryIdStatic = new ProductCategoryRelations().CategoryEntityUsingCategoryId;
		internal static readonly IEntityRelation ProductEntityUsingProductIdStatic = new ProductCategoryRelations().ProductEntityUsingProductId;

		/// <summary>CTor</summary>
		static StaticProductCategoryRelations()
		{
		}
	}
}
