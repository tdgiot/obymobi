﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: TimeZone. </summary>
	public partial class TimeZoneRelations
	{
		/// <summary>CTor</summary>
		public TimeZoneRelations()
		{
		}

		/// <summary>Gets all relations of the TimeZoneEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CompanyEntityUsingTimeZoneId);
			toReturn.Add(this.PointOfInterestEntityUsingTimeZoneId);
			toReturn.Add(this.ReportProcessingTaskEntityUsingTimeZoneId);
			toReturn.Add(this.UserEntityUsingTimeZoneId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between TimeZoneEntity and CompanyEntity over the 1:n relation they have, using the relation between the fields:
		/// TimeZone.TimeZoneId - Company.TimeZoneId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingTimeZoneId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CompanyCollection" , true);
				relation.AddEntityFieldPair(TimeZoneFields.TimeZoneId, CompanyFields.TimeZoneId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TimeZoneEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TimeZoneEntity and PointOfInterestEntity over the 1:n relation they have, using the relation between the fields:
		/// TimeZone.TimeZoneId - PointOfInterest.TimeZoneId
		/// </summary>
		public virtual IEntityRelation PointOfInterestEntityUsingTimeZoneId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PointOfInterestCollection" , true);
				relation.AddEntityFieldPair(TimeZoneFields.TimeZoneId, PointOfInterestFields.TimeZoneId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TimeZoneEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PointOfInterestEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TimeZoneEntity and ReportProcessingTaskEntity over the 1:n relation they have, using the relation between the fields:
		/// TimeZone.TimeZoneId - ReportProcessingTask.TimeZoneId
		/// </summary>
		public virtual IEntityRelation ReportProcessingTaskEntityUsingTimeZoneId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ReportProcessingTaskCollection" , true);
				relation.AddEntityFieldPair(TimeZoneFields.TimeZoneId, ReportProcessingTaskFields.TimeZoneId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TimeZoneEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportProcessingTaskEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TimeZoneEntity and UserEntity over the 1:n relation they have, using the relation between the fields:
		/// TimeZone.TimeZoneId - User.TimeZoneId
		/// </summary>
		public virtual IEntityRelation UserEntityUsingTimeZoneId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UserCollection" , true);
				relation.AddEntityFieldPair(TimeZoneFields.TimeZoneId, UserFields.TimeZoneId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TimeZoneEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticTimeZoneRelations
	{
		internal static readonly IEntityRelation CompanyEntityUsingTimeZoneIdStatic = new TimeZoneRelations().CompanyEntityUsingTimeZoneId;
		internal static readonly IEntityRelation PointOfInterestEntityUsingTimeZoneIdStatic = new TimeZoneRelations().PointOfInterestEntityUsingTimeZoneId;
		internal static readonly IEntityRelation ReportProcessingTaskEntityUsingTimeZoneIdStatic = new TimeZoneRelations().ReportProcessingTaskEntityUsingTimeZoneId;
		internal static readonly IEntityRelation UserEntityUsingTimeZoneIdStatic = new TimeZoneRelations().UserEntityUsingTimeZoneId;

		/// <summary>CTor</summary>
		static StaticTimeZoneRelations()
		{
		}
	}
}
