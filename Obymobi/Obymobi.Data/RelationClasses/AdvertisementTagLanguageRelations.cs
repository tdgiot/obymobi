﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: AdvertisementTagLanguage. </summary>
	public partial class AdvertisementTagLanguageRelations
	{
		/// <summary>CTor</summary>
		public AdvertisementTagLanguageRelations()
		{
		}

		/// <summary>Gets all relations of the AdvertisementTagLanguageEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AdvertisementTagEntityUsingAdvertisementTagId);
			toReturn.Add(this.LanguageEntityUsingLanguageId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between AdvertisementTagLanguageEntity and AdvertisementTagEntity over the m:1 relation they have, using the relation between the fields:
		/// AdvertisementTagLanguage.AdvertisementTagId - AdvertisementTag.AdvertisementTagId
		/// </summary>
		public virtual IEntityRelation AdvertisementTagEntityUsingAdvertisementTagId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AdvertisementTagEntity", false);
				relation.AddEntityFieldPair(AdvertisementTagFields.AdvertisementTagId, AdvertisementTagLanguageFields.AdvertisementTagId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementTagEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementTagLanguageEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AdvertisementTagLanguageEntity and LanguageEntity over the m:1 relation they have, using the relation between the fields:
		/// AdvertisementTagLanguage.LanguageId - Language.LanguageId
		/// </summary>
		public virtual IEntityRelation LanguageEntityUsingLanguageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "LanguageEntity", false);
				relation.AddEntityFieldPair(LanguageFields.LanguageId, AdvertisementTagLanguageFields.LanguageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementTagLanguageEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticAdvertisementTagLanguageRelations
	{
		internal static readonly IEntityRelation AdvertisementTagEntityUsingAdvertisementTagIdStatic = new AdvertisementTagLanguageRelations().AdvertisementTagEntityUsingAdvertisementTagId;
		internal static readonly IEntityRelation LanguageEntityUsingLanguageIdStatic = new AdvertisementTagLanguageRelations().LanguageEntityUsingLanguageId;

		/// <summary>CTor</summary>
		static StaticAdvertisementTagLanguageRelations()
		{
		}
	}
}
