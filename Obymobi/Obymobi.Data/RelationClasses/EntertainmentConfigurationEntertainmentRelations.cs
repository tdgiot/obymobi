﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: EntertainmentConfigurationEntertainment. </summary>
	public partial class EntertainmentConfigurationEntertainmentRelations
	{
		/// <summary>CTor</summary>
		public EntertainmentConfigurationEntertainmentRelations()
		{
		}

		/// <summary>Gets all relations of the EntertainmentConfigurationEntertainmentEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.EntertainmentEntityUsingEntertainmentId);
			toReturn.Add(this.EntertainmentConfigurationEntityUsingEntertainmentConfigurationId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between EntertainmentConfigurationEntertainmentEntity and EntertainmentEntity over the m:1 relation they have, using the relation between the fields:
		/// EntertainmentConfigurationEntertainment.EntertainmentId - Entertainment.EntertainmentId
		/// </summary>
		public virtual IEntityRelation EntertainmentEntityUsingEntertainmentId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "EntertainmentEntity", false);
				relation.AddEntityFieldPair(EntertainmentFields.EntertainmentId, EntertainmentConfigurationEntertainmentFields.EntertainmentId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentConfigurationEntertainmentEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between EntertainmentConfigurationEntertainmentEntity and EntertainmentConfigurationEntity over the m:1 relation they have, using the relation between the fields:
		/// EntertainmentConfigurationEntertainment.EntertainmentConfigurationId - EntertainmentConfiguration.EntertainmentConfigurationId
		/// </summary>
		public virtual IEntityRelation EntertainmentConfigurationEntityUsingEntertainmentConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "EntertainmentConfigurationEntity", false);
				relation.AddEntityFieldPair(EntertainmentConfigurationFields.EntertainmentConfigurationId, EntertainmentConfigurationEntertainmentFields.EntertainmentConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentConfigurationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentConfigurationEntertainmentEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticEntertainmentConfigurationEntertainmentRelations
	{
		internal static readonly IEntityRelation EntertainmentEntityUsingEntertainmentIdStatic = new EntertainmentConfigurationEntertainmentRelations().EntertainmentEntityUsingEntertainmentId;
		internal static readonly IEntityRelation EntertainmentConfigurationEntityUsingEntertainmentConfigurationIdStatic = new EntertainmentConfigurationEntertainmentRelations().EntertainmentConfigurationEntityUsingEntertainmentConfigurationId;

		/// <summary>CTor</summary>
		static StaticEntertainmentConfigurationEntertainmentRelations()
		{
		}
	}
}
