﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: DeliverypointgroupAdvertisement. </summary>
	public partial class DeliverypointgroupAdvertisementRelations
	{
		/// <summary>CTor</summary>
		public DeliverypointgroupAdvertisementRelations()
		{
		}

		/// <summary>Gets all relations of the DeliverypointgroupAdvertisementEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AdvertisementEntityUsingAdvertisementId);
			toReturn.Add(this.DeliverypointgroupEntityUsingDeliverypointgroupId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupAdvertisementEntity and AdvertisementEntity over the m:1 relation they have, using the relation between the fields:
		/// DeliverypointgroupAdvertisement.AdvertisementId - Advertisement.AdvertisementId
		/// </summary>
		public virtual IEntityRelation AdvertisementEntityUsingAdvertisementId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AdvertisementEntity", false);
				relation.AddEntityFieldPair(AdvertisementFields.AdvertisementId, DeliverypointgroupAdvertisementFields.AdvertisementId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupAdvertisementEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupAdvertisementEntity and DeliverypointgroupEntity over the m:1 relation they have, using the relation between the fields:
		/// DeliverypointgroupAdvertisement.DeliverypointgroupId - Deliverypointgroup.DeliverypointgroupId
		/// </summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingDeliverypointgroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DeliverypointgroupEntity", false);
				relation.AddEntityFieldPair(DeliverypointgroupFields.DeliverypointgroupId, DeliverypointgroupAdvertisementFields.DeliverypointgroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupAdvertisementEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticDeliverypointgroupAdvertisementRelations
	{
		internal static readonly IEntityRelation AdvertisementEntityUsingAdvertisementIdStatic = new DeliverypointgroupAdvertisementRelations().AdvertisementEntityUsingAdvertisementId;
		internal static readonly IEntityRelation DeliverypointgroupEntityUsingDeliverypointgroupIdStatic = new DeliverypointgroupAdvertisementRelations().DeliverypointgroupEntityUsingDeliverypointgroupId;

		/// <summary>CTor</summary>
		static StaticDeliverypointgroupAdvertisementRelations()
		{
		}
	}
}
