﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Orderitem. </summary>
	public partial class OrderitemRelations
	{
		/// <summary>CTor</summary>
		public OrderitemRelations()
		{
		}

		/// <summary>Gets all relations of the OrderitemEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.OrderitemAlterationitemEntityUsingOrderitemId);
			toReturn.Add(this.OrderitemTagEntityUsingOrderitemId);
			toReturn.Add(this.CategoryEntityUsingCategoryId);
			toReturn.Add(this.OrderEntityUsingOrderId);
			toReturn.Add(this.PriceLevelItemEntityUsingPriceLevelItemId);
			toReturn.Add(this.ProductEntityUsingProductId);
			toReturn.Add(this.TaxTariffEntityUsingTaxTariffId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between OrderitemEntity and OrderitemAlterationitemEntity over the 1:n relation they have, using the relation between the fields:
		/// Orderitem.OrderitemId - OrderitemAlterationitem.OrderitemId
		/// </summary>
		public virtual IEntityRelation OrderitemAlterationitemEntityUsingOrderitemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrderitemAlterationitemCollection" , true);
				relation.AddEntityFieldPair(OrderitemFields.OrderitemId, OrderitemAlterationitemFields.OrderitemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderitemEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderitemAlterationitemEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between OrderitemEntity and OrderitemTagEntity over the 1:n relation they have, using the relation between the fields:
		/// Orderitem.OrderitemId - OrderitemTag.OrderitemId
		/// </summary>
		public virtual IEntityRelation OrderitemTagEntityUsingOrderitemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrderitemTagCollection" , true);
				relation.AddEntityFieldPair(OrderitemFields.OrderitemId, OrderitemTagFields.OrderitemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderitemEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderitemTagEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between OrderitemEntity and CategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// Orderitem.CategoryId - Category.CategoryId
		/// </summary>
		public virtual IEntityRelation CategoryEntityUsingCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CategoryEntity", false);
				relation.AddEntityFieldPair(CategoryFields.CategoryId, OrderitemFields.CategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderitemEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OrderitemEntity and OrderEntity over the m:1 relation they have, using the relation between the fields:
		/// Orderitem.OrderId - Order.OrderId
		/// </summary>
		public virtual IEntityRelation OrderEntityUsingOrderId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "OrderEntity", false);
				relation.AddEntityFieldPair(OrderFields.OrderId, OrderitemFields.OrderId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderitemEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OrderitemEntity and PriceLevelItemEntity over the m:1 relation they have, using the relation between the fields:
		/// Orderitem.PriceLevelItemId - PriceLevelItem.PriceLevelItemId
		/// </summary>
		public virtual IEntityRelation PriceLevelItemEntityUsingPriceLevelItemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PriceLevelItemEntity", false);
				relation.AddEntityFieldPair(PriceLevelItemFields.PriceLevelItemId, OrderitemFields.PriceLevelItemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PriceLevelItemEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderitemEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OrderitemEntity and ProductEntity over the m:1 relation they have, using the relation between the fields:
		/// Orderitem.ProductId - Product.ProductId
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ProductEntity", false);
				relation.AddEntityFieldPair(ProductFields.ProductId, OrderitemFields.ProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderitemEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OrderitemEntity and TaxTariffEntity over the m:1 relation they have, using the relation between the fields:
		/// Orderitem.TaxTariffId - TaxTariff.TaxTariffId
		/// </summary>
		public virtual IEntityRelation TaxTariffEntityUsingTaxTariffId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TaxTariffEntity", false);
				relation.AddEntityFieldPair(TaxTariffFields.TaxTariffId, OrderitemFields.TaxTariffId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TaxTariffEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderitemEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticOrderitemRelations
	{
		internal static readonly IEntityRelation OrderitemAlterationitemEntityUsingOrderitemIdStatic = new OrderitemRelations().OrderitemAlterationitemEntityUsingOrderitemId;
		internal static readonly IEntityRelation OrderitemTagEntityUsingOrderitemIdStatic = new OrderitemRelations().OrderitemTagEntityUsingOrderitemId;
		internal static readonly IEntityRelation CategoryEntityUsingCategoryIdStatic = new OrderitemRelations().CategoryEntityUsingCategoryId;
		internal static readonly IEntityRelation OrderEntityUsingOrderIdStatic = new OrderitemRelations().OrderEntityUsingOrderId;
		internal static readonly IEntityRelation PriceLevelItemEntityUsingPriceLevelItemIdStatic = new OrderitemRelations().PriceLevelItemEntityUsingPriceLevelItemId;
		internal static readonly IEntityRelation ProductEntityUsingProductIdStatic = new OrderitemRelations().ProductEntityUsingProductId;
		internal static readonly IEntityRelation TaxTariffEntityUsingTaxTariffIdStatic = new OrderitemRelations().TaxTariffEntityUsingTaxTariffId;

		/// <summary>CTor</summary>
		static StaticOrderitemRelations()
		{
		}
	}
}
