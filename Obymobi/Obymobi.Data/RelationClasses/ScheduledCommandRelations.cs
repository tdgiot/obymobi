﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ScheduledCommand. </summary>
	public partial class ScheduledCommandRelations
	{
		/// <summary>CTor</summary>
		public ScheduledCommandRelations()
		{
		}

		/// <summary>Gets all relations of the ScheduledCommandEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ClientEntityUsingClientId);
			toReturn.Add(this.ScheduledCommandTaskEntityUsingScheduledCommandTaskId);
			toReturn.Add(this.TerminalEntityUsingTerminalId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between ScheduledCommandEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// ScheduledCommand.ClientId - Client.ClientId
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingClientId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ClientEntity", false);
				relation.AddEntityFieldPair(ClientFields.ClientId, ScheduledCommandFields.ClientId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduledCommandEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ScheduledCommandEntity and ScheduledCommandTaskEntity over the m:1 relation they have, using the relation between the fields:
		/// ScheduledCommand.ScheduledCommandTaskId - ScheduledCommandTask.ScheduledCommandTaskId
		/// </summary>
		public virtual IEntityRelation ScheduledCommandTaskEntityUsingScheduledCommandTaskId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ScheduledCommandTaskEntity", false);
				relation.AddEntityFieldPair(ScheduledCommandTaskFields.ScheduledCommandTaskId, ScheduledCommandFields.ScheduledCommandTaskId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduledCommandTaskEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduledCommandEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ScheduledCommandEntity and TerminalEntity over the m:1 relation they have, using the relation between the fields:
		/// ScheduledCommand.TerminalId - Terminal.TerminalId
		/// </summary>
		public virtual IEntityRelation TerminalEntityUsingTerminalId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TerminalEntity", false);
				relation.AddEntityFieldPair(TerminalFields.TerminalId, ScheduledCommandFields.TerminalId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduledCommandEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticScheduledCommandRelations
	{
		internal static readonly IEntityRelation ClientEntityUsingClientIdStatic = new ScheduledCommandRelations().ClientEntityUsingClientId;
		internal static readonly IEntityRelation ScheduledCommandTaskEntityUsingScheduledCommandTaskIdStatic = new ScheduledCommandRelations().ScheduledCommandTaskEntityUsingScheduledCommandTaskId;
		internal static readonly IEntityRelation TerminalEntityUsingTerminalIdStatic = new ScheduledCommandRelations().TerminalEntityUsingTerminalId;

		/// <summary>CTor</summary>
		static StaticScheduledCommandRelations()
		{
		}
	}
}
