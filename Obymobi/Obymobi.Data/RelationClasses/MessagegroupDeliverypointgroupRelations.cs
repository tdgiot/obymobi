﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: MessagegroupDeliverypointgroup. </summary>
	public partial class MessagegroupDeliverypointgroupRelations
	{
		/// <summary>CTor</summary>
		public MessagegroupDeliverypointgroupRelations()
		{
		}

		/// <summary>Gets all relations of the MessagegroupDeliverypointgroupEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.DeliverypointgroupEntityUsingDeliverypointgroupId);
			toReturn.Add(this.MessagegroupEntityUsingMessagegroupId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between MessagegroupDeliverypointgroupEntity and DeliverypointgroupEntity over the m:1 relation they have, using the relation between the fields:
		/// MessagegroupDeliverypointgroup.DeliverypointgroupId - Deliverypointgroup.DeliverypointgroupId
		/// </summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingDeliverypointgroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DeliverypointgroupEntity", false);
				relation.AddEntityFieldPair(DeliverypointgroupFields.DeliverypointgroupId, MessagegroupDeliverypointgroupFields.DeliverypointgroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessagegroupDeliverypointgroupEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MessagegroupDeliverypointgroupEntity and MessagegroupEntity over the m:1 relation they have, using the relation between the fields:
		/// MessagegroupDeliverypointgroup.MessagegroupId - Messagegroup.MessagegroupId
		/// </summary>
		public virtual IEntityRelation MessagegroupEntityUsingMessagegroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "MessagegroupEntity", false);
				relation.AddEntityFieldPair(MessagegroupFields.MessagegroupId, MessagegroupDeliverypointgroupFields.MessagegroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessagegroupEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessagegroupDeliverypointgroupEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticMessagegroupDeliverypointgroupRelations
	{
		internal static readonly IEntityRelation DeliverypointgroupEntityUsingDeliverypointgroupIdStatic = new MessagegroupDeliverypointgroupRelations().DeliverypointgroupEntityUsingDeliverypointgroupId;
		internal static readonly IEntityRelation MessagegroupEntityUsingMessagegroupIdStatic = new MessagegroupDeliverypointgroupRelations().MessagegroupEntityUsingMessagegroupId;

		/// <summary>CTor</summary>
		static StaticMessagegroupDeliverypointgroupRelations()
		{
		}
	}
}
