﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Site. </summary>
	public partial class SiteRelations
	{
		/// <summary>CTor</summary>
		public SiteRelations()
		{
		}

		/// <summary>Gets all relations of the SiteEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AdvertisementEntityUsingActionSiteId);
			toReturn.Add(this.AvailabilityEntityUsingActionSiteId);
			toReturn.Add(this.CustomTextEntityUsingSiteId);
			toReturn.Add(this.EntertainmentEntityUsingSiteId);
			toReturn.Add(this.MediaEntityUsingSiteId);
			toReturn.Add(this.MediaEntityUsingActionSiteId);
			toReturn.Add(this.MessageEntityUsingSiteId);
			toReturn.Add(this.MessageTemplateEntityUsingSiteId);
			toReturn.Add(this.PageEntityUsingSiteId);
			toReturn.Add(this.ScheduledMessageEntityUsingSiteId);
			toReturn.Add(this.SiteCultureEntityUsingSiteId);
			toReturn.Add(this.SiteLanguageEntityUsingSiteId);
			toReturn.Add(this.UITabEntityUsingSiteId);
			toReturn.Add(this.UIWidgetEntityUsingSiteId);
			toReturn.Add(this.TimestampEntityUsingSiteId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			toReturn.Add(this.SiteTemplateEntityUsingSiteTemplateId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between SiteEntity and AdvertisementEntity over the 1:n relation they have, using the relation between the fields:
		/// Site.SiteId - Advertisement.ActionSiteId
		/// </summary>
		public virtual IEntityRelation AdvertisementEntityUsingActionSiteId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AdvertisementCollection" , true);
				relation.AddEntityFieldPair(SiteFields.SiteId, AdvertisementFields.ActionSiteId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SiteEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SiteEntity and AvailabilityEntity over the 1:n relation they have, using the relation between the fields:
		/// Site.SiteId - Availability.ActionSiteId
		/// </summary>
		public virtual IEntityRelation AvailabilityEntityUsingActionSiteId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AvailabilityCollection" , true);
				relation.AddEntityFieldPair(SiteFields.SiteId, AvailabilityFields.ActionSiteId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SiteEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AvailabilityEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SiteEntity and CustomTextEntity over the 1:n relation they have, using the relation between the fields:
		/// Site.SiteId - CustomText.SiteId
		/// </summary>
		public virtual IEntityRelation CustomTextEntityUsingSiteId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomTextCollection" , true);
				relation.AddEntityFieldPair(SiteFields.SiteId, CustomTextFields.SiteId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SiteEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SiteEntity and EntertainmentEntity over the 1:n relation they have, using the relation between the fields:
		/// Site.SiteId - Entertainment.SiteId
		/// </summary>
		public virtual IEntityRelation EntertainmentEntityUsingSiteId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "EntertainmentCollection" , true);
				relation.AddEntityFieldPair(SiteFields.SiteId, EntertainmentFields.SiteId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SiteEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SiteEntity and MediaEntity over the 1:n relation they have, using the relation between the fields:
		/// Site.SiteId - Media.SiteId
		/// </summary>
		public virtual IEntityRelation MediaEntityUsingSiteId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MediaCollection" , true);
				relation.AddEntityFieldPair(SiteFields.SiteId, MediaFields.SiteId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SiteEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SiteEntity and MediaEntity over the 1:n relation they have, using the relation between the fields:
		/// Site.SiteId - Media.ActionSiteId
		/// </summary>
		public virtual IEntityRelation MediaEntityUsingActionSiteId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MediaCollection_" , true);
				relation.AddEntityFieldPair(SiteFields.SiteId, MediaFields.ActionSiteId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SiteEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SiteEntity and MessageEntity over the 1:n relation they have, using the relation between the fields:
		/// Site.SiteId - Message.SiteId
		/// </summary>
		public virtual IEntityRelation MessageEntityUsingSiteId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MessageCollection" , true);
				relation.AddEntityFieldPair(SiteFields.SiteId, MessageFields.SiteId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SiteEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SiteEntity and MessageTemplateEntity over the 1:n relation they have, using the relation between the fields:
		/// Site.SiteId - MessageTemplate.SiteId
		/// </summary>
		public virtual IEntityRelation MessageTemplateEntityUsingSiteId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MessageTemplateCollection" , true);
				relation.AddEntityFieldPair(SiteFields.SiteId, MessageTemplateFields.SiteId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SiteEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageTemplateEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SiteEntity and PageEntity over the 1:n relation they have, using the relation between the fields:
		/// Site.SiteId - Page.SiteId
		/// </summary>
		public virtual IEntityRelation PageEntityUsingSiteId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PageCollection" , true);
				relation.AddEntityFieldPair(SiteFields.SiteId, PageFields.SiteId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SiteEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SiteEntity and ScheduledMessageEntity over the 1:n relation they have, using the relation between the fields:
		/// Site.SiteId - ScheduledMessage.SiteId
		/// </summary>
		public virtual IEntityRelation ScheduledMessageEntityUsingSiteId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ScheduledMessageCollection" , true);
				relation.AddEntityFieldPair(SiteFields.SiteId, ScheduledMessageFields.SiteId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SiteEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduledMessageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SiteEntity and SiteCultureEntity over the 1:n relation they have, using the relation between the fields:
		/// Site.SiteId - SiteCulture.SiteId
		/// </summary>
		public virtual IEntityRelation SiteCultureEntityUsingSiteId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SiteCultureCollection" , true);
				relation.AddEntityFieldPair(SiteFields.SiteId, SiteCultureFields.SiteId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SiteEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SiteCultureEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SiteEntity and SiteLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Site.SiteId - SiteLanguage.SiteId
		/// </summary>
		public virtual IEntityRelation SiteLanguageEntityUsingSiteId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SiteLanguageCollection" , true);
				relation.AddEntityFieldPair(SiteFields.SiteId, SiteLanguageFields.SiteId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SiteEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SiteLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SiteEntity and UITabEntity over the 1:n relation they have, using the relation between the fields:
		/// Site.SiteId - UITab.SiteId
		/// </summary>
		public virtual IEntityRelation UITabEntityUsingSiteId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UITabCollection" , true);
				relation.AddEntityFieldPair(SiteFields.SiteId, UITabFields.SiteId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SiteEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UITabEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SiteEntity and UIWidgetEntity over the 1:n relation they have, using the relation between the fields:
		/// Site.SiteId - UIWidget.SiteId
		/// </summary>
		public virtual IEntityRelation UIWidgetEntityUsingSiteId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UIWidgetCollection" , true);
				relation.AddEntityFieldPair(SiteFields.SiteId, UIWidgetFields.SiteId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SiteEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIWidgetEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SiteEntity and TimestampEntity over the 1:1 relation they have, using the relation between the fields:
		/// Site.SiteId - Timestamp.SiteId
		/// </summary>
		public virtual IEntityRelation TimestampEntityUsingSiteId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "TimestampCollection", true);


				relation.AddEntityFieldPair(SiteFields.SiteId, TimestampFields.SiteId);


				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SiteEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TimestampEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SiteEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// Site.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, SiteFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SiteEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between SiteEntity and SiteTemplateEntity over the m:1 relation they have, using the relation between the fields:
		/// Site.SiteTemplateId - SiteTemplate.SiteTemplateId
		/// </summary>
		public virtual IEntityRelation SiteTemplateEntityUsingSiteTemplateId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SiteTemplateEntity", false);
				relation.AddEntityFieldPair(SiteTemplateFields.SiteTemplateId, SiteFields.SiteTemplateId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SiteTemplateEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SiteEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticSiteRelations
	{
		internal static readonly IEntityRelation AdvertisementEntityUsingActionSiteIdStatic = new SiteRelations().AdvertisementEntityUsingActionSiteId;
		internal static readonly IEntityRelation AvailabilityEntityUsingActionSiteIdStatic = new SiteRelations().AvailabilityEntityUsingActionSiteId;
		internal static readonly IEntityRelation CustomTextEntityUsingSiteIdStatic = new SiteRelations().CustomTextEntityUsingSiteId;
		internal static readonly IEntityRelation EntertainmentEntityUsingSiteIdStatic = new SiteRelations().EntertainmentEntityUsingSiteId;
		internal static readonly IEntityRelation MediaEntityUsingSiteIdStatic = new SiteRelations().MediaEntityUsingSiteId;
		internal static readonly IEntityRelation MediaEntityUsingActionSiteIdStatic = new SiteRelations().MediaEntityUsingActionSiteId;
		internal static readonly IEntityRelation MessageEntityUsingSiteIdStatic = new SiteRelations().MessageEntityUsingSiteId;
		internal static readonly IEntityRelation MessageTemplateEntityUsingSiteIdStatic = new SiteRelations().MessageTemplateEntityUsingSiteId;
		internal static readonly IEntityRelation PageEntityUsingSiteIdStatic = new SiteRelations().PageEntityUsingSiteId;
		internal static readonly IEntityRelation ScheduledMessageEntityUsingSiteIdStatic = new SiteRelations().ScheduledMessageEntityUsingSiteId;
		internal static readonly IEntityRelation SiteCultureEntityUsingSiteIdStatic = new SiteRelations().SiteCultureEntityUsingSiteId;
		internal static readonly IEntityRelation SiteLanguageEntityUsingSiteIdStatic = new SiteRelations().SiteLanguageEntityUsingSiteId;
		internal static readonly IEntityRelation UITabEntityUsingSiteIdStatic = new SiteRelations().UITabEntityUsingSiteId;
		internal static readonly IEntityRelation UIWidgetEntityUsingSiteIdStatic = new SiteRelations().UIWidgetEntityUsingSiteId;
		internal static readonly IEntityRelation TimestampEntityUsingSiteIdStatic = new SiteRelations().TimestampEntityUsingSiteId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new SiteRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation SiteTemplateEntityUsingSiteTemplateIdStatic = new SiteRelations().SiteTemplateEntityUsingSiteTemplateId;

		/// <summary>CTor</summary>
		static StaticSiteRelations()
		{
		}
	}
}
