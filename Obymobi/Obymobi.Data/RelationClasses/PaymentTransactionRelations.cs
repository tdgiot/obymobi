﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: PaymentTransaction. </summary>
	public partial class PaymentTransactionRelations
	{
		/// <summary>CTor</summary>
		public PaymentTransactionRelations()
		{
		}

		/// <summary>Gets all relations of the PaymentTransactionEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.PaymentTransactionLogEntityUsingPaymentTransactionId);
			toReturn.Add(this.PaymentTransactionSplitEntityUsingPaymentTransactionId);
			toReturn.Add(this.OrderEntityUsingOrderId);
			toReturn.Add(this.PaymentIntegrationConfigurationEntityUsingPaymentIntegrationConfigurationId);
			toReturn.Add(this.PaymentProviderEntityUsingPaymentProviderId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between PaymentTransactionEntity and PaymentTransactionLogEntity over the 1:n relation they have, using the relation between the fields:
		/// PaymentTransaction.PaymentTransactionId - PaymentTransactionLog.PaymentTransactionId
		/// </summary>
		public virtual IEntityRelation PaymentTransactionLogEntityUsingPaymentTransactionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PaymentTransactionLogCollection" , true);
				relation.AddEntityFieldPair(PaymentTransactionFields.PaymentTransactionId, PaymentTransactionLogFields.PaymentTransactionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentTransactionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentTransactionLogEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PaymentTransactionEntity and PaymentTransactionSplitEntity over the 1:n relation they have, using the relation between the fields:
		/// PaymentTransaction.PaymentTransactionId - PaymentTransactionSplit.PaymentTransactionId
		/// </summary>
		public virtual IEntityRelation PaymentTransactionSplitEntityUsingPaymentTransactionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PaymentTransactionSplitCollection" , true);
				relation.AddEntityFieldPair(PaymentTransactionFields.PaymentTransactionId, PaymentTransactionSplitFields.PaymentTransactionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentTransactionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentTransactionSplitEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between PaymentTransactionEntity and OrderEntity over the m:1 relation they have, using the relation between the fields:
		/// PaymentTransaction.OrderId - Order.OrderId
		/// </summary>
		public virtual IEntityRelation OrderEntityUsingOrderId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "OrderEntity", false);
				relation.AddEntityFieldPair(OrderFields.OrderId, PaymentTransactionFields.OrderId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentTransactionEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between PaymentTransactionEntity and PaymentIntegrationConfigurationEntity over the m:1 relation they have, using the relation between the fields:
		/// PaymentTransaction.PaymentIntegrationConfigurationId - PaymentIntegrationConfiguration.PaymentIntegrationConfigurationId
		/// </summary>
		public virtual IEntityRelation PaymentIntegrationConfigurationEntityUsingPaymentIntegrationConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PaymentIntegrationConfigurationEntity", false);
				relation.AddEntityFieldPair(PaymentIntegrationConfigurationFields.PaymentIntegrationConfigurationId, PaymentTransactionFields.PaymentIntegrationConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentIntegrationConfigurationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentTransactionEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between PaymentTransactionEntity and PaymentProviderEntity over the m:1 relation they have, using the relation between the fields:
		/// PaymentTransaction.PaymentProviderId - PaymentProvider.PaymentProviderId
		/// </summary>
		public virtual IEntityRelation PaymentProviderEntityUsingPaymentProviderId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PaymentProviderEntity", false);
				relation.AddEntityFieldPair(PaymentProviderFields.PaymentProviderId, PaymentTransactionFields.PaymentProviderId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentProviderEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentTransactionEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPaymentTransactionRelations
	{
		internal static readonly IEntityRelation PaymentTransactionLogEntityUsingPaymentTransactionIdStatic = new PaymentTransactionRelations().PaymentTransactionLogEntityUsingPaymentTransactionId;
		internal static readonly IEntityRelation PaymentTransactionSplitEntityUsingPaymentTransactionIdStatic = new PaymentTransactionRelations().PaymentTransactionSplitEntityUsingPaymentTransactionId;
		internal static readonly IEntityRelation OrderEntityUsingOrderIdStatic = new PaymentTransactionRelations().OrderEntityUsingOrderId;
		internal static readonly IEntityRelation PaymentIntegrationConfigurationEntityUsingPaymentIntegrationConfigurationIdStatic = new PaymentTransactionRelations().PaymentIntegrationConfigurationEntityUsingPaymentIntegrationConfigurationId;
		internal static readonly IEntityRelation PaymentProviderEntityUsingPaymentProviderIdStatic = new PaymentTransactionRelations().PaymentProviderEntityUsingPaymentProviderId;

		/// <summary>CTor</summary>
		static StaticPaymentTransactionRelations()
		{
		}
	}
}
