﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: CloudProcessingTask. </summary>
	public partial class CloudProcessingTaskRelations
	{
		/// <summary>CTor</summary>
		public CloudProcessingTaskRelations()
		{
		}

		/// <summary>Gets all relations of the CloudProcessingTaskEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.EntertainmentEntityUsingEntertainmentId);
			toReturn.Add(this.EntertainmentFileEntityUsingEntertainmentFileId);
			toReturn.Add(this.MediaRatioTypeMediaEntityUsingMediaRatioTypeMediaId);
			toReturn.Add(this.ReleaseEntityUsingReleaseId);
			toReturn.Add(this.WeatherEntityUsingWeatherId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between CloudProcessingTaskEntity and EntertainmentEntity over the m:1 relation they have, using the relation between the fields:
		/// CloudProcessingTask.EntertainmentId - Entertainment.EntertainmentId
		/// </summary>
		public virtual IEntityRelation EntertainmentEntityUsingEntertainmentId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "EntertainmentEntity", false);
				relation.AddEntityFieldPair(EntertainmentFields.EntertainmentId, CloudProcessingTaskFields.EntertainmentId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CloudProcessingTaskEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CloudProcessingTaskEntity and EntertainmentFileEntity over the m:1 relation they have, using the relation between the fields:
		/// CloudProcessingTask.EntertainmentFileId - EntertainmentFile.EntertainmentFileId
		/// </summary>
		public virtual IEntityRelation EntertainmentFileEntityUsingEntertainmentFileId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "EntertainmentFileEntity", false);
				relation.AddEntityFieldPair(EntertainmentFileFields.EntertainmentFileId, CloudProcessingTaskFields.EntertainmentFileId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentFileEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CloudProcessingTaskEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CloudProcessingTaskEntity and MediaRatioTypeMediaEntity over the m:1 relation they have, using the relation between the fields:
		/// CloudProcessingTask.MediaRatioTypeMediaId - MediaRatioTypeMedia.MediaRatioTypeMediaId
		/// </summary>
		public virtual IEntityRelation MediaRatioTypeMediaEntityUsingMediaRatioTypeMediaId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "MediaRatioTypeMediaEntity", false);
				relation.AddEntityFieldPair(MediaRatioTypeMediaFields.MediaRatioTypeMediaId, CloudProcessingTaskFields.MediaRatioTypeMediaId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaRatioTypeMediaEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CloudProcessingTaskEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CloudProcessingTaskEntity and ReleaseEntity over the m:1 relation they have, using the relation between the fields:
		/// CloudProcessingTask.ReleaseId - Release.ReleaseId
		/// </summary>
		public virtual IEntityRelation ReleaseEntityUsingReleaseId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ReleaseEntity", false);
				relation.AddEntityFieldPair(ReleaseFields.ReleaseId, CloudProcessingTaskFields.ReleaseId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReleaseEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CloudProcessingTaskEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CloudProcessingTaskEntity and WeatherEntity over the m:1 relation they have, using the relation between the fields:
		/// CloudProcessingTask.WeatherId - Weather.WeatherId
		/// </summary>
		public virtual IEntityRelation WeatherEntityUsingWeatherId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "WeatherEntity", false);
				relation.AddEntityFieldPair(WeatherFields.WeatherId, CloudProcessingTaskFields.WeatherId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WeatherEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CloudProcessingTaskEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCloudProcessingTaskRelations
	{
		internal static readonly IEntityRelation EntertainmentEntityUsingEntertainmentIdStatic = new CloudProcessingTaskRelations().EntertainmentEntityUsingEntertainmentId;
		internal static readonly IEntityRelation EntertainmentFileEntityUsingEntertainmentFileIdStatic = new CloudProcessingTaskRelations().EntertainmentFileEntityUsingEntertainmentFileId;
		internal static readonly IEntityRelation MediaRatioTypeMediaEntityUsingMediaRatioTypeMediaIdStatic = new CloudProcessingTaskRelations().MediaRatioTypeMediaEntityUsingMediaRatioTypeMediaId;
		internal static readonly IEntityRelation ReleaseEntityUsingReleaseIdStatic = new CloudProcessingTaskRelations().ReleaseEntityUsingReleaseId;
		internal static readonly IEntityRelation WeatherEntityUsingWeatherIdStatic = new CloudProcessingTaskRelations().WeatherEntityUsingWeatherId;

		/// <summary>CTor</summary>
		static StaticCloudProcessingTaskRelations()
		{
		}
	}
}
