﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: FeatureFlag. </summary>
	public partial class FeatureFlagRelations
	{
		/// <summary>CTor</summary>
		public FeatureFlagRelations()
		{
		}

		/// <summary>Gets all relations of the FeatureFlagEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ApplicationConfigurationEntityUsingApplicationConfigurationId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between FeatureFlagEntity and ApplicationConfigurationEntity over the m:1 relation they have, using the relation between the fields:
		/// FeatureFlag.ApplicationConfigurationId - ApplicationConfiguration.ApplicationConfigurationId
		/// </summary>
		public virtual IEntityRelation ApplicationConfigurationEntityUsingApplicationConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ApplicationConfigurationEntity", false);
				relation.AddEntityFieldPair(ApplicationConfigurationFields.ApplicationConfigurationId, FeatureFlagFields.ApplicationConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ApplicationConfigurationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FeatureFlagEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between FeatureFlagEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// FeatureFlag.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, FeatureFlagFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FeatureFlagEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticFeatureFlagRelations
	{
		internal static readonly IEntityRelation ApplicationConfigurationEntityUsingApplicationConfigurationIdStatic = new FeatureFlagRelations().ApplicationConfigurationEntityUsingApplicationConfigurationId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new FeatureFlagRelations().CompanyEntityUsingCompanyId;

		/// <summary>CTor</summary>
		static StaticFeatureFlagRelations()
		{
		}
	}
}
