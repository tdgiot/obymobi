﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: PageElement. </summary>
	public partial class PageElementRelations
	{
		/// <summary>CTor</summary>
		public PageElementRelations()
		{
		}

		/// <summary>Gets all relations of the PageElementEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.MediaEntityUsingPageElementId);
			toReturn.Add(this.LanguageEntityUsingLanguageId);
			toReturn.Add(this.PageEntityUsingPageId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between PageElementEntity and MediaEntity over the 1:n relation they have, using the relation between the fields:
		/// PageElement.PageElementId - Media.PageElementId
		/// </summary>
		public virtual IEntityRelation MediaEntityUsingPageElementId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MediaCollection" , true);
				relation.AddEntityFieldPair(PageElementFields.PageElementId, MediaFields.PageElementId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageElementEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between PageElementEntity and LanguageEntity over the m:1 relation they have, using the relation between the fields:
		/// PageElement.LanguageId - Language.LanguageId
		/// </summary>
		public virtual IEntityRelation LanguageEntityUsingLanguageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "LanguageEntity", false);
				relation.AddEntityFieldPair(LanguageFields.LanguageId, PageElementFields.LanguageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageElementEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between PageElementEntity and PageEntity over the m:1 relation they have, using the relation between the fields:
		/// PageElement.PageId - Page.PageId
		/// </summary>
		public virtual IEntityRelation PageEntityUsingPageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PageEntity", false);
				relation.AddEntityFieldPair(PageFields.PageId, PageElementFields.PageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageElementEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPageElementRelations
	{
		internal static readonly IEntityRelation MediaEntityUsingPageElementIdStatic = new PageElementRelations().MediaEntityUsingPageElementId;
		internal static readonly IEntityRelation LanguageEntityUsingLanguageIdStatic = new PageElementRelations().LanguageEntityUsingLanguageId;
		internal static readonly IEntityRelation PageEntityUsingPageIdStatic = new PageElementRelations().PageEntityUsingPageId;

		/// <summary>CTor</summary>
		static StaticPageElementRelations()
		{
		}
	}
}
