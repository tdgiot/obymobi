﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: CategorySuggestion. </summary>
	public partial class CategorySuggestionRelations
	{
		/// <summary>CTor</summary>
		public CategorySuggestionRelations()
		{
		}

		/// <summary>Gets all relations of the CategorySuggestionEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CategoryEntityUsingCategoryId);
			toReturn.Add(this.ProductEntityUsingProductId);
			toReturn.Add(this.ProductCategoryEntityUsingProductCategoryId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between CategorySuggestionEntity and CategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// CategorySuggestion.CategoryId - Category.CategoryId
		/// </summary>
		public virtual IEntityRelation CategoryEntityUsingCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CategoryEntity", false);
				relation.AddEntityFieldPair(CategoryFields.CategoryId, CategorySuggestionFields.CategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategorySuggestionEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CategorySuggestionEntity and ProductEntity over the m:1 relation they have, using the relation between the fields:
		/// CategorySuggestion.ProductId - Product.ProductId
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ProductEntity", false);
				relation.AddEntityFieldPair(ProductFields.ProductId, CategorySuggestionFields.ProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategorySuggestionEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CategorySuggestionEntity and ProductCategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// CategorySuggestion.ProductCategoryId - ProductCategory.ProductCategoryId
		/// </summary>
		public virtual IEntityRelation ProductCategoryEntityUsingProductCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ProductCategoryEntity", false);
				relation.AddEntityFieldPair(ProductCategoryFields.ProductCategoryId, CategorySuggestionFields.ProductCategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductCategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategorySuggestionEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCategorySuggestionRelations
	{
		internal static readonly IEntityRelation CategoryEntityUsingCategoryIdStatic = new CategorySuggestionRelations().CategoryEntityUsingCategoryId;
		internal static readonly IEntityRelation ProductEntityUsingProductIdStatic = new CategorySuggestionRelations().ProductEntityUsingProductId;
		internal static readonly IEntityRelation ProductCategoryEntityUsingProductCategoryIdStatic = new CategorySuggestionRelations().ProductCategoryEntityUsingProductCategoryId;

		/// <summary>CTor</summary>
		static StaticCategorySuggestionRelations()
		{
		}
	}
}
