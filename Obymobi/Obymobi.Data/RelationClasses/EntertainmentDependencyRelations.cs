﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: EntertainmentDependency. </summary>
	public partial class EntertainmentDependencyRelations
	{
		/// <summary>CTor</summary>
		public EntertainmentDependencyRelations()
		{
		}

		/// <summary>Gets all relations of the EntertainmentDependencyEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.EntertainmentEntityUsingDependentEntertainmentId);
			toReturn.Add(this.EntertainmentEntityUsingEntertainmentId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between EntertainmentDependencyEntity and EntertainmentEntity over the m:1 relation they have, using the relation between the fields:
		/// EntertainmentDependency.DependentEntertainmentId - Entertainment.EntertainmentId
		/// </summary>
		public virtual IEntityRelation EntertainmentEntityUsingDependentEntertainmentId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DependentEntertainmentEntity", false);
				relation.AddEntityFieldPair(EntertainmentFields.EntertainmentId, EntertainmentDependencyFields.DependentEntertainmentId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentDependencyEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between EntertainmentDependencyEntity and EntertainmentEntity over the m:1 relation they have, using the relation between the fields:
		/// EntertainmentDependency.EntertainmentId - Entertainment.EntertainmentId
		/// </summary>
		public virtual IEntityRelation EntertainmentEntityUsingEntertainmentId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "EntertainmentEntity", false);
				relation.AddEntityFieldPair(EntertainmentFields.EntertainmentId, EntertainmentDependencyFields.EntertainmentId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentDependencyEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticEntertainmentDependencyRelations
	{
		internal static readonly IEntityRelation EntertainmentEntityUsingDependentEntertainmentIdStatic = new EntertainmentDependencyRelations().EntertainmentEntityUsingDependentEntertainmentId;
		internal static readonly IEntityRelation EntertainmentEntityUsingEntertainmentIdStatic = new EntertainmentDependencyRelations().EntertainmentEntityUsingEntertainmentId;

		/// <summary>CTor</summary>
		static StaticEntertainmentDependencyRelations()
		{
		}
	}
}
