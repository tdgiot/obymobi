﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Announcement. </summary>
	public partial class AnnouncementRelations
	{
		/// <summary>CTor</summary>
		public AnnouncementRelations()
		{
		}

		/// <summary>Gets all relations of the AnnouncementEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AnnouncementLanguageEntityUsingAnnouncementId);
			toReturn.Add(this.CustomTextEntityUsingAnnouncementId);
			toReturn.Add(this.DeliverypointgroupEntityUsingReorderNotificationAnnouncementId);
			toReturn.Add(this.DeliverypointgroupAnnouncementEntityUsingAnnouncementId);
			toReturn.Add(this.CategoryEntityUsingOnNoCategory);
			toReturn.Add(this.CategoryEntityUsingOnYesCategory);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			toReturn.Add(this.DeliverypointgroupEntityUsingDeliverypointgroupId);
			toReturn.Add(this.EntertainmentEntityUsingOnYesEntertainment);
			toReturn.Add(this.EntertainmentcategoryEntityUsingOnYesEntertainmentCategory);
			toReturn.Add(this.EntertainmentcategoryEntityUsingOnNoEntertainmentCategory);
			toReturn.Add(this.MediaEntityUsingMediaId);
			toReturn.Add(this.ProductEntityUsingOnYesProduct);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between AnnouncementEntity and AnnouncementLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Announcement.AnnouncementId - AnnouncementLanguage.AnnouncementId
		/// </summary>
		public virtual IEntityRelation AnnouncementLanguageEntityUsingAnnouncementId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AnnouncementLanguageCollection" , true);
				relation.AddEntityFieldPair(AnnouncementFields.AnnouncementId, AnnouncementLanguageFields.AnnouncementId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AnnouncementEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AnnouncementLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AnnouncementEntity and CustomTextEntity over the 1:n relation they have, using the relation between the fields:
		/// Announcement.AnnouncementId - CustomText.AnnouncementId
		/// </summary>
		public virtual IEntityRelation CustomTextEntityUsingAnnouncementId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomTextCollection" , true);
				relation.AddEntityFieldPair(AnnouncementFields.AnnouncementId, CustomTextFields.AnnouncementId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AnnouncementEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AnnouncementEntity and DeliverypointgroupEntity over the 1:n relation they have, using the relation between the fields:
		/// Announcement.AnnouncementId - Deliverypointgroup.ReorderNotificationAnnouncementId
		/// </summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingReorderNotificationAnnouncementId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeliverypointgroupCollection" , true);
				relation.AddEntityFieldPair(AnnouncementFields.AnnouncementId, DeliverypointgroupFields.ReorderNotificationAnnouncementId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AnnouncementEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AnnouncementEntity and DeliverypointgroupAnnouncementEntity over the 1:n relation they have, using the relation between the fields:
		/// Announcement.AnnouncementId - DeliverypointgroupAnnouncement.AnnouncementId
		/// </summary>
		public virtual IEntityRelation DeliverypointgroupAnnouncementEntityUsingAnnouncementId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeliverypointgroupAnnouncementCollection" , true);
				relation.AddEntityFieldPair(AnnouncementFields.AnnouncementId, DeliverypointgroupAnnouncementFields.AnnouncementId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AnnouncementEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupAnnouncementEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between AnnouncementEntity and CategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// Announcement.OnNoCategory - Category.CategoryId
		/// </summary>
		public virtual IEntityRelation CategoryEntityUsingOnNoCategory
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CategoryEntity", false);
				relation.AddEntityFieldPair(CategoryFields.CategoryId, AnnouncementFields.OnNoCategory);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AnnouncementEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AnnouncementEntity and CategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// Announcement.OnYesCategory - Category.CategoryId
		/// </summary>
		public virtual IEntityRelation CategoryEntityUsingOnYesCategory
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CategoryEntity_", false);
				relation.AddEntityFieldPair(CategoryFields.CategoryId, AnnouncementFields.OnYesCategory);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AnnouncementEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AnnouncementEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// Announcement.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, AnnouncementFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AnnouncementEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AnnouncementEntity and DeliverypointgroupEntity over the m:1 relation they have, using the relation between the fields:
		/// Announcement.DeliverypointgroupId - Deliverypointgroup.DeliverypointgroupId
		/// </summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingDeliverypointgroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DeliverypointgroupEntity", false);
				relation.AddEntityFieldPair(DeliverypointgroupFields.DeliverypointgroupId, AnnouncementFields.DeliverypointgroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AnnouncementEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AnnouncementEntity and EntertainmentEntity over the m:1 relation they have, using the relation between the fields:
		/// Announcement.OnYesEntertainment - Entertainment.EntertainmentId
		/// </summary>
		public virtual IEntityRelation EntertainmentEntityUsingOnYesEntertainment
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "EntertainmentEntity", false);
				relation.AddEntityFieldPair(EntertainmentFields.EntertainmentId, AnnouncementFields.OnYesEntertainment);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AnnouncementEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AnnouncementEntity and EntertainmentcategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// Announcement.OnYesEntertainmentCategory - Entertainmentcategory.EntertainmentcategoryId
		/// </summary>
		public virtual IEntityRelation EntertainmentcategoryEntityUsingOnYesEntertainmentCategory
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "EntertainmentcategoryEntity", false);
				relation.AddEntityFieldPair(EntertainmentcategoryFields.EntertainmentcategoryId, AnnouncementFields.OnYesEntertainmentCategory);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentcategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AnnouncementEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AnnouncementEntity and EntertainmentcategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// Announcement.OnNoEntertainmentCategory - Entertainmentcategory.EntertainmentcategoryId
		/// </summary>
		public virtual IEntityRelation EntertainmentcategoryEntityUsingOnNoEntertainmentCategory
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "EntertainmentcategoryEntity_", false);
				relation.AddEntityFieldPair(EntertainmentcategoryFields.EntertainmentcategoryId, AnnouncementFields.OnNoEntertainmentCategory);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentcategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AnnouncementEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AnnouncementEntity and MediaEntity over the m:1 relation they have, using the relation between the fields:
		/// Announcement.MediaId - Media.MediaId
		/// </summary>
		public virtual IEntityRelation MediaEntityUsingMediaId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "MediaEntity", false);
				relation.AddEntityFieldPair(MediaFields.MediaId, AnnouncementFields.MediaId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AnnouncementEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AnnouncementEntity and ProductEntity over the m:1 relation they have, using the relation between the fields:
		/// Announcement.OnYesProduct - Product.ProductId
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingOnYesProduct
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ProductEntity", false);
				relation.AddEntityFieldPair(ProductFields.ProductId, AnnouncementFields.OnYesProduct);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AnnouncementEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticAnnouncementRelations
	{
		internal static readonly IEntityRelation AnnouncementLanguageEntityUsingAnnouncementIdStatic = new AnnouncementRelations().AnnouncementLanguageEntityUsingAnnouncementId;
		internal static readonly IEntityRelation CustomTextEntityUsingAnnouncementIdStatic = new AnnouncementRelations().CustomTextEntityUsingAnnouncementId;
		internal static readonly IEntityRelation DeliverypointgroupEntityUsingReorderNotificationAnnouncementIdStatic = new AnnouncementRelations().DeliverypointgroupEntityUsingReorderNotificationAnnouncementId;
		internal static readonly IEntityRelation DeliverypointgroupAnnouncementEntityUsingAnnouncementIdStatic = new AnnouncementRelations().DeliverypointgroupAnnouncementEntityUsingAnnouncementId;
		internal static readonly IEntityRelation CategoryEntityUsingOnNoCategoryStatic = new AnnouncementRelations().CategoryEntityUsingOnNoCategory;
		internal static readonly IEntityRelation CategoryEntityUsingOnYesCategoryStatic = new AnnouncementRelations().CategoryEntityUsingOnYesCategory;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new AnnouncementRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation DeliverypointgroupEntityUsingDeliverypointgroupIdStatic = new AnnouncementRelations().DeliverypointgroupEntityUsingDeliverypointgroupId;
		internal static readonly IEntityRelation EntertainmentEntityUsingOnYesEntertainmentStatic = new AnnouncementRelations().EntertainmentEntityUsingOnYesEntertainment;
		internal static readonly IEntityRelation EntertainmentcategoryEntityUsingOnYesEntertainmentCategoryStatic = new AnnouncementRelations().EntertainmentcategoryEntityUsingOnYesEntertainmentCategory;
		internal static readonly IEntityRelation EntertainmentcategoryEntityUsingOnNoEntertainmentCategoryStatic = new AnnouncementRelations().EntertainmentcategoryEntityUsingOnNoEntertainmentCategory;
		internal static readonly IEntityRelation MediaEntityUsingMediaIdStatic = new AnnouncementRelations().MediaEntityUsingMediaId;
		internal static readonly IEntityRelation ProductEntityUsingOnYesProductStatic = new AnnouncementRelations().ProductEntityUsingOnYesProduct;

		/// <summary>CTor</summary>
		static StaticAnnouncementRelations()
		{
		}
	}
}
