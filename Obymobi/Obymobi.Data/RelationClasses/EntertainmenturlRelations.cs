﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Entertainmenturl. </summary>
	public partial class EntertainmenturlRelations
	{
		/// <summary>CTor</summary>
		public EntertainmenturlRelations()
		{
		}

		/// <summary>Gets all relations of the EntertainmenturlEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.EntertainmentEntityUsingDefaultEntertainmenturlId);
			toReturn.Add(this.EntertainmentEntityUsingEntertainmentId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between EntertainmenturlEntity and EntertainmentEntity over the 1:n relation they have, using the relation between the fields:
		/// Entertainmenturl.EntertainmenturlId - Entertainment.DefaultEntertainmenturlId
		/// </summary>
		public virtual IEntityRelation EntertainmentEntityUsingDefaultEntertainmenturlId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "EntertainmentCollection" , true);
				relation.AddEntityFieldPair(EntertainmenturlFields.EntertainmenturlId, EntertainmentFields.DefaultEntertainmenturlId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmenturlEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between EntertainmenturlEntity and EntertainmentEntity over the m:1 relation they have, using the relation between the fields:
		/// Entertainmenturl.EntertainmentId - Entertainment.EntertainmentId
		/// </summary>
		public virtual IEntityRelation EntertainmentEntityUsingEntertainmentId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "EntertainmentEntity", false);
				relation.AddEntityFieldPair(EntertainmentFields.EntertainmentId, EntertainmenturlFields.EntertainmentId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmenturlEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticEntertainmenturlRelations
	{
		internal static readonly IEntityRelation EntertainmentEntityUsingDefaultEntertainmenturlIdStatic = new EntertainmenturlRelations().EntertainmentEntityUsingDefaultEntertainmenturlId;
		internal static readonly IEntityRelation EntertainmentEntityUsingEntertainmentIdStatic = new EntertainmenturlRelations().EntertainmentEntityUsingEntertainmentId;

		/// <summary>CTor</summary>
		static StaticEntertainmenturlRelations()
		{
		}
	}
}
