﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: GameSession. </summary>
	public partial class GameSessionRelations
	{
		/// <summary>CTor</summary>
		public GameSessionRelations()
		{
		}

		/// <summary>Gets all relations of the GameSessionEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ClientEntityUsingClientId);
			toReturn.Add(this.DeliverypointEntityUsingDeliverypointId);
			toReturn.Add(this.GameEntityUsingGameId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between GameSessionEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// GameSession.ClientId - Client.ClientId
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingClientId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ClientEntity", false);
				relation.AddEntityFieldPair(ClientFields.ClientId, GameSessionFields.ClientId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GameSessionEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between GameSessionEntity and DeliverypointEntity over the m:1 relation they have, using the relation between the fields:
		/// GameSession.DeliverypointId - Deliverypoint.DeliverypointId
		/// </summary>
		public virtual IEntityRelation DeliverypointEntityUsingDeliverypointId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DeliverypointEntity", false);
				relation.AddEntityFieldPair(DeliverypointFields.DeliverypointId, GameSessionFields.DeliverypointId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GameSessionEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between GameSessionEntity and GameEntity over the m:1 relation they have, using the relation between the fields:
		/// GameSession.GameId - Game.GameId
		/// </summary>
		public virtual IEntityRelation GameEntityUsingGameId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "GameEntity", false);
				relation.AddEntityFieldPair(GameFields.GameId, GameSessionFields.GameId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GameEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GameSessionEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticGameSessionRelations
	{
		internal static readonly IEntityRelation ClientEntityUsingClientIdStatic = new GameSessionRelations().ClientEntityUsingClientId;
		internal static readonly IEntityRelation DeliverypointEntityUsingDeliverypointIdStatic = new GameSessionRelations().DeliverypointEntityUsingDeliverypointId;
		internal static readonly IEntityRelation GameEntityUsingGameIdStatic = new GameSessionRelations().GameEntityUsingGameId;

		/// <summary>CTor</summary>
		static StaticGameSessionRelations()
		{
		}
	}
}
