﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Brand. </summary>
	public partial class BrandRelations
	{
		/// <summary>CTor</summary>
		public BrandRelations()
		{
		}

		/// <summary>Gets all relations of the BrandEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AlterationEntityUsingBrandId);
			toReturn.Add(this.AlterationoptionEntityUsingBrandId);
			toReturn.Add(this.BrandCultureEntityUsingBrandId);
			toReturn.Add(this.CompanyBrandEntityUsingBrandId);
			toReturn.Add(this.GenericalterationEntityUsingBrandId);
			toReturn.Add(this.GenericcategoryEntityUsingBrandId);
			toReturn.Add(this.GenericproductEntityUsingBrandId);
			toReturn.Add(this.ProductEntityUsingBrandId);
			toReturn.Add(this.UserBrandEntityUsingBrandId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between BrandEntity and AlterationEntity over the 1:n relation they have, using the relation between the fields:
		/// Brand.BrandId - Alteration.BrandId
		/// </summary>
		public virtual IEntityRelation AlterationEntityUsingBrandId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AlterationCollection" , true);
				relation.AddEntityFieldPair(BrandFields.BrandId, AlterationFields.BrandId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BrandEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between BrandEntity and AlterationoptionEntity over the 1:n relation they have, using the relation between the fields:
		/// Brand.BrandId - Alterationoption.BrandId
		/// </summary>
		public virtual IEntityRelation AlterationoptionEntityUsingBrandId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AlterationoptionCollection" , true);
				relation.AddEntityFieldPair(BrandFields.BrandId, AlterationoptionFields.BrandId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BrandEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationoptionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between BrandEntity and BrandCultureEntity over the 1:n relation they have, using the relation between the fields:
		/// Brand.BrandId - BrandCulture.BrandId
		/// </summary>
		public virtual IEntityRelation BrandCultureEntityUsingBrandId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "BrandCultureCollection" , true);
				relation.AddEntityFieldPair(BrandFields.BrandId, BrandCultureFields.BrandId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BrandEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BrandCultureEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between BrandEntity and CompanyBrandEntity over the 1:n relation they have, using the relation between the fields:
		/// Brand.BrandId - CompanyBrand.BrandId
		/// </summary>
		public virtual IEntityRelation CompanyBrandEntityUsingBrandId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CompanyBrandCollection" , true);
				relation.AddEntityFieldPair(BrandFields.BrandId, CompanyBrandFields.BrandId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BrandEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyBrandEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between BrandEntity and GenericalterationEntity over the 1:n relation they have, using the relation between the fields:
		/// Brand.BrandId - Genericalteration.BrandId
		/// </summary>
		public virtual IEntityRelation GenericalterationEntityUsingBrandId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "GenericalterationCollection" , true);
				relation.AddEntityFieldPair(BrandFields.BrandId, GenericalterationFields.BrandId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BrandEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericalterationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between BrandEntity and GenericcategoryEntity over the 1:n relation they have, using the relation between the fields:
		/// Brand.BrandId - Genericcategory.BrandId
		/// </summary>
		public virtual IEntityRelation GenericcategoryEntityUsingBrandId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "GenericcategoryCollection" , true);
				relation.AddEntityFieldPair(BrandFields.BrandId, GenericcategoryFields.BrandId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BrandEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericcategoryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between BrandEntity and GenericproductEntity over the 1:n relation they have, using the relation between the fields:
		/// Brand.BrandId - Genericproduct.BrandId
		/// </summary>
		public virtual IEntityRelation GenericproductEntityUsingBrandId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "GenericproductCollection" , true);
				relation.AddEntityFieldPair(BrandFields.BrandId, GenericproductFields.BrandId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BrandEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericproductEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between BrandEntity and ProductEntity over the 1:n relation they have, using the relation between the fields:
		/// Brand.BrandId - Product.BrandId
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingBrandId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ProductCollection" , true);
				relation.AddEntityFieldPair(BrandFields.BrandId, ProductFields.BrandId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BrandEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between BrandEntity and UserBrandEntity over the 1:n relation they have, using the relation between the fields:
		/// Brand.BrandId - UserBrand.BrandId
		/// </summary>
		public virtual IEntityRelation UserBrandEntityUsingBrandId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UserBrandCollection" , true);
				relation.AddEntityFieldPair(BrandFields.BrandId, UserBrandFields.BrandId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BrandEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserBrandEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticBrandRelations
	{
		internal static readonly IEntityRelation AlterationEntityUsingBrandIdStatic = new BrandRelations().AlterationEntityUsingBrandId;
		internal static readonly IEntityRelation AlterationoptionEntityUsingBrandIdStatic = new BrandRelations().AlterationoptionEntityUsingBrandId;
		internal static readonly IEntityRelation BrandCultureEntityUsingBrandIdStatic = new BrandRelations().BrandCultureEntityUsingBrandId;
		internal static readonly IEntityRelation CompanyBrandEntityUsingBrandIdStatic = new BrandRelations().CompanyBrandEntityUsingBrandId;
		internal static readonly IEntityRelation GenericalterationEntityUsingBrandIdStatic = new BrandRelations().GenericalterationEntityUsingBrandId;
		internal static readonly IEntityRelation GenericcategoryEntityUsingBrandIdStatic = new BrandRelations().GenericcategoryEntityUsingBrandId;
		internal static readonly IEntityRelation GenericproductEntityUsingBrandIdStatic = new BrandRelations().GenericproductEntityUsingBrandId;
		internal static readonly IEntityRelation ProductEntityUsingBrandIdStatic = new BrandRelations().ProductEntityUsingBrandId;
		internal static readonly IEntityRelation UserBrandEntityUsingBrandIdStatic = new BrandRelations().UserBrandEntityUsingBrandId;

		/// <summary>CTor</summary>
		static StaticBrandRelations()
		{
		}
	}
}
