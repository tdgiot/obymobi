﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Productgroup. </summary>
	public partial class ProductgroupRelations
	{
		/// <summary>CTor</summary>
		public ProductgroupRelations()
		{
		}

		/// <summary>Gets all relations of the ProductgroupEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CustomTextEntityUsingProductgroupId);
			toReturn.Add(this.MediaEntityUsingProductgroupId);
			toReturn.Add(this.ProductEntityUsingProductgroupId);
			toReturn.Add(this.ProductgroupItemEntityUsingProductgroupId);
			toReturn.Add(this.ProductgroupItemEntityUsingNestedProductgroupId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ProductgroupEntity and CustomTextEntity over the 1:n relation they have, using the relation between the fields:
		/// Productgroup.ProductgroupId - CustomText.ProductgroupId
		/// </summary>
		public virtual IEntityRelation CustomTextEntityUsingProductgroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomTextCollection" , true);
				relation.AddEntityFieldPair(ProductgroupFields.ProductgroupId, CustomTextFields.ProductgroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductgroupEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductgroupEntity and MediaEntity over the 1:n relation they have, using the relation between the fields:
		/// Productgroup.ProductgroupId - Media.ProductgroupId
		/// </summary>
		public virtual IEntityRelation MediaEntityUsingProductgroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MediaCollection" , true);
				relation.AddEntityFieldPair(ProductgroupFields.ProductgroupId, MediaFields.ProductgroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductgroupEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductgroupEntity and ProductEntity over the 1:n relation they have, using the relation between the fields:
		/// Productgroup.ProductgroupId - Product.ProductgroupId
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingProductgroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ProductCollection" , true);
				relation.AddEntityFieldPair(ProductgroupFields.ProductgroupId, ProductFields.ProductgroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductgroupEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductgroupEntity and ProductgroupItemEntity over the 1:n relation they have, using the relation between the fields:
		/// Productgroup.ProductgroupId - ProductgroupItem.ProductgroupId
		/// </summary>
		public virtual IEntityRelation ProductgroupItemEntityUsingProductgroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ProductgroupItemCollection" , true);
				relation.AddEntityFieldPair(ProductgroupFields.ProductgroupId, ProductgroupItemFields.ProductgroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductgroupEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductgroupItemEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductgroupEntity and ProductgroupItemEntity over the 1:n relation they have, using the relation between the fields:
		/// Productgroup.ProductgroupId - ProductgroupItem.NestedProductgroupId
		/// </summary>
		public virtual IEntityRelation ProductgroupItemEntityUsingNestedProductgroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ProductgroupItemCollection1" , true);
				relation.AddEntityFieldPair(ProductgroupFields.ProductgroupId, ProductgroupItemFields.NestedProductgroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductgroupEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductgroupItemEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between ProductgroupEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// Productgroup.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, ProductgroupFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductgroupEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticProductgroupRelations
	{
		internal static readonly IEntityRelation CustomTextEntityUsingProductgroupIdStatic = new ProductgroupRelations().CustomTextEntityUsingProductgroupId;
		internal static readonly IEntityRelation MediaEntityUsingProductgroupIdStatic = new ProductgroupRelations().MediaEntityUsingProductgroupId;
		internal static readonly IEntityRelation ProductEntityUsingProductgroupIdStatic = new ProductgroupRelations().ProductEntityUsingProductgroupId;
		internal static readonly IEntityRelation ProductgroupItemEntityUsingProductgroupIdStatic = new ProductgroupRelations().ProductgroupItemEntityUsingProductgroupId;
		internal static readonly IEntityRelation ProductgroupItemEntityUsingNestedProductgroupIdStatic = new ProductgroupRelations().ProductgroupItemEntityUsingNestedProductgroupId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new ProductgroupRelations().CompanyEntityUsingCompanyId;

		/// <summary>CTor</summary>
		static StaticProductgroupRelations()
		{
		}
	}
}
