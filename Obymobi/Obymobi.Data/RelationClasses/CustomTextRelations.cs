﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: CustomText. </summary>
	public partial class CustomTextRelations
	{
		/// <summary>CTor</summary>
		public CustomTextRelations()
		{
		}

		/// <summary>Gets all relations of the CustomTextEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ActionButtonEntityUsingActionButtonId);
			toReturn.Add(this.AdvertisementEntityUsingAdvertisementId);
			toReturn.Add(this.AlterationEntityUsingAlterationId);
			toReturn.Add(this.AlterationoptionEntityUsingAlterationoptionId);
			toReturn.Add(this.AmenityEntityUsingAmenityId);
			toReturn.Add(this.AnnouncementEntityUsingAnnouncementId);
			toReturn.Add(this.ApplicationConfigurationEntityUsingApplicationConfigurationId);
			toReturn.Add(this.CarouselItemEntityUsingCarouselItemId);
			toReturn.Add(this.LandingPageEntityUsingLandingPageId);
			toReturn.Add(this.NavigationMenuItemEntityUsingNavigationMenuItemId);
			toReturn.Add(this.WidgetEntityUsingWidgetId);
			toReturn.Add(this.AttachmentEntityUsingAttachmentId);
			toReturn.Add(this.AvailabilityEntityUsingAvailabilityId);
			toReturn.Add(this.CategoryEntityUsingCategoryId);
			toReturn.Add(this.CheckoutMethodEntityUsingCheckoutMethodId);
			toReturn.Add(this.ClientConfigurationEntityUsingClientConfigurationId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			toReturn.Add(this.DeliverypointgroupEntityUsingDeliverypointgroupId);
			toReturn.Add(this.EntertainmentcategoryEntityUsingEntertainmentcategoryId);
			toReturn.Add(this.GenericcategoryEntityUsingGenericcategoryId);
			toReturn.Add(this.GenericproductEntityUsingGenericproductId);
			toReturn.Add(this.InfraredCommandEntityUsingInfraredCommandId);
			toReturn.Add(this.LanguageEntityUsingLanguageId);
			toReturn.Add(this.OutletEntityUsingOutletId);
			toReturn.Add(this.PageEntityUsingPageId);
			toReturn.Add(this.PageTemplateEntityUsingPageTemplateId);
			toReturn.Add(this.PointOfInterestEntityUsingPointOfInterestId);
			toReturn.Add(this.ProductEntityUsingProductId);
			toReturn.Add(this.ProductgroupEntityUsingProductgroupId);
			toReturn.Add(this.RoomControlAreaEntityUsingRoomControlAreaId);
			toReturn.Add(this.RoomControlComponentEntityUsingRoomControlComponentId);
			toReturn.Add(this.RoomControlSectionEntityUsingRoomControlSectionId);
			toReturn.Add(this.RoomControlSectionItemEntityUsingRoomControlSectionItemId);
			toReturn.Add(this.RoomControlWidgetEntityUsingRoomControlWidgetId);
			toReturn.Add(this.RoutestephandlerEntityUsingRoutestephandlerId);
			toReturn.Add(this.ScheduledMessageEntityUsingScheduledMessageId);
			toReturn.Add(this.ServiceMethodEntityUsingServiceMethodId);
			toReturn.Add(this.SiteEntityUsingSiteId);
			toReturn.Add(this.SiteTemplateEntityUsingSiteTemplateId);
			toReturn.Add(this.StationEntityUsingStationId);
			toReturn.Add(this.UIFooterItemEntityUsingUIFooterItemId);
			toReturn.Add(this.UITabEntityUsingUITabId);
			toReturn.Add(this.UIWidgetEntityUsingUIWidgetId);
			toReturn.Add(this.VenueCategoryEntityUsingVenueCategoryId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between CustomTextEntity and ActionButtonEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomText.ActionButtonId - ActionButton.ActionButtonId
		/// </summary>
		public virtual IEntityRelation ActionButtonEntityUsingActionButtonId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ActionButtonEntity", false);
				relation.AddEntityFieldPair(ActionButtonFields.ActionButtonId, CustomTextFields.ActionButtonId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ActionButtonEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CustomTextEntity and AdvertisementEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomText.AdvertisementId - Advertisement.AdvertisementId
		/// </summary>
		public virtual IEntityRelation AdvertisementEntityUsingAdvertisementId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AdvertisementEntity", false);
				relation.AddEntityFieldPair(AdvertisementFields.AdvertisementId, CustomTextFields.AdvertisementId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CustomTextEntity and AlterationEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomText.AlterationId - Alteration.AlterationId
		/// </summary>
		public virtual IEntityRelation AlterationEntityUsingAlterationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AlterationEntity", false);
				relation.AddEntityFieldPair(AlterationFields.AlterationId, CustomTextFields.AlterationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CustomTextEntity and AlterationoptionEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomText.AlterationoptionId - Alterationoption.AlterationoptionId
		/// </summary>
		public virtual IEntityRelation AlterationoptionEntityUsingAlterationoptionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AlterationoptionEntity", false);
				relation.AddEntityFieldPair(AlterationoptionFields.AlterationoptionId, CustomTextFields.AlterationoptionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationoptionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CustomTextEntity and AmenityEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomText.AmenityId - Amenity.AmenityId
		/// </summary>
		public virtual IEntityRelation AmenityEntityUsingAmenityId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AmenityEntity", false);
				relation.AddEntityFieldPair(AmenityFields.AmenityId, CustomTextFields.AmenityId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AmenityEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CustomTextEntity and AnnouncementEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomText.AnnouncementId - Announcement.AnnouncementId
		/// </summary>
		public virtual IEntityRelation AnnouncementEntityUsingAnnouncementId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AnnouncementEntity", false);
				relation.AddEntityFieldPair(AnnouncementFields.AnnouncementId, CustomTextFields.AnnouncementId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AnnouncementEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CustomTextEntity and ApplicationConfigurationEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomText.ApplicationConfigurationId - ApplicationConfiguration.ApplicationConfigurationId
		/// </summary>
		public virtual IEntityRelation ApplicationConfigurationEntityUsingApplicationConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ApplicationConfigurationEntity", false);
				relation.AddEntityFieldPair(ApplicationConfigurationFields.ApplicationConfigurationId, CustomTextFields.ApplicationConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ApplicationConfigurationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CustomTextEntity and CarouselItemEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomText.CarouselItemId - CarouselItem.CarouselItemId
		/// </summary>
		public virtual IEntityRelation CarouselItemEntityUsingCarouselItemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CarouselItemEntity", false);
				relation.AddEntityFieldPair(CarouselItemFields.CarouselItemId, CustomTextFields.CarouselItemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CarouselItemEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CustomTextEntity and LandingPageEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomText.LandingPageId - LandingPage.LandingPageId
		/// </summary>
		public virtual IEntityRelation LandingPageEntityUsingLandingPageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "LandingPageEntity", false);
				relation.AddEntityFieldPair(LandingPageFields.LandingPageId, CustomTextFields.LandingPageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LandingPageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CustomTextEntity and NavigationMenuItemEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomText.NavigationMenuItemId - NavigationMenuItem.NavigationMenuItemId
		/// </summary>
		public virtual IEntityRelation NavigationMenuItemEntityUsingNavigationMenuItemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "NavigationMenuItemEntity", false);
				relation.AddEntityFieldPair(NavigationMenuItemFields.NavigationMenuItemId, CustomTextFields.NavigationMenuItemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NavigationMenuItemEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CustomTextEntity and WidgetEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomText.WidgetId - Widget.WidgetId
		/// </summary>
		public virtual IEntityRelation WidgetEntityUsingWidgetId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "WidgetEntity", false);
				relation.AddEntityFieldPair(WidgetFields.WidgetId, CustomTextFields.WidgetId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WidgetEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CustomTextEntity and AttachmentEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomText.AttachmentId - Attachment.AttachmentId
		/// </summary>
		public virtual IEntityRelation AttachmentEntityUsingAttachmentId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AttachmentEntity", false);
				relation.AddEntityFieldPair(AttachmentFields.AttachmentId, CustomTextFields.AttachmentId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttachmentEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CustomTextEntity and AvailabilityEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomText.AvailabilityId - Availability.AvailabilityId
		/// </summary>
		public virtual IEntityRelation AvailabilityEntityUsingAvailabilityId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AvailabilityEntity", false);
				relation.AddEntityFieldPair(AvailabilityFields.AvailabilityId, CustomTextFields.AvailabilityId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AvailabilityEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CustomTextEntity and CategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomText.CategoryId - Category.CategoryId
		/// </summary>
		public virtual IEntityRelation CategoryEntityUsingCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CategoryEntity", false);
				relation.AddEntityFieldPair(CategoryFields.CategoryId, CustomTextFields.CategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CustomTextEntity and CheckoutMethodEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomText.CheckoutMethodId - CheckoutMethod.CheckoutMethodId
		/// </summary>
		public virtual IEntityRelation CheckoutMethodEntityUsingCheckoutMethodId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CheckoutMethodEntity", false);
				relation.AddEntityFieldPair(CheckoutMethodFields.CheckoutMethodId, CustomTextFields.CheckoutMethodId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CheckoutMethodEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CustomTextEntity and ClientConfigurationEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomText.ClientConfigurationId - ClientConfiguration.ClientConfigurationId
		/// </summary>
		public virtual IEntityRelation ClientConfigurationEntityUsingClientConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ClientConfigurationEntity", false);
				relation.AddEntityFieldPair(ClientConfigurationFields.ClientConfigurationId, CustomTextFields.ClientConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientConfigurationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CustomTextEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomText.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, CustomTextFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CustomTextEntity and DeliverypointgroupEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomText.DeliverypointgroupId - Deliverypointgroup.DeliverypointgroupId
		/// </summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingDeliverypointgroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DeliverypointgroupEntity", false);
				relation.AddEntityFieldPair(DeliverypointgroupFields.DeliverypointgroupId, CustomTextFields.DeliverypointgroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CustomTextEntity and EntertainmentcategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomText.EntertainmentcategoryId - Entertainmentcategory.EntertainmentcategoryId
		/// </summary>
		public virtual IEntityRelation EntertainmentcategoryEntityUsingEntertainmentcategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "EntertainmentcategoryEntity", false);
				relation.AddEntityFieldPair(EntertainmentcategoryFields.EntertainmentcategoryId, CustomTextFields.EntertainmentcategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentcategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CustomTextEntity and GenericcategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomText.GenericcategoryId - Genericcategory.GenericcategoryId
		/// </summary>
		public virtual IEntityRelation GenericcategoryEntityUsingGenericcategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "GenericcategoryEntity", false);
				relation.AddEntityFieldPair(GenericcategoryFields.GenericcategoryId, CustomTextFields.GenericcategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericcategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CustomTextEntity and GenericproductEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomText.GenericproductId - Genericproduct.GenericproductId
		/// </summary>
		public virtual IEntityRelation GenericproductEntityUsingGenericproductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "GenericproductEntity", false);
				relation.AddEntityFieldPair(GenericproductFields.GenericproductId, CustomTextFields.GenericproductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericproductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CustomTextEntity and InfraredCommandEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomText.InfraredCommandId - InfraredCommand.InfraredCommandId
		/// </summary>
		public virtual IEntityRelation InfraredCommandEntityUsingInfraredCommandId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "InfraredCommandEntity", false);
				relation.AddEntityFieldPair(InfraredCommandFields.InfraredCommandId, CustomTextFields.InfraredCommandId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InfraredCommandEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CustomTextEntity and LanguageEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomText.LanguageId - Language.LanguageId
		/// </summary>
		public virtual IEntityRelation LanguageEntityUsingLanguageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "LanguageEntity", false);
				relation.AddEntityFieldPair(LanguageFields.LanguageId, CustomTextFields.LanguageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CustomTextEntity and OutletEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomText.OutletId - Outlet.OutletId
		/// </summary>
		public virtual IEntityRelation OutletEntityUsingOutletId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "OutletEntity", false);
				relation.AddEntityFieldPair(OutletFields.OutletId, CustomTextFields.OutletId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OutletEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CustomTextEntity and PageEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomText.PageId - Page.PageId
		/// </summary>
		public virtual IEntityRelation PageEntityUsingPageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PageEntity", false);
				relation.AddEntityFieldPair(PageFields.PageId, CustomTextFields.PageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CustomTextEntity and PageTemplateEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomText.PageTemplateId - PageTemplate.PageTemplateId
		/// </summary>
		public virtual IEntityRelation PageTemplateEntityUsingPageTemplateId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PageTemplateEntity", false);
				relation.AddEntityFieldPair(PageTemplateFields.PageTemplateId, CustomTextFields.PageTemplateId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageTemplateEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CustomTextEntity and PointOfInterestEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomText.PointOfInterestId - PointOfInterest.PointOfInterestId
		/// </summary>
		public virtual IEntityRelation PointOfInterestEntityUsingPointOfInterestId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PointOfInterestEntity", false);
				relation.AddEntityFieldPair(PointOfInterestFields.PointOfInterestId, CustomTextFields.PointOfInterestId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PointOfInterestEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CustomTextEntity and ProductEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomText.ProductId - Product.ProductId
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ProductEntity", false);
				relation.AddEntityFieldPair(ProductFields.ProductId, CustomTextFields.ProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CustomTextEntity and ProductgroupEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomText.ProductgroupId - Productgroup.ProductgroupId
		/// </summary>
		public virtual IEntityRelation ProductgroupEntityUsingProductgroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ProductgroupEntity", false);
				relation.AddEntityFieldPair(ProductgroupFields.ProductgroupId, CustomTextFields.ProductgroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductgroupEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CustomTextEntity and RoomControlAreaEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomText.RoomControlAreaId - RoomControlArea.RoomControlAreaId
		/// </summary>
		public virtual IEntityRelation RoomControlAreaEntityUsingRoomControlAreaId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RoomControlAreaEntity", false);
				relation.AddEntityFieldPair(RoomControlAreaFields.RoomControlAreaId, CustomTextFields.RoomControlAreaId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlAreaEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CustomTextEntity and RoomControlComponentEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomText.RoomControlComponentId - RoomControlComponent.RoomControlComponentId
		/// </summary>
		public virtual IEntityRelation RoomControlComponentEntityUsingRoomControlComponentId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RoomControlComponentEntity", false);
				relation.AddEntityFieldPair(RoomControlComponentFields.RoomControlComponentId, CustomTextFields.RoomControlComponentId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlComponentEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CustomTextEntity and RoomControlSectionEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomText.RoomControlSectionId - RoomControlSection.RoomControlSectionId
		/// </summary>
		public virtual IEntityRelation RoomControlSectionEntityUsingRoomControlSectionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RoomControlSectionEntity", false);
				relation.AddEntityFieldPair(RoomControlSectionFields.RoomControlSectionId, CustomTextFields.RoomControlSectionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlSectionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CustomTextEntity and RoomControlSectionItemEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomText.RoomControlSectionItemId - RoomControlSectionItem.RoomControlSectionItemId
		/// </summary>
		public virtual IEntityRelation RoomControlSectionItemEntityUsingRoomControlSectionItemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RoomControlSectionItemEntity", false);
				relation.AddEntityFieldPair(RoomControlSectionItemFields.RoomControlSectionItemId, CustomTextFields.RoomControlSectionItemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlSectionItemEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CustomTextEntity and RoomControlWidgetEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomText.RoomControlWidgetId - RoomControlWidget.RoomControlWidgetId
		/// </summary>
		public virtual IEntityRelation RoomControlWidgetEntityUsingRoomControlWidgetId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RoomControlWidgetEntity", false);
				relation.AddEntityFieldPair(RoomControlWidgetFields.RoomControlWidgetId, CustomTextFields.RoomControlWidgetId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlWidgetEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CustomTextEntity and RoutestephandlerEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomText.RoutestephandlerId - Routestephandler.RoutestephandlerId
		/// </summary>
		public virtual IEntityRelation RoutestephandlerEntityUsingRoutestephandlerId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RoutestephandlerEntity", false);
				relation.AddEntityFieldPair(RoutestephandlerFields.RoutestephandlerId, CustomTextFields.RoutestephandlerId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoutestephandlerEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CustomTextEntity and ScheduledMessageEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomText.ScheduledMessageId - ScheduledMessage.ScheduledMessageId
		/// </summary>
		public virtual IEntityRelation ScheduledMessageEntityUsingScheduledMessageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ScheduledMessageEntity", false);
				relation.AddEntityFieldPair(ScheduledMessageFields.ScheduledMessageId, CustomTextFields.ScheduledMessageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduledMessageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CustomTextEntity and ServiceMethodEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomText.ServiceMethodId - ServiceMethod.ServiceMethodId
		/// </summary>
		public virtual IEntityRelation ServiceMethodEntityUsingServiceMethodId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ServiceMethodEntity", false);
				relation.AddEntityFieldPair(ServiceMethodFields.ServiceMethodId, CustomTextFields.ServiceMethodId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ServiceMethodEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CustomTextEntity and SiteEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomText.SiteId - Site.SiteId
		/// </summary>
		public virtual IEntityRelation SiteEntityUsingSiteId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SiteEntity", false);
				relation.AddEntityFieldPair(SiteFields.SiteId, CustomTextFields.SiteId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SiteEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CustomTextEntity and SiteTemplateEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomText.SiteTemplateId - SiteTemplate.SiteTemplateId
		/// </summary>
		public virtual IEntityRelation SiteTemplateEntityUsingSiteTemplateId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SiteTemplateEntity", false);
				relation.AddEntityFieldPair(SiteTemplateFields.SiteTemplateId, CustomTextFields.SiteTemplateId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SiteTemplateEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CustomTextEntity and StationEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomText.StationId - Station.StationId
		/// </summary>
		public virtual IEntityRelation StationEntityUsingStationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "StationEntity", false);
				relation.AddEntityFieldPair(StationFields.StationId, CustomTextFields.StationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("StationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CustomTextEntity and UIFooterItemEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomText.UIFooterItemId - UIFooterItem.UIFooterItemId
		/// </summary>
		public virtual IEntityRelation UIFooterItemEntityUsingUIFooterItemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UIFooterItemEntity", false);
				relation.AddEntityFieldPair(UIFooterItemFields.UIFooterItemId, CustomTextFields.UIFooterItemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIFooterItemEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CustomTextEntity and UITabEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomText.UITabId - UITab.UITabId
		/// </summary>
		public virtual IEntityRelation UITabEntityUsingUITabId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UITabEntity", false);
				relation.AddEntityFieldPair(UITabFields.UITabId, CustomTextFields.UITabId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UITabEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CustomTextEntity and UIWidgetEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomText.UIWidgetId - UIWidget.UIWidgetId
		/// </summary>
		public virtual IEntityRelation UIWidgetEntityUsingUIWidgetId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UIWidgetEntity", false);
				relation.AddEntityFieldPair(UIWidgetFields.UIWidgetId, CustomTextFields.UIWidgetId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIWidgetEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CustomTextEntity and VenueCategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomText.VenueCategoryId - VenueCategory.VenueCategoryId
		/// </summary>
		public virtual IEntityRelation VenueCategoryEntityUsingVenueCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "VenueCategoryEntity", false);
				relation.AddEntityFieldPair(VenueCategoryFields.VenueCategoryId, CustomTextFields.VenueCategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VenueCategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCustomTextRelations
	{
		internal static readonly IEntityRelation ActionButtonEntityUsingActionButtonIdStatic = new CustomTextRelations().ActionButtonEntityUsingActionButtonId;
		internal static readonly IEntityRelation AdvertisementEntityUsingAdvertisementIdStatic = new CustomTextRelations().AdvertisementEntityUsingAdvertisementId;
		internal static readonly IEntityRelation AlterationEntityUsingAlterationIdStatic = new CustomTextRelations().AlterationEntityUsingAlterationId;
		internal static readonly IEntityRelation AlterationoptionEntityUsingAlterationoptionIdStatic = new CustomTextRelations().AlterationoptionEntityUsingAlterationoptionId;
		internal static readonly IEntityRelation AmenityEntityUsingAmenityIdStatic = new CustomTextRelations().AmenityEntityUsingAmenityId;
		internal static readonly IEntityRelation AnnouncementEntityUsingAnnouncementIdStatic = new CustomTextRelations().AnnouncementEntityUsingAnnouncementId;
		internal static readonly IEntityRelation ApplicationConfigurationEntityUsingApplicationConfigurationIdStatic = new CustomTextRelations().ApplicationConfigurationEntityUsingApplicationConfigurationId;
		internal static readonly IEntityRelation CarouselItemEntityUsingCarouselItemIdStatic = new CustomTextRelations().CarouselItemEntityUsingCarouselItemId;
		internal static readonly IEntityRelation LandingPageEntityUsingLandingPageIdStatic = new CustomTextRelations().LandingPageEntityUsingLandingPageId;
		internal static readonly IEntityRelation NavigationMenuItemEntityUsingNavigationMenuItemIdStatic = new CustomTextRelations().NavigationMenuItemEntityUsingNavigationMenuItemId;
		internal static readonly IEntityRelation WidgetEntityUsingWidgetIdStatic = new CustomTextRelations().WidgetEntityUsingWidgetId;
		internal static readonly IEntityRelation AttachmentEntityUsingAttachmentIdStatic = new CustomTextRelations().AttachmentEntityUsingAttachmentId;
		internal static readonly IEntityRelation AvailabilityEntityUsingAvailabilityIdStatic = new CustomTextRelations().AvailabilityEntityUsingAvailabilityId;
		internal static readonly IEntityRelation CategoryEntityUsingCategoryIdStatic = new CustomTextRelations().CategoryEntityUsingCategoryId;
		internal static readonly IEntityRelation CheckoutMethodEntityUsingCheckoutMethodIdStatic = new CustomTextRelations().CheckoutMethodEntityUsingCheckoutMethodId;
		internal static readonly IEntityRelation ClientConfigurationEntityUsingClientConfigurationIdStatic = new CustomTextRelations().ClientConfigurationEntityUsingClientConfigurationId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new CustomTextRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation DeliverypointgroupEntityUsingDeliverypointgroupIdStatic = new CustomTextRelations().DeliverypointgroupEntityUsingDeliverypointgroupId;
		internal static readonly IEntityRelation EntertainmentcategoryEntityUsingEntertainmentcategoryIdStatic = new CustomTextRelations().EntertainmentcategoryEntityUsingEntertainmentcategoryId;
		internal static readonly IEntityRelation GenericcategoryEntityUsingGenericcategoryIdStatic = new CustomTextRelations().GenericcategoryEntityUsingGenericcategoryId;
		internal static readonly IEntityRelation GenericproductEntityUsingGenericproductIdStatic = new CustomTextRelations().GenericproductEntityUsingGenericproductId;
		internal static readonly IEntityRelation InfraredCommandEntityUsingInfraredCommandIdStatic = new CustomTextRelations().InfraredCommandEntityUsingInfraredCommandId;
		internal static readonly IEntityRelation LanguageEntityUsingLanguageIdStatic = new CustomTextRelations().LanguageEntityUsingLanguageId;
		internal static readonly IEntityRelation OutletEntityUsingOutletIdStatic = new CustomTextRelations().OutletEntityUsingOutletId;
		internal static readonly IEntityRelation PageEntityUsingPageIdStatic = new CustomTextRelations().PageEntityUsingPageId;
		internal static readonly IEntityRelation PageTemplateEntityUsingPageTemplateIdStatic = new CustomTextRelations().PageTemplateEntityUsingPageTemplateId;
		internal static readonly IEntityRelation PointOfInterestEntityUsingPointOfInterestIdStatic = new CustomTextRelations().PointOfInterestEntityUsingPointOfInterestId;
		internal static readonly IEntityRelation ProductEntityUsingProductIdStatic = new CustomTextRelations().ProductEntityUsingProductId;
		internal static readonly IEntityRelation ProductgroupEntityUsingProductgroupIdStatic = new CustomTextRelations().ProductgroupEntityUsingProductgroupId;
		internal static readonly IEntityRelation RoomControlAreaEntityUsingRoomControlAreaIdStatic = new CustomTextRelations().RoomControlAreaEntityUsingRoomControlAreaId;
		internal static readonly IEntityRelation RoomControlComponentEntityUsingRoomControlComponentIdStatic = new CustomTextRelations().RoomControlComponentEntityUsingRoomControlComponentId;
		internal static readonly IEntityRelation RoomControlSectionEntityUsingRoomControlSectionIdStatic = new CustomTextRelations().RoomControlSectionEntityUsingRoomControlSectionId;
		internal static readonly IEntityRelation RoomControlSectionItemEntityUsingRoomControlSectionItemIdStatic = new CustomTextRelations().RoomControlSectionItemEntityUsingRoomControlSectionItemId;
		internal static readonly IEntityRelation RoomControlWidgetEntityUsingRoomControlWidgetIdStatic = new CustomTextRelations().RoomControlWidgetEntityUsingRoomControlWidgetId;
		internal static readonly IEntityRelation RoutestephandlerEntityUsingRoutestephandlerIdStatic = new CustomTextRelations().RoutestephandlerEntityUsingRoutestephandlerId;
		internal static readonly IEntityRelation ScheduledMessageEntityUsingScheduledMessageIdStatic = new CustomTextRelations().ScheduledMessageEntityUsingScheduledMessageId;
		internal static readonly IEntityRelation ServiceMethodEntityUsingServiceMethodIdStatic = new CustomTextRelations().ServiceMethodEntityUsingServiceMethodId;
		internal static readonly IEntityRelation SiteEntityUsingSiteIdStatic = new CustomTextRelations().SiteEntityUsingSiteId;
		internal static readonly IEntityRelation SiteTemplateEntityUsingSiteTemplateIdStatic = new CustomTextRelations().SiteTemplateEntityUsingSiteTemplateId;
		internal static readonly IEntityRelation StationEntityUsingStationIdStatic = new CustomTextRelations().StationEntityUsingStationId;
		internal static readonly IEntityRelation UIFooterItemEntityUsingUIFooterItemIdStatic = new CustomTextRelations().UIFooterItemEntityUsingUIFooterItemId;
		internal static readonly IEntityRelation UITabEntityUsingUITabIdStatic = new CustomTextRelations().UITabEntityUsingUITabId;
		internal static readonly IEntityRelation UIWidgetEntityUsingUIWidgetIdStatic = new CustomTextRelations().UIWidgetEntityUsingUIWidgetId;
		internal static readonly IEntityRelation VenueCategoryEntityUsingVenueCategoryIdStatic = new CustomTextRelations().VenueCategoryEntityUsingVenueCategoryId;

		/// <summary>CTor</summary>
		static StaticCustomTextRelations()
		{
		}
	}
}
