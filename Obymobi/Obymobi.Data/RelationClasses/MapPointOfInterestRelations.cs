﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: MapPointOfInterest. </summary>
	public partial class MapPointOfInterestRelations
	{
		/// <summary>CTor</summary>
		public MapPointOfInterestRelations()
		{
		}

		/// <summary>Gets all relations of the MapPointOfInterestEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.MapEntityUsingMapId);
			toReturn.Add(this.PointOfInterestEntityUsingPointOfInterestId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between MapPointOfInterestEntity and MapEntity over the m:1 relation they have, using the relation between the fields:
		/// MapPointOfInterest.MapId - Map.MapId
		/// </summary>
		public virtual IEntityRelation MapEntityUsingMapId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "MapEntity", false);
				relation.AddEntityFieldPair(MapFields.MapId, MapPointOfInterestFields.MapId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MapEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MapPointOfInterestEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MapPointOfInterestEntity and PointOfInterestEntity over the m:1 relation they have, using the relation between the fields:
		/// MapPointOfInterest.PointOfInterestId - PointOfInterest.PointOfInterestId
		/// </summary>
		public virtual IEntityRelation PointOfInterestEntityUsingPointOfInterestId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PointOfInterestEntity", false);
				relation.AddEntityFieldPair(PointOfInterestFields.PointOfInterestId, MapPointOfInterestFields.PointOfInterestId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PointOfInterestEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MapPointOfInterestEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticMapPointOfInterestRelations
	{
		internal static readonly IEntityRelation MapEntityUsingMapIdStatic = new MapPointOfInterestRelations().MapEntityUsingMapId;
		internal static readonly IEntityRelation PointOfInterestEntityUsingPointOfInterestIdStatic = new MapPointOfInterestRelations().PointOfInterestEntityUsingPointOfInterestId;

		/// <summary>CTor</summary>
		static StaticMapPointOfInterestRelations()
		{
		}
	}
}
