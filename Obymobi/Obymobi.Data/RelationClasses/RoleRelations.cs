﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Role. </summary>
	public partial class RoleRelations
	{
		/// <summary>CTor</summary>
		public RoleRelations()
		{
		}

		/// <summary>Gets all relations of the RoleEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.RoleModuleRightsEntityUsingRoleId);
			toReturn.Add(this.RoleUIElementRightsEntityUsingRoleId);
			toReturn.Add(this.RoleUIElementSubPanelRightsEntityUsingRoleId);
			toReturn.Add(this.UserRoleEntityUsingRoleId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between RoleEntity and RoleModuleRightsEntity over the 1:n relation they have, using the relation between the fields:
		/// Role.RoleId - RoleModuleRights.RoleId
		/// </summary>
		public virtual IEntityRelation RoleModuleRightsEntityUsingRoleId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RoleModuleRightsCollection" , true);
				relation.AddEntityFieldPair(RoleFields.RoleId, RoleModuleRightsFields.RoleId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoleEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoleModuleRightsEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RoleEntity and RoleUIElementRightsEntity over the 1:n relation they have, using the relation between the fields:
		/// Role.RoleId - RoleUIElementRights.RoleId
		/// </summary>
		public virtual IEntityRelation RoleUIElementRightsEntityUsingRoleId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RoleUIElementRightsCollection" , true);
				relation.AddEntityFieldPair(RoleFields.RoleId, RoleUIElementRightsFields.RoleId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoleEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoleUIElementRightsEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RoleEntity and RoleUIElementSubPanelRightsEntity over the 1:n relation they have, using the relation between the fields:
		/// Role.RoleId - RoleUIElementSubPanelRights.RoleId
		/// </summary>
		public virtual IEntityRelation RoleUIElementSubPanelRightsEntityUsingRoleId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RoleUIElementSubPanelRightsCollection" , true);
				relation.AddEntityFieldPair(RoleFields.RoleId, RoleUIElementSubPanelRightsFields.RoleId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoleEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoleUIElementSubPanelRightsEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RoleEntity and UserRoleEntity over the 1:n relation they have, using the relation between the fields:
		/// Role.RoleId - UserRole.RoleId
		/// </summary>
		public virtual IEntityRelation UserRoleEntityUsingRoleId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UserRoleCollection" , true);
				relation.AddEntityFieldPair(RoleFields.RoleId, UserRoleFields.RoleId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoleEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserRoleEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticRoleRelations
	{
		internal static readonly IEntityRelation RoleModuleRightsEntityUsingRoleIdStatic = new RoleRelations().RoleModuleRightsEntityUsingRoleId;
		internal static readonly IEntityRelation RoleUIElementRightsEntityUsingRoleIdStatic = new RoleRelations().RoleUIElementRightsEntityUsingRoleId;
		internal static readonly IEntityRelation RoleUIElementSubPanelRightsEntityUsingRoleIdStatic = new RoleRelations().RoleUIElementSubPanelRightsEntityUsingRoleId;
		internal static readonly IEntityRelation UserRoleEntityUsingRoleIdStatic = new RoleRelations().UserRoleEntityUsingRoleId;

		/// <summary>CTor</summary>
		static StaticRoleRelations()
		{
		}
	}
}
