﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Country. </summary>
	public partial class CountryRelations
	{
		/// <summary>CTor</summary>
		public CountryRelations()
		{
		}

		/// <summary>Gets all relations of the CountryEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CompanyEntityUsingCountryId);
			toReturn.Add(this.PointOfInterestEntityUsingCountryId);
			toReturn.Add(this.VattariffEntityUsingCountryId);
			toReturn.Add(this.CurrencyEntityUsingCurrencyId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between CountryEntity and CompanyEntity over the 1:n relation they have, using the relation between the fields:
		/// Country.CountryId - Company.CountryId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCountryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CompanyCollection" , true);
				relation.AddEntityFieldPair(CountryFields.CountryId, CompanyFields.CountryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CountryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CountryEntity and PointOfInterestEntity over the 1:n relation they have, using the relation between the fields:
		/// Country.CountryId - PointOfInterest.CountryId
		/// </summary>
		public virtual IEntityRelation PointOfInterestEntityUsingCountryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PointOfInterestCollection" , true);
				relation.AddEntityFieldPair(CountryFields.CountryId, PointOfInterestFields.CountryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CountryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PointOfInterestEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CountryEntity and VattariffEntity over the 1:n relation they have, using the relation between the fields:
		/// Country.CountryId - Vattariff.CountryId
		/// </summary>
		public virtual IEntityRelation VattariffEntityUsingCountryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "VattariffCollection" , true);
				relation.AddEntityFieldPair(CountryFields.CountryId, VattariffFields.CountryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CountryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VattariffEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between CountryEntity and CurrencyEntity over the m:1 relation they have, using the relation between the fields:
		/// Country.CurrencyId - Currency.CurrencyId
		/// </summary>
		public virtual IEntityRelation CurrencyEntityUsingCurrencyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CurrencyEntity", false);
				relation.AddEntityFieldPair(CurrencyFields.CurrencyId, CountryFields.CurrencyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CurrencyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CountryEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCountryRelations
	{
		internal static readonly IEntityRelation CompanyEntityUsingCountryIdStatic = new CountryRelations().CompanyEntityUsingCountryId;
		internal static readonly IEntityRelation PointOfInterestEntityUsingCountryIdStatic = new CountryRelations().PointOfInterestEntityUsingCountryId;
		internal static readonly IEntityRelation VattariffEntityUsingCountryIdStatic = new CountryRelations().VattariffEntityUsingCountryId;
		internal static readonly IEntityRelation CurrencyEntityUsingCurrencyIdStatic = new CountryRelations().CurrencyEntityUsingCurrencyId;

		/// <summary>CTor</summary>
		static StaticCountryRelations()
		{
		}
	}
}
