﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: NavigationMenuItem. </summary>
	public partial class NavigationMenuItemRelations
	{
		/// <summary>CTor</summary>
		public NavigationMenuItemRelations()
		{
		}

		/// <summary>Gets all relations of the NavigationMenuItemEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CustomTextEntityUsingNavigationMenuItemId);
			toReturn.Add(this.ActionEntityUsingActionId);
			toReturn.Add(this.NavigationMenuEntityUsingNavigationMenuId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between NavigationMenuItemEntity and CustomTextEntity over the 1:n relation they have, using the relation between the fields:
		/// NavigationMenuItem.NavigationMenuItemId - CustomText.NavigationMenuItemId
		/// </summary>
		public virtual IEntityRelation CustomTextEntityUsingNavigationMenuItemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomTextCollection" , true);
				relation.AddEntityFieldPair(NavigationMenuItemFields.NavigationMenuItemId, CustomTextFields.NavigationMenuItemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NavigationMenuItemEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between NavigationMenuItemEntity and ActionEntity over the m:1 relation they have, using the relation between the fields:
		/// NavigationMenuItem.ActionId - Action.ActionId
		/// </summary>
		public virtual IEntityRelation ActionEntityUsingActionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ActionEntity", false);
				relation.AddEntityFieldPair(ActionFields.ActionId, NavigationMenuItemFields.ActionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ActionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NavigationMenuItemEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between NavigationMenuItemEntity and NavigationMenuEntity over the m:1 relation they have, using the relation between the fields:
		/// NavigationMenuItem.NavigationMenuId - NavigationMenu.NavigationMenuId
		/// </summary>
		public virtual IEntityRelation NavigationMenuEntityUsingNavigationMenuId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "NavigationMenuEntity", false);
				relation.AddEntityFieldPair(NavigationMenuFields.NavigationMenuId, NavigationMenuItemFields.NavigationMenuId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NavigationMenuEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NavigationMenuItemEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticNavigationMenuItemRelations
	{
		internal static readonly IEntityRelation CustomTextEntityUsingNavigationMenuItemIdStatic = new NavigationMenuItemRelations().CustomTextEntityUsingNavigationMenuItemId;
		internal static readonly IEntityRelation ActionEntityUsingActionIdStatic = new NavigationMenuItemRelations().ActionEntityUsingActionId;
		internal static readonly IEntityRelation NavigationMenuEntityUsingNavigationMenuIdStatic = new NavigationMenuItemRelations().NavigationMenuEntityUsingNavigationMenuId;

		/// <summary>CTor</summary>
		static StaticNavigationMenuItemRelations()
		{
		}
	}
}
