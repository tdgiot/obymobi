﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ReceiptTemplate. </summary>
	public partial class ReceiptTemplateRelations
	{
		/// <summary>CTor</summary>
		public ReceiptTemplateRelations()
		{
		}

		/// <summary>Gets all relations of the ReceiptTemplateEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CheckoutMethodEntityUsingReceiptTemplateId);
			toReturn.Add(this.ReceiptEntityUsingReceiptTemplateId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ReceiptTemplateEntity and CheckoutMethodEntity over the 1:n relation they have, using the relation between the fields:
		/// ReceiptTemplate.ReceiptTemplateId - CheckoutMethod.ReceiptTemplateId
		/// </summary>
		public virtual IEntityRelation CheckoutMethodEntityUsingReceiptTemplateId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CheckoutMethodCollection" , true);
				relation.AddEntityFieldPair(ReceiptTemplateFields.ReceiptTemplateId, CheckoutMethodFields.ReceiptTemplateId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReceiptTemplateEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CheckoutMethodEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ReceiptTemplateEntity and ReceiptEntity over the 1:n relation they have, using the relation between the fields:
		/// ReceiptTemplate.ReceiptTemplateId - Receipt.ReceiptTemplateId
		/// </summary>
		public virtual IEntityRelation ReceiptEntityUsingReceiptTemplateId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ReceiptCollection" , true);
				relation.AddEntityFieldPair(ReceiptTemplateFields.ReceiptTemplateId, ReceiptFields.ReceiptTemplateId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReceiptTemplateEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReceiptEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between ReceiptTemplateEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// ReceiptTemplate.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, ReceiptTemplateFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReceiptTemplateEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticReceiptTemplateRelations
	{
		internal static readonly IEntityRelation CheckoutMethodEntityUsingReceiptTemplateIdStatic = new ReceiptTemplateRelations().CheckoutMethodEntityUsingReceiptTemplateId;
		internal static readonly IEntityRelation ReceiptEntityUsingReceiptTemplateIdStatic = new ReceiptTemplateRelations().ReceiptEntityUsingReceiptTemplateId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new ReceiptTemplateRelations().CompanyEntityUsingCompanyId;

		/// <summary>CTor</summary>
		static StaticReceiptTemplateRelations()
		{
		}
	}
}
