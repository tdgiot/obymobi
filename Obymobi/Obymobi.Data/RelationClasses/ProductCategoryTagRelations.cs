﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ProductCategoryTag. </summary>
	public partial class ProductCategoryTagRelations
	{
		/// <summary>CTor</summary>
		public ProductCategoryTagRelations()
		{
		}

		/// <summary>Gets all relations of the ProductCategoryTagEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.OrderitemTagEntityUsingProductCategoryTagId);
			toReturn.Add(this.CategoryEntityUsingCategoryId);
			toReturn.Add(this.ProductCategoryEntityUsingProductCategoryId);
			toReturn.Add(this.TagEntityUsingTagId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ProductCategoryTagEntity and OrderitemTagEntity over the 1:n relation they have, using the relation between the fields:
		/// ProductCategoryTag.ProductCategoryTagId - OrderitemTag.ProductCategoryTagId
		/// </summary>
		public virtual IEntityRelation OrderitemTagEntityUsingProductCategoryTagId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrderitemTagCollection" , true);
				relation.AddEntityFieldPair(ProductCategoryTagFields.ProductCategoryTagId, OrderitemTagFields.ProductCategoryTagId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductCategoryTagEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderitemTagEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between ProductCategoryTagEntity and CategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// ProductCategoryTag.CategoryId - Category.CategoryId
		/// </summary>
		public virtual IEntityRelation CategoryEntityUsingCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CategoryEntity", false);
				relation.AddEntityFieldPair(CategoryFields.CategoryId, ProductCategoryTagFields.CategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductCategoryTagEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ProductCategoryTagEntity and ProductCategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// ProductCategoryTag.ProductCategoryId - ProductCategory.ProductCategoryId
		/// </summary>
		public virtual IEntityRelation ProductCategoryEntityUsingProductCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ProductCategoryEntity", false);
				relation.AddEntityFieldPair(ProductCategoryFields.ProductCategoryId, ProductCategoryTagFields.ProductCategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductCategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductCategoryTagEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ProductCategoryTagEntity and TagEntity over the m:1 relation they have, using the relation between the fields:
		/// ProductCategoryTag.TagId - Tag.TagId
		/// </summary>
		public virtual IEntityRelation TagEntityUsingTagId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TagEntity", false);
				relation.AddEntityFieldPair(TagFields.TagId, ProductCategoryTagFields.TagId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TagEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductCategoryTagEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticProductCategoryTagRelations
	{
		internal static readonly IEntityRelation OrderitemTagEntityUsingProductCategoryTagIdStatic = new ProductCategoryTagRelations().OrderitemTagEntityUsingProductCategoryTagId;
		internal static readonly IEntityRelation CategoryEntityUsingCategoryIdStatic = new ProductCategoryTagRelations().CategoryEntityUsingCategoryId;
		internal static readonly IEntityRelation ProductCategoryEntityUsingProductCategoryIdStatic = new ProductCategoryTagRelations().ProductCategoryEntityUsingProductCategoryId;
		internal static readonly IEntityRelation TagEntityUsingTagIdStatic = new ProductCategoryTagRelations().TagEntityUsingTagId;

		/// <summary>CTor</summary>
		static StaticProductCategoryTagRelations()
		{
		}
	}
}
