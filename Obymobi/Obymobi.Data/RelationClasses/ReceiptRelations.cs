﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Receipt. </summary>
	public partial class ReceiptRelations
	{
		/// <summary>CTor</summary>
		public ReceiptRelations()
		{
		}

		/// <summary>Gets all relations of the ReceiptEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			toReturn.Add(this.OrderEntityUsingOrderId);
			toReturn.Add(this.OutletSellerInformationEntityUsingOutletSellerInformationId);
			toReturn.Add(this.ReceiptTemplateEntityUsingReceiptTemplateId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between ReceiptEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// Receipt.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, ReceiptFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReceiptEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ReceiptEntity and OrderEntity over the m:1 relation they have, using the relation between the fields:
		/// Receipt.OrderId - Order.OrderId
		/// </summary>
		public virtual IEntityRelation OrderEntityUsingOrderId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "OrderEntity", false);
				relation.AddEntityFieldPair(OrderFields.OrderId, ReceiptFields.OrderId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReceiptEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ReceiptEntity and OutletSellerInformationEntity over the m:1 relation they have, using the relation between the fields:
		/// Receipt.OutletSellerInformationId - OutletSellerInformation.OutletSellerInformationId
		/// </summary>
		public virtual IEntityRelation OutletSellerInformationEntityUsingOutletSellerInformationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "OutletSellerInformationEntity", false);
				relation.AddEntityFieldPair(OutletSellerInformationFields.OutletSellerInformationId, ReceiptFields.OutletSellerInformationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OutletSellerInformationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReceiptEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ReceiptEntity and ReceiptTemplateEntity over the m:1 relation they have, using the relation between the fields:
		/// Receipt.ReceiptTemplateId - ReceiptTemplate.ReceiptTemplateId
		/// </summary>
		public virtual IEntityRelation ReceiptTemplateEntityUsingReceiptTemplateId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ReceiptTemplateEntity", false);
				relation.AddEntityFieldPair(ReceiptTemplateFields.ReceiptTemplateId, ReceiptFields.ReceiptTemplateId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReceiptTemplateEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReceiptEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticReceiptRelations
	{
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new ReceiptRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation OrderEntityUsingOrderIdStatic = new ReceiptRelations().OrderEntityUsingOrderId;
		internal static readonly IEntityRelation OutletSellerInformationEntityUsingOutletSellerInformationIdStatic = new ReceiptRelations().OutletSellerInformationEntityUsingOutletSellerInformationId;
		internal static readonly IEntityRelation ReceiptTemplateEntityUsingReceiptTemplateIdStatic = new ReceiptRelations().ReceiptTemplateEntityUsingReceiptTemplateId;

		/// <summary>CTor</summary>
		static StaticReceiptRelations()
		{
		}
	}
}
