﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Genericalteration. </summary>
	public partial class GenericalterationRelations
	{
		/// <summary>CTor</summary>
		public GenericalterationRelations()
		{
		}

		/// <summary>Gets all relations of the GenericalterationEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AlterationEntityUsingGenericalterationId);
			toReturn.Add(this.GenericalterationitemEntityUsingGenericalterationId);
			toReturn.Add(this.GenericproductGenericalterationEntityUsingGenericalterationId);
			toReturn.Add(this.BrandEntityUsingBrandId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between GenericalterationEntity and AlterationEntity over the 1:n relation they have, using the relation between the fields:
		/// Genericalteration.GenericalterationId - Alteration.GenericalterationId
		/// </summary>
		public virtual IEntityRelation AlterationEntityUsingGenericalterationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AlterationCollection" , true);
				relation.AddEntityFieldPair(GenericalterationFields.GenericalterationId, AlterationFields.GenericalterationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericalterationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between GenericalterationEntity and GenericalterationitemEntity over the 1:n relation they have, using the relation between the fields:
		/// Genericalteration.GenericalterationId - Genericalterationitem.GenericalterationId
		/// </summary>
		public virtual IEntityRelation GenericalterationitemEntityUsingGenericalterationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "GenericalterationitemCollection" , true);
				relation.AddEntityFieldPair(GenericalterationFields.GenericalterationId, GenericalterationitemFields.GenericalterationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericalterationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericalterationitemEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between GenericalterationEntity and GenericproductGenericalterationEntity over the 1:n relation they have, using the relation between the fields:
		/// Genericalteration.GenericalterationId - GenericproductGenericalteration.GenericalterationId
		/// </summary>
		public virtual IEntityRelation GenericproductGenericalterationEntityUsingGenericalterationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "GenericproductGenericalterationCollection" , true);
				relation.AddEntityFieldPair(GenericalterationFields.GenericalterationId, GenericproductGenericalterationFields.GenericalterationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericalterationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericproductGenericalterationEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between GenericalterationEntity and BrandEntity over the m:1 relation they have, using the relation between the fields:
		/// Genericalteration.BrandId - Brand.BrandId
		/// </summary>
		public virtual IEntityRelation BrandEntityUsingBrandId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "BrandEntity", false);
				relation.AddEntityFieldPair(BrandFields.BrandId, GenericalterationFields.BrandId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BrandEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericalterationEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticGenericalterationRelations
	{
		internal static readonly IEntityRelation AlterationEntityUsingGenericalterationIdStatic = new GenericalterationRelations().AlterationEntityUsingGenericalterationId;
		internal static readonly IEntityRelation GenericalterationitemEntityUsingGenericalterationIdStatic = new GenericalterationRelations().GenericalterationitemEntityUsingGenericalterationId;
		internal static readonly IEntityRelation GenericproductGenericalterationEntityUsingGenericalterationIdStatic = new GenericalterationRelations().GenericproductGenericalterationEntityUsingGenericalterationId;
		internal static readonly IEntityRelation BrandEntityUsingBrandIdStatic = new GenericalterationRelations().BrandEntityUsingBrandId;

		/// <summary>CTor</summary>
		static StaticGenericalterationRelations()
		{
		}
	}
}
