﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Genericproduct. </summary>
	public partial class GenericproductRelations
	{
		/// <summary>CTor</summary>
		public GenericproductRelations()
		{
		}

		/// <summary>Gets all relations of the GenericproductEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AdvertisementEntityUsingGenericproductId);
			toReturn.Add(this.AdvertisementTagGenericproductEntityUsingGenericproductId);
			toReturn.Add(this.AttachmentEntityUsingGenericproductId);
			toReturn.Add(this.CustomTextEntityUsingGenericproductId);
			toReturn.Add(this.GenericproductGenericalterationEntityUsingGenericproductId);
			toReturn.Add(this.GenericproductLanguageEntityUsingGenericproductId);
			toReturn.Add(this.MediaEntityUsingGenericproductId);
			toReturn.Add(this.ProductEntityUsingGenericproductId);
			toReturn.Add(this.BrandEntityUsingBrandId);
			toReturn.Add(this.GenericcategoryEntityUsingGenericcategoryId);
			toReturn.Add(this.SupplierEntityUsingSupplierId);
			toReturn.Add(this.VattariffEntityUsingVattariffId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between GenericproductEntity and AdvertisementEntity over the 1:n relation they have, using the relation between the fields:
		/// Genericproduct.GenericproductId - Advertisement.GenericproductId
		/// </summary>
		public virtual IEntityRelation AdvertisementEntityUsingGenericproductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AdvertisementCollection" , true);
				relation.AddEntityFieldPair(GenericproductFields.GenericproductId, AdvertisementFields.GenericproductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericproductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between GenericproductEntity and AdvertisementTagGenericproductEntity over the 1:n relation they have, using the relation between the fields:
		/// Genericproduct.GenericproductId - AdvertisementTagGenericproduct.GenericproductId
		/// </summary>
		public virtual IEntityRelation AdvertisementTagGenericproductEntityUsingGenericproductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AdvertisementTagGenericproductCollection" , true);
				relation.AddEntityFieldPair(GenericproductFields.GenericproductId, AdvertisementTagGenericproductFields.GenericproductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericproductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementTagGenericproductEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between GenericproductEntity and AttachmentEntity over the 1:n relation they have, using the relation between the fields:
		/// Genericproduct.GenericproductId - Attachment.GenericproductId
		/// </summary>
		public virtual IEntityRelation AttachmentEntityUsingGenericproductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AttachmentCollection" , true);
				relation.AddEntityFieldPair(GenericproductFields.GenericproductId, AttachmentFields.GenericproductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericproductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttachmentEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between GenericproductEntity and CustomTextEntity over the 1:n relation they have, using the relation between the fields:
		/// Genericproduct.GenericproductId - CustomText.GenericproductId
		/// </summary>
		public virtual IEntityRelation CustomTextEntityUsingGenericproductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomTextCollection" , true);
				relation.AddEntityFieldPair(GenericproductFields.GenericproductId, CustomTextFields.GenericproductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericproductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between GenericproductEntity and GenericproductGenericalterationEntity over the 1:n relation they have, using the relation between the fields:
		/// Genericproduct.GenericproductId - GenericproductGenericalteration.GenericproductId
		/// </summary>
		public virtual IEntityRelation GenericproductGenericalterationEntityUsingGenericproductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "GenericproductGenericalterationCollection" , true);
				relation.AddEntityFieldPair(GenericproductFields.GenericproductId, GenericproductGenericalterationFields.GenericproductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericproductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericproductGenericalterationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between GenericproductEntity and GenericproductLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Genericproduct.GenericproductId - GenericproductLanguage.GenericproductId
		/// </summary>
		public virtual IEntityRelation GenericproductLanguageEntityUsingGenericproductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "GenericproductLanguageCollection" , true);
				relation.AddEntityFieldPair(GenericproductFields.GenericproductId, GenericproductLanguageFields.GenericproductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericproductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericproductLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between GenericproductEntity and MediaEntity over the 1:n relation they have, using the relation between the fields:
		/// Genericproduct.GenericproductId - Media.GenericproductId
		/// </summary>
		public virtual IEntityRelation MediaEntityUsingGenericproductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MediaCollection" , true);
				relation.AddEntityFieldPair(GenericproductFields.GenericproductId, MediaFields.GenericproductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericproductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between GenericproductEntity and ProductEntity over the 1:n relation they have, using the relation between the fields:
		/// Genericproduct.GenericproductId - Product.GenericproductId
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingGenericproductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ProductCollection" , true);
				relation.AddEntityFieldPair(GenericproductFields.GenericproductId, ProductFields.GenericproductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericproductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between GenericproductEntity and BrandEntity over the m:1 relation they have, using the relation between the fields:
		/// Genericproduct.BrandId - Brand.BrandId
		/// </summary>
		public virtual IEntityRelation BrandEntityUsingBrandId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "BrandEntity", false);
				relation.AddEntityFieldPair(BrandFields.BrandId, GenericproductFields.BrandId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BrandEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericproductEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between GenericproductEntity and GenericcategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// Genericproduct.GenericcategoryId - Genericcategory.GenericcategoryId
		/// </summary>
		public virtual IEntityRelation GenericcategoryEntityUsingGenericcategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "GenericcategoryEntity", false);
				relation.AddEntityFieldPair(GenericcategoryFields.GenericcategoryId, GenericproductFields.GenericcategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericcategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericproductEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between GenericproductEntity and SupplierEntity over the m:1 relation they have, using the relation between the fields:
		/// Genericproduct.SupplierId - Supplier.SupplierId
		/// </summary>
		public virtual IEntityRelation SupplierEntityUsingSupplierId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SupplierEntity", false);
				relation.AddEntityFieldPair(SupplierFields.SupplierId, GenericproductFields.SupplierId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SupplierEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericproductEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between GenericproductEntity and VattariffEntity over the m:1 relation they have, using the relation between the fields:
		/// Genericproduct.VattariffId - Vattariff.VattariffId
		/// </summary>
		public virtual IEntityRelation VattariffEntityUsingVattariffId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "VattariffEntity", false);
				relation.AddEntityFieldPair(VattariffFields.VattariffId, GenericproductFields.VattariffId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VattariffEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericproductEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticGenericproductRelations
	{
		internal static readonly IEntityRelation AdvertisementEntityUsingGenericproductIdStatic = new GenericproductRelations().AdvertisementEntityUsingGenericproductId;
		internal static readonly IEntityRelation AdvertisementTagGenericproductEntityUsingGenericproductIdStatic = new GenericproductRelations().AdvertisementTagGenericproductEntityUsingGenericproductId;
		internal static readonly IEntityRelation AttachmentEntityUsingGenericproductIdStatic = new GenericproductRelations().AttachmentEntityUsingGenericproductId;
		internal static readonly IEntityRelation CustomTextEntityUsingGenericproductIdStatic = new GenericproductRelations().CustomTextEntityUsingGenericproductId;
		internal static readonly IEntityRelation GenericproductGenericalterationEntityUsingGenericproductIdStatic = new GenericproductRelations().GenericproductGenericalterationEntityUsingGenericproductId;
		internal static readonly IEntityRelation GenericproductLanguageEntityUsingGenericproductIdStatic = new GenericproductRelations().GenericproductLanguageEntityUsingGenericproductId;
		internal static readonly IEntityRelation MediaEntityUsingGenericproductIdStatic = new GenericproductRelations().MediaEntityUsingGenericproductId;
		internal static readonly IEntityRelation ProductEntityUsingGenericproductIdStatic = new GenericproductRelations().ProductEntityUsingGenericproductId;
		internal static readonly IEntityRelation BrandEntityUsingBrandIdStatic = new GenericproductRelations().BrandEntityUsingBrandId;
		internal static readonly IEntityRelation GenericcategoryEntityUsingGenericcategoryIdStatic = new GenericproductRelations().GenericcategoryEntityUsingGenericcategoryId;
		internal static readonly IEntityRelation SupplierEntityUsingSupplierIdStatic = new GenericproductRelations().SupplierEntityUsingSupplierId;
		internal static readonly IEntityRelation VattariffEntityUsingVattariffIdStatic = new GenericproductRelations().VattariffEntityUsingVattariffId;

		/// <summary>CTor</summary>
		static StaticGenericproductRelations()
		{
		}
	}
}
