﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ClientConfiguration. </summary>
	public partial class ClientConfigurationRelations
	{
		/// <summary>CTor</summary>
		public ClientConfigurationRelations()
		{
		}

		/// <summary>Gets all relations of the ClientConfigurationEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ClientConfigurationRouteEntityUsingClientConfigurationId);
			toReturn.Add(this.CustomTextEntityUsingClientConfigurationId);
			toReturn.Add(this.DeliverypointEntityUsingClientConfigurationId);
			toReturn.Add(this.DeliverypointgroupEntityUsingClientConfigurationId);
			toReturn.Add(this.MediaEntityUsingClientConfigurationId);
			toReturn.Add(this.PmsActionRuleEntityUsingClientConfigurationId);
			toReturn.Add(this.AdvertisementConfigurationEntityUsingAdvertisementConfigurationId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			toReturn.Add(this.DeliverypointgroupEntityUsingDeliverypointgroupId);
			toReturn.Add(this.EntertainmentConfigurationEntityUsingEntertainmentConfigurationId);
			toReturn.Add(this.MenuEntityUsingMenuId);
			toReturn.Add(this.PriceScheduleEntityUsingPriceScheduleId);
			toReturn.Add(this.RoomControlConfigurationEntityUsingRoomControlConfigurationId);
			toReturn.Add(this.UIModeEntityUsingUIModeId);
			toReturn.Add(this.UIScheduleEntityUsingUIScheduleId);
			toReturn.Add(this.UIThemeEntityUsingUIThemeId);
			toReturn.Add(this.WifiConfigurationEntityUsingWifiConfigurationId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ClientConfigurationEntity and ClientConfigurationRouteEntity over the 1:n relation they have, using the relation between the fields:
		/// ClientConfiguration.ClientConfigurationId - ClientConfigurationRoute.ClientConfigurationId
		/// </summary>
		public virtual IEntityRelation ClientConfigurationRouteEntityUsingClientConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ClientConfigurationRouteCollection" , true);
				relation.AddEntityFieldPair(ClientConfigurationFields.ClientConfigurationId, ClientConfigurationRouteFields.ClientConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientConfigurationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientConfigurationRouteEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientConfigurationEntity and CustomTextEntity over the 1:n relation they have, using the relation between the fields:
		/// ClientConfiguration.ClientConfigurationId - CustomText.ClientConfigurationId
		/// </summary>
		public virtual IEntityRelation CustomTextEntityUsingClientConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomTextCollection" , true);
				relation.AddEntityFieldPair(ClientConfigurationFields.ClientConfigurationId, CustomTextFields.ClientConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientConfigurationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientConfigurationEntity and DeliverypointEntity over the 1:n relation they have, using the relation between the fields:
		/// ClientConfiguration.ClientConfigurationId - Deliverypoint.ClientConfigurationId
		/// </summary>
		public virtual IEntityRelation DeliverypointEntityUsingClientConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeliverypointCollection" , true);
				relation.AddEntityFieldPair(ClientConfigurationFields.ClientConfigurationId, DeliverypointFields.ClientConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientConfigurationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientConfigurationEntity and DeliverypointgroupEntity over the 1:n relation they have, using the relation between the fields:
		/// ClientConfiguration.ClientConfigurationId - Deliverypointgroup.ClientConfigurationId
		/// </summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingClientConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeliverypointgroupCollection" , true);
				relation.AddEntityFieldPair(ClientConfigurationFields.ClientConfigurationId, DeliverypointgroupFields.ClientConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientConfigurationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientConfigurationEntity and MediaEntity over the 1:n relation they have, using the relation between the fields:
		/// ClientConfiguration.ClientConfigurationId - Media.ClientConfigurationId
		/// </summary>
		public virtual IEntityRelation MediaEntityUsingClientConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MediaCollection" , true);
				relation.AddEntityFieldPair(ClientConfigurationFields.ClientConfigurationId, MediaFields.ClientConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientConfigurationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientConfigurationEntity and PmsActionRuleEntity over the 1:n relation they have, using the relation between the fields:
		/// ClientConfiguration.ClientConfigurationId - PmsActionRule.ClientConfigurationId
		/// </summary>
		public virtual IEntityRelation PmsActionRuleEntityUsingClientConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PmsActionRuleCollection" , true);
				relation.AddEntityFieldPair(ClientConfigurationFields.ClientConfigurationId, PmsActionRuleFields.ClientConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientConfigurationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PmsActionRuleEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between ClientConfigurationEntity and AdvertisementConfigurationEntity over the m:1 relation they have, using the relation between the fields:
		/// ClientConfiguration.AdvertisementConfigurationId - AdvertisementConfiguration.AdvertisementConfigurationId
		/// </summary>
		public virtual IEntityRelation AdvertisementConfigurationEntityUsingAdvertisementConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AdvertisementConfigurationEntity", false);
				relation.AddEntityFieldPair(AdvertisementConfigurationFields.AdvertisementConfigurationId, ClientConfigurationFields.AdvertisementConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementConfigurationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientConfigurationEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ClientConfigurationEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// ClientConfiguration.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, ClientConfigurationFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientConfigurationEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ClientConfigurationEntity and DeliverypointgroupEntity over the m:1 relation they have, using the relation between the fields:
		/// ClientConfiguration.DeliverypointgroupId - Deliverypointgroup.DeliverypointgroupId
		/// </summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingDeliverypointgroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DeliverypointgroupEntity", false);
				relation.AddEntityFieldPair(DeliverypointgroupFields.DeliverypointgroupId, ClientConfigurationFields.DeliverypointgroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientConfigurationEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ClientConfigurationEntity and EntertainmentConfigurationEntity over the m:1 relation they have, using the relation between the fields:
		/// ClientConfiguration.EntertainmentConfigurationId - EntertainmentConfiguration.EntertainmentConfigurationId
		/// </summary>
		public virtual IEntityRelation EntertainmentConfigurationEntityUsingEntertainmentConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "EntertainmentConfigurationEntity", false);
				relation.AddEntityFieldPair(EntertainmentConfigurationFields.EntertainmentConfigurationId, ClientConfigurationFields.EntertainmentConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentConfigurationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientConfigurationEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ClientConfigurationEntity and MenuEntity over the m:1 relation they have, using the relation between the fields:
		/// ClientConfiguration.MenuId - Menu.MenuId
		/// </summary>
		public virtual IEntityRelation MenuEntityUsingMenuId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "MenuEntity", false);
				relation.AddEntityFieldPair(MenuFields.MenuId, ClientConfigurationFields.MenuId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MenuEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientConfigurationEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ClientConfigurationEntity and PriceScheduleEntity over the m:1 relation they have, using the relation between the fields:
		/// ClientConfiguration.PriceScheduleId - PriceSchedule.PriceScheduleId
		/// </summary>
		public virtual IEntityRelation PriceScheduleEntityUsingPriceScheduleId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PriceScheduleEntity", false);
				relation.AddEntityFieldPair(PriceScheduleFields.PriceScheduleId, ClientConfigurationFields.PriceScheduleId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PriceScheduleEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientConfigurationEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ClientConfigurationEntity and RoomControlConfigurationEntity over the m:1 relation they have, using the relation between the fields:
		/// ClientConfiguration.RoomControlConfigurationId - RoomControlConfiguration.RoomControlConfigurationId
		/// </summary>
		public virtual IEntityRelation RoomControlConfigurationEntityUsingRoomControlConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RoomControlConfigurationEntity", false);
				relation.AddEntityFieldPair(RoomControlConfigurationFields.RoomControlConfigurationId, ClientConfigurationFields.RoomControlConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlConfigurationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientConfigurationEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ClientConfigurationEntity and UIModeEntity over the m:1 relation they have, using the relation between the fields:
		/// ClientConfiguration.UIModeId - UIMode.UIModeId
		/// </summary>
		public virtual IEntityRelation UIModeEntityUsingUIModeId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UIModeEntity", false);
				relation.AddEntityFieldPair(UIModeFields.UIModeId, ClientConfigurationFields.UIModeId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIModeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientConfigurationEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ClientConfigurationEntity and UIScheduleEntity over the m:1 relation they have, using the relation between the fields:
		/// ClientConfiguration.UIScheduleId - UISchedule.UIScheduleId
		/// </summary>
		public virtual IEntityRelation UIScheduleEntityUsingUIScheduleId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UIScheduleEntity", false);
				relation.AddEntityFieldPair(UIScheduleFields.UIScheduleId, ClientConfigurationFields.UIScheduleId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIScheduleEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientConfigurationEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ClientConfigurationEntity and UIThemeEntity over the m:1 relation they have, using the relation between the fields:
		/// ClientConfiguration.UIThemeId - UITheme.UIThemeId
		/// </summary>
		public virtual IEntityRelation UIThemeEntityUsingUIThemeId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UIThemeEntity", false);
				relation.AddEntityFieldPair(UIThemeFields.UIThemeId, ClientConfigurationFields.UIThemeId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIThemeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientConfigurationEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ClientConfigurationEntity and WifiConfigurationEntity over the m:1 relation they have, using the relation between the fields:
		/// ClientConfiguration.WifiConfigurationId - WifiConfiguration.WifiConfigurationId
		/// </summary>
		public virtual IEntityRelation WifiConfigurationEntityUsingWifiConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "WifiConfigurationEntity", false);
				relation.AddEntityFieldPair(WifiConfigurationFields.WifiConfigurationId, ClientConfigurationFields.WifiConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WifiConfigurationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientConfigurationEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticClientConfigurationRelations
	{
		internal static readonly IEntityRelation ClientConfigurationRouteEntityUsingClientConfigurationIdStatic = new ClientConfigurationRelations().ClientConfigurationRouteEntityUsingClientConfigurationId;
		internal static readonly IEntityRelation CustomTextEntityUsingClientConfigurationIdStatic = new ClientConfigurationRelations().CustomTextEntityUsingClientConfigurationId;
		internal static readonly IEntityRelation DeliverypointEntityUsingClientConfigurationIdStatic = new ClientConfigurationRelations().DeliverypointEntityUsingClientConfigurationId;
		internal static readonly IEntityRelation DeliverypointgroupEntityUsingClientConfigurationIdStatic = new ClientConfigurationRelations().DeliverypointgroupEntityUsingClientConfigurationId;
		internal static readonly IEntityRelation MediaEntityUsingClientConfigurationIdStatic = new ClientConfigurationRelations().MediaEntityUsingClientConfigurationId;
		internal static readonly IEntityRelation PmsActionRuleEntityUsingClientConfigurationIdStatic = new ClientConfigurationRelations().PmsActionRuleEntityUsingClientConfigurationId;
		internal static readonly IEntityRelation AdvertisementConfigurationEntityUsingAdvertisementConfigurationIdStatic = new ClientConfigurationRelations().AdvertisementConfigurationEntityUsingAdvertisementConfigurationId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new ClientConfigurationRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation DeliverypointgroupEntityUsingDeliverypointgroupIdStatic = new ClientConfigurationRelations().DeliverypointgroupEntityUsingDeliverypointgroupId;
		internal static readonly IEntityRelation EntertainmentConfigurationEntityUsingEntertainmentConfigurationIdStatic = new ClientConfigurationRelations().EntertainmentConfigurationEntityUsingEntertainmentConfigurationId;
		internal static readonly IEntityRelation MenuEntityUsingMenuIdStatic = new ClientConfigurationRelations().MenuEntityUsingMenuId;
		internal static readonly IEntityRelation PriceScheduleEntityUsingPriceScheduleIdStatic = new ClientConfigurationRelations().PriceScheduleEntityUsingPriceScheduleId;
		internal static readonly IEntityRelation RoomControlConfigurationEntityUsingRoomControlConfigurationIdStatic = new ClientConfigurationRelations().RoomControlConfigurationEntityUsingRoomControlConfigurationId;
		internal static readonly IEntityRelation UIModeEntityUsingUIModeIdStatic = new ClientConfigurationRelations().UIModeEntityUsingUIModeId;
		internal static readonly IEntityRelation UIScheduleEntityUsingUIScheduleIdStatic = new ClientConfigurationRelations().UIScheduleEntityUsingUIScheduleId;
		internal static readonly IEntityRelation UIThemeEntityUsingUIThemeIdStatic = new ClientConfigurationRelations().UIThemeEntityUsingUIThemeId;
		internal static readonly IEntityRelation WifiConfigurationEntityUsingWifiConfigurationIdStatic = new ClientConfigurationRelations().WifiConfigurationEntityUsingWifiConfigurationId;

		/// <summary>CTor</summary>
		static StaticClientConfigurationRelations()
		{
		}
	}
}
