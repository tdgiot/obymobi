﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: MediaProcessingTask. </summary>
	public partial class MediaProcessingTaskRelations
	{
		/// <summary>CTor</summary>
		public MediaProcessingTaskRelations()
		{
		}

		/// <summary>Gets all relations of the MediaProcessingTaskEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.MediaRatioTypeMediaEntityUsingMediaRatioTypeMediaId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between MediaProcessingTaskEntity and MediaRatioTypeMediaEntity over the m:1 relation they have, using the relation between the fields:
		/// MediaProcessingTask.MediaRatioTypeMediaId - MediaRatioTypeMedia.MediaRatioTypeMediaId
		/// </summary>
		public virtual IEntityRelation MediaRatioTypeMediaEntityUsingMediaRatioTypeMediaId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "MediaRatioTypeMediaEntity", false);
				relation.AddEntityFieldPair(MediaRatioTypeMediaFields.MediaRatioTypeMediaId, MediaProcessingTaskFields.MediaRatioTypeMediaId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaRatioTypeMediaEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaProcessingTaskEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticMediaProcessingTaskRelations
	{
		internal static readonly IEntityRelation MediaRatioTypeMediaEntityUsingMediaRatioTypeMediaIdStatic = new MediaProcessingTaskRelations().MediaRatioTypeMediaEntityUsingMediaRatioTypeMediaId;

		/// <summary>CTor</summary>
		static StaticMediaProcessingTaskRelations()
		{
		}
	}
}
