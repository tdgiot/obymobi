﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Page. </summary>
	public partial class PageRelations
	{
		/// <summary>CTor</summary>
		public PageRelations()
		{
		}

		/// <summary>Gets all relations of the PageEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AdvertisementEntityUsingActionPageId);
			toReturn.Add(this.AttachmentEntityUsingPageId);
			toReturn.Add(this.AvailabilityEntityUsingActionPageId);
			toReturn.Add(this.CustomTextEntityUsingPageId);
			toReturn.Add(this.MediaEntityUsingPageId);
			toReturn.Add(this.MediaEntityUsingActionPageId);
			toReturn.Add(this.MessageEntityUsingPageId);
			toReturn.Add(this.MessageTemplateEntityUsingPageId);
			toReturn.Add(this.PageEntityUsingParentPageId);
			toReturn.Add(this.PageElementEntityUsingPageId);
			toReturn.Add(this.PageLanguageEntityUsingPageId);
			toReturn.Add(this.ScheduledMessageEntityUsingPageId);
			toReturn.Add(this.UIWidgetEntityUsingPageId);
			toReturn.Add(this.PageEntityUsingPageIdParentPageId);
			toReturn.Add(this.PageTemplateEntityUsingPageTemplateId);
			toReturn.Add(this.SiteEntityUsingSiteId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between PageEntity and AdvertisementEntity over the 1:n relation they have, using the relation between the fields:
		/// Page.PageId - Advertisement.ActionPageId
		/// </summary>
		public virtual IEntityRelation AdvertisementEntityUsingActionPageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AdvertisementCollection" , true);
				relation.AddEntityFieldPair(PageFields.PageId, AdvertisementFields.ActionPageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PageEntity and AttachmentEntity over the 1:n relation they have, using the relation between the fields:
		/// Page.PageId - Attachment.PageId
		/// </summary>
		public virtual IEntityRelation AttachmentEntityUsingPageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AttachmentCollection" , true);
				relation.AddEntityFieldPair(PageFields.PageId, AttachmentFields.PageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttachmentEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PageEntity and AvailabilityEntity over the 1:n relation they have, using the relation between the fields:
		/// Page.PageId - Availability.ActionPageId
		/// </summary>
		public virtual IEntityRelation AvailabilityEntityUsingActionPageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AvailabilityCollection" , true);
				relation.AddEntityFieldPair(PageFields.PageId, AvailabilityFields.ActionPageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AvailabilityEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PageEntity and CustomTextEntity over the 1:n relation they have, using the relation between the fields:
		/// Page.PageId - CustomText.PageId
		/// </summary>
		public virtual IEntityRelation CustomTextEntityUsingPageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomTextCollection" , true);
				relation.AddEntityFieldPair(PageFields.PageId, CustomTextFields.PageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PageEntity and MediaEntity over the 1:n relation they have, using the relation between the fields:
		/// Page.PageId - Media.PageId
		/// </summary>
		public virtual IEntityRelation MediaEntityUsingPageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MediaCollection" , true);
				relation.AddEntityFieldPair(PageFields.PageId, MediaFields.PageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PageEntity and MediaEntity over the 1:n relation they have, using the relation between the fields:
		/// Page.PageId - Media.ActionPageId
		/// </summary>
		public virtual IEntityRelation MediaEntityUsingActionPageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MediaCollection_" , true);
				relation.AddEntityFieldPair(PageFields.PageId, MediaFields.ActionPageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PageEntity and MessageEntity over the 1:n relation they have, using the relation between the fields:
		/// Page.PageId - Message.PageId
		/// </summary>
		public virtual IEntityRelation MessageEntityUsingPageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MessageCollection" , true);
				relation.AddEntityFieldPair(PageFields.PageId, MessageFields.PageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PageEntity and MessageTemplateEntity over the 1:n relation they have, using the relation between the fields:
		/// Page.PageId - MessageTemplate.PageId
		/// </summary>
		public virtual IEntityRelation MessageTemplateEntityUsingPageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MessageTemplateCollection" , true);
				relation.AddEntityFieldPair(PageFields.PageId, MessageTemplateFields.PageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageTemplateEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PageEntity and PageEntity over the 1:n relation they have, using the relation between the fields:
		/// Page.PageId - Page.ParentPageId
		/// </summary>
		public virtual IEntityRelation PageEntityUsingParentPageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PageCollection" , true);
				relation.AddEntityFieldPair(PageFields.PageId, PageFields.ParentPageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PageEntity and PageElementEntity over the 1:n relation they have, using the relation between the fields:
		/// Page.PageId - PageElement.PageId
		/// </summary>
		public virtual IEntityRelation PageElementEntityUsingPageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PageElementCollection" , true);
				relation.AddEntityFieldPair(PageFields.PageId, PageElementFields.PageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageElementEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PageEntity and PageLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Page.PageId - PageLanguage.PageId
		/// </summary>
		public virtual IEntityRelation PageLanguageEntityUsingPageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PageLanguageCollection" , true);
				relation.AddEntityFieldPair(PageFields.PageId, PageLanguageFields.PageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PageEntity and ScheduledMessageEntity over the 1:n relation they have, using the relation between the fields:
		/// Page.PageId - ScheduledMessage.PageId
		/// </summary>
		public virtual IEntityRelation ScheduledMessageEntityUsingPageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ScheduledMessageCollection" , true);
				relation.AddEntityFieldPair(PageFields.PageId, ScheduledMessageFields.PageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduledMessageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PageEntity and UIWidgetEntity over the 1:n relation they have, using the relation between the fields:
		/// Page.PageId - UIWidget.PageId
		/// </summary>
		public virtual IEntityRelation UIWidgetEntityUsingPageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UIWidgetCollection" , true);
				relation.AddEntityFieldPair(PageFields.PageId, UIWidgetFields.PageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIWidgetEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between PageEntity and PageEntity over the m:1 relation they have, using the relation between the fields:
		/// Page.ParentPageId - Page.PageId
		/// </summary>
		public virtual IEntityRelation PageEntityUsingPageIdParentPageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PageEntity", false);
				relation.AddEntityFieldPair(PageFields.PageId, PageFields.ParentPageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between PageEntity and PageTemplateEntity over the m:1 relation they have, using the relation between the fields:
		/// Page.PageTemplateId - PageTemplate.PageTemplateId
		/// </summary>
		public virtual IEntityRelation PageTemplateEntityUsingPageTemplateId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PageTemplateEntity", false);
				relation.AddEntityFieldPair(PageTemplateFields.PageTemplateId, PageFields.PageTemplateId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageTemplateEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between PageEntity and SiteEntity over the m:1 relation they have, using the relation between the fields:
		/// Page.SiteId - Site.SiteId
		/// </summary>
		public virtual IEntityRelation SiteEntityUsingSiteId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SiteEntity", false);
				relation.AddEntityFieldPair(SiteFields.SiteId, PageFields.SiteId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SiteEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPageRelations
	{
		internal static readonly IEntityRelation AdvertisementEntityUsingActionPageIdStatic = new PageRelations().AdvertisementEntityUsingActionPageId;
		internal static readonly IEntityRelation AttachmentEntityUsingPageIdStatic = new PageRelations().AttachmentEntityUsingPageId;
		internal static readonly IEntityRelation AvailabilityEntityUsingActionPageIdStatic = new PageRelations().AvailabilityEntityUsingActionPageId;
		internal static readonly IEntityRelation CustomTextEntityUsingPageIdStatic = new PageRelations().CustomTextEntityUsingPageId;
		internal static readonly IEntityRelation MediaEntityUsingPageIdStatic = new PageRelations().MediaEntityUsingPageId;
		internal static readonly IEntityRelation MediaEntityUsingActionPageIdStatic = new PageRelations().MediaEntityUsingActionPageId;
		internal static readonly IEntityRelation MessageEntityUsingPageIdStatic = new PageRelations().MessageEntityUsingPageId;
		internal static readonly IEntityRelation MessageTemplateEntityUsingPageIdStatic = new PageRelations().MessageTemplateEntityUsingPageId;
		internal static readonly IEntityRelation PageEntityUsingParentPageIdStatic = new PageRelations().PageEntityUsingParentPageId;
		internal static readonly IEntityRelation PageElementEntityUsingPageIdStatic = new PageRelations().PageElementEntityUsingPageId;
		internal static readonly IEntityRelation PageLanguageEntityUsingPageIdStatic = new PageRelations().PageLanguageEntityUsingPageId;
		internal static readonly IEntityRelation ScheduledMessageEntityUsingPageIdStatic = new PageRelations().ScheduledMessageEntityUsingPageId;
		internal static readonly IEntityRelation UIWidgetEntityUsingPageIdStatic = new PageRelations().UIWidgetEntityUsingPageId;
		internal static readonly IEntityRelation PageEntityUsingPageIdParentPageIdStatic = new PageRelations().PageEntityUsingPageIdParentPageId;
		internal static readonly IEntityRelation PageTemplateEntityUsingPageTemplateIdStatic = new PageRelations().PageTemplateEntityUsingPageTemplateId;
		internal static readonly IEntityRelation SiteEntityUsingSiteIdStatic = new PageRelations().SiteEntityUsingSiteId;

		/// <summary>CTor</summary>
		static StaticPageRelations()
		{
		}
	}
}
