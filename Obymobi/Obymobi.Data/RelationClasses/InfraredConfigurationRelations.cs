﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: InfraredConfiguration. </summary>
	public partial class InfraredConfigurationRelations
	{
		/// <summary>CTor</summary>
		public InfraredConfigurationRelations()
		{
		}

		/// <summary>Gets all relations of the InfraredConfigurationEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.InfraredCommandEntityUsingInfraredConfigurationId);
			toReturn.Add(this.RoomControlComponentEntityUsingInfraredConfigurationId);
			toReturn.Add(this.RoomControlSectionItemEntityUsingInfraredConfigurationId);
			toReturn.Add(this.RoomControlWidgetEntityUsingInfraredConfigurationId);
			toReturn.Add(this.TimestampEntityUsingInfraredConfigurationId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between InfraredConfigurationEntity and InfraredCommandEntity over the 1:n relation they have, using the relation between the fields:
		/// InfraredConfiguration.InfraredConfigurationId - InfraredCommand.InfraredConfigurationId
		/// </summary>
		public virtual IEntityRelation InfraredCommandEntityUsingInfraredConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "InfraredCommandCollection" , true);
				relation.AddEntityFieldPair(InfraredConfigurationFields.InfraredConfigurationId, InfraredCommandFields.InfraredConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InfraredConfigurationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InfraredCommandEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between InfraredConfigurationEntity and RoomControlComponentEntity over the 1:n relation they have, using the relation between the fields:
		/// InfraredConfiguration.InfraredConfigurationId - RoomControlComponent.InfraredConfigurationId
		/// </summary>
		public virtual IEntityRelation RoomControlComponentEntityUsingInfraredConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RoomControlComponentCollection" , true);
				relation.AddEntityFieldPair(InfraredConfigurationFields.InfraredConfigurationId, RoomControlComponentFields.InfraredConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InfraredConfigurationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlComponentEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between InfraredConfigurationEntity and RoomControlSectionItemEntity over the 1:n relation they have, using the relation between the fields:
		/// InfraredConfiguration.InfraredConfigurationId - RoomControlSectionItem.InfraredConfigurationId
		/// </summary>
		public virtual IEntityRelation RoomControlSectionItemEntityUsingInfraredConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RoomControlSectionItemCollection" , true);
				relation.AddEntityFieldPair(InfraredConfigurationFields.InfraredConfigurationId, RoomControlSectionItemFields.InfraredConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InfraredConfigurationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlSectionItemEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between InfraredConfigurationEntity and RoomControlWidgetEntity over the 1:n relation they have, using the relation between the fields:
		/// InfraredConfiguration.InfraredConfigurationId - RoomControlWidget.InfraredConfigurationId
		/// </summary>
		public virtual IEntityRelation RoomControlWidgetEntityUsingInfraredConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RoomControlWidgetCollection" , true);
				relation.AddEntityFieldPair(InfraredConfigurationFields.InfraredConfigurationId, RoomControlWidgetFields.InfraredConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InfraredConfigurationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlWidgetEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between InfraredConfigurationEntity and TimestampEntity over the 1:1 relation they have, using the relation between the fields:
		/// InfraredConfiguration.InfraredConfigurationId - Timestamp.InfraredConfigurationId
		/// </summary>
		public virtual IEntityRelation TimestampEntityUsingInfraredConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "TimestampCollection", true);


				relation.AddEntityFieldPair(InfraredConfigurationFields.InfraredConfigurationId, TimestampFields.InfraredConfigurationId);


				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InfraredConfigurationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TimestampEntity", false);
				return relation;
			}
		}

		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticInfraredConfigurationRelations
	{
		internal static readonly IEntityRelation InfraredCommandEntityUsingInfraredConfigurationIdStatic = new InfraredConfigurationRelations().InfraredCommandEntityUsingInfraredConfigurationId;
		internal static readonly IEntityRelation RoomControlComponentEntityUsingInfraredConfigurationIdStatic = new InfraredConfigurationRelations().RoomControlComponentEntityUsingInfraredConfigurationId;
		internal static readonly IEntityRelation RoomControlSectionItemEntityUsingInfraredConfigurationIdStatic = new InfraredConfigurationRelations().RoomControlSectionItemEntityUsingInfraredConfigurationId;
		internal static readonly IEntityRelation RoomControlWidgetEntityUsingInfraredConfigurationIdStatic = new InfraredConfigurationRelations().RoomControlWidgetEntityUsingInfraredConfigurationId;
		internal static readonly IEntityRelation TimestampEntityUsingInfraredConfigurationIdStatic = new InfraredConfigurationRelations().TimestampEntityUsingInfraredConfigurationId;

		/// <summary>CTor</summary>
		static StaticInfraredConfigurationRelations()
		{
		}
	}
}
