﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ApplicationConfiguration. </summary>
	public partial class ApplicationConfigurationRelations
	{
		/// <summary>CTor</summary>
		public ApplicationConfigurationRelations()
		{
		}

		/// <summary>Gets all relations of the ApplicationConfigurationEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ActionEntityUsingApplicationConfigurationId);
			toReturn.Add(this.FeatureFlagEntityUsingApplicationConfigurationId);
			toReturn.Add(this.LandingPageEntityUsingApplicationConfigurationId);
			toReturn.Add(this.NavigationMenuEntityUsingApplicationConfigurationId);
			toReturn.Add(this.ThemeEntityUsingApplicationConfigurationId);
			toReturn.Add(this.WidgetEntityUsingApplicationConfigurationId);
			toReturn.Add(this.CustomTextEntityUsingApplicationConfigurationId);
			toReturn.Add(this.MediaEntityUsingApplicationConfigurationId);
			toReturn.Add(this.LandingPageEntityUsingLandingPageId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ApplicationConfigurationEntity and ActionEntity over the 1:n relation they have, using the relation between the fields:
		/// ApplicationConfiguration.ApplicationConfigurationId - Action.ApplicationConfigurationId
		/// </summary>
		public virtual IEntityRelation ActionEntityUsingApplicationConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ActionCollection" , true);
				relation.AddEntityFieldPair(ApplicationConfigurationFields.ApplicationConfigurationId, ActionFields.ApplicationConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ApplicationConfigurationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ActionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ApplicationConfigurationEntity and FeatureFlagEntity over the 1:n relation they have, using the relation between the fields:
		/// ApplicationConfiguration.ApplicationConfigurationId - FeatureFlag.ApplicationConfigurationId
		/// </summary>
		public virtual IEntityRelation FeatureFlagEntityUsingApplicationConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FeatureFlagCollection" , true);
				relation.AddEntityFieldPair(ApplicationConfigurationFields.ApplicationConfigurationId, FeatureFlagFields.ApplicationConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ApplicationConfigurationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FeatureFlagEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ApplicationConfigurationEntity and LandingPageEntity over the 1:n relation they have, using the relation between the fields:
		/// ApplicationConfiguration.ApplicationConfigurationId - LandingPage.ApplicationConfigurationId
		/// </summary>
		public virtual IEntityRelation LandingPageEntityUsingApplicationConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "LandingPageCollection" , true);
				relation.AddEntityFieldPair(ApplicationConfigurationFields.ApplicationConfigurationId, LandingPageFields.ApplicationConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ApplicationConfigurationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LandingPageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ApplicationConfigurationEntity and NavigationMenuEntity over the 1:n relation they have, using the relation between the fields:
		/// ApplicationConfiguration.ApplicationConfigurationId - NavigationMenu.ApplicationConfigurationId
		/// </summary>
		public virtual IEntityRelation NavigationMenuEntityUsingApplicationConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "NavigationMenuCollection" , true);
				relation.AddEntityFieldPair(ApplicationConfigurationFields.ApplicationConfigurationId, NavigationMenuFields.ApplicationConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ApplicationConfigurationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NavigationMenuEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ApplicationConfigurationEntity and ThemeEntity over the 1:n relation they have, using the relation between the fields:
		/// ApplicationConfiguration.ApplicationConfigurationId - Theme.ApplicationConfigurationId
		/// </summary>
		public virtual IEntityRelation ThemeEntityUsingApplicationConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ThemeCollection" , true);
				relation.AddEntityFieldPair(ApplicationConfigurationFields.ApplicationConfigurationId, ThemeFields.ApplicationConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ApplicationConfigurationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ThemeEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ApplicationConfigurationEntity and WidgetEntity over the 1:n relation they have, using the relation between the fields:
		/// ApplicationConfiguration.ApplicationConfigurationId - Widget.ApplicationConfigurationId
		/// </summary>
		public virtual IEntityRelation WidgetEntityUsingApplicationConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "WidgetCollection" , true);
				relation.AddEntityFieldPair(ApplicationConfigurationFields.ApplicationConfigurationId, WidgetFields.ApplicationConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ApplicationConfigurationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WidgetEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ApplicationConfigurationEntity and CustomTextEntity over the 1:n relation they have, using the relation between the fields:
		/// ApplicationConfiguration.ApplicationConfigurationId - CustomText.ApplicationConfigurationId
		/// </summary>
		public virtual IEntityRelation CustomTextEntityUsingApplicationConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomTextCollection" , true);
				relation.AddEntityFieldPair(ApplicationConfigurationFields.ApplicationConfigurationId, CustomTextFields.ApplicationConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ApplicationConfigurationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ApplicationConfigurationEntity and MediaEntity over the 1:n relation they have, using the relation between the fields:
		/// ApplicationConfiguration.ApplicationConfigurationId - Media.ApplicationConfigurationId
		/// </summary>
		public virtual IEntityRelation MediaEntityUsingApplicationConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MediaCollection" , true);
				relation.AddEntityFieldPair(ApplicationConfigurationFields.ApplicationConfigurationId, MediaFields.ApplicationConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ApplicationConfigurationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between ApplicationConfigurationEntity and LandingPageEntity over the m:1 relation they have, using the relation between the fields:
		/// ApplicationConfiguration.LandingPageId - LandingPage.LandingPageId
		/// </summary>
		public virtual IEntityRelation LandingPageEntityUsingLandingPageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DefaultLandingPageEntity", false);
				relation.AddEntityFieldPair(LandingPageFields.LandingPageId, ApplicationConfigurationFields.LandingPageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LandingPageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ApplicationConfigurationEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ApplicationConfigurationEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// ApplicationConfiguration.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, ApplicationConfigurationFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ApplicationConfigurationEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticApplicationConfigurationRelations
	{
		internal static readonly IEntityRelation ActionEntityUsingApplicationConfigurationIdStatic = new ApplicationConfigurationRelations().ActionEntityUsingApplicationConfigurationId;
		internal static readonly IEntityRelation FeatureFlagEntityUsingApplicationConfigurationIdStatic = new ApplicationConfigurationRelations().FeatureFlagEntityUsingApplicationConfigurationId;
		internal static readonly IEntityRelation LandingPageEntityUsingApplicationConfigurationIdStatic = new ApplicationConfigurationRelations().LandingPageEntityUsingApplicationConfigurationId;
		internal static readonly IEntityRelation NavigationMenuEntityUsingApplicationConfigurationIdStatic = new ApplicationConfigurationRelations().NavigationMenuEntityUsingApplicationConfigurationId;
		internal static readonly IEntityRelation ThemeEntityUsingApplicationConfigurationIdStatic = new ApplicationConfigurationRelations().ThemeEntityUsingApplicationConfigurationId;
		internal static readonly IEntityRelation WidgetEntityUsingApplicationConfigurationIdStatic = new ApplicationConfigurationRelations().WidgetEntityUsingApplicationConfigurationId;
		internal static readonly IEntityRelation CustomTextEntityUsingApplicationConfigurationIdStatic = new ApplicationConfigurationRelations().CustomTextEntityUsingApplicationConfigurationId;
		internal static readonly IEntityRelation MediaEntityUsingApplicationConfigurationIdStatic = new ApplicationConfigurationRelations().MediaEntityUsingApplicationConfigurationId;
		internal static readonly IEntityRelation LandingPageEntityUsingLandingPageIdStatic = new ApplicationConfigurationRelations().LandingPageEntityUsingLandingPageId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new ApplicationConfigurationRelations().CompanyEntityUsingCompanyId;

		/// <summary>CTor</summary>
		static StaticApplicationConfigurationRelations()
		{
		}
	}
}
