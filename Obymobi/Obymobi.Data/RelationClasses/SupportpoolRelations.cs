﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Supportpool. </summary>
	public partial class SupportpoolRelations
	{
		/// <summary>CTor</summary>
		public SupportpoolRelations()
		{
		}

		/// <summary>Gets all relations of the SupportpoolEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CompanyEntityUsingSupportpoolId);
			toReturn.Add(this.OrderRoutestephandlerEntityUsingSupportpoolId);
			toReturn.Add(this.OrderRoutestephandlerHistoryEntityUsingSupportpoolId);
			toReturn.Add(this.RoutestephandlerEntityUsingSupportpoolId);
			toReturn.Add(this.SupportpoolNotificationRecipientEntityUsingSupportpoolId);
			toReturn.Add(this.SupportpoolSupportagentEntityUsingSupportpoolId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between SupportpoolEntity and CompanyEntity over the 1:n relation they have, using the relation between the fields:
		/// Supportpool.SupportpoolId - Company.SupportpoolId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingSupportpoolId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CompanyCollection" , true);
				relation.AddEntityFieldPair(SupportpoolFields.SupportpoolId, CompanyFields.SupportpoolId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SupportpoolEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SupportpoolEntity and OrderRoutestephandlerEntity over the 1:n relation they have, using the relation between the fields:
		/// Supportpool.SupportpoolId - OrderRoutestephandler.SupportpoolId
		/// </summary>
		public virtual IEntityRelation OrderRoutestephandlerEntityUsingSupportpoolId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrderRoutestephandlerCollection" , true);
				relation.AddEntityFieldPair(SupportpoolFields.SupportpoolId, OrderRoutestephandlerFields.SupportpoolId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SupportpoolEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderRoutestephandlerEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SupportpoolEntity and OrderRoutestephandlerHistoryEntity over the 1:n relation they have, using the relation between the fields:
		/// Supportpool.SupportpoolId - OrderRoutestephandlerHistory.SupportpoolId
		/// </summary>
		public virtual IEntityRelation OrderRoutestephandlerHistoryEntityUsingSupportpoolId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrderRoutestephandlerHistoryCollection" , true);
				relation.AddEntityFieldPair(SupportpoolFields.SupportpoolId, OrderRoutestephandlerHistoryFields.SupportpoolId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SupportpoolEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderRoutestephandlerHistoryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SupportpoolEntity and RoutestephandlerEntity over the 1:n relation they have, using the relation between the fields:
		/// Supportpool.SupportpoolId - Routestephandler.SupportpoolId
		/// </summary>
		public virtual IEntityRelation RoutestephandlerEntityUsingSupportpoolId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RoutestephandlerCollection" , true);
				relation.AddEntityFieldPair(SupportpoolFields.SupportpoolId, RoutestephandlerFields.SupportpoolId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SupportpoolEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoutestephandlerEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SupportpoolEntity and SupportpoolNotificationRecipientEntity over the 1:n relation they have, using the relation between the fields:
		/// Supportpool.SupportpoolId - SupportpoolNotificationRecipient.SupportpoolId
		/// </summary>
		public virtual IEntityRelation SupportpoolNotificationRecipientEntityUsingSupportpoolId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SupportpoolNotificationRecipientCollection" , true);
				relation.AddEntityFieldPair(SupportpoolFields.SupportpoolId, SupportpoolNotificationRecipientFields.SupportpoolId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SupportpoolEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SupportpoolNotificationRecipientEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SupportpoolEntity and SupportpoolSupportagentEntity over the 1:n relation they have, using the relation between the fields:
		/// Supportpool.SupportpoolId - SupportpoolSupportagent.SupportpoolId
		/// </summary>
		public virtual IEntityRelation SupportpoolSupportagentEntityUsingSupportpoolId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SupportpoolSupportagentCollection" , true);
				relation.AddEntityFieldPair(SupportpoolFields.SupportpoolId, SupportpoolSupportagentFields.SupportpoolId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SupportpoolEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SupportpoolSupportagentEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticSupportpoolRelations
	{
		internal static readonly IEntityRelation CompanyEntityUsingSupportpoolIdStatic = new SupportpoolRelations().CompanyEntityUsingSupportpoolId;
		internal static readonly IEntityRelation OrderRoutestephandlerEntityUsingSupportpoolIdStatic = new SupportpoolRelations().OrderRoutestephandlerEntityUsingSupportpoolId;
		internal static readonly IEntityRelation OrderRoutestephandlerHistoryEntityUsingSupportpoolIdStatic = new SupportpoolRelations().OrderRoutestephandlerHistoryEntityUsingSupportpoolId;
		internal static readonly IEntityRelation RoutestephandlerEntityUsingSupportpoolIdStatic = new SupportpoolRelations().RoutestephandlerEntityUsingSupportpoolId;
		internal static readonly IEntityRelation SupportpoolNotificationRecipientEntityUsingSupportpoolIdStatic = new SupportpoolRelations().SupportpoolNotificationRecipientEntityUsingSupportpoolId;
		internal static readonly IEntityRelation SupportpoolSupportagentEntityUsingSupportpoolIdStatic = new SupportpoolRelations().SupportpoolSupportagentEntityUsingSupportpoolId;

		/// <summary>CTor</summary>
		static StaticSupportpoolRelations()
		{
		}
	}
}
