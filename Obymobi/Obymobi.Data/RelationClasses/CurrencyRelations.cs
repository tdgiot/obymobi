﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Currency. </summary>
	public partial class CurrencyRelations
	{
		/// <summary>CTor</summary>
		public CurrencyRelations()
		{
		}

		/// <summary>Gets all relations of the CurrencyEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CompanyEntityUsingCurrencyId);
			toReturn.Add(this.CountryEntityUsingCurrencyId);
			toReturn.Add(this.OrderEntityUsingCurrencyId);
			toReturn.Add(this.PointOfInterestEntityUsingCurrencyId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between CurrencyEntity and CompanyEntity over the 1:n relation they have, using the relation between the fields:
		/// Currency.CurrencyId - Company.CurrencyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCurrencyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CompanyCollection" , true);
				relation.AddEntityFieldPair(CurrencyFields.CurrencyId, CompanyFields.CurrencyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CurrencyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CurrencyEntity and CountryEntity over the 1:n relation they have, using the relation between the fields:
		/// Currency.CurrencyId - Country.CurrencyId
		/// </summary>
		public virtual IEntityRelation CountryEntityUsingCurrencyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CountryCollection" , true);
				relation.AddEntityFieldPair(CurrencyFields.CurrencyId, CountryFields.CurrencyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CurrencyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CountryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CurrencyEntity and OrderEntity over the 1:n relation they have, using the relation between the fields:
		/// Currency.CurrencyId - Order.CurrencyId
		/// </summary>
		public virtual IEntityRelation OrderEntityUsingCurrencyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrderCollection" , true);
				relation.AddEntityFieldPair(CurrencyFields.CurrencyId, OrderFields.CurrencyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CurrencyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CurrencyEntity and PointOfInterestEntity over the 1:n relation they have, using the relation between the fields:
		/// Currency.CurrencyId - PointOfInterest.CurrencyId
		/// </summary>
		public virtual IEntityRelation PointOfInterestEntityUsingCurrencyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PointOfInterestCollection" , true);
				relation.AddEntityFieldPair(CurrencyFields.CurrencyId, PointOfInterestFields.CurrencyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CurrencyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PointOfInterestEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCurrencyRelations
	{
		internal static readonly IEntityRelation CompanyEntityUsingCurrencyIdStatic = new CurrencyRelations().CompanyEntityUsingCurrencyId;
		internal static readonly IEntityRelation CountryEntityUsingCurrencyIdStatic = new CurrencyRelations().CountryEntityUsingCurrencyId;
		internal static readonly IEntityRelation OrderEntityUsingCurrencyIdStatic = new CurrencyRelations().OrderEntityUsingCurrencyId;
		internal static readonly IEntityRelation PointOfInterestEntityUsingCurrencyIdStatic = new CurrencyRelations().PointOfInterestEntityUsingCurrencyId;

		/// <summary>CTor</summary>
		static StaticCurrencyRelations()
		{
		}
	}
}
