﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: PmsReportConfiguration. </summary>
	public partial class PmsReportConfigurationRelations
	{
		/// <summary>CTor</summary>
		public PmsReportConfigurationRelations()
		{
		}

		/// <summary>Gets all relations of the PmsReportConfigurationEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.PmsActionRuleEntityUsingPmsReportConfigurationId);
			toReturn.Add(this.PmsReportColumnEntityUsingPmsReportConfigurationId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			toReturn.Add(this.PmsReportColumnEntityUsingRoomNumberColumnId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between PmsReportConfigurationEntity and PmsActionRuleEntity over the 1:n relation they have, using the relation between the fields:
		/// PmsReportConfiguration.PmsReportConfigurationId - PmsActionRule.PmsReportConfigurationId
		/// </summary>
		public virtual IEntityRelation PmsActionRuleEntityUsingPmsReportConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PmsActionRuleCollection" , true);
				relation.AddEntityFieldPair(PmsReportConfigurationFields.PmsReportConfigurationId, PmsActionRuleFields.PmsReportConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PmsReportConfigurationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PmsActionRuleEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PmsReportConfigurationEntity and PmsReportColumnEntity over the 1:n relation they have, using the relation between the fields:
		/// PmsReportConfiguration.PmsReportConfigurationId - PmsReportColumn.PmsReportConfigurationId
		/// </summary>
		public virtual IEntityRelation PmsReportColumnEntityUsingPmsReportConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PmsReportColumnCollection" , true);
				relation.AddEntityFieldPair(PmsReportConfigurationFields.PmsReportConfigurationId, PmsReportColumnFields.PmsReportConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PmsReportConfigurationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PmsReportColumnEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between PmsReportConfigurationEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// PmsReportConfiguration.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, PmsReportConfigurationFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PmsReportConfigurationEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between PmsReportConfigurationEntity and PmsReportColumnEntity over the m:1 relation they have, using the relation between the fields:
		/// PmsReportConfiguration.RoomNumberColumnId - PmsReportColumn.PmsReportColumnId
		/// </summary>
		public virtual IEntityRelation PmsReportColumnEntityUsingRoomNumberColumnId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PmsReportColumnEntity", false);
				relation.AddEntityFieldPair(PmsReportColumnFields.PmsReportColumnId, PmsReportConfigurationFields.RoomNumberColumnId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PmsReportColumnEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PmsReportConfigurationEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPmsReportConfigurationRelations
	{
		internal static readonly IEntityRelation PmsActionRuleEntityUsingPmsReportConfigurationIdStatic = new PmsReportConfigurationRelations().PmsActionRuleEntityUsingPmsReportConfigurationId;
		internal static readonly IEntityRelation PmsReportColumnEntityUsingPmsReportConfigurationIdStatic = new PmsReportConfigurationRelations().PmsReportColumnEntityUsingPmsReportConfigurationId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new PmsReportConfigurationRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation PmsReportColumnEntityUsingRoomNumberColumnIdStatic = new PmsReportConfigurationRelations().PmsReportColumnEntityUsingRoomNumberColumnId;

		/// <summary>CTor</summary>
		static StaticPmsReportConfigurationRelations()
		{
		}
	}
}
