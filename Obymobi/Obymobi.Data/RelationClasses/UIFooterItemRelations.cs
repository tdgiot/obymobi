﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: UIFooterItem. </summary>
	public partial class UIFooterItemRelations
	{
		/// <summary>CTor</summary>
		public UIFooterItemRelations()
		{
		}

		/// <summary>Gets all relations of the UIFooterItemEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CustomTextEntityUsingUIFooterItemId);
			toReturn.Add(this.MediaEntityUsingUIFooterItemId);
			toReturn.Add(this.UIFooterItemLanguageEntityUsingUIFooterItemId);
			toReturn.Add(this.UIModeEntityUsingUIModeId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between UIFooterItemEntity and CustomTextEntity over the 1:n relation they have, using the relation between the fields:
		/// UIFooterItem.UIFooterItemId - CustomText.UIFooterItemId
		/// </summary>
		public virtual IEntityRelation CustomTextEntityUsingUIFooterItemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomTextCollection" , true);
				relation.AddEntityFieldPair(UIFooterItemFields.UIFooterItemId, CustomTextFields.UIFooterItemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIFooterItemEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UIFooterItemEntity and MediaEntity over the 1:n relation they have, using the relation between the fields:
		/// UIFooterItem.UIFooterItemId - Media.UIFooterItemId
		/// </summary>
		public virtual IEntityRelation MediaEntityUsingUIFooterItemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MediaCollection" , true);
				relation.AddEntityFieldPair(UIFooterItemFields.UIFooterItemId, MediaFields.UIFooterItemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIFooterItemEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UIFooterItemEntity and UIFooterItemLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// UIFooterItem.UIFooterItemId - UIFooterItemLanguage.UIFooterItemId
		/// </summary>
		public virtual IEntityRelation UIFooterItemLanguageEntityUsingUIFooterItemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UIFooterItemLanguageCollection" , true);
				relation.AddEntityFieldPair(UIFooterItemFields.UIFooterItemId, UIFooterItemLanguageFields.UIFooterItemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIFooterItemEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIFooterItemLanguageEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between UIFooterItemEntity and UIModeEntity over the m:1 relation they have, using the relation between the fields:
		/// UIFooterItem.UIModeId - UIMode.UIModeId
		/// </summary>
		public virtual IEntityRelation UIModeEntityUsingUIModeId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UIModeEntity", false);
				relation.AddEntityFieldPair(UIModeFields.UIModeId, UIFooterItemFields.UIModeId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIModeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIFooterItemEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticUIFooterItemRelations
	{
		internal static readonly IEntityRelation CustomTextEntityUsingUIFooterItemIdStatic = new UIFooterItemRelations().CustomTextEntityUsingUIFooterItemId;
		internal static readonly IEntityRelation MediaEntityUsingUIFooterItemIdStatic = new UIFooterItemRelations().MediaEntityUsingUIFooterItemId;
		internal static readonly IEntityRelation UIFooterItemLanguageEntityUsingUIFooterItemIdStatic = new UIFooterItemRelations().UIFooterItemLanguageEntityUsingUIFooterItemId;
		internal static readonly IEntityRelation UIModeEntityUsingUIModeIdStatic = new UIFooterItemRelations().UIModeEntityUsingUIModeId;

		/// <summary>CTor</summary>
		static StaticUIFooterItemRelations()
		{
		}
	}
}
