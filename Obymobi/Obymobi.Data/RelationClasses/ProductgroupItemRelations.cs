﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ProductgroupItem. </summary>
	public partial class ProductgroupItemRelations
	{
		/// <summary>CTor</summary>
		public ProductgroupItemRelations()
		{
		}

		/// <summary>Gets all relations of the ProductgroupItemEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ProductEntityUsingProductId);
			toReturn.Add(this.ProductgroupEntityUsingProductgroupId);
			toReturn.Add(this.ProductgroupEntityUsingNestedProductgroupId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between ProductgroupItemEntity and ProductEntity over the m:1 relation they have, using the relation between the fields:
		/// ProductgroupItem.ProductId - Product.ProductId
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ProductEntity", false);
				relation.AddEntityFieldPair(ProductFields.ProductId, ProductgroupItemFields.ProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductgroupItemEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ProductgroupItemEntity and ProductgroupEntity over the m:1 relation they have, using the relation between the fields:
		/// ProductgroupItem.ProductgroupId - Productgroup.ProductgroupId
		/// </summary>
		public virtual IEntityRelation ProductgroupEntityUsingProductgroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ProductgroupEntity", false);
				relation.AddEntityFieldPair(ProductgroupFields.ProductgroupId, ProductgroupItemFields.ProductgroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductgroupEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductgroupItemEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ProductgroupItemEntity and ProductgroupEntity over the m:1 relation they have, using the relation between the fields:
		/// ProductgroupItem.NestedProductgroupId - Productgroup.ProductgroupId
		/// </summary>
		public virtual IEntityRelation ProductgroupEntityUsingNestedProductgroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ProductgroupEntity1", false);
				relation.AddEntityFieldPair(ProductgroupFields.ProductgroupId, ProductgroupItemFields.NestedProductgroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductgroupEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductgroupItemEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticProductgroupItemRelations
	{
		internal static readonly IEntityRelation ProductEntityUsingProductIdStatic = new ProductgroupItemRelations().ProductEntityUsingProductId;
		internal static readonly IEntityRelation ProductgroupEntityUsingProductgroupIdStatic = new ProductgroupItemRelations().ProductgroupEntityUsingProductgroupId;
		internal static readonly IEntityRelation ProductgroupEntityUsingNestedProductgroupIdStatic = new ProductgroupItemRelations().ProductgroupEntityUsingNestedProductgroupId;

		/// <summary>CTor</summary>
		static StaticProductgroupItemRelations()
		{
		}
	}
}
