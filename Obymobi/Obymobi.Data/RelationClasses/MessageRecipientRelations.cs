﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: MessageRecipient. </summary>
	public partial class MessageRecipientRelations
	{
		/// <summary>CTor</summary>
		public MessageRecipientRelations()
		{
		}

		/// <summary>Gets all relations of the MessageRecipientEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CategoryEntityUsingCategoryId);
			toReturn.Add(this.ClientEntityUsingClientId);
			toReturn.Add(this.CustomerEntityUsingCustomerId);
			toReturn.Add(this.DeliverypointEntityUsingDeliverypointId);
			toReturn.Add(this.EntertainmentEntityUsingEntertainmentId);
			toReturn.Add(this.MessageEntityUsingMessageId);
			toReturn.Add(this.ProductEntityUsingProductId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between MessageRecipientEntity and CategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// MessageRecipient.CategoryId - Category.CategoryId
		/// </summary>
		public virtual IEntityRelation CategoryEntityUsingCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CategoryEntity", false);
				relation.AddEntityFieldPair(CategoryFields.CategoryId, MessageRecipientFields.CategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageRecipientEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MessageRecipientEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// MessageRecipient.ClientId - Client.ClientId
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingClientId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ClientEntity", false);
				relation.AddEntityFieldPair(ClientFields.ClientId, MessageRecipientFields.ClientId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageRecipientEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MessageRecipientEntity and CustomerEntity over the m:1 relation they have, using the relation between the fields:
		/// MessageRecipient.CustomerId - Customer.CustomerId
		/// </summary>
		public virtual IEntityRelation CustomerEntityUsingCustomerId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CustomerEntity", false);
				relation.AddEntityFieldPair(CustomerFields.CustomerId, MessageRecipientFields.CustomerId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomerEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageRecipientEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MessageRecipientEntity and DeliverypointEntity over the m:1 relation they have, using the relation between the fields:
		/// MessageRecipient.DeliverypointId - Deliverypoint.DeliverypointId
		/// </summary>
		public virtual IEntityRelation DeliverypointEntityUsingDeliverypointId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DeliverypointEntity", false);
				relation.AddEntityFieldPair(DeliverypointFields.DeliverypointId, MessageRecipientFields.DeliverypointId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageRecipientEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MessageRecipientEntity and EntertainmentEntity over the m:1 relation they have, using the relation between the fields:
		/// MessageRecipient.EntertainmentId - Entertainment.EntertainmentId
		/// </summary>
		public virtual IEntityRelation EntertainmentEntityUsingEntertainmentId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "EntertainmentEntity", false);
				relation.AddEntityFieldPair(EntertainmentFields.EntertainmentId, MessageRecipientFields.EntertainmentId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageRecipientEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MessageRecipientEntity and MessageEntity over the m:1 relation they have, using the relation between the fields:
		/// MessageRecipient.MessageId - Message.MessageId
		/// </summary>
		public virtual IEntityRelation MessageEntityUsingMessageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "MessageEntity", false);
				relation.AddEntityFieldPair(MessageFields.MessageId, MessageRecipientFields.MessageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageRecipientEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MessageRecipientEntity and ProductEntity over the m:1 relation they have, using the relation between the fields:
		/// MessageRecipient.ProductId - Product.ProductId
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ProductEntity", false);
				relation.AddEntityFieldPair(ProductFields.ProductId, MessageRecipientFields.ProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageRecipientEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticMessageRecipientRelations
	{
		internal static readonly IEntityRelation CategoryEntityUsingCategoryIdStatic = new MessageRecipientRelations().CategoryEntityUsingCategoryId;
		internal static readonly IEntityRelation ClientEntityUsingClientIdStatic = new MessageRecipientRelations().ClientEntityUsingClientId;
		internal static readonly IEntityRelation CustomerEntityUsingCustomerIdStatic = new MessageRecipientRelations().CustomerEntityUsingCustomerId;
		internal static readonly IEntityRelation DeliverypointEntityUsingDeliverypointIdStatic = new MessageRecipientRelations().DeliverypointEntityUsingDeliverypointId;
		internal static readonly IEntityRelation EntertainmentEntityUsingEntertainmentIdStatic = new MessageRecipientRelations().EntertainmentEntityUsingEntertainmentId;
		internal static readonly IEntityRelation MessageEntityUsingMessageIdStatic = new MessageRecipientRelations().MessageEntityUsingMessageId;
		internal static readonly IEntityRelation ProductEntityUsingProductIdStatic = new MessageRecipientRelations().ProductEntityUsingProductId;

		/// <summary>CTor</summary>
		static StaticMessageRecipientRelations()
		{
		}
	}
}
