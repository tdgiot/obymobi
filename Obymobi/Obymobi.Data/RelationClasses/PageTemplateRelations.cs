﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: PageTemplate. </summary>
	public partial class PageTemplateRelations
	{
		/// <summary>CTor</summary>
		public PageTemplateRelations()
		{
		}

		/// <summary>Gets all relations of the PageTemplateEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AttachmentEntityUsingPageTemplateId);
			toReturn.Add(this.CustomTextEntityUsingPageTemplateId);
			toReturn.Add(this.MediaEntityUsingPageTemplateId);
			toReturn.Add(this.PageEntityUsingPageTemplateId);
			toReturn.Add(this.PageTemplateEntityUsingParentPageTemplateId);
			toReturn.Add(this.PageTemplateElementEntityUsingPageTemplateId);
			toReturn.Add(this.PageTemplateLanguageEntityUsingPageTemplateId);
			toReturn.Add(this.PageTemplateEntityUsingPageTemplateIdParentPageTemplateId);
			toReturn.Add(this.SiteTemplateEntityUsingSiteTemplateId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between PageTemplateEntity and AttachmentEntity over the 1:n relation they have, using the relation between the fields:
		/// PageTemplate.PageTemplateId - Attachment.PageTemplateId
		/// </summary>
		public virtual IEntityRelation AttachmentEntityUsingPageTemplateId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AttachmentCollection" , true);
				relation.AddEntityFieldPair(PageTemplateFields.PageTemplateId, AttachmentFields.PageTemplateId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageTemplateEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttachmentEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PageTemplateEntity and CustomTextEntity over the 1:n relation they have, using the relation between the fields:
		/// PageTemplate.PageTemplateId - CustomText.PageTemplateId
		/// </summary>
		public virtual IEntityRelation CustomTextEntityUsingPageTemplateId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomTextCollection" , true);
				relation.AddEntityFieldPair(PageTemplateFields.PageTemplateId, CustomTextFields.PageTemplateId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageTemplateEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PageTemplateEntity and MediaEntity over the 1:n relation they have, using the relation between the fields:
		/// PageTemplate.PageTemplateId - Media.PageTemplateId
		/// </summary>
		public virtual IEntityRelation MediaEntityUsingPageTemplateId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MediaCollection" , true);
				relation.AddEntityFieldPair(PageTemplateFields.PageTemplateId, MediaFields.PageTemplateId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageTemplateEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PageTemplateEntity and PageEntity over the 1:n relation they have, using the relation between the fields:
		/// PageTemplate.PageTemplateId - Page.PageTemplateId
		/// </summary>
		public virtual IEntityRelation PageEntityUsingPageTemplateId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PageCollection" , true);
				relation.AddEntityFieldPair(PageTemplateFields.PageTemplateId, PageFields.PageTemplateId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageTemplateEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PageTemplateEntity and PageTemplateEntity over the 1:n relation they have, using the relation between the fields:
		/// PageTemplate.PageTemplateId - PageTemplate.ParentPageTemplateId
		/// </summary>
		public virtual IEntityRelation PageTemplateEntityUsingParentPageTemplateId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PageTemplateCollection" , true);
				relation.AddEntityFieldPair(PageTemplateFields.PageTemplateId, PageTemplateFields.ParentPageTemplateId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageTemplateEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageTemplateEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PageTemplateEntity and PageTemplateElementEntity over the 1:n relation they have, using the relation between the fields:
		/// PageTemplate.PageTemplateId - PageTemplateElement.PageTemplateId
		/// </summary>
		public virtual IEntityRelation PageTemplateElementEntityUsingPageTemplateId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PageTemplateElementCollection" , true);
				relation.AddEntityFieldPair(PageTemplateFields.PageTemplateId, PageTemplateElementFields.PageTemplateId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageTemplateEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageTemplateElementEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PageTemplateEntity and PageTemplateLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// PageTemplate.PageTemplateId - PageTemplateLanguage.PageTemplateId
		/// </summary>
		public virtual IEntityRelation PageTemplateLanguageEntityUsingPageTemplateId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PageTemplateLanguageCollection" , true);
				relation.AddEntityFieldPair(PageTemplateFields.PageTemplateId, PageTemplateLanguageFields.PageTemplateId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageTemplateEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageTemplateLanguageEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between PageTemplateEntity and PageTemplateEntity over the m:1 relation they have, using the relation between the fields:
		/// PageTemplate.ParentPageTemplateId - PageTemplate.PageTemplateId
		/// </summary>
		public virtual IEntityRelation PageTemplateEntityUsingPageTemplateIdParentPageTemplateId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PageTemplateEntity", false);
				relation.AddEntityFieldPair(PageTemplateFields.PageTemplateId, PageTemplateFields.ParentPageTemplateId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageTemplateEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageTemplateEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between PageTemplateEntity and SiteTemplateEntity over the m:1 relation they have, using the relation between the fields:
		/// PageTemplate.SiteTemplateId - SiteTemplate.SiteTemplateId
		/// </summary>
		public virtual IEntityRelation SiteTemplateEntityUsingSiteTemplateId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SiteTemplateEntity", false);
				relation.AddEntityFieldPair(SiteTemplateFields.SiteTemplateId, PageTemplateFields.SiteTemplateId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SiteTemplateEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageTemplateEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPageTemplateRelations
	{
		internal static readonly IEntityRelation AttachmentEntityUsingPageTemplateIdStatic = new PageTemplateRelations().AttachmentEntityUsingPageTemplateId;
		internal static readonly IEntityRelation CustomTextEntityUsingPageTemplateIdStatic = new PageTemplateRelations().CustomTextEntityUsingPageTemplateId;
		internal static readonly IEntityRelation MediaEntityUsingPageTemplateIdStatic = new PageTemplateRelations().MediaEntityUsingPageTemplateId;
		internal static readonly IEntityRelation PageEntityUsingPageTemplateIdStatic = new PageTemplateRelations().PageEntityUsingPageTemplateId;
		internal static readonly IEntityRelation PageTemplateEntityUsingParentPageTemplateIdStatic = new PageTemplateRelations().PageTemplateEntityUsingParentPageTemplateId;
		internal static readonly IEntityRelation PageTemplateElementEntityUsingPageTemplateIdStatic = new PageTemplateRelations().PageTemplateElementEntityUsingPageTemplateId;
		internal static readonly IEntityRelation PageTemplateLanguageEntityUsingPageTemplateIdStatic = new PageTemplateRelations().PageTemplateLanguageEntityUsingPageTemplateId;
		internal static readonly IEntityRelation PageTemplateEntityUsingPageTemplateIdParentPageTemplateIdStatic = new PageTemplateRelations().PageTemplateEntityUsingPageTemplateIdParentPageTemplateId;
		internal static readonly IEntityRelation SiteTemplateEntityUsingSiteTemplateIdStatic = new PageTemplateRelations().SiteTemplateEntityUsingSiteTemplateId;

		/// <summary>CTor</summary>
		static StaticPageTemplateRelations()
		{
		}
	}
}
