﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Deliverypoint. </summary>
	public partial class DeliverypointRelations
	{
		/// <summary>CTor</summary>
		public DeliverypointRelations()
		{
		}

		/// <summary>Gets all relations of the DeliverypointEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ClientEntityUsingDeliverypointId);
			toReturn.Add(this.ClientEntityUsingLastDeliverypointId);
			toReturn.Add(this.DeliverypointExternalDeliverypointEntityUsingDeliverypointId);
			toReturn.Add(this.GameSessionEntityUsingDeliverypointId);
			toReturn.Add(this.IcrtouchprintermappingDeliverypointEntityUsingDeliverypointId);
			toReturn.Add(this.MessageEntityUsingDeliverypointId);
			toReturn.Add(this.MessagegroupDeliverypointEntityUsingDeliverypointId);
			toReturn.Add(this.MessageRecipientEntityUsingDeliverypointId);
			toReturn.Add(this.NetmessageEntityUsingReceiverDeliverypointId);
			toReturn.Add(this.NetmessageEntityUsingSenderDeliverypointId);
			toReturn.Add(this.OrderEntityUsingDeliverypointId);
			toReturn.Add(this.OrderEntityUsingChargeToDeliverypointId);
			toReturn.Add(this.TerminalEntityUsingAltSystemMessagesDeliverypointId);
			toReturn.Add(this.TerminalEntityUsingSystemMessagesDeliverypointId);
			toReturn.Add(this.TimestampEntityUsingDeliverypointId);
			toReturn.Add(this.ClientConfigurationEntityUsingClientConfigurationId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			toReturn.Add(this.DeliverypointgroupEntityUsingDeliverypointgroupId);
			toReturn.Add(this.DeviceEntityUsingDeviceId);
			toReturn.Add(this.PosdeliverypointEntityUsingPosdeliverypointId);
			toReturn.Add(this.RoomControlAreaEntityUsingRoomControlAreaId);
			toReturn.Add(this.RoomControlConfigurationEntityUsingRoomControlConfigurationId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between DeliverypointEntity and ClientEntity over the 1:n relation they have, using the relation between the fields:
		/// Deliverypoint.DeliverypointId - Client.DeliverypointId
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingDeliverypointId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ClientCollection" , true);
				relation.AddEntityFieldPair(DeliverypointFields.DeliverypointId, ClientFields.DeliverypointId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointEntity and ClientEntity over the 1:n relation they have, using the relation between the fields:
		/// Deliverypoint.DeliverypointId - Client.LastDeliverypointId
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingLastDeliverypointId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ClientCollection_" , true);
				relation.AddEntityFieldPair(DeliverypointFields.DeliverypointId, ClientFields.LastDeliverypointId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointEntity and DeliverypointExternalDeliverypointEntity over the 1:n relation they have, using the relation between the fields:
		/// Deliverypoint.DeliverypointId - DeliverypointExternalDeliverypoint.DeliverypointId
		/// </summary>
		public virtual IEntityRelation DeliverypointExternalDeliverypointEntityUsingDeliverypointId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeliverypointExternalDeliverypointCollection" , true);
				relation.AddEntityFieldPair(DeliverypointFields.DeliverypointId, DeliverypointExternalDeliverypointFields.DeliverypointId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointExternalDeliverypointEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointEntity and GameSessionEntity over the 1:n relation they have, using the relation between the fields:
		/// Deliverypoint.DeliverypointId - GameSession.DeliverypointId
		/// </summary>
		public virtual IEntityRelation GameSessionEntityUsingDeliverypointId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "GameSessionCollection" , true);
				relation.AddEntityFieldPair(DeliverypointFields.DeliverypointId, GameSessionFields.DeliverypointId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GameSessionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointEntity and IcrtouchprintermappingDeliverypointEntity over the 1:n relation they have, using the relation between the fields:
		/// Deliverypoint.DeliverypointId - IcrtouchprintermappingDeliverypoint.DeliverypointId
		/// </summary>
		public virtual IEntityRelation IcrtouchprintermappingDeliverypointEntityUsingDeliverypointId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "IcrtouchprintermappingDeliverypointCollection" , true);
				relation.AddEntityFieldPair(DeliverypointFields.DeliverypointId, IcrtouchprintermappingDeliverypointFields.DeliverypointId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("IcrtouchprintermappingDeliverypointEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointEntity and MessageEntity over the 1:n relation they have, using the relation between the fields:
		/// Deliverypoint.DeliverypointId - Message.DeliverypointId
		/// </summary>
		public virtual IEntityRelation MessageEntityUsingDeliverypointId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MessageCollection" , true);
				relation.AddEntityFieldPair(DeliverypointFields.DeliverypointId, MessageFields.DeliverypointId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointEntity and MessagegroupDeliverypointEntity over the 1:n relation they have, using the relation between the fields:
		/// Deliverypoint.DeliverypointId - MessagegroupDeliverypoint.DeliverypointId
		/// </summary>
		public virtual IEntityRelation MessagegroupDeliverypointEntityUsingDeliverypointId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MessagegroupDeliverypointCollection" , true);
				relation.AddEntityFieldPair(DeliverypointFields.DeliverypointId, MessagegroupDeliverypointFields.DeliverypointId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessagegroupDeliverypointEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointEntity and MessageRecipientEntity over the 1:n relation they have, using the relation between the fields:
		/// Deliverypoint.DeliverypointId - MessageRecipient.DeliverypointId
		/// </summary>
		public virtual IEntityRelation MessageRecipientEntityUsingDeliverypointId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MessageRecipientCollection" , true);
				relation.AddEntityFieldPair(DeliverypointFields.DeliverypointId, MessageRecipientFields.DeliverypointId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageRecipientEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointEntity and NetmessageEntity over the 1:n relation they have, using the relation between the fields:
		/// Deliverypoint.DeliverypointId - Netmessage.ReceiverDeliverypointId
		/// </summary>
		public virtual IEntityRelation NetmessageEntityUsingReceiverDeliverypointId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ReceivedNetmessageCollection" , true);
				relation.AddEntityFieldPair(DeliverypointFields.DeliverypointId, NetmessageFields.ReceiverDeliverypointId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NetmessageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointEntity and NetmessageEntity over the 1:n relation they have, using the relation between the fields:
		/// Deliverypoint.DeliverypointId - Netmessage.SenderDeliverypointId
		/// </summary>
		public virtual IEntityRelation NetmessageEntityUsingSenderDeliverypointId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SentNetmessageCollection" , true);
				relation.AddEntityFieldPair(DeliverypointFields.DeliverypointId, NetmessageFields.SenderDeliverypointId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NetmessageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointEntity and OrderEntity over the 1:n relation they have, using the relation between the fields:
		/// Deliverypoint.DeliverypointId - Order.DeliverypointId
		/// </summary>
		public virtual IEntityRelation OrderEntityUsingDeliverypointId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrderCollection" , true);
				relation.AddEntityFieldPair(DeliverypointFields.DeliverypointId, OrderFields.DeliverypointId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointEntity and OrderEntity over the 1:n relation they have, using the relation between the fields:
		/// Deliverypoint.DeliverypointId - Order.ChargeToDeliverypointId
		/// </summary>
		public virtual IEntityRelation OrderEntityUsingChargeToDeliverypointId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrderCollection_" , true);
				relation.AddEntityFieldPair(DeliverypointFields.DeliverypointId, OrderFields.ChargeToDeliverypointId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointEntity and TerminalEntity over the 1:n relation they have, using the relation between the fields:
		/// Deliverypoint.DeliverypointId - Terminal.AltSystemMessagesDeliverypointId
		/// </summary>
		public virtual IEntityRelation TerminalEntityUsingAltSystemMessagesDeliverypointId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TerminalCollection_" , true);
				relation.AddEntityFieldPair(DeliverypointFields.DeliverypointId, TerminalFields.AltSystemMessagesDeliverypointId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointEntity and TerminalEntity over the 1:n relation they have, using the relation between the fields:
		/// Deliverypoint.DeliverypointId - Terminal.SystemMessagesDeliverypointId
		/// </summary>
		public virtual IEntityRelation TerminalEntityUsingSystemMessagesDeliverypointId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TerminalCollection" , true);
				relation.AddEntityFieldPair(DeliverypointFields.DeliverypointId, TerminalFields.SystemMessagesDeliverypointId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointEntity and TimestampEntity over the 1:1 relation they have, using the relation between the fields:
		/// Deliverypoint.DeliverypointId - Timestamp.DeliverypointId
		/// </summary>
		public virtual IEntityRelation TimestampEntityUsingDeliverypointId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "TimestampCollection", true);


				relation.AddEntityFieldPair(DeliverypointFields.DeliverypointId, TimestampFields.DeliverypointId);


				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TimestampEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeliverypointEntity and ClientConfigurationEntity over the m:1 relation they have, using the relation between the fields:
		/// Deliverypoint.ClientConfigurationId - ClientConfiguration.ClientConfigurationId
		/// </summary>
		public virtual IEntityRelation ClientConfigurationEntityUsingClientConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ClientConfigurationEntity", false);
				relation.AddEntityFieldPair(ClientConfigurationFields.ClientConfigurationId, DeliverypointFields.ClientConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientConfigurationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between DeliverypointEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// Deliverypoint.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, DeliverypointFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between DeliverypointEntity and DeliverypointgroupEntity over the m:1 relation they have, using the relation between the fields:
		/// Deliverypoint.DeliverypointgroupId - Deliverypointgroup.DeliverypointgroupId
		/// </summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingDeliverypointgroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DeliverypointgroupEntity", false);
				relation.AddEntityFieldPair(DeliverypointgroupFields.DeliverypointgroupId, DeliverypointFields.DeliverypointgroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between DeliverypointEntity and DeviceEntity over the m:1 relation they have, using the relation between the fields:
		/// Deliverypoint.DeviceId - Device.DeviceId
		/// </summary>
		public virtual IEntityRelation DeviceEntityUsingDeviceId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DeviceEntity", false);
				relation.AddEntityFieldPair(DeviceFields.DeviceId, DeliverypointFields.DeviceId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between DeliverypointEntity and PosdeliverypointEntity over the m:1 relation they have, using the relation between the fields:
		/// Deliverypoint.PosdeliverypointId - Posdeliverypoint.PosdeliverypointId
		/// </summary>
		public virtual IEntityRelation PosdeliverypointEntityUsingPosdeliverypointId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PosdeliverypointEntity", false);
				relation.AddEntityFieldPair(PosdeliverypointFields.PosdeliverypointId, DeliverypointFields.PosdeliverypointId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PosdeliverypointEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between DeliverypointEntity and RoomControlAreaEntity over the m:1 relation they have, using the relation between the fields:
		/// Deliverypoint.RoomControlAreaId - RoomControlArea.RoomControlAreaId
		/// </summary>
		public virtual IEntityRelation RoomControlAreaEntityUsingRoomControlAreaId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RoomControlAreaEntity", false);
				relation.AddEntityFieldPair(RoomControlAreaFields.RoomControlAreaId, DeliverypointFields.RoomControlAreaId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlAreaEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between DeliverypointEntity and RoomControlConfigurationEntity over the m:1 relation they have, using the relation between the fields:
		/// Deliverypoint.RoomControlConfigurationId - RoomControlConfiguration.RoomControlConfigurationId
		/// </summary>
		public virtual IEntityRelation RoomControlConfigurationEntityUsingRoomControlConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RoomControlConfigurationEntity", false);
				relation.AddEntityFieldPair(RoomControlConfigurationFields.RoomControlConfigurationId, DeliverypointFields.RoomControlConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlConfigurationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticDeliverypointRelations
	{
		internal static readonly IEntityRelation ClientEntityUsingDeliverypointIdStatic = new DeliverypointRelations().ClientEntityUsingDeliverypointId;
		internal static readonly IEntityRelation ClientEntityUsingLastDeliverypointIdStatic = new DeliverypointRelations().ClientEntityUsingLastDeliverypointId;
		internal static readonly IEntityRelation DeliverypointExternalDeliverypointEntityUsingDeliverypointIdStatic = new DeliverypointRelations().DeliverypointExternalDeliverypointEntityUsingDeliverypointId;
		internal static readonly IEntityRelation GameSessionEntityUsingDeliverypointIdStatic = new DeliverypointRelations().GameSessionEntityUsingDeliverypointId;
		internal static readonly IEntityRelation IcrtouchprintermappingDeliverypointEntityUsingDeliverypointIdStatic = new DeliverypointRelations().IcrtouchprintermappingDeliverypointEntityUsingDeliverypointId;
		internal static readonly IEntityRelation MessageEntityUsingDeliverypointIdStatic = new DeliverypointRelations().MessageEntityUsingDeliverypointId;
		internal static readonly IEntityRelation MessagegroupDeliverypointEntityUsingDeliverypointIdStatic = new DeliverypointRelations().MessagegroupDeliverypointEntityUsingDeliverypointId;
		internal static readonly IEntityRelation MessageRecipientEntityUsingDeliverypointIdStatic = new DeliverypointRelations().MessageRecipientEntityUsingDeliverypointId;
		internal static readonly IEntityRelation NetmessageEntityUsingReceiverDeliverypointIdStatic = new DeliverypointRelations().NetmessageEntityUsingReceiverDeliverypointId;
		internal static readonly IEntityRelation NetmessageEntityUsingSenderDeliverypointIdStatic = new DeliverypointRelations().NetmessageEntityUsingSenderDeliverypointId;
		internal static readonly IEntityRelation OrderEntityUsingDeliverypointIdStatic = new DeliverypointRelations().OrderEntityUsingDeliverypointId;
		internal static readonly IEntityRelation OrderEntityUsingChargeToDeliverypointIdStatic = new DeliverypointRelations().OrderEntityUsingChargeToDeliverypointId;
		internal static readonly IEntityRelation TerminalEntityUsingAltSystemMessagesDeliverypointIdStatic = new DeliverypointRelations().TerminalEntityUsingAltSystemMessagesDeliverypointId;
		internal static readonly IEntityRelation TerminalEntityUsingSystemMessagesDeliverypointIdStatic = new DeliverypointRelations().TerminalEntityUsingSystemMessagesDeliverypointId;
		internal static readonly IEntityRelation TimestampEntityUsingDeliverypointIdStatic = new DeliverypointRelations().TimestampEntityUsingDeliverypointId;
		internal static readonly IEntityRelation ClientConfigurationEntityUsingClientConfigurationIdStatic = new DeliverypointRelations().ClientConfigurationEntityUsingClientConfigurationId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new DeliverypointRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation DeliverypointgroupEntityUsingDeliverypointgroupIdStatic = new DeliverypointRelations().DeliverypointgroupEntityUsingDeliverypointgroupId;
		internal static readonly IEntityRelation DeviceEntityUsingDeviceIdStatic = new DeliverypointRelations().DeviceEntityUsingDeviceId;
		internal static readonly IEntityRelation PosdeliverypointEntityUsingPosdeliverypointIdStatic = new DeliverypointRelations().PosdeliverypointEntityUsingPosdeliverypointId;
		internal static readonly IEntityRelation RoomControlAreaEntityUsingRoomControlAreaIdStatic = new DeliverypointRelations().RoomControlAreaEntityUsingRoomControlAreaId;
		internal static readonly IEntityRelation RoomControlConfigurationEntityUsingRoomControlConfigurationIdStatic = new DeliverypointRelations().RoomControlConfigurationEntityUsingRoomControlConfigurationId;

		/// <summary>CTor</summary>
		static StaticDeliverypointRelations()
		{
		}
	}
}
