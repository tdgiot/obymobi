﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Advertisement. </summary>
	public partial class AdvertisementRelations
	{
		/// <summary>CTor</summary>
		public AdvertisementRelations()
		{
		}

		/// <summary>Gets all relations of the AdvertisementEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AdvertisementConfigurationAdvertisementEntityUsingAdvertisementId);
			toReturn.Add(this.AdvertisementLanguageEntityUsingAdvertisementId);
			toReturn.Add(this.AdvertisementTagAdvertisementEntityUsingAdvertisementId);
			toReturn.Add(this.CustomTextEntityUsingAdvertisementId);
			toReturn.Add(this.DeliverypointgroupAdvertisementEntityUsingAdvertisementId);
			toReturn.Add(this.MediaEntityUsingAdvertisementId);
			toReturn.Add(this.UIWidgetEntityUsingAdvertisementId);
			toReturn.Add(this.CategoryEntityUsingActionCategoryId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			toReturn.Add(this.DeliverypointgroupEntityUsingDeliverypointgroupId);
			toReturn.Add(this.EntertainmentEntityUsingActionEntertainmentId);
			toReturn.Add(this.EntertainmentEntityUsingEntertainmentId);
			toReturn.Add(this.EntertainmentcategoryEntityUsingActionEntertainmentCategoryId);
			toReturn.Add(this.GenericproductEntityUsingGenericproductId);
			toReturn.Add(this.PageEntityUsingActionPageId);
			toReturn.Add(this.ProductEntityUsingProductId);
			toReturn.Add(this.ProductCategoryEntityUsingProductCategoryId);
			toReturn.Add(this.SiteEntityUsingActionSiteId);
			toReturn.Add(this.SupplierEntityUsingSupplierId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between AdvertisementEntity and AdvertisementConfigurationAdvertisementEntity over the 1:n relation they have, using the relation between the fields:
		/// Advertisement.AdvertisementId - AdvertisementConfigurationAdvertisement.AdvertisementId
		/// </summary>
		public virtual IEntityRelation AdvertisementConfigurationAdvertisementEntityUsingAdvertisementId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AdvertisementConfigurationAdvertisementCollection" , true);
				relation.AddEntityFieldPair(AdvertisementFields.AdvertisementId, AdvertisementConfigurationAdvertisementFields.AdvertisementId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementConfigurationAdvertisementEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AdvertisementEntity and AdvertisementLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Advertisement.AdvertisementId - AdvertisementLanguage.AdvertisementId
		/// </summary>
		public virtual IEntityRelation AdvertisementLanguageEntityUsingAdvertisementId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AdvertisementLanguageCollection" , true);
				relation.AddEntityFieldPair(AdvertisementFields.AdvertisementId, AdvertisementLanguageFields.AdvertisementId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AdvertisementEntity and AdvertisementTagAdvertisementEntity over the 1:n relation they have, using the relation between the fields:
		/// Advertisement.AdvertisementId - AdvertisementTagAdvertisement.AdvertisementId
		/// </summary>
		public virtual IEntityRelation AdvertisementTagAdvertisementEntityUsingAdvertisementId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AdvertisementTagAdvertisementCollection" , true);
				relation.AddEntityFieldPair(AdvertisementFields.AdvertisementId, AdvertisementTagAdvertisementFields.AdvertisementId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementTagAdvertisementEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AdvertisementEntity and CustomTextEntity over the 1:n relation they have, using the relation between the fields:
		/// Advertisement.AdvertisementId - CustomText.AdvertisementId
		/// </summary>
		public virtual IEntityRelation CustomTextEntityUsingAdvertisementId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomTextCollection" , true);
				relation.AddEntityFieldPair(AdvertisementFields.AdvertisementId, CustomTextFields.AdvertisementId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AdvertisementEntity and DeliverypointgroupAdvertisementEntity over the 1:n relation they have, using the relation between the fields:
		/// Advertisement.AdvertisementId - DeliverypointgroupAdvertisement.AdvertisementId
		/// </summary>
		public virtual IEntityRelation DeliverypointgroupAdvertisementEntityUsingAdvertisementId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeliverypointgroupAdvertisementCollection" , true);
				relation.AddEntityFieldPair(AdvertisementFields.AdvertisementId, DeliverypointgroupAdvertisementFields.AdvertisementId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupAdvertisementEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AdvertisementEntity and MediaEntity over the 1:n relation they have, using the relation between the fields:
		/// Advertisement.AdvertisementId - Media.AdvertisementId
		/// </summary>
		public virtual IEntityRelation MediaEntityUsingAdvertisementId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MediaCollection" , true);
				relation.AddEntityFieldPair(AdvertisementFields.AdvertisementId, MediaFields.AdvertisementId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AdvertisementEntity and UIWidgetEntity over the 1:n relation they have, using the relation between the fields:
		/// Advertisement.AdvertisementId - UIWidget.AdvertisementId
		/// </summary>
		public virtual IEntityRelation UIWidgetEntityUsingAdvertisementId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UIWidgetCollection" , true);
				relation.AddEntityFieldPair(AdvertisementFields.AdvertisementId, UIWidgetFields.AdvertisementId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIWidgetEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between AdvertisementEntity and CategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// Advertisement.ActionCategoryId - Category.CategoryId
		/// </summary>
		public virtual IEntityRelation CategoryEntityUsingActionCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ActionCategoryEntity", false);
				relation.AddEntityFieldPair(CategoryFields.CategoryId, AdvertisementFields.ActionCategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AdvertisementEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// Advertisement.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, AdvertisementFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AdvertisementEntity and DeliverypointgroupEntity over the m:1 relation they have, using the relation between the fields:
		/// Advertisement.DeliverypointgroupId - Deliverypointgroup.DeliverypointgroupId
		/// </summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingDeliverypointgroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DeliverypointgroupEntity", false);
				relation.AddEntityFieldPair(DeliverypointgroupFields.DeliverypointgroupId, AdvertisementFields.DeliverypointgroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AdvertisementEntity and EntertainmentEntity over the m:1 relation they have, using the relation between the fields:
		/// Advertisement.ActionEntertainmentId - Entertainment.EntertainmentId
		/// </summary>
		public virtual IEntityRelation EntertainmentEntityUsingActionEntertainmentId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ActionEntertainmentEntity", false);
				relation.AddEntityFieldPair(EntertainmentFields.EntertainmentId, AdvertisementFields.ActionEntertainmentId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AdvertisementEntity and EntertainmentEntity over the m:1 relation they have, using the relation between the fields:
		/// Advertisement.EntertainmentId - Entertainment.EntertainmentId
		/// </summary>
		public virtual IEntityRelation EntertainmentEntityUsingEntertainmentId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "EntertainmentEntity", false);
				relation.AddEntityFieldPair(EntertainmentFields.EntertainmentId, AdvertisementFields.EntertainmentId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AdvertisementEntity and EntertainmentcategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// Advertisement.ActionEntertainmentCategoryId - Entertainmentcategory.EntertainmentcategoryId
		/// </summary>
		public virtual IEntityRelation EntertainmentcategoryEntityUsingActionEntertainmentCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ActionEntertainmentcategoryEntity", false);
				relation.AddEntityFieldPair(EntertainmentcategoryFields.EntertainmentcategoryId, AdvertisementFields.ActionEntertainmentCategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentcategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AdvertisementEntity and GenericproductEntity over the m:1 relation they have, using the relation between the fields:
		/// Advertisement.GenericproductId - Genericproduct.GenericproductId
		/// </summary>
		public virtual IEntityRelation GenericproductEntityUsingGenericproductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "GenericproductEntity", false);
				relation.AddEntityFieldPair(GenericproductFields.GenericproductId, AdvertisementFields.GenericproductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericproductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AdvertisementEntity and PageEntity over the m:1 relation they have, using the relation between the fields:
		/// Advertisement.ActionPageId - Page.PageId
		/// </summary>
		public virtual IEntityRelation PageEntityUsingActionPageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PageEntity", false);
				relation.AddEntityFieldPair(PageFields.PageId, AdvertisementFields.ActionPageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AdvertisementEntity and ProductEntity over the m:1 relation they have, using the relation between the fields:
		/// Advertisement.ProductId - Product.ProductId
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ProductEntity", false);
				relation.AddEntityFieldPair(ProductFields.ProductId, AdvertisementFields.ProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AdvertisementEntity and ProductCategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// Advertisement.ProductCategoryId - ProductCategory.ProductCategoryId
		/// </summary>
		public virtual IEntityRelation ProductCategoryEntityUsingProductCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ProductCategoryEntity", false);
				relation.AddEntityFieldPair(ProductCategoryFields.ProductCategoryId, AdvertisementFields.ProductCategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductCategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AdvertisementEntity and SiteEntity over the m:1 relation they have, using the relation between the fields:
		/// Advertisement.ActionSiteId - Site.SiteId
		/// </summary>
		public virtual IEntityRelation SiteEntityUsingActionSiteId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SiteEntity", false);
				relation.AddEntityFieldPair(SiteFields.SiteId, AdvertisementFields.ActionSiteId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SiteEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AdvertisementEntity and SupplierEntity over the m:1 relation they have, using the relation between the fields:
		/// Advertisement.SupplierId - Supplier.SupplierId
		/// </summary>
		public virtual IEntityRelation SupplierEntityUsingSupplierId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SupplierEntity", false);
				relation.AddEntityFieldPair(SupplierFields.SupplierId, AdvertisementFields.SupplierId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SupplierEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticAdvertisementRelations
	{
		internal static readonly IEntityRelation AdvertisementConfigurationAdvertisementEntityUsingAdvertisementIdStatic = new AdvertisementRelations().AdvertisementConfigurationAdvertisementEntityUsingAdvertisementId;
		internal static readonly IEntityRelation AdvertisementLanguageEntityUsingAdvertisementIdStatic = new AdvertisementRelations().AdvertisementLanguageEntityUsingAdvertisementId;
		internal static readonly IEntityRelation AdvertisementTagAdvertisementEntityUsingAdvertisementIdStatic = new AdvertisementRelations().AdvertisementTagAdvertisementEntityUsingAdvertisementId;
		internal static readonly IEntityRelation CustomTextEntityUsingAdvertisementIdStatic = new AdvertisementRelations().CustomTextEntityUsingAdvertisementId;
		internal static readonly IEntityRelation DeliverypointgroupAdvertisementEntityUsingAdvertisementIdStatic = new AdvertisementRelations().DeliverypointgroupAdvertisementEntityUsingAdvertisementId;
		internal static readonly IEntityRelation MediaEntityUsingAdvertisementIdStatic = new AdvertisementRelations().MediaEntityUsingAdvertisementId;
		internal static readonly IEntityRelation UIWidgetEntityUsingAdvertisementIdStatic = new AdvertisementRelations().UIWidgetEntityUsingAdvertisementId;
		internal static readonly IEntityRelation CategoryEntityUsingActionCategoryIdStatic = new AdvertisementRelations().CategoryEntityUsingActionCategoryId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new AdvertisementRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation DeliverypointgroupEntityUsingDeliverypointgroupIdStatic = new AdvertisementRelations().DeliverypointgroupEntityUsingDeliverypointgroupId;
		internal static readonly IEntityRelation EntertainmentEntityUsingActionEntertainmentIdStatic = new AdvertisementRelations().EntertainmentEntityUsingActionEntertainmentId;
		internal static readonly IEntityRelation EntertainmentEntityUsingEntertainmentIdStatic = new AdvertisementRelations().EntertainmentEntityUsingEntertainmentId;
		internal static readonly IEntityRelation EntertainmentcategoryEntityUsingActionEntertainmentCategoryIdStatic = new AdvertisementRelations().EntertainmentcategoryEntityUsingActionEntertainmentCategoryId;
		internal static readonly IEntityRelation GenericproductEntityUsingGenericproductIdStatic = new AdvertisementRelations().GenericproductEntityUsingGenericproductId;
		internal static readonly IEntityRelation PageEntityUsingActionPageIdStatic = new AdvertisementRelations().PageEntityUsingActionPageId;
		internal static readonly IEntityRelation ProductEntityUsingProductIdStatic = new AdvertisementRelations().ProductEntityUsingProductId;
		internal static readonly IEntityRelation ProductCategoryEntityUsingProductCategoryIdStatic = new AdvertisementRelations().ProductCategoryEntityUsingProductCategoryId;
		internal static readonly IEntityRelation SiteEntityUsingActionSiteIdStatic = new AdvertisementRelations().SiteEntityUsingActionSiteId;
		internal static readonly IEntityRelation SupplierEntityUsingSupplierIdStatic = new AdvertisementRelations().SupplierEntityUsingSupplierId;

		/// <summary>CTor</summary>
		static StaticAdvertisementRelations()
		{
		}
	}
}
