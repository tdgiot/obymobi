﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ProductSuggestion. </summary>
	public partial class ProductSuggestionRelations
	{
		/// <summary>CTor</summary>
		public ProductSuggestionRelations()
		{
		}

		/// <summary>Gets all relations of the ProductSuggestionEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ProductEntityUsingProductId);
			toReturn.Add(this.ProductEntityUsingSuggestedProductId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between ProductSuggestionEntity and ProductEntity over the m:1 relation they have, using the relation between the fields:
		/// ProductSuggestion.ProductId - Product.ProductId
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ProductEntity", false);
				relation.AddEntityFieldPair(ProductFields.ProductId, ProductSuggestionFields.ProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductSuggestionEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ProductSuggestionEntity and ProductEntity over the m:1 relation they have, using the relation between the fields:
		/// ProductSuggestion.SuggestedProductId - Product.ProductId
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingSuggestedProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SuggestedProductEntity", false);
				relation.AddEntityFieldPair(ProductFields.ProductId, ProductSuggestionFields.SuggestedProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductSuggestionEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticProductSuggestionRelations
	{
		internal static readonly IEntityRelation ProductEntityUsingProductIdStatic = new ProductSuggestionRelations().ProductEntityUsingProductId;
		internal static readonly IEntityRelation ProductEntityUsingSuggestedProductIdStatic = new ProductSuggestionRelations().ProductEntityUsingSuggestedProductId;

		/// <summary>CTor</summary>
		static StaticProductSuggestionRelations()
		{
		}
	}
}
