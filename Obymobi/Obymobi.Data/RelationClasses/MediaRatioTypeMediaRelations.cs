﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: MediaRatioTypeMedia. </summary>
	public partial class MediaRatioTypeMediaRelations
	{
		/// <summary>CTor</summary>
		public MediaRatioTypeMediaRelations()
		{
		}

		/// <summary>Gets all relations of the MediaRatioTypeMediaEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CloudProcessingTaskEntityUsingMediaRatioTypeMediaId);
			toReturn.Add(this.MediaProcessingTaskEntityUsingMediaRatioTypeMediaId);
			toReturn.Add(this.MediaRatioTypeMediaFileEntityUsingMediaRatioTypeMediaId);
			toReturn.Add(this.MediaEntityUsingMediaId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between MediaRatioTypeMediaEntity and CloudProcessingTaskEntity over the 1:n relation they have, using the relation between the fields:
		/// MediaRatioTypeMedia.MediaRatioTypeMediaId - CloudProcessingTask.MediaRatioTypeMediaId
		/// </summary>
		public virtual IEntityRelation CloudProcessingTaskEntityUsingMediaRatioTypeMediaId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CloudProcessingTaskCollection" , true);
				relation.AddEntityFieldPair(MediaRatioTypeMediaFields.MediaRatioTypeMediaId, CloudProcessingTaskFields.MediaRatioTypeMediaId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaRatioTypeMediaEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CloudProcessingTaskEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between MediaRatioTypeMediaEntity and MediaProcessingTaskEntity over the 1:n relation they have, using the relation between the fields:
		/// MediaRatioTypeMedia.MediaRatioTypeMediaId - MediaProcessingTask.MediaRatioTypeMediaId
		/// </summary>
		public virtual IEntityRelation MediaProcessingTaskEntityUsingMediaRatioTypeMediaId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MediaProcessingTaskCollection" , true);
				relation.AddEntityFieldPair(MediaRatioTypeMediaFields.MediaRatioTypeMediaId, MediaProcessingTaskFields.MediaRatioTypeMediaId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaRatioTypeMediaEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaProcessingTaskEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between MediaRatioTypeMediaEntity and MediaRatioTypeMediaFileEntity over the 1:1 relation they have, using the relation between the fields:
		/// MediaRatioTypeMedia.MediaRatioTypeMediaId - MediaRatioTypeMediaFile.MediaRatioTypeMediaId
		/// </summary>
		public virtual IEntityRelation MediaRatioTypeMediaFileEntityUsingMediaRatioTypeMediaId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "MediaRatioTypeMediaFileEntity", true);


				relation.AddEntityFieldPair(MediaRatioTypeMediaFields.MediaRatioTypeMediaId, MediaRatioTypeMediaFileFields.MediaRatioTypeMediaId);


				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaRatioTypeMediaEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaRatioTypeMediaFileEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between MediaRatioTypeMediaEntity and MediaEntity over the m:1 relation they have, using the relation between the fields:
		/// MediaRatioTypeMedia.MediaId - Media.MediaId
		/// </summary>
		public virtual IEntityRelation MediaEntityUsingMediaId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "MediaEntity", false);
				relation.AddEntityFieldPair(MediaFields.MediaId, MediaRatioTypeMediaFields.MediaId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaRatioTypeMediaEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticMediaRatioTypeMediaRelations
	{
		internal static readonly IEntityRelation CloudProcessingTaskEntityUsingMediaRatioTypeMediaIdStatic = new MediaRatioTypeMediaRelations().CloudProcessingTaskEntityUsingMediaRatioTypeMediaId;
		internal static readonly IEntityRelation MediaProcessingTaskEntityUsingMediaRatioTypeMediaIdStatic = new MediaRatioTypeMediaRelations().MediaProcessingTaskEntityUsingMediaRatioTypeMediaId;
		internal static readonly IEntityRelation MediaRatioTypeMediaFileEntityUsingMediaRatioTypeMediaIdStatic = new MediaRatioTypeMediaRelations().MediaRatioTypeMediaFileEntityUsingMediaRatioTypeMediaId;
		internal static readonly IEntityRelation MediaEntityUsingMediaIdStatic = new MediaRatioTypeMediaRelations().MediaEntityUsingMediaId;

		/// <summary>CTor</summary>
		static StaticMediaRatioTypeMediaRelations()
		{
		}
	}
}
