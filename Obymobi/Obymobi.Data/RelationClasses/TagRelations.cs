﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Tag. </summary>
	public partial class TagRelations
	{
		/// <summary>CTor</summary>
		public TagRelations()
		{
		}

		/// <summary>Gets all relations of the TagEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AlterationoptionTagEntityUsingTagId);
			toReturn.Add(this.CategoryTagEntityUsingTagId);
			toReturn.Add(this.OrderitemAlterationitemTagEntityUsingTagId);
			toReturn.Add(this.OrderitemTagEntityUsingTagId);
			toReturn.Add(this.ProductCategoryTagEntityUsingTagId);
			toReturn.Add(this.ProductTagEntityUsingTagId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between TagEntity and AlterationoptionTagEntity over the 1:n relation they have, using the relation between the fields:
		/// Tag.TagId - AlterationoptionTag.TagId
		/// </summary>
		public virtual IEntityRelation AlterationoptionTagEntityUsingTagId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AlterationoptionTagCollection" , true);
				relation.AddEntityFieldPair(TagFields.TagId, AlterationoptionTagFields.TagId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TagEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationoptionTagEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TagEntity and CategoryTagEntity over the 1:n relation they have, using the relation between the fields:
		/// Tag.TagId - CategoryTag.TagId
		/// </summary>
		public virtual IEntityRelation CategoryTagEntityUsingTagId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CategoryTagCollection" , true);
				relation.AddEntityFieldPair(TagFields.TagId, CategoryTagFields.TagId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TagEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryTagEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TagEntity and OrderitemAlterationitemTagEntity over the 1:n relation they have, using the relation between the fields:
		/// Tag.TagId - OrderitemAlterationitemTag.TagId
		/// </summary>
		public virtual IEntityRelation OrderitemAlterationitemTagEntityUsingTagId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrderitemAlterationitemTagCollection" , true);
				relation.AddEntityFieldPair(TagFields.TagId, OrderitemAlterationitemTagFields.TagId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TagEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderitemAlterationitemTagEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TagEntity and OrderitemTagEntity over the 1:n relation they have, using the relation between the fields:
		/// Tag.TagId - OrderitemTag.TagId
		/// </summary>
		public virtual IEntityRelation OrderitemTagEntityUsingTagId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrderitemTagCollection" , true);
				relation.AddEntityFieldPair(TagFields.TagId, OrderitemTagFields.TagId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TagEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderitemTagEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TagEntity and ProductCategoryTagEntity over the 1:n relation they have, using the relation between the fields:
		/// Tag.TagId - ProductCategoryTag.TagId
		/// </summary>
		public virtual IEntityRelation ProductCategoryTagEntityUsingTagId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ProductCategoryTagCollection" , true);
				relation.AddEntityFieldPair(TagFields.TagId, ProductCategoryTagFields.TagId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TagEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductCategoryTagEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TagEntity and ProductTagEntity over the 1:n relation they have, using the relation between the fields:
		/// Tag.TagId - ProductTag.TagId
		/// </summary>
		public virtual IEntityRelation ProductTagEntityUsingTagId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ProductTagCollection" , true);
				relation.AddEntityFieldPair(TagFields.TagId, ProductTagFields.TagId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TagEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductTagEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between TagEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// Tag.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, TagFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TagEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticTagRelations
	{
		internal static readonly IEntityRelation AlterationoptionTagEntityUsingTagIdStatic = new TagRelations().AlterationoptionTagEntityUsingTagId;
		internal static readonly IEntityRelation CategoryTagEntityUsingTagIdStatic = new TagRelations().CategoryTagEntityUsingTagId;
		internal static readonly IEntityRelation OrderitemAlterationitemTagEntityUsingTagIdStatic = new TagRelations().OrderitemAlterationitemTagEntityUsingTagId;
		internal static readonly IEntityRelation OrderitemTagEntityUsingTagIdStatic = new TagRelations().OrderitemTagEntityUsingTagId;
		internal static readonly IEntityRelation ProductCategoryTagEntityUsingTagIdStatic = new TagRelations().ProductCategoryTagEntityUsingTagId;
		internal static readonly IEntityRelation ProductTagEntityUsingTagIdStatic = new TagRelations().ProductTagEntityUsingTagId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new TagRelations().CompanyEntityUsingCompanyId;

		/// <summary>CTor</summary>
		static StaticTagRelations()
		{
		}
	}
}
