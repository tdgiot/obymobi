﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: MessageTemplateCategory. </summary>
	public partial class MessageTemplateCategoryRelations
	{
		/// <summary>CTor</summary>
		public MessageTemplateCategoryRelations()
		{
		}

		/// <summary>Gets all relations of the MessageTemplateCategoryEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.MessageTemplateCategoryMessageTemplateEntityUsingMessageTemplateCategoryId);
			toReturn.Add(this.TerminalMessageTemplateCategoryEntityUsingMessageTemplateCategoryId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between MessageTemplateCategoryEntity and MessageTemplateCategoryMessageTemplateEntity over the 1:n relation they have, using the relation between the fields:
		/// MessageTemplateCategory.MessageTemplateCategoryId - MessageTemplateCategoryMessageTemplate.MessageTemplateCategoryId
		/// </summary>
		public virtual IEntityRelation MessageTemplateCategoryMessageTemplateEntityUsingMessageTemplateCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MessageTemplateCategoryMessageTemplateCollection" , true);
				relation.AddEntityFieldPair(MessageTemplateCategoryFields.MessageTemplateCategoryId, MessageTemplateCategoryMessageTemplateFields.MessageTemplateCategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageTemplateCategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageTemplateCategoryMessageTemplateEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between MessageTemplateCategoryEntity and TerminalMessageTemplateCategoryEntity over the 1:n relation they have, using the relation between the fields:
		/// MessageTemplateCategory.MessageTemplateCategoryId - TerminalMessageTemplateCategory.MessageTemplateCategoryId
		/// </summary>
		public virtual IEntityRelation TerminalMessageTemplateCategoryEntityUsingMessageTemplateCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TerminalMessageTemplateCategoryCollection" , true);
				relation.AddEntityFieldPair(MessageTemplateCategoryFields.MessageTemplateCategoryId, TerminalMessageTemplateCategoryFields.MessageTemplateCategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageTemplateCategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalMessageTemplateCategoryEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between MessageTemplateCategoryEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// MessageTemplateCategory.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, MessageTemplateCategoryFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageTemplateCategoryEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticMessageTemplateCategoryRelations
	{
		internal static readonly IEntityRelation MessageTemplateCategoryMessageTemplateEntityUsingMessageTemplateCategoryIdStatic = new MessageTemplateCategoryRelations().MessageTemplateCategoryMessageTemplateEntityUsingMessageTemplateCategoryId;
		internal static readonly IEntityRelation TerminalMessageTemplateCategoryEntityUsingMessageTemplateCategoryIdStatic = new MessageTemplateCategoryRelations().TerminalMessageTemplateCategoryEntityUsingMessageTemplateCategoryId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new MessageTemplateCategoryRelations().CompanyEntityUsingCompanyId;

		/// <summary>CTor</summary>
		static StaticMessageTemplateCategoryRelations()
		{
		}
	}
}
