﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ExternalMenu. </summary>
	public partial class ExternalMenuRelations
	{
		/// <summary>CTor</summary>
		public ExternalMenuRelations()
		{
		}

		/// <summary>Gets all relations of the ExternalMenuEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ExternalProductEntityUsingExternalMenuId);
			toReturn.Add(this.CompanyEntityUsingParentCompanyId);
			toReturn.Add(this.ExternalSystemEntityUsingExternalSystemId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ExternalMenuEntity and ExternalProductEntity over the 1:n relation they have, using the relation between the fields:
		/// ExternalMenu.ExternalMenuId - ExternalProduct.ExternalMenuId
		/// </summary>
		public virtual IEntityRelation ExternalProductEntityUsingExternalMenuId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ExternalProductCollection" , true);
				relation.AddEntityFieldPair(ExternalMenuFields.ExternalMenuId, ExternalProductFields.ExternalMenuId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalMenuEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalProductEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between ExternalMenuEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// ExternalMenu.ParentCompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingParentCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, ExternalMenuFields.ParentCompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalMenuEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ExternalMenuEntity and ExternalSystemEntity over the m:1 relation they have, using the relation between the fields:
		/// ExternalMenu.ExternalSystemId - ExternalSystem.ExternalSystemId
		/// </summary>
		public virtual IEntityRelation ExternalSystemEntityUsingExternalSystemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ExternalSystemEntity", false);
				relation.AddEntityFieldPair(ExternalSystemFields.ExternalSystemId, ExternalMenuFields.ExternalSystemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalSystemEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalMenuEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticExternalMenuRelations
	{
		internal static readonly IEntityRelation ExternalProductEntityUsingExternalMenuIdStatic = new ExternalMenuRelations().ExternalProductEntityUsingExternalMenuId;
		internal static readonly IEntityRelation CompanyEntityUsingParentCompanyIdStatic = new ExternalMenuRelations().CompanyEntityUsingParentCompanyId;
		internal static readonly IEntityRelation ExternalSystemEntityUsingExternalSystemIdStatic = new ExternalMenuRelations().ExternalSystemEntityUsingExternalSystemId;

		/// <summary>CTor</summary>
		static StaticExternalMenuRelations()
		{
		}
	}
}
