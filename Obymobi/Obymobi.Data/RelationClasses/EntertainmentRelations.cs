﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Entertainment. </summary>
	public partial class EntertainmentRelations
	{
		/// <summary>CTor</summary>
		public EntertainmentRelations()
		{
		}

		/// <summary>Gets all relations of the EntertainmentEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AdvertisementEntityUsingActionEntertainmentId);
			toReturn.Add(this.AdvertisementEntityUsingEntertainmentId);
			toReturn.Add(this.AdvertisementTagEntertainmentEntityUsingEntertainmentId);
			toReturn.Add(this.AnnouncementEntityUsingOnYesEntertainment);
			toReturn.Add(this.AvailabilityEntityUsingActionEntertainmentId);
			toReturn.Add(this.ClientEntertainmentEntityUsingEntertainmentId);
			toReturn.Add(this.CloudProcessingTaskEntityUsingEntertainmentId);
			toReturn.Add(this.CompanyEntertainmentEntityUsingEntertainmentId);
			toReturn.Add(this.DeliverypointgroupEntertainmentEntityUsingEntertainmentId);
			toReturn.Add(this.EntertainmentConfigurationEntertainmentEntityUsingEntertainmentId);
			toReturn.Add(this.EntertainmentDependencyEntityUsingDependentEntertainmentId);
			toReturn.Add(this.EntertainmentDependencyEntityUsingEntertainmentId);
			toReturn.Add(this.EntertainmenturlEntityUsingEntertainmentId);
			toReturn.Add(this.MediaEntityUsingActionEntertainmentId);
			toReturn.Add(this.MediaEntityUsingEntertainmentId);
			toReturn.Add(this.MessageEntityUsingEntertainmentId);
			toReturn.Add(this.MessageRecipientEntityUsingEntertainmentId);
			toReturn.Add(this.MessageTemplateEntityUsingEntertainmentId);
			toReturn.Add(this.ScheduledMessageEntityUsingEntertainmentId);
			toReturn.Add(this.TerminalEntityUsingBrowser1);
			toReturn.Add(this.TerminalEntityUsingBrowser2);
			toReturn.Add(this.TerminalEntityUsingCmsPage);
			toReturn.Add(this.UITabEntityUsingEntertainmentId);
			toReturn.Add(this.UIWidgetEntityUsingEntertainmentId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			toReturn.Add(this.EntertainmentcategoryEntityUsingEntertainmentcategoryId);
			toReturn.Add(this.EntertainmentFileEntityUsingEntertainmentFileId);
			toReturn.Add(this.EntertainmenturlEntityUsingDefaultEntertainmenturlId);
			toReturn.Add(this.SiteEntityUsingSiteId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between EntertainmentEntity and AdvertisementEntity over the 1:n relation they have, using the relation between the fields:
		/// Entertainment.EntertainmentId - Advertisement.ActionEntertainmentId
		/// </summary>
		public virtual IEntityRelation AdvertisementEntityUsingActionEntertainmentId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ActionForAdvertisementCollection" , true);
				relation.AddEntityFieldPair(EntertainmentFields.EntertainmentId, AdvertisementFields.ActionEntertainmentId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between EntertainmentEntity and AdvertisementEntity over the 1:n relation they have, using the relation between the fields:
		/// Entertainment.EntertainmentId - Advertisement.EntertainmentId
		/// </summary>
		public virtual IEntityRelation AdvertisementEntityUsingEntertainmentId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AdvertisementCollection" , true);
				relation.AddEntityFieldPair(EntertainmentFields.EntertainmentId, AdvertisementFields.EntertainmentId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between EntertainmentEntity and AdvertisementTagEntertainmentEntity over the 1:n relation they have, using the relation between the fields:
		/// Entertainment.EntertainmentId - AdvertisementTagEntertainment.EntertainmentId
		/// </summary>
		public virtual IEntityRelation AdvertisementTagEntertainmentEntityUsingEntertainmentId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AdvertisementTagEntertainmentCollection" , true);
				relation.AddEntityFieldPair(EntertainmentFields.EntertainmentId, AdvertisementTagEntertainmentFields.EntertainmentId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementTagEntertainmentEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between EntertainmentEntity and AnnouncementEntity over the 1:n relation they have, using the relation between the fields:
		/// Entertainment.EntertainmentId - Announcement.OnYesEntertainment
		/// </summary>
		public virtual IEntityRelation AnnouncementEntityUsingOnYesEntertainment
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AnnouncementCollection" , true);
				relation.AddEntityFieldPair(EntertainmentFields.EntertainmentId, AnnouncementFields.OnYesEntertainment);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AnnouncementEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between EntertainmentEntity and AvailabilityEntity over the 1:n relation they have, using the relation between the fields:
		/// Entertainment.EntertainmentId - Availability.ActionEntertainmentId
		/// </summary>
		public virtual IEntityRelation AvailabilityEntityUsingActionEntertainmentId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AvailabilityCollection" , true);
				relation.AddEntityFieldPair(EntertainmentFields.EntertainmentId, AvailabilityFields.ActionEntertainmentId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AvailabilityEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between EntertainmentEntity and ClientEntertainmentEntity over the 1:n relation they have, using the relation between the fields:
		/// Entertainment.EntertainmentId - ClientEntertainment.EntertainmentId
		/// </summary>
		public virtual IEntityRelation ClientEntertainmentEntityUsingEntertainmentId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ClientEntertainmentCollection" , true);
				relation.AddEntityFieldPair(EntertainmentFields.EntertainmentId, ClientEntertainmentFields.EntertainmentId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntertainmentEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between EntertainmentEntity and CloudProcessingTaskEntity over the 1:n relation they have, using the relation between the fields:
		/// Entertainment.EntertainmentId - CloudProcessingTask.EntertainmentId
		/// </summary>
		public virtual IEntityRelation CloudProcessingTaskEntityUsingEntertainmentId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CloudProcessingTaskCollection" , true);
				relation.AddEntityFieldPair(EntertainmentFields.EntertainmentId, CloudProcessingTaskFields.EntertainmentId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CloudProcessingTaskEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between EntertainmentEntity and CompanyEntertainmentEntity over the 1:n relation they have, using the relation between the fields:
		/// Entertainment.EntertainmentId - CompanyEntertainment.EntertainmentId
		/// </summary>
		public virtual IEntityRelation CompanyEntertainmentEntityUsingEntertainmentId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CompanyEntertainmentCollection" , true);
				relation.AddEntityFieldPair(EntertainmentFields.EntertainmentId, CompanyEntertainmentFields.EntertainmentId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntertainmentEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between EntertainmentEntity and DeliverypointgroupEntertainmentEntity over the 1:n relation they have, using the relation between the fields:
		/// Entertainment.EntertainmentId - DeliverypointgroupEntertainment.EntertainmentId
		/// </summary>
		public virtual IEntityRelation DeliverypointgroupEntertainmentEntityUsingEntertainmentId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeliverypointgroupEntertainmentCollection" , true);
				relation.AddEntityFieldPair(EntertainmentFields.EntertainmentId, DeliverypointgroupEntertainmentFields.EntertainmentId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntertainmentEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between EntertainmentEntity and EntertainmentConfigurationEntertainmentEntity over the 1:n relation they have, using the relation between the fields:
		/// Entertainment.EntertainmentId - EntertainmentConfigurationEntertainment.EntertainmentId
		/// </summary>
		public virtual IEntityRelation EntertainmentConfigurationEntertainmentEntityUsingEntertainmentId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "EntertainmentConfigurationEntertainmentCollection" , true);
				relation.AddEntityFieldPair(EntertainmentFields.EntertainmentId, EntertainmentConfigurationEntertainmentFields.EntertainmentId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentConfigurationEntertainmentEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between EntertainmentEntity and EntertainmentDependencyEntity over the 1:n relation they have, using the relation between the fields:
		/// Entertainment.EntertainmentId - EntertainmentDependency.DependentEntertainmentId
		/// </summary>
		public virtual IEntityRelation EntertainmentDependencyEntityUsingDependentEntertainmentId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DependentEntertainmentDependencyCollection" , true);
				relation.AddEntityFieldPair(EntertainmentFields.EntertainmentId, EntertainmentDependencyFields.DependentEntertainmentId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentDependencyEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between EntertainmentEntity and EntertainmentDependencyEntity over the 1:n relation they have, using the relation between the fields:
		/// Entertainment.EntertainmentId - EntertainmentDependency.EntertainmentId
		/// </summary>
		public virtual IEntityRelation EntertainmentDependencyEntityUsingEntertainmentId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "EntertainmentDependencyCollection" , true);
				relation.AddEntityFieldPair(EntertainmentFields.EntertainmentId, EntertainmentDependencyFields.EntertainmentId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentDependencyEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between EntertainmentEntity and EntertainmenturlEntity over the 1:n relation they have, using the relation between the fields:
		/// Entertainment.EntertainmentId - Entertainmenturl.EntertainmentId
		/// </summary>
		public virtual IEntityRelation EntertainmenturlEntityUsingEntertainmentId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "EntertainmenturlCollection" , true);
				relation.AddEntityFieldPair(EntertainmentFields.EntertainmentId, EntertainmenturlFields.EntertainmentId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmenturlEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between EntertainmentEntity and MediaEntity over the 1:n relation they have, using the relation between the fields:
		/// Entertainment.EntertainmentId - Media.ActionEntertainmentId
		/// </summary>
		public virtual IEntityRelation MediaEntityUsingActionEntertainmentId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ActionMediaCollection" , true);
				relation.AddEntityFieldPair(EntertainmentFields.EntertainmentId, MediaFields.ActionEntertainmentId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between EntertainmentEntity and MediaEntity over the 1:n relation they have, using the relation between the fields:
		/// Entertainment.EntertainmentId - Media.EntertainmentId
		/// </summary>
		public virtual IEntityRelation MediaEntityUsingEntertainmentId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MediaCollection" , true);
				relation.AddEntityFieldPair(EntertainmentFields.EntertainmentId, MediaFields.EntertainmentId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between EntertainmentEntity and MessageEntity over the 1:n relation they have, using the relation between the fields:
		/// Entertainment.EntertainmentId - Message.EntertainmentId
		/// </summary>
		public virtual IEntityRelation MessageEntityUsingEntertainmentId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MessageCollection" , true);
				relation.AddEntityFieldPair(EntertainmentFields.EntertainmentId, MessageFields.EntertainmentId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between EntertainmentEntity and MessageRecipientEntity over the 1:n relation they have, using the relation between the fields:
		/// Entertainment.EntertainmentId - MessageRecipient.EntertainmentId
		/// </summary>
		public virtual IEntityRelation MessageRecipientEntityUsingEntertainmentId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MessageRecipientCollection" , true);
				relation.AddEntityFieldPair(EntertainmentFields.EntertainmentId, MessageRecipientFields.EntertainmentId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageRecipientEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between EntertainmentEntity and MessageTemplateEntity over the 1:n relation they have, using the relation between the fields:
		/// Entertainment.EntertainmentId - MessageTemplate.EntertainmentId
		/// </summary>
		public virtual IEntityRelation MessageTemplateEntityUsingEntertainmentId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MessageTemplateCollection" , true);
				relation.AddEntityFieldPair(EntertainmentFields.EntertainmentId, MessageTemplateFields.EntertainmentId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageTemplateEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between EntertainmentEntity and ScheduledMessageEntity over the 1:n relation they have, using the relation between the fields:
		/// Entertainment.EntertainmentId - ScheduledMessage.EntertainmentId
		/// </summary>
		public virtual IEntityRelation ScheduledMessageEntityUsingEntertainmentId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ScheduledMessageCollection" , true);
				relation.AddEntityFieldPair(EntertainmentFields.EntertainmentId, ScheduledMessageFields.EntertainmentId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduledMessageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between EntertainmentEntity and TerminalEntity over the 1:n relation they have, using the relation between the fields:
		/// Entertainment.EntertainmentId - Terminal.Browser1
		/// </summary>
		public virtual IEntityRelation TerminalEntityUsingBrowser1
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TerminalCollection" , true);
				relation.AddEntityFieldPair(EntertainmentFields.EntertainmentId, TerminalFields.Browser1);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between EntertainmentEntity and TerminalEntity over the 1:n relation they have, using the relation between the fields:
		/// Entertainment.EntertainmentId - Terminal.Browser2
		/// </summary>
		public virtual IEntityRelation TerminalEntityUsingBrowser2
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TerminalCollection_" , true);
				relation.AddEntityFieldPair(EntertainmentFields.EntertainmentId, TerminalFields.Browser2);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between EntertainmentEntity and TerminalEntity over the 1:n relation they have, using the relation between the fields:
		/// Entertainment.EntertainmentId - Terminal.CmsPage
		/// </summary>
		public virtual IEntityRelation TerminalEntityUsingCmsPage
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TerminalCollection__" , true);
				relation.AddEntityFieldPair(EntertainmentFields.EntertainmentId, TerminalFields.CmsPage);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between EntertainmentEntity and UITabEntity over the 1:n relation they have, using the relation between the fields:
		/// Entertainment.EntertainmentId - UITab.EntertainmentId
		/// </summary>
		public virtual IEntityRelation UITabEntityUsingEntertainmentId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UITabCollection" , true);
				relation.AddEntityFieldPair(EntertainmentFields.EntertainmentId, UITabFields.EntertainmentId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UITabEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between EntertainmentEntity and UIWidgetEntity over the 1:n relation they have, using the relation between the fields:
		/// Entertainment.EntertainmentId - UIWidget.EntertainmentId
		/// </summary>
		public virtual IEntityRelation UIWidgetEntityUsingEntertainmentId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UIWidgetCollection" , true);
				relation.AddEntityFieldPair(EntertainmentFields.EntertainmentId, UIWidgetFields.EntertainmentId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIWidgetEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between EntertainmentEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// Entertainment.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, EntertainmentFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between EntertainmentEntity and EntertainmentcategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// Entertainment.EntertainmentcategoryId - Entertainmentcategory.EntertainmentcategoryId
		/// </summary>
		public virtual IEntityRelation EntertainmentcategoryEntityUsingEntertainmentcategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "EntertainmentcategoryEntity", false);
				relation.AddEntityFieldPair(EntertainmentcategoryFields.EntertainmentcategoryId, EntertainmentFields.EntertainmentcategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentcategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between EntertainmentEntity and EntertainmentFileEntity over the m:1 relation they have, using the relation between the fields:
		/// Entertainment.EntertainmentFileId - EntertainmentFile.EntertainmentFileId
		/// </summary>
		public virtual IEntityRelation EntertainmentFileEntityUsingEntertainmentFileId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "EntertainmentFileEntity", false);
				relation.AddEntityFieldPair(EntertainmentFileFields.EntertainmentFileId, EntertainmentFields.EntertainmentFileId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentFileEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between EntertainmentEntity and EntertainmenturlEntity over the m:1 relation they have, using the relation between the fields:
		/// Entertainment.DefaultEntertainmenturlId - Entertainmenturl.EntertainmenturlId
		/// </summary>
		public virtual IEntityRelation EntertainmenturlEntityUsingDefaultEntertainmenturlId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "EntertainmenturlEntity", false);
				relation.AddEntityFieldPair(EntertainmenturlFields.EntertainmenturlId, EntertainmentFields.DefaultEntertainmenturlId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmenturlEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between EntertainmentEntity and SiteEntity over the m:1 relation they have, using the relation between the fields:
		/// Entertainment.SiteId - Site.SiteId
		/// </summary>
		public virtual IEntityRelation SiteEntityUsingSiteId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SiteEntity", false);
				relation.AddEntityFieldPair(SiteFields.SiteId, EntertainmentFields.SiteId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SiteEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticEntertainmentRelations
	{
		internal static readonly IEntityRelation AdvertisementEntityUsingActionEntertainmentIdStatic = new EntertainmentRelations().AdvertisementEntityUsingActionEntertainmentId;
		internal static readonly IEntityRelation AdvertisementEntityUsingEntertainmentIdStatic = new EntertainmentRelations().AdvertisementEntityUsingEntertainmentId;
		internal static readonly IEntityRelation AdvertisementTagEntertainmentEntityUsingEntertainmentIdStatic = new EntertainmentRelations().AdvertisementTagEntertainmentEntityUsingEntertainmentId;
		internal static readonly IEntityRelation AnnouncementEntityUsingOnYesEntertainmentStatic = new EntertainmentRelations().AnnouncementEntityUsingOnYesEntertainment;
		internal static readonly IEntityRelation AvailabilityEntityUsingActionEntertainmentIdStatic = new EntertainmentRelations().AvailabilityEntityUsingActionEntertainmentId;
		internal static readonly IEntityRelation ClientEntertainmentEntityUsingEntertainmentIdStatic = new EntertainmentRelations().ClientEntertainmentEntityUsingEntertainmentId;
		internal static readonly IEntityRelation CloudProcessingTaskEntityUsingEntertainmentIdStatic = new EntertainmentRelations().CloudProcessingTaskEntityUsingEntertainmentId;
		internal static readonly IEntityRelation CompanyEntertainmentEntityUsingEntertainmentIdStatic = new EntertainmentRelations().CompanyEntertainmentEntityUsingEntertainmentId;
		internal static readonly IEntityRelation DeliverypointgroupEntertainmentEntityUsingEntertainmentIdStatic = new EntertainmentRelations().DeliverypointgroupEntertainmentEntityUsingEntertainmentId;
		internal static readonly IEntityRelation EntertainmentConfigurationEntertainmentEntityUsingEntertainmentIdStatic = new EntertainmentRelations().EntertainmentConfigurationEntertainmentEntityUsingEntertainmentId;
		internal static readonly IEntityRelation EntertainmentDependencyEntityUsingDependentEntertainmentIdStatic = new EntertainmentRelations().EntertainmentDependencyEntityUsingDependentEntertainmentId;
		internal static readonly IEntityRelation EntertainmentDependencyEntityUsingEntertainmentIdStatic = new EntertainmentRelations().EntertainmentDependencyEntityUsingEntertainmentId;
		internal static readonly IEntityRelation EntertainmenturlEntityUsingEntertainmentIdStatic = new EntertainmentRelations().EntertainmenturlEntityUsingEntertainmentId;
		internal static readonly IEntityRelation MediaEntityUsingActionEntertainmentIdStatic = new EntertainmentRelations().MediaEntityUsingActionEntertainmentId;
		internal static readonly IEntityRelation MediaEntityUsingEntertainmentIdStatic = new EntertainmentRelations().MediaEntityUsingEntertainmentId;
		internal static readonly IEntityRelation MessageEntityUsingEntertainmentIdStatic = new EntertainmentRelations().MessageEntityUsingEntertainmentId;
		internal static readonly IEntityRelation MessageRecipientEntityUsingEntertainmentIdStatic = new EntertainmentRelations().MessageRecipientEntityUsingEntertainmentId;
		internal static readonly IEntityRelation MessageTemplateEntityUsingEntertainmentIdStatic = new EntertainmentRelations().MessageTemplateEntityUsingEntertainmentId;
		internal static readonly IEntityRelation ScheduledMessageEntityUsingEntertainmentIdStatic = new EntertainmentRelations().ScheduledMessageEntityUsingEntertainmentId;
		internal static readonly IEntityRelation TerminalEntityUsingBrowser1Static = new EntertainmentRelations().TerminalEntityUsingBrowser1;
		internal static readonly IEntityRelation TerminalEntityUsingBrowser2Static = new EntertainmentRelations().TerminalEntityUsingBrowser2;
		internal static readonly IEntityRelation TerminalEntityUsingCmsPageStatic = new EntertainmentRelations().TerminalEntityUsingCmsPage;
		internal static readonly IEntityRelation UITabEntityUsingEntertainmentIdStatic = new EntertainmentRelations().UITabEntityUsingEntertainmentId;
		internal static readonly IEntityRelation UIWidgetEntityUsingEntertainmentIdStatic = new EntertainmentRelations().UIWidgetEntityUsingEntertainmentId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new EntertainmentRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation EntertainmentcategoryEntityUsingEntertainmentcategoryIdStatic = new EntertainmentRelations().EntertainmentcategoryEntityUsingEntertainmentcategoryId;
		internal static readonly IEntityRelation EntertainmentFileEntityUsingEntertainmentFileIdStatic = new EntertainmentRelations().EntertainmentFileEntityUsingEntertainmentFileId;
		internal static readonly IEntityRelation EntertainmenturlEntityUsingDefaultEntertainmenturlIdStatic = new EntertainmentRelations().EntertainmenturlEntityUsingDefaultEntertainmenturlId;
		internal static readonly IEntityRelation SiteEntityUsingSiteIdStatic = new EntertainmentRelations().SiteEntityUsingSiteId;

		/// <summary>CTor</summary>
		static StaticEntertainmentRelations()
		{
		}
	}
}
