﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ClientConfigurationRoute. </summary>
	public partial class ClientConfigurationRouteRelations
	{
		/// <summary>CTor</summary>
		public ClientConfigurationRouteRelations()
		{
		}

		/// <summary>Gets all relations of the ClientConfigurationRouteEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ClientConfigurationEntityUsingClientConfigurationId);
			toReturn.Add(this.RouteEntityUsingRouteId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between ClientConfigurationRouteEntity and ClientConfigurationEntity over the m:1 relation they have, using the relation between the fields:
		/// ClientConfigurationRoute.ClientConfigurationId - ClientConfiguration.ClientConfigurationId
		/// </summary>
		public virtual IEntityRelation ClientConfigurationEntityUsingClientConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ClientConfigurationEntity", false);
				relation.AddEntityFieldPair(ClientConfigurationFields.ClientConfigurationId, ClientConfigurationRouteFields.ClientConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientConfigurationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientConfigurationRouteEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ClientConfigurationRouteEntity and RouteEntity over the m:1 relation they have, using the relation between the fields:
		/// ClientConfigurationRoute.RouteId - Route.RouteId
		/// </summary>
		public virtual IEntityRelation RouteEntityUsingRouteId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RouteEntity", false);
				relation.AddEntityFieldPair(RouteFields.RouteId, ClientConfigurationRouteFields.RouteId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RouteEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientConfigurationRouteEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticClientConfigurationRouteRelations
	{
		internal static readonly IEntityRelation ClientConfigurationEntityUsingClientConfigurationIdStatic = new ClientConfigurationRouteRelations().ClientConfigurationEntityUsingClientConfigurationId;
		internal static readonly IEntityRelation RouteEntityUsingRouteIdStatic = new ClientConfigurationRouteRelations().RouteEntityUsingRouteId;

		/// <summary>CTor</summary>
		static StaticClientConfigurationRouteRelations()
		{
		}
	}
}
