﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Message. </summary>
	public partial class MessageRelations
	{
		/// <summary>CTor</summary>
		public MessageRelations()
		{
		}

		/// <summary>Gets all relations of the MessageEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.MessageRecipientEntityUsingMessageId);
			toReturn.Add(this.ScheduledMessageHistoryEntityUsingMessageId);
			toReturn.Add(this.CategoryEntityUsingCategoryId);
			toReturn.Add(this.ClientEntityUsingClientId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			toReturn.Add(this.CustomerEntityUsingCustomerId);
			toReturn.Add(this.DeliverypointEntityUsingDeliverypointId);
			toReturn.Add(this.EntertainmentEntityUsingEntertainmentId);
			toReturn.Add(this.MediaEntityUsingMediaId);
			toReturn.Add(this.OrderEntityUsingOrderId);
			toReturn.Add(this.PageEntityUsingPageId);
			toReturn.Add(this.ProductCategoryEntityUsingProductCategoryId);
			toReturn.Add(this.SiteEntityUsingSiteId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between MessageEntity and MessageRecipientEntity over the 1:n relation they have, using the relation between the fields:
		/// Message.MessageId - MessageRecipient.MessageId
		/// </summary>
		public virtual IEntityRelation MessageRecipientEntityUsingMessageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MessageRecipientCollection" , true);
				relation.AddEntityFieldPair(MessageFields.MessageId, MessageRecipientFields.MessageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageRecipientEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between MessageEntity and ScheduledMessageHistoryEntity over the 1:n relation they have, using the relation between the fields:
		/// Message.MessageId - ScheduledMessageHistory.MessageId
		/// </summary>
		public virtual IEntityRelation ScheduledMessageHistoryEntityUsingMessageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ScheduledMessageHistoryCollection" , true);
				relation.AddEntityFieldPair(MessageFields.MessageId, ScheduledMessageHistoryFields.MessageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduledMessageHistoryEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between MessageEntity and CategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// Message.CategoryId - Category.CategoryId
		/// </summary>
		public virtual IEntityRelation CategoryEntityUsingCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CategoryEntity", false);
				relation.AddEntityFieldPair(CategoryFields.CategoryId, MessageFields.CategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MessageEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// Message.ClientId - Client.ClientId
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingClientId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ClientEntity", false);
				relation.AddEntityFieldPair(ClientFields.ClientId, MessageFields.ClientId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MessageEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// Message.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, MessageFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MessageEntity and CustomerEntity over the m:1 relation they have, using the relation between the fields:
		/// Message.CustomerId - Customer.CustomerId
		/// </summary>
		public virtual IEntityRelation CustomerEntityUsingCustomerId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CustomerEntity", false);
				relation.AddEntityFieldPair(CustomerFields.CustomerId, MessageFields.CustomerId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomerEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MessageEntity and DeliverypointEntity over the m:1 relation they have, using the relation between the fields:
		/// Message.DeliverypointId - Deliverypoint.DeliverypointId
		/// </summary>
		public virtual IEntityRelation DeliverypointEntityUsingDeliverypointId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DeliverypointEntity", false);
				relation.AddEntityFieldPair(DeliverypointFields.DeliverypointId, MessageFields.DeliverypointId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MessageEntity and EntertainmentEntity over the m:1 relation they have, using the relation between the fields:
		/// Message.EntertainmentId - Entertainment.EntertainmentId
		/// </summary>
		public virtual IEntityRelation EntertainmentEntityUsingEntertainmentId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "EntertainmentEntity", false);
				relation.AddEntityFieldPair(EntertainmentFields.EntertainmentId, MessageFields.EntertainmentId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MessageEntity and MediaEntity over the m:1 relation they have, using the relation between the fields:
		/// Message.MediaId - Media.MediaId
		/// </summary>
		public virtual IEntityRelation MediaEntityUsingMediaId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "MediaEntity", false);
				relation.AddEntityFieldPair(MediaFields.MediaId, MessageFields.MediaId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MessageEntity and OrderEntity over the m:1 relation they have, using the relation between the fields:
		/// Message.OrderId - Order.OrderId
		/// </summary>
		public virtual IEntityRelation OrderEntityUsingOrderId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "OrderEntity", false);
				relation.AddEntityFieldPair(OrderFields.OrderId, MessageFields.OrderId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MessageEntity and PageEntity over the m:1 relation they have, using the relation between the fields:
		/// Message.PageId - Page.PageId
		/// </summary>
		public virtual IEntityRelation PageEntityUsingPageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PageEntity", false);
				relation.AddEntityFieldPair(PageFields.PageId, MessageFields.PageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MessageEntity and ProductCategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// Message.ProductCategoryId - ProductCategory.ProductCategoryId
		/// </summary>
		public virtual IEntityRelation ProductCategoryEntityUsingProductCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ProductCategoryEntity", false);
				relation.AddEntityFieldPair(ProductCategoryFields.ProductCategoryId, MessageFields.ProductCategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductCategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MessageEntity and SiteEntity over the m:1 relation they have, using the relation between the fields:
		/// Message.SiteId - Site.SiteId
		/// </summary>
		public virtual IEntityRelation SiteEntityUsingSiteId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SiteEntity", false);
				relation.AddEntityFieldPair(SiteFields.SiteId, MessageFields.SiteId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SiteEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticMessageRelations
	{
		internal static readonly IEntityRelation MessageRecipientEntityUsingMessageIdStatic = new MessageRelations().MessageRecipientEntityUsingMessageId;
		internal static readonly IEntityRelation ScheduledMessageHistoryEntityUsingMessageIdStatic = new MessageRelations().ScheduledMessageHistoryEntityUsingMessageId;
		internal static readonly IEntityRelation CategoryEntityUsingCategoryIdStatic = new MessageRelations().CategoryEntityUsingCategoryId;
		internal static readonly IEntityRelation ClientEntityUsingClientIdStatic = new MessageRelations().ClientEntityUsingClientId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new MessageRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation CustomerEntityUsingCustomerIdStatic = new MessageRelations().CustomerEntityUsingCustomerId;
		internal static readonly IEntityRelation DeliverypointEntityUsingDeliverypointIdStatic = new MessageRelations().DeliverypointEntityUsingDeliverypointId;
		internal static readonly IEntityRelation EntertainmentEntityUsingEntertainmentIdStatic = new MessageRelations().EntertainmentEntityUsingEntertainmentId;
		internal static readonly IEntityRelation MediaEntityUsingMediaIdStatic = new MessageRelations().MediaEntityUsingMediaId;
		internal static readonly IEntityRelation OrderEntityUsingOrderIdStatic = new MessageRelations().OrderEntityUsingOrderId;
		internal static readonly IEntityRelation PageEntityUsingPageIdStatic = new MessageRelations().PageEntityUsingPageId;
		internal static readonly IEntityRelation ProductCategoryEntityUsingProductCategoryIdStatic = new MessageRelations().ProductCategoryEntityUsingProductCategoryId;
		internal static readonly IEntityRelation SiteEntityUsingSiteIdStatic = new MessageRelations().SiteEntityUsingSiteId;

		/// <summary>CTor</summary>
		static StaticMessageRelations()
		{
		}
	}
}
