﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: TerminalLog. </summary>
	public partial class TerminalLogRelations
	{
		/// <summary>CTor</summary>
		public TerminalLogRelations()
		{
		}

		/// <summary>Gets all relations of the TerminalLogEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.OrderEntityUsingOrderId);
			toReturn.Add(this.TerminalEntityUsingTerminalId);
			toReturn.Add(this.TerminalLogFileEntityUsingTerminalLogFileId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between TerminalLogEntity and OrderEntity over the m:1 relation they have, using the relation between the fields:
		/// TerminalLog.OrderId - Order.OrderId
		/// </summary>
		public virtual IEntityRelation OrderEntityUsingOrderId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "OrderEntity", false);
				relation.AddEntityFieldPair(OrderFields.OrderId, TerminalLogFields.OrderId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalLogEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TerminalLogEntity and TerminalEntity over the m:1 relation they have, using the relation between the fields:
		/// TerminalLog.TerminalId - Terminal.TerminalId
		/// </summary>
		public virtual IEntityRelation TerminalEntityUsingTerminalId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TerminalEntity", false);
				relation.AddEntityFieldPair(TerminalFields.TerminalId, TerminalLogFields.TerminalId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalLogEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TerminalLogEntity and TerminalLogFileEntity over the m:1 relation they have, using the relation between the fields:
		/// TerminalLog.TerminalLogFileId - TerminalLogFile.TerminalLogFileId
		/// </summary>
		public virtual IEntityRelation TerminalLogFileEntityUsingTerminalLogFileId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TerminalLogFileEntity", false);
				relation.AddEntityFieldPair(TerminalLogFileFields.TerminalLogFileId, TerminalLogFields.TerminalLogFileId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalLogFileEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalLogEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticTerminalLogRelations
	{
		internal static readonly IEntityRelation OrderEntityUsingOrderIdStatic = new TerminalLogRelations().OrderEntityUsingOrderId;
		internal static readonly IEntityRelation TerminalEntityUsingTerminalIdStatic = new TerminalLogRelations().TerminalEntityUsingTerminalId;
		internal static readonly IEntityRelation TerminalLogFileEntityUsingTerminalLogFileIdStatic = new TerminalLogRelations().TerminalLogFileEntityUsingTerminalLogFileId;

		/// <summary>CTor</summary>
		static StaticTerminalLogRelations()
		{
		}
	}
}
