﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: UIThemeTextSize. </summary>
	public partial class UIThemeTextSizeRelations
	{
		/// <summary>CTor</summary>
		public UIThemeTextSizeRelations()
		{
		}

		/// <summary>Gets all relations of the UIThemeTextSizeEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.UIThemeEntityUsingUIThemeId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between UIThemeTextSizeEntity and UIThemeEntity over the m:1 relation they have, using the relation between the fields:
		/// UIThemeTextSize.UIThemeId - UITheme.UIThemeId
		/// </summary>
		public virtual IEntityRelation UIThemeEntityUsingUIThemeId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UIThemeEntity", false);
				relation.AddEntityFieldPair(UIThemeFields.UIThemeId, UIThemeTextSizeFields.UIThemeId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIThemeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIThemeTextSizeEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticUIThemeTextSizeRelations
	{
		internal static readonly IEntityRelation UIThemeEntityUsingUIThemeIdStatic = new UIThemeTextSizeRelations().UIThemeEntityUsingUIThemeId;

		/// <summary>CTor</summary>
		static StaticUIThemeTextSizeRelations()
		{
		}
	}
}
