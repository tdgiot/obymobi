﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Vattariff. </summary>
	public partial class VattariffRelations
	{
		/// <summary>CTor</summary>
		public VattariffRelations()
		{
		}

		/// <summary>Gets all relations of the VattariffEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.GenericproductEntityUsingVattariffId);
			toReturn.Add(this.ProductEntityUsingVattariffId);
			toReturn.Add(this.CountryEntityUsingCountryId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between VattariffEntity and GenericproductEntity over the 1:n relation they have, using the relation between the fields:
		/// Vattariff.VattariffId - Genericproduct.VattariffId
		/// </summary>
		public virtual IEntityRelation GenericproductEntityUsingVattariffId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "GenericproductCollection" , true);
				relation.AddEntityFieldPair(VattariffFields.VattariffId, GenericproductFields.VattariffId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VattariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericproductEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between VattariffEntity and ProductEntity over the 1:n relation they have, using the relation between the fields:
		/// Vattariff.VattariffId - Product.VattariffId
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingVattariffId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ProductCollection" , true);
				relation.AddEntityFieldPair(VattariffFields.VattariffId, ProductFields.VattariffId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VattariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between VattariffEntity and CountryEntity over the m:1 relation they have, using the relation between the fields:
		/// Vattariff.CountryId - Country.CountryId
		/// </summary>
		public virtual IEntityRelation CountryEntityUsingCountryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CountryEntity", false);
				relation.AddEntityFieldPair(CountryFields.CountryId, VattariffFields.CountryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CountryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VattariffEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticVattariffRelations
	{
		internal static readonly IEntityRelation GenericproductEntityUsingVattariffIdStatic = new VattariffRelations().GenericproductEntityUsingVattariffId;
		internal static readonly IEntityRelation ProductEntityUsingVattariffIdStatic = new VattariffRelations().ProductEntityUsingVattariffId;
		internal static readonly IEntityRelation CountryEntityUsingCountryIdStatic = new VattariffRelations().CountryEntityUsingCountryId;

		/// <summary>CTor</summary>
		static StaticVattariffRelations()
		{
		}
	}
}
