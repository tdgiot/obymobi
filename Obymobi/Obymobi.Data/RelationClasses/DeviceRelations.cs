﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Device. </summary>
	public partial class DeviceRelations
	{
		/// <summary>CTor</summary>
		public DeviceRelations()
		{
		}

		/// <summary>Gets all relations of the DeviceEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ClientEntityUsingDeviceId);
			toReturn.Add(this.DeliverypointEntityUsingDeviceId);
			toReturn.Add(this.DevicemediaEntityUsingDeviceId);
			toReturn.Add(this.DeviceTokenHistoryEntityUsingDeviceId);
			toReturn.Add(this.DeviceTokenTaskEntityUsingDeviceId);
			toReturn.Add(this.TerminalEntityUsingDeviceId);
			toReturn.Add(this.CustomerEntityUsingCustomerId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between DeviceEntity and ClientEntity over the 1:n relation they have, using the relation between the fields:
		/// Device.DeviceId - Client.DeviceId
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingDeviceId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ClientCollection" , true);
				relation.AddEntityFieldPair(DeviceFields.DeviceId, ClientFields.DeviceId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeviceEntity and DeliverypointEntity over the 1:n relation they have, using the relation between the fields:
		/// Device.DeviceId - Deliverypoint.DeviceId
		/// </summary>
		public virtual IEntityRelation DeliverypointEntityUsingDeviceId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeliverypointCollection" , true);
				relation.AddEntityFieldPair(DeviceFields.DeviceId, DeliverypointFields.DeviceId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeviceEntity and DevicemediaEntity over the 1:n relation they have, using the relation between the fields:
		/// Device.DeviceId - Devicemedia.DeviceId
		/// </summary>
		public virtual IEntityRelation DevicemediaEntityUsingDeviceId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DevicemediumCollection" , true);
				relation.AddEntityFieldPair(DeviceFields.DeviceId, DevicemediaFields.DeviceId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DevicemediaEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeviceEntity and DeviceTokenHistoryEntity over the 1:n relation they have, using the relation between the fields:
		/// Device.DeviceId - DeviceTokenHistory.DeviceId
		/// </summary>
		public virtual IEntityRelation DeviceTokenHistoryEntityUsingDeviceId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeviceTokenHistoryCollection" , true);
				relation.AddEntityFieldPair(DeviceFields.DeviceId, DeviceTokenHistoryFields.DeviceId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceTokenHistoryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeviceEntity and DeviceTokenTaskEntity over the 1:n relation they have, using the relation between the fields:
		/// Device.DeviceId - DeviceTokenTask.DeviceId
		/// </summary>
		public virtual IEntityRelation DeviceTokenTaskEntityUsingDeviceId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeviceTokenTaskCollection" , true);
				relation.AddEntityFieldPair(DeviceFields.DeviceId, DeviceTokenTaskFields.DeviceId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceTokenTaskEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeviceEntity and TerminalEntity over the 1:n relation they have, using the relation between the fields:
		/// Device.DeviceId - Terminal.DeviceId
		/// </summary>
		public virtual IEntityRelation TerminalEntityUsingDeviceId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TerminalCollection" , true);
				relation.AddEntityFieldPair(DeviceFields.DeviceId, TerminalFields.DeviceId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between DeviceEntity and CustomerEntity over the m:1 relation they have, using the relation between the fields:
		/// Device.CustomerId - Customer.CustomerId
		/// </summary>
		public virtual IEntityRelation CustomerEntityUsingCustomerId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CustomerEntity", false);
				relation.AddEntityFieldPair(CustomerFields.CustomerId, DeviceFields.CustomerId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomerEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticDeviceRelations
	{
		internal static readonly IEntityRelation ClientEntityUsingDeviceIdStatic = new DeviceRelations().ClientEntityUsingDeviceId;
		internal static readonly IEntityRelation DeliverypointEntityUsingDeviceIdStatic = new DeviceRelations().DeliverypointEntityUsingDeviceId;
		internal static readonly IEntityRelation DevicemediaEntityUsingDeviceIdStatic = new DeviceRelations().DevicemediaEntityUsingDeviceId;
		internal static readonly IEntityRelation DeviceTokenHistoryEntityUsingDeviceIdStatic = new DeviceRelations().DeviceTokenHistoryEntityUsingDeviceId;
		internal static readonly IEntityRelation DeviceTokenTaskEntityUsingDeviceIdStatic = new DeviceRelations().DeviceTokenTaskEntityUsingDeviceId;
		internal static readonly IEntityRelation TerminalEntityUsingDeviceIdStatic = new DeviceRelations().TerminalEntityUsingDeviceId;
		internal static readonly IEntityRelation CustomerEntityUsingCustomerIdStatic = new DeviceRelations().CustomerEntityUsingCustomerId;

		/// <summary>CTor</summary>
		static StaticDeviceRelations()
		{
		}
	}
}
