﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Customer. </summary>
	public partial class CustomerRelations
	{
		/// <summary>CTor</summary>
		public CustomerRelations()
		{
		}

		/// <summary>Gets all relations of the CustomerEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.DeviceEntityUsingCustomerId);
			toReturn.Add(this.MessageEntityUsingCustomerId);
			toReturn.Add(this.MessageRecipientEntityUsingCustomerId);
			toReturn.Add(this.NetmessageEntityUsingReceiverCustomerId);
			toReturn.Add(this.NetmessageEntityUsingSenderCustomerId);
			toReturn.Add(this.OrderEntityUsingCustomerId);
			toReturn.Add(this.SurveyResultEntityUsingCustomerId);
			toReturn.Add(this.ClientEntityUsingClientId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between CustomerEntity and DeviceEntity over the 1:n relation they have, using the relation between the fields:
		/// Customer.CustomerId - Device.CustomerId
		/// </summary>
		public virtual IEntityRelation DeviceEntityUsingCustomerId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeviceCollection" , true);
				relation.AddEntityFieldPair(CustomerFields.CustomerId, DeviceFields.CustomerId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomerEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CustomerEntity and MessageEntity over the 1:n relation they have, using the relation between the fields:
		/// Customer.CustomerId - Message.CustomerId
		/// </summary>
		public virtual IEntityRelation MessageEntityUsingCustomerId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MessageCollection" , true);
				relation.AddEntityFieldPair(CustomerFields.CustomerId, MessageFields.CustomerId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomerEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CustomerEntity and MessageRecipientEntity over the 1:n relation they have, using the relation between the fields:
		/// Customer.CustomerId - MessageRecipient.CustomerId
		/// </summary>
		public virtual IEntityRelation MessageRecipientEntityUsingCustomerId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MessageRecipientCollection" , true);
				relation.AddEntityFieldPair(CustomerFields.CustomerId, MessageRecipientFields.CustomerId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomerEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageRecipientEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CustomerEntity and NetmessageEntity over the 1:n relation they have, using the relation between the fields:
		/// Customer.CustomerId - Netmessage.ReceiverCustomerId
		/// </summary>
		public virtual IEntityRelation NetmessageEntityUsingReceiverCustomerId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ReceivedNetmessageCollection" , true);
				relation.AddEntityFieldPair(CustomerFields.CustomerId, NetmessageFields.ReceiverCustomerId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomerEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NetmessageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CustomerEntity and NetmessageEntity over the 1:n relation they have, using the relation between the fields:
		/// Customer.CustomerId - Netmessage.SenderCustomerId
		/// </summary>
		public virtual IEntityRelation NetmessageEntityUsingSenderCustomerId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SentNetmessageCollection" , true);
				relation.AddEntityFieldPair(CustomerFields.CustomerId, NetmessageFields.SenderCustomerId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomerEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NetmessageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CustomerEntity and OrderEntity over the 1:n relation they have, using the relation between the fields:
		/// Customer.CustomerId - Order.CustomerId
		/// </summary>
		public virtual IEntityRelation OrderEntityUsingCustomerId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrderCollection" , true);
				relation.AddEntityFieldPair(CustomerFields.CustomerId, OrderFields.CustomerId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomerEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CustomerEntity and SurveyResultEntity over the 1:n relation they have, using the relation between the fields:
		/// Customer.CustomerId - SurveyResult.CustomerId
		/// </summary>
		public virtual IEntityRelation SurveyResultEntityUsingCustomerId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SurveyResultCollection" , true);
				relation.AddEntityFieldPair(CustomerFields.CustomerId, SurveyResultFields.CustomerId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomerEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyResultEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between CustomerEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// Customer.ClientId - Client.ClientId
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingClientId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ClientEntity", false);
				relation.AddEntityFieldPair(ClientFields.ClientId, CustomerFields.ClientId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomerEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCustomerRelations
	{
		internal static readonly IEntityRelation DeviceEntityUsingCustomerIdStatic = new CustomerRelations().DeviceEntityUsingCustomerId;
		internal static readonly IEntityRelation MessageEntityUsingCustomerIdStatic = new CustomerRelations().MessageEntityUsingCustomerId;
		internal static readonly IEntityRelation MessageRecipientEntityUsingCustomerIdStatic = new CustomerRelations().MessageRecipientEntityUsingCustomerId;
		internal static readonly IEntityRelation NetmessageEntityUsingReceiverCustomerIdStatic = new CustomerRelations().NetmessageEntityUsingReceiverCustomerId;
		internal static readonly IEntityRelation NetmessageEntityUsingSenderCustomerIdStatic = new CustomerRelations().NetmessageEntityUsingSenderCustomerId;
		internal static readonly IEntityRelation OrderEntityUsingCustomerIdStatic = new CustomerRelations().OrderEntityUsingCustomerId;
		internal static readonly IEntityRelation SurveyResultEntityUsingCustomerIdStatic = new CustomerRelations().SurveyResultEntityUsingCustomerId;
		internal static readonly IEntityRelation ClientEntityUsingClientIdStatic = new CustomerRelations().ClientEntityUsingClientId;

		/// <summary>CTor</summary>
		static StaticCustomerRelations()
		{
		}
	}
}
