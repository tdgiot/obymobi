﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Alteration. </summary>
	public partial class AlterationRelations
	{
		/// <summary>CTor</summary>
		public AlterationRelations()
		{
		}

		/// <summary>Gets all relations of the AlterationEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AlterationEntityUsingParentAlterationId);
			toReturn.Add(this.AlterationitemEntityUsingAlterationId);
			toReturn.Add(this.AlterationitemAlterationEntityUsingAlterationId);
			toReturn.Add(this.AlterationLanguageEntityUsingAlterationId);
			toReturn.Add(this.AlterationProductEntityUsingAlterationId);
			toReturn.Add(this.CategoryAlterationEntityUsingAlterationId);
			toReturn.Add(this.CustomTextEntityUsingAlterationId);
			toReturn.Add(this.MediaEntityUsingAlterationId);
			toReturn.Add(this.OrderitemAlterationitemEntityUsingAlterationId);
			toReturn.Add(this.PriceLevelItemEntityUsingAlterationId);
			toReturn.Add(this.ProductAlterationEntityUsingAlterationId);
			toReturn.Add(this.AlterationEntityUsingAlterationIdParentAlterationId);
			toReturn.Add(this.BrandEntityUsingBrandId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			toReturn.Add(this.ExternalProductEntityUsingExternalProductId);
			toReturn.Add(this.GenericalterationEntityUsingGenericalterationId);
			toReturn.Add(this.PosalterationEntityUsingPosalterationId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between AlterationEntity and AlterationEntity over the 1:n relation they have, using the relation between the fields:
		/// Alteration.AlterationId - Alteration.ParentAlterationId
		/// </summary>
		public virtual IEntityRelation AlterationEntityUsingParentAlterationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AlterationCollection" , true);
				relation.AddEntityFieldPair(AlterationFields.AlterationId, AlterationFields.ParentAlterationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AlterationEntity and AlterationitemEntity over the 1:n relation they have, using the relation between the fields:
		/// Alteration.AlterationId - Alterationitem.AlterationId
		/// </summary>
		public virtual IEntityRelation AlterationitemEntityUsingAlterationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AlterationitemCollection" , true);
				relation.AddEntityFieldPair(AlterationFields.AlterationId, AlterationitemFields.AlterationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationitemEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AlterationEntity and AlterationitemAlterationEntity over the 1:n relation they have, using the relation between the fields:
		/// Alteration.AlterationId - AlterationitemAlteration.AlterationId
		/// </summary>
		public virtual IEntityRelation AlterationitemAlterationEntityUsingAlterationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AlterationitemAlterationCollection" , true);
				relation.AddEntityFieldPair(AlterationFields.AlterationId, AlterationitemAlterationFields.AlterationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationitemAlterationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AlterationEntity and AlterationLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Alteration.AlterationId - AlterationLanguage.AlterationId
		/// </summary>
		public virtual IEntityRelation AlterationLanguageEntityUsingAlterationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AlterationLanguageCollection" , true);
				relation.AddEntityFieldPair(AlterationFields.AlterationId, AlterationLanguageFields.AlterationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AlterationEntity and AlterationProductEntity over the 1:n relation they have, using the relation between the fields:
		/// Alteration.AlterationId - AlterationProduct.AlterationId
		/// </summary>
		public virtual IEntityRelation AlterationProductEntityUsingAlterationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AlterationProductCollection" , true);
				relation.AddEntityFieldPair(AlterationFields.AlterationId, AlterationProductFields.AlterationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationProductEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AlterationEntity and CategoryAlterationEntity over the 1:n relation they have, using the relation between the fields:
		/// Alteration.AlterationId - CategoryAlteration.AlterationId
		/// </summary>
		public virtual IEntityRelation CategoryAlterationEntityUsingAlterationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CategoryAlterationCollection" , true);
				relation.AddEntityFieldPair(AlterationFields.AlterationId, CategoryAlterationFields.AlterationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryAlterationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AlterationEntity and CustomTextEntity over the 1:n relation they have, using the relation between the fields:
		/// Alteration.AlterationId - CustomText.AlterationId
		/// </summary>
		public virtual IEntityRelation CustomTextEntityUsingAlterationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomTextCollection" , true);
				relation.AddEntityFieldPair(AlterationFields.AlterationId, CustomTextFields.AlterationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AlterationEntity and MediaEntity over the 1:n relation they have, using the relation between the fields:
		/// Alteration.AlterationId - Media.AlterationId
		/// </summary>
		public virtual IEntityRelation MediaEntityUsingAlterationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MediaCollection" , true);
				relation.AddEntityFieldPair(AlterationFields.AlterationId, MediaFields.AlterationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AlterationEntity and OrderitemAlterationitemEntity over the 1:n relation they have, using the relation between the fields:
		/// Alteration.AlterationId - OrderitemAlterationitem.AlterationId
		/// </summary>
		public virtual IEntityRelation OrderitemAlterationitemEntityUsingAlterationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrderitemAlterationitemCollection" , true);
				relation.AddEntityFieldPair(AlterationFields.AlterationId, OrderitemAlterationitemFields.AlterationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderitemAlterationitemEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AlterationEntity and PriceLevelItemEntity over the 1:n relation they have, using the relation between the fields:
		/// Alteration.AlterationId - PriceLevelItem.AlterationId
		/// </summary>
		public virtual IEntityRelation PriceLevelItemEntityUsingAlterationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PriceLevelItemCollection" , true);
				relation.AddEntityFieldPair(AlterationFields.AlterationId, PriceLevelItemFields.AlterationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PriceLevelItemEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AlterationEntity and ProductAlterationEntity over the 1:n relation they have, using the relation between the fields:
		/// Alteration.AlterationId - ProductAlteration.AlterationId
		/// </summary>
		public virtual IEntityRelation ProductAlterationEntityUsingAlterationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ProductAlterationCollection" , true);
				relation.AddEntityFieldPair(AlterationFields.AlterationId, ProductAlterationFields.AlterationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductAlterationEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between AlterationEntity and AlterationEntity over the m:1 relation they have, using the relation between the fields:
		/// Alteration.ParentAlterationId - Alteration.AlterationId
		/// </summary>
		public virtual IEntityRelation AlterationEntityUsingAlterationIdParentAlterationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ParentAlterationEntity", false);
				relation.AddEntityFieldPair(AlterationFields.AlterationId, AlterationFields.ParentAlterationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AlterationEntity and BrandEntity over the m:1 relation they have, using the relation between the fields:
		/// Alteration.BrandId - Brand.BrandId
		/// </summary>
		public virtual IEntityRelation BrandEntityUsingBrandId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "BrandEntity", false);
				relation.AddEntityFieldPair(BrandFields.BrandId, AlterationFields.BrandId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BrandEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AlterationEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// Alteration.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, AlterationFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AlterationEntity and ExternalProductEntity over the m:1 relation they have, using the relation between the fields:
		/// Alteration.ExternalProductId - ExternalProduct.ExternalProductId
		/// </summary>
		public virtual IEntityRelation ExternalProductEntityUsingExternalProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ExternalProductEntity", false);
				relation.AddEntityFieldPair(ExternalProductFields.ExternalProductId, AlterationFields.ExternalProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalProductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AlterationEntity and GenericalterationEntity over the m:1 relation they have, using the relation between the fields:
		/// Alteration.GenericalterationId - Genericalteration.GenericalterationId
		/// </summary>
		public virtual IEntityRelation GenericalterationEntityUsingGenericalterationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "GenericalterationEntity", false);
				relation.AddEntityFieldPair(GenericalterationFields.GenericalterationId, AlterationFields.GenericalterationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericalterationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AlterationEntity and PosalterationEntity over the m:1 relation they have, using the relation between the fields:
		/// Alteration.PosalterationId - Posalteration.PosalterationId
		/// </summary>
		public virtual IEntityRelation PosalterationEntityUsingPosalterationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PosAlterationEntity", false);
				relation.AddEntityFieldPair(PosalterationFields.PosalterationId, AlterationFields.PosalterationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PosalterationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticAlterationRelations
	{
		internal static readonly IEntityRelation AlterationEntityUsingParentAlterationIdStatic = new AlterationRelations().AlterationEntityUsingParentAlterationId;
		internal static readonly IEntityRelation AlterationitemEntityUsingAlterationIdStatic = new AlterationRelations().AlterationitemEntityUsingAlterationId;
		internal static readonly IEntityRelation AlterationitemAlterationEntityUsingAlterationIdStatic = new AlterationRelations().AlterationitemAlterationEntityUsingAlterationId;
		internal static readonly IEntityRelation AlterationLanguageEntityUsingAlterationIdStatic = new AlterationRelations().AlterationLanguageEntityUsingAlterationId;
		internal static readonly IEntityRelation AlterationProductEntityUsingAlterationIdStatic = new AlterationRelations().AlterationProductEntityUsingAlterationId;
		internal static readonly IEntityRelation CategoryAlterationEntityUsingAlterationIdStatic = new AlterationRelations().CategoryAlterationEntityUsingAlterationId;
		internal static readonly IEntityRelation CustomTextEntityUsingAlterationIdStatic = new AlterationRelations().CustomTextEntityUsingAlterationId;
		internal static readonly IEntityRelation MediaEntityUsingAlterationIdStatic = new AlterationRelations().MediaEntityUsingAlterationId;
		internal static readonly IEntityRelation OrderitemAlterationitemEntityUsingAlterationIdStatic = new AlterationRelations().OrderitemAlterationitemEntityUsingAlterationId;
		internal static readonly IEntityRelation PriceLevelItemEntityUsingAlterationIdStatic = new AlterationRelations().PriceLevelItemEntityUsingAlterationId;
		internal static readonly IEntityRelation ProductAlterationEntityUsingAlterationIdStatic = new AlterationRelations().ProductAlterationEntityUsingAlterationId;
		internal static readonly IEntityRelation AlterationEntityUsingAlterationIdParentAlterationIdStatic = new AlterationRelations().AlterationEntityUsingAlterationIdParentAlterationId;
		internal static readonly IEntityRelation BrandEntityUsingBrandIdStatic = new AlterationRelations().BrandEntityUsingBrandId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new AlterationRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation ExternalProductEntityUsingExternalProductIdStatic = new AlterationRelations().ExternalProductEntityUsingExternalProductId;
		internal static readonly IEntityRelation GenericalterationEntityUsingGenericalterationIdStatic = new AlterationRelations().GenericalterationEntityUsingGenericalterationId;
		internal static readonly IEntityRelation PosalterationEntityUsingPosalterationIdStatic = new AlterationRelations().PosalterationEntityUsingPosalterationId;

		/// <summary>CTor</summary>
		static StaticAlterationRelations()
		{
		}
	}
}
