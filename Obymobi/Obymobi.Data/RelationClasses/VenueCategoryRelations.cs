﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: VenueCategory. </summary>
	public partial class VenueCategoryRelations
	{
		/// <summary>CTor</summary>
		public VenueCategoryRelations()
		{
		}

		/// <summary>Gets all relations of the VenueCategoryEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CompanyVenueCategoryEntityUsingVenueCategoryId);
			toReturn.Add(this.CustomTextEntityUsingVenueCategoryId);
			toReturn.Add(this.PointOfInterestVenueCategoryEntityUsingVenueCategoryId);
			toReturn.Add(this.VenueCategoryEntityUsingParentVenueCategoryId);
			toReturn.Add(this.VenueCategoryLanguageEntityUsingVenueCategoryId);
			toReturn.Add(this.VenueCategoryEntityUsingVenueCategoryIdParentVenueCategoryId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between VenueCategoryEntity and CompanyVenueCategoryEntity over the 1:n relation they have, using the relation between the fields:
		/// VenueCategory.VenueCategoryId - CompanyVenueCategory.VenueCategoryId
		/// </summary>
		public virtual IEntityRelation CompanyVenueCategoryEntityUsingVenueCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CompanyVenueCategoryCollection" , true);
				relation.AddEntityFieldPair(VenueCategoryFields.VenueCategoryId, CompanyVenueCategoryFields.VenueCategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VenueCategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyVenueCategoryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between VenueCategoryEntity and CustomTextEntity over the 1:n relation they have, using the relation between the fields:
		/// VenueCategory.VenueCategoryId - CustomText.VenueCategoryId
		/// </summary>
		public virtual IEntityRelation CustomTextEntityUsingVenueCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomTextCollection" , true);
				relation.AddEntityFieldPair(VenueCategoryFields.VenueCategoryId, CustomTextFields.VenueCategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VenueCategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between VenueCategoryEntity and PointOfInterestVenueCategoryEntity over the 1:n relation they have, using the relation between the fields:
		/// VenueCategory.VenueCategoryId - PointOfInterestVenueCategory.VenueCategoryId
		/// </summary>
		public virtual IEntityRelation PointOfInterestVenueCategoryEntityUsingVenueCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PointOfInterestVenueCategoryCollection" , true);
				relation.AddEntityFieldPair(VenueCategoryFields.VenueCategoryId, PointOfInterestVenueCategoryFields.VenueCategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VenueCategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PointOfInterestVenueCategoryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between VenueCategoryEntity and VenueCategoryEntity over the 1:n relation they have, using the relation between the fields:
		/// VenueCategory.VenueCategoryId - VenueCategory.ParentVenueCategoryId
		/// </summary>
		public virtual IEntityRelation VenueCategoryEntityUsingParentVenueCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ChildVenueCategoryCollection" , true);
				relation.AddEntityFieldPair(VenueCategoryFields.VenueCategoryId, VenueCategoryFields.ParentVenueCategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VenueCategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VenueCategoryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between VenueCategoryEntity and VenueCategoryLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// VenueCategory.VenueCategoryId - VenueCategoryLanguage.VenueCategoryId
		/// </summary>
		public virtual IEntityRelation VenueCategoryLanguageEntityUsingVenueCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "VenueCategoryLanguageCollection" , true);
				relation.AddEntityFieldPair(VenueCategoryFields.VenueCategoryId, VenueCategoryLanguageFields.VenueCategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VenueCategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VenueCategoryLanguageEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between VenueCategoryEntity and VenueCategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// VenueCategory.ParentVenueCategoryId - VenueCategory.VenueCategoryId
		/// </summary>
		public virtual IEntityRelation VenueCategoryEntityUsingVenueCategoryIdParentVenueCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ParentVenueCategoryEntity", false);
				relation.AddEntityFieldPair(VenueCategoryFields.VenueCategoryId, VenueCategoryFields.ParentVenueCategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VenueCategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VenueCategoryEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticVenueCategoryRelations
	{
		internal static readonly IEntityRelation CompanyVenueCategoryEntityUsingVenueCategoryIdStatic = new VenueCategoryRelations().CompanyVenueCategoryEntityUsingVenueCategoryId;
		internal static readonly IEntityRelation CustomTextEntityUsingVenueCategoryIdStatic = new VenueCategoryRelations().CustomTextEntityUsingVenueCategoryId;
		internal static readonly IEntityRelation PointOfInterestVenueCategoryEntityUsingVenueCategoryIdStatic = new VenueCategoryRelations().PointOfInterestVenueCategoryEntityUsingVenueCategoryId;
		internal static readonly IEntityRelation VenueCategoryEntityUsingParentVenueCategoryIdStatic = new VenueCategoryRelations().VenueCategoryEntityUsingParentVenueCategoryId;
		internal static readonly IEntityRelation VenueCategoryLanguageEntityUsingVenueCategoryIdStatic = new VenueCategoryRelations().VenueCategoryLanguageEntityUsingVenueCategoryId;
		internal static readonly IEntityRelation VenueCategoryEntityUsingVenueCategoryIdParentVenueCategoryIdStatic = new VenueCategoryRelations().VenueCategoryEntityUsingVenueCategoryIdParentVenueCategoryId;

		/// <summary>CTor</summary>
		static StaticVenueCategoryRelations()
		{
		}
	}
}
