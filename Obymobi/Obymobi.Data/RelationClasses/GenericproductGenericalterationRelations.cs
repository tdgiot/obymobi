﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: GenericproductGenericalteration. </summary>
	public partial class GenericproductGenericalterationRelations
	{
		/// <summary>CTor</summary>
		public GenericproductGenericalterationRelations()
		{
		}

		/// <summary>Gets all relations of the GenericproductGenericalterationEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.GenericalterationEntityUsingGenericalterationId);
			toReturn.Add(this.GenericproductEntityUsingGenericproductId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between GenericproductGenericalterationEntity and GenericalterationEntity over the m:1 relation they have, using the relation between the fields:
		/// GenericproductGenericalteration.GenericalterationId - Genericalteration.GenericalterationId
		/// </summary>
		public virtual IEntityRelation GenericalterationEntityUsingGenericalterationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "GenericalterationEntity", false);
				relation.AddEntityFieldPair(GenericalterationFields.GenericalterationId, GenericproductGenericalterationFields.GenericalterationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericalterationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericproductGenericalterationEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between GenericproductGenericalterationEntity and GenericproductEntity over the m:1 relation they have, using the relation between the fields:
		/// GenericproductGenericalteration.GenericproductId - Genericproduct.GenericproductId
		/// </summary>
		public virtual IEntityRelation GenericproductEntityUsingGenericproductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "GenericproductEntity", false);
				relation.AddEntityFieldPair(GenericproductFields.GenericproductId, GenericproductGenericalterationFields.GenericproductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericproductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericproductGenericalterationEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticGenericproductGenericalterationRelations
	{
		internal static readonly IEntityRelation GenericalterationEntityUsingGenericalterationIdStatic = new GenericproductGenericalterationRelations().GenericalterationEntityUsingGenericalterationId;
		internal static readonly IEntityRelation GenericproductEntityUsingGenericproductIdStatic = new GenericproductGenericalterationRelations().GenericproductEntityUsingGenericproductId;

		/// <summary>CTor</summary>
		static StaticGenericproductGenericalterationRelations()
		{
		}
	}
}
