﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: EntertainmentConfiguration. </summary>
	public partial class EntertainmentConfigurationRelations
	{
		/// <summary>CTor</summary>
		public EntertainmentConfigurationRelations()
		{
		}

		/// <summary>Gets all relations of the EntertainmentConfigurationEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ClientConfigurationEntityUsingEntertainmentConfigurationId);
			toReturn.Add(this.EntertainmentConfigurationEntertainmentEntityUsingEntertainmentConfigurationId);
			toReturn.Add(this.TimestampEntityUsingEntertainmentConfigurationId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between EntertainmentConfigurationEntity and ClientConfigurationEntity over the 1:n relation they have, using the relation between the fields:
		/// EntertainmentConfiguration.EntertainmentConfigurationId - ClientConfiguration.EntertainmentConfigurationId
		/// </summary>
		public virtual IEntityRelation ClientConfigurationEntityUsingEntertainmentConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ClientConfigurationCollection" , true);
				relation.AddEntityFieldPair(EntertainmentConfigurationFields.EntertainmentConfigurationId, ClientConfigurationFields.EntertainmentConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentConfigurationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientConfigurationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between EntertainmentConfigurationEntity and EntertainmentConfigurationEntertainmentEntity over the 1:n relation they have, using the relation between the fields:
		/// EntertainmentConfiguration.EntertainmentConfigurationId - EntertainmentConfigurationEntertainment.EntertainmentConfigurationId
		/// </summary>
		public virtual IEntityRelation EntertainmentConfigurationEntertainmentEntityUsingEntertainmentConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "EntertainmentConfigurationEntertainmentCollection" , true);
				relation.AddEntityFieldPair(EntertainmentConfigurationFields.EntertainmentConfigurationId, EntertainmentConfigurationEntertainmentFields.EntertainmentConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentConfigurationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentConfigurationEntertainmentEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between EntertainmentConfigurationEntity and TimestampEntity over the 1:1 relation they have, using the relation between the fields:
		/// EntertainmentConfiguration.EntertainmentConfigurationId - Timestamp.EntertainmentConfigurationId
		/// </summary>
		public virtual IEntityRelation TimestampEntityUsingEntertainmentConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "TimestampCollection", true);


				relation.AddEntityFieldPair(EntertainmentConfigurationFields.EntertainmentConfigurationId, TimestampFields.EntertainmentConfigurationId);


				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentConfigurationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TimestampEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between EntertainmentConfigurationEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// EntertainmentConfiguration.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, EntertainmentConfigurationFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentConfigurationEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticEntertainmentConfigurationRelations
	{
		internal static readonly IEntityRelation ClientConfigurationEntityUsingEntertainmentConfigurationIdStatic = new EntertainmentConfigurationRelations().ClientConfigurationEntityUsingEntertainmentConfigurationId;
		internal static readonly IEntityRelation EntertainmentConfigurationEntertainmentEntityUsingEntertainmentConfigurationIdStatic = new EntertainmentConfigurationRelations().EntertainmentConfigurationEntertainmentEntityUsingEntertainmentConfigurationId;
		internal static readonly IEntityRelation TimestampEntityUsingEntertainmentConfigurationIdStatic = new EntertainmentConfigurationRelations().TimestampEntityUsingEntertainmentConfigurationId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new EntertainmentConfigurationRelations().CompanyEntityUsingCompanyId;

		/// <summary>CTor</summary>
		static StaticEntertainmentConfigurationRelations()
		{
		}
	}
}
