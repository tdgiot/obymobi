﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: AffiliateCampaignAffiliatePartner. </summary>
	public partial class AffiliateCampaignAffiliatePartnerRelations
	{
		/// <summary>CTor</summary>
		public AffiliateCampaignAffiliatePartnerRelations()
		{
		}

		/// <summary>Gets all relations of the AffiliateCampaignAffiliatePartnerEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AffiliateCampaignEntityUsingAffiliateCampaignId);
			toReturn.Add(this.AffiliatePartnerEntityUsingAffiliatePartnerId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between AffiliateCampaignAffiliatePartnerEntity and AffiliateCampaignEntity over the m:1 relation they have, using the relation between the fields:
		/// AffiliateCampaignAffiliatePartner.AffiliateCampaignId - AffiliateCampaign.AffiliateCampaignId
		/// </summary>
		public virtual IEntityRelation AffiliateCampaignEntityUsingAffiliateCampaignId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AffiliateCampaignEntity", false);
				relation.AddEntityFieldPair(AffiliateCampaignFields.AffiliateCampaignId, AffiliateCampaignAffiliatePartnerFields.AffiliateCampaignId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AffiliateCampaignEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AffiliateCampaignAffiliatePartnerEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AffiliateCampaignAffiliatePartnerEntity and AffiliatePartnerEntity over the m:1 relation they have, using the relation between the fields:
		/// AffiliateCampaignAffiliatePartner.AffiliatePartnerId - AffiliatePartner.AffiliatePartnerId
		/// </summary>
		public virtual IEntityRelation AffiliatePartnerEntityUsingAffiliatePartnerId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AffiliatePartnerEntity", false);
				relation.AddEntityFieldPair(AffiliatePartnerFields.AffiliatePartnerId, AffiliateCampaignAffiliatePartnerFields.AffiliatePartnerId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AffiliatePartnerEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AffiliateCampaignAffiliatePartnerEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticAffiliateCampaignAffiliatePartnerRelations
	{
		internal static readonly IEntityRelation AffiliateCampaignEntityUsingAffiliateCampaignIdStatic = new AffiliateCampaignAffiliatePartnerRelations().AffiliateCampaignEntityUsingAffiliateCampaignId;
		internal static readonly IEntityRelation AffiliatePartnerEntityUsingAffiliatePartnerIdStatic = new AffiliateCampaignAffiliatePartnerRelations().AffiliatePartnerEntityUsingAffiliatePartnerId;

		/// <summary>CTor</summary>
		static StaticAffiliateCampaignAffiliatePartnerRelations()
		{
		}
	}
}
