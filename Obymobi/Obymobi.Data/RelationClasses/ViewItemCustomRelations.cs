﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ViewItemCustom. </summary>
	public partial class ViewItemCustomRelations
	{
		/// <summary>CTor</summary>
		public ViewItemCustomRelations()
		{
		}

		/// <summary>Gets all relations of the ViewItemCustomEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ViewCustomEntityUsingViewCustomId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between ViewItemCustomEntity and ViewCustomEntity over the m:1 relation they have, using the relation between the fields:
		/// ViewItemCustom.ViewCustomId - ViewCustom.ViewCustomId
		/// </summary>
		public virtual IEntityRelation ViewCustomEntityUsingViewCustomId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ViewCustomEntity", false);
				relation.AddEntityFieldPair(ViewCustomFields.ViewCustomId, ViewItemCustomFields.ViewCustomId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ViewCustomEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ViewItemCustomEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticViewItemCustomRelations
	{
		internal static readonly IEntityRelation ViewCustomEntityUsingViewCustomIdStatic = new ViewItemCustomRelations().ViewCustomEntityUsingViewCustomId;

		/// <summary>CTor</summary>
		static StaticViewItemCustomRelations()
		{
		}
	}
}
