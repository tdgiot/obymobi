﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: AccessCode. </summary>
	public partial class AccessCodeRelations
	{
		/// <summary>CTor</summary>
		public AccessCodeRelations()
		{
		}

		/// <summary>Gets all relations of the AccessCodeEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AccessCodeCompanyEntityUsingAccessCodeId);
			toReturn.Add(this.AccessCodePointOfInterestEntityUsingAccessCodeId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between AccessCodeEntity and AccessCodeCompanyEntity over the 1:n relation they have, using the relation between the fields:
		/// AccessCode.AccessCodeId - AccessCodeCompany.AccessCodeId
		/// </summary>
		public virtual IEntityRelation AccessCodeCompanyEntityUsingAccessCodeId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AccessCodeCompanyCollection" , true);
				relation.AddEntityFieldPair(AccessCodeFields.AccessCodeId, AccessCodeCompanyFields.AccessCodeId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AccessCodeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AccessCodeCompanyEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AccessCodeEntity and AccessCodePointOfInterestEntity over the 1:n relation they have, using the relation between the fields:
		/// AccessCode.AccessCodeId - AccessCodePointOfInterest.AccessCodeId
		/// </summary>
		public virtual IEntityRelation AccessCodePointOfInterestEntityUsingAccessCodeId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AccessCodePointOfInterestCollection" , true);
				relation.AddEntityFieldPair(AccessCodeFields.AccessCodeId, AccessCodePointOfInterestFields.AccessCodeId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AccessCodeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AccessCodePointOfInterestEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticAccessCodeRelations
	{
		internal static readonly IEntityRelation AccessCodeCompanyEntityUsingAccessCodeIdStatic = new AccessCodeRelations().AccessCodeCompanyEntityUsingAccessCodeId;
		internal static readonly IEntityRelation AccessCodePointOfInterestEntityUsingAccessCodeIdStatic = new AccessCodeRelations().AccessCodePointOfInterestEntityUsingAccessCodeId;

		/// <summary>CTor</summary>
		static StaticAccessCodeRelations()
		{
		}
	}
}
