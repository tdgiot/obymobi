﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Publishing. </summary>
	public partial class PublishingRelations
	{
		/// <summary>CTor</summary>
		public PublishingRelations()
		{
		}

		/// <summary>Gets all relations of the PublishingEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.PublishingItemEntityUsingPublishingId);
			toReturn.Add(this.UserEntityUsingUserId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between PublishingEntity and PublishingItemEntity over the 1:n relation they have, using the relation between the fields:
		/// Publishing.PublishingId - PublishingItem.PublishingId
		/// </summary>
		public virtual IEntityRelation PublishingItemEntityUsingPublishingId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PublishingItemCollection" , true);
				relation.AddEntityFieldPair(PublishingFields.PublishingId, PublishingItemFields.PublishingId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PublishingEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PublishingItemEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between PublishingEntity and UserEntity over the m:1 relation they have, using the relation between the fields:
		/// Publishing.UserId - User.UserId
		/// </summary>
		public virtual IEntityRelation UserEntityUsingUserId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UserEntity", false);
				relation.AddEntityFieldPair(UserFields.UserId, PublishingFields.UserId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PublishingEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPublishingRelations
	{
		internal static readonly IEntityRelation PublishingItemEntityUsingPublishingIdStatic = new PublishingRelations().PublishingItemEntityUsingPublishingId;
		internal static readonly IEntityRelation UserEntityUsingUserIdStatic = new PublishingRelations().UserEntityUsingUserId;

		/// <summary>CTor</summary>
		static StaticPublishingRelations()
		{
		}
	}
}
