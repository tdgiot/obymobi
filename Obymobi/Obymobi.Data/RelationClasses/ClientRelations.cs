﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Client. </summary>
	public partial class ClientRelations
	{
		/// <summary>CTor</summary>
		public ClientRelations()
		{
		}

		/// <summary>Gets all relations of the ClientEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ClientEntertainmentEntityUsingClientId);
			toReturn.Add(this.ClientLogEntityUsingClientId);
			toReturn.Add(this.ClientStateEntityUsingClientId);
			toReturn.Add(this.CustomerEntityUsingClientId);
			toReturn.Add(this.GameSessionEntityUsingClientId);
			toReturn.Add(this.MessageEntityUsingClientId);
			toReturn.Add(this.MessageRecipientEntityUsingClientId);
			toReturn.Add(this.NetmessageEntityUsingReceiverClientId);
			toReturn.Add(this.NetmessageEntityUsingSenderClientId);
			toReturn.Add(this.OrderEntityUsingClientId);
			toReturn.Add(this.ScheduledCommandEntityUsingClientId);
			toReturn.Add(this.SurveyResultEntityUsingClientId);
			toReturn.Add(this.TimestampEntityUsingClientId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			toReturn.Add(this.DeliverypointEntityUsingDeliverypointId);
			toReturn.Add(this.DeliverypointEntityUsingLastDeliverypointId);
			toReturn.Add(this.DeliverypointgroupEntityUsingDeliverypointGroupId);
			toReturn.Add(this.DeviceEntityUsingDeviceId);
			toReturn.Add(this.RoomControlAreaEntityUsingRoomControlAreaId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and ClientEntertainmentEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientId - ClientEntertainment.ClientId
		/// </summary>
		public virtual IEntityRelation ClientEntertainmentEntityUsingClientId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ClientEntertainmentCollection" , true);
				relation.AddEntityFieldPair(ClientFields.ClientId, ClientEntertainmentFields.ClientId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntertainmentEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and ClientLogEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientId - ClientLog.ClientId
		/// </summary>
		public virtual IEntityRelation ClientLogEntityUsingClientId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ClientLogCollection" , true);
				relation.AddEntityFieldPair(ClientFields.ClientId, ClientLogFields.ClientId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientLogEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and ClientStateEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientId - ClientState.ClientId
		/// </summary>
		public virtual IEntityRelation ClientStateEntityUsingClientId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ClientStateCollection" , true);
				relation.AddEntityFieldPair(ClientFields.ClientId, ClientStateFields.ClientId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientStateEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and CustomerEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientId - Customer.ClientId
		/// </summary>
		public virtual IEntityRelation CustomerEntityUsingClientId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomerCollection" , true);
				relation.AddEntityFieldPair(ClientFields.ClientId, CustomerFields.ClientId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomerEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and GameSessionEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientId - GameSession.ClientId
		/// </summary>
		public virtual IEntityRelation GameSessionEntityUsingClientId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "GameSessionCollection" , true);
				relation.AddEntityFieldPair(ClientFields.ClientId, GameSessionFields.ClientId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GameSessionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and MessageEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientId - Message.ClientId
		/// </summary>
		public virtual IEntityRelation MessageEntityUsingClientId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MessageCollection" , true);
				relation.AddEntityFieldPair(ClientFields.ClientId, MessageFields.ClientId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and MessageRecipientEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientId - MessageRecipient.ClientId
		/// </summary>
		public virtual IEntityRelation MessageRecipientEntityUsingClientId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MessageRecipientCollection" , true);
				relation.AddEntityFieldPair(ClientFields.ClientId, MessageRecipientFields.ClientId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageRecipientEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and NetmessageEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientId - Netmessage.ReceiverClientId
		/// </summary>
		public virtual IEntityRelation NetmessageEntityUsingReceiverClientId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ReceivedNetmessageCollection" , true);
				relation.AddEntityFieldPair(ClientFields.ClientId, NetmessageFields.ReceiverClientId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NetmessageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and NetmessageEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientId - Netmessage.SenderClientId
		/// </summary>
		public virtual IEntityRelation NetmessageEntityUsingSenderClientId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SentNetmessageCollection" , true);
				relation.AddEntityFieldPair(ClientFields.ClientId, NetmessageFields.SenderClientId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NetmessageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and OrderEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientId - Order.ClientId
		/// </summary>
		public virtual IEntityRelation OrderEntityUsingClientId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrderCollection" , true);
				relation.AddEntityFieldPair(ClientFields.ClientId, OrderFields.ClientId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and ScheduledCommandEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientId - ScheduledCommand.ClientId
		/// </summary>
		public virtual IEntityRelation ScheduledCommandEntityUsingClientId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ScheduledCommandCollection" , true);
				relation.AddEntityFieldPair(ClientFields.ClientId, ScheduledCommandFields.ClientId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduledCommandEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and SurveyResultEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientId - SurveyResult.ClientId
		/// </summary>
		public virtual IEntityRelation SurveyResultEntityUsingClientId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SurveyResultCollection" , true);
				relation.AddEntityFieldPair(ClientFields.ClientId, SurveyResultFields.ClientId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyResultEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and TimestampEntity over the 1:1 relation they have, using the relation between the fields:
		/// Client.ClientId - Timestamp.ClientId
		/// </summary>
		public virtual IEntityRelation TimestampEntityUsingClientId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "TimestampCollection", true);


				relation.AddEntityFieldPair(ClientFields.ClientId, TimestampFields.ClientId);


				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TimestampEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// Client.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, ClientFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ClientEntity and DeliverypointEntity over the m:1 relation they have, using the relation between the fields:
		/// Client.DeliverypointId - Deliverypoint.DeliverypointId
		/// </summary>
		public virtual IEntityRelation DeliverypointEntityUsingDeliverypointId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DeliverypointEntity", false);
				relation.AddEntityFieldPair(DeliverypointFields.DeliverypointId, ClientFields.DeliverypointId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ClientEntity and DeliverypointEntity over the m:1 relation they have, using the relation between the fields:
		/// Client.LastDeliverypointId - Deliverypoint.DeliverypointId
		/// </summary>
		public virtual IEntityRelation DeliverypointEntityUsingLastDeliverypointId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "LastDeliverypointEntity", false);
				relation.AddEntityFieldPair(DeliverypointFields.DeliverypointId, ClientFields.LastDeliverypointId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ClientEntity and DeliverypointgroupEntity over the m:1 relation they have, using the relation between the fields:
		/// Client.DeliverypointGroupId - Deliverypointgroup.DeliverypointgroupId
		/// </summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingDeliverypointGroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DeliverypointgroupEntity", false);
				relation.AddEntityFieldPair(DeliverypointgroupFields.DeliverypointgroupId, ClientFields.DeliverypointGroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ClientEntity and DeviceEntity over the m:1 relation they have, using the relation between the fields:
		/// Client.DeviceId - Device.DeviceId
		/// </summary>
		public virtual IEntityRelation DeviceEntityUsingDeviceId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DeviceEntity", false);
				relation.AddEntityFieldPair(DeviceFields.DeviceId, ClientFields.DeviceId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ClientEntity and RoomControlAreaEntity over the m:1 relation they have, using the relation between the fields:
		/// Client.RoomControlAreaId - RoomControlArea.RoomControlAreaId
		/// </summary>
		public virtual IEntityRelation RoomControlAreaEntityUsingRoomControlAreaId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RoomControlAreaEntity", false);
				relation.AddEntityFieldPair(RoomControlAreaFields.RoomControlAreaId, ClientFields.RoomControlAreaId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlAreaEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticClientRelations
	{
		internal static readonly IEntityRelation ClientEntertainmentEntityUsingClientIdStatic = new ClientRelations().ClientEntertainmentEntityUsingClientId;
		internal static readonly IEntityRelation ClientLogEntityUsingClientIdStatic = new ClientRelations().ClientLogEntityUsingClientId;
		internal static readonly IEntityRelation ClientStateEntityUsingClientIdStatic = new ClientRelations().ClientStateEntityUsingClientId;
		internal static readonly IEntityRelation CustomerEntityUsingClientIdStatic = new ClientRelations().CustomerEntityUsingClientId;
		internal static readonly IEntityRelation GameSessionEntityUsingClientIdStatic = new ClientRelations().GameSessionEntityUsingClientId;
		internal static readonly IEntityRelation MessageEntityUsingClientIdStatic = new ClientRelations().MessageEntityUsingClientId;
		internal static readonly IEntityRelation MessageRecipientEntityUsingClientIdStatic = new ClientRelations().MessageRecipientEntityUsingClientId;
		internal static readonly IEntityRelation NetmessageEntityUsingReceiverClientIdStatic = new ClientRelations().NetmessageEntityUsingReceiverClientId;
		internal static readonly IEntityRelation NetmessageEntityUsingSenderClientIdStatic = new ClientRelations().NetmessageEntityUsingSenderClientId;
		internal static readonly IEntityRelation OrderEntityUsingClientIdStatic = new ClientRelations().OrderEntityUsingClientId;
		internal static readonly IEntityRelation ScheduledCommandEntityUsingClientIdStatic = new ClientRelations().ScheduledCommandEntityUsingClientId;
		internal static readonly IEntityRelation SurveyResultEntityUsingClientIdStatic = new ClientRelations().SurveyResultEntityUsingClientId;
		internal static readonly IEntityRelation TimestampEntityUsingClientIdStatic = new ClientRelations().TimestampEntityUsingClientId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new ClientRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation DeliverypointEntityUsingDeliverypointIdStatic = new ClientRelations().DeliverypointEntityUsingDeliverypointId;
		internal static readonly IEntityRelation DeliverypointEntityUsingLastDeliverypointIdStatic = new ClientRelations().DeliverypointEntityUsingLastDeliverypointId;
		internal static readonly IEntityRelation DeliverypointgroupEntityUsingDeliverypointGroupIdStatic = new ClientRelations().DeliverypointgroupEntityUsingDeliverypointGroupId;
		internal static readonly IEntityRelation DeviceEntityUsingDeviceIdStatic = new ClientRelations().DeviceEntityUsingDeviceId;
		internal static readonly IEntityRelation RoomControlAreaEntityUsingRoomControlAreaIdStatic = new ClientRelations().RoomControlAreaEntityUsingRoomControlAreaId;

		/// <summary>CTor</summary>
		static StaticClientRelations()
		{
		}
	}
}
