﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: GameSessionReport. </summary>
	public partial class GameSessionReportRelations
	{
		/// <summary>CTor</summary>
		public GameSessionReportRelations()
		{
		}

		/// <summary>Gets all relations of the GameSessionReportEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.GameSessionReportItemEntityUsingGameSessionReportId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between GameSessionReportEntity and GameSessionReportItemEntity over the 1:n relation they have, using the relation between the fields:
		/// GameSessionReport.GameSessionReportId - GameSessionReportItem.GameSessionReportId
		/// </summary>
		public virtual IEntityRelation GameSessionReportItemEntityUsingGameSessionReportId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "GameSessionReportItemCollection" , true);
				relation.AddEntityFieldPair(GameSessionReportFields.GameSessionReportId, GameSessionReportItemFields.GameSessionReportId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GameSessionReportEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GameSessionReportItemEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between GameSessionReportEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// GameSessionReport.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, GameSessionReportFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GameSessionReportEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticGameSessionReportRelations
	{
		internal static readonly IEntityRelation GameSessionReportItemEntityUsingGameSessionReportIdStatic = new GameSessionReportRelations().GameSessionReportItemEntityUsingGameSessionReportId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new GameSessionReportRelations().CompanyEntityUsingCompanyId;

		/// <summary>CTor</summary>
		static StaticGameSessionReportRelations()
		{
		}
	}
}
