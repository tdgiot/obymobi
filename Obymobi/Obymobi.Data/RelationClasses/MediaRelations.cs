﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Media. </summary>
	public partial class MediaRelations
	{
		/// <summary>CTor</summary>
		public MediaRelations()
		{
		}

		/// <summary>Gets all relations of the MediaEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AnnouncementEntityUsingMediaId);
			toReturn.Add(this.MediaEntityUsingAgnosticMediaId);
			toReturn.Add(this.MediaCultureEntityUsingMediaId);
			toReturn.Add(this.MediaLanguageEntityUsingMediaId);
			toReturn.Add(this.MediaRatioTypeMediaEntityUsingMediaId);
			toReturn.Add(this.MediaRelationshipEntityUsingMediaId);
			toReturn.Add(this.MessageEntityUsingMediaId);
			toReturn.Add(this.MessageTemplateEntityUsingMediaId);
			toReturn.Add(this.ScheduledMessageEntityUsingMediaId);
			toReturn.Add(this.UIScheduleItemEntityUsingMediaId);
			toReturn.Add(this.AdvertisementEntityUsingAdvertisementId);
			toReturn.Add(this.AlterationEntityUsingAlterationId);
			toReturn.Add(this.AlterationoptionEntityUsingAlterationoptionId);
			toReturn.Add(this.ApplicationConfigurationEntityUsingApplicationConfigurationId);
			toReturn.Add(this.CarouselItemEntityUsingCarouselItemId);
			toReturn.Add(this.LandingPageEntityUsingLandingPageId);
			toReturn.Add(this.WidgetActionBannerEntityUsingWidgetId);
			toReturn.Add(this.WidgetHeroEntityUsingWidgetId);
			toReturn.Add(this.AttachmentEntityUsingAttachmentId);
			toReturn.Add(this.CategoryEntityUsingActionCategoryId);
			toReturn.Add(this.CategoryEntityUsingCategoryId);
			toReturn.Add(this.ClientConfigurationEntityUsingClientConfigurationId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			toReturn.Add(this.DeliverypointgroupEntityUsingDeliverypointgroupId);
			toReturn.Add(this.EntertainmentEntityUsingActionEntertainmentId);
			toReturn.Add(this.EntertainmentEntityUsingEntertainmentId);
			toReturn.Add(this.EntertainmentcategoryEntityUsingActionEntertainmentcategoryId);
			toReturn.Add(this.GenericcategoryEntityUsingGenericcategoryId);
			toReturn.Add(this.GenericproductEntityUsingGenericproductId);
			toReturn.Add(this.MediaEntityUsingMediaIdAgnosticMediaId);
			toReturn.Add(this.PageEntityUsingPageId);
			toReturn.Add(this.PageEntityUsingActionPageId);
			toReturn.Add(this.PageElementEntityUsingPageElementId);
			toReturn.Add(this.PageTemplateEntityUsingPageTemplateId);
			toReturn.Add(this.PageTemplateElementEntityUsingPageTemplateElementId);
			toReturn.Add(this.PointOfInterestEntityUsingPointOfInterestId);
			toReturn.Add(this.ProductEntityUsingActionProductId);
			toReturn.Add(this.ProductEntityUsingProductId);
			toReturn.Add(this.ProductgroupEntityUsingProductgroupId);
			toReturn.Add(this.RoomControlSectionEntityUsingRoomControlSectionId);
			toReturn.Add(this.RoomControlSectionItemEntityUsingRoomControlSectionItemId);
			toReturn.Add(this.RoutestephandlerEntityUsingRoutestephandlerId);
			toReturn.Add(this.SiteEntityUsingSiteId);
			toReturn.Add(this.SiteEntityUsingActionSiteId);
			toReturn.Add(this.StationEntityUsingStationId);
			toReturn.Add(this.SurveyEntityUsingSurveyId);
			toReturn.Add(this.SurveyPageEntityUsingSurveyPageId);
			toReturn.Add(this.UIFooterItemEntityUsingUIFooterItemId);
			toReturn.Add(this.UIThemeEntityUsingUIThemeId);
			toReturn.Add(this.UIWidgetEntityUsingUIWidgetId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between MediaEntity and AnnouncementEntity over the 1:n relation they have, using the relation between the fields:
		/// Media.MediaId - Announcement.MediaId
		/// </summary>
		public virtual IEntityRelation AnnouncementEntityUsingMediaId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AnnouncementCollection" , true);
				relation.AddEntityFieldPair(MediaFields.MediaId, AnnouncementFields.MediaId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AnnouncementEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between MediaEntity and MediaEntity over the 1:n relation they have, using the relation between the fields:
		/// Media.MediaId - Media.AgnosticMediaId
		/// </summary>
		public virtual IEntityRelation MediaEntityUsingAgnosticMediaId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MediaCollection" , true);
				relation.AddEntityFieldPair(MediaFields.MediaId, MediaFields.AgnosticMediaId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between MediaEntity and MediaCultureEntity over the 1:n relation they have, using the relation between the fields:
		/// Media.MediaId - MediaCulture.MediaId
		/// </summary>
		public virtual IEntityRelation MediaCultureEntityUsingMediaId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MediaCultureCollection" , true);
				relation.AddEntityFieldPair(MediaFields.MediaId, MediaCultureFields.MediaId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaCultureEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between MediaEntity and MediaLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Media.MediaId - MediaLanguage.MediaId
		/// </summary>
		public virtual IEntityRelation MediaLanguageEntityUsingMediaId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MediaLanguageCollection" , true);
				relation.AddEntityFieldPair(MediaFields.MediaId, MediaLanguageFields.MediaId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between MediaEntity and MediaRatioTypeMediaEntity over the 1:n relation they have, using the relation between the fields:
		/// Media.MediaId - MediaRatioTypeMedia.MediaId
		/// </summary>
		public virtual IEntityRelation MediaRatioTypeMediaEntityUsingMediaId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MediaRatioTypeMediaCollection" , true);
				relation.AddEntityFieldPair(MediaFields.MediaId, MediaRatioTypeMediaFields.MediaId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaRatioTypeMediaEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between MediaEntity and MediaRelationshipEntity over the 1:n relation they have, using the relation between the fields:
		/// Media.MediaId - MediaRelationship.MediaId
		/// </summary>
		public virtual IEntityRelation MediaRelationshipEntityUsingMediaId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MediaRelationshipCollection" , true);
				relation.AddEntityFieldPair(MediaFields.MediaId, MediaRelationshipFields.MediaId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaRelationshipEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between MediaEntity and MessageEntity over the 1:n relation they have, using the relation between the fields:
		/// Media.MediaId - Message.MediaId
		/// </summary>
		public virtual IEntityRelation MessageEntityUsingMediaId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MessageCollection" , true);
				relation.AddEntityFieldPair(MediaFields.MediaId, MessageFields.MediaId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between MediaEntity and MessageTemplateEntity over the 1:n relation they have, using the relation between the fields:
		/// Media.MediaId - MessageTemplate.MediaId
		/// </summary>
		public virtual IEntityRelation MessageTemplateEntityUsingMediaId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MessageTemplateCollection" , true);
				relation.AddEntityFieldPair(MediaFields.MediaId, MessageTemplateFields.MediaId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageTemplateEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between MediaEntity and ScheduledMessageEntity over the 1:n relation they have, using the relation between the fields:
		/// Media.MediaId - ScheduledMessage.MediaId
		/// </summary>
		public virtual IEntityRelation ScheduledMessageEntityUsingMediaId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ScheduledMessageCollection" , true);
				relation.AddEntityFieldPair(MediaFields.MediaId, ScheduledMessageFields.MediaId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduledMessageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between MediaEntity and UIScheduleItemEntity over the 1:n relation they have, using the relation between the fields:
		/// Media.MediaId - UIScheduleItem.MediaId
		/// </summary>
		public virtual IEntityRelation UIScheduleItemEntityUsingMediaId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UIScheduleItemCollection" , true);
				relation.AddEntityFieldPair(MediaFields.MediaId, UIScheduleItemFields.MediaId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIScheduleItemEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between MediaEntity and AdvertisementEntity over the m:1 relation they have, using the relation between the fields:
		/// Media.AdvertisementId - Advertisement.AdvertisementId
		/// </summary>
		public virtual IEntityRelation AdvertisementEntityUsingAdvertisementId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AdvertisementEntity", false);
				relation.AddEntityFieldPair(AdvertisementFields.AdvertisementId, MediaFields.AdvertisementId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MediaEntity and AlterationEntity over the m:1 relation they have, using the relation between the fields:
		/// Media.AlterationId - Alteration.AlterationId
		/// </summary>
		public virtual IEntityRelation AlterationEntityUsingAlterationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AlterationEntity", false);
				relation.AddEntityFieldPair(AlterationFields.AlterationId, MediaFields.AlterationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MediaEntity and AlterationoptionEntity over the m:1 relation they have, using the relation between the fields:
		/// Media.AlterationoptionId - Alterationoption.AlterationoptionId
		/// </summary>
		public virtual IEntityRelation AlterationoptionEntityUsingAlterationoptionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AlterationoptionEntity", false);
				relation.AddEntityFieldPair(AlterationoptionFields.AlterationoptionId, MediaFields.AlterationoptionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationoptionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MediaEntity and ApplicationConfigurationEntity over the m:1 relation they have, using the relation between the fields:
		/// Media.ApplicationConfigurationId - ApplicationConfiguration.ApplicationConfigurationId
		/// </summary>
		public virtual IEntityRelation ApplicationConfigurationEntityUsingApplicationConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ApplicationConfigurationEntity", false);
				relation.AddEntityFieldPair(ApplicationConfigurationFields.ApplicationConfigurationId, MediaFields.ApplicationConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ApplicationConfigurationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MediaEntity and CarouselItemEntity over the m:1 relation they have, using the relation between the fields:
		/// Media.CarouselItemId - CarouselItem.CarouselItemId
		/// </summary>
		public virtual IEntityRelation CarouselItemEntityUsingCarouselItemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CarouselItemEntity", false);
				relation.AddEntityFieldPair(CarouselItemFields.CarouselItemId, MediaFields.CarouselItemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CarouselItemEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MediaEntity and LandingPageEntity over the m:1 relation they have, using the relation between the fields:
		/// Media.LandingPageId - LandingPage.LandingPageId
		/// </summary>
		public virtual IEntityRelation LandingPageEntityUsingLandingPageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "LandingPageEntity", false);
				relation.AddEntityFieldPair(LandingPageFields.LandingPageId, MediaFields.LandingPageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LandingPageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MediaEntity and WidgetActionBannerEntity over the m:1 relation they have, using the relation between the fields:
		/// Media.WidgetId - WidgetActionBanner.WidgetId
		/// </summary>
		public virtual IEntityRelation WidgetActionBannerEntityUsingWidgetId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "WidgetActionBannerEntity", false);
				relation.AddEntityFieldPair(WidgetActionBannerFields.WidgetId, MediaFields.WidgetId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WidgetActionBannerEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MediaEntity and WidgetHeroEntity over the m:1 relation they have, using the relation between the fields:
		/// Media.WidgetId - WidgetHero.WidgetId
		/// </summary>
		public virtual IEntityRelation WidgetHeroEntityUsingWidgetId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "WidgetHeroEntity", false);
				relation.AddEntityFieldPair(WidgetHeroFields.WidgetId, MediaFields.WidgetId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WidgetHeroEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MediaEntity and AttachmentEntity over the m:1 relation they have, using the relation between the fields:
		/// Media.AttachmentId - Attachment.AttachmentId
		/// </summary>
		public virtual IEntityRelation AttachmentEntityUsingAttachmentId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AttachmentEntity", false);
				relation.AddEntityFieldPair(AttachmentFields.AttachmentId, MediaFields.AttachmentId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttachmentEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MediaEntity and CategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// Media.ActionCategoryId - Category.CategoryId
		/// </summary>
		public virtual IEntityRelation CategoryEntityUsingActionCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ActionCategoryEntity", false);
				relation.AddEntityFieldPair(CategoryFields.CategoryId, MediaFields.ActionCategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MediaEntity and CategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// Media.CategoryId - Category.CategoryId
		/// </summary>
		public virtual IEntityRelation CategoryEntityUsingCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CategoryEntity", false);
				relation.AddEntityFieldPair(CategoryFields.CategoryId, MediaFields.CategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MediaEntity and ClientConfigurationEntity over the m:1 relation they have, using the relation between the fields:
		/// Media.ClientConfigurationId - ClientConfiguration.ClientConfigurationId
		/// </summary>
		public virtual IEntityRelation ClientConfigurationEntityUsingClientConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ClientConfigurationEntity", false);
				relation.AddEntityFieldPair(ClientConfigurationFields.ClientConfigurationId, MediaFields.ClientConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientConfigurationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MediaEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// Media.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, MediaFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MediaEntity and DeliverypointgroupEntity over the m:1 relation they have, using the relation between the fields:
		/// Media.DeliverypointgroupId - Deliverypointgroup.DeliverypointgroupId
		/// </summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingDeliverypointgroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DeliverypointgroupEntity", false);
				relation.AddEntityFieldPair(DeliverypointgroupFields.DeliverypointgroupId, MediaFields.DeliverypointgroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MediaEntity and EntertainmentEntity over the m:1 relation they have, using the relation between the fields:
		/// Media.ActionEntertainmentId - Entertainment.EntertainmentId
		/// </summary>
		public virtual IEntityRelation EntertainmentEntityUsingActionEntertainmentId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ActionEntertainmentEntity", false);
				relation.AddEntityFieldPair(EntertainmentFields.EntertainmentId, MediaFields.ActionEntertainmentId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MediaEntity and EntertainmentEntity over the m:1 relation they have, using the relation between the fields:
		/// Media.EntertainmentId - Entertainment.EntertainmentId
		/// </summary>
		public virtual IEntityRelation EntertainmentEntityUsingEntertainmentId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "EntertainmentEntity", false);
				relation.AddEntityFieldPair(EntertainmentFields.EntertainmentId, MediaFields.EntertainmentId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MediaEntity and EntertainmentcategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// Media.ActionEntertainmentcategoryId - Entertainmentcategory.EntertainmentcategoryId
		/// </summary>
		public virtual IEntityRelation EntertainmentcategoryEntityUsingActionEntertainmentcategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ActionEntertainmentcategoryEntity", false);
				relation.AddEntityFieldPair(EntertainmentcategoryFields.EntertainmentcategoryId, MediaFields.ActionEntertainmentcategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentcategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MediaEntity and GenericcategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// Media.GenericcategoryId - Genericcategory.GenericcategoryId
		/// </summary>
		public virtual IEntityRelation GenericcategoryEntityUsingGenericcategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "GenericcategoryEntity", false);
				relation.AddEntityFieldPair(GenericcategoryFields.GenericcategoryId, MediaFields.GenericcategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericcategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MediaEntity and GenericproductEntity over the m:1 relation they have, using the relation between the fields:
		/// Media.GenericproductId - Genericproduct.GenericproductId
		/// </summary>
		public virtual IEntityRelation GenericproductEntityUsingGenericproductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "GenericproductEntity", false);
				relation.AddEntityFieldPair(GenericproductFields.GenericproductId, MediaFields.GenericproductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericproductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MediaEntity and MediaEntity over the m:1 relation they have, using the relation between the fields:
		/// Media.AgnosticMediaId - Media.MediaId
		/// </summary>
		public virtual IEntityRelation MediaEntityUsingMediaIdAgnosticMediaId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "MediaEntity", false);
				relation.AddEntityFieldPair(MediaFields.MediaId, MediaFields.AgnosticMediaId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MediaEntity and PageEntity over the m:1 relation they have, using the relation between the fields:
		/// Media.PageId - Page.PageId
		/// </summary>
		public virtual IEntityRelation PageEntityUsingPageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PageEntity", false);
				relation.AddEntityFieldPair(PageFields.PageId, MediaFields.PageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MediaEntity and PageEntity over the m:1 relation they have, using the relation between the fields:
		/// Media.ActionPageId - Page.PageId
		/// </summary>
		public virtual IEntityRelation PageEntityUsingActionPageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PageEntity_", false);
				relation.AddEntityFieldPair(PageFields.PageId, MediaFields.ActionPageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MediaEntity and PageElementEntity over the m:1 relation they have, using the relation between the fields:
		/// Media.PageElementId - PageElement.PageElementId
		/// </summary>
		public virtual IEntityRelation PageElementEntityUsingPageElementId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PageElementEntity", false);
				relation.AddEntityFieldPair(PageElementFields.PageElementId, MediaFields.PageElementId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageElementEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MediaEntity and PageTemplateEntity over the m:1 relation they have, using the relation between the fields:
		/// Media.PageTemplateId - PageTemplate.PageTemplateId
		/// </summary>
		public virtual IEntityRelation PageTemplateEntityUsingPageTemplateId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PageTemplateEntity", false);
				relation.AddEntityFieldPair(PageTemplateFields.PageTemplateId, MediaFields.PageTemplateId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageTemplateEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MediaEntity and PageTemplateElementEntity over the m:1 relation they have, using the relation between the fields:
		/// Media.PageTemplateElementId - PageTemplateElement.PageTemplateElementId
		/// </summary>
		public virtual IEntityRelation PageTemplateElementEntityUsingPageTemplateElementId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PageTemplateElementEntity", false);
				relation.AddEntityFieldPair(PageTemplateElementFields.PageTemplateElementId, MediaFields.PageTemplateElementId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageTemplateElementEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MediaEntity and PointOfInterestEntity over the m:1 relation they have, using the relation between the fields:
		/// Media.PointOfInterestId - PointOfInterest.PointOfInterestId
		/// </summary>
		public virtual IEntityRelation PointOfInterestEntityUsingPointOfInterestId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PointOfInterestEntity", false);
				relation.AddEntityFieldPair(PointOfInterestFields.PointOfInterestId, MediaFields.PointOfInterestId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PointOfInterestEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MediaEntity and ProductEntity over the m:1 relation they have, using the relation between the fields:
		/// Media.ActionProductId - Product.ProductId
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingActionProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ActionProductEntity", false);
				relation.AddEntityFieldPair(ProductFields.ProductId, MediaFields.ActionProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MediaEntity and ProductEntity over the m:1 relation they have, using the relation between the fields:
		/// Media.ProductId - Product.ProductId
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ProductEntity", false);
				relation.AddEntityFieldPair(ProductFields.ProductId, MediaFields.ProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MediaEntity and ProductgroupEntity over the m:1 relation they have, using the relation between the fields:
		/// Media.ProductgroupId - Productgroup.ProductgroupId
		/// </summary>
		public virtual IEntityRelation ProductgroupEntityUsingProductgroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ProductgroupEntity", false);
				relation.AddEntityFieldPair(ProductgroupFields.ProductgroupId, MediaFields.ProductgroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductgroupEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MediaEntity and RoomControlSectionEntity over the m:1 relation they have, using the relation between the fields:
		/// Media.RoomControlSectionId - RoomControlSection.RoomControlSectionId
		/// </summary>
		public virtual IEntityRelation RoomControlSectionEntityUsingRoomControlSectionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RoomControlSectionEntity", false);
				relation.AddEntityFieldPair(RoomControlSectionFields.RoomControlSectionId, MediaFields.RoomControlSectionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlSectionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MediaEntity and RoomControlSectionItemEntity over the m:1 relation they have, using the relation between the fields:
		/// Media.RoomControlSectionItemId - RoomControlSectionItem.RoomControlSectionItemId
		/// </summary>
		public virtual IEntityRelation RoomControlSectionItemEntityUsingRoomControlSectionItemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RoomControlSectionItemEntity", false);
				relation.AddEntityFieldPair(RoomControlSectionItemFields.RoomControlSectionItemId, MediaFields.RoomControlSectionItemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlSectionItemEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MediaEntity and RoutestephandlerEntity over the m:1 relation they have, using the relation between the fields:
		/// Media.RoutestephandlerId - Routestephandler.RoutestephandlerId
		/// </summary>
		public virtual IEntityRelation RoutestephandlerEntityUsingRoutestephandlerId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RoutestephandlerEntity", false);
				relation.AddEntityFieldPair(RoutestephandlerFields.RoutestephandlerId, MediaFields.RoutestephandlerId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoutestephandlerEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MediaEntity and SiteEntity over the m:1 relation they have, using the relation between the fields:
		/// Media.SiteId - Site.SiteId
		/// </summary>
		public virtual IEntityRelation SiteEntityUsingSiteId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SiteEntity", false);
				relation.AddEntityFieldPair(SiteFields.SiteId, MediaFields.SiteId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SiteEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MediaEntity and SiteEntity over the m:1 relation they have, using the relation between the fields:
		/// Media.ActionSiteId - Site.SiteId
		/// </summary>
		public virtual IEntityRelation SiteEntityUsingActionSiteId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SiteEntity_", false);
				relation.AddEntityFieldPair(SiteFields.SiteId, MediaFields.ActionSiteId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SiteEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MediaEntity and StationEntity over the m:1 relation they have, using the relation between the fields:
		/// Media.StationId - Station.StationId
		/// </summary>
		public virtual IEntityRelation StationEntityUsingStationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "StationEntity", false);
				relation.AddEntityFieldPair(StationFields.StationId, MediaFields.StationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("StationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MediaEntity and SurveyEntity over the m:1 relation they have, using the relation between the fields:
		/// Media.SurveyId - Survey.SurveyId
		/// </summary>
		public virtual IEntityRelation SurveyEntityUsingSurveyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SurveyEntity", false);
				relation.AddEntityFieldPair(SurveyFields.SurveyId, MediaFields.SurveyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MediaEntity and SurveyPageEntity over the m:1 relation they have, using the relation between the fields:
		/// Media.SurveyPageId - SurveyPage.SurveyPageId
		/// </summary>
		public virtual IEntityRelation SurveyPageEntityUsingSurveyPageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SurveyPageEntity", false);
				relation.AddEntityFieldPair(SurveyPageFields.SurveyPageId, MediaFields.SurveyPageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyPageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MediaEntity and UIFooterItemEntity over the m:1 relation they have, using the relation between the fields:
		/// Media.UIFooterItemId - UIFooterItem.UIFooterItemId
		/// </summary>
		public virtual IEntityRelation UIFooterItemEntityUsingUIFooterItemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UIFooterItemEntity", false);
				relation.AddEntityFieldPair(UIFooterItemFields.UIFooterItemId, MediaFields.UIFooterItemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIFooterItemEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MediaEntity and UIThemeEntity over the m:1 relation they have, using the relation between the fields:
		/// Media.UIThemeId - UITheme.UIThemeId
		/// </summary>
		public virtual IEntityRelation UIThemeEntityUsingUIThemeId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UIThemeEntity", false);
				relation.AddEntityFieldPair(UIThemeFields.UIThemeId, MediaFields.UIThemeId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIThemeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MediaEntity and UIWidgetEntity over the m:1 relation they have, using the relation between the fields:
		/// Media.UIWidgetId - UIWidget.UIWidgetId
		/// </summary>
		public virtual IEntityRelation UIWidgetEntityUsingUIWidgetId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UIWidgetEntity", false);
				relation.AddEntityFieldPair(UIWidgetFields.UIWidgetId, MediaFields.UIWidgetId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIWidgetEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticMediaRelations
	{
		internal static readonly IEntityRelation AnnouncementEntityUsingMediaIdStatic = new MediaRelations().AnnouncementEntityUsingMediaId;
		internal static readonly IEntityRelation MediaEntityUsingAgnosticMediaIdStatic = new MediaRelations().MediaEntityUsingAgnosticMediaId;
		internal static readonly IEntityRelation MediaCultureEntityUsingMediaIdStatic = new MediaRelations().MediaCultureEntityUsingMediaId;
		internal static readonly IEntityRelation MediaLanguageEntityUsingMediaIdStatic = new MediaRelations().MediaLanguageEntityUsingMediaId;
		internal static readonly IEntityRelation MediaRatioTypeMediaEntityUsingMediaIdStatic = new MediaRelations().MediaRatioTypeMediaEntityUsingMediaId;
		internal static readonly IEntityRelation MediaRelationshipEntityUsingMediaIdStatic = new MediaRelations().MediaRelationshipEntityUsingMediaId;
		internal static readonly IEntityRelation MessageEntityUsingMediaIdStatic = new MediaRelations().MessageEntityUsingMediaId;
		internal static readonly IEntityRelation MessageTemplateEntityUsingMediaIdStatic = new MediaRelations().MessageTemplateEntityUsingMediaId;
		internal static readonly IEntityRelation ScheduledMessageEntityUsingMediaIdStatic = new MediaRelations().ScheduledMessageEntityUsingMediaId;
		internal static readonly IEntityRelation UIScheduleItemEntityUsingMediaIdStatic = new MediaRelations().UIScheduleItemEntityUsingMediaId;
		internal static readonly IEntityRelation AdvertisementEntityUsingAdvertisementIdStatic = new MediaRelations().AdvertisementEntityUsingAdvertisementId;
		internal static readonly IEntityRelation AlterationEntityUsingAlterationIdStatic = new MediaRelations().AlterationEntityUsingAlterationId;
		internal static readonly IEntityRelation AlterationoptionEntityUsingAlterationoptionIdStatic = new MediaRelations().AlterationoptionEntityUsingAlterationoptionId;
		internal static readonly IEntityRelation ApplicationConfigurationEntityUsingApplicationConfigurationIdStatic = new MediaRelations().ApplicationConfigurationEntityUsingApplicationConfigurationId;
		internal static readonly IEntityRelation CarouselItemEntityUsingCarouselItemIdStatic = new MediaRelations().CarouselItemEntityUsingCarouselItemId;
		internal static readonly IEntityRelation LandingPageEntityUsingLandingPageIdStatic = new MediaRelations().LandingPageEntityUsingLandingPageId;
		internal static readonly IEntityRelation WidgetActionBannerEntityUsingWidgetIdStatic = new MediaRelations().WidgetActionBannerEntityUsingWidgetId;
		internal static readonly IEntityRelation WidgetHeroEntityUsingWidgetIdStatic = new MediaRelations().WidgetHeroEntityUsingWidgetId;
		internal static readonly IEntityRelation AttachmentEntityUsingAttachmentIdStatic = new MediaRelations().AttachmentEntityUsingAttachmentId;
		internal static readonly IEntityRelation CategoryEntityUsingActionCategoryIdStatic = new MediaRelations().CategoryEntityUsingActionCategoryId;
		internal static readonly IEntityRelation CategoryEntityUsingCategoryIdStatic = new MediaRelations().CategoryEntityUsingCategoryId;
		internal static readonly IEntityRelation ClientConfigurationEntityUsingClientConfigurationIdStatic = new MediaRelations().ClientConfigurationEntityUsingClientConfigurationId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new MediaRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation DeliverypointgroupEntityUsingDeliverypointgroupIdStatic = new MediaRelations().DeliverypointgroupEntityUsingDeliverypointgroupId;
		internal static readonly IEntityRelation EntertainmentEntityUsingActionEntertainmentIdStatic = new MediaRelations().EntertainmentEntityUsingActionEntertainmentId;
		internal static readonly IEntityRelation EntertainmentEntityUsingEntertainmentIdStatic = new MediaRelations().EntertainmentEntityUsingEntertainmentId;
		internal static readonly IEntityRelation EntertainmentcategoryEntityUsingActionEntertainmentcategoryIdStatic = new MediaRelations().EntertainmentcategoryEntityUsingActionEntertainmentcategoryId;
		internal static readonly IEntityRelation GenericcategoryEntityUsingGenericcategoryIdStatic = new MediaRelations().GenericcategoryEntityUsingGenericcategoryId;
		internal static readonly IEntityRelation GenericproductEntityUsingGenericproductIdStatic = new MediaRelations().GenericproductEntityUsingGenericproductId;
		internal static readonly IEntityRelation MediaEntityUsingMediaIdAgnosticMediaIdStatic = new MediaRelations().MediaEntityUsingMediaIdAgnosticMediaId;
		internal static readonly IEntityRelation PageEntityUsingPageIdStatic = new MediaRelations().PageEntityUsingPageId;
		internal static readonly IEntityRelation PageEntityUsingActionPageIdStatic = new MediaRelations().PageEntityUsingActionPageId;
		internal static readonly IEntityRelation PageElementEntityUsingPageElementIdStatic = new MediaRelations().PageElementEntityUsingPageElementId;
		internal static readonly IEntityRelation PageTemplateEntityUsingPageTemplateIdStatic = new MediaRelations().PageTemplateEntityUsingPageTemplateId;
		internal static readonly IEntityRelation PageTemplateElementEntityUsingPageTemplateElementIdStatic = new MediaRelations().PageTemplateElementEntityUsingPageTemplateElementId;
		internal static readonly IEntityRelation PointOfInterestEntityUsingPointOfInterestIdStatic = new MediaRelations().PointOfInterestEntityUsingPointOfInterestId;
		internal static readonly IEntityRelation ProductEntityUsingActionProductIdStatic = new MediaRelations().ProductEntityUsingActionProductId;
		internal static readonly IEntityRelation ProductEntityUsingProductIdStatic = new MediaRelations().ProductEntityUsingProductId;
		internal static readonly IEntityRelation ProductgroupEntityUsingProductgroupIdStatic = new MediaRelations().ProductgroupEntityUsingProductgroupId;
		internal static readonly IEntityRelation RoomControlSectionEntityUsingRoomControlSectionIdStatic = new MediaRelations().RoomControlSectionEntityUsingRoomControlSectionId;
		internal static readonly IEntityRelation RoomControlSectionItemEntityUsingRoomControlSectionItemIdStatic = new MediaRelations().RoomControlSectionItemEntityUsingRoomControlSectionItemId;
		internal static readonly IEntityRelation RoutestephandlerEntityUsingRoutestephandlerIdStatic = new MediaRelations().RoutestephandlerEntityUsingRoutestephandlerId;
		internal static readonly IEntityRelation SiteEntityUsingSiteIdStatic = new MediaRelations().SiteEntityUsingSiteId;
		internal static readonly IEntityRelation SiteEntityUsingActionSiteIdStatic = new MediaRelations().SiteEntityUsingActionSiteId;
		internal static readonly IEntityRelation StationEntityUsingStationIdStatic = new MediaRelations().StationEntityUsingStationId;
		internal static readonly IEntityRelation SurveyEntityUsingSurveyIdStatic = new MediaRelations().SurveyEntityUsingSurveyId;
		internal static readonly IEntityRelation SurveyPageEntityUsingSurveyPageIdStatic = new MediaRelations().SurveyPageEntityUsingSurveyPageId;
		internal static readonly IEntityRelation UIFooterItemEntityUsingUIFooterItemIdStatic = new MediaRelations().UIFooterItemEntityUsingUIFooterItemId;
		internal static readonly IEntityRelation UIThemeEntityUsingUIThemeIdStatic = new MediaRelations().UIThemeEntityUsingUIThemeId;
		internal static readonly IEntityRelation UIWidgetEntityUsingUIWidgetIdStatic = new MediaRelations().UIWidgetEntityUsingUIWidgetId;

		/// <summary>CTor</summary>
		static StaticMediaRelations()
		{
		}
	}
}
