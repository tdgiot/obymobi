﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: OrderRoutestephandlerHistory. </summary>
	public partial class OrderRoutestephandlerHistoryRelations
	{
		/// <summary>CTor</summary>
		public OrderRoutestephandlerHistoryRelations()
		{
		}

		/// <summary>Gets all relations of the OrderRoutestephandlerHistoryEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ExternalSystemEntityUsingExternalSystemId);
			toReturn.Add(this.OrderEntityUsingOrderId);
			toReturn.Add(this.SupportpoolEntityUsingSupportpoolId);
			toReturn.Add(this.TerminalEntityUsingForwardedFromTerminalId);
			toReturn.Add(this.TerminalEntityUsingTerminalId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between OrderRoutestephandlerHistoryEntity and ExternalSystemEntity over the m:1 relation they have, using the relation between the fields:
		/// OrderRoutestephandlerHistory.ExternalSystemId - ExternalSystem.ExternalSystemId
		/// </summary>
		public virtual IEntityRelation ExternalSystemEntityUsingExternalSystemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ExternalSystemEntity", false);
				relation.AddEntityFieldPair(ExternalSystemFields.ExternalSystemId, OrderRoutestephandlerHistoryFields.ExternalSystemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalSystemEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderRoutestephandlerHistoryEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OrderRoutestephandlerHistoryEntity and OrderEntity over the m:1 relation they have, using the relation between the fields:
		/// OrderRoutestephandlerHistory.OrderId - Order.OrderId
		/// </summary>
		public virtual IEntityRelation OrderEntityUsingOrderId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "OrderEntity", false);
				relation.AddEntityFieldPair(OrderFields.OrderId, OrderRoutestephandlerHistoryFields.OrderId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderRoutestephandlerHistoryEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OrderRoutestephandlerHistoryEntity and SupportpoolEntity over the m:1 relation they have, using the relation between the fields:
		/// OrderRoutestephandlerHistory.SupportpoolId - Supportpool.SupportpoolId
		/// </summary>
		public virtual IEntityRelation SupportpoolEntityUsingSupportpoolId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SupportpoolEntity", false);
				relation.AddEntityFieldPair(SupportpoolFields.SupportpoolId, OrderRoutestephandlerHistoryFields.SupportpoolId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SupportpoolEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderRoutestephandlerHistoryEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OrderRoutestephandlerHistoryEntity and TerminalEntity over the m:1 relation they have, using the relation between the fields:
		/// OrderRoutestephandlerHistory.ForwardedFromTerminalId - Terminal.TerminalId
		/// </summary>
		public virtual IEntityRelation TerminalEntityUsingForwardedFromTerminalId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ForwardedFromTerminalEntity", false);
				relation.AddEntityFieldPair(TerminalFields.TerminalId, OrderRoutestephandlerHistoryFields.ForwardedFromTerminalId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderRoutestephandlerHistoryEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OrderRoutestephandlerHistoryEntity and TerminalEntity over the m:1 relation they have, using the relation between the fields:
		/// OrderRoutestephandlerHistory.TerminalId - Terminal.TerminalId
		/// </summary>
		public virtual IEntityRelation TerminalEntityUsingTerminalId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TerminalEntity", false);
				relation.AddEntityFieldPair(TerminalFields.TerminalId, OrderRoutestephandlerHistoryFields.TerminalId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderRoutestephandlerHistoryEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticOrderRoutestephandlerHistoryRelations
	{
		internal static readonly IEntityRelation ExternalSystemEntityUsingExternalSystemIdStatic = new OrderRoutestephandlerHistoryRelations().ExternalSystemEntityUsingExternalSystemId;
		internal static readonly IEntityRelation OrderEntityUsingOrderIdStatic = new OrderRoutestephandlerHistoryRelations().OrderEntityUsingOrderId;
		internal static readonly IEntityRelation SupportpoolEntityUsingSupportpoolIdStatic = new OrderRoutestephandlerHistoryRelations().SupportpoolEntityUsingSupportpoolId;
		internal static readonly IEntityRelation TerminalEntityUsingForwardedFromTerminalIdStatic = new OrderRoutestephandlerHistoryRelations().TerminalEntityUsingForwardedFromTerminalId;
		internal static readonly IEntityRelation TerminalEntityUsingTerminalIdStatic = new OrderRoutestephandlerHistoryRelations().TerminalEntityUsingTerminalId;

		/// <summary>CTor</summary>
		static StaticOrderRoutestephandlerHistoryRelations()
		{
		}
	}
}
