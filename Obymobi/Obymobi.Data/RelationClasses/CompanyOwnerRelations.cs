﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: CompanyOwner. </summary>
	public partial class CompanyOwnerRelations
	{
		/// <summary>CTor</summary>
		public CompanyOwnerRelations()
		{
		}

		/// <summary>Gets all relations of the CompanyOwnerEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CompanyEntityUsingCompanyOwnerId);
			toReturn.Add(this.UserEntityUsingUserId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between CompanyOwnerEntity and CompanyEntity over the 1:n relation they have, using the relation between the fields:
		/// CompanyOwner.CompanyOwnerId - Company.CompanyOwnerId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyOwnerId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CompanyCollection" , true);
				relation.AddEntityFieldPair(CompanyOwnerFields.CompanyOwnerId, CompanyFields.CompanyOwnerId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyOwnerEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between CompanyOwnerEntity and UserEntity over the m:1 relation they have, using the relation between the fields:
		/// CompanyOwner.UserId - User.UserId
		/// </summary>
		public virtual IEntityRelation UserEntityUsingUserId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UserEntity", false);
				relation.AddEntityFieldPair(UserFields.UserId, CompanyOwnerFields.UserId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyOwnerEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCompanyOwnerRelations
	{
		internal static readonly IEntityRelation CompanyEntityUsingCompanyOwnerIdStatic = new CompanyOwnerRelations().CompanyEntityUsingCompanyOwnerId;
		internal static readonly IEntityRelation UserEntityUsingUserIdStatic = new CompanyOwnerRelations().UserEntityUsingUserId;

		/// <summary>CTor</summary>
		static StaticCompanyOwnerRelations()
		{
		}
	}
}
