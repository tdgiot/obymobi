﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: SupportpoolSupportagent. </summary>
	public partial class SupportpoolSupportagentRelations
	{
		/// <summary>CTor</summary>
		public SupportpoolSupportagentRelations()
		{
		}

		/// <summary>Gets all relations of the SupportpoolSupportagentEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.SupportagentEntityUsingSupportagentId);
			toReturn.Add(this.SupportpoolEntityUsingSupportpoolId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between SupportpoolSupportagentEntity and SupportagentEntity over the m:1 relation they have, using the relation between the fields:
		/// SupportpoolSupportagent.SupportagentId - Supportagent.SupportagentId
		/// </summary>
		public virtual IEntityRelation SupportagentEntityUsingSupportagentId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SupportagentEntity", false);
				relation.AddEntityFieldPair(SupportagentFields.SupportagentId, SupportpoolSupportagentFields.SupportagentId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SupportagentEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SupportpoolSupportagentEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between SupportpoolSupportagentEntity and SupportpoolEntity over the m:1 relation they have, using the relation between the fields:
		/// SupportpoolSupportagent.SupportpoolId - Supportpool.SupportpoolId
		/// </summary>
		public virtual IEntityRelation SupportpoolEntityUsingSupportpoolId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SupportpoolEntity", false);
				relation.AddEntityFieldPair(SupportpoolFields.SupportpoolId, SupportpoolSupportagentFields.SupportpoolId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SupportpoolEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SupportpoolSupportagentEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticSupportpoolSupportagentRelations
	{
		internal static readonly IEntityRelation SupportagentEntityUsingSupportagentIdStatic = new SupportpoolSupportagentRelations().SupportagentEntityUsingSupportagentId;
		internal static readonly IEntityRelation SupportpoolEntityUsingSupportpoolIdStatic = new SupportpoolSupportagentRelations().SupportpoolEntityUsingSupportpoolId;

		/// <summary>CTor</summary>
		static StaticSupportpoolSupportagentRelations()
		{
		}
	}
}
