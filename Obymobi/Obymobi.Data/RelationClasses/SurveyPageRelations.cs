﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: SurveyPage. </summary>
	public partial class SurveyPageRelations
	{
		/// <summary>CTor</summary>
		public SurveyPageRelations()
		{
		}

		/// <summary>Gets all relations of the SurveyPageEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.MediaEntityUsingSurveyPageId);
			toReturn.Add(this.SurveyQuestionEntityUsingSurveyPageId);
			toReturn.Add(this.SurveyEntityUsingSurveyId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between SurveyPageEntity and MediaEntity over the 1:n relation they have, using the relation between the fields:
		/// SurveyPage.SurveyPageId - Media.SurveyPageId
		/// </summary>
		public virtual IEntityRelation MediaEntityUsingSurveyPageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MediaCollection" , true);
				relation.AddEntityFieldPair(SurveyPageFields.SurveyPageId, MediaFields.SurveyPageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyPageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SurveyPageEntity and SurveyQuestionEntity over the 1:n relation they have, using the relation between the fields:
		/// SurveyPage.SurveyPageId - SurveyQuestion.SurveyPageId
		/// </summary>
		public virtual IEntityRelation SurveyQuestionEntityUsingSurveyPageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SurveyQuestionCollection" , true);
				relation.AddEntityFieldPair(SurveyPageFields.SurveyPageId, SurveyQuestionFields.SurveyPageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyPageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyQuestionEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between SurveyPageEntity and SurveyEntity over the m:1 relation they have, using the relation between the fields:
		/// SurveyPage.SurveyId - Survey.SurveyId
		/// </summary>
		public virtual IEntityRelation SurveyEntityUsingSurveyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SurveyEntity", false);
				relation.AddEntityFieldPair(SurveyFields.SurveyId, SurveyPageFields.SurveyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyPageEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticSurveyPageRelations
	{
		internal static readonly IEntityRelation MediaEntityUsingSurveyPageIdStatic = new SurveyPageRelations().MediaEntityUsingSurveyPageId;
		internal static readonly IEntityRelation SurveyQuestionEntityUsingSurveyPageIdStatic = new SurveyPageRelations().SurveyQuestionEntityUsingSurveyPageId;
		internal static readonly IEntityRelation SurveyEntityUsingSurveyIdStatic = new SurveyPageRelations().SurveyEntityUsingSurveyId;

		/// <summary>CTor</summary>
		static StaticSurveyPageRelations()
		{
		}
	}
}
