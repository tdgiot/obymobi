﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Attachment. </summary>
	public partial class AttachmentRelations
	{
		/// <summary>CTor</summary>
		public AttachmentRelations()
		{
		}

		/// <summary>Gets all relations of the AttachmentEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AttachmentLanguageEntityUsingAttachmentId);
			toReturn.Add(this.CustomTextEntityUsingAttachmentId);
			toReturn.Add(this.MediaEntityUsingAttachmentId);
			toReturn.Add(this.ProductAttachmentEntityUsingAttachmentId);
			toReturn.Add(this.GenericproductEntityUsingGenericproductId);
			toReturn.Add(this.PageEntityUsingPageId);
			toReturn.Add(this.PageTemplateEntityUsingPageTemplateId);
			toReturn.Add(this.ProductEntityUsingProductId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between AttachmentEntity and AttachmentLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Attachment.AttachmentId - AttachmentLanguage.AttachmentId
		/// </summary>
		public virtual IEntityRelation AttachmentLanguageEntityUsingAttachmentId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AttachmentLanguageCollection" , true);
				relation.AddEntityFieldPair(AttachmentFields.AttachmentId, AttachmentLanguageFields.AttachmentId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttachmentEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttachmentLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AttachmentEntity and CustomTextEntity over the 1:n relation they have, using the relation between the fields:
		/// Attachment.AttachmentId - CustomText.AttachmentId
		/// </summary>
		public virtual IEntityRelation CustomTextEntityUsingAttachmentId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomTextCollection" , true);
				relation.AddEntityFieldPair(AttachmentFields.AttachmentId, CustomTextFields.AttachmentId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttachmentEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AttachmentEntity and MediaEntity over the 1:n relation they have, using the relation between the fields:
		/// Attachment.AttachmentId - Media.AttachmentId
		/// </summary>
		public virtual IEntityRelation MediaEntityUsingAttachmentId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MediaCollection" , true);
				relation.AddEntityFieldPair(AttachmentFields.AttachmentId, MediaFields.AttachmentId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttachmentEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AttachmentEntity and ProductAttachmentEntity over the 1:n relation they have, using the relation between the fields:
		/// Attachment.AttachmentId - ProductAttachment.AttachmentId
		/// </summary>
		public virtual IEntityRelation ProductAttachmentEntityUsingAttachmentId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ProductAttachmentCollection" , true);
				relation.AddEntityFieldPair(AttachmentFields.AttachmentId, ProductAttachmentFields.AttachmentId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttachmentEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductAttachmentEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between AttachmentEntity and GenericproductEntity over the m:1 relation they have, using the relation between the fields:
		/// Attachment.GenericproductId - Genericproduct.GenericproductId
		/// </summary>
		public virtual IEntityRelation GenericproductEntityUsingGenericproductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "GenericproductEntity", false);
				relation.AddEntityFieldPair(GenericproductFields.GenericproductId, AttachmentFields.GenericproductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericproductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttachmentEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AttachmentEntity and PageEntity over the m:1 relation they have, using the relation between the fields:
		/// Attachment.PageId - Page.PageId
		/// </summary>
		public virtual IEntityRelation PageEntityUsingPageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PageEntity", false);
				relation.AddEntityFieldPair(PageFields.PageId, AttachmentFields.PageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttachmentEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AttachmentEntity and PageTemplateEntity over the m:1 relation they have, using the relation between the fields:
		/// Attachment.PageTemplateId - PageTemplate.PageTemplateId
		/// </summary>
		public virtual IEntityRelation PageTemplateEntityUsingPageTemplateId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PageTemplateEntity", false);
				relation.AddEntityFieldPair(PageTemplateFields.PageTemplateId, AttachmentFields.PageTemplateId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageTemplateEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttachmentEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AttachmentEntity and ProductEntity over the m:1 relation they have, using the relation between the fields:
		/// Attachment.ProductId - Product.ProductId
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ProductEntity", false);
				relation.AddEntityFieldPair(ProductFields.ProductId, AttachmentFields.ProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttachmentEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticAttachmentRelations
	{
		internal static readonly IEntityRelation AttachmentLanguageEntityUsingAttachmentIdStatic = new AttachmentRelations().AttachmentLanguageEntityUsingAttachmentId;
		internal static readonly IEntityRelation CustomTextEntityUsingAttachmentIdStatic = new AttachmentRelations().CustomTextEntityUsingAttachmentId;
		internal static readonly IEntityRelation MediaEntityUsingAttachmentIdStatic = new AttachmentRelations().MediaEntityUsingAttachmentId;
		internal static readonly IEntityRelation ProductAttachmentEntityUsingAttachmentIdStatic = new AttachmentRelations().ProductAttachmentEntityUsingAttachmentId;
		internal static readonly IEntityRelation GenericproductEntityUsingGenericproductIdStatic = new AttachmentRelations().GenericproductEntityUsingGenericproductId;
		internal static readonly IEntityRelation PageEntityUsingPageIdStatic = new AttachmentRelations().PageEntityUsingPageId;
		internal static readonly IEntityRelation PageTemplateEntityUsingPageTemplateIdStatic = new AttachmentRelations().PageTemplateEntityUsingPageTemplateId;
		internal static readonly IEntityRelation ProductEntityUsingProductIdStatic = new AttachmentRelations().ProductEntityUsingProductId;

		/// <summary>CTor</summary>
		static StaticAttachmentRelations()
		{
		}
	}
}
