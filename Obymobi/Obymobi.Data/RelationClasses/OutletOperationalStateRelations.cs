﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: OutletOperationalState. </summary>
	public partial class OutletOperationalStateRelations
	{
		/// <summary>CTor</summary>
		public OutletOperationalStateRelations()
		{
		}

		/// <summary>Gets all relations of the OutletOperationalStateEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.OutletEntityUsingOutletOperationalStateId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between OutletOperationalStateEntity and OutletEntity over the 1:n relation they have, using the relation between the fields:
		/// OutletOperationalState.OutletOperationalStateId - Outlet.OutletOperationalStateId
		/// </summary>
		public virtual IEntityRelation OutletEntityUsingOutletOperationalStateId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OutletCollection" , true);
				relation.AddEntityFieldPair(OutletOperationalStateFields.OutletOperationalStateId, OutletFields.OutletOperationalStateId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OutletOperationalStateEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OutletEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticOutletOperationalStateRelations
	{
		internal static readonly IEntityRelation OutletEntityUsingOutletOperationalStateIdStatic = new OutletOperationalStateRelations().OutletEntityUsingOutletOperationalStateId;

		/// <summary>CTor</summary>
		static StaticOutletOperationalStateRelations()
		{
		}
	}
}
