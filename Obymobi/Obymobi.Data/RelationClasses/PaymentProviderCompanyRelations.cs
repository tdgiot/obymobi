﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: PaymentProviderCompany. </summary>
	public partial class PaymentProviderCompanyRelations
	{
		/// <summary>CTor</summary>
		public PaymentProviderCompanyRelations()
		{
		}

		/// <summary>Gets all relations of the PaymentProviderCompanyEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			toReturn.Add(this.PaymentProviderEntityUsingPaymentProviderId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between PaymentProviderCompanyEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// PaymentProviderCompany.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, PaymentProviderCompanyFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentProviderCompanyEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between PaymentProviderCompanyEntity and PaymentProviderEntity over the m:1 relation they have, using the relation between the fields:
		/// PaymentProviderCompany.PaymentProviderId - PaymentProvider.PaymentProviderId
		/// </summary>
		public virtual IEntityRelation PaymentProviderEntityUsingPaymentProviderId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PaymentProviderEntity", false);
				relation.AddEntityFieldPair(PaymentProviderFields.PaymentProviderId, PaymentProviderCompanyFields.PaymentProviderId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentProviderEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentProviderCompanyEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPaymentProviderCompanyRelations
	{
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new PaymentProviderCompanyRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation PaymentProviderEntityUsingPaymentProviderIdStatic = new PaymentProviderCompanyRelations().PaymentProviderEntityUsingPaymentProviderId;

		/// <summary>CTor</summary>
		static StaticPaymentProviderCompanyRelations()
		{
		}
	}
}
