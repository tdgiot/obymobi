﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: SiteTemplate. </summary>
	public partial class SiteTemplateRelations
	{
		/// <summary>CTor</summary>
		public SiteTemplateRelations()
		{
		}

		/// <summary>Gets all relations of the SiteTemplateEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CustomTextEntityUsingSiteTemplateId);
			toReturn.Add(this.PageTemplateEntityUsingSiteTemplateId);
			toReturn.Add(this.SiteEntityUsingSiteTemplateId);
			toReturn.Add(this.SiteTemplateCultureEntityUsingSiteTemplateId);
			toReturn.Add(this.SiteTemplateLanguageEntityUsingSiteTemplateId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between SiteTemplateEntity and CustomTextEntity over the 1:n relation they have, using the relation between the fields:
		/// SiteTemplate.SiteTemplateId - CustomText.SiteTemplateId
		/// </summary>
		public virtual IEntityRelation CustomTextEntityUsingSiteTemplateId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomTextCollection" , true);
				relation.AddEntityFieldPair(SiteTemplateFields.SiteTemplateId, CustomTextFields.SiteTemplateId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SiteTemplateEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SiteTemplateEntity and PageTemplateEntity over the 1:n relation they have, using the relation between the fields:
		/// SiteTemplate.SiteTemplateId - PageTemplate.SiteTemplateId
		/// </summary>
		public virtual IEntityRelation PageTemplateEntityUsingSiteTemplateId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PageTemplateCollection" , true);
				relation.AddEntityFieldPair(SiteTemplateFields.SiteTemplateId, PageTemplateFields.SiteTemplateId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SiteTemplateEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageTemplateEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SiteTemplateEntity and SiteEntity over the 1:n relation they have, using the relation between the fields:
		/// SiteTemplate.SiteTemplateId - Site.SiteTemplateId
		/// </summary>
		public virtual IEntityRelation SiteEntityUsingSiteTemplateId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SiteCollection" , true);
				relation.AddEntityFieldPair(SiteTemplateFields.SiteTemplateId, SiteFields.SiteTemplateId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SiteTemplateEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SiteEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SiteTemplateEntity and SiteTemplateCultureEntity over the 1:n relation they have, using the relation between the fields:
		/// SiteTemplate.SiteTemplateId - SiteTemplateCulture.SiteTemplateId
		/// </summary>
		public virtual IEntityRelation SiteTemplateCultureEntityUsingSiteTemplateId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SiteTemplateCultureCollection" , true);
				relation.AddEntityFieldPair(SiteTemplateFields.SiteTemplateId, SiteTemplateCultureFields.SiteTemplateId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SiteTemplateEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SiteTemplateCultureEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SiteTemplateEntity and SiteTemplateLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// SiteTemplate.SiteTemplateId - SiteTemplateLanguage.SiteTemplateId
		/// </summary>
		public virtual IEntityRelation SiteTemplateLanguageEntityUsingSiteTemplateId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SiteTemplateLanguageCollection" , true);
				relation.AddEntityFieldPair(SiteTemplateFields.SiteTemplateId, SiteTemplateLanguageFields.SiteTemplateId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SiteTemplateEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SiteTemplateLanguageEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticSiteTemplateRelations
	{
		internal static readonly IEntityRelation CustomTextEntityUsingSiteTemplateIdStatic = new SiteTemplateRelations().CustomTextEntityUsingSiteTemplateId;
		internal static readonly IEntityRelation PageTemplateEntityUsingSiteTemplateIdStatic = new SiteTemplateRelations().PageTemplateEntityUsingSiteTemplateId;
		internal static readonly IEntityRelation SiteEntityUsingSiteTemplateIdStatic = new SiteTemplateRelations().SiteEntityUsingSiteTemplateId;
		internal static readonly IEntityRelation SiteTemplateCultureEntityUsingSiteTemplateIdStatic = new SiteTemplateRelations().SiteTemplateCultureEntityUsingSiteTemplateId;
		internal static readonly IEntityRelation SiteTemplateLanguageEntityUsingSiteTemplateIdStatic = new SiteTemplateRelations().SiteTemplateLanguageEntityUsingSiteTemplateId;

		/// <summary>CTor</summary>
		static StaticSiteTemplateRelations()
		{
		}
	}
}
