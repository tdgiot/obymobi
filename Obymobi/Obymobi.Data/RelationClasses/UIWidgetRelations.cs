﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: UIWidget. </summary>
	public partial class UIWidgetRelations
	{
		/// <summary>CTor</summary>
		public UIWidgetRelations()
		{
		}

		/// <summary>Gets all relations of the UIWidgetEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CustomTextEntityUsingUIWidgetId);
			toReturn.Add(this.MediaEntityUsingUIWidgetId);
			toReturn.Add(this.UIScheduleItemEntityUsingUIWidgetId);
			toReturn.Add(this.UIWidgetAvailabilityEntityUsingUIWidgetId);
			toReturn.Add(this.UIWidgetLanguageEntityUsingUIWidgetId);
			toReturn.Add(this.UIWidgetTimerEntityUsingUIWidgetId);
			toReturn.Add(this.AdvertisementEntityUsingAdvertisementId);
			toReturn.Add(this.CategoryEntityUsingCategoryId);
			toReturn.Add(this.EntertainmentEntityUsingEntertainmentId);
			toReturn.Add(this.EntertainmentcategoryEntityUsingEntertainmentcategoryId);
			toReturn.Add(this.PageEntityUsingPageId);
			toReturn.Add(this.ProductCategoryEntityUsingProductCategoryId);
			toReturn.Add(this.RoomControlAreaEntityUsingRoomControlAreaId);
			toReturn.Add(this.RoomControlSectionEntityUsingRoomControlSectionId);
			toReturn.Add(this.SiteEntityUsingSiteId);
			toReturn.Add(this.UITabEntityUsingUITabId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between UIWidgetEntity and CustomTextEntity over the 1:n relation they have, using the relation between the fields:
		/// UIWidget.UIWidgetId - CustomText.UIWidgetId
		/// </summary>
		public virtual IEntityRelation CustomTextEntityUsingUIWidgetId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomTextCollection" , true);
				relation.AddEntityFieldPair(UIWidgetFields.UIWidgetId, CustomTextFields.UIWidgetId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIWidgetEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UIWidgetEntity and MediaEntity over the 1:n relation they have, using the relation between the fields:
		/// UIWidget.UIWidgetId - Media.UIWidgetId
		/// </summary>
		public virtual IEntityRelation MediaEntityUsingUIWidgetId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MediaCollection" , true);
				relation.AddEntityFieldPair(UIWidgetFields.UIWidgetId, MediaFields.UIWidgetId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIWidgetEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UIWidgetEntity and UIScheduleItemEntity over the 1:n relation they have, using the relation between the fields:
		/// UIWidget.UIWidgetId - UIScheduleItem.UIWidgetId
		/// </summary>
		public virtual IEntityRelation UIScheduleItemEntityUsingUIWidgetId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UIScheduleItemCollection" , true);
				relation.AddEntityFieldPair(UIWidgetFields.UIWidgetId, UIScheduleItemFields.UIWidgetId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIWidgetEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIScheduleItemEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UIWidgetEntity and UIWidgetAvailabilityEntity over the 1:n relation they have, using the relation between the fields:
		/// UIWidget.UIWidgetId - UIWidgetAvailability.UIWidgetId
		/// </summary>
		public virtual IEntityRelation UIWidgetAvailabilityEntityUsingUIWidgetId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UIWidgetAvailabilityCollection" , true);
				relation.AddEntityFieldPair(UIWidgetFields.UIWidgetId, UIWidgetAvailabilityFields.UIWidgetId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIWidgetEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIWidgetAvailabilityEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UIWidgetEntity and UIWidgetLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// UIWidget.UIWidgetId - UIWidgetLanguage.UIWidgetId
		/// </summary>
		public virtual IEntityRelation UIWidgetLanguageEntityUsingUIWidgetId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UIWidgetLanguageCollection" , true);
				relation.AddEntityFieldPair(UIWidgetFields.UIWidgetId, UIWidgetLanguageFields.UIWidgetId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIWidgetEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIWidgetLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UIWidgetEntity and UIWidgetTimerEntity over the 1:n relation they have, using the relation between the fields:
		/// UIWidget.UIWidgetId - UIWidgetTimer.UIWidgetId
		/// </summary>
		public virtual IEntityRelation UIWidgetTimerEntityUsingUIWidgetId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UIWidgetTimerCollection" , true);
				relation.AddEntityFieldPair(UIWidgetFields.UIWidgetId, UIWidgetTimerFields.UIWidgetId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIWidgetEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIWidgetTimerEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between UIWidgetEntity and AdvertisementEntity over the m:1 relation they have, using the relation between the fields:
		/// UIWidget.AdvertisementId - Advertisement.AdvertisementId
		/// </summary>
		public virtual IEntityRelation AdvertisementEntityUsingAdvertisementId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AdvertisementEntity", false);
				relation.AddEntityFieldPair(AdvertisementFields.AdvertisementId, UIWidgetFields.AdvertisementId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIWidgetEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between UIWidgetEntity and CategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// UIWidget.CategoryId - Category.CategoryId
		/// </summary>
		public virtual IEntityRelation CategoryEntityUsingCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CategoryEntity", false);
				relation.AddEntityFieldPair(CategoryFields.CategoryId, UIWidgetFields.CategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIWidgetEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between UIWidgetEntity and EntertainmentEntity over the m:1 relation they have, using the relation between the fields:
		/// UIWidget.EntertainmentId - Entertainment.EntertainmentId
		/// </summary>
		public virtual IEntityRelation EntertainmentEntityUsingEntertainmentId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "EntertainmentEntity", false);
				relation.AddEntityFieldPair(EntertainmentFields.EntertainmentId, UIWidgetFields.EntertainmentId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIWidgetEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between UIWidgetEntity and EntertainmentcategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// UIWidget.EntertainmentcategoryId - Entertainmentcategory.EntertainmentcategoryId
		/// </summary>
		public virtual IEntityRelation EntertainmentcategoryEntityUsingEntertainmentcategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "EntertainmentcategoryEntity", false);
				relation.AddEntityFieldPair(EntertainmentcategoryFields.EntertainmentcategoryId, UIWidgetFields.EntertainmentcategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentcategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIWidgetEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between UIWidgetEntity and PageEntity over the m:1 relation they have, using the relation between the fields:
		/// UIWidget.PageId - Page.PageId
		/// </summary>
		public virtual IEntityRelation PageEntityUsingPageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PageEntity", false);
				relation.AddEntityFieldPair(PageFields.PageId, UIWidgetFields.PageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIWidgetEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between UIWidgetEntity and ProductCategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// UIWidget.ProductCategoryId - ProductCategory.ProductCategoryId
		/// </summary>
		public virtual IEntityRelation ProductCategoryEntityUsingProductCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ProductCategoryEntity", false);
				relation.AddEntityFieldPair(ProductCategoryFields.ProductCategoryId, UIWidgetFields.ProductCategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductCategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIWidgetEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between UIWidgetEntity and RoomControlAreaEntity over the m:1 relation they have, using the relation between the fields:
		/// UIWidget.RoomControlAreaId - RoomControlArea.RoomControlAreaId
		/// </summary>
		public virtual IEntityRelation RoomControlAreaEntityUsingRoomControlAreaId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RoomControlAreaEntity", false);
				relation.AddEntityFieldPair(RoomControlAreaFields.RoomControlAreaId, UIWidgetFields.RoomControlAreaId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlAreaEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIWidgetEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between UIWidgetEntity and RoomControlSectionEntity over the m:1 relation they have, using the relation between the fields:
		/// UIWidget.RoomControlSectionId - RoomControlSection.RoomControlSectionId
		/// </summary>
		public virtual IEntityRelation RoomControlSectionEntityUsingRoomControlSectionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RoomControlSectionEntity", false);
				relation.AddEntityFieldPair(RoomControlSectionFields.RoomControlSectionId, UIWidgetFields.RoomControlSectionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlSectionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIWidgetEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between UIWidgetEntity and SiteEntity over the m:1 relation they have, using the relation between the fields:
		/// UIWidget.SiteId - Site.SiteId
		/// </summary>
		public virtual IEntityRelation SiteEntityUsingSiteId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SiteEntity", false);
				relation.AddEntityFieldPair(SiteFields.SiteId, UIWidgetFields.SiteId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SiteEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIWidgetEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between UIWidgetEntity and UITabEntity over the m:1 relation they have, using the relation between the fields:
		/// UIWidget.UITabId - UITab.UITabId
		/// </summary>
		public virtual IEntityRelation UITabEntityUsingUITabId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UITabEntity", false);
				relation.AddEntityFieldPair(UITabFields.UITabId, UIWidgetFields.UITabId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UITabEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIWidgetEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticUIWidgetRelations
	{
		internal static readonly IEntityRelation CustomTextEntityUsingUIWidgetIdStatic = new UIWidgetRelations().CustomTextEntityUsingUIWidgetId;
		internal static readonly IEntityRelation MediaEntityUsingUIWidgetIdStatic = new UIWidgetRelations().MediaEntityUsingUIWidgetId;
		internal static readonly IEntityRelation UIScheduleItemEntityUsingUIWidgetIdStatic = new UIWidgetRelations().UIScheduleItemEntityUsingUIWidgetId;
		internal static readonly IEntityRelation UIWidgetAvailabilityEntityUsingUIWidgetIdStatic = new UIWidgetRelations().UIWidgetAvailabilityEntityUsingUIWidgetId;
		internal static readonly IEntityRelation UIWidgetLanguageEntityUsingUIWidgetIdStatic = new UIWidgetRelations().UIWidgetLanguageEntityUsingUIWidgetId;
		internal static readonly IEntityRelation UIWidgetTimerEntityUsingUIWidgetIdStatic = new UIWidgetRelations().UIWidgetTimerEntityUsingUIWidgetId;
		internal static readonly IEntityRelation AdvertisementEntityUsingAdvertisementIdStatic = new UIWidgetRelations().AdvertisementEntityUsingAdvertisementId;
		internal static readonly IEntityRelation CategoryEntityUsingCategoryIdStatic = new UIWidgetRelations().CategoryEntityUsingCategoryId;
		internal static readonly IEntityRelation EntertainmentEntityUsingEntertainmentIdStatic = new UIWidgetRelations().EntertainmentEntityUsingEntertainmentId;
		internal static readonly IEntityRelation EntertainmentcategoryEntityUsingEntertainmentcategoryIdStatic = new UIWidgetRelations().EntertainmentcategoryEntityUsingEntertainmentcategoryId;
		internal static readonly IEntityRelation PageEntityUsingPageIdStatic = new UIWidgetRelations().PageEntityUsingPageId;
		internal static readonly IEntityRelation ProductCategoryEntityUsingProductCategoryIdStatic = new UIWidgetRelations().ProductCategoryEntityUsingProductCategoryId;
		internal static readonly IEntityRelation RoomControlAreaEntityUsingRoomControlAreaIdStatic = new UIWidgetRelations().RoomControlAreaEntityUsingRoomControlAreaId;
		internal static readonly IEntityRelation RoomControlSectionEntityUsingRoomControlSectionIdStatic = new UIWidgetRelations().RoomControlSectionEntityUsingRoomControlSectionId;
		internal static readonly IEntityRelation SiteEntityUsingSiteIdStatic = new UIWidgetRelations().SiteEntityUsingSiteId;
		internal static readonly IEntityRelation UITabEntityUsingUITabIdStatic = new UIWidgetRelations().UITabEntityUsingUITabId;

		/// <summary>CTor</summary>
		static StaticUIWidgetRelations()
		{
		}
	}
}
