﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: AmenityLanguage. </summary>
	public partial class AmenityLanguageRelations
	{
		/// <summary>CTor</summary>
		public AmenityLanguageRelations()
		{
		}

		/// <summary>Gets all relations of the AmenityLanguageEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AmenityEntityUsingAmenityId);
			toReturn.Add(this.LanguageEntityUsingLanguageId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between AmenityLanguageEntity and AmenityEntity over the m:1 relation they have, using the relation between the fields:
		/// AmenityLanguage.AmenityId - Amenity.AmenityId
		/// </summary>
		public virtual IEntityRelation AmenityEntityUsingAmenityId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AmenityEntity", false);
				relation.AddEntityFieldPair(AmenityFields.AmenityId, AmenityLanguageFields.AmenityId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AmenityEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AmenityLanguageEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AmenityLanguageEntity and LanguageEntity over the m:1 relation they have, using the relation between the fields:
		/// AmenityLanguage.LanguageId - Language.LanguageId
		/// </summary>
		public virtual IEntityRelation LanguageEntityUsingLanguageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "LanguageEntity", false);
				relation.AddEntityFieldPair(LanguageFields.LanguageId, AmenityLanguageFields.LanguageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AmenityLanguageEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticAmenityLanguageRelations
	{
		internal static readonly IEntityRelation AmenityEntityUsingAmenityIdStatic = new AmenityLanguageRelations().AmenityEntityUsingAmenityId;
		internal static readonly IEntityRelation LanguageEntityUsingLanguageIdStatic = new AmenityLanguageRelations().LanguageEntityUsingLanguageId;

		/// <summary>CTor</summary>
		static StaticAmenityLanguageRelations()
		{
		}
	}
}
