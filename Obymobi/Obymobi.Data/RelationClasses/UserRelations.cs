﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: User. </summary>
	public partial class UserRelations
	{
		/// <summary>CTor</summary>
		public UserRelations()
		{
		}

		/// <summary>Gets all relations of the UserEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CompanyOwnerEntityUsingUserId);
			toReturn.Add(this.DeviceTokenHistoryEntityUsingUserId);
			toReturn.Add(this.DeviceTokenTaskEntityUsingUserId);
			toReturn.Add(this.PublishingEntityUsingUserId);
			toReturn.Add(this.TerminalEntityUsingAutomaticSignOnUserId);
			toReturn.Add(this.UserBrandEntityUsingUserId);
			toReturn.Add(this.UserLogonEntityUsingUserId);
			toReturn.Add(this.UserRoleEntityUsingUserId);
			toReturn.Add(this.ViewCustomEntityUsingUserId);
			toReturn.Add(this.AccountEntityUsingAccountId);
			toReturn.Add(this.TimeZoneEntityUsingTimeZoneId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between UserEntity and CompanyOwnerEntity over the 1:n relation they have, using the relation between the fields:
		/// User.UserId - CompanyOwner.UserId
		/// </summary>
		public virtual IEntityRelation CompanyOwnerEntityUsingUserId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CompanyOwnerCollection" , true);
				relation.AddEntityFieldPair(UserFields.UserId, CompanyOwnerFields.UserId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyOwnerEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UserEntity and DeviceTokenHistoryEntity over the 1:n relation they have, using the relation between the fields:
		/// User.UserId - DeviceTokenHistory.UserId
		/// </summary>
		public virtual IEntityRelation DeviceTokenHistoryEntityUsingUserId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeviceTokenHistoryCollection" , true);
				relation.AddEntityFieldPair(UserFields.UserId, DeviceTokenHistoryFields.UserId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceTokenHistoryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UserEntity and DeviceTokenTaskEntity over the 1:n relation they have, using the relation between the fields:
		/// User.UserId - DeviceTokenTask.UserId
		/// </summary>
		public virtual IEntityRelation DeviceTokenTaskEntityUsingUserId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeviceTokenTaskCollection" , true);
				relation.AddEntityFieldPair(UserFields.UserId, DeviceTokenTaskFields.UserId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceTokenTaskEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UserEntity and PublishingEntity over the 1:n relation they have, using the relation between the fields:
		/// User.UserId - Publishing.UserId
		/// </summary>
		public virtual IEntityRelation PublishingEntityUsingUserId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PublishingCollection" , true);
				relation.AddEntityFieldPair(UserFields.UserId, PublishingFields.UserId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PublishingEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UserEntity and TerminalEntity over the 1:n relation they have, using the relation between the fields:
		/// User.UserId - Terminal.AutomaticSignOnUserId
		/// </summary>
		public virtual IEntityRelation TerminalEntityUsingAutomaticSignOnUserId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TerminalCollection" , true);
				relation.AddEntityFieldPair(UserFields.UserId, TerminalFields.AutomaticSignOnUserId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UserEntity and UserBrandEntity over the 1:n relation they have, using the relation between the fields:
		/// User.UserId - UserBrand.UserId
		/// </summary>
		public virtual IEntityRelation UserBrandEntityUsingUserId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UserBrandCollection" , true);
				relation.AddEntityFieldPair(UserFields.UserId, UserBrandFields.UserId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserBrandEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UserEntity and UserLogonEntity over the 1:n relation they have, using the relation between the fields:
		/// User.UserId - UserLogon.UserId
		/// </summary>
		public virtual IEntityRelation UserLogonEntityUsingUserId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UserLogonCollection" , true);
				relation.AddEntityFieldPair(UserFields.UserId, UserLogonFields.UserId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserLogonEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UserEntity and UserRoleEntity over the 1:n relation they have, using the relation between the fields:
		/// User.UserId - UserRole.UserId
		/// </summary>
		public virtual IEntityRelation UserRoleEntityUsingUserId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UserRoleCollection" , true);
				relation.AddEntityFieldPair(UserFields.UserId, UserRoleFields.UserId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserRoleEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UserEntity and ViewCustomEntity over the 1:n relation they have, using the relation between the fields:
		/// User.UserId - ViewCustom.UserId
		/// </summary>
		public virtual IEntityRelation ViewCustomEntityUsingUserId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ViewCustomCollection" , true);
				relation.AddEntityFieldPair(UserFields.UserId, ViewCustomFields.UserId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ViewCustomEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between UserEntity and AccountEntity over the m:1 relation they have, using the relation between the fields:
		/// User.AccountId - Account.AccountId
		/// </summary>
		public virtual IEntityRelation AccountEntityUsingAccountId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AccountEntity", false);
				relation.AddEntityFieldPair(AccountFields.AccountId, UserFields.AccountId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AccountEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between UserEntity and TimeZoneEntity over the m:1 relation they have, using the relation between the fields:
		/// User.TimeZoneId - TimeZone.TimeZoneId
		/// </summary>
		public virtual IEntityRelation TimeZoneEntityUsingTimeZoneId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TimeZoneEntity", false);
				relation.AddEntityFieldPair(TimeZoneFields.TimeZoneId, UserFields.TimeZoneId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TimeZoneEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticUserRelations
	{
		internal static readonly IEntityRelation CompanyOwnerEntityUsingUserIdStatic = new UserRelations().CompanyOwnerEntityUsingUserId;
		internal static readonly IEntityRelation DeviceTokenHistoryEntityUsingUserIdStatic = new UserRelations().DeviceTokenHistoryEntityUsingUserId;
		internal static readonly IEntityRelation DeviceTokenTaskEntityUsingUserIdStatic = new UserRelations().DeviceTokenTaskEntityUsingUserId;
		internal static readonly IEntityRelation PublishingEntityUsingUserIdStatic = new UserRelations().PublishingEntityUsingUserId;
		internal static readonly IEntityRelation TerminalEntityUsingAutomaticSignOnUserIdStatic = new UserRelations().TerminalEntityUsingAutomaticSignOnUserId;
		internal static readonly IEntityRelation UserBrandEntityUsingUserIdStatic = new UserRelations().UserBrandEntityUsingUserId;
		internal static readonly IEntityRelation UserLogonEntityUsingUserIdStatic = new UserRelations().UserLogonEntityUsingUserId;
		internal static readonly IEntityRelation UserRoleEntityUsingUserIdStatic = new UserRelations().UserRoleEntityUsingUserId;
		internal static readonly IEntityRelation ViewCustomEntityUsingUserIdStatic = new UserRelations().ViewCustomEntityUsingUserId;
		internal static readonly IEntityRelation AccountEntityUsingAccountIdStatic = new UserRelations().AccountEntityUsingAccountId;
		internal static readonly IEntityRelation TimeZoneEntityUsingTimeZoneIdStatic = new UserRelations().TimeZoneEntityUsingTimeZoneId;

		/// <summary>CTor</summary>
		static StaticUserRelations()
		{
		}
	}
}
