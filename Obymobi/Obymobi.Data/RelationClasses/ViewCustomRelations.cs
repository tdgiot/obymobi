﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ViewCustom. </summary>
	public partial class ViewCustomRelations
	{
		/// <summary>CTor</summary>
		public ViewCustomRelations()
		{
		}

		/// <summary>Gets all relations of the ViewCustomEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ViewItemCustomEntityUsingViewCustomId);
			toReturn.Add(this.UserEntityUsingUserId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ViewCustomEntity and ViewItemCustomEntity over the 1:n relation they have, using the relation between the fields:
		/// ViewCustom.ViewCustomId - ViewItemCustom.ViewCustomId
		/// </summary>
		public virtual IEntityRelation ViewItemCustomEntityUsingViewCustomId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ViewItemCustomCollection" , true);
				relation.AddEntityFieldPair(ViewCustomFields.ViewCustomId, ViewItemCustomFields.ViewCustomId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ViewCustomEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ViewItemCustomEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between ViewCustomEntity and UserEntity over the m:1 relation they have, using the relation between the fields:
		/// ViewCustom.UserId - User.UserId
		/// </summary>
		public virtual IEntityRelation UserEntityUsingUserId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UserEntity", false);
				relation.AddEntityFieldPair(UserFields.UserId, ViewCustomFields.UserId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ViewCustomEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticViewCustomRelations
	{
		internal static readonly IEntityRelation ViewItemCustomEntityUsingViewCustomIdStatic = new ViewCustomRelations().ViewItemCustomEntityUsingViewCustomId;
		internal static readonly IEntityRelation UserEntityUsingUserIdStatic = new ViewCustomRelations().UserEntityUsingUserId;

		/// <summary>CTor</summary>
		static StaticViewCustomRelations()
		{
		}
	}
}
