﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: PaymentIntegrationConfiguration. </summary>
	public partial class PaymentIntegrationConfigurationRelations
	{
		/// <summary>CTor</summary>
		public PaymentIntegrationConfigurationRelations()
		{
		}

		/// <summary>Gets all relations of the PaymentIntegrationConfigurationEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AdyenPaymentMethodEntityUsingPaymentIntegrationConfigurationId);
			toReturn.Add(this.CheckoutMethodEntityUsingPaymentIntegrationConfigurationId);
			toReturn.Add(this.PaymentTransactionEntityUsingPaymentIntegrationConfigurationId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			toReturn.Add(this.PaymentProviderEntityUsingPaymentProviderId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between PaymentIntegrationConfigurationEntity and AdyenPaymentMethodEntity over the 1:n relation they have, using the relation between the fields:
		/// PaymentIntegrationConfiguration.PaymentIntegrationConfigurationId - AdyenPaymentMethod.PaymentIntegrationConfigurationId
		/// </summary>
		public virtual IEntityRelation AdyenPaymentMethodEntityUsingPaymentIntegrationConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AdyenPaymentMethodCollection" , true);
				relation.AddEntityFieldPair(PaymentIntegrationConfigurationFields.PaymentIntegrationConfigurationId, AdyenPaymentMethodFields.PaymentIntegrationConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentIntegrationConfigurationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdyenPaymentMethodEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PaymentIntegrationConfigurationEntity and CheckoutMethodEntity over the 1:n relation they have, using the relation between the fields:
		/// PaymentIntegrationConfiguration.PaymentIntegrationConfigurationId - CheckoutMethod.PaymentIntegrationConfigurationId
		/// </summary>
		public virtual IEntityRelation CheckoutMethodEntityUsingPaymentIntegrationConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CheckoutMethodCollection" , true);
				relation.AddEntityFieldPair(PaymentIntegrationConfigurationFields.PaymentIntegrationConfigurationId, CheckoutMethodFields.PaymentIntegrationConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentIntegrationConfigurationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CheckoutMethodEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PaymentIntegrationConfigurationEntity and PaymentTransactionEntity over the 1:n relation they have, using the relation between the fields:
		/// PaymentIntegrationConfiguration.PaymentIntegrationConfigurationId - PaymentTransaction.PaymentIntegrationConfigurationId
		/// </summary>
		public virtual IEntityRelation PaymentTransactionEntityUsingPaymentIntegrationConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PaymentTransactionCollection" , true);
				relation.AddEntityFieldPair(PaymentIntegrationConfigurationFields.PaymentIntegrationConfigurationId, PaymentTransactionFields.PaymentIntegrationConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentIntegrationConfigurationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentTransactionEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between PaymentIntegrationConfigurationEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// PaymentIntegrationConfiguration.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, PaymentIntegrationConfigurationFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentIntegrationConfigurationEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between PaymentIntegrationConfigurationEntity and PaymentProviderEntity over the m:1 relation they have, using the relation between the fields:
		/// PaymentIntegrationConfiguration.PaymentProviderId - PaymentProvider.PaymentProviderId
		/// </summary>
		public virtual IEntityRelation PaymentProviderEntityUsingPaymentProviderId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PaymentProviderEntity", false);
				relation.AddEntityFieldPair(PaymentProviderFields.PaymentProviderId, PaymentIntegrationConfigurationFields.PaymentProviderId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentProviderEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentIntegrationConfigurationEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPaymentIntegrationConfigurationRelations
	{
		internal static readonly IEntityRelation AdyenPaymentMethodEntityUsingPaymentIntegrationConfigurationIdStatic = new PaymentIntegrationConfigurationRelations().AdyenPaymentMethodEntityUsingPaymentIntegrationConfigurationId;
		internal static readonly IEntityRelation CheckoutMethodEntityUsingPaymentIntegrationConfigurationIdStatic = new PaymentIntegrationConfigurationRelations().CheckoutMethodEntityUsingPaymentIntegrationConfigurationId;
		internal static readonly IEntityRelation PaymentTransactionEntityUsingPaymentIntegrationConfigurationIdStatic = new PaymentIntegrationConfigurationRelations().PaymentTransactionEntityUsingPaymentIntegrationConfigurationId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new PaymentIntegrationConfigurationRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation PaymentProviderEntityUsingPaymentProviderIdStatic = new PaymentIntegrationConfigurationRelations().PaymentProviderEntityUsingPaymentProviderId;

		/// <summary>CTor</summary>
		static StaticPaymentIntegrationConfigurationRelations()
		{
		}
	}
}
