﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: NavigationMenu. </summary>
	public partial class NavigationMenuRelations
	{
		/// <summary>CTor</summary>
		public NavigationMenuRelations()
		{
		}

		/// <summary>Gets all relations of the NavigationMenuEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.LandingPageEntityUsingNavigationMenuId);
			toReturn.Add(this.NavigationMenuItemEntityUsingNavigationMenuId);
			toReturn.Add(this.NavigationMenuWidgetEntityUsingNavigationMenuId);
			toReturn.Add(this.ApplicationConfigurationEntityUsingApplicationConfigurationId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between NavigationMenuEntity and LandingPageEntity over the 1:n relation they have, using the relation between the fields:
		/// NavigationMenu.NavigationMenuId - LandingPage.NavigationMenuId
		/// </summary>
		public virtual IEntityRelation LandingPageEntityUsingNavigationMenuId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "LandingPageCollection" , true);
				relation.AddEntityFieldPair(NavigationMenuFields.NavigationMenuId, LandingPageFields.NavigationMenuId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NavigationMenuEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LandingPageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between NavigationMenuEntity and NavigationMenuItemEntity over the 1:n relation they have, using the relation between the fields:
		/// NavigationMenu.NavigationMenuId - NavigationMenuItem.NavigationMenuId
		/// </summary>
		public virtual IEntityRelation NavigationMenuItemEntityUsingNavigationMenuId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "NavigationMenuItemCollection" , true);
				relation.AddEntityFieldPair(NavigationMenuFields.NavigationMenuId, NavigationMenuItemFields.NavigationMenuId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NavigationMenuEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NavigationMenuItemEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between NavigationMenuEntity and NavigationMenuWidgetEntity over the 1:n relation they have, using the relation between the fields:
		/// NavigationMenu.NavigationMenuId - NavigationMenuWidget.NavigationMenuId
		/// </summary>
		public virtual IEntityRelation NavigationMenuWidgetEntityUsingNavigationMenuId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "NavigationMenuWidgetCollection" , true);
				relation.AddEntityFieldPair(NavigationMenuFields.NavigationMenuId, NavigationMenuWidgetFields.NavigationMenuId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NavigationMenuEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NavigationMenuWidgetEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between NavigationMenuEntity and ApplicationConfigurationEntity over the m:1 relation they have, using the relation between the fields:
		/// NavigationMenu.ApplicationConfigurationId - ApplicationConfiguration.ApplicationConfigurationId
		/// </summary>
		public virtual IEntityRelation ApplicationConfigurationEntityUsingApplicationConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ApplicationConfigurationEntity", false);
				relation.AddEntityFieldPair(ApplicationConfigurationFields.ApplicationConfigurationId, NavigationMenuFields.ApplicationConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ApplicationConfigurationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NavigationMenuEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticNavigationMenuRelations
	{
		internal static readonly IEntityRelation LandingPageEntityUsingNavigationMenuIdStatic = new NavigationMenuRelations().LandingPageEntityUsingNavigationMenuId;
		internal static readonly IEntityRelation NavigationMenuItemEntityUsingNavigationMenuIdStatic = new NavigationMenuRelations().NavigationMenuItemEntityUsingNavigationMenuId;
		internal static readonly IEntityRelation NavigationMenuWidgetEntityUsingNavigationMenuIdStatic = new NavigationMenuRelations().NavigationMenuWidgetEntityUsingNavigationMenuId;
		internal static readonly IEntityRelation ApplicationConfigurationEntityUsingApplicationConfigurationIdStatic = new NavigationMenuRelations().ApplicationConfigurationEntityUsingApplicationConfigurationId;

		/// <summary>CTor</summary>
		static StaticNavigationMenuRelations()
		{
		}
	}
}
