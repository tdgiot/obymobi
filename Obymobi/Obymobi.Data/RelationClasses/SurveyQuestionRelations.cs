﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: SurveyQuestion. </summary>
	public partial class SurveyQuestionRelations
	{
		/// <summary>CTor</summary>
		public SurveyQuestionRelations()
		{
		}

		/// <summary>Gets all relations of the SurveyQuestionEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.SurveyAnswerEntityUsingSurveyQuestionId);
			toReturn.Add(this.SurveyAnswerEntityUsingTargetSurveyQuestionId);
			toReturn.Add(this.SurveyQuestionEntityUsingParentQuestionId);
			toReturn.Add(this.SurveyQuestionLanguageEntityUsingSurveyQuestionId);
			toReturn.Add(this.SurveyResultEntityUsingSurveyQuestionId);
			toReturn.Add(this.SurveyPageEntityUsingSurveyPageId);
			toReturn.Add(this.SurveyQuestionEntityUsingSurveyQuestionIdParentQuestionId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between SurveyQuestionEntity and SurveyAnswerEntity over the 1:n relation they have, using the relation between the fields:
		/// SurveyQuestion.SurveyQuestionId - SurveyAnswer.SurveyQuestionId
		/// </summary>
		public virtual IEntityRelation SurveyAnswerEntityUsingSurveyQuestionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SurveyAnswerCollection" , true);
				relation.AddEntityFieldPair(SurveyQuestionFields.SurveyQuestionId, SurveyAnswerFields.SurveyQuestionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyQuestionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyAnswerEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SurveyQuestionEntity and SurveyAnswerEntity over the 1:n relation they have, using the relation between the fields:
		/// SurveyQuestion.SurveyQuestionId - SurveyAnswer.TargetSurveyQuestionId
		/// </summary>
		public virtual IEntityRelation SurveyAnswerEntityUsingTargetSurveyQuestionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SurveyAnswerTargetCollection" , true);
				relation.AddEntityFieldPair(SurveyQuestionFields.SurveyQuestionId, SurveyAnswerFields.TargetSurveyQuestionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyQuestionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyAnswerEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SurveyQuestionEntity and SurveyQuestionEntity over the 1:n relation they have, using the relation between the fields:
		/// SurveyQuestion.SurveyQuestionId - SurveyQuestion.ParentQuestionId
		/// </summary>
		public virtual IEntityRelation SurveyQuestionEntityUsingParentQuestionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SurveyQuestionCollection" , true);
				relation.AddEntityFieldPair(SurveyQuestionFields.SurveyQuestionId, SurveyQuestionFields.ParentQuestionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyQuestionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyQuestionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SurveyQuestionEntity and SurveyQuestionLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// SurveyQuestion.SurveyQuestionId - SurveyQuestionLanguage.SurveyQuestionId
		/// </summary>
		public virtual IEntityRelation SurveyQuestionLanguageEntityUsingSurveyQuestionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SurveyQuestionLanguageCollection" , true);
				relation.AddEntityFieldPair(SurveyQuestionFields.SurveyQuestionId, SurveyQuestionLanguageFields.SurveyQuestionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyQuestionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyQuestionLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SurveyQuestionEntity and SurveyResultEntity over the 1:n relation they have, using the relation between the fields:
		/// SurveyQuestion.SurveyQuestionId - SurveyResult.SurveyQuestionId
		/// </summary>
		public virtual IEntityRelation SurveyResultEntityUsingSurveyQuestionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SurveyResultCollection" , true);
				relation.AddEntityFieldPair(SurveyQuestionFields.SurveyQuestionId, SurveyResultFields.SurveyQuestionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyQuestionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyResultEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between SurveyQuestionEntity and SurveyPageEntity over the m:1 relation they have, using the relation between the fields:
		/// SurveyQuestion.SurveyPageId - SurveyPage.SurveyPageId
		/// </summary>
		public virtual IEntityRelation SurveyPageEntityUsingSurveyPageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SurveyPageEntity", false);
				relation.AddEntityFieldPair(SurveyPageFields.SurveyPageId, SurveyQuestionFields.SurveyPageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyPageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyQuestionEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between SurveyQuestionEntity and SurveyQuestionEntity over the m:1 relation they have, using the relation between the fields:
		/// SurveyQuestion.ParentQuestionId - SurveyQuestion.SurveyQuestionId
		/// </summary>
		public virtual IEntityRelation SurveyQuestionEntityUsingSurveyQuestionIdParentQuestionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ParentSurveyQuestionEntity", false);
				relation.AddEntityFieldPair(SurveyQuestionFields.SurveyQuestionId, SurveyQuestionFields.ParentQuestionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyQuestionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyQuestionEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticSurveyQuestionRelations
	{
		internal static readonly IEntityRelation SurveyAnswerEntityUsingSurveyQuestionIdStatic = new SurveyQuestionRelations().SurveyAnswerEntityUsingSurveyQuestionId;
		internal static readonly IEntityRelation SurveyAnswerEntityUsingTargetSurveyQuestionIdStatic = new SurveyQuestionRelations().SurveyAnswerEntityUsingTargetSurveyQuestionId;
		internal static readonly IEntityRelation SurveyQuestionEntityUsingParentQuestionIdStatic = new SurveyQuestionRelations().SurveyQuestionEntityUsingParentQuestionId;
		internal static readonly IEntityRelation SurveyQuestionLanguageEntityUsingSurveyQuestionIdStatic = new SurveyQuestionRelations().SurveyQuestionLanguageEntityUsingSurveyQuestionId;
		internal static readonly IEntityRelation SurveyResultEntityUsingSurveyQuestionIdStatic = new SurveyQuestionRelations().SurveyResultEntityUsingSurveyQuestionId;
		internal static readonly IEntityRelation SurveyPageEntityUsingSurveyPageIdStatic = new SurveyQuestionRelations().SurveyPageEntityUsingSurveyPageId;
		internal static readonly IEntityRelation SurveyQuestionEntityUsingSurveyQuestionIdParentQuestionIdStatic = new SurveyQuestionRelations().SurveyQuestionEntityUsingSurveyQuestionIdParentQuestionId;

		/// <summary>CTor</summary>
		static StaticSurveyQuestionRelations()
		{
		}
	}
}
