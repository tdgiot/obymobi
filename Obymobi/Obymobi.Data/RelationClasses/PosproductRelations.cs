﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Posproduct. </summary>
	public partial class PosproductRelations
	{
		/// <summary>CTor</summary>
		public PosproductRelations()
		{
		}

		/// <summary>Gets all relations of the PosproductEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AlterationoptionEntityUsingPosproductId);
			toReturn.Add(this.ProductEntityUsingPosproductId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between PosproductEntity and AlterationoptionEntity over the 1:n relation they have, using the relation between the fields:
		/// Posproduct.PosproductId - Alterationoption.PosproductId
		/// </summary>
		public virtual IEntityRelation AlterationoptionEntityUsingPosproductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AlterationoptionCollection" , true);
				relation.AddEntityFieldPair(PosproductFields.PosproductId, AlterationoptionFields.PosproductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PosproductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationoptionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PosproductEntity and ProductEntity over the 1:n relation they have, using the relation between the fields:
		/// Posproduct.PosproductId - Product.PosproductId
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingPosproductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ProductCollection" , true);
				relation.AddEntityFieldPair(PosproductFields.PosproductId, ProductFields.PosproductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PosproductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between PosproductEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// Posproduct.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, PosproductFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PosproductEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPosproductRelations
	{
		internal static readonly IEntityRelation AlterationoptionEntityUsingPosproductIdStatic = new PosproductRelations().AlterationoptionEntityUsingPosproductId;
		internal static readonly IEntityRelation ProductEntityUsingPosproductIdStatic = new PosproductRelations().ProductEntityUsingPosproductId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new PosproductRelations().CompanyEntityUsingCompanyId;

		/// <summary>CTor</summary>
		static StaticPosproductRelations()
		{
		}
	}
}
