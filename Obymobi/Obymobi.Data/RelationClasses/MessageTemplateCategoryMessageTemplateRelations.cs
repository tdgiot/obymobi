﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: MessageTemplateCategoryMessageTemplate. </summary>
	public partial class MessageTemplateCategoryMessageTemplateRelations
	{
		/// <summary>CTor</summary>
		public MessageTemplateCategoryMessageTemplateRelations()
		{
		}

		/// <summary>Gets all relations of the MessageTemplateCategoryMessageTemplateEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.MessageTemplateEntityUsingMessageTemplateId);
			toReturn.Add(this.MessageTemplateCategoryEntityUsingMessageTemplateCategoryId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between MessageTemplateCategoryMessageTemplateEntity and MessageTemplateEntity over the m:1 relation they have, using the relation between the fields:
		/// MessageTemplateCategoryMessageTemplate.MessageTemplateId - MessageTemplate.MessageTemplateId
		/// </summary>
		public virtual IEntityRelation MessageTemplateEntityUsingMessageTemplateId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "MessageTemplateEntity", false);
				relation.AddEntityFieldPair(MessageTemplateFields.MessageTemplateId, MessageTemplateCategoryMessageTemplateFields.MessageTemplateId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageTemplateEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageTemplateCategoryMessageTemplateEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MessageTemplateCategoryMessageTemplateEntity and MessageTemplateCategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// MessageTemplateCategoryMessageTemplate.MessageTemplateCategoryId - MessageTemplateCategory.MessageTemplateCategoryId
		/// </summary>
		public virtual IEntityRelation MessageTemplateCategoryEntityUsingMessageTemplateCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "MessageTemplateCategoryEntity", false);
				relation.AddEntityFieldPair(MessageTemplateCategoryFields.MessageTemplateCategoryId, MessageTemplateCategoryMessageTemplateFields.MessageTemplateCategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageTemplateCategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageTemplateCategoryMessageTemplateEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticMessageTemplateCategoryMessageTemplateRelations
	{
		internal static readonly IEntityRelation MessageTemplateEntityUsingMessageTemplateIdStatic = new MessageTemplateCategoryMessageTemplateRelations().MessageTemplateEntityUsingMessageTemplateId;
		internal static readonly IEntityRelation MessageTemplateCategoryEntityUsingMessageTemplateCategoryIdStatic = new MessageTemplateCategoryMessageTemplateRelations().MessageTemplateCategoryEntityUsingMessageTemplateCategoryId;

		/// <summary>CTor</summary>
		static StaticMessageTemplateCategoryMessageTemplateRelations()
		{
		}
	}
}
