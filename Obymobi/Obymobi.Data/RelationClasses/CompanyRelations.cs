﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Company. </summary>
	public partial class CompanyRelations
	{
		/// <summary>CTor</summary>
		public CompanyRelations()
		{
		}

		/// <summary>Gets all relations of the CompanyEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AccessCodeCompanyEntityUsingCompanyId);
			toReturn.Add(this.AccountCompanyEntityUsingCompanyId);
			toReturn.Add(this.AdvertisementEntityUsingCompanyId);
			toReturn.Add(this.AdvertisementConfigurationEntityUsingCompanyId);
			toReturn.Add(this.AlterationEntityUsingCompanyId);
			toReturn.Add(this.AlterationoptionEntityUsingCompanyId);
			toReturn.Add(this.AnnouncementEntityUsingCompanyId);
			toReturn.Add(this.ApiAuthenticationEntityUsingCompanyId);
			toReturn.Add(this.ApplicationConfigurationEntityUsingCompanyId);
			toReturn.Add(this.FeatureFlagEntityUsingCompanyId);
			toReturn.Add(this.AvailabilityEntityUsingCompanyId);
			toReturn.Add(this.BusinesshoursEntityUsingCompanyId);
			toReturn.Add(this.BusinesshoursexceptionEntityUsingCompanyId);
			toReturn.Add(this.CategoryEntityUsingCompanyId);
			toReturn.Add(this.CheckoutMethodEntityUsingCompanyId);
			toReturn.Add(this.CheckoutMethodDeliverypointgroupEntityUsingParentCompanyId);
			toReturn.Add(this.ClientEntityUsingCompanyId);
			toReturn.Add(this.ClientConfigurationEntityUsingCompanyId);
			toReturn.Add(this.CloudStorageAccountEntityUsingCompanyId);
			toReturn.Add(this.CompanyAmenityEntityUsingCompanyId);
			toReturn.Add(this.CompanyBrandEntityUsingCompanyId);
			toReturn.Add(this.CompanyCultureEntityUsingCompanyId);
			toReturn.Add(this.CompanyCurrencyEntityUsingCompanyId);
			toReturn.Add(this.CompanyEntertainmentEntityUsingCompanyId);
			toReturn.Add(this.CompanyLanguageEntityUsingCompanyId);
			toReturn.Add(this.CompanyManagementTaskEntityUsingCompanyId);
			toReturn.Add(this.CompanyReleaseEntityUsingCompanyId);
			toReturn.Add(this.CompanyVenueCategoryEntityUsingCompanyId);
			toReturn.Add(this.CustomTextEntityUsingCompanyId);
			toReturn.Add(this.DeliverypointEntityUsingCompanyId);
			toReturn.Add(this.DeliverypointgroupEntityUsingCompanyId);
			toReturn.Add(this.EntertainmentEntityUsingCompanyId);
			toReturn.Add(this.EntertainmentConfigurationEntityUsingCompanyId);
			toReturn.Add(this.ExternalMenuEntityUsingParentCompanyId);
			toReturn.Add(this.ExternalSystemEntityUsingCompanyId);
			toReturn.Add(this.GameSessionReportEntityUsingCompanyId);
			toReturn.Add(this.GameSessionReportConfigurationEntityUsingCompanyId);
			toReturn.Add(this.GuestInformationEntityUsingCompanyId);
			toReturn.Add(this.MapEntityUsingCompanyId);
			toReturn.Add(this.MediaEntityUsingCompanyId);
			toReturn.Add(this.MenuEntityUsingCompanyId);
			toReturn.Add(this.MessageEntityUsingCompanyId);
			toReturn.Add(this.MessagegroupEntityUsingCompanyId);
			toReturn.Add(this.MessageTemplateEntityUsingCompanyId);
			toReturn.Add(this.MessageTemplateCategoryEntityUsingCompanyId);
			toReturn.Add(this.NetmessageEntityUsingReceiverCompanyId);
			toReturn.Add(this.NetmessageEntityUsingSenderCompanyId);
			toReturn.Add(this.OptInEntityUsingParentCompanyId);
			toReturn.Add(this.OrderEntityUsingCompanyId);
			toReturn.Add(this.OutletEntityUsingCompanyId);
			toReturn.Add(this.PaymentIntegrationConfigurationEntityUsingCompanyId);
			toReturn.Add(this.PaymentProviderEntityUsingCompanyId);
			toReturn.Add(this.PaymentProviderCompanyEntityUsingCompanyId);
			toReturn.Add(this.PmsReportConfigurationEntityUsingCompanyId);
			toReturn.Add(this.PosalterationEntityUsingCompanyId);
			toReturn.Add(this.PosalterationitemEntityUsingCompanyId);
			toReturn.Add(this.PosalterationoptionEntityUsingCompanyId);
			toReturn.Add(this.PoscategoryEntityUsingCompanyId);
			toReturn.Add(this.PosdeliverypointEntityUsingCompanyId);
			toReturn.Add(this.PosdeliverypointgroupEntityUsingCompanyId);
			toReturn.Add(this.PospaymentmethodEntityUsingCompanyId);
			toReturn.Add(this.PosproductEntityUsingCompanyId);
			toReturn.Add(this.PosproductPosalterationEntityUsingCompanyId);
			toReturn.Add(this.PriceLevelEntityUsingCompanyId);
			toReturn.Add(this.PriceScheduleEntityUsingCompanyId);
			toReturn.Add(this.ProductEntityUsingCompanyId);
			toReturn.Add(this.ProductgroupEntityUsingCompanyId);
			toReturn.Add(this.ReceiptEntityUsingCompanyId);
			toReturn.Add(this.ReceiptTemplateEntityUsingCompanyId);
			toReturn.Add(this.ReportProcessingTaskEntityUsingCompanyId);
			toReturn.Add(this.ReportProcessingTaskTemplateEntityUsingCompanyId);
			toReturn.Add(this.RoomControlConfigurationEntityUsingCompanyId);
			toReturn.Add(this.RouteEntityUsingCompanyId);
			toReturn.Add(this.ScheduleEntityUsingCompanyId);
			toReturn.Add(this.ScheduledCommandTaskEntityUsingCompanyId);
			toReturn.Add(this.ScheduledMessageEntityUsingCompanyId);
			toReturn.Add(this.ServiceMethodEntityUsingCompanyId);
			toReturn.Add(this.ServiceMethodDeliverypointgroupEntityUsingParentCompanyId);
			toReturn.Add(this.SetupCodeEntityUsingCompanyId);
			toReturn.Add(this.SiteEntityUsingCompanyId);
			toReturn.Add(this.SmsKeywordEntityUsingCompanyId);
			toReturn.Add(this.StationListEntityUsingCompanyId);
			toReturn.Add(this.SurveyEntityUsingCompanyId);
			toReturn.Add(this.TagEntityUsingCompanyId);
			toReturn.Add(this.TaxTariffEntityUsingCompanyId);
			toReturn.Add(this.TerminalEntityUsingCompanyId);
			toReturn.Add(this.TerminalConfigurationEntityUsingCompanyId);
			toReturn.Add(this.UIModeEntityUsingCompanyId);
			toReturn.Add(this.UIScheduleEntityUsingCompanyId);
			toReturn.Add(this.UIThemeEntityUsingCompanyId);
			toReturn.Add(this.WifiConfigurationEntityUsingCompanyId);
			toReturn.Add(this.TimestampEntityUsingCompanyId);
			toReturn.Add(this.ActionButtonEntityUsingActionButtonId);
			toReturn.Add(this.CompanygroupEntityUsingCompanygroupId);
			toReturn.Add(this.CompanyOwnerEntityUsingCompanyOwnerId);
			toReturn.Add(this.CountryEntityUsingCountryId);
			toReturn.Add(this.CurrencyEntityUsingCurrencyId);
			toReturn.Add(this.LanguageEntityUsingLanguageId);
			toReturn.Add(this.RouteEntityUsingRouteId);
			toReturn.Add(this.SupportpoolEntityUsingSupportpoolId);
			toReturn.Add(this.TimeZoneEntityUsingTimeZoneId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and AccessCodeCompanyEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - AccessCodeCompany.CompanyId
		/// </summary>
		public virtual IEntityRelation AccessCodeCompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AccessCodeCompanyCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, AccessCodeCompanyFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AccessCodeCompanyEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and AccountCompanyEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - AccountCompany.CompanyId
		/// </summary>
		public virtual IEntityRelation AccountCompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AccountCompanyCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, AccountCompanyFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AccountCompanyEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and AdvertisementEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - Advertisement.CompanyId
		/// </summary>
		public virtual IEntityRelation AdvertisementEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AdvertisementCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, AdvertisementFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and AdvertisementConfigurationEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - AdvertisementConfiguration.CompanyId
		/// </summary>
		public virtual IEntityRelation AdvertisementConfigurationEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AdvertisementConfigurationCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, AdvertisementConfigurationFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementConfigurationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and AlterationEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - Alteration.CompanyId
		/// </summary>
		public virtual IEntityRelation AlterationEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AlterationCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, AlterationFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and AlterationoptionEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - Alterationoption.CompanyId
		/// </summary>
		public virtual IEntityRelation AlterationoptionEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AlterationoptionCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, AlterationoptionFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationoptionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and AnnouncementEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - Announcement.CompanyId
		/// </summary>
		public virtual IEntityRelation AnnouncementEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AnnouncementCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, AnnouncementFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AnnouncementEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and ApiAuthenticationEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - ApiAuthentication.CompanyId
		/// </summary>
		public virtual IEntityRelation ApiAuthenticationEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ApiAuthenticationCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, ApiAuthenticationFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ApiAuthenticationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and ApplicationConfigurationEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - ApplicationConfiguration.CompanyId
		/// </summary>
		public virtual IEntityRelation ApplicationConfigurationEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ApplicationConfigurationCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, ApplicationConfigurationFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ApplicationConfigurationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and FeatureFlagEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - FeatureFlag.CompanyId
		/// </summary>
		public virtual IEntityRelation FeatureFlagEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FeatureFlagCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, FeatureFlagFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FeatureFlagEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and AvailabilityEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - Availability.CompanyId
		/// </summary>
		public virtual IEntityRelation AvailabilityEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AvailabilityCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, AvailabilityFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AvailabilityEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and BusinesshoursEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - Businesshours.CompanyId
		/// </summary>
		public virtual IEntityRelation BusinesshoursEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "BusinesshoursCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, BusinesshoursFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinesshoursEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and BusinesshoursexceptionEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - Businesshoursexception.CompanyId
		/// </summary>
		public virtual IEntityRelation BusinesshoursexceptionEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "BusinesshoursexceptionCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, BusinesshoursexceptionFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinesshoursexceptionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and CategoryEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - Category.CompanyId
		/// </summary>
		public virtual IEntityRelation CategoryEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CategoryCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, CategoryFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and CheckoutMethodEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - CheckoutMethod.CompanyId
		/// </summary>
		public virtual IEntityRelation CheckoutMethodEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CheckoutMethodCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, CheckoutMethodFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CheckoutMethodEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and CheckoutMethodDeliverypointgroupEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - CheckoutMethodDeliverypointgroup.ParentCompanyId
		/// </summary>
		public virtual IEntityRelation CheckoutMethodDeliverypointgroupEntityUsingParentCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CheckoutMethodDeliverypointgroupCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, CheckoutMethodDeliverypointgroupFields.ParentCompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CheckoutMethodDeliverypointgroupEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and ClientEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - Client.CompanyId
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ClientCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, ClientFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and ClientConfigurationEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - ClientConfiguration.CompanyId
		/// </summary>
		public virtual IEntityRelation ClientConfigurationEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ClientConfigurationCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, ClientConfigurationFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientConfigurationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and CloudStorageAccountEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - CloudStorageAccount.CompanyId
		/// </summary>
		public virtual IEntityRelation CloudStorageAccountEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CloudStorageAccountCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, CloudStorageAccountFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CloudStorageAccountEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and CompanyAmenityEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - CompanyAmenity.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyAmenityEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CompanyAmenityCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, CompanyAmenityFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyAmenityEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and CompanyBrandEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - CompanyBrand.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyBrandEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CompanyBrandCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, CompanyBrandFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyBrandEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and CompanyCultureEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - CompanyCulture.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyCultureEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CompanyCultureCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, CompanyCultureFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyCultureEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and CompanyCurrencyEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - CompanyCurrency.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyCurrencyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CompanyCurrencyCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, CompanyCurrencyFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyCurrencyEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and CompanyEntertainmentEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - CompanyEntertainment.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntertainmentEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CompanyEntertainmentCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, CompanyEntertainmentFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntertainmentEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and CompanyLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - CompanyLanguage.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyLanguageEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CompanyLanguageCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, CompanyLanguageFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and CompanyManagementTaskEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - CompanyManagementTask.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyManagementTaskEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CompanyManagementTaskCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, CompanyManagementTaskFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyManagementTaskEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and CompanyReleaseEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - CompanyRelease.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyReleaseEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CompanyReleaseCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, CompanyReleaseFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyReleaseEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and CompanyVenueCategoryEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - CompanyVenueCategory.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyVenueCategoryEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CompanyVenueCategoryCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, CompanyVenueCategoryFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyVenueCategoryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and CustomTextEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - CustomText.CompanyId
		/// </summary>
		public virtual IEntityRelation CustomTextEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomTextCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, CustomTextFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and DeliverypointEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - Deliverypoint.CompanyId
		/// </summary>
		public virtual IEntityRelation DeliverypointEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeliverypointCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, DeliverypointFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and DeliverypointgroupEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - Deliverypointgroup.CompanyId
		/// </summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeliverypointgroupCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, DeliverypointgroupFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and EntertainmentEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - Entertainment.CompanyId
		/// </summary>
		public virtual IEntityRelation EntertainmentEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "EntertainmentCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, EntertainmentFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and EntertainmentConfigurationEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - EntertainmentConfiguration.CompanyId
		/// </summary>
		public virtual IEntityRelation EntertainmentConfigurationEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "EntertainmentConfigurationCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, EntertainmentConfigurationFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentConfigurationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and ExternalMenuEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - ExternalMenu.ParentCompanyId
		/// </summary>
		public virtual IEntityRelation ExternalMenuEntityUsingParentCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ExternalMenuCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, ExternalMenuFields.ParentCompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalMenuEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and ExternalSystemEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - ExternalSystem.CompanyId
		/// </summary>
		public virtual IEntityRelation ExternalSystemEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ExternalSystemCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, ExternalSystemFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalSystemEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and GameSessionReportEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - GameSessionReport.CompanyId
		/// </summary>
		public virtual IEntityRelation GameSessionReportEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "GameSessionReportCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, GameSessionReportFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GameSessionReportEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and GameSessionReportConfigurationEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - GameSessionReportConfiguration.CompanyId
		/// </summary>
		public virtual IEntityRelation GameSessionReportConfigurationEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "GameSessionReportConfigurationCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, GameSessionReportConfigurationFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GameSessionReportConfigurationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and GuestInformationEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - GuestInformation.CompanyId
		/// </summary>
		public virtual IEntityRelation GuestInformationEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "GuestInformationCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, GuestInformationFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GuestInformationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and MapEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - Map.CompanyId
		/// </summary>
		public virtual IEntityRelation MapEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MapCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, MapFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MapEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and MediaEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - Media.CompanyId
		/// </summary>
		public virtual IEntityRelation MediaEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MediaCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, MediaFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and MenuEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - Menu.CompanyId
		/// </summary>
		public virtual IEntityRelation MenuEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MenuCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, MenuFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MenuEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and MessageEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - Message.CompanyId
		/// </summary>
		public virtual IEntityRelation MessageEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MessageCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, MessageFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and MessagegroupEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - Messagegroup.CompanyId
		/// </summary>
		public virtual IEntityRelation MessagegroupEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MessagegroupCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, MessagegroupFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessagegroupEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and MessageTemplateEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - MessageTemplate.CompanyId
		/// </summary>
		public virtual IEntityRelation MessageTemplateEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MessageTemplateCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, MessageTemplateFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageTemplateEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and MessageTemplateCategoryEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - MessageTemplateCategory.CompanyId
		/// </summary>
		public virtual IEntityRelation MessageTemplateCategoryEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MessageTemplateCategoryCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, MessageTemplateCategoryFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageTemplateCategoryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and NetmessageEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - Netmessage.ReceiverCompanyId
		/// </summary>
		public virtual IEntityRelation NetmessageEntityUsingReceiverCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ReceivedNetmessageCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, NetmessageFields.ReceiverCompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NetmessageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and NetmessageEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - Netmessage.SenderCompanyId
		/// </summary>
		public virtual IEntityRelation NetmessageEntityUsingSenderCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SentNetmessageCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, NetmessageFields.SenderCompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NetmessageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and OptInEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - OptIn.ParentCompanyId
		/// </summary>
		public virtual IEntityRelation OptInEntityUsingParentCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OptInCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, OptInFields.ParentCompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OptInEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and OrderEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - Order.CompanyId
		/// </summary>
		public virtual IEntityRelation OrderEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrderCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, OrderFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and OutletEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - Outlet.CompanyId
		/// </summary>
		public virtual IEntityRelation OutletEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OutletCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, OutletFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OutletEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and PaymentIntegrationConfigurationEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - PaymentIntegrationConfiguration.CompanyId
		/// </summary>
		public virtual IEntityRelation PaymentIntegrationConfigurationEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PaymentIntegrationConfigurationCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, PaymentIntegrationConfigurationFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentIntegrationConfigurationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and PaymentProviderEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - PaymentProvider.CompanyId
		/// </summary>
		public virtual IEntityRelation PaymentProviderEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PaymentProviderCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, PaymentProviderFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentProviderEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and PaymentProviderCompanyEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - PaymentProviderCompany.CompanyId
		/// </summary>
		public virtual IEntityRelation PaymentProviderCompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PaymentProviderCompanyCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, PaymentProviderCompanyFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentProviderCompanyEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and PmsReportConfigurationEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - PmsReportConfiguration.CompanyId
		/// </summary>
		public virtual IEntityRelation PmsReportConfigurationEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PmsReportConfigurationCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, PmsReportConfigurationFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PmsReportConfigurationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and PosalterationEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - Posalteration.CompanyId
		/// </summary>
		public virtual IEntityRelation PosalterationEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PosalterationCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, PosalterationFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PosalterationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and PosalterationitemEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - Posalterationitem.CompanyId
		/// </summary>
		public virtual IEntityRelation PosalterationitemEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PosalterationitemCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, PosalterationitemFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PosalterationitemEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and PosalterationoptionEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - Posalterationoption.CompanyId
		/// </summary>
		public virtual IEntityRelation PosalterationoptionEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PosalterationoptionCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, PosalterationoptionFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PosalterationoptionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and PoscategoryEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - Poscategory.CompanyId
		/// </summary>
		public virtual IEntityRelation PoscategoryEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PoscategoryCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, PoscategoryFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PoscategoryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and PosdeliverypointEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - Posdeliverypoint.CompanyId
		/// </summary>
		public virtual IEntityRelation PosdeliverypointEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PosdeliverypointCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, PosdeliverypointFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PosdeliverypointEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and PosdeliverypointgroupEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - Posdeliverypointgroup.CompanyId
		/// </summary>
		public virtual IEntityRelation PosdeliverypointgroupEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PosdeliverypointgroupCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, PosdeliverypointgroupFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PosdeliverypointgroupEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and PospaymentmethodEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - Pospaymentmethod.CompanyId
		/// </summary>
		public virtual IEntityRelation PospaymentmethodEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PospaymentmethodCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, PospaymentmethodFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PospaymentmethodEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and PosproductEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - Posproduct.CompanyId
		/// </summary>
		public virtual IEntityRelation PosproductEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PosproductCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, PosproductFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PosproductEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and PosproductPosalterationEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - PosproductPosalteration.CompanyId
		/// </summary>
		public virtual IEntityRelation PosproductPosalterationEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PosproductPosalterationCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, PosproductPosalterationFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PosproductPosalterationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and PriceLevelEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - PriceLevel.CompanyId
		/// </summary>
		public virtual IEntityRelation PriceLevelEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PriceLevelCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, PriceLevelFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PriceLevelEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and PriceScheduleEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - PriceSchedule.CompanyId
		/// </summary>
		public virtual IEntityRelation PriceScheduleEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PriceScheduleCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, PriceScheduleFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PriceScheduleEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and ProductEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - Product.CompanyId
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ProductCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, ProductFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and ProductgroupEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - Productgroup.CompanyId
		/// </summary>
		public virtual IEntityRelation ProductgroupEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ProductgroupCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, ProductgroupFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductgroupEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and ReceiptEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - Receipt.CompanyId
		/// </summary>
		public virtual IEntityRelation ReceiptEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ReceiptCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, ReceiptFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReceiptEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and ReceiptTemplateEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - ReceiptTemplate.CompanyId
		/// </summary>
		public virtual IEntityRelation ReceiptTemplateEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ReceiptTemplateCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, ReceiptTemplateFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReceiptTemplateEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and ReportProcessingTaskEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - ReportProcessingTask.CompanyId
		/// </summary>
		public virtual IEntityRelation ReportProcessingTaskEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ReportProcessingTaskCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, ReportProcessingTaskFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportProcessingTaskEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and ReportProcessingTaskTemplateEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - ReportProcessingTaskTemplate.CompanyId
		/// </summary>
		public virtual IEntityRelation ReportProcessingTaskTemplateEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ReportProcessingTaskTemplateCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, ReportProcessingTaskTemplateFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportProcessingTaskTemplateEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and RoomControlConfigurationEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - RoomControlConfiguration.CompanyId
		/// </summary>
		public virtual IEntityRelation RoomControlConfigurationEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RoomControlConfigurationCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, RoomControlConfigurationFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlConfigurationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and RouteEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - Route.CompanyId
		/// </summary>
		public virtual IEntityRelation RouteEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RouteCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, RouteFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RouteEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and ScheduleEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - Schedule.CompanyId
		/// </summary>
		public virtual IEntityRelation ScheduleEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ScheduleCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, ScheduleFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduleEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and ScheduledCommandTaskEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - ScheduledCommandTask.CompanyId
		/// </summary>
		public virtual IEntityRelation ScheduledCommandTaskEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ScheduledCommandTaskCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, ScheduledCommandTaskFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduledCommandTaskEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and ScheduledMessageEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - ScheduledMessage.CompanyId
		/// </summary>
		public virtual IEntityRelation ScheduledMessageEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ScheduledMessageCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, ScheduledMessageFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduledMessageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and ServiceMethodEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - ServiceMethod.CompanyId
		/// </summary>
		public virtual IEntityRelation ServiceMethodEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ServiceMethodCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, ServiceMethodFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ServiceMethodEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and ServiceMethodDeliverypointgroupEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - ServiceMethodDeliverypointgroup.ParentCompanyId
		/// </summary>
		public virtual IEntityRelation ServiceMethodDeliverypointgroupEntityUsingParentCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ServiceMethodDeliverypointgroupCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, ServiceMethodDeliverypointgroupFields.ParentCompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ServiceMethodDeliverypointgroupEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and SetupCodeEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - SetupCode.CompanyId
		/// </summary>
		public virtual IEntityRelation SetupCodeEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SetupCodeCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, SetupCodeFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SetupCodeEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and SiteEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - Site.CompanyId
		/// </summary>
		public virtual IEntityRelation SiteEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SiteCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, SiteFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SiteEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and SmsKeywordEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - SmsKeyword.CompanyId
		/// </summary>
		public virtual IEntityRelation SmsKeywordEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SmsKeywordCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, SmsKeywordFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SmsKeywordEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and StationListEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - StationList.CompanyId
		/// </summary>
		public virtual IEntityRelation StationListEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "StationListCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, StationListFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("StationListEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and SurveyEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - Survey.CompanyId
		/// </summary>
		public virtual IEntityRelation SurveyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SurveyCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, SurveyFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and TagEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - Tag.CompanyId
		/// </summary>
		public virtual IEntityRelation TagEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TagCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, TagFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TagEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and TaxTariffEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - TaxTariff.CompanyId
		/// </summary>
		public virtual IEntityRelation TaxTariffEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TaxTariffCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, TaxTariffFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TaxTariffEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and TerminalEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - Terminal.CompanyId
		/// </summary>
		public virtual IEntityRelation TerminalEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TerminalCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, TerminalFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and TerminalConfigurationEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - TerminalConfiguration.CompanyId
		/// </summary>
		public virtual IEntityRelation TerminalConfigurationEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TerminalConfigurationCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, TerminalConfigurationFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalConfigurationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and UIModeEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - UIMode.CompanyId
		/// </summary>
		public virtual IEntityRelation UIModeEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UIModeCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, UIModeFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIModeEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and UIScheduleEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - UISchedule.CompanyId
		/// </summary>
		public virtual IEntityRelation UIScheduleEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UIScheduleCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, UIScheduleFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIScheduleEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and UIThemeEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - UITheme.CompanyId
		/// </summary>
		public virtual IEntityRelation UIThemeEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UIThemeCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, UIThemeFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIThemeEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and WifiConfigurationEntity over the 1:n relation they have, using the relation between the fields:
		/// Company.CompanyId - WifiConfiguration.CompanyId
		/// </summary>
		public virtual IEntityRelation WifiConfigurationEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "WifiConfigurationCollection" , true);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, WifiConfigurationFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WifiConfigurationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and TimestampEntity over the 1:1 relation they have, using the relation between the fields:
		/// Company.CompanyId - Timestamp.CompanyId
		/// </summary>
		public virtual IEntityRelation TimestampEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "TimestampCollection", true);


				relation.AddEntityFieldPair(CompanyFields.CompanyId, TimestampFields.CompanyId);


				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TimestampEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and ActionButtonEntity over the m:1 relation they have, using the relation between the fields:
		/// Company.ActionButtonId - ActionButton.ActionButtonId
		/// </summary>
		public virtual IEntityRelation ActionButtonEntityUsingActionButtonId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ActionButtonEntity", false);
				relation.AddEntityFieldPair(ActionButtonFields.ActionButtonId, CompanyFields.ActionButtonId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ActionButtonEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and CompanygroupEntity over the m:1 relation they have, using the relation between the fields:
		/// Company.CompanygroupId - Companygroup.CompanygroupId
		/// </summary>
		public virtual IEntityRelation CompanygroupEntityUsingCompanygroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanygroupEntity", false);
				relation.AddEntityFieldPair(CompanygroupFields.CompanygroupId, CompanyFields.CompanygroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanygroupEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and CompanyOwnerEntity over the m:1 relation they have, using the relation between the fields:
		/// Company.CompanyOwnerId - CompanyOwner.CompanyOwnerId
		/// </summary>
		public virtual IEntityRelation CompanyOwnerEntityUsingCompanyOwnerId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyOwnerEntity", false);
				relation.AddEntityFieldPair(CompanyOwnerFields.CompanyOwnerId, CompanyFields.CompanyOwnerId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyOwnerEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and CountryEntity over the m:1 relation they have, using the relation between the fields:
		/// Company.CountryId - Country.CountryId
		/// </summary>
		public virtual IEntityRelation CountryEntityUsingCountryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CountryEntity", false);
				relation.AddEntityFieldPair(CountryFields.CountryId, CompanyFields.CountryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CountryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and CurrencyEntity over the m:1 relation they have, using the relation between the fields:
		/// Company.CurrencyId - Currency.CurrencyId
		/// </summary>
		public virtual IEntityRelation CurrencyEntityUsingCurrencyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CurrencyEntity", false);
				relation.AddEntityFieldPair(CurrencyFields.CurrencyId, CompanyFields.CurrencyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CurrencyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and LanguageEntity over the m:1 relation they have, using the relation between the fields:
		/// Company.LanguageId - Language.LanguageId
		/// </summary>
		public virtual IEntityRelation LanguageEntityUsingLanguageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "LanguageEntity", false);
				relation.AddEntityFieldPair(LanguageFields.LanguageId, CompanyFields.LanguageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and RouteEntity over the m:1 relation they have, using the relation between the fields:
		/// Company.RouteId - Route.RouteId
		/// </summary>
		public virtual IEntityRelation RouteEntityUsingRouteId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RouteEntity", false);
				relation.AddEntityFieldPair(RouteFields.RouteId, CompanyFields.RouteId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RouteEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and SupportpoolEntity over the m:1 relation they have, using the relation between the fields:
		/// Company.SupportpoolId - Supportpool.SupportpoolId
		/// </summary>
		public virtual IEntityRelation SupportpoolEntityUsingSupportpoolId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SupportpoolEntity", false);
				relation.AddEntityFieldPair(SupportpoolFields.SupportpoolId, CompanyFields.SupportpoolId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SupportpoolEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CompanyEntity and TimeZoneEntity over the m:1 relation they have, using the relation between the fields:
		/// Company.TimeZoneId - TimeZone.TimeZoneId
		/// </summary>
		public virtual IEntityRelation TimeZoneEntityUsingTimeZoneId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TimeZoneEntity", false);
				relation.AddEntityFieldPair(TimeZoneFields.TimeZoneId, CompanyFields.TimeZoneId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TimeZoneEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCompanyRelations
	{
		internal static readonly IEntityRelation AccessCodeCompanyEntityUsingCompanyIdStatic = new CompanyRelations().AccessCodeCompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation AccountCompanyEntityUsingCompanyIdStatic = new CompanyRelations().AccountCompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation AdvertisementEntityUsingCompanyIdStatic = new CompanyRelations().AdvertisementEntityUsingCompanyId;
		internal static readonly IEntityRelation AdvertisementConfigurationEntityUsingCompanyIdStatic = new CompanyRelations().AdvertisementConfigurationEntityUsingCompanyId;
		internal static readonly IEntityRelation AlterationEntityUsingCompanyIdStatic = new CompanyRelations().AlterationEntityUsingCompanyId;
		internal static readonly IEntityRelation AlterationoptionEntityUsingCompanyIdStatic = new CompanyRelations().AlterationoptionEntityUsingCompanyId;
		internal static readonly IEntityRelation AnnouncementEntityUsingCompanyIdStatic = new CompanyRelations().AnnouncementEntityUsingCompanyId;
		internal static readonly IEntityRelation ApiAuthenticationEntityUsingCompanyIdStatic = new CompanyRelations().ApiAuthenticationEntityUsingCompanyId;
		internal static readonly IEntityRelation ApplicationConfigurationEntityUsingCompanyIdStatic = new CompanyRelations().ApplicationConfigurationEntityUsingCompanyId;
		internal static readonly IEntityRelation FeatureFlagEntityUsingCompanyIdStatic = new CompanyRelations().FeatureFlagEntityUsingCompanyId;
		internal static readonly IEntityRelation AvailabilityEntityUsingCompanyIdStatic = new CompanyRelations().AvailabilityEntityUsingCompanyId;
		internal static readonly IEntityRelation BusinesshoursEntityUsingCompanyIdStatic = new CompanyRelations().BusinesshoursEntityUsingCompanyId;
		internal static readonly IEntityRelation BusinesshoursexceptionEntityUsingCompanyIdStatic = new CompanyRelations().BusinesshoursexceptionEntityUsingCompanyId;
		internal static readonly IEntityRelation CategoryEntityUsingCompanyIdStatic = new CompanyRelations().CategoryEntityUsingCompanyId;
		internal static readonly IEntityRelation CheckoutMethodEntityUsingCompanyIdStatic = new CompanyRelations().CheckoutMethodEntityUsingCompanyId;
		internal static readonly IEntityRelation CheckoutMethodDeliverypointgroupEntityUsingParentCompanyIdStatic = new CompanyRelations().CheckoutMethodDeliverypointgroupEntityUsingParentCompanyId;
		internal static readonly IEntityRelation ClientEntityUsingCompanyIdStatic = new CompanyRelations().ClientEntityUsingCompanyId;
		internal static readonly IEntityRelation ClientConfigurationEntityUsingCompanyIdStatic = new CompanyRelations().ClientConfigurationEntityUsingCompanyId;
		internal static readonly IEntityRelation CloudStorageAccountEntityUsingCompanyIdStatic = new CompanyRelations().CloudStorageAccountEntityUsingCompanyId;
		internal static readonly IEntityRelation CompanyAmenityEntityUsingCompanyIdStatic = new CompanyRelations().CompanyAmenityEntityUsingCompanyId;
		internal static readonly IEntityRelation CompanyBrandEntityUsingCompanyIdStatic = new CompanyRelations().CompanyBrandEntityUsingCompanyId;
		internal static readonly IEntityRelation CompanyCultureEntityUsingCompanyIdStatic = new CompanyRelations().CompanyCultureEntityUsingCompanyId;
		internal static readonly IEntityRelation CompanyCurrencyEntityUsingCompanyIdStatic = new CompanyRelations().CompanyCurrencyEntityUsingCompanyId;
		internal static readonly IEntityRelation CompanyEntertainmentEntityUsingCompanyIdStatic = new CompanyRelations().CompanyEntertainmentEntityUsingCompanyId;
		internal static readonly IEntityRelation CompanyLanguageEntityUsingCompanyIdStatic = new CompanyRelations().CompanyLanguageEntityUsingCompanyId;
		internal static readonly IEntityRelation CompanyManagementTaskEntityUsingCompanyIdStatic = new CompanyRelations().CompanyManagementTaskEntityUsingCompanyId;
		internal static readonly IEntityRelation CompanyReleaseEntityUsingCompanyIdStatic = new CompanyRelations().CompanyReleaseEntityUsingCompanyId;
		internal static readonly IEntityRelation CompanyVenueCategoryEntityUsingCompanyIdStatic = new CompanyRelations().CompanyVenueCategoryEntityUsingCompanyId;
		internal static readonly IEntityRelation CustomTextEntityUsingCompanyIdStatic = new CompanyRelations().CustomTextEntityUsingCompanyId;
		internal static readonly IEntityRelation DeliverypointEntityUsingCompanyIdStatic = new CompanyRelations().DeliverypointEntityUsingCompanyId;
		internal static readonly IEntityRelation DeliverypointgroupEntityUsingCompanyIdStatic = new CompanyRelations().DeliverypointgroupEntityUsingCompanyId;
		internal static readonly IEntityRelation EntertainmentEntityUsingCompanyIdStatic = new CompanyRelations().EntertainmentEntityUsingCompanyId;
		internal static readonly IEntityRelation EntertainmentConfigurationEntityUsingCompanyIdStatic = new CompanyRelations().EntertainmentConfigurationEntityUsingCompanyId;
		internal static readonly IEntityRelation ExternalMenuEntityUsingParentCompanyIdStatic = new CompanyRelations().ExternalMenuEntityUsingParentCompanyId;
		internal static readonly IEntityRelation ExternalSystemEntityUsingCompanyIdStatic = new CompanyRelations().ExternalSystemEntityUsingCompanyId;
		internal static readonly IEntityRelation GameSessionReportEntityUsingCompanyIdStatic = new CompanyRelations().GameSessionReportEntityUsingCompanyId;
		internal static readonly IEntityRelation GameSessionReportConfigurationEntityUsingCompanyIdStatic = new CompanyRelations().GameSessionReportConfigurationEntityUsingCompanyId;
		internal static readonly IEntityRelation GuestInformationEntityUsingCompanyIdStatic = new CompanyRelations().GuestInformationEntityUsingCompanyId;
		internal static readonly IEntityRelation MapEntityUsingCompanyIdStatic = new CompanyRelations().MapEntityUsingCompanyId;
		internal static readonly IEntityRelation MediaEntityUsingCompanyIdStatic = new CompanyRelations().MediaEntityUsingCompanyId;
		internal static readonly IEntityRelation MenuEntityUsingCompanyIdStatic = new CompanyRelations().MenuEntityUsingCompanyId;
		internal static readonly IEntityRelation MessageEntityUsingCompanyIdStatic = new CompanyRelations().MessageEntityUsingCompanyId;
		internal static readonly IEntityRelation MessagegroupEntityUsingCompanyIdStatic = new CompanyRelations().MessagegroupEntityUsingCompanyId;
		internal static readonly IEntityRelation MessageTemplateEntityUsingCompanyIdStatic = new CompanyRelations().MessageTemplateEntityUsingCompanyId;
		internal static readonly IEntityRelation MessageTemplateCategoryEntityUsingCompanyIdStatic = new CompanyRelations().MessageTemplateCategoryEntityUsingCompanyId;
		internal static readonly IEntityRelation NetmessageEntityUsingReceiverCompanyIdStatic = new CompanyRelations().NetmessageEntityUsingReceiverCompanyId;
		internal static readonly IEntityRelation NetmessageEntityUsingSenderCompanyIdStatic = new CompanyRelations().NetmessageEntityUsingSenderCompanyId;
		internal static readonly IEntityRelation OptInEntityUsingParentCompanyIdStatic = new CompanyRelations().OptInEntityUsingParentCompanyId;
		internal static readonly IEntityRelation OrderEntityUsingCompanyIdStatic = new CompanyRelations().OrderEntityUsingCompanyId;
		internal static readonly IEntityRelation OutletEntityUsingCompanyIdStatic = new CompanyRelations().OutletEntityUsingCompanyId;
		internal static readonly IEntityRelation PaymentIntegrationConfigurationEntityUsingCompanyIdStatic = new CompanyRelations().PaymentIntegrationConfigurationEntityUsingCompanyId;
		internal static readonly IEntityRelation PaymentProviderEntityUsingCompanyIdStatic = new CompanyRelations().PaymentProviderEntityUsingCompanyId;
		internal static readonly IEntityRelation PaymentProviderCompanyEntityUsingCompanyIdStatic = new CompanyRelations().PaymentProviderCompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation PmsReportConfigurationEntityUsingCompanyIdStatic = new CompanyRelations().PmsReportConfigurationEntityUsingCompanyId;
		internal static readonly IEntityRelation PosalterationEntityUsingCompanyIdStatic = new CompanyRelations().PosalterationEntityUsingCompanyId;
		internal static readonly IEntityRelation PosalterationitemEntityUsingCompanyIdStatic = new CompanyRelations().PosalterationitemEntityUsingCompanyId;
		internal static readonly IEntityRelation PosalterationoptionEntityUsingCompanyIdStatic = new CompanyRelations().PosalterationoptionEntityUsingCompanyId;
		internal static readonly IEntityRelation PoscategoryEntityUsingCompanyIdStatic = new CompanyRelations().PoscategoryEntityUsingCompanyId;
		internal static readonly IEntityRelation PosdeliverypointEntityUsingCompanyIdStatic = new CompanyRelations().PosdeliverypointEntityUsingCompanyId;
		internal static readonly IEntityRelation PosdeliverypointgroupEntityUsingCompanyIdStatic = new CompanyRelations().PosdeliverypointgroupEntityUsingCompanyId;
		internal static readonly IEntityRelation PospaymentmethodEntityUsingCompanyIdStatic = new CompanyRelations().PospaymentmethodEntityUsingCompanyId;
		internal static readonly IEntityRelation PosproductEntityUsingCompanyIdStatic = new CompanyRelations().PosproductEntityUsingCompanyId;
		internal static readonly IEntityRelation PosproductPosalterationEntityUsingCompanyIdStatic = new CompanyRelations().PosproductPosalterationEntityUsingCompanyId;
		internal static readonly IEntityRelation PriceLevelEntityUsingCompanyIdStatic = new CompanyRelations().PriceLevelEntityUsingCompanyId;
		internal static readonly IEntityRelation PriceScheduleEntityUsingCompanyIdStatic = new CompanyRelations().PriceScheduleEntityUsingCompanyId;
		internal static readonly IEntityRelation ProductEntityUsingCompanyIdStatic = new CompanyRelations().ProductEntityUsingCompanyId;
		internal static readonly IEntityRelation ProductgroupEntityUsingCompanyIdStatic = new CompanyRelations().ProductgroupEntityUsingCompanyId;
		internal static readonly IEntityRelation ReceiptEntityUsingCompanyIdStatic = new CompanyRelations().ReceiptEntityUsingCompanyId;
		internal static readonly IEntityRelation ReceiptTemplateEntityUsingCompanyIdStatic = new CompanyRelations().ReceiptTemplateEntityUsingCompanyId;
		internal static readonly IEntityRelation ReportProcessingTaskEntityUsingCompanyIdStatic = new CompanyRelations().ReportProcessingTaskEntityUsingCompanyId;
		internal static readonly IEntityRelation ReportProcessingTaskTemplateEntityUsingCompanyIdStatic = new CompanyRelations().ReportProcessingTaskTemplateEntityUsingCompanyId;
		internal static readonly IEntityRelation RoomControlConfigurationEntityUsingCompanyIdStatic = new CompanyRelations().RoomControlConfigurationEntityUsingCompanyId;
		internal static readonly IEntityRelation RouteEntityUsingCompanyIdStatic = new CompanyRelations().RouteEntityUsingCompanyId;
		internal static readonly IEntityRelation ScheduleEntityUsingCompanyIdStatic = new CompanyRelations().ScheduleEntityUsingCompanyId;
		internal static readonly IEntityRelation ScheduledCommandTaskEntityUsingCompanyIdStatic = new CompanyRelations().ScheduledCommandTaskEntityUsingCompanyId;
		internal static readonly IEntityRelation ScheduledMessageEntityUsingCompanyIdStatic = new CompanyRelations().ScheduledMessageEntityUsingCompanyId;
		internal static readonly IEntityRelation ServiceMethodEntityUsingCompanyIdStatic = new CompanyRelations().ServiceMethodEntityUsingCompanyId;
		internal static readonly IEntityRelation ServiceMethodDeliverypointgroupEntityUsingParentCompanyIdStatic = new CompanyRelations().ServiceMethodDeliverypointgroupEntityUsingParentCompanyId;
		internal static readonly IEntityRelation SetupCodeEntityUsingCompanyIdStatic = new CompanyRelations().SetupCodeEntityUsingCompanyId;
		internal static readonly IEntityRelation SiteEntityUsingCompanyIdStatic = new CompanyRelations().SiteEntityUsingCompanyId;
		internal static readonly IEntityRelation SmsKeywordEntityUsingCompanyIdStatic = new CompanyRelations().SmsKeywordEntityUsingCompanyId;
		internal static readonly IEntityRelation StationListEntityUsingCompanyIdStatic = new CompanyRelations().StationListEntityUsingCompanyId;
		internal static readonly IEntityRelation SurveyEntityUsingCompanyIdStatic = new CompanyRelations().SurveyEntityUsingCompanyId;
		internal static readonly IEntityRelation TagEntityUsingCompanyIdStatic = new CompanyRelations().TagEntityUsingCompanyId;
		internal static readonly IEntityRelation TaxTariffEntityUsingCompanyIdStatic = new CompanyRelations().TaxTariffEntityUsingCompanyId;
		internal static readonly IEntityRelation TerminalEntityUsingCompanyIdStatic = new CompanyRelations().TerminalEntityUsingCompanyId;
		internal static readonly IEntityRelation TerminalConfigurationEntityUsingCompanyIdStatic = new CompanyRelations().TerminalConfigurationEntityUsingCompanyId;
		internal static readonly IEntityRelation UIModeEntityUsingCompanyIdStatic = new CompanyRelations().UIModeEntityUsingCompanyId;
		internal static readonly IEntityRelation UIScheduleEntityUsingCompanyIdStatic = new CompanyRelations().UIScheduleEntityUsingCompanyId;
		internal static readonly IEntityRelation UIThemeEntityUsingCompanyIdStatic = new CompanyRelations().UIThemeEntityUsingCompanyId;
		internal static readonly IEntityRelation WifiConfigurationEntityUsingCompanyIdStatic = new CompanyRelations().WifiConfigurationEntityUsingCompanyId;
		internal static readonly IEntityRelation TimestampEntityUsingCompanyIdStatic = new CompanyRelations().TimestampEntityUsingCompanyId;
		internal static readonly IEntityRelation ActionButtonEntityUsingActionButtonIdStatic = new CompanyRelations().ActionButtonEntityUsingActionButtonId;
		internal static readonly IEntityRelation CompanygroupEntityUsingCompanygroupIdStatic = new CompanyRelations().CompanygroupEntityUsingCompanygroupId;
		internal static readonly IEntityRelation CompanyOwnerEntityUsingCompanyOwnerIdStatic = new CompanyRelations().CompanyOwnerEntityUsingCompanyOwnerId;
		internal static readonly IEntityRelation CountryEntityUsingCountryIdStatic = new CompanyRelations().CountryEntityUsingCountryId;
		internal static readonly IEntityRelation CurrencyEntityUsingCurrencyIdStatic = new CompanyRelations().CurrencyEntityUsingCurrencyId;
		internal static readonly IEntityRelation LanguageEntityUsingLanguageIdStatic = new CompanyRelations().LanguageEntityUsingLanguageId;
		internal static readonly IEntityRelation RouteEntityUsingRouteIdStatic = new CompanyRelations().RouteEntityUsingRouteId;
		internal static readonly IEntityRelation SupportpoolEntityUsingSupportpoolIdStatic = new CompanyRelations().SupportpoolEntityUsingSupportpoolId;
		internal static readonly IEntityRelation TimeZoneEntityUsingTimeZoneIdStatic = new CompanyRelations().TimeZoneEntityUsingTimeZoneId;

		/// <summary>CTor</summary>
		static StaticCompanyRelations()
		{
		}
	}
}
