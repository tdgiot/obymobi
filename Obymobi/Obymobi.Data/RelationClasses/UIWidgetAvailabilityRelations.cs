﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: UIWidgetAvailability. </summary>
	public partial class UIWidgetAvailabilityRelations
	{
		/// <summary>CTor</summary>
		public UIWidgetAvailabilityRelations()
		{
		}

		/// <summary>Gets all relations of the UIWidgetAvailabilityEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AvailabilityEntityUsingAvailabilityId);
			toReturn.Add(this.UIWidgetEntityUsingUIWidgetId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between UIWidgetAvailabilityEntity and AvailabilityEntity over the m:1 relation they have, using the relation between the fields:
		/// UIWidgetAvailability.AvailabilityId - Availability.AvailabilityId
		/// </summary>
		public virtual IEntityRelation AvailabilityEntityUsingAvailabilityId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AvailabilityEntity", false);
				relation.AddEntityFieldPair(AvailabilityFields.AvailabilityId, UIWidgetAvailabilityFields.AvailabilityId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AvailabilityEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIWidgetAvailabilityEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between UIWidgetAvailabilityEntity and UIWidgetEntity over the m:1 relation they have, using the relation between the fields:
		/// UIWidgetAvailability.UIWidgetId - UIWidget.UIWidgetId
		/// </summary>
		public virtual IEntityRelation UIWidgetEntityUsingUIWidgetId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UIWidgetEntity", false);
				relation.AddEntityFieldPair(UIWidgetFields.UIWidgetId, UIWidgetAvailabilityFields.UIWidgetId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIWidgetEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIWidgetAvailabilityEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticUIWidgetAvailabilityRelations
	{
		internal static readonly IEntityRelation AvailabilityEntityUsingAvailabilityIdStatic = new UIWidgetAvailabilityRelations().AvailabilityEntityUsingAvailabilityId;
		internal static readonly IEntityRelation UIWidgetEntityUsingUIWidgetIdStatic = new UIWidgetAvailabilityRelations().UIWidgetEntityUsingUIWidgetId;

		/// <summary>CTor</summary>
		static StaticUIWidgetAvailabilityRelations()
		{
		}
	}
}
