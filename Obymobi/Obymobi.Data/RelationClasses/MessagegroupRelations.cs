﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Messagegroup. </summary>
	public partial class MessagegroupRelations
	{
		/// <summary>CTor</summary>
		public MessagegroupRelations()
		{
		}

		/// <summary>Gets all relations of the MessagegroupEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.MessagegroupDeliverypointEntityUsingMessagegroupId);
			toReturn.Add(this.PmsActionRuleEntityUsingMessagegroupId);
			toReturn.Add(this.ScheduledMessageHistoryEntityUsingMessagegroupId);
			toReturn.Add(this.UIScheduleItemOccurrenceEntityUsingMessagegroupId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between MessagegroupEntity and MessagegroupDeliverypointEntity over the 1:n relation they have, using the relation between the fields:
		/// Messagegroup.MessagegroupId - MessagegroupDeliverypoint.MessagegroupId
		/// </summary>
		public virtual IEntityRelation MessagegroupDeliverypointEntityUsingMessagegroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MessagegroupDeliverypointCollection" , true);
				relation.AddEntityFieldPair(MessagegroupFields.MessagegroupId, MessagegroupDeliverypointFields.MessagegroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessagegroupEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessagegroupDeliverypointEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between MessagegroupEntity and PmsActionRuleEntity over the 1:n relation they have, using the relation between the fields:
		/// Messagegroup.MessagegroupId - PmsActionRule.MessagegroupId
		/// </summary>
		public virtual IEntityRelation PmsActionRuleEntityUsingMessagegroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PmsActionRuleCollection" , true);
				relation.AddEntityFieldPair(MessagegroupFields.MessagegroupId, PmsActionRuleFields.MessagegroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessagegroupEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PmsActionRuleEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between MessagegroupEntity and ScheduledMessageHistoryEntity over the 1:n relation they have, using the relation between the fields:
		/// Messagegroup.MessagegroupId - ScheduledMessageHistory.MessagegroupId
		/// </summary>
		public virtual IEntityRelation ScheduledMessageHistoryEntityUsingMessagegroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ScheduledMessageHistoryCollection" , true);
				relation.AddEntityFieldPair(MessagegroupFields.MessagegroupId, ScheduledMessageHistoryFields.MessagegroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessagegroupEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduledMessageHistoryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between MessagegroupEntity and UIScheduleItemOccurrenceEntity over the 1:n relation they have, using the relation between the fields:
		/// Messagegroup.MessagegroupId - UIScheduleItemOccurrence.MessagegroupId
		/// </summary>
		public virtual IEntityRelation UIScheduleItemOccurrenceEntityUsingMessagegroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UIScheduleItemOccurrenceCollection" , true);
				relation.AddEntityFieldPair(MessagegroupFields.MessagegroupId, UIScheduleItemOccurrenceFields.MessagegroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessagegroupEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIScheduleItemOccurrenceEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between MessagegroupEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// Messagegroup.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, MessagegroupFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessagegroupEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticMessagegroupRelations
	{
		internal static readonly IEntityRelation MessagegroupDeliverypointEntityUsingMessagegroupIdStatic = new MessagegroupRelations().MessagegroupDeliverypointEntityUsingMessagegroupId;
		internal static readonly IEntityRelation PmsActionRuleEntityUsingMessagegroupIdStatic = new MessagegroupRelations().PmsActionRuleEntityUsingMessagegroupId;
		internal static readonly IEntityRelation ScheduledMessageHistoryEntityUsingMessagegroupIdStatic = new MessagegroupRelations().ScheduledMessageHistoryEntityUsingMessagegroupId;
		internal static readonly IEntityRelation UIScheduleItemOccurrenceEntityUsingMessagegroupIdStatic = new MessagegroupRelations().UIScheduleItemOccurrenceEntityUsingMessagegroupId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new MessagegroupRelations().CompanyEntityUsingCompanyId;

		/// <summary>CTor</summary>
		static StaticMessagegroupRelations()
		{
		}
	}
}
