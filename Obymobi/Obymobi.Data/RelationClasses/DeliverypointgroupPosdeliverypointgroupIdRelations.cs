﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 2.6
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates.NET20
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the static Relations variant for the entity: DeliverypointgroupPosdeliverypointgroupId. </summary>
	public partial class DeliverypointgroupPosdeliverypointgroupIdRelations
	{
		/// <summary>CTor</summary>
		public DeliverypointgroupPosdeliverypointgroupIdRelations()
		{
		}

		/// <summary>Gets all relations of the DeliverypointgroupPosdeliverypointgroupIdEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();


			toReturn.Add(this.DeliverypointgroupEntityUsingDeliverypointgroupId);
			toReturn.Add(this.PosdeliverypointgroupEntityUsingPosdeliverypointgroupId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupPosdeliverypointgroupIdEntity and DeliverypointgroupEntity over the m:1 relation they have, using the relation between the fields:
		/// DeliverypointgroupPosdeliverypointgroupId.DeliverypointgroupId - Deliverypointgroup.DeliverypointgroupId
		/// </summary>
		public virtual IEntityRelation DeliverypointgroupEntityUsingDeliverypointgroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DeliverypointgroupEntity", false);
				relation.AddEntityFieldPair(DeliverypointgroupFields.DeliverypointgroupId, DeliverypointgroupPosdeliverypointgroupIdFields.DeliverypointgroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupPosdeliverypointgroupIdEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between DeliverypointgroupPosdeliverypointgroupIdEntity and PosdeliverypointgroupEntity over the m:1 relation they have, using the relation between the fields:
		/// DeliverypointgroupPosdeliverypointgroupId.PosdeliverypointgroupId - Posdeliverypointgroup.PosdeliverypointgroupId
		/// </summary>
		public virtual IEntityRelation PosdeliverypointgroupEntityUsingPosdeliverypointgroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PosdeliverypointgroupEntity", false);
				relation.AddEntityFieldPair(PosdeliverypointgroupFields.PosdeliverypointgroupId, DeliverypointgroupPosdeliverypointgroupIdFields.PosdeliverypointgroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PosdeliverypointgroupEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointgroupPosdeliverypointgroupIdEntity", true);
				return relation;
			}
		}

		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}

		#endregion

		#region Included Code

		#endregion
	}
}
