﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ReportProcessingTaskFile. </summary>
	public partial class ReportProcessingTaskFileRelations
	{
		/// <summary>CTor</summary>
		public ReportProcessingTaskFileRelations()
		{
		}

		/// <summary>Gets all relations of the ReportProcessingTaskFileEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ReportProcessingTaskEntityUsingReportProcessingTaskId);
			return toReturn;
		}

		#region Class Property Declarations


		/// <summary>Returns a new IEntityRelation object, between ReportProcessingTaskFileEntity and ReportProcessingTaskEntity over the 1:1 relation they have, using the relation between the fields:
		/// ReportProcessingTaskFile.ReportProcessingTaskId - ReportProcessingTask.ReportProcessingTaskId
		/// </summary>
		public virtual IEntityRelation ReportProcessingTaskEntityUsingReportProcessingTaskId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "ReportProcessingTaskEntity", false);




				relation.AddEntityFieldPair(ReportProcessingTaskFields.ReportProcessingTaskId, ReportProcessingTaskFileFields.ReportProcessingTaskId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportProcessingTaskEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportProcessingTaskFileEntity", true);
				return relation;
			}
		}

		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticReportProcessingTaskFileRelations
	{
		internal static readonly IEntityRelation ReportProcessingTaskEntityUsingReportProcessingTaskIdStatic = new ReportProcessingTaskFileRelations().ReportProcessingTaskEntityUsingReportProcessingTaskId;

		/// <summary>CTor</summary>
		static StaticReportProcessingTaskFileRelations()
		{
		}
	}
}
