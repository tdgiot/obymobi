﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Outlet. </summary>
	public partial class OutletRelations
	{
		/// <summary>CTor</summary>
		public OutletRelations()
		{
		}

		/// <summary>Gets all relations of the OutletEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.LandingPageEntityUsingOutletId);
			toReturn.Add(this.BusinesshoursEntityUsingOutletId);
			toReturn.Add(this.CheckoutMethodEntityUsingOutletId);
			toReturn.Add(this.CustomTextEntityUsingOutletId);
			toReturn.Add(this.OptInEntityUsingOutletId);
			toReturn.Add(this.OrderEntityUsingOutletId);
			toReturn.Add(this.ServiceMethodEntityUsingOutletId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			toReturn.Add(this.OutletOperationalStateEntityUsingOutletOperationalStateId);
			toReturn.Add(this.OutletSellerInformationEntityUsingOutletSellerInformationId);
			toReturn.Add(this.ProductEntityUsingDeliveryChargeProductId);
			toReturn.Add(this.ProductEntityUsingServiceChargeProductId);
			toReturn.Add(this.ProductEntityUsingTippingProductId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between OutletEntity and LandingPageEntity over the 1:n relation they have, using the relation between the fields:
		/// Outlet.OutletId - LandingPage.OutletId
		/// </summary>
		public virtual IEntityRelation LandingPageEntityUsingOutletId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "LandingPageCollection" , true);
				relation.AddEntityFieldPair(OutletFields.OutletId, LandingPageFields.OutletId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OutletEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LandingPageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between OutletEntity and BusinesshoursEntity over the 1:n relation they have, using the relation between the fields:
		/// Outlet.OutletId - Businesshours.OutletId
		/// </summary>
		public virtual IEntityRelation BusinesshoursEntityUsingOutletId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "BusinesshoursCollection" , true);
				relation.AddEntityFieldPair(OutletFields.OutletId, BusinesshoursFields.OutletId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OutletEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinesshoursEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between OutletEntity and CheckoutMethodEntity over the 1:n relation they have, using the relation between the fields:
		/// Outlet.OutletId - CheckoutMethod.OutletId
		/// </summary>
		public virtual IEntityRelation CheckoutMethodEntityUsingOutletId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CheckoutMethodCollection" , true);
				relation.AddEntityFieldPair(OutletFields.OutletId, CheckoutMethodFields.OutletId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OutletEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CheckoutMethodEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between OutletEntity and CustomTextEntity over the 1:n relation they have, using the relation between the fields:
		/// Outlet.OutletId - CustomText.OutletId
		/// </summary>
		public virtual IEntityRelation CustomTextEntityUsingOutletId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomTextCollection" , true);
				relation.AddEntityFieldPair(OutletFields.OutletId, CustomTextFields.OutletId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OutletEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between OutletEntity and OptInEntity over the 1:n relation they have, using the relation between the fields:
		/// Outlet.OutletId - OptIn.OutletId
		/// </summary>
		public virtual IEntityRelation OptInEntityUsingOutletId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OptInCollection" , true);
				relation.AddEntityFieldPair(OutletFields.OutletId, OptInFields.OutletId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OutletEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OptInEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between OutletEntity and OrderEntity over the 1:n relation they have, using the relation between the fields:
		/// Outlet.OutletId - Order.OutletId
		/// </summary>
		public virtual IEntityRelation OrderEntityUsingOutletId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrderCollection" , true);
				relation.AddEntityFieldPair(OutletFields.OutletId, OrderFields.OutletId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OutletEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between OutletEntity and ServiceMethodEntity over the 1:n relation they have, using the relation between the fields:
		/// Outlet.OutletId - ServiceMethod.OutletId
		/// </summary>
		public virtual IEntityRelation ServiceMethodEntityUsingOutletId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ServiceMethodCollection" , true);
				relation.AddEntityFieldPair(OutletFields.OutletId, ServiceMethodFields.OutletId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OutletEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ServiceMethodEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between OutletEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// Outlet.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, OutletFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OutletEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OutletEntity and OutletOperationalStateEntity over the m:1 relation they have, using the relation between the fields:
		/// Outlet.OutletOperationalStateId - OutletOperationalState.OutletOperationalStateId
		/// </summary>
		public virtual IEntityRelation OutletOperationalStateEntityUsingOutletOperationalStateId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "OutletOperationalStateEntity", false);
				relation.AddEntityFieldPair(OutletOperationalStateFields.OutletOperationalStateId, OutletFields.OutletOperationalStateId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OutletOperationalStateEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OutletEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OutletEntity and OutletSellerInformationEntity over the m:1 relation they have, using the relation between the fields:
		/// Outlet.OutletSellerInformationId - OutletSellerInformation.OutletSellerInformationId
		/// </summary>
		public virtual IEntityRelation OutletSellerInformationEntityUsingOutletSellerInformationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "OutletSellerInformationEntity", false);
				relation.AddEntityFieldPair(OutletSellerInformationFields.OutletSellerInformationId, OutletFields.OutletSellerInformationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OutletSellerInformationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OutletEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OutletEntity and ProductEntity over the m:1 relation they have, using the relation between the fields:
		/// Outlet.DeliveryChargeProductId - Product.ProductId
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingDeliveryChargeProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DeliveryChargeProductEntity", false);
				relation.AddEntityFieldPair(ProductFields.ProductId, OutletFields.DeliveryChargeProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OutletEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OutletEntity and ProductEntity over the m:1 relation they have, using the relation between the fields:
		/// Outlet.ServiceChargeProductId - Product.ProductId
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingServiceChargeProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ServiceChargeProductEntity", false);
				relation.AddEntityFieldPair(ProductFields.ProductId, OutletFields.ServiceChargeProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OutletEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OutletEntity and ProductEntity over the m:1 relation they have, using the relation between the fields:
		/// Outlet.TippingProductId - Product.ProductId
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingTippingProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TippingProductEntity", false);
				relation.AddEntityFieldPair(ProductFields.ProductId, OutletFields.TippingProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OutletEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticOutletRelations
	{
		internal static readonly IEntityRelation LandingPageEntityUsingOutletIdStatic = new OutletRelations().LandingPageEntityUsingOutletId;
		internal static readonly IEntityRelation BusinesshoursEntityUsingOutletIdStatic = new OutletRelations().BusinesshoursEntityUsingOutletId;
		internal static readonly IEntityRelation CheckoutMethodEntityUsingOutletIdStatic = new OutletRelations().CheckoutMethodEntityUsingOutletId;
		internal static readonly IEntityRelation CustomTextEntityUsingOutletIdStatic = new OutletRelations().CustomTextEntityUsingOutletId;
		internal static readonly IEntityRelation OptInEntityUsingOutletIdStatic = new OutletRelations().OptInEntityUsingOutletId;
		internal static readonly IEntityRelation OrderEntityUsingOutletIdStatic = new OutletRelations().OrderEntityUsingOutletId;
		internal static readonly IEntityRelation ServiceMethodEntityUsingOutletIdStatic = new OutletRelations().ServiceMethodEntityUsingOutletId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new OutletRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation OutletOperationalStateEntityUsingOutletOperationalStateIdStatic = new OutletRelations().OutletOperationalStateEntityUsingOutletOperationalStateId;
		internal static readonly IEntityRelation OutletSellerInformationEntityUsingOutletSellerInformationIdStatic = new OutletRelations().OutletSellerInformationEntityUsingOutletSellerInformationId;
		internal static readonly IEntityRelation ProductEntityUsingDeliveryChargeProductIdStatic = new OutletRelations().ProductEntityUsingDeliveryChargeProductId;
		internal static readonly IEntityRelation ProductEntityUsingServiceChargeProductIdStatic = new OutletRelations().ProductEntityUsingServiceChargeProductId;
		internal static readonly IEntityRelation ProductEntityUsingTippingProductIdStatic = new OutletRelations().ProductEntityUsingTippingProductId;

		/// <summary>CTor</summary>
		static StaticOutletRelations()
		{
		}
	}
}
