﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: PmsRule. </summary>
	public partial class PmsRuleRelations
	{
		/// <summary>CTor</summary>
		public PmsRuleRelations()
		{
		}

		/// <summary>Gets all relations of the PmsRuleEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.PmsActionRuleEntityUsingPmsActionRuleId);
			toReturn.Add(this.PmsReportColumnEntityUsingPmsReportColumnId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between PmsRuleEntity and PmsActionRuleEntity over the m:1 relation they have, using the relation between the fields:
		/// PmsRule.PmsActionRuleId - PmsActionRule.PmsActionRuleId
		/// </summary>
		public virtual IEntityRelation PmsActionRuleEntityUsingPmsActionRuleId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PmsActionRuleEntity", false);
				relation.AddEntityFieldPair(PmsActionRuleFields.PmsActionRuleId, PmsRuleFields.PmsActionRuleId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PmsActionRuleEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PmsRuleEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between PmsRuleEntity and PmsReportColumnEntity over the m:1 relation they have, using the relation between the fields:
		/// PmsRule.PmsReportColumnId - PmsReportColumn.PmsReportColumnId
		/// </summary>
		public virtual IEntityRelation PmsReportColumnEntityUsingPmsReportColumnId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PmsReportColumnEntity", false);
				relation.AddEntityFieldPair(PmsReportColumnFields.PmsReportColumnId, PmsRuleFields.PmsReportColumnId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PmsReportColumnEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PmsRuleEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPmsRuleRelations
	{
		internal static readonly IEntityRelation PmsActionRuleEntityUsingPmsActionRuleIdStatic = new PmsRuleRelations().PmsActionRuleEntityUsingPmsActionRuleId;
		internal static readonly IEntityRelation PmsReportColumnEntityUsingPmsReportColumnIdStatic = new PmsRuleRelations().PmsReportColumnEntityUsingPmsReportColumnId;

		/// <summary>CTor</summary>
		static StaticPmsRuleRelations()
		{
		}
	}
}
