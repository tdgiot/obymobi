﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: RoomControlConfiguration. </summary>
	public partial class RoomControlConfigurationRelations
	{
		/// <summary>CTor</summary>
		public RoomControlConfigurationRelations()
		{
		}

		/// <summary>Gets all relations of the RoomControlConfigurationEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ClientConfigurationEntityUsingRoomControlConfigurationId);
			toReturn.Add(this.DeliverypointEntityUsingRoomControlConfigurationId);
			toReturn.Add(this.RoomControlAreaEntityUsingRoomControlConfigurationId);
			toReturn.Add(this.TimestampEntityUsingRoomControlConfigurationId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between RoomControlConfigurationEntity and ClientConfigurationEntity over the 1:n relation they have, using the relation between the fields:
		/// RoomControlConfiguration.RoomControlConfigurationId - ClientConfiguration.RoomControlConfigurationId
		/// </summary>
		public virtual IEntityRelation ClientConfigurationEntityUsingRoomControlConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ClientConfigurationCollection" , true);
				relation.AddEntityFieldPair(RoomControlConfigurationFields.RoomControlConfigurationId, ClientConfigurationFields.RoomControlConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlConfigurationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientConfigurationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RoomControlConfigurationEntity and DeliverypointEntity over the 1:n relation they have, using the relation between the fields:
		/// RoomControlConfiguration.RoomControlConfigurationId - Deliverypoint.RoomControlConfigurationId
		/// </summary>
		public virtual IEntityRelation DeliverypointEntityUsingRoomControlConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeliverypointCollection" , true);
				relation.AddEntityFieldPair(RoomControlConfigurationFields.RoomControlConfigurationId, DeliverypointFields.RoomControlConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlConfigurationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RoomControlConfigurationEntity and RoomControlAreaEntity over the 1:n relation they have, using the relation between the fields:
		/// RoomControlConfiguration.RoomControlConfigurationId - RoomControlArea.RoomControlConfigurationId
		/// </summary>
		public virtual IEntityRelation RoomControlAreaEntityUsingRoomControlConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RoomControlAreaCollection" , true);
				relation.AddEntityFieldPair(RoomControlConfigurationFields.RoomControlConfigurationId, RoomControlAreaFields.RoomControlConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlConfigurationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlAreaEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RoomControlConfigurationEntity and TimestampEntity over the 1:1 relation they have, using the relation between the fields:
		/// RoomControlConfiguration.RoomControlConfigurationId - Timestamp.RoomControlConfigurationId
		/// </summary>
		public virtual IEntityRelation TimestampEntityUsingRoomControlConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "TimestampCollection", true);


				relation.AddEntityFieldPair(RoomControlConfigurationFields.RoomControlConfigurationId, TimestampFields.RoomControlConfigurationId);


				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlConfigurationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TimestampEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RoomControlConfigurationEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// RoomControlConfiguration.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, RoomControlConfigurationFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlConfigurationEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticRoomControlConfigurationRelations
	{
		internal static readonly IEntityRelation ClientConfigurationEntityUsingRoomControlConfigurationIdStatic = new RoomControlConfigurationRelations().ClientConfigurationEntityUsingRoomControlConfigurationId;
		internal static readonly IEntityRelation DeliverypointEntityUsingRoomControlConfigurationIdStatic = new RoomControlConfigurationRelations().DeliverypointEntityUsingRoomControlConfigurationId;
		internal static readonly IEntityRelation RoomControlAreaEntityUsingRoomControlConfigurationIdStatic = new RoomControlConfigurationRelations().RoomControlAreaEntityUsingRoomControlConfigurationId;
		internal static readonly IEntityRelation TimestampEntityUsingRoomControlConfigurationIdStatic = new RoomControlConfigurationRelations().TimestampEntityUsingRoomControlConfigurationId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new RoomControlConfigurationRelations().CompanyEntityUsingCompanyId;

		/// <summary>CTor</summary>
		static StaticRoomControlConfigurationRelations()
		{
		}
	}
}
