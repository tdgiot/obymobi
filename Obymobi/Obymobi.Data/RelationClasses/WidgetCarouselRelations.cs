﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: WidgetCarousel. </summary>
	public partial class WidgetCarouselRelations : WidgetRelations
	{
		/// <summary>CTor</summary>
		public WidgetCarouselRelations()
		{
		}

		/// <summary>Gets all relations of the WidgetCarouselEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public override List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = base.GetAllRelations();
			toReturn.Add(this.CarouselItemEntityUsingWidgetCarouselId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between WidgetCarouselEntity and CarouselItemEntity over the 1:n relation they have, using the relation between the fields:
		/// WidgetCarousel.WidgetId - CarouselItem.WidgetCarouselId
		/// </summary>
		public virtual IEntityRelation CarouselItemEntityUsingWidgetCarouselId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CarouselItemCollection" , true);
				relation.AddEntityFieldPair(WidgetCarouselFields.WidgetId, CarouselItemFields.WidgetCarouselId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WidgetCarouselEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CarouselItemEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between WidgetCarouselEntity and LandingPageWidgetEntity over the 1:n relation they have, using the relation between the fields:
		/// WidgetCarousel.WidgetId - LandingPageWidget.WidgetId
		/// </summary>
		public override IEntityRelation LandingPageWidgetEntityUsingWidgetId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "LandingPageWidgetCollection" , true);
				relation.AddEntityFieldPair(WidgetCarouselFields.WidgetId, LandingPageWidgetFields.WidgetId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WidgetCarouselEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LandingPageWidgetEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between WidgetCarouselEntity and NavigationMenuWidgetEntity over the 1:n relation they have, using the relation between the fields:
		/// WidgetCarousel.WidgetId - NavigationMenuWidget.WidgetId
		/// </summary>
		public override IEntityRelation NavigationMenuWidgetEntityUsingWidgetId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "NavigationMenuWidgetCollection" , true);
				relation.AddEntityFieldPair(WidgetCarouselFields.WidgetId, NavigationMenuWidgetFields.WidgetId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WidgetCarouselEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NavigationMenuWidgetEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between WidgetCarouselEntity and WidgetGroupWidgetEntity over the 1:n relation they have, using the relation between the fields:
		/// WidgetCarousel.WidgetId - WidgetGroupWidget.WidgetId
		/// </summary>
		public override IEntityRelation WidgetGroupWidgetEntityUsingWidgetId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ChildWidgetCollection" , true);
				relation.AddEntityFieldPair(WidgetCarouselFields.WidgetId, WidgetGroupWidgetFields.WidgetId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WidgetCarouselEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WidgetGroupWidgetEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between WidgetCarouselEntity and CustomTextEntity over the 1:n relation they have, using the relation between the fields:
		/// WidgetCarousel.WidgetId - CustomText.WidgetId
		/// </summary>
		public override IEntityRelation CustomTextEntityUsingWidgetId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomTextCollection" , true);
				relation.AddEntityFieldPair(WidgetCarouselFields.WidgetId, CustomTextFields.WidgetId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WidgetCarouselEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between WidgetCarouselEntity and ActionEntity over the m:1 relation they have, using the relation between the fields:
		/// WidgetCarousel.ActionId - Action.ActionId
		/// </summary>
		public override IEntityRelation ActionEntityUsingActionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ActionEntity", false);
				relation.AddEntityFieldPair(ActionFields.ActionId, WidgetCarouselFields.ActionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ActionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WidgetCarouselEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between WidgetCarouselEntity and ApplicationConfigurationEntity over the m:1 relation they have, using the relation between the fields:
		/// WidgetCarousel.ApplicationConfigurationId - ApplicationConfiguration.ApplicationConfigurationId
		/// </summary>
		public override IEntityRelation ApplicationConfigurationEntityUsingApplicationConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ApplicationConfigurationEntity", false);
				relation.AddEntityFieldPair(ApplicationConfigurationFields.ApplicationConfigurationId, WidgetCarouselFields.ApplicationConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ApplicationConfigurationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WidgetCarouselEntity", true);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between WidgetCarouselEntity and WidgetEntity over the 1:1 relation they have, which is used to build a target per entity hierarchy</summary>
		internal IEntityRelation RelationToSuperTypeWidgetEntity
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, false);
				relation.AddEntityFieldPair(WidgetFields.WidgetId, WidgetCarouselFields.WidgetId);
				relation.IsHierarchyRelation=true;
				return relation;
			}
		}

		
		/// <summary>Returns the relation object the entity, to which this relation factory belongs, has with the subtype with the specified name</summary>
		/// <param name="subTypeEntityName">name of direct subtype which is a subtype of the current entity through the relation to return.</param>
		/// <returns>relation which makes the current entity a supertype of the subtype entity with the name specified, or null if not applicable/found</returns>
		public override IEntityRelation GetSubTypeRelation(string subTypeEntityName)
		{
			return null;
		}
		
		/// <summary>Returns the relation object the entity, to which this relation factory belongs, has with its supertype, if applicable.</summary>
		/// <returns>relation which makes the current entity a subtype of its supertype entity or null if not applicable/found</returns>
		public override IEntityRelation GetSuperTypeRelation()
		{
			return this.RelationToSuperTypeWidgetEntity;
		}

		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticWidgetCarouselRelations
	{
		internal static readonly IEntityRelation CarouselItemEntityUsingWidgetCarouselIdStatic = new WidgetCarouselRelations().CarouselItemEntityUsingWidgetCarouselId;
		internal static readonly IEntityRelation LandingPageWidgetEntityUsingWidgetIdStatic = new WidgetCarouselRelations().LandingPageWidgetEntityUsingWidgetId;
		internal static readonly IEntityRelation NavigationMenuWidgetEntityUsingWidgetIdStatic = new WidgetCarouselRelations().NavigationMenuWidgetEntityUsingWidgetId;
		internal static readonly IEntityRelation WidgetGroupWidgetEntityUsingWidgetIdStatic = new WidgetCarouselRelations().WidgetGroupWidgetEntityUsingWidgetId;
		internal static readonly IEntityRelation CustomTextEntityUsingWidgetIdStatic = new WidgetCarouselRelations().CustomTextEntityUsingWidgetId;
		internal static readonly IEntityRelation ActionEntityUsingActionIdStatic = new WidgetCarouselRelations().ActionEntityUsingActionId;
		internal static readonly IEntityRelation ApplicationConfigurationEntityUsingApplicationConfigurationIdStatic = new WidgetCarouselRelations().ApplicationConfigurationEntityUsingApplicationConfigurationId;

		/// <summary>CTor</summary>
		static StaticWidgetCarouselRelations()
		{
		}
	}
}
