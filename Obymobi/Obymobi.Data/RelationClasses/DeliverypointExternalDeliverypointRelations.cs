﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: DeliverypointExternalDeliverypoint. </summary>
	public partial class DeliverypointExternalDeliverypointRelations
	{
		/// <summary>CTor</summary>
		public DeliverypointExternalDeliverypointRelations()
		{
		}

		/// <summary>Gets all relations of the DeliverypointExternalDeliverypointEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.DeliverypointEntityUsingDeliverypointId);
			toReturn.Add(this.ExternalDeliverypointEntityUsingExternalDeliverypointId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between DeliverypointExternalDeliverypointEntity and DeliverypointEntity over the m:1 relation they have, using the relation between the fields:
		/// DeliverypointExternalDeliverypoint.DeliverypointId - Deliverypoint.DeliverypointId
		/// </summary>
		public virtual IEntityRelation DeliverypointEntityUsingDeliverypointId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DeliverypointEntity", false);
				relation.AddEntityFieldPair(DeliverypointFields.DeliverypointId, DeliverypointExternalDeliverypointFields.DeliverypointId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointExternalDeliverypointEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between DeliverypointExternalDeliverypointEntity and ExternalDeliverypointEntity over the m:1 relation they have, using the relation between the fields:
		/// DeliverypointExternalDeliverypoint.ExternalDeliverypointId - ExternalDeliverypoint.ExternalDeliverypointId
		/// </summary>
		public virtual IEntityRelation ExternalDeliverypointEntityUsingExternalDeliverypointId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ExternalDeliverypointEntity", false);
				relation.AddEntityFieldPair(ExternalDeliverypointFields.ExternalDeliverypointId, DeliverypointExternalDeliverypointFields.ExternalDeliverypointId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalDeliverypointEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointExternalDeliverypointEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticDeliverypointExternalDeliverypointRelations
	{
		internal static readonly IEntityRelation DeliverypointEntityUsingDeliverypointIdStatic = new DeliverypointExternalDeliverypointRelations().DeliverypointEntityUsingDeliverypointId;
		internal static readonly IEntityRelation ExternalDeliverypointEntityUsingExternalDeliverypointIdStatic = new DeliverypointExternalDeliverypointRelations().ExternalDeliverypointEntityUsingExternalDeliverypointId;

		/// <summary>CTor</summary>
		static StaticDeliverypointExternalDeliverypointRelations()
		{
		}
	}
}
