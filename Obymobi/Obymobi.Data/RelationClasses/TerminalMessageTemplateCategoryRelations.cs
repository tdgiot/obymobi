﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: TerminalMessageTemplateCategory. </summary>
	public partial class TerminalMessageTemplateCategoryRelations
	{
		/// <summary>CTor</summary>
		public TerminalMessageTemplateCategoryRelations()
		{
		}

		/// <summary>Gets all relations of the TerminalMessageTemplateCategoryEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.MessageTemplateCategoryEntityUsingMessageTemplateCategoryId);
			toReturn.Add(this.TerminalEntityUsingTerminalId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between TerminalMessageTemplateCategoryEntity and MessageTemplateCategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// TerminalMessageTemplateCategory.MessageTemplateCategoryId - MessageTemplateCategory.MessageTemplateCategoryId
		/// </summary>
		public virtual IEntityRelation MessageTemplateCategoryEntityUsingMessageTemplateCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "MessageTemplateCategoryEntity", false);
				relation.AddEntityFieldPair(MessageTemplateCategoryFields.MessageTemplateCategoryId, TerminalMessageTemplateCategoryFields.MessageTemplateCategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageTemplateCategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalMessageTemplateCategoryEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TerminalMessageTemplateCategoryEntity and TerminalEntity over the m:1 relation they have, using the relation between the fields:
		/// TerminalMessageTemplateCategory.TerminalId - Terminal.TerminalId
		/// </summary>
		public virtual IEntityRelation TerminalEntityUsingTerminalId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TerminalEntity", false);
				relation.AddEntityFieldPair(TerminalFields.TerminalId, TerminalMessageTemplateCategoryFields.TerminalId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalMessageTemplateCategoryEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticTerminalMessageTemplateCategoryRelations
	{
		internal static readonly IEntityRelation MessageTemplateCategoryEntityUsingMessageTemplateCategoryIdStatic = new TerminalMessageTemplateCategoryRelations().MessageTemplateCategoryEntityUsingMessageTemplateCategoryId;
		internal static readonly IEntityRelation TerminalEntityUsingTerminalIdStatic = new TerminalMessageTemplateCategoryRelations().TerminalEntityUsingTerminalId;

		/// <summary>CTor</summary>
		static StaticTerminalMessageTemplateCategoryRelations()
		{
		}
	}
}
