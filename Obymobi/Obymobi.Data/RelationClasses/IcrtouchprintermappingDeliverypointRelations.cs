﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: IcrtouchprintermappingDeliverypoint. </summary>
	public partial class IcrtouchprintermappingDeliverypointRelations
	{
		/// <summary>CTor</summary>
		public IcrtouchprintermappingDeliverypointRelations()
		{
		}

		/// <summary>Gets all relations of the IcrtouchprintermappingDeliverypointEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.DeliverypointEntityUsingDeliverypointId);
			toReturn.Add(this.IcrtouchprintermappingEntityUsingIcrtouchprintermappingId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between IcrtouchprintermappingDeliverypointEntity and DeliverypointEntity over the m:1 relation they have, using the relation between the fields:
		/// IcrtouchprintermappingDeliverypoint.DeliverypointId - Deliverypoint.DeliverypointId
		/// </summary>
		public virtual IEntityRelation DeliverypointEntityUsingDeliverypointId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DeliverypointEntity", false);
				relation.AddEntityFieldPair(DeliverypointFields.DeliverypointId, IcrtouchprintermappingDeliverypointFields.DeliverypointId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeliverypointEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("IcrtouchprintermappingDeliverypointEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between IcrtouchprintermappingDeliverypointEntity and IcrtouchprintermappingEntity over the m:1 relation they have, using the relation between the fields:
		/// IcrtouchprintermappingDeliverypoint.IcrtouchprintermappingId - Icrtouchprintermapping.IcrtouchprintermappingId
		/// </summary>
		public virtual IEntityRelation IcrtouchprintermappingEntityUsingIcrtouchprintermappingId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "IcrtouchprintermappingEntity", false);
				relation.AddEntityFieldPair(IcrtouchprintermappingFields.IcrtouchprintermappingId, IcrtouchprintermappingDeliverypointFields.IcrtouchprintermappingId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("IcrtouchprintermappingEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("IcrtouchprintermappingDeliverypointEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticIcrtouchprintermappingDeliverypointRelations
	{
		internal static readonly IEntityRelation DeliverypointEntityUsingDeliverypointIdStatic = new IcrtouchprintermappingDeliverypointRelations().DeliverypointEntityUsingDeliverypointId;
		internal static readonly IEntityRelation IcrtouchprintermappingEntityUsingIcrtouchprintermappingIdStatic = new IcrtouchprintermappingDeliverypointRelations().IcrtouchprintermappingEntityUsingIcrtouchprintermappingId;

		/// <summary>CTor</summary>
		static StaticIcrtouchprintermappingDeliverypointRelations()
		{
		}
	}
}
