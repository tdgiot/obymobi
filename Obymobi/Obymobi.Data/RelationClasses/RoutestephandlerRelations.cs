﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Routestephandler. </summary>
	public partial class RoutestephandlerRelations
	{
		/// <summary>CTor</summary>
		public RoutestephandlerRelations()
		{
		}

		/// <summary>Gets all relations of the RoutestephandlerEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CustomTextEntityUsingRoutestephandlerId);
			toReturn.Add(this.MediaEntityUsingRoutestephandlerId);
			toReturn.Add(this.ExternalSystemEntityUsingExternalSystemId);
			toReturn.Add(this.RoutestepEntityUsingRoutestepId);
			toReturn.Add(this.SupportpoolEntityUsingSupportpoolId);
			toReturn.Add(this.TerminalEntityUsingTerminalId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between RoutestephandlerEntity and CustomTextEntity over the 1:n relation they have, using the relation between the fields:
		/// Routestephandler.RoutestephandlerId - CustomText.RoutestephandlerId
		/// </summary>
		public virtual IEntityRelation CustomTextEntityUsingRoutestephandlerId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomTextCollection" , true);
				relation.AddEntityFieldPair(RoutestephandlerFields.RoutestephandlerId, CustomTextFields.RoutestephandlerId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoutestephandlerEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RoutestephandlerEntity and MediaEntity over the 1:n relation they have, using the relation between the fields:
		/// Routestephandler.RoutestephandlerId - Media.RoutestephandlerId
		/// </summary>
		public virtual IEntityRelation MediaEntityUsingRoutestephandlerId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MediaCollection" , true);
				relation.AddEntityFieldPair(RoutestephandlerFields.RoutestephandlerId, MediaFields.RoutestephandlerId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoutestephandlerEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between RoutestephandlerEntity and ExternalSystemEntity over the m:1 relation they have, using the relation between the fields:
		/// Routestephandler.ExternalSystemId - ExternalSystem.ExternalSystemId
		/// </summary>
		public virtual IEntityRelation ExternalSystemEntityUsingExternalSystemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ExternalSystemEntity", false);
				relation.AddEntityFieldPair(ExternalSystemFields.ExternalSystemId, RoutestephandlerFields.ExternalSystemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalSystemEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoutestephandlerEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RoutestephandlerEntity and RoutestepEntity over the m:1 relation they have, using the relation between the fields:
		/// Routestephandler.RoutestepId - Routestep.RoutestepId
		/// </summary>
		public virtual IEntityRelation RoutestepEntityUsingRoutestepId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RoutestepEntity", false);
				relation.AddEntityFieldPair(RoutestepFields.RoutestepId, RoutestephandlerFields.RoutestepId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoutestepEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoutestephandlerEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RoutestephandlerEntity and SupportpoolEntity over the m:1 relation they have, using the relation between the fields:
		/// Routestephandler.SupportpoolId - Supportpool.SupportpoolId
		/// </summary>
		public virtual IEntityRelation SupportpoolEntityUsingSupportpoolId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SupportpoolEntity", false);
				relation.AddEntityFieldPair(SupportpoolFields.SupportpoolId, RoutestephandlerFields.SupportpoolId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SupportpoolEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoutestephandlerEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RoutestephandlerEntity and TerminalEntity over the m:1 relation they have, using the relation between the fields:
		/// Routestephandler.TerminalId - Terminal.TerminalId
		/// </summary>
		public virtual IEntityRelation TerminalEntityUsingTerminalId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TerminalEntity", false);
				relation.AddEntityFieldPair(TerminalFields.TerminalId, RoutestephandlerFields.TerminalId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TerminalEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoutestephandlerEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticRoutestephandlerRelations
	{
		internal static readonly IEntityRelation CustomTextEntityUsingRoutestephandlerIdStatic = new RoutestephandlerRelations().CustomTextEntityUsingRoutestephandlerId;
		internal static readonly IEntityRelation MediaEntityUsingRoutestephandlerIdStatic = new RoutestephandlerRelations().MediaEntityUsingRoutestephandlerId;
		internal static readonly IEntityRelation ExternalSystemEntityUsingExternalSystemIdStatic = new RoutestephandlerRelations().ExternalSystemEntityUsingExternalSystemId;
		internal static readonly IEntityRelation RoutestepEntityUsingRoutestepIdStatic = new RoutestephandlerRelations().RoutestepEntityUsingRoutestepId;
		internal static readonly IEntityRelation SupportpoolEntityUsingSupportpoolIdStatic = new RoutestephandlerRelations().SupportpoolEntityUsingSupportpoolId;
		internal static readonly IEntityRelation TerminalEntityUsingTerminalIdStatic = new RoutestephandlerRelations().TerminalEntityUsingTerminalId;

		/// <summary>CTor</summary>
		static StaticRoutestephandlerRelations()
		{
		}
	}
}
