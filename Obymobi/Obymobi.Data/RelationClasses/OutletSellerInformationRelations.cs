﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: OutletSellerInformation. </summary>
	public partial class OutletSellerInformationRelations
	{
		/// <summary>CTor</summary>
		public OutletSellerInformationRelations()
		{
		}

		/// <summary>Gets all relations of the OutletSellerInformationEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.OutletEntityUsingOutletSellerInformationId);
			toReturn.Add(this.ReceiptEntityUsingOutletSellerInformationId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between OutletSellerInformationEntity and OutletEntity over the 1:n relation they have, using the relation between the fields:
		/// OutletSellerInformation.OutletSellerInformationId - Outlet.OutletSellerInformationId
		/// </summary>
		public virtual IEntityRelation OutletEntityUsingOutletSellerInformationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OutletCollection" , true);
				relation.AddEntityFieldPair(OutletSellerInformationFields.OutletSellerInformationId, OutletFields.OutletSellerInformationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OutletSellerInformationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OutletEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between OutletSellerInformationEntity and ReceiptEntity over the 1:n relation they have, using the relation between the fields:
		/// OutletSellerInformation.OutletSellerInformationId - Receipt.OutletSellerInformationId
		/// </summary>
		public virtual IEntityRelation ReceiptEntityUsingOutletSellerInformationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ReceiptCollection" , true);
				relation.AddEntityFieldPair(OutletSellerInformationFields.OutletSellerInformationId, ReceiptFields.OutletSellerInformationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OutletSellerInformationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReceiptEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticOutletSellerInformationRelations
	{
		internal static readonly IEntityRelation OutletEntityUsingOutletSellerInformationIdStatic = new OutletSellerInformationRelations().OutletEntityUsingOutletSellerInformationId;
		internal static readonly IEntityRelation ReceiptEntityUsingOutletSellerInformationIdStatic = new OutletSellerInformationRelations().ReceiptEntityUsingOutletSellerInformationId;

		/// <summary>CTor</summary>
		static StaticOutletSellerInformationRelations()
		{
		}
	}
}
