﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: StationList. </summary>
	public partial class StationListRelations
	{
		/// <summary>CTor</summary>
		public StationListRelations()
		{
		}

		/// <summary>Gets all relations of the StationListEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.RoomControlSectionItemEntityUsingStationListId);
			toReturn.Add(this.StationEntityUsingStationListId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between StationListEntity and RoomControlSectionItemEntity over the 1:n relation they have, using the relation between the fields:
		/// StationList.StationListId - RoomControlSectionItem.StationListId
		/// </summary>
		public virtual IEntityRelation RoomControlSectionItemEntityUsingStationListId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RoomControlSectionItemCollection" , true);
				relation.AddEntityFieldPair(StationListFields.StationListId, RoomControlSectionItemFields.StationListId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("StationListEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlSectionItemEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between StationListEntity and StationEntity over the 1:n relation they have, using the relation between the fields:
		/// StationList.StationListId - Station.StationListId
		/// </summary>
		public virtual IEntityRelation StationEntityUsingStationListId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "StationCollection" , true);
				relation.AddEntityFieldPair(StationListFields.StationListId, StationFields.StationListId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("StationListEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("StationEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between StationListEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// StationList.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, StationListFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("StationListEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticStationListRelations
	{
		internal static readonly IEntityRelation RoomControlSectionItemEntityUsingStationListIdStatic = new StationListRelations().RoomControlSectionItemEntityUsingStationListId;
		internal static readonly IEntityRelation StationEntityUsingStationListIdStatic = new StationListRelations().StationEntityUsingStationListId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new StationListRelations().CompanyEntityUsingCompanyId;

		/// <summary>CTor</summary>
		static StaticStationListRelations()
		{
		}
	}
}
