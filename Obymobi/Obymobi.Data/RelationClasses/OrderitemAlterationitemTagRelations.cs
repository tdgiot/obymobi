﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: OrderitemAlterationitemTag. </summary>
	public partial class OrderitemAlterationitemTagRelations
	{
		/// <summary>CTor</summary>
		public OrderitemAlterationitemTagRelations()
		{
		}

		/// <summary>Gets all relations of the OrderitemAlterationitemTagEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AlterationoptionEntityUsingAlterationoptionId);
			toReturn.Add(this.OrderitemAlterationitemEntityUsingOrderitemAlterationitemId);
			toReturn.Add(this.TagEntityUsingTagId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between OrderitemAlterationitemTagEntity and AlterationoptionEntity over the m:1 relation they have, using the relation between the fields:
		/// OrderitemAlterationitemTag.AlterationoptionId - Alterationoption.AlterationoptionId
		/// </summary>
		public virtual IEntityRelation AlterationoptionEntityUsingAlterationoptionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AlterationoptionEntity", false);
				relation.AddEntityFieldPair(AlterationoptionFields.AlterationoptionId, OrderitemAlterationitemTagFields.AlterationoptionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationoptionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderitemAlterationitemTagEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OrderitemAlterationitemTagEntity and OrderitemAlterationitemEntity over the m:1 relation they have, using the relation between the fields:
		/// OrderitemAlterationitemTag.OrderitemAlterationitemId - OrderitemAlterationitem.OrderitemAlterationitemId
		/// </summary>
		public virtual IEntityRelation OrderitemAlterationitemEntityUsingOrderitemAlterationitemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "OrderitemAlterationitemEntity", false);
				relation.AddEntityFieldPair(OrderitemAlterationitemFields.OrderitemAlterationitemId, OrderitemAlterationitemTagFields.OrderitemAlterationitemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderitemAlterationitemEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderitemAlterationitemTagEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OrderitemAlterationitemTagEntity and TagEntity over the m:1 relation they have, using the relation between the fields:
		/// OrderitemAlterationitemTag.TagId - Tag.TagId
		/// </summary>
		public virtual IEntityRelation TagEntityUsingTagId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TagEntity", false);
				relation.AddEntityFieldPair(TagFields.TagId, OrderitemAlterationitemTagFields.TagId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TagEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderitemAlterationitemTagEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticOrderitemAlterationitemTagRelations
	{
		internal static readonly IEntityRelation AlterationoptionEntityUsingAlterationoptionIdStatic = new OrderitemAlterationitemTagRelations().AlterationoptionEntityUsingAlterationoptionId;
		internal static readonly IEntityRelation OrderitemAlterationitemEntityUsingOrderitemAlterationitemIdStatic = new OrderitemAlterationitemTagRelations().OrderitemAlterationitemEntityUsingOrderitemAlterationitemId;
		internal static readonly IEntityRelation TagEntityUsingTagIdStatic = new OrderitemAlterationitemTagRelations().TagEntityUsingTagId;

		/// <summary>CTor</summary>
		static StaticOrderitemAlterationitemTagRelations()
		{
		}
	}
}
