﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Category. </summary>
	public partial class CategoryRelations
	{
		/// <summary>CTor</summary>
		public CategoryRelations()
		{
		}

		/// <summary>Gets all relations of the CategoryEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AdvertisementEntityUsingActionCategoryId);
			toReturn.Add(this.AdvertisementTagCategoryEntityUsingCategoryId);
			toReturn.Add(this.AnnouncementEntityUsingOnNoCategory);
			toReturn.Add(this.AnnouncementEntityUsingOnYesCategory);
			toReturn.Add(this.ActionEntityUsingCategoryId);
			toReturn.Add(this.AvailabilityEntityUsingActionCategoryId);
			toReturn.Add(this.CategoryEntityUsingParentCategoryId);
			toReturn.Add(this.CategoryAlterationEntityUsingCategoryId);
			toReturn.Add(this.CategoryLanguageEntityUsingCategoryId);
			toReturn.Add(this.CategorySuggestionEntityUsingCategoryId);
			toReturn.Add(this.CategoryTagEntityUsingCategoryId);
			toReturn.Add(this.CustomTextEntityUsingCategoryId);
			toReturn.Add(this.MediaEntityUsingActionCategoryId);
			toReturn.Add(this.MediaEntityUsingCategoryId);
			toReturn.Add(this.MessageEntityUsingCategoryId);
			toReturn.Add(this.MessageRecipientEntityUsingCategoryId);
			toReturn.Add(this.MessageTemplateEntityUsingCategoryId);
			toReturn.Add(this.OrderHourEntityUsingCategoryId);
			toReturn.Add(this.OrderitemEntityUsingCategoryId);
			toReturn.Add(this.ProductCategoryEntityUsingCategoryId);
			toReturn.Add(this.ProductCategoryTagEntityUsingCategoryId);
			toReturn.Add(this.ScheduledMessageEntityUsingCategoryId);
			toReturn.Add(this.UITabEntityUsingCategoryId);
			toReturn.Add(this.UIWidgetEntityUsingCategoryId);
			toReturn.Add(this.CategoryEntityUsingCategoryIdParentCategoryId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			toReturn.Add(this.GenericcategoryEntityUsingGenericcategoryId);
			toReturn.Add(this.MenuEntityUsingMenuId);
			toReturn.Add(this.PoscategoryEntityUsingPoscategoryId);
			toReturn.Add(this.ProductEntityUsingProductId);
			toReturn.Add(this.RouteEntityUsingRouteId);
			toReturn.Add(this.ScheduleEntityUsingScheduleId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between CategoryEntity and AdvertisementEntity over the 1:n relation they have, using the relation between the fields:
		/// Category.CategoryId - Advertisement.ActionCategoryId
		/// </summary>
		public virtual IEntityRelation AdvertisementEntityUsingActionCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AdvertisementCollection" , true);
				relation.AddEntityFieldPair(CategoryFields.CategoryId, AdvertisementFields.ActionCategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CategoryEntity and AdvertisementTagCategoryEntity over the 1:n relation they have, using the relation between the fields:
		/// Category.CategoryId - AdvertisementTagCategory.CategoryId
		/// </summary>
		public virtual IEntityRelation AdvertisementTagCategoryEntityUsingCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AdvertisementTagCategoryCollection" , true);
				relation.AddEntityFieldPair(CategoryFields.CategoryId, AdvertisementTagCategoryFields.CategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdvertisementTagCategoryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CategoryEntity and AnnouncementEntity over the 1:n relation they have, using the relation between the fields:
		/// Category.CategoryId - Announcement.OnNoCategory
		/// </summary>
		public virtual IEntityRelation AnnouncementEntityUsingOnNoCategory
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AnnouncementCollection_" , true);
				relation.AddEntityFieldPair(CategoryFields.CategoryId, AnnouncementFields.OnNoCategory);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AnnouncementEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CategoryEntity and AnnouncementEntity over the 1:n relation they have, using the relation between the fields:
		/// Category.CategoryId - Announcement.OnYesCategory
		/// </summary>
		public virtual IEntityRelation AnnouncementEntityUsingOnYesCategory
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AnnouncementCollection" , true);
				relation.AddEntityFieldPair(CategoryFields.CategoryId, AnnouncementFields.OnYesCategory);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AnnouncementEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CategoryEntity and ActionEntity over the 1:n relation they have, using the relation between the fields:
		/// Category.CategoryId - Action.CategoryId
		/// </summary>
		public virtual IEntityRelation ActionEntityUsingCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ActionCollection" , true);
				relation.AddEntityFieldPair(CategoryFields.CategoryId, ActionFields.CategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ActionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CategoryEntity and AvailabilityEntity over the 1:n relation they have, using the relation between the fields:
		/// Category.CategoryId - Availability.ActionCategoryId
		/// </summary>
		public virtual IEntityRelation AvailabilityEntityUsingActionCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AvailabilityCollection" , true);
				relation.AddEntityFieldPair(CategoryFields.CategoryId, AvailabilityFields.ActionCategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AvailabilityEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CategoryEntity and CategoryEntity over the 1:n relation they have, using the relation between the fields:
		/// Category.CategoryId - Category.ParentCategoryId
		/// </summary>
		public virtual IEntityRelation CategoryEntityUsingParentCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ChildCategoryCollection" , true);
				relation.AddEntityFieldPair(CategoryFields.CategoryId, CategoryFields.ParentCategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CategoryEntity and CategoryAlterationEntity over the 1:n relation they have, using the relation between the fields:
		/// Category.CategoryId - CategoryAlteration.CategoryId
		/// </summary>
		public virtual IEntityRelation CategoryAlterationEntityUsingCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CategoryAlterationCollection" , true);
				relation.AddEntityFieldPair(CategoryFields.CategoryId, CategoryAlterationFields.CategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryAlterationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CategoryEntity and CategoryLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Category.CategoryId - CategoryLanguage.CategoryId
		/// </summary>
		public virtual IEntityRelation CategoryLanguageEntityUsingCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CategoryLanguageCollection" , true);
				relation.AddEntityFieldPair(CategoryFields.CategoryId, CategoryLanguageFields.CategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CategoryEntity and CategorySuggestionEntity over the 1:n relation they have, using the relation between the fields:
		/// Category.CategoryId - CategorySuggestion.CategoryId
		/// </summary>
		public virtual IEntityRelation CategorySuggestionEntityUsingCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CategorySuggestionCollection" , true);
				relation.AddEntityFieldPair(CategoryFields.CategoryId, CategorySuggestionFields.CategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategorySuggestionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CategoryEntity and CategoryTagEntity over the 1:n relation they have, using the relation between the fields:
		/// Category.CategoryId - CategoryTag.CategoryId
		/// </summary>
		public virtual IEntityRelation CategoryTagEntityUsingCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CategoryTagCollection" , true);
				relation.AddEntityFieldPair(CategoryFields.CategoryId, CategoryTagFields.CategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryTagEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CategoryEntity and CustomTextEntity over the 1:n relation they have, using the relation between the fields:
		/// Category.CategoryId - CustomText.CategoryId
		/// </summary>
		public virtual IEntityRelation CustomTextEntityUsingCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomTextCollection" , true);
				relation.AddEntityFieldPair(CategoryFields.CategoryId, CustomTextFields.CategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CategoryEntity and MediaEntity over the 1:n relation they have, using the relation between the fields:
		/// Category.CategoryId - Media.ActionCategoryId
		/// </summary>
		public virtual IEntityRelation MediaEntityUsingActionCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ActionMediaCollection" , true);
				relation.AddEntityFieldPair(CategoryFields.CategoryId, MediaFields.ActionCategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CategoryEntity and MediaEntity over the 1:n relation they have, using the relation between the fields:
		/// Category.CategoryId - Media.CategoryId
		/// </summary>
		public virtual IEntityRelation MediaEntityUsingCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MediaCollection" , true);
				relation.AddEntityFieldPair(CategoryFields.CategoryId, MediaFields.CategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CategoryEntity and MessageEntity over the 1:n relation they have, using the relation between the fields:
		/// Category.CategoryId - Message.CategoryId
		/// </summary>
		public virtual IEntityRelation MessageEntityUsingCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MessageCollection" , true);
				relation.AddEntityFieldPair(CategoryFields.CategoryId, MessageFields.CategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CategoryEntity and MessageRecipientEntity over the 1:n relation they have, using the relation between the fields:
		/// Category.CategoryId - MessageRecipient.CategoryId
		/// </summary>
		public virtual IEntityRelation MessageRecipientEntityUsingCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MessageRecipientCollection" , true);
				relation.AddEntityFieldPair(CategoryFields.CategoryId, MessageRecipientFields.CategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageRecipientEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CategoryEntity and MessageTemplateEntity over the 1:n relation they have, using the relation between the fields:
		/// Category.CategoryId - MessageTemplate.CategoryId
		/// </summary>
		public virtual IEntityRelation MessageTemplateEntityUsingCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MessageTemplateCollection" , true);
				relation.AddEntityFieldPair(CategoryFields.CategoryId, MessageTemplateFields.CategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageTemplateEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CategoryEntity and OrderHourEntity over the 1:n relation they have, using the relation between the fields:
		/// Category.CategoryId - OrderHour.CategoryId
		/// </summary>
		public virtual IEntityRelation OrderHourEntityUsingCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrderHourCollection" , true);
				relation.AddEntityFieldPair(CategoryFields.CategoryId, OrderHourFields.CategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderHourEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CategoryEntity and OrderitemEntity over the 1:n relation they have, using the relation between the fields:
		/// Category.CategoryId - Orderitem.CategoryId
		/// </summary>
		public virtual IEntityRelation OrderitemEntityUsingCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrderitemCollection" , true);
				relation.AddEntityFieldPair(CategoryFields.CategoryId, OrderitemFields.CategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderitemEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CategoryEntity and ProductCategoryEntity over the 1:n relation they have, using the relation between the fields:
		/// Category.CategoryId - ProductCategory.CategoryId
		/// </summary>
		public virtual IEntityRelation ProductCategoryEntityUsingCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ProductCategoryCollection" , true);
				relation.AddEntityFieldPair(CategoryFields.CategoryId, ProductCategoryFields.CategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductCategoryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CategoryEntity and ProductCategoryTagEntity over the 1:n relation they have, using the relation between the fields:
		/// Category.CategoryId - ProductCategoryTag.CategoryId
		/// </summary>
		public virtual IEntityRelation ProductCategoryTagEntityUsingCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ProductCategoryTagCollection" , true);
				relation.AddEntityFieldPair(CategoryFields.CategoryId, ProductCategoryTagFields.CategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductCategoryTagEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CategoryEntity and ScheduledMessageEntity over the 1:n relation they have, using the relation between the fields:
		/// Category.CategoryId - ScheduledMessage.CategoryId
		/// </summary>
		public virtual IEntityRelation ScheduledMessageEntityUsingCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ScheduledMessageCollection" , true);
				relation.AddEntityFieldPair(CategoryFields.CategoryId, ScheduledMessageFields.CategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduledMessageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CategoryEntity and UITabEntity over the 1:n relation they have, using the relation between the fields:
		/// Category.CategoryId - UITab.CategoryId
		/// </summary>
		public virtual IEntityRelation UITabEntityUsingCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UITabCollection" , true);
				relation.AddEntityFieldPair(CategoryFields.CategoryId, UITabFields.CategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UITabEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CategoryEntity and UIWidgetEntity over the 1:n relation they have, using the relation between the fields:
		/// Category.CategoryId - UIWidget.CategoryId
		/// </summary>
		public virtual IEntityRelation UIWidgetEntityUsingCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UIWidgetCollection" , true);
				relation.AddEntityFieldPair(CategoryFields.CategoryId, UIWidgetFields.CategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIWidgetEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between CategoryEntity and CategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// Category.ParentCategoryId - Category.CategoryId
		/// </summary>
		public virtual IEntityRelation CategoryEntityUsingCategoryIdParentCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ParentCategoryEntity", false);
				relation.AddEntityFieldPair(CategoryFields.CategoryId, CategoryFields.ParentCategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CategoryEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// Category.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, CategoryFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CategoryEntity and GenericcategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// Category.GenericcategoryId - Genericcategory.GenericcategoryId
		/// </summary>
		public virtual IEntityRelation GenericcategoryEntityUsingGenericcategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "GenericcategoryEntity", false);
				relation.AddEntityFieldPair(GenericcategoryFields.GenericcategoryId, CategoryFields.GenericcategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericcategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CategoryEntity and MenuEntity over the m:1 relation they have, using the relation between the fields:
		/// Category.MenuId - Menu.MenuId
		/// </summary>
		public virtual IEntityRelation MenuEntityUsingMenuId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "MenuEntity", false);
				relation.AddEntityFieldPair(MenuFields.MenuId, CategoryFields.MenuId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MenuEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CategoryEntity and PoscategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// Category.PoscategoryId - Poscategory.PoscategoryId
		/// </summary>
		public virtual IEntityRelation PoscategoryEntityUsingPoscategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PoscategoryEntity", false);
				relation.AddEntityFieldPair(PoscategoryFields.PoscategoryId, CategoryFields.PoscategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PoscategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CategoryEntity and ProductEntity over the m:1 relation they have, using the relation between the fields:
		/// Category.ProductId - Product.ProductId
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ProductEntity", false);
				relation.AddEntityFieldPair(ProductFields.ProductId, CategoryFields.ProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CategoryEntity and RouteEntity over the m:1 relation they have, using the relation between the fields:
		/// Category.RouteId - Route.RouteId
		/// </summary>
		public virtual IEntityRelation RouteEntityUsingRouteId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RouteEntity", false);
				relation.AddEntityFieldPair(RouteFields.RouteId, CategoryFields.RouteId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RouteEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CategoryEntity and ScheduleEntity over the m:1 relation they have, using the relation between the fields:
		/// Category.ScheduleId - Schedule.ScheduleId
		/// </summary>
		public virtual IEntityRelation ScheduleEntityUsingScheduleId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ScheduleEntity", false);
				relation.AddEntityFieldPair(ScheduleFields.ScheduleId, CategoryFields.ScheduleId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ScheduleEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCategoryRelations
	{
		internal static readonly IEntityRelation AdvertisementEntityUsingActionCategoryIdStatic = new CategoryRelations().AdvertisementEntityUsingActionCategoryId;
		internal static readonly IEntityRelation AdvertisementTagCategoryEntityUsingCategoryIdStatic = new CategoryRelations().AdvertisementTagCategoryEntityUsingCategoryId;
		internal static readonly IEntityRelation AnnouncementEntityUsingOnNoCategoryStatic = new CategoryRelations().AnnouncementEntityUsingOnNoCategory;
		internal static readonly IEntityRelation AnnouncementEntityUsingOnYesCategoryStatic = new CategoryRelations().AnnouncementEntityUsingOnYesCategory;
		internal static readonly IEntityRelation ActionEntityUsingCategoryIdStatic = new CategoryRelations().ActionEntityUsingCategoryId;
		internal static readonly IEntityRelation AvailabilityEntityUsingActionCategoryIdStatic = new CategoryRelations().AvailabilityEntityUsingActionCategoryId;
		internal static readonly IEntityRelation CategoryEntityUsingParentCategoryIdStatic = new CategoryRelations().CategoryEntityUsingParentCategoryId;
		internal static readonly IEntityRelation CategoryAlterationEntityUsingCategoryIdStatic = new CategoryRelations().CategoryAlterationEntityUsingCategoryId;
		internal static readonly IEntityRelation CategoryLanguageEntityUsingCategoryIdStatic = new CategoryRelations().CategoryLanguageEntityUsingCategoryId;
		internal static readonly IEntityRelation CategorySuggestionEntityUsingCategoryIdStatic = new CategoryRelations().CategorySuggestionEntityUsingCategoryId;
		internal static readonly IEntityRelation CategoryTagEntityUsingCategoryIdStatic = new CategoryRelations().CategoryTagEntityUsingCategoryId;
		internal static readonly IEntityRelation CustomTextEntityUsingCategoryIdStatic = new CategoryRelations().CustomTextEntityUsingCategoryId;
		internal static readonly IEntityRelation MediaEntityUsingActionCategoryIdStatic = new CategoryRelations().MediaEntityUsingActionCategoryId;
		internal static readonly IEntityRelation MediaEntityUsingCategoryIdStatic = new CategoryRelations().MediaEntityUsingCategoryId;
		internal static readonly IEntityRelation MessageEntityUsingCategoryIdStatic = new CategoryRelations().MessageEntityUsingCategoryId;
		internal static readonly IEntityRelation MessageRecipientEntityUsingCategoryIdStatic = new CategoryRelations().MessageRecipientEntityUsingCategoryId;
		internal static readonly IEntityRelation MessageTemplateEntityUsingCategoryIdStatic = new CategoryRelations().MessageTemplateEntityUsingCategoryId;
		internal static readonly IEntityRelation OrderHourEntityUsingCategoryIdStatic = new CategoryRelations().OrderHourEntityUsingCategoryId;
		internal static readonly IEntityRelation OrderitemEntityUsingCategoryIdStatic = new CategoryRelations().OrderitemEntityUsingCategoryId;
		internal static readonly IEntityRelation ProductCategoryEntityUsingCategoryIdStatic = new CategoryRelations().ProductCategoryEntityUsingCategoryId;
		internal static readonly IEntityRelation ProductCategoryTagEntityUsingCategoryIdStatic = new CategoryRelations().ProductCategoryTagEntityUsingCategoryId;
		internal static readonly IEntityRelation ScheduledMessageEntityUsingCategoryIdStatic = new CategoryRelations().ScheduledMessageEntityUsingCategoryId;
		internal static readonly IEntityRelation UITabEntityUsingCategoryIdStatic = new CategoryRelations().UITabEntityUsingCategoryId;
		internal static readonly IEntityRelation UIWidgetEntityUsingCategoryIdStatic = new CategoryRelations().UIWidgetEntityUsingCategoryId;
		internal static readonly IEntityRelation CategoryEntityUsingCategoryIdParentCategoryIdStatic = new CategoryRelations().CategoryEntityUsingCategoryIdParentCategoryId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new CategoryRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation GenericcategoryEntityUsingGenericcategoryIdStatic = new CategoryRelations().GenericcategoryEntityUsingGenericcategoryId;
		internal static readonly IEntityRelation MenuEntityUsingMenuIdStatic = new CategoryRelations().MenuEntityUsingMenuId;
		internal static readonly IEntityRelation PoscategoryEntityUsingPoscategoryIdStatic = new CategoryRelations().PoscategoryEntityUsingPoscategoryId;
		internal static readonly IEntityRelation ProductEntityUsingProductIdStatic = new CategoryRelations().ProductEntityUsingProductId;
		internal static readonly IEntityRelation RouteEntityUsingRouteIdStatic = new CategoryRelations().RouteEntityUsingRouteId;
		internal static readonly IEntityRelation ScheduleEntityUsingScheduleIdStatic = new CategoryRelations().ScheduleEntityUsingScheduleId;

		/// <summary>CTor</summary>
		static StaticCategoryRelations()
		{
		}
	}
}
