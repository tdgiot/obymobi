﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: RoomControlComponentLanguage. </summary>
	public partial class RoomControlComponentLanguageRelations
	{
		/// <summary>CTor</summary>
		public RoomControlComponentLanguageRelations()
		{
		}

		/// <summary>Gets all relations of the RoomControlComponentLanguageEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.LanguageEntityUsingLanguageId);
			toReturn.Add(this.RoomControlComponentEntityUsingRoomControlComponentId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between RoomControlComponentLanguageEntity and LanguageEntity over the m:1 relation they have, using the relation between the fields:
		/// RoomControlComponentLanguage.LanguageId - Language.LanguageId
		/// </summary>
		public virtual IEntityRelation LanguageEntityUsingLanguageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "LanguageEntity", false);
				relation.AddEntityFieldPair(LanguageFields.LanguageId, RoomControlComponentLanguageFields.LanguageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlComponentLanguageEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RoomControlComponentLanguageEntity and RoomControlComponentEntity over the m:1 relation they have, using the relation between the fields:
		/// RoomControlComponentLanguage.RoomControlComponentId - RoomControlComponent.RoomControlComponentId
		/// </summary>
		public virtual IEntityRelation RoomControlComponentEntityUsingRoomControlComponentId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RoomControlComponentEntity", false);
				relation.AddEntityFieldPair(RoomControlComponentFields.RoomControlComponentId, RoomControlComponentLanguageFields.RoomControlComponentId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlComponentEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlComponentLanguageEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticRoomControlComponentLanguageRelations
	{
		internal static readonly IEntityRelation LanguageEntityUsingLanguageIdStatic = new RoomControlComponentLanguageRelations().LanguageEntityUsingLanguageId;
		internal static readonly IEntityRelation RoomControlComponentEntityUsingRoomControlComponentIdStatic = new RoomControlComponentLanguageRelations().RoomControlComponentEntityUsingRoomControlComponentId;

		/// <summary>CTor</summary>
		static StaticRoomControlComponentLanguageRelations()
		{
		}
	}
}
