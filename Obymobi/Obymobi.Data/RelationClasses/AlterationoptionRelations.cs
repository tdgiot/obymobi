﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Alterationoption. </summary>
	public partial class AlterationoptionRelations
	{
		/// <summary>CTor</summary>
		public AlterationoptionRelations()
		{
		}

		/// <summary>Gets all relations of the AlterationoptionEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AlterationitemEntityUsingAlterationoptionId);
			toReturn.Add(this.AlterationoptionLanguageEntityUsingAlterationoptionId);
			toReturn.Add(this.AlterationoptionTagEntityUsingAlterationoptionId);
			toReturn.Add(this.CustomTextEntityUsingAlterationoptionId);
			toReturn.Add(this.MediaEntityUsingAlterationoptionId);
			toReturn.Add(this.OrderitemAlterationitemTagEntityUsingAlterationoptionId);
			toReturn.Add(this.PriceLevelItemEntityUsingAlterationoptionId);
			toReturn.Add(this.BrandEntityUsingBrandId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			toReturn.Add(this.ExternalProductEntityUsingExternalProductId);
			toReturn.Add(this.GenericalterationoptionEntityUsingGenericalterationoptionId);
			toReturn.Add(this.PosalterationoptionEntityUsingPosalterationoptionId);
			toReturn.Add(this.PosproductEntityUsingPosproductId);
			toReturn.Add(this.ProductEntityUsingProductId);
			toReturn.Add(this.TaxTariffEntityUsingTaxTariffId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between AlterationoptionEntity and AlterationitemEntity over the 1:n relation they have, using the relation between the fields:
		/// Alterationoption.AlterationoptionId - Alterationitem.AlterationoptionId
		/// </summary>
		public virtual IEntityRelation AlterationitemEntityUsingAlterationoptionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AlterationitemCollection" , true);
				relation.AddEntityFieldPair(AlterationoptionFields.AlterationoptionId, AlterationitemFields.AlterationoptionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationoptionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationitemEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AlterationoptionEntity and AlterationoptionLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// Alterationoption.AlterationoptionId - AlterationoptionLanguage.AlterationoptionId
		/// </summary>
		public virtual IEntityRelation AlterationoptionLanguageEntityUsingAlterationoptionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AlterationoptionLanguageCollection" , true);
				relation.AddEntityFieldPair(AlterationoptionFields.AlterationoptionId, AlterationoptionLanguageFields.AlterationoptionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationoptionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationoptionLanguageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AlterationoptionEntity and AlterationoptionTagEntity over the 1:n relation they have, using the relation between the fields:
		/// Alterationoption.AlterationoptionId - AlterationoptionTag.AlterationoptionId
		/// </summary>
		public virtual IEntityRelation AlterationoptionTagEntityUsingAlterationoptionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AlterationoptionTagCollection" , true);
				relation.AddEntityFieldPair(AlterationoptionFields.AlterationoptionId, AlterationoptionTagFields.AlterationoptionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationoptionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationoptionTagEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AlterationoptionEntity and CustomTextEntity over the 1:n relation they have, using the relation between the fields:
		/// Alterationoption.AlterationoptionId - CustomText.AlterationoptionId
		/// </summary>
		public virtual IEntityRelation CustomTextEntityUsingAlterationoptionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomTextCollection" , true);
				relation.AddEntityFieldPair(AlterationoptionFields.AlterationoptionId, CustomTextFields.AlterationoptionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationoptionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AlterationoptionEntity and MediaEntity over the 1:n relation they have, using the relation between the fields:
		/// Alterationoption.AlterationoptionId - Media.AlterationoptionId
		/// </summary>
		public virtual IEntityRelation MediaEntityUsingAlterationoptionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MediaCollection" , true);
				relation.AddEntityFieldPair(AlterationoptionFields.AlterationoptionId, MediaFields.AlterationoptionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationoptionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AlterationoptionEntity and OrderitemAlterationitemTagEntity over the 1:n relation they have, using the relation between the fields:
		/// Alterationoption.AlterationoptionId - OrderitemAlterationitemTag.AlterationoptionId
		/// </summary>
		public virtual IEntityRelation OrderitemAlterationitemTagEntityUsingAlterationoptionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrderitemAlterationitemTagCollection" , true);
				relation.AddEntityFieldPair(AlterationoptionFields.AlterationoptionId, OrderitemAlterationitemTagFields.AlterationoptionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationoptionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderitemAlterationitemTagEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AlterationoptionEntity and PriceLevelItemEntity over the 1:n relation they have, using the relation between the fields:
		/// Alterationoption.AlterationoptionId - PriceLevelItem.AlterationoptionId
		/// </summary>
		public virtual IEntityRelation PriceLevelItemEntityUsingAlterationoptionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PriceLevelItemCollection" , true);
				relation.AddEntityFieldPair(AlterationoptionFields.AlterationoptionId, PriceLevelItemFields.AlterationoptionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationoptionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PriceLevelItemEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between AlterationoptionEntity and BrandEntity over the m:1 relation they have, using the relation between the fields:
		/// Alterationoption.BrandId - Brand.BrandId
		/// </summary>
		public virtual IEntityRelation BrandEntityUsingBrandId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "BrandEntity", false);
				relation.AddEntityFieldPair(BrandFields.BrandId, AlterationoptionFields.BrandId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BrandEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationoptionEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AlterationoptionEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// Alterationoption.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, AlterationoptionFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationoptionEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AlterationoptionEntity and ExternalProductEntity over the m:1 relation they have, using the relation between the fields:
		/// Alterationoption.ExternalProductId - ExternalProduct.ExternalProductId
		/// </summary>
		public virtual IEntityRelation ExternalProductEntityUsingExternalProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ExternalProductEntity", false);
				relation.AddEntityFieldPair(ExternalProductFields.ExternalProductId, AlterationoptionFields.ExternalProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalProductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationoptionEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AlterationoptionEntity and GenericalterationoptionEntity over the m:1 relation they have, using the relation between the fields:
		/// Alterationoption.GenericalterationoptionId - Genericalterationoption.GenericalterationoptionId
		/// </summary>
		public virtual IEntityRelation GenericalterationoptionEntityUsingGenericalterationoptionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "GenericalterationoptionEntity", false);
				relation.AddEntityFieldPair(GenericalterationoptionFields.GenericalterationoptionId, AlterationoptionFields.GenericalterationoptionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GenericalterationoptionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationoptionEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AlterationoptionEntity and PosalterationoptionEntity over the m:1 relation they have, using the relation between the fields:
		/// Alterationoption.PosalterationoptionId - Posalterationoption.PosalterationoptionId
		/// </summary>
		public virtual IEntityRelation PosalterationoptionEntityUsingPosalterationoptionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PosalterationoptionEntity", false);
				relation.AddEntityFieldPair(PosalterationoptionFields.PosalterationoptionId, AlterationoptionFields.PosalterationoptionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PosalterationoptionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationoptionEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AlterationoptionEntity and PosproductEntity over the m:1 relation they have, using the relation between the fields:
		/// Alterationoption.PosproductId - Posproduct.PosproductId
		/// </summary>
		public virtual IEntityRelation PosproductEntityUsingPosproductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PosproductEntity", false);
				relation.AddEntityFieldPair(PosproductFields.PosproductId, AlterationoptionFields.PosproductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PosproductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationoptionEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AlterationoptionEntity and ProductEntity over the m:1 relation they have, using the relation between the fields:
		/// Alterationoption.ProductId - Product.ProductId
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingProductId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ProductEntity", false);
				relation.AddEntityFieldPair(ProductFields.ProductId, AlterationoptionFields.ProductId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationoptionEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AlterationoptionEntity and TaxTariffEntity over the m:1 relation they have, using the relation between the fields:
		/// Alterationoption.TaxTariffId - TaxTariff.TaxTariffId
		/// </summary>
		public virtual IEntityRelation TaxTariffEntityUsingTaxTariffId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TaxTariffEntity", false);
				relation.AddEntityFieldPair(TaxTariffFields.TaxTariffId, AlterationoptionFields.TaxTariffId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TaxTariffEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AlterationoptionEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticAlterationoptionRelations
	{
		internal static readonly IEntityRelation AlterationitemEntityUsingAlterationoptionIdStatic = new AlterationoptionRelations().AlterationitemEntityUsingAlterationoptionId;
		internal static readonly IEntityRelation AlterationoptionLanguageEntityUsingAlterationoptionIdStatic = new AlterationoptionRelations().AlterationoptionLanguageEntityUsingAlterationoptionId;
		internal static readonly IEntityRelation AlterationoptionTagEntityUsingAlterationoptionIdStatic = new AlterationoptionRelations().AlterationoptionTagEntityUsingAlterationoptionId;
		internal static readonly IEntityRelation CustomTextEntityUsingAlterationoptionIdStatic = new AlterationoptionRelations().CustomTextEntityUsingAlterationoptionId;
		internal static readonly IEntityRelation MediaEntityUsingAlterationoptionIdStatic = new AlterationoptionRelations().MediaEntityUsingAlterationoptionId;
		internal static readonly IEntityRelation OrderitemAlterationitemTagEntityUsingAlterationoptionIdStatic = new AlterationoptionRelations().OrderitemAlterationitemTagEntityUsingAlterationoptionId;
		internal static readonly IEntityRelation PriceLevelItemEntityUsingAlterationoptionIdStatic = new AlterationoptionRelations().PriceLevelItemEntityUsingAlterationoptionId;
		internal static readonly IEntityRelation BrandEntityUsingBrandIdStatic = new AlterationoptionRelations().BrandEntityUsingBrandId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new AlterationoptionRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation ExternalProductEntityUsingExternalProductIdStatic = new AlterationoptionRelations().ExternalProductEntityUsingExternalProductId;
		internal static readonly IEntityRelation GenericalterationoptionEntityUsingGenericalterationoptionIdStatic = new AlterationoptionRelations().GenericalterationoptionEntityUsingGenericalterationoptionId;
		internal static readonly IEntityRelation PosalterationoptionEntityUsingPosalterationoptionIdStatic = new AlterationoptionRelations().PosalterationoptionEntityUsingPosalterationoptionId;
		internal static readonly IEntityRelation PosproductEntityUsingPosproductIdStatic = new AlterationoptionRelations().PosproductEntityUsingPosproductId;
		internal static readonly IEntityRelation ProductEntityUsingProductIdStatic = new AlterationoptionRelations().ProductEntityUsingProductId;
		internal static readonly IEntityRelation TaxTariffEntityUsingTaxTariffIdStatic = new AlterationoptionRelations().TaxTariffEntityUsingTaxTariffId;

		/// <summary>CTor</summary>
		static StaticAlterationoptionRelations()
		{
		}
	}
}
