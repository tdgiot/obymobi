﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: LandingPage. </summary>
	public partial class LandingPageRelations
	{
		/// <summary>CTor</summary>
		public LandingPageRelations()
		{
		}

		/// <summary>Gets all relations of the LandingPageEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ActionEntityUsingLandingPageId);
			toReturn.Add(this.ApplicationConfigurationEntityUsingLandingPageId);
			toReturn.Add(this.LandingPageWidgetEntityUsingLandingPageId);
			toReturn.Add(this.CustomTextEntityUsingLandingPageId);
			toReturn.Add(this.MediaEntityUsingLandingPageId);
			toReturn.Add(this.ApplicationConfigurationEntityUsingApplicationConfigurationId);
			toReturn.Add(this.NavigationMenuEntityUsingNavigationMenuId);
			toReturn.Add(this.ThemeEntityUsingThemeId);
			toReturn.Add(this.OutletEntityUsingOutletId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between LandingPageEntity and ActionEntity over the 1:n relation they have, using the relation between the fields:
		/// LandingPage.LandingPageId - Action.LandingPageId
		/// </summary>
		public virtual IEntityRelation ActionEntityUsingLandingPageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ActionCollection" , true);
				relation.AddEntityFieldPair(LandingPageFields.LandingPageId, ActionFields.LandingPageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LandingPageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ActionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LandingPageEntity and ApplicationConfigurationEntity over the 1:n relation they have, using the relation between the fields:
		/// LandingPage.LandingPageId - ApplicationConfiguration.LandingPageId
		/// </summary>
		public virtual IEntityRelation ApplicationConfigurationEntityUsingLandingPageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DefaultApplicationConfigurationCollection" , true);
				relation.AddEntityFieldPair(LandingPageFields.LandingPageId, ApplicationConfigurationFields.LandingPageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LandingPageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ApplicationConfigurationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LandingPageEntity and LandingPageWidgetEntity over the 1:n relation they have, using the relation between the fields:
		/// LandingPage.LandingPageId - LandingPageWidget.LandingPageId
		/// </summary>
		public virtual IEntityRelation LandingPageWidgetEntityUsingLandingPageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "LandingPageWidgetCollection" , true);
				relation.AddEntityFieldPair(LandingPageFields.LandingPageId, LandingPageWidgetFields.LandingPageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LandingPageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LandingPageWidgetEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LandingPageEntity and CustomTextEntity over the 1:n relation they have, using the relation between the fields:
		/// LandingPage.LandingPageId - CustomText.LandingPageId
		/// </summary>
		public virtual IEntityRelation CustomTextEntityUsingLandingPageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomTextCollection" , true);
				relation.AddEntityFieldPair(LandingPageFields.LandingPageId, CustomTextFields.LandingPageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LandingPageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LandingPageEntity and MediaEntity over the 1:n relation they have, using the relation between the fields:
		/// LandingPage.LandingPageId - Media.LandingPageId
		/// </summary>
		public virtual IEntityRelation MediaEntityUsingLandingPageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MediaCollection" , true);
				relation.AddEntityFieldPair(LandingPageFields.LandingPageId, MediaFields.LandingPageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LandingPageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MediaEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between LandingPageEntity and ApplicationConfigurationEntity over the m:1 relation they have, using the relation between the fields:
		/// LandingPage.ApplicationConfigurationId - ApplicationConfiguration.ApplicationConfigurationId
		/// </summary>
		public virtual IEntityRelation ApplicationConfigurationEntityUsingApplicationConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ApplicationConfigurationEntity", false);
				relation.AddEntityFieldPair(ApplicationConfigurationFields.ApplicationConfigurationId, LandingPageFields.ApplicationConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ApplicationConfigurationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LandingPageEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between LandingPageEntity and NavigationMenuEntity over the m:1 relation they have, using the relation between the fields:
		/// LandingPage.NavigationMenuId - NavigationMenu.NavigationMenuId
		/// </summary>
		public virtual IEntityRelation NavigationMenuEntityUsingNavigationMenuId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "NavigationMenuEntity", false);
				relation.AddEntityFieldPair(NavigationMenuFields.NavigationMenuId, LandingPageFields.NavigationMenuId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NavigationMenuEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LandingPageEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between LandingPageEntity and ThemeEntity over the m:1 relation they have, using the relation between the fields:
		/// LandingPage.ThemeId - Theme.ThemeId
		/// </summary>
		public virtual IEntityRelation ThemeEntityUsingThemeId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ThemeEntity", false);
				relation.AddEntityFieldPair(ThemeFields.ThemeId, LandingPageFields.ThemeId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ThemeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LandingPageEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between LandingPageEntity and OutletEntity over the m:1 relation they have, using the relation between the fields:
		/// LandingPage.OutletId - Outlet.OutletId
		/// </summary>
		public virtual IEntityRelation OutletEntityUsingOutletId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "OutletEntity", false);
				relation.AddEntityFieldPair(OutletFields.OutletId, LandingPageFields.OutletId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OutletEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LandingPageEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticLandingPageRelations
	{
		internal static readonly IEntityRelation ActionEntityUsingLandingPageIdStatic = new LandingPageRelations().ActionEntityUsingLandingPageId;
		internal static readonly IEntityRelation ApplicationConfigurationEntityUsingLandingPageIdStatic = new LandingPageRelations().ApplicationConfigurationEntityUsingLandingPageId;
		internal static readonly IEntityRelation LandingPageWidgetEntityUsingLandingPageIdStatic = new LandingPageRelations().LandingPageWidgetEntityUsingLandingPageId;
		internal static readonly IEntityRelation CustomTextEntityUsingLandingPageIdStatic = new LandingPageRelations().CustomTextEntityUsingLandingPageId;
		internal static readonly IEntityRelation MediaEntityUsingLandingPageIdStatic = new LandingPageRelations().MediaEntityUsingLandingPageId;
		internal static readonly IEntityRelation ApplicationConfigurationEntityUsingApplicationConfigurationIdStatic = new LandingPageRelations().ApplicationConfigurationEntityUsingApplicationConfigurationId;
		internal static readonly IEntityRelation NavigationMenuEntityUsingNavigationMenuIdStatic = new LandingPageRelations().NavigationMenuEntityUsingNavigationMenuId;
		internal static readonly IEntityRelation ThemeEntityUsingThemeIdStatic = new LandingPageRelations().ThemeEntityUsingThemeId;
		internal static readonly IEntityRelation OutletEntityUsingOutletIdStatic = new LandingPageRelations().OutletEntityUsingOutletId;

		/// <summary>CTor</summary>
		static StaticLandingPageRelations()
		{
		}
	}
}
