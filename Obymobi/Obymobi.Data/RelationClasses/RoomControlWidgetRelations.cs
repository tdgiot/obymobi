﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: RoomControlWidget. </summary>
	public partial class RoomControlWidgetRelations
	{
		/// <summary>CTor</summary>
		public RoomControlWidgetRelations()
		{
		}

		/// <summary>Gets all relations of the RoomControlWidgetEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CustomTextEntityUsingRoomControlWidgetId);
			toReturn.Add(this.RoomControlWidgetLanguageEntityUsingRoomControlWidgetId);
			toReturn.Add(this.InfraredConfigurationEntityUsingInfraredConfigurationId);
			toReturn.Add(this.RoomControlComponentEntityUsingRoomControlComponentId1);
			toReturn.Add(this.RoomControlComponentEntityUsingRoomControlComponentId10);
			toReturn.Add(this.RoomControlComponentEntityUsingRoomControlComponentId2);
			toReturn.Add(this.RoomControlComponentEntityUsingRoomControlComponentId3);
			toReturn.Add(this.RoomControlComponentEntityUsingRoomControlComponentId4);
			toReturn.Add(this.RoomControlComponentEntityUsingRoomControlComponentId5);
			toReturn.Add(this.RoomControlComponentEntityUsingRoomControlComponentId6);
			toReturn.Add(this.RoomControlComponentEntityUsingRoomControlComponentId7);
			toReturn.Add(this.RoomControlComponentEntityUsingRoomControlComponentId8);
			toReturn.Add(this.RoomControlComponentEntityUsingRoomControlComponentId9);
			toReturn.Add(this.RoomControlSectionEntityUsingRoomControlSectionId);
			toReturn.Add(this.RoomControlSectionItemEntityUsingRoomControlSectionItemId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between RoomControlWidgetEntity and CustomTextEntity over the 1:n relation they have, using the relation between the fields:
		/// RoomControlWidget.RoomControlWidgetId - CustomText.RoomControlWidgetId
		/// </summary>
		public virtual IEntityRelation CustomTextEntityUsingRoomControlWidgetId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomTextCollection" , true);
				relation.AddEntityFieldPair(RoomControlWidgetFields.RoomControlWidgetId, CustomTextFields.RoomControlWidgetId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlWidgetEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RoomControlWidgetEntity and RoomControlWidgetLanguageEntity over the 1:n relation they have, using the relation between the fields:
		/// RoomControlWidget.RoomControlWidgetId - RoomControlWidgetLanguage.RoomControlWidgetId
		/// </summary>
		public virtual IEntityRelation RoomControlWidgetLanguageEntityUsingRoomControlWidgetId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RoomControlWidgetLanguageCollection" , true);
				relation.AddEntityFieldPair(RoomControlWidgetFields.RoomControlWidgetId, RoomControlWidgetLanguageFields.RoomControlWidgetId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlWidgetEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlWidgetLanguageEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between RoomControlWidgetEntity and InfraredConfigurationEntity over the m:1 relation they have, using the relation between the fields:
		/// RoomControlWidget.InfraredConfigurationId - InfraredConfiguration.InfraredConfigurationId
		/// </summary>
		public virtual IEntityRelation InfraredConfigurationEntityUsingInfraredConfigurationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "InfraredConfigurationEntity", false);
				relation.AddEntityFieldPair(InfraredConfigurationFields.InfraredConfigurationId, RoomControlWidgetFields.InfraredConfigurationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InfraredConfigurationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlWidgetEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RoomControlWidgetEntity and RoomControlComponentEntity over the m:1 relation they have, using the relation between the fields:
		/// RoomControlWidget.RoomControlComponentId1 - RoomControlComponent.RoomControlComponentId
		/// </summary>
		public virtual IEntityRelation RoomControlComponentEntityUsingRoomControlComponentId1
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RoomControlComponentEntity", false);
				relation.AddEntityFieldPair(RoomControlComponentFields.RoomControlComponentId, RoomControlWidgetFields.RoomControlComponentId1);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlComponentEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlWidgetEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RoomControlWidgetEntity and RoomControlComponentEntity over the m:1 relation they have, using the relation between the fields:
		/// RoomControlWidget.RoomControlComponentId10 - RoomControlComponent.RoomControlComponentId
		/// </summary>
		public virtual IEntityRelation RoomControlComponentEntityUsingRoomControlComponentId10
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RoomControlComponentEntity_", false);
				relation.AddEntityFieldPair(RoomControlComponentFields.RoomControlComponentId, RoomControlWidgetFields.RoomControlComponentId10);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlComponentEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlWidgetEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RoomControlWidgetEntity and RoomControlComponentEntity over the m:1 relation they have, using the relation between the fields:
		/// RoomControlWidget.RoomControlComponentId2 - RoomControlComponent.RoomControlComponentId
		/// </summary>
		public virtual IEntityRelation RoomControlComponentEntityUsingRoomControlComponentId2
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RoomControlComponentEntity__", false);
				relation.AddEntityFieldPair(RoomControlComponentFields.RoomControlComponentId, RoomControlWidgetFields.RoomControlComponentId2);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlComponentEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlWidgetEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RoomControlWidgetEntity and RoomControlComponentEntity over the m:1 relation they have, using the relation between the fields:
		/// RoomControlWidget.RoomControlComponentId3 - RoomControlComponent.RoomControlComponentId
		/// </summary>
		public virtual IEntityRelation RoomControlComponentEntityUsingRoomControlComponentId3
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RoomControlComponentEntity___", false);
				relation.AddEntityFieldPair(RoomControlComponentFields.RoomControlComponentId, RoomControlWidgetFields.RoomControlComponentId3);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlComponentEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlWidgetEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RoomControlWidgetEntity and RoomControlComponentEntity over the m:1 relation they have, using the relation between the fields:
		/// RoomControlWidget.RoomControlComponentId4 - RoomControlComponent.RoomControlComponentId
		/// </summary>
		public virtual IEntityRelation RoomControlComponentEntityUsingRoomControlComponentId4
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RoomControlComponentEntity____", false);
				relation.AddEntityFieldPair(RoomControlComponentFields.RoomControlComponentId, RoomControlWidgetFields.RoomControlComponentId4);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlComponentEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlWidgetEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RoomControlWidgetEntity and RoomControlComponentEntity over the m:1 relation they have, using the relation between the fields:
		/// RoomControlWidget.RoomControlComponentId5 - RoomControlComponent.RoomControlComponentId
		/// </summary>
		public virtual IEntityRelation RoomControlComponentEntityUsingRoomControlComponentId5
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RoomControlComponentEntity_____", false);
				relation.AddEntityFieldPair(RoomControlComponentFields.RoomControlComponentId, RoomControlWidgetFields.RoomControlComponentId5);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlComponentEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlWidgetEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RoomControlWidgetEntity and RoomControlComponentEntity over the m:1 relation they have, using the relation between the fields:
		/// RoomControlWidget.RoomControlComponentId6 - RoomControlComponent.RoomControlComponentId
		/// </summary>
		public virtual IEntityRelation RoomControlComponentEntityUsingRoomControlComponentId6
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RoomControlComponentEntity______", false);
				relation.AddEntityFieldPair(RoomControlComponentFields.RoomControlComponentId, RoomControlWidgetFields.RoomControlComponentId6);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlComponentEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlWidgetEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RoomControlWidgetEntity and RoomControlComponentEntity over the m:1 relation they have, using the relation between the fields:
		/// RoomControlWidget.RoomControlComponentId7 - RoomControlComponent.RoomControlComponentId
		/// </summary>
		public virtual IEntityRelation RoomControlComponentEntityUsingRoomControlComponentId7
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RoomControlComponentEntity_______", false);
				relation.AddEntityFieldPair(RoomControlComponentFields.RoomControlComponentId, RoomControlWidgetFields.RoomControlComponentId7);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlComponentEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlWidgetEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RoomControlWidgetEntity and RoomControlComponentEntity over the m:1 relation they have, using the relation between the fields:
		/// RoomControlWidget.RoomControlComponentId8 - RoomControlComponent.RoomControlComponentId
		/// </summary>
		public virtual IEntityRelation RoomControlComponentEntityUsingRoomControlComponentId8
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RoomControlComponentEntity________", false);
				relation.AddEntityFieldPair(RoomControlComponentFields.RoomControlComponentId, RoomControlWidgetFields.RoomControlComponentId8);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlComponentEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlWidgetEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RoomControlWidgetEntity and RoomControlComponentEntity over the m:1 relation they have, using the relation between the fields:
		/// RoomControlWidget.RoomControlComponentId9 - RoomControlComponent.RoomControlComponentId
		/// </summary>
		public virtual IEntityRelation RoomControlComponentEntityUsingRoomControlComponentId9
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RoomControlComponentEntity_________", false);
				relation.AddEntityFieldPair(RoomControlComponentFields.RoomControlComponentId, RoomControlWidgetFields.RoomControlComponentId9);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlComponentEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlWidgetEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RoomControlWidgetEntity and RoomControlSectionEntity over the m:1 relation they have, using the relation between the fields:
		/// RoomControlWidget.RoomControlSectionId - RoomControlSection.RoomControlSectionId
		/// </summary>
		public virtual IEntityRelation RoomControlSectionEntityUsingRoomControlSectionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RoomControlSectionEntity", false);
				relation.AddEntityFieldPair(RoomControlSectionFields.RoomControlSectionId, RoomControlWidgetFields.RoomControlSectionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlSectionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlWidgetEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RoomControlWidgetEntity and RoomControlSectionItemEntity over the m:1 relation they have, using the relation between the fields:
		/// RoomControlWidget.RoomControlSectionItemId - RoomControlSectionItem.RoomControlSectionItemId
		/// </summary>
		public virtual IEntityRelation RoomControlSectionItemEntityUsingRoomControlSectionItemId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RoomControlSectionItemEntity", false);
				relation.AddEntityFieldPair(RoomControlSectionItemFields.RoomControlSectionItemId, RoomControlWidgetFields.RoomControlSectionItemId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlSectionItemEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoomControlWidgetEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticRoomControlWidgetRelations
	{
		internal static readonly IEntityRelation CustomTextEntityUsingRoomControlWidgetIdStatic = new RoomControlWidgetRelations().CustomTextEntityUsingRoomControlWidgetId;
		internal static readonly IEntityRelation RoomControlWidgetLanguageEntityUsingRoomControlWidgetIdStatic = new RoomControlWidgetRelations().RoomControlWidgetLanguageEntityUsingRoomControlWidgetId;
		internal static readonly IEntityRelation InfraredConfigurationEntityUsingInfraredConfigurationIdStatic = new RoomControlWidgetRelations().InfraredConfigurationEntityUsingInfraredConfigurationId;
		internal static readonly IEntityRelation RoomControlComponentEntityUsingRoomControlComponentId1Static = new RoomControlWidgetRelations().RoomControlComponentEntityUsingRoomControlComponentId1;
		internal static readonly IEntityRelation RoomControlComponentEntityUsingRoomControlComponentId10Static = new RoomControlWidgetRelations().RoomControlComponentEntityUsingRoomControlComponentId10;
		internal static readonly IEntityRelation RoomControlComponentEntityUsingRoomControlComponentId2Static = new RoomControlWidgetRelations().RoomControlComponentEntityUsingRoomControlComponentId2;
		internal static readonly IEntityRelation RoomControlComponentEntityUsingRoomControlComponentId3Static = new RoomControlWidgetRelations().RoomControlComponentEntityUsingRoomControlComponentId3;
		internal static readonly IEntityRelation RoomControlComponentEntityUsingRoomControlComponentId4Static = new RoomControlWidgetRelations().RoomControlComponentEntityUsingRoomControlComponentId4;
		internal static readonly IEntityRelation RoomControlComponentEntityUsingRoomControlComponentId5Static = new RoomControlWidgetRelations().RoomControlComponentEntityUsingRoomControlComponentId5;
		internal static readonly IEntityRelation RoomControlComponentEntityUsingRoomControlComponentId6Static = new RoomControlWidgetRelations().RoomControlComponentEntityUsingRoomControlComponentId6;
		internal static readonly IEntityRelation RoomControlComponentEntityUsingRoomControlComponentId7Static = new RoomControlWidgetRelations().RoomControlComponentEntityUsingRoomControlComponentId7;
		internal static readonly IEntityRelation RoomControlComponentEntityUsingRoomControlComponentId8Static = new RoomControlWidgetRelations().RoomControlComponentEntityUsingRoomControlComponentId8;
		internal static readonly IEntityRelation RoomControlComponentEntityUsingRoomControlComponentId9Static = new RoomControlWidgetRelations().RoomControlComponentEntityUsingRoomControlComponentId9;
		internal static readonly IEntityRelation RoomControlSectionEntityUsingRoomControlSectionIdStatic = new RoomControlWidgetRelations().RoomControlSectionEntityUsingRoomControlSectionId;
		internal static readonly IEntityRelation RoomControlSectionItemEntityUsingRoomControlSectionItemIdStatic = new RoomControlWidgetRelations().RoomControlSectionItemEntityUsingRoomControlSectionItemId;

		/// <summary>CTor</summary>
		static StaticRoomControlWidgetRelations()
		{
		}
	}
}
