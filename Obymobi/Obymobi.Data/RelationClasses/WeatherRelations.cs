﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Weather. </summary>
	public partial class WeatherRelations
	{
		/// <summary>CTor</summary>
		public WeatherRelations()
		{
		}

		/// <summary>Gets all relations of the WeatherEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CloudProcessingTaskEntityUsingWeatherId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between WeatherEntity and CloudProcessingTaskEntity over the 1:n relation they have, using the relation between the fields:
		/// Weather.WeatherId - CloudProcessingTask.WeatherId
		/// </summary>
		public virtual IEntityRelation CloudProcessingTaskEntityUsingWeatherId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CloudProcessingTaskCollection" , true);
				relation.AddEntityFieldPair(WeatherFields.WeatherId, CloudProcessingTaskFields.WeatherId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WeatherEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CloudProcessingTaskEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticWeatherRelations
	{
		internal static readonly IEntityRelation CloudProcessingTaskEntityUsingWeatherIdStatic = new WeatherRelations().CloudProcessingTaskEntityUsingWeatherId;

		/// <summary>CTor</summary>
		static StaticWeatherRelations()
		{
		}
	}
}
