﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Availability. </summary>
	public partial class AvailabilityRelations
	{
		/// <summary>CTor</summary>
		public AvailabilityRelations()
		{
		}

		/// <summary>Gets all relations of the AvailabilityEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CustomTextEntityUsingAvailabilityId);
			toReturn.Add(this.UIWidgetAvailabilityEntityUsingAvailabilityId);
			toReturn.Add(this.CategoryEntityUsingActionCategoryId);
			toReturn.Add(this.CompanyEntityUsingCompanyId);
			toReturn.Add(this.EntertainmentEntityUsingActionEntertainmentId);
			toReturn.Add(this.PageEntityUsingActionPageId);
			toReturn.Add(this.ProductCategoryEntityUsingActionProductCategoryId);
			toReturn.Add(this.SiteEntityUsingActionSiteId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between AvailabilityEntity and CustomTextEntity over the 1:n relation they have, using the relation between the fields:
		/// Availability.AvailabilityId - CustomText.AvailabilityId
		/// </summary>
		public virtual IEntityRelation CustomTextEntityUsingAvailabilityId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomTextCollection" , true);
				relation.AddEntityFieldPair(AvailabilityFields.AvailabilityId, CustomTextFields.AvailabilityId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AvailabilityEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomTextEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AvailabilityEntity and UIWidgetAvailabilityEntity over the 1:n relation they have, using the relation between the fields:
		/// Availability.AvailabilityId - UIWidgetAvailability.AvailabilityId
		/// </summary>
		public virtual IEntityRelation UIWidgetAvailabilityEntityUsingAvailabilityId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UIWidgetAvailabilityCollection" , true);
				relation.AddEntityFieldPair(AvailabilityFields.AvailabilityId, UIWidgetAvailabilityFields.AvailabilityId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AvailabilityEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UIWidgetAvailabilityEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between AvailabilityEntity and CategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// Availability.ActionCategoryId - Category.CategoryId
		/// </summary>
		public virtual IEntityRelation CategoryEntityUsingActionCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CategoryEntity", false);
				relation.AddEntityFieldPair(CategoryFields.CategoryId, AvailabilityFields.ActionCategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AvailabilityEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AvailabilityEntity and CompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// Availability.CompanyId - Company.CompanyId
		/// </summary>
		public virtual IEntityRelation CompanyEntityUsingCompanyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CompanyEntity", false);
				relation.AddEntityFieldPair(CompanyFields.CompanyId, AvailabilityFields.CompanyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AvailabilityEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AvailabilityEntity and EntertainmentEntity over the m:1 relation they have, using the relation between the fields:
		/// Availability.ActionEntertainmentId - Entertainment.EntertainmentId
		/// </summary>
		public virtual IEntityRelation EntertainmentEntityUsingActionEntertainmentId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "EntertainmentEntity", false);
				relation.AddEntityFieldPair(EntertainmentFields.EntertainmentId, AvailabilityFields.ActionEntertainmentId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntertainmentEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AvailabilityEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AvailabilityEntity and PageEntity over the m:1 relation they have, using the relation between the fields:
		/// Availability.ActionPageId - Page.PageId
		/// </summary>
		public virtual IEntityRelation PageEntityUsingActionPageId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PageEntity", false);
				relation.AddEntityFieldPair(PageFields.PageId, AvailabilityFields.ActionPageId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AvailabilityEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AvailabilityEntity and ProductCategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// Availability.ActionProductCategoryId - ProductCategory.ProductCategoryId
		/// </summary>
		public virtual IEntityRelation ProductCategoryEntityUsingActionProductCategoryId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ProductCategoryEntity", false);
				relation.AddEntityFieldPair(ProductCategoryFields.ProductCategoryId, AvailabilityFields.ActionProductCategoryId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductCategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AvailabilityEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AvailabilityEntity and SiteEntity over the m:1 relation they have, using the relation between the fields:
		/// Availability.ActionSiteId - Site.SiteId
		/// </summary>
		public virtual IEntityRelation SiteEntityUsingActionSiteId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SiteEntity", false);
				relation.AddEntityFieldPair(SiteFields.SiteId, AvailabilityFields.ActionSiteId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SiteEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AvailabilityEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticAvailabilityRelations
	{
		internal static readonly IEntityRelation CustomTextEntityUsingAvailabilityIdStatic = new AvailabilityRelations().CustomTextEntityUsingAvailabilityId;
		internal static readonly IEntityRelation UIWidgetAvailabilityEntityUsingAvailabilityIdStatic = new AvailabilityRelations().UIWidgetAvailabilityEntityUsingAvailabilityId;
		internal static readonly IEntityRelation CategoryEntityUsingActionCategoryIdStatic = new AvailabilityRelations().CategoryEntityUsingActionCategoryId;
		internal static readonly IEntityRelation CompanyEntityUsingCompanyIdStatic = new AvailabilityRelations().CompanyEntityUsingCompanyId;
		internal static readonly IEntityRelation EntertainmentEntityUsingActionEntertainmentIdStatic = new AvailabilityRelations().EntertainmentEntityUsingActionEntertainmentId;
		internal static readonly IEntityRelation PageEntityUsingActionPageIdStatic = new AvailabilityRelations().PageEntityUsingActionPageId;
		internal static readonly IEntityRelation ProductCategoryEntityUsingActionProductCategoryIdStatic = new AvailabilityRelations().ProductCategoryEntityUsingActionProductCategoryId;
		internal static readonly IEntityRelation SiteEntityUsingActionSiteIdStatic = new AvailabilityRelations().SiteEntityUsingActionSiteId;

		/// <summary>CTor</summary>
		static StaticAvailabilityRelations()
		{
		}
	}
}
