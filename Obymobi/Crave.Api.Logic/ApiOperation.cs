﻿using Crave.Api.Logic.Timestamps;
using Dionysos;
using System;
using System.Collections.Generic;

namespace Crave.Api.Logic
{
    public class ApiOperation
    {
        #region Constructors

        public ApiOperation(string requestName, string path, ApiCallerType callerType, TimestampFieldIndexPair timestampFieldIndexPair = null)
        {
            this.RequestName = requestName;
            this.Path = path;
            this.CallerType = callerType;
            this.TimestampFieldIndexes = timestampFieldIndexPair;

            this.ParseParameters();
        }

        #endregion

        #region Properties

        public string RequestName { get; set; }

        public string Path { get; set; }

        public ApiCallerType CallerType { get; set; }

        public TimestampFieldIndexPair TimestampFieldIndexes { get; set; }

        public IEnumerable<string> Parameters { get; set; }

        #endregion

        #region Methods

        public string GetPathWithParameters(Dictionary<string, object> parameterNamesAndValues, string postfix = "")
        {
            string pathWithParameters = this.Path;

            foreach (string parameterName in parameterNamesAndValues.Keys)
            {
                object parameterValue = parameterNamesAndValues[parameterName];
                pathWithParameters = pathWithParameters.Replace("{" + parameterName + "}", parameterValue.ToString());
            }

            return pathWithParameters + postfix;
        }

        public string GetFilename(Dictionary<string, object> parameterNamesAndValues, DateTime timestamp)
        {
            string fileName = this.RequestName;

            foreach (string parameterName in parameterNamesAndValues.Keys)
            {
                object parameterValue = parameterNamesAndValues[parameterName];
                fileName += string.Format("_{0}-{1}", parameterName, parameterValue.ToString());
            }

            fileName += "-" + timestamp.ToUnixTime();
            fileName += ".json.gz";

            return fileName;
        }

        private void ParseParameters()
        {
            List<string> parameters = new List<string>();

            string[] parts = this.Path.Split("/", StringSplitOptions.RemoveEmptyEntries);
            foreach (string part in parts)
            {
                if (part.StartsWith("{") && part.EndsWith("}"))
                    parameters.Add(part.Substring(1, part.Length-2));
            }

            this.Parameters = parameters;
        }

        #endregion
    }
}
