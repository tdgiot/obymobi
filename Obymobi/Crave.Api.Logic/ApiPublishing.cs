﻿using Crave.Api.Logic.Timestamps;
using Dionysos;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Crave.Api.Logic
{
    public class ApiPublishing
    {
        #region Properties

        public string BaseUrl { get; set; }

        public ApiOperation ApiOperation { get; set; }

        public TimestampBase Timestamp { get; set; }

        public ApiCallAuthentication Authentication { get; set; }

        public bool Force { get; set; }

        public string Url { get; set; }

        public string FileName { get; set; }

        #endregion

        #region Methods

        public PublishResult<object> Execute()
        {
            PublishResult<object> publishResult = null;

            string path = this.ApiOperation.GetPathWithParameters(this.Timestamp.ParameterNamesAndValues);
            string publishPath = path + ApiMetaData.Publish;
            string publishRequestName = this.ApiOperation.RequestName.Replace("Get", "Publish");

            ApiCall apiCall = new ApiCall
            {
                BaseUrl = this.BaseUrl,
                Path = publishPath,
                RequestName = publishRequestName,
                Authentication = this.Authentication
            };

            this.Url = apiCall.GetUrlForApiCall();

            if (this.Timestamp.HasUnpublishedChanges() || this.Force)
            {
                string json = apiCall.Execute();
                if (!json.IsNullOrWhiteSpace())
                {
                    JsonSerializerSettings settings = new JsonSerializerSettings();
                    settings.NullValueHandling = NullValueHandling.Ignore;
                    settings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                    publishResult = JsonConvert.DeserializeObject<PublishResult<object>>(json, settings);
                }
            }

            return publishResult;
        }

        #endregion
    }
}
