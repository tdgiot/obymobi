﻿namespace Crave.Api.Logic
{
    public class SimpleApiResult
    {
        /// <summary>
        /// Gets or sets a value indicating whether the API call was successful.
        /// </summary>
        /// <remarks>This property is intentionally made a Nullable, otherwise it won't show up in the API result as ServiceStack excludes default values.</remarks>
        public bool? Success { get; set; }

        public string ErrorMessage { get; set; }
    }
}