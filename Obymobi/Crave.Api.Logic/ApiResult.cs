﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crave.Api.Logic
{
    public class ApiResult<T>
    {
        #region Properties

        /// <summary>
        /// Gets the code as an <see cref="int" /> value.
        /// </summary>
        public int Code { get; set; }

        /// <summary>
        /// Gets the message of the API result.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets the stack trace of the API result.
        /// </summary>
        public string StackTrace { get; set; }

        /// <summary>
        /// Gets the type of the result code enumerator for the API call.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Gets the amount of models returned in the API call.
        /// </summary>
        public int? ModelCount { get; set; }

        /// <summary>
        /// Gets the total elapsed milliseconds for the API call.
        /// </summary>
        public long? Elapsed { get; set; }

        /// <summary>
        /// Gets the elapsed milliseconds for request conversion of the API call.
        /// </summary>
        public long? ElapsedRequestConversion { get; set; }

        /// <summary>
        /// Gets the elapsed milliseconds for the validation of the API call.
        /// </summary>
        public long? ElapsedValidation { get; set; }

        /// <summary>
        /// Gets the elapsed milliseconds for the authentication of the API call.
        /// </summary>
        public long? ElapsedAuthentication { get; set; }

        /// <summary>
        /// Gets the elapsed milliseconds for the execution of the use-case for the API call.
        /// </summary>
        public long ElapsedUseCase { get; set; }

        /// <summary>
        /// Gets the elapsed milliseconds for response conversion of the API call.
        /// </summary>
        public long? ElapsedResponseConversion { get; set; }

        /// <summary>
        /// Gets or sets the diagnostics log for the API result.
        /// </summary>
        public string DiagnosticsLog { get; set; }

        /// <summary>
        /// Gets or sets the data transfer objects (DTO's) of the API call.
        /// </summary>
        public T Models { get; set; }

        #endregion
    }
}
