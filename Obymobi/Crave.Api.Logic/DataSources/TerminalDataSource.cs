﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dionysos.Data.LLBLGen;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Api.Logic.DataSources
{
    public class TerminalDataSource : DataSourceBase<TerminalCollection>
    {
        public override TerminalCollection GetEntities(int companyId)
        {
            return EntityCollection.GetMulti<TerminalCollection>(TerminalFields.CompanyId == companyId, null, null, null, TerminalFields.Name);
        }
    }
}
