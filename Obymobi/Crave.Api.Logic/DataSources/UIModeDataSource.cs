﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dionysos.Data.LLBLGen;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Api.Logic.DataSources
{
    public class UIModeDataSource : DataSourceBase<UIModeCollection>
    {
        public override UIModeCollection GetEntities(int companyId)
        {
            UIModeCollection uiModes = new UIModeCollection();
            
            uiModes.AddRange(GetCompanyUIModes(companyId));
            uiModes.AddRange(GetPointOfInterestUIModes(companyId));

            return uiModes;
        }

        private ICollection<UIModeEntity> GetCompanyUIModes(int companyId)
        {
            return EntityCollection.GetMulti<UIModeCollection>(UIModeFields.CompanyId == companyId, null, null, null, UIModeFields.Name);
        }

        private ICollection<UIModeEntity> GetPointOfInterestUIModes(int companyId)
        {
            RelationCollection relations = new RelationCollection();
            relations.Add(UIModeEntity.Relations.PointOfInterestEntityUsingPointOfInterestId);
            relations.Add(PointOfInterestEntity.Relations.MapPointOfInterestEntityUsingPointOfInterestId);
            relations.Add(MapPointOfInterestEntity.Relations.MapEntityUsingMapId);

            PredicateExpression filter = new PredicateExpression();
            filter.Add(MapFields.CompanyId == companyId);

            return EntityCollection.GetMulti<UIModeCollection>(filter, null, relations, null, new IncludeFieldsList(UIModeFields.Name));
        }
    }
}
