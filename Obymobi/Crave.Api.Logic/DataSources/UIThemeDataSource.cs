﻿using Dionysos.Data.LLBLGen;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Api.Logic.DataSources
{
    public class UIThemeDataSource : DataSourceBase<UIThemeCollection>
    {
        public override UIThemeCollection GetEntities(int companyId)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(DeliverypointgroupFields.CompanyId == companyId);

            RelationCollection relations = new RelationCollection();
            relations.Add(UIThemeEntity.Relations.DeliverypointgroupEntityUsingUIThemeId);

            return EntityCollection.GetMulti<UIThemeCollection>(filter, null, relations, null, new IncludeFieldsList { UIThemeFields.Name });
        }
    }
}
