﻿using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crave.Api.Logic.DataSources
{
    public abstract class DataSourceBase<TEntityCollection> where TEntityCollection : IEnumerable<IEntity>
    {
        public abstract TEntityCollection GetEntities(int companyId);
    }
}
