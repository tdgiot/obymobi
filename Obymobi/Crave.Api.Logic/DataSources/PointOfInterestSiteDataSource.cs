﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dionysos.Data.LLBLGen;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Cms;

namespace Crave.Api.Logic.DataSources
{
    public class PointOfInterestSiteDataSource : DataSourceBase<PointOfInterestCollection>
    {
        public override PointOfInterestCollection GetEntities(int companyId)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(UIModeFields.CompanyId == companyId);
            filter.Add(PageElementFields.PageElementType == PageElementType.Venue);

            RelationCollection relations = new RelationCollection();
            relations.Add(UIModeEntityBase.Relations.UITabEntityUsingUIModeId);
            relations.Add(UITabEntityBase.Relations.SiteEntityUsingSiteId);
            relations.Add(SiteEntityBase.Relations.PageEntityUsingSiteId);
            relations.Add(PageEntityBase.Relations.PageElementEntityUsingPageId);

            PageElementCollection pageElementCollection = EntityCollection.GetMulti<PageElementCollection>(filter, null, relations, null, new IncludeFieldsList { PageElementFields.IntValue2 });
            if (!pageElementCollection.Any())
            {
                return new PointOfInterestCollection();
            }

            PredicateExpression pointOfInterestFilter = new PredicateExpression();
            pointOfInterestFilter.Add(PointOfInterestFields.PointOfInterestId == pageElementCollection.Select(x => x.IntValue2).ToArray());

            return EntityCollection.GetMulti<PointOfInterestCollection>(pointOfInterestFilter, null, null, null, PointOfInterestFields.Name);
        }
    }
}
