﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dionysos.Data.LLBLGen;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;

namespace Crave.Api.Logic.DataSources
{
    public class MapDataSource : DataSourceBase<MapCollection>
    {
        public override MapCollection GetEntities(int companyId)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(UIModeFields.CompanyId == companyId);

            RelationCollection relations = new RelationCollection();
            relations.Add(UIModeEntity.Relations.UITabEntityUsingUIModeId);
            relations.Add(UITabEntity.Relations.MapEntityUsingMapId);

            return EntityCollection.GetMulti<MapCollection>(filter, null, relations, null, new IncludeFieldsList { MapFields.Name });
        }
    }
}
