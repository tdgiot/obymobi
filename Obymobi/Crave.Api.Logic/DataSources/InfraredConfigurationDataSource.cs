﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dionysos.Data.LLBLGen;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Api.Logic.DataSources
{
    public class InfraredConfigurationDataSource : DataSourceBase<InfraredConfigurationCollection>
    {
        public override InfraredConfigurationCollection GetEntities(int companyId)
        {
            InfraredConfigurationCollection infraredConfigurationCollection = new InfraredConfigurationCollection();

            infraredConfigurationCollection.AddRange(this.GetInfraredConfigurationCollectionForRoomControlWidgets(companyId));
            infraredConfigurationCollection.AddRange(this.GetInfraredConfigurationCollectionForRoomControlComponents(companyId));
            infraredConfigurationCollection.AddRange(this.GetInfraredConfigurationCollectionForRoomControlSectionItems(companyId));

            return infraredConfigurationCollection.GroupBy(x => x.InfraredConfigurationId)
                                                  .Select(x => x.First())
                                                  .OrderBy(x => x.Name)
                                                  .ToEntityCollection<InfraredConfigurationCollection>();
        }

        private InfraredConfigurationCollection GetInfraredConfigurationCollectionForRoomControlWidgets(int companyId)
        {
            RelationCollection relations = new RelationCollection();
            relations.Add(RoomControlConfigurationEntity.Relations.RoomControlAreaEntityUsingRoomControlConfigurationId);
            relations.Add(RoomControlAreaEntity.Relations.RoomControlSectionEntityUsingRoomControlAreaId);
            relations.Add(RoomControlSectionEntity.Relations.RoomControlWidgetEntityUsingRoomControlSectionId);
            relations.Add(RoomControlWidgetEntity.Relations.InfraredConfigurationEntityUsingInfraredConfigurationId);

            return EntityCollection.GetMulti<InfraredConfigurationCollection>(RoomControlConfigurationFields.CompanyId == companyId, null, relations, null, new IncludeFieldsList { InfraredConfigurationFields.Name });
        }

        private InfraredConfigurationCollection GetInfraredConfigurationCollectionForRoomControlComponents(int companyId)
        {
            RelationCollection relations = new RelationCollection();
            relations.Add(RoomControlConfigurationEntity.Relations.RoomControlAreaEntityUsingRoomControlConfigurationId);
            relations.Add(RoomControlAreaEntity.Relations.RoomControlSectionEntityUsingRoomControlAreaId);
            relations.Add(RoomControlSectionEntity.Relations.RoomControlComponentEntityUsingRoomControlSectionId);
            relations.Add(RoomControlComponentEntity.Relations.InfraredConfigurationEntityUsingInfraredConfigurationId);

            return EntityCollection.GetMulti<InfraredConfigurationCollection>(RoomControlConfigurationFields.CompanyId == companyId, null, relations, null, new IncludeFieldsList { InfraredConfigurationFields.Name });
        }

        private InfraredConfigurationCollection GetInfraredConfigurationCollectionForRoomControlSectionItems(int companyId)
        {
            RelationCollection relations = new RelationCollection();
            relations.Add(RoomControlConfigurationEntity.Relations.RoomControlAreaEntityUsingRoomControlConfigurationId);
            relations.Add(RoomControlAreaEntity.Relations.RoomControlSectionEntityUsingRoomControlAreaId);
            relations.Add(RoomControlSectionEntity.Relations.RoomControlSectionItemEntityUsingRoomControlSectionId);
            relations.Add(RoomControlSectionItemEntity.Relations.InfraredConfigurationEntityUsingInfraredConfigurationId);

            return EntityCollection.GetMulti<InfraredConfigurationCollection>(RoomControlConfigurationFields.CompanyId == companyId, null, relations, null, new IncludeFieldsList { InfraredConfigurationFields.Name });
        }
    }
}
