﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dionysos.Data.LLBLGen;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;

namespace Crave.Api.Logic.DataSources
{
    public class MessagegroupDataSource : DataSourceBase<MessagegroupCollection>
    {
        public override MessagegroupCollection GetEntities(int companyId)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(MessagegroupFields.CompanyId == companyId);

            return EntityCollection.GetMulti<MessagegroupCollection>(filter, null, null, null, new IncludeFieldsList { MessagegroupFields.Name });
        }
    }
}
