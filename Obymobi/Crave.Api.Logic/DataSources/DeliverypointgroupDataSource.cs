﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dionysos.Data.LLBLGen;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Api.Logic.DataSources
{
    public class DeliverypointgroupDataSource : DataSourceBase<DeliverypointgroupCollection>
    {
        public override DeliverypointgroupCollection GetEntities(int companyId)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(DeliverypointgroupFields.CompanyId == companyId);
            filter.Add(DeliverypointgroupFields.Active == true);

            return EntityCollection.GetMulti<DeliverypointgroupCollection>(filter, null, null, null, DeliverypointgroupFields.Name);
        }
    }
}
