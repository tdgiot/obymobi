﻿using Dionysos.Data.LLBLGen;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Api.Logic.DataSources
{
    public class UIScheduleDataSource : DataSourceBase<UIScheduleCollection>
    {
        public override UIScheduleCollection GetEntities(int companyId)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(UIScheduleFields.CompanyId == companyId);
            filter.Add(UIScheduleFields.Type == UIScheduleType.Legacy);

            return EntityCollection.GetMulti<UIScheduleCollection>(filter, null, null, null, UIScheduleFields.Name);
        }
    }
}
