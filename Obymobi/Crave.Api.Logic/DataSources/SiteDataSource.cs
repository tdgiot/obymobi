﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dionysos.Data.LLBLGen;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;

namespace Crave.Api.Logic.DataSources
{
    public class SiteDataSource : DataSourceBase<SiteCollection>
    {
        public override SiteCollection GetEntities(int companyId)
        {
            List<int> siteIds = new List<int>();

            siteIds.AddRange(this.GetSiteCollectionLinkedAsUITab(companyId));
            siteIds.AddRange(this.GetSiteIdsLinkedAsEntertainment(companyId));

            siteIds = siteIds.Distinct().ToList();

            return this.GetSiteCollection(siteIds);
        }

        private SiteCollection GetSiteCollection(List<int> siteIds)
        {
            return EntityCollection.GetMulti<SiteCollection>(new PredicateExpression{ SiteFields.SiteId == siteIds }, null, null, null, new IncludeFieldsList { SiteFields.Name });
        }

        private List<int> GetSiteCollectionLinkedAsUITab(int companyId)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(UIModeFields.CompanyId == companyId);
            filter.Add(UITabFields.SiteId != DBNull.Value);

            RelationCollection relations = new RelationCollection();
            relations.Add(UIModeEntityBase.Relations.UITabEntityUsingUIModeId);

            UITabCollection uiTabCollection = EntityCollection.GetMulti<UITabCollection>(filter, null, relations, null, new IncludeFieldsList {UITabFields.SiteId});
            return uiTabCollection.Where(x => x.SiteId.HasValue).Select(x => x.SiteId.Value).ToList();
        }

        private List<int> GetSiteIdsLinkedAsEntertainment(int companyId)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(EntertainmentConfigurationFields.CompanyId == companyId);
            filter.Add(EntertainmentFields.SiteId != DBNull.Value);

            RelationCollection relations = new RelationCollection();
            relations.Add(EntertainmentEntityBase.Relations.EntertainmentConfigurationEntertainmentEntityUsingEntertainmentId);
            relations.Add(EntertainmentConfigurationEntertainmentEntityBase.Relations.EntertainmentConfigurationEntityUsingEntertainmentConfigurationId);

            EntertainmentCollection entertainmentCollection = EntityCollection.GetMulti<EntertainmentCollection>(filter, null, relations, null, new IncludeFieldsList {EntertainmentFields.SiteId});
            return entertainmentCollection.Where(x => x.SiteId.HasValue).Select(x => x.SiteId.Value).ToList();
        }
    }
}
