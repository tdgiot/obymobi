﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dionysos.Data.LLBLGen;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data;
using Obymobi.Data.EntityClasses;

namespace Crave.Api.Logic.DataSources
{
    public class CompanyReleaseDataSource : DataSourceBase<CompanyReleaseCollection>
    {
        public override CompanyReleaseCollection GetEntities(int companyId)
        {
            PrefetchPath prefetch = new PrefetchPath(EntityType.CompanyReleaseEntity);
            prefetch.Add(CompanyReleaseEntity.PrefetchPathReleaseEntity).SubPath.Add(ReleaseEntity.PrefetchPathApplicationEntity, new IncludeFieldsList(ApplicationFields.Name, ApplicationFields.Code));

            RelationCollection relations = new RelationCollection();
            relations.Add(CompanyReleaseEntity.Relations.ReleaseEntityUsingReleaseId);
            relations.Add(ReleaseEntity.Relations.ApplicationEntityUsingApplicationId);

            return EntityCollection.GetMulti<CompanyReleaseCollection>(CompanyReleaseFields.CompanyId == companyId, null, relations, prefetch, new IncludeFieldsList());
        }
    }
}
