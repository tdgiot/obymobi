﻿using System;
using System.Collections.Generic;
using System.Linq;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Api.Logic.DataSources
{
    public class PublishingDataSource
    {
        public PublishingEntity GetEntity(string publishRequest, Dictionary<string, object> parameterNamesAndValues)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(PublishingFields.PublishRequest == publishRequest);

            if (parameterNamesAndValues.Any())
            {
                string[] parameterNames = parameterNamesAndValues.Keys.ToArray();
                object[] parameterValues = parameterNamesAndValues.Values.ToArray();

                if (parameterNames.Length > 0 && parameterValues.Length > 0)
                {
                    filter.Add(PublishingFields.Parameter1Name == parameterNames[0]);
                    filter.Add(PublishingFields.Parameter1Value == parameterValues[0]);
                }

                if (parameterNames.Length > 1 && parameterValues.Length > 1)
                {
                    filter.Add(PublishingFields.Parameter2Name == parameterNames[1]);
                    filter.Add(PublishingFields.Parameter2Value == parameterValues[1]);
                }
            }
        
            SortExpression sort = new SortExpression(PublishingFields.PublishingId | SortOperator.Descending);

            IncludeFieldsList includes = new IncludeFieldsList
            {
                PublishingFields.PublishingId,
                PublishingFields.Username
            };

            PublishingCollection publishingCollection = new PublishingCollection();
            publishingCollection.GetMulti(filter, 1, sort, null, null, includes, 0, 0);

            if (publishingCollection.Any())
            {
                return publishingCollection[0];
            }

            return null;
        }
    }
}
