﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dionysos.Data.LLBLGen;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Api.Logic.DataSources
{
    public class RoomControlConfigurationDataSource : DataSourceBase<RoomControlConfigurationCollection>
    {
        public override RoomControlConfigurationCollection GetEntities(int companyId)
        {
            return EntityCollection.GetMulti<RoomControlConfigurationCollection>(RoomControlConfigurationFields.CompanyId == companyId, null, null, null, RoomControlConfigurationFields.Name);
        }
    }
}
