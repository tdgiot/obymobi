﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dionysos.Data.LLBLGen;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Cms;

namespace Crave.Api.Logic.DataSources
{
    public class PointOfInterestMapDataSource : DataSourceBase<PointOfInterestCollection>
    {
        public override PointOfInterestCollection GetEntities(int companyId)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(UIModeFields.CompanyId == companyId);

            RelationCollection relations = new RelationCollection();
            relations.Add(UIModeEntityBase.Relations.UITabEntityUsingUIModeId);
            relations.Add(UITabEntityBase.Relations.MapEntityUsingMapId);
            relations.Add(MapEntityBase.Relations.MapPointOfInterestEntityUsingMapId);
            relations.Add(MapPointOfInterestEntityBase.Relations.PointOfInterestEntityUsingPointOfInterestId);

            return EntityCollection.GetMulti<PointOfInterestCollection>(filter, null, relations, null, new IncludeFieldsList { PointOfInterestFields.Name });
        }
    }
}
