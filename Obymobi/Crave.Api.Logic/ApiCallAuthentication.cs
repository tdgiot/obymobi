﻿using System;
using System.Collections.Generic;
using Dionysos;
using Obymobi.Security;

namespace Crave.Api.Logic
{
    public class ApiCallAuthentication
    {
        #region Properties

        public string Identifier { get; set; }

        public string DeviceToken { get; set; }

        public Dictionary<string, object> ParameterNamesAndValues { get;set; }

        public long Timestamp { get; } = DateTimeUtil.ToUnixTime(DateTime.UtcNow);

        #endregion

        #region Methods

        public string GetAuthenticationQueryString(string requestName)
        {
            List<object> parameters = new List<object>();
            parameters.Add(requestName);
            parameters.Add(this.Timestamp);
            parameters.Add(this.Identifier);

            foreach (string parameterKey in this.ParameterNamesAndValues.Keys)
                parameters.Add(this.ParameterNamesAndValues[parameterKey]);

            if (this.DeviceToken.IsNullOrWhiteSpace())
                throw new InvalidOperationException("No salt specified!");
            
            string hash = Hasher.GetHashFromParameters(this.DeviceToken, parameters.ToArray());

            string queryString = "?";
            queryString += string.Format("ts={0}", this.Timestamp);
            queryString += string.Format("&id={0}", this.Identifier);
            queryString += string.Format("&h={0}", hash);

            return queryString;
        }

        #endregion
    }
}
