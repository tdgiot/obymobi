﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crave.Api.Logic
{
    public class PublishResult<T> : ApiResult<T>
    {
        /// <summary>
        /// Gets or sets the cloud processing task id
        /// </summary>
        public int CloudProcessingTaskId { get; set; }

        /// <summary>
        /// Gets or sets the urls of the files that were published.
        /// </summary>
        public IEnumerable<string> Urls { get; set; }

        /// <summary>
        /// Gets or sets the log for the publishing.
        /// </summary>
        public string Log { get; set; }
    }
}
