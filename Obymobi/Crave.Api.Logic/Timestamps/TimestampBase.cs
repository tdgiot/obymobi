﻿using System;
using System.Collections.Generic;
using System.Linq;
using Obymobi.Data;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Api.Logic.Timestamps
{
    public abstract class TimestampBase
    {
        #region Constructors

        public TimestampBase(object foreignKeyValue)
        {
            this.ForeignKeyValue = foreignKeyValue;
            this.TimestampEntity = TimestampHelper.GetOrCreateTimestampEntity(this.ForeignKeyField, this.ForeignKeyValue, null, null);
            this.SetLastModifiedFieldsToDefaultValues();
        }

        #endregion

        #region Properties

        public TimestampEntity TimestampEntity { get; private set; }

        public object ForeignKeyValue { get; private set; }

        public abstract EntityField ForeignKeyField { get; }

        public virtual Dictionary<string, object> ParameterNamesAndValues
        {
            get 
            { 
                return new Dictionary<string, object> { { this.ForeignKeyField.Name, this.ForeignKeyValue } };
            }
        }

        public abstract IEnumerable<TimestampFieldIndexPair> Fields { get; }

        #endregion

        #region Methods

        public DateTime? GetTimestampUtc(TimestampFieldIndex field)
        {
            return this.TimestampEntity.Fields[(int)field].CurrentValue as DateTime?;
        }

        public DateTime? GetLastModifiedUtc()
        {
            DateTime? latestLastModifiedUtc = null;

            foreach (TimestampFieldIndexPair timestampFieldIndexPair in this.Fields)
            {
                TimestampFieldIndex modifiedField = timestampFieldIndexPair.ModifiedField;

                DateTime? lastModifiedUtc = this.TimestampEntity.Fields[(int)modifiedField].CurrentValue as DateTime?;
                if (lastModifiedUtc.HasValue)
                {
                    if (!latestLastModifiedUtc.HasValue)
                        latestLastModifiedUtc = lastModifiedUtc;
                    else if (latestLastModifiedUtc.Value < lastModifiedUtc.Value)
                        latestLastModifiedUtc = lastModifiedUtc;
                }
            }

            return latestLastModifiedUtc;
        }

        public DateTime? GetLastPublishedUtc()
        {
            DateTime? latestLastPublishedUtc = null;

            foreach (TimestampFieldIndexPair timestampFieldIndexPair in this.Fields)
            {
                TimestampFieldIndex modifiedField = timestampFieldIndexPair.ModifiedField;
                TimestampFieldIndex publishedField = timestampFieldIndexPair.PublishedField;

                DateTime? lastPublishedUtc = this.TimestampEntity.Fields[(int)publishedField].CurrentValue as DateTime?;
                if (lastPublishedUtc.HasValue)
                {
                    if (!latestLastPublishedUtc.HasValue)
                        latestLastPublishedUtc = lastPublishedUtc;
                    else if (latestLastPublishedUtc.Value < lastPublishedUtc.Value)
                        latestLastPublishedUtc = lastPublishedUtc;
                }
            }

            return latestLastPublishedUtc;
        }

        public void UpdatePublishedTimestamps()
        {
            DateTime utcNow = DateTime.UtcNow;

            foreach (TimestampFieldIndexPair timestampFieldIndexPair in this.Fields)
            {
                TimestampFieldIndex modifiedField = timestampFieldIndexPair.ModifiedField;
                TimestampFieldIndex publishedField = timestampFieldIndexPair.PublishedField;

                this.TimestampEntity.SetNewFieldValue((int)publishedField, utcNow);
            }

            this.TimestampEntity.Save();
        }
        
        public bool HasUnpublishedChanges()
        {
            bool hasUnpublishedChanges = false;

            foreach (TimestampFieldIndexPair timestampFieldIndexPair in this.Fields)
            {
                TimestampFieldIndex modifiedField = timestampFieldIndexPair.ModifiedField;
                TimestampFieldIndex publishedField = timestampFieldIndexPair.PublishedField;

                if (!this.IsFieldDirty(modifiedField, publishedField))
                    continue;

                hasUnpublishedChanges = true;
                break;
            }

            return hasUnpublishedChanges;
        }

        public bool IsFieldDirty(TimestampFieldIndexPair timestampFieldIndexPair)
        {
            return this.IsFieldDirty(timestampFieldIndexPair.ModifiedField, timestampFieldIndexPair.PublishedField);
        }

        public bool IsFieldDirty(TimestampFieldIndex modifiedField, TimestampFieldIndex publishedField)
        {
            DateTime? modifiedUtc = this.TimestampEntity.Fields[(int)modifiedField].CurrentValue as DateTime?;
            DateTime? publishedUtc = this.TimestampEntity.Fields[(int)publishedField].CurrentValue as DateTime?;

            if (!modifiedUtc.HasValue) // We cannot determine without a modified value
                return false;

            if (publishedUtc.HasValue && publishedUtc.Value >= modifiedUtc.Value) // Not dirty
                return false;

            return true;
        }

        private void SetLastModifiedFieldsToDefaultValues()
        {
            bool update = false;

            foreach (TimestampFieldIndexPair timestampFieldIndexPair in this.Fields)
            {
                TimestampFieldIndex modifiedField = timestampFieldIndexPair.ModifiedField;

                DateTime? modifiedUtc = this.TimestampEntity.Fields[(int)modifiedField].CurrentValue as DateTime?;
                if (!modifiedUtc.HasValue)
                {
                    this.TimestampEntity.SetNewFieldValue((int)modifiedField, DateTime.UtcNow);
                    update = true;
                }
            }

            if (update)
                this.TimestampEntity.Save();
        }

        #endregion
    }
}
