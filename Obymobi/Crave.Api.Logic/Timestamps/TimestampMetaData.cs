﻿using Obymobi.Data;

namespace Crave.Api.Logic.Timestamps
{
    public class TimestampMetaData
    {
        // Advertisement configuration
        public static TimestampFieldIndexPair AdvertisementConfiguration => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedAdvertisementConfigurationUTC, TimestampFieldIndex.PublishedAdvertisementConfigurationUTC);
        public static TimestampFieldIndexPair AdvertisementConfigurationCustomText => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedAdvertisementConfigurationCustomTextUTC, TimestampFieldIndex.PublishedAdvertisementConfigurationCustomTextUTC);
        public static TimestampFieldIndexPair AdvertisementConfigurationMedia => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedAdvertisementConfigurationMediaUTC, TimestampFieldIndex.PublishedAdvertisementConfigurationMediaUTC);

        // Announcement actions
        public static TimestampFieldIndexPair AnnouncementActions => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedAnnouncementActionsUTC, TimestampFieldIndex.PublishedAnnouncementActionsUTC);

        // Availabilities
        public static TimestampFieldIndexPair Availabilities => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedAvailabilitiesUTC, TimestampFieldIndex.PublishedAvailabilitiesUTC);
        public static TimestampFieldIndexPair AvailabilitiesCustomText => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedAvailabilitiesCustomTextUTC, TimestampFieldIndex.PublishedAvailabilitiesCustomTextUTC);

        // Client configuration
        public static TimestampFieldIndexPair ClientConfiguration => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedClientConfigurationUTC, TimestampFieldIndex.PublishedClientConfigurationUTC);
        public static TimestampFieldIndexPair ClientConfigurationCustomText => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedClientConfigurationCustomTextUTC, TimestampFieldIndex.PublishedClientConfigurationCustomTextUTC);
        public static TimestampFieldIndexPair ClientConfigurationMedia => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedClientConfigurationMediaUTC, TimestampFieldIndex.PublishedClientConfigurationMediaUTC);

        // Client
        public static TimestampFieldIndexPair Client => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedClientUTC, TimestampFieldIndex.PublishedClientUTC);

        // Cloud storage accounts
        public static TimestampFieldIndexPair CloudStorageAccounts => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedCloudStorageAccountsUTC, TimestampFieldIndex.PublishedCloudStorageAccountsUTC);

        // Company amenities
        public static TimestampFieldIndexPair Amenities => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedAmenitiesUTC, TimestampFieldIndex.PublishedAmenitiesUTC);
        public static TimestampFieldIndexPair AmenitiesCustomText => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedAmenitiesCustomTextUTC, TimestampFieldIndex.PublishedAmenitiesCustomTextUTC);

        // Company releases
        public static TimestampFieldIndexPair CompanyReleases => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedCompanyReleasesUTC, TimestampFieldIndex.PublishedCompanyReleasesUTC);

        // Company
        public static TimestampFieldIndexPair Company => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedCompanyUTC, TimestampFieldIndex.PublishedCompanyUTC);
        public static TimestampFieldIndexPair CompanyCustomText => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedCompanyCustomTextUTC, TimestampFieldIndex.PublishedCompanyCustomTextUTC);
        public static TimestampFieldIndexPair CompanyMedia => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedCompanyMediaUTC, TimestampFieldIndex.PublishedCompanyMediaUTC);

        // Company venue categories
        public static TimestampFieldIndexPair CompanyVenueCategories => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedCompanyVenueCategoriesUTC, TimestampFieldIndex.PublishedCompanyVenueCategoriesUTC);
        public static TimestampFieldIndexPair CompanyVenueCategoriesCustomText => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedCompanyVenueCategoriesCustomTextUTC, TimestampFieldIndex.PublishedCompanyVenueCategoriesCustomTextUTC);

        // Deliverypoints
        public static TimestampFieldIndexPair Deliverypoints => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedDeliverypointsUTC, TimestampFieldIndex.PublishedDeliverypointsUTC);

        // Deliverypoint
        public static TimestampFieldIndexPair Deliverypoint => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedDeliverypointUTC, TimestampFieldIndex.PublishedDeliverypointUTC);

        // Delivery time
        public static TimestampFieldIndexPair DeliveryTime => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedDeliveryTimeUTC, TimestampFieldIndex.PublishedDeliveryTimeUTC);

        // Entertainment configuration
        public static TimestampFieldIndexPair EntertainmentConfiguration => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedEntertainmentConfigurationUTC, TimestampFieldIndex.PublishedEntertainmentConfigurationUTC);
        //public static TimestampFieldIndexPair EntertainmentConfigurationCustomText => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedEntertainmentConfigurationCustomTextUTC, TimestampFieldIndex.PublishedEntertainmentConfigurationCustomTextUTC);
        public static TimestampFieldIndexPair EntertainmentConfigurationMedia => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedEntertainmentConfigurationMediaUTC, TimestampFieldIndex.PublishedEntertainmentConfigurationMediaUTC);
        public static TimestampFieldIndexPair EntertainmentCategories => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedEntertainmentCategoriesUTC, TimestampFieldIndex.PublishedEntertainmentCategoriesUTC);
        public static TimestampFieldIndexPair EntertainmentCategoriesCustomText => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedEntertainmentCategoriesCustomTextUTC, TimestampFieldIndex.PublishedEntertainmentCategoriesCustomTextUTC);
        //public static TimestampFieldIndexPair EntertainmentCategoriesMedia => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedEntertainmentCategoriesMediaUTC, TimestampFieldIndex.PublishedEntertainmentCategoriesMediaUTC);

        // Infrared configuration
        public static TimestampFieldIndexPair InfraredConfiguration => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedInfraredConfigurationUTC, TimestampFieldIndex.PublishedInfraredConfigurationUTC);
        public static TimestampFieldIndexPair InfraredConfigurationCustomText => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedInfraredConfigurationCustomTextUTC, TimestampFieldIndex.PublishedInfraredConfigurationCustomTextUTC);

        // Map
        public static TimestampFieldIndexPair Map => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedMapUTC, TimestampFieldIndex.PublishedMapUTC);
        public static TimestampFieldIndexPair MapCustomText => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedMapCustomTextUTC, TimestampFieldIndex.PublishedMapCustomTextUTC);
        public static TimestampFieldIndexPair MapMedia => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedMapMediaUTC, TimestampFieldIndex.PublishedMapMediaUTC);

        // Menu
        public static TimestampFieldIndexPair Alterations => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedAlterationsUTC, TimestampFieldIndex.PublishedAlterationsUTC);
        public static TimestampFieldIndexPair AlterationsCustomText => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedAlterationsCustomTextUTC, TimestampFieldIndex.PublishedAlterationsCustomTextUTC);
        public static TimestampFieldIndexPair AlterationsMedia => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedAlterationsMediaUTC, TimestampFieldIndex.PublishedAlterationsMediaUTC);
        public static TimestampFieldIndexPair Menu => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedMenuUTC, TimestampFieldIndex.PublishedMenuUTC);
        public static TimestampFieldIndexPair MenuCustomText => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedMenuCustomTextUTC, TimestampFieldIndex.PublishedMenuCustomTextUTC);
        public static TimestampFieldIndexPair MenuMedia => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedMenuMediaUTC, TimestampFieldIndex.PublishedMenuMediaUTC);
        public static TimestampFieldIndexPair Productgroups => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedProductgroupsUTC, TimestampFieldIndex.PublishedProductgroupsUTC);
        public static TimestampFieldIndexPair ProductgroupsCustomText => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedProductgroupsCustomTextUTC, TimestampFieldIndex.PublishedProductgroupsCustomTextUTC);
        public static TimestampFieldIndexPair ProductgroupsMedia => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedProductgroupsMediaUTC, TimestampFieldIndex.PublishedProductgroupsMediaUTC);
        public static TimestampFieldIndexPair Products => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedProductsUTC, TimestampFieldIndex.PublishedProductsUTC);
        public static TimestampFieldIndexPair ProductsCustomText => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedProductsCustomTextUTC, TimestampFieldIndex.PublishedProductsCustomTextUTC);
        public static TimestampFieldIndexPair ProductsMedia => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedProductsMediaUTC, TimestampFieldIndex.PublishedProductsMediaUTC);
        public static TimestampFieldIndexPair Schedules => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedSchedulesUTC, TimestampFieldIndex.PublishedSchedulesUTC);

        // Message group
        public static TimestampFieldIndexPair Messagegroups => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedMessagegroupsUTC, TimestampFieldIndex.PublishedMessagegroupsUTC);

        // Message template
        public static TimestampFieldIndexPair MessageTemplates => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedMessageTemplatesUTC, TimestampFieldIndex.PublishedMessageTemplatesUTC);

        // Point of interest
        public static TimestampFieldIndexPair PointOfInterest => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedPointOfInterestUTC, TimestampFieldIndex.PublishedPointOfInterestUTC);
        public static TimestampFieldIndexPair PointOfInterestCustomText => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedPointOfInterestCustomTextUTC, TimestampFieldIndex.PublishedPointOfInterestCustomTextUTC);
        public static TimestampFieldIndexPair PointOfInterestMedia => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedPointOfInterestMediaUTC, TimestampFieldIndex.PublishedPointOfInterestMediaUTC);
        public static TimestampFieldIndexPair PointOfInterestVenueCategories => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedPointOfInterestVenueCategoriesUTC, TimestampFieldIndex.PublishedPointOfInterestVenueCategoriesUTC);
        public static TimestampFieldIndexPair PointOfInterestVenueCategoriesCustomText => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedPointOfInterestVenueCategoriesCustomTextUTC, TimestampFieldIndex.PublishedPointOfInterestVenueCategoriesCustomTextUTC);

        // Price schedule
        public static TimestampFieldIndexPair PriceSchedule => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedPriceScheduleUTC, TimestampFieldIndex.PublishedPriceScheduleUTC);

        // Room control configuration
        public static TimestampFieldIndexPair RoomControlConfiguration => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedRoomControlConfigurationUTC, TimestampFieldIndex.PublishedRoomControlConfigurationUTC);
        public static TimestampFieldIndexPair RoomControlConfigurationCustomText => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedRoomControlConfigurationCustomTextUTC, TimestampFieldIndex.PublishedRoomControlConfigurationCustomTextUTC);
        public static TimestampFieldIndexPair RoomControlConfigurationMedia => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedRoomControlConfigurationMediaUTC, TimestampFieldIndex.PublishedRoomControlConfigurationMediaUTC);

        // Site
        public static TimestampFieldIndexPair Site => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedSiteUTC, TimestampFieldIndex.PublishedSiteUTC);
        public static TimestampFieldIndexPair SiteCustomText => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedSiteCustomTextUTC, TimestampFieldIndex.PublishedSiteCustomTextUTC);
        public static TimestampFieldIndexPair SiteMedia => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedSiteMediaUTC, TimestampFieldIndex.PublishedSiteMediaUTC);

        // Terminal configuration
        public static TimestampFieldIndexPair TerminalConfiguration => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedTerminalConfigurationUTC, TimestampFieldIndex.PublishedTerminalConfigurationUTC);

        // Terminal
        public static TimestampFieldIndexPair Terminal => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedTerminalUTC, TimestampFieldIndex.PublishedTerminalUTC);

        // UI modes
        public static TimestampFieldIndexPair UIModes => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedUIModesUTC, TimestampFieldIndex.PublishedUIModesUTC);
        public static TimestampFieldIndexPair UIModesCustomText => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedUIModesCustomTextUTC, TimestampFieldIndex.PublishedUIModesCustomTextUTC);
        public static TimestampFieldIndexPair UIModesMedia => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedUIModesMediaUTC, TimestampFieldIndex.PublishedUIModesMediaUTC);

        // UI mode
        public static TimestampFieldIndexPair UIMode => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedUIModeUTC, TimestampFieldIndex.PublishedUIModeUTC);
        public static TimestampFieldIndexPair UIModeCustomText => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedUIModeCustomTextUTC, TimestampFieldIndex.PublishedUIModeCustomTextUTC);
        public static TimestampFieldIndexPair UIModeMedia => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedUIModeMediaUTC, TimestampFieldIndex.PublishedUIModeMediaUTC);

        // UI schedule
        public static TimestampFieldIndexPair UISchedule => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedUIScheduleUTC, TimestampFieldIndex.PublishedUIScheduleUTC);

        // UI theme
        public static TimestampFieldIndexPair UITheme => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedUIThemeUTC, TimestampFieldIndex.PublishedUIThemeUTC);
        public static TimestampFieldIndexPair UIThemeMedia => new TimestampFieldIndexPair(TimestampFieldIndex.ModifiedUIThemeMediaUTC, TimestampFieldIndex.PublishedUIThemeMediaUTC);
    }
}
