﻿using Obymobi.Data;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crave.Api.Logic.Timestamps
{
    public class PointOfInterestVenueCategoriesTimestamp : TimestampBase
    {
        public PointOfInterestVenueCategoriesTimestamp(int pointOfInterestId)
            : base(pointOfInterestId)
        {
        }

        public override EntityField ForeignKeyField
        {
            get { return TimestampFields.PointOfInterestId; }
        }

        public override IEnumerable<TimestampFieldIndexPair> Fields
        {
            get
            {
                return new TimestampFieldIndexPair[]
                {
                    TimestampMetaData.PointOfInterestVenueCategories,
                    TimestampMetaData.PointOfInterestVenueCategoriesCustomText
                };
            }
        }
    }
}
