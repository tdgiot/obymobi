﻿using Obymobi.Data;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crave.Api.Logic.Timestamps
{
    public class CompanyReleasesTimestamp : TimestampBase
    {

        public CompanyReleasesTimestamp(int companyId)
            : base(companyId)
        {
            
        }

        public override EntityField ForeignKeyField
        {
            get { return TimestampFields.CompanyId; }
        }

        public override IEnumerable<TimestampFieldIndexPair> Fields
        {
            get
            {
                return new TimestampFieldIndexPair[]
                {
                    TimestampMetaData.CompanyReleases
                };
            }
        }

        public override Dictionary<string, object> ParameterNamesAndValues
        {
            get
            {
                return new Dictionary<string, object> 
                { 
                    { this.ForeignKeyField.Name, this.ForeignKeyValue }
                };
            }
        }
    }
}
