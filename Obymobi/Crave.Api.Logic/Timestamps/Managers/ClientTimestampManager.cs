﻿using Dionysos.Data.LLBLGen;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crave.Api.Logic.Timestamps.Managers
{
    public class ClientTimestampManager : TimestampManagerBase
    {
        protected override EntityField IdentifierField
        {
            get { return ClientFields.ClientId; }
        }

        protected override IEntityCollection GetEntities(int companyId)
        {
            return EntityCollection.GetMulti<ClientCollection>(ClientFields.CompanyId == companyId, null, this.IdentifierField);
        }

        protected override TimestampBase GetTimestamp(object identifier)
        {
            return new ClientTimestamp((int)identifier);
        }
    }
}
