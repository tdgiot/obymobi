﻿using Dionysos.Data.LLBLGen;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crave.Api.Logic.Timestamps.Managers
{
    public class UIModesTimestampManager : TimestampManagerBase
    {
        protected override EntityField IdentifierField
        {
            get { return UIModeFields.UIModeId; }
        }

        protected override IEntityCollection GetEntities(int companyId)
        {
            return EntityCollection.GetMulti<UIModeCollection>(UIModeFields.CompanyId == companyId, null, this.IdentifierField);
        }

        protected override TimestampBase GetTimestamp(object identifier)
        {
            return new UIModesTimestamp((int)identifier);
        }
    }
}
