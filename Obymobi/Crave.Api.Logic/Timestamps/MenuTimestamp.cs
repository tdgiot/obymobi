﻿using Obymobi.Data;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System.Collections.Generic;

namespace Crave.Api.Logic.Timestamps
{
    public class MenuTimestamp : TimestampBase
    {
        public MenuTimestamp(int menuId) 
            : base(menuId)
        {
        }

        public override EntityField ForeignKeyField
        {
            get { return TimestampFields.MenuId; }
        }

        public override IEnumerable<TimestampFieldIndexPair> Fields
        {
            get
            {
                return new TimestampFieldIndexPair[]
                {
                    TimestampMetaData.Alterations,
                    TimestampMetaData.AlterationsCustomText,
                    TimestampMetaData.AlterationsMedia,
                    TimestampMetaData.Menu,
                    TimestampMetaData.MenuCustomText,
                    TimestampMetaData.MenuMedia,
                    TimestampMetaData.Productgroups,
                    TimestampMetaData.ProductgroupsCustomText,
                    TimestampMetaData.ProductgroupsMedia,
                    TimestampMetaData.Products,
                    TimestampMetaData.ProductsCustomText,
                    TimestampMetaData.ProductsMedia,
                    TimestampMetaData.Schedules
                };
            }
        }
    }
}
