﻿using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Api.Logic.Timestamps
{
    public class AdvertisementConfigurationTimestamp : TimestampBase
    {
        public AdvertisementConfigurationTimestamp(int advertisementConfigurationId)
            : base(advertisementConfigurationId)
        {
        }

        public override EntityField ForeignKeyField
        {
            get { return TimestampFields.AdvertisementConfigurationId; }
        }

        public override IEnumerable<TimestampFieldIndexPair> Fields
        {
            get 
            {
                return new TimestampFieldIndexPair[]
                {
                    TimestampMetaData.AdvertisementConfiguration,
                    TimestampMetaData.AdvertisementConfigurationCustomText,
                    TimestampMetaData.AdvertisementConfigurationMedia
                };
            }
        }
    }
}
