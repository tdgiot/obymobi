﻿using Obymobi.Data;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crave.Api.Logic.Timestamps
{
    public class TerminalTimestamp : TimestampBase
    {
        public TerminalTimestamp(int terminalId)
            : base(terminalId)
        {
        }

        public override EntityField ForeignKeyField
        {
            get { return TimestampFields.TerminalId; }
        }

        public override IEnumerable<TimestampFieldIndexPair> Fields
        {
            get
            {
                return new TimestampFieldIndexPair[]
                {
                    TimestampMetaData.Terminal
                };
            }
        }
    }
}
