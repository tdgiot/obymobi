﻿using Obymobi.Data;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crave.Api.Logic.Timestamps
{
    public class MapTimestamp : TimestampBase
    {
        public MapTimestamp(int mapId)
            : base(mapId)
        {
        }

        public override EntityField ForeignKeyField
        {
            get { return TimestampFields.MapId; }
        }

        public override IEnumerable<TimestampFieldIndexPair> Fields
        {
            get
            {
                return new TimestampFieldIndexPair[]
                {
                    TimestampMetaData.Map
                };
            }
        }
    }
}
