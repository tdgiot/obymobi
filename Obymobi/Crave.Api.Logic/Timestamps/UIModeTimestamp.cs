﻿using Obymobi.Data;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crave.Api.Logic.Timestamps
{
    public class UIModeTimestamp : TimestampBase
    {
        public UIModeTimestamp(int uiModeId)
            : base(uiModeId)
        {
        }

        public override EntityField ForeignKeyField
        {
            get { return TimestampFields.UIModeId; }
        }

        public override IEnumerable<TimestampFieldIndexPair> Fields
        {
            get
            {
                return new TimestampFieldIndexPair[]
                {
                    TimestampMetaData.UIMode,
                    TimestampMetaData.UIModeCustomText,
                    TimestampMetaData.UIModeMedia
                };
            }
        }
    }
}
