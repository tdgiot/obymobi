﻿using Obymobi.Data;

namespace Crave.Api.Logic.Timestamps
{
    public class TimestampFieldIndexPair
    {
        public TimestampFieldIndexPair(TimestampFieldIndex modifiedField, TimestampFieldIndex publishedField)
        {
            this.ModifiedField = modifiedField;
            this.PublishedField = publishedField;
        }

        public TimestampFieldIndex ModifiedField;
        public TimestampFieldIndex PublishedField;
    }
}
