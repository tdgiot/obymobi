﻿using Obymobi.Data;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crave.Api.Logic.Timestamps
{
    public class SiteTimestamp : TimestampBase
    {
        private readonly string cultureCode;

        public SiteTimestamp(int siteId, string cultureCode)
            : base(siteId)
        {
            this.cultureCode = cultureCode;
        }

        public override EntityField ForeignKeyField
        {
            get { return TimestampFields.SiteId; }
        }

        public override IEnumerable<TimestampFieldIndexPair> Fields
        {
            get
            {
                return new TimestampFieldIndexPair[]
                {
                    TimestampMetaData.Site,
                    TimestampMetaData.SiteCustomText,
                    TimestampMetaData.SiteMedia
                };
            }
        }

        public override Dictionary<string, object> ParameterNamesAndValues
        {
            get
            {
                return new Dictionary<string, object> 
                { 
                    { this.ForeignKeyField.Name, this.ForeignKeyValue },
                    { "CultureCode", this.cultureCode }
                };
            }
        }
    }
}
