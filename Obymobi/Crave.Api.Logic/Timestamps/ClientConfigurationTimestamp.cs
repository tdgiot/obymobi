﻿using Obymobi.Data;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crave.Api.Logic.Timestamps
{
    public class ClientConfigurationTimestamp : TimestampBase
    {
        public ClientConfigurationTimestamp(int clientConfigurationId)
            : base(clientConfigurationId)
        {
        }

        public override EntityField ForeignKeyField
        {
            get { return TimestampFields.ClientConfigurationId; }
        }

        public override IEnumerable<TimestampFieldIndexPair> Fields
        {
            get
            {
                return new TimestampFieldIndexPair[]
                {
                    TimestampMetaData.ClientConfiguration,
                    TimestampMetaData.ClientConfigurationCustomText,
                    TimestampMetaData.ClientConfigurationMedia
                };
            }
        }
    }
}
