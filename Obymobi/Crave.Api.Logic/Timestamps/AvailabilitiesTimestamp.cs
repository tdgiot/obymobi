﻿using System.Collections.Generic;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Api.Logic.Timestamps
{
    public class AvailabilitiesTimestamp : TimestampBase
    {
        public AvailabilitiesTimestamp(int companyId)
            : base(companyId)
        {
        }

        public override EntityField ForeignKeyField
        {
            get { return TimestampFields.CompanyId; }
        }

        public override IEnumerable<TimestampFieldIndexPair> Fields
        {
            get
            {
                return new TimestampFieldIndexPair[]
                {
                    TimestampMetaData.Availabilities,
                    TimestampMetaData.AvailabilitiesCustomText
                };
            }
        }
    }
}
