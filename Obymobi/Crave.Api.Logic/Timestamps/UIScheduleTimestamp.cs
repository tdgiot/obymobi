﻿using Obymobi.Data;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crave.Api.Logic.Timestamps
{
    public class UIScheduleTimestamp : TimestampBase
    {
        public UIScheduleTimestamp(int uiScheduleId)
            : base(uiScheduleId)
        {
        }

        public override EntityField ForeignKeyField
        {
            get { return TimestampFields.UIScheduleId; }
        }

        public override IEnumerable<TimestampFieldIndexPair> Fields
        {
            get
            {
                return new TimestampFieldIndexPair[]
                {
                    TimestampMetaData.UISchedule
                };
            }
        }
    }
}
