﻿using Obymobi.Data;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crave.Api.Logic.Timestamps
{
    public class InfraredConfigurationTimestamp : TimestampBase
    {
        public InfraredConfigurationTimestamp(int infraredConfigurationId)
            : base(infraredConfigurationId)
        {
        }

        public override EntityField ForeignKeyField
        {
            get { return TimestampFields.InfraredConfigurationId; }
        }

        public override IEnumerable<TimestampFieldIndexPair> Fields
        {
            get
            {
                return new TimestampFieldIndexPair[]
                {
                    TimestampMetaData.InfraredConfiguration,
                    TimestampMetaData.InfraredConfigurationCustomText
                };
            }
        }
    }
}
