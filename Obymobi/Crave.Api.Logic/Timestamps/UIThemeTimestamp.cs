﻿using Obymobi.Data;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crave.Api.Logic.Timestamps
{
    public class UIThemeTimestamp : TimestampBase
    {
        public UIThemeTimestamp(int uiThemeId)
            : base(uiThemeId)
        {
        }

        public override EntityField ForeignKeyField
        {
            get { return TimestampFields.UIThemeId; }
        }

        public override IEnumerable<TimestampFieldIndexPair> Fields
        {
            get
            {
                return new TimestampFieldIndexPair[]
                {
                    TimestampMetaData.UITheme,
                    TimestampMetaData.UIThemeMedia
                };
            }
        }
    }
}
