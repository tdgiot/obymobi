﻿using Obymobi.Data;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crave.Api.Logic.Timestamps
{
    public class RoomControlConfigurationTimestamp : TimestampBase
    {
        public RoomControlConfigurationTimestamp(int roomControlConfigurationId)
            : base(roomControlConfigurationId)
        {
        }

        public override EntityField ForeignKeyField
        {
            get { return TimestampFields.RoomControlConfigurationId; }
        }

        public override IEnumerable<TimestampFieldIndexPair> Fields
        {
            get
            {
                return new TimestampFieldIndexPair[]
                {
                    TimestampMetaData.RoomControlConfiguration,
                    TimestampMetaData.RoomControlConfigurationCustomText,
                    TimestampMetaData.RoomControlConfigurationMedia
                };
            }
        }
    }
}
