﻿using System.Collections.Generic;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Api.Logic.Timestamps
{
    public class MessageTemplatesTimestamp : TimestampBase
    {
        public MessageTemplatesTimestamp(int terminalId)
            : base(terminalId)
        { }

        public override EntityField ForeignKeyField
        {
            get { return TimestampFields.TerminalId; }
        }

        public override IEnumerable<TimestampFieldIndexPair> Fields
        {
            get
            {
                return new TimestampFieldIndexPair[]
                {
                    TimestampMetaData.MessageTemplates
                };
            }
        }
    }
}
