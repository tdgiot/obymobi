﻿using Obymobi.Data;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crave.Api.Logic.Timestamps
{
    public class EntertainmentConfigurationTimestamp : TimestampBase
    {
        public EntertainmentConfigurationTimestamp(int entertainmentConfigurationId)
            : base(entertainmentConfigurationId)
        {
        }

        public override EntityField ForeignKeyField
        {
            get { return TimestampFields.EntertainmentConfigurationId; }
        }

        public override IEnumerable<TimestampFieldIndexPair> Fields
        {
            get
            {
                return new TimestampFieldIndexPair[]
                {
                    TimestampMetaData.EntertainmentConfiguration,
                    //TimestampMetaData.EntertainmentConfigurationCustomText,
                    TimestampMetaData.EntertainmentConfigurationMedia,
                    TimestampMetaData.EntertainmentCategories,
                    TimestampMetaData.EntertainmentCategoriesCustomText,
                    //TimestampMetaData.EntertainmentCategoriesMedia
                };
            }
        }
    }
}
