﻿using System;
using Crave.Api.Logic.Requests;
using Crave.Api.Logic.Timestamps;

namespace Crave.Api.Logic.UseCases
{
    public class PublishApiOperationUseCase
    {
        public string Execute(PublishApiOperationRequest request)
        {
            string baseUrl = request.BaseUrl;
            ApiOperation apiOperation = request.ApiOperation;
            TimestampBase timestamp = request.Timestamp;
            ApiCallAuthentication authentication = request.Authentication;
            bool force = request.Force;

            ApiPublishing apiPublishing = new ApiPublishing
            {
                BaseUrl = baseUrl,
                ApiOperation = apiOperation,
                Timestamp = timestamp,
                Authentication = authentication,
                Force = force
            };

            PublishResult<object> publishResult = apiPublishing.Execute();
            if (publishResult == null)
            {
                throw new InvalidOperationException(string.Format("Something went wrong whilst trying to publish API operation '{0}' on url '{1}'. Publish result is null!", apiOperation.RequestName, apiPublishing.Url));
            }
            else if (publishResult.Code != 100)
            {
                throw new InvalidOperationException(string.Format("Something went wrong whilst trying to publish API operation '{0}' on url '{1}'. Publish result code is '{2}'!", apiOperation.RequestName, apiPublishing.Url, publishResult.Code));
            }

            return apiPublishing.Url;
        }
    }
}
