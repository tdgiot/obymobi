﻿using System.Collections.Generic;
using Crave.Api.Logic.Requests;
using Dionysos;
using Dionysos.Security.Cryptography;
using Obymobi.Api.Logic.Repositories;
using Obymobi.Data.EntityClasses;

namespace Crave.Api.Logic.UseCases
{
    public class GetApiCallAuthenticationUseCase
    {
        private readonly ClientRepository clientRepository;
        private readonly TerminalRepository terminalRepository;
        private readonly DeviceRepository deviceRepository;

        public GetApiCallAuthenticationUseCase()
        {
            clientRepository = new ClientRepository();
            terminalRepository = new TerminalRepository();
            deviceRepository = new DeviceRepository();
        }

        public ApiCallAuthentication Execute(GetApiCallAuthenticationRequest request)
        {
            ApiCallAuthentication authentication;

            int companyId = request.CompanyId;
            ApiCallerType callerType = request.CallerType;
            Dictionary<string, object> parameterNamesAndValues = request.ParameterNamesAndValues;

            if (callerType == ApiCallerType.Client)
            {
                authentication = GetAuthenticationForClient(companyId, parameterNamesAndValues);
            }
            else
            {
                authentication = GetAuthenticationForTerminal(companyId, parameterNamesAndValues);
            }

            return authentication;
        }

        private ApiCallAuthentication GetAuthenticationForClient(int companyId, Dictionary<string, object> parameterNamesAndValues)
        {
            ClientEntity client = clientRepository.GetFirstClientWithDeviceTokenForCompany(companyId);
            if (client == null)
            {
                client = CreateClientWithDeviceTokenForCompany(companyId);
            }

            return GetAuthenticationForDevice(client.DeviceEntity, parameterNamesAndValues);
        }

        private ApiCallAuthentication GetAuthenticationForTerminal(int companyId, Dictionary<string, object> parameterNamesAndValues)
        {
            TerminalEntity terminal = terminalRepository.GetFirstTerminalWithDeviceTokenForCompany(companyId);
            if (terminal == null)
            {
                terminal = CreateTerminalWithDeviceTokenForCompany(companyId);
            }

            return GetAuthenticationForDevice(terminal.DeviceEntity, parameterNamesAndValues);
        }

        private static ApiCallAuthentication GetAuthenticationForDevice(DeviceEntity device, Dictionary<string, object> parameterNamesAndValues)
        {
            string identifier = device.Identifier;
            string deviceToken = device.Token;

            return new ApiCallAuthentication
            {
                Identifier = identifier,
                DeviceToken = Cryptographer.DecryptUsingCBC(deviceToken),
                ParameterNamesAndValues = parameterNamesAndValues
            };
        }

        private ClientEntity CreateClientWithDeviceTokenForCompany(int companyId)
        {
            DeviceEntity device = GetOrCreateDevice(string.Format("dummy-client-{0}", companyId));
            ClientEntity client = CreateClient(companyId, device);

            clientRepository.Save(client, true);

            return client;
        }

        private TerminalEntity CreateTerminalWithDeviceTokenForCompany(int companyId)
        {
            DeviceEntity device = GetOrCreateDevice(string.Format("dummy-terminal-{0}", companyId));
            TerminalEntity terminal = CreateTerminal(companyId, device);

            terminalRepository.Save(terminal, true);

            return terminal;
        }

        private DeviceEntity GetOrCreateDevice(string identifier)
        {
            return deviceRepository.GetDevice(identifier) ?? CreateDeviceEntity(identifier);
        }

        private static DeviceEntity CreateDeviceEntity(string identifier)
        {
            DeviceEntity deviceEntity = new DeviceEntity();
            deviceEntity.Token = Cryptographer.EncryptUsingCBC(RandomUtil.GetRandomLowerCaseString(16));
            deviceEntity.Identifier = identifier;
            deviceEntity.DeviceModel = Obymobi.Enums.DeviceModel.Unknown;
            deviceEntity.AuthorizerToUse = null;

            return deviceEntity;
        }

        private static ClientEntity CreateClient(int companyId, DeviceEntity deviceEntity)
        {
            ClientEntity clientEntity = new ClientEntity();
            clientEntity.CompanyId = companyId;
            clientEntity.DeviceEntity = deviceEntity;
            clientEntity.AuthorizerToUse = null;

            return clientEntity;
        }

        private static TerminalEntity CreateTerminal(int companyId, DeviceEntity deviceEntity)
        {
            TerminalEntity terminalEntity = new TerminalEntity();
            terminalEntity.CompanyId = companyId;
            terminalEntity.DeviceEntity = deviceEntity;
            terminalEntity.Name = deviceEntity.Identifier;
            terminalEntity.AuthorizerToUse = null;

            return terminalEntity;
        }
    }
}
