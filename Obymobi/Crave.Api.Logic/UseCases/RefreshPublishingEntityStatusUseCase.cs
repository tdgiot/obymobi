﻿using Crave.Api.Logic.Requests;
using Obymobi.Data.EntityClasses;
using System;
using System.Linq;
using Dionysos;
using Obymobi.Enums;

namespace Crave.Api.Logic.UseCases
{
    public class RefreshPublishingEntityStatusUseCase
    {
        public bool Execute(RefreshPublishingEntityStatusRequest request)
        {
            PublishingEntity publishingEntity = new PublishingEntity(request.PublishingId);
            if (publishingEntity.IsNew)
                return false;

            if (!publishingEntity.PublishingItemCollection.Any())
                return false;

            if (publishingEntity.PublishingItemCollection.Any(x => x.Status == PublishingItemStatus.Failure))
            {
                // There is at least one item that failed
                publishingEntity.Status = PublishingStatus.Failure;
            }
            else
            { 
                if (publishingEntity.PublishingItemCollection.Any(x => x.PublishedTicks != publishingEntity.PublishingItemCollection[0].PublishedTicks))
                {
                    // Not all ticks are the same
                    publishingEntity.Status = PublishingStatus.SuccessAfterRetry;
                }
                else
                {
                    // All ticks are the same
                    publishingEntity.Status = PublishingStatus.Success;
                }
            }

            publishingEntity.StatusText = publishingEntity.Status.ToString();

            return publishingEntity.Save();
        }
    }
}
