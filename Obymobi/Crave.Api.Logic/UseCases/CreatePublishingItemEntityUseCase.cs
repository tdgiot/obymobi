﻿using System;
using Crave.Api.Logic.Requests;
using Dionysos;
using Obymobi.Data.EntityClasses;

namespace Crave.Api.Logic.UseCases
{
    public class CreatePublishingItemEntityUseCase
    {
        public bool Execute(CreatePublishingItemEntityRequest request)
        {
            PublishingItemEntity publishingItemEntity = new PublishingItemEntity
            {
                PublishingId = request.PublishingEntity.PublishingId,
                ApiOperation = request.ApiOperation,
                PublishedUTC = request.Timestamp.FromUnixTime(),
                PublishedTicks = request.Timestamp,
                Status = request.Status,
                StatusText = request.Status.ToString(),
                Log = request.Log
            };

            int counter = 1;
            foreach (string parameterName in request.ParameterNamesAndValues.Keys)
            {
                string parameterValue = request.ParameterNamesAndValues[parameterName].ToString();
                if (counter == 1)
                {
                    publishingItemEntity.Parameter1Name = parameterName;
                    publishingItemEntity.Parameter1Value = parameterValue;
                }
                else
                {
                    publishingItemEntity.Parameter2Name = parameterName;
                    publishingItemEntity.Parameter2Value = parameterValue;
                }
                counter++;
            }

            return publishingItemEntity.Save();
        }
    }
}
