﻿using System;
using Crave.Api.Logic.Requests;
using Dionysos;
using Obymobi.Data.EntityClasses;

namespace Crave.Api.Logic.UseCases
{
    public class UpdatePublishingItemEntityUseCase
    {
        public bool Execute(UpdatePublishingItemEntityRequest request)
        {
            PublishingItemEntity publishingItemEntity = request.PublishingItemEntity;

            publishingItemEntity.PublishedUTC = request.Timestamp.FromUnixTime();
            publishingItemEntity.PublishedTicks = request.Timestamp;
            publishingItemEntity.Status = request.Status;
            publishingItemEntity.StatusText = request.Status.ToString();
            publishingItemEntity.Log = request.Log;

            return publishingItemEntity.Save();
        }
    }
}
