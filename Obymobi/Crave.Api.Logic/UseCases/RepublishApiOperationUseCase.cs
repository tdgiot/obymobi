﻿using System;
using System.Linq;
using System.Reflection;
using Crave.Api.Logic.Requests;
using Crave.Api.Logic.Requests.Publishing;
using Dionysos;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;

namespace Crave.Api.Logic.UseCases
{
    public class RepublishApiOperationUseCase
    {
        #region Fields

        private readonly GetApiCallAuthenticationUseCase getApiCallAuthenticationUseCase = new GetApiCallAuthenticationUseCase();
        private readonly PublishApiOperationUseCase publishApiOperationUseCase = new PublishApiOperationUseCase();
        private readonly UpdatePublishingItemEntityUseCase updatePublishingItemEntityUseCase = new UpdatePublishingItemEntityUseCase();
        private readonly RefreshPublishingEntityStatusUseCase refreshPublishingEntityStatusUseCase = new RefreshPublishingEntityStatusUseCase();

        #endregion

        #region Methods

        public void Execute(RepublishApiOperationRequest request)
        {
            PublishingItemEntity publishingItemEntity = request.PublishingItemEntity;
            int companyId = request.CompanyId;
            UserEntity userEntity = request.UserEntity;
            string baseUrl = request.BaseUrl;
            bool force = request.Force;

            PublishRequestBase originalPublishRequest = this.CreateRepublishRequest(publishingItemEntity.PublishingEntity, companyId, userEntity, baseUrl, true);
            if (originalPublishRequest == null)
                return;

            // Get the API operation from the original publish request
            ApiOperation apiOperation = originalPublishRequest.ApiOperations.FirstOrDefault(x => x.RequestName.Equals(publishingItemEntity.ApiOperation));
            if (apiOperation != null)
            {
                // Publish the API operation
                this.Publish(originalPublishRequest, publishingItemEntity, apiOperation, force);
            }
            else
            { 
                // Or get it from its related requests
                foreach (PublishRequestBase relatedRequest in originalPublishRequest.RelatedRequests)
                {
                    relatedRequest.CompanyId = originalPublishRequest.CompanyId;
                    relatedRequest.BaseUrl = originalPublishRequest.BaseUrl;
                    relatedRequest.User = originalPublishRequest.User;
                    relatedRequest.Force = originalPublishRequest.Force;

                    apiOperation = relatedRequest.ApiOperations.FirstOrDefault(x => x.RequestName.Equals(publishingItemEntity.ApiOperation));
                    if (apiOperation != null && relatedRequest.GetParameterString() == publishingItemEntity.GetParameterString())
                    {
                        // Publish the API operation
                        this.Publish(relatedRequest, publishingItemEntity, apiOperation, force);
                        break;
                    }
                }
            }

            if (apiOperation == null)
                return;

            // Refresh the status of the publishing item
            RefreshPublishingEntityStatusRequest refreshPublishingEntityStatusRequest = new RefreshPublishingEntityStatusRequest { PublishingId = publishingItemEntity.PublishingEntity.PublishingId };
            this.refreshPublishingEntityStatusUseCase.Execute(refreshPublishingEntityStatusRequest);
        }

        private PublishRequestBase CreateRepublishRequest(PublishingEntity originalPublishingEntity, int companyId, UserEntity userEntity, string baseUrl, bool force)
        {
            string publishRequestFullTypeName = originalPublishingEntity.PublishRequest;
            Assembly publishRequestAssembly = typeof(PublishMenu).Assembly;
            Type publishRequestType = publishRequestAssembly.GetType(publishRequestFullTypeName);

            PublishRequestBase publishRequest = Activator.CreateInstance(publishRequestType) as PublishRequestBase;
            if (publishRequest == null)
                return null;

            // Set the name and value of parameter 1
            if (!originalPublishingEntity.Parameter1Name.IsNullOrWhiteSpace() && !originalPublishingEntity.Parameter1Value.IsNullOrWhiteSpace())
            {
                MemberInfo[] memberInfo = publishRequestType.GetMember(originalPublishingEntity.Parameter1Name);
                if (memberInfo.Length > 0)
                {
                    Type propertyType = ((PropertyInfo)memberInfo[0]).PropertyType;
                    object value = Convert.ChangeType(originalPublishingEntity.Parameter1Value, propertyType);
                    publishRequestType.InvokeMember(originalPublishingEntity.Parameter1Name, BindingFlags.Instance | BindingFlags.Public | BindingFlags.SetProperty, Type.DefaultBinder, publishRequest, new object[] { value });
                }
            }

            // (Optional) Set the name and value of parameter 2
            if (!originalPublishingEntity.Parameter2Name.IsNullOrWhiteSpace() && !originalPublishingEntity.Parameter2Value.IsNullOrWhiteSpace())
            {
                MemberInfo[] memberInfo = publishRequestType.GetMember(originalPublishingEntity.Parameter2Name);
                if (memberInfo.Length > 0)
                {
                    Type propertyType = ((PropertyInfo)memberInfo[0]).PropertyType;
                    object value = Convert.ChangeType(originalPublishingEntity.Parameter2Value, propertyType);
                    publishRequestType.InvokeMember(originalPublishingEntity.Parameter2Name, BindingFlags.Instance | BindingFlags.Public | BindingFlags.SetProperty, Type.DefaultBinder, publishRequest, new object[] { value });
                }
            }

            // Set some default params
            publishRequest.CompanyId = companyId;
            publishRequest.User = userEntity;
            publishRequest.BaseUrl = baseUrl;
            publishRequest.Force = force;

            return publishRequest;
        }

        private bool Publish(PublishRequestBase request, PublishingItemEntity publishingItemEntity, ApiOperation apiOperation, bool force)
        {
            bool success = true;

            // Get the authentication
            ApiCallAuthentication authentication = this.GetApiCallAuthentication(request);

            // Publish the API operation
            string log;
            PublishingItemStatus status;
            success = PublishApiOperation(request, success, authentication, apiOperation, out log, out status);

            // Update the publishing item
            UpdatePublishingItemEntityRequest updatePublishingItemEntityRequest = new UpdatePublishingItemEntityRequest
            {
                PublishingItemEntity = publishingItemEntity,
                Timestamp = authentication.Timestamp,
                Status = status,
                Log = log
            };
            this.updatePublishingItemEntityUseCase.Execute(updatePublishingItemEntityRequest);

            return success;
        }

        #region Helper methods to execute use-cases

        private ApiCallAuthentication GetApiCallAuthentication(PublishRequestBase request)
        {
            GetApiCallAuthenticationRequest getApiCallAuthenticationRequest = new GetApiCallAuthenticationRequest
            {
                CompanyId = request.CompanyId,
                CallerType = request.CallerType,
                ParameterNamesAndValues = request.ParameterNamesAndValues
            };
            return this.getApiCallAuthenticationUseCase.Execute(getApiCallAuthenticationRequest);
        }

        private bool PublishApiOperation(PublishRequestBase request, bool success, ApiCallAuthentication authentication, ApiOperation apiOperation, out string log, out PublishingItemStatus status)
        {
            log = string.Empty;
            try
            {
                PublishApiOperationRequest publishApiOperationRequest = new PublishApiOperationRequest
                {
                    BaseUrl = request.BaseUrl,
                    ApiOperation = apiOperation,
                    Timestamp = request.Timestamp,
                    Authentication = authentication,
                    Force = request.Force
                };
                string url = this.publishApiOperationUseCase.Execute(publishApiOperationRequest);

                status = PublishingItemStatus.Success;
                log = string.Format("API operation '{0}' successfully published on url '{1}'.", apiOperation.RequestName, url);
            }
            catch (Exception ex)
            {
                status = PublishingItemStatus.Failure;
                success = false;
                log = string.Format("Something went wrong whilst trying to publish API operation '{0}'. Exception: '{1}'\r\n", apiOperation.RequestName, ex.ToString());
            }

            return success;
        }

        #endregion

        #endregion
    }
}
