﻿using System;
using System.Linq;
using System.Diagnostics;
using Crave.Api.Logic.Requests;
using Crave.Api.Logic.Requests.Publishing;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;

namespace Crave.Api.Logic.UseCases
{
    public class PublishUseCase
    {
        #region Fields

        private Stopwatch stopwatch;
        private PublishingEntity publishingEntity;

        // TODO Use dependency injection
        private readonly GetApiCallAuthenticationUseCase getApiCallAuthenticationUseCase = new GetApiCallAuthenticationUseCase();
        private readonly PublishApiOperationUseCase publishApiOperationUseCase = new PublishApiOperationUseCase();
        private readonly CreatePublishingEntityUseCase createPublishingEntityUseCase = new CreatePublishingEntityUseCase();
        private readonly CreatePublishingItemEntityUseCase createPublishingItemEntityUseCase = new CreatePublishingItemEntityUseCase();
        private readonly UpdatePublishingEntityUseCase updatePublishingEntityUseCase = new UpdatePublishingEntityUseCase();
        private readonly UpdatePublishingItemEntityUseCase updatePublishingItemEntityUseCase = new UpdatePublishingItemEntityUseCase();
        private readonly RefreshPublishingEntityStatusUseCase refreshPublishingEntityStatusUseCase = new RefreshPublishingEntityStatusUseCase();

        #endregion

        #region Methods

        public PublishingEntity Execute(PublishRequestBase publishRequest)
        {
            PublishingStatus publishingStatus = PublishingStatus.Success;
            bool force = publishRequest.Force;

            // Create the publishing entity
            CreatePublishingEntityRequest createPublishingEntityRequest = new CreatePublishingEntityRequest
            {
                PublishRequest = publishRequest,
                Status = publishingStatus,
                ParameterNamesAndValues = publishRequest.ParameterNamesAndValues,
                User = publishRequest.User
            };
            this.publishingEntity = this.createPublishingEntityUseCase.Execute(createPublishingEntityRequest);

            // Start a new stopwatch
            this.stopwatch = Stopwatch.StartNew();

            // Get the authentication
            ApiCallAuthentication authentication = this.GetApiCallAuthentication(publishRequest);

            if (!this.HasDirtyApiOperations(publishRequest) && !publishRequest.Force)
            {
                publishingStatus = PublishingStatus.NothingToPublish;
            }
            else if (!this.PublishApiOperations(publishRequest, authentication, force))
            {
                publishingStatus = PublishingStatus.Failure;
            }
            else if (publishRequest.RelatedRequests.Any())
            {
                // Publish the data for the related requests
                foreach (PublishRequestBase relatedPublishRequest in publishRequest.RelatedRequests)
                {
                    relatedPublishRequest.CompanyId = publishRequest.CompanyId;
                    relatedPublishRequest.BaseUrl = publishRequest.BaseUrl;
                    relatedPublishRequest.User = publishRequest.User;
                    relatedPublishRequest.Force = publishRequest.Force;

                    if (!this.PublishApiOperations(relatedPublishRequest, authentication, force))
                    {
                        publishingStatus = PublishingStatus.Failure;
                    }
                }
            }

            this.stopwatch.Stop();
            string log = string.Format("Publishing finished in {0}ms.", this.stopwatch.ElapsedMilliseconds);

            // Update the status of the publishing entity
            UpdatePublishingEntityRequest updatePublishingEntityRequest = new UpdatePublishingEntityRequest
            {
                PublishingEntity = this.publishingEntity,
                Status = publishingStatus,
                Log = log
            };
            return this.updatePublishingEntityUseCase.Execute(updatePublishingEntityRequest);
        }

        private bool HasDirtyApiOperations(PublishRequestBase request)
        {
            bool hasDirtyApiOperations = false;

            foreach (ApiOperation apiOperation in request.ApiOperations)
            {
                if (apiOperation.TimestampFieldIndexes != null && request.Timestamp.IsFieldDirty(apiOperation.TimestampFieldIndexes))
                {
                    hasDirtyApiOperations = true;
                    break;
                }
            }

            return hasDirtyApiOperations;
        }

        private bool PublishApiOperations(PublishRequestBase request, ApiCallAuthentication authentication, bool force)
        {
            bool success = true;

            // Publish the individual API operations
            foreach (ApiOperation apiOperation in request.ApiOperations)
            {
                if (apiOperation.TimestampFieldIndexes != null && !request.Timestamp.IsFieldDirty(apiOperation.TimestampFieldIndexes) && !request.Force)
                {
                    // API operation is not dirty, skip
                    continue;
                }

                string log;
                PublishingItemStatus status;
                success = this.PublishApiOperation(request, success, authentication, apiOperation, out log, out status);

                // Create the publishing item
                CreatePublishingItemEntityRequest createPublishingItemEntityRequest = new CreatePublishingItemEntityRequest
                {
                    PublishingEntity = this.publishingEntity,
                    ApiOperation = apiOperation.RequestName,
                    ParameterNamesAndValues = request.ParameterNamesAndValues,
                    Timestamp = authentication.Timestamp,
                    Status = status,
                    Log = log
                };
                this.createPublishingItemEntityUseCase.Execute(createPublishingItemEntityRequest);
            }

            return success;
        }

        private bool PublishApiOperation(PublishRequestBase request, bool success, ApiCallAuthentication authentication, ApiOperation apiOperation, out string log, out PublishingItemStatus status)
        {
            log = string.Empty;
            try
            {
                PublishApiOperationRequest publishApiOperationRequest = new PublishApiOperationRequest
                {
                    BaseUrl = request.BaseUrl,
                    ApiOperation = apiOperation,
                    Timestamp = request.Timestamp,
                    Authentication = authentication,
                    Force = request.Force
                };
                string url = this.publishApiOperationUseCase.Execute(publishApiOperationRequest);

                status = PublishingItemStatus.Success;
                log = string.Format("API operation '{0}' successfully published on url '{1}'.", apiOperation.RequestName, url);
            }
            catch (Exception ex)
            {
                status = PublishingItemStatus.Failure;
                success = false;
                log = string.Format("Something went wrong whilst trying to publish API operation '{0}'. Exception: '{1}'\r\n", apiOperation.RequestName, ex.ToString());
            }

            return success;
        }

        #region Helper methods to execute use-cases

        private ApiCallAuthentication GetApiCallAuthentication(PublishRequestBase request)
        {
            GetApiCallAuthenticationRequest getApiCallAuthenticationRequest = new GetApiCallAuthenticationRequest
            {
                CompanyId = request.CompanyId,
                CallerType = request.CallerType,
                ParameterNamesAndValues = request.ParameterNamesAndValues
            };
            return this.getApiCallAuthenticationUseCase.Execute(getApiCallAuthenticationRequest);
        }

        #endregion

        #endregion
    }
}
