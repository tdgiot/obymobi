﻿using Crave.Api.Logic.Requests;
using Obymobi.Data.EntityClasses;
using System;
using System.Linq;
using Dionysos;

namespace Crave.Api.Logic.UseCases
{
    public class CreatePublishingEntityUseCase
    {
        public PublishingEntity Execute(CreatePublishingEntityRequest request)
        {
            DateTime utcNow = DateTime.UtcNow;

            PublishingEntity publishingEntity = new PublishingEntity
            {
                PublishRequest = request.PublishRequest.GetType().ToString(),
                PublishedUTC = utcNow,
                PublishedTicks = utcNow.ToUnixTime(),
                UserId = request.User.UserId,
                Username = request.User.Username,
                Status = request.Status,
                StatusText = request.Status.ToString().FormatPascalCase(),
                Log = request.Log
            };

            if (request.ParameterNamesAndValues.Any())
            {
                string[] parameterNames = request.ParameterNamesAndValues.Keys.ToArray();
                object[] parameterValues = request.ParameterNamesAndValues.Values.ToArray();

                if (parameterNames.Length > 0 && parameterValues.Length > 0)
                {
                    publishingEntity.Parameter1Name = parameterNames[0];
                    publishingEntity.Parameter1Value = parameterValues[0].ToString();
                }

                if (parameterNames.Length > 1 && parameterValues.Length > 1)
                {
                    publishingEntity.Parameter2Name = parameterNames[1];
                    publishingEntity.Parameter2Value = parameterValues[1].ToString();
                }
            }

            publishingEntity.Save();

            return publishingEntity;
        }
    }
}
