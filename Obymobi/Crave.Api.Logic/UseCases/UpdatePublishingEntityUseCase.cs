﻿using Crave.Api.Logic.Requests;
using Obymobi.Data.EntityClasses;
using System;
using System.Linq;
using Dionysos;

namespace Crave.Api.Logic.UseCases
{
    public class UpdatePublishingEntityUseCase
    {
        public PublishingEntity Execute(UpdatePublishingEntityRequest request)
        {
            PublishingEntity publishingEntity = request.PublishingEntity;

            publishingEntity.Status = request.Status;
            publishingEntity.StatusText = request.Status.ToString().FormatPascalCase();
            publishingEntity.Log = request.Log;
            publishingEntity.Save();

            return publishingEntity;
        }
    }
}
