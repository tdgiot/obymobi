﻿using Crave.Api.Logic.Timestamps;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Crave.Api.Logic.Requests
{
    public class PublishApiOperationRequest
    {
        public string BaseUrl { get; set; }

        public ApiOperation ApiOperation { get; set; }

        public TimestampBase Timestamp { get; set; }

        public ApiCallAuthentication Authentication { get; set; }

        public bool Force { get; set; }
    }
}
