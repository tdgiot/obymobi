﻿namespace Crave.Api.Logic.Requests
{
    public class RefreshPublishingEntityStatusRequest
    {
        public int PublishingId { get; set; }
    }
}
