﻿using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Crave.Api.Logic.Timestamps;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crave.Api.Logic.Requests.Publishing
{
    public class PublishTerminalConfiguration : PublishRequestBase
    {
        public int TerminalConfigurationId { get; set; }

        public override TimestampBase CreateTimestampObject()
        {
            return new TerminalConfigurationTimestamp(this.TerminalConfigurationId);
        }

        public override ApiCallerType CallerType
        {
            get { return ApiCallerType.Terminal; }
        }

        public override List<ApiOperation> ApiOperations
        {
            get
            {
                return new List<ApiOperation>
                {
                    ApiMetaData.GetTerminalConfiguration
                };
            }
        }

        public override Dictionary<string, object> ParameterNamesAndValues => new Dictionary<string, object>
        {
            { nameof(this.TerminalConfigurationId), this.TerminalConfigurationId }
        };

        public override IEntity Entity
        {
            get
            {
                TerminalConfigurationEntity entity = new TerminalConfigurationEntity();
                entity.FetchUsingPK(this.TerminalConfigurationId, null, null, new IncludeFieldsList(TerminalConfigurationFields.Name));
                return entity;
            }
        }
    }
}
