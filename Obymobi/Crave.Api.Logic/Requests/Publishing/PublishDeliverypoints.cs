﻿using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Crave.Api.Logic.Timestamps;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crave.Api.Logic.Requests.Publishing
{
    public class PublishDeliverypoints : PublishRequestBase
    {
        public int DeliverypointgroupId { get; set; }

        public override TimestampBase CreateTimestampObject()
        {
            return new DeliverypointsTimestamp(this.DeliverypointgroupId);
        }

        public override ApiCallerType CallerType
        {
            get { return ApiCallerType.Client; }
        }

        public override List<ApiOperation> ApiOperations
        {
            get
            {
                return new List<ApiOperation>
                {
                    ApiMetaData.GetDeliverypoints
                };
            }
        }

        public override Dictionary<string, object> ParameterNamesAndValues => new Dictionary<string, object>
        {
            { nameof(this.DeliverypointgroupId), this.DeliverypointgroupId }
        };

        public override IEntity Entity
        {
            get
            {
                DeliverypointgroupEntity entity = new DeliverypointgroupEntity();
                entity.FetchUsingPK(this.DeliverypointgroupId, null, null, new IncludeFieldsList(DeliverypointgroupFields.Name));
                return entity;
            }
        }
    }
}
