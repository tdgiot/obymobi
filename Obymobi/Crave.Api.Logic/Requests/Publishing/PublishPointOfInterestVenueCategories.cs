﻿using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Crave.Api.Logic.Timestamps;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crave.Api.Logic.Requests.Publishing
{
    public class PublishPointOfInterestVenueCategories : PublishRequestBase
    {
        public int PointOfInterestId { get; set; }

        public override TimestampBase CreateTimestampObject()
        {
            return new PointOfInterestVenueCategoriesTimestamp(this.PointOfInterestId);
        }

        public override ApiCallerType CallerType
        {
            get { return ApiCallerType.Client; }
        }

        public override List<ApiOperation> ApiOperations
        {
            get
            {
                return new List<ApiOperation>
                {
                    ApiMetaData.GetPointOfInterestVenueCategories,
                    ApiMetaData.GetPointOfInterestVenueCategoriesCustomText
                };
            }
        }

        public override Dictionary<string, object> ParameterNamesAndValues => new Dictionary<string, object>
        {
            { nameof(this.PointOfInterestId), this.PointOfInterestId }
        };

        public override IEntity Entity
        {
            get
            {
                PointOfInterestEntity entity = new PointOfInterestEntity();
                entity.FetchUsingPK(this.PointOfInterestId, null, null, new IncludeFieldsList(PointOfInterestFields.Name));
                return entity;
            }
        }
    }
}
