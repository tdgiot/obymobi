﻿using System.Collections.Generic;
using Crave.Api.Logic.DataSources;
using Crave.Api.Logic.Timestamps;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Api.Logic.Requests.Publishing
{
    public class PublishAnnouncementActions : PublishRequestBase
    {
        public override TimestampBase CreateTimestampObject()
        {
            return new AnnouncementActionsTimestamp(this.CompanyId);
        }

        public override ApiCallerType CallerType
        {
            get { return ApiCallerType.Terminal; }
        }

        public override List<ApiOperation> ApiOperations
        {
            get
            {
                return new List<ApiOperation>
                {
                    ApiMetaData.GetAnnouncementActions
                };
            }
        }

        public override List<PublishRequestBase> RelatedRequests => new List<PublishRequestBase>();

        public override Dictionary<string, object> ParameterNamesAndValues => new Dictionary<string, object>
        {
            { nameof(this.CompanyId), this.CompanyId }
        };

        public override IEntity Entity
        {
            get
            {
                CompanyEntity entity = new CompanyEntity();
                entity.FetchUsingPK(this.CompanyId, null, null, new IncludeFieldsList(CompanyFields.Name));
                return entity;
            }
        }
    }
}
