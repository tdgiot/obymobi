﻿using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Crave.Api.Logic.Timestamps;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crave.Api.Logic.Requests.Publishing
{
    public class PublishDeliverypoint : PublishRequestBase
    {
        public int DeliverypointId { get; set; }

        public override TimestampBase CreateTimestampObject()
        {
            return new DeliverypointTimestamp(this.DeliverypointId);
        }

        public override ApiCallerType CallerType
        {
            get { return ApiCallerType.Client; }
        }

        public override List<ApiOperation> ApiOperations
        {
            get
            {
                return new List<ApiOperation>
                {
                    ApiMetaData.GetDeliverypointById
                };
            }
        }

        public override List<PublishRequestBase> RelatedRequests
        {
            get
            {
                List<PublishRequestBase> publishRequests = new List<PublishRequestBase>();
                publishRequests.Add(new PublishDeliverypoints { DeliverypointgroupId = (this.Entity as DeliverypointEntity).DeliverypointgroupId });

                return publishRequests;
            }
        }

        public override Dictionary<string, object> ParameterNamesAndValues => new Dictionary<string, object>
        {
            { nameof(this.DeliverypointId), this.DeliverypointId }
        };

        public override IEntity Entity
        {
            get
            {
                DeliverypointEntity entity = new DeliverypointEntity();
                entity.FetchUsingPK(this.DeliverypointId, null, null, new IncludeFieldsList(DeliverypointFields.Name));
                return entity;
            }
        }
    }
}
