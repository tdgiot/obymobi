﻿using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Crave.Api.Logic.Timestamps;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crave.Api.Logic.Requests.Publishing
{
    public class PublishUITheme : PublishRequestBase
    {
        public int UIThemeId { get; set; }

        public override TimestampBase CreateTimestampObject()
        {
            return new UIThemeTimestamp(this.UIThemeId);
        }

        public override ApiCallerType CallerType
        {
            get { return ApiCallerType.Client; }
        }

        public override List<ApiOperation> ApiOperations
        {
            get
            {
                return new List<ApiOperation>
                {
                    ApiMetaData.GetUITheme,
                    ApiMetaData.GetUIThemeMedia
                };
            }
        }

        public override Dictionary<string, object> ParameterNamesAndValues => new Dictionary<string, object>
        {
            { nameof(this.UIThemeId), this.UIThemeId }
        };

        public override IEntity Entity
        {
            get
            {
                UIThemeEntity entity = new UIThemeEntity();
                entity.FetchUsingPK(this.UIThemeId, null, null, new IncludeFieldsList(UIThemeFields.Name));
                return entity;
            }
        }
    }
}
