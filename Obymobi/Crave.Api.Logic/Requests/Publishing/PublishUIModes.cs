﻿using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Crave.Api.Logic.Timestamps;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crave.Api.Logic.Requests.Publishing
{
    public class PublishUIModes : PublishRequestBase
    {
        public override TimestampBase CreateTimestampObject()
        {
            return new UIModesTimestamp(this.CompanyId);
        }

        public override ApiCallerType CallerType
        {
            get { return ApiCallerType.Client; }
        }

        public override List<ApiOperation> ApiOperations
        {
            get
            {
                return new List<ApiOperation>
                {
                    ApiMetaData.GetUIModes,
                    ApiMetaData.GetUIModesMedia,
                    ApiMetaData.GetUIModesCustomText
                };
            }
        }

        public override Dictionary<string, object> ParameterNamesAndValues => new Dictionary<string, object>
        {
            { nameof(this.CompanyId), this.CompanyId }
        };

        public override IEntity Entity
        {
            get
            {
                CompanyEntity entity = new CompanyEntity();
                entity.FetchUsingPK(this.CompanyId, null, null, new IncludeFieldsList(CompanyFields.Name));
                return entity;
            }
        }
    }
}
