﻿using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Crave.Api.Logic.Timestamps;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crave.Api.Logic.Requests.Publishing
{
    public class PublishInfraredConfiguration : PublishRequestBase
    {
        public int InfraredConfigurationId { get; set; }

        public override TimestampBase CreateTimestampObject()
        {
            return new InfraredConfigurationTimestamp(this.InfraredConfigurationId);
        }

        public override ApiCallerType CallerType
        {
            get { return ApiCallerType.Client; }
        }

        public override List<ApiOperation> ApiOperations
        {
            get
            {
                return new List<ApiOperation>
                {
                    ApiMetaData.GetInfraredConfiguration,
                    ApiMetaData.GetInfraredConfigurationCustomText
                };
            }
        }

        public override Dictionary<string, object> ParameterNamesAndValues => new Dictionary<string, object>
        {
            { nameof(this.InfraredConfigurationId), this.InfraredConfigurationId }
        };

        public override IEntity Entity
        {
            get
            {
                InfraredConfigurationEntity entity = new InfraredConfigurationEntity();
                entity.FetchUsingPK(this.InfraredConfigurationId, null, null, new IncludeFieldsList(InfraredConfigurationFields.Name));
                return entity;
            }
        }
    }
}
