﻿using System.Collections.Generic;
using Crave.Api.Logic.DataSources;
using Crave.Api.Logic.Timestamps;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Api.Logic.Requests.Publishing
{
    public class PublishMap : PublishRequestBase
    {
        public int MapId { get; set; }

        public override TimestampBase CreateTimestampObject()
        {
            return new MapTimestamp(this.MapId);
        }

        public override ApiCallerType CallerType
        {
            get { return ApiCallerType.Client; }
        }

        public override List<ApiOperation> ApiOperations
        {
            get
            {
                return new List<ApiOperation>
                {
                    ApiMetaData.GetMap,
                    ApiMetaData.GetMapMedia,
                    ApiMetaData.GetMapCustomText,
                };
            }
        }

        public override List<PublishRequestBase> RelatedRequests
        {
            get
            {
                return new List<PublishRequestBase>();
            }
        }

        public override Dictionary<string, object> ParameterNamesAndValues => new Dictionary<string, object>
        {
            { nameof(this.MapId), this.MapId }
        };

        public override IEntity Entity
        {
            get
            {
                MapEntity entity = new MapEntity();
                entity.FetchUsingPK(this.MapId, null, null, new IncludeFieldsList(MapFields.Name));
                return entity;
            }
        }
    }
}
