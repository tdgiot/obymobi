﻿using Crave.Api.Logic.Timestamps;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crave.Api.Logic.Requests.Publishing
{
    public class PublishPriceSchedule : PublishRequestBase
    {
        public int PriceScheduleId { get; set; }

        public override TimestampBase CreateTimestampObject()
        {
            return new PriceScheduleTimestamp(this.PriceScheduleId);
        }

        public override ApiCallerType CallerType
        {
            get { return ApiCallerType.Client; }
        }

        public override List<ApiOperation> ApiOperations
        {
            get
            {
                return new List<ApiOperation>
                {
                    ApiMetaData.GetPriceSchedule
                };
            }
        }

        public override Dictionary<string, object> ParameterNamesAndValues => new Dictionary<string, object>
        {
            { nameof(this.PriceScheduleId), this.PriceScheduleId }
        };

        public override IEntity Entity
        {
            get
            {
                PriceScheduleEntity entity = new PriceScheduleEntity();
                entity.FetchUsingPK(this.PriceScheduleId, null, null, new IncludeFieldsList(PriceScheduleFields.Name));
                return entity;
            }
        }
    }
}
