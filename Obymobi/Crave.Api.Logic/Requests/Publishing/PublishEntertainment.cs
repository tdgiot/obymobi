﻿using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Crave.Api.Logic.Timestamps;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crave.Api.Logic.Requests.Publishing
{
    public class PublishEntertainment : PublishRequestBase
    {
        public int EntertainmentConfigurationId { get; set; }

        public override TimestampBase CreateTimestampObject()
        {
            return new EntertainmentConfigurationTimestamp(this.EntertainmentConfigurationId);
        }

        public override ApiCallerType CallerType
        {
            get { return ApiCallerType.Client; }
        }

        public override List<ApiOperation> ApiOperations
        {
            get
            {
                return new List<ApiOperation>
                {
                    ApiMetaData.GetEntertainments,
                    ApiMetaData.GetEntertainmentsMedia,
                    ApiMetaData.GetEntertainmentCategories,
                    ApiMetaData.GetEntertainmentCategoriesCustomText
                };
            }
        }

        public override Dictionary<string, object> ParameterNamesAndValues => new Dictionary<string, object>
        {
            { nameof(this.EntertainmentConfigurationId), this.EntertainmentConfigurationId }
        };

        public override IEntity Entity
        {
            get
            {
                EntertainmentConfigurationEntity entity = new EntertainmentConfigurationEntity();
                entity.FetchUsingPK(this.EntertainmentConfigurationId, null, null, new IncludeFieldsList(EntertainmentConfigurationFields.Name));
                return entity;
            }
        }
    }
}
