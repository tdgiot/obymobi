﻿using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Crave.Api.Logic.Timestamps;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crave.Api.Logic.Requests.Publishing
{
    public class PublishUISchedule : PublishRequestBase
    {
        public int UIScheduleId { get; set; }

        public override TimestampBase CreateTimestampObject()
        {
            return new UIScheduleTimestamp(this.UIScheduleId);
        }

        public override ApiCallerType CallerType
        {
            get { return ApiCallerType.Client; }
        }

        public override List<ApiOperation> ApiOperations
        {
            get
            {
                return new List<ApiOperation>
                {
                    ApiMetaData.GetUISchedule
                };
            }
        }

        public override Dictionary<string, object> ParameterNamesAndValues => new Dictionary<string, object>
        {
            { nameof(this.UIScheduleId), this.UIScheduleId }
        };

        public override IEntity Entity
        {
            get
            {
                UIScheduleEntity entity = new UIScheduleEntity();
                entity.FetchUsingPK(this.UIScheduleId, null, null, new IncludeFieldsList(UIScheduleFields.Name));
                return entity;
            }
        }
    }
}
