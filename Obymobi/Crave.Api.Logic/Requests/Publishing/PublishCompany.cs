﻿using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Crave.Api.Logic.Timestamps;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crave.Api.Logic.Requests.Publishing
{
    public class PublishCompany : PublishRequestBase
    {
        public override TimestampBase CreateTimestampObject()
        {
            return new CompanyTimestamp(this.CompanyId);
        }

        public override ApiCallerType CallerType
        {
            get { return ApiCallerType.Client; }
        }

        public override List<ApiOperation> ApiOperations
        {
            get
            {
                return new List<ApiOperation>
                {
                    ApiMetaData.GetCompany,
                    ApiMetaData.GetCompanyMedia,
                    ApiMetaData.GetCompanyCustomText
                };
            }
        }

        public override List<PublishRequestBase> RelatedRequests
        {
            get
            {
                List<PublishRequestBase> publishRequests = new List<PublishRequestBase>();
                publishRequests.Add(new PublishCompanyAmenities { CompanyId = this.CompanyId });
                publishRequests.Add(new PublishCompanyReleases { CompanyId = this.CompanyId });
                publishRequests.Add(new PublishCompanyVenueCategories { CompanyId = this.CompanyId });

                return publishRequests;
            }
        }

        public override Dictionary<string, object> ParameterNamesAndValues => new Dictionary<string, object>
        {
            { nameof(this.CompanyId), this.CompanyId }
        };

        public override IEntity Entity
        {
            get
            {
                CompanyEntity entity = new CompanyEntity();
                entity.FetchUsingPK(this.CompanyId, null, null, new IncludeFieldsList(CompanyFields.Name));
                return entity;
            }
        }
    }
}
