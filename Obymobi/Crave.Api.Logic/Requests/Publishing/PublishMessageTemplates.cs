﻿using System.Collections.Generic;
using Crave.Api.Logic.Timestamps;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Api.Logic.Requests.Publishing
{
    public class PublishMessageTemplates : PublishRequestBase
    {
        public int TerminalId { get; set; }

        public override TimestampBase CreateTimestampObject()
        {
            return new MessageTemplatesTimestamp(this.TerminalId);
        }

        public override ApiCallerType CallerType
        {
            get { return ApiCallerType.Terminal; }
        }

        public override List<ApiOperation> ApiOperations
        {
            get
            {
                return new List<ApiOperation>
                {
                    ApiMetaData.GetMessageTemplates
                };
            }
        }

        public override List<PublishRequestBase> RelatedRequests => new List<PublishRequestBase>();

        public override Dictionary<string, object> ParameterNamesAndValues => new Dictionary<string, object>
        {
            { nameof(this.TerminalId), this.TerminalId }
        };

        public override IEntity Entity
        {
            get
            {
                TerminalEntity entity = new TerminalEntity();
                entity.FetchUsingPK(this.TerminalId, null, null, new IncludeFieldsList(TerminalFields.Name));

                return entity;
            }
        }
    }
}
