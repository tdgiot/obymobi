﻿using System.Collections.Generic;
using Crave.Api.Logic.DataSources;
using Crave.Api.Logic.Timestamps;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Api.Logic.Requests.Publishing
{
    public class PublishRoomControlConfiguration : PublishRequestBase
    {
        public int RoomControlConfigurationId { get; set; }

        public override TimestampBase CreateTimestampObject()
        {
            return new RoomControlConfigurationTimestamp(this.RoomControlConfigurationId);
        }

        public override ApiCallerType CallerType
        {
            get { return ApiCallerType.Client; }
        }

        public override List<ApiOperation> ApiOperations
        {
            get
            {
                return new List<ApiOperation>
                {
                    ApiMetaData.GetRoomControlConfiguration,
                    ApiMetaData.GetRoomControlConfigurationMedia,
                    ApiMetaData.GetRoomControlConfigurationCustomText
                };
            }
        }

        public override Dictionary<string, object> ParameterNamesAndValues => new Dictionary<string, object>
        {
            { nameof(this.RoomControlConfigurationId), this.RoomControlConfigurationId }
        };

        public override IEntity Entity
        {
            get
            {
                RoomControlConfigurationEntity entity = new RoomControlConfigurationEntity();
                entity.FetchUsingPK(this.RoomControlConfigurationId, null, null, new IncludeFieldsList(RoomControlConfigurationFields.Name));
                return entity;
            }
        }
    }
}
