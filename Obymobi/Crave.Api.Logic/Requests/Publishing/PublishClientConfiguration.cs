﻿using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Crave.Api.Logic.Timestamps;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crave.Api.Logic.Requests.Publishing
{
    public class PublishClientConfiguration : PublishRequestBase
    {
        public int ClientConfigurationId { get; set; }

        public override TimestampBase CreateTimestampObject()
        {
            return new ClientConfigurationTimestamp(this.ClientConfigurationId);
        }

        public override ApiCallerType CallerType
        {
            get { return ApiCallerType.Client; }
        }

        public override List<ApiOperation> ApiOperations
        {
            get
            {
                return new List<ApiOperation>
                {
                    ApiMetaData.GetClientConfiguration,
                    ApiMetaData.GetClientConfigurationCustomText,
                    ApiMetaData.GetClientConfigurationMedia,
                    ApiMetaData.GetClientConfigurationTimestamps
                };
            }
        }

        public override Dictionary<string, object> ParameterNamesAndValues => new Dictionary<string, object>
        {
            { nameof(this.ClientConfigurationId), this.ClientConfigurationId }
        };

        public override IEntity Entity
        {
            get
            {
                ClientConfigurationEntity entity = new ClientConfigurationEntity();
                entity.FetchUsingPK(this.ClientConfigurationId, null, null, new IncludeFieldsList(ClientConfigurationFields.Name));
                return entity;
            }
        }
    }
}
