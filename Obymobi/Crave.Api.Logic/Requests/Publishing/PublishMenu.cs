﻿using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Crave.Api.Logic.Timestamps;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crave.Api.Logic.Requests.Publishing
{
    public class PublishMenu : PublishRequestBase
    {
        public int MenuId { get; set; }

        public override TimestampBase CreateTimestampObject()
        {
            return new MenuTimestamp(this.MenuId);
        }

        public override ApiCallerType CallerType
        {
            get { return ApiCallerType.Client; }
        }

        public override List<ApiOperation> ApiOperations
        {
            get
            {
                return new List<ApiOperation>
                {
                    ApiMetaData.GetAlterations,
                    ApiMetaData.GetAlterationsMedia,
                    ApiMetaData.GetAlterationsCustomText,
                    ApiMetaData.GetLegacyAlterations,
                    ApiMetaData.GetLegacyAlterationsMedia,
                    ApiMetaData.GetLegacyAlterationsCustomText,
                    ApiMetaData.GetMenu,
                    ApiMetaData.GetMenuMedia,
                    ApiMetaData.GetMenuCustomText,
                    ApiMetaData.GetProducts,
                    ApiMetaData.GetProductsMedia,
                    ApiMetaData.GetProductsCustomText,
                    ApiMetaData.GetProductgroups,
                    ApiMetaData.GetProductgroupsMedia,
                    ApiMetaData.GetProductgroupsCustomText,
                    ApiMetaData.GetSchedules
                };
            }
        }

        public override Dictionary<string, object> ParameterNamesAndValues => new Dictionary<string, object>
        {
            { nameof(this.MenuId), this.MenuId }
        };

        public override IEntity Entity
        {
            get
            {
                MenuEntity entity = new MenuEntity();
                entity.FetchUsingPK(this.MenuId, null, null, new IncludeFieldsList(MenuFields.Name));
                return entity;
            }
        }
    }
}
