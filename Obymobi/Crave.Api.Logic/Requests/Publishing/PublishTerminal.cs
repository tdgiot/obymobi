﻿using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Crave.Api.Logic.Timestamps;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crave.Api.Logic.Requests.Publishing
{
    public class PublishTerminal : PublishRequestBase
    {
        public int TerminalId { get; set; }

        public override TimestampBase CreateTimestampObject()
        {
            return new TerminalTimestamp(this.TerminalId);
        }

        public override ApiCallerType CallerType
        {
            get { return ApiCallerType.Terminal; }
        }

        public override List<ApiOperation> ApiOperations
        {
            get
            {
                return new List<ApiOperation>
                {
                    ApiMetaData.GetTerminal
                };
            }
        }

        public override Dictionary<string, object> ParameterNamesAndValues => new Dictionary<string, object>
        {
            { nameof(this.TerminalId), this.TerminalId }
        };

        public override IEntity Entity
        {
            get
            {
                TerminalEntity entity = new TerminalEntity();
                entity.FetchUsingPK(this.TerminalId, null, null, new IncludeFieldsList(TerminalFields.Name));
                return entity;
            }
        }
    }
}
