﻿using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Crave.Api.Logic.Timestamps;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crave.Api.Logic.Requests.Publishing
{
    public class PublishUIMode : PublishRequestBase
    {
        public int UIModeId { get; set; }

        public override TimestampBase CreateTimestampObject()
        {
            return new UIModeTimestamp(this.UIModeId);
        }

        public override ApiCallerType CallerType
        {
            get { return ApiCallerType.Client; }
        }

        public override List<ApiOperation> ApiOperations
        {
            get
            {
                return new List<ApiOperation>
                {
                    ApiMetaData.GetUIMode,
                    ApiMetaData.GetUIModeMedia,
                    ApiMetaData.GetUIModeCustomText
                };
            }
        }

        public override Dictionary<string, object> ParameterNamesAndValues => new Dictionary<string, object>
        {
            { nameof(this.UIModeId), this.UIModeId }
        };

        public override List<PublishRequestBase> RelatedRequests
        {
            get
            {
                List<PublishRequestBase> publishRequests = new List<PublishRequestBase>();

                if ((this.Entity as UIModeEntity).CompanyId.HasValue)
                    publishRequests.Add(new PublishUIModes { CompanyId = (this.Entity as UIModeEntity).CompanyId.Value });

                return publishRequests;
            }
        }

        public override IEntity Entity
        {
            get
            {
                UIModeEntity entity = new UIModeEntity();
                entity.FetchUsingPK(this.UIModeId, null, null, new IncludeFieldsList(UIModeFields.Name));
                return entity;
            }
        }
    }
}
