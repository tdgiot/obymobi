﻿using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Crave.Api.Logic.Timestamps;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System.Collections.Generic;

namespace Crave.Api.Logic.Requests.Publishing
{
    public class PublishAvailabilities : PublishRequestBase
    {
        public override TimestampBase CreateTimestampObject()
        {
            return new AvailabilitiesTimestamp(this.CompanyId);
        }

        public override ApiCallerType CallerType
        {
            get { return ApiCallerType.Client; }
        }

        public override List<ApiOperation> ApiOperations
        {
            get
            {
                return new List<ApiOperation>
                {
                    ApiMetaData.GetAvailabilities,
                    ApiMetaData.GetAvailabilitiesCustomText
                };
            }
        }

        public override Dictionary<string, object> ParameterNamesAndValues => new Dictionary<string, object>
        {
            { nameof(this.CompanyId), this.CompanyId }
        };

        public override IEntity Entity
        {
            get
            {
                CompanyEntity entity = new CompanyEntity();
                entity.FetchUsingPK(this.CompanyId, null, null, new IncludeFieldsList(CompanyFields.Name));
                return entity;
            }
        }
    }
}
