﻿using Dionysos.Data;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Crave.Api.Logic.Timestamps;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crave.Api.Logic.Requests.Publishing
{
    public class PublishAdvertisements : PublishRequestBase
    {
        public int AdvertisementConfigurationId { get; set; }

        public override TimestampBase CreateTimestampObject()
        {
            return new AdvertisementConfigurationTimestamp(this.AdvertisementConfigurationId);
        }

        public override ApiCallerType CallerType
        {
            get { return ApiCallerType.Client; }
        }

        public override List<ApiOperation> ApiOperations
        {
            get
            {
                return new List<ApiOperation>
                {
                    ApiMetaData.GetAdvertisements,
                    ApiMetaData.GetAdvertisementsMedia,
                    ApiMetaData.GetAdvertisementsCustomText
                };
            }
        }

        public override Dictionary<string, object> ParameterNamesAndValues => new Dictionary<string, object>
        {
            { nameof(this.AdvertisementConfigurationId), this.AdvertisementConfigurationId }
        };

        public override IEntity Entity
        {
            get
            {
                AdvertisementConfigurationEntity entity = new AdvertisementConfigurationEntity();
                entity.FetchUsingPK(this.AdvertisementConfigurationId, null, null, new IncludeFieldsList(AdvertisementConfigurationFields.Name));
                return entity;
            }
        }
    }
}
