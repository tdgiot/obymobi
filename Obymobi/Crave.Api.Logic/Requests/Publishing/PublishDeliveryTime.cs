﻿using Crave.Api.Logic.Timestamps;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System.Collections.Generic;

namespace Crave.Api.Logic.Requests.Publishing
{
    public class PublishDeliveryTime : PublishRequestBase
    {
        public int DeliverypointgroupId { get; set; }

        public override TimestampBase CreateTimestampObject()
        {
            return new DeliveryTimeTimestamp(this.DeliverypointgroupId);
        }

        public override ApiCallerType CallerType => ApiCallerType.Client;

        public override List<ApiOperation> ApiOperations =>
            new List<ApiOperation>
            {
                ApiMetaData.GetDeliveryTime
            };

        public override Dictionary<string, object> ParameterNamesAndValues => new Dictionary<string, object>
        {
            { nameof(this.DeliverypointgroupId), this.DeliverypointgroupId }
        };

        public override IEntity Entity
        {
            get
            {
                DeliverypointgroupEntity entity = new DeliverypointgroupEntity();
                entity.FetchUsingPK(this.DeliverypointgroupId, null, null, new IncludeFieldsList(DeliverypointgroupFields.Name));
                return entity;
            }
        }
    }
}