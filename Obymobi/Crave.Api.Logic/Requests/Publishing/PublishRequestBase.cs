﻿using Dionysos;
using Dionysos.Data;
using Obymobi.Data.EntityClasses;
using Crave.Api.Logic.Timestamps;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Crave.Api.Logic.Requests.Publishing
{
    public abstract class PublishRequestBase
    {
        private TimestampBase CachedTimestamp { get; set; }

        /// <summary>
        /// Gets or sets the company id for the publish request,
        /// which is used within the authentication process.
        /// </summary>
        public int CompanyId { get; set; }

        /// <summary>
        /// Gets or sets the active <see cref="UserEntity"/> instance,
        /// which is used for tracking users that did the publish.
        /// </summary>
        public UserEntity User { get; set; }

        /// <summary>
        /// Gets or sets the base url of the REST API.
        /// </summary>
        public string BaseUrl { get; set; }

        /// <summary>
        /// Gets or sets a flag indicating whether the publish should be
        /// executed in a forced way i.e. all API calls are published
        /// regardless of they were changed or not.
        /// </summary>
        public bool Force { get; set; }

        public abstract ApiCallerType CallerType { get; }
        
        public abstract List<ApiOperation> ApiOperations { get; }

        public virtual List<PublishRequestBase> RelatedRequests
        {
            get
            {
                return new List<PublishRequestBase>();
            }
        }

        public abstract Dictionary<string, object> ParameterNamesAndValues { get; }

        public string Key => $"{this.GetType().Name}_{this.GetParameterString()}";

        public virtual IEntity Entity 
        {
            get { return null; }
        }

        public TimestampBase Timestamp
        {
            get
            {
                if (this.CachedTimestamp == null)
                {
                    this.CachedTimestamp = this.CreateTimestampObject();
                }

                return this.CachedTimestamp;
            }
        }

        public abstract TimestampBase CreateTimestampObject();

        public virtual string GetParameterString()
        {
            string parameters = string.Empty;

            foreach (string key in this.ParameterNamesAndValues.Keys)
            {
                string value = this.ParameterNamesAndValues[key].ToString();

                if (!parameters.IsNullOrWhiteSpace())
                    parameters += "_";

                parameters += $"{key}-{value}";
            }

            return parameters;
        }

        public override string ToString()
        {
            if (this.Entity == null)
                return base.ToString();

            return string.Format("{0} '{1}'", EntityInformationUtil.GetFriendlyName(this.Entity), EntityInformationUtil.GetShowFieldNameValue(this.Entity));
        }
    }
}
