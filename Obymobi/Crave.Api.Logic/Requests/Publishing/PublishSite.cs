﻿using System;
using System.Collections.Generic;
using Crave.Api.Logic.DataSources;
using Crave.Api.Logic.Timestamps;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Api.Logic.Requests.Publishing
{
    public class PublishSite : PublishRequestBase
    {
        public int SiteId { get; set; }

        public string CultureCode { get; set; }

        public override TimestampBase CreateTimestampObject()
        {
            return new SiteTimestamp(this.SiteId, this.CultureCode);
        }

        public override ApiCallerType CallerType
        {
            get { return ApiCallerType.Client; }
        }

        public override List<ApiOperation> ApiOperations
        {
            get
            {
                return new List<ApiOperation>
                {
                    ApiMetaData.GetSite,
                    ApiMetaData.GetSiteMedia,
                    ApiMetaData.GetSiteCustomText
                };
            }
        }

        public override List<PublishRequestBase> RelatedRequests
        {
            get
            {
                List<PublishRequestBase> publishRequests = new List<PublishRequestBase>();

                // Sites per culture
                SiteCultureCollection siteCultureCollection = new SiteEntity(this.SiteId).SiteCultureCollection;
                foreach (SiteCultureEntity siteCultureEntity in siteCultureCollection)
                {
                    if (siteCultureEntity.CultureCode.Equals(this.CultureCode, StringComparison.InvariantCultureIgnoreCase))
                        continue; // Skip, because it is the same as the main request

                    PublishSite publishSiteRequest = new PublishSite 
                    { 
                        SiteId = siteCultureEntity.SiteId,
                        CultureCode = siteCultureEntity.CultureCode
                    };
                    publishRequests.Add(publishSiteRequest);
                }

                return publishRequests;
            }
        }

        public override Dictionary<string, object> ParameterNamesAndValues => new Dictionary<string, object>
        {
            { nameof(this.SiteId), this.SiteId },
            { nameof(this.CultureCode), this.CultureCode },
        };

        public override IEntity Entity
        {
            get
            {
                SiteEntity entity = new SiteEntity();
                entity.FetchUsingPK(this.SiteId, null, null, new IncludeFieldsList(SiteFields.Name));
                return entity;
            }
        }
    }
}
