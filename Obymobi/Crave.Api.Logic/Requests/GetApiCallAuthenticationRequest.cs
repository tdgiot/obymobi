﻿using System.Collections.Generic;

namespace Crave.Api.Logic.Requests
{
    public class GetApiCallAuthenticationRequest
    {
        public int CompanyId { get; set; }

        public ApiCallerType CallerType { get; set; }

        public Dictionary<string, object> ParameterNamesAndValues { get; set; }
    }
}
