﻿using Crave.Api.Logic.Timestamps;
using Obymobi.Data.EntityClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Crave.Api.Logic.Requests
{
    public class RepublishApiOperationRequest
    {
        public PublishingItemEntity PublishingItemEntity { get; set; }

        public int CompanyId { get; set; }

        public UserEntity UserEntity { get; set; }

        public string BaseUrl { get; set; }

        public bool Force { get; set; }
    }
}
