﻿using Obymobi.Data.EntityClasses;
using Obymobi.Enums;

namespace Crave.Api.Logic.Requests
{
    public class UpdatePublishingItemEntityRequest
    {
        public PublishingItemEntity PublishingItemEntity { get; set; }

        public long Timestamp { get; set; }

        public PublishingItemStatus Status { get; set; }

        public string Log { get; set; }
    }
}
