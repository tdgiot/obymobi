﻿using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;

namespace Crave.Api.Logic.Requests
{
    public class CreatePublishingItemEntityRequest
    {
        public PublishingEntity PublishingEntity { get; set; }

        public string ApiOperation { get; set; }

        public Dictionary<string, object> ParameterNamesAndValues { get; set; }

        public long Timestamp { get; set; }

        public PublishingItemStatus Status { get; set; }

        public string Log { get; set; }
    }
}
