﻿using Obymobi.Data.EntityClasses;
using Obymobi.Enums;

namespace Crave.Api.Logic.Requests
{
    public class UpdatePublishingEntityRequest
    {
        public PublishingEntity PublishingEntity { get; set; }

        public PublishingStatus Status { get; set; }

        public string Log { get; set; }
    }
}
