﻿using System.Collections.Generic;
using Crave.Api.Logic.Requests.Publishing;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;

namespace Crave.Api.Logic.Requests
{
    public class CreatePublishingEntityRequest
    {
        public PublishRequestBase PublishRequest { get; set; }

        public PublishingStatus Status { get; set; }

        public string Log { get; set; }

        public Dictionary<string, object> ParameterNamesAndValues { get; set; }

        public UserEntity User { get; set; }
    }
}
