﻿using Crave.Api.Logic.Timestamps;
using System.Collections.Generic;
using System.Reflection;

namespace Crave.Api.Logic
{
    public class ApiMetaData
    {
        private const string Timestamp = "/timestamp";
        private const string Media = "/media";
        private const string CustomText = "/customtext";
        public const string Publish = "/publish";

        // GetAdvertisements
        public static readonly ApiOperation GetAdvertisements = new ApiOperation("GetAdvertisements", "/advertisements/{AdvertisementConfigurationId}", ApiCallerType.Client, TimestampMetaData.AdvertisementConfiguration);
        public static readonly ApiOperation GetAdvertisementsTimestamp = new ApiOperation("GetAdvertisementsTimestamp", GetAdvertisements.Path + Timestamp, ApiCallerType.Client);
        public static readonly ApiOperation GetAdvertisementsMedia = new ApiOperation("GetAdvertisementsMedia", GetAdvertisements.Path + Media, ApiCallerType.Client, TimestampMetaData.AdvertisementConfigurationMedia);
        public static readonly ApiOperation GetAdvertisementsMediaTimestamp = new ApiOperation("GetAdvertisementsMediaTimestamp", GetAdvertisements.Path + Media + Timestamp, ApiCallerType.Client);
        public static readonly ApiOperation GetAdvertisementsCustomText = new ApiOperation("GetAdvertisementsCustomText", GetAdvertisements.Path + CustomText, ApiCallerType.Client, TimestampMetaData.AdvertisementConfigurationCustomText);
        public static readonly ApiOperation GetAdvertisementsCustomTextTimestamp = new ApiOperation("GetAdvertisementsCustomTextTimestamp", GetAdvertisements.Path + CustomText + Timestamp, ApiCallerType.Client);

        // GetAlterations
        public static readonly ApiOperation GetAlterations = new ApiOperation("GetAlterations", "/menu/{MenuId}/alterations", ApiCallerType.Client, TimestampMetaData.Alterations);
        public static readonly ApiOperation GetAlterationsTimestamp = new ApiOperation("GetAlterationsTimestamp", GetAlterations.Path + Timestamp, ApiCallerType.Client);
        public static readonly ApiOperation GetAlterationsMedia = new ApiOperation("GetAlterationsMedia", GetAlterations.Path + Media, ApiCallerType.Client, TimestampMetaData.AlterationsMedia);
        public static readonly ApiOperation GetAlterationsMediaTimestamp = new ApiOperation("GetAlterationsMediaTimestamp", GetAlterations.Path + Media + Timestamp, ApiCallerType.Client);
        public static readonly ApiOperation GetAlterationsCustomText = new ApiOperation("GetAlterationsCustomText", GetAlterations.Path + CustomText, ApiCallerType.Client, TimestampMetaData.AlterationsCustomText);
        public static readonly ApiOperation GetAlterationsCustomTextTimestamp = new ApiOperation("GetAlterationsCustomTextTimestamp", GetAlterations.Path + CustomText + Timestamp, ApiCallerType.Client);

        // GetAnnouncementActions
        public static readonly ApiOperation GetAnnouncementActions = new ApiOperation("GetAnnouncementActions", "/company/{CompanyId}/announcementactions", ApiCallerType.Terminal, TimestampMetaData.AnnouncementActions);
        public static readonly ApiOperation GetAnnouncementActionsTimestamp = new ApiOperation("GetAnnouncementActionsTimestamp", GetAnnouncementActions + Timestamp, ApiCallerType.Terminal);
        public static readonly ApiOperation GetAnnouncementActionsForCategories = new ApiOperation("GetAnnouncementActionsForCategories", "/company/{CompanyId}/announcementactions/categories", ApiCallerType.Terminal);
        public static readonly ApiOperation GetAnnouncementActionsForEntertainments = new ApiOperation("GetAnnouncementActionsForEntertainments", "/company/{CompanyId}/announcementactions/entertainments", ApiCallerType.Terminal);
        public static readonly ApiOperation GetAnnouncementActionsForProducts = new ApiOperation("GetAnnouncementActionsForProducts", "/company/{CompanyId}/announcementactions/products", ApiCallerType.Terminal);

        // GetAvailabilities
        public static readonly ApiOperation GetAvailabilities = new ApiOperation("GetAvailabilities", "/company/{CompanyId}/availabilities", ApiCallerType.Client, TimestampMetaData.Availabilities);
        public static readonly ApiOperation GetAvailabilitiesTimestamp = new ApiOperation("GetAvailabilitiesCustomText", GetAvailabilities.Path + Timestamp, ApiCallerType.Client);
        public static readonly ApiOperation GetAvailabilitiesCustomText = new ApiOperation("GetAvailabilitiesCustomText", GetAvailabilities.Path + CustomText, ApiCallerType.Client, TimestampMetaData.AvailabilitiesCustomText);
        public static readonly ApiOperation GetAvailabilitiesCustomTextTimestamp = new ApiOperation("GetAvailabilitiesCustomTextTimestamp", GetAvailabilities.Path + CustomText + Timestamp, ApiCallerType.Client);

        // GetAvailability
        public static readonly ApiOperation GetAvailability = new ApiOperation("GetAvailability", "/availability/{AvailabilityId}", ApiCallerType.Client);

        // GetClient
        public static readonly ApiOperation GetClient = new ApiOperation("GetClient", "/client/{ClientId}", ApiCallerType.Client, TimestampMetaData.Client);
        public static readonly ApiOperation GetClientTimestamp = new ApiOperation("GetClientTimestamp", GetClient.Path + Timestamp, ApiCallerType.Client);

        // GetClientConfiguration
        public static readonly ApiOperation GetClientConfiguration = new ApiOperation("GetClientConfiguration", "/clientconfiguration/{ClientConfigurationId}", ApiCallerType.Client, TimestampMetaData.ClientConfiguration);
        public static readonly ApiOperation GetClientConfigurationTimestamp = new ApiOperation("GetClientConfigurationTimestamp", GetClientConfiguration.Path + Timestamp, ApiCallerType.Client);
        public static readonly ApiOperation GetClientConfigurationMedia = new ApiOperation("GetClientConfigurationMedia", GetClientConfiguration.Path + Media, ApiCallerType.Client, TimestampMetaData.ClientConfigurationMedia);
        public static readonly ApiOperation GetClientConfigurationMediaTimestamp = new ApiOperation("GetClientConfigurationMediaTimestamp", GetClientConfiguration.Path + Media + Timestamp, ApiCallerType.Client);
        public static readonly ApiOperation GetClientConfigurationCustomText = new ApiOperation("GetClientConfigurationCustomText", GetClientConfiguration.Path + CustomText, ApiCallerType.Client, TimestampMetaData.ClientConfigurationCustomText);
        public static readonly ApiOperation GetClientConfigurationCustomTextTimestamp = new ApiOperation("GetClientConfigurationCustomTextTimestamp", GetClientConfiguration.Path + CustomText + Timestamp, ApiCallerType.Client);
        public static readonly ApiOperation GetClientConfigurationTimestamps = new ApiOperation("GetClientConfigurationTimestamps", GetClientConfiguration.Path + Timestamp + "s", ApiCallerType.Client);

        // GetStorageAccounts
        public static readonly ApiOperation GetCloudStorageAccounts = new ApiOperation("GetCloudStorageAccounts", "/company/{CompanyId}/cloudstorageaccounts", ApiCallerType.Client, TimestampMetaData.CloudStorageAccounts);
        public static readonly ApiOperation GetCloudStorageAccountsTimestamp = new ApiOperation("GetCloudStorageAccountsTimestamp", GetCloudStorageAccounts.Path + Timestamp, ApiCallerType.Client);

        // GetCompany
        public static readonly ApiOperation GetCompany = new ApiOperation("GetCompany", "/company/{CompanyId}", ApiCallerType.Client, TimestampMetaData.Company);
        public static readonly ApiOperation GetCompanyTimestamp = new ApiOperation("GetCompanyTimestamp", GetCompany.Path + Timestamp, ApiCallerType.Client);
        public static readonly ApiOperation GetCompanyMedia = new ApiOperation("GetCompanyMedia", GetCompany.Path + Media, ApiCallerType.Client, TimestampMetaData.CompanyMedia);
        public static readonly ApiOperation GetCompanyMediaTimestamp = new ApiOperation("GetCompanyMediaTimestamp", GetCompany.Path + Media + Timestamp, ApiCallerType.Client);
        public static readonly ApiOperation GetCompanyCustomText = new ApiOperation("GetCompanyCustomText", GetCompany.Path + CustomText, ApiCallerType.Client, TimestampMetaData.CompanyCustomText);
        public static readonly ApiOperation GetCompanyCustomTextTimestamp = new ApiOperation("GetCompanyCustomTextTimestamp", GetCompany.Path + CustomText + Timestamp, ApiCallerType.Client);

        // GetCompanyAmenities
        public static readonly ApiOperation GetCompanyAmenities = new ApiOperation("GetCompanyAmenities", "/company/{CompanyId}/amenities", ApiCallerType.Client, TimestampMetaData.Amenities);
        public static readonly ApiOperation GetCompanyAmenitiesTimestamp = new ApiOperation("GetCompanyAmenitiesTimestamp", GetCompanyAmenities.Path + Timestamp, ApiCallerType.Client);
        public static readonly ApiOperation GetCompanyAmenitiesCustomText = new ApiOperation("GetCompanyAmenitiesCustomText", GetCompanyAmenities.Path + CustomText, ApiCallerType.Client, TimestampMetaData.AmenitiesCustomText);
        public static readonly ApiOperation GetCompanyAmenitiesCustomTextTimestamp = new ApiOperation("GetCompanyAmenitiesCustomTextTimestamp", GetCompanyAmenities.Path + CustomText + Timestamp, ApiCallerType.Client);

        // GetCompanyRelease
        public static readonly ApiOperation GetCompanyReleases = new ApiOperation("GetCompanyReleases", "/company/{CompanyId}/releases", ApiCallerType.Client, TimestampMetaData.CompanyReleases);
        public static readonly ApiOperation GetCompanyReleasesTimestamp = new ApiOperation("GetCompanyReleasesTimestamp", GetCompanyReleases.Path + Timestamp, ApiCallerType.Client);

        // GetCompanySynopsis
        public static readonly ApiOperation GetCompanySynopsis = new ApiOperation("GetCompanySynopsis", "/company/{Username}/{Password}", ApiCallerType.Client);

        // GetCompanyVenueCategories
        public static readonly ApiOperation GetCompanyVenueCategories = new ApiOperation("GetCompanyVenueCategories", "/company/{CompanyId}/venuecategories", ApiCallerType.Client, TimestampMetaData.CompanyVenueCategories);
        public static readonly ApiOperation GetCompanyVenueCategoriesTimestamp = new ApiOperation("GetCompanyVenueCategoriesTimestamp", GetCompanyVenueCategories.Path + Timestamp, ApiCallerType.Client);
        public static readonly ApiOperation GetCompanyVenueCategoriesCustomText = new ApiOperation("GetCompanyVenueCategoriesCustomText", GetCompanyVenueCategories.Path + CustomText, ApiCallerType.Client, TimestampMetaData.CompanyVenueCategoriesCustomText);
        public static readonly ApiOperation GetCompanyVenueCategoriesCustomTextTimestamp = new ApiOperation("GetCompanyVenueCategoriesCustomTextTimestamp", GetCompanyVenueCategories.Path + CustomText + Timestamp, ApiCallerType.Client);

        // GetDeliverypoint
        public static readonly ApiOperation GetDeliverypointById = new ApiOperation("GetDeliverypointById", "/deliverypoint/{DeliverypointId}", ApiCallerType.Client, TimestampMetaData.Deliverypoint);
        public static readonly ApiOperation GetDeliverypointByIdTimestamp = new ApiOperation("GetDeliverypointByIdTimestamp", GetDeliverypointById.Path + Timestamp, ApiCallerType.Client);

        // GetDeliverypointByNumber
        public static readonly ApiOperation GetDeliverypointByNumber = new ApiOperation("GetDeliverypointByNumber", "/deliverypointgroup/{DeliverypointgroupId}/deliverypoint/{DeliverypointNumber}", ApiCallerType.Client);

        // GetDeliverypoints
        public static readonly ApiOperation GetDeliverypoints = new ApiOperation("GetDeliverypoints", "/deliverypointgroup/{DeliverypointgroupId}/deliverypoints", ApiCallerType.Client, TimestampMetaData.Deliverypoints);
        public static readonly ApiOperation GetDeliverypointsTimestamp = new ApiOperation("GetDeliverypointsTimestamp", GetDeliverypoints.Path + Timestamp, ApiCallerType.Client);

        // GetDeliveryTime
        public static readonly ApiOperation GetDeliveryTime = new ApiOperation("GetDeliveryTime", "/deliverypointgroup/{DeliverypointgroupId}/deliverytime", ApiCallerType.Client, TimestampMetaData.DeliveryTime);
        public static readonly ApiOperation GetDeliveryTimeTimestamp = new ApiOperation("GetDeliveryTimeTimestamp", GetDeliveryTime.Path + Timestamp, ApiCallerType.Client);

        // GetEntertainmentCategories
        public static readonly ApiOperation GetEntertainmentCategories = new ApiOperation("GetEntertainmentCategories", "/entertainment/{EntertainmentConfigurationId}/categories", ApiCallerType.Client, TimestampMetaData.EntertainmentCategories);
        public static readonly ApiOperation GetEntertainmentCategoriesTimestamp = new ApiOperation("GetEntertainmentCategoriesTimestamp", GetEntertainmentCategories.Path + Timestamp, ApiCallerType.Client);
        public static readonly ApiOperation GetEntertainmentCategoriesCustomText = new ApiOperation("GetEntertainmentCategoriesCustomText", GetEntertainmentCategories.Path + CustomText, ApiCallerType.Client, TimestampMetaData.EntertainmentCategoriesCustomText);
        public static readonly ApiOperation GetEntertainmentCategoriesCustomTextTimestamp = new ApiOperation("GetEntertainmentCategoriesCustomTextTimestamp", GetEntertainmentCategories.Path + CustomText + Timestamp, ApiCallerType.Client);

        // GetEntertainments
        public static readonly ApiOperation GetEntertainments = new ApiOperation("GetEntertainments", "/entertainment/{EntertainmentConfigurationId}", ApiCallerType.Client, TimestampMetaData.EntertainmentConfiguration);
        public static readonly ApiOperation GetEntertainmentsTimestamp = new ApiOperation("GetEntertainmentsTimestamp", GetEntertainments.Path + Timestamp, ApiCallerType.Client);
        public static readonly ApiOperation GetEntertainmentsMedia = new ApiOperation("GetEntertainmentsMedia", GetEntertainments.Path + Media, ApiCallerType.Client, TimestampMetaData.EntertainmentConfigurationMedia);
        public static readonly ApiOperation GetEntertainmentsMediaTimestamp = new ApiOperation("GetEntertainmentsMediaTimestamp", GetEntertainments.Path + Media + Timestamp, ApiCallerType.Client);

        // GetGuestGroups
        public static readonly ApiOperation GetGuestGroups = new ApiOperation("GetGuestGroups", "/company/{CompanyId}/guestgroups", ApiCallerType.Client);

        // GetGuestInformation
        public static readonly ApiOperation GetGuestInformation = new ApiOperation("GetGuestInformation", "/company/{CompanyId}/guestinformation", ApiCallerType.Client);

        // GetHealth
        public static readonly ApiOperation GetHealth = new ApiOperation("GetHealth", "/health", ApiCallerType.Client);

        // GetInfraredConfiguration
        public static readonly ApiOperation GetInfraredConfiguration = new ApiOperation("GetInfraredConfiguration", "/infraredconfiguration/{InfraredConfigurationId}", ApiCallerType.Client, TimestampMetaData.InfraredConfiguration);
        public static readonly ApiOperation GetInfraredConfigurationTimestamp = new ApiOperation("GetInfraredConfigurationTimestamp", GetInfraredConfiguration.Path + Timestamp, ApiCallerType.Client);
        public static readonly ApiOperation GetInfraredConfigurationCustomText = new ApiOperation("GetInfraredConfigurationCustomText", GetInfraredConfiguration.Path + CustomText, ApiCallerType.Client, TimestampMetaData.InfraredConfigurationCustomText);
        public static readonly ApiOperation GetInfraredConfigurationCustomTextTimestamp = new ApiOperation("GetInfraredConfigurationCustomTextTimestamp", GetInfraredConfiguration.Path + CustomText + Timestamp, ApiCallerType.Client);

        // GetLegacyAlterations
        public static readonly ApiOperation GetLegacyAlterations = new ApiOperation("GetLegacyAlterations", "/menu/{MenuId}/alterations/legacy", ApiCallerType.Client, TimestampMetaData.Alterations);
        public static readonly ApiOperation GetLegacyAlterationsTimestamp = new ApiOperation("GetLegacyAlterationsTimestamp", GetLegacyAlterations.Path + Timestamp, ApiCallerType.Client);
        public static readonly ApiOperation GetLegacyAlterationsMedia = new ApiOperation("GetLegacyAlterationsMedia", GetLegacyAlterations.Path + Media, ApiCallerType.Client, TimestampMetaData.AlterationsMedia);
        public static readonly ApiOperation GetLegacyAlterationsMediaTimestamp = new ApiOperation("GetLegacyAlterationsMediaTimestamp", GetLegacyAlterations.Path + Media + Timestamp, ApiCallerType.Client);
        public static readonly ApiOperation GetLegacyAlterationsCustomText = new ApiOperation("GetLegacyAlterationsCustomText", GetLegacyAlterations.Path + CustomText, ApiCallerType.Client, TimestampMetaData.AlterationsCustomText);
        public static readonly ApiOperation GetLegacyAlterationsCustomTextTimestamp = new ApiOperation("GetLegacyAlterationsCustomTextTimestamp", GetLegacyAlterations.Path + CustomText + Timestamp, ApiCallerType.Client);

        // GetMap
        public static readonly ApiOperation GetMap = new ApiOperation("GetMap", "/map/{MapId}", ApiCallerType.Client, TimestampMetaData.Map);
        public static readonly ApiOperation GetMapTimestamp = new ApiOperation("GetMapTimestamp", GetMap.Path + Timestamp, ApiCallerType.Client);
        public static readonly ApiOperation GetMapMedia = new ApiOperation("GetMapMedia", GetMap.Path + Media, ApiCallerType.Client, TimestampMetaData.MapMedia);
        public static readonly ApiOperation GetMapMediaTimestamp = new ApiOperation("GetMapMediaTimestamp", GetMap.Path + Media + Timestamp, ApiCallerType.Client);
        public static readonly ApiOperation GetMapCustomText = new ApiOperation("GetMapCustomText", GetMap.Path + CustomText, ApiCallerType.Client, TimestampMetaData.MapCustomText);
        public static readonly ApiOperation GetMapCustomTextTimestamp = new ApiOperation("GetMapCustomTextTimestamp", GetMap.Path + CustomText + Timestamp, ApiCallerType.Client);

        // GetMenu
        public static readonly ApiOperation GetMenu = new ApiOperation("GetMenu", "/menu/{MenuId}", ApiCallerType.Client, TimestampMetaData.Menu);
        public static readonly ApiOperation GetMenuTimestamp = new ApiOperation("GetMenuTimestamp", GetMenu.Path + Timestamp, ApiCallerType.Client);
        public static readonly ApiOperation GetMenuMedia = new ApiOperation("GetMenuMedia", GetMenu.Path + Media, ApiCallerType.Client, TimestampMetaData.MenuMedia);
        public static readonly ApiOperation GetMenuMediaTimestamp = new ApiOperation("GetMenuMediaTimestamp", GetMenu.Path + Media + Timestamp, ApiCallerType.Client);
        public static readonly ApiOperation GetMenuCustomText = new ApiOperation("GetMenuCustomText", GetMenu.Path + CustomText, ApiCallerType.Client, TimestampMetaData.MenuCustomText);
        public static readonly ApiOperation GetMenuCustomTextTimestamp = new ApiOperation("GetMenuCustomTextTimestamp", GetMenu.Path + CustomText + Timestamp, ApiCallerType.Client);

        // GetMessagegroups
        public static readonly ApiOperation GetMessagegroups = new ApiOperation("GetMessagegroups", "/messagegroups/{MessagegroupIds}", ApiCallerType.Terminal, TimestampMetaData.Messagegroups);

        // GetMessagegroupsForCompany
        public static readonly ApiOperation GetMessagegroupsForCompany = new ApiOperation("GetMessagegroupsForCompany", "/company/{CompanyId}/messagegroups", ApiCallerType.Terminal, TimestampMetaData.Messagegroups);
        public static readonly ApiOperation GetMessagegroupsForCompanyTimestamp = new ApiOperation("GetMessagegroupsForCompanyTimestamp", GetMessagegroupsForCompany.Path + Timestamp, ApiCallerType.Terminal);

        // GetMessages
        public static readonly ApiOperation GetMessages = new ApiOperation("GetMessages", "/deliverypoint/{DeliverypointId}/messages", ApiCallerType.Client);
        public static readonly ApiOperation GetMessagesTimestamp = new ApiOperation("GetMessagesTimestamp", GetMessages.Path + Timestamp, ApiCallerType.Client);

        // GetMessageTemplates
        public static readonly ApiOperation GetMessageTemplates = new ApiOperation("GetMessageTemplates", "/terminal/{TerminalId}/messagetemplates", ApiCallerType.Terminal, TimestampMetaData.MessageTemplates);
        public static readonly ApiOperation GetMessageTemplatesTimestamp = new ApiOperation("GetMessageTemplatesTimestamp", GetMessageTemplates.Path + Timestamp, ApiCallerType.Terminal, TimestampMetaData.MessageTemplates);

        // GetNetmessages
        public static readonly ApiOperation GetNetmessages = new ApiOperation("GetNetmessages", "/device/{Identifier}/netmessages", ApiCallerType.Client);
        public static readonly ApiOperation GetNetmessagesTimestamp = new ApiOperation("GetNetmessagesTimestamp", GetNetmessages.Path + Timestamp, ApiCallerType.Client);

        // GetOrders
        public static readonly ApiOperation GetOrders = new ApiOperation("GetOrders", "/terminal/{TerminalId}/orders", ApiCallerType.Terminal);
        public static readonly ApiOperation GetOrdersTimestamp = new ApiOperation("GetOrdersTimestamp", GetOrders.Path + Timestamp, ApiCallerType.Terminal);

        // GetOrdersHistory
        public static readonly ApiOperation GetOrdersHistory = new ApiOperation("GetOrdersHistory", "/terminal/{TerminalId}/orders/history", ApiCallerType.Terminal);
        public static readonly ApiOperation GetOrdersHistoryTimestamp = new ApiOperation("GetOrdersHistoryTimestamp", GetOrdersHistory.Path + Timestamp, ApiCallerType.Terminal);

        // GetOrdersMaster
        public static readonly ApiOperation GetOrdersMaster = new ApiOperation("GetOrdersMaster", "/terminal/{TerminalId}/orders/master", ApiCallerType.Terminal);
        public static readonly ApiOperation GetOrdersMasterTimestamp = new ApiOperation("GetOrdersMasterTimestamp", GetOrdersMaster.Path + Timestamp, ApiCallerType.Terminal);

        // GetPendingOrderStatus
        public static readonly ApiOperation GetPendingOrderStatus = new ApiOperation("GetPendingOrderStatus", "/orders/{OrderIds}/status", ApiCallerType.Client);

        // GetPmsTerminalStatus
        public static readonly ApiOperation GetPmsTerminalStatus = new ApiOperation("GetPmsTerminalStatus", "/company/{CompanyId}/pmsterminalstatus", ApiCallerType.Terminal);
        public static readonly ApiOperation GetPmsTerminalStatusTimestamp = new ApiOperation("GetPmsTerminalStatusTimestamp", GetPmsTerminalStatus.Path + Timestamp, ApiCallerType.Terminal);

        // GetPointOfInterest
        public static readonly ApiOperation GetPointOfInterest = new ApiOperation("GetPointOfInterest", "/pointofinterest/{PointOfInterestId}", ApiCallerType.Client, TimestampMetaData.PointOfInterest);
        public static readonly ApiOperation GetPointOfInterestTimestamp = new ApiOperation("GetPointOfInterestTimestamp", GetPointOfInterest.Path + Timestamp, ApiCallerType.Client);
        public static readonly ApiOperation GetPointOfInterestMedia = new ApiOperation("GetPointOfInterestMedia", GetPointOfInterest.Path + Media, ApiCallerType.Client, TimestampMetaData.PointOfInterestMedia);
        public static readonly ApiOperation GetPointOfInterestMediaTimestamp = new ApiOperation("GetPointOfInterestMediaTimestamp", GetPointOfInterest.Path + Media + Timestamp, ApiCallerType.Client);
        public static readonly ApiOperation GetPointOfInterestCustomText = new ApiOperation("GetPointOfInterestCustomText", GetPointOfInterest.Path + CustomText, ApiCallerType.Client, TimestampMetaData.PointOfInterestCustomText);
        public static readonly ApiOperation GetPointOfInterestCustomTextTimestamp = new ApiOperation("GetPointOfInterestCustomTextTimestamp", GetPointOfInterest.Path + CustomText + Timestamp, ApiCallerType.Client);

        // GetPointOfInterestAmenities

        // GetPointOfInterestVenueCategories
        public static readonly ApiOperation GetPointOfInterestVenueCategories = new ApiOperation("GetPointOfInterestVenueCategories", "/pointofinterest/{PointOfInterestId}/venuecategories", ApiCallerType.Client, TimestampMetaData.PointOfInterestVenueCategories);
        public static readonly ApiOperation GetPointOfInterestVenueCategoriesTimestamp = new ApiOperation("GetPointOfInterestVenueCategoriesTimestamp", GetPointOfInterestVenueCategories.Path + Timestamp, ApiCallerType.Client);
        public static readonly ApiOperation GetPointOfInterestVenueCategoriesCustomText = new ApiOperation("GetPointOfInterestVenueCategoriesCustomText", GetPointOfInterestVenueCategories.Path + CustomText, ApiCallerType.Client, TimestampMetaData.PointOfInterestVenueCategoriesCustomText);
        public static readonly ApiOperation GetPointOfInterestVenueCategoriesCustomTextTimestamp = new ApiOperation("GetPointOfInterestVenueCategoriesCustomTextTimestamp", GetPointOfInterestVenueCategories.Path + CustomText + Timestamp, ApiCallerType.Client);

        // GetPriceSchedule
        public static readonly ApiOperation GetPriceSchedule = new ApiOperation("GetPriceSchedule", "/priceschedule/{PriceScheduleId}", ApiCallerType.Client, TimestampMetaData.PriceSchedule);
        public static readonly ApiOperation GetPriceScheduleTimestamp = new ApiOperation("GetPriceScheduleTimestamp", GetPriceSchedule.Path + Timestamp, ApiCallerType.Client);

        // GetProductgroups
        public static readonly ApiOperation GetProductgroups = new ApiOperation("GetProductgroups", "/menu/{MenuId}/productgroups", ApiCallerType.Client, TimestampMetaData.Productgroups);
        public static readonly ApiOperation GetProductgroupsTimestamp = new ApiOperation("GetProductgroupsTimestamp", GetProductgroups.Path + Timestamp, ApiCallerType.Client);
        public static readonly ApiOperation GetProductgroupsMedia = new ApiOperation("GetProductgroupsMedia", GetProductgroups.Path + Media, ApiCallerType.Client, TimestampMetaData.ProductgroupsMedia);
        public static readonly ApiOperation GetProductgroupsMediaTimestamp = new ApiOperation("GetProductgroupsMediaTimestamp", GetProductgroups.Path + Media + Timestamp, ApiCallerType.Client);
        public static readonly ApiOperation GetProductgroupsCustomText = new ApiOperation("GetProductgroupsCustomText", GetProductgroups.Path + CustomText, ApiCallerType.Client, TimestampMetaData.ProductgroupsCustomText);
        public static readonly ApiOperation GetProductgroupsCustomTextTimestamp = new ApiOperation("GetProductgroupsCustomTextTimestamp", GetProductgroups.Path + CustomText + Timestamp, ApiCallerType.Client);

        // GetProducts
        public static readonly ApiOperation GetProducts = new ApiOperation("GetProducts", "/menu/{MenuId}/products", ApiCallerType.Client, TimestampMetaData.Products);
        public static readonly ApiOperation GetProductsTimestamp = new ApiOperation("GetProductsTimestamp", GetProducts.Path + Timestamp, ApiCallerType.Client);
        public static readonly ApiOperation GetProductsMedia = new ApiOperation("GetProductsMedia", GetProducts.Path + Media, ApiCallerType.Client, TimestampMetaData.ProductsMedia);
        public static readonly ApiOperation GetProductsMediaTimestamp = new ApiOperation("GetProductsMediaTimestamp", GetProducts.Path + Media + Timestamp, ApiCallerType.Client);
        public static readonly ApiOperation GetProductsCustomText = new ApiOperation("GetProductsCustomText", GetProducts.Path + CustomText, ApiCallerType.Client, TimestampMetaData.ProductsCustomText);
        public static readonly ApiOperation GetProductsCustomTextTimestamp = new ApiOperation("GetProductsCustomTextTimestamp", GetProducts.Path + CustomText + Timestamp, ApiCallerType.Client);

        // GetRelease
        public static readonly ApiOperation GetRelease = new ApiOperation("GetRelease", "/release/{ApplicationCode}", ApiCallerType.Client);
        public static readonly ApiOperation GetReleaseTimestamp = new ApiOperation("GetReleaseTimestamp", GetRelease.Path + Timestamp, ApiCallerType.Client);

        // GetRoomControlConfiguration
        public static readonly ApiOperation GetRoomControlConfiguration = new ApiOperation("GetRoomControlConfiguration", "/roomcontrolconfiguration/{RoomControlConfigurationId}", ApiCallerType.Client, TimestampMetaData.RoomControlConfiguration);
        public static readonly ApiOperation GetRoomControlConfigurationTimestamp = new ApiOperation("GetRoomControlConfigurationTimestamp", GetRoomControlConfiguration.Path + Timestamp, ApiCallerType.Client);
        public static readonly ApiOperation GetRoomControlConfigurationMedia = new ApiOperation("GetRoomControlConfigurationMedia", GetRoomControlConfiguration.Path + Media, ApiCallerType.Client, TimestampMetaData.RoomControlConfigurationMedia);
        public static readonly ApiOperation GetRoomControlConfigurationMediaTimestamp = new ApiOperation("GetRoomControlConfigurationMediaTimestamp", GetRoomControlConfiguration.Path + Media + Timestamp, ApiCallerType.Client);
        public static readonly ApiOperation GetRoomControlConfigurationCustomText = new ApiOperation("GetRoomControlConfigurationCustomText", GetRoomControlConfiguration.Path + CustomText, ApiCallerType.Client, TimestampMetaData.RoomControlConfigurationCustomText);
        public static readonly ApiOperation GetRoomControlConfigurationCustomTextTimestamp = new ApiOperation("GetRoomControlConfigurationCustomTextTimestamp", GetRoomControlConfiguration.Path + CustomText + Timestamp, ApiCallerType.Client);

        // GetSchedules
        public static readonly ApiOperation GetSchedules = new ApiOperation("GetSchedules", "/schedules/{MenuId}", ApiCallerType.Client, TimestampMetaData.Schedules);
        public static readonly ApiOperation GetSchedulesTimestamp = new ApiOperation("GetSchedulesTimestamp", GetSchedules.Path + Timestamp, ApiCallerType.Client);

        // GetSite
        public static readonly ApiOperation GetSite = new ApiOperation("GetSite", "/site/{SiteId}/Cultures/{CultureCode}", ApiCallerType.Client, TimestampMetaData.Site);
        public static readonly ApiOperation GetSiteTimestamp = new ApiOperation("GetSiteTimestamp", GetSite.Path + Timestamp, ApiCallerType.Client);
        public static readonly ApiOperation GetSiteMedia = new ApiOperation("GetSiteMedia", GetSite.Path + Media, ApiCallerType.Client, TimestampMetaData.SiteMedia);
        public static readonly ApiOperation GetSiteMediaTimestamp = new ApiOperation("GetSiteMediaTimestamp", GetSite.Path + Media + Timestamp, ApiCallerType.Client);
        public static readonly ApiOperation GetSiteCustomText = new ApiOperation("GetSiteCustomText", GetSite.Path + CustomText, ApiCallerType.Client, TimestampMetaData.SiteCustomText);
        public static readonly ApiOperation GetSiteCustomTextTimestamp = new ApiOperation("GetSiteCustomTextTimestamp", GetSite.Path + CustomText + Timestamp, ApiCallerType.Client);

        // GetTerminal
        public static readonly ApiOperation GetTerminal = new ApiOperation("GetTerminal", "/terminal/{TerminalId}", ApiCallerType.Client, TimestampMetaData.Terminal);
        public static readonly ApiOperation GetTerminalTimestamp = new ApiOperation("GetTerminalTimestamp", GetTerminal.Path + Timestamp, ApiCallerType.Client);

        // GetTerminalConfiguration
        public static readonly ApiOperation GetTerminalConfiguration = new ApiOperation("GetTerminalConfiguration", "/terminalconfiguration/{TerminalConfigurationId}", ApiCallerType.Client, TimestampMetaData.TerminalConfiguration);
        public static readonly ApiOperation GetTerminalConfigurationTimestamp = new ApiOperation("GetTerminalConfigurationTimestamp", GetTerminalConfiguration.Path + Timestamp, ApiCallerType.Client);

        // GetTimeUtc
        public static readonly ApiOperation GetTimeUtc = new ApiOperation("GetTimeUtc", "/timeutc", ApiCallerType.Client);

        // GetUIMode
        public static readonly ApiOperation GetUIMode = new ApiOperation("GetUIMode", "/uimode/{UIModeId}", ApiCallerType.Client, TimestampMetaData.UIMode);
        public static readonly ApiOperation GetUIModeTimestamp = new ApiOperation("GetUIModeTimestamp", GetUIMode.Path + Timestamp, ApiCallerType.Client);
        public static readonly ApiOperation GetUIModeMedia = new ApiOperation("GetUIModeMedia", GetUIMode.Path + Media, ApiCallerType.Client, TimestampMetaData.UIModeMedia);
        public static readonly ApiOperation GetUIModeMediaTimestamp = new ApiOperation("GetUIModeMediaTimestamp", GetUIMode.Path + Media + Timestamp, ApiCallerType.Client);
        public static readonly ApiOperation GetUIModeCustomText = new ApiOperation("GetUIModeCustomText", GetUIMode.Path + CustomText, ApiCallerType.Client, TimestampMetaData.UIModeCustomText);
        public static readonly ApiOperation GetUIModeCustomTextTimestamp = new ApiOperation("GetUIModeCustomTextTimestamp", GetUIMode.Path + CustomText + Timestamp, ApiCallerType.Client);

        // GetUIModes
        public static readonly ApiOperation GetUIModes = new ApiOperation("GetUIModes", "/company/{CompanyId}/uimodes", ApiCallerType.Client, TimestampMetaData.UIModes);
        public static readonly ApiOperation GetUIModesTimestamp = new ApiOperation("GetUIModesTimestamp", GetUIModes.Path + Timestamp, ApiCallerType.Client);
        public static readonly ApiOperation GetUIModesMedia = new ApiOperation("GetUIModesMedia", GetUIModes.Path + Media, ApiCallerType.Client, TimestampMetaData.UIModesMedia);
        public static readonly ApiOperation GetUIModesMediaTimestamp = new ApiOperation("GetUIModesMediaTimestamp", GetUIModes.Path + Media + Timestamp, ApiCallerType.Client);
        public static readonly ApiOperation GetUIModesCustomText = new ApiOperation("GetUIModesCustomText", GetUIModes.Path + CustomText, ApiCallerType.Client, TimestampMetaData.UIModesCustomText);
        public static readonly ApiOperation GetUIModesCustomTextTimestamp = new ApiOperation("GetUIModesCustomTextTimestamp", GetUIModes.Path + CustomText + Timestamp, ApiCallerType.Client);

        // GetUISchedules
        public static readonly ApiOperation GetUISchedule = new ApiOperation("GetUISchedule", "/uischedule/{UIScheduleId}", ApiCallerType.Client, TimestampMetaData.UISchedule);
        public static readonly ApiOperation GetUIScheduleTimestamp = new ApiOperation("GetUIScheduleTimestamp", GetUISchedule.Path + Timestamp, ApiCallerType.Client);

        // GetUITheme
        public static readonly ApiOperation GetUITheme = new ApiOperation("GetUITheme", "/uitheme/{UIThemeId}", ApiCallerType.Client, TimestampMetaData.UITheme);
        public static readonly ApiOperation GetUIThemeTimestamp = new ApiOperation("GetUIThemeTimestamp", GetUITheme.Path + Timestamp, ApiCallerType.Client);
        public static readonly ApiOperation GetUIThemeMedia = new ApiOperation("GetUIThemeMedia", GetUITheme.Path + Media, ApiCallerType.Client, TimestampMetaData.UIThemeMedia);
        public static readonly ApiOperation GetUIThemeMediaTimestamp = new ApiOperation("GetUIThemeMediaTimestamp", GetUITheme.Path + Media + Timestamp, ApiCallerType.Client);

        static ApiMetaData()
        {
            Operations = new Dictionary<string, ApiOperation>();

            foreach (FieldInfo field in typeof(ApiMetaData).GetFields(BindingFlags.Public | BindingFlags.Static))
            {
                ApiOperation apiOperation = field.GetValue(null) as ApiOperation;
                if (apiOperation != null)
                    Operations.Add(field.Name, apiOperation);
            }
        }

        public static Dictionary<string, ApiOperation> Operations { get; private set; }
    }
}
