﻿using System;
using System.Net;

namespace Crave.Api.Logic
{
    public class ApiCall
    {
        #region Properties

        public string BaseUrl { get; set; }

        public string Path { get; set; }

        public string RequestName { get; set; }

        public ApiOperation ApiOperation { get; set; }

        public ApiCallAuthentication Authentication { get; set; }

        #endregion

        #region Methods

        public string Execute()
        {
            string result = string.Empty;
            string url = this.GetUrlForApiCall();

            try
            {
                using (WebClient webClient = new WebClient())
                {
                    webClient.Proxy = null;
                    result = webClient.DownloadString(url);
                }
            }
            catch (Exception ex)
            {
                throw new WebException(string.Format("An exception occurred while executing the API call on url: {0}. Message: {1}", url, ex.Message));
            }

            return result;
        }

        public string GetUrlForApiCall()
        {
            string url = this.BaseUrl;

            if (url.EndsWith("/"))
                url = url.Substring(0, url.Length - 1);

            url += this.Path;
            url += this.Authentication.GetAuthenticationQueryString(this.RequestName);
            url += "&format=json";
            url += "&diag=true";

            return url;
        }

        #endregion
    }
}
