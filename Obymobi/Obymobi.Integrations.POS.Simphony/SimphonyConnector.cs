﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Xml.Serialization;
using Dionysos;
using Obymobi.Enums;
using Obymobi.Integrations.POS.Interfaces;
using Obymobi.Integrations.POS.Simphony.Models.Requests;
using Obymobi.Integrations.POS.Simphony.Models.Responses;
using Obymobi.Integrations.POS.Simphony.simphonyposapiweb;
using Obymobi.Logging;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;

namespace Obymobi.Integrations.POS.Simphony
{
    public class SimphonyConnector : IPOSConnector
    {
        private readonly SimphonyConfigurationAdapter configuration;

        private const int MaxCondimentGroups = 256;

        private readonly SimphonyPosAPIWeb simphonyApi;

        public SimphonyConnector(SimphonyConfigurationAdapter configAdapter)
        {
            this.configuration = configAdapter;

            this.simphonyApi = new SimphonyPosAPIWeb
            {
                Url = this.configuration.WebserviceUrl
            };

            if (this.simphonyApi.Url.IsNullOrWhiteSpace())
            {
                WriteToLogExtended("SimphonyConnector", "Webservice url is empty");
            }

            WriteToLogExtended("SimphonyConnector", "WebserviceUrl: {0}", this.configuration.WebserviceUrl);
            WriteToLogExtended("SimphonyConnector", "Vendor Code: {0}", this.configuration.VendorCode);
            WriteToLogExtended("SimphonyConnector", "Employee Object Number: {0}", this.configuration.EmployeeObjectNum);
            WriteToLogExtended("SimphonyConnector", "Tender Media Object Number: {0}", this.configuration.TenderMediaObjectNum);
            WriteToLogExtended("SimphonyConnector", "Check Order Type: {0}", this.configuration.OrderType);
            WriteToLogExtended("SimphonyConnector", "Revenue Centers: {0}", string.Join(",", this.configuration.RevenueCenters));
            WriteToLogExtended("SimphonyConnector", "Use Old Post Transaction Method: {0}", this.configuration.UseOldPostTransactionMethod);
            WriteToLogExtended("SimphonyConnector", "Sync In Stock Products Only: {0}", this.configuration.SyncInStockProductsOnly);
        }

        public Poscategory[] GetPoscategories()
        {
            List<Poscategory> posCategories = new List<Poscategory>();

            foreach (int revenueCenter in this.configuration.RevenueCenters)
            {
                List<FamilyGroups.DbFamilyGroup> familyGroups = GetFamilyGroups(revenueCenter);
                List<MajorGroups.DbMajorGroup> majorGroups = GetMajorGroups(revenueCenter);

                foreach (FamilyGroups.DbFamilyGroup familyGroup in familyGroups)
                {

                    if (int.TryParse(familyGroup.FamGrpID, out int familyGroupId))
                    {
                        posCategories.Add(new Poscategory(familyGroupId, revenueCenter, familyGroup.ObjectNumber, familyGroup.Name.StringText, revenueCenter.ToString()));
                    }
                }

                foreach (MajorGroups.DbMajorGroup majorGroup in majorGroups)
                {

                    if (int.TryParse(majorGroup.MajGrpID, out int majorGroupId))
                    {
                        posCategories.Add(new Poscategory(majorGroupId, revenueCenter, majorGroup.ObjectNumber, majorGroup.Name.StringText, revenueCenter.ToString()));
                    }
                }
            }

            return posCategories.ToArray();
        }

        public Posdeliverypointgroup[] GetPosdeliverypointgroups() => new Posdeliverypointgroup[0];

        public Posdeliverypoint[] GetPosdeliverypoints()
        {
            List<Posdeliverypoint> posdeliverypoints = new List<Posdeliverypoint>();

            foreach (int revenueCenter in this.configuration.RevenueCenters)
            {
                List<Tables.DbDiningTable> diningTables = GetDiningTables(revenueCenter);

                posdeliverypoints.AddRange(diningTables.Select(d => new Posdeliverypoint
                {
                    ExternalId = d.ObjectNumber,
                    Number = d.ObjectNumber,
                    Name = d.Name.StringText,
                    RevenueCenter = revenueCenter.ToString()
                }));
            }

            return posdeliverypoints.ToArray();
        }

        public void PostPosSynchronisation()
        {

        }

        public Posorder GetPosorder(string deliverypointNumber) => null;

        public OrderProcessingError SaveOrder(Posorder posorder)
        {
            try
            {
                foreach (int revenueCenter in this.configuration.RevenueCenters)
                {
                    if (this.configuration.UseOldPostTransactionMethod)
                    {
                        PostTransactionExRequest request = BuildPostTransactionExRequest(posorder, revenueCenter);

                        string requestXml = XmlHelper.Serialize(request);
                        WriteToLogExtended("SaveOrder", "Submit order request:\n\r{0}", requestXml);

                        DoPostTransactionEx(request);

                        string responseXml = XmlHelper.Serialize(request.pTotalsResponse);

                        return ProcessOrderOperationalResult(requestXml, responseXml,
                            request.pTotalsResponse.OperationalResult, posorder);

                    }
                    else
                    {
                        PostTransactionEx2Request request = BuildPostTransactionEx2Request(posorder, revenueCenter);

                        string requestXml = XmlHelper.Serialize(request);
                        WriteToLogExtended("SaveOrder", "Submit order request:\n\r{0}", requestXml);

                        DoPostTransactionEx2(request);

                        string responseXml = XmlHelper.Serialize(request.pTotalsResponseEx);

                        return ProcessOrderOperationalResult(requestXml, responseXml,
                            request.pTotalsResponseEx.OperationalResult, posorder);
                    }
                }

                //No revenue centers configured
                return OrderProcessingError.PosErrorConfigurationIncorrect;
            }
            catch (WebException)
            {
                posorder.ErrorMessage = "Unable to communicate with Simphony API";
                return OrderProcessingError.PosErrorConnectivityProblem;
            }
        }

        public bool PrintConfirmationReceipt(string confirmationCode, string name, string phonenumber, string deliverypointNumber) => true;

        public bool PrintServiceRequestReceipt(string serviceDescription) => true;

        public bool PrintCheckoutRequestReceipt(string checkoutDescription) => true;

        public bool NotifyLockedDeliverypoint() => true;

        public string ErrorMessage { get; set; }

        public void PrePosSynchronisation()
        {

        }

        public Posproduct[] GetPosproducts()
        {
            List<Posproduct> posproducts = new List<Posproduct>();

            foreach (int revenueCenter in configuration.RevenueCenters)
            {
                List<DbMenuItemDefinition> menuItemDefinitions = GetMenuItemDefinitions(revenueCenter);
                List<DbMenuItemClass> menuItemClasses = GetMenuItemClasses(revenueCenter);
                List<DbMenuItemPrice> menuItemPrices = GetMenuItemPrices(revenueCenter);

                //Alteration Items
                List<DbMenuItemClass> condimentMenuItemClasses = menuItemClasses.Where(m => BinaryStringHasBitSet(m.MemberOfCondiments)).ToList();

                foreach (DbMenuItemDefinition menuItemDefinition in menuItemDefinitions)
                {
                    if (configuration.SyncInStockProductsOnly &&
                        bool.TryParse(menuItemDefinition.OutOfMenuItem, out bool outOfSock) && outOfSock)
                    {
                        continue;
                    }

                    DbMenuItemClass menuItemClass = menuItemClasses.SingleOrDefault(c =>
                        c.ObjectNumber == menuItemDefinition.MenuItemClassObjNum);

                    //MenuItemDefinition is a Product if its linked MenuItemClass has a binary string with bits set for RequiredCondiments (If the MenuItemDefinition is an Alteration then the MemberOfCondiments binary will have bits set instead)
                    if (menuItemClass != null && BinaryStringHasBitSet(menuItemDefinition.MainLevels) &&
                        !BinaryStringHasBitSet(menuItemClass.MemberOfCondiments))
                    {
                        Posproduct posproduct = new Posproduct
                        {
                            ExternalId = menuItemDefinition.MiMasterObjNum, //Used for adding this item to a Simphony order
                            CompanyId = revenueCenter,
                            Name = menuItemDefinition.Name1.StringText,
                            Description = menuItemDefinition.LongDescriptor.StringText,
                            PriceIn = GetPriceForProduct(menuItemDefinition.MenuItemDefID,
                                menuItemDefinition.SequenceNum, menuItemPrices),

                            /* PRODUCT ALTERATION CONVERSION
                            
                            1. Find all the MenuItemClass where RequiredCondiments or AllowedCondiments has a 1 set which match the bit set for the MemberOfCondiments field for the MenuItemClass beloning to the Product's MenuItemDefinition
                            1.1. Convert these PosAlteration
                            
                            2. Find all MenuItemClass where MemberOfCondiment flag corresponds with step 1 flag
                            
                            3. Find MenuItemDefinitions where MenuItemClassObjNum equals step 2 MenuItemClass.ObjectNumber
                            3.1. Convert to PosAlterationoption
                            3.2. Set fieldvalue1 to say if required or not (from model of step 2)

                            */

                            Posalterations = GetPosalterationsForProduct(
                                menuItemDefinition,
                                menuItemClass,
                                menuItemDefinitions,
                                condimentMenuItemClasses,
                                revenueCenter),
                            VatTariff = 1,
                            RevenueCenter = revenueCenter.ToString()

                        };

                        if (menuItemDefinition.MenuLevelEntries != null &&
                            menuItemDefinition.MenuLevelEntries.DbMenuItemMenuLevelEntry.Any())
                        {
                            //ToDo: Some assumptions being made here: the example data suggests that MenuLevelEntries is either empty or has two entries. I'm assuming that the first one is used for the main level and the second for the sublevel when making an order.
                            //ToDo: Should we store EntryIndex or ObjectNumber here? Example data suggests both always fall under range 0-8... Chose ObjectNumber as it always seems to be in ascending order
                            posproduct.FieldValue2 = menuItemDefinition.MenuLevelEntries.DbMenuItemMenuLevelEntry[0].MenuLevelObjNum;
                            posproduct.FieldValue3 = menuItemDefinition.MenuLevelEntries.DbMenuItemMenuLevelEntry[1].MenuLevelObjNum;
                        }

                        posproducts.Add(posproduct);
                    }
                }
            }

            return posproducts.ToArray();
        }

        private OrderProcessingError ProcessOrderOperationalResult(string requestXml, string responseXml, SimphonyPosApi_OperationalResult operationalResult, Posorder posorder)
        {
            WriteToLogExtended("SaveOrder", "Submit order response:\n\r{0}", responseXml);

            if (!operationalResult.Success)
            {
                if (operationalResult.ErrorMessage != null)
                {
                    if (operationalResult.ErrorMessage.Contains("Object reference not set to an instance of an object"))
                    {
                        posorder.ErrorMessage = "Attempted to place an order with either an unknown product or tender media or the chosen product was out of stock";
                    }
                    else
                    {
                        posorder.ErrorMessage = $"{operationalResult.ErrorMessage}\n";
                    }

                    WriteToLogExtended("SaveOrder", $"Order could not be saved, Request: {requestXml}, Response: {responseXml}");
                }
                else
                {
                    posorder.ErrorMessage = "No error message from POS";
                }

                return OrderProcessingError.PosErrorSaveOrderFailed;
            }
            WriteToLogExtended("SaveOrder", "Order saved successfully.");
            return OrderProcessingError.None;
        }

        private PostTransactionExRequest BuildPostTransactionExRequest(Posorder posorder, int revenueCenter)
        {
            PostTransactionExRequest request = new PostTransactionExRequest();

            List<SimphonyPosApi_MenuItemDefinition> alterations = new List<SimphonyPosApi_MenuItemDefinition>();
            List<SimphonyPosApi_MenuItem> ppMenuItems = new List<SimphonyPosApi_MenuItem>();

            List<string> orderNotes = new List<string>();
            orderNotes.Add(posorder.Notes);

            request.VendorCode = this.configuration.VendorCode;

            request.pGuestCheck = new SimphonyPosApi_GuestCheck
            {
                CheckGuestCount = posorder.GuestCount,
                CheckEmployeeObjectNum = this.configuration.EmployeeObjectNum,
                CheckRevenueCenterID = revenueCenter,
                CheckTableObjectNum = int.Parse(posorder.PosdeliverypointExternalId),
                PPrintJobIds = new List<int>().ToArray(),
                PCheckInfoLines = new List<string>().ToArray(),
                CheckDateToFire = DateTime.UtcNow,
                CheckOrderType = this.configuration.OrderType
            };

            request.pTmedDetail = new SimphonyPosApi_TmedDetailItemEx()
            {
                TmedEPayment = new SimphonyPosApi_EPayment(),
                TmedObjectNum = this.configuration.TenderMediaObjectNum
            };

            request.ppComboMeals = new SimphonyPosApi_ComboMeal[] { };
            request.pSubTotalDiscount = new SimphonyPosApi_Discount { };
            request.pServiceChg = new SimphonyPosApi_SvcCharge { };
            request.pTotalsResponse = new SimphonyPosApi_TotalsResponse { };
            request.ppVoucherOutput = new string[] { };

            foreach (Posorderitem orderitem in posorder.Posorderitems)
            {
                if (!string.IsNullOrWhiteSpace(orderitem.Notes))
                {
                    orderNotes.Add(orderitem.Notes);
                }

                if (orderitem.Type == OrderitemType.Product)
                {
                    SimphonyPosApi_MenuItemDefinition product = new SimphonyPosApi_MenuItemDefinition
                    {
                        MiObjectNum = int.Parse(orderitem.PosproductExternalId),
                        // Shows up in the POS as an extra description under the product - not sure if we want to show this - keeping it for now
                        //MiReference = orderitem.Description 
                    };

                    if (orderitem.Posalterationitems != null && orderitem.Posalterationitems.Any())
                    {
                        foreach (Posalterationitem alteration in orderitem.Posalterationitems)
                        {
                            if (!string.IsNullOrWhiteSpace(alteration.ExternalPosalterationoptionId))
                            {
                                alterations.Add(new SimphonyPosApi_MenuItemDefinition()
                                {
                                    MiObjectNum = int.Parse(alteration.ExternalPosalterationoptionId),
                                });
                            }
                        }
                    }

                    ppMenuItems.Add(new SimphonyPosApi_MenuItem()
                    {
                        MenuItem = product,
                        Condiments = alterations.ToArray()
                    });
                }

                if (orderitem.Type == OrderitemType.Tip)
                {
                    //ToDo: Not sure if this is the right place
                    request.pSubTotalDiscount = new SimphonyPosApi_Discount
                    {
                        DiscAmountOrPercent = orderitem.PriceIn.ToString()
                    };
                }

                if (orderitem.Type == OrderitemType.ServiceCharge)
                {
                    request.pServiceChg.SvcChgAmountOrPercent = orderitem.PriceIn.ToString();
                }
            }

            request.ppMenuItems = ppMenuItems.ToArray();
            request.pGuestCheck.PCheckInfoLines = orderNotes.ToArray();

            return request;
        }

        private void DoPostTransactionEx(PostTransactionExRequest request)
        {
            SimphonyPosApi_GuestCheck pGuestCheck = request.pGuestCheck;
            SimphonyPosApi_MenuItem[] ppMenuItems = request.ppMenuItems;
            SimphonyPosApi_ComboMeal[] ppComboMeals = request.ppComboMeals;
            SimphonyPosApi_Discount pSubTotalDiscount = request.pSubTotalDiscount;
            SimphonyPosApi_TmedDetailItemEx pTmedDetail = request.pTmedDetail;
            SimphonyPosApi_SvcCharge pSvcChg = request.pServiceChg;
            SimphonyPosApi_TotalsResponse pTotalsResponse = new SimphonyPosApi_TotalsResponse();
            string[] ppCheckPrintLines = new string[] { };
            string[] ppVoucherOutput = new string[] { };

            this.simphonyApi.PostTransactionEx(this.configuration.VendorCode, ref pGuestCheck, ref ppMenuItems, ref ppComboMeals, ref pSvcChg, ref pSubTotalDiscount, ref pTmedDetail, ref pTotalsResponse, ref ppCheckPrintLines, ref ppVoucherOutput);

            request.pTotalsResponse = pTotalsResponse;
        }

        private PostTransactionEx2Request BuildPostTransactionEx2Request(Posorder posorder, int revenueCenter)
        {
            PostTransactionEx2Request request = new PostTransactionEx2Request();

            List<SimphonyPosApi_MenuItemDefinitionEx> alterations = new List<SimphonyPosApi_MenuItemDefinitionEx>();
            List<SimphonyPosApi_MenuItemEx> ppMenuItemsEx = new List<SimphonyPosApi_MenuItemEx>();
            List<SimphonyPosApi_DiscountEx> pSubTotalDiscountEx = new List<SimphonyPosApi_DiscountEx>();

            List<string> orderNotes = new List<string>();
            orderNotes.Add(posorder.Notes);

            request.pGuestCheck = new SimphonyPosApi_GuestCheck
            {
                CheckGuestCount = posorder.GuestCount,
                CheckEmployeeObjectNum = this.configuration.EmployeeObjectNum,
                CheckRevenueCenterID = revenueCenter,
                CheckTableObjectNum = int.Parse(posorder.PosdeliverypointExternalId),
                PPrintJobIds = new List<int>().ToArray(),
                PCheckInfoLines = new List<string>().ToArray(),
                CheckDateToFire = DateTime.UtcNow,
                CheckOrderType = 1
            };

            request.ppComboMealsEx = new SimphonyPosApi_ComboMealEx[] { };
            request.pSvcChargeEx = new SimphonyPosApi_SvcChargeEx { };
            request.pTotalsResponseEx = new SimphonyPosApi_TotalsResponseEx { };
            request.ppVoucherOutput = new string[] { };

            request.pTmedDetailEx2 = new SimphonyPosApi_TmedDetailItemEx2[]
            {
                new SimphonyPosApi_TmedDetailItemEx2
                {
                    TmedEPayment = new SimphonyPosApi_EPaymentEx(),
                    TmedObjectNum = this.configuration.TenderMediaObjectNum
                }
            };

            foreach (Posorderitem orderitem in posorder.Posorderitems)
            {
                if (orderitem.Type == OrderitemType.Product)
                {
                    for (int i = 0; i < orderitem.Quantity; i++)
                    {
                        if (!string.IsNullOrWhiteSpace(orderitem.Notes))
                        {
                            orderNotes.Add(orderitem.Notes);
                        }

                        SimphonyPosApi_MenuItemDefinitionEx product = new SimphonyPosApi_MenuItemDefinitionEx
                        {
                            MiObjectNum = int.Parse(orderitem.PosproductExternalId),
                        };

                        if (orderitem.Posalterationitems != null && orderitem.Posalterationitems.Any())
                        {
                            foreach (Posalterationitem alteration in orderitem.Posalterationitems)
                            {
                                if (!string.IsNullOrWhiteSpace(alteration.ExternalPosalterationoptionId))
                                {
                                    alterations.Add(new SimphonyPosApi_MenuItemDefinitionEx()
                                    {
                                        MiObjectNum = int.Parse(alteration.ExternalPosalterationoptionId)
                                    });
                                }
                            }
                        }

                        ppMenuItemsEx.Add(new SimphonyPosApi_MenuItemEx
                        {
                            MenuItem = product,
                            Condiments = alterations.ToArray()
                        });
                    }
                }

                if (orderitem.Type == OrderitemType.ServiceCharge)
                {
                    request.pSvcChargeEx.SvcChgAmountOrPercent = orderitem.PriceIn.ToString();
                }

                if (orderitem.Type == OrderitemType.Tip)
                {
                    //ToDo: Not sure if this is the correct place
                    pSubTotalDiscountEx.Add(new SimphonyPosApi_DiscountEx
                    {
                        DiscAmountOrPercent = orderitem.PriceIn.ToString()
                    });
                }
            }

            request.ppMenuItemsEx = ppMenuItemsEx.ToArray();
            request.pGuestCheck.PCheckInfoLines = orderNotes.ToArray();

            return request;
        }

        private void DoPostTransactionEx2(PostTransactionEx2Request request)
        {
            SimphonyPosApi_GuestCheck pGuestCheck = request.pGuestCheck;
            SimphonyPosApi_MenuItemEx[] ppMenuItems = request.ppMenuItemsEx;
            SimphonyPosApi_ComboMealEx[] ppComboMeals = request.ppComboMealsEx;
            SimphonyPosApi_DiscountEx[] pSubTotalDiscount = request.pSubTotalDiscountEx;
            SimphonyPosApi_TmedDetailItemEx2[] pTmedDetail = request.pTmedDetailEx2;
            SimphonyPosApi_SvcChargeEx pSvcChg = request.pSvcChargeEx;
            SimphonyPosApi_TotalsResponseEx pTotalsResponse = new SimphonyPosApi_TotalsResponseEx();
            string[] ppCheckPrintLines = new string[] { };
            string[] ppVoucherOutput = new string[] { };

            this.simphonyApi.PostTransactionEx2(ref pGuestCheck, ref ppMenuItems, ref ppComboMeals, ref pSvcChg, ref pSubTotalDiscount, ref pTmedDetail, ref pTotalsResponse, ref ppCheckPrintLines, ref ppVoucherOutput, new SimphonyPosApi_Extensibility[] { });

            request.pTotalsResponseEx = pTotalsResponse;
        }

        /// <summary>
        /// Maps MenuItemDefinitions (Alterations) if the MenuItemClass has a condiment group bit set which corresponds to the bit set for the MenuItemDefinition's RequiredCondiment bit.
        /// Once we have determined this, get all MenuItemDefinitions relating to this MenuItemClass as these contain the information we need to map to a POS Alteration
        /// </summary>
        /// <param name="requiredCondimentsForProduct">The binary string indicating which condiments are required for the product</param>
        /// <returns></returns>
        private Posalteration[] GetPosalterationsForProduct(
            DbMenuItemDefinition productMenuItemDefinition,
            DbMenuItemClass menuItemClass,
            List<DbMenuItemDefinition> menuItemDefinitions,
            List<DbMenuItemClass> menuItemClasses,
            int revenueCenter)
        {
            if (!BinaryStringHasBitSet(menuItemClass.RequiredCondiments) && !BinaryStringHasBitSet(menuItemClass.AllowedCondiments))
            {
                return new Posalteration[] { };
            }

            List<Posalteration> posalterations = new List<Posalteration>();

            foreach (DbMenuItemClass alteration in menuItemClasses)
            {
                char[] alterationMemberOfCondimentBits = alteration.MemberOfCondiments.ToCharArray();
                char[] menuItemClassRequiredCondimentsBits = menuItemClass.RequiredCondiments.ToCharArray();
                char[] menuItemClassAllowedCondimentsBits = menuItemClass.AllowedCondiments.ToCharArray();

                for (int i = 0; i < MaxCondimentGroups; i++)
                {
                    if ((alterationMemberOfCondimentBits[i] == menuItemClassRequiredCondimentsBits[i] && menuItemClassRequiredCondimentsBits[i] == '1') ||
                        (alterationMemberOfCondimentBits[i] == menuItemClassAllowedCondimentsBits[i] && menuItemClassAllowedCondimentsBits[i] == '1'))
                    {
                        posalterations.Add(new Posalteration
                        {
                            ExternalId = alteration.MenuItemClassID,
                            RevenueCenter = revenueCenter.ToString(),
                            Name = alteration.Name.StringText,
                            Posalterationoptions = GetPosalterationoptionsForProduct(alteration, menuItemDefinitions, productMenuItemDefinition.MiMasterObjNum, menuItemClassRequiredCondimentsBits[i] == '1', revenueCenter)
                        });
                    }
                }
            }

            return posalterations.GroupBy(p => p.ExternalId).Select(grp => grp.FirstOrDefault()).ToArray();
        }

        private Posalterationoption[] GetPosalterationoptionsForProduct(DbMenuItemClass alteration, List<DbMenuItemDefinition> menuItemDefinitions, string posProductExternalId, bool requiredCondiment, int revenueCenter)
        {
            var alterationoptionMenuItemDefinitions = menuItemDefinitions.Where(m => m.MenuItemClassObjNum == alteration.ObjectNumber);

            List<Posalterationoption> posalterationoptions = new List<Posalterationoption>();

            posalterationoptions.AddRange(alterationoptionMenuItemDefinitions.Select(a => new Posalterationoption
            {
                ExternalId = a.MiMasterObjNum,
                PosproductExternalId = posProductExternalId,
                PosalterationoptionId = int.Parse(a.MenuItemDefID),
                RevenueCenter = revenueCenter.ToString(),
                Name = a.Name1.StringText,
                FieldValue1 = requiredCondiment == true ? "Required" : "Optional"
            }));

            return posalterationoptions.ToArray();
        }

        private List<Tables.DbDiningTable> GetDiningTables(int revenueCenter)
        {
            SimphonyPosApi_ConfigInfoResponse response = new SimphonyPosApi_ConfigInfoResponse();

            this.simphonyApi.GetConfigurationInfo(this.configuration.VendorCode, this.configuration.EmployeeObjectNum, new[] { 18 }, revenueCenter, ref response);

            if (string.IsNullOrWhiteSpace(response.Tables))
            {
                return new List<Tables.DbDiningTable>();
            }

            return MapXmlToEntity<Tables.ArrayOfDbDiningTable>(response.Tables).DbDiningTable;
        }

        private List<MajorGroups.DbMajorGroup> GetMajorGroups(int revenueCenter)
        {
            SimphonyPosApi_ConfigInfoResponse response = new SimphonyPosApi_ConfigInfoResponse();

            this.simphonyApi.GetConfigurationInfo(this.configuration.VendorCode, this.configuration.EmployeeObjectNum, new[] { 9 }, revenueCenter, ref response);

            if (string.IsNullOrWhiteSpace(response.MajorGroup))
            {
                return new List<MajorGroups.DbMajorGroup>();
            }

            return MapXmlToEntity<MajorGroups.ArrayOfDbMajorGroup>(response.MajorGroup).DbMajorGroup;
        }

        private List<FamilyGroups.DbFamilyGroup> GetFamilyGroups(int revenueCenter)
        {
            SimphonyPosApi_ConfigInfoResponse response = new SimphonyPosApi_ConfigInfoResponse();

            this.simphonyApi.GetConfigurationInfo(this.configuration.VendorCode, this.configuration.EmployeeObjectNum, new[] { 8 }, revenueCenter, ref response);

            if (string.IsNullOrWhiteSpace(response.FamilyGroups))
            {
                return new List<FamilyGroups.DbFamilyGroup>();
            }

            return MapXmlToEntity<FamilyGroups.ArrayOfDbFamilyGroup>(response.FamilyGroups).DbFamilyGroup;
        }

        private List<DbMenuItemDefinition> GetMenuItemDefinitions(int revenueCenter)
        {
            SimphonyPosApi_ConfigInfoResponse response = new SimphonyPosApi_ConfigInfoResponse();

            this.simphonyApi.GetConfigurationInfo(this.configuration.VendorCode, this.configuration.EmployeeObjectNum, new[] { 1 }, revenueCenter, ref response);

            if (string.IsNullOrWhiteSpace(response.MenuItemDefinitions))
            {
                return new List<DbMenuItemDefinition>();
            }

            return MapXmlToEntity<ArrayOfDbMenuItemDefinition>(response.MenuItemDefinitions).DbMenuItemDefinition;
        }

        private List<DbMenuItemClass> GetMenuItemClasses(int revenueCenter)
        {
            SimphonyPosApi_ConfigInfoResponse response = new SimphonyPosApi_ConfigInfoResponse();

            this.simphonyApi.GetConfigurationInfo(this.configuration.VendorCode, this.configuration.EmployeeObjectNum, new[] { 3 }, revenueCenter, ref response);

            if (string.IsNullOrWhiteSpace(response.MenuItemClass))
            {
                return new List<DbMenuItemClass>();
            }

            return MapXmlToEntity<ArrayOfDbMenuItemClass>(response.MenuItemClass).DbMenuItemClass;
        }

        private List<DbMenuItemPrice> GetMenuItemPrices(int revenueCenter)
        {
            SimphonyPosApi_ConfigInfoResponse response = new SimphonyPosApi_ConfigInfoResponse();

            this.simphonyApi.GetConfigurationInfo(this.configuration.VendorCode, this.configuration.EmployeeObjectNum, new[] { 2 }, revenueCenter, ref response);

            if (string.IsNullOrWhiteSpace(response.MenuItemPrice))
            {
                return new List<DbMenuItemPrice>();
            }

            return MapXmlToEntity<ArrayOfDbMenuItemPrice>(response.MenuItemPrice).DbMenuItemPrice;
        }

        //ToDo: Making an assumption that a Product MenuItemDefinition can only ever have two associated MenuItemPrices, one with a TaxClassObjNum of 0 and one of non 0. Potential bug
        private decimal GetPriceForProduct(string menuItemDefinitionId, string sequenceNumber, IEnumerable<DbMenuItemPrice> menuItemPrices)
        {
            string price = "0.00";

            //In certain cases, a menu item definition has multiple prices. Only the menu level index and sequence number fields differ between the two. How do we determine which price to use? Using sequence number but not sure if that's correct
            IEnumerable<DbMenuItemPrice> pricesForProduct = menuItemPrices.Where(p => p.MenuItemDefID == menuItemDefinitionId && p.TaxClassObjNum == "0").ToList();

            if (pricesForProduct.Count() == 1)
            {
                price = pricesForProduct.Single().Price;
            }
            else if (pricesForProduct.Count() > 1)
            {
                price = pricesForProduct.Single(p => p.SequenceNum == sequenceNumber).Price;
            }

            return System.Math.Round(decimal.Parse(price, CultureInfo.InvariantCulture), 2,
                MidpointRounding.AwayFromZero);
        }

        private bool BinaryStringHasBitSet(string binaryString) => binaryString.Contains("1");

        /// <summary>
        /// Checks if the MemberOfCondiments binary value has the same bit set as the RequiredCondimentsBinary. We use this to map Alterations to a Product
        /// </summary>
        /// <param name="memberOfCondimentsBinary">MemberOfCondiments binary string from the MenuItemClass (Alteration)</param>
        /// <param name="requiredOrAllowedCondimentsBinary">RequiredCondiments Or AllowedCondiments binary string from the MenuItemDefinition (Product)</param>
        /// <returns></returns>
        private bool MenuItemClassIsAnAlterationForProduct(string memberOfCondimentsBinary, string requiredCondimentsBinary, string allowedCondimentsBinary)
        {
            char[] memberOfCondimentsBinaryArray = memberOfCondimentsBinary.ToCharArray();
            char[] requiredCondimentsBinaryArray = requiredCondimentsBinary.ToCharArray();
            char[] allowedCondimentsBinaryArray = allowedCondimentsBinary.ToCharArray();

            for (int i = 0; i < MaxCondimentGroups; i++)
            {
                if (memberOfCondimentsBinaryArray[i] == '1')
                {
                    if (memberOfCondimentsBinaryArray[i] == requiredCondimentsBinaryArray[i])
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private void WriteToLogExtended(string methodName, string message, params object[] args) => ConsoleLogger.WriteToLog("SimphonyConnector - {0} - {1}", methodName, string.Format(message, args));

        private T MapXmlToEntity<T>(string xml) where T : class
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));

            using (StringReader sr = new StringReader(xml))
            {
                return (T)xmlSerializer.Deserialize(sr);
            }
        }
    }
}
