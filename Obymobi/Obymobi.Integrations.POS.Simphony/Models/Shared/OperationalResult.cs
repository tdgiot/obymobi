﻿namespace Obymobi.Integrations.POS.Simphony.Models.Shared
{
    public class OperationalResult
    {
        public bool Success { get; set; }
        public  ErrorCode ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
    }

    public enum ErrorCode
    {
        AmountNotEntered,
        AppInitInProgress,
        CCAuthDeclined,
        CCAuthDeclinedWithMessage,
        CCServerDown,
        CheckEmployeeNumberMismatch,
        CheckNotFound,
        CheckListNotFound,
        CheckOpenedOnSystem,
        CheckTableNumberMismatch,
        ComboMealNotFound,
        ConnectionDown,
        DataOutOfRange,
        DetailDoesNotSupportTriggeredEvents,
        DiscountNotFound,
        DiscountAmountRequired,
        DiscountAmountTooLarge,
        DiscountAmountZero,
        DiscountItemNotAllowed,
        DiscountNotAllowedFilterActive,
        DiscountOnParentCombo,
        DuplicateLineNumber,
        EGatewayClientStartError,
        EGatewayClientStopError,
        EGatewayConnectionError,
        EGatewayConnectionNotInPool,
        EGatewayWaitConnectionTimeout,
        EmployeeClockIOStatusMismatch,
        EmployeeIDMismatch,
        EmployeeNotFound,
        EmployeeRVCMismatch,
        ErrorCreatingGuestcheck,
        ErrorInvalidWorkstation,
        ErrorReadingCheck,
        ErrorPickupCheck,
        FailedDataStoreInitialization,
        FailedDbSettingLoad,
        FailedErrorTranslationInitial,
        FailedPostCARequest,
        FailedInitialization,
        FailedLoggerInitialization,
        FailedSecurityAPIInitialization,
        FailedSubmitPrintJob,
        InternalCommunicationError,
        InternalProcessingError,
        InvalidArguments,
        InvaildAuthCode,
        InvalidCheckNumber,
        InvalidCreditCardExpirationDate,
        InvalidCreditCardHost,
        InvalidCreditCardNumber,
        InvalidClientName,
        InvalidClosedDays,
        InvalidConfigInfoRequestType,
        InvalidConfigInfoType,
        InvalidCustomerInfo,
        InvalidDetailLine,
        InvalidDetailLineType,
        InvalidEmployeeNumber,
        InvalidGuestCount,
        InvalidLineNumber,
        InvalidMenuItemPrice,
        InvalidOrderTypeNumber,
        InvalidPropertyNum,
        InvalidRvcNum,
        InvalidServingPeriod,
        InvalidTableNumber,
        InvalidTranslationSpecifier,
        ItemDiscountNeedsParentItem,
        LicensingFailed,
        MenuItemOutOfOrder,
        MissingDetailLinesElement,
        MissingTransactionElement,
        MissingTransactionHeaderElement,
        NoRequestHeader,
        NoSalesForDiscount,
        NotImplemented,
        NoSalesToApplyServiceCharge,
        NullInput,
        PaidPartially,
        PaymentAborted,
        PriceMenuItemWithZeroAmount,
        SecurityInitFailed,
        ServiceChargeTaxClassNotFound,
        Success,
        TenderTypeNotFound,
        TransactionEmployeeNotFound,
        TranslationFileNotAvailable,
        UnhandledException,
        UnknownCreditCardType,
        UnknownExceptionCode,
        TransactionLocked,
        WorkstationNotification
    }
}
