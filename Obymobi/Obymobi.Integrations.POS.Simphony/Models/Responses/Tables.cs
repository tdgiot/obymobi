﻿using System.Xml.Serialization;
using System.Collections.Generic;

namespace Obymobi.Integrations.POS.Simphony.Models.Responses
{
    public class Tables
    {
        [XmlRoot(ElementName = "Name")]
        public class Name
        {
            [XmlElement(ElementName = "StringNumberId")]
            public string StringNumberId { get; set; }

            [XmlElement(ElementName = "StringText")]
            public string StringText { get; set; }
        }

        [XmlRoot(ElementName = "DbDiningTable")]
        public class DbDiningTable
        {
            [XmlElement(ElementName = "DiningTableID")]
            public string DiningTableID { get; set; }

            [XmlElement(ElementName = "HierStrucID")]
            public string HierStrucID { get; set; }

            [XmlElement(ElementName = "ObjectNumber")]
            public string ObjectNumber { get; set; }

            [XmlElement(ElementName = "Name")] public Name Name { get; set; }

            [XmlElement(ElementName = "DiningTableClassNum")]
            public string DiningTableClassNum { get; set; }
        }

        [XmlRoot(ElementName = "ArrayOfDbDiningTable")]
        public class ArrayOfDbDiningTable
        {
            [XmlElement(ElementName = "DbDiningTable")]
            public List<DbDiningTable> DbDiningTable { get; set; }

            [XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
            public string Xsi { get; set; }

            [XmlAttribute(AttributeName = "xsd", Namespace = "http://www.w3.org/2000/xmlns/")]
            public string Xsd { get; set; }
        }
    }
}