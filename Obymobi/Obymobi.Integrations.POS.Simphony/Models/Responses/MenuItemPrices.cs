﻿using System.Xml.Serialization;
using System.Collections.Generic;

namespace Obymobi.Integrations.POS.Simphony.Models.Responses
{
	[XmlRoot(ElementName = "DbMenuItemPrice")]
	public class DbMenuItemPrice
	{
		[XmlElement(ElementName = "MenuItemPriceID")]
		public string MenuItemPriceID { get; set; }
		[XmlElement(ElementName = "HierStrucID")]
		public string HierStrucID { get; set; }
		[XmlElement(ElementName = "MenuItemDefID")]
		public string MenuItemDefID { get; set; }
		[XmlElement(ElementName = "SequenceNum")]
		public string SequenceNum { get; set; }
		[XmlElement(ElementName = "MenuLvlIndex")]
		public string MenuLvlIndex { get; set; }
		[XmlElement(ElementName = "OptionBits")]
		public string OptionBits { get; set; }
		[XmlElement(ElementName = "Price")]
		public string Price { get; set; }
		[XmlElement(ElementName = "PrepCost")]
		public string PrepCost { get; set; }
		[XmlElement(ElementName = "RecipeNameID")]
		public string RecipeNameID { get; set; }
		[XmlElement(ElementName = "PriceGroupID")]
		public string PriceGroupID { get; set; }
		[XmlElement(ElementName = "TaxClassObjNum")]
		public string TaxClassObjNum { get; set; }
		[XmlElement(ElementName = "ChangeSetObjNum")]
		public string ChangeSetObjNum { get; set; }
		[XmlElement(ElementName = "PosRef")]
		public string PosRef { get; set; }
		[XmlElement(ElementName = "ServiceChargeGroupObjNum")]
		public string ServiceChargeGroupObjNum { get; set; }
		[XmlElement(ElementName = "ParentTaxClassOvrdObjNmbr")]
		public string ParentTaxClassOvrdObjNmbr { get; set; }
	}

	[XmlRoot(ElementName = "ArrayOfDbMenuItemPrice")]
	public class ArrayOfDbMenuItemPrice
	{
		[XmlElement(ElementName = "DbMenuItemPrice")]
		public List<DbMenuItemPrice> DbMenuItemPrice { get; set; }
		[XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string Xsi { get; set; }
		[XmlAttribute(AttributeName = "xsd", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string Xsd { get; set; }
	}

}
