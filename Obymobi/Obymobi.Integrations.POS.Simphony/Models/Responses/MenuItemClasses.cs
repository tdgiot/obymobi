﻿using System.Xml.Serialization;
using System.Collections.Generic;

namespace Obymobi.Integrations.POS.Simphony.Models.Responses
{
	[XmlRoot(ElementName = "Name")]
	public class Name
	{
		[XmlElement(ElementName = "StringNumberId")]
		public string StringNumberId { get; set; }
		[XmlElement(ElementName = "StringText")]
		public string StringText { get; set; }
	}

	[XmlRoot(ElementName = "RefillDescriptor")]
	public class RefillDescriptor
	{
		[XmlElement(ElementName = "StringNumberId")]
		public string StringNumberId { get; set; }
		[XmlElement(ElementName = "StringText")]
		public string StringText { get; set; }
	}

	[XmlRoot(ElementName = "DbMenuItemClass")]
	public class DbMenuItemClass
	{
		[XmlElement(ElementName = "MenuItemClassID")]
		public string MenuItemClassID { get; set; }
		[XmlElement(ElementName = "HierStrucID")]
		public string HierStrucID { get; set; }
		[XmlElement(ElementName = "ObjectNumber")]
		public string ObjectNumber { get; set; }
		[XmlElement(ElementName = "Name")]
		public Name Name { get; set; }
		[XmlElement(ElementName = "OptionBits")]
		public string OptionBits { get; set; }
		[XmlElement(ElementName = "TransDfltMain")]
		public string TransDfltMain { get; set; }
		[XmlElement(ElementName = "TransDfltSub")]
		public string TransDfltSub { get; set; }
		[XmlElement(ElementName = "DatabasePrintOptionBits")]
		public string DatabasePrintOptionBits { get; set; }
		[XmlElement(ElementName = "PrintGroup")]
		public string PrintGroup { get; set; }
		[XmlElement(ElementName = "PrivilegeGroup")]
		public string PrivilegeGroup { get; set; }
		[XmlElement(ElementName = "DscntItmzrIndex")]
		public string DscntItmzrIndex { get; set; }
		[XmlElement(ElementName = "SvcChgItmzrIndex")]
		public string SvcChgItmzrIndex { get; set; }
		[XmlElement(ElementName = "Halo")]
		public string Halo { get; set; }
		[XmlElement(ElementName = "CondimentOrderingType")]
		public string CondimentOrderingType { get; set; }
		[XmlElement(ElementName = "TaxClassObjNum")]
		public string TaxClassObjNum { get; set; }
		[XmlElement(ElementName = "SlsItmzrIndex")]
		public string SlsItmzrIndex { get; set; }
		[XmlElement(ElementName = "MainMenuLvlIndex")]
		public string MainMenuLvlIndex { get; set; }
		[XmlElement(ElementName = "SubMenuLvlIndex")]
		public string SubMenuLvlIndex { get; set; }
		[XmlElement(ElementName = "DefaultMasterGroupObjNum")]
		public string DefaultMasterGroupObjNum { get; set; }
		[XmlElement(ElementName = "CountEntryType")]
		public string CountEntryType { get; set; }
		[XmlElement(ElementName = "CountDisplayType")]
		public string CountDisplayType { get; set; }
		[XmlElement(ElementName = "PricingCalculation")]
		public string PricingCalculation { get; set; }
		[XmlElement(ElementName = "MemberOfCondiments")]
		public string MemberOfCondiments { get; set; }
		[XmlElement(ElementName = "RequiredCondiments")]
		public string RequiredCondiments { get; set; }
		[XmlElement(ElementName = "AllowedCondiments")]
		public string AllowedCondiments { get; set; }
		[XmlElement(ElementName = "RoutingGroupObjNum")]
		public string RoutingGroupObjNum { get; set; }
		[XmlElement(ElementName = "KdsCourseNum")]
		public string KdsCourseNum { get; set; }
		[XmlElement(ElementName = "PrintClassObjNum")]
		public string PrintClassObjNum { get; set; }
		[XmlElement(ElementName = "NextPage")]
		public string NextPage { get; set; }
		[XmlElement(ElementName = "CondimentPrefixType")]
		public string CondimentPrefixType { get; set; }
		[XmlElement(ElementName = "DefaultOrderTypeIndex")]
		public string DefaultOrderTypeIndex { get; set; }
		[XmlElement(ElementName = "KdsHighlightSchemeObjNum")]
		public string KdsHighlightSchemeObjNum { get; set; }
		[XmlElement(ElementName = "MaxRefillCount")]
		public string MaxRefillCount { get; set; }
		[XmlElement(ElementName = "RefillDescriptor")]
		public RefillDescriptor RefillDescriptor { get; set; }
		[XmlElement(ElementName = "PopupCondOrderPageObjNum")]
		public string PopupCondOrderPageObjNum { get; set; }
		[XmlElement(ElementName = "PopupCondEditPageObjNum")]
		public string PopupCondEditPageObjNum { get; set; }
		[XmlElement(ElementName = "ServiceChargeGroupObjNum")]
		public string ServiceChargeGroupObjNum { get; set; }
		[XmlElement(ElementName = "PreProdChitPrintClassObjNum")]
		public string PreProdChitPrintClassObjNum { get; set; }
		[XmlElement(ElementName = "AllergenClassObjNum")]
		public string AllergenClassObjNum { get; set; }
		[XmlElement(ElementName = "CondimentHandling")]
		public string CondimentHandling { get; set; }
	}

	[XmlRoot(ElementName = "ArrayOfDbMenuItemClass")]
	public class ArrayOfDbMenuItemClass
	{
		[XmlElement(ElementName = "DbMenuItemClass")]
		public List<DbMenuItemClass> DbMenuItemClass { get; set; }
		[XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string Xsi { get; set; }
		[XmlAttribute(AttributeName = "xsd", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string Xsd { get; set; }
	}

}
