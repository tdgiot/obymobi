﻿using System.Xml.Serialization;

namespace Obymobi.Integrations.POS.Simphony.Models.Responses
{
    public class ServiceCharge
    {
		[XmlRoot(ElementName = "Name")]
		public class Name
		{
			[XmlElement(ElementName = "StringNumberId")]
			public string StringNumberId { get; set; }
			[XmlElement(ElementName = "StringText")]
			public string StringText { get; set; }
		}

		[XmlRoot(ElementName = "DbServiceCharge")]
		public class DbServiceCharge
		{
			[XmlElement(ElementName = "SvcChgID")]
			public string SvcChgID { get; set; }
			[XmlElement(ElementName = "HierStrucID")]
			public string HierStrucID { get; set; }
			[XmlElement(ElementName = "ObjectNumber")]
			public string ObjectNumber { get; set; }
			[XmlElement(ElementName = "Name")]
			public Name Name { get; set; }
			[XmlElement(ElementName = "OptionBits")]
			public string OptionBits { get; set; }
			[XmlElement(ElementName = "DatabasePrintOptionBits")]
			public string DatabasePrintOptionBits { get; set; }
			[XmlElement(ElementName = "Nlu")]
			public string Nlu { get; set; }
			[XmlElement(ElementName = "TransDfltMain")]
			public string TransDfltMain { get; set; }
			[XmlElement(ElementName = "MainMenuLvlIndex")]
			public string MainMenuLvlIndex { get; set; }
			[XmlElement(ElementName = "TransDfltSub")]
			public string TransDfltSub { get; set; }
			[XmlElement(ElementName = "SubMenuLvlIndex")]
			public string SubMenuLvlIndex { get; set; }
			[XmlElement(ElementName = "Percentage")]
			public string Percentage { get; set; }
			[XmlElement(ElementName = "Amount")]
			public string Amount { get; set; }
			[XmlElement(ElementName = "TipsPaidTendMedNumber")]
			public string TipsPaidTendMedNumber { get; set; }
			[XmlElement(ElementName = "PercentTipsPaid")]
			public string PercentTipsPaid { get; set; }
			[XmlElement(ElementName = "PrivilegeGrp")]
			public string PrivilegeGrp { get; set; }
			[XmlElement(ElementName = "IconNumber")]
			public string IconNumber { get; set; }
			[XmlElement(ElementName = "Halo")]
			public string Halo { get; set; }
			[XmlElement(ElementName = "TaxClassObjNum")]
			public string TaxClassObjNum { get; set; }
			[XmlElement(ElementName = "SluIndex")]
			public string SluIndex { get; set; }
			[XmlElement(ElementName = "HhtSluIndex")]
			public string HhtSluIndex { get; set; }
			[XmlElement(ElementName = "ChangeSetObjNum")]
			public string ChangeSetObjNum { get; set; }
			[XmlElement(ElementName = "EffectivityID")]
			public string EffectivityID { get; set; }
			[XmlElement(ElementName = "RptGrpObjNum")]
			public string RptGrpObjNum { get; set; }
			[XmlElement(ElementName = "PrintClassObjNum")]
			public string PrintClassObjNum { get; set; }
			[XmlElement(ElementName = "MainLevels")]
			public string MainLevels { get; set; }
			[XmlElement(ElementName = "SubLevels")]
			public string SubLevels { get; set; }
			[XmlElement(ElementName = "SvcItemizers")]
			public string SvcItemizers { get; set; }
		}

		[XmlRoot(ElementName = "ArrayOfDbServiceCharge")]
		public class ArrayOfDbServiceCharge
		{
			[XmlElement(ElementName = "DbServiceCharge")]
			public DbServiceCharge DbServiceCharge { get; set; }
		}
	}
}
