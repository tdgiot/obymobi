﻿using System.Xml.Serialization;
using System.Collections.Generic;

namespace Obymobi.Integrations.POS.Simphony.Models.Responses
{
	[XmlRoot(ElementName = "Name1")]
	public class Name1
	{
		[XmlElement(ElementName = "StringNumberId")]
		public string StringNumberId { get; set; }
		[XmlElement(ElementName = "StringText")]
		public string StringText { get; set; }
	}

	[XmlRoot(ElementName = "Name2")]
	public class Name2
	{
		[XmlElement(ElementName = "StringNumberId")]
		public string StringNumberId { get; set; }
		[XmlElement(ElementName = "StringText")]
		public string StringText { get; set; }
	}

	[XmlRoot(ElementName = "Name3")]
	public class Name3
	{
		[XmlElement(ElementName = "StringNumberId")]
		public string StringNumberId { get; set; }
		[XmlElement(ElementName = "StringText")]
		public string StringText { get; set; }
	}

	[XmlRoot(ElementName = "LongDescriptor")]
	public class LongDescriptor
	{
		[XmlElement(ElementName = "StringNumberId")]
		public string StringNumberId { get; set; }
		[XmlElement(ElementName = "StringText")]
		public string StringText { get; set; }
	}

	[XmlRoot(ElementName = "DbMenuItemDefinition")]
	public class DbMenuItemDefinition
	{
		[XmlElement(ElementName = "NameOptions")]
		public string NameOptions { get; set; }
		[XmlElement(ElementName = "MenuItemDefID")]
		public string MenuItemDefID { get; set; }
		[XmlElement(ElementName = "HierStrucID")]
		public string HierStrucID { get; set; }
		[XmlElement(ElementName = "MenuItemMasterID")]
		public string MenuItemMasterID { get; set; }
		[XmlElement(ElementName = "SequenceNum")]
		public string SequenceNum { get; set; }
		[XmlElement(ElementName = "Name1")]
		public Name1 Name1 { get; set; }
		[XmlElement(ElementName = "Name2")]
		public Name2 Name2 { get; set; }
		[XmlElement(ElementName = "SluSort")]
		public string SluSort { get; set; }
		[XmlElement(ElementName = "NluNumber")]
		public string NluNumber { get; set; }
		[XmlElement(ElementName = "Tare")]
		public string Tare { get; set; }
		[XmlElement(ElementName = "Surcharge")]
		public string Surcharge { get; set; }
		[XmlElement(ElementName = "IconNumber")]
		public string IconNumber { get; set; }
		[XmlElement(ElementName = "OptionBits")]
		public string OptionBits { get; set; }
		[XmlElement(ElementName = "SpecialCount")]
		public string SpecialCount { get; set; }
		[XmlElement(ElementName = "PrepTime")]
		public string PrepTime { get; set; }
		[XmlElement(ElementName = "Name3")]
		public Name3 Name3 { get; set; }
		[XmlElement(ElementName = "LongDescriptor")]
		public LongDescriptor LongDescriptor { get; set; }
		[XmlElement(ElementName = "MenuItemClassObjNum")]
		public string MenuItemClassObjNum { get; set; }
		[XmlElement(ElementName = "NluGroupIndex")]
		public string NluGroupIndex { get; set; }
		[XmlElement(ElementName = "SluIndex")]
		public string SluIndex { get; set; }
		[XmlElement(ElementName = "HhtSluIndex")]
		public string HhtSluIndex { get; set; }
		[XmlElement(ElementName = "MainLevels")]
		public string MainLevels { get; set; }
		[XmlElement(ElementName = "SubLevels")]
		public string SubLevels { get; set; }
		[XmlElement(ElementName = "PosRef")]
		public string PosRef { get; set; }
		[XmlElement(ElementName = "PrintClassObjNum")]
		public string PrintClassObjNum { get; set; }
		[XmlElement(ElementName = "PrefixLevelOverride")]
		public string PrefixLevelOverride { get; set; }
		[XmlElement(ElementName = "GuestCount")]
		public string GuestCount { get; set; }
		[XmlElement(ElementName = "Slu2")]
		public string Slu2 { get; set; }
		[XmlElement(ElementName = "Slu3")]
		public string Slu3 { get; set; }
		[XmlElement(ElementName = "Slu4")]
		public string Slu4 { get; set; }
		[XmlElement(ElementName = "Slu5")]
		public string Slu5 { get; set; }
		[XmlElement(ElementName = "Slu6")]
		public string Slu6 { get; set; }
		[XmlElement(ElementName = "Slu7")]
		public string Slu7 { get; set; }
		[XmlElement(ElementName = "Slu8")]
		public string Slu8 { get; set; }
		[XmlElement(ElementName = "Quantity")]
		public string Quantity { get; set; }
		[XmlElement(ElementName = "AllergenClassObjNum")]
		public string AllergenClassObjNum { get; set; }
		[XmlElement(ElementName = "CapacityUsage")]
		public string CapacityUsage { get; set; }
		[XmlElement(ElementName = "NextScreen")]
		public string NextScreen { get; set; }
		[XmlElement(ElementName = "MiMasterObjNum")]
		public string MiMasterObjNum { get; set; }
		[XmlElement(ElementName = "CheckAvailability")]
		public string CheckAvailability { get; set; }
		[XmlElement(ElementName = "OutOfMenuItem")]
		public string OutOfMenuItem { get; set; }
		[XmlElement(ElementName = "DefaultCondiments")]
		public DefaultCondiments DefaultCondiments { get; set; }
		[XmlElement(ElementName = "MenuLevelEntries")]
		public MenuLevelEntries MenuLevelEntries { get; set; }
	}

	[XmlRoot(ElementName = "DbMenuItemDefaultCondiment")]
	public class DbMenuItemDefaultCondiment
	{
		[XmlElement(ElementName = "MenuItemDefaultCondID")]
		public string MenuItemDefaultCondID { get; set; }
		[XmlElement(ElementName = "DefLevelOverride")]
		public string DefLevelOverride { get; set; }
		[XmlElement(ElementName = "SortGroup")]
		public string SortGroup { get; set; }
		[XmlElement(ElementName = "PriceLevelOverride")]
		public string PriceLevelOverride { get; set; }
		[XmlElement(ElementName = "HierStrucID")]
		public string HierStrucID { get; set; }
		[XmlElement(ElementName = "MenuItemDefID")]
		public string MenuItemDefID { get; set; }
		[XmlElement(ElementName = "OptionBits")]
		public string OptionBits { get; set; }
		[XmlElement(ElementName = "CondimentSetObjNum")]
		public string CondimentSetObjNum { get; set; }
	}

	[XmlRoot(ElementName = "DefaultCondiments")]
	public class DefaultCondiments
	{
		[XmlElement(ElementName = "DbMenuItemDefaultCondiment")]
		public DbMenuItemDefaultCondiment DbMenuItemDefaultCondiment { get; set; }
	}

	[XmlRoot(ElementName = "DbMenuItemMenuLevelEntry")]
	public class DbMenuItemMenuLevelEntry
	{
		[XmlElement(ElementName = "MenuItemMenuLevelEntryID")]
		public string MenuItemMenuLevelEntryID { get; set; }
		[XmlElement(ElementName = "HierStrucID")]
		public string HierStrucID { get; set; }
		[XmlElement(ElementName = "MenuItemDefID")]
		public string MenuItemDefID { get; set; }
		[XmlElement(ElementName = "MenuLvlEntryIndex")]
		public string MenuLvlEntryIndex { get; set; }
		[XmlElement(ElementName = "MenuLevelObjNum")]
		public string MenuLevelObjNum { get; set; }
	}

	[XmlRoot(ElementName = "MenuLevelEntries")]
	public class MenuLevelEntries
	{
		[XmlElement(ElementName = "DbMenuItemMenuLevelEntry")]
		public List<DbMenuItemMenuLevelEntry> DbMenuItemMenuLevelEntry { get; set; }
	}

	[XmlRoot(ElementName = "ArrayOfDbMenuItemDefinition")]
	public class ArrayOfDbMenuItemDefinition
	{
		[XmlElement(ElementName = "DbMenuItemDefinition")]
		public List<DbMenuItemDefinition> DbMenuItemDefinition { get; set; }
		[XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string Xsi { get; set; }
		[XmlAttribute(AttributeName = "xsd", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string Xsd { get; set; }
	}

}
