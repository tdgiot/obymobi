﻿using System.Xml.Serialization;
using System.Collections.Generic;

namespace Obymobi.Integrations.POS.Simphony.Models.Responses
{
    public class FamilyGroups
    {
        [XmlRoot(ElementName = "Name")]
        public class Name
        {
            [XmlElement(ElementName = "StringNumberId")]
            public string StringNumberId { get; set; }

            [XmlElement(ElementName = "StringText")]
            public string StringText { get; set; }
        }

        [XmlRoot(ElementName = "DbFamilyGroup")]
        public class DbFamilyGroup
        {
            [XmlElement(ElementName = "FamGrpID")] public string FamGrpID { get; set; }

            [XmlElement(ElementName = "HierStrucID")]
            public string HierStrucID { get; set; }

            [XmlElement(ElementName = "ObjectNumber")]
            public string ObjectNumber { get; set; }

            [XmlElement(ElementName = "Name")] public Name Name { get; set; }
            [XmlElement(ElementName = "MajGrpID")] public string MajGrpID { get; set; }

            [XmlElement(ElementName = "ReportGroup")]
            public string ReportGroup { get; set; }
        }

        [XmlRoot(ElementName = "ArrayOfDbFamilyGroup")]
        public class ArrayOfDbFamilyGroup
        {
            [XmlElement(ElementName = "DbFamilyGroup")]
            public List<DbFamilyGroup> DbFamilyGroup { get; set; }

            [XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
            public string Xsi { get; set; }

            [XmlAttribute(AttributeName = "xsd", Namespace = "http://www.w3.org/2000/xmlns/")]
            public string Xsd { get; set; }
        }
    }

}