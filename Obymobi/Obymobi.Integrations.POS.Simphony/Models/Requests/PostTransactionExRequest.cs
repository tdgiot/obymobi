﻿using Obymobi.Integrations.POS.Simphony.simphonyposapiweb;

namespace Obymobi.Integrations.POS.Simphony.Models.Requests
{
    public class PostTransactionExRequest : PostTransactionRequestBase
    {
        public string VendorCode { get; set; }
        public SimphonyPosApi_MenuItem[] ppMenuItems { get; set; }
        public SimphonyPosApi_ComboMeal[] ppComboMeals { get; set; }
        public SimphonyPosApi_SvcCharge pServiceChg { get; set; }
        public SimphonyPosApi_Discount pSubTotalDiscount { get; set; }
        public SimphonyPosApi_TmedDetailItemEx pTmedDetail { get; set; }
        public SimphonyPosApi_TotalsResponse pTotalsResponse { get; set; }
    }
}
