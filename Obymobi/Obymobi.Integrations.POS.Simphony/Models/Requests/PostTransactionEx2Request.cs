﻿using Obymobi.Integrations.POS.Simphony.simphonyposapiweb;

namespace Obymobi.Integrations.POS.Simphony.Models.Requests
{
    public class PostTransactionEx2Request : PostTransactionRequestBase
    {
        public SimphonyPosApi_MenuItemEx[] ppMenuItemsEx { get; set; }
        public SimphonyPosApi_ComboMealEx[] ppComboMealsEx { get; set; }
        public SimphonyPosApi_SvcChargeEx pSvcChargeEx { get; set; }
        public SimphonyPosApi_DiscountEx[] pSubTotalDiscountEx { get; set; }
        public SimphonyPosApi_TmedDetailItemEx2[] pTmedDetailEx2 { get; set; }
        public SimphonyPosApi_TotalsResponseEx pTotalsResponseEx { get; set; }
    }
}
