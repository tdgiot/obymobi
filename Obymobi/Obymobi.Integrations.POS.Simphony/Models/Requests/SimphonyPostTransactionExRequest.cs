﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;
using Obymobi.Integrations.POS.Simphony.Models.Shared;

namespace Obymobi.Integrations.POS.Simphony.Models.Requests
{
    [XmlRoot("PostTransactionEx")]
    public class SimphonyPostTransactionExRequest
    {
        [XmlElement("vendorCode")]
        public string VendorCode { get; set; }
        [XmlElement("revenueCenter")]
        public string RevenueCenter { get; set; }
        [XmlElement("orderType")]
        public int OrderType { get; set; }
        [XmlElement("employeeNumber")]
        public string EmployeeNumber { get; set; }

        [XmlElement("pGuestCheck")]
        public GuestCheck GuestCheck { get; set; }
        [XmlArray("ppMenuItems")]
        [XmlArrayItem("SimphonyPosApi_MenuItem")]
        public MenuItemWithCondiments[] MenuItems { get; set; }
        [XmlArray("ppComboMeal")]
        [XmlArrayItem("SimphonyPosApi_ComboMeal")]
        public ComboMeal[] ComboMeals { get; set; }
        [XmlElement("pServiceChg")]
        public ServiceCharge ServiceCharge { get; set; }
        public SubTotalDiscount SubTotalDiscount { get; set; }
        [XmlElement("pTmedDetail")]
        public TenderMediaDetails TenderMediaDetails { get; set; }
        [XmlElement("pTotalsResponse")]
        public Response Response { get; set; }
    }

    public class Response
    {
        public OperationalResult OperationalResult { get; set; }
        [XmlElement("TotalsAutoSvcChgTotals")]
        public string AutomaticServiceChargeTotals { get; set; }
        [XmlElement("TotalsOtherTotals")]
        public string OtherTotals { get; set; }
        [XmlElement("TotalsSubTotal")]
        public string SubTotal { get; set; }
        [XmlElement("TotalsTaxTotals")]
        public string TaxTotals { get; set; }
        [XmlElement("TotalsAmountDue")]
        public string AmountDue { get; set; }
    }

    public class ComboMeal
    {
        public MenuItemWithCondiments ComboMealMainItem { get; set; }
        public MenuItemWithCondiments ComboMealMenuItem { get; set; }
        [XmlArray("SideItems")]
        [XmlArrayItem("SimphonyPosApi_MenuItem")]
        public MenuItemWithCondiments[] SideItems { get; set; }
    }

    public class ServiceCharge
    {
        [XmlElement("SvcChgAmountOrPercent")]
        public string ServiceChargeAmountOrPercent { get; set; }
        [XmlElement("SvcChgObjectNum")]
        public int ServiceChargeObjectNumber { get; set; }
        [XmlElement("SvcChgReference")]
        public string ServiceChargeReference { get; set; }
    }

    public class SubTotalDiscount
    {
        [XmlElement("DiscAmountOrPercentage")]
        public string DiscountAmountOrPercentage { get; set; }
        [XmlElement("DiscObjectNum")]
        public string DiscountObjectNumber { get; set; }
        [XmlElement("DiscReference")]
        public string DiscountReference { get; set; }
    }

    public class TenderMediaDetails
    {
        [XmlElement("TmedEPayment")]
        public TenderMediaPayment TenderMediaPayment { get; set; }
        [XmlElement("TmedObjectNum")]
        public int TenderMediaObjectNumber { get; set; }
        [XmlElement("TmedPartialPayment")]
        public string TenderMediaPartialPayment { get; set; }
        [XmlElement("TmedReference")]
        public string TenderMediaReference { get; set; }
    }

    public class TenderMediaPayment
    {
        public string AccountDataSource { get; set; }
        public string AccountType { get; set; }
        [XmlElement("AccNumber")]
        public string AccountNumber { get; set; }
        public string AddressVerification { get; set; }
        public string AuthorizationCode { get; set; }
        public string BaseAmount { get; set; }
        public string CashBackAmount { get; set; }
        [XmlElement("CVVNumber")]
        public string CvvNumber { get; set; }
        public string DeviceId { get; set; }
        public DateTime ExpirationDate { get; set; }
        public string InterfaceName { get; set; }
        public string IssueNumber { get; set; }
        [XmlElement("KeySerialNum")]
        public string KeySerialNumber { get; set; }
        public string PaymentCommand { get; set; }
        public string PinBlock { get; set; }
        public DateTime StartDate { get; set; }
        public string SvcAccountType { get; set; }
        public string SvcResponse { get; set; }
        public string TipAmount { get; set; }
        public string Track1Data { get; set; }
        public string Track2Data { get; set; }
        public string Track3Data { get; set; }
        public string PaymentCommandString { get; set; }
    }

    public enum AccountDataSource
    {
        [Description("SOURCE_UNDEFINED")]
        SourceUndefined,
        [Description("MAGNETIC_STRIPE")]
        MagneticStripe,
        [Description("RFID_TRACK_DATA_RULES")]
        RfidTrackDataRules,
        [Description("RFID_M_CHIP_RULES")]
        RfidMChipRules,
        [Description("MANUALLY_KEYED_TRACK_1_CAPABLE")]
        ManuallyKeyedTrack1Capable,
        [Description("MANUALLY_KEYED_TRACK_2_CAPABLE")]
        ManuallyKeyedTrack2Capable,
        [Description("MANUALLY_KEYED_NO_CARD_READER")]
        ManuallyKeyedNoCardReader
    }

    public enum AccountType
    {
        [Description("ACCOUNT_TYPE_UNDEFINED")]
        AccountTypeUndefined,
        [Description("CHECKING")]
        Checking,
        [Description("SAVINGS")]
        Savings
    }

    public enum PaymentCommand
    {
        [Description("NO_E_PAYMENT")]
        NoEPayment,
        [Description("CREDIT_AUTHORIZE_ONLY")]
        CreditAuthorizeOnly,
        [Description("CREDIT_AUTHORIZE_AND_PAY")]
        CreditAuthorizeAndPay,
        [Description("DEBIT_AUTHORIZE_ONLY")]
        DebitAuthorizeOnly,
        [Description("DEBIT_AUTHORIZE_AND_PAY")]
        DebitAuthorizeAndPay,
        [Description("STORED_VALUE_CARD_AUTHORIZE")]
        StoredValueCardAuthorize,
        [Description("STORED_VALUE_CARD_REDEEM")]
        StoredValueCardRedeem,
        [Description("GET_DEBIT_CANCEL_VOUCHER")]
        GetDebitCancelVoucher
    }

    public class GuestCheck
    {
        public DateTime CheckDateToFire { get; set; }
        [XmlElement("CheckEmployeeObjectNum")]
        public string CheckEmployeeObjectNumber { get; set; }
        public string CheckGuestCount { get; set; }
        public string CheckId { get; set; }
        [XmlElement("CheckNum")]
        public string CheckNumber { get; set; }
        public string CheckOrderType { get; set; }
        public string CheckRevenueCenterId { get; set; }
        public string CheckSeq { get; set; }
        public string CheckStatusBits { get; set; }
        [XmlElement("CheckTableObjectNum")]
        public string CheckTableObjectNumber { get; set; }
        [XmlArray("pCheckInfoLines")]
        public string[] CheckInfoLines { get; set; }
        [XmlElement("EventObjectNum")]
        public string EventObjectNum { get; set; }
    }

    public class MenuItemWithCondiments
    {
        public Condiments Condiments { get; set; }
        public MenuItem MenuItem { get; set; }
    }

    public class Condiments
    {
        [XmlArray("Condiments")]
        [XmlArrayItem("SimphonyPosApi_MenuItemDefinition")]
        public MenuItem[] ListOfCondiments { get; set; }
    }

    public class MenuItem
    {
        public ItemDiscount ItemDiscount { get; set; }
        [XmlElement("MiObjectNum")]
        public string MiObjectNumber { get; set; }
        public string MiOverridePrice { get; set; }
        public string MiReference { get; set; }
        public string MiWeight { get; set; }
        public string MiMenuLevel { get; set; }
        public string MiSubLevel { get; set; }
        public string MiPriveLevel { get; set; }
    }

    public class ItemDiscount
    {
        [XmlElement("DiscAmountOrPercent")]
        public string DiscountAmountOrPercentage { get; set; }
        [XmlElement("DiscObjectNum")]
        public string DiscountObjectNumber { get; set; }
        [XmlElement("DiscountReference")]
        public string DiscReference { get; set; }
    }
}
