﻿using System.Xml.Serialization;

namespace Obymobi.Integrations.POS.Simphony.Models.Requests
{
    [XmlRoot("GetConfigurationInfo")]
    public class SimphonyGetConfigurationInfoRequest
    {
        [XmlElement("employeeObjectNum")]
        public string EmployeeObjectNumber { get; set; }
        [XmlElement("configurationItemType")]
        public int[] ConfigurationItemType { get; set; }
        [XmlElement("revenueCenter")]
        public string RevenueCenter { get; set; }
    }
}
