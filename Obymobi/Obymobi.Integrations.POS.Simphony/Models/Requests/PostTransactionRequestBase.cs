﻿using Obymobi.Integrations.POS.Simphony.simphonyposapiweb;

namespace Obymobi.Integrations.POS.Simphony.Models.Requests
{
    public class PostTransactionRequestBase
    {
        public SimphonyPosApi_GuestCheck pGuestCheck { get; set; }
        public string[] ppCheckPrintLines { get; set; }
        public string[] ppVoucherOutput { get; set; }
    }
}
