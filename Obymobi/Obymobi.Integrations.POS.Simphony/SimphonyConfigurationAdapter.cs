﻿using System.Collections.Generic;
using Dionysos;
using Obymobi.Data.EntityClasses;
using Obymobi.Integrations.Interfaces;
using Obymobi.Logic.Model;

namespace Obymobi.Integrations.POS.Simphony
{
    public class SimphonyConfigurationAdapter : IConfigurationAdapter
    {
        /// <summary>
        /// Read configuration from a Terminal Model
        /// </summary>
        /// <param name="terminal"></param>
        public void ReadFromTerminalModel(Terminal terminal)
        {
            this.WebserviceUrl = terminal.PosValue1;

            if (int.TryParse(terminal.PosValue2, out int employeeNumber))
            {
                this.EmployeeObjectNum = employeeNumber;
            }

            this.RevenueCenters = ParseRevenueCenters(terminal.PosValue3);

            if (bool.TryParse(terminal.PosValue4, out bool useOldPostTransactionMethod))
            {
                this.UseOldPostTransactionMethod = useOldPostTransactionMethod;
            }

            if (bool.TryParse(terminal.PosValue5, out bool syncInStockProductsOnly))
            {
                this.SyncInStockProductsOnly = syncInStockProductsOnly;
            }

            this.VendorCode = terminal.PosValue6;

            if (int.TryParse(terminal.PosValue7, out int tenderMediaObjectNum))
            {
                this.TenderMediaObjectNum = tenderMediaObjectNum;
            }

            if (int.TryParse(terminal.PosValue8, out int orderType))
            {
                this.OrderType = orderType;
            }

        }

        /// <summary>
        /// Read configuration from a Terminal Entity
        /// </summary>
        /// <param name="terminal"></param>
        public void ReadFromTerminalEntity(TerminalEntity terminal)
        {
            this.WebserviceUrl = terminal.PosValue1;

            if (int.TryParse(terminal.PosValue2, out int employeeNumber))
            {
                this.EmployeeObjectNum = employeeNumber;
            }

            this.RevenueCenters = ParseRevenueCenters(terminal.PosValue3);

            if (bool.TryParse(terminal.PosValue4, out bool useOldPostTransactionMethod))
            {
                this.UseOldPostTransactionMethod = useOldPostTransactionMethod;
            }

            if (bool.TryParse(terminal.PosValue5, out bool syncInStockProductsOnly))
            {
                this.SyncInStockProductsOnly = syncInStockProductsOnly;
            }

            this.VendorCode = terminal.PosValue6;

            if (int.TryParse(terminal.PosValue7, out int tenderMediaObjectNum))
            {
                this.TenderMediaObjectNum = tenderMediaObjectNum;
            }

            if (int.TryParse(terminal.PosValue8, out int orderType))
            {
                this.OrderType = orderType;
            }
        }

        /// <summary>
        /// Read configuration from Configuration Manager
        /// </summary>
        public void ReadFromConfigurationManager()
        {
            this.WebserviceUrl = ConfigurationManager.GetString(POSConfigurationConstants.SimphonyWebserviceUrl);
            this.EmployeeObjectNum = ConfigurationManager.GetInt(POSConfigurationConstants.SimphonyEmployeeObjectNum);
            this.RevenueCenters = ParseRevenueCenters(ConfigurationManager.GetString(POSConfigurationConstants.SimphonyRevenueCenters));
            this.UseOldPostTransactionMethod = ConfigurationManager.GetBool(POSConfigurationConstants.SimphonyUseOldPostTransactionMethod);
            this.SyncInStockProductsOnly = ConfigurationManager.GetBool(POSConfigurationConstants.SimphonySyncInStockProductsOnly);
            this.VendorCode = ConfigurationManager.GetString(POSConfigurationConstants.SimphonyVendorCode);
            this.TenderMediaObjectNum = ConfigurationManager.GetInt(POSConfigurationConstants.SimphonyTenderMediaObjectNum);
            this.OrderType = ConfigurationManager.GetInt(POSConfigurationConstants.SimphonyOrderType);
        }

        /// <summary>
        /// Write configuration to Configuration Manager
        /// </summary>
        public void WriteToConfigurationManager()
        {
            ConfigurationManager.SetValue(POSConfigurationConstants.SimphonyWebserviceUrl, this.WebserviceUrl);
            ConfigurationManager.SetValue(POSConfigurationConstants.SimphonyEmployeeObjectNum, this.EmployeeObjectNum);
            ConfigurationManager.SetValue(POSConfigurationConstants.SimphonyRevenueCenters, this.RevenueCenters);
            ConfigurationManager.SetValue(POSConfigurationConstants.SimphonyUseOldPostTransactionMethod, this.UseOldPostTransactionMethod);
            ConfigurationManager.SetValue(POSConfigurationConstants.SimphonySyncInStockProductsOnly, this.SyncInStockProductsOnly);
            ConfigurationManager.SetValue(POSConfigurationConstants.SimphonyVendorCode, this.VendorCode);
            ConfigurationManager.SetValue(POSConfigurationConstants.SimphonyTenderMediaObjectNum, this.TenderMediaObjectNum);
            ConfigurationManager.SetValue(POSConfigurationConstants.SimphonyOrderType, this.OrderType);
        }

        /// <summary>
        /// Write configuration to Terminal Entity
        /// </summary>
        /// <param name="terminal"></param>
        public void WriteToTerminalEntity(TerminalEntity terminal)
        {
            terminal.PosValue1 = this.WebserviceUrl;
            terminal.PosValue2 = this.EmployeeObjectNum.ToString();
            terminal.PosValue3 = MergeRevenueCenters();
            terminal.PosValue4 = this.UseOldPostTransactionMethod.ToString();
            terminal.PosValue5 = this.SyncInStockProductsOnly.ToString();
            terminal.PosValue6 = this.VendorCode;
            terminal.PosValue7 = this.TenderMediaObjectNum.ToString();
            terminal.PosValue8 = this.OrderType.ToString();
        }

        /// <summary>
        /// Write configuration to Terminal Model
        /// </summary>
        /// <param name="terminal"></param>
        public void WriteToTerminalModel(Terminal terminal)
        {
            terminal.PosValue1 = this.WebserviceUrl;
            terminal.PosValue2 = this.EmployeeObjectNum.ToString();
            terminal.PosValue3 = MergeRevenueCenters();
            terminal.PosValue4 = this.UseOldPostTransactionMethod.ToString();
            terminal.PosValue5 = this.SyncInStockProductsOnly.ToString();
            terminal.PosValue6 = this.VendorCode;
            terminal.PosValue7 = this.TenderMediaObjectNum.ToString();
            terminal.PosValue8 = this.OrderType.ToString();
        }

        private List<int> ParseRevenueCenters(string input)
        {
            List<int> output = new List<int>();
            string[] split = input.ReplaceLineBreaks(",").Trim().Split(',');
            foreach (string revenueCenter in split)
            {
                if (int.TryParse(revenueCenter.Trim(), out int id))
                {
                    output.Add(id);
                }
            }

            return output;
        }

        private string MergeRevenueCenters() => string.Join(",", this.RevenueCenters);

        /// <summary>
        /// Gets or sets webservice url
        /// </summary>
        /// <value>
        /// The webservice url
        /// </value>
        public string WebserviceUrl { get; set; }

        /// <summary>
        /// Gets or sets the employee object num
        /// </summary>
        public int EmployeeObjectNum { get; set; }

        /// <summary>
        /// Gets or sets the comma separated list of revenue centers
        /// </summary>
        public List<int> RevenueCenters { get; set; } = new List<int>();

        /// <summary>
        /// Gets or sets the flag used to switch between old and new PostTransaction methods
        /// </summary>
        public bool UseOldPostTransactionMethod { get; set; } = true;

        /// <summary>
        /// Gets or sets the flag to determine if we want to only sync products which are in stock
        /// </summary>
        public bool SyncInStockProductsOnly { get; set; } = true;

        /// <summary>
        /// Gets or sets the vendor code
        /// </summary>
        public string VendorCode { get; set; }

        /// <summary>
        /// Gets or sets the tender media object number
        /// </summary>
        public int TenderMediaObjectNum { get; set; } = 101;

        /// <summary>
        /// Gets or sets the order type (eg, dine-in, delivery)
        /// </summary>
        public int OrderType { get; set; } = 1;
    }
}
