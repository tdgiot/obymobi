﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;
using Obymobi.Security;

namespace CraveSimulator
{
    public class Client
    {
        #region  Fields

        //private const string WebserviceUrl = "http://192.168.57.30/trunk/api/21.0/CraveService.asmx/";
        private const string WebserviceUrl = "https://dev.crave-emenu.com/api/20.0/CraveService.asmx/";

        private readonly string clientStatusXml;

        #endregion

        #region Constructors

        public Client()
        {
            ClientStatus clientStatus = new ClientStatus();
            this.clientStatusXml = XmlHelper.Serialize(clientStatus);
        }

        #endregion

        #region Properties

        public int CompanyId { get; set; }

        public int ClientId { get; set; }

        public string MacAddress { get; set; }

        public string Salt { get; set; }

        #endregion

        public string CallIsDatabaseAlive()
        {
            return DoWebserviceCall(Client.WebserviceUrl + "IsDatabaseAlive");
        }

        public string CallGetClient(string macAddress, int deliverypointgroupId)
        {
            NameValueCollection args = new NameValueCollection();
            args.Add("clientId", "0");
            args.Add("clientmacAddress", macAddress);
            args.Add("deliverypointgroupId", deliverypointgroupId.ToString());
            args.Add("deliverypointId", "0");
            args.Add("applicationVersion", "");
            args.Add("osVersion", "");
            args.Add("deviceType", "3");
            args.Add("deviceModel", "51");

            return DoWebserviceCall(Client.WebserviceUrl + "GetClient" + CreateParameterString(args));
        }

        public string CallGetSetClientStatus()
        {
            NameValueCollection args = new NameValueCollection();
            args.Add("xml", this.clientStatusXml);

            return DoWebserviceCall(Client.WebserviceUrl + "GetSetClientStatus" + CreateParameterString(args));
        }

        private string DoWebserviceCall(string url)
        {
            string result = string.Empty;

            using (WebClient webClient = new WebClient())
            {
                webClient.Proxy = null;
                result = webClient.DownloadString(url);
            }

            return result;
        }

        private string CreateParameterString(NameValueCollection args)
        {
            // Default parameters:
            // timestamp
            // macAddress
            // hash

            DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            long timestamp = Convert.ToInt64((DateTime.Now.ToUniversalTime() - epoch).TotalSeconds);

            string parameters = string.Format("?timestamp={0}", timestamp);
            parameters += string.Format("&macAddress={0}", this.MacAddress);

            List<object> objs = new List<object>();
            foreach (string key in args.Keys)
            {
                parameters += string.Format("&{0}={1}", key, args[key]);
                objs.Add(args[key]);
            }

            string hash = Hasher.GetHashFromParameters(this.Salt, timestamp, this.MacAddress, objs.ToArray());
            parameters += string.Format("&hash={0}", hash);

            return parameters;
        }

        public string GetRandomMacAddress()
        {
            Random random = new Random();
            byte[] buffer = new byte[6];
            random.NextBytes(buffer);
            string result = string.Concat(buffer.Select(x => string.Format("{0}:", x.ToString("X2"))).ToArray());
            return result.TrimEnd(':');
        }
    }
}