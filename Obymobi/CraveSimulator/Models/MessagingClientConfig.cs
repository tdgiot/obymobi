﻿namespace CraveSimulator.Models
{
    public class MessagingClientConfig
    {
        public int Id { get; set; }
        public string Identifier { get; set; }
        public string Salt { get; set; }

        public int ClientId { get; set; }
        public int CompanyId { get; set; }
        public int DeliverypointgroupId { get; set; }
        public int DeliverypointId { get; set; }
    }
}