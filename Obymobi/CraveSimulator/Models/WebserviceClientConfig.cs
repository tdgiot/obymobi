﻿namespace CraveSimulator.Models
{
    public class WebserviceClientConfig
    {
        public int CompanyId { get; set; }
        public int ClientId { get; set; }
        public string MacAddress { get; set; }
        public string Salt { get; set; }

        public bool RunAsync { get; set; }
    }
}