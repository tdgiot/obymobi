﻿using System.Windows.Forms;

namespace CraveSimulator.Models
{
    public class StatusMessage
    {
        public string Message { get; set; } 
        public string Title { get; set; }
        public MessageBoxIcon Icon { get; set; }

        public StatusMessage()
        {
            this.Icon = MessageBoxIcon.None;
        }
    }
}