﻿using System;
using Obymobi.Constants;
using Obymobi.Enums;

namespace CraveSimulator.Models
{
    public class WebserviceConfig
    {
        public WebserviceConfig()
        {
            this.Environment = CloudEnvironment.Manual;
            this.ApiVersion = ObymobiConstants.API_VERSION;
            this.ManualBaseUrl = "http://localhost/trunk/";
        }

        public CloudEnvironment Environment { get; set; }
        public string ApiVersion { get; set; }
        public string ManualBaseUrl { get; set; }

        public WebserviceConfigChangedEventArgs GetEventArg()
        {
            return new WebserviceConfigChangedEventArgs
                   {
                       Environment = this.Environment,
                       BaseUrl = GetBaseUrl(),
                       ManualBaseUrl = this.ManualBaseUrl,
                       ApiVersion = this.ApiVersion
                   };
        }

        public string GetBaseUrl()
        {
            switch (this.Environment)
            {
                case CloudEnvironment.ProductionPrimary:
                    return ObymobiConstants.PrimaryWebserviceBaseUrl;
                case CloudEnvironment.Test:
                    return ObymobiConstants.TestWebserviceBaseUrl;
                case CloudEnvironment.Development:
                    return ObymobiConstants.DevelopmentWebserviceBaseUrl;
            }

            string url = this.ManualBaseUrl;
            if (!this.ManualBaseUrl.EndsWith("/"))
                url += "/";

            return url;
        }
    }
    
    public class WebserviceConfigChangedEventArgs : EventArgs
    {
        public CloudEnvironment Environment { get; set; }
        public string BaseUrl { get; set; }
        public string ManualBaseUrl { get; set; }
        public string ApiVersion { get; set; }
    }
}