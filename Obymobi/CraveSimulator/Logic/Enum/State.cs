﻿namespace CraveSimulator.Logic.Enum
{
    public enum State
    {
        Starting,
        Running,
        Stopping,
        Stopped
    }
}