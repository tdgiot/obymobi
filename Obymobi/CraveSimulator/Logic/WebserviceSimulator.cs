﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;
using System.Timers;
using CraveSimulator.Logic.Enum;
using CraveSimulator.Logic.Interfaces;
using CraveSimulator.Managers;
using CraveSimulator.Models;
using Dionysos;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Timer = System.Timers.Timer;

namespace CraveSimulator.Logic
{
    public class WebserviceSimulator : IAsyncWebserviceResult
    {
        #region  Fields

        private readonly object lockObject = new object();

        private CancellationTokenSource cancelToken;
        private ClientCollection clientCollection;
        private CountdownEvent countDown;

        private Timer flushTimer;

        private bool isAsync;

        private int numOfThreads = Environment.ProcessorCount;

        private Stopwatch runTime = new Stopwatch();

        private State simulatorState = State.Stopped;
        private long totalCompletedCalls;
        private long totalCompletedCallsFlush;

        private BlockingCollection<WebserviceClient> webserviceClientQueue;

        #endregion

        #region Properties

        public State SimulatorState
        {
            get { return this.simulatorState; }
            private set
            {
                this.simulatorState = value;
                OnSimulatorStateChanged(value);
            }
        }

        public bool IsAsync
        {
            get { return this.isAsync; }
            set
            {
                if (this.SimulatorState != State.Stopped)
                {
                    throw new AccessViolationException("Can not change IsAsync value while running");
                }

                this.isAsync = value;
            }
        }

        public int NumOfThreads
        {
            get { return this.numOfThreads; }
            set
            {
                if (this.SimulatorState != State.Stopped)
                {
                    throw new AccessViolationException("Can not change NumOfThreads value while running");
                }

                this.numOfThreads = value;
            }
        }

        #endregion

        #region Methods

        public void CallComplete(WebserviceClient client)
        {
            this.webserviceClientQueue.TryAdd(client);

            lock(this.lockObject)
            {
                this.totalCompletedCalls++;
                this.totalCompletedCallsFlush++;
            }
        }

        #endregion

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern bool GetProcessAffinityMask(IntPtr currentProcess, ref long lpProcessAffinityMask, ref long lpSystemAffinityMask);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int GetCurrentThreadId();

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int GetCurrentProcessorNumber();

        public event EventHandler<string> WriteLog;
        public event EventHandler<State> StateChanged;

        public void Start()
        {
            if (this.SimulatorState != State.Stopped)
            {
                OnWriteLog("Unable to start simulator. Current state: {0}", this.SimulatorState);
            }

            this.SimulatorState = State.Starting;

            this.totalCompletedCalls = 0;
            this.totalCompletedCallsFlush = 0;

            this.cancelToken = new CancellationTokenSource();
            this.countDown = new CountdownEvent(this.NumOfThreads);

            this.flushTimer = new Timer();
            this.flushTimer.Elapsed += FlushData;
            this.flushTimer.Interval = 1000;

            InitializeData();

            this.runTime.Reset();
            this.runTime.Start();

            StartWorkingThreads();

            this.flushTimer.Start();

            this.SimulatorState = State.Running;
        }

        public void Stop()
        {
            if (this.SimulatorState != State.Running)
            {
                OnWriteLog("Unable to stop simulator. Current state: {0}", this.SimulatorState);
            }

            OnWriteLog("Signalling threads to stop working");
            this.SimulatorState = State.Stopping;

            // Stopping threads
            this.cancelToken.Cancel();
            this.countDown.Wait();

            Thread.Sleep(2000);

            // Stop flush timer
            this.flushTimer.Stop();

            this.runTime.Stop();

            OnWriteLog("=====================================================================");
            OnWriteLog("Run time: {0}", this.runTime.Elapsed.ToString("g"));
            OnWriteLog("Total calls: {0}", this.totalCompletedCalls);
            OnWriteLog("Average call/second: {0:0.00}", (this.totalCompletedCalls / this.runTime.Elapsed.TotalSeconds));

            this.SimulatorState = State.Stopped;
        }

        private void InitializeData()
        {
            OnWriteLog("Initializing simulation data");

            WebserviceConfig wsConfig = WebserviceManager.Instance.GetConfig();

            ClientCollection allClients = DataManager.Instance.LoadClients();
            int count = allClients.Count;
            int totalAmount = (this.NumOfThreads * 25);
            if (count > totalAmount)
            {
                this.clientCollection = new ClientCollection();

                Random rnGesus = new Random((int)DateTime.Now.Ticks);
                for (int i = 0; i < totalAmount; i++)
                {
                    this.clientCollection.Add(allClients[rnGesus.Next(0, count - 1)]);
                }
            }
            else
            {
                this.clientCollection = new ClientCollection(allClients);
            }

            this.webserviceClientQueue = new BlockingCollection<WebserviceClient>(new ConcurrentQueue<WebserviceClient>());
            foreach (ClientEntity clientEntity in this.clientCollection)
            {
                WebserviceClientConfig wcConfig = new WebserviceClientConfig
                                                  {
                                                      CompanyId = clientEntity.CompanyId,
                                                      Salt = clientEntity.CompanyEntity.Salt,
                                                      ClientId = clientEntity.ClientId,
                                                      MacAddress = clientEntity.MacAddress
                                                  };

                this.webserviceClientQueue.Add(new WebserviceClient(wsConfig, wcConfig, this));
            }

            OnWriteLog("Created {0} webservice clients", this.webserviceClientQueue.Count);
        }

        private void StartWorkingThreads()
        {
            long lpProcessAffinityMask = 0;
            long lpSystemAffinityMask = 0;
            IntPtr currentProcess = Process.GetCurrentProcess().Handle;

            WebserviceSimulator.GetProcessAffinityMask(currentProcess, ref lpProcessAffinityMask, ref lpSystemAffinityMask);

            OnWriteLog("Starting {0} worker threads", this.NumOfThreads);

            bool setProcessorAffinity = this.NumOfThreads <= Environment.ProcessorCount;

            // Start threads
            for (int i = 0; i < this.NumOfThreads; i++)
            {
                long currProcMask = (setProcessorAffinity) ? (lpSystemAffinityMask & (1 << i)) : -1;
                
                Thread t = new Thread(ThreadDoWork);
                t.Start(new object[] {i, currProcMask});
            }
        }

        private void ThreadDoWork(object parameters)
        {
            object[] parameterArray = (object[])parameters;
            int currThreadId = (int)parameterArray[0];
            long currProcMask = (long)parameterArray[1];

            if (currProcMask >= 1)
            {
                foreach (ProcessThread pt in Process.GetCurrentProcess().Threads)
                {
                    int utid = WebserviceSimulator.GetCurrentThreadId();
                    if (utid == pt.Id)
                    {
                        pt.ProcessorAffinity = (IntPtr)(currProcMask); // Set affinity for this thread to the mask in curProcMask

                        Thread.Sleep(100);
                        OnWriteLog("Running thread #{0} on processor {1} (forced)", currThreadId, WebserviceSimulator.GetCurrentProcessorNumber());

                        break;
                    }
                }
            }
            else
            {
                OnWriteLog("Running thread #{0} on processor {1}", currThreadId, WebserviceSimulator.GetCurrentProcessorNumber());
            }

            try
            {
                while (!this.cancelToken.IsCancellationRequested)
                {
                    WebserviceClient client = this.webserviceClientQueue.Take();
                    client.CallGetSetClientStatus();
                }
            }
            catch (Exception ex)
            {
                OnWriteLog("Thread #{0} - Exception: {1}", currThreadId, ex.Message);
            }

            OnWriteLog("Thread #{0} stopped", currThreadId);

            this.countDown.Signal();
        }

        private void FlushData(object state, ElapsedEventArgs e)
        {
            long totalCalls;
            long totalCallsFlush;
            lock(this.lockObject)
            {
                totalCalls = this.totalCompletedCalls;
                totalCallsFlush = this.totalCompletedCallsFlush;
                this.totalCompletedCallsFlush = 0; // Reset
            }

            OnWriteLog("Total completed calls: {0} | Since last flush: {1} | Queue Length: {2}", totalCalls, totalCallsFlush, this.webserviceClientQueue.Count);
        }

        protected virtual void OnWriteLog(string msg, params object[] args)
        {
            EventHandler<string> handler = this.WriteLog;
            if (handler != null)
            {
                handler(this, "[Webservice Simulator] " + msg.FormatSafe(args));
            }
        }

        protected virtual void OnSimulatorStateChanged(State s)
        {
            EventHandler<State> handler = this.StateChanged;
            if (handler != null)
            {
                handler(this, s);
            }
        }
    }
}