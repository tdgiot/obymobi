﻿using System;
using Dionysos;
using Microsoft.AspNet.SignalR.Client;
using Obymobi.Logic.Comet.Netmessages;
using Obymobi.Logic.Model;

namespace CraveSimulator.Logic.Comet
{
    public abstract class BaseClient
    {
        private readonly object handlerLock = new object();
        private readonly InfiniteLooper pingPongChecker;

        protected bool IsAuthenticated { get; set; }

        protected string BaseUrl { get; set; }
        protected string MacAddress { get; set; }
        protected string Salt { get; set; }
        protected int CompanyId { get; set; }
        protected int DeliverypointgroupId { get; set; }
        protected int ClientId { get; set; }

        protected string CometIdentifier { get; set; }

        public ConnectionState State { get; protected set; }

        protected DateTime LastPong { get; set; }

        public event EventHandler<string> OnLog;

        protected BaseClient()
        {
            this.IsAuthenticated = false;
            this.LastPong = DateTime.Now;

            this.pingPongChecker = new InfiniteLooper(() =>
            {
                if (this.State == ConnectionState.Connected && !CheckLastPingPong())
                {
                    OnEventLog("Ping/Pong - No ping message received for 2 minutes, disconnecting client.");

                    Disconnect();

                    this.State = ConnectionState.Disconnected;

                    Connect();
                }
            }, 30000, -1, OnExceptionHandler);
        }

        public abstract void Connect();

        public abstract void Disconnect(bool exit = false);

        public abstract void TransmitMessage(Netmessage message);

        public void Pong()
        {
            TransmitMessage(new NetmessagePong());

            this.LastPong = DateTime.Now;
        }

        protected void StartPingPongChecker()
        {
            lock (this.handlerLock)
            {
                if (this.pingPongChecker != null && !this.pingPongChecker.IsRunning)
                {
                    OnEventLog("Ping/Pong - Starting PingPongChecker");

                    this.pingPongChecker.Start();
                }
            }
        }

        protected void StopPingPongChecker()
        {
            lock (this.handlerLock)
            {
                if (this.pingPongChecker != null && this.pingPongChecker.IsRunning)
                {
                    OnEventLog("Ping/Pong - Stopping PingPongChecker");
                    this.pingPongChecker.Stop();
                }
            }
        }

        private void OnExceptionHandler(Exception exception)
        {
            OnEventLog("Ping/Pong Exception - " + exception.Message);

            StopPingPongChecker();
            StartPingPongChecker();
        }

        private bool CheckLastPingPong()
        {
            return (DateTime.Now.Subtract(this.LastPong) < new TimeSpan(0, 2, 0));
        }

        protected void OnEventLog(string message, params object[] args)
        {
            string intermediateString = string.Format(message, args);
            //string finalString = string.Format("[{0}] {1}", this.MacAddress, intermediateString);

            if (this.OnLog != null)
            {
                this.OnLog.Invoke(this, intermediateString);
            }
        }
    }
}