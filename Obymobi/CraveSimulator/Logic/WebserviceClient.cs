﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using CraveSimulator.Logic.Interfaces;
using CraveSimulator.Models;
using Dionysos;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;
using Obymobi.Security;

namespace CraveSimulator.Logic
{
    public class WebserviceClient
    {
        private readonly WebserviceClientConfig clientConfig;
        private readonly IAsyncWebserviceResult receiver;

        private readonly string webserviceBaseUrl;
        private string clientStatusXml;

        public WebserviceClient(WebserviceConfig config, WebserviceClientConfig clientConfig, IAsyncWebserviceResult receiver)
        {
            this.clientConfig = clientConfig;
            this.receiver = receiver;

            this.webserviceBaseUrl = string.Format("{0}api/{1}/CraveService.asmx/", config.GetBaseUrl(), config.ApiVersion);

            ClientStatus clientStatus = new ClientStatus();
            this.clientStatusXml = XmlHelper.Serialize(clientStatus);
        }

        public string CallIsDatabaseAlive()
        {
            return DoWebserviceCall("IsDatabaseAlive");
        }

        public string CallGetClient(string macAddress, int deliverypointgroupId)
        {
            NameValueCollection args = new NameValueCollection();
            args.Add("clientId", "0");
            args.Add("clientmacAddress", macAddress);
            args.Add("deliverypointgroupId", deliverypointgroupId.ToString());
            args.Add("deliverypointId", "0");
            args.Add("applicationVersion", "");
            args.Add("osVersion", "");
            args.Add("deviceType", "3");
            args.Add("deviceModel", "51");

            return DoWebserviceCall("GetClient" + CreateParameterString(args));
        }

        public string CallGetSetClientStatus()
        {
            int rndNumber = RandomUtil.GetRandomNumber(100);

            ClientStatus clientStatus = new ClientStatus();
            clientStatus.BatteryLevel = rndNumber;
            clientStatus.IsCharging = (rndNumber % 2) == 0;
            clientStatus.AgentIsRunning = (rndNumber % 5) == 1;
            this.clientStatusXml = XmlHelper.Serialize(clientStatus);

            NameValueCollection args = new NameValueCollection();
            args.Add("xml", this.clientStatusXml);

            return DoWebserviceCall("GetSetClientStatus" + CreateParameterString(args));
        }

        private string CreateParameterString(NameValueCollection args)
        {
            // Default parameters:
            // timestamp
            // macAddress
            // hash

            DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            long timestamp = Convert.ToInt64((DateTime.Now.ToUniversalTime() - epoch).TotalSeconds);

            string parameters = string.Format("?timestamp={0}", timestamp);
            parameters += string.Format("&macAddress={0}", this.clientConfig.MacAddress);

            List<object> objs = new List<object>();
            foreach (string key in args.Keys)
            {
                parameters += string.Format("&{0}={1}", key, args[key]);
                objs.Add(args[key]);
            }

            string hash = Hasher.GetHashFromParameters(this.clientConfig.Salt, timestamp, this.clientConfig.MacAddress, objs.ToArray());
            parameters += string.Format("&hash={0}", hash);

            return parameters;
        }

        public string GetRandomMacAddress()
        {
            Random random = new Random();
            byte[] buffer = new byte[6];
            random.NextBytes(buffer);
            string result = string.Concat(buffer.Select(x => string.Format("{0}:", x.ToString("X2"))).ToArray());
            return result.TrimEnd(':');
        }

        private string DoWebserviceCall(string url)
        {
            string result = string.Empty;

            using (WebClient webClient = new WebClient())
            {
                webClient.Proxy = null;

                if (this.clientConfig.RunAsync)
                {
                    webClient.DownloadStringCompleted += WebClient_DownloadStringCompleted;
                    webClient.DownloadStringAsync(new Uri(this.webserviceBaseUrl + url));
                }
                else
                {
                    webClient.DownloadString(new Uri(this.webserviceBaseUrl + url));
                    this.receiver.CallComplete(this);
                }
            }

            return result;
        }

        private void WebClient_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            this.receiver.CallComplete(this);
        }
    }
}