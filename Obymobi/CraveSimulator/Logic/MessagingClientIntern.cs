﻿using CraveSimulator.Logic.Comet;
using CraveSimulator.Logic.Interfaces;
using CraveSimulator.Models;
using Dionysos;

namespace CraveSimulator.Logic
{
    public class MessagingClientIntern
    {
        private readonly IMessagingClientCallback callbackReceiver;
        private readonly string cometUrl;
        private readonly MessagingClientConfig config;

        private SignalRClient signalRClient;

        public string Identifier
        {
            get { return this.config.Identifier; }
        }

        public MessagingClientIntern(WebserviceConfig wsConfig, MessagingClientConfig mcConfig, IMessagingClientCallback callback)
        {
            this.callbackReceiver = callback;

            this.cometUrl = string.Format("{0}messaging/signalr", wsConfig.GetBaseUrl());
            this.config = mcConfig;
        }

        public bool Start()
        {
            this.signalRClient = new SignalRClient(this.cometUrl, this.config.Identifier, this.config.Salt, this.config.ClientId, this.config.DeliverypointgroupId, this.config.CompanyId);
            this.signalRClient.OnLog += signalRClient_OnLog;

            this.signalRClient.Connect();

            return true;
        }

        public void Exit()
        {
            this.signalRClient.Disconnect(true);
        }

        void signalRClient_OnLog(object sender, string e)
        {
            WriteToLog(e);
        }

        private void WriteToLog(string message, params object[] args)
        {
            message = message.FormatSafe(args);
            this.callbackReceiver.WriteToLog(string.Format("[{0}] {1}", this.config.Id, message));
        }
    }
}