﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using CraveSimulator.Logic.Enum;
using CraveSimulator.Logic.Interfaces;
using CraveSimulator.Managers;
using CraveSimulator.Models;
using Dionysos;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;

namespace CraveSimulator.Logic
{
    public class MessagingSimulator : IMessagingClientCallback
    {
        private State simulatorState = State.Stopped;

        private readonly ConcurrentDictionary<string, MessagingClientIntern> clients = new ConcurrentDictionary<string, MessagingClientIntern>();
        private ClientCollection clientCollection = new ClientCollection();

        private CancellationTokenSource cancelToken;

        private int totalClients;
        private int initialClientCount;
        private int incrementalClientCount;
        private int incrementDelay;

        private int totalCreated;

        public event EventHandler<string> WriteLog;
        public event EventHandler<State> StateChanged;

        public State State
        {
            get { return this.simulatorState; }
            set
            {
                this.simulatorState = value;
                OnSimulatorStateChanged(value);
            }
        }

        public void Start(int initialCount, int incrementalCount, int total, int delay)
        {
            if (!SetState(State.Starting, State.Stopped))
            {
                return;
            }

            this.State = State.Starting;

            this.totalClients = total;
            this.initialClientCount = initialCount;
            this.incrementalClientCount = incrementalCount;
            this.incrementDelay = delay;

            this.totalCreated = 0;
            this.cancelToken = new CancellationTokenSource();

            InitializeClients();

            Task.Factory.StartNew(() => StartConnections(this.cancelToken.Token));

            this.State = State.Running;
        }

        public void Stop()
        {
            if (!SetState(State.Stopping, State.Running))
            {
                return;
            }

            this.cancelToken.Cancel();
            
            foreach (MessagingClientIntern client in this.clients.Values)
            {
                client.Exit();
            }

            this.State = State.Stopped;
        }

        public bool CheckIfProcessExists()
        {
            return File.Exists(MessagingClient.MESSAGING_PROCESS_NAME);
        }

        public void AddExtraClient()
        {
            if (this.clients.Count < this.totalClients)
            {
                WriteToLog("Unable to add more clients, initial startup is not done yet");
                return;
            }
            if (this.clients.Count == this.clientCollection.Count)
            {
                WriteToLog("Unable to add more clients, no more free ClientEntities");
                return;
            }
            
            WebserviceConfig wsConfig = WebserviceManager.Instance.GetConfig();
            ClientEntity entity = this.clientCollection[this.totalCreated];
            
            AddClient(entity, wsConfig);
        }

        private bool SetState(State newState, State requiredState)
        {
            State currentState;
            lock(this)
            {
                currentState = this.State;
                if (currentState == requiredState)
                {
                    this.State = newState;
                    return true;
                }
            }

            WriteToLog("Unable to change state to {0}, current state is {1}", newState, currentState);

            return false;
        }

        private void InitializeClients()
        {
            OnWriteLog("Initializing simulation data");

            this.clients.Clear();

            ClientCollection allClients = DataManager.Instance.LoadClients();
            this.clientCollection = new ClientCollection(allClients);

            if (this.clientCollection.Count < this.totalClients)
            {
                this.totalClients = this.clientCollection.Count;

                if (this.initialClientCount > this.totalClients)
                {
                    this.initialClientCount = this.totalClients;
                }
            }

            OnWriteLog("Initialized {0} clients", this.clientCollection.Count);
        }

        private void StartConnections(CancellationToken cancellationToken)
        {
            ServicePointManager.DefaultConnectionLimit = 255;

            WebserviceConfig wsConfig = WebserviceManager.Instance.GetConfig();

            bool useSteppingStartup = this.incrementalClientCount > 0 && (this.initialClientCount < this.totalClients);

            bool initialInit = true;
            int createdClients = 0;
            foreach (ClientEntity clientEntity in this.clientCollection)
            {
                if (cancellationToken.IsCancellationRequested)
                {
                    WriteToLog("StartConnections - Cancelled");
                    break;
                }

                if (initialInit && createdClients >= this.initialClientCount)
                {
                    initialInit = false;
                }

                if (!initialInit && useSteppingStartup && createdClients >= this.incrementalClientCount)
                {
                    WriteToLog("StartConnections - Waiting {0} seconds before creating more clients", this.incrementDelay);
                    if (cancellationToken.WaitHandle.WaitOne(this.incrementDelay * 1000))
                    {
                        WriteToLog("StartConnections - Cancelled");
                        break;
                    }

                    createdClients = 0;
                }

                if (AddClient(clientEntity, wsConfig))
                {
                    createdClients++;

                    if (cancellationToken.WaitHandle.WaitOne(100))
                    {
                        WriteToLog("StartConnections - Cancelled");
                        break;
                    }

                    if (this.totalCreated >= this.totalClients)
                    {
                        WriteToLog("StartConnections - Enough clients: {0}/{1}", this.totalCreated, this.totalClients);
                        break;
                    }
                }
            }

            WriteToLog("Finished creating clients. Total created: {0}", this.totalCreated);
        }

        private bool AddClient(ClientEntity clientEntity, WebserviceConfig wsConfig)
        {
            MessagingClientConfig mcConfig = new MessagingClientConfig
            {
                Id = (this.totalCreated + 1),
                Identifier = clientEntity.MacAddress,
                ClientId = clientEntity.ClientId,
                CompanyId = clientEntity.CompanyId,
                Salt = clientEntity.CompanyEntity.Salt,
                DeliverypointgroupId = clientEntity.DeliverypointGroupId.GetValueOrDefault(),
                DeliverypointId = clientEntity.DeliverypointId.GetValueOrDefault()
            };

            // Create MessagingClient
            MessagingClientIntern client = new MessagingClientIntern(wsConfig, mcConfig, this);

            if (client.Start())
            {
                this.totalCreated++;

                // Add client to list
                this.clients.TryAdd(client.Identifier, client);

                WriteToLog("StartConnections - Added client {0} - {1}", mcConfig.Id, client.Identifier);

                return true;
            }
            
            WriteToLog("Failed to start client: {0}", mcConfig.Identifier);
            
            return false;
        }

        private void OnWriteLog(string msg, params object[] args)
        {
            EventHandler<string> handler = this.WriteLog;
            if (handler != null)
            {
                handler(this, "[Messaging Simulator] " + msg.FormatSafe(args));
            }
        }

        private void OnSimulatorStateChanged(State s)
        {
            EventHandler<State> handler = this.StateChanged;
            if (handler != null)
            {
                handler(this, s);
            }
        }

        public void WriteToLog(string message, params object[] args)
        {
            OnWriteLog(message, args);
        }

        public void Exited(string identifier)
        {
            MessagingClientIntern client;
            if (this.clients.TryRemove(identifier, out client))
            {
                WriteToLog("Client {0} exited!", client.Identifier);
            }
        }
    }
}