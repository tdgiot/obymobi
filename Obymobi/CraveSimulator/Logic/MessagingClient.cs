﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using CraveSimulator.Logic.Interfaces;
using CraveSimulator.Models;
using Dionysos;

namespace CraveSimulator.Logic
{
    public class MessagingClient
    {
        public const string MESSAGING_PROCESS_NAME = "CraveSimulator.Messaging.exe";

        private readonly IMessagingClientCallback callbackReceiver;
        private readonly string cometUrl;
        private readonly MessagingClientConfig config;

        private Process process;

        public string Identifier
        {
            get { return this.config.Identifier; }
        }

        public MessagingClient(WebserviceConfig wsConfig, MessagingClientConfig mcConfig, IMessagingClientCallback callback)
        {
            this.callbackReceiver = callback;

            this.cometUrl = string.Format("{0}messaging/signalr", wsConfig.GetBaseUrl());
            this.config = mcConfig;
        }

        public bool Start()
        {
            string processArgs = string.Format("{0} {1} {2} {3} {4} {5}", this.cometUrl,
                                                       this.config.Identifier,
                                                       this.config.Salt,
                                                       this.config.ClientId,
                                                       this.config.DeliverypointgroupId,
                                                       this.config.CompanyId);
            ProcessStartInfo startInfo = new ProcessStartInfo
            {
                FileName = MESSAGING_PROCESS_NAME,
                Arguments = processArgs,
                WorkingDirectory = Application.StartupPath,
                UseShellExecute = false,
                CreateNoWindow = true,
                ErrorDialog = false,
                WindowStyle = ProcessWindowStyle.Hidden,
                RedirectStandardOutput = true,
                RedirectStandardInput = true,
                RedirectStandardError = true,

            };
            this.process = new Process();
            this.process.EnableRaisingEvents = true;
            this.process.OutputDataReceived += process_OutputDataReceived;
            this.process.ErrorDataReceived += process_ErrorDataReceived;
            this.process.Exited += process_Exited;
            this.process.StartInfo = startInfo;

            bool started;
            try
            {
                WriteToLog("Starting process...");
                started = this.process.Start();

                if (started)
                {
                    this.process.BeginErrorReadLine();
                    this.process.BeginOutputReadLine();
                }
                else
                {
                    WriteToLog("Failed to start process, unknown error");
                }
            }
            catch (Exception ex)
            {
                started = false;
                this.process.Dispose();

                WriteToLog("Failed to start process: {0}", ex.Message);
            }

            return started;
        }

        public void Exit()
        {
            WriteToLog("Closing process");

            //this.process.OutputDataReceived -= process_OutputDataReceived;

            using (StreamWriter input = this.process.StandardInput)
            {
                input.Write('q');
                input.Write('\n');
                input.Flush();
            }

            if (!this.process.WaitForExit(5000))
            {
                if (!this.process.CloseMainWindow())
                {
                    this.process.Kill();
                }
            }
            this.process.Close();
            //this.process = null;
        }

        void process_Exited(object sender, EventArgs e)
        {
            this.callbackReceiver.Exited(this.config.Identifier);
        }

        void process_ErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.Data))
            {
                WriteToLog(e.Data);
            }
        }

        void process_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.Data))
            {
                WriteToLog(e.Data);
            }
        }

        private void WriteToLog(string message, params object[] args)
        {
            message = message.FormatSafe(args);
            this.callbackReceiver.WriteToLog(string.Format("[{0}] {1}", this.config.Id, message));
        }
    }
}