﻿namespace CraveSimulator.Logic.Interfaces
{
    public interface IMessagingClientCallback
    {
        void WriteToLog(string message, params object[] args);

        void Exited(string identifier);
    }
}