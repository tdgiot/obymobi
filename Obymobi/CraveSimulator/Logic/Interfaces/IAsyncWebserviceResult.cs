﻿namespace CraveSimulator.Logic.Interfaces
{
    public interface IAsyncWebserviceResult
    {
        void CallComplete(WebserviceClient client);
    }
}