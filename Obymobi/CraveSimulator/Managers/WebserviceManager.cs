﻿using System;
using System.Net;
using CraveSimulator.Models;
using Obymobi.Enums;

namespace CraveSimulator.Managers
{
    public class WebserviceManager
    {
        #region  Fields

        private static WebserviceManager instance;

        private readonly WebserviceConfig webserviceConfig;

        #endregion

        #region Constructors

        private WebserviceManager()
        {
            this.webserviceConfig = new WebserviceConfig();
        }

        public static WebserviceManager Instance 
        {
            get
            {
                return instance ?? (instance = new WebserviceManager());
            }
        }

        #endregion

        #region Properties

        public CloudEnvironment Environment
        {
            get { return this.webserviceConfig.Environment; }
            set
            {
                this.webserviceConfig.Environment = value;
                OnWebserviceConfigChanged(this.webserviceConfig.GetEventArg());
            }
        }

        public string ApiVersion
        {
            get { return this.webserviceConfig.ApiVersion; }
            set
            {
                this.webserviceConfig.ApiVersion = value;
                OnWebserviceConfigChanged(this.webserviceConfig.GetEventArg());
            }
        }

        public string BaseUrl
        {
            get { return this.webserviceConfig.GetBaseUrl(); }
        }

        public string ManualBaseUrl
        {
            get { return this.webserviceConfig.ManualBaseUrl; }
            set
            {
                this.webserviceConfig.ManualBaseUrl = value;
                if (this.Environment == CloudEnvironment.Manual)
                {
                    // Manual base url is only used for manual environments
                    OnWebserviceConfigChanged(this.webserviceConfig.GetEventArg());
                }
            }
        }

        #endregion

        #region Methods

        public WebserviceConfig GetConfig()
        {
            return this.webserviceConfig;
        }

        public bool ValidateWebservice(out string errorMessage)
        {
            errorMessage = "";

            string webserviceUrl = string.Format("{0}api/{1}/CraveService.asmx", this.BaseUrl, this.ApiVersion);

            WebRequest request = HttpWebRequest.Create(webserviceUrl);
            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                if (response != null)
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        errorMessage = "Unable to connect to webservice";
                    }
                    response.Close();
                }
            }

            return (errorMessage.Length == 0);
        }

        public bool ValidateMessaging(out string errorMessage)
        {
            errorMessage = "";

            string messagingUrl = string.Format("{0}messaging/signalr/js", this.BaseUrl);
            WebRequest request = HttpWebRequest.Create(messagingUrl);
            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                if (response != null)
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        errorMessage = "Unable to connect to messaging";
                    }
                    response.Close();
                }
            }

            return (errorMessage.Length == 0);
        }

        #endregion

        #region Events

        public EventHandler<WebserviceConfigChangedEventArgs> WebserviceConfigChanged;

        #endregion

        #region Event Invokers

        protected virtual void OnWebserviceConfigChanged(WebserviceConfigChangedEventArgs e)
        {
            EventHandler<WebserviceConfigChangedEventArgs> handler = this.WebserviceConfigChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        #endregion
    }
}