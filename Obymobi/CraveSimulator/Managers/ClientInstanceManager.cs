﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CraveSimulator.Managers
{
    public class ClientInstanceManager
    {
        #region Fields

        private readonly object lockThis = new object();

        #endregion

        #region Methods

        public void Reset()
        {
            this.ClientInstances = new List<ClientInstance>();
            this.CallsTotal = 0;
            this.CallsSuccess = 0;
            this.CallsFailure = 0;
            this.Start = DateTime.Now;
        }

        public void IncrementCall(bool success)
        {
            lock(this.lockThis)
            {
                this.CallsTotal++;

                if (success)
                {
                    this.CallsSuccess++;
                }
                else
                {
                    this.CallsFailure++;
                }

                OnCallsIncremented(null, null);
            }
        }

        private void OnCallsIncremented(object sender, EventArgs e)
        {
            if (this.CallsIncremented != null)
            {
                this.CallsIncremented(sender, e);
            }
        }

        #endregion

        #region Properties

        public List<ClientInstance> ClientInstances { get; set; } 

        public int CallsTotal { get; set; }

        public int CallsSuccess { get; set; }

        public int CallsFailure { get; set; }

        public DateTime Start { get; set; }

        #endregion

        #region Events

        public event EventHandler CallsIncremented;

        #endregion
    }
}
