﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace CraveSimulator.Managers
{
    public class DataManager
    {
        #region  Fields

        private static DataManager instance;

        #endregion
        
        #region Properties
        
        private ClientCollection AllClients { get; set; }

        public static DataManager Instance
        {
            get { return (DataManager.instance ?? (DataManager.instance = new DataManager())); }
        }

        public string ConnectionString
        {
            get { return CommonDaoBase.ActualConnectionString; }
            set { CommonDaoBase.ActualConnectionString = value; }
        }

        #endregion

        public bool ConnectToDatabase(string connectionString)
        {
            if (connectionString.Length == 0)
            {
                return false;
            }

            if (!connectionString.Equals(CommonDaoBase.ActualConnectionString, StringComparison.Ordinal))
            {
                // Clear clients on db conn string change
                this.AllClients.Clear();
            }

            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    // Set connection string if we can connect
                    CommonDaoBase.ActualConnectionString = connectionString;

                    return true;
                }
            }
            catch (SqlException)
            {
                return false;
            }
        }

        public ClientCollection LoadClients()
        {
            if (this.AllClients == null || this.AllClients.Count == 0)
            {
                PredicateExpression filter = new PredicateExpression(ClientFields.DeviceId != DBNull.Value);

                PrefetchPath prefetch = new PrefetchPath(EntityType.ClientEntity);
                prefetch.Add(ClientEntity.PrefetchPathDeviceEntity);
                prefetch.Add(ClientEntity.PrefetchPathCompanyEntity);

                IncludeFieldsList includeFields = new IncludeFieldsList
                                                  {
                                                      ClientFields.ClientId,
                                                      ClientFields.CompanyId,
                                                      ClientFields.DeliverypointId,
                                                      ClientFields.DeliverypointGroupId,
                                                      CompanyFields.Salt,
                                                      DeviceFields.Identifier
                                                  };

                this.AllClients = new ClientCollection();
                this.AllClients.GetMulti(filter, includeFields, prefetch);
            }

            return this.AllClients;
        }

        public bool Validate(out string errorMessage)
        {
            errorMessage = "";

            if (CommonDaoBase.ActualConnectionString.Length > 0)
            {
                if (!ConnectToDatabase(CommonDaoBase.ActualConnectionString))
                {
                    errorMessage = "Unable to connect to the database";
                }
            }
            else
            {
                errorMessage = "Database connection string is not set or saved";
            }

            return (errorMessage.Length == 0);
        }
    }
}