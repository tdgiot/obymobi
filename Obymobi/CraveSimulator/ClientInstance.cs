﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;
using CraveSimulator.Managers;
using Timer = System.Timers.Timer;
using System.Threading;

namespace CraveSimulator
{
    public class ClientInstance
    {
        #region Constructors

        public ClientInstance(Client client, ClientInstanceManager clientInstanceManager, Form form, TextBox textbox)
        {
            this.Client = client;
            this.ClientInstanceManager = clientInstanceManager;
            this.Form = form;
            this.Textbox = textbox;
        }

        #endregion

        #region Methods

        public void CreateClient(int deliverypointgroup)
        {
            this.Client.MacAddress = this.Client.GetRandomMacAddress().ToLower();
            this.Client.CallGetClient(this.Client.MacAddress, deliverypointgroup);
        }

        public void Start()
        {
            this.CallsTotal = 0;

            this.GetSetClientStatusTimer = new Timer();
            this.GetSetClientStatusTimer.Interval = 1000; // new Random().Next(1000, 1000); // Every second
            this.GetSetClientStatusTimer.Elapsed += GetSetClientStatusTimer_Elapsed;
            this.GetSetClientStatusTimer.Start();
        }

        public void Stop()
        {
            this.GetSetClientStatusTimer.Stop();
            this.GetSetClientStatusTimer.Dispose();
        }

        #endregion

        #region Properties

        public Client Client { get; set; }

        public ClientInstanceManager ClientInstanceManager { get; set; }

        public Form Form { get; set; }

        private TextBox Textbox { get; set; }

        private Timer GetSetClientStatusTimer { get; set; }

        public int CallsTotal { get; set; }

        #endregion

        #region Event handlers

        private void GetSetClientStatusTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Task.Factory.StartNew(() => this.Client.CallGetSetClientStatus(), TaskCreationOptions.LongRunning);
            this.CallsTotal++;

            if (this.CallsTotal % 10 == 0)
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }

        #endregion
    }
}
