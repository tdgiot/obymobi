﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;
using CraveSimulator.Logic;
using CraveSimulator.Logic.Enum;
using CraveSimulator.Managers;
using CraveSimulator.Models;
using Dionysos;
using Obymobi.Enums;

namespace CraveSimulator.Controllers
{
    public class FormMainController
    {
        private readonly WebserviceManager webserviceManager;
        private readonly DataManager dataManager;

        private readonly WebserviceSimulator webserviceSimulator;
        private readonly MessagingSimulator messagingSimulator;

        #region Events

        public event EventHandler<string> WriteLog;
        public event EventHandler<StatusMessage> StatusMessage;
        public event EventHandler<WebserviceConfigChangedEventArgs> WebserviceConfigChanged;
        public event EventHandler<State> SimulatorStateChanged;

        #endregion

        #region Ctor

        public FormMainController()
        {
            this.webserviceManager = WebserviceManager.Instance;
            this.dataManager = DataManager.Instance;

            this.webserviceSimulator = new WebserviceSimulator();
            this.messagingSimulator = new MessagingSimulator();

            // Hookup events
            this.webserviceManager.WebserviceConfigChanged += (sender, args) => OnWebserviceConfigChanged(args);

            this.webserviceSimulator.StateChanged += (sender, state) => OnSimulatorStateChanged(state);
            this.webserviceSimulator.WriteLog += (sender, s) => OnWriteLog(s);

            this.messagingSimulator.StateChanged += (sender, state) => OnSimulatorStateChanged(state);
            this.messagingSimulator.WriteLog += (sender, s) => OnWriteLog(s);
        }

        #endregion

        #region Public Methods

        public void SetEnvironment(CloudEnvironment environment)
        {
            this.webserviceManager.Environment = environment;
        }

        public void SetApiVersion(int version)
        {
            this.webserviceManager.ApiVersion = string.Format("{0}.0", version);
        }

        public void SetManualBaseUrl(string url)
        {
            if (this.webserviceManager.Environment == CloudEnvironment.Manual)
            {
                this.webserviceManager.ManualBaseUrl = url;
            }
        }

        public void SetConnectionString(string connectionString)
        {
            this.dataManager.ConnectionString = connectionString;

            string errorMessage;
            if (this.dataManager.Validate(out errorMessage))
            {
                OnWriteLog("Database connection save: {0}", connectionString);
            }
            else
            {
                OnStatusMessage(errorMessage, "Database Error", MessageBoxIcon.Error);
            }
        }

        public void ControlWebserviceStimulation(bool runAsync, int numThreads)
        {
            State state = this.webserviceSimulator.SimulatorState;
            if (state == State.Running)
            {
                Task.Factory.StartNew(() => this.webserviceSimulator.Stop());
            }
            else if (state == State.Stopped)
            {
                string errorMessage;
                if (!DataManager.Instance.Validate(out errorMessage))
                {
                    OnStatusMessage(errorMessage, "Database Error", MessageBoxIcon.Error);
                    return;
                }
                OnWriteLog("Database connection OK!");

                if (!WebserviceManager.Instance.ValidateWebservice(out errorMessage))
                {
                    OnStatusMessage(errorMessage, "Webservice Error", MessageBoxIcon.Error);
                    return;
                }
                OnWriteLog("Webservice connection OK!");

                this.webserviceSimulator.NumOfThreads = numThreads;
                this.webserviceSimulator.IsAsync = runAsync;

                this.webserviceSimulator.Start();
            }
            else
            {
                OnWriteLog("Unable to start/stop simulation. Current state: {0}", state);
            }
        }

        public void ControlMessagingStimulation(int initialCount, int incrementalCount, int totalClients, int delay)
        {
            State state = this.messagingSimulator.State;
            if (state == State.Running)
            {
                this.messagingSimulator.Stop();
            }
            else if (state == State.Stopped)
            {
                string errorMessage;
                if (!DataManager.Instance.Validate(out errorMessage))
                {
                    OnStatusMessage(errorMessage, "Database Error", MessageBoxIcon.Error);
                    return;
                }
                OnWriteLog("Database connection OK!");

                if (!WebserviceManager.Instance.ValidateMessaging(out errorMessage))
                {
                    OnStatusMessage(errorMessage, "Messaging Error", MessageBoxIcon.Error);
                    return;
                }
                OnWriteLog("Messaging connection OK!");

                if (!this.messagingSimulator.CheckIfProcessExists())
                {
                    errorMessage = "External process '" + MessagingClient.MESSAGING_PROCESS_NAME + "' does not exists in working directory";
                    OnStatusMessage(errorMessage, "Process Error", MessageBoxIcon.Error);
                    return;
                }
                OnWriteLog("External process exists!");

                this.messagingSimulator.Start(initialCount, incrementalCount, totalClients, delay);
            }
        }

        public void AddExtraClient()
        {
            this.messagingSimulator.AddExtraClient();
        }

        #endregion

        #region Event Invokers

        protected virtual void OnWebserviceConfigChanged(WebserviceConfigChangedEventArgs e)
        {
            EventHandler<WebserviceConfigChangedEventArgs> handler = this.WebserviceConfigChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        protected virtual void OnWriteLog(string msg, params object[] args)
        {
            EventHandler<string> handler = this.WriteLog;
            if (handler != null)
            {
                handler(this, msg.FormatSafe(args));
            }
        }

        protected virtual void OnStatusMessage(string msg, string title, MessageBoxIcon icon = MessageBoxIcon.None)
        {
            OnWriteLog(msg);

            EventHandler<StatusMessage> handler = this.StatusMessage;
            if (handler != null)
            {
                handler(this, new StatusMessage
                              {
                                  Message = msg,
                                  Title = title,
                                  Icon = icon
                              });
            }
        }

        protected virtual void OnSimulatorStateChanged(State state)
        {
            EventHandler<State> handler = this.SimulatorStateChanged;
            if (handler != null)
            {
                handler(this, state);
            }
        }

        #endregion
    }
}