﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using CraveSimulator.Controllers;
using CraveSimulator.Logic.Enum;
using CraveSimulator.Models;
using Obymobi.Data.DaoClasses;
using Obymobi.Enums;

namespace CraveSimulator
{
    public partial class FormMain : Form
    {
        [DllImport("User32.dll", CharSet = CharSet.Auto, EntryPoint = "SendMessage")]
        static extern IntPtr SendMessage(IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam);
        const int WM_VSCROLL = 277;
        const int SB_BOTTOM = 7;

        #region Constructors

        public FormMain()
        {
            InitializeComponent();

            this.Controller = new FormMainController();
            
            SetDefaults();
            HookupEvents();

            this.Controller.SetEnvironment(CloudEnvironment.Manual);

            WriteToLog("Application initialize complete");
        }

        #endregion

        #region Properties

        private FormMainController Controller { get; set; }

        #endregion

        private void SetDefaults()
        {
            this.cbWebservice.SelectedIndex = 0;
            this.numThreads.Value = Environment.ProcessorCount;

            if (TestUtil.IsPcDeveloper)
            {
                this.tbConnectionString.Text = "data source=localhost;initial catalog=ObymobiProduction;integrated security=SSPI;persist security info=False;packet size=4096";
                this.Controller.SetConnectionString(this.tbConnectionString.Text);
            }
        }

        private void HookupEvents()
        {
            this.Controller.WriteLog += (sender, s) => WriteToLog(s);
            this.Controller.StatusMessage += Controller_StatusMessage;
            this.Controller.WebserviceConfigChanged += Controller_WebserviceConfigChanged;
            this.Controller.SimulatorStateChanged += Controller_SimulatorStateChanged;
        }

        public void WriteToLog(string log, params object[] args)
        {
            if (this.tbLog.InvokeRequired)
            {
                this.tbLog.BeginInvoke((MethodInvoker)(() => WriteToLog(log, args)));
                return;
            }

            if (args.Length > 0)
            {
                log = string.Format(log, args);
            }

            this.tbLog.AppendText(string.Format("{0:HH:mm:ss.fff} {1}\r\n", DateTime.Now, log)); 
        }

        #region Event handlers

        private void Controller_StatusMessage(object sender, StatusMessage e)
        {
            if (this.InvokeRequired)
            {
                Invoke((MethodInvoker)(() => Controller_StatusMessage(sender, e)));
            }
            else
            {
                MessageBox.Show(e.Message, e.Title, MessageBoxButtons.OK, e.Icon);
            }
        }

        private void Controller_WebserviceConfigChanged(object sender, WebserviceConfigChangedEventArgs e)
        {
            this.tbBaseUrl.Enabled = (e.Environment == CloudEnvironment.Manual);
            if (e.Environment != CloudEnvironment.Manual)
                this.tbBaseUrl.Text = e.BaseUrl;
            else
                this.tbBaseUrl.Text = e.ManualBaseUrl;

            this.numApiVersion.Value = (int)double.Parse(e.ApiVersion.Replace(".", ","));

            this.lblApiUrl.Text = string.Format("{0}api/{1}/CraveService.asmx", e.BaseUrl, e.ApiVersion);
            this.lblMessagingUrl.Text = string.Format("{0}messaging/", e.BaseUrl);
        }

        private void Controller_SimulatorStateChanged(object sender, State e)
        {
            if (this.btnDoSomethingAwesome.InvokeRequired)
            {
                Invoke((MethodInvoker)(() => Controller_SimulatorStateChanged(sender, e)));
            }
            else
            {
                string text = "Start simulation";
                switch (e)
                {
                    case State.Stopping:
                        text = "Stopping simulation...";
                        break;
                    case State.Starting:
                        text = "Starting stimulation...";
                        break;
                    case State.Running:
                        text = "Stop stimulation";
                        break;
                }

                this.btnDoSomethingAwesome.Text = text;
                this.btnDoSomethingAwesome.Enabled = (e == State.Stopped || e == State.Running);

                this.panelWebservice.Enabled = (e == State.Stopped);
                this.panelDatabase.Enabled = (e == State.Stopped);
                this.panelSettings.Enabled = (e == State.Stopped);

                this.rbTypeWebservice.Enabled = (e == State.Stopped);
                this.rbTypeMessaging.Enabled = (e == State.Stopped);

                this.btnAddClient.Enabled = (e == State.Running);
            }
        }        

        private void btnDoSomethingAwesome_Click(object sender, EventArgs e)
        {
            if (this.rbTypeWebservice.Checked)
            {
                this.Controller.ControlWebserviceStimulation(this.cbRunAsync.Checked, (int)this.numThreads.Value);
            }
            else if (this.rbTypeMessaging.Checked)
            {
                int initialClients = (int)this.numMessagingInitialClients.Value;
                int incrementClients = (int)this.numMessagingIncrementClients.Value;
                int incrementDelay = (int)this.numMessagingDelay.Value;
                int totalClients = (int)this.numMessagingTotalClients.Value;
                
                this.Controller.ControlMessagingStimulation(initialClients, incrementClients, totalClients, incrementDelay);
            }
        }

        private void tbBaseUrl_TextChanged(object sender, EventArgs e)
        {
            this.Controller.SetManualBaseUrl(this.tbBaseUrl.Text);
        }

        private void cbWebservice_SelectedIndexChanged(object sender, EventArgs e)
        {
            CloudEnvironment environment = CloudEnvironment.Manual;
            switch (this.cbWebservice.SelectedIndex)
            {
                case 1: // Dev
                    environment = CloudEnvironment.Development;
                    break;
                case 2: // Test
                    environment = CloudEnvironment.Test;
                    break;
                case 3: // Live
                    environment = CloudEnvironment.ProductionPrimary;
                    break;
            }

            this.Controller.SetEnvironment(environment);
        }

        private void numApiVersion_ValueChanged(object sender, EventArgs e)
        {
            this.Controller.SetApiVersion((int)this.numApiVersion.Value);
        }

        private void btnConnectDatabase_Click(object sender, EventArgs e)
        {
            this.Controller.SetConnectionString(this.tbConnectionString.Text);
        }

        private void tbLog_TextChanged(object sender, EventArgs e)
        {
            IntPtr ptrWparam = new IntPtr(SB_BOTTOM);
            IntPtr ptrLparam = new IntPtr(0);
            SendMessage(this.tbLog.Handle, WM_VSCROLL, ptrWparam, ptrLparam);
        }

        private void tbConnectionString_TextChanged(object sender, EventArgs e)
        {
            this.Controller.SetConnectionString(this.tbConnectionString.Text);
        }

        private void numMessagingInitialClients_ValueChanged(object sender, EventArgs e)
        {
            if (this.numMessagingInitialClients.Value > this.numMessagingTotalClients.Value)
            {
                this.numMessagingTotalClients.Value = this.numMessagingInitialClients.Value;
            }
        }

        private void numMessagingTotalClients_ValueChanged(object sender, EventArgs e)
        {
            if (this.numMessagingTotalClients.Value < this.numMessagingInitialClients.Value)
            {
                this.numMessagingInitialClients.Value = this.numMessagingTotalClients.Value;
            }
        }

        private void tcSettings_Selected(object sender, TabControlEventArgs e)
        {
            if (e.TabPage == this.tpSettingsWebservice)
            {
                this.rbTypeWebservice.Checked = true;
            }
            else
            {
                this.rbTypeMessaging.Checked = true;
            }
        }

        private void rbType_CheckedChanged(object sender, EventArgs e)
        {
            this.tcSettings.SelectedTab = this.rbTypeWebservice.Checked ? this.tpSettingsWebservice : this.tpSettingsMessaging;

            this.btnAddClient.Visible = this.rbTypeMessaging.Checked;
        }

        private void btnAddClient_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < 10; i++)
                this.Controller.AddExtraClient();
        }

        #endregion
    }
}