﻿namespace CraveSimulator
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.btnDoSomethingAwesome = new System.Windows.Forms.Button();
            this.tbLog = new System.Windows.Forms.TextBox();
            this.panelWebservice = new System.Windows.Forms.GroupBox();
            this.lblApiUrl = new System.Windows.Forms.Label();
            this.lblMessagingUrl = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.numApiVersion = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.tbBaseUrl = new System.Windows.Forms.TextBox();
            this.cbWebservice = new System.Windows.Forms.ComboBox();
            this.panelDatabase = new System.Windows.Forms.GroupBox();
            this.btnConnectDatabase = new System.Windows.Forms.Button();
            this.tbConnectionString = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cbRunAsync = new System.Windows.Forms.CheckBox();
            this.panelSettings = new System.Windows.Forms.GroupBox();
            this.tcSettings = new System.Windows.Forms.TabControl();
            this.tpSettingsWebservice = new System.Windows.Forms.TabPage();
            this.label6 = new System.Windows.Forms.Label();
            this.numThreads = new System.Windows.Forms.NumericUpDown();
            this.tpSettingsMessaging = new System.Windows.Forms.TabPage();
            this.label11 = new System.Windows.Forms.Label();
            this.numMessagingDelay = new System.Windows.Forms.NumericUpDown();
            this.numMessagingIncrementClients = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.numMessagingInitialClients = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.numMessagingTotalClients = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnAddClient = new System.Windows.Forms.Button();
            this.rbTypeMessaging = new System.Windows.Forms.RadioButton();
            this.rbTypeWebservice = new System.Windows.Forms.RadioButton();
            this.panelWebservice.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numApiVersion)).BeginInit();
            this.panelDatabase.SuspendLayout();
            this.panelSettings.SuspendLayout();
            this.tcSettings.SuspendLayout();
            this.tpSettingsWebservice.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numThreads)).BeginInit();
            this.tpSettingsMessaging.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numMessagingDelay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMessagingIncrementClients)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMessagingInitialClients)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMessagingTotalClients)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnDoSomethingAwesome
            // 
            this.btnDoSomethingAwesome.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDoSomethingAwesome.Location = new System.Drawing.Point(150, 41);
            this.btnDoSomethingAwesome.Name = "btnDoSomethingAwesome";
            this.btnDoSomethingAwesome.Size = new System.Drawing.Size(267, 58);
            this.btnDoSomethingAwesome.TabIndex = 0;
            this.btnDoSomethingAwesome.Text = "Start simulation";
            this.btnDoSomethingAwesome.UseVisualStyleBackColor = true;
            this.btnDoSomethingAwesome.Click += new System.EventHandler(this.btnDoSomethingAwesome_Click);
            // 
            // tbLog
            // 
            this.tbLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbLog.Location = new System.Drawing.Point(0, 310);
            this.tbLog.Multiline = true;
            this.tbLog.Name = "tbLog";
            this.tbLog.ReadOnly = true;
            this.tbLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbLog.Size = new System.Drawing.Size(1008, 443);
            this.tbLog.TabIndex = 1;
            this.tbLog.TextChanged += new System.EventHandler(this.tbLog_TextChanged);
            // 
            // panelWebservice
            // 
            this.panelWebservice.Controls.Add(this.lblApiUrl);
            this.panelWebservice.Controls.Add(this.lblMessagingUrl);
            this.panelWebservice.Controls.Add(this.label4);
            this.panelWebservice.Controls.Add(this.label3);
            this.panelWebservice.Controls.Add(this.label2);
            this.panelWebservice.Controls.Add(this.numApiVersion);
            this.panelWebservice.Controls.Add(this.label1);
            this.panelWebservice.Controls.Add(this.tbBaseUrl);
            this.panelWebservice.Controls.Add(this.cbWebservice);
            this.panelWebservice.Location = new System.Drawing.Point(12, 12);
            this.panelWebservice.Name = "panelWebservice";
            this.panelWebservice.Size = new System.Drawing.Size(487, 120);
            this.panelWebservice.TabIndex = 9;
            this.panelWebservice.TabStop = false;
            this.panelWebservice.Text = "Webservice";
            // 
            // lblApiUrl
            // 
            this.lblApiUrl.AutoSize = true;
            this.lblApiUrl.Location = new System.Drawing.Point(70, 82);
            this.lblApiUrl.Name = "lblApiUrl";
            this.lblApiUrl.Size = new System.Drawing.Size(38, 13);
            this.lblApiUrl.TabIndex = 8;
            this.lblApiUrl.Text = "http://";
            // 
            // lblMessagingUrl
            // 
            this.lblMessagingUrl.AutoSize = true;
            this.lblMessagingUrl.Location = new System.Drawing.Point(70, 95);
            this.lblMessagingUrl.Name = "lblMessagingUrl";
            this.lblMessagingUrl.Size = new System.Drawing.Size(38, 13);
            this.lblMessagingUrl.TabIndex = 7;
            this.lblMessagingUrl.Text = "http://";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(24, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "API";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Messaging";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "API Version";
            // 
            // numApiVersion
            // 
            this.numApiVersion.Location = new System.Drawing.Point(74, 46);
            this.numApiVersion.Minimum = new decimal(new int[] {
            19,
            0,
            0,
            0});
            this.numApiVersion.Name = "numApiVersion";
            this.numApiVersion.Size = new System.Drawing.Size(47, 20);
            this.numApiVersion.TabIndex = 2;
            this.numApiVersion.Value = new decimal(new int[] {
            21,
            0,
            0,
            0});
            this.numApiVersion.ValueChanged += new System.EventHandler(this.numApiVersion_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Base URL";
            // 
            // tbBaseUrl
            // 
            this.tbBaseUrl.Location = new System.Drawing.Point(177, 20);
            this.tbBaseUrl.Name = "tbBaseUrl";
            this.tbBaseUrl.Size = new System.Drawing.Size(291, 20);
            this.tbBaseUrl.TabIndex = 1;
            this.tbBaseUrl.Text = "http://localhost/trunk/";
            this.tbBaseUrl.TextChanged += new System.EventHandler(this.tbBaseUrl_TextChanged);
            // 
            // cbWebservice
            // 
            this.cbWebservice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbWebservice.FormattingEnabled = true;
            this.cbWebservice.Items.AddRange(new object[] {
            "Manual",
            "Dev",
            "Test",
            "Live"});
            this.cbWebservice.Location = new System.Drawing.Point(74, 19);
            this.cbWebservice.Name = "cbWebservice";
            this.cbWebservice.Size = new System.Drawing.Size(97, 21);
            this.cbWebservice.TabIndex = 0;
            this.cbWebservice.SelectedIndexChanged += new System.EventHandler(this.cbWebservice_SelectedIndexChanged);
            // 
            // panelDatabase
            // 
            this.panelDatabase.Controls.Add(this.btnConnectDatabase);
            this.panelDatabase.Controls.Add(this.tbConnectionString);
            this.panelDatabase.Controls.Add(this.label5);
            this.panelDatabase.Location = new System.Drawing.Point(505, 12);
            this.panelDatabase.Name = "panelDatabase";
            this.panelDatabase.Size = new System.Drawing.Size(491, 120);
            this.panelDatabase.TabIndex = 10;
            this.panelDatabase.TabStop = false;
            this.panelDatabase.Text = "Database";
            // 
            // btnConnectDatabase
            // 
            this.btnConnectDatabase.Location = new System.Drawing.Point(9, 72);
            this.btnConnectDatabase.Name = "btnConnectDatabase";
            this.btnConnectDatabase.Size = new System.Drawing.Size(139, 23);
            this.btnConnectDatabase.TabIndex = 11;
            this.btnConnectDatabase.Text = "Test Connection String";
            this.btnConnectDatabase.UseVisualStyleBackColor = true;
            this.btnConnectDatabase.Click += new System.EventHandler(this.btnConnectDatabase_Click);
            // 
            // tbConnectionString
            // 
            this.tbConnectionString.Location = new System.Drawing.Point(9, 38);
            this.tbConnectionString.Name = "tbConnectionString";
            this.tbConnectionString.Size = new System.Drawing.Size(408, 20);
            this.tbConnectionString.TabIndex = 3;
            this.tbConnectionString.Leave += new System.EventHandler(this.tbConnectionString_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(91, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Connection String";
            // 
            // cbRunAsync
            // 
            this.cbRunAsync.AutoSize = true;
            this.cbRunAsync.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbRunAsync.Location = new System.Drawing.Point(6, 9);
            this.cbRunAsync.Name = "cbRunAsync";
            this.cbRunAsync.Size = new System.Drawing.Size(78, 17);
            this.cbRunAsync.TabIndex = 11;
            this.cbRunAsync.Text = "Run Async";
            this.cbRunAsync.UseVisualStyleBackColor = true;
            // 
            // panelSettings
            // 
            this.panelSettings.Controls.Add(this.tcSettings);
            this.panelSettings.Location = new System.Drawing.Point(12, 138);
            this.panelSettings.Name = "panelSettings";
            this.panelSettings.Size = new System.Drawing.Size(487, 166);
            this.panelSettings.TabIndex = 12;
            this.panelSettings.TabStop = false;
            this.panelSettings.Text = "Settings";
            // 
            // tcSettings
            // 
            this.tcSettings.Controls.Add(this.tpSettingsWebservice);
            this.tcSettings.Controls.Add(this.tpSettingsMessaging);
            this.tcSettings.Location = new System.Drawing.Point(9, 19);
            this.tcSettings.Name = "tcSettings";
            this.tcSettings.SelectedIndex = 0;
            this.tcSettings.Size = new System.Drawing.Size(472, 141);
            this.tcSettings.TabIndex = 14;
            this.tcSettings.Selected += new System.Windows.Forms.TabControlEventHandler(this.tcSettings_Selected);
            // 
            // tpSettingsWebservice
            // 
            this.tpSettingsWebservice.Controls.Add(this.cbRunAsync);
            this.tpSettingsWebservice.Controls.Add(this.label6);
            this.tpSettingsWebservice.Controls.Add(this.numThreads);
            this.tpSettingsWebservice.Location = new System.Drawing.Point(4, 22);
            this.tpSettingsWebservice.Name = "tpSettingsWebservice";
            this.tpSettingsWebservice.Padding = new System.Windows.Forms.Padding(3);
            this.tpSettingsWebservice.Size = new System.Drawing.Size(464, 115);
            this.tpSettingsWebservice.TabIndex = 0;
            this.tpSettingsWebservice.Text = "Webservice";
            this.tpSettingsWebservice.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 39);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Threads";
            // 
            // numThreads
            // 
            this.numThreads.Location = new System.Drawing.Point(71, 32);
            this.numThreads.Maximum = new decimal(new int[] {
            64,
            0,
            0,
            0});
            this.numThreads.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numThreads.Name = "numThreads";
            this.numThreads.Size = new System.Drawing.Size(47, 20);
            this.numThreads.TabIndex = 13;
            this.numThreads.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // tpSettingsMessaging
            // 
            this.tpSettingsMessaging.Controls.Add(this.label11);
            this.tpSettingsMessaging.Controls.Add(this.numMessagingDelay);
            this.tpSettingsMessaging.Controls.Add(this.numMessagingIncrementClients);
            this.tpSettingsMessaging.Controls.Add(this.label10);
            this.tpSettingsMessaging.Controls.Add(this.label9);
            this.tpSettingsMessaging.Controls.Add(this.numMessagingInitialClients);
            this.tpSettingsMessaging.Controls.Add(this.label8);
            this.tpSettingsMessaging.Controls.Add(this.numMessagingTotalClients);
            this.tpSettingsMessaging.Controls.Add(this.label7);
            this.tpSettingsMessaging.Location = new System.Drawing.Point(4, 22);
            this.tpSettingsMessaging.Name = "tpSettingsMessaging";
            this.tpSettingsMessaging.Padding = new System.Windows.Forms.Padding(3);
            this.tpSettingsMessaging.Size = new System.Drawing.Size(464, 115);
            this.tpSettingsMessaging.TabIndex = 1;
            this.tpSettingsMessaging.Text = "Messaging";
            this.tpSettingsMessaging.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(159, 62);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 13);
            this.label11.TabIndex = 8;
            this.label11.Text = "(seconds)";
            // 
            // numMessagingDelay
            // 
            this.numMessagingDelay.Location = new System.Drawing.Point(100, 58);
            this.numMessagingDelay.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.numMessagingDelay.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numMessagingDelay.Name = "numMessagingDelay";
            this.numMessagingDelay.Size = new System.Drawing.Size(58, 20);
            this.numMessagingDelay.TabIndex = 3;
            this.numMessagingDelay.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numMessagingIncrementClients
            // 
            this.numMessagingIncrementClients.Location = new System.Drawing.Point(100, 32);
            this.numMessagingIncrementClients.Name = "numMessagingIncrementClients";
            this.numMessagingIncrementClients.Size = new System.Drawing.Size(58, 20);
            this.numMessagingIncrementClients.TabIndex = 2;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 60);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(84, 13);
            this.label10.TabIndex = 5;
            this.label10.Text = "Increment Delay";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 34);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(88, 13);
            this.label9.TabIndex = 4;
            this.label9.Text = "Increment Clients";
            // 
            // numMessagingInitialClients
            // 
            this.numMessagingInitialClients.Location = new System.Drawing.Point(100, 6);
            this.numMessagingInitialClients.Name = "numMessagingInitialClients";
            this.numMessagingInitialClients.Size = new System.Drawing.Size(58, 20);
            this.numMessagingInitialClients.TabIndex = 1;
            this.numMessagingInitialClients.ValueChanged += new System.EventHandler(this.numMessagingInitialClients_ValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 10);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Initial Clients";
            // 
            // numMessagingTotalClients
            // 
            this.numMessagingTotalClients.Location = new System.Drawing.Point(100, 84);
            this.numMessagingTotalClients.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numMessagingTotalClients.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numMessagingTotalClients.Name = "numMessagingTotalClients";
            this.numMessagingTotalClients.Size = new System.Drawing.Size(58, 20);
            this.numMessagingTotalClients.TabIndex = 4;
            this.numMessagingTotalClients.Value = new decimal(new int[] {
            25,
            0,
            0,
            0});
            this.numMessagingTotalClients.ValueChanged += new System.EventHandler(this.numMessagingTotalClients_ValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 88);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Total Clients";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnAddClient);
            this.groupBox1.Controls.Add(this.rbTypeMessaging);
            this.groupBox1.Controls.Add(this.rbTypeWebservice);
            this.groupBox1.Controls.Add(this.btnDoSomethingAwesome);
            this.groupBox1.Location = new System.Drawing.Point(505, 138);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(491, 166);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Simulation";
            // 
            // btnAddClient
            // 
            this.btnAddClient.Enabled = false;
            this.btnAddClient.Location = new System.Drawing.Point(212, 105);
            this.btnAddClient.Name = "btnAddClient";
            this.btnAddClient.Size = new System.Drawing.Size(133, 23);
            this.btnAddClient.TabIndex = 3;
            this.btnAddClient.Text = "Add 10 extra clients";
            this.btnAddClient.UseVisualStyleBackColor = true;
            this.btnAddClient.Visible = false;
            this.btnAddClient.Click += new System.EventHandler(this.btnAddClient_Click);
            // 
            // rbTypeMessaging
            // 
            this.rbTypeMessaging.AutoSize = true;
            this.rbTypeMessaging.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.rbTypeMessaging.Location = new System.Drawing.Point(39, 73);
            this.rbTypeMessaging.Name = "rbTypeMessaging";
            this.rbTypeMessaging.Size = new System.Drawing.Size(76, 17);
            this.rbTypeMessaging.TabIndex = 2;
            this.rbTypeMessaging.Text = "Messaging";
            this.rbTypeMessaging.UseVisualStyleBackColor = true;
            this.rbTypeMessaging.CheckedChanged += new System.EventHandler(this.rbType_CheckedChanged);
            // 
            // rbTypeWebservice
            // 
            this.rbTypeWebservice.AutoSize = true;
            this.rbTypeWebservice.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.rbTypeWebservice.Checked = true;
            this.rbTypeWebservice.Location = new System.Drawing.Point(33, 49);
            this.rbTypeWebservice.Name = "rbTypeWebservice";
            this.rbTypeWebservice.Size = new System.Drawing.Size(82, 17);
            this.rbTypeWebservice.TabIndex = 1;
            this.rbTypeWebservice.TabStop = true;
            this.rbTypeWebservice.Text = "Webservice";
            this.rbTypeWebservice.UseVisualStyleBackColor = true;
            this.rbTypeWebservice.CheckedChanged += new System.EventHandler(this.rbType_CheckedChanged);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 753);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panelSettings);
            this.Controls.Add(this.panelDatabase);
            this.Controls.Add(this.panelWebservice);
            this.Controls.Add(this.tbLog);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormMain";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Crave Simulator 2000";
            this.panelWebservice.ResumeLayout(false);
            this.panelWebservice.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numApiVersion)).EndInit();
            this.panelDatabase.ResumeLayout(false);
            this.panelDatabase.PerformLayout();
            this.panelSettings.ResumeLayout(false);
            this.tcSettings.ResumeLayout(false);
            this.tpSettingsWebservice.ResumeLayout(false);
            this.tpSettingsWebservice.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numThreads)).EndInit();
            this.tpSettingsMessaging.ResumeLayout(false);
            this.tpSettingsMessaging.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numMessagingDelay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMessagingIncrementClients)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMessagingInitialClients)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMessagingTotalClients)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnDoSomethingAwesome;
        private System.Windows.Forms.TextBox tbLog;
        private System.Windows.Forms.GroupBox panelWebservice;
        private System.Windows.Forms.Label lblApiUrl;
        private System.Windows.Forms.Label lblMessagingUrl;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numApiVersion;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbBaseUrl;
        private System.Windows.Forms.ComboBox cbWebservice;
        private System.Windows.Forms.GroupBox panelDatabase;
        private System.Windows.Forms.Button btnConnectDatabase;
        private System.Windows.Forms.TextBox tbConnectionString;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox cbRunAsync;
        private System.Windows.Forms.GroupBox panelSettings;
        private System.Windows.Forms.NumericUpDown numThreads;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TabControl tcSettings;
        private System.Windows.Forms.TabPage tpSettingsWebservice;
        private System.Windows.Forms.TabPage tpSettingsMessaging;
        private System.Windows.Forms.NumericUpDown numMessagingTotalClients;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbTypeMessaging;
        private System.Windows.Forms.RadioButton rbTypeWebservice;
        private System.Windows.Forms.NumericUpDown numMessagingDelay;
        private System.Windows.Forms.NumericUpDown numMessagingIncrementClients;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown numMessagingInitialClients;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnAddClient;
    }
}

