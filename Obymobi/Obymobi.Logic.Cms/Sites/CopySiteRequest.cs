﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Obymobi.Data.EntityClasses;

namespace Obymobi.Logic.Cms.Sites
{
    public class CopySiteRequest
    {
        public int SiteId;
        public string SiteName;        
        public Func<MediaEntity, MediaEntity, bool> CopyMediaOnCdn;
    }
}
