﻿using Obymobi.Data.EntityClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using Dionysos.Data.LLBLGen;
using System.Collections;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data;
using Obymobi.Logic.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Cms.Sites;
using Obymobi.Logic.Media;

namespace Obymobi.Logic.Cms
{
    public static class SiteHelper
    {
        public enum SiteHelperResult
        { 
            UnspecifiedEntitySaveException = 202,
            NoSiteIsExistingForSiteId = 203,
            LanguageCodeInvalid = 204,
            CultureCodeEmpty = 205
        }

        #region Retrieval

        /// <summary>
        /// Gets the last modified ticks for the site with the specified id
        /// </summary>
        /// <param name="siteId">The id of the site to get the ticks for</param>
        /// <returns></returns>
        public static long GetSiteTicks(int siteId)
        {
            IncludeFieldsList fields = new IncludeFieldsList();
            fields.Add(SiteFields.LastModifiedUTC);

            var siteEntity = new SiteEntity(siteId);
            if (siteEntity.IsNew)
                throw new ObymobiException(GetSiteTicksResult.UnknownSiteId, "Id: {0}", siteId);

            return siteEntity.LastModifiedUTC.Ticks;
        }

        public static bool IsGenericSite(int siteId)
        {
            IncludeFieldsList fields = new IncludeFieldsList();
            fields.Add(SiteFields.CompanyId);

            var siteEntity = new SiteEntity(siteId);
            if (siteEntity.IsNew)
                throw new ObymobiException(GetSiteTicksResult.UnknownSiteId, "Id: {0}", siteId);

            return !siteEntity.CompanyId.HasValue;
        }        

        /// <summary>
        /// Get a Site complety prefetched and translated for usage in the webservice
        /// </summary>
        /// <param name="siteId">Site Id</param>
        /// <param name="cultureCode">Culture code - Will use en-GB if empty</param>
        /// <param name="deviceType">Device Type</param>
        /// <param name="tablet">Tablet?</param>
        /// <returns></returns>
        public static SiteEntity GetTranslatedSite(int siteId, string cultureCode, DeviceType deviceType, bool tablet)
        {
            // Assume the default culture is English GB (TODO: change to Chinese when they take over)
            SiteEntity siteToReturn = SiteHelper.GetSite(siteId, "en-GB", deviceType, tablet);
            if (siteToReturn != null && !siteToReturn.IsNew && !string.IsNullOrEmpty(cultureCode) && !cultureCode.Equals("en-GB", StringComparison.InvariantCultureIgnoreCase))
            {
                // Fetch the site in the requested culture
                SiteEntity siteTranslated = SiteHelper.GetSite(siteId, cultureCode, deviceType, tablet);
                if (siteTranslated != null)
                {
                    Dictionary<CustomTextType, string> siteCustomTexts = siteTranslated.CustomTextCollection.ToCultureCodeSpecificDictionary(cultureCode);
                    
                    // Set the translated description
                    string translatedDescription = siteCustomTexts.GetValueOrDefault(CustomTextType.SiteDescription);
                    if (!translatedDescription.IsNullOrWhiteSpace())
                    {
                        siteTranslated.Description = translatedDescription;
                    }

                    foreach (PageEntity translatedPage in siteTranslated.PageCollection)
                    {
                        PageEntity defaultPage = siteToReturn.PageCollection.FirstOrDefault(x => x.PageId == translatedPage.PageId);
                        if (defaultPage != null)
                        {
                            // Check for each element if we have a translation
                            foreach (PageElementEntity pageElementTranslated in translatedPage.PageElementCollection)
                            {
                                PageElementEntity pageElementDefault = defaultPage.PageElementCollection.FirstOrDefault(x => x.SystemName == pageElementTranslated.SystemName);
                                if (pageElementDefault == null) continue;

                                if (pageElementTranslated.StringValue1.IsNullOrWhiteSpace())
                                    pageElementTranslated.StringValue1 = pageElementDefault.StringValue1;
                                if (pageElementTranslated.StringValue2.IsNullOrWhiteSpace())
                                    pageElementTranslated.StringValue2 = pageElementDefault.StringValue2;
                                if (pageElementTranslated.StringValue3.IsNullOrWhiteSpace())
                                    pageElementTranslated.StringValue3 = pageElementDefault.StringValue3;
                                if (pageElementTranslated.StringValue4.IsNullOrWhiteSpace())
                                    pageElementTranslated.StringValue4 = pageElementDefault.StringValue4;
                                if (pageElementTranslated.StringValue5.IsNullOrWhiteSpace())
                                    pageElementTranslated.StringValue5 = pageElementDefault.StringValue5;
                            }

                            // It could be that the default page has more pageElements already defined than the translated one, copy these too to show at least something
                            if (defaultPage.PageElementCollection.Count > translatedPage.PageElementCollection.Count)
                            {
                                List<string> alreadyAddedSystemNames = translatedPage.PageElementCollection.Select(x => x.SystemName).ToList();
                                PageElementCollection elementsToAdd = new PageElementCollection();
                                for (int i = 0; i < defaultPage.PageElementCollection.Count; i++)                                
                                {
                                    if (!alreadyAddedSystemNames.Contains(defaultPage.PageElementCollection[i].SystemName))
                                    {
                                        alreadyAddedSystemNames.Add(defaultPage.PageElementCollection[i].SystemName);
                                        elementsToAdd.Add(defaultPage.PageElementCollection[i]);
                                    }
                                }

                                if (elementsToAdd.Count > 0)
                                {
                                    translatedPage.PageElementCollection.AddRange(elementsToAdd);
                                }
                            }
                        }
                    }

                    // Return the translated site (for attached media, attachments, pageTemplates etc which we need in the entity converters)
                    siteToReturn = siteTranslated;
                }                
            }
            return siteToReturn;
        }

        /// <summary>
        /// Get a Site complety prefetched for usage in the webservice
        /// </summary>
        /// <param name="siteId">Site Id</param>
        /// <param name="cultureCode">Culture code</param>
        /// <param name="deviceType">Device Type</param>
        /// <param name="tablet">Tablet?</param>
        /// <returns></returns>
        public static SiteEntity GetSite(int siteId, string cultureCode, DeviceType deviceType, bool tablet)
        {
            if (cultureCode.IsNullOrWhiteSpace())
            {
                throw new ObymobiException(SiteHelperResult.CultureCodeEmpty, "SiteId: {0}", siteId);
            }            

            // Create the prefetch path for the site
            PrefetchPath prefetchSite = new PrefetchPath(EntityType.SiteEntity);

            // Site > Media 
            // Site > Media > MediaRatioType
            prefetchSite.Add(SiteEntity.GetPrefetchForSiteMedia(deviceType));

            // Site > CustomTextCollection
            IPrefetchPathElement prefetchCustomTextCollection = SiteEntityBase.PrefetchPathCustomTextCollection;
            prefetchCustomTextCollection.Filter = new PredicateExpression(CustomTextFields.CultureCode == cultureCode);
            prefetchSite.Add(prefetchCustomTextCollection);

            // Site > Page
            // Site > Page > Media
            // Site > Page > Media > MediaRatioTypeMedia
            // Site > Page > CustomTextCollection
            // Site > Page > PageElement
            // Site > Page > PageElement > Media
            // Site > Page > PageElement > Media > MediaRatioTypeMedia
            // Site > Page > Attachment
            // Site > Page > Attachment > Media
            // Site > Page > Attachment > Media > MediaRatioTypeMedia
            // Site > Page > Attachment > CustomTextCollection
            // Site > Page > PageTemplate
            // Site > Page > PageTemplate > CustomTextCollection
            // Site > Page > PageTemplate > Media
            // Site > Page > PageTemplate > Media > MediaRatioTypeMedia            
            // Site > Page > PageTemplate > PageTemplateElement
            // Site > Page > PageTemplate > PageTemplateElement > Media
            // Site > Page > PageTemplate > PageTemplateElement > Media > MediaRatioTypeMedia
            // Site > Page > PageTemplate > Attachment
            // Site > Page > PageTemplate > Attachment > Media
            // Site > Page > PageTemplate > Attachment > Media > MediaRatioTypeMedia
            prefetchSite.Add(SiteEntity.GetPrefetchForSitePages(deviceType, cultureCode));

            SiteEntity site = new SiteEntity(siteId, prefetchSite);

            if (site.IsNew)
                throw new ObymobiException(SiteHelperResult.NoSiteIsExistingForSiteId, "SiteId: {0}", siteId);

            return site;
        }

        public static SiteCollection GetSitesAvailableToPointOfInterests(IncludeFieldsList includeFieldList)
        {
            SiteCollection sites = new SiteCollection();
            PredicateExpression filter = new PredicateExpression();
            filter.Add(SiteFields.CompanyId == DBNull.Value);
            SortExpression sort = new SortExpression();
            sort.Add(SiteFields.Name | SortOperator.Ascending);

            sites.GetMulti(filter, 0, sort, null, null, includeFieldList, 0, 0);

            return sites;
        }

        public static void QueueMedia(SiteEntity site)
        {
            MediaRatioTypeMediaCollection mrtms = new MediaRatioTypeMediaCollection();
            RelationCollection joins = new RelationCollection();
            joins.Add(MediaRatioTypeMediaEntity.Relations.MediaEntityUsingMediaId, JoinHint.Left);
            joins.Add(MediaEntity.Relations.PageEntityUsingPageId, "PageJoinedFromMedia", JoinHint.Left);            
            joins.Add(MediaEntity.Relations.PageElementEntityUsingPageElementId, JoinHint.Left);            
            joins.Add(PageElementEntity.Relations.PageEntityUsingPageId, "PageJoinedFromPageElement", JoinHint.Left);

            // These are prefetched as they are used in the MediaHelper
            PrefetchPath path = new PrefetchPath(EntityType.MediaRatioTypeMediaEntity);
            var mediaForMediaRatioTypeEntity = MediaRatioTypeMediaEntity.PrefetchPathMediaEntity;
            
            var pageForMediaEntityPath = MediaEntity.PrefetchPathPageEntity;
            pageForMediaEntityPath.SubPath.Add(PageEntity.PrefetchPathSiteEntity);

            var pageElementForMediaEntityPath = MediaEntity.PrefetchPathPageElementEntity;
            pageElementForMediaEntityPath.SubPath.Add(PageElementEntity.PrefetchPathPageEntity).SubPath.Add(PageEntity.PrefetchPathSiteEntity);

            mediaForMediaRatioTypeEntity.SubPath.Add(pageForMediaEntityPath);
            mediaForMediaRatioTypeEntity.SubPath.Add(pageElementForMediaEntityPath);

            PredicateExpression filter = new PredicateExpression();
            filter.Add(new FieldCompareValuePredicate(PageFields.SiteId, ComparisonOperator.Equal, site.SiteId, "PageJoinedFromMedia"));
            filter.AddWithOr(new FieldCompareValuePredicate(PageFields.SiteId, ComparisonOperator.Equal, site.SiteId, "PageJoinedFromPageElement"));

            mrtms.AddToTransaction(site);
            mrtms.GetMulti(filter, joins);

            foreach (var mrtm in mrtms)
            {
                MediaHelper.QueueMediaRatioTypeMediaFileTask(mrtm, MediaProcessingTaskEntity.ProcessingAction.Upload, true, null, site.GetCurrentTransaction());
            }            
        }

        public static SiteCollection GetSites(int companyId, bool includeGenericSites)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(SiteFields.CompanyId == companyId);

            SortExpression sort = new SortExpression(new SortClause(SiteFields.Name, SortOperator.Ascending));

            if (includeGenericSites)
            {
                filter.AddWithOr(SiteFields.CompanyId == DBNull.Value);
            }

            return SiteHelper.GetSites(filter, sort);
        }

        public static SiteCollection GetSites(PredicateExpression filter, SortExpression sort)
        {
            SiteCollection sites = new SiteCollection();
            sites.GetMulti(filter, 0, sort, null, null);

            return sites;
        }

        #endregion

        #region Copying

        public static SiteEntity CopySite(CopySiteRequest request)
        {
            // GK A Gentle Reminder - If you're changing this Copy method you probably also need to check the copy methods
            // on the SiteTemplateHelper.
            // Save the pages recursive
            PrefetchPath sitePath = new PrefetchPath(EntityType.SiteEntity);
            sitePath.Add(SiteEntityBase.PrefetchPathCustomTextCollection);
            sitePath.Add(SiteEntityBase.PrefetchPathSiteCultureCollection);

            IPrefetchPathElement mediaPathForPageElement = PageElementEntityBase.PrefetchPathMediaCollection;
            mediaPathForPageElement.SubPath.Add(MediaEntityBase.PrefetchPathMediaRatioTypeMediaCollection);

            IPrefetchPathElement pageElementPathForPage = PageEntityBase.PrefetchPathPageElementCollection;
            pageElementPathForPage.SubPath.Add(mediaPathForPageElement);            

            IPrefetchPathElement pagePathForSite = SiteEntityBase.PrefetchPathPageCollection;
            pagePathForSite.SubPath.Add(pageElementPathForPage);
            pagePathForSite.SubPath.Add(PageEntityBase.PrefetchPathCustomTextCollection);
            sitePath.Add(pagePathForSite);

            SiteEntity source = new SiteEntity(request.SiteId, sitePath);

            if (source.IsNew)
                throw new ObymobiException(SiteHelperResult.NoSiteIsExistingForSiteId, "SiteId: {0}", request.SiteId);

            Transaction transaction = new Transaction(System.Data.IsolationLevel.ReadUncommitted, "SiteHelper.CopySite-SiteId{0}-to-{1}".FormatSafe(source.SiteId, request.SiteName));
            SiteEntity target = null;
            try
            {
                target = new SiteEntity();
                target.AddToTransaction(transaction);
                target.CompanyId = source.CompanyId;
                target.Name = request.SiteName;
                target.Description = source.Description;
                target.SiteType = source.SiteType;
                target.SiteTemplateId = source.SiteTemplateId;
                target.Save();

                SiteHelper.CopyMedia(source.MediaCollection, MediaFields.SiteId, target.SiteId, transaction, request.CopyMediaOnCdn);

                target.PageCollection.AddRange(SiteHelper.CopyPages(source.PageCollection, target.SiteId, null, false, transaction, request.CopyMediaOnCdn));
                target.CustomTextCollection.AddRange(source.CustomTextCollection.Copy(CustomTextFields.SiteId));
                target.SiteCultureCollection.AddRange(SiteHelper.CopySiteCultures(source.SiteCultureCollection, SiteCultureFields.SiteId));

                target.Save(true);

                // Need to upload Media
                SiteHelper.QueueMedia(target);

                transaction.Commit();
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                throw new ObymobiException(SiteHelperResult.UnspecifiedEntitySaveException, ex);
            }
            finally
            {
                transaction.Dispose();
            }

            return target;
        }

        private static void CopyMedia(MediaCollection mediaCollection, EntityField field, int id, Transaction transaction, Func<MediaEntity, MediaEntity, bool> copyMediaOnCdn)
        {
            // Mapping old id to new entity
            Dictionary<int, MediaEntity> mappedMedia = new Dictionary<int, MediaEntity>();

            foreach (MediaEntity media in mediaCollection)
            {
                CopyMediaRequest request = new CopyMediaRequest
                {
                    MediaToCopy = media,
                    ReferenceField = field,
                    ReferenceValue = id,
                    CopyMediaOnCdn = copyMediaOnCdn,
                    Transaction = transaction
                };

                mappedMedia.Add(media.MediaId, MediaHelper.CopyMedia(request));
            }

            // Re-map agnostic media
            foreach (var mediaEntity in mappedMedia.Values)
            {
                if (mediaEntity.AgnosticMediaId.HasValue)
                {
                    MediaEntity agnosticMedia;
                    if (mappedMedia.TryGetValue(mediaEntity.AgnosticMediaId.Value, out agnosticMedia))
                    {
                        mediaEntity.AgnosticMediaId = agnosticMedia.MediaId;
                    }
                    else
                    {
                        mediaEntity.AgnosticMediaId = null;
                    }

                    mediaEntity.AddToTransaction(transaction);
                    mediaEntity.Save();
                }
            }
        }

        public static PageCollection CopyPages(PageCollection sources, int siteId, PageEntity parent, bool asRoot, Transaction transaction, Func<MediaEntity, MediaEntity, bool> copyMediaOnCdn)
        {
            // GK A Gentle Reminder - If you're changing this Copy method you probably also need to check the copy methods
            // on the SiteTemplateHelper.
            PageCollection targets = new PageCollection();

            foreach(PageEntity source in sources)
            {
                if (source.ParentPageId.HasValue && parent == null && !asRoot)
                    continue; // will be done recuserivaly.

                PageEntity target = new PageEntity();
                if (transaction != null) target.AddToTransaction(transaction);
                LLBLGenEntityUtil.CopyFields(source, target, null);                
                target.SiteId = siteId;
                target.PageTemplateId = source.PageTemplateId;
                target.Save();

                CopyMedia(source.MediaCollection, MediaFields.PageId, target.PageId, transaction, copyMediaOnCdn);
                
                foreach (PageElementEntity peSource in source.PageElementCollection)
                {
                    PageElementEntity peTarget = new PageElementEntity();
                    if (transaction != null) peTarget.AddToTransaction(transaction);
                    LLBLGenEntityUtil.CopyFields(peSource, peTarget);
                    peTarget.PageId = target.PageId;
                    peTarget.Save();

                    CopyMedia(peSource.MediaCollection, MediaFields.PageElementId, peTarget.PageElementId, transaction, copyMediaOnCdn);
                }

                target.CustomTextCollection.AddRange(source.CustomTextCollection.Copy(CustomTextFields.PageId));

                // Attachments 
                ArrayList attachmentFieldsNotToBeCopied = new ArrayList();
                attachmentFieldsNotToBeCopied.Add(AttachmentFields.PageId);
                foreach (AttachmentEntity atSource in source.AttachmentCollection)
                {
                    AttachmentEntity atTarget = new AttachmentEntity();
                    if (transaction != null) atTarget.AddToTransaction(transaction);
                    LLBLGenEntityUtil.CopyFields(atSource, atTarget, attachmentFieldsNotToBeCopied);
                    atTarget.CustomTextCollection.AddRange(atSource.CustomTextCollection.Copy(CustomTextFields.AttachmentId));
                    atTarget.PageId = target.PageId;
                    atTarget.Save(true);

                    // Attachment media
                    CopyMedia(atSource.MediaCollection, MediaFields.AttachmentId, atTarget.AttachmentId, transaction, copyMediaOnCdn);
                }

                if (source.PageCollection.Count > 0)
                {
                    SiteHelper.CopyPages(source.PageCollection, siteId, target, false, transaction, copyMediaOnCdn);
                }

                if (parent != null)
                    parent.PageCollection.Add(target);
                else
                    targets.Add(target);
            }

            return targets;
        }

        private static SiteCultureCollection CopySiteCultures(SiteCultureCollection oldSiteCultures, EntityField fieldToExclude)
        {
            ArrayList excludes = new ArrayList() {fieldToExclude};
            SiteCultureCollection newSiteCultureCollection = new SiteCultureCollection();
            foreach (SiteCultureEntity oldSiteCultureEntity in oldSiteCultures)
            {
                SiteCultureEntity newSiteCultureEntity = new SiteCultureEntity();
                LLBLGenEntityUtil.CopyFields(oldSiteCultureEntity, newSiteCultureEntity, excludes);
                newSiteCultureCollection.Add(newSiteCultureEntity);
            }
            return newSiteCultureCollection;
        }

        #endregion         

        #region Validation

        /// <summary>
        /// Determines which SiteTypes are compatible with the Site Template (this depends on the used Page Types)
        /// </summary>
        /// <returns></returns>
        public static List<SiteType> SiteTypesCompatibleWithSite(SiteEntity site)
        {
            List<SiteType> toReturn = new List<SiteType>();

            foreach (var siteType in EnumUtil.GetValues<SiteType>())
            {
                bool compatible = true;
                List<PageType> compatiblePageTypes = PageTypeHelper.GetPageTypes(siteType).Select(x => x.PageType).ToList();
                if (site.PageCollection != null)
                {
                    foreach (var page in site.PageCollection)
                    {
                        if (!compatiblePageTypes.Contains(page.PageType.ToEnum<PageType>()))
                        {
                            compatible = false;
                            break;
                        }
                    }
                }

                if (compatible)
                    toReturn.Add(siteType);
            }

            return toReturn;
        }

        /// <summary>
        /// Warning: Only the Name and CompanyId are loaded for the Company
        /// </summary>
        /// <param name="site"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public static CompanyCollection CompaniesUsingSiteAsUITab(SiteEntity site, ITransaction transaction, List<int> companyIds, bool loadAllCompanyFields = false)
        {
            RelationCollection joins = new RelationCollection();
            joins.Add(CompanyEntity.Relations.UIModeEntityUsingCompanyId);
            joins.Add(UIModeEntity.Relations.UITabEntityUsingUIModeId);

            PredicateExpression filter = new PredicateExpression();
            filter.Add(UITabFields.SiteId == site.SiteId);
            filter.Add(CompanyFields.CompanyId == companyIds);

            ExcludeIncludeFieldsList fieldList = new ExcludeFieldsList();
            if (!loadAllCompanyFields)
            {
                fieldList = new IncludeFieldsList();
                fieldList.Add(CompanyFields.Name);
            }

            CompanyCollection companies = new CompanyCollection();
            companies.AddToTransaction(transaction);            
            companies.GetMulti(filter, 0, null, joins, null, fieldList, 0, 0);

            return companies;
        }

        public static SiteEntity GetSite(int siteId, bool refreshCache)
        {
            string cacheKey = string.Format("Sites.{0}", siteId);

            SiteEntity site = null;
            if (refreshCache || !Dionysos.Web.CacheHelper.TryGetValue(cacheKey, false, out site))
            {
                PrefetchPath path = new PrefetchPath(EntityType.SiteEntity);                

                IPrefetchPathElement page1Prefetch = path.Add(SiteEntityBase.PrefetchPathPageCollection);
                IPrefetchPathElement pageTemplate1Prefetch = page1Prefetch.SubPath.Add(PageEntityBase.PrefetchPathPageTemplateEntity);

                IPrefetchPathElement page2Prefetch = page1Prefetch.SubPath.Add(PageEntityBase.PrefetchPathPageCollection);
                IPrefetchPathElement pageTemplate2Prefetch = page2Prefetch.SubPath.Add(PageEntityBase.PrefetchPathPageTemplateEntity);

                IPrefetchPathElement page3Prefetch = page2Prefetch.SubPath.Add(PageEntityBase.PrefetchPathPageCollection);
                IPrefetchPathElement pageTemplate3Prefetch = page3Prefetch.SubPath.Add(PageEntityBase.PrefetchPathPageTemplateEntity);

                IPrefetchPathElement page4Prefetch = page3Prefetch.SubPath.Add(PageEntityBase.PrefetchPathPageCollection);
                IPrefetchPathElement pageTemplate4Prefetch = page4Prefetch.SubPath.Add(PageEntityBase.PrefetchPathPageTemplateEntity);

                site = new SiteEntity(siteId, path);

                Dionysos.Web.CacheHelper.AddSlidingExpire(false, cacheKey, site, 60);
            }
            return site;
        }

        #endregion
    }
}
