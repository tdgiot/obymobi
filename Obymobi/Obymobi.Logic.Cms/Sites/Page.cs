﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace Obymobi.Logic.Cms
{
    public class Page
    {
        public Page()
        {
        }

        public Page(int parentPageId, int pageId, string name, int sortOrder)
        {
            this.Name = name;
            this.PageId = pageId;
            this.ParentPageId = parentPageId;
            //this.SubPages = new List<Page>();
            this.PageType = 2;
            this.SortOrder = sortOrder;
        }

        public int PageId { get; set; }
        public int ParentPageId { get; set; }
        public string Name { get; set; }
        public int PageType { get; set; }
        public int SortOrder { get; set; }


    }
}