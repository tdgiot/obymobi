﻿using Obymobi.Data;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Cms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

// http://stackoverflow.com/questions/17156991/gridview-with-objectdatasource-updatemethod
// http://msdn.microsoft.com/en-us/library/9a4kyhcx.aspx
// http://www.codeproject.com/Articles/667155/ObjectDataSource
// http://geekswithblogs.net/samerpaul/archive/2009/07/26/tutorial-why-the-objectdatasource-is-my-best-friend.aspx
namespace Obymobi.Logic.Cms
{
    public class PageDataSource : ObjectDataSource
    {
        private EntityType entityType;
        private int siteId;
        private List<IPage> datasource;

        private PageManager pageManager;

        public PageDataSource(EntityType entityType, int siteId, List<IPage> datasource)
        {
            switch (entityType)
            {
                case EntityType.PageEntity:
                    this.DataObjectTypeName = typeof(PageEntity).FullName;
                    break;
                case EntityType.PageTemplateEntity:
                    this.DataObjectTypeName = typeof(PageTemplateEntity).FullName;
                    break;
                default:
                    throw new NotImplementedException("PageDataSource can only work with PageTemplateEntity and PageEntity");
            }

            this.TypeName = typeof(PageManager).FullName;
            this.entityType = entityType;
            this.siteId = siteId;
            this.datasource = datasource;

            // Create interface to get sort of typed safety
            this.InsertMethod = "InsertPage";
            this.UpdateMethod = "UpdatePage";
            this.DeleteMethod = "DeletePage";
            this.SelectMethod = "GetPages";
            this.OldValuesParameterFormatString = "sheit_{0}";

            this.ObjectCreating += PageDataSource_ObjectCreating;
        }

        void PageDataSource_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            this.pageManager = new PageManager(this.entityType, this.siteId, this.datasource);
            e.ObjectInstance = this.pageManager;
        }

        public PageManager PageManager
        {
            get
            {
                return this.pageManager;
            }
        }
    }
}