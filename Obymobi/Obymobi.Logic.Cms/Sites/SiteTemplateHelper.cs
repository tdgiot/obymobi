﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;
using Dionysos.Data.LLBLGen;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data;
using Obymobi.Logic.Cms.Sites;
using Obymobi.Logic.Media;

namespace Obymobi.Logic.Cms
{
    public static class SiteTemplateHelper
    {
        public enum SiteTemplateHelperResult
        { 
            SiteTypesDontMatch = 200,
            SiteAlreadyContainsPages = 201,            
            UnspecifiedEntitySaveException = 202,
            YouNeedToSupplyEitherASiteOrSiteName = 203,
            NoSiteTemplateIsExistingForSiteTemplateId = 204
        }

        #region Copying

        /// <summary>
        /// Copies the PageTemplate structure to the Pages of the Site. 
        /// Throws an exception if the SiteTypes don't match or if the Site already has pages
        /// </summary>
        /// <param name="siteTemplate"></param>
        /// <param name="site"></param>
        /// <returns></returns>
        public static SiteEntity CopySiteTemplateToSite(CopySiteTemplateRequest request)
        {
            // GK A Gentle Reminder - If you're changing this Copy method you probably also need to check the copy methods
            // on the SiteHelper.

            SiteTemplateEntity siteTemplate = request.SiteTemplate;
            SiteEntity site = request.Site;

            if (site == null && request.SiteName.IsNullOrWhiteSpace())
                throw new ObymobiException(SiteTemplateHelperResult.YouNeedToSupplyEitherASiteOrSiteName);

            if (site == null && !request.SiteName.IsNullOrWhiteSpace())
            {
                site = new SiteEntity();
                site.SiteType =  siteTemplate.SiteType;
                site.SiteTemplateId = siteTemplate.SiteTemplateId;
                site.Name = request.SiteName;                
            }

            if (siteTemplate.SiteType != site.SiteType)
            {
                throw new ObymobiException(SiteTemplateHelperResult.SiteTypesDontMatch, "SiteTemplate '{0}' (Id:{1}): {2}, Site '{3}' (Id:{4}): {5}",
                    siteTemplate.Name, siteTemplate.SiteTemplateId, siteTemplate.SiteType.ToEnum<SiteType>(),
                    site.Name, site.SiteId, site.SiteType.ToEnum<SiteType>());
            }
            if (site.PageCollection.Count > 0)
            {
                throw new ObymobiException(SiteTemplateHelperResult.SiteAlreadyContainsPages, "Site '{0}' already has '{1}' pages.", site.Name, site.PageCollection.Count);
            }            

            // Save the pages recursive
            Transaction transaction = new Transaction(System.Data.IsolationLevel.ReadUncommitted, "SiteTemplateHelper.CopySiteTemplateToSite-{0}-to-{1}".FormatSafe(siteTemplate.SiteTemplateId, site.SiteId));
            try
            {
                // Custom texts
                if (site.SiteCultureCollection.Count <= 0)
                {
                    foreach (SiteTemplateCultureEntity siteTemplateCultureEntity in siteTemplate.SiteTemplateCultureCollection)
                    {
                        SiteCultureEntity siteCultureEntity = new SiteCultureEntity();
                        siteCultureEntity.CultureCode = siteTemplateCultureEntity.CultureCode;
                        site.SiteCultureCollection.Add(siteCultureEntity);
                    }
                }

                site.SiteTemplateId = siteTemplate.SiteTemplateId;
                site.AddToTransaction(transaction);
                site.Save(true);
                request.Site = site;

                // Copy Pages
                PageCollection pages = SiteTemplateHelper.CopyTemplatesPagesToPages(request, siteTemplate.PageTemplateCollection, null, transaction);
                foreach (PageEntity page in pages)
                {
                    page.AddToTransaction(transaction);
                    page.Save(true);
                }
                transaction.Commit();
            }
            catch(Exception ex)
            {
                transaction.Rollback();
                throw new ObymobiException(SiteTemplateHelperResult.UnspecifiedEntitySaveException, ex);
            }
            finally
            {
                transaction.Dispose();
            }

            return site;
        }

        private static void CopyMedia(MediaCollection mediaCollection, EntityField field, int id, Transaction transaction, Func<MediaEntity, MediaEntity, bool> copyMediaOnCdn)
        {
            // Mapping old id to new entity
            Dictionary<int, MediaEntity> mappedMedia = new Dictionary<int, MediaEntity>();

            foreach (MediaEntity media in mediaCollection)
            {
                CopyMediaRequest request = new CopyMediaRequest
                {
                    MediaToCopy = media,
                    ReferenceField = field,
                    ReferenceValue = id,
                    CopyMediaOnCdn = copyMediaOnCdn,
                    Transaction = transaction
                };

                mappedMedia.Add(media.MediaId, MediaHelper.CopyMedia(request));
            }

            // Re-map agnostic media
            foreach (var mediaEntity in mappedMedia.Values)
            {
                if (mediaEntity.AgnosticMediaId.HasValue)
                {
                    MediaEntity agnosticMedia;
                    if (mappedMedia.TryGetValue(mediaEntity.AgnosticMediaId.Value, out agnosticMedia))
                    {
                        mediaEntity.AgnosticMediaId = agnosticMedia.MediaId;
                    }
                    else
                    {
                        mediaEntity.AgnosticMediaId = null;
                    }

                    mediaEntity.AddToTransaction(transaction);
                    mediaEntity.Save();
                }
            }
        }

        private static PageCollection CopyTemplatesPagesToPages(CopySiteTemplateRequest copySiteTemplateRequest, PageTemplateCollection pageTemplates, PageEntity parentPage, Transaction transaction)
        {
            // GK A Gentle Reminder - If you're changing this Copy method you probably also need to check the copy methods
            // on the SiteHelper.
            PageCollection toReturn = new PageCollection();
            foreach (PageTemplateEntity pageTemplate in pageTemplates)
            {
                if (pageTemplate.ParentPageTemplateId.HasValue && parentPage == null)
                    continue; // Skip becasue will be done recursively later

                PageEntity page = new PageEntity();
                if (transaction != null) page.AddToTransaction(transaction);
                page.Name = pageTemplate.Name;
                page.SiteId = copySiteTemplateRequest.Site.SiteId;
                page.PageTemplateId = pageTemplate.PageTemplateId;
                page.PageType = pageTemplate.PageType;                
                page.SortOrder = pageTemplate.SortOrder;
                page.CustomTextCollection.AddRange(pageTemplate.CustomTextCollection.Copy(CustomTextFields.PageTemplateId));
                page.Save(true);

                // Page Elements
                foreach (PageTemplateElementEntity pageTemplateElement in pageTemplate.PageTemplateElementCollection)
                {
                    PageElementEntity pageElement = new PageElementEntity();
                    if (transaction != null) pageElement.AddToTransaction(transaction);
                    pageElement.PageId = page.PageId;
                    pageElement.LanguageId = pageTemplateElement.LanguageId;
                    pageElement.PageElementType = pageTemplateElement.PageElementType;
                    pageElement.SystemName = pageTemplateElement.SystemName;
                    pageElement.StringValue1 = pageTemplateElement.StringValue1;
                    pageElement.StringValue2 = pageTemplateElement.StringValue2;
                    pageElement.StringValue3 = pageTemplateElement.StringValue3;
                    pageElement.StringValue4 = pageTemplateElement.StringValue4;
                    pageElement.StringValue5 = pageTemplateElement.StringValue5;
                    pageElement.IntValue1 = pageTemplateElement.IntValue1;
                    pageElement.IntValue2 = pageTemplateElement.IntValue2;
                    pageElement.IntValue3 = pageTemplateElement.IntValue3;
                    pageElement.IntValue4 = pageTemplateElement.IntValue4;
                    pageElement.IntValue5 = pageTemplateElement.IntValue5;
                    pageElement.BoolValue1 = pageTemplateElement.BoolValue1;
                    pageElement.BoolValue2 = pageTemplateElement.BoolValue2;
                    pageElement.BoolValue3 = pageTemplateElement.BoolValue3;
                    pageElement.BoolValue4 = pageTemplateElement.BoolValue4;
                    pageElement.BoolValue5 = pageTemplateElement.BoolValue5;
                    pageElement.CultureCode = pageTemplateElement.CultureCode;
                    pageElement.Save();

                    // Page element media
                    CopyMedia(pageTemplateElement.MediaCollection, MediaFields.PageElementId, pageElement.PageElementId, transaction, copySiteTemplateRequest.CopyMediaOnCdn);
                }

                // Attachments
                if (copySiteTemplateRequest.CopyAttachments)
                {
                    foreach (AttachmentEntity templateAttachment in pageTemplate.AttachmentCollection)
                    {
                        AttachmentEntity pageAttachment = new AttachmentEntity();
                        if (transaction != null) pageAttachment.AddToTransaction(transaction);
                        LLBLGenEntityUtil.CopyFields(templateAttachment, pageAttachment, null);
                        pageAttachment.PageTemplateId = null;
                        pageAttachment.PageId = page.PageId;
                        pageAttachment.CustomTextCollection.AddRange(templateAttachment.CustomTextCollection.Copy(CustomTextFields.AttachmentId));
                        pageAttachment.Save(true);

                        // Attachment media
                        CopyMedia(templateAttachment.MediaCollection, MediaFields.AttachmentId, pageAttachment.AttachmentId, transaction, copySiteTemplateRequest.CopyMediaOnCdn);                   
                    }                    
                }
                
                // Media
                if (copySiteTemplateRequest.CopyMedia)
                {
                    CopyMedia(pageTemplate.MediaCollection, MediaFields.PageId, page.PageId, transaction, copySiteTemplateRequest.CopyMediaOnCdn);                   
                }

                if (pageTemplate.PageTemplateCollection.Count > 0)
                {
                    SiteTemplateHelper.CopyTemplatesPagesToPages(copySiteTemplateRequest, pageTemplate.PageTemplateCollection, page, transaction);
                }

                if (parentPage != null)
                    parentPage.PageCollection.Add(page);
                else
                    toReturn.Add(page);
            }
            return toReturn;
        }

        public static SiteTemplateEntity CreateSiteTemplateFromSite(this SiteEntity site, string templateName)
        {
            // GK A Gentle Reminder - If you're changing this Copy method you probably also need to check the copy methods
            // on the SiteHelper.
            // Save the pages recursive
            Transaction transaction = new Transaction(System.Data.IsolationLevel.ReadUncommitted, "SiteTemplateHelper.CreateSiteTemplateFromSite-SiteId{0}-to-{1}".FormatSafe(site.SiteId, templateName));
            SiteTemplateEntity template = null;
            try
            {
                template = new SiteTemplateEntity();
                template.AddToTransaction(transaction);                
                template.Name = templateName;
                template.SiteType = site.SiteType;
                template.Save();

                template.PageTemplateCollection.AddRange(SiteTemplateHelper.CopyPagesToTemplatePages(site.PageCollection, template.SiteTemplateId, null));
                template.SiteTemplateCultureCollection.AddRange(SiteTemplateHelper.CopySiteCulturesToSiteTemplateCultures(site.SiteCultureCollection, SiteCultureFields.SiteId));
                template.Save(true);

                transaction.Commit();
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                throw new ObymobiException(SiteTemplateHelperResult.UnspecifiedEntitySaveException, ex);
            }
            finally
            {
                transaction.Dispose();
            }

            return template;
        }

        private static SiteTemplateCultureCollection CopySiteCulturesToSiteTemplateCultures(SiteCultureCollection siteCultureCollection, EntityField fieldToExclude)
        {
            ArrayList excludes = new ArrayList() {fieldToExclude};
            SiteTemplateCultureCollection newSiteTemplateCultureCollection = new SiteTemplateCultureCollection();
            foreach (SiteCultureEntity siteCulture in siteCultureCollection)
            {
                SiteTemplateCultureEntity siteTemplateCultureEntity = new SiteTemplateCultureEntity();
                LLBLGenEntityUtil.CopyFields(siteCulture, siteTemplateCultureEntity, excludes);
                newSiteTemplateCultureCollection.Add(siteTemplateCultureEntity);
            }
            return newSiteTemplateCultureCollection;
        }

        private static PageTemplateCollection CopyPagesToTemplatePages(PageCollection pages, int siteTemplateId, PageTemplateEntity parentPage = null)
        {
            // GK A Gentle Reminder - If you're changing this Copy method you probably also need to check the copy methods
            // on the SiteHelper.
            PageTemplateCollection toReturn = new PageTemplateCollection();

            foreach (var page in pages)
            {
                if (page.ParentPageId.HasValue && parentPage == null)
                    continue; // Skip becasue will be done recursively later

                PageTemplateEntity pageTemplate = new PageTemplateEntity();
                pageTemplate.Name = page.Name;
                pageTemplate.SiteTemplateId = siteTemplateId;
                pageTemplate.PageType = page.PageType;
                pageTemplate.SortOrder = page.SortOrder;

                if (page.PageCollection.Count > 0)
                {
                    SiteTemplateHelper.CopyPagesToTemplatePages(page.PageCollection, siteTemplateId, pageTemplate);
                }

                ArrayList excludes = new ArrayList() {CustomTextFields.PageId};                
                foreach (CustomTextEntity oldCustomTextEntity in page.CustomTextCollection)
                {
                    CustomTextEntity newCustomTextEntity = new CustomTextEntity();
                    LLBLGenEntityUtil.CopyFields(oldCustomTextEntity, newCustomTextEntity, excludes);
                    pageTemplate.CustomTextCollection.Add(newCustomTextEntity);
                }

                if (parentPage != null)
                    parentPage.PageTemplateCollection.Add(pageTemplate);
                else
                    toReturn.Add(pageTemplate);
            }

            return toReturn;
        }

        public static PageElementEntity CreatePageElementEntityFromPageTemplateElementEntity(PageTemplateElementEntity pageTemplate, int pageId)
        {
            PageElementEntity pageElement = new PageElementEntity();
            pageElement.PageElementId = -1;
            pageElement.PageId = pageId;
            pageElement.LanguageId = pageTemplate.LanguageId;
            pageElement.SystemName = pageTemplate.SystemName;
            pageElement.PageElementType = pageTemplate.PageElementType;
            pageElement.StringValue1 = pageTemplate.StringValue1;
            pageElement.StringValue2 = pageTemplate.StringValue2;
            pageElement.StringValue3 = pageTemplate.StringValue3;
            pageElement.StringValue4 = pageTemplate.StringValue4;
            pageElement.StringValue5 = pageTemplate.StringValue5;
            pageElement.BoolValue1 = pageTemplate.BoolValue1;
            pageElement.BoolValue2 = pageTemplate.BoolValue2;
            pageElement.BoolValue3 = pageTemplate.BoolValue3;
            pageElement.BoolValue4 = pageTemplate.BoolValue4;
            pageElement.BoolValue5 = pageTemplate.BoolValue5;
            pageElement.IntValue1 = pageTemplate.IntValue1;
            pageElement.IntValue2 = pageTemplate.IntValue2;
            pageElement.IntValue3 = pageTemplate.IntValue3;
            pageElement.IntValue4 = pageTemplate.IntValue4;
            pageElement.IntValue5 = pageTemplate.IntValue5;
            pageElement.CultureCode = pageTemplate.CultureCode;

            // Media
            pageElement.MediaCollection.AddRange(pageTemplate.MediaCollection);

            return pageElement;          
        }

        #endregion

        #region Validation

        /// <summary>
        /// Determines which SiteTypes are compatible with the Site Template (this depends on the used Page Types)
        /// </summary>
        /// <returns></returns>
        public static List<SiteType> SiteTypesCompatibleWithTemplate(SiteTemplateEntity siteTemplate)
        {
            List<SiteType> toReturn = new List<SiteType>();

            foreach (var siteType in EnumUtil.GetValues<SiteType>())
            {
                bool compatible = true;
                List<PageType> compatiblePageTypes = PageTypeHelper.GetPageTypes(siteType).Select(x => x.PageType).ToList();
                if (siteTemplate.PageTemplateCollection != null)
                {
                    foreach (var pageTemplate in siteTemplate.PageTemplateCollection)
                    {
                        if (!compatiblePageTypes.Contains(pageTemplate.PageType.ToEnum<PageType>()))
                        {
                            compatible = false;
                            break;
                        }
                    }
                }

                if (compatible)
                    toReturn.Add(siteType);
            }

            return toReturn;
        }

        #endregion        
    }
}
