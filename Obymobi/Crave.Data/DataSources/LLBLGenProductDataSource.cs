﻿using System;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Crave.Data.DataSources
{
    [Serializable]
    public class LLBLGenProductDataSource
    {
        public ProductEntity GetProduct(int productId)
        {
            return new ProductEntity(productId);
        }

        public ProductCollection GetProductsByBrandProductId(int brandProductId)
        {
            PredicateExpression filter = new PredicateExpression
            {
                ProductFields.BrandProductId == brandProductId
            };

            return this.GetProductCollectionByFilter(filter);
        }

        private ProductCollection GetProductCollectionByFilter(PredicateExpression filter)
        {
            ProductCollection productCollection = new ProductCollection();
            productCollection.GetMulti(filter);

            return productCollection;
        }

        public bool Save(ProductEntity product, bool recursive)
        {
            return product.Save(recursive);
        }
    }
}
