﻿using System;
using Obymobi.Data.EntityClasses;

namespace Crave.Data.DataSources
{
    [Serializable]
    public class LLBLGenProductAlterationDataSource
    {
        public bool Save(ProductAlterationEntity productAlteration)
        {
            return productAlteration.Save();
        }
    }
}
