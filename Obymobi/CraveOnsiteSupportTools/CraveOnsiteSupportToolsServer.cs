﻿using Obymobi.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.ServiceProcess;
using System.Threading.Tasks;

namespace CraveOnsiteSupportTools
{
    public partial class CraveOnsiteSupportToolsServer : ServiceBase
    {
        private static ConsoleApplication application;
        private static Dictionary<string, object> arguments;

        public CraveOnsiteSupportToolsServer(Dictionary<string, object> args)
        {
            InitializeComponent();

            CraveOnsiteSupportToolsServer.arguments = args;
        }

        protected override void OnStart(string[] args)
        {
            ConsoleLogger.IsConsoleMode = false;
            ConsoleLogger.WriteToLog("Service - OnStart");

            string applicationPath = Assembly.GetExecutingAssembly().Location;
            applicationPath = Path.GetDirectoryName(applicationPath);
            CraveOnsiteSupportToolsServer.application = new ConsoleApplication(applicationPath, CraveOnsiteSupportToolsServer.arguments);

            Task.Factory.StartNew(() =>
            {
                try
                {
                    if (!CraveOnsiteSupportToolsServer.application.Start())
                    {
                        CraveOnsiteSupportToolsServer.application.Close();
                    }
                }
                catch (Exception ex)
                {
                    ConsoleLogger.WriteToLog("GLOBAL EXCEPTION: {0}\n\n{1}", ex.Message, ex.StackTrace);
                }
            });
        }

        protected override void OnStop()
        {
            ConsoleLogger.IsConsoleMode = false;
            ConsoleLogger.WriteToLog("Service - OnStop");

            try
            {
                ConsoleApplication.Instance.Close();
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("Error while closing application: {0}", ex.Message);
            }
        }
    }
}
