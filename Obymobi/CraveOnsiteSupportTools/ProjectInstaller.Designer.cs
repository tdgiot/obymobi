﻿namespace CraveOnsiteSupportTools
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CraveOnsiteSupportToolsProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.CraveOnsiteSupportToolsServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // CraveOnsiteSupportToolsProcessInstaller
            // 
            this.CraveOnsiteSupportToolsProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.CraveOnsiteSupportToolsProcessInstaller.Password = null;
            this.CraveOnsiteSupportToolsProcessInstaller.Username = null;
            // 
            // CraveOnsiteSupportToolsServiceInstaller
            // 
            this.CraveOnsiteSupportToolsServiceInstaller.Description = "A fallback line of communication with the Crave Cloud.";
            this.CraveOnsiteSupportToolsServiceInstaller.DisplayName = "Crave On-site SupportTools";
            this.CraveOnsiteSupportToolsServiceInstaller.ServiceName = "CraveOnsiteSupportTools";
            this.CraveOnsiteSupportToolsServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.CraveOnsiteSupportToolsProcessInstaller,
            this.CraveOnsiteSupportToolsServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller CraveOnsiteSupportToolsProcessInstaller;
        private System.ServiceProcess.ServiceInstaller CraveOnsiteSupportToolsServiceInstaller;
    }
}