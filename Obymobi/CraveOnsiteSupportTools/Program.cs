using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using Dionysos.Logging;
using Microsoft.Win32;

namespace CraveOnsiteSupportTools
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            // Begin with this
            Program.InitializeGlobals();

            // Parse any arguments passed
            Dictionary<string, object> args = Program.ParseConsoleArguments();

            string applicationPath = Assembly.GetExecutingAssembly().Location;
            applicationPath = Path.GetDirectoryName(applicationPath);

            if (Environment.CommandLine.Contains("-console"))
            {
                ConsoleApplication application = new ConsoleApplication(applicationPath, args);
                if (!application.Start())
                {
                    application.Close();
                }

                Console.WriteLine(@"-----------------------------------------");
                Console.WriteLine(@"Press enter to continue...");
                Console.ReadLine();
            }
            else
            {
                ServiceBase[] servicesToRun = new ServiceBase[]
                {
                    new CraveOnsiteSupportToolsServer(args)
                };
                ServiceBase.Run(servicesToRun);
            }            
        }

        private static Dictionary<string, object> ParseConsoleArguments()
        {
            Dictionary<string, object> arguments = new Dictionary<string, object>();
            arguments.Add("console", false);
            arguments.Add("webservice", "");

            string[] args = Environment.CommandLine.Split(' ');
            for (int i = 0; i < args.Length; i++)
            {
                string arg = args[i];

                if (arg == "-console")
                    arguments["console"] = true;
                else if (arg == "-webservice")
                {
                    if (++i < args.Length)
                        arguments["webservice"] = args[i];
                }
            }

            return arguments;
        }

        private static void InitializeGlobals()
        {
            // Set the application information
            Dionysos.Global.ApplicationInfo = new Dionysos.ServiceProcess.ServiceInfo();
            Dionysos.Global.ApplicationInfo.ApplicationName = "CraveOnsiteSupportTools";
            Dionysos.Global.ApplicationInfo.ApplicationVersion = "1.2019021901";
            Dionysos.Global.ApplicationInfo.BasePath = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            Dionysos.GlobalLight.LoggingProvider = new AsyncLoggingProvider(Path.Combine(Dionysos.Global.ApplicationInfo.BasePath, "Logs"), "CloudStatus", 14, "CraveOnsiteSupportTools");

            // Try to write the application details to the registry
            try
            {
                RegistryKey craveKey = Registry.LocalMachine.CreateSubKey(@"Software\Crave");
                using (RegistryKey reqsrvKey = craveKey.CreateSubKey(Dionysos.Global.ApplicationInfo.ApplicationName))
                {
                    reqsrvKey.SetValue("Name", Dionysos.Global.ApplicationInfo.ApplicationName);
                    reqsrvKey.SetValue("Version", Dionysos.Global.ApplicationInfo.ApplicationVersion);
                    reqsrvKey.SetValue("BasePath", Dionysos.Global.ApplicationInfo.BasePath);
                }
            }
            catch
            {
            }
        }
    }
}
