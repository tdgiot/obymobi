﻿using System;
using System.Collections.Generic;
using CraveOnsiteSupportTools.Logic.Commands;
using Dionysos;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using Obymobi.Logging;

namespace CraveOnsiteSupportTools.Logic
{
    public class DeviceCommands
    {
        #region Properties

        private static Dictionary<string, CommandBase> commands;

        #endregion

        #region Constructors

        public DeviceCommands()
        {
            this.Init();
        }

        #endregion

        #region Methods

        private void Init()
        {
            DeviceCommands.commands = new Dictionary<string, CommandBase>();
            DeviceCommands.commands.Add(CloseAppCommand.COMMAND, new CloseAppCommand());
            DeviceCommands.commands.Add(GetConfigCommand.COMMAND, new GetConfigCommand());
            DeviceCommands.commands.Add(GetLogCommand.COMMAND, new GetLogCommand());
            DeviceCommands.commands.Add(HelpCommand.COMMAND, new HelpCommand());
            DeviceCommands.commands.Add(SetConfigCommand.COMMAND, new SetConfigCommand());
            DeviceCommands.commands.Add(StartAppCommand.COMMAND, new StartAppCommand());
            DeviceCommands.commands.Add(GetFileCommand.COMMAND, new GetFileCommand());
            DeviceCommands.commands.Add(PutFileCommand.COMMAND, new PutFileCommand());
            DeviceCommands.commands.Add(UpdateCommand.COMMAND, new UpdateCommand());

#if !DISABLE_UNSAFE_COMMANDS
            DeviceCommands.commands.Add(RebootCommand.COMMAND, new RebootCommand());
            DeviceCommands.commands.Add(CmdCommand.COMMAND, new CmdCommand());
            DeviceCommands.commands.Add(ScreenshotCommand.COMMAND, new ScreenshotCommand());
#endif
        }

        public void Execute(string command)
        {
            command = command.Trim();
            string totalResult = string.Empty;

            if (command.Contains(" ** "))
            {
                string[] commands = command.Split(new string[] { "[**]" }, StringSplitOptions.None);                 
			    foreach (string cmd in commands)
			    {
				    if (cmd.Trim().Length > 0)
				    {
                        string tmp = DeviceCommands.ExecuteCommand(cmd.Trim());

					    if (!tmp.IsNullOrWhiteSpace() && !totalResult.IsNullOrWhiteSpace())
						    totalResult += "<br/>" + tmp;
					    else
						    totalResult += tmp;
				    }
			    }
		    }
		    else
		    {
                totalResult = DeviceCommands.ExecuteCommand(command);
		    }

		    if (!totalResult.IsNullOrWhiteSpace())
		    {
			    DeviceCommands.SendMessage(totalResult);
		    }
        }

        public static string ExecuteCommand(string command)
        {
            ConsoleLogger.WriteToLog("DeviceCommands - ExecuteCommand - Executing command: " + command);
            DeviceCommands.SendMessage("Executing command: " + command);

            string action = command;
            string args = "";
            int cmdEndIndex = command.IndexOf(" ", StringComparison.InvariantCultureIgnoreCase);
            if (cmdEndIndex > 0)
            {
                action = command.Substring(0, cmdEndIndex).Trim();
                args = command.Substring(cmdEndIndex, command.Length - cmdEndIndex).Trim();
            }

            action = action.ToLowerInvariant();
            if (DeviceCommands.commands.ContainsKey(action))
            {
                try
                {
                    CommandBase cmd = DeviceCommands.commands[action];
                    return cmd.Execute(args);
                }
                catch (Exception ex)
                {
                    ConsoleLogger.WriteToLog("DeviceCommands - ExecuteCommand - Error while executing command: {0}, Exception: {1}", command, ex.Message);
                    return string.Format("DeviceCommand - ExecuteCommand - Error while executing command: {0}, Exception: {1}", command, ex.Message);
                }
            }
            return "Empty result";
        }

        public static void SendMessage(string message)
        {
            Netmessage netmessage = new Netmessage();
            netmessage.SetMessageType(NetmessageType.AgentCommandResponse);
            netmessage.FieldValue1 = message;
            netmessage.FieldValue2 = Global.MacAddress;

            Server.Instance.SendMessage(netmessage);
        }

        #endregion
    }
}
