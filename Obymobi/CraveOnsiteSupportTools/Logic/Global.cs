﻿using System;
using CraveOnsiteSupportTools.Logic.Enum;

namespace CraveOnsiteSupportTools.Logic
{
	public class Global
	{
	    public static bool IsConsoleMode { get; set; }
        public static ServerState ServerState { get; set; }
        public static string RootDirectory { get; set; }
        public static Obymobi.Logic.Status.CloudStatus Status { get; set; }
        public static DateTime LastPollingUpdate { get; set; }

        // General
        public static string CompanyOwner { get; set; }
        public static string CompanyPassword { get; set; }        
        public static int TerminalId { get; set; }
        public static int CompanyId { get; set; }
        public static string MacAddress { get; set; }
        public static string Salt { get; set; }
        public static string SaltPms { get; set; }
        public static string CdnRootContainer { get; set; }
        public static int RequestInterval { get; set; }
        
        public static Dionysos.Logging.ILoggingProvider Logger { get; set; }                
	}
}
