﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CraveOnsiteSupportTools.Logic;
using CraveOnsiteSupportTools.Logic.Constants;
using Global = CraveOnsiteSupportTools.Logic.Global;

namespace CraveOnsiteSupportTools.Logic.Commands
{
    public class GetLogCommand : CommandBase
    {
        public const string COMMAND = "getlog";

        public override string Execute(string args)
        {
            this.Result = "OK";

            bool getAll = false;

            string serviceName = SupportToolsConstants.ONSITE_SERVER_SERVICE_NAME;            

		    string[] values = args.Split(' ');
		    bool getAppFolder = false;
		    foreach (string value in values)
		    {
			    if (value.Equals("-app", StringComparison.InvariantCultureIgnoreCase))
			    {
				    getAppFolder = true;
			    }
			    else if (value.Equals("-all", StringComparison.InvariantCultureIgnoreCase))
			    {
				    if (getAppFolder)
				    {
					    getAppFolder = false;
				    }
				    getAll = true;
			    }
			    else if (getAppFolder)
			    {
			        serviceName = value;			        
			    }
		    }

            Application application = SupportToolsConstants.GetInstalledApplications().SingleOrDefault(x => x.ServiceName.Equals(serviceName, StringComparison.InvariantCultureIgnoreCase));
            if (application == null)
            {
                this.Result = string.Format("Service '{0}' is not installed on this PC", serviceName);                
            }
            else
            {
                string path = string.Format(@"{0}{1}\", application.Path, "Logs");
                string[] files = Directory.GetFiles(path);

                List<string> filesToUpload = new List<string>();
                if (getAll)
                {
                    filesToUpload.AddRange(files);
                }
                else
                {
                    DateTime lastMod = DateTime.MinValue;
                    foreach (string file in files)
                    {
                        DateTime fileLastMod = File.GetLastWriteTime(file);
                        if (fileLastMod > lastMod && !file.StartsWith("WebservicePoller-", StringComparison.InvariantCultureIgnoreCase) && !file.StartsWith("CloudStatus-", StringComparison.InvariantCultureIgnoreCase))
                        {
                            lastMod = fileLastMod;
                            filesToUpload = new List<string>();
                            filesToUpload.Add(file);
                        }
                    }
                }

                if (filesToUpload.Count > 0)
                {
                    DeviceCommands.SendMessage("Number of files to upload: " + filesToUpload.Count);

                    string tempDirectory = Path.Combine(SupportToolsConstants.TEMP_DIR, application.Name);
                    if (!Directory.Exists(tempDirectory))
                    {
                        Directory.CreateDirectory(tempDirectory);
                    }

                    for (int i = 0; i < filesToUpload.Count; i++)
                    {
                        string file = filesToUpload[i];
                        string filename = Path.GetFileName(file);
                        string container = SupportToolsConstants.GetCompanyContainer();
                        string key = string.Format("logs/T-{0}/{1}", Global.TerminalId, filename);
                        string tmpFilepath = Path.Combine(tempDirectory, filename);

                        // Copy log file to a temp directory so we can read it while the supporttools can still write its log
                        using (FileStream fs = File.Open(file, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                        {
                            using (FileStream outputFile = new FileStream(tmpFilepath, FileMode.Create))
                            {
                                byte[] buffer = new byte[32768];
                                int bytes;
                                while ((bytes = fs.Read(buffer, 0, buffer.Length)) > 0)
                                {
                                    outputFile.Write(buffer, 0, bytes);
                                }
                            }
                        }

                        if (CdnManager.Instance.UploadSync(container, key, File.ReadAllText(tmpFilepath)))
                        {
                            List<string> urls = CdnManager.Instance.GetUrl(container, key);
                            foreach (string url in urls)
                            {
                                DeviceCommands.SendMessage(string.Format("{0}: {1} - {2}", (i + 1), key, url));
                            }
                        }
                    }
                }
            }

            return this.Result;
        }        
    }
}
