﻿using System;
using System.Drawing;
using System.Windows.Forms;
using CraveOnsiteSupportTools.Logic.Managers;
using CraveOnsiteSupportTools.Logic;

namespace CraveOnsiteSupportTools.Logic.Commands
{
    public class ScreenshotCommand : CommandBase
    {
        public const string COMMAND = "getscreenshot";

        public override string Execute(string args)
        {
            try
            {
                using (Bitmap bmpScreenCapture = new Bitmap(Screen.PrimaryScreen.Bounds.Width,
                                        Screen.PrimaryScreen.Bounds.Height))
                {
                    using (Graphics g = Graphics.FromImage(bmpScreenCapture))
                    {
                        g.CopyFromScreen(Screen.PrimaryScreen.Bounds.X,
                                            Screen.PrimaryScreen.Bounds.Y,
                                            0, 0,
                                            bmpScreenCapture.Size,
                                            CopyPixelOperation.SourceCopy);
                    }

                    ImageConverter converter = new ImageConverter();
                    byte[] file = (byte[])converter.ConvertTo(bmpScreenCapture, typeof(byte[]));
                    string filename = string.Format("screenshot_{0}_{1}.jpg", Global.TerminalId, DateTime.Now.Ticks);

                    if (WebserviceManager.UploadFile(Convert.ToBase64String(file), filename))
                    {
                        this.Result = "OK - " + Global.Status.WebserviceBaseUrl + "/Downloads/Devicemedia/" + filename;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Result = "Failure: " + ex.Message;
            }
            return this.Result;
        }
    }
}
