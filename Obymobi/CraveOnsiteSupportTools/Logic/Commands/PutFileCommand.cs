﻿using System;
using System.IO;
using System.Net;
using CraveOnsiteSupportTools.Logic.Constants;
using Dionysos;
using Dionysos.IO;
using Dionysos.Net;

namespace CraveOnsiteSupportTools.Logic.Commands
{
    public class PutFileCommand : CommandBase
    {
        public const string COMMAND = "putfile";

        public override string Execute(string args)
        {
            this.Result = "Empty";

            string[] values = args.Split(' ');
            if (values.Length == 0)
            {
                this.Result = "Missing parameter(s)";
            }
		    else
            {
                string downloadUrl = values[0].Trim();                
		        if (!downloadUrl.StartsWith("http"))
		        {
		            this.Result = "Supplied parameter (" + downloadUrl + ") is not an url.";
		        }
		        else
		        {
                    string filepath = StringUtil.CombineWithForwardSlash(SupportToolsConstants.INSTALL_DIR, "Downloads");

                    if (values.Length == 2)
                    {
                        filepath = values[1].Trim();
                    }

                    if (!filepath.Contains(".", StringComparison.InvariantCultureIgnoreCase))
                    {
                        filepath = StringUtil.CombineWithForwardSlash(filepath, Path.GetFileName(downloadUrl));
                        filepath = filepath.Replace("/", @"\");
                    }

		            string directoryName = Path.GetDirectoryName(filepath);
		            if (!Directory.Exists(directoryName))
		            {
                        Directory.CreateDirectory(directoryName);
		            }
		            
                    using (WebClient client = new WebClient())
                    {
                        client.DownloadFile(downloadUrl, filepath);
                    }

		            this.Result = string.Format("Downloaded file successfully to: {0}", filepath);
		        }
		    }
            return this.Result;
        }        
    }
}
