﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using CraveOnsiteSupportTools.Logic;
using CraveOnsiteSupportTools.Logic.Constants;
using CraveOnsiteSupportTools.Logic.Managers;

namespace CraveOnsiteSupportTools.Logic.Commands
{
    public class UpdateCommand : CommandBase
    {
        public const string COMMAND = "update";

        public override string Execute(string args)
        {
            this.Result = "Empty";

            string[] values = args.Split(' ');
            if (values.Length == 0)
            {
                this.Result = "Missing parameter(s)";
            }
		    else
            {
                string serviceName = values[0].Trim();

                Application application = SupportToolsConstants.GetInstalledApplications().SingleOrDefault(x => x.ServiceName.Equals(serviceName, StringComparison.InvariantCultureIgnoreCase));
                if (application == null)
                {
                    this.Result = string.Format("Service '{0}' isn't installed on this pc.", serviceName);
                }
                else if (application.ServiceName.Equals(SupportToolsConstants.ONSITE_SUPPORTTOOLS_SERVICE_NAME, StringComparison.InvariantCultureIgnoreCase))
                {
                    this.Result = string.Format("Service '{0}' can't update itself. This has to be done by the agent.", serviceName);
                }
                else
                {
                    int version;
                    string downloadLocation;
                    if (UpdateManager.IsUpdateAvailable(application, out version, out downloadLocation))
                    {
                        DeviceCommands.SendMessage(string.Format("Update available for application '{0}' from url '{1}'.", application.Name, downloadLocation));

                        string filepath;
                        if (UpdateManager.DownloadApplication(application, version.ToString(), downloadLocation, out filepath))
                        {
                            DeviceCommands.SendMessage(string.Format("Finished downloading update for application '{0}' from url '{1}'.", application.Name, downloadLocation));

                            if (UpdateManager.InstallApplication(application, filepath))
                            {
                                DeviceCommands.SendMessage(string.Format("Finished installating update for application '{0}'.", application.Name));
                            }
                        }
                    }
                }
            }
            return this.Result;
        }        
    }
}
