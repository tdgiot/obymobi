﻿using CraveOnsiteSupportTools.Logic.Managers;
using System;
using System.Linq;
using CraveOnsiteSupportTools.Logic;
using CraveOnsiteSupportTools.Logic.Constants;

namespace CraveOnsiteSupportTools.Logic.Commands
{
    public class CloseAppCommand : CommandBase
    {
        public const string COMMAND = "closeapp";

        public override string Execute(string args)
        {
            string[] values = args.Split(' ');
            if (values.Length == 0)
            {
                this.Result = "Missing parameter(s)";
            }
            else if (values.Length == 2 && !values[0].Trim().Equals("-r", StringComparison.InvariantCultureIgnoreCase))
            {
                this.Result = "Unknown parameter: " + values[0].Trim();
            }
            else
            {
                bool isRestart = false;
                string serviceName = values[0].Trim();

                if (values.Length == 2)
                {
                    isRestart = true;
                    serviceName = values[1].Trim();
                }

                Application application = SupportToolsConstants.GetInstalledApplications().SingleOrDefault(x => x.ServiceName.Equals(serviceName, StringComparison.InvariantCultureIgnoreCase));
                if (application != null)
                {
                    ServiceManager.Stop(application.ServiceName, true);
                    this.Result = string.Format("{0} stopped", serviceName);

                    if (isRestart)
                    {
                        ServiceManager.Start(application.ServiceName, true);
                        this.Result = string.Format("{0} restarted", serviceName);
                    }
                }                               
            }
            return this.Result;
        }
    }
}
