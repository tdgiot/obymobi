﻿namespace CraveOnsiteSupportTools.Logic.Commands
{
    public class RebootCommand : CommandBase
    {
        public const string COMMAND = "reboot";

        public override string Execute(string args)
        {
            this.Result = "OK";

            System.Diagnostics.Process.Start("shutdown.exe", "-r -t 0");

            return this.Result;
        }
    }
}