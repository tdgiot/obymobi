﻿using System.Threading.Tasks;
using CraveOnsiteSupportTools.Logic;
using Dionysos;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using Obymobi.Logging;

namespace CraveOnsiteSupportTools.Logic.Commands
{
    public abstract class CommandBase
    {
        #region Constructor

        protected CommandBase()
        {
            this.Result = string.Empty;
        }

        #endregion

        #region Methods

        public virtual string Execute(string args)
        {
            return this.Result;
        }

        public void OnComplete()
        {
            if (!this.Result.IsNullOrWhiteSpace())
            {
                ConsoleLogger.WriteToLog("Sending command result...");

                Netmessage netmessage = new Netmessage();
                netmessage.SetMessageType(NetmessageType.AgentCommandResponse);
                netmessage.FieldValue1 = this.Result;

                Server.Instance.SendMessage(netmessage);
            }
        }

        #endregion

        #region Properties

        public string Result { get; set; }

        #endregion
    }
}
