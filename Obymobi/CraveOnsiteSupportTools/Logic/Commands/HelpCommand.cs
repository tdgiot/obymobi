﻿namespace CraveOnsiteSupportTools.Logic.Commands
{
    public class HelpCommand : CommandBase
    {
        public const string COMMAND = "help";

        public HelpCommand()
        {
            this.Result = "List of available commands:<br/>" +
                "* help - Show this list.<br/>" +
                "* reboot - Restart the whole device.<br/>" +
                "* cmd [command] - Execute a command on the command line.<br/>" +
                "* getconfig <-app CraveOnsiteServer|CraveOnsiteAgent|CraveOnsiteSupportTools> - Gets the config of the application.<br/>" +
                "* setconfig [url] <-app CraveOnsiteServer|CraveOnsiteAgent|CraveOnsiteSupportTools> - Downloads the configuration from specified url and replaces the config file for the service.<br/>" +                
                "* putfile [url] &lt;destination> - Downloads a file from specified url. If no destination is given the default will be used (C://Program Files/Crave/Downloads/).<br/>" +
                "* getfile [path] - Uploads a file to the webservice. Returns URL of that file for download.<br/>" +                
                "* startapp [servicename] - Starts a service using service name.<br/>" +
                "* closeapp <-r> [servicename] - Closes a service application using service name. Use the -r flag to restart the service.<br/>" +
                "* getlog <-all> <-app CraveOnsiteServer|CraveOnsiteAgent|CraveOnsiteSupportTools> - Gets the latest log. Use the -all flag to get all available logs.<br/>" +
                "* update [CraveOnsiteServer|CraveOnsiteAgent] - Updates or installs the specified application.<br/>" +
                "<br/>To chain multiple commands use double star ( ** ).";
        }
    }
}
