﻿using System;
using System.ServiceProcess;

namespace CraveOnsiteSupportTools.Logic.Commands
{
    public class StartAppCommand : CommandBase
    {
        public const string COMMAND = "startapp";

        public override string Execute(string args)
        {
            string[] values = args.Split(' ');
            if (values.Length < 1)
            {
                this.Result = "Missing parameter(s)";
            }            
            else
            {
                string serviceName = values[0].Trim();

                ServiceController sc = new ServiceController(serviceName);
                if (sc.Status != ServiceControllerStatus.Running)
                {
                    sc.Start();
                    sc.WaitForStatus(ServiceControllerStatus.Running, new TimeSpan(0, 0, 30));
                    this.Result = string.Format("{0} running", serviceName);
                }
                else
                {
                    this.Result = string.Format("{0} was already running", serviceName);
                }
            }
            return this.Result;
        }
    }
}
