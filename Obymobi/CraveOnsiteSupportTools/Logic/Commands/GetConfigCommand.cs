﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CraveOnsiteSupportTools.Logic.Constants;
using CraveOnsiteSupportTools.Logic.Managers;
using Global = CraveOnsiteSupportTools.Logic.Global;
using CraveOnsiteSupportTools.Logic;

namespace CraveOnsiteSupportTools.Logic.Commands
{
    public class GetConfigCommand : CommandBase
    {
        public const string COMMAND = "getconfig";

        public override string Execute(string args)
        {
            string serviceName = SupportToolsConstants.ONSITE_SERVER_SERVICE_NAME;

            string[] values = args.Split(' ');
            bool getAppFolder = false;
            foreach (string value in values)
            {
                if (value.Equals("-app", StringComparison.InvariantCultureIgnoreCase))
                {
                    getAppFolder = true;
                }
                else if (getAppFolder)
                {
                    serviceName = value;
                }
            }

            Application application = SupportToolsConstants.GetInstalledApplications().SingleOrDefault(x => x.ServiceName.Equals(serviceName, StringComparison.InvariantCultureIgnoreCase));
            if (application != null)
            {
                string filename = string.Format("{0}.config", serviceName);
                string filepath = string.Format(@"{0}{1}", application.Path, filename);

                if (File.Exists(filepath))
                {
                    string container = SupportToolsConstants.GetCompanyContainer();
                    string key = string.Format("temp/T-{0}/{1}", Global.TerminalId, filename);

                    if (CdnManager.Instance.UploadSync(container, key, File.ReadAllText(filepath)))
                    {
                        List<string> urls = CdnManager.Instance.GetUrl(container, key);
                        foreach (string url in urls)
                        {
                            DeviceCommands.SendMessage(string.Format("Uploaded config to CDN: {0} - {1}", key, url));
                        }
                    }
                }
                else
                {
                    this.Result = string.Format("No config found for service: {0}", serviceName);
                }
            }
            return this.Result;
        }         
    }
}
