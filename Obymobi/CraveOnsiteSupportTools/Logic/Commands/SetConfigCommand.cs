﻿using System;
using System.IO;
using System.Linq;
using CraveOnsiteSupportTools.Logic;
using CraveOnsiteSupportTools.Logic.Constants;
using Dionysos;
using Dionysos.IO;
using Dionysos.Net;

namespace CraveOnsiteSupportTools.Logic.Commands
{
    public class SetConfigCommand : CommandBase
    {
        public const string COMMAND = "setconfig";

        public override string Execute(string args)
        {
            this.Result = "OK";

            string[] values = args.Split(' ');
            if (values.Length == 0)
            {
                this.Result = "Missing parameter(s)";
            }
		    else
		    {
                string serviceName = SupportToolsConstants.ONSITE_SERVER_SERVICE_NAME;
                bool getAppFolder = false;
                foreach (string value in values)
                {
                    if (value.Equals("-app", StringComparison.InvariantCultureIgnoreCase))
                    {
                        getAppFolder = true;
                    }
                    else if (getAppFolder)
                    {
                        serviceName = value;
                    }
                }

                Application application = SupportToolsConstants.GetInstalledApplications().SingleOrDefault(x => x.ServiceName.Equals(serviceName, StringComparison.InvariantCultureIgnoreCase));
		        if (application != null)
		        {
		            string filename = string.Format(@"{0}{1}.config", application.Path, application.ServiceName);

		            string downloadUrl = values[0].Trim();
		            if (!downloadUrl.StartsWith("http"))
		            {
		                this.Result = "Supplied parameter (" + downloadUrl + ") is not an url.";
		            }
		            else
		            {
		                string content = HttpRequestHelper.GetResponseAsString(downloadUrl);
		                if (!content.IsNullOrWhiteSpace())
		                {
		                    File.WriteAllText(filename, content);
		                    this.Result = string.Format("Config updated for: {0}", serviceName);
		                }
		            }
		        }
		    }
            return this.Result;
        }        
    }
}
