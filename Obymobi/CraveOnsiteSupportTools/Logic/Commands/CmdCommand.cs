﻿using System;
using System.Diagnostics;

namespace CraveOnsiteSupportTools.Logic.Commands
{
    public class CmdCommand : CommandBase
    {
        public const string COMMAND = "cmd";

        public override string Execute(string args)
        {
            Process process = null;
            try
            {
                ProcessStartInfo startInfo = new ProcessStartInfo("cmd", "/c " + args) {WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden, UseShellExecute = false, RedirectStandardOutput = true, CreateNoWindow = true};

                process = Process.Start(startInfo);                
                this.Result = process.StandardOutput.ReadToEnd();
            }
            catch (Exception ex)
            {
                this.Result = "Failure: " + ex.Message;
            }
            finally
            {
                if (process != null)
                {
                    process.Close();
                }                
            }
            return this.Result;
        }
    }
}
