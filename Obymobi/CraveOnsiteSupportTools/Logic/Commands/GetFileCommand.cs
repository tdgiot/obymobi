﻿using System;
using System.IO;
using CraveOnsiteSupportTools.Logic.Managers;
using Global = CraveOnsiteSupportTools.Logic.Global;

namespace CraveOnsiteSupportTools.Logic.Commands
{
    public class GetFileCommand : CommandBase
    {
        public const string COMMAND = "getfile";

        public override string Execute(string args)
        {
            if (args.Length == 0)
            {
                this.Result = "Missing parameter(s)";
            }
            else
            {
                string filepath = args.Trim();
                if (File.Exists(filepath))
                {
                    string filename = string.Format("{0}_{1}{2}", Path.GetFileNameWithoutExtension(filepath), DateTime.Now.Ticks, Path.GetExtension(filepath));
                    byte[] file = File.ReadAllBytes(filepath);

                    if (WebserviceManager.UploadFile(Convert.ToBase64String(file), filename))
                    {
                        this.Result = "OK - " + Global.Status.WebserviceBaseUrl + "/Downloads/Devicemedia/" + filename;
                    }
                }
            }
            return this.Result;
        }         
    }
}
