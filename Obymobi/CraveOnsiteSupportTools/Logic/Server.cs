﻿using System;
using System.Threading;
using CraveOnsiteServer.Net;
using CraveOnsiteSupportTools.Comet;
using CraveOnsiteSupportTools.Logic.Constants;
using CraveOnsiteSupportTools.Logic.Enum;
using CraveOnsiteSupportTools.Logic.Managers;
using CraveOnsiteSupportTools.Logic.Queue;
using Dionysos;
using Dionysos.Net;
using Obymobi.Constants;
using Obymobi.Logic.Model;
using Obymobi.Enums;
using Global = CraveOnsiteSupportTools.Logic.Global;
using Obymobi.Logging;

namespace CraveOnsiteSupportTools.Logic
{
    public class Server
    {
        #region Fields

        private static Server instance;
        private static readonly CountdownEvent Countdown = new CountdownEvent(1);
        private ConnectionSwitcher connectionSwitcher;
        
        public bool IsRestart { get; set; }

        #endregion

        #region Constructor

        private Server()
        {
            this.IsRestart = false;
        }        

        public static Server Instance
        {
            get { return Server.instance ?? (Server.instance = new Server()); }
        }

        public static void Reset()
        {
            Server.instance = null;
        }

        #endregion

        #region Events

        private void Status_WebserviceUrlChanged(object sender, EventArgs e)
        {
            WebserviceManager.Instance.Webservice.Url = Global.Status.CraveServiceUrl;
        }

        private void Status_Logged(object sender, LoggingLevel level, string log)
        {
            ConsoleLogger.WriteToLog(log);
        }

        #endregion

        #region Server/Client Control

        public bool StartServer()
        {
            ConsoleLogger.WriteToLog("** STARTING SERVER **");

            Server.Countdown.Reset();

            // Start Comet Connection
            this.connectionSwitcher = new ConnectionSwitcher();
            this.connectionSwitcher.OnLog += message => ConsoleLogger.WriteToLog(message, LoggingLevel.Verbose);
			this.connectionSwitcher.Initialize();

            Global.Status.WebserviceUrlChanged += this.Status_WebserviceUrlChanged;
            Global.Status.Logged += this.Status_Logged;

            // Communicate the support tools version back to the backend
            WebserviceManager.SetApplicationVersion(ApplicationCode.SupportTools, Dionysos.Global.ApplicationInfo.ApplicationVersion);

            // Create a log message that the terminal has been started
            WriteToLog log = new WriteToLog
            {
                Message = String.Format("CraveOnsiteSupportTools {0} ({1}) started\r\n", Global.TerminalId, NetUtil.GetMachineName()),
                TerminalLogType = (int)TerminalLogType.Start
            };

            log.Log += string.Format("Machine Name:\t{0}\r\n", NetUtil.GetMachineName());
            log.Log += string.Format("Local IP(s):\t{0}\r\n", NetUtil.GetLocalIpsJoined());
            log.Log += string.Format("Remote IP:\t{0}\r\n", NetUtil.GetRemoteIp());            

            WebserviceManager.WriteToLog(log);

            // Block until singnal has been called
            Server.Countdown.Wait();

            ConsoleLogger.WriteToLog("***** SERVER CLOSED ******");

            Global.ServerState = ServerState.OFFLINE;

            if (this.IsRestart)
            {
                ConsoleApplication.Instance.Restart();
            }

            return true;
        }

        public void StopServer()
        {
            ConsoleLogger.WriteToLog("** STOPPING SERVER **");

            // Write to log
            string message;
            if (this.IsRestart)
                message = String.Format("Terminal {0} ({1}) is restarting\r\n", Global.TerminalId, NetUtil.GetMachineName());
            else
                message = String.Format("Terminal {0} ({1}) stopped\r\n", Global.TerminalId, NetUtil.GetMachineName());                

            WriteToLog log = new WriteToLog
            {
                Message = message,                
                TerminalLogType = (int)TerminalLogType.Stop
            };

            // Write the log message to the webservice
            WebserviceManager.WriteToLog(log);

            // Stopping ConnectionSwitcher
            ConsoleLogger.WriteToLog("Stopping Connection Switcher");

            if (this.connectionSwitcher != null)
            {
                this.connectionSwitcher.Stop();                
            }

            // Signal blocking to continue
            Server.Countdown.Signal();
        }

        public void RestartServer()
        {
            this.IsRestart = true;
            this.StopServer();
        }

        public void SendMessage(Netmessage netmessage)
        {
            if (this.connectionSwitcher != null)
            {
                IConnectionHandler handler;
                if (this.connectionSwitcher.TryGetHandler(ClientCommunicationMethod.SignalR, out handler))
                {
                    ((CometHandler)handler).SendMessage(netmessage);
                }
            }            
        }

        public void ReloadData()
        {
            ConsoleLogger.WriteToLog(">>>>>>>>>> RELOADING MODE - BEGIN <<<<<<<<<<");
            ConsoleLogger.WriteToLog("Note: Server is not accepting any new connections while reloading!!");
            Global.ServerState = ServerState.RELOADING;

            ConsoleLogger.WriteToLog("- Checking internet & webservice connection.");
            Global.Status.Refresh();
            if (Global.Status.IsWebserviceAvailable)
            {
                ConsoleLogger.WriteToLog("Webservice available.");
            }
            else
            {
                ConsoleLogger.WriteToLog("Reloading failed. Could not connect to the webservice.");
            }

            ConsoleLogger.WriteToLog(">>>>>>>>>> RELOADING MODE - END <<<<<<<<<<");

            Global.ServerState = ServerState.ONLINE;
        }

        #endregion        
    }
}
