﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CraveOnsiteSupportTools.Logic.Constants;
using Dionysos;
using Microsoft.Win32;
using System.IO;
using System.Net;
using Dionysos.IO;
using System.Threading;

namespace CraveOnsiteSupportTools.Logic.Managers
{
    public static class UpdateManager
    {
        public static bool IsUpdateAvailable(Application application, out int latestVersion, out string downloadLocation)
        {
            bool toReturn = false;

            string tmpDownloadLocation = string.Empty;
            string tmpLatestVersion = string.Empty;

            string versionAndDownloadLocation = WebserviceManager.GetReleaseAndDownloadLocation(application.Name);
            if (versionAndDownloadLocation.IsNullOrWhiteSpace())
            {
                DeviceCommands.SendMessage(string.Format("GetReleaseAndDownloadLocation returned an empty result for application: " + application.Name));
            }
            else if (!versionAndDownloadLocation.Contains("|"))
            {
                DeviceCommands.SendMessage(string.Format("Download location doesn't contain both a version and a location: " + versionAndDownloadLocation));
            }
            else
            {
                versionAndDownloadLocation = versionAndDownloadLocation.StartsWith("[") ? versionAndDownloadLocation.Substring(1) : versionAndDownloadLocation;
                versionAndDownloadLocation = versionAndDownloadLocation.EndsWith("]") ? versionAndDownloadLocation.Substring(0, versionAndDownloadLocation.Length - 1) : versionAndDownloadLocation;

                string[] parts = versionAndDownloadLocation.Split('|');
                string version = parts[0];
                string location = parts[1];

                string installedVersion = UpdateManager.GetInstalledVersion(application);
                 
                int installedVersionInt = 0;
                int latestVersionInt = 0;

                if (!int.TryParse(version, out latestVersionInt))
                {
                    DeviceCommands.SendMessage(string.Format("Latest version '{0}' for application '{1}' could not be parsed to an integer!", version, application.Name));
                }
                else if (!installedVersion.IsNullOrWhiteSpace() && !int.TryParse(installedVersion, out installedVersionInt))
                {
                    DeviceCommands.SendMessage(string.Format("Installed version '{0}' for application '{1}' could not be parsed to an integer!", installedVersion, application.Name));
                }
                else if (!installedVersion.IsNullOrWhiteSpace() && latestVersionInt <= installedVersionInt)
                {
                    DeviceCommands.SendMessage(string.Format("Installed version '{0}' for application '{2}' is up-to-date compared to latest version '{1}'.", installedVersion, version, application.Name));
                }
                else
                {
                    tmpDownloadLocation = location;
                    tmpLatestVersion = version;
                    toReturn = true;
                }
            }

            downloadLocation = tmpDownloadLocation;
            latestVersion = tmpLatestVersion.IsNullOrWhiteSpace() ? 0 : int.Parse(tmpLatestVersion);

            return toReturn;
        }

        public static bool DownloadApplication(Application application, string version, string downloadLocation, out string filepath)
        {
            bool success = false;
            string tmpFilepath = string.Empty;

            foreach (string cdnUrl in Global.Status.CdnBaseUrls)
            {
                if (!success)
                {
                    string baseUrl = cdnUrl.EndsWith("/") ? cdnUrl : cdnUrl + "/";
                    string url = baseUrl + downloadLocation;

                    string applicationDirectory = Path.Combine(SupportToolsConstants.UPDATES_DIR, application.Name);
                    if (!Directory.Exists(applicationDirectory))
                    {
                        Directory.CreateDirectory(applicationDirectory);
                    }

                    string versionDirectory = Path.Combine(applicationDirectory, version);
                    if (!Directory.Exists(versionDirectory))
                    {
                        Directory.CreateDirectory(versionDirectory);
                    }

                    string cdnFilepath = Path.Combine(versionDirectory, SupportToolsConstants.UPDATE_FILENAME);
                    if (File.Exists(cdnFilepath))
                    {
                        File.Delete(cdnFilepath);
                    }
                    else
                    {
                        DeviceCommands.SendMessage(string.Format(@"Downloading update for application '{0}' from url '{1}'.", application.Name, url));

                        using (WebClient client = new WebClient())
                        {
                            client.DownloadFile(url, cdnFilepath);
                        }
                        tmpFilepath = cdnFilepath;
                        success = true;
                    }            
                }
            }
            filepath = tmpFilepath;
            return success;
        }

        public static bool InstallApplication(Application application, string filepath)
        {
            bool result = false;
            if (!ServiceManager.Stop(application.ServiceName, true))
            {
                DeviceCommands.SendMessage(string.Format("Failed to stop service '{0}' within 60 seconds.", application.ServiceName));
            }
            else if (!File.Exists(filepath))
            {
                DeviceCommands.SendMessage(string.Format("Update for service '{0}' doesn't exist on path '{1}'.", application.ServiceName, filepath));
            }
            else if (!ZipUtil.Unzip(filepath, application.Path, true))
            {
                DeviceCommands.SendMessage(string.Format("Update for service '{0}' failed to unzip for file '{1}'.", application.ServiceName, filepath));
            }
            else
            {
                string postUpdateBatchFile = Path.Combine(application.Path, "postupdate.bat");
                if (File.Exists(postUpdateBatchFile))
                {
                    DeviceCommands.SendMessage(string.Format("Starting post-update batch file '{0}'.", postUpdateBatchFile));

                    Process.Start(postUpdateBatchFile);
                    Thread.Sleep(5000);
                }

                if (!ServiceManager.Start(application.ServiceName, true))
                {
                    DeviceCommands.SendMessage(string.Format("Failed to start service '{0}'.", application.ServiceName));
                }
                else
                {
                    DeviceCommands.SendMessage(string.Format("Service '{0}' started.", application.ServiceName));
                    result = true;
                }
            }
            return result;
        }

        private static string GetInstalledVersion(Application application)
        {
            string version = string.Empty;

            try
            {
                RegistryKey craveKey = Registry.LocalMachine.CreateSubKey(@"Software\Crave");
                if (craveKey != null)
                {
                    string subKeyName = craveKey.GetSubKeyNames().SingleOrDefault(x => x.Equals(application.Name, StringComparison.InvariantCultureIgnoreCase));
                    if (!subKeyName.IsNullOrWhiteSpace())
                    {
                        using (RegistryKey appKey = craveKey.OpenSubKey(subKeyName))
                        {
                            if (appKey != null)
                            {
                                foreach (string valueName in appKey.GetValueNames())
                                {
                                    if (valueName.Equals("Version", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        version = appKey.GetValue(valueName).ToString();
                                        if (version.StartsWith("1."))
                                            version = version.Substring(2);

                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                DeviceCommands.SendMessage(string.Format("GetInstalledVersion - Exception: {0}", ex.Message));
            }

            return version;
        }
    }
}
