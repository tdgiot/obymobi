﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dionysos;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Obymobi.Security;
using Obymobi.Logic.Model;
using Obymobi.Logging;

namespace CraveOnsiteSupportTools.Logic.Managers
{
    public class WebserviceManager
    {
        #region Fields

        private static WebserviceManager sInstance;

        public CraveService.CraveService Webservice { get; private set; }

        #endregion

        #region Constructor

        private WebserviceManager()
        {
            try
            {
                this.Webservice = new CraveService.CraveService();
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("ERROR: Exception while creating new WebserviceManager instance: {0}\n\n{1}", ex.Message, ex.StackTrace);
            }
        }

        public static WebserviceManager Instance
        {
            get { return WebserviceManager.sInstance ?? (WebserviceManager.sInstance = new WebserviceManager()); }
        }

        #endregion

        #region Methods

        public void InitializeWebservice()
        {
            this.Webservice = new CraveService.CraveService
            {
                Url = Global.Status.CraveServiceUrl,
                EnableDecompression = true
            };
        }

        public void Reset()
        {
            WebserviceManager.sInstance = null;
        }

        public static bool VerifyNetmessage(string netmessageGuid)
        {
            if (netmessageGuid.IsNullOrWhiteSpace())
                return true;

            string methodCall = string.Empty;
            bool result = false;

            try
            {
                string hash;
                long timestamp = WebserviceManager.GetTimeStampAndHash(out hash, Global.MacAddress, netmessageGuid);

                methodCall = "WebserviceManager.VerifyNetmessageByGuid({0}, {1}, {2}, {3})".FormatSafe(timestamp, Global.MacAddress, netmessageGuid, string.Empty, hash);

                CraveService.ObyTypedResultOfSimpleWebserviceResult wsResult = WebserviceManager.Instance.Webservice.VerifyNetmessageByGuid(timestamp, Global.MacAddress, netmessageGuid, string.Empty, hash);

                if (wsResult == null)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result is NULL", methodCall);
                }
                else if (wsResult.ResultCode != (int)GenericWebserviceCallResult.Success)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result: {1} - {2}", methodCall, wsResult.ResultCode, wsResult.ResultMessage);
                }
                else
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("ERROR: {0} Exception: {1} - {2}", methodCall, ex.Message, (ex.InnerException != null) ? ex.InnerException.Message : "");
            }

            return result;
        }

        public static bool CompleteNetmessage(string netmessageGuid)
        {
            string methodCall = string.Empty;
            bool result = false;

            try
            {
                string hash;
                long timestamp = WebserviceManager.GetTimeStampAndHash(out hash, Global.MacAddress, netmessageGuid);

                string errorMessage = string.Empty;
                methodCall = "WebserviceManager.CompleteNetmessageByGuid({0}, {1}, {2}, {3})".FormatSafe(timestamp, Global.MacAddress, netmessageGuid, errorMessage, string.Empty, hash);

                CraveService.ObyTypedResultOfSimpleWebserviceResult wsResult = WebserviceManager.Instance.Webservice.CompleteNetmessageByGuid(timestamp, Global.MacAddress, netmessageGuid, errorMessage, string.Empty, hash);

                if (wsResult == null)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result is NULL", methodCall);
                }
                else if (wsResult.ResultCode != (int)GenericWebserviceCallResult.Success)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result: {1} - {2}", methodCall, wsResult.ResultCode, wsResult.ResultMessage);
                }
                else
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("ERROR: {0} Exception: {1} - {2}", methodCall, ex.Message, (ex.InnerException != null) ? ex.InnerException.Message : "");
            }

            return result;
        }

        public static Company GetCompany()
        {
            Company toReturn = null;
            string methodCall = string.Empty;
            try
            {
                string hash;
                long timestamp = WebserviceManager.GetTimeStampAndHash(out hash, Global.MacAddress, Global.CompanyId);

                methodCall = "WebserviceManager.GetCompany({0}, {1}, {2}, {3})".FormatSafe(timestamp, Global.MacAddress, Global.CompanyId, hash);

                CraveService.ObyTypedResultOfCompany wsResult = WebserviceManager.Instance.Webservice.GetCompany(timestamp, Global.MacAddress, Global.CompanyId, hash);
                if (wsResult == null)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result is NULL", methodCall);
                }
                else if (wsResult.ResultCode != (int)GenericWebserviceCallResult.Success)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result: {1} - {2}", methodCall, wsResult.ResultCode, wsResult.ResultMessage);
                }
                else
                    toReturn = WebserviceManager.ConvertModel<Company>(wsResult.ModelCollection[0]);
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("ERROR: {0} Exception: {1} - {2}", methodCall, ex.Message, (ex.InnerException != null) ? ex.InnerException.Message : "");
            }

            return toReturn;
        }

        public static Netmessage[] GetNetmessages()
        {
            Netmessage[] toReturn = null;
            string methodCall = string.Empty;
            try
            {
                string hash;
                long timestamp = WebserviceManager.GetTimeStampAndHash(out hash, Global.MacAddress, 0, 0, 0, 0, Global.TerminalId);

                methodCall = "WebserviceManager.GetNetmessages({0}, {1}, 0, 0, 0, 0, {2}, {3})".FormatSafe(timestamp, Global.MacAddress, Global.TerminalId, hash);

                CraveService.ObyTypedResultOfNetmessage wsResult = WebserviceManager.Instance.Webservice.GetNetmessages(timestamp, Global.MacAddress, 0, 0, 0, 0, Global.TerminalId, hash);
                if (wsResult == null)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result is NULL", methodCall);
                }
                else if (wsResult.ResultCode != (int)GenericWebserviceCallResult.Success)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result: {1} - {2}", methodCall, wsResult.ResultCode, wsResult.ResultMessage);
                }
                else
                    toReturn = WebserviceManager.ConvertModel<Netmessage[]>(wsResult.ModelCollection);
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("ERROR: {0} Exception: {1} - {2}", methodCall, ex.Message, (ex.InnerException != null) ? ex.InnerException.Message : "");
            }

            return toReturn;
        }

        public static bool UploadFile(string file, string fileName)
        {
            string methodCall = string.Empty;
            bool result = false;

            try
            {
                string hash;
                long timestamp = WebserviceManager.GetTimeStampAndHash(out hash, Global.MacAddress, file, fileName);

                methodCall = "WebserviceManager.CompleteNetmessageByGuid({0}, {1}, {2}, {3})".FormatSafe(timestamp, Global.MacAddress, fileName, hash);

                CraveService.ObyTypedResultOfSimpleWebserviceResult wsResult = WebserviceManager.Instance.Webservice.UploadFile(timestamp, Global.MacAddress, file, fileName, hash);

                if (wsResult == null)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result is NULL", methodCall);
                }
                else if (wsResult.ResultCode != (int)GenericWebserviceCallResult.Success)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result: {1} - {2}", methodCall, wsResult.ResultCode, wsResult.ResultMessage);
                }
                else
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("ERROR: {0} Exception: {1} - {2}", methodCall, ex.Message, (ex.InnerException != null) ? ex.InnerException.Message : "");
            }

            return result;
        }

        public static bool WriteToLog(WriteToLog logObj)
        {
            bool result = false;
            string methodCall = string.Empty;
            // (long timestamp, string macAddress, int terminalId, int orderId, string orderGuid, int type, string message, string status, string log, string hash)
            try
            {
                string hash;
                int orderId = logObj.orderId ?? -1;
                logObj.Message = logObj.Message.Replace(Environment.NewLine, "\n");
                logObj.Log = logObj.Log.Replace(Environment.NewLine, "\n");
                logObj.Log = logObj.Log.Replace("\n\r", "\n");
                long timestamp = WebserviceManager.GetTimeStampAndHash(out hash, Global.MacAddress, Global.CompanyId, Global.TerminalId, orderId, logObj.OrderGuid, logObj.TerminalLogType, logObj.Message, logObj.Status, logObj.Log);

                methodCall = "WebserviceManager.WriteToLog({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10})"
                    .FormatSafe(timestamp, Global.MacAddress, Global.CompanyId, Global.TerminalId, orderId, logObj.OrderGuid, logObj.TerminalLogType, logObj.Message, logObj.Status, logObj.Log, hash);

                CraveService.ObyTypedResultOfSimpleWebserviceResult wsResult =
                    WebserviceManager.Instance.Webservice.WriteToLog(timestamp, Global.MacAddress, Global.CompanyId, Global.TerminalId, orderId, logObj.OrderGuid, logObj.TerminalLogType, logObj.Message, logObj.Status, logObj.Log, hash);
                if (wsResult != null && wsResult.ResultCode == (int)GenericWebserviceCallResult.Success)
                    result = true;
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("ERROR: {0} Exception: {1} - {2}", methodCall, ex.Message, (ex.InnerException != null) ? ex.InnerException.Message : "");
            }

            return result;
        }

        public static string GetReleaseAndDownloadLocation(string applicationName)
        {
            string methodCall = string.Empty;
            string result = string.Empty;

            try
            {
                string hash;
                long timestamp = WebserviceManager.GetTimeStampAndHash(out hash, Global.MacAddress, applicationName);

                methodCall = "WebserviceManager.GetReleaseAndDownloadLocation({0}, {1}, {2}, {3})".FormatSafe(timestamp, Global.MacAddress, applicationName, hash);

                result = WebserviceManager.Instance.Webservice.GetReleaseAndDownloadLocation(timestamp, Global.MacAddress, applicationName, hash);
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("ERROR: {0} Exception: {1} - {2}", methodCall, ex.Message, (ex.InnerException != null) ? ex.InnerException.Message : "");
            }

            return result;
        }

        public static void SetApplicationVersion(string applicationCode, string versionNumber)
        {
            string methodCall = string.Empty;            
            try
            {
                string hash;
                long timestamp = WebserviceManager.GetTimeStampAndHash(out hash, Global.MacAddress, applicationCode, versionNumber);

                methodCall = "WebserviceManager.SetApplicationVersion({0}, {1}, {2}, {3})".FormatSafe(timestamp, Global.MacAddress, applicationCode, versionNumber, hash);

                WebserviceManager.Instance.Webservice.SetApplicationVersion(timestamp, Global.MacAddress, applicationCode, versionNumber, hash);
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("ERROR: {0} Exception: {1} - {2}", methodCall, ex.Message, (ex.InnerException != null) ? ex.InnerException.Message : "");
            }
        }

        public static T ConvertModel<T>(object obj)
        {
            return XmlHelper.Deserialize<T>(XmlHelper.Serialize(obj), false);
        }

        public static long GetTimeStampAndHash(out string hash, params object[] parameters)
        {
            long timestamp = DateTime.UtcNow.ToUnixTime();
            hash = Hasher.GetHashFromParameters(Global.Salt, timestamp, parameters);

            return timestamp;
        }

        #endregion
    }
}
