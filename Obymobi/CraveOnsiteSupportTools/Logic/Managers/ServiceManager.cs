﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace CraveOnsiteSupportTools.Logic.Managers
{
    public static class ServiceManager
    {
        public static bool Start(string serviceName, bool awaitResult)
        {
            ServiceController controller = new ServiceController(serviceName);
            if (controller.Status != ServiceControllerStatus.Running)
            {
                controller.Start();
            }

            if (awaitResult)
            {
                controller.WaitForStatus(ServiceControllerStatus.Running, new TimeSpan(0, 0, 60));
            }

            return controller.Status == ServiceControllerStatus.Running;
        }

        public static bool Stop(string serviceName, bool awaitResult)
        {
            ServiceController controller = new ServiceController(serviceName);
            if (controller.Status != ServiceControllerStatus.Stopped)
            {
                controller.Stop();
            }

            if (awaitResult)
            {
                controller.WaitForStatus(ServiceControllerStatus.Stopped, new TimeSpan(0, 0, 60));
            }

            return controller.Status == ServiceControllerStatus.Stopped;
        }
    }
}
