﻿using System;
using System.Collections.Generic;
using System.IO;
using System.ServiceProcess;
using System.Threading;
using CraveOnsiteServer.Net;
using CraveOnsiteSupportTools.Comet;
using Dionysos;
using Dionysos.ServiceProcess.HelperClasses;
using Global = CraveOnsiteSupportTools.Logic.Global;
using CraveOnsiteSupportTools.Logic;
using CraveOnsiteSupportTools.Logic.Constants;
using CraveOnsiteSupportTools.Logic.Managers;
using Obymobi.Logic.Model;
using Obymobi.Logging;

namespace CraveOnsiteServer.Logic
{
    public class HeartbeatHandler : IConnectionHandler
    {
        #region Constants

        private const int LOG_CLEANUP_THRESHOLD = 86400; // 24 Hours

        #endregion

        private Thread webserviceThread;
        private readonly AutoResetEvent isStopped = new AutoResetEvent(false);

        private bool netmessageHandlingEnabled;

        private NetmessageHandler netmessageHandler;

        private DateTime lastLogCleanup = DateTime.MinValue;

        public HeartbeatHandler()
        {
            netmessageHandler = new NetmessageHandler();

            webserviceThread = new Thread(WebservicePoller);
            webserviceThread.Start();
        }        

        public void Activate()
        {
            ConsoleLogger.WriteToLog("HeartbeatHandler - Activated");
            netmessageHandlingEnabled = true;
        }

        public void Deactivate()
        {
            ConsoleLogger.WriteToLog("HeartbeatHandler - Deactivated");
            netmessageHandlingEnabled = false;
        }

        public bool IsConnectionReady()
        {
            return true;
        }

        public void Destroy()
        {
            ConsoleLogger.WriteToLog("HeartbeatHandler - Destroy");
            netmessageHandler = null;

            StopThread();
        }

        private void StopThread()
        {
            if (webserviceThread != null && webserviceThread.IsAlive)
            {
                isStopped.Set();

                webserviceThread.Join(new TimeSpan(0, 0, 10));
                webserviceThread = null;
            }
        }

        #region Webservice Poller Methods

        private void WebservicePoller()
        {
            ConsoleLogger.WriteToLog("WebservicePoller - START");

            bool lastWebserviceStatus = Global.Status.IsWebserviceAvailable;

            while (!isStopped.WaitOne(new TimeSpan(0, 0, Global.RequestInterval)))
            {
                ConsoleLogger.WriteToLog("------------------- START --------------------", LoggingLevel.Debug, "WebservicePoller-", false);

                Global.LastPollingUpdate = DateTime.Now;

                // Check connection
                try
                {
                    ConsoleLogger.WriteToLog("- Check connectivity", LoggingLevel.Debug, "WebservicePoller-", false);
                    Global.Status.Refresh();
                }
                catch (Exception ex)
                {
                    ConsoleLogger.WriteToLog("Exception while refreshing CloudStatus: {0}", ex.Message);
                    ConsoleLogger.WriteToLog(ex.ProcessStackTrace(true));
                }

                bool webserviceAlive = Global.Status.IsWebserviceAvailable;
                if (webserviceAlive)
                {
                    // We had no internet or webservice connection,
                    // initialize our webservice
                    if (!lastWebserviceStatus)
                    {
                        ConsoleLogger.WriteToLog("- Re-initiazing webservice", LoggingLevel.Debug, "WebservicePoller-", false);

                        WebserviceManager.Instance.InitializeWebservice();
                    }

                    // ----------------------------------------------------------------
                    // Comet/Netmessage
                    if (netmessageHandlingEnabled)
                    {
                        ConsoleLogger.WriteToLog("- Receiving Netmessages from webservice", LoggingLevel.Debug, "WebservicePoller-", false);
                        ReceiveNetmessages();
                    }                    
                }
                else
                {
                    ConsoleLogger.WriteToLog("- Failed to find online webservice", LoggingLevel.Debug, "WebservicePoller-", false);
                }

                // Cleaning up old logs
                if (lastLogCleanup.TotalSecondsToNow() >= HeartbeatHandler.LOG_CLEANUP_THRESHOLD)
                {
                    lastLogCleanup = DateTime.Now;

                    ConsoleLogger.WriteToLog("- Cleaning Logs", LoggingLevel.Verbose, "WebservicePoller-", false);
                    ConsoleLogger.Cleanup(30);
                }

                ConsoleLogger.WriteToLog("------------------- END --------------------", LoggingLevel.Debug, "WebservicePoller-", false);

                lastWebserviceStatus = webserviceAlive;
            }

            ConsoleLogger.WriteToLog("WebservicePoller - STOPPED");
        }

        private void ReceiveNetmessages()
        {
            Netmessage[] netmessages = WebserviceManager.GetNetmessages();
            if (netmessages != null && netmessages.Length > 0)
            {
                foreach (Netmessage netmessage in netmessages)
                {
                    this.netmessageHandler.OnNetmessageReceived(netmessage);
                }
            }
        }        

        #endregion
    }
}
