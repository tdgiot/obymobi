﻿using System;
using System.Collections.Generic;
using Dionysos;
using Obymobi;
using Obymobi.Constants;
using Obymobi.Enums;
using System.IO;

namespace CraveOnsiteSupportTools.Logic.Constants
{
    public static class SupportToolsConstants
    {
        public const string INSTALL_DIR = @"C:\Program Files\Crave\";
        public const string UPDATES_DIR = SupportToolsConstants.INSTALL_DIR + @"Updates\";
        public const string TEMP_DIR = SupportToolsConstants.INSTALL_DIR + @"Temp\";
        public const string UPDATE_FILENAME = "update.zip";

        public const string ONSITE_SERVER_SERVICE_NAME = "CraveOnsiteServer";
        public const string ONSITE_AGENT_SERVICE_NAME = "CraveOnsiteAgent";
        public const string ONSITE_SUPPORTTOOLS_SERVICE_NAME = "CraveOnsiteSupportTools";

        public static List<Application> GetInstalledApplications()
        {
            List<Application> applicationList = new List<Application>();

            string path = @"C:\Program Files\Crave\Crave Onsite Server\CraveOnsiteServer.exe";
            if (File.Exists(path))
                applicationList.Add(new Application("CraveOnsiteServer", path.GetAllBeforeFirstOccurenceOf("CraveOnsiteServer.exe", false), SupportToolsConstants.ONSITE_SERVER_SERVICE_NAME));
            
            path = @"C:\Program Files\Crave\Crave Onsite Agent\CraveOnsiteAgent.exe";
            if (File.Exists(path))
                applicationList.Add(new Application("CraveOnsiteAgent", path.GetAllBeforeFirstOccurenceOf("CraveOnsiteAgent.exe", false), SupportToolsConstants.ONSITE_AGENT_SERVICE_NAME));

            path = @"C:\Program Files\Crave\Crave Onsite SupportTools\CraveOnsiteSupportTools.exe";
            if (File.Exists(path))
                applicationList.Add(new Application("CraveOnsiteSupportTools", path.GetAllBeforeFirstOccurenceOf("CraveOnsiteSupportTools.exe", false), SupportToolsConstants.ONSITE_SUPPORTTOOLS_SERVICE_NAME));

            return applicationList;
        }

        public static string GetCompanyContainer()
        {
            if (Global.CdnRootContainer.IsNullOrWhiteSpace() && Global.Status.CloudEnvironment == CloudEnvironment.Manual)
                throw new Exception("Global.CdnRootContainer is empty");

            string environmentPrefix = Global.CdnRootContainer.Replace("cravecloud-", "");
            if (Global.Status.CloudEnvironment != CloudEnvironment.Manual)
            {
                environmentPrefix = WebEnvironmentHelper.GetEnvironmentPrefixForCdn(Global.Status.CloudEnvironment);
            }
            return string.Format(ObymobiConstants.AmazonS3AwsCompanyContainer, environmentPrefix, Global.CompanyId);
        }
    }
}
