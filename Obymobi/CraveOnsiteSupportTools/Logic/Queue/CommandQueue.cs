﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using CraveOnsiteSupportTools.Logic.Commands;
using Obymobi.Logging;

namespace CraveOnsiteSupportTools.Logic.Queue
{
    public class CommandQueue
    {
        #region Fields

        private static CommandQueue sInstance;

        private readonly object queueLocker = new object();
        private readonly Queue<CommandExecutor> queue;
        private Thread consumerThread;
        private bool isRunning = true;

        #endregion

        #region Constructors

        private CommandQueue()
        {
            this.queue = new Queue<CommandExecutor>();
        }

        public static CommandQueue Instance
        {
            get { return CommandQueue.sInstance ?? (CommandQueue.sInstance = new CommandQueue()); }
        }

        #endregion

        #region Methods

        public void StartConsumer()
        {
            ConsoleLogger.WriteToLog("Starting command queue consumer thread");
            this.isRunning = true;

            if (consumerThread == null)
            {
                consumerThread = new Thread(this.ConsumeItems) { IsBackground = true };
                consumerThread.Start();
            }
        }

        public void StopConsumer()
        {
            ConsoleLogger.WriteToLog("Stopping command queue consumer thread");
            if (consumerThread != null)
            {
                isRunning = false;

                consumerThread.Join(new TimeSpan(0, 1, 0));
                consumerThread = null;
            }
        }

        public void Enqueue(CommandBase command, string args)
        {
            if (!this.isRunning)
                return;

            lock(this.queueLocker)
            {
                this.queue.Enqueue(new CommandExecutor(command, args));
                Monitor.PulseAll(this.queueLocker);
            }
        }

        private void ConsumeItems()
        {
            while (true)
            {
                lock (this.queueLocker)
                {
                    while (this.queue.Count == 0)
                    {
                        // ...otherwise, wait for manual process
                        Monitor.Wait(this.queueLocker);
                    }

                    bool doesItemExist = (this.queue.Count > 0);
                    if (doesItemExist)
                    {
                        CommandExecutor executor = this.queue.Dequeue();
                        if (executor == null)
                            break;

                        executor.Execute();                        
                    }
                }
            }
        }

        private class CommandExecutor
        {
            private readonly CommandBase commmand;
            private readonly string args;

            public CommandExecutor(CommandBase cmd, string args)
            {
                this.commmand = cmd;
                this.args = args;
            }

            public void Execute()
            {
                this.commmand.Execute(this.args);
            }
        }

        #endregion
    }
}
