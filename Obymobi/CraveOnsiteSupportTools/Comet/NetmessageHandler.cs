﻿using System;
using Obymobi.Enums;
using Obymobi.Logic.Comet.Netmessages;
using Obymobi.Logic.Model;
using CraveOnsiteSupportTools.Logic;
using CraveOnsiteSupportTools.Logic.Managers;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logging;

namespace CraveOnsiteSupportTools.Comet
{
    public class NetmessageHandler
    {
        private readonly CometHandler client;
        private readonly DeviceCommands commands;

        public NetmessageHandler(CometHandler cometHandler = null)
        {
            client = cometHandler;
            commands = new DeviceCommands();
        }

        public void OnNetmessageReceived(Netmessage netmessage)
        {                        
            try
            {
                switch (netmessage.GetMessageType())
                {
                    case NetmessageType.AgentCommandRequest:
                        AgentCommandRequest(netmessage);
                        break;
                    case NetmessageType.AuthenticateResult:
                        AuthenticateResult(netmessage);
                        break;                    
                    case NetmessageType.Ping:
                        Pong();
                        break;
                    case NetmessageType.Disconnect:
                        Disconnect();
                        break;                    
                    default:
                        ConsoleLogger.WriteToLog("NetmessageHandler - MessageType {0} is not handled.", netmessage.GetMessageType());
                        break;
                }
            }
            catch(Exception ex)
            {
                int commandInt;
                TerminalCommand command;
                if (netmessage.MessageType == NetmessageType.TerminalCommand && int.TryParse(netmessage.FieldValue1, out commandInt) && EnumUtil.TryParse(commandInt, out command))
                {
                    ConsoleLogger.WriteToLog("NetmessageHandler - Message Received and ended in exception, Terminal Command: '{0}', FV1: '{1}', FV2: '{2}', FV3: '{3}'",
                        command.ToString(), netmessage.FieldValue1, netmessage.FieldValue2, netmessage.FieldValue3);                                        
                }
                else
                {
                    ConsoleLogger.WriteToLog("NetmessageHandler - Message Received and ended in exception, Type: '{0}', FV1: '{1}', FV2: '{2}', FV3: '{3}'",
                        netmessage.MessageType, netmessage.FieldValue1, netmessage.FieldValue2, netmessage.FieldValue3);
                }

                ConsoleLogger.WriteToLog("NetmessageHandler - Exception: {0}", ex.Message);

                throw;
            }

        }

        private void Disconnect()
        {
            if (this.client != null)
            {
                this.client.Disconnect(true);
            }
        }

        private void Pong()
        {
            if (this.client != null && this.client.IsConnectionReady())
            {
                this.client.Pong();
            }
        }

        private void AuthenticateResult(Netmessage message)
        {
            NetmessageAuthenticateResult netmessage = message.ConvertTo<NetmessageAuthenticateResult>();

            if (this.client != null)
            {
                if (netmessage.IsAuthenticated)
                {
                    ConsoleLogger.WriteToLog("Authenticated to CometServer");
                    this.client.SetAuthenticated(true);

                    NetmessageSetClientType clientType = new NetmessageSetClientType();
                    clientType.ClientType = NetmessageClientType.SupportTools;
                    clientType.TerminalId = Global.TerminalId;

                    this.client.SendMessage(clientType);
                }
                else
                {
                    this.client.SetAuthenticated(false);
                    ConsoleLogger.WriteToLog("Failed to autenticate. Message: {0}", message.FieldValue2);
                }
            }
        }

        private void AgentCommandRequest(Netmessage message)
        {
            message = NetmessageHelper.CreateTypedNetmessageModelFromModel(message);
            ConsoleLogger.WriteToLog("NetmessageHandler - AgentCommandRequest - {0} - {1}", message.NetmessageId, message.ToString());
            if (WebserviceManager.VerifyNetmessage(message.Guid))
            {
                try
                {
                    NetmessageAgentCommandRequest agentCommand = message.ConvertTo<NetmessageAgentCommandRequest>();
                    this.commands.Execute(agentCommand.FieldValue2);
                }
                catch (Exception ex)
                {
                    // In case of exception, just retrieve orders
                    ConsoleLogger.WriteToLog("NetmessageHandler - AgentCommandRequest: ERROR - " + ex.Message);
                }
                WebserviceManager.CompleteNetmessage(message.Guid);
            }
        }
    }
}
