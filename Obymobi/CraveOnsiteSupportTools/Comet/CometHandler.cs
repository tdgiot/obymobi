﻿using System;
using System.Threading.Tasks;
using CraveOnsiteServer.Logic;
using CraveOnsiteServer.Net;
using Dionysos;
using Dionysos.Net;
using Obymobi.Enums;
using Obymobi.Logic.Comet;
using Obymobi.Logic.Comet.Interfaces;
using Obymobi.Logic.Comet.Netmessages;
using Obymobi.Logic.Model;
using Global = CraveOnsiteSupportTools.Logic.Global;
using Obymobi.Logging;

namespace CraveOnsiteSupportTools.Comet
{
    public class CometHandler : IConnectionHandler, ICometClientEventListener
    {
        public CometClient Client;
        private readonly object handlerLock = new object();
        private readonly IConnectionEventListener connectionEventListener;
        private ConnectionState connectionState = ConnectionState.Disconnected;
        private readonly InfiniteLooper pingPongChecker;

        private readonly NetmessageHandler messageHandler;

        public CometHandler(IConnectionEventListener eventListener)
        {
            connectionEventListener = eventListener;
            messageHandler = new NetmessageHandler(this);

            pingPongChecker = new InfiniteLooper(() =>
                {
                    if (State == ConnectionState.Connected && this.Client != null && !this.Client.CheckLastPingPong())
                    {
                        OnEventLog("Ping/Pong - No ping message received for 2 minutes, disconnecting client.");

                        this.Client.Dispose();
                        this.Client = null;

                        State = ConnectionState.Disconnected;

                        Connect();
                    }
                }, 30000, -1, OnExceptionHandler);

            State = ConnectionState.Disconnected;
            Global.Status.WebserviceUrlChanged += Status_WebserviceUrlChanged;
        }

        private void OnExceptionHandler(Exception exception)
        {
            OnEventLog("Ping/Pong Exception - " + exception.Message);

            StopPingPongChecker();
            StartPingPongChecker();
        }

        internal void Connect()
        {
            if (State == ConnectionState.Disconnected || State == ConnectionState.Reconnecting)
            {
                State = ConnectionState.Connecting;

                if (this.Client != null)
                {
                    this.Client.Dispose();
                    this.Client = null;
                }

                OnEventLog("Connect - Connecting to CometServer: '{0}'", Global.Status.MessagingUrl);
                SignalRClient signalRClient = new SignalRClient(this, Global.Status.MessagingUrl, Global.MacAddress, Global.Salt);
                signalRClient.ForceWebSocket = false; // AutoTransport

                this.Client = signalRClient;
                this.Client.Connect();
            }
            else
            {
                OnEventLog("Connect - Can not connect becuase of current State {0}", State);
            }
        }

        internal void Disconnect(bool forced = false)
        {
            if (this.Client != null && this.Client.IsConnected())
            {
                OnEventLog("Disconnect");
                State = ConnectionState.Disconnecting;

                this.Client.Disconnect();

                if (forced)
                    this.connectionEventListener.OnCometConnectionChanged(false);
            }
        }

        private void ReloadConnection(int delay)
        {
            if (State != ConnectionState.Reconnecting)
            {
                State = ConnectionState.Reconnecting;

                if (this.Client != null)
                {
                    this.Client.Dispose();
                    this.Client = null;
                }

                if (delay <= 0)
                {
                    OnEventLog("CometDesktopProvider.ReloadConnection - Reconnecting to CometServer", delay);
                    Connect();
                }
                else
                {
                    OnEventLog("CometDesktopProvider.ReloadConnection - Reconnecting to CometServer in {0} seconds", delay);
                    Task.Factory.StartDelayed(delay * 1000, Connect);
                }
            }
        }

        public ConnectionState State
        {
            get
            {
                lock(handlerLock)
                {
                    return connectionState;
                }
            }
            private set
            {
                ConnectionState oldState;
                ConnectionState newState = value;
                lock(handlerLock)
                {
                    if (connectionState == value)
                    {
                        return;
                    }

                    oldState = connectionState;
                }

                if (newState == ConnectionState.Disconnecting && oldState != ConnectionState.Connected)
                {
                    OnEventLog("ConnectionState - Tried to change state to 'Disconnecting' while state is '{0}'", oldState);
                    return;
                }

                // Update state
                connectionState = newState;

                OnEventLog("ConnectionState - Changed connection state from '{0}' to '{1}'.", oldState, newState);

                if (newState == ConnectionState.Connected && (oldState == ConnectionState.Connecting || oldState == ConnectionState.Reconnecting))
                {
                    if (this.Client.IsAuthenticated)
                        this.connectionEventListener.OnCometConnectionChanged(true);

                    StartPingPongChecker();
                }
                else if (newState == ConnectionState.Disconnected && oldState != ConnectionState.Disconnecting)
                {
                    this.connectionEventListener.OnCometConnectionChanged(false);
                    ReloadConnection(30);
                    StopPingPongChecker();
                }
            }

        }

        public void SetAuthenticated(bool authenticated)
        {
            this.Client.SetAuthenticated(authenticated);

            this.Client.IsAuthenticated = authenticated;
            if (authenticated)
            {
                this.connectionEventListener.OnCometConnectionChanged(true);
            }
            else
            {
                ReloadConnection(30);
            }
        }

        public void SendMessage(Netmessage message)
        {
            if (IsConnectionReady())
            {
                this.Client.SendMessage(message);
            }
        }

        public void OnConnectFailed()
        {
            ConsoleLogger.WriteToLog("CometHandler - Failed to connect to server!");

            ReloadConnection(30);
        }

        public void OnConnectionChanged(bool isConnected)
        {
            OnEventLog("OnConnectionChanged - Connected: {0}", isConnected);

            if (isConnected)
            {
                State = ConnectionState.Connected;
            }
            else
            {
                this.Client.IsAuthenticated = false;
                this.Client.SetAuthenticated(false);

                if (State == ConnectionState.Connected || State == ConnectionState.Disconnecting)
                {
                    State = ConnectionState.Disconnected;
                    ReloadConnection(30);
                }
            }
        }

        public void OnEventLog(string message, params object[] args)
        {
            if (args.Length > 0)
                message = message.FormatSafe(args);

            ConsoleLogger.WriteToLog("CometHandler - " + message);
        }

        public void Activate()
        {
            if (State == ConnectionState.Disconnected || State == ConnectionState.Reconnecting)
            {
                Connect();
            }
        }

        public void Deactivate()
        {
            // Do nothing
        }

        public bool IsConnectionReady()
        {
            lock(handlerLock)
            {
                return (this.Client != null && this.Client.IsConnected() && this.Client.IsAuthenticated);
            }
        }

        public void Destroy()
        {
            lock (handlerLock)
            {
                connectionState = ConnectionState.Disconnected;

                if (this.Client != null)
                {
                    this.Client.Dispose();
                    this.Client = null;
                }

                StopPingPongChecker();
            }
        }

        private void Status_WebserviceUrlChanged(object sender, EventArgs e)
        {
            lock(handlerLock)
            {
                if (this.Client != null)
                {
                    this.Client.Dispose();
                    this.Client = null;
                }
            }

            State = ConnectionState.Disconnected;
            Connect();
        }

        #region Ping/Pong

        private void StartPingPongChecker()
        {
            lock (handlerLock)
            {
                if (this.pingPongChecker != null && !this.pingPongChecker.IsRunning)
                {
                    OnEventLog("Ping/Pong - Starting PingPongChecker");
                    this.pingPongChecker.Start();
                    OnEventLog("Ping/Pong - PingPongChecker Started");
                }
            }
        }

        private void StopPingPongChecker()
        {
            lock (handlerLock)
            {
                if (this.pingPongChecker != null && this.pingPongChecker.IsRunning)
                {
                    OnEventLog("Ping/Pong - Stopping PingPongChecker");
                    this.pingPongChecker.Stop();
                    OnEventLog("Ping/Pong - PingPongChecker Stopped");
                }
            }
        }

        public void Pong()
        {
            // Respond with a Pong
            NetmessagePong pong = new NetmessagePong();
            pong.PrivateIpAddresses = NetUtil.GetLocalIpsJoined();
            pong.PublicIpAddress = NetUtil.GetRemoteIp();
            pong.IsCharging = true;
            pong.BatteryLevel = 100;
            pong.CurrentCloudEnvironment = Global.Status.CloudEnvironment;
            pong.SupportToolsIsRunning =  true;

            lock(handlerLock)
            {
                if (IsConnectionReady())
                {
                    this.Client.LastPong = DateTime.UtcNow;
                    this.SendMessage(pong);
                }
            }
        }


        #endregion

        #region Receive Methods

        public void OnNetmessageReceived(Netmessage netmessage)
        {
            if (messageHandler != null)
            {
                messageHandler.OnNetmessageReceived(netmessage);
            }
        }

        #endregion
	}
}
