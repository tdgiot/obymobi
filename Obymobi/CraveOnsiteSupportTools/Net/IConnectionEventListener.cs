﻿namespace CraveOnsiteServer.Net
{
    public interface IConnectionEventListener
    {
        void OnWebserviceConnectionChanged(bool isConnected);

        void OnCometConnectionChanged(bool isConnected);
    }
}
