﻿namespace CraveOnsiteServer.Net
{
    public interface IConnectionHandler
    {
        void Activate();

        void Deactivate();

        bool IsConnectionReady();

        void Destroy();
    }
}
