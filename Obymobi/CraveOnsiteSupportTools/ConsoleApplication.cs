﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Diagnostics;
using System.Threading.Tasks;
using CraveOnsiteSupportTools.Logic;
using CraveOnsiteSupportTools.Logic.Constants;
using CraveOnsiteSupportTools.Logic.Enum;
using CraveOnsiteSupportTools.Logic.Managers;
using Dionysos;
using Dionysos.Configuration;
using Obymobi.Configuration;
using Obymobi.Constants;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using Global = CraveOnsiteSupportTools.Logic.Global;
using Obymobi.Logging;

namespace CraveOnsiteSupportTools
{
	public class ConsoleApplication
	{
		public static ConsoleApplication Instance { get; private set; }
		public static Dictionary<string, object> Arguments;

		public ConsoleApplication(string applicationPath, Dictionary<string, object> args)
		{
            TaskScheduler.UnobservedTaskException += ConsoleApplication.TaskScheduler_UnobservedTaskException;

			ConsoleApplication.Instance = this;
			ConsoleApplication.Arguments = args;

            Global.IsConsoleMode = ConsoleLogger.IsConsoleMode = (bool)ConsoleApplication.Arguments["console"];

            ConsoleLogger.WriteToLog(@"======================================================");
            ConsoleLogger.WriteToLog(@"Starting Crave Onsite Support Tools {0}{1}", Dionysos.Global.ApplicationInfo.ApplicationVersion, (TestUtil.IsPcDeveloper) ? " (Developer Edition)" : "");
            ConsoleLogger.WriteToLog(@"======================================================");
            ConsoleLogger.WriteToLog(@"Base dir: {0}", applicationPath);
            ConsoleLogger.WriteToLog("");

            Global.RootDirectory = applicationPath;
            if (Global.RootDirectory == null)
            {
                ConsoleLogger.WriteToLog("ERROR: Could not determine application path!");
            }
		}

        static void TaskScheduler_UnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs e)
        {
            if (e.Exception == null)
            {
                e.SetObserved();
                return;
            }

            e.Exception.Flatten().Handle(ex =>
                               {
                                   ConsoleLogger.WriteToLog("InnerException: " + ex.Message);
                                   ConsoleLogger.WriteToLog("InnerException StackTrace: " + ex.ProcessStackTrace());
                                   ConsoleLogger.WriteToLog("InnerException ToString: " + ex.ToString());
                                   return true;
                               });
            e.SetObserved();
        }

        public bool Start()
        {
            Stopwatch startupTimer = new Stopwatch();
            startupTimer.Start();

            // Initialize Configuration and Globals
            ConsoleLogger.WriteToLog("Initializing configuration and globals...");
            this.InitializeGlobals();

            // Starting server
            Server.Instance.StartServer();

            startupTimer.Stop();
            ConsoleLogger.WriteToLog("Server initialized in {0} seconds.", (startupTimer.ElapsedMilliseconds / 1000));

            return true;
        }

        public void Close()
        {
            Server.Instance.StopServer();            
        }

	    public void Restart()
	    {
            ConsoleLogger.WriteToLog("*******************************************");
            ConsoleLogger.WriteToLog("*************** RESTARTING ****************");
            ConsoleLogger.WriteToLog("*******************************************");

	        WebserviceManager.Instance.Reset();
            Server.Reset();

	        this.Start();
	    }

	    private void InitializeGlobals()
		{
			// Set the configuration provider
            XmlConfigurationProvider provider = this.GetOnsiteServerConfigurationProvider();
			Dionysos.Global.ConfigurationProvider = provider;
            Dionysos.Global.ConfigurationInfo.Add(new CraveCloudConfigInfo());
            Dionysos.Global.ConfigurationInfo.Add(new CraveOnSiteServerConfigInfo());

            Global.MacAddress = ConfigurationManager.GetString(CraveOnSiteServerConfigConstants.MacAddress);
            Global.CompanyOwner = ConfigurationManager.GetString(CraveOnSiteServerConfigConstants.CompanyOwner);
            Global.CompanyPassword = ConfigurationManager.GetPassword(CraveOnSiteServerConfigConstants.CompanyPassword);
            Global.TerminalId = ConfigurationManager.GetInt(CraveOnSiteServerConfigConstants.TerminalId);
            Global.CompanyId = ConfigurationManager.GetInt(CraveOnSiteServerConfigConstants.CompanyId);
            Global.Salt = Dionysos.Security.Cryptography.Cryptographer.DecryptStringUsingRijndael(ConfigurationManager.GetString(CraveOnSiteServerConfigConstants.Salt));
            Global.RequestInterval = ConfigurationManager.GetInt(CraveOnSiteServerConfigConstants.RequestInterval);
            Global.ServerState = ServerState.OFFLINE;

            if (ConfigurationManager.GetString(CraveCloudConfigConstants.CloudEnvironment) == "NOTSET")
            {
                ConsoleLogger.WriteToLog("Upgrading to CloudEnvironment Configuration");

                // Old installation, upgrade!
                string webserviceUrl = ConfigurationManager.GetString(CraveOnSiteServerConfigConstants.WebserviceUrl1);

                if (webserviceUrl.Contains("app.", StringComparison.InvariantCultureIgnoreCase))
                    ConfigurationManager.SetValue(CraveCloudConfigConstants.CloudEnvironment, CloudEnvironment.ProductionPrimary.ToString());
                else if (webserviceUrl.Contains("dev.", StringComparison.InvariantCultureIgnoreCase))
                    ConfigurationManager.SetValue(CraveCloudConfigConstants.CloudEnvironment, CloudEnvironment.Development.ToString());
                else if (webserviceUrl.Contains("test.", StringComparison.InvariantCultureIgnoreCase))
                    ConfigurationManager.SetValue(CraveCloudConfigConstants.CloudEnvironment, CloudEnvironment.Test.ToString());
                else
                {
                    ConfigurationManager.SetValue(CraveCloudConfigConstants.CloudEnvironment, CloudEnvironment.Manual.ToString());
                    ConfigurationManager.SetValue(CraveCloudConfigConstants.ManualWebserviceBaseUrl, webserviceUrl);
                }

                ConsoleLogger.WriteToLog("CloudEnvironment Configuration upgraded to: {0}", ConfigurationManager.GetString(CraveCloudConfigConstants.CloudEnvironment));
            }

            Global.Status = new Obymobi.Logic.Status.CloudStatus();
            ConsoleLogger.WriteToLog("Check Cloud Status...");
            Global.Status.Refresh();
            Global.Status.NocStatusTextFile = string.Format(ObymobiConstants.NocStatusTextFile_Company, Global.CompanyId);

            if (Global.Status.IsWebserviceAvailable)
            {
                ConsoleLogger.WriteToLog("Initializing webservice using url '{0}'...", Global.Status.CraveServiceUrl);
                WebserviceManager.Instance.InitializeWebservice();
            }

            Company company = WebserviceManager.GetCompany();
            if (company == null)
            {
                ConsoleLogger.WriteToLog("Failed to load company at startup. Can't continue...");
                throw new Exception("Unable to load company");
            }
            else
            {
                Global.CdnRootContainer = this.GetCdnRootContainerForEnvironment(Global.Status.CloudEnvironment);
                CdnManager.Instance.Initialize(company.CloudStorageAccounts, Global.CdnRootContainer);
            }

            ConsoleLogger.WriteToLog("");
            ConsoleLogger.WriteToLog("************************************************");
            ConsoleLogger.WriteToLog("** Version:               {0}", Dionysos.Global.ApplicationInfo.ApplicationVersion);
            ConsoleLogger.WriteToLog("** Root Directory:        {0}", Global.RootDirectory);
            ConsoleLogger.WriteToLog("**");
            ConsoleLogger.WriteToLog("** MAC Address:           {0}", Global.MacAddress);
            ConsoleLogger.WriteToLog("** Company Id:            {0}", Global.CompanyId);
            ConsoleLogger.WriteToLog("** Company Owner:         {0}", Global.CompanyOwner);
            ConsoleLogger.WriteToLog("** Terminal Id:           {0}", Global.TerminalId);
            ConsoleLogger.WriteToLog("** CDN container:         {0}", Global.CdnRootContainer);
            ConsoleLogger.WriteToLog("**");
            ConsoleLogger.WriteToLog("** Cloud Environment:     {0}", Global.Status.CloudEnvironment);
            if (Global.Status.CloudEnvironment == CloudEnvironment.Manual)
                ConsoleLogger.WriteToLog("** Webservice:     {0}", Global.Status.ManualWebserviceBaseUrl);
            ConsoleLogger.WriteToLog("************************************************");
            ConsoleLogger.WriteToLog("");
		}	    

	    private XmlConfigurationProvider GetOnsiteServerConfigurationProvider()
        {
            string path = string.Format(@"{0}\Crave Onsite Server\CraveOnSiteServer.config", SupportToolsConstants.INSTALL_DIR);
            XmlConfigurationProvider provider = null;
            if (File.Exists(path))
            {
                provider = new XmlConfigurationProvider();
                provider.ManualConfigFilePath = path;
            }
            else
            {
                ConsoleLogger.WriteToLog("CraveOnSiteServer.config could not be found at '{0}'.", path);
            }

            return provider;
        }

        public string GetCdnRootContainerForEnvironment(CloudEnvironment environment)
        {
            string bucketName;
            switch (environment)
            {
                case CloudEnvironment.ProductionPrimary:
                case CloudEnvironment.ProductionSecondary:
                case CloudEnvironment.ProductionOverwrite:
                    bucketName = ObymobiConstants.PrimaryAmazonRootContainer;
                    break;
                case CloudEnvironment.Test:
                    bucketName = ObymobiConstants.TestAmazonRootContainer;
                    break;
                case CloudEnvironment.Development:
                    bucketName = ObymobiConstants.DevelopmentAmazonRootContainer;
                    break;
                case CloudEnvironment.Manual:
                    bucketName = string.Format("{0}", ConfigurationManager.GetString(CraveOnSiteServerConfigConstants.ManualCdnRootContainer));
                    break;
                default:
                    throw new NotImplementedException(environment.ToString());
            }

            return bucketName;
        }    
	}
}
