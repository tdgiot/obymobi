﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;

namespace Obymobi.Webservice.Tests
{
    [TestClass]
    public class WebserviceTester
    {
        #region Fields

        private CompanyCollection companyCollection = null;
        private bool initialized = false;

        #endregion

        #region Test methods

        [TestMethod]
        public void GetMenuTest()
        {
            if (!this.initialized)
                this.Initialize();

               
        }

        #endregion

        #region Helper methods

        private void Initialize()
        {
            // Get the companies that have the UseMonitoring flag set to true
            this.companyCollection = this.GetMonitoredCompanies();

            // Set the initialized flag
            this.initialized = true;
        }

        private CompanyCollection GetMonitoredCompanies()
        {
            CompanyCollection companyCollection = new CompanyCollection();
            companyCollection.GetMulti(CompanyFields.UseMonitoring == true);

            return companyCollection;
        }

        #endregion
    }
}
