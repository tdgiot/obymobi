﻿using System;
using System.Collections.Generic;
using Adyen;
using Adyen.Model.Checkout;
using Adyen.Service;
using Obymobi.Integrations.Payment.Models;
using System.Linq;
using System.Net;
using Adyen.HttpClient;
using Environment = Adyen.Model.Enum.Environment;
using PaymentMethod = Obymobi.Integrations.Payment.Models.PaymentMethod;

namespace Obymobi.Integrations.Payment.Adyen
{
    public class AdyenClient : IClient
    {
        private readonly string apiKey;
        private readonly bool isProduction;
        private readonly string liveEndpointUrlPrefix;
        private readonly string merchantAccount;

        public AdyenClient(string apiKey,
                           bool isProduction,
                           string liveEndpointUrlPrefix,
                           string merchantAccount)
        {
            this.apiKey = apiKey;
            this.isProduction = isProduction;
            this.liveEndpointUrlPrefix = liveEndpointUrlPrefix;
            this.merchantAccount = merchantAccount;
        }

        public bool IsAuthenticated()
        {
            GetPaymentMethodsResponse response = this.GetPaymentMethods();
            return response.ErrorCode == (int)HttpStatusCode.Forbidden;
        }

        public GetPaymentMethodsResponse GetPaymentMethods()
        {
            GetPaymentMethodsResponse response;

            Client client = this.CreateClient();

            try
            {   
                Checkout checkout = new Checkout(client);

                PaymentMethodsResponse paymentMethodsResponse = checkout.PaymentMethods(new PaymentMethodsRequest(merchantAccount: this.merchantAccount) { Channel = PaymentMethodsRequest.ChannelEnum.Web });
                IEnumerable<PaymentMethod> paymentMethods = paymentMethodsResponse.PaymentMethods.Select(x => new Models.PaymentMethod { Name = x.Name, Type = x.Type });
                response = new GetPaymentMethodsResponse(true, paymentMethods);
            }
            catch (HttpClientException hex)
            {
                response = new GetPaymentMethodsResponse(false, hex.Message, hex.Code);
            }
            catch (Exception ex)
            {
                response = new GetPaymentMethodsResponse(false, ex.Message);
            }           

            return response;
        }

        private Client CreateClient()
        {
            return this.isProduction ? new Client(this.apiKey, Environment.Live, this.liveEndpointUrlPrefix) : new Client(this.apiKey, Environment.Test);
        }
    }
}
