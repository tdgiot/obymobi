﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Integrations.Payment.Adyen
{
    public class AdyenClientFactory : IClientFactory
    {
        private readonly IPaymentProviderConfiguration paymentProviderConfiguration;

        public AdyenClientFactory(IPaymentProviderConfiguration paymentProviderConfiguration)
        {
            this.paymentProviderConfiguration = paymentProviderConfiguration;
        }

        public IClient Create()
        {
            return new AdyenClient(this.paymentProviderConfiguration.ApiKey,
                                   this.paymentProviderConfiguration.IsProduction,
                                   this.paymentProviderConfiguration.LiveEndpointUrlPrefix,
                                   this.paymentProviderConfiguration.AdyenMerchantCode);
        }
    }
}
