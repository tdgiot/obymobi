﻿namespace CraveRoomControlServer.Logic
{
	public class Global
	{
		public static bool IsConsoleMode { get; set; }

		public static string RootDirectory { get; set; }
		
        public static Dionysos.Logging.ILoggingProvider Logger { get; set; }                
	}
}
