﻿using System;
using System.IO;
using System.Reflection;
using Microsoft.Win32;
using Topshelf;

namespace CraveRoomControlServer
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        private static void Main()
        {
            // Begin with this
            InitializeGlobals();

            string applicationPath = Assembly.GetExecutingAssembly().Location;
            applicationPath = Path.GetDirectoryName(applicationPath);

            TopshelfExitCode rc = HostFactory.Run(x =>
                                     {
                                         x.Service<RoomControlService>(sc =>
                                                                       {
                                                                           sc.ConstructUsing(() => new RoomControlService(applicationPath));
                                                                           sc.WhenStarted((service, hc) => service.Start(hc));
                                                                           sc.WhenStopped((service, hc) => service.Stop(hc));
                                                                       });
                                         x.SetDescription("Room control server proxy");
                                         x.SetDisplayName("Crave Room Control Server");
                                         x.SetServiceName("CraveRoomControlServer");
                                     });

            int exitCode = (int)Convert.ChangeType(rc, rc.GetTypeCode());
            Environment.ExitCode = exitCode;
        }


        private static void InitializeGlobals()
        {
            // Set the application information
            Dionysos.Global.ApplicationInfo = new Dionysos.ServiceProcess.ServiceInfo
            {
                ApplicationName = "CraveRoomControlServer",
                ApplicationVersion = "1.2020010904",
                BasePath = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location)
            };

            // Try to write the application details to the registry
            RegistryKey craveKey = Registry.LocalMachine.CreateSubKey(@"Software\Crave");
            if (craveKey == null)
            {
                return;
            }

            using (RegistryKey reqsrvKey = craveKey.CreateSubKey(Dionysos.Global.ApplicationInfo.ApplicationName))
            {
                if (reqsrvKey == null)
                {
                    return;
                }

                reqsrvKey.SetValue("Name", Dionysos.Global.ApplicationInfo.ApplicationName);
                reqsrvKey.SetValue("Version", Dionysos.Global.ApplicationInfo.ApplicationVersion);
                reqsrvKey.SetValue("BasePath", Dionysos.Global.ApplicationInfo.BasePath);
            }
        }
    }
}
