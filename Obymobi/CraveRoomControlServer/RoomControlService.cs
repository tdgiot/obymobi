﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using Crave.RoomControl;
using Crave.RoomControl.Inncom;
using Crave.RoomControl.Inncom.Config;
using Crave.RoomControl.Interel;
using CraveRoomControlServer.Config;
using CraveRoomControlServer.Logic;
using Dionysos;
using Dionysos.Configuration;
using Serilog;
using Serilog.Events;
using Topshelf;
using Topshelf.Hosts;
using Global = CraveRoomControlServer.Logic.Global;

namespace CraveRoomControlServer
{
    public class RoomControlService : ServiceControl
    {
        public static ILogger Logger { get; set; }

        private IIntegration integration;

        private CraveIntegrationServer integrationServer;

        public RoomControlService(string applicationPath)
        {
            TaskScheduler.UnobservedTaskException += TaskScheduler_UnobservedTaskException;

            Global.RootDirectory = applicationPath;
        }

        public bool Start(HostControl hostControl)
        {
            ConfigureLogging(hostControl is ConsoleRunHost);

            Logger.Information("======================================================");
            Logger.Information("Starting Crave Room Control Server {0}{1}", Dionysos.Global.ApplicationInfo.ApplicationVersion, (TestUtil.IsPcDeveloper) ? " (Developer Edition)" : "");
            Logger.Information("======================================================");
            Logger.Information("Base dir: {0}", Global.RootDirectory);
            Logger.Information("Version: {version}", Dionysos.Global.ApplicationInfo.ApplicationVersion);
            Logger.Verbose("Verbose logging: Enabled");
            Logger.Debug("Debug logging: Enabled");

            if (Global.RootDirectory == null)
            {
                Logger.Error("Could not determine application path!");
                return false;
            }

            Stopwatch startupTimer = new Stopwatch();
            startupTimer.Start();

            XmlConfigurationProvider provider = new XmlConfigurationProvider { ManualConfigFilePath = Path.Combine(Global.RootDirectory, "CraveRoomControlServer.config") };
            Dionysos.Global.ConfigurationProvider = provider;
            Dionysos.Global.ConfigurationInfo.Add(new CraveRoomControlServerConfigInfo());
            Dionysos.Global.ConfigurationInfo.Add(new InncomConfigInfo());
            Dionysos.Global.ConfigurationInfo.Add(new InterelServerConfigInfo());

            try
            {
                LoadIntegrations();
            }
            catch (Exception e)
            {
                Logger.Error(e, "Failed loading integration");
                return false;
            }

            this.integration.Start();
            this.integration.Synchronize();

            this.integrationServer = new CraveIntegrationServer(Logger, ConfigurationManager.GetInt(CraveRoomControlServerConfigConstants.ServerPort), this.integration);

            try
            {

                this.integrationServer.Start();
            }
            catch (Exception e)
            {
                Logger.Fatal(e, e.Message);
                return false;
            }

            startupTimer.Stop();

            if (startupTimer.ElapsedMilliseconds >= 2000)
            {
                Logger.Information("Server initialized in {0} seconds.", (startupTimer.ElapsedMilliseconds / 1000));
            }
            else
            {
                Logger.Information("Server initialized in {0} ms.", startupTimer.ElapsedMilliseconds);
            }

            return true;
        }

        public bool Stop(HostControl hostControl)
        {
            Logger.Information("Stopping services");

            if (this.integrationServer != null)
            {
                this.integrationServer.Stop();
            }

            if (this.integration != null)
            {
                this.integration.Stop();
            }

            return true;
        }

        private static void TaskScheduler_UnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs e)
        {
            if (e.Exception == null)
            {
                e.SetObserved();
                return;
            }

            Logger.Error(e.Exception, "Unobserved Exception");

            e.SetObserved();
        }

        private void ConfigureLogging(bool isConsole)
        {
            LoggerConfiguration loggerConfiguration = new LoggerConfiguration()
                                                      .MinimumLevel.Verbose()
                                                      .WriteTo.Trace()
                                                      .WriteTo.File("Logs\\log-.txt", rollingInterval: RollingInterval.Day, restrictedToMinimumLevel: LogEventLevel.Verbose);



            if (isConsole)
            {
                loggerConfiguration.WriteTo.Console();
            }

            Logger = loggerConfiguration.CreateLogger();
        }

        private void LoadIntegrations()
        {
            Logger.Information("Loading room control integrations");
            IntegrationType type;

            try
            {
                int integrationType = ConfigurationManager.GetInt(CraveRoomControlServerConfigConstants.Type);
                type = (IntegrationType)integrationType;
                Logger.Information(" * Integration Type: {type}", type);
            }
            catch (Exception)
            {
                throw new ApplicationException("Unable to parse integration type!");
            }

            if (type == IntegrationType.Interel)
            {
                InterelService interelService = new InterelService.Builder(Logger)
                                                .SetRSAKey(ConfigurationManager.GetString(InterelServerConfigInfo.Constants.PUBLIC_RSA_KEY))
                                                .SetInterelServerUrl(ConfigurationManager.GetString(InterelServerConfigInfo.Constants.INTEREL_SERVER))
                                                .SetIncomingWebHookUrl(ConfigurationManager.GetString(InterelServerConfigInfo.Constants.INCOMING_WEBHOOK_URL))
                                                .SetIncomingWebHookPort(ConfigurationManager.GetInt(InterelServerConfigInfo.Constants.INCOMING_WEBHOOK_PORT))
                                                .SetTokensFilename(ConfigurationManager.GetString(InterelServerConfigInfo.Constants.TOKENS_FILE))
                                                .SetOverrideIncomingWebHookUrl(ConfigurationManager.GetString(InterelServerConfigInfo.Constants.INCOMING_WEBHOOK_URL_OVERRIDE))
                                                .Build();
                interelService.Initialize();
                this.integration = interelService;
            }
            else if (type == IntegrationType.Inncom)
            {
                string serverHost = Dionysos.Global.ConfigurationProvider.GetString(InncomConfigConstants.InncomHost);
                int serverPort = Dionysos.Global.ConfigurationProvider.GetInt(InncomConfigConstants.InncomPort);
                this.integration = new InncomServer(Logger, serverHost, serverPort);
            }
            else
            {
                this.integration = new DummyIntegration(Logger);
            }

        }
    }
}
