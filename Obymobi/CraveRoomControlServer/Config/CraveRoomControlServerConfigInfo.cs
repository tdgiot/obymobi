﻿using Dionysos.Configuration;

namespace CraveRoomControlServer.Config
{
    public class CraveRoomControlServerConfigInfo : ConfigurationItemCollection
    {
        const string SECTION_NAME = "CraveRoomControlServer";

        public CraveRoomControlServerConfigInfo()
        {
            Add(new ConfigurationItem(SECTION_NAME, CraveRoomControlServerConfigConstants.Type, "Integration type (0=demo, 1=inncom, 2=interel)", 0, typeof(int)));
            Add(new ConfigurationItem(SECTION_NAME, CraveRoomControlServerConfigConstants.ServerPort, "Listening port of the room control server", 80, typeof(int)));
        }
    }
}
