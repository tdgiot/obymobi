﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model.Mobile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.Api.EntityConverters.Mobile
{
    public class CategorySuggestionEntityConverter : MobileEntityConverterBase<CategorySuggestionEntity, CategorySuggestionCollection, CategorySuggestion, Obymobi.Logic.Model.CategorySuggestion>
    {
        public override CategorySuggestion ConvertEntityToMobileModel(CategorySuggestionEntity entity)
        {
            CategorySuggestion categorySuggestion = null;

            if (entity.CategoryId.HasValue && (entity.ProductId.HasValue || entity.ProductCategoryId.HasValue))
            {
                categorySuggestion = new CategorySuggestion();
                if(entity.ProductCategoryId.HasValue)
                {
                    categorySuggestion.CategoryId = entity.ProductCategoryEntity.CategoryId;
                    categorySuggestion.ProductId = entity.ProductCategoryEntity.ProductId;
                }
                else
                {
                    categorySuggestion.CategoryId = entity.CategoryId ?? 0;
                    categorySuggestion.ProductId = entity.ProductId ?? 0;
                }
                categorySuggestion.Checkout = entity.Checkout;
            }

            return categorySuggestion;
        }

        public override Model.CategorySuggestion ConvertMobileModelToModel(CategorySuggestion mobileModel)
        {
            throw new NotImplementedException();
        }
    }
}
