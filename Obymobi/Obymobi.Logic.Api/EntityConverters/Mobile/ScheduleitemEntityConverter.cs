﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model.Mobile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.Api.EntityConverters.Mobile
{
    public class ScheduleitemEntityConverter : MobileEntityConverterBase<ScheduleitemEntity, ScheduleitemCollection, Scheduleitem, Obymobi.Logic.Model.Scheduleitem>
    {
        public override Scheduleitem ConvertEntityToMobileModel(ScheduleitemEntity entity)
        {
            Scheduleitem scheduleitem = new Scheduleitem();
            scheduleitem.ScheduleitemId = entity.ScheduleitemId;
            scheduleitem.DayOfweek = (int)entity.DayOfWeek.Value; // TODO
            scheduleitem.TimeStart = entity.TimeStart;
            scheduleitem.TimeEnd = entity.TimeEnd;

            return scheduleitem;
        }

        public override Model.Scheduleitem ConvertMobileModelToModel(Scheduleitem mobileModel)
        {
            throw new NotImplementedException();
        }
    }
}
