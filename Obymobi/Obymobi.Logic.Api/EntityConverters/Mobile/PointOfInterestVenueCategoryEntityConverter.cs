﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model.Mobile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.Api.EntityConverters.Mobile
{
    public class PointOfInterestVenueCategoryEntityConverter : MobileEntityConverterBase<PointOfInterestVenueCategoryEntity, PointOfInterestVenueCategoryCollection, VenueCategory, Obymobi.Logic.Model.VenueCategory>
    {
        public override VenueCategory ConvertEntityToMobileModel(PointOfInterestVenueCategoryEntity entity)
        {
            VenueCategoryEntity venueCategoryEntity = entity.VenueCategoryEntity;

            VenueCategory venueCategory = new VenueCategory();
            venueCategory.VenueCategoryId = venueCategoryEntity.VenueCategoryId;
            //venueCategory.Name = venueCategoryEntity.Name;

            return venueCategory;
        }

        public override Model.VenueCategory ConvertMobileModelToModel(VenueCategory mobileModel)
        {
            throw new NotImplementedException();
        }
    }
}
