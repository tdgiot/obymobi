﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model.Mobile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.Api.EntityConverters.Mobile
{
    public class AmenityEntityConverter : MobileEntityConverterBase<AmenityEntity, AmenityCollection, Amenity, Obymobi.Logic.Model.Amenity>
    {
        public override Amenity ConvertEntityToMobileModel(AmenityEntity entity)
        {
            Amenity amenity = new Amenity();
            amenity.AmenityId = entity.AmenityId;
            amenity.Name = entity.Name;

            // Custom texts
            amenity.CustomTexts = new CustomTextEntityConverter().ConvertEntityCollectionToMobileModelArray(entity.CustomTextCollection);

            return amenity;
        }

        public override Model.Amenity ConvertMobileModelToModel(Amenity mobileModel)
        {
            throw new NotImplementedException();
        }
    }
}
