﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model.Mobile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.Api.EntityConverters.Mobile
{
    public class CustomTextEntityConverter : MobileEntityConverterBase<CustomTextEntity, CustomTextCollection, CustomText, Obymobi.Logic.Model.CustomText>
    {
        private readonly string parent;

        public CustomTextEntityConverter(string parent = "")
        {
            this.parent = parent;
        }

        public override CustomText ConvertEntityToMobileModel(CustomTextEntity entity)
        {
            CustomText customText = new CustomText();
            customText.CustomTextId = entity.CustomTextId;
            customText.Type = (int)entity.Type;
            customText.Text = entity.Text;
            customText.CultureCode = entity.CultureCode;
            customText.ForeignKey = entity.RelatedEntityId;

            return customText;
        }        

        public override Model.CustomText ConvertMobileModelToModel(CustomText mobileModel)
        {
            Model.CustomText model = new Model.CustomText();
            model.CustomTextId = mobileModel.CustomTextId;
            model.Type = mobileModel.Type;
            model.Text = mobileModel.Text;
            model.CultureCode = mobileModel.CultureCode;
            model.ForeignKey = mobileModel.ForeignKey;
            model.Parent = this.parent;

            return model;            
        }
    }
}
