﻿using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic.Cms;
using Obymobi.Logic.HelperClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Dionysos;
using MarkdownSharp;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;
using MarkdownSharp.Extensions;

namespace Obymobi.Logic.Api.EntityConverters.Mobile
{
    
    public class PageElementEntityConverter : MobileEntityConverterBase<PageElementEntity, PageElementCollection, Obymobi.Logic.Model.Mobile.PageElement, Obymobi.Logic.Model.PageElement> // ModelBase because there is no normal model for Menu
    {
        #region Fields

        private DeviceType deviceType = DeviceType.Unknown;        

        #endregion

        #region Constructors

        public PageElementEntityConverter(DeviceType deviceType)
        {
            this.deviceType = deviceType;
        }

        #endregion

        #region Methods

        public override Model.Mobile.PageElement ConvertEntityToMobileModel(PageElementEntity entity)
        {
            Model.Mobile.PageElement pageElement = new Model.Mobile.PageElement();
            pageElement.PageElementId = entity.PageElementId;            
            pageElement.PageId = entity.PageId;
            pageElement.SystemName = entity.SystemName;
            pageElement.PageElementType = entity.PageElementType;
            pageElement.CultureCode = entity.CultureCode;
            pageElement.LanguageCode = entity.CultureCode.IsNullOrWhiteSpace() ? entity.CultureCode : Obymobi.Culture.Mappings[entity.CultureCode].Language.CodeAlpha2.ToLowerInvariant();

            string stringValue1 = entity.StringValue1;

            if (entity.PageElementType == (int)PageElementType.MultiLineText)
            {
                Markdown markdown = MarkdownHelper.GetDefaultInstance();

                stringValue1 = HttpUtility.HtmlEncode(stringValue1);
                stringValue1 = markdown.Transform(stringValue1);

                if (stringValue1.EndsWith("\n"))
                {
                    stringValue1 = stringValue1.Substring(0, stringValue1.Length - 1);
                }                
            }

            pageElement.StringValue1 = stringValue1;
            pageElement.StringValue2 = entity.StringValue2;
            pageElement.StringValue3 = entity.StringValue3;
            pageElement.StringValue4 = entity.StringValue4;
            pageElement.StringValue5 = entity.StringValue5;
            pageElement.BoolValue1 = entity.BoolValue1;
            pageElement.BoolValue2 = entity.BoolValue2;
            pageElement.BoolValue3 = entity.BoolValue3;
            pageElement.BoolValue4 = entity.BoolValue4;
            pageElement.BoolValue5 = entity.BoolValue5;
            pageElement.IntValue1 = entity.IntValue1;
            pageElement.IntValue2 = entity.IntValue2;
            pageElement.IntValue3 = entity.IntValue3;
            pageElement.IntValue4 = entity.IntValue4;
            pageElement.IntValue5 = entity.IntValue5;

            // Root categories            
            pageElement.Media = new MediaEntityConverter(false, this.deviceType, "PAGES_PAGEELEMENTS").ConvertEntityCollectionToMobileModelArray(entity.MediaCollection);            

            return pageElement;
        }

        public override Model.Mobile.PageElement[] ConvertEntityCollectionToMobileModelArray(PageElementCollection entityCollection)
        {
            var pageElements = new List<Model.Mobile.PageElement>();

            foreach (var pageElement in entityCollection)
            {
                pageElements.Add(this.ConvertEntityToMobileModel(pageElement));
            }

            return pageElements.ToArray();
        }

        public override Model.PageElement ConvertMobileModelToModel(Model.Mobile.PageElement mobileModel)
        {
            var pageElement = new Model.PageElement();
            pageElement.PageElementId = mobileModel.PageElementId;
            pageElement.PageId = mobileModel.PageId;
            pageElement.PageTemplateElementId = mobileModel.PageTemplateElementId;
            pageElement.LanguageCode = mobileModel.LanguageCode;
            pageElement.SystemName = mobileModel.SystemName;
            pageElement.PageElementType = mobileModel.PageElementType;

            pageElement.StringValue1 = mobileModel.StringValue1;
            pageElement.StringValue2 = mobileModel.StringValue2;
            pageElement.StringValue3 = mobileModel.StringValue3;
            pageElement.StringValue4 = mobileModel.StringValue4;
            pageElement.StringValue5 = mobileModel.StringValue5;
            pageElement.BoolValue1 = mobileModel.BoolValue1;
            pageElement.BoolValue2 = mobileModel.BoolValue2;
            pageElement.BoolValue3 = mobileModel.BoolValue3;
            pageElement.BoolValue4 = mobileModel.BoolValue4;
            pageElement.BoolValue5 = mobileModel.BoolValue5;
            pageElement.IntValue1 = mobileModel.IntValue1;
            pageElement.IntValue2 = mobileModel.IntValue2;
            pageElement.IntValue3 = mobileModel.IntValue3;
            pageElement.IntValue4 = mobileModel.IntValue4;
            pageElement.IntValue5 = mobileModel.IntValue5;

            // Root categories            
            if (mobileModel.Media != null)
            {
                var mediaConverter = new MediaEntityConverter(false, this.deviceType, "PAGES_PAGEELEMENTS");
                pageElement.Media = mobileModel.Media.Select(mediaConverter.ConvertMobileModelToModel).ToArray();
            }            

            return pageElement;
        }

        #endregion
    }
}
