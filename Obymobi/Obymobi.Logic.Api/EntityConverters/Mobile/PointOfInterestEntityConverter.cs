﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model.Mobile;
using Dionysos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.Api.EntityConverters.Mobile
{
    public class PointOfInterestEntityConverter : MobileEntityConverterBase<PointOfInterestEntity, PointOfInterestCollection, Company, Obymobi.Logic.Model.Company>
    {
        #region Fields

        private bool minified = false;
        private DeviceType deviceType = DeviceType.Unknown;
        private bool tablet = false;

        #endregion

        #region Constructors

        public PointOfInterestEntityConverter(bool minified, DeviceType deviceType, bool tablet)
        {
            this.minified = minified;
            this.deviceType = deviceType;
            this.tablet = tablet;
        }

        public override Company ConvertEntityToMobileModel(PointOfInterestEntity entity)
        {
            Company company = new Company();
            company.CompanyId = entity.PointOfInterestId * -1;
            company.Name = entity.Name;
            company.Latitude = entity.Latitude;
            company.Longitude = entity.Longitude;
            company.Zipcode = entity.Zipcode;
            company.City = entity.City;
            company.IsPointOfInterest = true;
            company.CostIndicationType = entity.CostIndicationType;
            company.CostIndicationValue = entity.CostIndicationValue;            
            company.CompanyDataLastModifiedTicks = entity.LastModifiedUTC.Ticks;
            company.CompanyMediaLastModifiedTicks = entity.LastModifiedUTC.Ticks;
            company.Country = entity.CountryEntity.Name;
            company.Currency = entity.CurrencyCode;
            company.TimeZone = entity.TimeZoneOlsonId;
            company.CultureCode = entity.CultureCode;

            // Venue categories
            company.VenueCategoryIds = entity.PointOfInterestVenueCategoryCollection.Select(x => x.VenueCategoryId).ToArray();

            // Amenities
            company.AmenityIds = entity.PointOfInterestAmenityCollection.Select(x => x.AmenityId).ToArray();

            // Business Hours
            company.BusinesshoursIntermediate = BusinesshoursHelper.ConvertBusinesshoursToIntermediateFormat(entity.BusinesshoursCollection);

            // Custom texts
            company.CustomTexts = new CustomTextEntityConverter().ConvertEntityCollectionToMobileModelArray(entity.CustomTextCollection);

            // Media
            company.Media = new MediaEntityConverter(this.minified, this.deviceType).ConvertEntityCollectionToMobileModelArray(entity.MediaCollection);

            if (!this.minified)
            {
                company.Addressline1 = entity.Addressline1;
                company.Addressline2 = entity.Addressline2;
                company.Addressline3 = entity.Addressline3;
                company.Telephone = entity.Telephone;
                company.ActionButtonId = entity.ActionButtonId;
                company.ActionButtonUrlMobile = entity.ActionButtonUrlMobile;
                company.ActionButtonUrlTablet = entity.ActionButtonUrlTablet;

                // Include the UIModes            
                if (this.tablet && entity.TabletUIModeEntity != null && !entity.TabletUIModeEntity.IsNew)
                {
                    company.PointOfInterestUIMode = new UIModeEntityConverter().ConvertEntityToMobileModel(entity.TabletUIModeEntity);
                }
                else if (!this.tablet && entity.MobileUIModeEntity != null && !entity.MobileUIModeEntity.IsNew)
                {
                    company.PointOfInterestUIMode = new UIModeEntityConverter().ConvertEntityToMobileModel(entity.MobileUIModeEntity);
                }
            }

            return company;
        }

        public override Model.Company ConvertMobileModelToModel(Company mobileModel)
        {
            throw new NotImplementedException();
        }

        public Model.Company ConvertMobileModelToModel(Company companyMobileModel, PointOfInterestEntity poiEntity)
        {
            var company = new Model.Company();
            company.CompanyId = companyMobileModel.CompanyId;
            company.Name = companyMobileModel.Name;
            company.Latitude = companyMobileModel.Latitude;
            company.Longitude = companyMobileModel.Longitude;
            company.Zipcode = companyMobileModel.Zipcode;
            company.City = companyMobileModel.City;
            company.CostIndicationType = companyMobileModel.CostIndicationType.GetValueOrDefault();
            company.CostIndicationValue = companyMobileModel.CostIndicationValue;
            company.CompanyDataLastModifiedTicks = companyMobileModel.CompanyDataLastModifiedTicks;
            company.CompanyMediaLastModifiedTicks = companyMobileModel.CompanyMediaLastModifiedTicks;
            company.Country = companyMobileModel.Country;
            company.Currency = companyMobileModel.Currency;
            company.Description = poiEntity.Description;
            company.DescriptionSingleLine = poiEntity.DescriptionSingleLine;
            company.BusinesshoursIntermediate = companyMobileModel.BusinesshoursIntermediate;
            company.TimeZone = companyMobileModel.TimeZone;

            var venueCategories = new List<Model.VenueCategory>();
            foreach(var venueCategoryEntity in poiEntity.PointOfInterestVenueCategoryCollection)
            {
                venueCategories.Add(VenueCategoryHelper.ConvertEntityToModel(venueCategoryEntity.VenueCategoryEntity));
            }
            company.VenueCategories = venueCategories.ToArray();

            var amenities = new List<Model.Amenity>();
            foreach (var amenityEntity in poiEntity.PointOfInterestAmenityCollection)
            {
                amenities.Add(AmenityHelper.ConvertEntityToModel(amenityEntity.AmenityEntity));
            }
            company.Amenities = amenities.ToArray();

            // Include the UIModes            
            if (this.tablet && poiEntity.TabletUIModeEntity != null && !poiEntity.TabletUIModeEntity.IsNew)
            {
                company.UIModes = new Model.UIMode[] { UIModeHelper.CreateUIModeModelFromEntity(poiEntity.TabletUIModeEntity) };
            }
            else if (!this.tablet && poiEntity.MobileUIModeEntity != null && !poiEntity.MobileUIModeEntity.IsNew)
            {
                company.UIModes = new Model.UIMode[] { UIModeHelper.CreateUIModeModelFromEntity(poiEntity.MobileUIModeEntity) };
            }

            var mediaConverter = new MediaEntityConverter(this.minified, this.deviceType);
            company.Media = companyMobileModel.Media.Select(mediaConverter.ConvertMobileModelToModel).ToArray();
            
            company.Addressline1 = companyMobileModel.Addressline1;
            company.Addressline2 = companyMobileModel.Addressline2;
            company.Addressline3 = companyMobileModel.Addressline3;

            var customTextConverter = new CustomTextEntityConverter();
            company.CustomTexts = companyMobileModel.CustomTexts.Select(customTextConverter.ConvertMobileModelToModel).ToArray();

            return company;
        }

        #endregion

        public Model.Company ConvertEntityToModel(PointOfInterestEntity poiEntity)
        {
            Company company = ConvertEntityToMobileModel(poiEntity);
            return ConvertMobileModelToModel(company, poiEntity);
        }
    }
}
