﻿using Dionysos;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using System.Collections.Generic;
using System.Linq;
using Obymobi.Logic.Cms;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Page = Obymobi.Logic.Model.Mobile.Page;
using PageElement = Obymobi.Logic.Model.Mobile.PageElement;

namespace Obymobi.Logic.Api.EntityConverters.Mobile
{
    public class PageEntityConverter : MobileEntityConverterBase<PageEntity, PageCollection, Obymobi.Logic.Model.Mobile.Page, Obymobi.Logic.Model.Page> // ModelBase because there is no normal model for Menu
    {
        #region Fields

        private DeviceType deviceType = DeviceType.Unknown;

        #endregion

        #region Constructors

        public PageEntityConverter(DeviceType deviceType)
        {
            this.deviceType = deviceType;
        }

        #endregion

        #region Methods

        public override Model.Mobile.Page ConvertEntityToMobileModel(PageEntity entity)
        {
            Page page = new Model.Mobile.Page();
            page.PageId = entity.PageId;
            page.SiteId = entity.SiteId;
            page.PageTemplateId = entity.PageTemplateId ?? -1;
            page.Name = entity.Name;
            page.ParentPageId = entity.ParentPageId ?? -1;            
            page.PageType = entity.PageType;
            page.SortOrder = entity.SortOrder;

            // Set the translated page name
            List<CustomTextEntity> pageNames = entity.CustomTextCollection.Where(x => x.Type == CustomTextType.PageName).ToList();
            if (pageNames.Count > 0 && !pageNames[0].Text.IsNullOrWhiteSpace())
            {
                page.Name = pageNames[0].Text;
            }                   
            else if (entity.PageTemplateId.HasValue)
            {
                List<CustomTextEntity> pageTemplateNames = entity.PageTemplateEntity.CustomTextCollection.Where(x => x.Type == CustomTextType.PageTemplateName).ToList();
                if (pageTemplateNames.Count > 0 && !pageTemplateNames[0].Text.IsNullOrWhiteSpace())
                {
                    page.Name = pageTemplateNames[0].Text;
                }                
                else if (entity.Name.IsNullOrWhiteSpace())
                {
                    page.Name = entity.PageTemplateEntity.Name;
                }                
            }
            
            // Page template
            if (!entity.PageTemplateId.HasValue)
            {
                // Page elements
                page.PageElements = new PageElementEntityConverter(this.deviceType).ConvertEntityCollectionToMobileModelArray(entity.PageElementCollection);

                // Custom texts
                page.CustomTexts = new CustomTextEntityConverter().ConvertEntityCollectionToMobileModelArray(entity.CustomTextCollection);
            }
            else
            {
                // Page elements
                PageTypeBase pageType = PageTypeHelper.GetPageTypeInstance(entity.PageTypeAsEnum);
                pageType.DataSource = entity;

                // The datasource of pageTypeElements can be PageElementEntities or PageTemplateElementEntities
                IEnumerable<IPageTypeElementData> pageElementDataSources = pageType.PageTypeElements.Select(pte => pte.DataSource);

                PageElementCollection pageElements = new PageElementCollection();
                PageTemplateElementCollection pageTemplateElements = new PageTemplateElementCollection();
                foreach (IPageTypeElementData element in pageElementDataSources)
                {
                    if (element is PageElementEntity)
                        pageElements.Add((PageElementEntity)element);
                    else if (element is PageTemplateElementEntity)
                        pageTemplateElements.Add((PageTemplateElementEntity)element);
                }

                List<PageElement> combinedPageElements = new List<Model.Mobile.PageElement>();
                combinedPageElements.AddRange(new PageElementEntityConverter(this.deviceType).ConvertEntityCollectionToMobileModelArray(pageElements));
                combinedPageElements.AddRange(new PageTemplateElementEntityConverter(this.deviceType, entity).ConvertEntityCollectionToMobileModelArray(pageTemplateElements));
                page.PageElements = combinedPageElements.ToArray();

                // Custom texts
                List<Obymobi.Logic.Model.Mobile.CustomText> customTexts = new List<Obymobi.Logic.Model.Mobile.CustomText>();

                CustomTextType[] pageCustomTextTypes = new[] {CustomTextType.PageName};
                CustomTextType[] pageTemplateCustomTextTypes = new[] {CustomTextType.PageTemplateName};

                CustomTextEntityConverter converter = new CustomTextEntityConverter();
                foreach (SiteCultureEntity siteCulture in entity.SiteEntity.SiteCultureCollection)
                {
                    for (int i = 0; i < pageCustomTextTypes.Length; i++)
                    {
                        // Get custom text from page
                        CustomTextEntity customTextEntity = entity.CustomTextCollection.SingleOrDefault(x => x.CultureCode == siteCulture.CultureCode && x.Type == pageCustomTextTypes[i]);
                        if (customTextEntity == null)
                        {
                            // Get custom text from page template
                            customTextEntity = entity.PageTemplateEntity.CustomTextCollection.SingleOrDefault(x => x.CultureCode == siteCulture.CultureCode && x.Type == pageTemplateCustomTextTypes[i]);
                            if (customTextEntity != null)
                            {
                                customTextEntity.PageTemplateId = null;
                                customTextEntity.PageId = entity.PageId;
                                customTextEntity.Type = pageCustomTextTypes[i];
                            }
                        }

                        if (customTextEntity != null)
                        {
                            customTexts.Add(converter.ConvertEntityToMobileModel(customTextEntity));
                        }
                    }
                }

                page.CustomTexts = customTexts.ToArray();
            }

            // Page media
            if (entity.MediaCollection.Count > 0 || !entity.PageTemplateId.HasValue)
                page.Media = new MediaEntityConverter(false, this.deviceType, "PAGES").ConvertEntityCollectionToMobileModelArray(entity.MediaCollection);
            else
                page.Media = new MediaEntityConverter(false, this.deviceType, "PAGES").ConvertEntityCollectionToMobileModelArray(entity.PageTemplateEntity.MediaCollection);

            // Page attachments
            if (entity.AttachmentCollection.Count > 0 || !entity.PageTemplateId.HasValue)
                page.Attachments = new AttachmentEntityConverter(this.deviceType).ConvertEntityCollectionToMobileModelArray(entity.AttachmentCollection);
            else
                page.Attachments = new AttachmentEntityConverter(this.deviceType).ConvertEntityCollectionToMobileModelArray(entity.PageTemplateEntity.AttachmentCollection);
            
            // Create extra pagelement if it's a venue page and company/poi have venue page description
            if (entity.PageTypeAsEnum == PageType.Venue)
            {
                List<string> pageCultureCodes = page.CustomTexts.Where(x => !string.IsNullOrWhiteSpace(x.CultureCode)).Select(x => x.CultureCode).Distinct().ToList();
                if (pageCultureCodes.Count == 0)
                {
                    pageCultureCodes.Add("en-GB");
                }

                foreach (PageElement pageElement in page.PageElements)
                {
                    if (pageElement.PageElementType != (int)PageElementType.Venue)
                        continue;

                    PageElement pageElementEntity = new Model.Mobile.PageElement();
                    pageElementEntity.PageId = entity.PageId;
                    pageElementEntity.PageElementType = (int)PageElementType.MultiLineText;

                    if (pageElement.IntValue1.GetValueOrDefault(0) > 0) // Company
                    {
                        Company company = CompanyHelper.GetCompanyByCompanyId(pageElement.IntValue1.GetValueOrDefault());

                        List<CustomText> companyCustomTexts = company.CustomTexts.Where(x => x.Type == (int)CustomTextType.CompanyVenuePageDescription && pageCultureCodes.Contains(x.CultureCode)).ToList();
                        if (companyCustomTexts.Count > 0)
                        {
                            pageElementEntity.StringValue1 = companyCustomTexts[0].Text;
                        }                        
                    }
                    else if (pageElement.IntValue2.GetValueOrDefault(0) > 0) // POI
                    {
                        PrefetchPath prefetch = new PrefetchPath(EntityType.PointOfInterestEntity);
                        prefetch.Add(PointOfInterestEntityBase.PrefetchPathCustomTextCollection);

                        PointOfInterestEntity poiEntity = new PointOfInterestEntity(pageElement.IntValue2.GetValueOrDefault(), prefetch);
                        if (!poiEntity.IsNew && poiEntity.CustomTextCollection.Count > 0)
                        {
                            List<CustomTextEntity> poiCustomTexts = poiEntity.CustomTextCollection.Where(x => x.Type == CustomTextType.PointOfInterestVenuePageDescription && pageCultureCodes.Contains(x.CultureCode)).ToList();
                            if (poiCustomTexts.Count > 0)
                            {
                                pageElementEntity.StringValue1 = poiCustomTexts[0].Text;
                            }                            
                        }
                    }

                    if (!pageElementEntity.StringValue1.IsNullOrWhiteSpace())
                    {
                        // Add page element to list if we have a value
                        List<PageElement> l = page.PageElements.ToList();
                        l.Add(pageElementEntity);
                        page.PageElements = l.ToArray();
                    }

                    break;
                }
            }

            return page;
        }

        public override Model.Mobile.Page[] ConvertEntityCollectionToMobileModelArray(PageCollection entityCollection)
        {
            List<Model.Mobile.Page> modelList = new List<Model.Mobile.Page>();

            if (entityCollection != null && entityCollection.Count > 0)
            {
                foreach (PageEntity entity in entityCollection)
                {
                    if (entity != null && entity.Visible) // Page should have Visible flag set to True
                    {
                        Model.Mobile.Page model = this.ConvertEntityToMobileModel(entity);
                        if (model != null)
                            modelList.Add(model);
                    }
                }
            }

            return modelList.ToArray();
        }

        public override Model.Page ConvertMobileModelToModel(Model.Mobile.Page mobileModel)
        {
            var page = new Model.Page();
            page.Name = mobileModel.Name;
            page.PageId = mobileModel.PageId;
            page.SiteId = mobileModel.SiteId;
            page.PageTemplateId = mobileModel.PageTemplateId;
            page.ParentPageId = mobileModel.ParentPageId;
            page.PageType = mobileModel.PageType;
            page.SortOrder = mobileModel.SortOrder;

            // Root categories
            var pageElementConverter = new PageElementEntityConverter(this.deviceType);
            page.PageElements = mobileModel.PageElements.Select(pageElementConverter.ConvertMobileModelToModel).ToArray();            

            var mediaConverter = new MediaRatioTypeMediaEntityConverter(false, null, this.deviceType, "PAGES");
            page.Media = mobileModel.Media.Select(mediaConverter.ConvertMobileModelToModel).ToArray();

            var attachmentConverter = new AttachmentEntityConverter(this.deviceType);
            page.Attachments = mobileModel.Attachments.Select(attachmentConverter.ConvertMobileModelToModel).ToArray();

            var customTextConverter = new CustomTextEntityConverter();
            page.CustomTexts = mobileModel.CustomTexts.Select(customTextConverter.ConvertMobileModelToModel).ToArray();

            return page;
        }

        #endregion
    }
}
