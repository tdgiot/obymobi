﻿using Dionysos;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Model.Mobile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.Logic.Api.EntityConverters.Mobile
{
    public class DeliverypointgroupEntityConverter : MobileEntityConverterBase<DeliverypointgroupEntity, DeliverypointgroupCollection, Deliverypointgroup, Obymobi.Logic.Model.Deliverypointgroup>
    {
        #region Fields

        private bool minified = false;
        private DeviceType deviceType = DeviceType.Unknown;
        private bool tablet = false;

        #endregion

        #region Constructors

        public DeliverypointgroupEntityConverter(bool minified, DeviceType deviceType, bool tablet)
        {
            this.minified = minified;
            this.deviceType = deviceType;
            this.tablet = tablet;
        }

        #endregion

        #region Methods

        public override Deliverypointgroup ConvertEntityToMobileModel(DeliverypointgroupEntity entity)
        {
            Dictionary<CustomTextType, string> customTexts = entity.CustomTextCollection.ToCultureCodeSpecificDictionary(entity.CompanyEntity.CultureCode);

            Deliverypointgroup deliverypointgroup = new Deliverypointgroup();
            deliverypointgroup.DeliverypointgroupId = entity.DeliverypointgroupId;
            deliverypointgroup.CompanyId = entity.CompanyId;
            deliverypointgroup.Name = entity.Name;
            deliverypointgroup.OrderHistoryDialogEnabled = entity.OrderHistoryDialogEnabled;
            if (entity.MenuId.HasValue)
            {
                deliverypointgroup.MenuId = entity.MenuId.Value;
            }
            deliverypointgroup.DeliverypointCaption = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupDeliverypointCaption);
            deliverypointgroup.HomepageSlideshowInterval = entity.HomepageSlideshowInterval;
            deliverypointgroup.GeoFencingEnabled = entity.GeoFencingEnabled;
            deliverypointgroup.GeoFencingRadius = entity.GeoFencingRadius;
            deliverypointgroup.HideCompanyDetails = entity.HideCompanyDetails;

            // Custom texts
            deliverypointgroup.CustomTexts = new CustomTextEntityConverter().ConvertEntityCollectionToMobileModelArray(entity.CustomTextCollection);

            if (!this.minified)
            {
                // Deliverypoints
                deliverypointgroup.Deliverypoints = new DeliverypointEntityConverter().ConvertEntityCollectionToMobileModelArray(entity.DeliverypointCollection);

                // UI modes
                UIModeEntity uiModeEntity = null;
                if(this.tablet && entity.TabletUIModeId.HasValue)
                {
                    uiModeEntity = entity.TabletUIModeEntity;
                }
                else if(!this.tablet && entity.MobileUIModeId.HasValue)
                {
                    uiModeEntity = entity.MobileUIModeEntity;
                }                

                if (uiModeEntity != null)
                {
                    UIModeCollection uiModes = new UIModeCollection();
                    uiModes.Add(uiModeEntity);
                    deliverypointgroup.UIModes = new UIModeEntityConverter().ConvertEntityCollectionToMobileModelArray(uiModes);
                }
            }

			entity.MediaCollection.Sort(MediaFields.SortOrder.FieldIndex, System.ComponentModel.ListSortDirection.Ascending);
            deliverypointgroup.Media = new MediaEntityConverter(this.minified, this.deviceType).ConvertEntityCollectionToMobileModelArray(entity.MediaCollection);            

            return deliverypointgroup;
        }

        public override Deliverypointgroup[] ConvertEntityCollectionToMobileModelArray(DeliverypointgroupCollection entityCollection)
        {
            List<Deliverypointgroup> modelList = new List<Deliverypointgroup>();

            if (entityCollection != null && entityCollection.Count > 0)
            {
                foreach (DeliverypointgroupEntity entity in entityCollection)
                {
                    // Check whether the DPG is available for mobile ordering 
                    // and whether it has a menu
                    if (entity != null && entity.AvailableOnObymobi && entity.MenuId.HasValue)
                    {
                        Deliverypointgroup model = this.ConvertEntityToMobileModel(entity);
                        if (model != null)
                            modelList.Add(model);
                    }
                }
            }

            return modelList.ToArray();
        }

        public override Model.Deliverypointgroup ConvertMobileModelToModel(Deliverypointgroup mobileModel)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
