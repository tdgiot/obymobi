﻿using Dionysos;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model.Mobile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.Api.EntityConverters.Mobile
{
    public class CompanyCultureEntityConverter : MobileEntityConverterBase<CompanyCultureEntity, CompanyCultureCollection, CompanyCulture, Obymobi.Logic.Model.CompanyCulture>
    {
        public override CompanyCulture ConvertEntityToMobileModel(CompanyCultureEntity entity)
        {
            CompanyCulture companyCulture = new CompanyCulture();
            companyCulture.CompanyCultureId = entity.CompanyCultureId;
            companyCulture.CultureCode = entity.CultureCode;
            if (!entity.CultureCode.IsNullOrWhiteSpace())
            {
                companyCulture.Name = Culture.Mappings[entity.CultureCode].Language.Name;
            }

            return companyCulture;
        }

        public override Model.CompanyCulture ConvertMobileModelToModel(CompanyCulture mobileModel)
        {
            throw new NotImplementedException();
        }
    }
}
