﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model.Mobile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.Api.EntityConverters.Mobile
{
    public class AccessCodeEntityConverter : MobileEntityConverterBase<AccessCodeEntity, AccessCodeCollection, AccessCode, Obymobi.Logic.Model.AccessCode>
    {
        public override AccessCode ConvertEntityToMobileModel(AccessCodeEntity entity)
        {
            AccessCode accessCode = new AccessCode();
            accessCode.AccessCodeId = entity.AccessCodeId;
            accessCode.Code = entity.Code;
            accessCode.Type = (int)entity.Type;
            accessCode.AnalyticsEnabled = entity.AnalyticsEnabled;

            return accessCode;
        }

        public override Model.AccessCode ConvertMobileModelToModel(AccessCode mobileModel)
        {
            throw new NotImplementedException();
        }
    }
}
