﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model.Mobile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Logic.Api.EntityConverters.Mobile
{
    public class CompanyEntityConverter : MobileEntityConverterBase<CompanyEntity, CompanyCollection, Company, Obymobi.Logic.Model.Company>
    {
        #region Fields

        private bool minified = false;
        private DeviceType deviceType = DeviceType.Unknown;
        private bool tablet = false;

        #endregion

        #region Constructors

        public CompanyEntityConverter(bool minified, DeviceType deviceType, bool tablet)
        {
            this.minified = minified;
            this.deviceType = deviceType;
            this.tablet = tablet;
        }

        #endregion

        public override Company ConvertEntityToMobileModel(CompanyEntity entity)
        {
            Company company = new Company();
            company.CompanyId = entity.CompanyId;
            company.Name = entity.Name;
            company.Zipcode = entity.Zipcode;
            company.City = entity.City;
            company.Country = entity.CountryEntity.Name;
            company.CompanyDataLastModifiedTicks = entity.CompanyDataLastModifiedUTC.Ticks;
            company.CompanyMediaLastModifiedTicks = entity.CompanyMediaLastModifiedUTC.Ticks;
            company.Latitude = entity.Latitude;
            company.Longitude = entity.Longitude;
            company.Currency = entity.CurrencyCode;
            company.CultureCode = entity.CultureCode;
            company.CostIndicationType = (int)entity.CostIndicationType;
            company.CostIndicationValue = entity.CostIndicationValue;
            company.TimeZone = entity.TimeZoneOlsonId;

            // Venue categories
            company.VenueCategoryIds = entity.CompanyVenueCategoryCollection.Select(x => x.VenueCategoryId).ToArray();

            // Amenities
            company.AmenityIds = entity.CompanyAmenityCollection.Select(x => x.AmenityId).ToArray();

            // Media
            company.Media = new MediaEntityConverter(this.minified, this.deviceType).ConvertEntityCollectionToMobileModelArray(entity.MediaCollection);

            // Business Hours
            company.BusinesshoursIntermediate = BusinesshoursHelper.ConvertBusinesshoursToIntermediateFormat(entity.BusinesshoursCollection);

            // Company cultures
            company.CompanyCultures = new CompanyCultureEntityConverter().ConvertEntityCollectionToMobileModelArray(entity.CompanyCultureCollection);

            // Company currencies
            company.CompanyCurrencys = new CompanyCurrencyEntityConverter().ConvertEntityCollectionToMobileModelArray(entity.CompanyCurrencyCollection);            

            if (!this.minified)
            {
                company.Addressline1 = entity.Addressline1;
                company.Addressline2 = entity.Addressline2;
                company.Addressline3 = entity.Addressline3;
                company.Telephone = entity.Telephone;
                company.GoogleAnalyticsId = entity.GoogleAnalyticsId;
                company.GeoFencingEnabled = entity.GeoFencingEnabled;
                company.GeoFencingRadius = entity.GeoFencingRadius;
                company.AllowFreeText = entity.AllowFreeText;
                company.ActionButtonId = entity.ActionButtonId;
                company.ActionButtonUrlMobile = entity.ActionButtonUrlMobile;
                company.ActionButtonUrlTablet = entity.ActionButtonUrlTablet;
                company.MenuDataLastModifiedTicks = entity.MenuDataLastModifiedUTC.Ticks;
                company.MenuMediaLastModifiedTicks = entity.MenuMediaLastModifiedUTC.Ticks;
                company.DeliverypointDataLastModifiedTicks = entity.DeliverypointDataLastModifiedUTC.Ticks;
                company.CultureCode = entity.CultureCode;

                // Deliverypoint groups > But only the ones for Mobile            
                DeliverypointgroupCollection dpgsForMobile = new DeliverypointgroupCollection();
                dpgsForMobile.AddRange(entity.DeliverypointgroupCollection.Where(x => x.AvailableOnObymobi));
                company.Deliverypointgroups = new DeliverypointgroupEntityConverter(this.minified, this.deviceType, this.tablet).ConvertEntityCollectionToMobileModelArray(dpgsForMobile);

                // Schedules
                company.Schedules = new ScheduleEntityConverter().ConvertEntityCollectionToMobileModelArray(entity.ScheduleCollection);

                // Custom texts
                company.CustomTexts = new CustomTextEntityConverter().ConvertEntityCollectionToMobileModelArray(entity.CustomTextCollection);
            }

            return company;
        }

        public override Company[] ConvertEntityCollectionToMobileModelArray(CompanyCollection entityCollection)
        {
            Company[] toReturn = base.ConvertEntityCollectionToMobileModelArray(entityCollection);

            return toReturn;
        }

        public override Model.Company ConvertMobileModelToModel(Company mobileModel)
        {
            throw new NotImplementedException();
        }
    }
}
