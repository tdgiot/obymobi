﻿using Dionysos;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model.Mobile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.Api.EntityConverters.Mobile
{
    public class CustomerEntityConverter : MobileEntityConverterBase<CustomerEntity, CustomerCollection, Customer, Obymobi.Logic.Model.Customer>
    {
        #region Constructors

        public CustomerEntityConverter()
        {
        }

        #endregion

        #region Fields

        public override Customer ConvertEntityToMobileModel(CustomerEntity entity)
        {
            Customer customer = new Customer();
            customer.CustomerId = entity.CustomerId;
            customer.Salt = entity.CommunicationSalt;            
            customer.EmailAddress = entity.Email;
            customer.Phonenumber = entity.Phonenumber;
            customer.Firstname = entity.Firstname;
            customer.Lastname = entity.Lastname;
            customer.LastnamePrefix = entity.LastnamePrefix;
            customer.BirthdateTimestamp = entity.Birthdate.HasValue ? DateTimeUtil.DateTimeToSimpleDateStamp(entity.Birthdate.Value) : string.Empty;
            customer.Gender = entity.Gender.HasValue ? (entity.Gender.Value ? 1 : 0) : -1;
            customer.AddressLine1 = entity.Addressline1;
            customer.AddressLine2 = entity.Addressline2;
            customer.Zipcode = entity.Zipcode;
            customer.City = entity.City;
            customer.CountryCode = entity.CountryCode;
            customer.AnonymousAccount = entity.AnonymousAccount;
            customer.Guid = entity.GUID;
            customer.Verified = entity.Verified;
            customer.CampaignName = entity.CampaignName;
            customer.CampaignSource = entity.CampaignSource;
            customer.CampaignMedium = entity.CampaignMedium;
            customer.CampaignContent = entity.CampaignContent;

            return customer;
        }

        public override Model.Customer ConvertMobileModelToModel(Customer mobileModel)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}   
