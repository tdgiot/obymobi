﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model.Mobile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.Api.EntityConverters.Mobile
{
    public class CompanyAmenityEntityConverter : MobileEntityConverterBase<CompanyAmenityEntity, CompanyAmenityCollection, Amenity, Obymobi.Logic.Model.Amenity>
    {
        public override Amenity ConvertEntityToMobileModel(CompanyAmenityEntity entity)
        {
            AmenityEntity amenityEntity = entity.AmenityEntity;

            Amenity amenity = new Amenity();
            amenity.AmenityId = amenityEntity.AmenityId;
            //amenity.Name = amenityEntity.Name;

            // Amenity languages
            //amenity.AmenityLanguages = new AmenityLanguageEntityConverter().ConvertEntityCollectionToMobileModelArray(amenityEntity.AmenityLanguageCollection);

            return amenity;
        }

        public override Model.Amenity ConvertMobileModelToModel(Amenity mobileModel)
        {
            throw new NotImplementedException();
        }
    }
}
