﻿using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model.Mobile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.Api.EntityConverters.Mobile
{
    public class CategoryEntityConverter : MobileEntityConverterBase<CategoryEntity, CategoryCollection, Category, Obymobi.Logic.Model.Category>
    {
        #region Fields

        private bool rootCategories = false;
        private DeviceType deviceType = DeviceType.Unknown;

        #endregion

        #region Constructors

        public CategoryEntityConverter(bool rootCategories, DeviceType deviceType)
        {
            this.rootCategories = rootCategories;
            this.deviceType = deviceType;
        }

        #endregion

        #region Methods

        public override Category ConvertEntityToMobileModel(CategoryEntity entity)
        {
            Category category = new Category();
            category.CategoryId = entity.CategoryId;
            category.ParentCategoryId = entity.ParentCategoryId.HasValue ? entity.ParentCategoryId.Value : -1;
            category.Name = entity.Name;
            category.Description = entity.Description;
            category.SortOrder = entity.SortOrder;
            category.HidePrices = entity.HidePrices;
            category.Type = (entity.Type > 0 ? entity.Type : CategoryHelper.GetTypeFromParent(entity));
            category.ScheduleId = entity.ScheduleId ?? 0;

            if (entity.Geofencing.HasValue)
                category.Geofencing = entity.Geofencing.Value;
            else
                category.Geofencing = (CategoryHelper.HasGeofencedParent(entity) ?? true);

            // Media
            if (entity.MediaCollection.Count > 0)
                category.Media = new MediaEntityConverter(false, this.deviceType).ConvertEntityCollectionToMobileModelArray(entity.MediaCollection);
            else if (entity.GenericcategoryId.HasValue)
                category.Media = new MediaEntityConverter(false, this.deviceType).ConvertEntityCollectionToMobileModelArray(entity.GenericcategoryEntity.MediaCollection);

            // Sub categories
            category.Categories = new CategoryEntityConverter(false, this.deviceType).ConvertEntityCollectionToMobileModelArray(entity.ChildCategoryCollection);

            // Products
            category.Products = new ProductCategoryEntityConverter(this.deviceType).ConvertEntityCollectionToMobileModelArray(entity.ProductCategoryCollection);

            // Category suggestions
            category.CategorySuggestions = new CategorySuggestionEntityConverter().ConvertEntityCollectionToMobileModelArray(entity.CategorySuggestionCollection);

            // Custom texts
            category.CustomTexts = new CustomTextEntityConverter().ConvertEntityCollectionToMobileModelArray(entity.CustomTextCollection);

            return category;
        }

        public override Category[] ConvertEntityCollectionToMobileModelArray(CategoryCollection entityCollection)
        {
            List<Category> modelList = new List<Category>();

            if (entityCollection != null && entityCollection.Count > 0)
            {
                entityCollection.Sort((int)CategoryFieldIndex.SortOrder, System.ComponentModel.ListSortDirection.Ascending);

                foreach (CategoryEntity entity in entityCollection)
                {
                    if (entity != null)
                    {
                        if (!this.rootCategories || !entity.ParentCategoryId.HasValue)
                        {
                            Category model = this.ConvertEntityToMobileModel(entity);
                            if (model != null)
                                modelList.Add(model);
                        }
                    }
                }
            }

            return modelList.ToArray();
        }

        public override Model.Category ConvertMobileModelToModel(Category mobileModel)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
