﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model.Mobile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.Api.EntityConverters.Mobile
{
    public class VenueCategoryEntityConverter : MobileEntityConverterBase<VenueCategoryEntity, VenueCategoryCollection, VenueCategory, Obymobi.Logic.Model.VenueCategory>
    {
        public override VenueCategory ConvertEntityToMobileModel(VenueCategoryEntity entity)
        {
            VenueCategory venueCategory = new VenueCategory();
            venueCategory.VenueCategoryId = entity.VenueCategoryId;
            venueCategory.Name = entity.Name;
            venueCategory.MarkerIcon = (int)entity.MarkerIcon;

            // Custom texts
            venueCategory.CustomTexts = new CustomTextEntityConverter().ConvertEntityCollectionToMobileModelArray(entity.CustomTextCollection);

            return venueCategory;
        }

        public override Model.VenueCategory ConvertMobileModelToModel(VenueCategory mobileModel)
        {
            throw new NotImplementedException();
        }
    }
}
