﻿using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model.Mobile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Enums;

namespace Obymobi.Logic.Api.EntityConverters.Mobile
{
    public class AlterationProductEntityConverter : MobileEntityConverterBase<AlterationProductEntity, AlterationProductCollection, AlterationProduct, Obymobi.Logic.Model.AlterationProduct>
    {
        private CategoryEntity category = null;
        private DeviceType deviceType = DeviceType.Unknown;

        public AlterationProductEntityConverter(CategoryEntity category, DeviceType deviceType)
        {
            this.category = category;
            this.deviceType = deviceType;
        }

        public override AlterationProduct ConvertEntityToMobileModel(AlterationProductEntity entity)
        {
            AlterationProduct alterationProduct = new AlterationProduct();
            alterationProduct.AlterationProductId = entity.AlterationProductId;
            alterationProduct.AlterationId = entity.AlterationId;
            alterationProduct.Visible = entity.Visible;
            alterationProduct.SortOrder = entity.SortOrder;

            List<Product> products = new List<Product>();
            products.Add(new ProductEntityConverter(this.category, this.deviceType).ConvertEntityToMobileModel(entity.ProductEntity));
            alterationProduct.Products = products.ToArray();

            return alterationProduct;            
        }

        public override Model.AlterationProduct ConvertMobileModelToModel(AlterationProduct mobileModel)
        {
            throw new NotImplementedException();
        }
    }
}
