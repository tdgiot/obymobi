﻿using Dionysos;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Logic.Model;

namespace Obymobi.Logic.Api.EntityConverters.Mobile
{
    public class MediaCultureEntityConverter : MobileEntityConverterBase<MediaCultureEntity, MediaCultureCollection, Obymobi.Logic.Model.Mobile.MediaCulture, Obymobi.Logic.Model.MediaCulture>
    {
        #region Fields

        private DeviceType deviceType = DeviceType.Unknown;
        private string parent = string.Empty;
        #endregion

        #region Constructors

        public MediaCultureEntityConverter(DeviceType deviceType, string parent = "")
        {
            this.deviceType = deviceType;
            this.parent = parent;
        }

        #endregion

        #region Methods

        public override Model.Mobile.MediaCulture ConvertEntityToMobileModel(MediaCultureEntity entity)
        {
            Model.Mobile.MediaCulture mediaCulture = new Model.Mobile.MediaCulture();
            mediaCulture.MediaCultureId = entity.MediaCultureId;
            mediaCulture.MediaId = entity.MediaId;
            mediaCulture.CultureCode = entity.CultureCode;
            mediaCulture.Parent = this.parent.IsNullOrWhiteSpace() ? "" : this.parent;

            return mediaCulture;
        }

        public override Model.MediaCulture ConvertMobileModelToModel(Model.Mobile.MediaCulture mobileModel)
        {
            MediaCulture mediaCulture = new MediaCulture();
            mediaCulture.MediaCultureId = mobileModel.MediaCultureId;
            mediaCulture.MediaId = mobileModel.MediaId;
            mediaCulture.CultureCode = mobileModel.CultureCode;
            mediaCulture.Parent = mobileModel.Parent;
            
            return mediaCulture;
        }

        #endregion

        public Model.MediaCulture ConvertEntityToModel(MediaCultureEntity entity)
        {
            Model.Mobile.MediaCulture mobileSite = ConvertEntityToMobileModel(entity);
            return ConvertMobileModelToModel(mobileSite);
        }
    }
}
