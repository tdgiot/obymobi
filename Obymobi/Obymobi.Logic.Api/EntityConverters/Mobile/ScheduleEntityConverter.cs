﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logic.Model.Mobile;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.Api.EntityConverters.Mobile
{
    public class ScheduleEntityConverter : MobileEntityConverterBase<ScheduleEntity, ScheduleCollection, Schedule, Obymobi.Logic.Model.Schedule>
    {
        public override Schedule ConvertEntityToMobileModel(ScheduleEntity entity)
        {
            Schedule schedule = null;

            // Get the view for the schedule items of the current product
            EntityView<ScheduleitemEntity> scheduleitemView = entity.ScheduleitemCollection.DefaultView;

            // Create and initialize a list to store the schedule items in
            List<Scheduleitem> scheduleitemList = new List<Scheduleitem>();

            // First, get the schedule items per specific day
            PredicateExpression dayFilter = new PredicateExpression();
            dayFilter.Add(ScheduleitemFields.DayOfWeek != DBNull.Value);
            scheduleitemView.Filter = dayFilter;

            bool anyDayDefined = false;

            foreach (ScheduleitemEntity scheduleitemEntity in scheduleitemView)
            {
                Scheduleitem scheduleitem = new Scheduleitem();
                scheduleitem.DayOfweek = (int)scheduleitemEntity.DayOfWeek.Value;
                scheduleitem.TimeStart = scheduleitemEntity.TimeStart;
                scheduleitem.TimeEnd = scheduleitemEntity.TimeEnd;
                scheduleitemList.Add(scheduleitem);

                anyDayDefined = true;
            }

            PredicateExpression noDayFilter = new PredicateExpression();
            noDayFilter.Add(ScheduleitemFields.DayOfWeek == DBNull.Value);
            scheduleitemView.Filter = noDayFilter;

            if (!anyDayDefined)
            {
                // Set the default order hours or mix in the category order hours
                for (int i = 0; i < 7; i++)
                {
                    bool found = false;
                    foreach (Scheduleitem temp in scheduleitemList)
                    {
                        if (temp.DayOfweek == i)
                        {
                            found = true;
                            break;
                        }
                    }

                    if (!found) // No order hours found for this day, add the default, if there's no default, at the categories
                    {
                        if (scheduleitemView.Count > 0)
                        {
                            foreach (var temp in scheduleitemView)
                            {
                                Scheduleitem scheduleitem = new Scheduleitem();
                                scheduleitem.DayOfweek = i;
                                scheduleitem.TimeStart = temp.TimeStart;
                                scheduleitem.TimeEnd = temp.TimeEnd;
                                scheduleitemList.Add(scheduleitem);
                                found = true;
                            }
                        }

                        if (!found) // Set the default
                        {
                            Scheduleitem scheduleitem = new Scheduleitem();
                            scheduleitem.DayOfweek = i;
                            scheduleitem.TimeStart = "0000";
                            scheduleitem.TimeEnd = "2359";
                            scheduleitemList.Add(scheduleitem);
                        }
                    }
                }
            }

            schedule = new Schedule();
            schedule.ScheduleId = entity.ScheduleId;
            schedule.Name = entity.Name;
            schedule.Scheduleitems = scheduleitemList.ToArray();

            return schedule;
        }

        public override Model.Schedule ConvertMobileModelToModel(Schedule mobileModel)
        {
            throw new NotImplementedException();
        }
    }
}
