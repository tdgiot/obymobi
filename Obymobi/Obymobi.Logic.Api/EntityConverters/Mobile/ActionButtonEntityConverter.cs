﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model.Mobile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.Api.EntityConverters.Mobile
{
    public class ActionButtonEntityConverter : MobileEntityConverterBase<ActionButtonEntity, ActionButtonCollection, ActionButton, Obymobi.Logic.Model.ActionButton>
    {
        public override ActionButton ConvertEntityToMobileModel(ActionButtonEntity entity)
        {
            ActionButton actionButton = new ActionButton();
            actionButton.ActionButtonId = entity.ActionButtonId;
            actionButton.Name = entity.Name;

            // Custom texts
            actionButton.CustomTexts = new CustomTextEntityConverter().ConvertEntityCollectionToMobileModelArray(entity.CustomTextCollection);

            return actionButton;
        }

        public override Model.ActionButton ConvertMobileModelToModel(ActionButton mobileModel)
        {
            throw new NotImplementedException();
        }
    }
}
