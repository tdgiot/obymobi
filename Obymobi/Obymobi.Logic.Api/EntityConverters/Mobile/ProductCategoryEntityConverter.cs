﻿using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model.Mobile;
using Obymobi.Logic.POS;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.Api.EntityConverters.Mobile
{
    public class ProductCategoryEntityConverter : MobileEntityConverterBase<ProductCategoryEntity, ProductCategoryCollection, Product, Obymobi.Logic.Model.Product>
    {
        #region Fields

        private DeviceType deviceType = DeviceType.Unknown;

        #endregion

        #region Constructors

        public ProductCategoryEntityConverter(DeviceType deviceType)
        {
            this.deviceType = deviceType;
        }

        #endregion

        #region Methods

        public override Product ConvertEntityToMobileModel(ProductCategoryEntity productCategoryEntity)
        {
            ProductEntity entity = productCategoryEntity.ProductEntity;

            Product product = new Product();
            product.ProductId = entity.ProductId;
            product.Name = entity.Name;
            product.Description = entity.Description;
            product.Type = entity.Type;
            product.PriceIn = entity.PriceIn ?? 0;
            product.ButtonText = entity.ButtonText;
            product.WebTypeSmartphoneUrl = entity.WebTypeSmartphoneUrl;
            product.WebTypeTabletUrl = entity.WebTypeTabletUrl;
            product.ScheduleId = entity.ScheduleId ?? 0;

            if (entity.GenericproductId.HasValue)
            {
                bool useDefault = true;
                bool nameEmpty = string.IsNullOrEmpty(entity.Name);
                bool useGenericproductDescription = !entity.ManualDescriptionEnabled;

                if (useDefault)
                {
                    if (nameEmpty)
                        product.Name = entity.GenericproductEntity.Name;

                    if (useGenericproductDescription)
                        product.Description = entity.GenericproductEntity.Description;
                }
            }

            product.CategoryId = productCategoryEntity.CategoryId;
            product.SortOrder = productCategoryEntity.SortOrder + (10000 * productCategoryEntity.CategoryEntity.SortOrder);
            product.Type = CategoryHelper.GetTypeFromParent(productCategoryEntity.CategoryEntity);
            if (product.Type == 0)
                product.Type = (int)ProductSubType.Normal;

            product.SubType = entity.SubType;

            if (entity.Geofencing.HasValue)
                product.Geofencing = entity.Geofencing.Value;
            else
                product.Geofencing = (CategoryHelper.HasGeofencedParent(productCategoryEntity.CategoryEntity) ?? true);

            if (entity.AllowFreeText.HasValue)
                product.AllowFreeText = entity.AllowFreeText.Value;
            else
                product.AllowFreeText = (CategoryHelper.HasAllowFreeTextParent(productCategoryEntity.CategoryEntity) ?? false);

            product.HidePrice = entity.HidePrice;

            // Alterations > Based on product, otherwise category
            List<Alteration> alterations = new List<Alteration>();
            if (entity.ProductAlterationCollection.Count > 0)
            {
                entity.ProductAlterationCollection.Sort((int)ProductAlterationFieldIndex.SortOrder, System.ComponentModel.ListSortDirection.Ascending);

                for (int i = 0; i < entity.ProductAlterationCollection.Count; i++)
                {
                    if (entity.ProductAlterationCollection[i].Version > 1)
                        continue;

                    List<Alteration> alterationsWithChildren = new AlterationEntityConverter(productCategoryEntity.CategoryEntity, this.deviceType).ConvertEntityToMobileModelWithChildren(entity.ProductAlterationCollection[i].AlterationEntity);
                    foreach (Alteration alteration in alterationsWithChildren)
                    {
                        if ((alteration.Type == (int)AlterationType.Options && alteration.Alterationoptions != null && alteration.Alterationoptions.Length > 0) || alteration.Type != (int)AlterationType.Options)
                        {
                            alterations.Add(alteration);
                        }
                    }
                }
                product.Alterations = alterations.ToArray();
            }
            else
            {
                this.AddAlterationsFromCategory(alterations, productCategoryEntity.CategoryEntity, POSConnectorType.Unknown, false);
                product.Alterations = alterations.ToArray();
            }

            // Media
            if (entity.GenericproductId.HasValue)
                product.Media = new MediaEntityConverter(false, this.deviceType).ConvertEntityCollectionToMobileModelArray(entity.GenericproductEntity.MediaCollection);
            else if (entity.MediaCollection.Count > 0)
                product.Media = new MediaEntityConverter(false, this.deviceType).ConvertEntityCollectionToMobileModelArray(entity.MediaCollection);

            // Product suggestions
            product.ProductSuggestions = new ProductCategorySuggestionEntityConverter().ConvertEntityCollectionToMobileModelArray(productCategoryEntity.ProductCategorySuggestionCollection);

            // Product Attachments
            product.Attachments = new AttachmentEntityConverter(this.deviceType).ConvertEntityCollectionToMobileModelArray(entity.AttachmentCollection);

            return product;
        }

        private void AddAlterationsFromCategory(List<Alteration> alterations, CategoryEntity category, POSConnectorType posConnectorType, bool addPosData)
        {
            if (category.CategoryAlterationCollection.Count > 0) // TODO Optimize: This is not prefetched
            {
                for (int i = 0; i < category.CategoryAlterationCollection.Count; i++)
                {
                    if (category.CategoryAlterationCollection[i].Version > 1)
                        continue;

                    List<Alteration> alterationsWithChildren = new AlterationEntityConverter(category, this.deviceType).ConvertEntityToMobileModelWithChildren(category.CategoryAlterationCollection[i].AlterationEntity);
                    foreach (Alteration alteration in alterationsWithChildren)
                    {
                        if ((alteration.Type == (int)AlterationType.Options && alteration.Alterationoptions != null && alteration.Alterationoptions.Length > 0) || alteration.Type != (int)AlterationType.Options)
                        {
                            alterations.Add(alteration);
                        }
                    }
                }
            }
            else if (category.ParentCategoryId.HasValue)
            {
                this.AddAlterationsFromCategory(alterations, category.ParentCategoryEntity, posConnectorType, addPosData); // TODO Optimize: This is not prefetched
            }
        }

        public override Product[] ConvertEntityCollectionToMobileModelArray(ProductCategoryCollection entityCollection)
        {
            List<Product> modelList = new List<Product>();

            if (entityCollection != null && entityCollection.Count > 0)
            {
                entityCollection.Sort((int)ProductCategoryFieldIndex.SortOrder, System.ComponentModel.ListSortDirection.Ascending);

                foreach (ProductCategoryEntity entity in entityCollection)
                {
                    if (entity != null)
                    {
                        Product model = this.ConvertEntityToMobileModel(entity);
                        if (model != null)
                            modelList.Add(model);
                    }
                }
            }

            return modelList.ToArray();
        }

        public override Model.Product ConvertMobileModelToModel(Product mobileModel)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
