﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.Api.EntityConverters.Mobile
{
    public class OrderitemEntityConverter : MobileEntityConverterBase<OrderitemEntity, OrderitemCollection, Model.Mobile.Orderitem, Model.Orderitem>
    {
        public override Model.Mobile.Orderitem ConvertEntityToMobileModel(OrderitemEntity entity)
        {
            Model.Mobile.Orderitem mobileModel = new Model.Mobile.Orderitem();
            mobileModel.OrderitemId = entity.OrderitemId;
            mobileModel.OrderId = entity.OrderId;
            mobileModel.Guid = entity.Guid;
            if (entity.CategoryId.HasValue)
                mobileModel.CategoryId = entity.CategoryId.Value;
            if (entity.ProductId.HasValue)
                mobileModel.ProductId = entity.ProductId.Value;
            mobileModel.ProductName = entity.ProductName;
            mobileModel.Quantity = entity.Quantity;
            mobileModel.ProductPriceIn = entity.ProductPriceIn;
            mobileModel.Notes = entity.Notes;

            // Alterationitems
            mobileModel.Alterationitems = new OrderitemAlterationitemEntityConverter().ConvertEntityCollectionToMobileModelArray(entity.OrderitemAlterationitemCollection);

            return mobileModel;
        }

        public override Model.Orderitem ConvertMobileModelToModel(Model.Mobile.Orderitem mobileModel)
        {
            Model.Orderitem model = new Model.Orderitem();
            model.OrderitemId = mobileModel.OrderitemId;
            model.OrderId = mobileModel.OrderId;
            model.Guid = mobileModel.Guid;
            model.CategoryId = mobileModel.CategoryId;
            model.ProductId = mobileModel.ProductId;
            model.ProductName = mobileModel.ProductName;
            model.Quantity = mobileModel.Quantity;
            model.ProductPriceIn = mobileModel.ProductPriceIn;
            model.Notes = mobileModel.Notes;

            // Alterationitems
            model.Alterationitems = new AlterationitemEntityConverter().ConvertMobileModelArrayToModelArray(mobileModel.Alterationitems);

            return model;
        }
    }
}
