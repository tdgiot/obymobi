﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model.Mobile;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;
using Obymobi.Enums;
using Obymobi.Logic.Cms;
using MarkdownSharp.Extensions;

namespace Obymobi.Logic.Api.EntityConverters.Mobile
{
    public class PageTemplateElementEntityConverter : MobileEntityConverterBase<PageTemplateElementEntity, PageTemplateElementCollection, PageElement, Obymobi.Logic.Model.PageElement>
    {
        #region Fields

        private DeviceType deviceType = DeviceType.Unknown;
        private PageEntity page;

        #endregion

        #region Constructors

        public PageTemplateElementEntityConverter(DeviceType deviceType, PageEntity page)
        {
            this.deviceType = deviceType;
            this.page = page;
        }

        #endregion

        #region Methods

        public override PageElement ConvertEntityToMobileModel(PageTemplateElementEntity entity)
        {
            Model.Mobile.PageElement pageElement = new Model.Mobile.PageElement();
            pageElement.PageElementId = -1;
            pageElement.PageTemplateElementId = entity.PageTemplateElementId;
            pageElement.PageId = this.page.PageId;
            pageElement.SystemName = entity.SystemName;
            pageElement.PageElementType = entity.PageElementType;
            pageElement.CultureCode = entity.CultureCode;
            pageElement.LanguageCode = entity.CultureCode.IsNullOrWhiteSpace() ? entity.CultureCode : Obymobi.Culture.Mappings[entity.CultureCode].Language.CodeAlpha2.ToLowerInvariant();

            // GK It ain't pretty, but I couldn't think of a better solution.
            // problem is that I don't want to Add Markdown to the Obymobi project (because it's also used for mobile) and therefore
            // it can't be added in the MultiLinTextElement class.                 
            if (entity.PageElementType == (int)PageElementType.MultiLineText)
            {
                // Since Markdown is not thread safe and apparently creation cost is low, we spawn one (https://code.google.com/p/markdownsharp/issues/detail?id=48)
                var markdown = MarkdownHelper.GetDefaultInstance();
                pageElement.StringValue1 = markdown.Transform(entity.StringValue1);

                if (pageElement.StringValue1.EndsWith("\n"))
                    pageElement.StringValue1 = pageElement.StringValue1.Substring(0, pageElement.StringValue1.Length - 1);
            }
            else
                pageElement.StringValue1 = entity.StringValue1;

            pageElement.StringValue2 = entity.StringValue2;
            pageElement.StringValue3 = entity.StringValue3;
            pageElement.StringValue4 = entity.StringValue4;
            pageElement.StringValue5 = entity.StringValue5;
            pageElement.BoolValue1 = entity.BoolValue1;
            pageElement.BoolValue2 = entity.BoolValue2;
            pageElement.BoolValue3 = entity.BoolValue3;
            pageElement.BoolValue4 = entity.BoolValue4;
            pageElement.BoolValue5 = entity.BoolValue5;
            pageElement.IntValue1 = entity.IntValue1;
            pageElement.IntValue2 = entity.IntValue2;
            pageElement.IntValue3 = entity.IntValue3;
            pageElement.IntValue4 = entity.IntValue4;
            pageElement.IntValue5 = entity.IntValue5;

            // Root categories            
            pageElement.Media = new MediaEntityConverter(false, this.deviceType, "PAGES_PAGEELEMENTS").ConvertEntityCollectionToMobileModelArray(entity.MediaCollection);

            return pageElement;          
        }

        public override PageElement[] ConvertEntityCollectionToMobileModelArray(PageTemplateElementCollection entityCollection)
        {
            List<PageElement> pageElements = new List<PageElement>();

            foreach (var pageElement in entityCollection)
            {
                pageElements.Add(this.ConvertEntityToMobileModel(pageElement));
            }

            return pageElements.ToArray();
        }        

        public override Model.PageElement ConvertMobileModelToModel(Model.Mobile.PageElement mobileModel)
        {
            var pageElement = new Model.PageElement();
            pageElement.PageElementId = mobileModel.PageElementId;
            pageElement.PageId = mobileModel.PageId;
            pageElement.PageTemplateElementId = mobileModel.PageTemplateElementId;
            pageElement.LanguageCode = mobileModel.LanguageCode;
            pageElement.SystemName = mobileModel.SystemName;
            pageElement.PageElementType = mobileModel.PageElementType;

            pageElement.StringValue1 = mobileModel.StringValue1;
            pageElement.StringValue2 = mobileModel.StringValue2;
            pageElement.StringValue3 = mobileModel.StringValue3;
            pageElement.StringValue4 = mobileModel.StringValue4;
            pageElement.StringValue5 = mobileModel.StringValue5;
            pageElement.BoolValue1 = mobileModel.BoolValue1;
            pageElement.BoolValue2 = mobileModel.BoolValue2;
            pageElement.BoolValue3 = mobileModel.BoolValue3;
            pageElement.BoolValue4 = mobileModel.BoolValue4;
            pageElement.BoolValue5 = mobileModel.BoolValue5;
            pageElement.IntValue1 = mobileModel.IntValue1;
            pageElement.IntValue2 = mobileModel.IntValue2;
            pageElement.IntValue3 = mobileModel.IntValue3;
            pageElement.IntValue4 = mobileModel.IntValue4;
            pageElement.IntValue5 = mobileModel.IntValue5;

            // Root categories            
            if (mobileModel.Media != null)
            {
                var mediaConverter = new MediaEntityConverter(false, this.deviceType, "PAGES_PAGEELEMENTS");
                pageElement.Media = mobileModel.Media.Select(mediaConverter.ConvertMobileModelToModel).ToArray();
            }

            return pageElement;   
        }       

        #endregion
    }
}
