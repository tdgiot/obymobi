using NUnit.Framework;
using Obymobi.Enums;

namespace Obymobi.Services.WebApplication.Tests
{
    public class EnvironmentSpecificMessageFactoryTest
    {
        [TestCase(CloudEnvironment.Development)]
        [TestCase(CloudEnvironment.Test)]
        [TestCase(CloudEnvironment.Staging)]
        public void Create_ForNonProductionEnvironment_PrependsEnvironmentToMessage(CloudEnvironment cloudEnvironment)
        {
            // Arrange
            const string message = "My test message.";
            string expectedMessage = $"[{cloudEnvironment}] {message}";
            IMessageFactory sut = new EnvironmentSpecificMessageFactory(cloudEnvironment);

            // Act
            string actualMessage = sut.Create(message);

            // Assert
            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestCase(CloudEnvironment.ProductionPrimary)]
        [TestCase(CloudEnvironment.ProductionSecondary)]
        [TestCase(CloudEnvironment.ProductionOverwrite)]
        public void Create_ForProductionEnvironment_DoesNotPrependEnvironmentToMessage(CloudEnvironment cloudEnvironment)
        {
            // Arrange
            const string message = "My test message.";
            string expectedMessage = message;
            IMessageFactory sut = new EnvironmentSpecificMessageFactory(cloudEnvironment);

            // Act
            string actualMessage = sut.Create(message);

            // Assert
            Assert.AreEqual(expectedMessage, actualMessage);
        }
    }
}
