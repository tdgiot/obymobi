﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Analytics.Google.Recording
{
    public sealed class AsyncHitRecorder
    {
        #region Singleton Pattern
        // http://csharpindepth.com/Articles/General/Singleton.aspx
        private static readonly Lazy<AsyncHitRecorder> lazy = new Lazy<AsyncHitRecorder>(() => new AsyncHitRecorder());

        public static AsyncHitRecorder Instance { get { return lazy.Value; } }

        private AsyncHitRecorder()
        {
            Task.Factory.StartNew(() => this.ProcessQueue(), TaskCreationOptions.LongRunning);
        }
        #endregion

        public static void RecordHit(Hit hit, string propertyId)
        {
            RecordHit(hit, new List<string> { propertyId });
        }

        public static void RecordHit(Hit hit, List<string> propertyIds)
        {
            Instance.hitQueue.Add(new AsyncHit { Hit = hit, TrackingIds = propertyIds });
        }

        BlockingCollection<AsyncHit> hitQueue = new BlockingCollection<AsyncHit>();

        private void ProcessQueue()
        {
            while (true)
            {
                AsyncHit hit = null;
                try
                {
                    hit = this.hitQueue.Take();

                    if (hit == null)
                        continue;

                    HitRecorder.RecordHit(hit.Hit, hit.TrackingIds);
                }
                catch
                {
                    // Don't crash.
                    Debug.WriteLine("Failed to record hit: {0} - {1} - {2} - {3}", hit.Hit.EventCategory, hit.Hit.EventAction, hit.Hit.EventLabel, hit.Hit.ScreenName);
                }
            }
        }

        private class AsyncHit
        {
            public Hit Hit { get; set; }
            public List<string> TrackingIds { get; set; }
        }
    }
}
