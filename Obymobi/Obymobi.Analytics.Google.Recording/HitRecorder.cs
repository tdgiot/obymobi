﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Analytics.Google.Recording
{
    public static class HitRecorder
    {
        public static void RecordHit(Hit hit, string propertyId)
        {
            HitRecorder.RecordHit(hit, new List<string> { propertyId });
        }

        public static void RecordHit(Hit hit, List<string> propertyIds)
        {
            string toReturn = string.Empty;
            Dictionary<string, string> parameters = HitRecorder.GetHitParameters(hit);

            StringBuilder sb = new StringBuilder();
            var parameterNames = parameters.Keys.ToList();
            for (int i = 0; i < parameterNames.Count; i++)
            {
                string parameterName = parameterNames[i];
                string value = WebUtility.UrlEncode(parameters[parameterName]);

                if (!String.IsNullOrWhiteSpace(value))
                {
                    if (i == 0)
                        sb.AppendFormat("{0}={1}", parameterName, value);
                    else
                        sb.AppendFormat("&{0}={1}", parameterName, value);
                }
            }

            string payLoadBase = sb.ToString();
            foreach (var propertyId in propertyIds)
            {
                // Production Url: "https://ssl.google-analytics.com/collect"
                // Validation Url: "https://www.google-analytics.com/debug/collect"
                //var request = (HttpWebRequest)WebRequest.Create("https://www.google-analytics.com/debug/collect");
                var request = (HttpWebRequest)WebRequest.Create("https://ssl.google-analytics.com/collect");
                request.Method = "POST";
                request.KeepAlive = false;
                request.ContentType = "text";

                string payLoad = payLoadBase + "&tid=" + propertyId;
                var payLoadAsBytes = Encoding.UTF8.GetBytes(payLoad);
                request.ContentLength = payLoadAsBytes.Length;
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(payLoadAsBytes, 0, payLoadAsBytes.Length);
                requestStream.Close();

                var response = (HttpWebResponse)request.GetResponse();
                using (var streamReader = new StreamReader(response.GetResponseStream()))
                {                    
                    Debug.WriteLine(string.Format("HitRecorder.RecordHit Result {0}: {1}", response.StatusCode, streamReader.ReadToEnd()));
                }
            }
        }

        private static Dictionary<string, string> GetHitParameters(Hit hit)
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();

            FieldInfo[] fields = hit.GetType().GetFields();

            // Iterate over all the fields 
            foreach (var field in fields)
            {
                try
                {
                    // If it's not a Google Parameter, skip/continue
                    var attributes = field.GetCustomAttributes(typeof(HitParameterAttribute), false);
                    if (attributes == null && attributes.Length != 1)
                        continue;

                    string parameterName = ((HitParameterAttribute)attributes[0]).GoogleParameterName;

                    string valueAsString = string.Empty;

                    if (field.GetValue(hit) == null)
                    {
                        // nohting
                    }
                    else if (field.FieldType == typeof(string))
                    {
                        valueAsString = (string)field.GetValue(hit);
                    }
                    else if (field.FieldType == typeof(int) || field.FieldType == typeof(Double))
                    {
                        valueAsString = string.Format(CultureInfo.InvariantCulture, "{0}", field.GetValue(hit));
                    }
                    else if (field.FieldType == typeof(bool))
                    {
                        valueAsString = (bool)field.GetValue(hit) ? "1" : "0";
                    }
                    else if (field.FieldType.IsEnum)
                    {                        
                        valueAsString = AnalyticsEnumUtil.GetAnalyticsEnumStringValue((Enum)field.GetValue(hit));
                    }
                    else
                        throw new NotImplementedException("No implementation for Type: " + field.FieldType.FullName);

                    if (String.IsNullOrWhiteSpace(valueAsString) && parameterName.StartsWith("cd"))
                    {
                        valueAsString = "X";
                    }

                    parameters.Add(parameterName, valueAsString);
                }
                catch
                {
                    // Don't let one value kill all the fun
                }
            }

            return parameters;
        }

    }
}
