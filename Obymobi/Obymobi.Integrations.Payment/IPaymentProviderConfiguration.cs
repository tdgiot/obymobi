﻿using Obymobi.Enums;

namespace Obymobi.Integrations.Payment
{
    public interface IPaymentProviderConfiguration
    {
        PaymentProviderType PaymentProviderType { get; }
        string ApiKey { get; }
        bool IsProduction { get; }
        string LiveEndpointUrlPrefix { get; }
        string AdyenMerchantCode { get; }
    }
}
