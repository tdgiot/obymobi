﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Integrations.Payment.Models
{
    public class PaymentMethod
    {
        public string Name { get; set; }
        public string Type { get; set; }
    }
}
