﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Obymobi.Enums;

namespace Obymobi.Integrations.Payment
{
    public class PaymentProviderConfiguration : IPaymentProviderConfiguration
    {
        public PaymentProviderType PaymentProviderType { get; set; }
        public string ApiKey { get; set; }
        public bool IsProduction { get; set; }
        public string LiveEndpointUrlPrefix { get; set; }
        public string AdyenMerchantCode { get; set; }
    }
}
