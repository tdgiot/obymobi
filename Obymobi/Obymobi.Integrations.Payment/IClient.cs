﻿using Obymobi.Integrations.Payment.Models;

namespace Obymobi.Integrations.Payment
{
    public interface IClient
    {
        bool IsAuthenticated();
        GetPaymentMethodsResponse GetPaymentMethods();
    }
}
