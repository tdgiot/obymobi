﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Integrations.Payment.Models
{
    public class GetPaymentMethodsResponse
    {
        public GetPaymentMethodsResponse(bool success, string errorMessage)
        {
            this.Success = success;
            this.ErrorMessage = errorMessage;
        }

        public GetPaymentMethodsResponse(bool success, string errorMessage, int errorCode)
        {
            this.Success = success;
            this.ErrorMessage = errorMessage;
            this.ErrorCode = errorCode;
        }

        public GetPaymentMethodsResponse(bool success, IEnumerable<PaymentMethod> paymentMethods)
        {
            this.Success = success;
            this.PaymentMethods = paymentMethods;
        }

        public bool Success { get; }
        public string ErrorMessage { get; }
        public int ErrorCode { get; }
        public IEnumerable<PaymentMethod> PaymentMethods { get; } = new List<PaymentMethod>();
    }
}
