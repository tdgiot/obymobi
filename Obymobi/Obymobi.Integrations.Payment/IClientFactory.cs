﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Integrations.Payment
{
    public interface IClientFactory
    {
        IClient Create();
    }
}
