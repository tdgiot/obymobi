﻿using CraveInfoGenesisService.Configuration;
using Topshelf;

namespace CraveInfoGenesisService
{
    class Program
    {
        static void Main(string[] args)
        {
            HostFactory.Run(x =>
            {
                x.Service<HostingConfig>();
                x.RunAsNetworkService();
                x.StartAutomatically();
                x.SetDisplayName("CraveInfoGenesisService");
                x.SetServiceName("CraveInfoGenesisService");
            });
        }
    }
}
