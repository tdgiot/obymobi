﻿using System;
using Microsoft.Owin.Hosting;
using Topshelf;

namespace CraveInfoGenesisService.Configuration
{
    public class HostingConfig : ServiceControl
    {
        private IDisposable webApplication;
        //private DataManager dataManager;

        public bool Start(HostControl hostControl)
        {
            this.webApplication = WebApp.Start<OwinConfig>("http://*:1337");
            return true;
        }

        public bool Stop(HostControl hostControl)
        {
            this.webApplication.Dispose();
            return true;
        }
    }
}
