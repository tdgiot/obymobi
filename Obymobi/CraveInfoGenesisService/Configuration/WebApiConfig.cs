﻿using System.Web.Http;

namespace CraveInfoGenesisService.Configuration
{
    public class WebApiConfig
    {
        public static HttpConfiguration Register()
        {
            HttpConfiguration config = new HttpConfiguration();
            config.MapHttpAttributeRoutes();
            config.EnsureInitialized();
            return config;
        }
    }
}
