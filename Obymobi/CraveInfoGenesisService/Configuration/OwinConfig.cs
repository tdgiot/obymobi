﻿using Owin;

namespace CraveInfoGenesisService.Configuration
{
    public class OwinConfig
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseWebApi(WebApiConfig.Register());
        }
    }
}
