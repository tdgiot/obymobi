﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using InfoGenesis.DatabaseSpecific;
using InfoGenesis.EntityClasses;
using InfoGenesis.HelperClasses;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace CraveInfoGenesisService.Controller
{
    [AllowAnonymous]
    public class ItemController : ApiController
    {
        private readonly int entId = 1;
        private readonly int priceLevelId = 1;

        [HttpGet]
        [Route("categories")]
        public IEnumerable<Poscategory> GetCategories()
        {
            List<Poscategory> categories = new List<Poscategory>();
            using(DataAccessAdapter adapter = new DataAccessAdapter())
            {
                EntityCollection<ProductClassMasterEntity> productClassCollection = new EntityCollection<ProductClassMasterEntity>();
                adapter.FetchEntityCollection(productClassCollection, null);

                foreach (ProductClassMasterEntity productClassEntity in productClassCollection)
                {
                    Poscategory poscategory = new Poscategory();
                    poscategory.ExternalId = productClassEntity.ProdClassId.ToString();
                    poscategory.Name = productClassEntity.ProdClassName;
                    categories.Add(poscategory);
                }
            }
            return categories;
        }

        [HttpGet]
        [Route("products")]
        public IEnumerable<Posproduct> GetProducts()
        {
            List<Posproduct> products = new List<Posproduct>();
            using (DataAccessAdapter adapter = new DataAccessAdapter())
            {
                EntityCollection<MenuItemMasterEntity> menuItemCollection = new EntityCollection<MenuItemMasterEntity>();
                adapter.FetchEntityCollection(menuItemCollection, new RelationPredicateBucket(MenuItemMasterFields.EntId == this.entId));

                foreach (MenuItemMasterEntity menuItemEntity in menuItemCollection)
                {
                    Posproduct posproduct = new Posproduct();
                    posproduct.ExternalPoscategoryId = menuItemEntity.ProdClassId.ToString();
                    posproduct.ExternalId = menuItemEntity.MenuItemId.ToString();
                    posproduct.Name = menuItemEntity.MenuItemName;
                    MiPriceJoinEntity priceEntity = this.GetPriceForItem(adapter, menuItemEntity.MenuItemId);
                    if (priceEntity != null)
                    {
                        posproduct.PriceIn = priceEntity.PriceNet ?? 0;
                    }
                    posproduct.Posalterations = this.GetPosalterationsForItem(adapter, menuItemEntity.MenuItemId);
                    posproduct.VatTariff = 1;
                    products.Add(posproduct);
                }                
            }
            return products;
        }

        private MiPriceJoinEntity GetPriceForItem(DataAccessAdapter adapter, int itemId)
        {
            MiPriceJoinEntity priceJoinEntity = null;

            RelationPredicateBucket filter = new RelationPredicateBucket();
            filter.PredicateExpression.Add(MiPriceJoinFields.EntId == this.entId);
            filter.PredicateExpression.Add(MiPriceJoinFields.PriceLevelId == this.priceLevelId);
            filter.PredicateExpression.Add(MiPriceJoinFields.MenuItemId == itemId);

            EntityCollection<MiPriceJoinEntity> priceJoinEntityCollection = new EntityCollection<MiPriceJoinEntity>();
            adapter.FetchEntityCollection(priceJoinEntityCollection, filter);

            if (priceJoinEntityCollection.Count > 0)
            {
                priceJoinEntity = priceJoinEntityCollection[0];
            }

            return priceJoinEntity;
        }

        private Posalteration[] GetPosalterationsForItem(DataAccessAdapter adapter, int itemId)
        {
            List<Posalteration> posalterations = new List<Posalteration>();

            RelationPredicateBucket filter = new RelationPredicateBucket();
            filter.PredicateExpression.Add(MiChoiceGrpJoinFields.EntId == this.entId);
            filter.PredicateExpression.Add(MiChoiceGrpJoinFields.MenuItemId == itemId);

            EntityCollection<MiChoiceGrpJoinEntity> itemChoiceGroupCollection = new EntityCollection<MiChoiceGrpJoinEntity>();
            adapter.FetchEntityCollection(itemChoiceGroupCollection, filter);

            foreach (MiChoiceGrpJoinEntity itemChoiceGroupEntity in itemChoiceGroupCollection)
            {
                Posalteration posalteration = this.GetPosalterationForChoiceGroup(adapter, itemChoiceGroupEntity.ChoiceGroupId);
                if (posalteration != null)
                {
                    posalterations.Add(posalteration);
                }
            }
            return posalterations.ToArray();
        }

        private Posalteration GetPosalterationForChoiceGroup(DataAccessAdapter adapter, int choiceGroupId)
        {
            Posalteration posalteration = null;

            RelationPredicateBucket filter = new RelationPredicateBucket();
            filter.PredicateExpression.Add(ChoiceGroupMasterFields.EntId == this.entId);
            filter.PredicateExpression.Add(ChoiceGroupMasterFields.ChoiceGroupId == choiceGroupId);

            EntityCollection<ChoiceGroupMasterEntity> choiceGroupEntityCollection = new EntityCollection<ChoiceGroupMasterEntity>();
            adapter.FetchEntityCollection(choiceGroupEntityCollection, filter);

            if (choiceGroupEntityCollection.Count > 0)
            {
                posalteration = new Posalteration();
                posalteration.ExternalId = choiceGroupEntityCollection[0].ChoiceGroupId.ToString();
                posalteration.Name = choiceGroupEntityCollection[0].ChoiceGroupName;
                posalteration.MinOptions = choiceGroupEntityCollection[0].MinNumMods ?? 0;
                posalteration.MaxOptions = choiceGroupEntityCollection[0].MaxNumMods ?? 0;
                posalteration.Posalterationoptions = this.GetPosalterationoptionsForChoiceGroup(adapter, choiceGroupEntityCollection[0].ChoiceGroupId);
            }

            return posalteration;
        }

        private Posalterationoption[] GetPosalterationoptionsForChoiceGroup(DataAccessAdapter adapter, int choiceGroupId)
        {
            RelationPredicateBucket filter = new RelationPredicateBucket();
            filter.PredicateExpression.Add(ChoiceGrpModJoinFields.EntId == this.entId);
            filter.PredicateExpression.Add(ChoiceGrpModJoinFields.ChoiceGroupId == choiceGroupId);

            EntityCollection<ChoiceGrpModJoinEntity> choicegroupModifierEntityCollection = new EntityCollection<ChoiceGrpModJoinEntity>();
            adapter.FetchEntityCollection(choicegroupModifierEntityCollection, filter);

            List<Posalterationoption> options = new List<Posalterationoption>();
            foreach (ChoiceGrpModJoinEntity choicegroupModifierEntity in choicegroupModifierEntityCollection)
            {
                Posalterationoption posalterationoption = this.GetPosalterationoptionForModifier(adapter,choicegroupModifierEntity.ModifierId);
                if (posalterationoption != null)
                {
                    options.Add(posalterationoption);
                }
            }
            return options.ToArray();
        }

        private Posalterationoption GetPosalterationoptionForModifier(DataAccessAdapter adapter, int modifierId)
        {
            Posalterationoption posalterationoption = null;

            RelationPredicateBucket filter = new RelationPredicateBucket();
            filter.PredicateExpression.Add(ModifierMasterFields.EntId == this.entId);
            filter.PredicateExpression.Add(ModifierMasterFields.ModifierId == modifierId);

            EntityCollection<ModifierMasterEntity> modifierEntityCollection = new EntityCollection<ModifierMasterEntity>();
            adapter.FetchEntityCollection(modifierEntityCollection, filter);

            if (modifierEntityCollection.Count > 0)
            {
                posalterationoption = new Posalterationoption();
                posalterationoption.ExternalId = modifierEntityCollection[0].ModifierId.ToString();
                posalterationoption.Name = modifierEntityCollection[0].ModifierName;
            }

            return posalterationoption;
        }
    }
}
