﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace InfoGenesis.DatabaseSpecific
{
	/// <summary>Singleton implementation of the PersistenceInfoProvider. This class is the singleton wrapper through which the actual instance is retrieved.</summary>
	/// <remarks>It uses a single instance of an internal class. The access isn't marked with locks as the PersistenceInfoProviderBase class is threadsafe.</remarks>
	internal static class PersistenceInfoProviderSingleton
	{
		#region Class Member Declarations
		private static readonly IPersistenceInfoProvider _providerInstance = new PersistenceInfoProviderCore();
		#endregion

		/// <summary>Dummy static constructor to make sure threadsafe initialization is performed.</summary>
		static PersistenceInfoProviderSingleton()
		{
		}

		/// <summary>Gets the singleton instance of the PersistenceInfoProviderCore</summary>
		/// <returns>Instance of the PersistenceInfoProvider.</returns>
		public static IPersistenceInfoProvider GetInstance()
		{
			return _providerInstance;
		}
	}

	/// <summary>Actual implementation of the PersistenceInfoProvider. Used by singleton wrapper.</summary>
	internal class PersistenceInfoProviderCore : PersistenceInfoProviderBase
	{
		/// <summary>Initializes a new instance of the <see cref="PersistenceInfoProviderCore"/> class.</summary>
		internal PersistenceInfoProviderCore()
		{
			Init();
		}

		/// <summary>Method which initializes the internal datastores with the structure of hierarchical types.</summary>
		private void Init()
		{
			this.InitClass(123);
			InitBonusCodeMasterEntityMappings();
			InitButtonAbbrJoinEntityMappings();
			InitCardTypeMasterEntityMappings();
			InitCardTypeRangeJoinEntityMappings();
			InitCfgVersionEntityMappings();
			InitCheckTypeMasterEntityMappings();
			InitChefMasterEntityMappings();
			InitChkTypeGratJoinEntityMappings();
			InitChkTypeGratRevJoinEntityMappings();
			InitChkTypeGratTaxJoinEntityMappings();
			InitChkTypeTaxGrpJoinEntityMappings();
			InitChkTypeTaxGrpTaxJoinEntityMappings();
			InitChkTypeVatGratJoinEntityMappings();
			InitChkTypeVatRevJoinEntityMappings();
			InitChoiceGroupMasterEntityMappings();
			InitChoiceGrpModJoinEntityMappings();
			InitComboItemJoinEntityMappings();
			InitComboMasterEntityMappings();
			InitConfigurationIdMasterEntityMappings();
			InitDeviceMasterEntityMappings();
			InitDiscoupMasterEntityMappings();
			InitEmpGroupMasterEntityMappings();
			InitEmpGrpEmpJoinEntityMappings();
			InitEmpJobcodeJoinEntityMappings();
			InitEmpMasterEntityMappings();
			InitFuncBttnTextJoinEntityMappings();
			InitGa4680ExportParmMasterEntityMappings();
			InitGa4680ImportParmMasterEntityMappings();
			InitGeneralLedgerMapMasterEntityMappings();
			InitGiftCardMasterEntityMappings();
			InitGratuityCatMasterEntityMappings();
			InitGratuityMasterEntityMappings();
			InitIdMasterEntityMappings();
			InitImageLibraryMasterEntityMappings();
			InitIntlDescriptorTextJoinEntityMappings();
			InitIpServerAttribMasterEntityMappings();
			InitJobcodeFuncBttnJoinEntityMappings();
			InitJobcodeFunctJoinEntityMappings();
			InitJobcodeMasterEntityMappings();
			InitKdsCategoryMasterEntityMappings();
			InitKdsVideoMasterEntityMappings();
			InitKpOptionGrpCopyJoinEntityMappings();
			InitKpOptionGrpFormatJoinEntityMappings();
			InitKpOptionGrpMasterEntityMappings();
			InitKpPrinterMasterEntityMappings();
			InitKpRouteMasterEntityMappings();
			InitKpRoutePrinterJoinEntityMappings();
			InitMachineOptionsMasterEntityMappings();
			InitMealPeriodMasterEntityMappings();
			InitMembershipMasterEntityMappings();
			InitMembershipProfitCenterJoinEntityMappings();
			InitMenuBttnObjJoinEntityMappings();
			InitMenuItemGroupMasterEntityMappings();
			InitMenuItemMasterEntityMappings();
			InitMenuMasterEntityMappings();
			InitMenuMigrpJoinEntityMappings();
			InitMiChoiceGrpJoinEntityMappings();
			InitMiKpprinterJoinEntityMappings();
			InitMiPriceJoinEntityMappings();
			InitMiSkuJoinEntityMappings();
			InitModifierKpprinterJoinEntityMappings();
			InitModifierMasterEntityMappings();
			InitModifierPriceJoinEntityMappings();
			InitOptionsMasterEntityMappings();
			InitPriceLevelMasterEntityMappings();
			InitProcessMasterEntityMappings();
			InitProductClassMasterEntityMappings();
			InitProfitCenterDayPartJoinEntityMappings();
			InitProfitCenterGroupJoinEntityMappings();
			InitProfitCenterGroupMasterEntityMappings();
			InitProfitCenterMasterEntityMappings();
			InitProfitCenterReceiptPrinterJoinEntityMappings();
			InitProfitCenterTableJoinEntityMappings();
			InitQuickTenderMasterEntityMappings();
			InitReceiptPrinterMasterEntityMappings();
			InitReportCatMasterEntityMappings();
			InitRevenueCatMasterEntityMappings();
			InitRevenueClassMasterEntityMappings();
			InitRowLockMasterEntityMappings();
			InitScreenTemplateMasterEntityMappings();
			InitScreenTemplateMigrpJoinEntityMappings();
			InitSecurityLevelMasterEntityMappings();
			InitSelectionGroupItemJoinEntityMappings();
			InitSelectionGroupMasterEntityMappings();
			InitServiceManagerProxyMasterEntityMappings();
			InitSmuCommandMasterEntityMappings();
			InitSmuStatusMasterEntityMappings();
			InitSpecialInstrMasterEntityMappings();
			InitSystemConfigurationMasterEntityMappings();
			InitTableLayoutMasterEntityMappings();
			InitTaxCatMasterEntityMappings();
			InitTaxGroupMasterEntityMappings();
			InitTaxGrpTaxJoinEntityMappings();
			InitTaxMasterEntityMappings();
			InitTaxRevCatJoinEntityMappings();
			InitTenderClassMasterEntityMappings();
			InitTenderMasterEntityMappings();
			InitTermGrpChkTypeJoinEntityMappings();
			InitTermGrpJobcodeJoinEntityMappings();
			InitTermGrpMasterEntityMappings();
			InitTermGrpMenuJoinEntityMappings();
			InitTermGrpMenuJoinCookerEntityMappings();
			InitTermGrpMpJoinEntityMappings();
			InitTermGrpMpmenuJoinEntityMappings();
			InitTermGrpTndrJoinEntityMappings();
			InitTermGrpTransitionJoinEntityMappings();
			InitTerminalMasterEntityMappings();
			InitTerminalProfitCtrJoinEntityMappings();
			InitTerminalTextGuestInfoJoinEntityMappings();
			InitTerminalTextMasterEntityMappings();
			InitTermOptionGrpMasterEntityMappings();
			InitTermOptionGrpOptionJoinEntityMappings();
			InitTermPrinterGrpKpprntJoinEntityMappings();
			InitTermPrinterGrpMasterEntityMappings();
			InitTermPrinterGrpRouteJoinEntityMappings();
			InitThemeColorJoinEntityMappings();
			InitThemeFontJoinEntityMappings();
			InitThemeMasterEntityMappings();
			InitThemeObjectJoinEntityMappings();
			InitUdefDateRangeInstanceEntityMappings();
			InitUdefPeriodTypeMasterEntityMappings();
			InitVoidReasonMasterEntityMappings();
			InitZooMasterEntityMappings();
		}

		/// <summary>Inits BonusCodeMasterEntity's mappings</summary>
		private void InitBonusCodeMasterEntityMappings()
		{
			this.AddElementMapping("BonusCodeMasterEntity", @"it_cfg", @"dbo", "Bonus_Code_Master", 6);
			this.AddElementFieldMapping("BonusCodeMasterEntity", "BonusCodeEndDate", "bonus_code_end_date", false, "Char", 8, 0, 0, false, "", null, typeof(System.String), 0);
			this.AddElementFieldMapping("BonusCodeMasterEntity", "BonusCodeGenId", "bonus_code_gen_id", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("BonusCodeMasterEntity", "BonusCodeName", "bonus_code_name", false, "NVarChar", 30, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("BonusCodeMasterEntity", "BonusCodeNo", "bonus_code_no", false, "VarChar", 10, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("BonusCodeMasterEntity", "BonusCodeStartDate", "bonus_code_start_date", false, "Char", 8, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("BonusCodeMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
		}

		/// <summary>Inits ButtonAbbrJoinEntity's mappings</summary>
		private void InitButtonAbbrJoinEntityMappings()
		{
			this.AddElementMapping("ButtonAbbrJoinEntity", @"it_cfg", @"dbo", "Button_Abbr_Join", 4);
			this.AddElementFieldMapping("ButtonAbbrJoinEntity", "ButtonId", "button_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ButtonAbbrJoinEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ButtonAbbrJoinEntity", "UserButtonAbbr1", "user_button_abbr1", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("ButtonAbbrJoinEntity", "UserButtonAbbr2", "user_button_abbr2", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 3);
		}

		/// <summary>Inits CardTypeMasterEntity's mappings</summary>
		private void InitCardTypeMasterEntityMappings()
		{
			this.AddElementMapping("CardTypeMasterEntity", @"it_cfg", @"dbo", "Card_Type_Master", 7);
			this.AddElementFieldMapping("CardTypeMasterEntity", "CardTypeId", "card_type_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("CardTypeMasterEntity", "CardTypeName", "card_type_name", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("CardTypeMasterEntity", "CardTypeNumLength", "card_type_num_length", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("CardTypeMasterEntity", "CardTypePrefixLength", "card_type_prefix_length", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("CardTypeMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("CardTypeMasterEntity", "StoreId", "store_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("CardTypeMasterEntity", "TenderId", "tender_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
		}

		/// <summary>Inits CardTypeRangeJoinEntity's mappings</summary>
		private void InitCardTypeRangeJoinEntityMappings()
		{
			this.AddElementMapping("CardTypeRangeJoinEntity", @"it_cfg", @"dbo", "Card_Type_Range_Join", 5);
			this.AddElementFieldMapping("CardTypeRangeJoinEntity", "CardTypeId", "card_type_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("CardTypeRangeJoinEntity", "CardTypePrefixHigh", "card_type_prefix_high", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("CardTypeRangeJoinEntity", "CardTypePrefixLow", "card_type_prefix_low", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("CardTypeRangeJoinEntity", "CardTypeRangeId", "card_type_range_id", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("CardTypeRangeJoinEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
		}

		/// <summary>Inits CfgVersionEntity's mappings</summary>
		private void InitCfgVersionEntityMappings()
		{
			this.AddElementMapping("CfgVersionEntity", @"it_cfg", @"dbo", "CFG_Version", 5);
			this.AddElementFieldMapping("CfgVersionEntity", "BuildVersion", "build_version", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 0);
			this.AddElementFieldMapping("CfgVersionEntity", "DbName", "DB_name", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("CfgVersionEntity", "MajorVersion", "major_version", false, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 2);
			this.AddElementFieldMapping("CfgVersionEntity", "MinorVersion", "minor_version", false, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 3);
			this.AddElementFieldMapping("CfgVersionEntity", "PatchVersion", "patch_version", false, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 4);
		}

		/// <summary>Inits CheckTypeMasterEntity's mappings</summary>
		private void InitCheckTypeMasterEntityMappings()
		{
			this.AddElementMapping("CheckTypeMasterEntity", @"it_cfg", @"dbo", "Check_Type_Master", 13);
			this.AddElementFieldMapping("CheckTypeMasterEntity", "CheckTypeAbbr1", "check_type_abbr1", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 0);
			this.AddElementFieldMapping("CheckTypeMasterEntity", "CheckTypeAbbr2", "check_type_abbr2", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("CheckTypeMasterEntity", "CheckTypeId", "check_type_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("CheckTypeMasterEntity", "CheckTypeName", "check_type_name", true, "NVarChar", 16, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("CheckTypeMasterEntity", "DefaultPriceLevelId", "default_price_level_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("CheckTypeMasterEntity", "DefaultSecId", "default_sec_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("CheckTypeMasterEntity", "DiscountId", "discount_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("CheckTypeMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("CheckTypeMasterEntity", "RoundBasis", "round_basis", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("CheckTypeMasterEntity", "RoundTypeId", "round_type_id", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 9);
			this.AddElementFieldMapping("CheckTypeMasterEntity", "RowVersion", "row_version", false, "Timestamp", 2147483647, 0, 0, false, "", null, typeof(System.Byte[]), 10);
			this.AddElementFieldMapping("CheckTypeMasterEntity", "SalesTippableFlag", "sales_tippable_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 11);
			this.AddElementFieldMapping("CheckTypeMasterEntity", "StoreId", "store_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
		}

		/// <summary>Inits ChefMasterEntity's mappings</summary>
		private void InitChefMasterEntityMappings()
		{
			this.AddElementMapping("ChefMasterEntity", @"it_cfg", @"dbo", "Chef_Master", 8);
			this.AddElementFieldMapping("ChefMasterEntity", "CookedFlag", "cooked_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 0);
			this.AddElementFieldMapping("ChefMasterEntity", "CookerHandle", "cooker_handle", true, "Decimal", 0, 20, 10, false, "", null, typeof(System.Decimal), 1);
			this.AddElementFieldMapping("ChefMasterEntity", "CookerKeyId", "cooker_key_id", false, "BigInt", 0, 19, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int64), 2);
			this.AddElementFieldMapping("ChefMasterEntity", "CustomerId", "customer_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("ChefMasterEntity", "DivisionId", "division_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("ChefMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("ChefMasterEntity", "LsrName", "lsr_name", true, "NVarChar", 20, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("ChefMasterEntity", "StoreId", "store_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
		}

		/// <summary>Inits ChkTypeGratJoinEntity's mappings</summary>
		private void InitChkTypeGratJoinEntityMappings()
		{
			this.AddElementMapping("ChkTypeGratJoinEntity", @"it_cfg", @"dbo", "ChkType_Grat_Join", 5);
			this.AddElementFieldMapping("ChkTypeGratJoinEntity", "CheckTypeId", "check_type_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ChkTypeGratJoinEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ChkTypeGratJoinEntity", "GratId", "grat_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ChkTypeGratJoinEntity", "RevCatCodeId", "rev_cat_code_id", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 3);
			this.AddElementFieldMapping("ChkTypeGratJoinEntity", "TaxCodeId", "tax_code_id", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 4);
		}

		/// <summary>Inits ChkTypeGratRevJoinEntity's mappings</summary>
		private void InitChkTypeGratRevJoinEntityMappings()
		{
			this.AddElementMapping("ChkTypeGratRevJoinEntity", @"it_cfg", @"dbo", "ChkType_Grat_Rev_Join", 4);
			this.AddElementFieldMapping("ChkTypeGratRevJoinEntity", "CheckTypeId", "check_type_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ChkTypeGratRevJoinEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ChkTypeGratRevJoinEntity", "GratId", "grat_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ChkTypeGratRevJoinEntity", "RevCatId", "rev_cat_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
		}

		/// <summary>Inits ChkTypeGratTaxJoinEntity's mappings</summary>
		private void InitChkTypeGratTaxJoinEntityMappings()
		{
			this.AddElementMapping("ChkTypeGratTaxJoinEntity", @"it_cfg", @"dbo", "ChkType_Grat_Tax_Join", 4);
			this.AddElementFieldMapping("ChkTypeGratTaxJoinEntity", "CheckTypeId", "check_type_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ChkTypeGratTaxJoinEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ChkTypeGratTaxJoinEntity", "GratId", "grat_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ChkTypeGratTaxJoinEntity", "TaxId", "tax_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
		}

		/// <summary>Inits ChkTypeTaxGrpJoinEntity's mappings</summary>
		private void InitChkTypeTaxGrpJoinEntityMappings()
		{
			this.AddElementMapping("ChkTypeTaxGrpJoinEntity", @"it_cfg", @"dbo", "ChkType_TaxGrp_Join", 4);
			this.AddElementFieldMapping("ChkTypeTaxGrpJoinEntity", "CheckTypeId", "check_type_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ChkTypeTaxGrpJoinEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ChkTypeTaxGrpJoinEntity", "TaxCodeId", "tax_code_id", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 2);
			this.AddElementFieldMapping("ChkTypeTaxGrpJoinEntity", "TaxGrpId", "tax_grp_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
		}

		/// <summary>Inits ChkTypeTaxGrpTaxJoinEntity's mappings</summary>
		private void InitChkTypeTaxGrpTaxJoinEntityMappings()
		{
			this.AddElementMapping("ChkTypeTaxGrpTaxJoinEntity", @"it_cfg", @"dbo", "ChkType_TaxGrp_Tax_Join", 4);
			this.AddElementFieldMapping("ChkTypeTaxGrpTaxJoinEntity", "CheckTypeId", "check_type_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ChkTypeTaxGrpTaxJoinEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ChkTypeTaxGrpTaxJoinEntity", "TaxGrpId", "tax_grp_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ChkTypeTaxGrpTaxJoinEntity", "TaxId", "tax_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
		}

		/// <summary>Inits ChkTypeVatGratJoinEntity's mappings</summary>
		private void InitChkTypeVatGratJoinEntityMappings()
		{
			this.AddElementMapping("ChkTypeVatGratJoinEntity", @"it_cfg", @"dbo", "ChkType_VAT_Grat_Join", 4);
			this.AddElementFieldMapping("ChkTypeVatGratJoinEntity", "CheckTypeId", "check_type_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ChkTypeVatGratJoinEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ChkTypeVatGratJoinEntity", "GratId", "grat_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ChkTypeVatGratJoinEntity", "TaxId", "tax_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
		}

		/// <summary>Inits ChkTypeVatRevJoinEntity's mappings</summary>
		private void InitChkTypeVatRevJoinEntityMappings()
		{
			this.AddElementMapping("ChkTypeVatRevJoinEntity", @"it_cfg", @"dbo", "ChkType_VAT_Rev_Join", 4);
			this.AddElementFieldMapping("ChkTypeVatRevJoinEntity", "CheckTypeId", "check_type_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ChkTypeVatRevJoinEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ChkTypeVatRevJoinEntity", "RevCatId", "rev_cat_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ChkTypeVatRevJoinEntity", "TaxId", "tax_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
		}

		/// <summary>Inits ChoiceGroupMasterEntity's mappings</summary>
		private void InitChoiceGroupMasterEntityMappings()
		{
			this.AddElementMapping("ChoiceGroupMasterEntity", @"it_cfg", @"dbo", "ChoiceGroup_Master", 11);
			this.AddElementFieldMapping("ChoiceGroupMasterEntity", "ChoiceGroupAbbr1", "choice_group_abbr1", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 0);
			this.AddElementFieldMapping("ChoiceGroupMasterEntity", "ChoiceGroupAbbr2", "choice_group_abbr2", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("ChoiceGroupMasterEntity", "ChoiceGroupId", "choice_group_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ChoiceGroupMasterEntity", "ChoiceGroupLabel", "choice_group_label", true, "NVarChar", 20, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("ChoiceGroupMasterEntity", "ChoiceGroupName", "choice_group_name", true, "NVarChar", 16, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("ChoiceGroupMasterEntity", "DataControlGroupId", "data_control_group_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("ChoiceGroupMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("ChoiceGroupMasterEntity", "MaxNumMods", "max_num_mods", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 7);
			this.AddElementFieldMapping("ChoiceGroupMasterEntity", "MinNumMods", "min_num_mods", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 8);
			this.AddElementFieldMapping("ChoiceGroupMasterEntity", "NumFreeMods", "num_free_mods", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 9);
			this.AddElementFieldMapping("ChoiceGroupMasterEntity", "StoreId", "store_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
		}

		/// <summary>Inits ChoiceGrpModJoinEntity's mappings</summary>
		private void InitChoiceGrpModJoinEntityMappings()
		{
			this.AddElementMapping("ChoiceGrpModJoinEntity", @"it_cfg", @"dbo", "ChoiceGrp_Mod_Join", 4);
			this.AddElementFieldMapping("ChoiceGrpModJoinEntity", "ChoiceGroupId", "choice_group_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ChoiceGrpModJoinEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ChoiceGrpModJoinEntity", "ModifierChoicegrpSeq", "modifier_choicegrp_seq", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 2);
			this.AddElementFieldMapping("ChoiceGrpModJoinEntity", "ModifierId", "modifier_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
		}

		/// <summary>Inits ComboItemJoinEntity's mappings</summary>
		private void InitComboItemJoinEntityMappings()
		{
			this.AddElementMapping("ComboItemJoinEntity", @"it_cfg", @"dbo", "Combo_Item_Join", 9);
			this.AddElementFieldMapping("ComboItemJoinEntity", "ComboId", "combo_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ComboItemJoinEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ComboItemJoinEntity", "ItemPrice", "item_price", false, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 2);
			this.AddElementFieldMapping("ComboItemJoinEntity", "ItemQty", "item_qty", false, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 3);
			this.AddElementFieldMapping("ComboItemJoinEntity", "KeyId", "key_id", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("ComboItemJoinEntity", "ObjectId", "object_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("ComboItemJoinEntity", "ObjectTypeId", "object_type_id", false, "TinyInt", 0, 3, 0, false, "", null, typeof(System.Byte), 6);
			this.AddElementFieldMapping("ComboItemJoinEntity", "PrintReceiptFlag", "print_receipt_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 7);
			this.AddElementFieldMapping("ComboItemJoinEntity", "SequenceId", "sequence_id", false, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 8);
		}

		/// <summary>Inits ComboMasterEntity's mappings</summary>
		private void InitComboMasterEntityMappings()
		{
			this.AddElementMapping("ComboMasterEntity", @"it_cfg", @"dbo", "Combo_Master", 11);
			this.AddElementFieldMapping("ComboMasterEntity", "ComboAbbr1", "combo_abbr1", false, "NVarChar", 20, 0, 0, false, "", null, typeof(System.String), 0);
			this.AddElementFieldMapping("ComboMasterEntity", "ComboAbbr2", "combo_abbr2", true, "NVarChar", 20, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("ComboMasterEntity", "ComboId", "combo_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ComboMasterEntity", "ComboKpLabel", "combo_KP_label", false, "NVarChar", 16, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("ComboMasterEntity", "ComboMenuItemId", "combo_menu_item_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("ComboMasterEntity", "ComboName", "combo_name", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("ComboMasterEntity", "ComboReceiptLabel", "combo_receipt_label", false, "NVarChar", 16, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("ComboMasterEntity", "DivisionId", "division_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("ComboMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("ComboMasterEntity", "KdsVideoLabel", "kds_video_label", true, "NVarChar", 20, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("ComboMasterEntity", "RowVersion", "row_version", false, "Timestamp", 2147483647, 0, 0, false, "", null, typeof(System.Byte[]), 10);
		}

		/// <summary>Inits ConfigurationIdMasterEntity's mappings</summary>
		private void InitConfigurationIdMasterEntityMappings()
		{
			this.AddElementMapping("ConfigurationIdMasterEntity", @"it_cfg", @"dbo", "Configuration_ID_Master", 7);
			this.AddElementFieldMapping("ConfigurationIdMasterEntity", "CustomerId", "customer_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ConfigurationIdMasterEntity", "DivId", "div_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ConfigurationIdMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ConfigurationIdMasterEntity", "ReservedId", "reserved_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("ConfigurationIdMasterEntity", "StoreId", "store_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("ConfigurationIdMasterEntity", "TableName", "table_name", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("ConfigurationIdMasterEntity", "Timeout", "timeout", false, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
		}

		/// <summary>Inits DeviceMasterEntity's mappings</summary>
		private void InitDeviceMasterEntityMappings()
		{
			this.AddElementMapping("DeviceMasterEntity", @"it_cfg", @"dbo", "Device_Master", 25);
			this.AddElementFieldMapping("DeviceMasterEntity", "BbArmingCharacter", "BB_arming_character", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 0);
			this.AddElementFieldMapping("DeviceMasterEntity", "BbOffsetId", "BB_offset_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("DeviceMasterEntity", "ConnectedDeviceId", "connected_device_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("DeviceMasterEntity", "ConnectionCodeId", "connection_code_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("DeviceMasterEntity", "DeviceAbbr", "device_abbr", true, "NVarChar", 4, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("DeviceMasterEntity", "DeviceDesc", "device_desc", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("DeviceMasterEntity", "DeviceId", "device_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("DeviceMasterEntity", "DeviceName", "device_name", true, "NVarChar", 20, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("DeviceMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("DeviceMasterEntity", "HardwareTypeId", "hardware_type_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("DeviceMasterEntity", "IpAddress", "ip_address", true, "NVarChar", 15, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("DeviceMasterEntity", "IpPrintingPort", "ip_printing_port", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 11);
			this.AddElementFieldMapping("DeviceMasterEntity", "IpStatusPort", "ip_status_port", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 12);
			this.AddElementFieldMapping("DeviceMasterEntity", "KpBackupDeviceId", "KP_backup_device_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 13);
			this.AddElementFieldMapping("DeviceMasterEntity", "KpOptionGrpId", "KP_option_grp_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 14);
			this.AddElementFieldMapping("DeviceMasterEntity", "ModelId", "model_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 15);
			this.AddElementFieldMapping("DeviceMasterEntity", "PortId", "port_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 16);
			this.AddElementFieldMapping("DeviceMasterEntity", "PrimaryLanguageId", "primary_language_id", false, "NVarChar", 9, 0, 0, false, "", null, typeof(System.String), 17);
			this.AddElementFieldMapping("DeviceMasterEntity", "SecondaryLanguageId", "secondary_language_id", true, "NVarChar", 9, 0, 0, false, "", null, typeof(System.String), 18);
			this.AddElementFieldMapping("DeviceMasterEntity", "StoreId", "store_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 19);
			this.AddElementFieldMapping("DeviceMasterEntity", "XferBaudRate", "xfer_baud_rate", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 20);
			this.AddElementFieldMapping("DeviceMasterEntity", "XferDataBits", "xfer_data_bits", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 21);
			this.AddElementFieldMapping("DeviceMasterEntity", "XferHandshakeId", "xfer_handshake_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 22);
			this.AddElementFieldMapping("DeviceMasterEntity", "XferParityId", "xfer_parity_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 23);
			this.AddElementFieldMapping("DeviceMasterEntity", "XferStopBits", "xfer_stop_bits", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 24);
		}

		/// <summary>Inits DiscoupMasterEntity's mappings</summary>
		private void InitDiscoupMasterEntityMappings()
		{
			this.AddElementMapping("DiscoupMasterEntity", @"it_cfg", @"dbo", "Discoup_Master", 29);
			this.AddElementFieldMapping("DiscoupMasterEntity", "AssocTenderId", "assoc_tender_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("DiscoupMasterEntity", "BevRevClassFlag", "bev_rev_class_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 1);
			this.AddElementFieldMapping("DiscoupMasterEntity", "DiscountExtraPromptCode", "discount_extra_prompt_code", false, "TinyInt", 0, 3, 0, false, "", null, typeof(System.Byte), 2);
			this.AddElementFieldMapping("DiscoupMasterEntity", "DiscoupAbbr1", "discoup_abbr1", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("DiscoupMasterEntity", "DiscoupAbbr2", "discoup_abbr2", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("DiscoupMasterEntity", "DiscoupAmt", "discoup_amt", true, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 5);
			this.AddElementFieldMapping("DiscoupMasterEntity", "DiscoupId", "discoup_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("DiscoupMasterEntity", "DiscoupItemLevelCodeId", "discoup_item_level_code_id", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 7);
			this.AddElementFieldMapping("DiscoupMasterEntity", "DiscoupMaxAmt", "discoup_max_amt", true, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 8);
			this.AddElementFieldMapping("DiscoupMasterEntity", "DiscoupMaxPercent", "discoup_max_percent", true, "Float", 0, 38, 0, false, "", null, typeof(System.Double), 9);
			this.AddElementFieldMapping("DiscoupMasterEntity", "DiscoupName", "discoup_name", true, "NVarChar", 16, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("DiscoupMasterEntity", "DiscoupOpenCodeId", "discoup_open_code_id", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 11);
			this.AddElementFieldMapping("DiscoupMasterEntity", "DiscoupPctAmtCodeId", "discoup_pct_amt_code_id", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 12);
			this.AddElementFieldMapping("DiscoupMasterEntity", "DiscoupPercent", "discoup_percent", true, "Float", 0, 38, 0, false, "", null, typeof(System.Double), 13);
			this.AddElementFieldMapping("DiscoupMasterEntity", "DiscoupTypeCodeId", "discoup_type_code_id", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 14);
			this.AddElementFieldMapping("DiscoupMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 15);
			this.AddElementFieldMapping("DiscoupMasterEntity", "ExclusiveFlag", "exclusive_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 16);
			this.AddElementFieldMapping("DiscoupMasterEntity", "FoodRevClassFlag", "food_rev_class_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 17);
			this.AddElementFieldMapping("DiscoupMasterEntity", "OtherRevClassFlag", "other_rev_class_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 18);
			this.AddElementFieldMapping("DiscoupMasterEntity", "PostAcctNo", "post_acct_no", true, "NVarChar", 30, 0, 0, false, "", null, typeof(System.String), 19);
			this.AddElementFieldMapping("DiscoupMasterEntity", "ProfitCtrGrpId", "profit_ctr_grp_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 20);
			this.AddElementFieldMapping("DiscoupMasterEntity", "PromptExtraDataFlag", "prompt_extra_data_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 21);
			this.AddElementFieldMapping("DiscoupMasterEntity", "RoundBasis", "round_basis", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 22);
			this.AddElementFieldMapping("DiscoupMasterEntity", "RoundTypeId", "round_type_id", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 23);
			this.AddElementFieldMapping("DiscoupMasterEntity", "RowVersion", "row_version", false, "Timestamp", 2147483647, 0, 0, false, "", null, typeof(System.Byte[]), 24);
			this.AddElementFieldMapping("DiscoupMasterEntity", "SecurityId", "security_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 25);
			this.AddElementFieldMapping("DiscoupMasterEntity", "SodaRevClassFlag", "soda_rev_class_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 26);
			this.AddElementFieldMapping("DiscoupMasterEntity", "StoreId", "store_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 27);
			this.AddElementFieldMapping("DiscoupMasterEntity", "ThreshholdAmt", "threshhold_amt", true, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 28);
		}

		/// <summary>Inits EmpGroupMasterEntity's mappings</summary>
		private void InitEmpGroupMasterEntityMappings()
		{
			this.AddElementMapping("EmpGroupMasterEntity", @"it_cfg", @"dbo", "Emp_Group_Master", 7);
			this.AddElementFieldMapping("EmpGroupMasterEntity", "EmpAutoUpdateFlag", "emp_auto_update_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 0);
			this.AddElementFieldMapping("EmpGroupMasterEntity", "EmpGrpAbbr1", "emp_grp_abbr1", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("EmpGroupMasterEntity", "EmpGrpAbbr2", "emp_grp_abbr2", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("EmpGroupMasterEntity", "EmpGrpId", "emp_grp_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("EmpGroupMasterEntity", "EmpGrpName", "emp_grp_name", true, "NVarChar", 30, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("EmpGroupMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("EmpGroupMasterEntity", "StoreId", "store_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
		}

		/// <summary>Inits EmpGrpEmpJoinEntity's mappings</summary>
		private void InitEmpGrpEmpJoinEntityMappings()
		{
			this.AddElementMapping("EmpGrpEmpJoinEntity", @"it_cfg", @"dbo", "EmpGrp_Emp_Join", 3);
			this.AddElementFieldMapping("EmpGrpEmpJoinEntity", "EmpGrpId", "emp_grp_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("EmpGrpEmpJoinEntity", "EmpId", "emp_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("EmpGrpEmpJoinEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
		}

		/// <summary>Inits EmpJobcodeJoinEntity's mappings</summary>
		private void InitEmpJobcodeJoinEntityMappings()
		{
			this.AddElementMapping("EmpJobcodeJoinEntity", @"it_cfg", @"dbo", "Emp_Jobcode_Join", 8);
			this.AddElementFieldMapping("EmpJobcodeJoinEntity", "EmpId", "emp_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("EmpJobcodeJoinEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("EmpJobcodeJoinEntity", "JobcodeAddDt", "jobcode_add_dt", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 2);
			this.AddElementFieldMapping("EmpJobcodeJoinEntity", "JobcodeId", "jobcode_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("EmpJobcodeJoinEntity", "JobcodeLaborRate", "jobcode_labor_rate", true, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 4);
			this.AddElementFieldMapping("EmpJobcodeJoinEntity", "ManagerFlag", "manager_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 5);
			this.AddElementFieldMapping("EmpJobcodeJoinEntity", "MultipleSignonFlag", "multiple_signon_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 6);
			this.AddElementFieldMapping("EmpJobcodeJoinEntity", "PrimaryJobcodeFlag", "primary_jobcode_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 7);
		}

		/// <summary>Inits EmpMasterEntity's mappings</summary>
		private void InitEmpMasterEntityMappings()
		{
			this.AddElementMapping("EmpMasterEntity", @"it_cfg", @"dbo", "Emp_Master", 27);
			this.AddElementFieldMapping("EmpMasterEntity", "DataControlGroupId", "data_control_group_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("EmpMasterEntity", "DefaultLanguageId", "default_language_id", false, "NVarChar", 9, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("EmpMasterEntity", "EmpBirthdate", "emp_birthdate", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 2);
			this.AddElementFieldMapping("EmpMasterEntity", "EmpCardNo", "emp_card_no", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("EmpMasterEntity", "EmpCity", "emp_city", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("EmpMasterEntity", "EmpCountry", "emp_country", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("EmpMasterEntity", "EmpEmergencyTel", "emp_emergency_tel", true, "NVarChar", 16, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("EmpMasterEntity", "EmpFirstName", "emp_first_name", true, "NVarChar", 16, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("EmpMasterEntity", "EmpHireDt", "emp_hire_dt", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("EmpMasterEntity", "EmpId", "emp_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("EmpMasterEntity", "EmpLastName", "emp_last_name", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("EmpMasterEntity", "EmpLastReviewDt", "emp_last_review_dt", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 11);
			this.AddElementFieldMapping("EmpMasterEntity", "EmpNextReviewDt", "emp_next_review_dt", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 12);
			this.AddElementFieldMapping("EmpMasterEntity", "EmpPassword", "emp_password", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 13);
			this.AddElementFieldMapping("EmpMasterEntity", "EmpPosName", "emp_pos_name", true, "NVarChar", 16, 0, 0, false, "", null, typeof(System.String), 14);
			this.AddElementFieldMapping("EmpMasterEntity", "EmpSsn", "emp_SSN", true, "NVarChar", 12, 0, 0, false, "", null, typeof(System.String), 15);
			this.AddElementFieldMapping("EmpMasterEntity", "EmpState", "emp_state", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 16);
			this.AddElementFieldMapping("EmpMasterEntity", "EmpStreet1", "emp_street1", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 17);
			this.AddElementFieldMapping("EmpMasterEntity", "EmpStreet2", "emp_street2", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 18);
			this.AddElementFieldMapping("EmpMasterEntity", "EmpTel", "emp_tel", true, "NVarChar", 16, 0, 0, false, "", null, typeof(System.String), 19);
			this.AddElementFieldMapping("EmpMasterEntity", "EmpTerminateDt", "emp_terminate_dt", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 20);
			this.AddElementFieldMapping("EmpMasterEntity", "EmpZip", "emp_zip", true, "NVarChar", 16, 0, 0, false, "", null, typeof(System.String), 21);
			this.AddElementFieldMapping("EmpMasterEntity", "EntId", "ent_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 22);
			this.AddElementFieldMapping("EmpMasterEntity", "RowVersion", "row_version", false, "Timestamp", 2147483647, 0, 0, false, "", null, typeof(System.Byte[]), 23);
			this.AddElementFieldMapping("EmpMasterEntity", "SecondaryId", "secondary_id", true, "NVarChar", 10, 0, 0, false, "", null, typeof(System.String), 24);
			this.AddElementFieldMapping("EmpMasterEntity", "StoreId", "store_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 25);
			this.AddElementFieldMapping("EmpMasterEntity", "SupervisorEmpId", "supervisor_emp_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 26);
		}

		/// <summary>Inits FuncBttnTextJoinEntity's mappings</summary>
		private void InitFuncBttnTextJoinEntityMappings()
		{
			this.AddElementMapping("FuncBttnTextJoinEntity", @"it_cfg", @"dbo", "FuncBttn_Text_Join", 3);
			this.AddElementFieldMapping("FuncBttnTextJoinEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("FuncBttnTextJoinEntity", "FuncbttnId", "funcbttn_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("FuncBttnTextJoinEntity", "FuncbttnText", "funcbttn_text", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 2);
		}

		/// <summary>Inits Ga4680ExportParmMasterEntity's mappings</summary>
		private void InitGa4680ExportParmMasterEntityMappings()
		{
			this.AddElementMapping("Ga4680ExportParmMasterEntity", @"it_cfg", @"dbo", "GA_4680_Export_Parm_Master", 26);
			this.AddElementFieldMapping("Ga4680ExportParmMasterEntity", "BusinessDayOffset", "business_day_offset", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 0);
			this.AddElementFieldMapping("Ga4680ExportParmMasterEntity", "BusinessDayOptionId", "business_day_option_id", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 1);
			this.AddElementFieldMapping("Ga4680ExportParmMasterEntity", "DelimiterValue", "delimiter_value", true, "NVarChar", 3, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("Ga4680ExportParmMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("Ga4680ExportParmMasterEntity", "FileFormatId", "file_format_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("Ga4680ExportParmMasterEntity", "Ga1ExportPath", "GA1_export_path", true, "NVarChar", 120, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("Ga4680ExportParmMasterEntity", "Ga1RequiredFlag", "GA1_required_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 6);
			this.AddElementFieldMapping("Ga4680ExportParmMasterEntity", "Ga2ExportPath", "GA2_export_path", true, "NVarChar", 120, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("Ga4680ExportParmMasterEntity", "Ga2RequiredFlag", "GA2_required_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 8);
			this.AddElementFieldMapping("Ga4680ExportParmMasterEntity", "Ga3ExportPath", "GA3_export_path", true, "NVarChar", 120, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("Ga4680ExportParmMasterEntity", "Ga3RequiredFlag", "GA3_required_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 10);
			this.AddElementFieldMapping("Ga4680ExportParmMasterEntity", "Ga4ExportPath", "GA4_export_path", true, "NVarChar", 120, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("Ga4680ExportParmMasterEntity", "Ga4RequiredFlag", "GA4_required_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 12);
			this.AddElementFieldMapping("Ga4680ExportParmMasterEntity", "Ga5ExportPath", "GA5_export_path", true, "NVarChar", 120, 0, 0, false, "", null, typeof(System.String), 13);
			this.AddElementFieldMapping("Ga4680ExportParmMasterEntity", "Ga5RequiredFlag", "GA5_required_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 14);
			this.AddElementFieldMapping("Ga4680ExportParmMasterEntity", "Ga6ExportPath", "GA6_export_path", true, "NVarChar", 120, 0, 0, false, "", null, typeof(System.String), 15);
			this.AddElementFieldMapping("Ga4680ExportParmMasterEntity", "Ga6RequiredFlag", "GA6_required_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 16);
			this.AddElementFieldMapping("Ga4680ExportParmMasterEntity", "Ga7ExportPath", "GA7_export_path", true, "NVarChar", 120, 0, 0, false, "", null, typeof(System.String), 17);
			this.AddElementFieldMapping("Ga4680ExportParmMasterEntity", "Ga7RequiredFlag", "GA7_required_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 18);
			this.AddElementFieldMapping("Ga4680ExportParmMasterEntity", "Ga8ExportPath", "GA8_export_path", true, "NVarChar", 120, 0, 0, false, "", null, typeof(System.String), 19);
			this.AddElementFieldMapping("Ga4680ExportParmMasterEntity", "Ga8RequiredFlag", "GA8_required_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 20);
			this.AddElementFieldMapping("Ga4680ExportParmMasterEntity", "Ga9ExportPath", "GA9_export_path", true, "NVarChar", 120, 0, 0, false, "", null, typeof(System.String), 21);
			this.AddElementFieldMapping("Ga4680ExportParmMasterEntity", "Ga9RequiredFlag", "GA9_required_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 22);
			this.AddElementFieldMapping("Ga4680ExportParmMasterEntity", "GaExportId", "GA_export_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 23);
			this.AddElementFieldMapping("Ga4680ExportParmMasterEntity", "LocationCode", "location_code", true, "NVarChar", 2, 0, 0, false, "", null, typeof(System.String), 24);
			this.AddElementFieldMapping("Ga4680ExportParmMasterEntity", "NumDays", "num_days", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 25);
		}

		/// <summary>Inits Ga4680ImportParmMasterEntity's mappings</summary>
		private void InitGa4680ImportParmMasterEntityMappings()
		{
			this.AddElementMapping("Ga4680ImportParmMasterEntity", @"it_cfg", @"dbo", "GA_4680_Import_Parm_Master", 32);
			this.AddElementFieldMapping("Ga4680ImportParmMasterEntity", "DelimiterRequiredFlag", "delimiter_required_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 0);
			this.AddElementFieldMapping("Ga4680ImportParmMasterEntity", "DelimiterValue", "delimiter_value", true, "NVarChar", 3, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("Ga4680ImportParmMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("Ga4680ImportParmMasterEntity", "Ga1ImportPath", "GA1_import_path", true, "NVarChar", 120, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("Ga4680ImportParmMasterEntity", "Ga1PreimportOptionId", "GA1_preimport_option_id", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 4);
			this.AddElementFieldMapping("Ga4680ImportParmMasterEntity", "Ga1RequiredFlag", "GA1_required_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 5);
			this.AddElementFieldMapping("Ga4680ImportParmMasterEntity", "Ga2ImportPath", "GA2_import_path", true, "NVarChar", 120, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("Ga4680ImportParmMasterEntity", "Ga2PreimportOptionId", "GA2_preimport_option_id", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 7);
			this.AddElementFieldMapping("Ga4680ImportParmMasterEntity", "Ga2RequiredFlag", "GA2_required_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 8);
			this.AddElementFieldMapping("Ga4680ImportParmMasterEntity", "Ga3ImportPath", "GA3_import_path", true, "NVarChar", 120, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("Ga4680ImportParmMasterEntity", "Ga3PreimportOptionId", "GA3_preimport_option_id", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 10);
			this.AddElementFieldMapping("Ga4680ImportParmMasterEntity", "Ga3RequiredFlag", "GA3_required_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 11);
			this.AddElementFieldMapping("Ga4680ImportParmMasterEntity", "Ga4ImportPath", "GA4_import_path", true, "NVarChar", 120, 0, 0, false, "", null, typeof(System.String), 12);
			this.AddElementFieldMapping("Ga4680ImportParmMasterEntity", "Ga4PreimportOptionId", "GA4_preimport_option_id", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 13);
			this.AddElementFieldMapping("Ga4680ImportParmMasterEntity", "Ga4RequiredFlag", "GA4_required_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 14);
			this.AddElementFieldMapping("Ga4680ImportParmMasterEntity", "Ga5ImportPath", "GA5_import_path", true, "NVarChar", 120, 0, 0, false, "", null, typeof(System.String), 15);
			this.AddElementFieldMapping("Ga4680ImportParmMasterEntity", "Ga5PreimportOptionId", "GA5_preimport_option_id", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 16);
			this.AddElementFieldMapping("Ga4680ImportParmMasterEntity", "Ga5RequiredFlag", "GA5_required_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 17);
			this.AddElementFieldMapping("Ga4680ImportParmMasterEntity", "Ga6ImportPath", "GA6_import_path", true, "NVarChar", 120, 0, 0, false, "", null, typeof(System.String), 18);
			this.AddElementFieldMapping("Ga4680ImportParmMasterEntity", "Ga6PreimportOptionId", "GA6_preimport_option_id", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 19);
			this.AddElementFieldMapping("Ga4680ImportParmMasterEntity", "Ga6RequiredFlag", "GA6_required_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 20);
			this.AddElementFieldMapping("Ga4680ImportParmMasterEntity", "Ga7ImportPath", "GA7_import_path", true, "NVarChar", 120, 0, 0, false, "", null, typeof(System.String), 21);
			this.AddElementFieldMapping("Ga4680ImportParmMasterEntity", "Ga7PreimportOptionId", "GA7_preimport_option_id", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 22);
			this.AddElementFieldMapping("Ga4680ImportParmMasterEntity", "Ga7RequiredFlag", "GA7_required_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 23);
			this.AddElementFieldMapping("Ga4680ImportParmMasterEntity", "Ga8ImportPath", "GA8_import_path", true, "NVarChar", 120, 0, 0, false, "", null, typeof(System.String), 24);
			this.AddElementFieldMapping("Ga4680ImportParmMasterEntity", "Ga8PreimportOptionId", "GA8_preimport_option_id", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 25);
			this.AddElementFieldMapping("Ga4680ImportParmMasterEntity", "Ga8RequiredFlag", "GA8_required_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 26);
			this.AddElementFieldMapping("Ga4680ImportParmMasterEntity", "Ga9ImportPath", "GA9_import_path", true, "NVarChar", 120, 0, 0, false, "", null, typeof(System.String), 27);
			this.AddElementFieldMapping("Ga4680ImportParmMasterEntity", "Ga9PreimportOptionId", "GA9_preimport_option_id", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 28);
			this.AddElementFieldMapping("Ga4680ImportParmMasterEntity", "Ga9RequiredFlag", "GA9_required_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 29);
			this.AddElementFieldMapping("Ga4680ImportParmMasterEntity", "GaImportId", "GA_import_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 30);
			this.AddElementFieldMapping("Ga4680ImportParmMasterEntity", "PreimportOptionId", "preimport_option_id", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 31);
		}

		/// <summary>Inits GeneralLedgerMapMasterEntity's mappings</summary>
		private void InitGeneralLedgerMapMasterEntityMappings()
		{
			this.AddElementMapping("GeneralLedgerMapMasterEntity", @"it_cfg", @"dbo", "General_Ledger_Map_Master", 8);
			this.AddElementFieldMapping("GeneralLedgerMapMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("GeneralLedgerMapMasterEntity", "GlAccountNo", "gl_account_no", true, "NVarChar", 20, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("GeneralLedgerMapMasterEntity", "GlClassId", "gl_class_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("GeneralLedgerMapMasterEntity", "GlMapGenId", "gl_map_gen_id", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("GeneralLedgerMapMasterEntity", "Parm1ObjectId", "parm1_object_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("GeneralLedgerMapMasterEntity", "Parm2ObjectId", "parm2_object_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("GeneralLedgerMapMasterEntity", "ProfitCenterId", "profit_center_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("GeneralLedgerMapMasterEntity", "ValidPermutationFlag", "valid_permutation_flag", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 7);
		}

		/// <summary>Inits GiftCardMasterEntity's mappings</summary>
		private void InitGiftCardMasterEntityMappings()
		{
			this.AddElementMapping("GiftCardMasterEntity", @"it_cfg", @"dbo", "Gift_Card_Master", 11);
			this.AddElementFieldMapping("GiftCardMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("GiftCardMasterEntity", "GiftCardAbbr1", "gift_card_abbr1", false, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("GiftCardMasterEntity", "GiftCardAbbr2", "gift_card_abbr2", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("GiftCardMasterEntity", "GiftCardAmt", "gift_card_amt", true, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 3);
			this.AddElementFieldMapping("GiftCardMasterEntity", "GiftCardId", "gift_card_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("GiftCardMasterEntity", "GiftCardMaxAmt", "gift_card_max_amt", true, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 5);
			this.AddElementFieldMapping("GiftCardMasterEntity", "GiftCardMinAmt", "gift_card_min_amt", true, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 6);
			this.AddElementFieldMapping("GiftCardMasterEntity", "GiftCardName", "gift_card_name", false, "NVarChar", 16, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("GiftCardMasterEntity", "GiftCardOpenCodeId", "gift_card_open_code_id", false, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 8);
			this.AddElementFieldMapping("GiftCardMasterEntity", "MenuItemId", "menu_item_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("GiftCardMasterEntity", "NodeId", "node_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
		}

		/// <summary>Inits GratuityCatMasterEntity's mappings</summary>
		private void InitGratuityCatMasterEntityMappings()
		{
			this.AddElementMapping("GratuityCatMasterEntity", @"it_cfg", @"dbo", "Gratuity_Cat_Master", 10);
			this.AddElementFieldMapping("GratuityCatMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("GratuityCatMasterEntity", "GratCatAbbr1", "grat_cat_abbr1", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("GratuityCatMasterEntity", "GratCatAbbr2", "grat_cat_abbr2", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("GratuityCatMasterEntity", "GratCatId", "grat_cat_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("GratuityCatMasterEntity", "GratCatName", "grat_cat_name", true, "NVarChar", 20, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("GratuityCatMasterEntity", "GratCatSvsChgFlag", "grat_cat_svs_chg_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 5);
			this.AddElementFieldMapping("GratuityCatMasterEntity", "LoyaltyEarnEligibleFlag", "loyalty_earn_eligible_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 6);
			this.AddElementFieldMapping("GratuityCatMasterEntity", "LoyaltyRedeemEligibleFlag", "loyalty_redeem_eligible_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 7);
			this.AddElementFieldMapping("GratuityCatMasterEntity", "RowVersion", "row_version", false, "Timestamp", 2147483647, 0, 0, false, "", null, typeof(System.Byte[]), 8);
			this.AddElementFieldMapping("GratuityCatMasterEntity", "StoreId", "store_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
		}

		/// <summary>Inits GratuityMasterEntity's mappings</summary>
		private void InitGratuityMasterEntityMappings()
		{
			this.AddElementMapping("GratuityMasterEntity", @"it_cfg", @"dbo", "Gratuity_Master", 23);
			this.AddElementFieldMapping("GratuityMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("GratuityMasterEntity", "GratAbbr1", "grat_abbr1", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("GratuityMasterEntity", "GratAbbr2", "grat_abbr2", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("GratuityMasterEntity", "GratAmt", "grat_amt", true, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 3);
			this.AddElementFieldMapping("GratuityMasterEntity", "GratCatId", "grat_cat_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("GratuityMasterEntity", "GratId", "grat_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("GratuityMasterEntity", "GratName", "grat_name", true, "NVarChar", 20, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("GratuityMasterEntity", "GratPercent", "grat_percent", true, "Float", 0, 38, 0, false, "", null, typeof(System.Double), 7);
			this.AddElementFieldMapping("GratuityMasterEntity", "GratPerCoverAmt", "grat_per_cover_amt", true, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 8);
			this.AddElementFieldMapping("GratuityMasterEntity", "GratRecipientCodeId", "grat_recipient_code_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("GratuityMasterEntity", "GratRecipientEmpId", "grat_recipient_emp_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("GratuityMasterEntity", "GratRemovableFlag", "grat_removable_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 11);
			this.AddElementFieldMapping("GratuityMasterEntity", "GratTermPayFlag", "grat_term_pay_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 12);
			this.AddElementFieldMapping("GratuityMasterEntity", "GratuityIsInclusiveFlag", "gratuity_is_inclusive_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 13);
			this.AddElementFieldMapping("GratuityMasterEntity", "InclusiveGratuityConvertedFlag", "inclusive_gratuity_converted_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 14);
			this.AddElementFieldMapping("GratuityMasterEntity", "MaxCoverThreshhold", "max_cover_threshhold", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 15);
			this.AddElementFieldMapping("GratuityMasterEntity", "MinCoverThreshhold", "min_cover_threshhold", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 16);
			this.AddElementFieldMapping("GratuityMasterEntity", "PercentOrAmountFlag", "percent_or_amount_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 17);
			this.AddElementFieldMapping("GratuityMasterEntity", "RoundBasis", "round_basis", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 18);
			this.AddElementFieldMapping("GratuityMasterEntity", "RoundTypeId", "round_type_id", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 19);
			this.AddElementFieldMapping("GratuityMasterEntity", "RowVersion", "row_version", false, "Timestamp", 2147483647, 0, 0, false, "", null, typeof(System.Byte[]), 20);
			this.AddElementFieldMapping("GratuityMasterEntity", "SalesCalculationOnNetOrGrossFlag", "sales_calculation_on_net_or_gross_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 21);
			this.AddElementFieldMapping("GratuityMasterEntity", "StoreId", "store_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 22);
		}

		/// <summary>Inits IdMasterEntity's mappings</summary>
		private void InitIdMasterEntityMappings()
		{
			this.AddElementMapping("IdMasterEntity", @"it_cfg", @"dbo", "ID_Master", 6);
			this.AddElementFieldMapping("IdMasterEntity", "CorpId", "corp_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("IdMasterEntity", "DivId", "div_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("IdMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("IdMasterEntity", "LastUsedId", "last_used_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("IdMasterEntity", "StoreId", "store_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("IdMasterEntity", "TableName", "table_name", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 5);
		}

		/// <summary>Inits ImageLibraryMasterEntity's mappings</summary>
		private void InitImageLibraryMasterEntityMappings()
		{
			this.AddElementMapping("ImageLibraryMasterEntity", @"it_cfg", @"dbo", "Image_Library_Master", 5);
			this.AddElementFieldMapping("ImageLibraryMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ImageLibraryMasterEntity", "Image", "image", false, "VarBinary", 2147483647, 0, 0, false, "", null, typeof(System.Byte[]), 1);
			this.AddElementFieldMapping("ImageLibraryMasterEntity", "ImageExtension", "image_extension", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("ImageLibraryMasterEntity", "ImageId", "image_id", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("ImageLibraryMasterEntity", "ImageName", "image_name", false, "NVarChar", 128, 0, 0, false, "", null, typeof(System.String), 4);
		}

		/// <summary>Inits IntlDescriptorTextJoinEntity's mappings</summary>
		private void InitIntlDescriptorTextJoinEntityMappings()
		{
			this.AddElementMapping("IntlDescriptorTextJoinEntity", @"it_cfg", @"dbo", "Intl_Descriptor_Text_Join", 8);
			this.AddElementFieldMapping("IntlDescriptorTextJoinEntity", "ControlId", "control_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("IntlDescriptorTextJoinEntity", "ControlText", "control_text", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("IntlDescriptorTextJoinEntity", "CustomerId", "customer_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("IntlDescriptorTextJoinEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("IntlDescriptorTextJoinEntity", "LanguageId", "language_id", false, "NVarChar", 9, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("IntlDescriptorTextJoinEntity", "NodeId", "node_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("IntlDescriptorTextJoinEntity", "ObjectId", "object_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("IntlDescriptorTextJoinEntity", "TranslatedFlag", "translated_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 7);
		}

		/// <summary>Inits IpServerAttribMasterEntity's mappings</summary>
		private void InitIpServerAttribMasterEntityMappings()
		{
			this.AddElementMapping("IpServerAttribMasterEntity", @"it_cfg", @"dbo", "IP_Server_Attrib_Master", 8);
			this.AddElementFieldMapping("IpServerAttribMasterEntity", "CurrentVersion", "current_version", true, "NVarChar", 30, 0, 0, false, "", null, typeof(System.String), 0);
			this.AddElementFieldMapping("IpServerAttribMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("IpServerAttribMasterEntity", "IpAddress", "IP_address", true, "NVarChar", 15, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("IpServerAttribMasterEntity", "IpTypeCodeId", "IP_type_code_id", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 3);
			this.AddElementFieldMapping("IpServerAttribMasterEntity", "ServerAttribDesc", "server_attrib_desc", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("IpServerAttribMasterEntity", "ServerAttribId", "server_attrib_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("IpServerAttribMasterEntity", "ServerHostName", "server_host_name", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("IpServerAttribMasterEntity", "StoreId", "store_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
		}

		/// <summary>Inits JobcodeFuncBttnJoinEntity's mappings</summary>
		private void InitJobcodeFuncBttnJoinEntityMappings()
		{
			this.AddElementMapping("JobcodeFuncBttnJoinEntity", @"it_cfg", @"dbo", "Jobcode_FuncBttn_Join", 4);
			this.AddElementFieldMapping("JobcodeFuncBttnJoinEntity", "EnabledFlag", "enabled_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 0);
			this.AddElementFieldMapping("JobcodeFuncBttnJoinEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("JobcodeFuncBttnJoinEntity", "FuncbttnId", "funcbttn_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("JobcodeFuncBttnJoinEntity", "JobcodeId", "jobcode_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
		}

		/// <summary>Inits JobcodeFunctJoinEntity's mappings</summary>
		private void InitJobcodeFunctJoinEntityMappings()
		{
			this.AddElementMapping("JobcodeFunctJoinEntity", @"it_cfg", @"dbo", "Jobcode_Funct_Join", 4);
			this.AddElementFieldMapping("JobcodeFunctJoinEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("JobcodeFunctJoinEntity", "FunctFlag", "funct_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 1);
			this.AddElementFieldMapping("JobcodeFunctJoinEntity", "FunctId", "funct_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("JobcodeFunctJoinEntity", "JobcodeId", "jobcode_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
		}

		/// <summary>Inits JobcodeMasterEntity's mappings</summary>
		private void InitJobcodeMasterEntityMappings()
		{
			this.AddElementMapping("JobcodeMasterEntity", @"it_cfg", @"dbo", "Jobcode_Master", 15);
			this.AddElementFieldMapping("JobcodeMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("JobcodeMasterEntity", "JobcodeAbbr", "jobcode_abbr", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("JobcodeMasterEntity", "JobcodeAbbr2", "jobcode_abbr2", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("JobcodeMasterEntity", "JobcodeCarryoverTipsFlag", "jobcode_carryover_tips_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 3);
			this.AddElementFieldMapping("JobcodeMasterEntity", "JobcodeId", "jobcode_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("JobcodeMasterEntity", "JobcodeLaborRate", "jobcode_labor_rate", true, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 5);
			this.AddElementFieldMapping("JobcodeMasterEntity", "JobcodeName", "jobcode_name", true, "NVarChar", 16, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("JobcodeMasterEntity", "JobcodePrintCashFlag", "jobcode_print_cash_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 7);
			this.AddElementFieldMapping("JobcodeMasterEntity", "JobcodePrintCashierFlag", "jobcode_print_cashier_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 8);
			this.AddElementFieldMapping("JobcodeMasterEntity", "JobcodePrintEmpIdFlag", "jobcode_print_emp_id_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 9);
			this.AddElementFieldMapping("JobcodeMasterEntity", "JobcodePrintServerFlag", "jobcode_print_server_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 10);
			this.AddElementFieldMapping("JobcodeMasterEntity", "JobcodeSecId", "jobcode_sec_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("JobcodeMasterEntity", "JobcodeTipPrompt", "jobcode_tip_prompt", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 12);
			this.AddElementFieldMapping("JobcodeMasterEntity", "RowVersion", "row_version", false, "Timestamp", 2147483647, 0, 0, false, "", null, typeof(System.Byte[]), 13);
			this.AddElementFieldMapping("JobcodeMasterEntity", "StoreId", "store_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 14);
		}

		/// <summary>Inits KdsCategoryMasterEntity's mappings</summary>
		private void InitKdsCategoryMasterEntityMappings()
		{
			this.AddElementMapping("KdsCategoryMasterEntity", @"it_cfg", @"dbo", "KDS_Category_Master", 4);
			this.AddElementFieldMapping("KdsCategoryMasterEntity", "DivisionId", "division_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("KdsCategoryMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("KdsCategoryMasterEntity", "KdsCategoryId", "kds_category_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("KdsCategoryMasterEntity", "KdsCategoryName", "kds_category_name", false, "NVarChar", 40, 0, 0, false, "", null, typeof(System.String), 3);
		}

		/// <summary>Inits KdsVideoMasterEntity's mappings</summary>
		private void InitKdsVideoMasterEntityMappings()
		{
			this.AddElementMapping("KdsVideoMasterEntity", @"it_cfg", @"dbo", "KDS_Video_Master", 4);
			this.AddElementFieldMapping("KdsVideoMasterEntity", "DivisionId", "division_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("KdsVideoMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("KdsVideoMasterEntity", "KdsVideoId", "kds_video_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("KdsVideoMasterEntity", "KdsVideoName", "kds_video_name", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 3);
		}

		/// <summary>Inits KpOptionGrpCopyJoinEntity's mappings</summary>
		private void InitKpOptionGrpCopyJoinEntityMappings()
		{
			this.AddElementMapping("KpOptionGrpCopyJoinEntity", @"it_cfg", @"dbo", "KP_Option_Grp_Copy_Join", 10);
			this.AddElementFieldMapping("KpOptionGrpCopyJoinEntity", "BeepFlag", "beep_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 0);
			this.AddElementFieldMapping("KpOptionGrpCopyJoinEntity", "CutCopyFlag", "cut_copy_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 1);
			this.AddElementFieldMapping("KpOptionGrpCopyJoinEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("KpOptionGrpCopyJoinEntity", "ExtraCodeId", "extra_code_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("KpOptionGrpCopyJoinEntity", "KpCopiesSeq", "KP_copies_seq", false, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 4);
			this.AddElementFieldMapping("KpOptionGrpCopyJoinEntity", "KpOptionGrpId", "KP_option_grp_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("KpOptionGrpCopyJoinEntity", "NumLinesAfter", "num_lines_after", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 6);
			this.AddElementFieldMapping("KpOptionGrpCopyJoinEntity", "NumLinesBefore", "num_lines_before", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 7);
			this.AddElementFieldMapping("KpOptionGrpCopyJoinEntity", "PrintHeaderFlag", "print_header_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 8);
			this.AddElementFieldMapping("KpOptionGrpCopyJoinEntity", "SortCodeId", "sort_code_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
		}

		/// <summary>Inits KpOptionGrpFormatJoinEntity's mappings</summary>
		private void InitKpOptionGrpFormatJoinEntityMappings()
		{
			this.AddElementMapping("KpOptionGrpFormatJoinEntity", @"it_cfg", @"dbo", "KP_Option_Grp_Format_Join", 8);
			this.AddElementFieldMapping("KpOptionGrpFormatJoinEntity", "AddLineFlag", "add_line_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 0);
			this.AddElementFieldMapping("KpOptionGrpFormatJoinEntity", "CharsizeCodeId", "charsize_code_id", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 1);
			this.AddElementFieldMapping("KpOptionGrpFormatJoinEntity", "ColorCodeId", "color_code_id", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 2);
			this.AddElementFieldMapping("KpOptionGrpFormatJoinEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("KpOptionGrpFormatJoinEntity", "FormatLabel", "format_label", true, "NVarChar", 30, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("KpOptionGrpFormatJoinEntity", "KpFormatId", "KP_format_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("KpOptionGrpFormatJoinEntity", "KpOptionGrpId", "KP_option_grp_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("KpOptionGrpFormatJoinEntity", "PrintFlag", "print_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 7);
		}

		/// <summary>Inits KpOptionGrpMasterEntity's mappings</summary>
		private void InitKpOptionGrpMasterEntityMappings()
		{
			this.AddElementMapping("KpOptionGrpMasterEntity", @"it_cfg", @"dbo", "KP_Option_Grp_Master", 6);
			this.AddElementFieldMapping("KpOptionGrpMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("KpOptionGrpMasterEntity", "KpOptionGrpAbbr1", "KP_option_grp_abbr1", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("KpOptionGrpMasterEntity", "KpOptionGrpAbbr2", "KP_option_grp_abbr2", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("KpOptionGrpMasterEntity", "KpOptionGrpId", "KP_option_grp_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("KpOptionGrpMasterEntity", "KpOptionGrpName", "KP_option_grp_name", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("KpOptionGrpMasterEntity", "StoreId", "store_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
		}

		/// <summary>Inits KpPrinterMasterEntity's mappings</summary>
		private void InitKpPrinterMasterEntityMappings()
		{
			this.AddElementMapping("KpPrinterMasterEntity", @"it_cfg", @"dbo", "KP_Printer_Master", 4);
			this.AddElementFieldMapping("KpPrinterMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("KpPrinterMasterEntity", "KpPrinterId", "KP_printer_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("KpPrinterMasterEntity", "KpPrinterName", "KP_printer_name", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("KpPrinterMasterEntity", "StoreId", "store_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
		}

		/// <summary>Inits KpRouteMasterEntity's mappings</summary>
		private void InitKpRouteMasterEntityMappings()
		{
			this.AddElementMapping("KpRouteMasterEntity", @"it_cfg", @"dbo", "KP_Route_Master", 4);
			this.AddElementFieldMapping("KpRouteMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("KpRouteMasterEntity", "KpRouteId", "kp_route_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("KpRouteMasterEntity", "KpRouteName", "kp_route_name", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("KpRouteMasterEntity", "StoreId", "store_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
		}

		/// <summary>Inits KpRoutePrinterJoinEntity's mappings</summary>
		private void InitKpRoutePrinterJoinEntityMappings()
		{
			this.AddElementMapping("KpRoutePrinterJoinEntity", @"it_cfg", @"dbo", "KP_Route_Printer_Join", 4);
			this.AddElementFieldMapping("KpRoutePrinterJoinEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("KpRoutePrinterJoinEntity", "KpDeviceId", "kp_device_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("KpRoutePrinterJoinEntity", "KpPrinterId", "kp_printer_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("KpRoutePrinterJoinEntity", "KpRouteId", "kp_route_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
		}

		/// <summary>Inits MachineOptionsMasterEntity's mappings</summary>
		private void InitMachineOptionsMasterEntityMappings()
		{
			this.AddElementMapping("MachineOptionsMasterEntity", @"it_cfg", @"dbo", "Machine_Options_Master", 2);
			this.AddElementFieldMapping("MachineOptionsMasterEntity", "OptionName", "option_name", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 0);
			this.AddElementFieldMapping("MachineOptionsMasterEntity", "OptionValue", "option_value", true, "NVarChar", 10, 0, 0, false, "", null, typeof(System.String), 1);
		}

		/// <summary>Inits MealPeriodMasterEntity's mappings</summary>
		private void InitMealPeriodMasterEntityMappings()
		{
			this.AddElementMapping("MealPeriodMasterEntity", @"it_cfg", @"dbo", "Meal_Period_Master", 12);
			this.AddElementFieldMapping("MealPeriodMasterEntity", "DefaultCheckTypeId", "default_check_type_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("MealPeriodMasterEntity", "DefaultPriceLevelId", "default_price_level_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("MealPeriodMasterEntity", "EntertainmentFlag", "entertainment_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 2);
			this.AddElementFieldMapping("MealPeriodMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("MealPeriodMasterEntity", "MealPeriodAbbr1", "meal_period_abbr1", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("MealPeriodMasterEntity", "MealPeriodAbbr2", "meal_period_abbr2", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("MealPeriodMasterEntity", "MealPeriodId", "meal_period_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("MealPeriodMasterEntity", "MealPeriodName", "meal_period_name", true, "NVarChar", 16, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("MealPeriodMasterEntity", "MealPeriodSecId", "meal_period_sec_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("MealPeriodMasterEntity", "ReceiptCode", "receipt_code", true, "NVarChar", 3, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("MealPeriodMasterEntity", "RowVersion", "row_version", false, "Timestamp", 2147483647, 0, 0, false, "", null, typeof(System.Byte[]), 10);
			this.AddElementFieldMapping("MealPeriodMasterEntity", "StoreId", "store_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
		}

		/// <summary>Inits MembershipMasterEntity's mappings</summary>
		private void InitMembershipMasterEntityMappings()
		{
			this.AddElementMapping("MembershipMasterEntity", @"it_cfg", @"dbo", "Membership_Master", 4);
			this.AddElementFieldMapping("MembershipMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("MembershipMasterEntity", "MembershipCode", "membership_code", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("MembershipMasterEntity", "MembershipId", "membership_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("MembershipMasterEntity", "MembershipName", "membership_name", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 3);
		}

		/// <summary>Inits MembershipProfitCenterJoinEntity's mappings</summary>
		private void InitMembershipProfitCenterJoinEntityMappings()
		{
			this.AddElementMapping("MembershipProfitCenterJoinEntity", @"it_cfg", @"dbo", "Membership_ProfitCenter_Join", 4);
			this.AddElementFieldMapping("MembershipProfitCenterJoinEntity", "CheckTypeId", "check_type_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("MembershipProfitCenterJoinEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("MembershipProfitCenterJoinEntity", "MembershipId", "membership_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("MembershipProfitCenterJoinEntity", "ProfitCenterId", "profit_center_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
		}

		/// <summary>Inits MenuBttnObjJoinEntity's mappings</summary>
		private void InitMenuBttnObjJoinEntityMappings()
		{
			this.AddElementMapping("MenuBttnObjJoinEntity", @"it_cfg", @"dbo", "Menu_Bttn_Obj_Join", 21);
			this.AddElementFieldMapping("MenuBttnObjJoinEntity", "ButtonBackcolor", "button_backcolor", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("MenuBttnObjJoinEntity", "ButtonBorderStyle", "button_border_style", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("MenuBttnObjJoinEntity", "ButtonColumn", "button_column", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("MenuBttnObjJoinEntity", "ButtonFontId", "button_font_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("MenuBttnObjJoinEntity", "ButtonForecolor", "button_forecolor", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("MenuBttnObjJoinEntity", "ButtonHeight", "button_height", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 5);
			this.AddElementFieldMapping("MenuBttnObjJoinEntity", "ButtonId", "button_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("MenuBttnObjJoinEntity", "ButtonImageId", "button_image_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("MenuBttnObjJoinEntity", "ButtonNextMenuId", "button_next_menu_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("MenuBttnObjJoinEntity", "ButtonRow", "button_row", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("MenuBttnObjJoinEntity", "ButtonShapeId", "button_shape_id", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 10);
			this.AddElementFieldMapping("MenuBttnObjJoinEntity", "ButtonWidth", "button_width", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 11);
			this.AddElementFieldMapping("MenuBttnObjJoinEntity", "DisplayImageFlag", "display_image_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 12);
			this.AddElementFieldMapping("MenuBttnObjJoinEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 13);
			this.AddElementFieldMapping("MenuBttnObjJoinEntity", "ErrorCodeId", "error_code_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 14);
			this.AddElementFieldMapping("MenuBttnObjJoinEntity", "MenuId", "menu_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 15);
			this.AddElementFieldMapping("MenuBttnObjJoinEntity", "ObjectId", "object_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 16);
			this.AddElementFieldMapping("MenuBttnObjJoinEntity", "ObjectTypeId", "object_type_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 17);
			this.AddElementFieldMapping("MenuBttnObjJoinEntity", "ScreenId", "screen_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 18);
			this.AddElementFieldMapping("MenuBttnObjJoinEntity", "StoreCreatedId", "store_created_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 19);
			this.AddElementFieldMapping("MenuBttnObjJoinEntity", "TerminalTypeId", "terminal_type_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 20);
		}

		/// <summary>Inits MenuItemGroupMasterEntity's mappings</summary>
		private void InitMenuItemGroupMasterEntityMappings()
		{
			this.AddElementMapping("MenuItemGroupMasterEntity", @"it_cfg", @"dbo", "Menu_Item_Group_Master", 4);
			this.AddElementFieldMapping("MenuItemGroupMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("MenuItemGroupMasterEntity", "MenuItemGroupId", "menu_item_group_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("MenuItemGroupMasterEntity", "MenuItemGroupName", "menu_item_group_name", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("MenuItemGroupMasterEntity", "StoreId", "store_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
		}

		/// <summary>Inits MenuItemMasterEntity's mappings</summary>
		private void InitMenuItemMasterEntityMappings()
		{
			this.AddElementMapping("MenuItemMasterEntity", @"it_cfg", @"dbo", "Menu_Item_Master", 37);
			this.AddElementFieldMapping("MenuItemMasterEntity", "BargunId", "bargun_id", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 0);
			this.AddElementFieldMapping("MenuItemMasterEntity", "Covers", "covers", false, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 1);
			this.AddElementFieldMapping("MenuItemMasterEntity", "DataControlGroupId", "data_control_group_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("MenuItemMasterEntity", "DefaultImageId", "default_image_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("MenuItemMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("MenuItemMasterEntity", "KdsCategoryId", "kds_category_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("MenuItemMasterEntity", "KdsCookTime", "kds_cook_time", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("MenuItemMasterEntity", "KdsVideoId", "kds_video_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("MenuItemMasterEntity", "KdsVideoLabel", "kds_video_label", true, "NVarChar", 20, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("MenuItemMasterEntity", "MenuItemAbbr1", "menu_item_abbr1", true, "NVarChar", 20, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("MenuItemMasterEntity", "MenuItemAbbr2", "menu_item_abbr2", true, "NVarChar", 20, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("MenuItemMasterEntity", "MenuItemGroupId", "menu_item_group_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("MenuItemMasterEntity", "MenuItemId", "menu_item_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("MenuItemMasterEntity", "MenuItemName", "menu_item_name", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 13);
			this.AddElementFieldMapping("MenuItemMasterEntity", "MiCostAmt", "mi_cost_amt", true, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 14);
			this.AddElementFieldMapping("MenuItemMasterEntity", "MiDiscountableFlag", "mi_discountable_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 15);
			this.AddElementFieldMapping("MenuItemMasterEntity", "MiEmpDiscountableFlag", "mi_emp_discountable_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 16);
			this.AddElementFieldMapping("MenuItemMasterEntity", "MiKpLabel", "mi_KP_label", true, "NVarChar", 16, 0, 0, false, "", null, typeof(System.String), 17);
			this.AddElementFieldMapping("MenuItemMasterEntity", "MiNotActiveFlag", "mi_not_active_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 18);
			this.AddElementFieldMapping("MenuItemMasterEntity", "MiOpenPricePrompt", "mi_open_price_prompt", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 19);
			this.AddElementFieldMapping("MenuItemMasterEntity", "MiPriceOverrideFlag", "mi_price_override_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 20);
			this.AddElementFieldMapping("MenuItemMasterEntity", "MiPrintFlag", "mi_print_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 21);
			this.AddElementFieldMapping("MenuItemMasterEntity", "MiReceiptLabel", "mi_receipt_label", true, "NVarChar", 16, 0, 0, false, "", null, typeof(System.String), 22);
			this.AddElementFieldMapping("MenuItemMasterEntity", "MiSecId", "mi_sec_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 23);
			this.AddElementFieldMapping("MenuItemMasterEntity", "MiTaxInclFlag", "mi_tax_incl_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 24);
			this.AddElementFieldMapping("MenuItemMasterEntity", "MiVoidableFlag", "mi_voidable_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 25);
			this.AddElementFieldMapping("MenuItemMasterEntity", "MiWeightFlag", "mi_weight_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 26);
			this.AddElementFieldMapping("MenuItemMasterEntity", "MiWeightTare", "mi_weight_tare", true, "Float", 0, 38, 0, false, "", null, typeof(System.Double), 27);
			this.AddElementFieldMapping("MenuItemMasterEntity", "OpenModifierFlag", "open_modifier_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 28);
			this.AddElementFieldMapping("MenuItemMasterEntity", "ProdClassId", "prod_class_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 29);
			this.AddElementFieldMapping("MenuItemMasterEntity", "RevCatId", "rev_cat_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 30);
			this.AddElementFieldMapping("MenuItemMasterEntity", "RowVersion", "row_version", false, "Timestamp", 2147483647, 0, 0, false, "", null, typeof(System.Byte[]), 31);
			this.AddElementFieldMapping("MenuItemMasterEntity", "RptCatId", "rpt_cat_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 32);
			this.AddElementFieldMapping("MenuItemMasterEntity", "SkuNo", "sku_no", true, "NVarChar", 30, 0, 0, false, "", null, typeof(System.String), 33);
			this.AddElementFieldMapping("MenuItemMasterEntity", "StoreCreatedId", "store_created_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 34);
			this.AddElementFieldMapping("MenuItemMasterEntity", "StoreId", "store_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 35);
			this.AddElementFieldMapping("MenuItemMasterEntity", "TaxGrpId", "tax_grp_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 36);
		}

		/// <summary>Inits MenuMasterEntity's mappings</summary>
		private void InitMenuMasterEntityMappings()
		{
			this.AddElementMapping("MenuMasterEntity", @"it_cfg", @"dbo", "Menu_Master", 15);
			this.AddElementFieldMapping("MenuMasterEntity", "AllowButtonOverlapFlag", "allow_button_overlap_flag", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 0);
			this.AddElementFieldMapping("MenuMasterEntity", "ButtonPadding", "button_padding", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("MenuMasterEntity", "DataControlGroupId", "data_control_group_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("MenuMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("MenuMasterEntity", "GridSize", "grid_size", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("MenuMasterEntity", "MenuAbbr1", "menu_abbr1", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("MenuMasterEntity", "MenuAbbr2", "menu_abbr2", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("MenuMasterEntity", "MenuId", "menu_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("MenuMasterEntity", "MenuName", "menu_name", true, "NVarChar", 20, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("MenuMasterEntity", "ScreenSizeId", "screen_size_id", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 9);
			this.AddElementFieldMapping("MenuMasterEntity", "ScreenTypeId", "screen_type_id", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 10);
			this.AddElementFieldMapping("MenuMasterEntity", "SnapToGridFlag", "snap_to_grid_flag", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 11);
			this.AddElementFieldMapping("MenuMasterEntity", "StoreId", "store_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("MenuMasterEntity", "SystemScreenFlag", "system_screen_flag", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 13);
			this.AddElementFieldMapping("MenuMasterEntity", "ThemeId", "theme_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 14);
		}

		/// <summary>Inits MenuMigrpJoinEntity's mappings</summary>
		private void InitMenuMigrpJoinEntityMappings()
		{
			this.AddElementMapping("MenuMigrpJoinEntity", @"it_cfg", @"dbo", "Menu_MIGrp_Join", 3);
			this.AddElementFieldMapping("MenuMigrpJoinEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("MenuMigrpJoinEntity", "MenuId", "menu_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("MenuMigrpJoinEntity", "MenuItemGroupId", "menu_item_group_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
		}

		/// <summary>Inits MiChoiceGrpJoinEntity's mappings</summary>
		private void InitMiChoiceGrpJoinEntityMappings()
		{
			this.AddElementMapping("MiChoiceGrpJoinEntity", @"it_cfg", @"dbo", "MI_ChoiceGrp_Join", 5);
			this.AddElementFieldMapping("MiChoiceGrpJoinEntity", "ChoiceGroupId", "choice_group_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("MiChoiceGrpJoinEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("MiChoiceGrpJoinEntity", "MenuItemId", "menu_item_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("MiChoiceGrpJoinEntity", "MiCgJoinId", "mi_cg_join_id", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("MiChoiceGrpJoinEntity", "MiChoicegrpSeq", "mi_choicegrp_seq", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 4);
		}

		/// <summary>Inits MiKpprinterJoinEntity's mappings</summary>
		private void InitMiKpprinterJoinEntityMappings()
		{
			this.AddElementMapping("MiKpprinterJoinEntity", @"it_cfg", @"dbo", "MI_KPPrinter_Join", 4);
			this.AddElementFieldMapping("MiKpprinterJoinEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("MiKpprinterJoinEntity", "KpPrinterId", "KP_printer_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("MiKpprinterJoinEntity", "MenuItemId", "menu_item_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("MiKpprinterJoinEntity", "PrimaryPrinterFlag", "primary_printer_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 3);
		}

		/// <summary>Inits MiPriceJoinEntity's mappings</summary>
		private void InitMiPriceJoinEntityMappings()
		{
			this.AddElementMapping("MiPriceJoinEntity", @"it_cfg", @"dbo", "MI_Price_Join", 6);
			this.AddElementFieldMapping("MiPriceJoinEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("MiPriceJoinEntity", "MenuItemId", "menu_item_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("MiPriceJoinEntity", "PriceLast", "price_last", true, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 2);
			this.AddElementFieldMapping("MiPriceJoinEntity", "PriceLevelId", "price_level_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("MiPriceJoinEntity", "PriceNet", "price_net", true, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 4);
			this.AddElementFieldMapping("MiPriceJoinEntity", "StoreCreatedId", "store_created_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
		}

		/// <summary>Inits MiSkuJoinEntity's mappings</summary>
		private void InitMiSkuJoinEntityMappings()
		{
			this.AddElementMapping("MiSkuJoinEntity", @"it_cfg", @"dbo", "MI_SKU_Join", 5);
			this.AddElementFieldMapping("MiSkuJoinEntity", "Description", "description", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 0);
			this.AddElementFieldMapping("MiSkuJoinEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("MiSkuJoinEntity", "MenuItemId", "menu_item_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("MiSkuJoinEntity", "SkuGenId", "sku_gen_id", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("MiSkuJoinEntity", "SkuNo", "sku_no", false, "NVarChar", 30, 0, 0, false, "", null, typeof(System.String), 4);
		}

		/// <summary>Inits ModifierKpprinterJoinEntity's mappings</summary>
		private void InitModifierKpprinterJoinEntityMappings()
		{
			this.AddElementMapping("ModifierKpprinterJoinEntity", @"it_cfg", @"dbo", "Modifier_KPPrinter_Join", 4);
			this.AddElementFieldMapping("ModifierKpprinterJoinEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ModifierKpprinterJoinEntity", "KpPrinterId", "KP_printer_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ModifierKpprinterJoinEntity", "ModifierId", "modifier_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ModifierKpprinterJoinEntity", "PrimaryPrinterFlag", "primary_printer_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 3);
		}

		/// <summary>Inits ModifierMasterEntity's mappings</summary>
		private void InitModifierMasterEntityMappings()
		{
			this.AddElementMapping("ModifierMasterEntity", @"it_cfg", @"dbo", "Modifier_Master", 17);
			this.AddElementFieldMapping("ModifierMasterEntity", "DataControlGroupId", "data_control_group_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ModifierMasterEntity", "DiscoupId", "discoup_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ModifierMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ModifierMasterEntity", "KdsVideoLabel", "kds_video_label", true, "NVarChar", 20, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("ModifierMasterEntity", "ModifierAbbr1", "modifier_abbr1", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("ModifierMasterEntity", "ModifierAbbr2", "modifier_abbr2", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("ModifierMasterEntity", "ModifierExtraChoicegrpId", "modifier_extra_choicegrp_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("ModifierMasterEntity", "ModifierId", "modifier_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("ModifierMasterEntity", "ModifierKpLabel", "modifier_KP_label", true, "NVarChar", 16, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("ModifierMasterEntity", "ModifierMenuItemId", "modifier_menu_item_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("ModifierMasterEntity", "ModifierName", "modifier_name", true, "NVarChar", 16, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("ModifierMasterEntity", "ModifierPrintFlag", "modifier_print_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 11);
			this.AddElementFieldMapping("ModifierMasterEntity", "ModifierSecId", "modifier_sec_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("ModifierMasterEntity", "ModifierWithMiFlag", "modifier_with_mi_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 13);
			this.AddElementFieldMapping("ModifierMasterEntity", "RowVersion", "row_version", false, "Timestamp", 2147483647, 0, 0, false, "", null, typeof(System.Byte[]), 14);
			this.AddElementFieldMapping("ModifierMasterEntity", "StoreCreatedId", "store_created_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 15);
			this.AddElementFieldMapping("ModifierMasterEntity", "StoreId", "store_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 16);
		}

		/// <summary>Inits ModifierPriceJoinEntity's mappings</summary>
		private void InitModifierPriceJoinEntityMappings()
		{
			this.AddElementMapping("ModifierPriceJoinEntity", @"it_cfg", @"dbo", "Modifier_Price_Join", 5);
			this.AddElementFieldMapping("ModifierPriceJoinEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ModifierPriceJoinEntity", "ModifierId", "modifier_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ModifierPriceJoinEntity", "PriceLast", "price_last", true, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 2);
			this.AddElementFieldMapping("ModifierPriceJoinEntity", "PriceLevelId", "price_level_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("ModifierPriceJoinEntity", "PriceNet", "price_net", true, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 4);
		}

		/// <summary>Inits OptionsMasterEntity's mappings</summary>
		private void InitOptionsMasterEntityMappings()
		{
			this.AddElementMapping("OptionsMasterEntity", @"it_cfg", @"dbo", "Options_Master", 5);
			this.AddElementFieldMapping("OptionsMasterEntity", "DualFontName", "dual_font_name", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 0);
			this.AddElementFieldMapping("OptionsMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("OptionsMasterEntity", "ThemeLanguageId", "theme_language_id", true, "NVarChar", 2, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("OptionsMasterEntity", "UdUserFlag", "ud_user_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 3);
			this.AddElementFieldMapping("OptionsMasterEntity", "UnicodeFlag", "unicode_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 4);
		}

		/// <summary>Inits PriceLevelMasterEntity's mappings</summary>
		private void InitPriceLevelMasterEntityMappings()
		{
			this.AddElementMapping("PriceLevelMasterEntity", @"it_cfg", @"dbo", "Price_Level_Master", 5);
			this.AddElementFieldMapping("PriceLevelMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("PriceLevelMasterEntity", "PriceLevelId", "price_level_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("PriceLevelMasterEntity", "PriceLevelName", "price_level_name", true, "NVarChar", 16, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("PriceLevelMasterEntity", "RowVersion", "row_version", false, "Timestamp", 2147483647, 0, 0, false, "", null, typeof(System.Byte[]), 3);
			this.AddElementFieldMapping("PriceLevelMasterEntity", "StoreId", "store_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
		}

		/// <summary>Inits ProcessMasterEntity's mappings</summary>
		private void InitProcessMasterEntityMappings()
		{
			this.AddElementMapping("ProcessMasterEntity", @"it_cfg", @"dbo", "Process_Master", 7);
			this.AddElementFieldMapping("ProcessMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ProcessMasterEntity", "InstalledServerId", "installed_server_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ProcessMasterEntity", "ProcessDesc", "process_desc", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("ProcessMasterEntity", "ProcessId", "process_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("ProcessMasterEntity", "ProcessName", "process_name", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("ProcessMasterEntity", "ProcessTypeId", "process_type_id", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 5);
			this.AddElementFieldMapping("ProcessMasterEntity", "StoreId", "store_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
		}

		/// <summary>Inits ProductClassMasterEntity's mappings</summary>
		private void InitProductClassMasterEntityMappings()
		{
			this.AddElementMapping("ProductClassMasterEntity", @"it_cfg", @"dbo", "Product_Class_Master", 11);
			this.AddElementFieldMapping("ProductClassMasterEntity", "DefaultMenuItemGroupId", "default_menu_item_group_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ProductClassMasterEntity", "DefaultRevCatId", "default_rev_cat_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ProductClassMasterEntity", "DefaultRptCatId", "default_rpt_cat_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ProductClassMasterEntity", "DefaultSecId", "default_sec_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("ProductClassMasterEntity", "DefaultTaxGrpId", "default_tax_grp_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("ProductClassMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("ProductClassMasterEntity", "ItemRestrictedFlag", "item_restricted_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 6);
			this.AddElementFieldMapping("ProductClassMasterEntity", "ProdClassId", "prod_class_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("ProductClassMasterEntity", "ProdClassName", "prod_class_name", true, "NVarChar", 16, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("ProductClassMasterEntity", "RowVersion", "row_version", false, "Timestamp", 2147483647, 0, 0, false, "", null, typeof(System.Byte[]), 9);
			this.AddElementFieldMapping("ProductClassMasterEntity", "StoreId", "store_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
		}

		/// <summary>Inits ProfitCenterDayPartJoinEntity's mappings</summary>
		private void InitProfitCenterDayPartJoinEntityMappings()
		{
			this.AddElementMapping("ProfitCenterDayPartJoinEntity", @"it_cfg", @"dbo", "Profit_Center_Day_Part_Join", 3);
			this.AddElementFieldMapping("ProfitCenterDayPartJoinEntity", "DayPartName", "day_part_name", true, "NVarChar", 30, 0, 0, false, "", null, typeof(System.String), 0);
			this.AddElementFieldMapping("ProfitCenterDayPartJoinEntity", "DayPartStartTime", "day_part_start_time", false, "NVarChar", 5, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("ProfitCenterDayPartJoinEntity", "ProfitCenterId", "profit_center_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
		}

		/// <summary>Inits ProfitCenterGroupJoinEntity's mappings</summary>
		private void InitProfitCenterGroupJoinEntityMappings()
		{
			this.AddElementMapping("ProfitCenterGroupJoinEntity", @"it_cfg", @"dbo", "Profit_Center_Group_Join", 3);
			this.AddElementFieldMapping("ProfitCenterGroupJoinEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ProfitCenterGroupJoinEntity", "ProfitCenterId", "profit_center_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ProfitCenterGroupJoinEntity", "ProfitCtrGrpId", "profit_ctr_grp_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
		}

		/// <summary>Inits ProfitCenterGroupMasterEntity's mappings</summary>
		private void InitProfitCenterGroupMasterEntityMappings()
		{
			this.AddElementMapping("ProfitCenterGroupMasterEntity", @"it_cfg", @"dbo", "Profit_Center_Group_Master", 9);
			this.AddElementFieldMapping("ProfitCenterGroupMasterEntity", "ChargingPattern", "charging_pattern", true, "NVarChar", 2, 0, 0, false, "", null, typeof(System.String), 0);
			this.AddElementFieldMapping("ProfitCenterGroupMasterEntity", "DataControlGroupId", "data_control_group_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ProfitCenterGroupMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ProfitCenterGroupMasterEntity", "ProfitCtrAutoUpdateFlag", "profit_ctr_auto_update_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 3);
			this.AddElementFieldMapping("ProfitCenterGroupMasterEntity", "ProfitCtrGrpAbbr1", "profit_ctr_grp_abbr1", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("ProfitCenterGroupMasterEntity", "ProfitCtrGrpAbbr2", "profit_ctr_grp_abbr2", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("ProfitCenterGroupMasterEntity", "ProfitCtrGrpId", "profit_ctr_grp_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("ProfitCenterGroupMasterEntity", "ProfitCtrGrpName", "profit_ctr_grp_name", true, "NVarChar", 30, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("ProfitCenterGroupMasterEntity", "StoreId", "store_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
		}

		/// <summary>Inits ProfitCenterMasterEntity's mappings</summary>
		private void InitProfitCenterMasterEntityMappings()
		{
			this.AddElementMapping("ProfitCenterMasterEntity", @"it_cfg", @"dbo", "Profit_Center_Master", 32);
			this.AddElementFieldMapping("ProfitCenterMasterEntity", "BypassCcAgencyThresholdAmount", "bypass_CC_agency_threshold_amount", false, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 0);
			this.AddElementFieldMapping("ProfitCenterMasterEntity", "BypassCcPrintingThresholdAmount", "bypass_CC_printing_threshold_amount", false, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 1);
			this.AddElementFieldMapping("ProfitCenterMasterEntity", "BypassCcVoiceAuthThresholdAmount", "bypass_CC_voice_auth_threshold_amount", false, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 2);
			this.AddElementFieldMapping("ProfitCenterMasterEntity", "ChkFtrLine1", "chk_ftr_line1", true, "NVarChar", 30, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("ProfitCenterMasterEntity", "ChkFtrLine2", "chk_ftr_line2", true, "NVarChar", 30, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("ProfitCenterMasterEntity", "ChkFtrLine3", "chk_ftr_line3", true, "NVarChar", 30, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("ProfitCenterMasterEntity", "ChkHdrLine1", "chk_hdr_line1", true, "NVarChar", 30, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("ProfitCenterMasterEntity", "ChkHdrLine2", "chk_hdr_line2", true, "NVarChar", 30, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("ProfitCenterMasterEntity", "ChkHdrLine3", "chk_hdr_line3", true, "NVarChar", 30, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("ProfitCenterMasterEntity", "DataControlGroupId", "data_control_group_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("ProfitCenterMasterEntity", "DefaultTableLayoutId", "default_table_layout_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("ProfitCenterMasterEntity", "DocLinesAdvance", "doc_lines_advance", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 11);
			this.AddElementFieldMapping("ProfitCenterMasterEntity", "EntId", "ent_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("ProfitCenterMasterEntity", "MaxDocLinesPage", "max_doc_lines_page", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 13);
			this.AddElementFieldMapping("ProfitCenterMasterEntity", "MerchantId", "merchant_id", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 14);
			this.AddElementFieldMapping("ProfitCenterMasterEntity", "MinRcptLinesPage", "min_rcpt_lines_page", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 15);
			this.AddElementFieldMapping("ProfitCenterMasterEntity", "PoleDisplayClosed", "pole_display_closed", true, "NVarChar", 20, 0, 0, false, "", null, typeof(System.String), 16);
			this.AddElementFieldMapping("ProfitCenterMasterEntity", "PoleDisplayOpen", "pole_display_open", true, "NVarChar", 20, 0, 0, false, "", null, typeof(System.String), 17);
			this.AddElementFieldMapping("ProfitCenterMasterEntity", "PrimaryLanguageId", "primary_language_id", false, "NVarChar", 9, 0, 0, false, "", null, typeof(System.String), 18);
			this.AddElementFieldMapping("ProfitCenterMasterEntity", "PrintByRevCatFlag", "print_by_rev_cat_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 19);
			this.AddElementFieldMapping("ProfitCenterMasterEntity", "ProfitCenterDesc", "profit_center_desc", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 20);
			this.AddElementFieldMapping("ProfitCenterMasterEntity", "ProfitCenterId", "profit_center_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 21);
			this.AddElementFieldMapping("ProfitCenterMasterEntity", "ProfitCenterName", "profit_center_name", true, "NVarChar", 30, 0, 0, false, "", null, typeof(System.String), 22);
			this.AddElementFieldMapping("ProfitCenterMasterEntity", "ProfitCtrAbbr1", "profit_ctr_abbr1", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 23);
			this.AddElementFieldMapping("ProfitCenterMasterEntity", "ProfitCtrAbbr2", "profit_ctr_abbr2", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 24);
			this.AddElementFieldMapping("ProfitCenterMasterEntity", "RowVersion", "row_version", false, "Timestamp", 2147483647, 0, 0, false, "", null, typeof(System.Byte[]), 25);
			this.AddElementFieldMapping("ProfitCenterMasterEntity", "SalesTippableFlag", "sales_tippable_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 26);
			this.AddElementFieldMapping("ProfitCenterMasterEntity", "SecondaryLanguageId", "secondary_language_id", true, "NVarChar", 9, 0, 0, false, "", null, typeof(System.String), 27);
			this.AddElementFieldMapping("ProfitCenterMasterEntity", "SourcePropertyCode", "source_property_code", true, "NVarChar", 6, 0, 0, false, "", null, typeof(System.String), 28);
			this.AddElementFieldMapping("ProfitCenterMasterEntity", "StoreId", "store_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 29);
			this.AddElementFieldMapping("ProfitCenterMasterEntity", "TipEnforcementCodeId", "tip_enforcement_code_id", false, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 30);
			this.AddElementFieldMapping("ProfitCenterMasterEntity", "TipMaxPercent", "tip_max_percent", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 31);
		}

		/// <summary>Inits ProfitCenterReceiptPrinterJoinEntity's mappings</summary>
		private void InitProfitCenterReceiptPrinterJoinEntityMappings()
		{
			this.AddElementMapping("ProfitCenterReceiptPrinterJoinEntity", @"it_cfg", @"dbo", "Profit_Center_Receipt_Printer_Join", 4);
			this.AddElementFieldMapping("ProfitCenterReceiptPrinterJoinEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ProfitCenterReceiptPrinterJoinEntity", "PrimaryFlag", "primary_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 1);
			this.AddElementFieldMapping("ProfitCenterReceiptPrinterJoinEntity", "ProfitCenterId", "profit_center_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ProfitCenterReceiptPrinterJoinEntity", "ReceiptPrinterId", "receipt_printer_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
		}

		/// <summary>Inits ProfitCenterTableJoinEntity's mappings</summary>
		private void InitProfitCenterTableJoinEntityMappings()
		{
			this.AddElementMapping("ProfitCenterTableJoinEntity", @"it_cfg", @"dbo", "Profit_Center_Table_Join", 13);
			this.AddElementFieldMapping("ProfitCenterTableJoinEntity", "BoothFlag", "booth_flag", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 0);
			this.AddElementFieldMapping("ProfitCenterTableJoinEntity", "EntId", "ent_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ProfitCenterTableJoinEntity", "MaxSeats", "max_seats", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ProfitCenterTableJoinEntity", "OutsideFlag", "outside_flag", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 3);
			this.AddElementFieldMapping("ProfitCenterTableJoinEntity", "PrivateFlag", "private_flag", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 4);
			this.AddElementFieldMapping("ProfitCenterTableJoinEntity", "ProfitCenterId", "profit_center_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("ProfitCenterTableJoinEntity", "SecurityId", "security_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("ProfitCenterTableJoinEntity", "SmokingFlag", "smoking_flag", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 7);
			this.AddElementFieldMapping("ProfitCenterTableJoinEntity", "TableId", "table_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("ProfitCenterTableJoinEntity", "TableName", "table_name", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("ProfitCenterTableJoinEntity", "TableNo", "table_no", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("ProfitCenterTableJoinEntity", "ViewFlag", "view_flag", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 11);
			this.AddElementFieldMapping("ProfitCenterTableJoinEntity", "VipFlag", "vip_flag", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 12);
		}

		/// <summary>Inits QuickTenderMasterEntity's mappings</summary>
		private void InitQuickTenderMasterEntityMappings()
		{
			this.AddElementMapping("QuickTenderMasterEntity", @"it_cfg", @"dbo", "Quick_Tender_Master", 8);
			this.AddElementFieldMapping("QuickTenderMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("QuickTenderMasterEntity", "PaymentAmount", "payment_amount", true, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 1);
			this.AddElementFieldMapping("QuickTenderMasterEntity", "QuickTenderAbbr1", "quick_tender_abbr1", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("QuickTenderMasterEntity", "QuickTenderAbbr2", "quick_tender_abbr2", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("QuickTenderMasterEntity", "QuickTenderId", "quick_tender_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("QuickTenderMasterEntity", "QuickTenderName", "quick_tender_name", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("QuickTenderMasterEntity", "StoreId", "store_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("QuickTenderMasterEntity", "TenderId", "tender_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
		}

		/// <summary>Inits ReceiptPrinterMasterEntity's mappings</summary>
		private void InitReceiptPrinterMasterEntityMappings()
		{
			this.AddElementMapping("ReceiptPrinterMasterEntity", @"it_cfg", @"dbo", "Receipt_Printer_Master", 11);
			this.AddElementFieldMapping("ReceiptPrinterMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ReceiptPrinterMasterEntity", "IpAddress", "ip_address", false, "VarChar", 40, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("ReceiptPrinterMasterEntity", "IpPrintingPort", "ip_printing_port", false, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 2);
			this.AddElementFieldMapping("ReceiptPrinterMasterEntity", "IpStatusPort", "ip_status_port", false, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 3);
			this.AddElementFieldMapping("ReceiptPrinterMasterEntity", "ReceiptPrinterDescription", "receipt_printer_description", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("ReceiptPrinterMasterEntity", "ReceiptPrinterId", "receipt_printer_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("ReceiptPrinterMasterEntity", "ReceiptPrinterName", "receipt_printer_name", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("ReceiptPrinterMasterEntity", "RpInvertedFlag", "rp_inverted_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 7);
			this.AddElementFieldMapping("ReceiptPrinterMasterEntity", "RpLeftMarginOffset", "rp_left_margin_offset", false, "TinyInt", 0, 3, 0, false, "", null, typeof(System.Byte), 8);
			this.AddElementFieldMapping("ReceiptPrinterMasterEntity", "StoreId", "store_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("ReceiptPrinterMasterEntity", "UseDefaultPrinterFontFlag", "use_default_printer_font_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 10);
		}

		/// <summary>Inits ReportCatMasterEntity's mappings</summary>
		private void InitReportCatMasterEntityMappings()
		{
			this.AddElementMapping("ReportCatMasterEntity", @"it_cfg", @"dbo", "Report_Cat_Master", 8);
			this.AddElementFieldMapping("ReportCatMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ReportCatMasterEntity", "RowVersion", "row_version", false, "Timestamp", 2147483647, 0, 0, false, "", null, typeof(System.Byte[]), 1);
			this.AddElementFieldMapping("ReportCatMasterEntity", "RptCatAbbr1", "rpt_cat_abbr1", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("ReportCatMasterEntity", "RptCatAbbr2", "rpt_cat_abbr2", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("ReportCatMasterEntity", "RptCatId", "rpt_cat_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("ReportCatMasterEntity", "RptCatName", "rpt_cat_name", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("ReportCatMasterEntity", "RptSummaryId", "rpt_summary_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("ReportCatMasterEntity", "StoreId", "store_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
		}

		/// <summary>Inits RevenueCatMasterEntity's mappings</summary>
		private void InitRevenueCatMasterEntityMappings()
		{
			this.AddElementMapping("RevenueCatMasterEntity", @"it_cfg", @"dbo", "Revenue_Cat_Master", 13);
			this.AddElementFieldMapping("RevenueCatMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("RevenueCatMasterEntity", "LoyaltyEarnEligibleFlag", "loyalty_earn_eligible_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 1);
			this.AddElementFieldMapping("RevenueCatMasterEntity", "LoyaltyRedeemEligibleFlag", "loyalty_redeem_eligible_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 2);
			this.AddElementFieldMapping("RevenueCatMasterEntity", "RevCatAbbr1", "rev_cat_abbr1", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("RevenueCatMasterEntity", "RevCatAbbr2", "rev_cat_abbr2", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("RevenueCatMasterEntity", "RevCatId", "rev_cat_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("RevenueCatMasterEntity", "RevCatName", "rev_cat_name", true, "NVarChar", 16, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("RevenueCatMasterEntity", "RevClassId", "rev_class_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("RevenueCatMasterEntity", "RowVersion", "row_version", false, "Timestamp", 2147483647, 0, 0, false, "", null, typeof(System.Byte[]), 8);
			this.AddElementFieldMapping("RevenueCatMasterEntity", "RptCatId", "rpt_cat_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("RevenueCatMasterEntity", "SalesTippableFlag", "sales_tippable_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 10);
			this.AddElementFieldMapping("RevenueCatMasterEntity", "StoreId", "store_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("RevenueCatMasterEntity", "TenderRestrictionFlag", "tender_restriction_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 12);
		}

		/// <summary>Inits RevenueClassMasterEntity's mappings</summary>
		private void InitRevenueClassMasterEntityMappings()
		{
			this.AddElementMapping("RevenueClassMasterEntity", @"it_cfg", @"dbo", "Revenue_Class_Master", 6);
			this.AddElementFieldMapping("RevenueClassMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("RevenueClassMasterEntity", "RevClassAlphaId", "rev_class_alpha_id", true, "NVarChar", 1, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("RevenueClassMasterEntity", "RevClassId", "rev_class_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("RevenueClassMasterEntity", "RevClassName", "rev_class_name", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("RevenueClassMasterEntity", "RowVersion", "row_version", false, "Timestamp", 2147483647, 0, 0, false, "", null, typeof(System.Byte[]), 4);
			this.AddElementFieldMapping("RevenueClassMasterEntity", "StoreId", "store_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
		}

		/// <summary>Inits RowLockMasterEntity's mappings</summary>
		private void InitRowLockMasterEntityMappings()
		{
			this.AddElementMapping("RowLockMasterEntity", @"it_cfg", @"dbo", "Row_Lock_Master", 4);
			this.AddElementFieldMapping("RowLockMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("RowLockMasterEntity", "LockTimeStamp", "lock_time_stamp", false, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 1);
			this.AddElementFieldMapping("RowLockMasterEntity", "TableId", "table_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("RowLockMasterEntity", "TableName", "table_name", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 3);
		}

		/// <summary>Inits ScreenTemplateMasterEntity's mappings</summary>
		private void InitScreenTemplateMasterEntityMappings()
		{
			this.AddElementMapping("ScreenTemplateMasterEntity", @"it_cfg", @"dbo", "Screen_Template_Master", 15);
			this.AddElementFieldMapping("ScreenTemplateMasterEntity", "AllowButtonOverlapFlag", "allow_button_overlap_flag", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 0);
			this.AddElementFieldMapping("ScreenTemplateMasterEntity", "ButtonPadding", "button_padding", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ScreenTemplateMasterEntity", "DefaultBackcolor", "default_backcolor", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ScreenTemplateMasterEntity", "DefaultHeight", "default_height", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 3);
			this.AddElementFieldMapping("ScreenTemplateMasterEntity", "DefaultWidth", "default_width", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 4);
			this.AddElementFieldMapping("ScreenTemplateMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("ScreenTemplateMasterEntity", "GridSize", "grid_size", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("ScreenTemplateMasterEntity", "LockDownFlag", "lock_down_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 7);
			this.AddElementFieldMapping("ScreenTemplateMasterEntity", "ScreenTemplateId", "screen_template_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("ScreenTemplateMasterEntity", "ScreenTemplateName", "screen_template_name", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("ScreenTemplateMasterEntity", "ScreenTemplateText", "screen_template_text", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("ScreenTemplateMasterEntity", "SnapToGridFlag", "snap_to_grid_flag", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 11);
			this.AddElementFieldMapping("ScreenTemplateMasterEntity", "StoreFlag", "store_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 12);
			this.AddElementFieldMapping("ScreenTemplateMasterEntity", "StoreId", "store_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 13);
			this.AddElementFieldMapping("ScreenTemplateMasterEntity", "ThemeId", "theme_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 14);
		}

		/// <summary>Inits ScreenTemplateMigrpJoinEntity's mappings</summary>
		private void InitScreenTemplateMigrpJoinEntityMappings()
		{
			this.AddElementMapping("ScreenTemplateMigrpJoinEntity", @"it_cfg", @"dbo", "ScreenTemplate_MIGrp_Join", 3);
			this.AddElementFieldMapping("ScreenTemplateMigrpJoinEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ScreenTemplateMigrpJoinEntity", "MenuItemGroupId", "menu_item_group_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ScreenTemplateMigrpJoinEntity", "ScreenTemplateId", "screen_template_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
		}

		/// <summary>Inits SecurityLevelMasterEntity's mappings</summary>
		private void InitSecurityLevelMasterEntityMappings()
		{
			this.AddElementMapping("SecurityLevelMasterEntity", @"it_cfg", @"dbo", "Security_Level_Master", 3);
			this.AddElementFieldMapping("SecurityLevelMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("SecurityLevelMasterEntity", "SecurityId", "security_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("SecurityLevelMasterEntity", "SecurityLevelName", "security_level_name", true, "NVarChar", 10, 0, 0, false, "", null, typeof(System.String), 2);
		}

		/// <summary>Inits SelectionGroupItemJoinEntity's mappings</summary>
		private void InitSelectionGroupItemJoinEntityMappings()
		{
			this.AddElementMapping("SelectionGroupItemJoinEntity", @"it_cfg", @"dbo", "Selection_Group_Item_Join", 5);
			this.AddElementFieldMapping("SelectionGroupItemJoinEntity", "AdjustedAmt", "adjusted_amt", false, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 0);
			this.AddElementFieldMapping("SelectionGroupItemJoinEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("SelectionGroupItemJoinEntity", "MenuItemId", "menu_item_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("SelectionGroupItemJoinEntity", "SelectionGroupId", "selection_group_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("SelectionGroupItemJoinEntity", "SequenceId", "sequence_id", false, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 4);
		}

		/// <summary>Inits SelectionGroupMasterEntity's mappings</summary>
		private void InitSelectionGroupMasterEntityMappings()
		{
			this.AddElementMapping("SelectionGroupMasterEntity", @"it_cfg", @"dbo", "Selection_Group_Master", 9);
			this.AddElementFieldMapping("SelectionGroupMasterEntity", "DivisionId", "division_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("SelectionGroupMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("SelectionGroupMasterEntity", "RowVersion", "row_version", false, "Timestamp", 2147483647, 0, 0, false, "", null, typeof(System.Byte[]), 2);
			this.AddElementFieldMapping("SelectionGroupMasterEntity", "SelectionGroupId", "selection_group_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("SelectionGroupMasterEntity", "SelectionGroupKpLabel", "selection_group_KP_label", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("SelectionGroupMasterEntity", "SelectionGroupName", "selection_group_name", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("SelectionGroupMasterEntity", "SelectionGroupReceiptLabel", "selection_group_receipt_label", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("SelectionGroupMasterEntity", "TerminalPendingText", "terminal_pending_text", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("SelectionGroupMasterEntity", "TerminalPrompt", "terminal_prompt", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 8);
		}

		/// <summary>Inits ServiceManagerProxyMasterEntity's mappings</summary>
		private void InitServiceManagerProxyMasterEntityMappings()
		{
			this.AddElementMapping("ServiceManagerProxyMasterEntity", @"it_cfg", @"dbo", "Service_Manager_Proxy_Master", 30);
			this.AddElementFieldMapping("ServiceManagerProxyMasterEntity", "CampusCardDuration", "campus_card_duration", false, "TinyInt", 0, 3, 0, false, "", null, typeof(System.Byte), 0);
			this.AddElementFieldMapping("ServiceManagerProxyMasterEntity", "CampusCardUrl", "campus_card_url", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("ServiceManagerProxyMasterEntity", "CampusCardVersion", "campus_card_version", true, "NVarChar", 10, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("ServiceManagerProxyMasterEntity", "CompCardDuration", "comp_card_duration", false, "TinyInt", 0, 3, 0, false, "", null, typeof(System.Byte), 3);
			this.AddElementFieldMapping("ServiceManagerProxyMasterEntity", "CompCardUrl", "comp_card_url", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("ServiceManagerProxyMasterEntity", "CompCardVersion", "comp_card_version", true, "NVarChar", 10, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("ServiceManagerProxyMasterEntity", "CredentialModeType", "credential_mode_type", false, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 6);
			this.AddElementFieldMapping("ServiceManagerProxyMasterEntity", "CreditCardDuration", "credit_card_duration", false, "TinyInt", 0, 3, 0, false, "", null, typeof(System.Byte), 7);
			this.AddElementFieldMapping("ServiceManagerProxyMasterEntity", "CreditCardUrl", "credit_card_url", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("ServiceManagerProxyMasterEntity", "CreditCardVersion", "credit_card_version", true, "NVarChar", 10, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("ServiceManagerProxyMasterEntity", "Description", "description", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("ServiceManagerProxyMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("ServiceManagerProxyMasterEntity", "GiftCardDuration", "gift_card_duration", false, "TinyInt", 0, 3, 0, false, "", null, typeof(System.Byte), 12);
			this.AddElementFieldMapping("ServiceManagerProxyMasterEntity", "GiftCardUrl", "gift_card_url", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 13);
			this.AddElementFieldMapping("ServiceManagerProxyMasterEntity", "GiftCardVersion", "gift_card_version", true, "NVarChar", 10, 0, 0, false, "", null, typeof(System.String), 14);
			this.AddElementFieldMapping("ServiceManagerProxyMasterEntity", "GuestInfoDuration", "guest_info_duration", false, "TinyInt", 0, 3, 0, false, "", null, typeof(System.Byte), 15);
			this.AddElementFieldMapping("ServiceManagerProxyMasterEntity", "GuestInfoUrl", "guest_info_url", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 16);
			this.AddElementFieldMapping("ServiceManagerProxyMasterEntity", "GuestInfoVersion", "guest_info_version", true, "NVarChar", 10, 0, 0, false, "", null, typeof(System.String), 17);
			this.AddElementFieldMapping("ServiceManagerProxyMasterEntity", "Hash", "hash", false, "BigInt", 0, 19, 0, false, "", null, typeof(System.Int64), 18);
			this.AddElementFieldMapping("ServiceManagerProxyMasterEntity", "LoyaltyCardDuration", "loyalty_card_duration", false, "TinyInt", 0, 3, 0, false, "", null, typeof(System.Byte), 19);
			this.AddElementFieldMapping("ServiceManagerProxyMasterEntity", "LoyaltyCardUrl", "loyalty_card_url", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 20);
			this.AddElementFieldMapping("ServiceManagerProxyMasterEntity", "LoyaltyCardVersion", "loyalty_card_version", true, "NVarChar", 10, 0, 0, false, "", null, typeof(System.String), 21);
			this.AddElementFieldMapping("ServiceManagerProxyMasterEntity", "Password", "password", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 22);
			this.AddElementFieldMapping("ServiceManagerProxyMasterEntity", "ServiceManagerProxyId", "service_manager_proxy_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 23);
			this.AddElementFieldMapping("ServiceManagerProxyMasterEntity", "ServiceManagerProxyName", "service_manager_proxy_name", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 24);
			this.AddElementFieldMapping("ServiceManagerProxyMasterEntity", "SignatureCaptureUrl", "signature_capture_url", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 25);
			this.AddElementFieldMapping("ServiceManagerProxyMasterEntity", "SignatureCaptureVersion", "signature_capture_version", true, "NVarChar", 10, 0, 0, false, "", null, typeof(System.String), 26);
			this.AddElementFieldMapping("ServiceManagerProxyMasterEntity", "SiteId", "site_id", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 27);
			this.AddElementFieldMapping("ServiceManagerProxyMasterEntity", "Username", "username", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 28);
			this.AddElementFieldMapping("ServiceManagerProxyMasterEntity", "UsingRguestPaymentDeviceFlag", "using_rguest_payment_device_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 29);
		}

		/// <summary>Inits SmuCommandMasterEntity's mappings</summary>
		private void InitSmuCommandMasterEntityMappings()
		{
			this.AddElementMapping("SmuCommandMasterEntity", @"it_cfg", @"dbo", "SMU_Command_Master", 9);
			this.AddElementFieldMapping("SmuCommandMasterEntity", "CommandId", "command_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("SmuCommandMasterEntity", "CreatedUtcDateTime", "created_utc_date_time", false, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 1);
			this.AddElementFieldMapping("SmuCommandMasterEntity", "DeviceId", "device_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("SmuCommandMasterEntity", "DeviceTypeId", "device_type_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("SmuCommandMasterEntity", "DivisionId", "division_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("SmuCommandMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("SmuCommandMasterEntity", "ExtraData", "extra_data", true, "NText", 1073741823, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("SmuCommandMasterEntity", "RecordId", "record_id", false, "UniqueIdentifier", 0, 0, 0, false, "", null, typeof(System.Guid), 7);
			this.AddElementFieldMapping("SmuCommandMasterEntity", "StoreId", "store_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
		}

		/// <summary>Inits SmuStatusMasterEntity's mappings</summary>
		private void InitSmuStatusMasterEntityMappings()
		{
			this.AddElementMapping("SmuStatusMasterEntity", @"it_cfg", @"dbo", "SMU_Status_Master", 27);
			this.AddElementFieldMapping("SmuStatusMasterEntity", "ConfigurationLoadUtcTime", "configuration_load_utc_time", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 0);
			this.AddElementFieldMapping("SmuStatusMasterEntity", "CurrentClockUtcTime", "current_clock_utc_time", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 1);
			this.AddElementFieldMapping("SmuStatusMasterEntity", "DeviceId", "device_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("SmuStatusMasterEntity", "DeviceIpAddress", "device_ip_address", true, "VarChar", 40, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("SmuStatusMasterEntity", "DeviceTypeId", "device_type_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("SmuStatusMasterEntity", "DivisionId", "division_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("SmuStatusMasterEntity", "Drawer1EmpId", "drawer1_emp_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("SmuStatusMasterEntity", "Drawer2EmpId", "drawer2_emp_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("SmuStatusMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("SmuStatusMasterEntity", "IsLtsInitSuccessFlag", "is_lts_init_success_flag", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 9);
			this.AddElementFieldMapping("SmuStatusMasterEntity", "IsTerminalLockedFlag", "is_terminal_locked_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 10);
			this.AddElementFieldMapping("SmuStatusMasterEntity", "IsTerminalOnline", "is_terminal_online", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 11);
			this.AddElementFieldMapping("SmuStatusMasterEntity", "IsTrainingModeFlag", "is_training_mode_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 12);
			this.AddElementFieldMapping("SmuStatusMasterEntity", "LastCheckNumber", "last_check_number", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 13);
			this.AddElementFieldMapping("SmuStatusMasterEntity", "LastUpdateUtcDateTime", "last_update_utc_date_time", false, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 14);
			this.AddElementFieldMapping("SmuStatusMasterEntity", "LtsAvailabilityFlag", "lts_availability_flag", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 15);
			this.AddElementFieldMapping("SmuStatusMasterEntity", "LtsServerIpAddress", "lts_server_ip_address", true, "VarChar", 40, 0, 0, false, "", null, typeof(System.String), 16);
			this.AddElementFieldMapping("SmuStatusMasterEntity", "MealPeriodId", "meal_period_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 17);
			this.AddElementFieldMapping("SmuStatusMasterEntity", "ProfitCenterId", "profit_center_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 18);
			this.AddElementFieldMapping("SmuStatusMasterEntity", "ProgramLoadUtcTime", "program_load_utc_time", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 19);
			this.AddElementFieldMapping("SmuStatusMasterEntity", "ScreenState", "screen_state", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 20);
			this.AddElementFieldMapping("SmuStatusMasterEntity", "SignonEmpId", "signon_emp_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 21);
			this.AddElementFieldMapping("SmuStatusMasterEntity", "SignonJobcodeId", "signon_jobcode_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 22);
			this.AddElementFieldMapping("SmuStatusMasterEntity", "SmuStatusId", "smu_status_id", false, "UniqueIdentifier", 0, 0, 0, false, "", null, typeof(System.Guid), 23);
			this.AddElementFieldMapping("SmuStatusMasterEntity", "StoreId", "store_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 24);
			this.AddElementFieldMapping("SmuStatusMasterEntity", "TlogCheckCount", "tlog_check_count", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 25);
			this.AddElementFieldMapping("SmuStatusMasterEntity", "VersionNumber", "version_number", true, "NVarChar", 20, 0, 0, false, "", null, typeof(System.String), 26);
		}

		/// <summary>Inits SpecialInstrMasterEntity's mappings</summary>
		private void InitSpecialInstrMasterEntityMappings()
		{
			this.AddElementMapping("SpecialInstrMasterEntity", @"it_cfg", @"dbo", "Special_Instr_Master", 7);
			this.AddElementFieldMapping("SpecialInstrMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("SpecialInstrMasterEntity", "RowVersion", "row_version", false, "Timestamp", 2147483647, 0, 0, false, "", null, typeof(System.Byte[]), 1);
			this.AddElementFieldMapping("SpecialInstrMasterEntity", "SiAbbr1", "SI_abbr1", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("SpecialInstrMasterEntity", "SiAbbr2", "SI_abbr2", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("SpecialInstrMasterEntity", "SiId", "SI_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("SpecialInstrMasterEntity", "SiName", "SI_name", true, "NVarChar", 20, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("SpecialInstrMasterEntity", "StoreId", "store_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
		}

		/// <summary>Inits SystemConfigurationMasterEntity's mappings</summary>
		private void InitSystemConfigurationMasterEntityMappings()
		{
			this.AddElementMapping("SystemConfigurationMasterEntity", @"it_cfg", @"dbo", "System_Configuration_Master", 2);
			this.AddElementFieldMapping("SystemConfigurationMasterEntity", "PropertyName", "property_name", false, "VarChar", 50, 0, 0, false, "", null, typeof(System.String), 0);
			this.AddElementFieldMapping("SystemConfigurationMasterEntity", "PropertyValue", "property_value", true, "NVarChar", 256, 0, 0, false, "", null, typeof(System.String), 1);
		}

		/// <summary>Inits TableLayoutMasterEntity's mappings</summary>
		private void InitTableLayoutMasterEntityMappings()
		{
			this.AddElementMapping("TableLayoutMasterEntity", @"it_cfg", @"dbo", "Table_Layout_Master", 14);
			this.AddElementFieldMapping("TableLayoutMasterEntity", "AllowButtonOverlapFlag", "allow_button_overlap_flag", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 0);
			this.AddElementFieldMapping("TableLayoutMasterEntity", "ButtonPadding", "button_padding", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("TableLayoutMasterEntity", "DataControlGroupId", "data_control_group_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("TableLayoutMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("TableLayoutMasterEntity", "GridSize", "grid_size", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("TableLayoutMasterEntity", "ScreenSizeId", "screen_size_id", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 5);
			this.AddElementFieldMapping("TableLayoutMasterEntity", "ScreenTypeId", "screen_type_id", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 6);
			this.AddElementFieldMapping("TableLayoutMasterEntity", "SnapToGridFlag", "snap_to_grid_flag", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 7);
			this.AddElementFieldMapping("TableLayoutMasterEntity", "StoreId", "store_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("TableLayoutMasterEntity", "ThemeId", "theme_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("TableLayoutMasterEntity", "TlAbbr1", "tl_abbr1", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("TableLayoutMasterEntity", "TlAbbr2", "tl_abbr2", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("TableLayoutMasterEntity", "TlId", "tl_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("TableLayoutMasterEntity", "TlName", "tl_name", true, "NVarChar", 20, 0, 0, false, "", null, typeof(System.String), 13);
		}

		/// <summary>Inits TaxCatMasterEntity's mappings</summary>
		private void InitTaxCatMasterEntityMappings()
		{
			this.AddElementMapping("TaxCatMasterEntity", @"it_cfg", @"dbo", "Tax_Cat_Master", 7);
			this.AddElementFieldMapping("TaxCatMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("TaxCatMasterEntity", "RowVersion", "row_version", false, "Timestamp", 2147483647, 0, 0, false, "", null, typeof(System.Byte[]), 1);
			this.AddElementFieldMapping("TaxCatMasterEntity", "StoreId", "store_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("TaxCatMasterEntity", "TaxCatAbbr1", "tax_cat_abbr1", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("TaxCatMasterEntity", "TaxCatAbbr2", "tax_cat_abbr2", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("TaxCatMasterEntity", "TaxCatId", "tax_cat_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("TaxCatMasterEntity", "TaxCatName", "tax_cat_name", true, "NVarChar", 16, 0, 0, false, "", null, typeof(System.String), 6);
		}

		/// <summary>Inits TaxGroupMasterEntity's mappings</summary>
		private void InitTaxGroupMasterEntityMappings()
		{
			this.AddElementMapping("TaxGroupMasterEntity", @"it_cfg", @"dbo", "Tax_Group_Master", 6);
			this.AddElementFieldMapping("TaxGroupMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("TaxGroupMasterEntity", "StoreId", "store_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("TaxGroupMasterEntity", "TaxGrpAbbr1", "tax_grp_abbr1", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("TaxGroupMasterEntity", "TaxGrpAbbr2", "tax_grp_abbr2", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("TaxGroupMasterEntity", "TaxGrpId", "tax_grp_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("TaxGroupMasterEntity", "TaxGrpName", "tax_grp_name", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 5);
		}

		/// <summary>Inits TaxGrpTaxJoinEntity's mappings</summary>
		private void InitTaxGrpTaxJoinEntityMappings()
		{
			this.AddElementMapping("TaxGrpTaxJoinEntity", @"it_cfg", @"dbo", "TaxGrp_Tax_Join", 3);
			this.AddElementFieldMapping("TaxGrpTaxJoinEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("TaxGrpTaxJoinEntity", "TaxGrpId", "tax_grp_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("TaxGrpTaxJoinEntity", "TaxId", "tax_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
		}

		/// <summary>Inits TaxMasterEntity's mappings</summary>
		private void InitTaxMasterEntityMappings()
		{
			this.AddElementMapping("TaxMasterEntity", @"it_cfg", @"dbo", "Tax_Master", 18);
			this.AddElementFieldMapping("TaxMasterEntity", "CompoundLevelId", "compound_level_id", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 0);
			this.AddElementFieldMapping("TaxMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("TaxMasterEntity", "LimitTaxReportingFlag", "limit_tax_reporting_flag", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 2);
			this.AddElementFieldMapping("TaxMasterEntity", "LoyaltyEarnEligibleFlag", "loyalty_earn_eligible_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 3);
			this.AddElementFieldMapping("TaxMasterEntity", "LoyaltyRedeemEligibleFlag", "loyalty_redeem_eligible_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 4);
			this.AddElementFieldMapping("TaxMasterEntity", "RoundBasis", "round_basis", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("TaxMasterEntity", "RoundTypeId", "round_type_id", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 6);
			this.AddElementFieldMapping("TaxMasterEntity", "RowVersion", "row_version", false, "Timestamp", 2147483647, 0, 0, false, "", null, typeof(System.Byte[]), 7);
			this.AddElementFieldMapping("TaxMasterEntity", "StoreId", "store_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("TaxMasterEntity", "TaxAbbr1", "tax_abbr1", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("TaxMasterEntity", "TaxAbbr2", "tax_abbr2", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("TaxMasterEntity", "TaxAmt", "tax_amt", true, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 11);
			this.AddElementFieldMapping("TaxMasterEntity", "TaxCatId", "tax_cat_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("TaxMasterEntity", "TaxId", "tax_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 13);
			this.AddElementFieldMapping("TaxMasterEntity", "TaxName", "tax_name", true, "NVarChar", 16, 0, 0, false, "", null, typeof(System.String), 14);
			this.AddElementFieldMapping("TaxMasterEntity", "TaxPercent", "tax_percent", true, "Float", 0, 38, 0, false, "", null, typeof(System.Double), 15);
			this.AddElementFieldMapping("TaxMasterEntity", "TaxRemovableFlag", "tax_removable_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 16);
			this.AddElementFieldMapping("TaxMasterEntity", "ThreshholdAmt", "threshhold_amt", true, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 17);
		}

		/// <summary>Inits TaxRevCatJoinEntity's mappings</summary>
		private void InitTaxRevCatJoinEntityMappings()
		{
			this.AddElementMapping("TaxRevCatJoinEntity", @"it_cfg", @"dbo", "Tax_RevCat_Join", 5);
			this.AddElementFieldMapping("TaxRevCatJoinEntity", "EmpPercent", "emp_percent", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("TaxRevCatJoinEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("TaxRevCatJoinEntity", "PatronPercent", "patron_percent", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("TaxRevCatJoinEntity", "RevCatId", "rev_cat_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("TaxRevCatJoinEntity", "TaxId", "tax_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
		}

		/// <summary>Inits TenderClassMasterEntity's mappings</summary>
		private void InitTenderClassMasterEntityMappings()
		{
			this.AddElementMapping("TenderClassMasterEntity", @"it_cfg", @"dbo", "Tender_Class_Master", 5);
			this.AddElementFieldMapping("TenderClassMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("TenderClassMasterEntity", "RowVersion", "row_version", false, "Timestamp", 2147483647, 0, 0, false, "", null, typeof(System.Byte[]), 1);
			this.AddElementFieldMapping("TenderClassMasterEntity", "StoreId", "store_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("TenderClassMasterEntity", "TenderClassId", "tender_class_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("TenderClassMasterEntity", "TenderClassName", "tender_class_name", true, "NVarChar", 30, 0, 0, false, "", null, typeof(System.String), 4);
		}

		/// <summary>Inits TenderMasterEntity's mappings</summary>
		private void InitTenderMasterEntityMappings()
		{
			this.AddElementMapping("TenderMasterEntity", @"it_cfg", @"dbo", "Tender_Master", 51);
			this.AddElementFieldMapping("TenderMasterEntity", "AdditionalCheckidCodeId", "additional_checkid_code_id", false, "TinyInt", 0, 3, 0, false, "", null, typeof(System.Byte), 0);
			this.AddElementFieldMapping("TenderMasterEntity", "AutoRemoveTaxFlag", "auto_remove_tax_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 1);
			this.AddElementFieldMapping("TenderMasterEntity", "BypassPdsFlag", "bypass_pds_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 2);
			this.AddElementFieldMapping("TenderMasterEntity", "CheckTypeId", "check_type_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("TenderMasterEntity", "CompTenderFlag", "comp_tender_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 4);
			this.AddElementFieldMapping("TenderMasterEntity", "DestinationPropertyCode", "destination_property_code", true, "NVarChar", 6, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("TenderMasterEntity", "DiscoupId", "discoup_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("TenderMasterEntity", "EmvCardTypeCode", "emv_card_type_code", true, "NVarChar", 20, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("TenderMasterEntity", "EnterTipPrompt", "enter_tip_prompt", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 8);
			this.AddElementFieldMapping("TenderMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("TenderMasterEntity", "FirstTenderFlag", "first_tender_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 10);
			this.AddElementFieldMapping("TenderMasterEntity", "FrankingCodeId", "franking_code_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("TenderMasterEntity", "IccDecimalPlaces", "icc_decimal_places", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 12);
			this.AddElementFieldMapping("TenderMasterEntity", "IccRate", "icc_rate", true, "Float", 0, 38, 0, false, "", null, typeof(System.Double), 13);
			this.AddElementFieldMapping("TenderMasterEntity", "LastTenderFlag", "last_tender_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 14);
			this.AddElementFieldMapping("TenderMasterEntity", "LoyaltyEarnEligibleFlag", "loyalty_earn_eligible_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 15);
			this.AddElementFieldMapping("TenderMasterEntity", "NumReceiptsPrint", "num_receipts_print", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 16);
			this.AddElementFieldMapping("TenderMasterEntity", "OpenCashdrwrCodeId", "open_cashdrwr_code_id", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 17);
			this.AddElementFieldMapping("TenderMasterEntity", "OvertenderCodeId", "overtender_code_id", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 18);
			this.AddElementFieldMapping("TenderMasterEntity", "PostAcctNo", "post_acct_no", true, "NVarChar", 30, 0, 0, false, "", null, typeof(System.String), 19);
			this.AddElementFieldMapping("TenderMasterEntity", "PostSiteId", "post_site_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 20);
			this.AddElementFieldMapping("TenderMasterEntity", "PostSystem1Flag", "post_system1_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 21);
			this.AddElementFieldMapping("TenderMasterEntity", "PostSystem2Flag", "post_system2_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 22);
			this.AddElementFieldMapping("TenderMasterEntity", "PostSystem3Flag", "post_system3_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 23);
			this.AddElementFieldMapping("TenderMasterEntity", "PostSystem4Flag", "post_system4_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 24);
			this.AddElementFieldMapping("TenderMasterEntity", "PostSystem5Flag", "post_system5_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 25);
			this.AddElementFieldMapping("TenderMasterEntity", "PostSystem6Flag", "post_system6_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 26);
			this.AddElementFieldMapping("TenderMasterEntity", "PostSystem7Flag", "post_system7_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 27);
			this.AddElementFieldMapping("TenderMasterEntity", "PostSystem8Flag", "post_system8_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 28);
			this.AddElementFieldMapping("TenderMasterEntity", "PriceLevelId", "price_level_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 29);
			this.AddElementFieldMapping("TenderMasterEntity", "PromptCvvFlag", "prompt_cvv_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 30);
			this.AddElementFieldMapping("TenderMasterEntity", "PromptExtraAlphaFlag", "prompt_extra_alpha_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 31);
			this.AddElementFieldMapping("TenderMasterEntity", "PromptExtraDataFlag", "prompt_extra_data_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 32);
			this.AddElementFieldMapping("TenderMasterEntity", "PromptZipcodeFlag", "prompt_zipcode_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 33);
			this.AddElementFieldMapping("TenderMasterEntity", "RequireAmtFlag", "require_amt_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 34);
			this.AddElementFieldMapping("TenderMasterEntity", "RestrictedFlag", "restricted_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 35);
			this.AddElementFieldMapping("TenderMasterEntity", "RowVersion", "row_version", false, "Timestamp", 2147483647, 0, 0, false, "", null, typeof(System.Byte[]), 36);
			this.AddElementFieldMapping("TenderMasterEntity", "SalesTippableFlag", "sales_tippable_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 37);
			this.AddElementFieldMapping("TenderMasterEntity", "SecurityId", "security_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 38);
			this.AddElementFieldMapping("TenderMasterEntity", "StoreId", "store_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 39);
			this.AddElementFieldMapping("TenderMasterEntity", "TaxCompCode", "tax_comp_code", false, "TinyInt", 0, 3, 0, false, "", null, typeof(System.Byte), 40);
			this.AddElementFieldMapping("TenderMasterEntity", "TenderAbbr1", "tender_abbr1", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 41);
			this.AddElementFieldMapping("TenderMasterEntity", "TenderAbbr2", "tender_abbr2", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 42);
			this.AddElementFieldMapping("TenderMasterEntity", "TenderClassId", "tender_class_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 43);
			this.AddElementFieldMapping("TenderMasterEntity", "TenderId", "tender_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 44);
			this.AddElementFieldMapping("TenderMasterEntity", "TenderLimit", "tender_limit", true, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 45);
			this.AddElementFieldMapping("TenderMasterEntity", "TenderName", "tender_name", true, "NVarChar", 16, 0, 0, false, "", null, typeof(System.String), 46);
			this.AddElementFieldMapping("TenderMasterEntity", "UseArchiveFlag", "use_archive_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 47);
			this.AddElementFieldMapping("TenderMasterEntity", "UseSigcapFlag", "use_sigcap_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 48);
			this.AddElementFieldMapping("TenderMasterEntity", "VerificationCodeId", "verification_code_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 49);
			this.AddElementFieldMapping("TenderMasterEntity", "VerificationManualEntryCodeId", "verification_manual_entry_code_id", false, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 50);
		}

		/// <summary>Inits TermGrpChkTypeJoinEntity's mappings</summary>
		private void InitTermGrpChkTypeJoinEntityMappings()
		{
			this.AddElementMapping("TermGrpChkTypeJoinEntity", @"it_cfg", @"dbo", "Term_Grp_ChkType_Join", 11);
			this.AddElementFieldMapping("TermGrpChkTypeJoinEntity", "CheckPriceLevelId", "check_price_level_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("TermGrpChkTypeJoinEntity", "CheckSecId", "check_sec_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("TermGrpChkTypeJoinEntity", "CheckTenderAllowed", "check_tender_allowed", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 2);
			this.AddElementFieldMapping("TermGrpChkTypeJoinEntity", "CheckTypeId", "check_type_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("TermGrpChkTypeJoinEntity", "CoversPrompt", "covers_prompt", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 4);
			this.AddElementFieldMapping("TermGrpChkTypeJoinEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("TermGrpChkTypeJoinEntity", "RoomNoPrompt", "room_no_prompt", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 6);
			this.AddElementFieldMapping("TermGrpChkTypeJoinEntity", "ServerPrompt", "server_prompt", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 7);
			this.AddElementFieldMapping("TermGrpChkTypeJoinEntity", "TablePrompt", "table_prompt", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 8);
			this.AddElementFieldMapping("TermGrpChkTypeJoinEntity", "TaxOverride", "tax_override", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 9);
			this.AddElementFieldMapping("TermGrpChkTypeJoinEntity", "TermGrpId", "term_grp_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
		}

		/// <summary>Inits TermGrpJobcodeJoinEntity's mappings</summary>
		private void InitTermGrpJobcodeJoinEntityMappings()
		{
			this.AddElementMapping("TermGrpJobcodeJoinEntity", @"it_cfg", @"dbo", "Term_Grp_Jobcode_Join", 3);
			this.AddElementFieldMapping("TermGrpJobcodeJoinEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("TermGrpJobcodeJoinEntity", "JobcodeId", "jobcode_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("TermGrpJobcodeJoinEntity", "TermGrpId", "term_grp_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
		}

		/// <summary>Inits TermGrpMasterEntity's mappings</summary>
		private void InitTermGrpMasterEntityMappings()
		{
			this.AddElementMapping("TermGrpMasterEntity", @"it_cfg", @"dbo", "Term_Grp_Master", 15);
			this.AddElementFieldMapping("TermGrpMasterEntity", "CalendarPeriodId", "calendar_period_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("TermGrpMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("TermGrpMasterEntity", "ScreenScheduleId", "screen_schedule_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("TermGrpMasterEntity", "SigcapButton1", "sigcap_button_1", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("TermGrpMasterEntity", "SigcapButton2", "sigcap_button_2", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("TermGrpMasterEntity", "SigcapButton3", "sigcap_button_3", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("TermGrpMasterEntity", "SigcapButton4", "sigcap_button_4", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("TermGrpMasterEntity", "SigcapTipPrompt", "sigcap_tip_prompt", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("TermGrpMasterEntity", "SigcapTypeCode", "sigcap_type_code", false, "TinyInt", 0, 3, 0, false, "", null, typeof(System.Byte), 8);
			this.AddElementFieldMapping("TermGrpMasterEntity", "SiMenuId", "SI_menu_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("TermGrpMasterEntity", "StartTimeFlag", "start_time_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 10);
			this.AddElementFieldMapping("TermGrpMasterEntity", "StoreId", "store_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("TermGrpMasterEntity", "TermGrpId", "term_grp_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("TermGrpMasterEntity", "TermGrpName", "term_grp_name", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 13);
			this.AddElementFieldMapping("TermGrpMasterEntity", "TerminalTextId", "terminal_text_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 14);
		}

		/// <summary>Inits TermGrpMenuJoinEntity's mappings</summary>
		private void InitTermGrpMenuJoinEntityMappings()
		{
			this.AddElementMapping("TermGrpMenuJoinEntity", @"it_cfg", @"dbo", "Term_Grp_Menu_Join", 5);
			this.AddElementFieldMapping("TermGrpMenuJoinEntity", "CheckFlag", "check_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 0);
			this.AddElementFieldMapping("TermGrpMenuJoinEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("TermGrpMenuJoinEntity", "MenuId", "menu_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("TermGrpMenuJoinEntity", "ParentMenuId", "parent_menu_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("TermGrpMenuJoinEntity", "TermGrpId", "term_grp_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
		}

		/// <summary>Inits TermGrpMenuJoinCookerEntity's mappings</summary>
		private void InitTermGrpMenuJoinCookerEntityMappings()
		{
			this.AddElementMapping("TermGrpMenuJoinCookerEntity", @"it_cfg", @"dbo", "Term_Grp_Menu_Join_Cooker", 6);
			this.AddElementFieldMapping("TermGrpMenuJoinCookerEntity", "CheckFlag", "check_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 0);
			this.AddElementFieldMapping("TermGrpMenuJoinCookerEntity", "CookerHandle", "cooker_handle", false, "Decimal", 0, 20, 10, false, "", null, typeof(System.Decimal), 1);
			this.AddElementFieldMapping("TermGrpMenuJoinCookerEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("TermGrpMenuJoinCookerEntity", "MenuId", "menu_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("TermGrpMenuJoinCookerEntity", "ParentMenuId", "parent_menu_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("TermGrpMenuJoinCookerEntity", "TermGrpId", "term_grp_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
		}

		/// <summary>Inits TermGrpMpJoinEntity's mappings</summary>
		private void InitTermGrpMpJoinEntityMappings()
		{
			this.AddElementMapping("TermGrpMpJoinEntity", @"it_cfg", @"dbo", "Term_Grp_MP_Join", 12);
			this.AddElementFieldMapping("TermGrpMpJoinEntity", "AutoCheckTypeId", "auto_check_type_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("TermGrpMpJoinEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("TermGrpMpJoinEntity", "MealPeriodId", "meal_period_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("TermGrpMpJoinEntity", "MealPeriodStartTime1", "meal_period_start_time_1", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
			this.AddElementFieldMapping("TermGrpMpJoinEntity", "MealPeriodStartTime2", "meal_period_start_time_2", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("TermGrpMpJoinEntity", "MealPeriodStartTime3", "meal_period_start_time_3", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("TermGrpMpJoinEntity", "MealPeriodStartTime4", "meal_period_start_time_4", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("TermGrpMpJoinEntity", "MealPeriodStartTime5", "meal_period_start_time_5", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("TermGrpMpJoinEntity", "MealPeriodStartTime6", "meal_period_start_time_6", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("TermGrpMpJoinEntity", "MealPeriodStartTime7", "meal_period_start_time_7", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 9);
			this.AddElementFieldMapping("TermGrpMpJoinEntity", "PriceLevelId", "price_level_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("TermGrpMpJoinEntity", "TermGrpId", "term_grp_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
		}

		/// <summary>Inits TermGrpMpmenuJoinEntity's mappings</summary>
		private void InitTermGrpMpmenuJoinEntityMappings()
		{
			this.AddElementMapping("TermGrpMpmenuJoinEntity", @"it_cfg", @"dbo", "Term_Grp_MPMenu_Join", 6);
			this.AddElementFieldMapping("TermGrpMpmenuJoinEntity", "CalendarDaySeqId", "calendar_day_seq_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("TermGrpMpmenuJoinEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("TermGrpMpmenuJoinEntity", "MealPeriodId", "meal_period_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("TermGrpMpmenuJoinEntity", "MenuId", "menu_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("TermGrpMpmenuJoinEntity", "MenuSeq", "menu_seq", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("TermGrpMpmenuJoinEntity", "TermGrpId", "term_grp_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
		}

		/// <summary>Inits TermGrpTndrJoinEntity's mappings</summary>
		private void InitTermGrpTndrJoinEntityMappings()
		{
			this.AddElementMapping("TermGrpTndrJoinEntity", @"it_cfg", @"dbo", "Term_Grp_Tndr_Join", 4);
			this.AddElementFieldMapping("TermGrpTndrJoinEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("TermGrpTndrJoinEntity", "SeqNo", "seq_no", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 1);
			this.AddElementFieldMapping("TermGrpTndrJoinEntity", "TenderId", "tender_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("TermGrpTndrJoinEntity", "TermGrpId", "term_grp_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
		}

		/// <summary>Inits TermGrpTransitionJoinEntity's mappings</summary>
		private void InitTermGrpTransitionJoinEntityMappings()
		{
			this.AddElementMapping("TermGrpTransitionJoinEntity", @"it_cfg", @"dbo", "Term_Grp_Transition_Join", 4);
			this.AddElementFieldMapping("TermGrpTransitionJoinEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("TermGrpTransitionJoinEntity", "ScreenId", "screen_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("TermGrpTransitionJoinEntity", "TermGrpId", "term_grp_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("TermGrpTransitionJoinEntity", "TransitionId", "transition_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
		}

		/// <summary>Inits TerminalMasterEntity's mappings</summary>
		private void InitTerminalMasterEntityMappings()
		{
			this.AddElementMapping("TerminalMasterEntity", @"it_cfg", @"dbo", "Terminal_Master", 29);
			this.AddElementFieldMapping("TerminalMasterEntity", "AltBargunTermId", "alt_bargun_term_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("TerminalMasterEntity", "AltRcptTermId", "alt_rcpt_term_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("TerminalMasterEntity", "BargunBumpOverrideFlag", "bargun_bump_override_flag", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 2);
			this.AddElementFieldMapping("TerminalMasterEntity", "BargunPrintDrinksOnBumpFlag", "bargun_print_drinks_on_bump_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 3);
			this.AddElementFieldMapping("TerminalMasterEntity", "BargunTermFlag", "bargun_term_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 4);
			this.AddElementFieldMapping("TerminalMasterEntity", "CurrentVersion", "current_version", true, "NVarChar", 15, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("TerminalMasterEntity", "DefaultTableLayoutId", "default_table_layout_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("TerminalMasterEntity", "FirstTableNo", "first_table_no", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("TerminalMasterEntity", "FolFileLoadFlag", "FOL_file_load_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 8);
			this.AddElementFieldMapping("TerminalMasterEntity", "GaFileLoadFlag", "GA_file_load_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 9);
			this.AddElementFieldMapping("TerminalMasterEntity", "IpAddress", "IP_address", true, "NVarChar", 15, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("TerminalMasterEntity", "LastPingUtcDateTime", "last_ping_utc_date_time", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 11);
			this.AddElementFieldMapping("TerminalMasterEntity", "NumTables", "num_tables", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("TerminalMasterEntity", "PedValue", "ped_value", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 13);
			this.AddElementFieldMapping("TerminalMasterEntity", "PrimaryProfitCenterId", "primary_profit_center_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 14);
			this.AddElementFieldMapping("TerminalMasterEntity", "ProfileId", "profile_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 15);
			this.AddElementFieldMapping("TerminalMasterEntity", "ReceiptPrinterType", "receipt_printer_type", false, "TinyInt", 0, 3, 0, false, "", null, typeof(System.Byte), 16);
			this.AddElementFieldMapping("TerminalMasterEntity", "RmsFileLoadFlag", "RMS_file_load_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 17);
			this.AddElementFieldMapping("TerminalMasterEntity", "RowVersion", "row_version", false, "Timestamp", 2147483647, 0, 0, false, "", null, typeof(System.Byte[]), 18);
			this.AddElementFieldMapping("TerminalMasterEntity", "StaticReceiptPrinterId", "static_receipt_printer_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 19);
			this.AddElementFieldMapping("TerminalMasterEntity", "TermActiveFlag", "term_active_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 20);
			this.AddElementFieldMapping("TerminalMasterEntity", "TermGrpId", "term_grp_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 21);
			this.AddElementFieldMapping("TerminalMasterEntity", "TermId", "term_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 22);
			this.AddElementFieldMapping("TerminalMasterEntity", "TermName", "term_name", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 23);
			this.AddElementFieldMapping("TerminalMasterEntity", "TermOptionGrpId", "term_option_grp_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 24);
			this.AddElementFieldMapping("TerminalMasterEntity", "TermPrinterGrpId", "term_printer_grp_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 25);
			this.AddElementFieldMapping("TerminalMasterEntity", "TermReceiptInfo", "term_receipt_info", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 26);
			this.AddElementFieldMapping("TerminalMasterEntity", "TermServiceGrpId", "term_service_grp_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 27);
			this.AddElementFieldMapping("TerminalMasterEntity", "VirtualTermFlag", "virtual_term_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 28);
		}

		/// <summary>Inits TerminalProfitCtrJoinEntity's mappings</summary>
		private void InitTerminalProfitCtrJoinEntityMappings()
		{
			this.AddElementMapping("TerminalProfitCtrJoinEntity", @"it_cfg", @"dbo", "Terminal_Profit_Ctr_Join", 4);
			this.AddElementFieldMapping("TerminalProfitCtrJoinEntity", "PrimaryProfitCenterFlag", "primary_profit_center_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 0);
			this.AddElementFieldMapping("TerminalProfitCtrJoinEntity", "ProfitCenterId", "profit_center_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("TerminalProfitCtrJoinEntity", "ProfitCenterSeq", "profit_center_seq", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 2);
			this.AddElementFieldMapping("TerminalProfitCtrJoinEntity", "TermId", "term_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
		}

		/// <summary>Inits TerminalTextGuestInfoJoinEntity's mappings</summary>
		private void InitTerminalTextGuestInfoJoinEntityMappings()
		{
			this.AddElementMapping("TerminalTextGuestInfoJoinEntity", @"it_cfg", @"dbo", "Terminal_Text_GuestInfo_Join", 5);
			this.AddElementFieldMapping("TerminalTextGuestInfoJoinEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("TerminalTextGuestInfoJoinEntity", "SequenceId", "sequence_id", false, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 1);
			this.AddElementFieldMapping("TerminalTextGuestInfoJoinEntity", "TerminalIntlId", "terminal_intl_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("TerminalTextGuestInfoJoinEntity", "TerminalIntlUserText", "terminal_intl_user_text", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("TerminalTextGuestInfoJoinEntity", "TerminalTextId", "terminal_text_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
		}

		/// <summary>Inits TerminalTextMasterEntity's mappings</summary>
		private void InitTerminalTextMasterEntityMappings()
		{
			this.AddElementMapping("TerminalTextMasterEntity", @"it_cfg", @"dbo", "Terminal_Text_Master", 4);
			this.AddElementFieldMapping("TerminalTextMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("TerminalTextMasterEntity", "StoreId", "store_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("TerminalTextMasterEntity", "TerminalTextId", "terminal_text_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("TerminalTextMasterEntity", "TerminalTextName", "terminal_text_name", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 3);
		}

		/// <summary>Inits TermOptionGrpMasterEntity's mappings</summary>
		private void InitTermOptionGrpMasterEntityMappings()
		{
			this.AddElementMapping("TermOptionGrpMasterEntity", @"it_cfg", @"dbo", "Term_Option_Grp_Master", 4);
			this.AddElementFieldMapping("TermOptionGrpMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("TermOptionGrpMasterEntity", "StoreId", "store_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("TermOptionGrpMasterEntity", "TermOptionGrpId", "term_option_grp_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("TermOptionGrpMasterEntity", "TermOptionGrpName", "term_option_grp_name", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 3);
		}

		/// <summary>Inits TermOptionGrpOptionJoinEntity's mappings</summary>
		private void InitTermOptionGrpOptionJoinEntityMappings()
		{
			this.AddElementMapping("TermOptionGrpOptionJoinEntity", @"it_cfg", @"dbo", "Term_Option_Grp_Option_Join", 4);
			this.AddElementFieldMapping("TermOptionGrpOptionJoinEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("TermOptionGrpOptionJoinEntity", "OptionFlag", "option_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 1);
			this.AddElementFieldMapping("TermOptionGrpOptionJoinEntity", "OptionId", "option_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("TermOptionGrpOptionJoinEntity", "TermOptionGrpId", "term_option_grp_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
		}

		/// <summary>Inits TermPrinterGrpKpprntJoinEntity's mappings</summary>
		private void InitTermPrinterGrpKpprntJoinEntityMappings()
		{
			this.AddElementMapping("TermPrinterGrpKpprntJoinEntity", @"it_cfg", @"dbo", "Term_Printer_Grp_KPPrnt_Join", 5);
			this.AddElementFieldMapping("TermPrinterGrpKpprntJoinEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("TermPrinterGrpKpprntJoinEntity", "KpAlternateDeviceId", "KP_alternate_device_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("TermPrinterGrpKpprntJoinEntity", "KpDeviceId", "KP_device_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("TermPrinterGrpKpprntJoinEntity", "KpPrinterId", "KP_printer_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("TermPrinterGrpKpprntJoinEntity", "TermPrinterGrpId", "term_printer_grp_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
		}

		/// <summary>Inits TermPrinterGrpMasterEntity's mappings</summary>
		private void InitTermPrinterGrpMasterEntityMappings()
		{
			this.AddElementMapping("TermPrinterGrpMasterEntity", @"it_cfg", @"dbo", "Term_Printer_Grp_Master", 9);
			this.AddElementFieldMapping("TermPrinterGrpMasterEntity", "AlternatePrintingCode", "alternate_printing_code", false, "TinyInt", 0, 3, 0, false, "", null, typeof(System.Byte), 0);
			this.AddElementFieldMapping("TermPrinterGrpMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("TermPrinterGrpMasterEntity", "StatusCodeId", "status_code_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("TermPrinterGrpMasterEntity", "StoreId", "store_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("TermPrinterGrpMasterEntity", "TermPrinterGrpAbbr1", "term_printer_grp_abbr1", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("TermPrinterGrpMasterEntity", "TermPrinterGrpAbbr2", "term_printer_grp_abbr2", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("TermPrinterGrpMasterEntity", "TermPrinterGrpId", "term_printer_grp_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("TermPrinterGrpMasterEntity", "TermPrinterGrpName", "term_printer_grp_name", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("TermPrinterGrpMasterEntity", "UseAlternateFlag", "use_alternate_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 8);
		}

		/// <summary>Inits TermPrinterGrpRouteJoinEntity's mappings</summary>
		private void InitTermPrinterGrpRouteJoinEntityMappings()
		{
			this.AddElementMapping("TermPrinterGrpRouteJoinEntity", @"it_cfg", @"dbo", "Term_Printer_Grp_Route_Join", 11);
			this.AddElementFieldMapping("TermPrinterGrpRouteJoinEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("TermPrinterGrpRouteJoinEntity", "GroupRouteGenId", "group_route_gen_id", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("TermPrinterGrpRouteJoinEntity", "KpRouteId", "kp_route_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("TermPrinterGrpRouteJoinEntity", "StartMinuteFri", "start_minute_fri", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("TermPrinterGrpRouteJoinEntity", "StartMinuteMon", "start_minute_mon", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("TermPrinterGrpRouteJoinEntity", "StartMinuteSat", "start_minute_sat", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("TermPrinterGrpRouteJoinEntity", "StartMinuteSun", "start_minute_sun", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("TermPrinterGrpRouteJoinEntity", "StartMinuteThu", "start_minute_thu", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("TermPrinterGrpRouteJoinEntity", "StartMinuteTue", "start_minute_tue", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("TermPrinterGrpRouteJoinEntity", "StartMinuteWed", "start_minute_wed", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("TermPrinterGrpRouteJoinEntity", "TermPrinterGrpId", "term_printer_grp_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
		}

		/// <summary>Inits ThemeColorJoinEntity's mappings</summary>
		private void InitThemeColorJoinEntityMappings()
		{
			this.AddElementMapping("ThemeColorJoinEntity", @"it_cfg", @"dbo", "Theme_Color_Join", 4);
			this.AddElementFieldMapping("ThemeColorJoinEntity", "ColorCodeId", "color_code_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ThemeColorJoinEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ThemeColorJoinEntity", "RgbColor", "RGB_color", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ThemeColorJoinEntity", "ThemeId", "theme_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
		}

		/// <summary>Inits ThemeFontJoinEntity's mappings</summary>
		private void InitThemeFontJoinEntityMappings()
		{
			this.AddElementMapping("ThemeFontJoinEntity", @"it_cfg", @"dbo", "Theme_Font_Join", 3);
			this.AddElementFieldMapping("ThemeFontJoinEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ThemeFontJoinEntity", "FontId", "font_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ThemeFontJoinEntity", "ThemeId", "theme_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
		}

		/// <summary>Inits ThemeMasterEntity's mappings</summary>
		private void InitThemeMasterEntityMappings()
		{
			this.AddElementMapping("ThemeMasterEntity", @"it_cfg", @"dbo", "Theme_Master", 4);
			this.AddElementFieldMapping("ThemeMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ThemeMasterEntity", "StoreId", "store_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ThemeMasterEntity", "ThemeId", "theme_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ThemeMasterEntity", "ThemeName", "theme_name", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 3);
		}

		/// <summary>Inits ThemeObjectJoinEntity's mappings</summary>
		private void InitThemeObjectJoinEntityMappings()
		{
			this.AddElementMapping("ThemeObjectJoinEntity", @"it_cfg", @"dbo", "Theme_Object_Join", 14);
			this.AddElementFieldMapping("ThemeObjectJoinEntity", "ButtonBackcolor", "button_backcolor", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ThemeObjectJoinEntity", "ButtonColumn", "button_column", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ThemeObjectJoinEntity", "ButtonFontId", "button_font_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ThemeObjectJoinEntity", "ButtonForecolor", "button_forecolor", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("ThemeObjectJoinEntity", "ButtonHeight", "button_height", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 4);
			this.AddElementFieldMapping("ThemeObjectJoinEntity", "ButtonImageId", "button_image_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("ThemeObjectJoinEntity", "ButtonNextMenuId", "button_next_menu_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("ThemeObjectJoinEntity", "ButtonRow", "button_row", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("ThemeObjectJoinEntity", "ButtonShapeId", "button_shape_id", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 8);
			this.AddElementFieldMapping("ThemeObjectJoinEntity", "ButtonWidth", "button_width", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 9);
			this.AddElementFieldMapping("ThemeObjectJoinEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("ThemeObjectJoinEntity", "ErrorCodeId", "error_code_id", true, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 11);
			this.AddElementFieldMapping("ThemeObjectJoinEntity", "ObjectTypeId", "object_type_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("ThemeObjectJoinEntity", "ThemeId", "theme_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 13);
		}

		/// <summary>Inits UdefDateRangeInstanceEntity's mappings</summary>
		private void InitUdefDateRangeInstanceEntityMappings()
		{
			this.AddElementMapping("UdefDateRangeInstanceEntity", @"it_cfg", @"dbo", "Udef_Date_Range_Instance", 9);
			this.AddElementFieldMapping("UdefDateRangeInstanceEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("UdefDateRangeInstanceEntity", "PeriodTypeId", "period_type_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("UdefDateRangeInstanceEntity", "RangeEndDate", "range_end_date", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 2);
			this.AddElementFieldMapping("UdefDateRangeInstanceEntity", "RangeEndTime", "range_end_time", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
			this.AddElementFieldMapping("UdefDateRangeInstanceEntity", "RangeInstanceDesc", "range_instance_desc", true, "NVarChar", 30, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("UdefDateRangeInstanceEntity", "RangeInstanceId", "range_instance_id", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("UdefDateRangeInstanceEntity", "RangeStartDate", "range_start_date", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("UdefDateRangeInstanceEntity", "RangeStartTime", "range_start_time", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("UdefDateRangeInstanceEntity", "StoreId", "store_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
		}

		/// <summary>Inits UdefPeriodTypeMasterEntity's mappings</summary>
		private void InitUdefPeriodTypeMasterEntityMappings()
		{
			this.AddElementMapping("UdefPeriodTypeMasterEntity", @"it_cfg", @"dbo", "Udef_Period_Type_Master", 4);
			this.AddElementFieldMapping("UdefPeriodTypeMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("UdefPeriodTypeMasterEntity", "PeriodTypeDesc", "period_type_desc", true, "NVarChar", 30, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("UdefPeriodTypeMasterEntity", "PeriodTypeId", "period_type_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("UdefPeriodTypeMasterEntity", "StoreId", "store_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
		}

		/// <summary>Inits VoidReasonMasterEntity's mappings</summary>
		private void InitVoidReasonMasterEntityMappings()
		{
			this.AddElementMapping("VoidReasonMasterEntity", @"it_cfg", @"dbo", "Void_Reason_Master", 13);
			this.AddElementFieldMapping("VoidReasonMasterEntity", "EntId", "ent_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("VoidReasonMasterEntity", "KpFlag", "KP_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 1);
			this.AddElementFieldMapping("VoidReasonMasterEntity", "PmixFlag", "PMix_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 2);
			this.AddElementFieldMapping("VoidReasonMasterEntity", "RefundCheckFlag", "refund_check_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 3);
			this.AddElementFieldMapping("VoidReasonMasterEntity", "RowVersion", "row_version", false, "Timestamp", 2147483647, 0, 0, false, "", null, typeof(System.Byte[]), 4);
			this.AddElementFieldMapping("VoidReasonMasterEntity", "StoreId", "store_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("VoidReasonMasterEntity", "VoidItemFlag", "void_item_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 6);
			this.AddElementFieldMapping("VoidReasonMasterEntity", "VoidOpenChkFlag", "void_open_chk_flag", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 7);
			this.AddElementFieldMapping("VoidReasonMasterEntity", "VoidReasonAbbr1", "void_reason_abbr1", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("VoidReasonMasterEntity", "VoidReasonAbbr2", "void_reason_abbr2", true, "NVarChar", 7, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("VoidReasonMasterEntity", "VoidReasonId", "void_reason_id", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("VoidReasonMasterEntity", "VoidReasonName", "void_reason_name", true, "NVarChar", 20, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("VoidReasonMasterEntity", "VoidReasonSecId", "void_reason_sec_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
		}

		/// <summary>Inits ZooMasterEntity's mappings</summary>
		private void InitZooMasterEntityMappings()
		{
			this.AddElementMapping("ZooMasterEntity", @"it_cfg", @"dbo", "Zoo_Master", 18);
			this.AddElementFieldMapping("ZooMasterEntity", "ConfigEventXml", "config_event_xml", false, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 0);
			this.AddElementFieldMapping("ZooMasterEntity", "CustomerId", "customer_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ZooMasterEntity", "DivisionId", "division_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ZooMasterEntity", "EntId", "ent_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("ZooMasterEntity", "MasterObjectDisplayName", "master_object_display_name", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("ZooMasterEntity", "MasterObjectId", "master_object_id", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("ZooMasterEntity", "ObjectDisplayName", "object_display_name", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("ZooMasterEntity", "ObjectId", "object_id", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("ZooMasterEntity", "ObjectTypeId", "object_type_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("ZooMasterEntity", "StoreId", "store_id", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("ZooMasterEntity", "TrackAction", "track_action", false, "NVarChar", 1, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("ZooMasterEntity", "TrackDttime", "track_dttime", false, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 11);
			this.AddElementFieldMapping("ZooMasterEntity", "TrackGroupId", "track_group_id", false, "BigInt", 0, 19, 0, false, "", null, typeof(System.Int64), 12);
			this.AddElementFieldMapping("ZooMasterEntity", "TrackSourceId", "track_source_id", false, "TinyInt", 0, 3, 0, false, "", null, typeof(System.Byte), 13);
			this.AddElementFieldMapping("ZooMasterEntity", "TrackUsername", "track_username", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 14);
			this.AddElementFieldMapping("ZooMasterEntity", "VirtualDatabaseName", "virtual_database_name", false, "VarChar", 100, 0, 0, false, "", null, typeof(System.String), 15);
			this.AddElementFieldMapping("ZooMasterEntity", "VirtualTableName", "virtual_table_name", false, "VarChar", 100, 0, 0, false, "", null, typeof(System.String), 16);
			this.AddElementFieldMapping("ZooMasterEntity", "ZooId", "zoo_id", false, "BigInt", 0, 19, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int64), 17);
		}

	}
}
