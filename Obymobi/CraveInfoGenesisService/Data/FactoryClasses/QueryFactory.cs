﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
////////////////////////////////////////////////////////////// 
using System;
using InfoGenesis.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.QuerySpec;

namespace InfoGenesis.FactoryClasses
{
	/// <summary>Factory class to produce DynamicQuery instances and EntityQuery instances</summary>
	public partial class QueryFactory
	{
		private int _aliasCounter = 0;

		/// <summary>Creates a new DynamicQuery instance with no alias set.</summary>
		/// <returns>Ready to use DynamicQuery instance</returns>
		public DynamicQuery Create()
		{
			return Create(string.Empty);
		}

		/// <summary>Creates a new DynamicQuery instance with the alias specified as the alias set.</summary>
		/// <param name="alias">The alias.</param>
		/// <returns>Ready to use DynamicQuery instance</returns>
		public DynamicQuery Create(string alias)
		{
			return new DynamicQuery(new ElementCreator(), alias, this.GetNextAliasCounterValue());
		}
	
		/// <summary>Creates a new EntityQuery for the entity of the type specified with no alias set.</summary>
		/// <typeparam name="TEntity">The type of the entity to produce the query for.</typeparam>
		/// <returns>ready to use EntityQuery instance</returns>
		public EntityQuery<TEntity> Create<TEntity>()
			where TEntity : IEntityCore
		{
			return Create<TEntity>(string.Empty);
		}

		/// <summary>Creates a new EntityQuery for the entity of the type specified with the alias specified as the alias set.</summary>
		/// <typeparam name="TEntity">The type of the entity to produce the query for.</typeparam>
		/// <param name="alias">The alias.</param>
		/// <returns>ready to use EntityQuery instance</returns>
		public EntityQuery<TEntity> Create<TEntity>(string alias)
			where TEntity : IEntityCore
		{
			return new EntityQuery<TEntity>(new ElementCreator(), alias, this.GetNextAliasCounterValue());
		}
				
		/// <summary>Creates a new field object with the name specified and of resulttype 'object'. Used for referring to aliased fields in another projection.</summary>
		/// <param name="fieldName">Name of the field.</param>
		/// <returns>Ready to use field object</returns>
		public EntityField2 Field(string fieldName)
		{
			return Field<object>(string.Empty, fieldName);
		}

		/// <summary>Creates a new field object with the name specified and of resulttype 'object'. Used for referring to aliased fields in another projection.</summary>
		/// <param name="targetAlias">The alias of the table/query to target.</param>
		/// <param name="fieldName">Name of the field.</param>
		/// <returns>Ready to use field object</returns>
		public EntityField2 Field(string targetAlias, string fieldName)
		{
			return Field<object>(targetAlias, fieldName);
		}

		/// <summary>Creates a new field object with the name specified and of resulttype 'TValue'. Used for referring to aliased fields in another projection.</summary>
		/// <typeparam name="TValue">The type of the value represented by the field.</typeparam>
		/// <param name="fieldName">Name of the field.</param>
		/// <returns>Ready to use field object</returns>
		public EntityField2 Field<TValue>(string fieldName)
		{
			return Field<TValue>(string.Empty, fieldName);
		}

		/// <summary>Creates a new field object with the name specified and of resulttype 'TValue'. Used for referring to aliased fields in another projection.</summary>
		/// <typeparam name="TValue">The type of the value.</typeparam>
		/// <param name="targetAlias">The alias of the table/query to target.</param>
		/// <param name="fieldName">Name of the field.</param>
		/// <returns>Ready to use field object</returns>
		public EntityField2 Field<TValue>(string targetAlias, string fieldName)
		{
			return new EntityField2(fieldName, targetAlias, typeof(TValue));
		}
		
		/// <summary>Gets the next alias counter value to produce artifical aliases with</summary>
		private int GetNextAliasCounterValue()
		{
			_aliasCounter++;
			return _aliasCounter;
		}
		
		/// <summary>Creates and returns a new EntityQuery for the MyBonusCodeMaster entity</summary>
		public EntityQuery<MyBonusCodeMasterEntity> BonusCodeMaster
		{
			get { return Create<MyBonusCodeMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyButtonAbbrJoin entity</summary>
		public EntityQuery<MyButtonAbbrJoinEntity> ButtonAbbrJoin
		{
			get { return Create<MyButtonAbbrJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyCardTypeMaster entity</summary>
		public EntityQuery<MyCardTypeMasterEntity> CardTypeMaster
		{
			get { return Create<MyCardTypeMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyCardTypeRangeJoin entity</summary>
		public EntityQuery<MyCardTypeRangeJoinEntity> CardTypeRangeJoin
		{
			get { return Create<MyCardTypeRangeJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyCfgVersion entity</summary>
		public EntityQuery<MyCfgVersionEntity> CfgVersion
		{
			get { return Create<MyCfgVersionEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyCheckTypeMaster entity</summary>
		public EntityQuery<MyCheckTypeMasterEntity> CheckTypeMaster
		{
			get { return Create<MyCheckTypeMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyChefMaster entity</summary>
		public EntityQuery<MyChefMasterEntity> ChefMaster
		{
			get { return Create<MyChefMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyChkTypeGratJoin entity</summary>
		public EntityQuery<MyChkTypeGratJoinEntity> ChkTypeGratJoin
		{
			get { return Create<MyChkTypeGratJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyChkTypeGratRevJoin entity</summary>
		public EntityQuery<MyChkTypeGratRevJoinEntity> ChkTypeGratRevJoin
		{
			get { return Create<MyChkTypeGratRevJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyChkTypeGratTaxJoin entity</summary>
		public EntityQuery<MyChkTypeGratTaxJoinEntity> ChkTypeGratTaxJoin
		{
			get { return Create<MyChkTypeGratTaxJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyChkTypeTaxGrpJoin entity</summary>
		public EntityQuery<MyChkTypeTaxGrpJoinEntity> ChkTypeTaxGrpJoin
		{
			get { return Create<MyChkTypeTaxGrpJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyChkTypeTaxGrpTaxJoin entity</summary>
		public EntityQuery<MyChkTypeTaxGrpTaxJoinEntity> ChkTypeTaxGrpTaxJoin
		{
			get { return Create<MyChkTypeTaxGrpTaxJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyChkTypeVatGratJoin entity</summary>
		public EntityQuery<MyChkTypeVatGratJoinEntity> ChkTypeVatGratJoin
		{
			get { return Create<MyChkTypeVatGratJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyChkTypeVatRevJoin entity</summary>
		public EntityQuery<MyChkTypeVatRevJoinEntity> ChkTypeVatRevJoin
		{
			get { return Create<MyChkTypeVatRevJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyChoiceGroupMaster entity</summary>
		public EntityQuery<MyChoiceGroupMasterEntity> ChoiceGroupMaster
		{
			get { return Create<MyChoiceGroupMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyChoiceGrpModJoin entity</summary>
		public EntityQuery<MyChoiceGrpModJoinEntity> ChoiceGrpModJoin
		{
			get { return Create<MyChoiceGrpModJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyComboItemJoin entity</summary>
		public EntityQuery<MyComboItemJoinEntity> ComboItemJoin
		{
			get { return Create<MyComboItemJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyComboMaster entity</summary>
		public EntityQuery<MyComboMasterEntity> ComboMaster
		{
			get { return Create<MyComboMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyConfigurationIdMaster entity</summary>
		public EntityQuery<MyConfigurationIdMasterEntity> ConfigurationIdMaster
		{
			get { return Create<MyConfigurationIdMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyDeviceMaster entity</summary>
		public EntityQuery<MyDeviceMasterEntity> DeviceMaster
		{
			get { return Create<MyDeviceMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyDiscoupMaster entity</summary>
		public EntityQuery<MyDiscoupMasterEntity> DiscoupMaster
		{
			get { return Create<MyDiscoupMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyEmpGroupMaster entity</summary>
		public EntityQuery<MyEmpGroupMasterEntity> EmpGroupMaster
		{
			get { return Create<MyEmpGroupMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyEmpGrpEmpJoin entity</summary>
		public EntityQuery<MyEmpGrpEmpJoinEntity> EmpGrpEmpJoin
		{
			get { return Create<MyEmpGrpEmpJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyEmpJobcodeJoin entity</summary>
		public EntityQuery<MyEmpJobcodeJoinEntity> EmpJobcodeJoin
		{
			get { return Create<MyEmpJobcodeJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyEmpMaster entity</summary>
		public EntityQuery<MyEmpMasterEntity> EmpMaster
		{
			get { return Create<MyEmpMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyFuncBttnTextJoin entity</summary>
		public EntityQuery<MyFuncBttnTextJoinEntity> FuncBttnTextJoin
		{
			get { return Create<MyFuncBttnTextJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyGa4680ExportParmMaster entity</summary>
		public EntityQuery<MyGa4680ExportParmMasterEntity> Ga4680ExportParmMaster
		{
			get { return Create<MyGa4680ExportParmMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyGa4680ImportParmMaster entity</summary>
		public EntityQuery<MyGa4680ImportParmMasterEntity> Ga4680ImportParmMaster
		{
			get { return Create<MyGa4680ImportParmMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyGeneralLedgerMapMaster entity</summary>
		public EntityQuery<MyGeneralLedgerMapMasterEntity> GeneralLedgerMapMaster
		{
			get { return Create<MyGeneralLedgerMapMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyGiftCardMaster entity</summary>
		public EntityQuery<MyGiftCardMasterEntity> GiftCardMaster
		{
			get { return Create<MyGiftCardMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyGratuityCatMaster entity</summary>
		public EntityQuery<MyGratuityCatMasterEntity> GratuityCatMaster
		{
			get { return Create<MyGratuityCatMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyGratuityMaster entity</summary>
		public EntityQuery<MyGratuityMasterEntity> GratuityMaster
		{
			get { return Create<MyGratuityMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyIdMaster entity</summary>
		public EntityQuery<MyIdMasterEntity> IdMaster
		{
			get { return Create<MyIdMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyImageLibraryMaster entity</summary>
		public EntityQuery<MyImageLibraryMasterEntity> ImageLibraryMaster
		{
			get { return Create<MyImageLibraryMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyIntlDescriptorTextJoin entity</summary>
		public EntityQuery<MyIntlDescriptorTextJoinEntity> IntlDescriptorTextJoin
		{
			get { return Create<MyIntlDescriptorTextJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyIpServerAttribMaster entity</summary>
		public EntityQuery<MyIpServerAttribMasterEntity> IpServerAttribMaster
		{
			get { return Create<MyIpServerAttribMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyJobcodeFuncBttnJoin entity</summary>
		public EntityQuery<MyJobcodeFuncBttnJoinEntity> JobcodeFuncBttnJoin
		{
			get { return Create<MyJobcodeFuncBttnJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyJobcodeFunctJoin entity</summary>
		public EntityQuery<MyJobcodeFunctJoinEntity> JobcodeFunctJoin
		{
			get { return Create<MyJobcodeFunctJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyJobcodeMaster entity</summary>
		public EntityQuery<MyJobcodeMasterEntity> JobcodeMaster
		{
			get { return Create<MyJobcodeMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyKdsCategoryMaster entity</summary>
		public EntityQuery<MyKdsCategoryMasterEntity> KdsCategoryMaster
		{
			get { return Create<MyKdsCategoryMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyKdsVideoMaster entity</summary>
		public EntityQuery<MyKdsVideoMasterEntity> KdsVideoMaster
		{
			get { return Create<MyKdsVideoMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyKpOptionGrpCopyJoin entity</summary>
		public EntityQuery<MyKpOptionGrpCopyJoinEntity> KpOptionGrpCopyJoin
		{
			get { return Create<MyKpOptionGrpCopyJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyKpOptionGrpFormatJoin entity</summary>
		public EntityQuery<MyKpOptionGrpFormatJoinEntity> KpOptionGrpFormatJoin
		{
			get { return Create<MyKpOptionGrpFormatJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyKpOptionGrpMaster entity</summary>
		public EntityQuery<MyKpOptionGrpMasterEntity> KpOptionGrpMaster
		{
			get { return Create<MyKpOptionGrpMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyKpPrinterMaster entity</summary>
		public EntityQuery<MyKpPrinterMasterEntity> KpPrinterMaster
		{
			get { return Create<MyKpPrinterMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyKpRouteMaster entity</summary>
		public EntityQuery<MyKpRouteMasterEntity> KpRouteMaster
		{
			get { return Create<MyKpRouteMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyKpRoutePrinterJoin entity</summary>
		public EntityQuery<MyKpRoutePrinterJoinEntity> KpRoutePrinterJoin
		{
			get { return Create<MyKpRoutePrinterJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyMachineOptionsMaster entity</summary>
		public EntityQuery<MyMachineOptionsMasterEntity> MachineOptionsMaster
		{
			get { return Create<MyMachineOptionsMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyMealPeriodMaster entity</summary>
		public EntityQuery<MyMealPeriodMasterEntity> MealPeriodMaster
		{
			get { return Create<MyMealPeriodMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyMembershipMaster entity</summary>
		public EntityQuery<MyMembershipMasterEntity> MembershipMaster
		{
			get { return Create<MyMembershipMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyMembershipProfitCenterJoin entity</summary>
		public EntityQuery<MyMembershipProfitCenterJoinEntity> MembershipProfitCenterJoin
		{
			get { return Create<MyMembershipProfitCenterJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyMenuBttnObjJoin entity</summary>
		public EntityQuery<MyMenuBttnObjJoinEntity> MenuBttnObjJoin
		{
			get { return Create<MyMenuBttnObjJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyMenuItemGroupMaster entity</summary>
		public EntityQuery<MyMenuItemGroupMasterEntity> MenuItemGroupMaster
		{
			get { return Create<MyMenuItemGroupMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyMenuItemMaster entity</summary>
		public EntityQuery<MyMenuItemMasterEntity> MenuItemMaster
		{
			get { return Create<MyMenuItemMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyMenuMaster entity</summary>
		public EntityQuery<MyMenuMasterEntity> MenuMaster
		{
			get { return Create<MyMenuMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyMenuMigrpJoin entity</summary>
		public EntityQuery<MyMenuMigrpJoinEntity> MenuMigrpJoin
		{
			get { return Create<MyMenuMigrpJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyMiChoiceGrpJoin entity</summary>
		public EntityQuery<MyMiChoiceGrpJoinEntity> MiChoiceGrpJoin
		{
			get { return Create<MyMiChoiceGrpJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyMiKpprinterJoin entity</summary>
		public EntityQuery<MyMiKpprinterJoinEntity> MiKpprinterJoin
		{
			get { return Create<MyMiKpprinterJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyMiPriceJoin entity</summary>
		public EntityQuery<MyMiPriceJoinEntity> MiPriceJoin
		{
			get { return Create<MyMiPriceJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyMiSkuJoin entity</summary>
		public EntityQuery<MyMiSkuJoinEntity> MiSkuJoin
		{
			get { return Create<MyMiSkuJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyModifierKpprinterJoin entity</summary>
		public EntityQuery<MyModifierKpprinterJoinEntity> ModifierKpprinterJoin
		{
			get { return Create<MyModifierKpprinterJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyModifierMaster entity</summary>
		public EntityQuery<MyModifierMasterEntity> ModifierMaster
		{
			get { return Create<MyModifierMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyModifierPriceJoin entity</summary>
		public EntityQuery<MyModifierPriceJoinEntity> ModifierPriceJoin
		{
			get { return Create<MyModifierPriceJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyOptionsMaster entity</summary>
		public EntityQuery<MyOptionsMasterEntity> OptionsMaster
		{
			get { return Create<MyOptionsMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyPriceLevelMaster entity</summary>
		public EntityQuery<MyPriceLevelMasterEntity> PriceLevelMaster
		{
			get { return Create<MyPriceLevelMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyProcessMaster entity</summary>
		public EntityQuery<MyProcessMasterEntity> ProcessMaster
		{
			get { return Create<MyProcessMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyProductClassMaster entity</summary>
		public EntityQuery<MyProductClassMasterEntity> ProductClassMaster
		{
			get { return Create<MyProductClassMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyProfitCenterDayPartJoin entity</summary>
		public EntityQuery<MyProfitCenterDayPartJoinEntity> ProfitCenterDayPartJoin
		{
			get { return Create<MyProfitCenterDayPartJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyProfitCenterGroupJoin entity</summary>
		public EntityQuery<MyProfitCenterGroupJoinEntity> ProfitCenterGroupJoin
		{
			get { return Create<MyProfitCenterGroupJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyProfitCenterGroupMaster entity</summary>
		public EntityQuery<MyProfitCenterGroupMasterEntity> ProfitCenterGroupMaster
		{
			get { return Create<MyProfitCenterGroupMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyProfitCenterMaster entity</summary>
		public EntityQuery<MyProfitCenterMasterEntity> ProfitCenterMaster
		{
			get { return Create<MyProfitCenterMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyProfitCenterReceiptPrinterJoin entity</summary>
		public EntityQuery<MyProfitCenterReceiptPrinterJoinEntity> ProfitCenterReceiptPrinterJoin
		{
			get { return Create<MyProfitCenterReceiptPrinterJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyProfitCenterTableJoin entity</summary>
		public EntityQuery<MyProfitCenterTableJoinEntity> ProfitCenterTableJoin
		{
			get { return Create<MyProfitCenterTableJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyQuickTenderMaster entity</summary>
		public EntityQuery<MyQuickTenderMasterEntity> QuickTenderMaster
		{
			get { return Create<MyQuickTenderMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyReceiptPrinterMaster entity</summary>
		public EntityQuery<MyReceiptPrinterMasterEntity> ReceiptPrinterMaster
		{
			get { return Create<MyReceiptPrinterMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyReportCatMaster entity</summary>
		public EntityQuery<MyReportCatMasterEntity> ReportCatMaster
		{
			get { return Create<MyReportCatMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyRevenueCatMaster entity</summary>
		public EntityQuery<MyRevenueCatMasterEntity> RevenueCatMaster
		{
			get { return Create<MyRevenueCatMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyRevenueClassMaster entity</summary>
		public EntityQuery<MyRevenueClassMasterEntity> RevenueClassMaster
		{
			get { return Create<MyRevenueClassMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyRowLockMaster entity</summary>
		public EntityQuery<MyRowLockMasterEntity> RowLockMaster
		{
			get { return Create<MyRowLockMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyScreenTemplateMaster entity</summary>
		public EntityQuery<MyScreenTemplateMasterEntity> ScreenTemplateMaster
		{
			get { return Create<MyScreenTemplateMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyScreenTemplateMigrpJoin entity</summary>
		public EntityQuery<MyScreenTemplateMigrpJoinEntity> ScreenTemplateMigrpJoin
		{
			get { return Create<MyScreenTemplateMigrpJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MySecurityLevelMaster entity</summary>
		public EntityQuery<MySecurityLevelMasterEntity> SecurityLevelMaster
		{
			get { return Create<MySecurityLevelMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MySelectionGroupItemJoin entity</summary>
		public EntityQuery<MySelectionGroupItemJoinEntity> SelectionGroupItemJoin
		{
			get { return Create<MySelectionGroupItemJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MySelectionGroupMaster entity</summary>
		public EntityQuery<MySelectionGroupMasterEntity> SelectionGroupMaster
		{
			get { return Create<MySelectionGroupMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyServiceManagerProxyMaster entity</summary>
		public EntityQuery<MyServiceManagerProxyMasterEntity> ServiceManagerProxyMaster
		{
			get { return Create<MyServiceManagerProxyMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MySmuCommandMaster entity</summary>
		public EntityQuery<MySmuCommandMasterEntity> SmuCommandMaster
		{
			get { return Create<MySmuCommandMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MySmuStatusMaster entity</summary>
		public EntityQuery<MySmuStatusMasterEntity> SmuStatusMaster
		{
			get { return Create<MySmuStatusMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MySpecialInstrMaster entity</summary>
		public EntityQuery<MySpecialInstrMasterEntity> SpecialInstrMaster
		{
			get { return Create<MySpecialInstrMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MySystemConfigurationMaster entity</summary>
		public EntityQuery<MySystemConfigurationMasterEntity> SystemConfigurationMaster
		{
			get { return Create<MySystemConfigurationMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyTableLayoutMaster entity</summary>
		public EntityQuery<MyTableLayoutMasterEntity> TableLayoutMaster
		{
			get { return Create<MyTableLayoutMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyTaxCatMaster entity</summary>
		public EntityQuery<MyTaxCatMasterEntity> TaxCatMaster
		{
			get { return Create<MyTaxCatMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyTaxGroupMaster entity</summary>
		public EntityQuery<MyTaxGroupMasterEntity> TaxGroupMaster
		{
			get { return Create<MyTaxGroupMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyTaxGrpTaxJoin entity</summary>
		public EntityQuery<MyTaxGrpTaxJoinEntity> TaxGrpTaxJoin
		{
			get { return Create<MyTaxGrpTaxJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyTaxMaster entity</summary>
		public EntityQuery<MyTaxMasterEntity> TaxMaster
		{
			get { return Create<MyTaxMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyTaxRevCatJoin entity</summary>
		public EntityQuery<MyTaxRevCatJoinEntity> TaxRevCatJoin
		{
			get { return Create<MyTaxRevCatJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyTenderClassMaster entity</summary>
		public EntityQuery<MyTenderClassMasterEntity> TenderClassMaster
		{
			get { return Create<MyTenderClassMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyTenderMaster entity</summary>
		public EntityQuery<MyTenderMasterEntity> TenderMaster
		{
			get { return Create<MyTenderMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyTermGrpChkTypeJoin entity</summary>
		public EntityQuery<MyTermGrpChkTypeJoinEntity> TermGrpChkTypeJoin
		{
			get { return Create<MyTermGrpChkTypeJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyTermGrpJobcodeJoin entity</summary>
		public EntityQuery<MyTermGrpJobcodeJoinEntity> TermGrpJobcodeJoin
		{
			get { return Create<MyTermGrpJobcodeJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyTermGrpMaster entity</summary>
		public EntityQuery<MyTermGrpMasterEntity> TermGrpMaster
		{
			get { return Create<MyTermGrpMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyTermGrpMenuJoin entity</summary>
		public EntityQuery<MyTermGrpMenuJoinEntity> TermGrpMenuJoin
		{
			get { return Create<MyTermGrpMenuJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyTermGrpMenuJoinCooker entity</summary>
		public EntityQuery<MyTermGrpMenuJoinCookerEntity> TermGrpMenuJoinCooker
		{
			get { return Create<MyTermGrpMenuJoinCookerEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyTermGrpMpJoin entity</summary>
		public EntityQuery<MyTermGrpMpJoinEntity> TermGrpMpJoin
		{
			get { return Create<MyTermGrpMpJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyTermGrpMpmenuJoin entity</summary>
		public EntityQuery<MyTermGrpMpmenuJoinEntity> TermGrpMpmenuJoin
		{
			get { return Create<MyTermGrpMpmenuJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyTermGrpTndrJoin entity</summary>
		public EntityQuery<MyTermGrpTndrJoinEntity> TermGrpTndrJoin
		{
			get { return Create<MyTermGrpTndrJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyTermGrpTransitionJoin entity</summary>
		public EntityQuery<MyTermGrpTransitionJoinEntity> TermGrpTransitionJoin
		{
			get { return Create<MyTermGrpTransitionJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyTerminalMaster entity</summary>
		public EntityQuery<MyTerminalMasterEntity> TerminalMaster
		{
			get { return Create<MyTerminalMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyTerminalProfitCtrJoin entity</summary>
		public EntityQuery<MyTerminalProfitCtrJoinEntity> TerminalProfitCtrJoin
		{
			get { return Create<MyTerminalProfitCtrJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyTerminalTextGuestInfoJoin entity</summary>
		public EntityQuery<MyTerminalTextGuestInfoJoinEntity> TerminalTextGuestInfoJoin
		{
			get { return Create<MyTerminalTextGuestInfoJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyTerminalTextMaster entity</summary>
		public EntityQuery<MyTerminalTextMasterEntity> TerminalTextMaster
		{
			get { return Create<MyTerminalTextMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyTermOptionGrpMaster entity</summary>
		public EntityQuery<MyTermOptionGrpMasterEntity> TermOptionGrpMaster
		{
			get { return Create<MyTermOptionGrpMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyTermOptionGrpOptionJoin entity</summary>
		public EntityQuery<MyTermOptionGrpOptionJoinEntity> TermOptionGrpOptionJoin
		{
			get { return Create<MyTermOptionGrpOptionJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyTermPrinterGrpKpprntJoin entity</summary>
		public EntityQuery<MyTermPrinterGrpKpprntJoinEntity> TermPrinterGrpKpprntJoin
		{
			get { return Create<MyTermPrinterGrpKpprntJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyTermPrinterGrpMaster entity</summary>
		public EntityQuery<MyTermPrinterGrpMasterEntity> TermPrinterGrpMaster
		{
			get { return Create<MyTermPrinterGrpMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyTermPrinterGrpRouteJoin entity</summary>
		public EntityQuery<MyTermPrinterGrpRouteJoinEntity> TermPrinterGrpRouteJoin
		{
			get { return Create<MyTermPrinterGrpRouteJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyThemeColorJoin entity</summary>
		public EntityQuery<MyThemeColorJoinEntity> ThemeColorJoin
		{
			get { return Create<MyThemeColorJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyThemeFontJoin entity</summary>
		public EntityQuery<MyThemeFontJoinEntity> ThemeFontJoin
		{
			get { return Create<MyThemeFontJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyThemeMaster entity</summary>
		public EntityQuery<MyThemeMasterEntity> ThemeMaster
		{
			get { return Create<MyThemeMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyThemeObjectJoin entity</summary>
		public EntityQuery<MyThemeObjectJoinEntity> ThemeObjectJoin
		{
			get { return Create<MyThemeObjectJoinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyUdefDateRangeInstance entity</summary>
		public EntityQuery<MyUdefDateRangeInstanceEntity> UdefDateRangeInstance
		{
			get { return Create<MyUdefDateRangeInstanceEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyUdefPeriodTypeMaster entity</summary>
		public EntityQuery<MyUdefPeriodTypeMasterEntity> UdefPeriodTypeMaster
		{
			get { return Create<MyUdefPeriodTypeMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyVoidReasonMaster entity</summary>
		public EntityQuery<MyVoidReasonMasterEntity> VoidReasonMaster
		{
			get { return Create<MyVoidReasonMasterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MyZooMaster entity</summary>
		public EntityQuery<MyZooMasterEntity> ZooMaster
		{
			get { return Create<MyZooMasterEntity>(); }
		}

	}
}