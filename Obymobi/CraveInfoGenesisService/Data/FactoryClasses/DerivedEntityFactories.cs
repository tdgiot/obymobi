﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using InfoGenesis.EntityClasses;
using InfoGenesis.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace InfoGenesis.FactoryClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	

	
	/// <summary>Factory to create new, empty MyBonusCodeMasterEntity objects.</summary>
	[Serializable]
	public partial class MyBonusCodeMasterEntityFactory : BonusCodeMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyBonusCodeMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyBonusCodeMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyBonusCodeMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewBonusCodeMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyBonusCodeMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyButtonAbbrJoinEntity objects.</summary>
	[Serializable]
	public partial class MyButtonAbbrJoinEntityFactory : ButtonAbbrJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyButtonAbbrJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyButtonAbbrJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyButtonAbbrJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewButtonAbbrJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyButtonAbbrJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyCardTypeMasterEntity objects.</summary>
	[Serializable]
	public partial class MyCardTypeMasterEntityFactory : CardTypeMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyCardTypeMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyCardTypeMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyCardTypeMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCardTypeMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyCardTypeMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyCardTypeRangeJoinEntity objects.</summary>
	[Serializable]
	public partial class MyCardTypeRangeJoinEntityFactory : CardTypeRangeJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyCardTypeRangeJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyCardTypeRangeJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyCardTypeRangeJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCardTypeRangeJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyCardTypeRangeJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyCfgVersionEntity objects.</summary>
	[Serializable]
	public partial class MyCfgVersionEntityFactory : CfgVersionEntityFactory
	{
		/// <summary>CTor</summary>
		public MyCfgVersionEntityFactory()
		{
		}

		/// <summary>Creates a new MyCfgVersionEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyCfgVersionEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCfgVersionUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyCfgVersionEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyCheckTypeMasterEntity objects.</summary>
	[Serializable]
	public partial class MyCheckTypeMasterEntityFactory : CheckTypeMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyCheckTypeMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyCheckTypeMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyCheckTypeMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCheckTypeMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyCheckTypeMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyChefMasterEntity objects.</summary>
	[Serializable]
	public partial class MyChefMasterEntityFactory : ChefMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyChefMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyChefMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyChefMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewChefMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyChefMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyChkTypeGratJoinEntity objects.</summary>
	[Serializable]
	public partial class MyChkTypeGratJoinEntityFactory : ChkTypeGratJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyChkTypeGratJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyChkTypeGratJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyChkTypeGratJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewChkTypeGratJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyChkTypeGratJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyChkTypeGratRevJoinEntity objects.</summary>
	[Serializable]
	public partial class MyChkTypeGratRevJoinEntityFactory : ChkTypeGratRevJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyChkTypeGratRevJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyChkTypeGratRevJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyChkTypeGratRevJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewChkTypeGratRevJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyChkTypeGratRevJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyChkTypeGratTaxJoinEntity objects.</summary>
	[Serializable]
	public partial class MyChkTypeGratTaxJoinEntityFactory : ChkTypeGratTaxJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyChkTypeGratTaxJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyChkTypeGratTaxJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyChkTypeGratTaxJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewChkTypeGratTaxJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyChkTypeGratTaxJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyChkTypeTaxGrpJoinEntity objects.</summary>
	[Serializable]
	public partial class MyChkTypeTaxGrpJoinEntityFactory : ChkTypeTaxGrpJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyChkTypeTaxGrpJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyChkTypeTaxGrpJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyChkTypeTaxGrpJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewChkTypeTaxGrpJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyChkTypeTaxGrpJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyChkTypeTaxGrpTaxJoinEntity objects.</summary>
	[Serializable]
	public partial class MyChkTypeTaxGrpTaxJoinEntityFactory : ChkTypeTaxGrpTaxJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyChkTypeTaxGrpTaxJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyChkTypeTaxGrpTaxJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyChkTypeTaxGrpTaxJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewChkTypeTaxGrpTaxJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyChkTypeTaxGrpTaxJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyChkTypeVatGratJoinEntity objects.</summary>
	[Serializable]
	public partial class MyChkTypeVatGratJoinEntityFactory : ChkTypeVatGratJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyChkTypeVatGratJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyChkTypeVatGratJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyChkTypeVatGratJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewChkTypeVatGratJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyChkTypeVatGratJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyChkTypeVatRevJoinEntity objects.</summary>
	[Serializable]
	public partial class MyChkTypeVatRevJoinEntityFactory : ChkTypeVatRevJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyChkTypeVatRevJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyChkTypeVatRevJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyChkTypeVatRevJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewChkTypeVatRevJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyChkTypeVatRevJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyChoiceGroupMasterEntity objects.</summary>
	[Serializable]
	public partial class MyChoiceGroupMasterEntityFactory : ChoiceGroupMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyChoiceGroupMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyChoiceGroupMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyChoiceGroupMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewChoiceGroupMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyChoiceGroupMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyChoiceGrpModJoinEntity objects.</summary>
	[Serializable]
	public partial class MyChoiceGrpModJoinEntityFactory : ChoiceGrpModJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyChoiceGrpModJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyChoiceGrpModJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyChoiceGrpModJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewChoiceGrpModJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyChoiceGrpModJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyComboItemJoinEntity objects.</summary>
	[Serializable]
	public partial class MyComboItemJoinEntityFactory : ComboItemJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyComboItemJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyComboItemJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyComboItemJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewComboItemJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyComboItemJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyComboMasterEntity objects.</summary>
	[Serializable]
	public partial class MyComboMasterEntityFactory : ComboMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyComboMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyComboMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyComboMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewComboMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyComboMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyConfigurationIdMasterEntity objects.</summary>
	[Serializable]
	public partial class MyConfigurationIdMasterEntityFactory : ConfigurationIdMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyConfigurationIdMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyConfigurationIdMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyConfigurationIdMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewConfigurationIdMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyConfigurationIdMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyDeviceMasterEntity objects.</summary>
	[Serializable]
	public partial class MyDeviceMasterEntityFactory : DeviceMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyDeviceMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyDeviceMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyDeviceMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDeviceMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyDeviceMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyDiscoupMasterEntity objects.</summary>
	[Serializable]
	public partial class MyDiscoupMasterEntityFactory : DiscoupMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyDiscoupMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyDiscoupMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyDiscoupMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDiscoupMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyDiscoupMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyEmpGroupMasterEntity objects.</summary>
	[Serializable]
	public partial class MyEmpGroupMasterEntityFactory : EmpGroupMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyEmpGroupMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyEmpGroupMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyEmpGroupMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewEmpGroupMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyEmpGroupMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyEmpGrpEmpJoinEntity objects.</summary>
	[Serializable]
	public partial class MyEmpGrpEmpJoinEntityFactory : EmpGrpEmpJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyEmpGrpEmpJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyEmpGrpEmpJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyEmpGrpEmpJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewEmpGrpEmpJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyEmpGrpEmpJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyEmpJobcodeJoinEntity objects.</summary>
	[Serializable]
	public partial class MyEmpJobcodeJoinEntityFactory : EmpJobcodeJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyEmpJobcodeJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyEmpJobcodeJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyEmpJobcodeJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewEmpJobcodeJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyEmpJobcodeJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyEmpMasterEntity objects.</summary>
	[Serializable]
	public partial class MyEmpMasterEntityFactory : EmpMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyEmpMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyEmpMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyEmpMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewEmpMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyEmpMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyFuncBttnTextJoinEntity objects.</summary>
	[Serializable]
	public partial class MyFuncBttnTextJoinEntityFactory : FuncBttnTextJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyFuncBttnTextJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyFuncBttnTextJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyFuncBttnTextJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewFuncBttnTextJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyFuncBttnTextJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyGa4680ExportParmMasterEntity objects.</summary>
	[Serializable]
	public partial class MyGa4680ExportParmMasterEntityFactory : Ga4680ExportParmMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyGa4680ExportParmMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyGa4680ExportParmMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyGa4680ExportParmMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewGa4680ExportParmMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyGa4680ExportParmMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyGa4680ImportParmMasterEntity objects.</summary>
	[Serializable]
	public partial class MyGa4680ImportParmMasterEntityFactory : Ga4680ImportParmMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyGa4680ImportParmMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyGa4680ImportParmMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyGa4680ImportParmMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewGa4680ImportParmMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyGa4680ImportParmMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyGeneralLedgerMapMasterEntity objects.</summary>
	[Serializable]
	public partial class MyGeneralLedgerMapMasterEntityFactory : GeneralLedgerMapMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyGeneralLedgerMapMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyGeneralLedgerMapMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyGeneralLedgerMapMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewGeneralLedgerMapMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyGeneralLedgerMapMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyGiftCardMasterEntity objects.</summary>
	[Serializable]
	public partial class MyGiftCardMasterEntityFactory : GiftCardMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyGiftCardMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyGiftCardMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyGiftCardMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewGiftCardMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyGiftCardMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyGratuityCatMasterEntity objects.</summary>
	[Serializable]
	public partial class MyGratuityCatMasterEntityFactory : GratuityCatMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyGratuityCatMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyGratuityCatMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyGratuityCatMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewGratuityCatMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyGratuityCatMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyGratuityMasterEntity objects.</summary>
	[Serializable]
	public partial class MyGratuityMasterEntityFactory : GratuityMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyGratuityMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyGratuityMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyGratuityMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewGratuityMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyGratuityMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyIdMasterEntity objects.</summary>
	[Serializable]
	public partial class MyIdMasterEntityFactory : IdMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyIdMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyIdMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyIdMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewIdMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyIdMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyImageLibraryMasterEntity objects.</summary>
	[Serializable]
	public partial class MyImageLibraryMasterEntityFactory : ImageLibraryMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyImageLibraryMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyImageLibraryMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyImageLibraryMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewImageLibraryMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyImageLibraryMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyIntlDescriptorTextJoinEntity objects.</summary>
	[Serializable]
	public partial class MyIntlDescriptorTextJoinEntityFactory : IntlDescriptorTextJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyIntlDescriptorTextJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyIntlDescriptorTextJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyIntlDescriptorTextJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewIntlDescriptorTextJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyIntlDescriptorTextJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyIpServerAttribMasterEntity objects.</summary>
	[Serializable]
	public partial class MyIpServerAttribMasterEntityFactory : IpServerAttribMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyIpServerAttribMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyIpServerAttribMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyIpServerAttribMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewIpServerAttribMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyIpServerAttribMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyJobcodeFuncBttnJoinEntity objects.</summary>
	[Serializable]
	public partial class MyJobcodeFuncBttnJoinEntityFactory : JobcodeFuncBttnJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyJobcodeFuncBttnJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyJobcodeFuncBttnJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyJobcodeFuncBttnJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewJobcodeFuncBttnJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyJobcodeFuncBttnJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyJobcodeFunctJoinEntity objects.</summary>
	[Serializable]
	public partial class MyJobcodeFunctJoinEntityFactory : JobcodeFunctJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyJobcodeFunctJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyJobcodeFunctJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyJobcodeFunctJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewJobcodeFunctJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyJobcodeFunctJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyJobcodeMasterEntity objects.</summary>
	[Serializable]
	public partial class MyJobcodeMasterEntityFactory : JobcodeMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyJobcodeMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyJobcodeMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyJobcodeMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewJobcodeMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyJobcodeMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyKdsCategoryMasterEntity objects.</summary>
	[Serializable]
	public partial class MyKdsCategoryMasterEntityFactory : KdsCategoryMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyKdsCategoryMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyKdsCategoryMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyKdsCategoryMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewKdsCategoryMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyKdsCategoryMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyKdsVideoMasterEntity objects.</summary>
	[Serializable]
	public partial class MyKdsVideoMasterEntityFactory : KdsVideoMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyKdsVideoMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyKdsVideoMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyKdsVideoMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewKdsVideoMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyKdsVideoMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyKpOptionGrpCopyJoinEntity objects.</summary>
	[Serializable]
	public partial class MyKpOptionGrpCopyJoinEntityFactory : KpOptionGrpCopyJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyKpOptionGrpCopyJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyKpOptionGrpCopyJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyKpOptionGrpCopyJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewKpOptionGrpCopyJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyKpOptionGrpCopyJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyKpOptionGrpFormatJoinEntity objects.</summary>
	[Serializable]
	public partial class MyKpOptionGrpFormatJoinEntityFactory : KpOptionGrpFormatJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyKpOptionGrpFormatJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyKpOptionGrpFormatJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyKpOptionGrpFormatJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewKpOptionGrpFormatJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyKpOptionGrpFormatJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyKpOptionGrpMasterEntity objects.</summary>
	[Serializable]
	public partial class MyKpOptionGrpMasterEntityFactory : KpOptionGrpMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyKpOptionGrpMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyKpOptionGrpMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyKpOptionGrpMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewKpOptionGrpMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyKpOptionGrpMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyKpPrinterMasterEntity objects.</summary>
	[Serializable]
	public partial class MyKpPrinterMasterEntityFactory : KpPrinterMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyKpPrinterMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyKpPrinterMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyKpPrinterMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewKpPrinterMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyKpPrinterMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyKpRouteMasterEntity objects.</summary>
	[Serializable]
	public partial class MyKpRouteMasterEntityFactory : KpRouteMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyKpRouteMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyKpRouteMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyKpRouteMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewKpRouteMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyKpRouteMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyKpRoutePrinterJoinEntity objects.</summary>
	[Serializable]
	public partial class MyKpRoutePrinterJoinEntityFactory : KpRoutePrinterJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyKpRoutePrinterJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyKpRoutePrinterJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyKpRoutePrinterJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewKpRoutePrinterJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyKpRoutePrinterJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyMachineOptionsMasterEntity objects.</summary>
	[Serializable]
	public partial class MyMachineOptionsMasterEntityFactory : MachineOptionsMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyMachineOptionsMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyMachineOptionsMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyMachineOptionsMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewMachineOptionsMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyMachineOptionsMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyMealPeriodMasterEntity objects.</summary>
	[Serializable]
	public partial class MyMealPeriodMasterEntityFactory : MealPeriodMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyMealPeriodMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyMealPeriodMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyMealPeriodMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewMealPeriodMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyMealPeriodMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyMembershipMasterEntity objects.</summary>
	[Serializable]
	public partial class MyMembershipMasterEntityFactory : MembershipMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyMembershipMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyMembershipMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyMembershipMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewMembershipMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyMembershipMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyMembershipProfitCenterJoinEntity objects.</summary>
	[Serializable]
	public partial class MyMembershipProfitCenterJoinEntityFactory : MembershipProfitCenterJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyMembershipProfitCenterJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyMembershipProfitCenterJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyMembershipProfitCenterJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewMembershipProfitCenterJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyMembershipProfitCenterJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyMenuBttnObjJoinEntity objects.</summary>
	[Serializable]
	public partial class MyMenuBttnObjJoinEntityFactory : MenuBttnObjJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyMenuBttnObjJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyMenuBttnObjJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyMenuBttnObjJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewMenuBttnObjJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyMenuBttnObjJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyMenuItemGroupMasterEntity objects.</summary>
	[Serializable]
	public partial class MyMenuItemGroupMasterEntityFactory : MenuItemGroupMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyMenuItemGroupMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyMenuItemGroupMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyMenuItemGroupMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewMenuItemGroupMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyMenuItemGroupMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyMenuItemMasterEntity objects.</summary>
	[Serializable]
	public partial class MyMenuItemMasterEntityFactory : MenuItemMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyMenuItemMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyMenuItemMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyMenuItemMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewMenuItemMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyMenuItemMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyMenuMasterEntity objects.</summary>
	[Serializable]
	public partial class MyMenuMasterEntityFactory : MenuMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyMenuMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyMenuMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyMenuMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewMenuMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyMenuMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyMenuMigrpJoinEntity objects.</summary>
	[Serializable]
	public partial class MyMenuMigrpJoinEntityFactory : MenuMigrpJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyMenuMigrpJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyMenuMigrpJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyMenuMigrpJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewMenuMigrpJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyMenuMigrpJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyMiChoiceGrpJoinEntity objects.</summary>
	[Serializable]
	public partial class MyMiChoiceGrpJoinEntityFactory : MiChoiceGrpJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyMiChoiceGrpJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyMiChoiceGrpJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyMiChoiceGrpJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewMiChoiceGrpJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyMiChoiceGrpJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyMiKpprinterJoinEntity objects.</summary>
	[Serializable]
	public partial class MyMiKpprinterJoinEntityFactory : MiKpprinterJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyMiKpprinterJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyMiKpprinterJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyMiKpprinterJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewMiKpprinterJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyMiKpprinterJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyMiPriceJoinEntity objects.</summary>
	[Serializable]
	public partial class MyMiPriceJoinEntityFactory : MiPriceJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyMiPriceJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyMiPriceJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyMiPriceJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewMiPriceJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyMiPriceJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyMiSkuJoinEntity objects.</summary>
	[Serializable]
	public partial class MyMiSkuJoinEntityFactory : MiSkuJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyMiSkuJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyMiSkuJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyMiSkuJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewMiSkuJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyMiSkuJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyModifierKpprinterJoinEntity objects.</summary>
	[Serializable]
	public partial class MyModifierKpprinterJoinEntityFactory : ModifierKpprinterJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyModifierKpprinterJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyModifierKpprinterJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyModifierKpprinterJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewModifierKpprinterJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyModifierKpprinterJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyModifierMasterEntity objects.</summary>
	[Serializable]
	public partial class MyModifierMasterEntityFactory : ModifierMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyModifierMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyModifierMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyModifierMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewModifierMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyModifierMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyModifierPriceJoinEntity objects.</summary>
	[Serializable]
	public partial class MyModifierPriceJoinEntityFactory : ModifierPriceJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyModifierPriceJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyModifierPriceJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyModifierPriceJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewModifierPriceJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyModifierPriceJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyOptionsMasterEntity objects.</summary>
	[Serializable]
	public partial class MyOptionsMasterEntityFactory : OptionsMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyOptionsMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyOptionsMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyOptionsMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewOptionsMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyOptionsMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyPriceLevelMasterEntity objects.</summary>
	[Serializable]
	public partial class MyPriceLevelMasterEntityFactory : PriceLevelMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyPriceLevelMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyPriceLevelMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyPriceLevelMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPriceLevelMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyPriceLevelMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyProcessMasterEntity objects.</summary>
	[Serializable]
	public partial class MyProcessMasterEntityFactory : ProcessMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyProcessMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyProcessMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyProcessMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewProcessMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyProcessMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyProductClassMasterEntity objects.</summary>
	[Serializable]
	public partial class MyProductClassMasterEntityFactory : ProductClassMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyProductClassMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyProductClassMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyProductClassMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewProductClassMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyProductClassMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyProfitCenterDayPartJoinEntity objects.</summary>
	[Serializable]
	public partial class MyProfitCenterDayPartJoinEntityFactory : ProfitCenterDayPartJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyProfitCenterDayPartJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyProfitCenterDayPartJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyProfitCenterDayPartJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewProfitCenterDayPartJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyProfitCenterDayPartJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyProfitCenterGroupJoinEntity objects.</summary>
	[Serializable]
	public partial class MyProfitCenterGroupJoinEntityFactory : ProfitCenterGroupJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyProfitCenterGroupJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyProfitCenterGroupJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyProfitCenterGroupJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewProfitCenterGroupJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyProfitCenterGroupJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyProfitCenterGroupMasterEntity objects.</summary>
	[Serializable]
	public partial class MyProfitCenterGroupMasterEntityFactory : ProfitCenterGroupMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyProfitCenterGroupMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyProfitCenterGroupMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyProfitCenterGroupMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewProfitCenterGroupMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyProfitCenterGroupMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyProfitCenterMasterEntity objects.</summary>
	[Serializable]
	public partial class MyProfitCenterMasterEntityFactory : ProfitCenterMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyProfitCenterMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyProfitCenterMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyProfitCenterMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewProfitCenterMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyProfitCenterMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyProfitCenterReceiptPrinterJoinEntity objects.</summary>
	[Serializable]
	public partial class MyProfitCenterReceiptPrinterJoinEntityFactory : ProfitCenterReceiptPrinterJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyProfitCenterReceiptPrinterJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyProfitCenterReceiptPrinterJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyProfitCenterReceiptPrinterJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewProfitCenterReceiptPrinterJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyProfitCenterReceiptPrinterJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyProfitCenterTableJoinEntity objects.</summary>
	[Serializable]
	public partial class MyProfitCenterTableJoinEntityFactory : ProfitCenterTableJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyProfitCenterTableJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyProfitCenterTableJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyProfitCenterTableJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewProfitCenterTableJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyProfitCenterTableJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyQuickTenderMasterEntity objects.</summary>
	[Serializable]
	public partial class MyQuickTenderMasterEntityFactory : QuickTenderMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyQuickTenderMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyQuickTenderMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyQuickTenderMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewQuickTenderMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyQuickTenderMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyReceiptPrinterMasterEntity objects.</summary>
	[Serializable]
	public partial class MyReceiptPrinterMasterEntityFactory : ReceiptPrinterMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyReceiptPrinterMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyReceiptPrinterMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyReceiptPrinterMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewReceiptPrinterMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyReceiptPrinterMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyReportCatMasterEntity objects.</summary>
	[Serializable]
	public partial class MyReportCatMasterEntityFactory : ReportCatMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyReportCatMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyReportCatMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyReportCatMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewReportCatMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyReportCatMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyRevenueCatMasterEntity objects.</summary>
	[Serializable]
	public partial class MyRevenueCatMasterEntityFactory : RevenueCatMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyRevenueCatMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyRevenueCatMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyRevenueCatMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRevenueCatMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyRevenueCatMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyRevenueClassMasterEntity objects.</summary>
	[Serializable]
	public partial class MyRevenueClassMasterEntityFactory : RevenueClassMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyRevenueClassMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyRevenueClassMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyRevenueClassMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRevenueClassMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyRevenueClassMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyRowLockMasterEntity objects.</summary>
	[Serializable]
	public partial class MyRowLockMasterEntityFactory : RowLockMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyRowLockMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyRowLockMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyRowLockMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRowLockMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyRowLockMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyScreenTemplateMasterEntity objects.</summary>
	[Serializable]
	public partial class MyScreenTemplateMasterEntityFactory : ScreenTemplateMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyScreenTemplateMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyScreenTemplateMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyScreenTemplateMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewScreenTemplateMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyScreenTemplateMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyScreenTemplateMigrpJoinEntity objects.</summary>
	[Serializable]
	public partial class MyScreenTemplateMigrpJoinEntityFactory : ScreenTemplateMigrpJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyScreenTemplateMigrpJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyScreenTemplateMigrpJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyScreenTemplateMigrpJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewScreenTemplateMigrpJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyScreenTemplateMigrpJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MySecurityLevelMasterEntity objects.</summary>
	[Serializable]
	public partial class MySecurityLevelMasterEntityFactory : SecurityLevelMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MySecurityLevelMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MySecurityLevelMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MySecurityLevelMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSecurityLevelMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MySecurityLevelMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MySelectionGroupItemJoinEntity objects.</summary>
	[Serializable]
	public partial class MySelectionGroupItemJoinEntityFactory : SelectionGroupItemJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MySelectionGroupItemJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MySelectionGroupItemJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MySelectionGroupItemJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSelectionGroupItemJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MySelectionGroupItemJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MySelectionGroupMasterEntity objects.</summary>
	[Serializable]
	public partial class MySelectionGroupMasterEntityFactory : SelectionGroupMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MySelectionGroupMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MySelectionGroupMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MySelectionGroupMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSelectionGroupMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MySelectionGroupMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyServiceManagerProxyMasterEntity objects.</summary>
	[Serializable]
	public partial class MyServiceManagerProxyMasterEntityFactory : ServiceManagerProxyMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyServiceManagerProxyMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyServiceManagerProxyMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyServiceManagerProxyMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewServiceManagerProxyMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyServiceManagerProxyMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MySmuCommandMasterEntity objects.</summary>
	[Serializable]
	public partial class MySmuCommandMasterEntityFactory : SmuCommandMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MySmuCommandMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MySmuCommandMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MySmuCommandMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSmuCommandMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MySmuCommandMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MySmuStatusMasterEntity objects.</summary>
	[Serializable]
	public partial class MySmuStatusMasterEntityFactory : SmuStatusMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MySmuStatusMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MySmuStatusMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MySmuStatusMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSmuStatusMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MySmuStatusMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MySpecialInstrMasterEntity objects.</summary>
	[Serializable]
	public partial class MySpecialInstrMasterEntityFactory : SpecialInstrMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MySpecialInstrMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MySpecialInstrMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MySpecialInstrMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSpecialInstrMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MySpecialInstrMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MySystemConfigurationMasterEntity objects.</summary>
	[Serializable]
	public partial class MySystemConfigurationMasterEntityFactory : SystemConfigurationMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MySystemConfigurationMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MySystemConfigurationMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MySystemConfigurationMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSystemConfigurationMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MySystemConfigurationMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyTableLayoutMasterEntity objects.</summary>
	[Serializable]
	public partial class MyTableLayoutMasterEntityFactory : TableLayoutMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyTableLayoutMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyTableLayoutMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyTableLayoutMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTableLayoutMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyTableLayoutMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyTaxCatMasterEntity objects.</summary>
	[Serializable]
	public partial class MyTaxCatMasterEntityFactory : TaxCatMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyTaxCatMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyTaxCatMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyTaxCatMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTaxCatMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyTaxCatMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyTaxGroupMasterEntity objects.</summary>
	[Serializable]
	public partial class MyTaxGroupMasterEntityFactory : TaxGroupMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyTaxGroupMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyTaxGroupMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyTaxGroupMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTaxGroupMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyTaxGroupMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyTaxGrpTaxJoinEntity objects.</summary>
	[Serializable]
	public partial class MyTaxGrpTaxJoinEntityFactory : TaxGrpTaxJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyTaxGrpTaxJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyTaxGrpTaxJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyTaxGrpTaxJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTaxGrpTaxJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyTaxGrpTaxJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyTaxMasterEntity objects.</summary>
	[Serializable]
	public partial class MyTaxMasterEntityFactory : TaxMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyTaxMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyTaxMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyTaxMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTaxMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyTaxMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyTaxRevCatJoinEntity objects.</summary>
	[Serializable]
	public partial class MyTaxRevCatJoinEntityFactory : TaxRevCatJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyTaxRevCatJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyTaxRevCatJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyTaxRevCatJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTaxRevCatJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyTaxRevCatJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyTenderClassMasterEntity objects.</summary>
	[Serializable]
	public partial class MyTenderClassMasterEntityFactory : TenderClassMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyTenderClassMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyTenderClassMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyTenderClassMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTenderClassMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyTenderClassMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyTenderMasterEntity objects.</summary>
	[Serializable]
	public partial class MyTenderMasterEntityFactory : TenderMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyTenderMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyTenderMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyTenderMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTenderMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyTenderMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyTermGrpChkTypeJoinEntity objects.</summary>
	[Serializable]
	public partial class MyTermGrpChkTypeJoinEntityFactory : TermGrpChkTypeJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyTermGrpChkTypeJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyTermGrpChkTypeJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyTermGrpChkTypeJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTermGrpChkTypeJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyTermGrpChkTypeJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyTermGrpJobcodeJoinEntity objects.</summary>
	[Serializable]
	public partial class MyTermGrpJobcodeJoinEntityFactory : TermGrpJobcodeJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyTermGrpJobcodeJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyTermGrpJobcodeJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyTermGrpJobcodeJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTermGrpJobcodeJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyTermGrpJobcodeJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyTermGrpMasterEntity objects.</summary>
	[Serializable]
	public partial class MyTermGrpMasterEntityFactory : TermGrpMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyTermGrpMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyTermGrpMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyTermGrpMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTermGrpMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyTermGrpMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyTermGrpMenuJoinEntity objects.</summary>
	[Serializable]
	public partial class MyTermGrpMenuJoinEntityFactory : TermGrpMenuJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyTermGrpMenuJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyTermGrpMenuJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyTermGrpMenuJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTermGrpMenuJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyTermGrpMenuJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyTermGrpMenuJoinCookerEntity objects.</summary>
	[Serializable]
	public partial class MyTermGrpMenuJoinCookerEntityFactory : TermGrpMenuJoinCookerEntityFactory
	{
		/// <summary>CTor</summary>
		public MyTermGrpMenuJoinCookerEntityFactory()
		{
		}

		/// <summary>Creates a new MyTermGrpMenuJoinCookerEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyTermGrpMenuJoinCookerEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTermGrpMenuJoinCookerUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyTermGrpMenuJoinCookerEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyTermGrpMpJoinEntity objects.</summary>
	[Serializable]
	public partial class MyTermGrpMpJoinEntityFactory : TermGrpMpJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyTermGrpMpJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyTermGrpMpJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyTermGrpMpJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTermGrpMpJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyTermGrpMpJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyTermGrpMpmenuJoinEntity objects.</summary>
	[Serializable]
	public partial class MyTermGrpMpmenuJoinEntityFactory : TermGrpMpmenuJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyTermGrpMpmenuJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyTermGrpMpmenuJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyTermGrpMpmenuJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTermGrpMpmenuJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyTermGrpMpmenuJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyTermGrpTndrJoinEntity objects.</summary>
	[Serializable]
	public partial class MyTermGrpTndrJoinEntityFactory : TermGrpTndrJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyTermGrpTndrJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyTermGrpTndrJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyTermGrpTndrJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTermGrpTndrJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyTermGrpTndrJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyTermGrpTransitionJoinEntity objects.</summary>
	[Serializable]
	public partial class MyTermGrpTransitionJoinEntityFactory : TermGrpTransitionJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyTermGrpTransitionJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyTermGrpTransitionJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyTermGrpTransitionJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTermGrpTransitionJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyTermGrpTransitionJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyTerminalMasterEntity objects.</summary>
	[Serializable]
	public partial class MyTerminalMasterEntityFactory : TerminalMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyTerminalMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyTerminalMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyTerminalMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTerminalMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyTerminalMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyTerminalProfitCtrJoinEntity objects.</summary>
	[Serializable]
	public partial class MyTerminalProfitCtrJoinEntityFactory : TerminalProfitCtrJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyTerminalProfitCtrJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyTerminalProfitCtrJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyTerminalProfitCtrJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTerminalProfitCtrJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyTerminalProfitCtrJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyTerminalTextGuestInfoJoinEntity objects.</summary>
	[Serializable]
	public partial class MyTerminalTextGuestInfoJoinEntityFactory : TerminalTextGuestInfoJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyTerminalTextGuestInfoJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyTerminalTextGuestInfoJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyTerminalTextGuestInfoJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTerminalTextGuestInfoJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyTerminalTextGuestInfoJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyTerminalTextMasterEntity objects.</summary>
	[Serializable]
	public partial class MyTerminalTextMasterEntityFactory : TerminalTextMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyTerminalTextMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyTerminalTextMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyTerminalTextMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTerminalTextMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyTerminalTextMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyTermOptionGrpMasterEntity objects.</summary>
	[Serializable]
	public partial class MyTermOptionGrpMasterEntityFactory : TermOptionGrpMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyTermOptionGrpMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyTermOptionGrpMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyTermOptionGrpMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTermOptionGrpMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyTermOptionGrpMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyTermOptionGrpOptionJoinEntity objects.</summary>
	[Serializable]
	public partial class MyTermOptionGrpOptionJoinEntityFactory : TermOptionGrpOptionJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyTermOptionGrpOptionJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyTermOptionGrpOptionJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyTermOptionGrpOptionJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTermOptionGrpOptionJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyTermOptionGrpOptionJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyTermPrinterGrpKpprntJoinEntity objects.</summary>
	[Serializable]
	public partial class MyTermPrinterGrpKpprntJoinEntityFactory : TermPrinterGrpKpprntJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyTermPrinterGrpKpprntJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyTermPrinterGrpKpprntJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyTermPrinterGrpKpprntJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTermPrinterGrpKpprntJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyTermPrinterGrpKpprntJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyTermPrinterGrpMasterEntity objects.</summary>
	[Serializable]
	public partial class MyTermPrinterGrpMasterEntityFactory : TermPrinterGrpMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyTermPrinterGrpMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyTermPrinterGrpMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyTermPrinterGrpMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTermPrinterGrpMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyTermPrinterGrpMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyTermPrinterGrpRouteJoinEntity objects.</summary>
	[Serializable]
	public partial class MyTermPrinterGrpRouteJoinEntityFactory : TermPrinterGrpRouteJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyTermPrinterGrpRouteJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyTermPrinterGrpRouteJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyTermPrinterGrpRouteJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTermPrinterGrpRouteJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyTermPrinterGrpRouteJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyThemeColorJoinEntity objects.</summary>
	[Serializable]
	public partial class MyThemeColorJoinEntityFactory : ThemeColorJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyThemeColorJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyThemeColorJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyThemeColorJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewThemeColorJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyThemeColorJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyThemeFontJoinEntity objects.</summary>
	[Serializable]
	public partial class MyThemeFontJoinEntityFactory : ThemeFontJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyThemeFontJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyThemeFontJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyThemeFontJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewThemeFontJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyThemeFontJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyThemeMasterEntity objects.</summary>
	[Serializable]
	public partial class MyThemeMasterEntityFactory : ThemeMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyThemeMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyThemeMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyThemeMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewThemeMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyThemeMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyThemeObjectJoinEntity objects.</summary>
	[Serializable]
	public partial class MyThemeObjectJoinEntityFactory : ThemeObjectJoinEntityFactory
	{
		/// <summary>CTor</summary>
		public MyThemeObjectJoinEntityFactory()
		{
		}

		/// <summary>Creates a new MyThemeObjectJoinEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyThemeObjectJoinEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewThemeObjectJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyThemeObjectJoinEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyUdefDateRangeInstanceEntity objects.</summary>
	[Serializable]
	public partial class MyUdefDateRangeInstanceEntityFactory : UdefDateRangeInstanceEntityFactory
	{
		/// <summary>CTor</summary>
		public MyUdefDateRangeInstanceEntityFactory()
		{
		}

		/// <summary>Creates a new MyUdefDateRangeInstanceEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyUdefDateRangeInstanceEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewUdefDateRangeInstanceUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyUdefDateRangeInstanceEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyUdefPeriodTypeMasterEntity objects.</summary>
	[Serializable]
	public partial class MyUdefPeriodTypeMasterEntityFactory : UdefPeriodTypeMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyUdefPeriodTypeMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyUdefPeriodTypeMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyUdefPeriodTypeMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewUdefPeriodTypeMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyUdefPeriodTypeMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyVoidReasonMasterEntity objects.</summary>
	[Serializable]
	public partial class MyVoidReasonMasterEntityFactory : VoidReasonMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyVoidReasonMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyVoidReasonMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyVoidReasonMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewVoidReasonMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyVoidReasonMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}	
	/// <summary>Factory to create new, empty MyZooMasterEntity objects.</summary>
	[Serializable]
	public partial class MyZooMasterEntityFactory : ZooMasterEntityFactory
	{
		/// <summary>CTor</summary>
		public MyZooMasterEntityFactory()
		{
		}

		/// <summary>Creates a new MyZooMasterEntity instance and will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields)
		{
			IEntity2 toReturn = new MyZooMasterEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewZooMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
				
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<MyZooMasterEntity>(this);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
		}
	}

	/// <summary>Factory to create new, empty Entity objects based on the entity type specified. Uses  entity specific factory objects</summary>
	public partial class GeneralEntityFactory
	{
		/// <summary>Creates a new, empty Entity object of the type specified</summary>
		/// <param name="entityTypeToCreate">The entity type to create.</param>
		/// <returns>A new, empty Entity object.</returns>
		public static IEntity2 CreateMy(InfoGenesis.EntityType entityTypeToCreate)
		{
			IEntityFactory2 factoryToUse = null;
			switch(entityTypeToCreate)
			{
				case InfoGenesis.EntityType.BonusCodeMasterEntity:
					factoryToUse = new MyBonusCodeMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.ButtonAbbrJoinEntity:
					factoryToUse = new MyButtonAbbrJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.CardTypeMasterEntity:
					factoryToUse = new MyCardTypeMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.CardTypeRangeJoinEntity:
					factoryToUse = new MyCardTypeRangeJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.CfgVersionEntity:
					factoryToUse = new MyCfgVersionEntityFactory();
					break;
				case InfoGenesis.EntityType.CheckTypeMasterEntity:
					factoryToUse = new MyCheckTypeMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.ChefMasterEntity:
					factoryToUse = new MyChefMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.ChkTypeGratJoinEntity:
					factoryToUse = new MyChkTypeGratJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.ChkTypeGratRevJoinEntity:
					factoryToUse = new MyChkTypeGratRevJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.ChkTypeGratTaxJoinEntity:
					factoryToUse = new MyChkTypeGratTaxJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.ChkTypeTaxGrpJoinEntity:
					factoryToUse = new MyChkTypeTaxGrpJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.ChkTypeTaxGrpTaxJoinEntity:
					factoryToUse = new MyChkTypeTaxGrpTaxJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.ChkTypeVatGratJoinEntity:
					factoryToUse = new MyChkTypeVatGratJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.ChkTypeVatRevJoinEntity:
					factoryToUse = new MyChkTypeVatRevJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.ChoiceGroupMasterEntity:
					factoryToUse = new MyChoiceGroupMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.ChoiceGrpModJoinEntity:
					factoryToUse = new MyChoiceGrpModJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.ComboItemJoinEntity:
					factoryToUse = new MyComboItemJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.ComboMasterEntity:
					factoryToUse = new MyComboMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.ConfigurationIdMasterEntity:
					factoryToUse = new MyConfigurationIdMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.DeviceMasterEntity:
					factoryToUse = new MyDeviceMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.DiscoupMasterEntity:
					factoryToUse = new MyDiscoupMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.EmpGroupMasterEntity:
					factoryToUse = new MyEmpGroupMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.EmpGrpEmpJoinEntity:
					factoryToUse = new MyEmpGrpEmpJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.EmpJobcodeJoinEntity:
					factoryToUse = new MyEmpJobcodeJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.EmpMasterEntity:
					factoryToUse = new MyEmpMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.FuncBttnTextJoinEntity:
					factoryToUse = new MyFuncBttnTextJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.Ga4680ExportParmMasterEntity:
					factoryToUse = new MyGa4680ExportParmMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.Ga4680ImportParmMasterEntity:
					factoryToUse = new MyGa4680ImportParmMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.GeneralLedgerMapMasterEntity:
					factoryToUse = new MyGeneralLedgerMapMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.GiftCardMasterEntity:
					factoryToUse = new MyGiftCardMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.GratuityCatMasterEntity:
					factoryToUse = new MyGratuityCatMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.GratuityMasterEntity:
					factoryToUse = new MyGratuityMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.IdMasterEntity:
					factoryToUse = new MyIdMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.ImageLibraryMasterEntity:
					factoryToUse = new MyImageLibraryMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.IntlDescriptorTextJoinEntity:
					factoryToUse = new MyIntlDescriptorTextJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.IpServerAttribMasterEntity:
					factoryToUse = new MyIpServerAttribMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.JobcodeFuncBttnJoinEntity:
					factoryToUse = new MyJobcodeFuncBttnJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.JobcodeFunctJoinEntity:
					factoryToUse = new MyJobcodeFunctJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.JobcodeMasterEntity:
					factoryToUse = new MyJobcodeMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.KdsCategoryMasterEntity:
					factoryToUse = new MyKdsCategoryMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.KdsVideoMasterEntity:
					factoryToUse = new MyKdsVideoMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.KpOptionGrpCopyJoinEntity:
					factoryToUse = new MyKpOptionGrpCopyJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.KpOptionGrpFormatJoinEntity:
					factoryToUse = new MyKpOptionGrpFormatJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.KpOptionGrpMasterEntity:
					factoryToUse = new MyKpOptionGrpMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.KpPrinterMasterEntity:
					factoryToUse = new MyKpPrinterMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.KpRouteMasterEntity:
					factoryToUse = new MyKpRouteMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.KpRoutePrinterJoinEntity:
					factoryToUse = new MyKpRoutePrinterJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.MachineOptionsMasterEntity:
					factoryToUse = new MyMachineOptionsMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.MealPeriodMasterEntity:
					factoryToUse = new MyMealPeriodMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.MembershipMasterEntity:
					factoryToUse = new MyMembershipMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.MembershipProfitCenterJoinEntity:
					factoryToUse = new MyMembershipProfitCenterJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.MenuBttnObjJoinEntity:
					factoryToUse = new MyMenuBttnObjJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.MenuItemGroupMasterEntity:
					factoryToUse = new MyMenuItemGroupMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.MenuItemMasterEntity:
					factoryToUse = new MyMenuItemMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.MenuMasterEntity:
					factoryToUse = new MyMenuMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.MenuMigrpJoinEntity:
					factoryToUse = new MyMenuMigrpJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.MiChoiceGrpJoinEntity:
					factoryToUse = new MyMiChoiceGrpJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.MiKpprinterJoinEntity:
					factoryToUse = new MyMiKpprinterJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.MiPriceJoinEntity:
					factoryToUse = new MyMiPriceJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.MiSkuJoinEntity:
					factoryToUse = new MyMiSkuJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.ModifierKpprinterJoinEntity:
					factoryToUse = new MyModifierKpprinterJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.ModifierMasterEntity:
					factoryToUse = new MyModifierMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.ModifierPriceJoinEntity:
					factoryToUse = new MyModifierPriceJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.OptionsMasterEntity:
					factoryToUse = new MyOptionsMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.PriceLevelMasterEntity:
					factoryToUse = new MyPriceLevelMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.ProcessMasterEntity:
					factoryToUse = new MyProcessMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.ProductClassMasterEntity:
					factoryToUse = new MyProductClassMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.ProfitCenterDayPartJoinEntity:
					factoryToUse = new MyProfitCenterDayPartJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.ProfitCenterGroupJoinEntity:
					factoryToUse = new MyProfitCenterGroupJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.ProfitCenterGroupMasterEntity:
					factoryToUse = new MyProfitCenterGroupMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.ProfitCenterMasterEntity:
					factoryToUse = new MyProfitCenterMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.ProfitCenterReceiptPrinterJoinEntity:
					factoryToUse = new MyProfitCenterReceiptPrinterJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.ProfitCenterTableJoinEntity:
					factoryToUse = new MyProfitCenterTableJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.QuickTenderMasterEntity:
					factoryToUse = new MyQuickTenderMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.ReceiptPrinterMasterEntity:
					factoryToUse = new MyReceiptPrinterMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.ReportCatMasterEntity:
					factoryToUse = new MyReportCatMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.RevenueCatMasterEntity:
					factoryToUse = new MyRevenueCatMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.RevenueClassMasterEntity:
					factoryToUse = new MyRevenueClassMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.RowLockMasterEntity:
					factoryToUse = new MyRowLockMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.ScreenTemplateMasterEntity:
					factoryToUse = new MyScreenTemplateMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.ScreenTemplateMigrpJoinEntity:
					factoryToUse = new MyScreenTemplateMigrpJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.SecurityLevelMasterEntity:
					factoryToUse = new MySecurityLevelMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.SelectionGroupItemJoinEntity:
					factoryToUse = new MySelectionGroupItemJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.SelectionGroupMasterEntity:
					factoryToUse = new MySelectionGroupMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.ServiceManagerProxyMasterEntity:
					factoryToUse = new MyServiceManagerProxyMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.SmuCommandMasterEntity:
					factoryToUse = new MySmuCommandMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.SmuStatusMasterEntity:
					factoryToUse = new MySmuStatusMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.SpecialInstrMasterEntity:
					factoryToUse = new MySpecialInstrMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.SystemConfigurationMasterEntity:
					factoryToUse = new MySystemConfigurationMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.TableLayoutMasterEntity:
					factoryToUse = new MyTableLayoutMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.TaxCatMasterEntity:
					factoryToUse = new MyTaxCatMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.TaxGroupMasterEntity:
					factoryToUse = new MyTaxGroupMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.TaxGrpTaxJoinEntity:
					factoryToUse = new MyTaxGrpTaxJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.TaxMasterEntity:
					factoryToUse = new MyTaxMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.TaxRevCatJoinEntity:
					factoryToUse = new MyTaxRevCatJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.TenderClassMasterEntity:
					factoryToUse = new MyTenderClassMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.TenderMasterEntity:
					factoryToUse = new MyTenderMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.TermGrpChkTypeJoinEntity:
					factoryToUse = new MyTermGrpChkTypeJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.TermGrpJobcodeJoinEntity:
					factoryToUse = new MyTermGrpJobcodeJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.TermGrpMasterEntity:
					factoryToUse = new MyTermGrpMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.TermGrpMenuJoinEntity:
					factoryToUse = new MyTermGrpMenuJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.TermGrpMenuJoinCookerEntity:
					factoryToUse = new MyTermGrpMenuJoinCookerEntityFactory();
					break;
				case InfoGenesis.EntityType.TermGrpMpJoinEntity:
					factoryToUse = new MyTermGrpMpJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.TermGrpMpmenuJoinEntity:
					factoryToUse = new MyTermGrpMpmenuJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.TermGrpTndrJoinEntity:
					factoryToUse = new MyTermGrpTndrJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.TermGrpTransitionJoinEntity:
					factoryToUse = new MyTermGrpTransitionJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.TerminalMasterEntity:
					factoryToUse = new MyTerminalMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.TerminalProfitCtrJoinEntity:
					factoryToUse = new MyTerminalProfitCtrJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.TerminalTextGuestInfoJoinEntity:
					factoryToUse = new MyTerminalTextGuestInfoJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.TerminalTextMasterEntity:
					factoryToUse = new MyTerminalTextMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.TermOptionGrpMasterEntity:
					factoryToUse = new MyTermOptionGrpMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.TermOptionGrpOptionJoinEntity:
					factoryToUse = new MyTermOptionGrpOptionJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.TermPrinterGrpKpprntJoinEntity:
					factoryToUse = new MyTermPrinterGrpKpprntJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.TermPrinterGrpMasterEntity:
					factoryToUse = new MyTermPrinterGrpMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.TermPrinterGrpRouteJoinEntity:
					factoryToUse = new MyTermPrinterGrpRouteJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.ThemeColorJoinEntity:
					factoryToUse = new MyThemeColorJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.ThemeFontJoinEntity:
					factoryToUse = new MyThemeFontJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.ThemeMasterEntity:
					factoryToUse = new MyThemeMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.ThemeObjectJoinEntity:
					factoryToUse = new MyThemeObjectJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.UdefDateRangeInstanceEntity:
					factoryToUse = new MyUdefDateRangeInstanceEntityFactory();
					break;
				case InfoGenesis.EntityType.UdefPeriodTypeMasterEntity:
					factoryToUse = new MyUdefPeriodTypeMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.VoidReasonMasterEntity:
					factoryToUse = new MyVoidReasonMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.ZooMasterEntity:
					factoryToUse = new MyZooMasterEntityFactory();
					break;
			}
			IEntity2 toReturn = null;
			if(factoryToUse != null)
			{
				toReturn = factoryToUse.Create();
			}
			return toReturn;
		}
	}
	
	/// <summary>Class which is used to obtain the entity factory based on the .NET type of the entity. </summary>
	[Serializable]
	public static class MyEntityFactoryFactory
	{
#if CF
		/// <summary>Gets the factory of the entity with the InfoGenesis.EntityType specified</summary>
		/// <param name="typeOfEntity">The type of entity.</param>
		/// <returns>factory to use or null if not found</returns>
		public static IEntityFactory2 GetFactory(InfoGenesis.EntityType typeOfEntity)
		{
			return GeneralEntityFactory.CreateMy(typeOfEntity).GetEntityFactory();
		}
#else
		private static Dictionary<Type, IEntityFactory2> _factoryPerType = new Dictionary<Type, IEntityFactory2>();

		/// <summary>Initializes the <see cref="EntityFactoryFactory"/> class.</summary>
		static MyEntityFactoryFactory()
		{
			Array entityTypeValues = Enum.GetValues(typeof(InfoGenesis.EntityType));
			foreach(int entityTypeValue in entityTypeValues)
			{
				IEntity2 dummy = GeneralEntityFactory.CreateMy((InfoGenesis.EntityType)entityTypeValue);
				_factoryPerType.Add(dummy.GetType(), dummy.GetEntityFactory());
			}
		}

		/// <summary>Gets the factory of the entity with the .NET type specified</summary>
		/// <param name="typeOfEntity">The type of entity.</param>
		/// <returns>factory to use or null if not found</returns>
		public static IEntityFactory2 GetFactory(Type typeOfEntity)
		{
			IEntityFactory2 toReturn = null;
			_factoryPerType.TryGetValue(typeOfEntity, out toReturn);
			return toReturn;
		}

		/// <summary>Gets the factory of the entity with the InfoGenesis.EntityType specified</summary>
		/// <param name="typeOfEntity">The type of entity.</param>
		/// <returns>factory to use or null if not found</returns>
		public static IEntityFactory2 GetFactory(InfoGenesis.EntityType typeOfEntity)
		{
			return GetFactory(GeneralEntityFactory.CreateMy(typeOfEntity).GetType());
		}
#endif		
	}
	
	/// <summary>Element creator for creating project elements from somewhere else, like inside Linq providers.</summary>
	public class MyElementCreator : ElementCreator
	{
		/// <summary>Implementation of the routine which gets the factory of the Entity type with the InfoGenesis.EntityType value passed in</summary>
		/// <param name="entityTypeValue">The entity type value.</param>
		/// <returns>the entity factory of the entity type or null if not found</returns>
		protected override IEntityFactoryCore GetFactoryImpl(int entityTypeValue)
		{
			return MyEntityFactoryFactory.GetFactory((InfoGenesis.EntityType)entityTypeValue);
		}
#if !CF		
		/// <summary>Implementation of the routine which gets the factory of the Entity type with the .NET type passed in</summary>
		/// <param name="typeOfEntity">The type of entity.</param>
		/// <returns>the entity factory of the entity type or null if not found</returns>
		protected override IEntityFactoryCore GetFactoryImpl(Type typeOfEntity)
		{
			return MyEntityFactoryFactory.GetFactory(typeOfEntity);
		}
#endif
	}

}
