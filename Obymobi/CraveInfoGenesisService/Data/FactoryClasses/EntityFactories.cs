﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using InfoGenesis.EntityClasses;
using InfoGenesis.HelperClasses;
using InfoGenesis.RelationClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace InfoGenesis.FactoryClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	
	
	/// <summary>general base class for the generated factories</summary>
	[Serializable]
	public partial class EntityFactoryBase2<TEntity> : EntityFactoryCore2
		where TEntity : EntityBase2, IEntity2
	{
		private readonly InfoGenesis.EntityType _typeOfEntity;
		private readonly bool _isInHierarchy;
		
		/// <summary>CTor</summary>
		/// <param name="entityName">Name of the entity.</param>
		/// <param name="typeOfEntity">The type of entity.</param>
		/// <param name="isInHierarchy">If true, the entity of this factory is in an inheritance hierarchy, false otherwise</param>
		public EntityFactoryBase2(string entityName, InfoGenesis.EntityType typeOfEntity, bool isInHierarchy) : base(entityName)
		{
			_typeOfEntity = typeOfEntity;
			_isInHierarchy = isInHierarchy;
		}
		
		/// <summary>Creates, using the generated EntityFieldsFactory, the IEntityFields2 object for the entity to create.</summary>
		/// <returns>Empty IEntityFields2 object.</returns>
		public override IEntityFields2 CreateFields()
		{
			return EntityFieldsFactory.CreateEntityFieldsObject(_typeOfEntity);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.Create((InfoGenesis.EntityType)entityTypeValue);
		}

		/// <summary>Creates the relations collection to the entity to join all targets so this entity can be fetched. </summary>
		/// <param name="objectAlias">The object alias to use for the elements in the relations.</param>
		/// <returns>null if the entity isn't in a hierarchy of type TargetPerEntity, otherwise the relations collection needed to join all targets together to fetch all subtypes of this entity and this entity itself</returns>
		public override IRelationCollection CreateHierarchyRelations(string objectAlias) 
		{
			return InheritanceInfoProviderSingleton.GetInstance().GetHierarchyRelations(this.ForEntityName, objectAlias);
		}

		/// <summary>This method retrieves, using the InheritanceInfoprovider, the factory for the entity represented by the values passed in.</summary>
		/// <param name="fieldValues">Field values read from the db, to determine which factory to return, based on the field values passed in.</param>
		/// <param name="entityFieldStartIndexesPerEntity">indexes into values where per entity type their own fields start.</param>
		/// <returns>the factory for the entity which is represented by the values passed in.</returns>
		public override IEntityFactory2 GetEntityFactory(object[] fieldValues, Dictionary<string, int> entityFieldStartIndexesPerEntity) 
		{
			IEntityFactory2 toReturn = (IEntityFactory2)InheritanceInfoProviderSingleton.GetInstance().GetEntityFactory(this.ForEntityName, fieldValues, entityFieldStartIndexesPerEntity);
			if(toReturn == null)
			{
				toReturn = this;
			}
			return toReturn;
		}
		
		/// <summary>Gets a predicateexpression which filters on the entity with type belonging to this factory.</summary>
		/// <param name="negate">Flag to produce a NOT filter, (true), or a normal filter (false). </param>
		/// <param name="objectAlias">The object alias to use for the predicate(s).</param>
		/// <returns>ready to use predicateexpression, or an empty predicate expression if the belonging entity isn't a hierarchical type.</returns>
		public override IPredicateExpression GetEntityTypeFilter(bool negate, string objectAlias) 
		{
			return InheritanceInfoProviderSingleton.GetInstance().GetEntityTypeFilter(this.ForEntityName, objectAlias, negate);
		}
						
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity which this factory belongs to.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<TEntity>(this);
		}
		
		/// <summary>Creates the hierarchy fields for the entity to which this factory belongs.</summary>
		/// <returns>IEntityFields2 object with the fields of all the entities in teh hierarchy of this entity or the fields of this entity if the entity isn't in a hierarchy.</returns>
		public override IEntityFields2 CreateHierarchyFields() 
		{
			return _isInHierarchy ? new EntityFields2(InheritanceInfoProviderSingleton.GetInstance().GetHierarchyFields(this.ForEntityName), InheritanceInfoProviderSingleton.GetInstance(), null) : base.CreateHierarchyFields();
		}
	}

	/// <summary>Factory to create new, empty BonusCodeMasterEntity objects.</summary>
	[Serializable]
	public partial class BonusCodeMasterEntityFactory : EntityFactoryBase2<BonusCodeMasterEntity> {
		/// <summary>CTor</summary>
		public BonusCodeMasterEntityFactory() : base("BonusCodeMasterEntity", InfoGenesis.EntityType.BonusCodeMasterEntity, false) { }
		
		/// <summary>Creates a new BonusCodeMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new BonusCodeMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewBonusCodeMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty ButtonAbbrJoinEntity objects.</summary>
	[Serializable]
	public partial class ButtonAbbrJoinEntityFactory : EntityFactoryBase2<ButtonAbbrJoinEntity> {
		/// <summary>CTor</summary>
		public ButtonAbbrJoinEntityFactory() : base("ButtonAbbrJoinEntity", InfoGenesis.EntityType.ButtonAbbrJoinEntity, false) { }
		
		/// <summary>Creates a new ButtonAbbrJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new ButtonAbbrJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewButtonAbbrJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty CardTypeMasterEntity objects.</summary>
	[Serializable]
	public partial class CardTypeMasterEntityFactory : EntityFactoryBase2<CardTypeMasterEntity> {
		/// <summary>CTor</summary>
		public CardTypeMasterEntityFactory() : base("CardTypeMasterEntity", InfoGenesis.EntityType.CardTypeMasterEntity, false) { }
		
		/// <summary>Creates a new CardTypeMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new CardTypeMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCardTypeMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty CardTypeRangeJoinEntity objects.</summary>
	[Serializable]
	public partial class CardTypeRangeJoinEntityFactory : EntityFactoryBase2<CardTypeRangeJoinEntity> {
		/// <summary>CTor</summary>
		public CardTypeRangeJoinEntityFactory() : base("CardTypeRangeJoinEntity", InfoGenesis.EntityType.CardTypeRangeJoinEntity, false) { }
		
		/// <summary>Creates a new CardTypeRangeJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new CardTypeRangeJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCardTypeRangeJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty CfgVersionEntity objects.</summary>
	[Serializable]
	public partial class CfgVersionEntityFactory : EntityFactoryBase2<CfgVersionEntity> {
		/// <summary>CTor</summary>
		public CfgVersionEntityFactory() : base("CfgVersionEntity", InfoGenesis.EntityType.CfgVersionEntity, false) { }
		
		/// <summary>Creates a new CfgVersionEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new CfgVersionEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCfgVersionUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty CheckTypeMasterEntity objects.</summary>
	[Serializable]
	public partial class CheckTypeMasterEntityFactory : EntityFactoryBase2<CheckTypeMasterEntity> {
		/// <summary>CTor</summary>
		public CheckTypeMasterEntityFactory() : base("CheckTypeMasterEntity", InfoGenesis.EntityType.CheckTypeMasterEntity, false) { }
		
		/// <summary>Creates a new CheckTypeMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new CheckTypeMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCheckTypeMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty ChefMasterEntity objects.</summary>
	[Serializable]
	public partial class ChefMasterEntityFactory : EntityFactoryBase2<ChefMasterEntity> {
		/// <summary>CTor</summary>
		public ChefMasterEntityFactory() : base("ChefMasterEntity", InfoGenesis.EntityType.ChefMasterEntity, false) { }
		
		/// <summary>Creates a new ChefMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new ChefMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewChefMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty ChkTypeGratJoinEntity objects.</summary>
	[Serializable]
	public partial class ChkTypeGratJoinEntityFactory : EntityFactoryBase2<ChkTypeGratJoinEntity> {
		/// <summary>CTor</summary>
		public ChkTypeGratJoinEntityFactory() : base("ChkTypeGratJoinEntity", InfoGenesis.EntityType.ChkTypeGratJoinEntity, false) { }
		
		/// <summary>Creates a new ChkTypeGratJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new ChkTypeGratJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewChkTypeGratJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty ChkTypeGratRevJoinEntity objects.</summary>
	[Serializable]
	public partial class ChkTypeGratRevJoinEntityFactory : EntityFactoryBase2<ChkTypeGratRevJoinEntity> {
		/// <summary>CTor</summary>
		public ChkTypeGratRevJoinEntityFactory() : base("ChkTypeGratRevJoinEntity", InfoGenesis.EntityType.ChkTypeGratRevJoinEntity, false) { }
		
		/// <summary>Creates a new ChkTypeGratRevJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new ChkTypeGratRevJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewChkTypeGratRevJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty ChkTypeGratTaxJoinEntity objects.</summary>
	[Serializable]
	public partial class ChkTypeGratTaxJoinEntityFactory : EntityFactoryBase2<ChkTypeGratTaxJoinEntity> {
		/// <summary>CTor</summary>
		public ChkTypeGratTaxJoinEntityFactory() : base("ChkTypeGratTaxJoinEntity", InfoGenesis.EntityType.ChkTypeGratTaxJoinEntity, false) { }
		
		/// <summary>Creates a new ChkTypeGratTaxJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new ChkTypeGratTaxJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewChkTypeGratTaxJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty ChkTypeTaxGrpJoinEntity objects.</summary>
	[Serializable]
	public partial class ChkTypeTaxGrpJoinEntityFactory : EntityFactoryBase2<ChkTypeTaxGrpJoinEntity> {
		/// <summary>CTor</summary>
		public ChkTypeTaxGrpJoinEntityFactory() : base("ChkTypeTaxGrpJoinEntity", InfoGenesis.EntityType.ChkTypeTaxGrpJoinEntity, false) { }
		
		/// <summary>Creates a new ChkTypeTaxGrpJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new ChkTypeTaxGrpJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewChkTypeTaxGrpJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty ChkTypeTaxGrpTaxJoinEntity objects.</summary>
	[Serializable]
	public partial class ChkTypeTaxGrpTaxJoinEntityFactory : EntityFactoryBase2<ChkTypeTaxGrpTaxJoinEntity> {
		/// <summary>CTor</summary>
		public ChkTypeTaxGrpTaxJoinEntityFactory() : base("ChkTypeTaxGrpTaxJoinEntity", InfoGenesis.EntityType.ChkTypeTaxGrpTaxJoinEntity, false) { }
		
		/// <summary>Creates a new ChkTypeTaxGrpTaxJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new ChkTypeTaxGrpTaxJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewChkTypeTaxGrpTaxJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty ChkTypeVatGratJoinEntity objects.</summary>
	[Serializable]
	public partial class ChkTypeVatGratJoinEntityFactory : EntityFactoryBase2<ChkTypeVatGratJoinEntity> {
		/// <summary>CTor</summary>
		public ChkTypeVatGratJoinEntityFactory() : base("ChkTypeVatGratJoinEntity", InfoGenesis.EntityType.ChkTypeVatGratJoinEntity, false) { }
		
		/// <summary>Creates a new ChkTypeVatGratJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new ChkTypeVatGratJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewChkTypeVatGratJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty ChkTypeVatRevJoinEntity objects.</summary>
	[Serializable]
	public partial class ChkTypeVatRevJoinEntityFactory : EntityFactoryBase2<ChkTypeVatRevJoinEntity> {
		/// <summary>CTor</summary>
		public ChkTypeVatRevJoinEntityFactory() : base("ChkTypeVatRevJoinEntity", InfoGenesis.EntityType.ChkTypeVatRevJoinEntity, false) { }
		
		/// <summary>Creates a new ChkTypeVatRevJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new ChkTypeVatRevJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewChkTypeVatRevJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty ChoiceGroupMasterEntity objects.</summary>
	[Serializable]
	public partial class ChoiceGroupMasterEntityFactory : EntityFactoryBase2<ChoiceGroupMasterEntity> {
		/// <summary>CTor</summary>
		public ChoiceGroupMasterEntityFactory() : base("ChoiceGroupMasterEntity", InfoGenesis.EntityType.ChoiceGroupMasterEntity, false) { }
		
		/// <summary>Creates a new ChoiceGroupMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new ChoiceGroupMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewChoiceGroupMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty ChoiceGrpModJoinEntity objects.</summary>
	[Serializable]
	public partial class ChoiceGrpModJoinEntityFactory : EntityFactoryBase2<ChoiceGrpModJoinEntity> {
		/// <summary>CTor</summary>
		public ChoiceGrpModJoinEntityFactory() : base("ChoiceGrpModJoinEntity", InfoGenesis.EntityType.ChoiceGrpModJoinEntity, false) { }
		
		/// <summary>Creates a new ChoiceGrpModJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new ChoiceGrpModJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewChoiceGrpModJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty ComboItemJoinEntity objects.</summary>
	[Serializable]
	public partial class ComboItemJoinEntityFactory : EntityFactoryBase2<ComboItemJoinEntity> {
		/// <summary>CTor</summary>
		public ComboItemJoinEntityFactory() : base("ComboItemJoinEntity", InfoGenesis.EntityType.ComboItemJoinEntity, false) { }
		
		/// <summary>Creates a new ComboItemJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new ComboItemJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewComboItemJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty ComboMasterEntity objects.</summary>
	[Serializable]
	public partial class ComboMasterEntityFactory : EntityFactoryBase2<ComboMasterEntity> {
		/// <summary>CTor</summary>
		public ComboMasterEntityFactory() : base("ComboMasterEntity", InfoGenesis.EntityType.ComboMasterEntity, false) { }
		
		/// <summary>Creates a new ComboMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new ComboMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewComboMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty ConfigurationIdMasterEntity objects.</summary>
	[Serializable]
	public partial class ConfigurationIdMasterEntityFactory : EntityFactoryBase2<ConfigurationIdMasterEntity> {
		/// <summary>CTor</summary>
		public ConfigurationIdMasterEntityFactory() : base("ConfigurationIdMasterEntity", InfoGenesis.EntityType.ConfigurationIdMasterEntity, false) { }
		
		/// <summary>Creates a new ConfigurationIdMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new ConfigurationIdMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewConfigurationIdMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty DeviceMasterEntity objects.</summary>
	[Serializable]
	public partial class DeviceMasterEntityFactory : EntityFactoryBase2<DeviceMasterEntity> {
		/// <summary>CTor</summary>
		public DeviceMasterEntityFactory() : base("DeviceMasterEntity", InfoGenesis.EntityType.DeviceMasterEntity, false) { }
		
		/// <summary>Creates a new DeviceMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new DeviceMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDeviceMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty DiscoupMasterEntity objects.</summary>
	[Serializable]
	public partial class DiscoupMasterEntityFactory : EntityFactoryBase2<DiscoupMasterEntity> {
		/// <summary>CTor</summary>
		public DiscoupMasterEntityFactory() : base("DiscoupMasterEntity", InfoGenesis.EntityType.DiscoupMasterEntity, false) { }
		
		/// <summary>Creates a new DiscoupMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new DiscoupMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDiscoupMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty EmpGroupMasterEntity objects.</summary>
	[Serializable]
	public partial class EmpGroupMasterEntityFactory : EntityFactoryBase2<EmpGroupMasterEntity> {
		/// <summary>CTor</summary>
		public EmpGroupMasterEntityFactory() : base("EmpGroupMasterEntity", InfoGenesis.EntityType.EmpGroupMasterEntity, false) { }
		
		/// <summary>Creates a new EmpGroupMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new EmpGroupMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewEmpGroupMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty EmpGrpEmpJoinEntity objects.</summary>
	[Serializable]
	public partial class EmpGrpEmpJoinEntityFactory : EntityFactoryBase2<EmpGrpEmpJoinEntity> {
		/// <summary>CTor</summary>
		public EmpGrpEmpJoinEntityFactory() : base("EmpGrpEmpJoinEntity", InfoGenesis.EntityType.EmpGrpEmpJoinEntity, false) { }
		
		/// <summary>Creates a new EmpGrpEmpJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new EmpGrpEmpJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewEmpGrpEmpJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty EmpJobcodeJoinEntity objects.</summary>
	[Serializable]
	public partial class EmpJobcodeJoinEntityFactory : EntityFactoryBase2<EmpJobcodeJoinEntity> {
		/// <summary>CTor</summary>
		public EmpJobcodeJoinEntityFactory() : base("EmpJobcodeJoinEntity", InfoGenesis.EntityType.EmpJobcodeJoinEntity, false) { }
		
		/// <summary>Creates a new EmpJobcodeJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new EmpJobcodeJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewEmpJobcodeJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty EmpMasterEntity objects.</summary>
	[Serializable]
	public partial class EmpMasterEntityFactory : EntityFactoryBase2<EmpMasterEntity> {
		/// <summary>CTor</summary>
		public EmpMasterEntityFactory() : base("EmpMasterEntity", InfoGenesis.EntityType.EmpMasterEntity, false) { }
		
		/// <summary>Creates a new EmpMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new EmpMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewEmpMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty FuncBttnTextJoinEntity objects.</summary>
	[Serializable]
	public partial class FuncBttnTextJoinEntityFactory : EntityFactoryBase2<FuncBttnTextJoinEntity> {
		/// <summary>CTor</summary>
		public FuncBttnTextJoinEntityFactory() : base("FuncBttnTextJoinEntity", InfoGenesis.EntityType.FuncBttnTextJoinEntity, false) { }
		
		/// <summary>Creates a new FuncBttnTextJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new FuncBttnTextJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewFuncBttnTextJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty Ga4680ExportParmMasterEntity objects.</summary>
	[Serializable]
	public partial class Ga4680ExportParmMasterEntityFactory : EntityFactoryBase2<Ga4680ExportParmMasterEntity> {
		/// <summary>CTor</summary>
		public Ga4680ExportParmMasterEntityFactory() : base("Ga4680ExportParmMasterEntity", InfoGenesis.EntityType.Ga4680ExportParmMasterEntity, false) { }
		
		/// <summary>Creates a new Ga4680ExportParmMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new Ga4680ExportParmMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewGa4680ExportParmMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty Ga4680ImportParmMasterEntity objects.</summary>
	[Serializable]
	public partial class Ga4680ImportParmMasterEntityFactory : EntityFactoryBase2<Ga4680ImportParmMasterEntity> {
		/// <summary>CTor</summary>
		public Ga4680ImportParmMasterEntityFactory() : base("Ga4680ImportParmMasterEntity", InfoGenesis.EntityType.Ga4680ImportParmMasterEntity, false) { }
		
		/// <summary>Creates a new Ga4680ImportParmMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new Ga4680ImportParmMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewGa4680ImportParmMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty GeneralLedgerMapMasterEntity objects.</summary>
	[Serializable]
	public partial class GeneralLedgerMapMasterEntityFactory : EntityFactoryBase2<GeneralLedgerMapMasterEntity> {
		/// <summary>CTor</summary>
		public GeneralLedgerMapMasterEntityFactory() : base("GeneralLedgerMapMasterEntity", InfoGenesis.EntityType.GeneralLedgerMapMasterEntity, false) { }
		
		/// <summary>Creates a new GeneralLedgerMapMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new GeneralLedgerMapMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewGeneralLedgerMapMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty GiftCardMasterEntity objects.</summary>
	[Serializable]
	public partial class GiftCardMasterEntityFactory : EntityFactoryBase2<GiftCardMasterEntity> {
		/// <summary>CTor</summary>
		public GiftCardMasterEntityFactory() : base("GiftCardMasterEntity", InfoGenesis.EntityType.GiftCardMasterEntity, false) { }
		
		/// <summary>Creates a new GiftCardMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new GiftCardMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewGiftCardMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty GratuityCatMasterEntity objects.</summary>
	[Serializable]
	public partial class GratuityCatMasterEntityFactory : EntityFactoryBase2<GratuityCatMasterEntity> {
		/// <summary>CTor</summary>
		public GratuityCatMasterEntityFactory() : base("GratuityCatMasterEntity", InfoGenesis.EntityType.GratuityCatMasterEntity, false) { }
		
		/// <summary>Creates a new GratuityCatMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new GratuityCatMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewGratuityCatMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty GratuityMasterEntity objects.</summary>
	[Serializable]
	public partial class GratuityMasterEntityFactory : EntityFactoryBase2<GratuityMasterEntity> {
		/// <summary>CTor</summary>
		public GratuityMasterEntityFactory() : base("GratuityMasterEntity", InfoGenesis.EntityType.GratuityMasterEntity, false) { }
		
		/// <summary>Creates a new GratuityMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new GratuityMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewGratuityMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty IdMasterEntity objects.</summary>
	[Serializable]
	public partial class IdMasterEntityFactory : EntityFactoryBase2<IdMasterEntity> {
		/// <summary>CTor</summary>
		public IdMasterEntityFactory() : base("IdMasterEntity", InfoGenesis.EntityType.IdMasterEntity, false) { }
		
		/// <summary>Creates a new IdMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new IdMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewIdMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty ImageLibraryMasterEntity objects.</summary>
	[Serializable]
	public partial class ImageLibraryMasterEntityFactory : EntityFactoryBase2<ImageLibraryMasterEntity> {
		/// <summary>CTor</summary>
		public ImageLibraryMasterEntityFactory() : base("ImageLibraryMasterEntity", InfoGenesis.EntityType.ImageLibraryMasterEntity, false) { }
		
		/// <summary>Creates a new ImageLibraryMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new ImageLibraryMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewImageLibraryMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty IntlDescriptorTextJoinEntity objects.</summary>
	[Serializable]
	public partial class IntlDescriptorTextJoinEntityFactory : EntityFactoryBase2<IntlDescriptorTextJoinEntity> {
		/// <summary>CTor</summary>
		public IntlDescriptorTextJoinEntityFactory() : base("IntlDescriptorTextJoinEntity", InfoGenesis.EntityType.IntlDescriptorTextJoinEntity, false) { }
		
		/// <summary>Creates a new IntlDescriptorTextJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new IntlDescriptorTextJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewIntlDescriptorTextJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty IpServerAttribMasterEntity objects.</summary>
	[Serializable]
	public partial class IpServerAttribMasterEntityFactory : EntityFactoryBase2<IpServerAttribMasterEntity> {
		/// <summary>CTor</summary>
		public IpServerAttribMasterEntityFactory() : base("IpServerAttribMasterEntity", InfoGenesis.EntityType.IpServerAttribMasterEntity, false) { }
		
		/// <summary>Creates a new IpServerAttribMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new IpServerAttribMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewIpServerAttribMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty JobcodeFuncBttnJoinEntity objects.</summary>
	[Serializable]
	public partial class JobcodeFuncBttnJoinEntityFactory : EntityFactoryBase2<JobcodeFuncBttnJoinEntity> {
		/// <summary>CTor</summary>
		public JobcodeFuncBttnJoinEntityFactory() : base("JobcodeFuncBttnJoinEntity", InfoGenesis.EntityType.JobcodeFuncBttnJoinEntity, false) { }
		
		/// <summary>Creates a new JobcodeFuncBttnJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new JobcodeFuncBttnJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewJobcodeFuncBttnJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty JobcodeFunctJoinEntity objects.</summary>
	[Serializable]
	public partial class JobcodeFunctJoinEntityFactory : EntityFactoryBase2<JobcodeFunctJoinEntity> {
		/// <summary>CTor</summary>
		public JobcodeFunctJoinEntityFactory() : base("JobcodeFunctJoinEntity", InfoGenesis.EntityType.JobcodeFunctJoinEntity, false) { }
		
		/// <summary>Creates a new JobcodeFunctJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new JobcodeFunctJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewJobcodeFunctJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty JobcodeMasterEntity objects.</summary>
	[Serializable]
	public partial class JobcodeMasterEntityFactory : EntityFactoryBase2<JobcodeMasterEntity> {
		/// <summary>CTor</summary>
		public JobcodeMasterEntityFactory() : base("JobcodeMasterEntity", InfoGenesis.EntityType.JobcodeMasterEntity, false) { }
		
		/// <summary>Creates a new JobcodeMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new JobcodeMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewJobcodeMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty KdsCategoryMasterEntity objects.</summary>
	[Serializable]
	public partial class KdsCategoryMasterEntityFactory : EntityFactoryBase2<KdsCategoryMasterEntity> {
		/// <summary>CTor</summary>
		public KdsCategoryMasterEntityFactory() : base("KdsCategoryMasterEntity", InfoGenesis.EntityType.KdsCategoryMasterEntity, false) { }
		
		/// <summary>Creates a new KdsCategoryMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new KdsCategoryMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewKdsCategoryMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty KdsVideoMasterEntity objects.</summary>
	[Serializable]
	public partial class KdsVideoMasterEntityFactory : EntityFactoryBase2<KdsVideoMasterEntity> {
		/// <summary>CTor</summary>
		public KdsVideoMasterEntityFactory() : base("KdsVideoMasterEntity", InfoGenesis.EntityType.KdsVideoMasterEntity, false) { }
		
		/// <summary>Creates a new KdsVideoMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new KdsVideoMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewKdsVideoMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty KpOptionGrpCopyJoinEntity objects.</summary>
	[Serializable]
	public partial class KpOptionGrpCopyJoinEntityFactory : EntityFactoryBase2<KpOptionGrpCopyJoinEntity> {
		/// <summary>CTor</summary>
		public KpOptionGrpCopyJoinEntityFactory() : base("KpOptionGrpCopyJoinEntity", InfoGenesis.EntityType.KpOptionGrpCopyJoinEntity, false) { }
		
		/// <summary>Creates a new KpOptionGrpCopyJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new KpOptionGrpCopyJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewKpOptionGrpCopyJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty KpOptionGrpFormatJoinEntity objects.</summary>
	[Serializable]
	public partial class KpOptionGrpFormatJoinEntityFactory : EntityFactoryBase2<KpOptionGrpFormatJoinEntity> {
		/// <summary>CTor</summary>
		public KpOptionGrpFormatJoinEntityFactory() : base("KpOptionGrpFormatJoinEntity", InfoGenesis.EntityType.KpOptionGrpFormatJoinEntity, false) { }
		
		/// <summary>Creates a new KpOptionGrpFormatJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new KpOptionGrpFormatJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewKpOptionGrpFormatJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty KpOptionGrpMasterEntity objects.</summary>
	[Serializable]
	public partial class KpOptionGrpMasterEntityFactory : EntityFactoryBase2<KpOptionGrpMasterEntity> {
		/// <summary>CTor</summary>
		public KpOptionGrpMasterEntityFactory() : base("KpOptionGrpMasterEntity", InfoGenesis.EntityType.KpOptionGrpMasterEntity, false) { }
		
		/// <summary>Creates a new KpOptionGrpMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new KpOptionGrpMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewKpOptionGrpMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty KpPrinterMasterEntity objects.</summary>
	[Serializable]
	public partial class KpPrinterMasterEntityFactory : EntityFactoryBase2<KpPrinterMasterEntity> {
		/// <summary>CTor</summary>
		public KpPrinterMasterEntityFactory() : base("KpPrinterMasterEntity", InfoGenesis.EntityType.KpPrinterMasterEntity, false) { }
		
		/// <summary>Creates a new KpPrinterMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new KpPrinterMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewKpPrinterMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty KpRouteMasterEntity objects.</summary>
	[Serializable]
	public partial class KpRouteMasterEntityFactory : EntityFactoryBase2<KpRouteMasterEntity> {
		/// <summary>CTor</summary>
		public KpRouteMasterEntityFactory() : base("KpRouteMasterEntity", InfoGenesis.EntityType.KpRouteMasterEntity, false) { }
		
		/// <summary>Creates a new KpRouteMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new KpRouteMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewKpRouteMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty KpRoutePrinterJoinEntity objects.</summary>
	[Serializable]
	public partial class KpRoutePrinterJoinEntityFactory : EntityFactoryBase2<KpRoutePrinterJoinEntity> {
		/// <summary>CTor</summary>
		public KpRoutePrinterJoinEntityFactory() : base("KpRoutePrinterJoinEntity", InfoGenesis.EntityType.KpRoutePrinterJoinEntity, false) { }
		
		/// <summary>Creates a new KpRoutePrinterJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new KpRoutePrinterJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewKpRoutePrinterJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty MachineOptionsMasterEntity objects.</summary>
	[Serializable]
	public partial class MachineOptionsMasterEntityFactory : EntityFactoryBase2<MachineOptionsMasterEntity> {
		/// <summary>CTor</summary>
		public MachineOptionsMasterEntityFactory() : base("MachineOptionsMasterEntity", InfoGenesis.EntityType.MachineOptionsMasterEntity, false) { }
		
		/// <summary>Creates a new MachineOptionsMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new MachineOptionsMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewMachineOptionsMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty MealPeriodMasterEntity objects.</summary>
	[Serializable]
	public partial class MealPeriodMasterEntityFactory : EntityFactoryBase2<MealPeriodMasterEntity> {
		/// <summary>CTor</summary>
		public MealPeriodMasterEntityFactory() : base("MealPeriodMasterEntity", InfoGenesis.EntityType.MealPeriodMasterEntity, false) { }
		
		/// <summary>Creates a new MealPeriodMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new MealPeriodMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewMealPeriodMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty MembershipMasterEntity objects.</summary>
	[Serializable]
	public partial class MembershipMasterEntityFactory : EntityFactoryBase2<MembershipMasterEntity> {
		/// <summary>CTor</summary>
		public MembershipMasterEntityFactory() : base("MembershipMasterEntity", InfoGenesis.EntityType.MembershipMasterEntity, false) { }
		
		/// <summary>Creates a new MembershipMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new MembershipMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewMembershipMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty MembershipProfitCenterJoinEntity objects.</summary>
	[Serializable]
	public partial class MembershipProfitCenterJoinEntityFactory : EntityFactoryBase2<MembershipProfitCenterJoinEntity> {
		/// <summary>CTor</summary>
		public MembershipProfitCenterJoinEntityFactory() : base("MembershipProfitCenterJoinEntity", InfoGenesis.EntityType.MembershipProfitCenterJoinEntity, false) { }
		
		/// <summary>Creates a new MembershipProfitCenterJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new MembershipProfitCenterJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewMembershipProfitCenterJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty MenuBttnObjJoinEntity objects.</summary>
	[Serializable]
	public partial class MenuBttnObjJoinEntityFactory : EntityFactoryBase2<MenuBttnObjJoinEntity> {
		/// <summary>CTor</summary>
		public MenuBttnObjJoinEntityFactory() : base("MenuBttnObjJoinEntity", InfoGenesis.EntityType.MenuBttnObjJoinEntity, false) { }
		
		/// <summary>Creates a new MenuBttnObjJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new MenuBttnObjJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewMenuBttnObjJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty MenuItemGroupMasterEntity objects.</summary>
	[Serializable]
	public partial class MenuItemGroupMasterEntityFactory : EntityFactoryBase2<MenuItemGroupMasterEntity> {
		/// <summary>CTor</summary>
		public MenuItemGroupMasterEntityFactory() : base("MenuItemGroupMasterEntity", InfoGenesis.EntityType.MenuItemGroupMasterEntity, false) { }
		
		/// <summary>Creates a new MenuItemGroupMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new MenuItemGroupMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewMenuItemGroupMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty MenuItemMasterEntity objects.</summary>
	[Serializable]
	public partial class MenuItemMasterEntityFactory : EntityFactoryBase2<MenuItemMasterEntity> {
		/// <summary>CTor</summary>
		public MenuItemMasterEntityFactory() : base("MenuItemMasterEntity", InfoGenesis.EntityType.MenuItemMasterEntity, false) { }
		
		/// <summary>Creates a new MenuItemMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new MenuItemMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewMenuItemMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty MenuMasterEntity objects.</summary>
	[Serializable]
	public partial class MenuMasterEntityFactory : EntityFactoryBase2<MenuMasterEntity> {
		/// <summary>CTor</summary>
		public MenuMasterEntityFactory() : base("MenuMasterEntity", InfoGenesis.EntityType.MenuMasterEntity, false) { }
		
		/// <summary>Creates a new MenuMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new MenuMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewMenuMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty MenuMigrpJoinEntity objects.</summary>
	[Serializable]
	public partial class MenuMigrpJoinEntityFactory : EntityFactoryBase2<MenuMigrpJoinEntity> {
		/// <summary>CTor</summary>
		public MenuMigrpJoinEntityFactory() : base("MenuMigrpJoinEntity", InfoGenesis.EntityType.MenuMigrpJoinEntity, false) { }
		
		/// <summary>Creates a new MenuMigrpJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new MenuMigrpJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewMenuMigrpJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty MiChoiceGrpJoinEntity objects.</summary>
	[Serializable]
	public partial class MiChoiceGrpJoinEntityFactory : EntityFactoryBase2<MiChoiceGrpJoinEntity> {
		/// <summary>CTor</summary>
		public MiChoiceGrpJoinEntityFactory() : base("MiChoiceGrpJoinEntity", InfoGenesis.EntityType.MiChoiceGrpJoinEntity, false) { }
		
		/// <summary>Creates a new MiChoiceGrpJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new MiChoiceGrpJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewMiChoiceGrpJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty MiKpprinterJoinEntity objects.</summary>
	[Serializable]
	public partial class MiKpprinterJoinEntityFactory : EntityFactoryBase2<MiKpprinterJoinEntity> {
		/// <summary>CTor</summary>
		public MiKpprinterJoinEntityFactory() : base("MiKpprinterJoinEntity", InfoGenesis.EntityType.MiKpprinterJoinEntity, false) { }
		
		/// <summary>Creates a new MiKpprinterJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new MiKpprinterJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewMiKpprinterJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty MiPriceJoinEntity objects.</summary>
	[Serializable]
	public partial class MiPriceJoinEntityFactory : EntityFactoryBase2<MiPriceJoinEntity> {
		/// <summary>CTor</summary>
		public MiPriceJoinEntityFactory() : base("MiPriceJoinEntity", InfoGenesis.EntityType.MiPriceJoinEntity, false) { }
		
		/// <summary>Creates a new MiPriceJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new MiPriceJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewMiPriceJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty MiSkuJoinEntity objects.</summary>
	[Serializable]
	public partial class MiSkuJoinEntityFactory : EntityFactoryBase2<MiSkuJoinEntity> {
		/// <summary>CTor</summary>
		public MiSkuJoinEntityFactory() : base("MiSkuJoinEntity", InfoGenesis.EntityType.MiSkuJoinEntity, false) { }
		
		/// <summary>Creates a new MiSkuJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new MiSkuJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewMiSkuJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty ModifierKpprinterJoinEntity objects.</summary>
	[Serializable]
	public partial class ModifierKpprinterJoinEntityFactory : EntityFactoryBase2<ModifierKpprinterJoinEntity> {
		/// <summary>CTor</summary>
		public ModifierKpprinterJoinEntityFactory() : base("ModifierKpprinterJoinEntity", InfoGenesis.EntityType.ModifierKpprinterJoinEntity, false) { }
		
		/// <summary>Creates a new ModifierKpprinterJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new ModifierKpprinterJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewModifierKpprinterJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty ModifierMasterEntity objects.</summary>
	[Serializable]
	public partial class ModifierMasterEntityFactory : EntityFactoryBase2<ModifierMasterEntity> {
		/// <summary>CTor</summary>
		public ModifierMasterEntityFactory() : base("ModifierMasterEntity", InfoGenesis.EntityType.ModifierMasterEntity, false) { }
		
		/// <summary>Creates a new ModifierMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new ModifierMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewModifierMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty ModifierPriceJoinEntity objects.</summary>
	[Serializable]
	public partial class ModifierPriceJoinEntityFactory : EntityFactoryBase2<ModifierPriceJoinEntity> {
		/// <summary>CTor</summary>
		public ModifierPriceJoinEntityFactory() : base("ModifierPriceJoinEntity", InfoGenesis.EntityType.ModifierPriceJoinEntity, false) { }
		
		/// <summary>Creates a new ModifierPriceJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new ModifierPriceJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewModifierPriceJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty OptionsMasterEntity objects.</summary>
	[Serializable]
	public partial class OptionsMasterEntityFactory : EntityFactoryBase2<OptionsMasterEntity> {
		/// <summary>CTor</summary>
		public OptionsMasterEntityFactory() : base("OptionsMasterEntity", InfoGenesis.EntityType.OptionsMasterEntity, false) { }
		
		/// <summary>Creates a new OptionsMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new OptionsMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewOptionsMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty PriceLevelMasterEntity objects.</summary>
	[Serializable]
	public partial class PriceLevelMasterEntityFactory : EntityFactoryBase2<PriceLevelMasterEntity> {
		/// <summary>CTor</summary>
		public PriceLevelMasterEntityFactory() : base("PriceLevelMasterEntity", InfoGenesis.EntityType.PriceLevelMasterEntity, false) { }
		
		/// <summary>Creates a new PriceLevelMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new PriceLevelMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPriceLevelMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty ProcessMasterEntity objects.</summary>
	[Serializable]
	public partial class ProcessMasterEntityFactory : EntityFactoryBase2<ProcessMasterEntity> {
		/// <summary>CTor</summary>
		public ProcessMasterEntityFactory() : base("ProcessMasterEntity", InfoGenesis.EntityType.ProcessMasterEntity, false) { }
		
		/// <summary>Creates a new ProcessMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new ProcessMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewProcessMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty ProductClassMasterEntity objects.</summary>
	[Serializable]
	public partial class ProductClassMasterEntityFactory : EntityFactoryBase2<ProductClassMasterEntity> {
		/// <summary>CTor</summary>
		public ProductClassMasterEntityFactory() : base("ProductClassMasterEntity", InfoGenesis.EntityType.ProductClassMasterEntity, false) { }
		
		/// <summary>Creates a new ProductClassMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new ProductClassMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewProductClassMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty ProfitCenterDayPartJoinEntity objects.</summary>
	[Serializable]
	public partial class ProfitCenterDayPartJoinEntityFactory : EntityFactoryBase2<ProfitCenterDayPartJoinEntity> {
		/// <summary>CTor</summary>
		public ProfitCenterDayPartJoinEntityFactory() : base("ProfitCenterDayPartJoinEntity", InfoGenesis.EntityType.ProfitCenterDayPartJoinEntity, false) { }
		
		/// <summary>Creates a new ProfitCenterDayPartJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new ProfitCenterDayPartJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewProfitCenterDayPartJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty ProfitCenterGroupJoinEntity objects.</summary>
	[Serializable]
	public partial class ProfitCenterGroupJoinEntityFactory : EntityFactoryBase2<ProfitCenterGroupJoinEntity> {
		/// <summary>CTor</summary>
		public ProfitCenterGroupJoinEntityFactory() : base("ProfitCenterGroupJoinEntity", InfoGenesis.EntityType.ProfitCenterGroupJoinEntity, false) { }
		
		/// <summary>Creates a new ProfitCenterGroupJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new ProfitCenterGroupJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewProfitCenterGroupJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty ProfitCenterGroupMasterEntity objects.</summary>
	[Serializable]
	public partial class ProfitCenterGroupMasterEntityFactory : EntityFactoryBase2<ProfitCenterGroupMasterEntity> {
		/// <summary>CTor</summary>
		public ProfitCenterGroupMasterEntityFactory() : base("ProfitCenterGroupMasterEntity", InfoGenesis.EntityType.ProfitCenterGroupMasterEntity, false) { }
		
		/// <summary>Creates a new ProfitCenterGroupMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new ProfitCenterGroupMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewProfitCenterGroupMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty ProfitCenterMasterEntity objects.</summary>
	[Serializable]
	public partial class ProfitCenterMasterEntityFactory : EntityFactoryBase2<ProfitCenterMasterEntity> {
		/// <summary>CTor</summary>
		public ProfitCenterMasterEntityFactory() : base("ProfitCenterMasterEntity", InfoGenesis.EntityType.ProfitCenterMasterEntity, false) { }
		
		/// <summary>Creates a new ProfitCenterMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new ProfitCenterMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewProfitCenterMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty ProfitCenterReceiptPrinterJoinEntity objects.</summary>
	[Serializable]
	public partial class ProfitCenterReceiptPrinterJoinEntityFactory : EntityFactoryBase2<ProfitCenterReceiptPrinterJoinEntity> {
		/// <summary>CTor</summary>
		public ProfitCenterReceiptPrinterJoinEntityFactory() : base("ProfitCenterReceiptPrinterJoinEntity", InfoGenesis.EntityType.ProfitCenterReceiptPrinterJoinEntity, false) { }
		
		/// <summary>Creates a new ProfitCenterReceiptPrinterJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new ProfitCenterReceiptPrinterJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewProfitCenterReceiptPrinterJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty ProfitCenterTableJoinEntity objects.</summary>
	[Serializable]
	public partial class ProfitCenterTableJoinEntityFactory : EntityFactoryBase2<ProfitCenterTableJoinEntity> {
		/// <summary>CTor</summary>
		public ProfitCenterTableJoinEntityFactory() : base("ProfitCenterTableJoinEntity", InfoGenesis.EntityType.ProfitCenterTableJoinEntity, false) { }
		
		/// <summary>Creates a new ProfitCenterTableJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new ProfitCenterTableJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewProfitCenterTableJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty QuickTenderMasterEntity objects.</summary>
	[Serializable]
	public partial class QuickTenderMasterEntityFactory : EntityFactoryBase2<QuickTenderMasterEntity> {
		/// <summary>CTor</summary>
		public QuickTenderMasterEntityFactory() : base("QuickTenderMasterEntity", InfoGenesis.EntityType.QuickTenderMasterEntity, false) { }
		
		/// <summary>Creates a new QuickTenderMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new QuickTenderMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewQuickTenderMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty ReceiptPrinterMasterEntity objects.</summary>
	[Serializable]
	public partial class ReceiptPrinterMasterEntityFactory : EntityFactoryBase2<ReceiptPrinterMasterEntity> {
		/// <summary>CTor</summary>
		public ReceiptPrinterMasterEntityFactory() : base("ReceiptPrinterMasterEntity", InfoGenesis.EntityType.ReceiptPrinterMasterEntity, false) { }
		
		/// <summary>Creates a new ReceiptPrinterMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new ReceiptPrinterMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewReceiptPrinterMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty ReportCatMasterEntity objects.</summary>
	[Serializable]
	public partial class ReportCatMasterEntityFactory : EntityFactoryBase2<ReportCatMasterEntity> {
		/// <summary>CTor</summary>
		public ReportCatMasterEntityFactory() : base("ReportCatMasterEntity", InfoGenesis.EntityType.ReportCatMasterEntity, false) { }
		
		/// <summary>Creates a new ReportCatMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new ReportCatMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewReportCatMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty RevenueCatMasterEntity objects.</summary>
	[Serializable]
	public partial class RevenueCatMasterEntityFactory : EntityFactoryBase2<RevenueCatMasterEntity> {
		/// <summary>CTor</summary>
		public RevenueCatMasterEntityFactory() : base("RevenueCatMasterEntity", InfoGenesis.EntityType.RevenueCatMasterEntity, false) { }
		
		/// <summary>Creates a new RevenueCatMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new RevenueCatMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRevenueCatMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty RevenueClassMasterEntity objects.</summary>
	[Serializable]
	public partial class RevenueClassMasterEntityFactory : EntityFactoryBase2<RevenueClassMasterEntity> {
		/// <summary>CTor</summary>
		public RevenueClassMasterEntityFactory() : base("RevenueClassMasterEntity", InfoGenesis.EntityType.RevenueClassMasterEntity, false) { }
		
		/// <summary>Creates a new RevenueClassMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new RevenueClassMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRevenueClassMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty RowLockMasterEntity objects.</summary>
	[Serializable]
	public partial class RowLockMasterEntityFactory : EntityFactoryBase2<RowLockMasterEntity> {
		/// <summary>CTor</summary>
		public RowLockMasterEntityFactory() : base("RowLockMasterEntity", InfoGenesis.EntityType.RowLockMasterEntity, false) { }
		
		/// <summary>Creates a new RowLockMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new RowLockMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRowLockMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty ScreenTemplateMasterEntity objects.</summary>
	[Serializable]
	public partial class ScreenTemplateMasterEntityFactory : EntityFactoryBase2<ScreenTemplateMasterEntity> {
		/// <summary>CTor</summary>
		public ScreenTemplateMasterEntityFactory() : base("ScreenTemplateMasterEntity", InfoGenesis.EntityType.ScreenTemplateMasterEntity, false) { }
		
		/// <summary>Creates a new ScreenTemplateMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new ScreenTemplateMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewScreenTemplateMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty ScreenTemplateMigrpJoinEntity objects.</summary>
	[Serializable]
	public partial class ScreenTemplateMigrpJoinEntityFactory : EntityFactoryBase2<ScreenTemplateMigrpJoinEntity> {
		/// <summary>CTor</summary>
		public ScreenTemplateMigrpJoinEntityFactory() : base("ScreenTemplateMigrpJoinEntity", InfoGenesis.EntityType.ScreenTemplateMigrpJoinEntity, false) { }
		
		/// <summary>Creates a new ScreenTemplateMigrpJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new ScreenTemplateMigrpJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewScreenTemplateMigrpJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty SecurityLevelMasterEntity objects.</summary>
	[Serializable]
	public partial class SecurityLevelMasterEntityFactory : EntityFactoryBase2<SecurityLevelMasterEntity> {
		/// <summary>CTor</summary>
		public SecurityLevelMasterEntityFactory() : base("SecurityLevelMasterEntity", InfoGenesis.EntityType.SecurityLevelMasterEntity, false) { }
		
		/// <summary>Creates a new SecurityLevelMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new SecurityLevelMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSecurityLevelMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty SelectionGroupItemJoinEntity objects.</summary>
	[Serializable]
	public partial class SelectionGroupItemJoinEntityFactory : EntityFactoryBase2<SelectionGroupItemJoinEntity> {
		/// <summary>CTor</summary>
		public SelectionGroupItemJoinEntityFactory() : base("SelectionGroupItemJoinEntity", InfoGenesis.EntityType.SelectionGroupItemJoinEntity, false) { }
		
		/// <summary>Creates a new SelectionGroupItemJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new SelectionGroupItemJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSelectionGroupItemJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty SelectionGroupMasterEntity objects.</summary>
	[Serializable]
	public partial class SelectionGroupMasterEntityFactory : EntityFactoryBase2<SelectionGroupMasterEntity> {
		/// <summary>CTor</summary>
		public SelectionGroupMasterEntityFactory() : base("SelectionGroupMasterEntity", InfoGenesis.EntityType.SelectionGroupMasterEntity, false) { }
		
		/// <summary>Creates a new SelectionGroupMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new SelectionGroupMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSelectionGroupMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty ServiceManagerProxyMasterEntity objects.</summary>
	[Serializable]
	public partial class ServiceManagerProxyMasterEntityFactory : EntityFactoryBase2<ServiceManagerProxyMasterEntity> {
		/// <summary>CTor</summary>
		public ServiceManagerProxyMasterEntityFactory() : base("ServiceManagerProxyMasterEntity", InfoGenesis.EntityType.ServiceManagerProxyMasterEntity, false) { }
		
		/// <summary>Creates a new ServiceManagerProxyMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new ServiceManagerProxyMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewServiceManagerProxyMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty SmuCommandMasterEntity objects.</summary>
	[Serializable]
	public partial class SmuCommandMasterEntityFactory : EntityFactoryBase2<SmuCommandMasterEntity> {
		/// <summary>CTor</summary>
		public SmuCommandMasterEntityFactory() : base("SmuCommandMasterEntity", InfoGenesis.EntityType.SmuCommandMasterEntity, false) { }
		
		/// <summary>Creates a new SmuCommandMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new SmuCommandMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSmuCommandMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty SmuStatusMasterEntity objects.</summary>
	[Serializable]
	public partial class SmuStatusMasterEntityFactory : EntityFactoryBase2<SmuStatusMasterEntity> {
		/// <summary>CTor</summary>
		public SmuStatusMasterEntityFactory() : base("SmuStatusMasterEntity", InfoGenesis.EntityType.SmuStatusMasterEntity, false) { }
		
		/// <summary>Creates a new SmuStatusMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new SmuStatusMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSmuStatusMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty SpecialInstrMasterEntity objects.</summary>
	[Serializable]
	public partial class SpecialInstrMasterEntityFactory : EntityFactoryBase2<SpecialInstrMasterEntity> {
		/// <summary>CTor</summary>
		public SpecialInstrMasterEntityFactory() : base("SpecialInstrMasterEntity", InfoGenesis.EntityType.SpecialInstrMasterEntity, false) { }
		
		/// <summary>Creates a new SpecialInstrMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new SpecialInstrMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSpecialInstrMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty SystemConfigurationMasterEntity objects.</summary>
	[Serializable]
	public partial class SystemConfigurationMasterEntityFactory : EntityFactoryBase2<SystemConfigurationMasterEntity> {
		/// <summary>CTor</summary>
		public SystemConfigurationMasterEntityFactory() : base("SystemConfigurationMasterEntity", InfoGenesis.EntityType.SystemConfigurationMasterEntity, false) { }
		
		/// <summary>Creates a new SystemConfigurationMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new SystemConfigurationMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSystemConfigurationMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty TableLayoutMasterEntity objects.</summary>
	[Serializable]
	public partial class TableLayoutMasterEntityFactory : EntityFactoryBase2<TableLayoutMasterEntity> {
		/// <summary>CTor</summary>
		public TableLayoutMasterEntityFactory() : base("TableLayoutMasterEntity", InfoGenesis.EntityType.TableLayoutMasterEntity, false) { }
		
		/// <summary>Creates a new TableLayoutMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new TableLayoutMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTableLayoutMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty TaxCatMasterEntity objects.</summary>
	[Serializable]
	public partial class TaxCatMasterEntityFactory : EntityFactoryBase2<TaxCatMasterEntity> {
		/// <summary>CTor</summary>
		public TaxCatMasterEntityFactory() : base("TaxCatMasterEntity", InfoGenesis.EntityType.TaxCatMasterEntity, false) { }
		
		/// <summary>Creates a new TaxCatMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new TaxCatMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTaxCatMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty TaxGroupMasterEntity objects.</summary>
	[Serializable]
	public partial class TaxGroupMasterEntityFactory : EntityFactoryBase2<TaxGroupMasterEntity> {
		/// <summary>CTor</summary>
		public TaxGroupMasterEntityFactory() : base("TaxGroupMasterEntity", InfoGenesis.EntityType.TaxGroupMasterEntity, false) { }
		
		/// <summary>Creates a new TaxGroupMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new TaxGroupMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTaxGroupMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty TaxGrpTaxJoinEntity objects.</summary>
	[Serializable]
	public partial class TaxGrpTaxJoinEntityFactory : EntityFactoryBase2<TaxGrpTaxJoinEntity> {
		/// <summary>CTor</summary>
		public TaxGrpTaxJoinEntityFactory() : base("TaxGrpTaxJoinEntity", InfoGenesis.EntityType.TaxGrpTaxJoinEntity, false) { }
		
		/// <summary>Creates a new TaxGrpTaxJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new TaxGrpTaxJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTaxGrpTaxJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty TaxMasterEntity objects.</summary>
	[Serializable]
	public partial class TaxMasterEntityFactory : EntityFactoryBase2<TaxMasterEntity> {
		/// <summary>CTor</summary>
		public TaxMasterEntityFactory() : base("TaxMasterEntity", InfoGenesis.EntityType.TaxMasterEntity, false) { }
		
		/// <summary>Creates a new TaxMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new TaxMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTaxMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty TaxRevCatJoinEntity objects.</summary>
	[Serializable]
	public partial class TaxRevCatJoinEntityFactory : EntityFactoryBase2<TaxRevCatJoinEntity> {
		/// <summary>CTor</summary>
		public TaxRevCatJoinEntityFactory() : base("TaxRevCatJoinEntity", InfoGenesis.EntityType.TaxRevCatJoinEntity, false) { }
		
		/// <summary>Creates a new TaxRevCatJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new TaxRevCatJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTaxRevCatJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty TenderClassMasterEntity objects.</summary>
	[Serializable]
	public partial class TenderClassMasterEntityFactory : EntityFactoryBase2<TenderClassMasterEntity> {
		/// <summary>CTor</summary>
		public TenderClassMasterEntityFactory() : base("TenderClassMasterEntity", InfoGenesis.EntityType.TenderClassMasterEntity, false) { }
		
		/// <summary>Creates a new TenderClassMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new TenderClassMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTenderClassMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty TenderMasterEntity objects.</summary>
	[Serializable]
	public partial class TenderMasterEntityFactory : EntityFactoryBase2<TenderMasterEntity> {
		/// <summary>CTor</summary>
		public TenderMasterEntityFactory() : base("TenderMasterEntity", InfoGenesis.EntityType.TenderMasterEntity, false) { }
		
		/// <summary>Creates a new TenderMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new TenderMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTenderMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty TermGrpChkTypeJoinEntity objects.</summary>
	[Serializable]
	public partial class TermGrpChkTypeJoinEntityFactory : EntityFactoryBase2<TermGrpChkTypeJoinEntity> {
		/// <summary>CTor</summary>
		public TermGrpChkTypeJoinEntityFactory() : base("TermGrpChkTypeJoinEntity", InfoGenesis.EntityType.TermGrpChkTypeJoinEntity, false) { }
		
		/// <summary>Creates a new TermGrpChkTypeJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new TermGrpChkTypeJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTermGrpChkTypeJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty TermGrpJobcodeJoinEntity objects.</summary>
	[Serializable]
	public partial class TermGrpJobcodeJoinEntityFactory : EntityFactoryBase2<TermGrpJobcodeJoinEntity> {
		/// <summary>CTor</summary>
		public TermGrpJobcodeJoinEntityFactory() : base("TermGrpJobcodeJoinEntity", InfoGenesis.EntityType.TermGrpJobcodeJoinEntity, false) { }
		
		/// <summary>Creates a new TermGrpJobcodeJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new TermGrpJobcodeJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTermGrpJobcodeJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty TermGrpMasterEntity objects.</summary>
	[Serializable]
	public partial class TermGrpMasterEntityFactory : EntityFactoryBase2<TermGrpMasterEntity> {
		/// <summary>CTor</summary>
		public TermGrpMasterEntityFactory() : base("TermGrpMasterEntity", InfoGenesis.EntityType.TermGrpMasterEntity, false) { }
		
		/// <summary>Creates a new TermGrpMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new TermGrpMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTermGrpMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty TermGrpMenuJoinEntity objects.</summary>
	[Serializable]
	public partial class TermGrpMenuJoinEntityFactory : EntityFactoryBase2<TermGrpMenuJoinEntity> {
		/// <summary>CTor</summary>
		public TermGrpMenuJoinEntityFactory() : base("TermGrpMenuJoinEntity", InfoGenesis.EntityType.TermGrpMenuJoinEntity, false) { }
		
		/// <summary>Creates a new TermGrpMenuJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new TermGrpMenuJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTermGrpMenuJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty TermGrpMenuJoinCookerEntity objects.</summary>
	[Serializable]
	public partial class TermGrpMenuJoinCookerEntityFactory : EntityFactoryBase2<TermGrpMenuJoinCookerEntity> {
		/// <summary>CTor</summary>
		public TermGrpMenuJoinCookerEntityFactory() : base("TermGrpMenuJoinCookerEntity", InfoGenesis.EntityType.TermGrpMenuJoinCookerEntity, false) { }
		
		/// <summary>Creates a new TermGrpMenuJoinCookerEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new TermGrpMenuJoinCookerEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTermGrpMenuJoinCookerUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty TermGrpMpJoinEntity objects.</summary>
	[Serializable]
	public partial class TermGrpMpJoinEntityFactory : EntityFactoryBase2<TermGrpMpJoinEntity> {
		/// <summary>CTor</summary>
		public TermGrpMpJoinEntityFactory() : base("TermGrpMpJoinEntity", InfoGenesis.EntityType.TermGrpMpJoinEntity, false) { }
		
		/// <summary>Creates a new TermGrpMpJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new TermGrpMpJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTermGrpMpJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty TermGrpMpmenuJoinEntity objects.</summary>
	[Serializable]
	public partial class TermGrpMpmenuJoinEntityFactory : EntityFactoryBase2<TermGrpMpmenuJoinEntity> {
		/// <summary>CTor</summary>
		public TermGrpMpmenuJoinEntityFactory() : base("TermGrpMpmenuJoinEntity", InfoGenesis.EntityType.TermGrpMpmenuJoinEntity, false) { }
		
		/// <summary>Creates a new TermGrpMpmenuJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new TermGrpMpmenuJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTermGrpMpmenuJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty TermGrpTndrJoinEntity objects.</summary>
	[Serializable]
	public partial class TermGrpTndrJoinEntityFactory : EntityFactoryBase2<TermGrpTndrJoinEntity> {
		/// <summary>CTor</summary>
		public TermGrpTndrJoinEntityFactory() : base("TermGrpTndrJoinEntity", InfoGenesis.EntityType.TermGrpTndrJoinEntity, false) { }
		
		/// <summary>Creates a new TermGrpTndrJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new TermGrpTndrJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTermGrpTndrJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty TermGrpTransitionJoinEntity objects.</summary>
	[Serializable]
	public partial class TermGrpTransitionJoinEntityFactory : EntityFactoryBase2<TermGrpTransitionJoinEntity> {
		/// <summary>CTor</summary>
		public TermGrpTransitionJoinEntityFactory() : base("TermGrpTransitionJoinEntity", InfoGenesis.EntityType.TermGrpTransitionJoinEntity, false) { }
		
		/// <summary>Creates a new TermGrpTransitionJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new TermGrpTransitionJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTermGrpTransitionJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty TerminalMasterEntity objects.</summary>
	[Serializable]
	public partial class TerminalMasterEntityFactory : EntityFactoryBase2<TerminalMasterEntity> {
		/// <summary>CTor</summary>
		public TerminalMasterEntityFactory() : base("TerminalMasterEntity", InfoGenesis.EntityType.TerminalMasterEntity, false) { }
		
		/// <summary>Creates a new TerminalMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new TerminalMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTerminalMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty TerminalProfitCtrJoinEntity objects.</summary>
	[Serializable]
	public partial class TerminalProfitCtrJoinEntityFactory : EntityFactoryBase2<TerminalProfitCtrJoinEntity> {
		/// <summary>CTor</summary>
		public TerminalProfitCtrJoinEntityFactory() : base("TerminalProfitCtrJoinEntity", InfoGenesis.EntityType.TerminalProfitCtrJoinEntity, false) { }
		
		/// <summary>Creates a new TerminalProfitCtrJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new TerminalProfitCtrJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTerminalProfitCtrJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty TerminalTextGuestInfoJoinEntity objects.</summary>
	[Serializable]
	public partial class TerminalTextGuestInfoJoinEntityFactory : EntityFactoryBase2<TerminalTextGuestInfoJoinEntity> {
		/// <summary>CTor</summary>
		public TerminalTextGuestInfoJoinEntityFactory() : base("TerminalTextGuestInfoJoinEntity", InfoGenesis.EntityType.TerminalTextGuestInfoJoinEntity, false) { }
		
		/// <summary>Creates a new TerminalTextGuestInfoJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new TerminalTextGuestInfoJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTerminalTextGuestInfoJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty TerminalTextMasterEntity objects.</summary>
	[Serializable]
	public partial class TerminalTextMasterEntityFactory : EntityFactoryBase2<TerminalTextMasterEntity> {
		/// <summary>CTor</summary>
		public TerminalTextMasterEntityFactory() : base("TerminalTextMasterEntity", InfoGenesis.EntityType.TerminalTextMasterEntity, false) { }
		
		/// <summary>Creates a new TerminalTextMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new TerminalTextMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTerminalTextMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty TermOptionGrpMasterEntity objects.</summary>
	[Serializable]
	public partial class TermOptionGrpMasterEntityFactory : EntityFactoryBase2<TermOptionGrpMasterEntity> {
		/// <summary>CTor</summary>
		public TermOptionGrpMasterEntityFactory() : base("TermOptionGrpMasterEntity", InfoGenesis.EntityType.TermOptionGrpMasterEntity, false) { }
		
		/// <summary>Creates a new TermOptionGrpMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new TermOptionGrpMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTermOptionGrpMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty TermOptionGrpOptionJoinEntity objects.</summary>
	[Serializable]
	public partial class TermOptionGrpOptionJoinEntityFactory : EntityFactoryBase2<TermOptionGrpOptionJoinEntity> {
		/// <summary>CTor</summary>
		public TermOptionGrpOptionJoinEntityFactory() : base("TermOptionGrpOptionJoinEntity", InfoGenesis.EntityType.TermOptionGrpOptionJoinEntity, false) { }
		
		/// <summary>Creates a new TermOptionGrpOptionJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new TermOptionGrpOptionJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTermOptionGrpOptionJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty TermPrinterGrpKpprntJoinEntity objects.</summary>
	[Serializable]
	public partial class TermPrinterGrpKpprntJoinEntityFactory : EntityFactoryBase2<TermPrinterGrpKpprntJoinEntity> {
		/// <summary>CTor</summary>
		public TermPrinterGrpKpprntJoinEntityFactory() : base("TermPrinterGrpKpprntJoinEntity", InfoGenesis.EntityType.TermPrinterGrpKpprntJoinEntity, false) { }
		
		/// <summary>Creates a new TermPrinterGrpKpprntJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new TermPrinterGrpKpprntJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTermPrinterGrpKpprntJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty TermPrinterGrpMasterEntity objects.</summary>
	[Serializable]
	public partial class TermPrinterGrpMasterEntityFactory : EntityFactoryBase2<TermPrinterGrpMasterEntity> {
		/// <summary>CTor</summary>
		public TermPrinterGrpMasterEntityFactory() : base("TermPrinterGrpMasterEntity", InfoGenesis.EntityType.TermPrinterGrpMasterEntity, false) { }
		
		/// <summary>Creates a new TermPrinterGrpMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new TermPrinterGrpMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTermPrinterGrpMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty TermPrinterGrpRouteJoinEntity objects.</summary>
	[Serializable]
	public partial class TermPrinterGrpRouteJoinEntityFactory : EntityFactoryBase2<TermPrinterGrpRouteJoinEntity> {
		/// <summary>CTor</summary>
		public TermPrinterGrpRouteJoinEntityFactory() : base("TermPrinterGrpRouteJoinEntity", InfoGenesis.EntityType.TermPrinterGrpRouteJoinEntity, false) { }
		
		/// <summary>Creates a new TermPrinterGrpRouteJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new TermPrinterGrpRouteJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTermPrinterGrpRouteJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty ThemeColorJoinEntity objects.</summary>
	[Serializable]
	public partial class ThemeColorJoinEntityFactory : EntityFactoryBase2<ThemeColorJoinEntity> {
		/// <summary>CTor</summary>
		public ThemeColorJoinEntityFactory() : base("ThemeColorJoinEntity", InfoGenesis.EntityType.ThemeColorJoinEntity, false) { }
		
		/// <summary>Creates a new ThemeColorJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new ThemeColorJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewThemeColorJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty ThemeFontJoinEntity objects.</summary>
	[Serializable]
	public partial class ThemeFontJoinEntityFactory : EntityFactoryBase2<ThemeFontJoinEntity> {
		/// <summary>CTor</summary>
		public ThemeFontJoinEntityFactory() : base("ThemeFontJoinEntity", InfoGenesis.EntityType.ThemeFontJoinEntity, false) { }
		
		/// <summary>Creates a new ThemeFontJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new ThemeFontJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewThemeFontJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty ThemeMasterEntity objects.</summary>
	[Serializable]
	public partial class ThemeMasterEntityFactory : EntityFactoryBase2<ThemeMasterEntity> {
		/// <summary>CTor</summary>
		public ThemeMasterEntityFactory() : base("ThemeMasterEntity", InfoGenesis.EntityType.ThemeMasterEntity, false) { }
		
		/// <summary>Creates a new ThemeMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new ThemeMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewThemeMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty ThemeObjectJoinEntity objects.</summary>
	[Serializable]
	public partial class ThemeObjectJoinEntityFactory : EntityFactoryBase2<ThemeObjectJoinEntity> {
		/// <summary>CTor</summary>
		public ThemeObjectJoinEntityFactory() : base("ThemeObjectJoinEntity", InfoGenesis.EntityType.ThemeObjectJoinEntity, false) { }
		
		/// <summary>Creates a new ThemeObjectJoinEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new ThemeObjectJoinEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewThemeObjectJoinUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty UdefDateRangeInstanceEntity objects.</summary>
	[Serializable]
	public partial class UdefDateRangeInstanceEntityFactory : EntityFactoryBase2<UdefDateRangeInstanceEntity> {
		/// <summary>CTor</summary>
		public UdefDateRangeInstanceEntityFactory() : base("UdefDateRangeInstanceEntity", InfoGenesis.EntityType.UdefDateRangeInstanceEntity, false) { }
		
		/// <summary>Creates a new UdefDateRangeInstanceEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new UdefDateRangeInstanceEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewUdefDateRangeInstanceUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty UdefPeriodTypeMasterEntity objects.</summary>
	[Serializable]
	public partial class UdefPeriodTypeMasterEntityFactory : EntityFactoryBase2<UdefPeriodTypeMasterEntity> {
		/// <summary>CTor</summary>
		public UdefPeriodTypeMasterEntityFactory() : base("UdefPeriodTypeMasterEntity", InfoGenesis.EntityType.UdefPeriodTypeMasterEntity, false) { }
		
		/// <summary>Creates a new UdefPeriodTypeMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new UdefPeriodTypeMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewUdefPeriodTypeMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty VoidReasonMasterEntity objects.</summary>
	[Serializable]
	public partial class VoidReasonMasterEntityFactory : EntityFactoryBase2<VoidReasonMasterEntity> {
		/// <summary>CTor</summary>
		public VoidReasonMasterEntityFactory() : base("VoidReasonMasterEntity", InfoGenesis.EntityType.VoidReasonMasterEntity, false) { }
		
		/// <summary>Creates a new VoidReasonMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new VoidReasonMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewVoidReasonMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty ZooMasterEntity objects.</summary>
	[Serializable]
	public partial class ZooMasterEntityFactory : EntityFactoryBase2<ZooMasterEntity> {
		/// <summary>CTor</summary>
		public ZooMasterEntityFactory() : base("ZooMasterEntity", InfoGenesis.EntityType.ZooMasterEntity, false) { }
		
		/// <summary>Creates a new ZooMasterEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new ZooMasterEntity(fields);
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewZooMasterUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}
		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty Entity objects based on the entity type specified. Uses  entity specific factory objects</summary>
	[Serializable]
	public partial class GeneralEntityFactory
	{
		/// <summary>Creates a new, empty Entity object of the type specified</summary>
		/// <param name="entityTypeToCreate">The entity type to create.</param>
		/// <returns>A new, empty Entity object.</returns>
		public static IEntity2 Create(InfoGenesis.EntityType entityTypeToCreate)
		{
			IEntityFactory2 factoryToUse = null;
			switch(entityTypeToCreate)
			{
				case InfoGenesis.EntityType.BonusCodeMasterEntity:
					factoryToUse = new BonusCodeMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.ButtonAbbrJoinEntity:
					factoryToUse = new ButtonAbbrJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.CardTypeMasterEntity:
					factoryToUse = new CardTypeMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.CardTypeRangeJoinEntity:
					factoryToUse = new CardTypeRangeJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.CfgVersionEntity:
					factoryToUse = new CfgVersionEntityFactory();
					break;
				case InfoGenesis.EntityType.CheckTypeMasterEntity:
					factoryToUse = new CheckTypeMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.ChefMasterEntity:
					factoryToUse = new ChefMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.ChkTypeGratJoinEntity:
					factoryToUse = new ChkTypeGratJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.ChkTypeGratRevJoinEntity:
					factoryToUse = new ChkTypeGratRevJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.ChkTypeGratTaxJoinEntity:
					factoryToUse = new ChkTypeGratTaxJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.ChkTypeTaxGrpJoinEntity:
					factoryToUse = new ChkTypeTaxGrpJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.ChkTypeTaxGrpTaxJoinEntity:
					factoryToUse = new ChkTypeTaxGrpTaxJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.ChkTypeVatGratJoinEntity:
					factoryToUse = new ChkTypeVatGratJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.ChkTypeVatRevJoinEntity:
					factoryToUse = new ChkTypeVatRevJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.ChoiceGroupMasterEntity:
					factoryToUse = new ChoiceGroupMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.ChoiceGrpModJoinEntity:
					factoryToUse = new ChoiceGrpModJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.ComboItemJoinEntity:
					factoryToUse = new ComboItemJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.ComboMasterEntity:
					factoryToUse = new ComboMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.ConfigurationIdMasterEntity:
					factoryToUse = new ConfigurationIdMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.DeviceMasterEntity:
					factoryToUse = new DeviceMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.DiscoupMasterEntity:
					factoryToUse = new DiscoupMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.EmpGroupMasterEntity:
					factoryToUse = new EmpGroupMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.EmpGrpEmpJoinEntity:
					factoryToUse = new EmpGrpEmpJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.EmpJobcodeJoinEntity:
					factoryToUse = new EmpJobcodeJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.EmpMasterEntity:
					factoryToUse = new EmpMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.FuncBttnTextJoinEntity:
					factoryToUse = new FuncBttnTextJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.Ga4680ExportParmMasterEntity:
					factoryToUse = new Ga4680ExportParmMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.Ga4680ImportParmMasterEntity:
					factoryToUse = new Ga4680ImportParmMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.GeneralLedgerMapMasterEntity:
					factoryToUse = new GeneralLedgerMapMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.GiftCardMasterEntity:
					factoryToUse = new GiftCardMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.GratuityCatMasterEntity:
					factoryToUse = new GratuityCatMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.GratuityMasterEntity:
					factoryToUse = new GratuityMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.IdMasterEntity:
					factoryToUse = new IdMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.ImageLibraryMasterEntity:
					factoryToUse = new ImageLibraryMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.IntlDescriptorTextJoinEntity:
					factoryToUse = new IntlDescriptorTextJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.IpServerAttribMasterEntity:
					factoryToUse = new IpServerAttribMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.JobcodeFuncBttnJoinEntity:
					factoryToUse = new JobcodeFuncBttnJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.JobcodeFunctJoinEntity:
					factoryToUse = new JobcodeFunctJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.JobcodeMasterEntity:
					factoryToUse = new JobcodeMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.KdsCategoryMasterEntity:
					factoryToUse = new KdsCategoryMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.KdsVideoMasterEntity:
					factoryToUse = new KdsVideoMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.KpOptionGrpCopyJoinEntity:
					factoryToUse = new KpOptionGrpCopyJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.KpOptionGrpFormatJoinEntity:
					factoryToUse = new KpOptionGrpFormatJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.KpOptionGrpMasterEntity:
					factoryToUse = new KpOptionGrpMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.KpPrinterMasterEntity:
					factoryToUse = new KpPrinterMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.KpRouteMasterEntity:
					factoryToUse = new KpRouteMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.KpRoutePrinterJoinEntity:
					factoryToUse = new KpRoutePrinterJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.MachineOptionsMasterEntity:
					factoryToUse = new MachineOptionsMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.MealPeriodMasterEntity:
					factoryToUse = new MealPeriodMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.MembershipMasterEntity:
					factoryToUse = new MembershipMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.MembershipProfitCenterJoinEntity:
					factoryToUse = new MembershipProfitCenterJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.MenuBttnObjJoinEntity:
					factoryToUse = new MenuBttnObjJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.MenuItemGroupMasterEntity:
					factoryToUse = new MenuItemGroupMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.MenuItemMasterEntity:
					factoryToUse = new MenuItemMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.MenuMasterEntity:
					factoryToUse = new MenuMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.MenuMigrpJoinEntity:
					factoryToUse = new MenuMigrpJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.MiChoiceGrpJoinEntity:
					factoryToUse = new MiChoiceGrpJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.MiKpprinterJoinEntity:
					factoryToUse = new MiKpprinterJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.MiPriceJoinEntity:
					factoryToUse = new MiPriceJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.MiSkuJoinEntity:
					factoryToUse = new MiSkuJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.ModifierKpprinterJoinEntity:
					factoryToUse = new ModifierKpprinterJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.ModifierMasterEntity:
					factoryToUse = new ModifierMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.ModifierPriceJoinEntity:
					factoryToUse = new ModifierPriceJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.OptionsMasterEntity:
					factoryToUse = new OptionsMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.PriceLevelMasterEntity:
					factoryToUse = new PriceLevelMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.ProcessMasterEntity:
					factoryToUse = new ProcessMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.ProductClassMasterEntity:
					factoryToUse = new ProductClassMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.ProfitCenterDayPartJoinEntity:
					factoryToUse = new ProfitCenterDayPartJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.ProfitCenterGroupJoinEntity:
					factoryToUse = new ProfitCenterGroupJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.ProfitCenterGroupMasterEntity:
					factoryToUse = new ProfitCenterGroupMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.ProfitCenterMasterEntity:
					factoryToUse = new ProfitCenterMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.ProfitCenterReceiptPrinterJoinEntity:
					factoryToUse = new ProfitCenterReceiptPrinterJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.ProfitCenterTableJoinEntity:
					factoryToUse = new ProfitCenterTableJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.QuickTenderMasterEntity:
					factoryToUse = new QuickTenderMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.ReceiptPrinterMasterEntity:
					factoryToUse = new ReceiptPrinterMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.ReportCatMasterEntity:
					factoryToUse = new ReportCatMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.RevenueCatMasterEntity:
					factoryToUse = new RevenueCatMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.RevenueClassMasterEntity:
					factoryToUse = new RevenueClassMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.RowLockMasterEntity:
					factoryToUse = new RowLockMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.ScreenTemplateMasterEntity:
					factoryToUse = new ScreenTemplateMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.ScreenTemplateMigrpJoinEntity:
					factoryToUse = new ScreenTemplateMigrpJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.SecurityLevelMasterEntity:
					factoryToUse = new SecurityLevelMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.SelectionGroupItemJoinEntity:
					factoryToUse = new SelectionGroupItemJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.SelectionGroupMasterEntity:
					factoryToUse = new SelectionGroupMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.ServiceManagerProxyMasterEntity:
					factoryToUse = new ServiceManagerProxyMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.SmuCommandMasterEntity:
					factoryToUse = new SmuCommandMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.SmuStatusMasterEntity:
					factoryToUse = new SmuStatusMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.SpecialInstrMasterEntity:
					factoryToUse = new SpecialInstrMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.SystemConfigurationMasterEntity:
					factoryToUse = new SystemConfigurationMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.TableLayoutMasterEntity:
					factoryToUse = new TableLayoutMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.TaxCatMasterEntity:
					factoryToUse = new TaxCatMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.TaxGroupMasterEntity:
					factoryToUse = new TaxGroupMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.TaxGrpTaxJoinEntity:
					factoryToUse = new TaxGrpTaxJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.TaxMasterEntity:
					factoryToUse = new TaxMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.TaxRevCatJoinEntity:
					factoryToUse = new TaxRevCatJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.TenderClassMasterEntity:
					factoryToUse = new TenderClassMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.TenderMasterEntity:
					factoryToUse = new TenderMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.TermGrpChkTypeJoinEntity:
					factoryToUse = new TermGrpChkTypeJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.TermGrpJobcodeJoinEntity:
					factoryToUse = new TermGrpJobcodeJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.TermGrpMasterEntity:
					factoryToUse = new TermGrpMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.TermGrpMenuJoinEntity:
					factoryToUse = new TermGrpMenuJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.TermGrpMenuJoinCookerEntity:
					factoryToUse = new TermGrpMenuJoinCookerEntityFactory();
					break;
				case InfoGenesis.EntityType.TermGrpMpJoinEntity:
					factoryToUse = new TermGrpMpJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.TermGrpMpmenuJoinEntity:
					factoryToUse = new TermGrpMpmenuJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.TermGrpTndrJoinEntity:
					factoryToUse = new TermGrpTndrJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.TermGrpTransitionJoinEntity:
					factoryToUse = new TermGrpTransitionJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.TerminalMasterEntity:
					factoryToUse = new TerminalMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.TerminalProfitCtrJoinEntity:
					factoryToUse = new TerminalProfitCtrJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.TerminalTextGuestInfoJoinEntity:
					factoryToUse = new TerminalTextGuestInfoJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.TerminalTextMasterEntity:
					factoryToUse = new TerminalTextMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.TermOptionGrpMasterEntity:
					factoryToUse = new TermOptionGrpMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.TermOptionGrpOptionJoinEntity:
					factoryToUse = new TermOptionGrpOptionJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.TermPrinterGrpKpprntJoinEntity:
					factoryToUse = new TermPrinterGrpKpprntJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.TermPrinterGrpMasterEntity:
					factoryToUse = new TermPrinterGrpMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.TermPrinterGrpRouteJoinEntity:
					factoryToUse = new TermPrinterGrpRouteJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.ThemeColorJoinEntity:
					factoryToUse = new ThemeColorJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.ThemeFontJoinEntity:
					factoryToUse = new ThemeFontJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.ThemeMasterEntity:
					factoryToUse = new ThemeMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.ThemeObjectJoinEntity:
					factoryToUse = new ThemeObjectJoinEntityFactory();
					break;
				case InfoGenesis.EntityType.UdefDateRangeInstanceEntity:
					factoryToUse = new UdefDateRangeInstanceEntityFactory();
					break;
				case InfoGenesis.EntityType.UdefPeriodTypeMasterEntity:
					factoryToUse = new UdefPeriodTypeMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.VoidReasonMasterEntity:
					factoryToUse = new VoidReasonMasterEntityFactory();
					break;
				case InfoGenesis.EntityType.ZooMasterEntity:
					factoryToUse = new ZooMasterEntityFactory();
					break;
			}
			IEntity2 toReturn = null;
			if(factoryToUse != null)
			{
				toReturn = factoryToUse.Create();
			}
			return toReturn;
		}		
	}
		
	/// <summary>Class which is used to obtain the entity factory based on the .NET type of the entity. </summary>
	[Serializable]
	public static class EntityFactoryFactory
	{
		private static Dictionary<Type, IEntityFactory2> _factoryPerType = new Dictionary<Type, IEntityFactory2>();

		/// <summary>Initializes the <see cref="EntityFactoryFactory"/> class.</summary>
		static EntityFactoryFactory()
		{
			Array entityTypeValues = Enum.GetValues(typeof(InfoGenesis.EntityType));
			foreach(int entityTypeValue in entityTypeValues)
			{
				IEntity2 dummy = GeneralEntityFactory.Create((InfoGenesis.EntityType)entityTypeValue);
				_factoryPerType.Add(dummy.GetType(), dummy.GetEntityFactory());
			}
		}

		/// <summary>Gets the factory of the entity with the .NET type specified</summary>
		/// <param name="typeOfEntity">The type of entity.</param>
		/// <returns>factory to use or null if not found</returns>
		public static IEntityFactory2 GetFactory(Type typeOfEntity)
		{
			IEntityFactory2 toReturn = null;
			_factoryPerType.TryGetValue(typeOfEntity, out toReturn);
			return toReturn;
		}

		/// <summary>Gets the factory of the entity with the InfoGenesis.EntityType specified</summary>
		/// <param name="typeOfEntity">The type of entity.</param>
		/// <returns>factory to use or null if not found</returns>
		public static IEntityFactory2 GetFactory(InfoGenesis.EntityType typeOfEntity)
		{
			return GetFactory(GeneralEntityFactory.Create(typeOfEntity).GetType());
		}
	}
		
	/// <summary>Element creator for creating project elements from somewhere else, like inside Linq providers.</summary>
	public class ElementCreator : ElementCreatorBase, IElementCreator2
	{
		/// <summary>Gets the factory of the Entity type with the InfoGenesis.EntityType value passed in</summary>
		/// <param name="entityTypeValue">The entity type value.</param>
		/// <returns>the entity factory of the entity type or null if not found</returns>
		public IEntityFactory2 GetFactory(int entityTypeValue)
		{
			return (IEntityFactory2)this.GetFactoryImpl(entityTypeValue);
		}
		
		/// <summary>Gets the factory of the Entity type with the .NET type passed in</summary>
		/// <param name="typeOfEntity">The type of entity.</param>
		/// <returns>the entity factory of the entity type or null if not found</returns>
		public IEntityFactory2 GetFactory(Type typeOfEntity)
		{
			return (IEntityFactory2)this.GetFactoryImpl(typeOfEntity);
		}

		/// <summary>Creates a new resultset fields object with the number of field slots reserved as specified</summary>
		/// <param name="numberOfFields">The number of fields.</param>
		/// <returns>ready to use resultsetfields object</returns>
		public IEntityFields2 CreateResultsetFields(int numberOfFields)
		{
			return new ResultsetFields(numberOfFields);
		}

		/// <summary>Creates a new dynamic relation instance</summary>
		/// <param name="leftOperand">The left operand.</param>
		/// <returns>ready to use dynamic relation</returns>
		public override IDynamicRelation CreateDynamicRelation(DerivedTableDefinition leftOperand)
		{
			return new DynamicRelation(leftOperand);
		}

		/// <summary>Creates a new dynamic relation instance</summary>
		/// <param name="leftOperand">The left operand.</param>
		/// <param name="joinType">Type of the join. If None is specified, Inner is assumed.</param>
		/// <param name="rightOperand">The right operand.</param>
		/// <param name="onClause">The on clause for the join.</param>
		/// <returns>ready to use dynamic relation</returns>
		public override IDynamicRelation CreateDynamicRelation(DerivedTableDefinition leftOperand, JoinHint joinType, DerivedTableDefinition rightOperand, IPredicate onClause)
		{
			return new DynamicRelation(leftOperand, joinType, rightOperand, onClause);
		}
		
		/// <summary>Obtains the inheritance info provider instance from the singleton </summary>
		/// <returns>The singleton instance of the inheritance info provider</returns>
		public override IInheritanceInfoProvider ObtainInheritanceInfoProviderInstance()
		{
			return InheritanceInfoProviderSingleton.GetInstance();
		}

		/// <summary>Creates a new dynamic relation instance</summary>
		/// <param name="leftOperand">The left operand.</param>
		/// <param name="joinType">Type of the join. If None is specified, Inner is assumed.</param>
		/// <param name="rightOperandEntityName">Name of the entity, which is used as the right operand.</param>
		/// <param name="aliasRightOperand">The alias of the right operand. If you don't want to / need to alias the right operand (only alias if you have to), specify string.Empty.</param>
		/// <param name="onClause">The on clause for the join.</param>
		/// <returns>ready to use dynamic relation</returns>
		public override IDynamicRelation CreateDynamicRelation(DerivedTableDefinition leftOperand, JoinHint joinType, string rightOperandEntityName, string aliasRightOperand, IPredicate onClause)
		{
			return new DynamicRelation(leftOperand, joinType, (InfoGenesis.EntityType)Enum.Parse(typeof(InfoGenesis.EntityType), rightOperandEntityName, false), aliasRightOperand, onClause);
		}

		/// <summary>Creates a new dynamic relation instance</summary>
		/// <param name="leftOperandEntityName">Name of the entity which is used as the left operand.</param>
		/// <param name="joinType">Type of the join. If None is specified, Inner is assumed.</param>
		/// <param name="rightOperandEntityName">Name of the entity, which is used as the right operand.</param>
		/// <param name="aliasLeftOperand">The alias of the left operand. If you don't want to / need to alias the right operand (only alias if you have to), specify string.Empty.</param>
		/// <param name="aliasRightOperand">The alias of the right operand. If you don't want to / need to alias the right operand (only alias if you have to), specify string.Empty.</param>
		/// <param name="onClause">The on clause for the join.</param>
		/// <returns>ready to use dynamic relation</returns>
		public override IDynamicRelation CreateDynamicRelation(string leftOperandEntityName, JoinHint joinType, string rightOperandEntityName, string aliasLeftOperand, string aliasRightOperand, IPredicate onClause)
		{
			return new DynamicRelation((InfoGenesis.EntityType)Enum.Parse(typeof(InfoGenesis.EntityType), leftOperandEntityName, false), joinType, (InfoGenesis.EntityType)Enum.Parse(typeof(InfoGenesis.EntityType), rightOperandEntityName, false), aliasLeftOperand, aliasRightOperand, onClause);
		}
		
		/// <summary>Implementation of the routine which gets the factory of the Entity type with the InfoGenesis.EntityType value passed in</summary>
		/// <param name="entityTypeValue">The entity type value.</param>
		/// <returns>the entity factory of the entity type or null if not found</returns>
		protected override IEntityFactoryCore GetFactoryImpl(int entityTypeValue)
		{
			return EntityFactoryFactory.GetFactory((InfoGenesis.EntityType)entityTypeValue);
		}

		/// <summary>Implementation of the routine which gets the factory of the Entity type with the .NET type passed in</summary>
		/// <param name="typeOfEntity">The type of entity.</param>
		/// <returns>the entity factory of the entity type or null if not found</returns>
		protected override IEntityFactoryCore GetFactoryImpl(Type typeOfEntity)
		{
			return EntityFactoryFactory.GetFactory(typeOfEntity);
		}

	}
}
