﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using SD.LLBLGen.Pro.ORMSupportClasses;
using InfoGenesis.FactoryClasses;
using InfoGenesis;

namespace InfoGenesis.HelperClasses
{
	/// <summary>Field Creation Class for entity BonusCodeMasterEntity</summary>
	public partial class BonusCodeMasterFields
	{
		/// <summary>Creates a new BonusCodeMasterEntity.BonusCodeEndDate field instance</summary>
		public static EntityField2 BonusCodeEndDate
		{
			get { return (EntityField2)EntityFieldFactory.Create(BonusCodeMasterFieldIndex.BonusCodeEndDate);}
		}
		/// <summary>Creates a new BonusCodeMasterEntity.BonusCodeGenId field instance</summary>
		public static EntityField2 BonusCodeGenId
		{
			get { return (EntityField2)EntityFieldFactory.Create(BonusCodeMasterFieldIndex.BonusCodeGenId);}
		}
		/// <summary>Creates a new BonusCodeMasterEntity.BonusCodeName field instance</summary>
		public static EntityField2 BonusCodeName
		{
			get { return (EntityField2)EntityFieldFactory.Create(BonusCodeMasterFieldIndex.BonusCodeName);}
		}
		/// <summary>Creates a new BonusCodeMasterEntity.BonusCodeNo field instance</summary>
		public static EntityField2 BonusCodeNo
		{
			get { return (EntityField2)EntityFieldFactory.Create(BonusCodeMasterFieldIndex.BonusCodeNo);}
		}
		/// <summary>Creates a new BonusCodeMasterEntity.BonusCodeStartDate field instance</summary>
		public static EntityField2 BonusCodeStartDate
		{
			get { return (EntityField2)EntityFieldFactory.Create(BonusCodeMasterFieldIndex.BonusCodeStartDate);}
		}
		/// <summary>Creates a new BonusCodeMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(BonusCodeMasterFieldIndex.EntId);}
		}
	}

	/// <summary>Field Creation Class for entity ButtonAbbrJoinEntity</summary>
	public partial class ButtonAbbrJoinFields
	{
		/// <summary>Creates a new ButtonAbbrJoinEntity.ButtonId field instance</summary>
		public static EntityField2 ButtonId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ButtonAbbrJoinFieldIndex.ButtonId);}
		}
		/// <summary>Creates a new ButtonAbbrJoinEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ButtonAbbrJoinFieldIndex.EntId);}
		}
		/// <summary>Creates a new ButtonAbbrJoinEntity.UserButtonAbbr1 field instance</summary>
		public static EntityField2 UserButtonAbbr1
		{
			get { return (EntityField2)EntityFieldFactory.Create(ButtonAbbrJoinFieldIndex.UserButtonAbbr1);}
		}
		/// <summary>Creates a new ButtonAbbrJoinEntity.UserButtonAbbr2 field instance</summary>
		public static EntityField2 UserButtonAbbr2
		{
			get { return (EntityField2)EntityFieldFactory.Create(ButtonAbbrJoinFieldIndex.UserButtonAbbr2);}
		}
	}

	/// <summary>Field Creation Class for entity CardTypeMasterEntity</summary>
	public partial class CardTypeMasterFields
	{
		/// <summary>Creates a new CardTypeMasterEntity.CardTypeId field instance</summary>
		public static EntityField2 CardTypeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(CardTypeMasterFieldIndex.CardTypeId);}
		}
		/// <summary>Creates a new CardTypeMasterEntity.CardTypeName field instance</summary>
		public static EntityField2 CardTypeName
		{
			get { return (EntityField2)EntityFieldFactory.Create(CardTypeMasterFieldIndex.CardTypeName);}
		}
		/// <summary>Creates a new CardTypeMasterEntity.CardTypeNumLength field instance</summary>
		public static EntityField2 CardTypeNumLength
		{
			get { return (EntityField2)EntityFieldFactory.Create(CardTypeMasterFieldIndex.CardTypeNumLength);}
		}
		/// <summary>Creates a new CardTypeMasterEntity.CardTypePrefixLength field instance</summary>
		public static EntityField2 CardTypePrefixLength
		{
			get { return (EntityField2)EntityFieldFactory.Create(CardTypeMasterFieldIndex.CardTypePrefixLength);}
		}
		/// <summary>Creates a new CardTypeMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(CardTypeMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new CardTypeMasterEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(CardTypeMasterFieldIndex.StoreId);}
		}
		/// <summary>Creates a new CardTypeMasterEntity.TenderId field instance</summary>
		public static EntityField2 TenderId
		{
			get { return (EntityField2)EntityFieldFactory.Create(CardTypeMasterFieldIndex.TenderId);}
		}
	}

	/// <summary>Field Creation Class for entity CardTypeRangeJoinEntity</summary>
	public partial class CardTypeRangeJoinFields
	{
		/// <summary>Creates a new CardTypeRangeJoinEntity.CardTypeId field instance</summary>
		public static EntityField2 CardTypeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(CardTypeRangeJoinFieldIndex.CardTypeId);}
		}
		/// <summary>Creates a new CardTypeRangeJoinEntity.CardTypePrefixHigh field instance</summary>
		public static EntityField2 CardTypePrefixHigh
		{
			get { return (EntityField2)EntityFieldFactory.Create(CardTypeRangeJoinFieldIndex.CardTypePrefixHigh);}
		}
		/// <summary>Creates a new CardTypeRangeJoinEntity.CardTypePrefixLow field instance</summary>
		public static EntityField2 CardTypePrefixLow
		{
			get { return (EntityField2)EntityFieldFactory.Create(CardTypeRangeJoinFieldIndex.CardTypePrefixLow);}
		}
		/// <summary>Creates a new CardTypeRangeJoinEntity.CardTypeRangeId field instance</summary>
		public static EntityField2 CardTypeRangeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(CardTypeRangeJoinFieldIndex.CardTypeRangeId);}
		}
		/// <summary>Creates a new CardTypeRangeJoinEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(CardTypeRangeJoinFieldIndex.EntId);}
		}
	}

	/// <summary>Field Creation Class for entity CfgVersionEntity</summary>
	public partial class CfgVersionFields
	{
		/// <summary>Creates a new CfgVersionEntity.BuildVersion field instance</summary>
		public static EntityField2 BuildVersion
		{
			get { return (EntityField2)EntityFieldFactory.Create(CfgVersionFieldIndex.BuildVersion);}
		}
		/// <summary>Creates a new CfgVersionEntity.DbName field instance</summary>
		public static EntityField2 DbName
		{
			get { return (EntityField2)EntityFieldFactory.Create(CfgVersionFieldIndex.DbName);}
		}
		/// <summary>Creates a new CfgVersionEntity.MajorVersion field instance</summary>
		public static EntityField2 MajorVersion
		{
			get { return (EntityField2)EntityFieldFactory.Create(CfgVersionFieldIndex.MajorVersion);}
		}
		/// <summary>Creates a new CfgVersionEntity.MinorVersion field instance</summary>
		public static EntityField2 MinorVersion
		{
			get { return (EntityField2)EntityFieldFactory.Create(CfgVersionFieldIndex.MinorVersion);}
		}
		/// <summary>Creates a new CfgVersionEntity.PatchVersion field instance</summary>
		public static EntityField2 PatchVersion
		{
			get { return (EntityField2)EntityFieldFactory.Create(CfgVersionFieldIndex.PatchVersion);}
		}
	}

	/// <summary>Field Creation Class for entity CheckTypeMasterEntity</summary>
	public partial class CheckTypeMasterFields
	{
		/// <summary>Creates a new CheckTypeMasterEntity.CheckTypeAbbr1 field instance</summary>
		public static EntityField2 CheckTypeAbbr1
		{
			get { return (EntityField2)EntityFieldFactory.Create(CheckTypeMasterFieldIndex.CheckTypeAbbr1);}
		}
		/// <summary>Creates a new CheckTypeMasterEntity.CheckTypeAbbr2 field instance</summary>
		public static EntityField2 CheckTypeAbbr2
		{
			get { return (EntityField2)EntityFieldFactory.Create(CheckTypeMasterFieldIndex.CheckTypeAbbr2);}
		}
		/// <summary>Creates a new CheckTypeMasterEntity.CheckTypeId field instance</summary>
		public static EntityField2 CheckTypeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(CheckTypeMasterFieldIndex.CheckTypeId);}
		}
		/// <summary>Creates a new CheckTypeMasterEntity.CheckTypeName field instance</summary>
		public static EntityField2 CheckTypeName
		{
			get { return (EntityField2)EntityFieldFactory.Create(CheckTypeMasterFieldIndex.CheckTypeName);}
		}
		/// <summary>Creates a new CheckTypeMasterEntity.DefaultPriceLevelId field instance</summary>
		public static EntityField2 DefaultPriceLevelId
		{
			get { return (EntityField2)EntityFieldFactory.Create(CheckTypeMasterFieldIndex.DefaultPriceLevelId);}
		}
		/// <summary>Creates a new CheckTypeMasterEntity.DefaultSecId field instance</summary>
		public static EntityField2 DefaultSecId
		{
			get { return (EntityField2)EntityFieldFactory.Create(CheckTypeMasterFieldIndex.DefaultSecId);}
		}
		/// <summary>Creates a new CheckTypeMasterEntity.DiscountId field instance</summary>
		public static EntityField2 DiscountId
		{
			get { return (EntityField2)EntityFieldFactory.Create(CheckTypeMasterFieldIndex.DiscountId);}
		}
		/// <summary>Creates a new CheckTypeMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(CheckTypeMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new CheckTypeMasterEntity.RoundBasis field instance</summary>
		public static EntityField2 RoundBasis
		{
			get { return (EntityField2)EntityFieldFactory.Create(CheckTypeMasterFieldIndex.RoundBasis);}
		}
		/// <summary>Creates a new CheckTypeMasterEntity.RoundTypeId field instance</summary>
		public static EntityField2 RoundTypeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(CheckTypeMasterFieldIndex.RoundTypeId);}
		}
		/// <summary>Creates a new CheckTypeMasterEntity.RowVersion field instance</summary>
		public static EntityField2 RowVersion
		{
			get { return (EntityField2)EntityFieldFactory.Create(CheckTypeMasterFieldIndex.RowVersion);}
		}
		/// <summary>Creates a new CheckTypeMasterEntity.SalesTippableFlag field instance</summary>
		public static EntityField2 SalesTippableFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(CheckTypeMasterFieldIndex.SalesTippableFlag);}
		}
		/// <summary>Creates a new CheckTypeMasterEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(CheckTypeMasterFieldIndex.StoreId);}
		}
	}

	/// <summary>Field Creation Class for entity ChefMasterEntity</summary>
	public partial class ChefMasterFields
	{
		/// <summary>Creates a new ChefMasterEntity.CookedFlag field instance</summary>
		public static EntityField2 CookedFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChefMasterFieldIndex.CookedFlag);}
		}
		/// <summary>Creates a new ChefMasterEntity.CookerHandle field instance</summary>
		public static EntityField2 CookerHandle
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChefMasterFieldIndex.CookerHandle);}
		}
		/// <summary>Creates a new ChefMasterEntity.CookerKeyId field instance</summary>
		public static EntityField2 CookerKeyId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChefMasterFieldIndex.CookerKeyId);}
		}
		/// <summary>Creates a new ChefMasterEntity.CustomerId field instance</summary>
		public static EntityField2 CustomerId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChefMasterFieldIndex.CustomerId);}
		}
		/// <summary>Creates a new ChefMasterEntity.DivisionId field instance</summary>
		public static EntityField2 DivisionId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChefMasterFieldIndex.DivisionId);}
		}
		/// <summary>Creates a new ChefMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChefMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new ChefMasterEntity.LsrName field instance</summary>
		public static EntityField2 LsrName
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChefMasterFieldIndex.LsrName);}
		}
		/// <summary>Creates a new ChefMasterEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChefMasterFieldIndex.StoreId);}
		}
	}

	/// <summary>Field Creation Class for entity ChkTypeGratJoinEntity</summary>
	public partial class ChkTypeGratJoinFields
	{
		/// <summary>Creates a new ChkTypeGratJoinEntity.CheckTypeId field instance</summary>
		public static EntityField2 CheckTypeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChkTypeGratJoinFieldIndex.CheckTypeId);}
		}
		/// <summary>Creates a new ChkTypeGratJoinEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChkTypeGratJoinFieldIndex.EntId);}
		}
		/// <summary>Creates a new ChkTypeGratJoinEntity.GratId field instance</summary>
		public static EntityField2 GratId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChkTypeGratJoinFieldIndex.GratId);}
		}
		/// <summary>Creates a new ChkTypeGratJoinEntity.RevCatCodeId field instance</summary>
		public static EntityField2 RevCatCodeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChkTypeGratJoinFieldIndex.RevCatCodeId);}
		}
		/// <summary>Creates a new ChkTypeGratJoinEntity.TaxCodeId field instance</summary>
		public static EntityField2 TaxCodeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChkTypeGratJoinFieldIndex.TaxCodeId);}
		}
	}

	/// <summary>Field Creation Class for entity ChkTypeGratRevJoinEntity</summary>
	public partial class ChkTypeGratRevJoinFields
	{
		/// <summary>Creates a new ChkTypeGratRevJoinEntity.CheckTypeId field instance</summary>
		public static EntityField2 CheckTypeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChkTypeGratRevJoinFieldIndex.CheckTypeId);}
		}
		/// <summary>Creates a new ChkTypeGratRevJoinEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChkTypeGratRevJoinFieldIndex.EntId);}
		}
		/// <summary>Creates a new ChkTypeGratRevJoinEntity.GratId field instance</summary>
		public static EntityField2 GratId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChkTypeGratRevJoinFieldIndex.GratId);}
		}
		/// <summary>Creates a new ChkTypeGratRevJoinEntity.RevCatId field instance</summary>
		public static EntityField2 RevCatId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChkTypeGratRevJoinFieldIndex.RevCatId);}
		}
	}

	/// <summary>Field Creation Class for entity ChkTypeGratTaxJoinEntity</summary>
	public partial class ChkTypeGratTaxJoinFields
	{
		/// <summary>Creates a new ChkTypeGratTaxJoinEntity.CheckTypeId field instance</summary>
		public static EntityField2 CheckTypeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChkTypeGratTaxJoinFieldIndex.CheckTypeId);}
		}
		/// <summary>Creates a new ChkTypeGratTaxJoinEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChkTypeGratTaxJoinFieldIndex.EntId);}
		}
		/// <summary>Creates a new ChkTypeGratTaxJoinEntity.GratId field instance</summary>
		public static EntityField2 GratId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChkTypeGratTaxJoinFieldIndex.GratId);}
		}
		/// <summary>Creates a new ChkTypeGratTaxJoinEntity.TaxId field instance</summary>
		public static EntityField2 TaxId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChkTypeGratTaxJoinFieldIndex.TaxId);}
		}
	}

	/// <summary>Field Creation Class for entity ChkTypeTaxGrpJoinEntity</summary>
	public partial class ChkTypeTaxGrpJoinFields
	{
		/// <summary>Creates a new ChkTypeTaxGrpJoinEntity.CheckTypeId field instance</summary>
		public static EntityField2 CheckTypeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChkTypeTaxGrpJoinFieldIndex.CheckTypeId);}
		}
		/// <summary>Creates a new ChkTypeTaxGrpJoinEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChkTypeTaxGrpJoinFieldIndex.EntId);}
		}
		/// <summary>Creates a new ChkTypeTaxGrpJoinEntity.TaxCodeId field instance</summary>
		public static EntityField2 TaxCodeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChkTypeTaxGrpJoinFieldIndex.TaxCodeId);}
		}
		/// <summary>Creates a new ChkTypeTaxGrpJoinEntity.TaxGrpId field instance</summary>
		public static EntityField2 TaxGrpId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChkTypeTaxGrpJoinFieldIndex.TaxGrpId);}
		}
	}

	/// <summary>Field Creation Class for entity ChkTypeTaxGrpTaxJoinEntity</summary>
	public partial class ChkTypeTaxGrpTaxJoinFields
	{
		/// <summary>Creates a new ChkTypeTaxGrpTaxJoinEntity.CheckTypeId field instance</summary>
		public static EntityField2 CheckTypeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChkTypeTaxGrpTaxJoinFieldIndex.CheckTypeId);}
		}
		/// <summary>Creates a new ChkTypeTaxGrpTaxJoinEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChkTypeTaxGrpTaxJoinFieldIndex.EntId);}
		}
		/// <summary>Creates a new ChkTypeTaxGrpTaxJoinEntity.TaxGrpId field instance</summary>
		public static EntityField2 TaxGrpId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChkTypeTaxGrpTaxJoinFieldIndex.TaxGrpId);}
		}
		/// <summary>Creates a new ChkTypeTaxGrpTaxJoinEntity.TaxId field instance</summary>
		public static EntityField2 TaxId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChkTypeTaxGrpTaxJoinFieldIndex.TaxId);}
		}
	}

	/// <summary>Field Creation Class for entity ChkTypeVatGratJoinEntity</summary>
	public partial class ChkTypeVatGratJoinFields
	{
		/// <summary>Creates a new ChkTypeVatGratJoinEntity.CheckTypeId field instance</summary>
		public static EntityField2 CheckTypeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChkTypeVatGratJoinFieldIndex.CheckTypeId);}
		}
		/// <summary>Creates a new ChkTypeVatGratJoinEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChkTypeVatGratJoinFieldIndex.EntId);}
		}
		/// <summary>Creates a new ChkTypeVatGratJoinEntity.GratId field instance</summary>
		public static EntityField2 GratId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChkTypeVatGratJoinFieldIndex.GratId);}
		}
		/// <summary>Creates a new ChkTypeVatGratJoinEntity.TaxId field instance</summary>
		public static EntityField2 TaxId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChkTypeVatGratJoinFieldIndex.TaxId);}
		}
	}

	/// <summary>Field Creation Class for entity ChkTypeVatRevJoinEntity</summary>
	public partial class ChkTypeVatRevJoinFields
	{
		/// <summary>Creates a new ChkTypeVatRevJoinEntity.CheckTypeId field instance</summary>
		public static EntityField2 CheckTypeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChkTypeVatRevJoinFieldIndex.CheckTypeId);}
		}
		/// <summary>Creates a new ChkTypeVatRevJoinEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChkTypeVatRevJoinFieldIndex.EntId);}
		}
		/// <summary>Creates a new ChkTypeVatRevJoinEntity.RevCatId field instance</summary>
		public static EntityField2 RevCatId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChkTypeVatRevJoinFieldIndex.RevCatId);}
		}
		/// <summary>Creates a new ChkTypeVatRevJoinEntity.TaxId field instance</summary>
		public static EntityField2 TaxId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChkTypeVatRevJoinFieldIndex.TaxId);}
		}
	}

	/// <summary>Field Creation Class for entity ChoiceGroupMasterEntity</summary>
	public partial class ChoiceGroupMasterFields
	{
		/// <summary>Creates a new ChoiceGroupMasterEntity.ChoiceGroupAbbr1 field instance</summary>
		public static EntityField2 ChoiceGroupAbbr1
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChoiceGroupMasterFieldIndex.ChoiceGroupAbbr1);}
		}
		/// <summary>Creates a new ChoiceGroupMasterEntity.ChoiceGroupAbbr2 field instance</summary>
		public static EntityField2 ChoiceGroupAbbr2
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChoiceGroupMasterFieldIndex.ChoiceGroupAbbr2);}
		}
		/// <summary>Creates a new ChoiceGroupMasterEntity.ChoiceGroupId field instance</summary>
		public static EntityField2 ChoiceGroupId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChoiceGroupMasterFieldIndex.ChoiceGroupId);}
		}
		/// <summary>Creates a new ChoiceGroupMasterEntity.ChoiceGroupLabel field instance</summary>
		public static EntityField2 ChoiceGroupLabel
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChoiceGroupMasterFieldIndex.ChoiceGroupLabel);}
		}
		/// <summary>Creates a new ChoiceGroupMasterEntity.ChoiceGroupName field instance</summary>
		public static EntityField2 ChoiceGroupName
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChoiceGroupMasterFieldIndex.ChoiceGroupName);}
		}
		/// <summary>Creates a new ChoiceGroupMasterEntity.DataControlGroupId field instance</summary>
		public static EntityField2 DataControlGroupId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChoiceGroupMasterFieldIndex.DataControlGroupId);}
		}
		/// <summary>Creates a new ChoiceGroupMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChoiceGroupMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new ChoiceGroupMasterEntity.MaxNumMods field instance</summary>
		public static EntityField2 MaxNumMods
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChoiceGroupMasterFieldIndex.MaxNumMods);}
		}
		/// <summary>Creates a new ChoiceGroupMasterEntity.MinNumMods field instance</summary>
		public static EntityField2 MinNumMods
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChoiceGroupMasterFieldIndex.MinNumMods);}
		}
		/// <summary>Creates a new ChoiceGroupMasterEntity.NumFreeMods field instance</summary>
		public static EntityField2 NumFreeMods
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChoiceGroupMasterFieldIndex.NumFreeMods);}
		}
		/// <summary>Creates a new ChoiceGroupMasterEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChoiceGroupMasterFieldIndex.StoreId);}
		}
	}

	/// <summary>Field Creation Class for entity ChoiceGrpModJoinEntity</summary>
	public partial class ChoiceGrpModJoinFields
	{
		/// <summary>Creates a new ChoiceGrpModJoinEntity.ChoiceGroupId field instance</summary>
		public static EntityField2 ChoiceGroupId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChoiceGrpModJoinFieldIndex.ChoiceGroupId);}
		}
		/// <summary>Creates a new ChoiceGrpModJoinEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChoiceGrpModJoinFieldIndex.EntId);}
		}
		/// <summary>Creates a new ChoiceGrpModJoinEntity.ModifierChoicegrpSeq field instance</summary>
		public static EntityField2 ModifierChoicegrpSeq
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChoiceGrpModJoinFieldIndex.ModifierChoicegrpSeq);}
		}
		/// <summary>Creates a new ChoiceGrpModJoinEntity.ModifierId field instance</summary>
		public static EntityField2 ModifierId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ChoiceGrpModJoinFieldIndex.ModifierId);}
		}
	}

	/// <summary>Field Creation Class for entity ComboItemJoinEntity</summary>
	public partial class ComboItemJoinFields
	{
		/// <summary>Creates a new ComboItemJoinEntity.ComboId field instance</summary>
		public static EntityField2 ComboId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ComboItemJoinFieldIndex.ComboId);}
		}
		/// <summary>Creates a new ComboItemJoinEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ComboItemJoinFieldIndex.EntId);}
		}
		/// <summary>Creates a new ComboItemJoinEntity.ItemPrice field instance</summary>
		public static EntityField2 ItemPrice
		{
			get { return (EntityField2)EntityFieldFactory.Create(ComboItemJoinFieldIndex.ItemPrice);}
		}
		/// <summary>Creates a new ComboItemJoinEntity.ItemQty field instance</summary>
		public static EntityField2 ItemQty
		{
			get { return (EntityField2)EntityFieldFactory.Create(ComboItemJoinFieldIndex.ItemQty);}
		}
		/// <summary>Creates a new ComboItemJoinEntity.KeyId field instance</summary>
		public static EntityField2 KeyId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ComboItemJoinFieldIndex.KeyId);}
		}
		/// <summary>Creates a new ComboItemJoinEntity.ObjectId field instance</summary>
		public static EntityField2 ObjectId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ComboItemJoinFieldIndex.ObjectId);}
		}
		/// <summary>Creates a new ComboItemJoinEntity.ObjectTypeId field instance</summary>
		public static EntityField2 ObjectTypeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ComboItemJoinFieldIndex.ObjectTypeId);}
		}
		/// <summary>Creates a new ComboItemJoinEntity.PrintReceiptFlag field instance</summary>
		public static EntityField2 PrintReceiptFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(ComboItemJoinFieldIndex.PrintReceiptFlag);}
		}
		/// <summary>Creates a new ComboItemJoinEntity.SequenceId field instance</summary>
		public static EntityField2 SequenceId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ComboItemJoinFieldIndex.SequenceId);}
		}
	}

	/// <summary>Field Creation Class for entity ComboMasterEntity</summary>
	public partial class ComboMasterFields
	{
		/// <summary>Creates a new ComboMasterEntity.ComboAbbr1 field instance</summary>
		public static EntityField2 ComboAbbr1
		{
			get { return (EntityField2)EntityFieldFactory.Create(ComboMasterFieldIndex.ComboAbbr1);}
		}
		/// <summary>Creates a new ComboMasterEntity.ComboAbbr2 field instance</summary>
		public static EntityField2 ComboAbbr2
		{
			get { return (EntityField2)EntityFieldFactory.Create(ComboMasterFieldIndex.ComboAbbr2);}
		}
		/// <summary>Creates a new ComboMasterEntity.ComboId field instance</summary>
		public static EntityField2 ComboId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ComboMasterFieldIndex.ComboId);}
		}
		/// <summary>Creates a new ComboMasterEntity.ComboKpLabel field instance</summary>
		public static EntityField2 ComboKpLabel
		{
			get { return (EntityField2)EntityFieldFactory.Create(ComboMasterFieldIndex.ComboKpLabel);}
		}
		/// <summary>Creates a new ComboMasterEntity.ComboMenuItemId field instance</summary>
		public static EntityField2 ComboMenuItemId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ComboMasterFieldIndex.ComboMenuItemId);}
		}
		/// <summary>Creates a new ComboMasterEntity.ComboName field instance</summary>
		public static EntityField2 ComboName
		{
			get { return (EntityField2)EntityFieldFactory.Create(ComboMasterFieldIndex.ComboName);}
		}
		/// <summary>Creates a new ComboMasterEntity.ComboReceiptLabel field instance</summary>
		public static EntityField2 ComboReceiptLabel
		{
			get { return (EntityField2)EntityFieldFactory.Create(ComboMasterFieldIndex.ComboReceiptLabel);}
		}
		/// <summary>Creates a new ComboMasterEntity.DivisionId field instance</summary>
		public static EntityField2 DivisionId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ComboMasterFieldIndex.DivisionId);}
		}
		/// <summary>Creates a new ComboMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ComboMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new ComboMasterEntity.KdsVideoLabel field instance</summary>
		public static EntityField2 KdsVideoLabel
		{
			get { return (EntityField2)EntityFieldFactory.Create(ComboMasterFieldIndex.KdsVideoLabel);}
		}
		/// <summary>Creates a new ComboMasterEntity.RowVersion field instance</summary>
		public static EntityField2 RowVersion
		{
			get { return (EntityField2)EntityFieldFactory.Create(ComboMasterFieldIndex.RowVersion);}
		}
	}

	/// <summary>Field Creation Class for entity ConfigurationIdMasterEntity</summary>
	public partial class ConfigurationIdMasterFields
	{
		/// <summary>Creates a new ConfigurationIdMasterEntity.CustomerId field instance</summary>
		public static EntityField2 CustomerId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ConfigurationIdMasterFieldIndex.CustomerId);}
		}
		/// <summary>Creates a new ConfigurationIdMasterEntity.DivId field instance</summary>
		public static EntityField2 DivId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ConfigurationIdMasterFieldIndex.DivId);}
		}
		/// <summary>Creates a new ConfigurationIdMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ConfigurationIdMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new ConfigurationIdMasterEntity.ReservedId field instance</summary>
		public static EntityField2 ReservedId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ConfigurationIdMasterFieldIndex.ReservedId);}
		}
		/// <summary>Creates a new ConfigurationIdMasterEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ConfigurationIdMasterFieldIndex.StoreId);}
		}
		/// <summary>Creates a new ConfigurationIdMasterEntity.TableName field instance</summary>
		public static EntityField2 TableName
		{
			get { return (EntityField2)EntityFieldFactory.Create(ConfigurationIdMasterFieldIndex.TableName);}
		}
		/// <summary>Creates a new ConfigurationIdMasterEntity.Timeout field instance</summary>
		public static EntityField2 Timeout
		{
			get { return (EntityField2)EntityFieldFactory.Create(ConfigurationIdMasterFieldIndex.Timeout);}
		}
	}

	/// <summary>Field Creation Class for entity DeviceMasterEntity</summary>
	public partial class DeviceMasterFields
	{
		/// <summary>Creates a new DeviceMasterEntity.BbArmingCharacter field instance</summary>
		public static EntityField2 BbArmingCharacter
		{
			get { return (EntityField2)EntityFieldFactory.Create(DeviceMasterFieldIndex.BbArmingCharacter);}
		}
		/// <summary>Creates a new DeviceMasterEntity.BbOffsetId field instance</summary>
		public static EntityField2 BbOffsetId
		{
			get { return (EntityField2)EntityFieldFactory.Create(DeviceMasterFieldIndex.BbOffsetId);}
		}
		/// <summary>Creates a new DeviceMasterEntity.ConnectedDeviceId field instance</summary>
		public static EntityField2 ConnectedDeviceId
		{
			get { return (EntityField2)EntityFieldFactory.Create(DeviceMasterFieldIndex.ConnectedDeviceId);}
		}
		/// <summary>Creates a new DeviceMasterEntity.ConnectionCodeId field instance</summary>
		public static EntityField2 ConnectionCodeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(DeviceMasterFieldIndex.ConnectionCodeId);}
		}
		/// <summary>Creates a new DeviceMasterEntity.DeviceAbbr field instance</summary>
		public static EntityField2 DeviceAbbr
		{
			get { return (EntityField2)EntityFieldFactory.Create(DeviceMasterFieldIndex.DeviceAbbr);}
		}
		/// <summary>Creates a new DeviceMasterEntity.DeviceDesc field instance</summary>
		public static EntityField2 DeviceDesc
		{
			get { return (EntityField2)EntityFieldFactory.Create(DeviceMasterFieldIndex.DeviceDesc);}
		}
		/// <summary>Creates a new DeviceMasterEntity.DeviceId field instance</summary>
		public static EntityField2 DeviceId
		{
			get { return (EntityField2)EntityFieldFactory.Create(DeviceMasterFieldIndex.DeviceId);}
		}
		/// <summary>Creates a new DeviceMasterEntity.DeviceName field instance</summary>
		public static EntityField2 DeviceName
		{
			get { return (EntityField2)EntityFieldFactory.Create(DeviceMasterFieldIndex.DeviceName);}
		}
		/// <summary>Creates a new DeviceMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(DeviceMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new DeviceMasterEntity.HardwareTypeId field instance</summary>
		public static EntityField2 HardwareTypeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(DeviceMasterFieldIndex.HardwareTypeId);}
		}
		/// <summary>Creates a new DeviceMasterEntity.IpAddress field instance</summary>
		public static EntityField2 IpAddress
		{
			get { return (EntityField2)EntityFieldFactory.Create(DeviceMasterFieldIndex.IpAddress);}
		}
		/// <summary>Creates a new DeviceMasterEntity.IpPrintingPort field instance</summary>
		public static EntityField2 IpPrintingPort
		{
			get { return (EntityField2)EntityFieldFactory.Create(DeviceMasterFieldIndex.IpPrintingPort);}
		}
		/// <summary>Creates a new DeviceMasterEntity.IpStatusPort field instance</summary>
		public static EntityField2 IpStatusPort
		{
			get { return (EntityField2)EntityFieldFactory.Create(DeviceMasterFieldIndex.IpStatusPort);}
		}
		/// <summary>Creates a new DeviceMasterEntity.KpBackupDeviceId field instance</summary>
		public static EntityField2 KpBackupDeviceId
		{
			get { return (EntityField2)EntityFieldFactory.Create(DeviceMasterFieldIndex.KpBackupDeviceId);}
		}
		/// <summary>Creates a new DeviceMasterEntity.KpOptionGrpId field instance</summary>
		public static EntityField2 KpOptionGrpId
		{
			get { return (EntityField2)EntityFieldFactory.Create(DeviceMasterFieldIndex.KpOptionGrpId);}
		}
		/// <summary>Creates a new DeviceMasterEntity.ModelId field instance</summary>
		public static EntityField2 ModelId
		{
			get { return (EntityField2)EntityFieldFactory.Create(DeviceMasterFieldIndex.ModelId);}
		}
		/// <summary>Creates a new DeviceMasterEntity.PortId field instance</summary>
		public static EntityField2 PortId
		{
			get { return (EntityField2)EntityFieldFactory.Create(DeviceMasterFieldIndex.PortId);}
		}
		/// <summary>Creates a new DeviceMasterEntity.PrimaryLanguageId field instance</summary>
		public static EntityField2 PrimaryLanguageId
		{
			get { return (EntityField2)EntityFieldFactory.Create(DeviceMasterFieldIndex.PrimaryLanguageId);}
		}
		/// <summary>Creates a new DeviceMasterEntity.SecondaryLanguageId field instance</summary>
		public static EntityField2 SecondaryLanguageId
		{
			get { return (EntityField2)EntityFieldFactory.Create(DeviceMasterFieldIndex.SecondaryLanguageId);}
		}
		/// <summary>Creates a new DeviceMasterEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(DeviceMasterFieldIndex.StoreId);}
		}
		/// <summary>Creates a new DeviceMasterEntity.XferBaudRate field instance</summary>
		public static EntityField2 XferBaudRate
		{
			get { return (EntityField2)EntityFieldFactory.Create(DeviceMasterFieldIndex.XferBaudRate);}
		}
		/// <summary>Creates a new DeviceMasterEntity.XferDataBits field instance</summary>
		public static EntityField2 XferDataBits
		{
			get { return (EntityField2)EntityFieldFactory.Create(DeviceMasterFieldIndex.XferDataBits);}
		}
		/// <summary>Creates a new DeviceMasterEntity.XferHandshakeId field instance</summary>
		public static EntityField2 XferHandshakeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(DeviceMasterFieldIndex.XferHandshakeId);}
		}
		/// <summary>Creates a new DeviceMasterEntity.XferParityId field instance</summary>
		public static EntityField2 XferParityId
		{
			get { return (EntityField2)EntityFieldFactory.Create(DeviceMasterFieldIndex.XferParityId);}
		}
		/// <summary>Creates a new DeviceMasterEntity.XferStopBits field instance</summary>
		public static EntityField2 XferStopBits
		{
			get { return (EntityField2)EntityFieldFactory.Create(DeviceMasterFieldIndex.XferStopBits);}
		}
	}

	/// <summary>Field Creation Class for entity DiscoupMasterEntity</summary>
	public partial class DiscoupMasterFields
	{
		/// <summary>Creates a new DiscoupMasterEntity.AssocTenderId field instance</summary>
		public static EntityField2 AssocTenderId
		{
			get { return (EntityField2)EntityFieldFactory.Create(DiscoupMasterFieldIndex.AssocTenderId);}
		}
		/// <summary>Creates a new DiscoupMasterEntity.BevRevClassFlag field instance</summary>
		public static EntityField2 BevRevClassFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(DiscoupMasterFieldIndex.BevRevClassFlag);}
		}
		/// <summary>Creates a new DiscoupMasterEntity.DiscountExtraPromptCode field instance</summary>
		public static EntityField2 DiscountExtraPromptCode
		{
			get { return (EntityField2)EntityFieldFactory.Create(DiscoupMasterFieldIndex.DiscountExtraPromptCode);}
		}
		/// <summary>Creates a new DiscoupMasterEntity.DiscoupAbbr1 field instance</summary>
		public static EntityField2 DiscoupAbbr1
		{
			get { return (EntityField2)EntityFieldFactory.Create(DiscoupMasterFieldIndex.DiscoupAbbr1);}
		}
		/// <summary>Creates a new DiscoupMasterEntity.DiscoupAbbr2 field instance</summary>
		public static EntityField2 DiscoupAbbr2
		{
			get { return (EntityField2)EntityFieldFactory.Create(DiscoupMasterFieldIndex.DiscoupAbbr2);}
		}
		/// <summary>Creates a new DiscoupMasterEntity.DiscoupAmt field instance</summary>
		public static EntityField2 DiscoupAmt
		{
			get { return (EntityField2)EntityFieldFactory.Create(DiscoupMasterFieldIndex.DiscoupAmt);}
		}
		/// <summary>Creates a new DiscoupMasterEntity.DiscoupId field instance</summary>
		public static EntityField2 DiscoupId
		{
			get { return (EntityField2)EntityFieldFactory.Create(DiscoupMasterFieldIndex.DiscoupId);}
		}
		/// <summary>Creates a new DiscoupMasterEntity.DiscoupItemLevelCodeId field instance</summary>
		public static EntityField2 DiscoupItemLevelCodeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(DiscoupMasterFieldIndex.DiscoupItemLevelCodeId);}
		}
		/// <summary>Creates a new DiscoupMasterEntity.DiscoupMaxAmt field instance</summary>
		public static EntityField2 DiscoupMaxAmt
		{
			get { return (EntityField2)EntityFieldFactory.Create(DiscoupMasterFieldIndex.DiscoupMaxAmt);}
		}
		/// <summary>Creates a new DiscoupMasterEntity.DiscoupMaxPercent field instance</summary>
		public static EntityField2 DiscoupMaxPercent
		{
			get { return (EntityField2)EntityFieldFactory.Create(DiscoupMasterFieldIndex.DiscoupMaxPercent);}
		}
		/// <summary>Creates a new DiscoupMasterEntity.DiscoupName field instance</summary>
		public static EntityField2 DiscoupName
		{
			get { return (EntityField2)EntityFieldFactory.Create(DiscoupMasterFieldIndex.DiscoupName);}
		}
		/// <summary>Creates a new DiscoupMasterEntity.DiscoupOpenCodeId field instance</summary>
		public static EntityField2 DiscoupOpenCodeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(DiscoupMasterFieldIndex.DiscoupOpenCodeId);}
		}
		/// <summary>Creates a new DiscoupMasterEntity.DiscoupPctAmtCodeId field instance</summary>
		public static EntityField2 DiscoupPctAmtCodeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(DiscoupMasterFieldIndex.DiscoupPctAmtCodeId);}
		}
		/// <summary>Creates a new DiscoupMasterEntity.DiscoupPercent field instance</summary>
		public static EntityField2 DiscoupPercent
		{
			get { return (EntityField2)EntityFieldFactory.Create(DiscoupMasterFieldIndex.DiscoupPercent);}
		}
		/// <summary>Creates a new DiscoupMasterEntity.DiscoupTypeCodeId field instance</summary>
		public static EntityField2 DiscoupTypeCodeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(DiscoupMasterFieldIndex.DiscoupTypeCodeId);}
		}
		/// <summary>Creates a new DiscoupMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(DiscoupMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new DiscoupMasterEntity.ExclusiveFlag field instance</summary>
		public static EntityField2 ExclusiveFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(DiscoupMasterFieldIndex.ExclusiveFlag);}
		}
		/// <summary>Creates a new DiscoupMasterEntity.FoodRevClassFlag field instance</summary>
		public static EntityField2 FoodRevClassFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(DiscoupMasterFieldIndex.FoodRevClassFlag);}
		}
		/// <summary>Creates a new DiscoupMasterEntity.OtherRevClassFlag field instance</summary>
		public static EntityField2 OtherRevClassFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(DiscoupMasterFieldIndex.OtherRevClassFlag);}
		}
		/// <summary>Creates a new DiscoupMasterEntity.PostAcctNo field instance</summary>
		public static EntityField2 PostAcctNo
		{
			get { return (EntityField2)EntityFieldFactory.Create(DiscoupMasterFieldIndex.PostAcctNo);}
		}
		/// <summary>Creates a new DiscoupMasterEntity.ProfitCtrGrpId field instance</summary>
		public static EntityField2 ProfitCtrGrpId
		{
			get { return (EntityField2)EntityFieldFactory.Create(DiscoupMasterFieldIndex.ProfitCtrGrpId);}
		}
		/// <summary>Creates a new DiscoupMasterEntity.PromptExtraDataFlag field instance</summary>
		public static EntityField2 PromptExtraDataFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(DiscoupMasterFieldIndex.PromptExtraDataFlag);}
		}
		/// <summary>Creates a new DiscoupMasterEntity.RoundBasis field instance</summary>
		public static EntityField2 RoundBasis
		{
			get { return (EntityField2)EntityFieldFactory.Create(DiscoupMasterFieldIndex.RoundBasis);}
		}
		/// <summary>Creates a new DiscoupMasterEntity.RoundTypeId field instance</summary>
		public static EntityField2 RoundTypeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(DiscoupMasterFieldIndex.RoundTypeId);}
		}
		/// <summary>Creates a new DiscoupMasterEntity.RowVersion field instance</summary>
		public static EntityField2 RowVersion
		{
			get { return (EntityField2)EntityFieldFactory.Create(DiscoupMasterFieldIndex.RowVersion);}
		}
		/// <summary>Creates a new DiscoupMasterEntity.SecurityId field instance</summary>
		public static EntityField2 SecurityId
		{
			get { return (EntityField2)EntityFieldFactory.Create(DiscoupMasterFieldIndex.SecurityId);}
		}
		/// <summary>Creates a new DiscoupMasterEntity.SodaRevClassFlag field instance</summary>
		public static EntityField2 SodaRevClassFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(DiscoupMasterFieldIndex.SodaRevClassFlag);}
		}
		/// <summary>Creates a new DiscoupMasterEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(DiscoupMasterFieldIndex.StoreId);}
		}
		/// <summary>Creates a new DiscoupMasterEntity.ThreshholdAmt field instance</summary>
		public static EntityField2 ThreshholdAmt
		{
			get { return (EntityField2)EntityFieldFactory.Create(DiscoupMasterFieldIndex.ThreshholdAmt);}
		}
	}

	/// <summary>Field Creation Class for entity EmpGroupMasterEntity</summary>
	public partial class EmpGroupMasterFields
	{
		/// <summary>Creates a new EmpGroupMasterEntity.EmpAutoUpdateFlag field instance</summary>
		public static EntityField2 EmpAutoUpdateFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(EmpGroupMasterFieldIndex.EmpAutoUpdateFlag);}
		}
		/// <summary>Creates a new EmpGroupMasterEntity.EmpGrpAbbr1 field instance</summary>
		public static EntityField2 EmpGrpAbbr1
		{
			get { return (EntityField2)EntityFieldFactory.Create(EmpGroupMasterFieldIndex.EmpGrpAbbr1);}
		}
		/// <summary>Creates a new EmpGroupMasterEntity.EmpGrpAbbr2 field instance</summary>
		public static EntityField2 EmpGrpAbbr2
		{
			get { return (EntityField2)EntityFieldFactory.Create(EmpGroupMasterFieldIndex.EmpGrpAbbr2);}
		}
		/// <summary>Creates a new EmpGroupMasterEntity.EmpGrpId field instance</summary>
		public static EntityField2 EmpGrpId
		{
			get { return (EntityField2)EntityFieldFactory.Create(EmpGroupMasterFieldIndex.EmpGrpId);}
		}
		/// <summary>Creates a new EmpGroupMasterEntity.EmpGrpName field instance</summary>
		public static EntityField2 EmpGrpName
		{
			get { return (EntityField2)EntityFieldFactory.Create(EmpGroupMasterFieldIndex.EmpGrpName);}
		}
		/// <summary>Creates a new EmpGroupMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(EmpGroupMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new EmpGroupMasterEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(EmpGroupMasterFieldIndex.StoreId);}
		}
	}

	/// <summary>Field Creation Class for entity EmpGrpEmpJoinEntity</summary>
	public partial class EmpGrpEmpJoinFields
	{
		/// <summary>Creates a new EmpGrpEmpJoinEntity.EmpGrpId field instance</summary>
		public static EntityField2 EmpGrpId
		{
			get { return (EntityField2)EntityFieldFactory.Create(EmpGrpEmpJoinFieldIndex.EmpGrpId);}
		}
		/// <summary>Creates a new EmpGrpEmpJoinEntity.EmpId field instance</summary>
		public static EntityField2 EmpId
		{
			get { return (EntityField2)EntityFieldFactory.Create(EmpGrpEmpJoinFieldIndex.EmpId);}
		}
		/// <summary>Creates a new EmpGrpEmpJoinEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(EmpGrpEmpJoinFieldIndex.EntId);}
		}
	}

	/// <summary>Field Creation Class for entity EmpJobcodeJoinEntity</summary>
	public partial class EmpJobcodeJoinFields
	{
		/// <summary>Creates a new EmpJobcodeJoinEntity.EmpId field instance</summary>
		public static EntityField2 EmpId
		{
			get { return (EntityField2)EntityFieldFactory.Create(EmpJobcodeJoinFieldIndex.EmpId);}
		}
		/// <summary>Creates a new EmpJobcodeJoinEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(EmpJobcodeJoinFieldIndex.EntId);}
		}
		/// <summary>Creates a new EmpJobcodeJoinEntity.JobcodeAddDt field instance</summary>
		public static EntityField2 JobcodeAddDt
		{
			get { return (EntityField2)EntityFieldFactory.Create(EmpJobcodeJoinFieldIndex.JobcodeAddDt);}
		}
		/// <summary>Creates a new EmpJobcodeJoinEntity.JobcodeId field instance</summary>
		public static EntityField2 JobcodeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(EmpJobcodeJoinFieldIndex.JobcodeId);}
		}
		/// <summary>Creates a new EmpJobcodeJoinEntity.JobcodeLaborRate field instance</summary>
		public static EntityField2 JobcodeLaborRate
		{
			get { return (EntityField2)EntityFieldFactory.Create(EmpJobcodeJoinFieldIndex.JobcodeLaborRate);}
		}
		/// <summary>Creates a new EmpJobcodeJoinEntity.ManagerFlag field instance</summary>
		public static EntityField2 ManagerFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(EmpJobcodeJoinFieldIndex.ManagerFlag);}
		}
		/// <summary>Creates a new EmpJobcodeJoinEntity.MultipleSignonFlag field instance</summary>
		public static EntityField2 MultipleSignonFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(EmpJobcodeJoinFieldIndex.MultipleSignonFlag);}
		}
		/// <summary>Creates a new EmpJobcodeJoinEntity.PrimaryJobcodeFlag field instance</summary>
		public static EntityField2 PrimaryJobcodeFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(EmpJobcodeJoinFieldIndex.PrimaryJobcodeFlag);}
		}
	}

	/// <summary>Field Creation Class for entity EmpMasterEntity</summary>
	public partial class EmpMasterFields
	{
		/// <summary>Creates a new EmpMasterEntity.DataControlGroupId field instance</summary>
		public static EntityField2 DataControlGroupId
		{
			get { return (EntityField2)EntityFieldFactory.Create(EmpMasterFieldIndex.DataControlGroupId);}
		}
		/// <summary>Creates a new EmpMasterEntity.DefaultLanguageId field instance</summary>
		public static EntityField2 DefaultLanguageId
		{
			get { return (EntityField2)EntityFieldFactory.Create(EmpMasterFieldIndex.DefaultLanguageId);}
		}
		/// <summary>Creates a new EmpMasterEntity.EmpBirthdate field instance</summary>
		public static EntityField2 EmpBirthdate
		{
			get { return (EntityField2)EntityFieldFactory.Create(EmpMasterFieldIndex.EmpBirthdate);}
		}
		/// <summary>Creates a new EmpMasterEntity.EmpCardNo field instance</summary>
		public static EntityField2 EmpCardNo
		{
			get { return (EntityField2)EntityFieldFactory.Create(EmpMasterFieldIndex.EmpCardNo);}
		}
		/// <summary>Creates a new EmpMasterEntity.EmpCity field instance</summary>
		public static EntityField2 EmpCity
		{
			get { return (EntityField2)EntityFieldFactory.Create(EmpMasterFieldIndex.EmpCity);}
		}
		/// <summary>Creates a new EmpMasterEntity.EmpCountry field instance</summary>
		public static EntityField2 EmpCountry
		{
			get { return (EntityField2)EntityFieldFactory.Create(EmpMasterFieldIndex.EmpCountry);}
		}
		/// <summary>Creates a new EmpMasterEntity.EmpEmergencyTel field instance</summary>
		public static EntityField2 EmpEmergencyTel
		{
			get { return (EntityField2)EntityFieldFactory.Create(EmpMasterFieldIndex.EmpEmergencyTel);}
		}
		/// <summary>Creates a new EmpMasterEntity.EmpFirstName field instance</summary>
		public static EntityField2 EmpFirstName
		{
			get { return (EntityField2)EntityFieldFactory.Create(EmpMasterFieldIndex.EmpFirstName);}
		}
		/// <summary>Creates a new EmpMasterEntity.EmpHireDt field instance</summary>
		public static EntityField2 EmpHireDt
		{
			get { return (EntityField2)EntityFieldFactory.Create(EmpMasterFieldIndex.EmpHireDt);}
		}
		/// <summary>Creates a new EmpMasterEntity.EmpId field instance</summary>
		public static EntityField2 EmpId
		{
			get { return (EntityField2)EntityFieldFactory.Create(EmpMasterFieldIndex.EmpId);}
		}
		/// <summary>Creates a new EmpMasterEntity.EmpLastName field instance</summary>
		public static EntityField2 EmpLastName
		{
			get { return (EntityField2)EntityFieldFactory.Create(EmpMasterFieldIndex.EmpLastName);}
		}
		/// <summary>Creates a new EmpMasterEntity.EmpLastReviewDt field instance</summary>
		public static EntityField2 EmpLastReviewDt
		{
			get { return (EntityField2)EntityFieldFactory.Create(EmpMasterFieldIndex.EmpLastReviewDt);}
		}
		/// <summary>Creates a new EmpMasterEntity.EmpNextReviewDt field instance</summary>
		public static EntityField2 EmpNextReviewDt
		{
			get { return (EntityField2)EntityFieldFactory.Create(EmpMasterFieldIndex.EmpNextReviewDt);}
		}
		/// <summary>Creates a new EmpMasterEntity.EmpPassword field instance</summary>
		public static EntityField2 EmpPassword
		{
			get { return (EntityField2)EntityFieldFactory.Create(EmpMasterFieldIndex.EmpPassword);}
		}
		/// <summary>Creates a new EmpMasterEntity.EmpPosName field instance</summary>
		public static EntityField2 EmpPosName
		{
			get { return (EntityField2)EntityFieldFactory.Create(EmpMasterFieldIndex.EmpPosName);}
		}
		/// <summary>Creates a new EmpMasterEntity.EmpSsn field instance</summary>
		public static EntityField2 EmpSsn
		{
			get { return (EntityField2)EntityFieldFactory.Create(EmpMasterFieldIndex.EmpSsn);}
		}
		/// <summary>Creates a new EmpMasterEntity.EmpState field instance</summary>
		public static EntityField2 EmpState
		{
			get { return (EntityField2)EntityFieldFactory.Create(EmpMasterFieldIndex.EmpState);}
		}
		/// <summary>Creates a new EmpMasterEntity.EmpStreet1 field instance</summary>
		public static EntityField2 EmpStreet1
		{
			get { return (EntityField2)EntityFieldFactory.Create(EmpMasterFieldIndex.EmpStreet1);}
		}
		/// <summary>Creates a new EmpMasterEntity.EmpStreet2 field instance</summary>
		public static EntityField2 EmpStreet2
		{
			get { return (EntityField2)EntityFieldFactory.Create(EmpMasterFieldIndex.EmpStreet2);}
		}
		/// <summary>Creates a new EmpMasterEntity.EmpTel field instance</summary>
		public static EntityField2 EmpTel
		{
			get { return (EntityField2)EntityFieldFactory.Create(EmpMasterFieldIndex.EmpTel);}
		}
		/// <summary>Creates a new EmpMasterEntity.EmpTerminateDt field instance</summary>
		public static EntityField2 EmpTerminateDt
		{
			get { return (EntityField2)EntityFieldFactory.Create(EmpMasterFieldIndex.EmpTerminateDt);}
		}
		/// <summary>Creates a new EmpMasterEntity.EmpZip field instance</summary>
		public static EntityField2 EmpZip
		{
			get { return (EntityField2)EntityFieldFactory.Create(EmpMasterFieldIndex.EmpZip);}
		}
		/// <summary>Creates a new EmpMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(EmpMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new EmpMasterEntity.RowVersion field instance</summary>
		public static EntityField2 RowVersion
		{
			get { return (EntityField2)EntityFieldFactory.Create(EmpMasterFieldIndex.RowVersion);}
		}
		/// <summary>Creates a new EmpMasterEntity.SecondaryId field instance</summary>
		public static EntityField2 SecondaryId
		{
			get { return (EntityField2)EntityFieldFactory.Create(EmpMasterFieldIndex.SecondaryId);}
		}
		/// <summary>Creates a new EmpMasterEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(EmpMasterFieldIndex.StoreId);}
		}
		/// <summary>Creates a new EmpMasterEntity.SupervisorEmpId field instance</summary>
		public static EntityField2 SupervisorEmpId
		{
			get { return (EntityField2)EntityFieldFactory.Create(EmpMasterFieldIndex.SupervisorEmpId);}
		}
	}

	/// <summary>Field Creation Class for entity FuncBttnTextJoinEntity</summary>
	public partial class FuncBttnTextJoinFields
	{
		/// <summary>Creates a new FuncBttnTextJoinEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(FuncBttnTextJoinFieldIndex.EntId);}
		}
		/// <summary>Creates a new FuncBttnTextJoinEntity.FuncbttnId field instance</summary>
		public static EntityField2 FuncbttnId
		{
			get { return (EntityField2)EntityFieldFactory.Create(FuncBttnTextJoinFieldIndex.FuncbttnId);}
		}
		/// <summary>Creates a new FuncBttnTextJoinEntity.FuncbttnText field instance</summary>
		public static EntityField2 FuncbttnText
		{
			get { return (EntityField2)EntityFieldFactory.Create(FuncBttnTextJoinFieldIndex.FuncbttnText);}
		}
	}

	/// <summary>Field Creation Class for entity Ga4680ExportParmMasterEntity</summary>
	public partial class Ga4680ExportParmMasterFields
	{
		/// <summary>Creates a new Ga4680ExportParmMasterEntity.BusinessDayOffset field instance</summary>
		public static EntityField2 BusinessDayOffset
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ExportParmMasterFieldIndex.BusinessDayOffset);}
		}
		/// <summary>Creates a new Ga4680ExportParmMasterEntity.BusinessDayOptionId field instance</summary>
		public static EntityField2 BusinessDayOptionId
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ExportParmMasterFieldIndex.BusinessDayOptionId);}
		}
		/// <summary>Creates a new Ga4680ExportParmMasterEntity.DelimiterValue field instance</summary>
		public static EntityField2 DelimiterValue
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ExportParmMasterFieldIndex.DelimiterValue);}
		}
		/// <summary>Creates a new Ga4680ExportParmMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ExportParmMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new Ga4680ExportParmMasterEntity.FileFormatId field instance</summary>
		public static EntityField2 FileFormatId
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ExportParmMasterFieldIndex.FileFormatId);}
		}
		/// <summary>Creates a new Ga4680ExportParmMasterEntity.Ga1ExportPath field instance</summary>
		public static EntityField2 Ga1ExportPath
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ExportParmMasterFieldIndex.Ga1ExportPath);}
		}
		/// <summary>Creates a new Ga4680ExportParmMasterEntity.Ga1RequiredFlag field instance</summary>
		public static EntityField2 Ga1RequiredFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ExportParmMasterFieldIndex.Ga1RequiredFlag);}
		}
		/// <summary>Creates a new Ga4680ExportParmMasterEntity.Ga2ExportPath field instance</summary>
		public static EntityField2 Ga2ExportPath
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ExportParmMasterFieldIndex.Ga2ExportPath);}
		}
		/// <summary>Creates a new Ga4680ExportParmMasterEntity.Ga2RequiredFlag field instance</summary>
		public static EntityField2 Ga2RequiredFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ExportParmMasterFieldIndex.Ga2RequiredFlag);}
		}
		/// <summary>Creates a new Ga4680ExportParmMasterEntity.Ga3ExportPath field instance</summary>
		public static EntityField2 Ga3ExportPath
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ExportParmMasterFieldIndex.Ga3ExportPath);}
		}
		/// <summary>Creates a new Ga4680ExportParmMasterEntity.Ga3RequiredFlag field instance</summary>
		public static EntityField2 Ga3RequiredFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ExportParmMasterFieldIndex.Ga3RequiredFlag);}
		}
		/// <summary>Creates a new Ga4680ExportParmMasterEntity.Ga4ExportPath field instance</summary>
		public static EntityField2 Ga4ExportPath
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ExportParmMasterFieldIndex.Ga4ExportPath);}
		}
		/// <summary>Creates a new Ga4680ExportParmMasterEntity.Ga4RequiredFlag field instance</summary>
		public static EntityField2 Ga4RequiredFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ExportParmMasterFieldIndex.Ga4RequiredFlag);}
		}
		/// <summary>Creates a new Ga4680ExportParmMasterEntity.Ga5ExportPath field instance</summary>
		public static EntityField2 Ga5ExportPath
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ExportParmMasterFieldIndex.Ga5ExportPath);}
		}
		/// <summary>Creates a new Ga4680ExportParmMasterEntity.Ga5RequiredFlag field instance</summary>
		public static EntityField2 Ga5RequiredFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ExportParmMasterFieldIndex.Ga5RequiredFlag);}
		}
		/// <summary>Creates a new Ga4680ExportParmMasterEntity.Ga6ExportPath field instance</summary>
		public static EntityField2 Ga6ExportPath
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ExportParmMasterFieldIndex.Ga6ExportPath);}
		}
		/// <summary>Creates a new Ga4680ExportParmMasterEntity.Ga6RequiredFlag field instance</summary>
		public static EntityField2 Ga6RequiredFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ExportParmMasterFieldIndex.Ga6RequiredFlag);}
		}
		/// <summary>Creates a new Ga4680ExportParmMasterEntity.Ga7ExportPath field instance</summary>
		public static EntityField2 Ga7ExportPath
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ExportParmMasterFieldIndex.Ga7ExportPath);}
		}
		/// <summary>Creates a new Ga4680ExportParmMasterEntity.Ga7RequiredFlag field instance</summary>
		public static EntityField2 Ga7RequiredFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ExportParmMasterFieldIndex.Ga7RequiredFlag);}
		}
		/// <summary>Creates a new Ga4680ExportParmMasterEntity.Ga8ExportPath field instance</summary>
		public static EntityField2 Ga8ExportPath
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ExportParmMasterFieldIndex.Ga8ExportPath);}
		}
		/// <summary>Creates a new Ga4680ExportParmMasterEntity.Ga8RequiredFlag field instance</summary>
		public static EntityField2 Ga8RequiredFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ExportParmMasterFieldIndex.Ga8RequiredFlag);}
		}
		/// <summary>Creates a new Ga4680ExportParmMasterEntity.Ga9ExportPath field instance</summary>
		public static EntityField2 Ga9ExportPath
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ExportParmMasterFieldIndex.Ga9ExportPath);}
		}
		/// <summary>Creates a new Ga4680ExportParmMasterEntity.Ga9RequiredFlag field instance</summary>
		public static EntityField2 Ga9RequiredFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ExportParmMasterFieldIndex.Ga9RequiredFlag);}
		}
		/// <summary>Creates a new Ga4680ExportParmMasterEntity.GaExportId field instance</summary>
		public static EntityField2 GaExportId
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ExportParmMasterFieldIndex.GaExportId);}
		}
		/// <summary>Creates a new Ga4680ExportParmMasterEntity.LocationCode field instance</summary>
		public static EntityField2 LocationCode
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ExportParmMasterFieldIndex.LocationCode);}
		}
		/// <summary>Creates a new Ga4680ExportParmMasterEntity.NumDays field instance</summary>
		public static EntityField2 NumDays
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ExportParmMasterFieldIndex.NumDays);}
		}
	}

	/// <summary>Field Creation Class for entity Ga4680ImportParmMasterEntity</summary>
	public partial class Ga4680ImportParmMasterFields
	{
		/// <summary>Creates a new Ga4680ImportParmMasterEntity.DelimiterRequiredFlag field instance</summary>
		public static EntityField2 DelimiterRequiredFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ImportParmMasterFieldIndex.DelimiterRequiredFlag);}
		}
		/// <summary>Creates a new Ga4680ImportParmMasterEntity.DelimiterValue field instance</summary>
		public static EntityField2 DelimiterValue
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ImportParmMasterFieldIndex.DelimiterValue);}
		}
		/// <summary>Creates a new Ga4680ImportParmMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ImportParmMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new Ga4680ImportParmMasterEntity.Ga1ImportPath field instance</summary>
		public static EntityField2 Ga1ImportPath
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ImportParmMasterFieldIndex.Ga1ImportPath);}
		}
		/// <summary>Creates a new Ga4680ImportParmMasterEntity.Ga1PreimportOptionId field instance</summary>
		public static EntityField2 Ga1PreimportOptionId
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ImportParmMasterFieldIndex.Ga1PreimportOptionId);}
		}
		/// <summary>Creates a new Ga4680ImportParmMasterEntity.Ga1RequiredFlag field instance</summary>
		public static EntityField2 Ga1RequiredFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ImportParmMasterFieldIndex.Ga1RequiredFlag);}
		}
		/// <summary>Creates a new Ga4680ImportParmMasterEntity.Ga2ImportPath field instance</summary>
		public static EntityField2 Ga2ImportPath
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ImportParmMasterFieldIndex.Ga2ImportPath);}
		}
		/// <summary>Creates a new Ga4680ImportParmMasterEntity.Ga2PreimportOptionId field instance</summary>
		public static EntityField2 Ga2PreimportOptionId
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ImportParmMasterFieldIndex.Ga2PreimportOptionId);}
		}
		/// <summary>Creates a new Ga4680ImportParmMasterEntity.Ga2RequiredFlag field instance</summary>
		public static EntityField2 Ga2RequiredFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ImportParmMasterFieldIndex.Ga2RequiredFlag);}
		}
		/// <summary>Creates a new Ga4680ImportParmMasterEntity.Ga3ImportPath field instance</summary>
		public static EntityField2 Ga3ImportPath
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ImportParmMasterFieldIndex.Ga3ImportPath);}
		}
		/// <summary>Creates a new Ga4680ImportParmMasterEntity.Ga3PreimportOptionId field instance</summary>
		public static EntityField2 Ga3PreimportOptionId
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ImportParmMasterFieldIndex.Ga3PreimportOptionId);}
		}
		/// <summary>Creates a new Ga4680ImportParmMasterEntity.Ga3RequiredFlag field instance</summary>
		public static EntityField2 Ga3RequiredFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ImportParmMasterFieldIndex.Ga3RequiredFlag);}
		}
		/// <summary>Creates a new Ga4680ImportParmMasterEntity.Ga4ImportPath field instance</summary>
		public static EntityField2 Ga4ImportPath
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ImportParmMasterFieldIndex.Ga4ImportPath);}
		}
		/// <summary>Creates a new Ga4680ImportParmMasterEntity.Ga4PreimportOptionId field instance</summary>
		public static EntityField2 Ga4PreimportOptionId
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ImportParmMasterFieldIndex.Ga4PreimportOptionId);}
		}
		/// <summary>Creates a new Ga4680ImportParmMasterEntity.Ga4RequiredFlag field instance</summary>
		public static EntityField2 Ga4RequiredFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ImportParmMasterFieldIndex.Ga4RequiredFlag);}
		}
		/// <summary>Creates a new Ga4680ImportParmMasterEntity.Ga5ImportPath field instance</summary>
		public static EntityField2 Ga5ImportPath
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ImportParmMasterFieldIndex.Ga5ImportPath);}
		}
		/// <summary>Creates a new Ga4680ImportParmMasterEntity.Ga5PreimportOptionId field instance</summary>
		public static EntityField2 Ga5PreimportOptionId
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ImportParmMasterFieldIndex.Ga5PreimportOptionId);}
		}
		/// <summary>Creates a new Ga4680ImportParmMasterEntity.Ga5RequiredFlag field instance</summary>
		public static EntityField2 Ga5RequiredFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ImportParmMasterFieldIndex.Ga5RequiredFlag);}
		}
		/// <summary>Creates a new Ga4680ImportParmMasterEntity.Ga6ImportPath field instance</summary>
		public static EntityField2 Ga6ImportPath
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ImportParmMasterFieldIndex.Ga6ImportPath);}
		}
		/// <summary>Creates a new Ga4680ImportParmMasterEntity.Ga6PreimportOptionId field instance</summary>
		public static EntityField2 Ga6PreimportOptionId
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ImportParmMasterFieldIndex.Ga6PreimportOptionId);}
		}
		/// <summary>Creates a new Ga4680ImportParmMasterEntity.Ga6RequiredFlag field instance</summary>
		public static EntityField2 Ga6RequiredFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ImportParmMasterFieldIndex.Ga6RequiredFlag);}
		}
		/// <summary>Creates a new Ga4680ImportParmMasterEntity.Ga7ImportPath field instance</summary>
		public static EntityField2 Ga7ImportPath
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ImportParmMasterFieldIndex.Ga7ImportPath);}
		}
		/// <summary>Creates a new Ga4680ImportParmMasterEntity.Ga7PreimportOptionId field instance</summary>
		public static EntityField2 Ga7PreimportOptionId
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ImportParmMasterFieldIndex.Ga7PreimportOptionId);}
		}
		/// <summary>Creates a new Ga4680ImportParmMasterEntity.Ga7RequiredFlag field instance</summary>
		public static EntityField2 Ga7RequiredFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ImportParmMasterFieldIndex.Ga7RequiredFlag);}
		}
		/// <summary>Creates a new Ga4680ImportParmMasterEntity.Ga8ImportPath field instance</summary>
		public static EntityField2 Ga8ImportPath
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ImportParmMasterFieldIndex.Ga8ImportPath);}
		}
		/// <summary>Creates a new Ga4680ImportParmMasterEntity.Ga8PreimportOptionId field instance</summary>
		public static EntityField2 Ga8PreimportOptionId
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ImportParmMasterFieldIndex.Ga8PreimportOptionId);}
		}
		/// <summary>Creates a new Ga4680ImportParmMasterEntity.Ga8RequiredFlag field instance</summary>
		public static EntityField2 Ga8RequiredFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ImportParmMasterFieldIndex.Ga8RequiredFlag);}
		}
		/// <summary>Creates a new Ga4680ImportParmMasterEntity.Ga9ImportPath field instance</summary>
		public static EntityField2 Ga9ImportPath
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ImportParmMasterFieldIndex.Ga9ImportPath);}
		}
		/// <summary>Creates a new Ga4680ImportParmMasterEntity.Ga9PreimportOptionId field instance</summary>
		public static EntityField2 Ga9PreimportOptionId
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ImportParmMasterFieldIndex.Ga9PreimportOptionId);}
		}
		/// <summary>Creates a new Ga4680ImportParmMasterEntity.Ga9RequiredFlag field instance</summary>
		public static EntityField2 Ga9RequiredFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ImportParmMasterFieldIndex.Ga9RequiredFlag);}
		}
		/// <summary>Creates a new Ga4680ImportParmMasterEntity.GaImportId field instance</summary>
		public static EntityField2 GaImportId
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ImportParmMasterFieldIndex.GaImportId);}
		}
		/// <summary>Creates a new Ga4680ImportParmMasterEntity.PreimportOptionId field instance</summary>
		public static EntityField2 PreimportOptionId
		{
			get { return (EntityField2)EntityFieldFactory.Create(Ga4680ImportParmMasterFieldIndex.PreimportOptionId);}
		}
	}

	/// <summary>Field Creation Class for entity GeneralLedgerMapMasterEntity</summary>
	public partial class GeneralLedgerMapMasterFields
	{
		/// <summary>Creates a new GeneralLedgerMapMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(GeneralLedgerMapMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new GeneralLedgerMapMasterEntity.GlAccountNo field instance</summary>
		public static EntityField2 GlAccountNo
		{
			get { return (EntityField2)EntityFieldFactory.Create(GeneralLedgerMapMasterFieldIndex.GlAccountNo);}
		}
		/// <summary>Creates a new GeneralLedgerMapMasterEntity.GlClassId field instance</summary>
		public static EntityField2 GlClassId
		{
			get { return (EntityField2)EntityFieldFactory.Create(GeneralLedgerMapMasterFieldIndex.GlClassId);}
		}
		/// <summary>Creates a new GeneralLedgerMapMasterEntity.GlMapGenId field instance</summary>
		public static EntityField2 GlMapGenId
		{
			get { return (EntityField2)EntityFieldFactory.Create(GeneralLedgerMapMasterFieldIndex.GlMapGenId);}
		}
		/// <summary>Creates a new GeneralLedgerMapMasterEntity.Parm1ObjectId field instance</summary>
		public static EntityField2 Parm1ObjectId
		{
			get { return (EntityField2)EntityFieldFactory.Create(GeneralLedgerMapMasterFieldIndex.Parm1ObjectId);}
		}
		/// <summary>Creates a new GeneralLedgerMapMasterEntity.Parm2ObjectId field instance</summary>
		public static EntityField2 Parm2ObjectId
		{
			get { return (EntityField2)EntityFieldFactory.Create(GeneralLedgerMapMasterFieldIndex.Parm2ObjectId);}
		}
		/// <summary>Creates a new GeneralLedgerMapMasterEntity.ProfitCenterId field instance</summary>
		public static EntityField2 ProfitCenterId
		{
			get { return (EntityField2)EntityFieldFactory.Create(GeneralLedgerMapMasterFieldIndex.ProfitCenterId);}
		}
		/// <summary>Creates a new GeneralLedgerMapMasterEntity.ValidPermutationFlag field instance</summary>
		public static EntityField2 ValidPermutationFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(GeneralLedgerMapMasterFieldIndex.ValidPermutationFlag);}
		}
	}

	/// <summary>Field Creation Class for entity GiftCardMasterEntity</summary>
	public partial class GiftCardMasterFields
	{
		/// <summary>Creates a new GiftCardMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(GiftCardMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new GiftCardMasterEntity.GiftCardAbbr1 field instance</summary>
		public static EntityField2 GiftCardAbbr1
		{
			get { return (EntityField2)EntityFieldFactory.Create(GiftCardMasterFieldIndex.GiftCardAbbr1);}
		}
		/// <summary>Creates a new GiftCardMasterEntity.GiftCardAbbr2 field instance</summary>
		public static EntityField2 GiftCardAbbr2
		{
			get { return (EntityField2)EntityFieldFactory.Create(GiftCardMasterFieldIndex.GiftCardAbbr2);}
		}
		/// <summary>Creates a new GiftCardMasterEntity.GiftCardAmt field instance</summary>
		public static EntityField2 GiftCardAmt
		{
			get { return (EntityField2)EntityFieldFactory.Create(GiftCardMasterFieldIndex.GiftCardAmt);}
		}
		/// <summary>Creates a new GiftCardMasterEntity.GiftCardId field instance</summary>
		public static EntityField2 GiftCardId
		{
			get { return (EntityField2)EntityFieldFactory.Create(GiftCardMasterFieldIndex.GiftCardId);}
		}
		/// <summary>Creates a new GiftCardMasterEntity.GiftCardMaxAmt field instance</summary>
		public static EntityField2 GiftCardMaxAmt
		{
			get { return (EntityField2)EntityFieldFactory.Create(GiftCardMasterFieldIndex.GiftCardMaxAmt);}
		}
		/// <summary>Creates a new GiftCardMasterEntity.GiftCardMinAmt field instance</summary>
		public static EntityField2 GiftCardMinAmt
		{
			get { return (EntityField2)EntityFieldFactory.Create(GiftCardMasterFieldIndex.GiftCardMinAmt);}
		}
		/// <summary>Creates a new GiftCardMasterEntity.GiftCardName field instance</summary>
		public static EntityField2 GiftCardName
		{
			get { return (EntityField2)EntityFieldFactory.Create(GiftCardMasterFieldIndex.GiftCardName);}
		}
		/// <summary>Creates a new GiftCardMasterEntity.GiftCardOpenCodeId field instance</summary>
		public static EntityField2 GiftCardOpenCodeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(GiftCardMasterFieldIndex.GiftCardOpenCodeId);}
		}
		/// <summary>Creates a new GiftCardMasterEntity.MenuItemId field instance</summary>
		public static EntityField2 MenuItemId
		{
			get { return (EntityField2)EntityFieldFactory.Create(GiftCardMasterFieldIndex.MenuItemId);}
		}
		/// <summary>Creates a new GiftCardMasterEntity.NodeId field instance</summary>
		public static EntityField2 NodeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(GiftCardMasterFieldIndex.NodeId);}
		}
	}

	/// <summary>Field Creation Class for entity GratuityCatMasterEntity</summary>
	public partial class GratuityCatMasterFields
	{
		/// <summary>Creates a new GratuityCatMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(GratuityCatMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new GratuityCatMasterEntity.GratCatAbbr1 field instance</summary>
		public static EntityField2 GratCatAbbr1
		{
			get { return (EntityField2)EntityFieldFactory.Create(GratuityCatMasterFieldIndex.GratCatAbbr1);}
		}
		/// <summary>Creates a new GratuityCatMasterEntity.GratCatAbbr2 field instance</summary>
		public static EntityField2 GratCatAbbr2
		{
			get { return (EntityField2)EntityFieldFactory.Create(GratuityCatMasterFieldIndex.GratCatAbbr2);}
		}
		/// <summary>Creates a new GratuityCatMasterEntity.GratCatId field instance</summary>
		public static EntityField2 GratCatId
		{
			get { return (EntityField2)EntityFieldFactory.Create(GratuityCatMasterFieldIndex.GratCatId);}
		}
		/// <summary>Creates a new GratuityCatMasterEntity.GratCatName field instance</summary>
		public static EntityField2 GratCatName
		{
			get { return (EntityField2)EntityFieldFactory.Create(GratuityCatMasterFieldIndex.GratCatName);}
		}
		/// <summary>Creates a new GratuityCatMasterEntity.GratCatSvsChgFlag field instance</summary>
		public static EntityField2 GratCatSvsChgFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(GratuityCatMasterFieldIndex.GratCatSvsChgFlag);}
		}
		/// <summary>Creates a new GratuityCatMasterEntity.LoyaltyEarnEligibleFlag field instance</summary>
		public static EntityField2 LoyaltyEarnEligibleFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(GratuityCatMasterFieldIndex.LoyaltyEarnEligibleFlag);}
		}
		/// <summary>Creates a new GratuityCatMasterEntity.LoyaltyRedeemEligibleFlag field instance</summary>
		public static EntityField2 LoyaltyRedeemEligibleFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(GratuityCatMasterFieldIndex.LoyaltyRedeemEligibleFlag);}
		}
		/// <summary>Creates a new GratuityCatMasterEntity.RowVersion field instance</summary>
		public static EntityField2 RowVersion
		{
			get { return (EntityField2)EntityFieldFactory.Create(GratuityCatMasterFieldIndex.RowVersion);}
		}
		/// <summary>Creates a new GratuityCatMasterEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(GratuityCatMasterFieldIndex.StoreId);}
		}
	}

	/// <summary>Field Creation Class for entity GratuityMasterEntity</summary>
	public partial class GratuityMasterFields
	{
		/// <summary>Creates a new GratuityMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(GratuityMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new GratuityMasterEntity.GratAbbr1 field instance</summary>
		public static EntityField2 GratAbbr1
		{
			get { return (EntityField2)EntityFieldFactory.Create(GratuityMasterFieldIndex.GratAbbr1);}
		}
		/// <summary>Creates a new GratuityMasterEntity.GratAbbr2 field instance</summary>
		public static EntityField2 GratAbbr2
		{
			get { return (EntityField2)EntityFieldFactory.Create(GratuityMasterFieldIndex.GratAbbr2);}
		}
		/// <summary>Creates a new GratuityMasterEntity.GratAmt field instance</summary>
		public static EntityField2 GratAmt
		{
			get { return (EntityField2)EntityFieldFactory.Create(GratuityMasterFieldIndex.GratAmt);}
		}
		/// <summary>Creates a new GratuityMasterEntity.GratCatId field instance</summary>
		public static EntityField2 GratCatId
		{
			get { return (EntityField2)EntityFieldFactory.Create(GratuityMasterFieldIndex.GratCatId);}
		}
		/// <summary>Creates a new GratuityMasterEntity.GratId field instance</summary>
		public static EntityField2 GratId
		{
			get { return (EntityField2)EntityFieldFactory.Create(GratuityMasterFieldIndex.GratId);}
		}
		/// <summary>Creates a new GratuityMasterEntity.GratName field instance</summary>
		public static EntityField2 GratName
		{
			get { return (EntityField2)EntityFieldFactory.Create(GratuityMasterFieldIndex.GratName);}
		}
		/// <summary>Creates a new GratuityMasterEntity.GratPercent field instance</summary>
		public static EntityField2 GratPercent
		{
			get { return (EntityField2)EntityFieldFactory.Create(GratuityMasterFieldIndex.GratPercent);}
		}
		/// <summary>Creates a new GratuityMasterEntity.GratPerCoverAmt field instance</summary>
		public static EntityField2 GratPerCoverAmt
		{
			get { return (EntityField2)EntityFieldFactory.Create(GratuityMasterFieldIndex.GratPerCoverAmt);}
		}
		/// <summary>Creates a new GratuityMasterEntity.GratRecipientCodeId field instance</summary>
		public static EntityField2 GratRecipientCodeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(GratuityMasterFieldIndex.GratRecipientCodeId);}
		}
		/// <summary>Creates a new GratuityMasterEntity.GratRecipientEmpId field instance</summary>
		public static EntityField2 GratRecipientEmpId
		{
			get { return (EntityField2)EntityFieldFactory.Create(GratuityMasterFieldIndex.GratRecipientEmpId);}
		}
		/// <summary>Creates a new GratuityMasterEntity.GratRemovableFlag field instance</summary>
		public static EntityField2 GratRemovableFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(GratuityMasterFieldIndex.GratRemovableFlag);}
		}
		/// <summary>Creates a new GratuityMasterEntity.GratTermPayFlag field instance</summary>
		public static EntityField2 GratTermPayFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(GratuityMasterFieldIndex.GratTermPayFlag);}
		}
		/// <summary>Creates a new GratuityMasterEntity.GratuityIsInclusiveFlag field instance</summary>
		public static EntityField2 GratuityIsInclusiveFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(GratuityMasterFieldIndex.GratuityIsInclusiveFlag);}
		}
		/// <summary>Creates a new GratuityMasterEntity.InclusiveGratuityConvertedFlag field instance</summary>
		public static EntityField2 InclusiveGratuityConvertedFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(GratuityMasterFieldIndex.InclusiveGratuityConvertedFlag);}
		}
		/// <summary>Creates a new GratuityMasterEntity.MaxCoverThreshhold field instance</summary>
		public static EntityField2 MaxCoverThreshhold
		{
			get { return (EntityField2)EntityFieldFactory.Create(GratuityMasterFieldIndex.MaxCoverThreshhold);}
		}
		/// <summary>Creates a new GratuityMasterEntity.MinCoverThreshhold field instance</summary>
		public static EntityField2 MinCoverThreshhold
		{
			get { return (EntityField2)EntityFieldFactory.Create(GratuityMasterFieldIndex.MinCoverThreshhold);}
		}
		/// <summary>Creates a new GratuityMasterEntity.PercentOrAmountFlag field instance</summary>
		public static EntityField2 PercentOrAmountFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(GratuityMasterFieldIndex.PercentOrAmountFlag);}
		}
		/// <summary>Creates a new GratuityMasterEntity.RoundBasis field instance</summary>
		public static EntityField2 RoundBasis
		{
			get { return (EntityField2)EntityFieldFactory.Create(GratuityMasterFieldIndex.RoundBasis);}
		}
		/// <summary>Creates a new GratuityMasterEntity.RoundTypeId field instance</summary>
		public static EntityField2 RoundTypeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(GratuityMasterFieldIndex.RoundTypeId);}
		}
		/// <summary>Creates a new GratuityMasterEntity.RowVersion field instance</summary>
		public static EntityField2 RowVersion
		{
			get { return (EntityField2)EntityFieldFactory.Create(GratuityMasterFieldIndex.RowVersion);}
		}
		/// <summary>Creates a new GratuityMasterEntity.SalesCalculationOnNetOrGrossFlag field instance</summary>
		public static EntityField2 SalesCalculationOnNetOrGrossFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(GratuityMasterFieldIndex.SalesCalculationOnNetOrGrossFlag);}
		}
		/// <summary>Creates a new GratuityMasterEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(GratuityMasterFieldIndex.StoreId);}
		}
	}

	/// <summary>Field Creation Class for entity IdMasterEntity</summary>
	public partial class IdMasterFields
	{
		/// <summary>Creates a new IdMasterEntity.CorpId field instance</summary>
		public static EntityField2 CorpId
		{
			get { return (EntityField2)EntityFieldFactory.Create(IdMasterFieldIndex.CorpId);}
		}
		/// <summary>Creates a new IdMasterEntity.DivId field instance</summary>
		public static EntityField2 DivId
		{
			get { return (EntityField2)EntityFieldFactory.Create(IdMasterFieldIndex.DivId);}
		}
		/// <summary>Creates a new IdMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(IdMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new IdMasterEntity.LastUsedId field instance</summary>
		public static EntityField2 LastUsedId
		{
			get { return (EntityField2)EntityFieldFactory.Create(IdMasterFieldIndex.LastUsedId);}
		}
		/// <summary>Creates a new IdMasterEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(IdMasterFieldIndex.StoreId);}
		}
		/// <summary>Creates a new IdMasterEntity.TableName field instance</summary>
		public static EntityField2 TableName
		{
			get { return (EntityField2)EntityFieldFactory.Create(IdMasterFieldIndex.TableName);}
		}
	}

	/// <summary>Field Creation Class for entity ImageLibraryMasterEntity</summary>
	public partial class ImageLibraryMasterFields
	{
		/// <summary>Creates a new ImageLibraryMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ImageLibraryMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new ImageLibraryMasterEntity.Image field instance</summary>
		public static EntityField2 Image
		{
			get { return (EntityField2)EntityFieldFactory.Create(ImageLibraryMasterFieldIndex.Image);}
		}
		/// <summary>Creates a new ImageLibraryMasterEntity.ImageExtension field instance</summary>
		public static EntityField2 ImageExtension
		{
			get { return (EntityField2)EntityFieldFactory.Create(ImageLibraryMasterFieldIndex.ImageExtension);}
		}
		/// <summary>Creates a new ImageLibraryMasterEntity.ImageId field instance</summary>
		public static EntityField2 ImageId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ImageLibraryMasterFieldIndex.ImageId);}
		}
		/// <summary>Creates a new ImageLibraryMasterEntity.ImageName field instance</summary>
		public static EntityField2 ImageName
		{
			get { return (EntityField2)EntityFieldFactory.Create(ImageLibraryMasterFieldIndex.ImageName);}
		}
	}

	/// <summary>Field Creation Class for entity IntlDescriptorTextJoinEntity</summary>
	public partial class IntlDescriptorTextJoinFields
	{
		/// <summary>Creates a new IntlDescriptorTextJoinEntity.ControlId field instance</summary>
		public static EntityField2 ControlId
		{
			get { return (EntityField2)EntityFieldFactory.Create(IntlDescriptorTextJoinFieldIndex.ControlId);}
		}
		/// <summary>Creates a new IntlDescriptorTextJoinEntity.ControlText field instance</summary>
		public static EntityField2 ControlText
		{
			get { return (EntityField2)EntityFieldFactory.Create(IntlDescriptorTextJoinFieldIndex.ControlText);}
		}
		/// <summary>Creates a new IntlDescriptorTextJoinEntity.CustomerId field instance</summary>
		public static EntityField2 CustomerId
		{
			get { return (EntityField2)EntityFieldFactory.Create(IntlDescriptorTextJoinFieldIndex.CustomerId);}
		}
		/// <summary>Creates a new IntlDescriptorTextJoinEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(IntlDescriptorTextJoinFieldIndex.EntId);}
		}
		/// <summary>Creates a new IntlDescriptorTextJoinEntity.LanguageId field instance</summary>
		public static EntityField2 LanguageId
		{
			get { return (EntityField2)EntityFieldFactory.Create(IntlDescriptorTextJoinFieldIndex.LanguageId);}
		}
		/// <summary>Creates a new IntlDescriptorTextJoinEntity.NodeId field instance</summary>
		public static EntityField2 NodeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(IntlDescriptorTextJoinFieldIndex.NodeId);}
		}
		/// <summary>Creates a new IntlDescriptorTextJoinEntity.ObjectId field instance</summary>
		public static EntityField2 ObjectId
		{
			get { return (EntityField2)EntityFieldFactory.Create(IntlDescriptorTextJoinFieldIndex.ObjectId);}
		}
		/// <summary>Creates a new IntlDescriptorTextJoinEntity.TranslatedFlag field instance</summary>
		public static EntityField2 TranslatedFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(IntlDescriptorTextJoinFieldIndex.TranslatedFlag);}
		}
	}

	/// <summary>Field Creation Class for entity IpServerAttribMasterEntity</summary>
	public partial class IpServerAttribMasterFields
	{
		/// <summary>Creates a new IpServerAttribMasterEntity.CurrentVersion field instance</summary>
		public static EntityField2 CurrentVersion
		{
			get { return (EntityField2)EntityFieldFactory.Create(IpServerAttribMasterFieldIndex.CurrentVersion);}
		}
		/// <summary>Creates a new IpServerAttribMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(IpServerAttribMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new IpServerAttribMasterEntity.IpAddress field instance</summary>
		public static EntityField2 IpAddress
		{
			get { return (EntityField2)EntityFieldFactory.Create(IpServerAttribMasterFieldIndex.IpAddress);}
		}
		/// <summary>Creates a new IpServerAttribMasterEntity.IpTypeCodeId field instance</summary>
		public static EntityField2 IpTypeCodeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(IpServerAttribMasterFieldIndex.IpTypeCodeId);}
		}
		/// <summary>Creates a new IpServerAttribMasterEntity.ServerAttribDesc field instance</summary>
		public static EntityField2 ServerAttribDesc
		{
			get { return (EntityField2)EntityFieldFactory.Create(IpServerAttribMasterFieldIndex.ServerAttribDesc);}
		}
		/// <summary>Creates a new IpServerAttribMasterEntity.ServerAttribId field instance</summary>
		public static EntityField2 ServerAttribId
		{
			get { return (EntityField2)EntityFieldFactory.Create(IpServerAttribMasterFieldIndex.ServerAttribId);}
		}
		/// <summary>Creates a new IpServerAttribMasterEntity.ServerHostName field instance</summary>
		public static EntityField2 ServerHostName
		{
			get { return (EntityField2)EntityFieldFactory.Create(IpServerAttribMasterFieldIndex.ServerHostName);}
		}
		/// <summary>Creates a new IpServerAttribMasterEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(IpServerAttribMasterFieldIndex.StoreId);}
		}
	}

	/// <summary>Field Creation Class for entity JobcodeFuncBttnJoinEntity</summary>
	public partial class JobcodeFuncBttnJoinFields
	{
		/// <summary>Creates a new JobcodeFuncBttnJoinEntity.EnabledFlag field instance</summary>
		public static EntityField2 EnabledFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(JobcodeFuncBttnJoinFieldIndex.EnabledFlag);}
		}
		/// <summary>Creates a new JobcodeFuncBttnJoinEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(JobcodeFuncBttnJoinFieldIndex.EntId);}
		}
		/// <summary>Creates a new JobcodeFuncBttnJoinEntity.FuncbttnId field instance</summary>
		public static EntityField2 FuncbttnId
		{
			get { return (EntityField2)EntityFieldFactory.Create(JobcodeFuncBttnJoinFieldIndex.FuncbttnId);}
		}
		/// <summary>Creates a new JobcodeFuncBttnJoinEntity.JobcodeId field instance</summary>
		public static EntityField2 JobcodeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(JobcodeFuncBttnJoinFieldIndex.JobcodeId);}
		}
	}

	/// <summary>Field Creation Class for entity JobcodeFunctJoinEntity</summary>
	public partial class JobcodeFunctJoinFields
	{
		/// <summary>Creates a new JobcodeFunctJoinEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(JobcodeFunctJoinFieldIndex.EntId);}
		}
		/// <summary>Creates a new JobcodeFunctJoinEntity.FunctFlag field instance</summary>
		public static EntityField2 FunctFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(JobcodeFunctJoinFieldIndex.FunctFlag);}
		}
		/// <summary>Creates a new JobcodeFunctJoinEntity.FunctId field instance</summary>
		public static EntityField2 FunctId
		{
			get { return (EntityField2)EntityFieldFactory.Create(JobcodeFunctJoinFieldIndex.FunctId);}
		}
		/// <summary>Creates a new JobcodeFunctJoinEntity.JobcodeId field instance</summary>
		public static EntityField2 JobcodeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(JobcodeFunctJoinFieldIndex.JobcodeId);}
		}
	}

	/// <summary>Field Creation Class for entity JobcodeMasterEntity</summary>
	public partial class JobcodeMasterFields
	{
		/// <summary>Creates a new JobcodeMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(JobcodeMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new JobcodeMasterEntity.JobcodeAbbr field instance</summary>
		public static EntityField2 JobcodeAbbr
		{
			get { return (EntityField2)EntityFieldFactory.Create(JobcodeMasterFieldIndex.JobcodeAbbr);}
		}
		/// <summary>Creates a new JobcodeMasterEntity.JobcodeAbbr2 field instance</summary>
		public static EntityField2 JobcodeAbbr2
		{
			get { return (EntityField2)EntityFieldFactory.Create(JobcodeMasterFieldIndex.JobcodeAbbr2);}
		}
		/// <summary>Creates a new JobcodeMasterEntity.JobcodeCarryoverTipsFlag field instance</summary>
		public static EntityField2 JobcodeCarryoverTipsFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(JobcodeMasterFieldIndex.JobcodeCarryoverTipsFlag);}
		}
		/// <summary>Creates a new JobcodeMasterEntity.JobcodeId field instance</summary>
		public static EntityField2 JobcodeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(JobcodeMasterFieldIndex.JobcodeId);}
		}
		/// <summary>Creates a new JobcodeMasterEntity.JobcodeLaborRate field instance</summary>
		public static EntityField2 JobcodeLaborRate
		{
			get { return (EntityField2)EntityFieldFactory.Create(JobcodeMasterFieldIndex.JobcodeLaborRate);}
		}
		/// <summary>Creates a new JobcodeMasterEntity.JobcodeName field instance</summary>
		public static EntityField2 JobcodeName
		{
			get { return (EntityField2)EntityFieldFactory.Create(JobcodeMasterFieldIndex.JobcodeName);}
		}
		/// <summary>Creates a new JobcodeMasterEntity.JobcodePrintCashFlag field instance</summary>
		public static EntityField2 JobcodePrintCashFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(JobcodeMasterFieldIndex.JobcodePrintCashFlag);}
		}
		/// <summary>Creates a new JobcodeMasterEntity.JobcodePrintCashierFlag field instance</summary>
		public static EntityField2 JobcodePrintCashierFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(JobcodeMasterFieldIndex.JobcodePrintCashierFlag);}
		}
		/// <summary>Creates a new JobcodeMasterEntity.JobcodePrintEmpIdFlag field instance</summary>
		public static EntityField2 JobcodePrintEmpIdFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(JobcodeMasterFieldIndex.JobcodePrintEmpIdFlag);}
		}
		/// <summary>Creates a new JobcodeMasterEntity.JobcodePrintServerFlag field instance</summary>
		public static EntityField2 JobcodePrintServerFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(JobcodeMasterFieldIndex.JobcodePrintServerFlag);}
		}
		/// <summary>Creates a new JobcodeMasterEntity.JobcodeSecId field instance</summary>
		public static EntityField2 JobcodeSecId
		{
			get { return (EntityField2)EntityFieldFactory.Create(JobcodeMasterFieldIndex.JobcodeSecId);}
		}
		/// <summary>Creates a new JobcodeMasterEntity.JobcodeTipPrompt field instance</summary>
		public static EntityField2 JobcodeTipPrompt
		{
			get { return (EntityField2)EntityFieldFactory.Create(JobcodeMasterFieldIndex.JobcodeTipPrompt);}
		}
		/// <summary>Creates a new JobcodeMasterEntity.RowVersion field instance</summary>
		public static EntityField2 RowVersion
		{
			get { return (EntityField2)EntityFieldFactory.Create(JobcodeMasterFieldIndex.RowVersion);}
		}
		/// <summary>Creates a new JobcodeMasterEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(JobcodeMasterFieldIndex.StoreId);}
		}
	}

	/// <summary>Field Creation Class for entity KdsCategoryMasterEntity</summary>
	public partial class KdsCategoryMasterFields
	{
		/// <summary>Creates a new KdsCategoryMasterEntity.DivisionId field instance</summary>
		public static EntityField2 DivisionId
		{
			get { return (EntityField2)EntityFieldFactory.Create(KdsCategoryMasterFieldIndex.DivisionId);}
		}
		/// <summary>Creates a new KdsCategoryMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(KdsCategoryMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new KdsCategoryMasterEntity.KdsCategoryId field instance</summary>
		public static EntityField2 KdsCategoryId
		{
			get { return (EntityField2)EntityFieldFactory.Create(KdsCategoryMasterFieldIndex.KdsCategoryId);}
		}
		/// <summary>Creates a new KdsCategoryMasterEntity.KdsCategoryName field instance</summary>
		public static EntityField2 KdsCategoryName
		{
			get { return (EntityField2)EntityFieldFactory.Create(KdsCategoryMasterFieldIndex.KdsCategoryName);}
		}
	}

	/// <summary>Field Creation Class for entity KdsVideoMasterEntity</summary>
	public partial class KdsVideoMasterFields
	{
		/// <summary>Creates a new KdsVideoMasterEntity.DivisionId field instance</summary>
		public static EntityField2 DivisionId
		{
			get { return (EntityField2)EntityFieldFactory.Create(KdsVideoMasterFieldIndex.DivisionId);}
		}
		/// <summary>Creates a new KdsVideoMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(KdsVideoMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new KdsVideoMasterEntity.KdsVideoId field instance</summary>
		public static EntityField2 KdsVideoId
		{
			get { return (EntityField2)EntityFieldFactory.Create(KdsVideoMasterFieldIndex.KdsVideoId);}
		}
		/// <summary>Creates a new KdsVideoMasterEntity.KdsVideoName field instance</summary>
		public static EntityField2 KdsVideoName
		{
			get { return (EntityField2)EntityFieldFactory.Create(KdsVideoMasterFieldIndex.KdsVideoName);}
		}
	}

	/// <summary>Field Creation Class for entity KpOptionGrpCopyJoinEntity</summary>
	public partial class KpOptionGrpCopyJoinFields
	{
		/// <summary>Creates a new KpOptionGrpCopyJoinEntity.BeepFlag field instance</summary>
		public static EntityField2 BeepFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(KpOptionGrpCopyJoinFieldIndex.BeepFlag);}
		}
		/// <summary>Creates a new KpOptionGrpCopyJoinEntity.CutCopyFlag field instance</summary>
		public static EntityField2 CutCopyFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(KpOptionGrpCopyJoinFieldIndex.CutCopyFlag);}
		}
		/// <summary>Creates a new KpOptionGrpCopyJoinEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(KpOptionGrpCopyJoinFieldIndex.EntId);}
		}
		/// <summary>Creates a new KpOptionGrpCopyJoinEntity.ExtraCodeId field instance</summary>
		public static EntityField2 ExtraCodeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(KpOptionGrpCopyJoinFieldIndex.ExtraCodeId);}
		}
		/// <summary>Creates a new KpOptionGrpCopyJoinEntity.KpCopiesSeq field instance</summary>
		public static EntityField2 KpCopiesSeq
		{
			get { return (EntityField2)EntityFieldFactory.Create(KpOptionGrpCopyJoinFieldIndex.KpCopiesSeq);}
		}
		/// <summary>Creates a new KpOptionGrpCopyJoinEntity.KpOptionGrpId field instance</summary>
		public static EntityField2 KpOptionGrpId
		{
			get { return (EntityField2)EntityFieldFactory.Create(KpOptionGrpCopyJoinFieldIndex.KpOptionGrpId);}
		}
		/// <summary>Creates a new KpOptionGrpCopyJoinEntity.NumLinesAfter field instance</summary>
		public static EntityField2 NumLinesAfter
		{
			get { return (EntityField2)EntityFieldFactory.Create(KpOptionGrpCopyJoinFieldIndex.NumLinesAfter);}
		}
		/// <summary>Creates a new KpOptionGrpCopyJoinEntity.NumLinesBefore field instance</summary>
		public static EntityField2 NumLinesBefore
		{
			get { return (EntityField2)EntityFieldFactory.Create(KpOptionGrpCopyJoinFieldIndex.NumLinesBefore);}
		}
		/// <summary>Creates a new KpOptionGrpCopyJoinEntity.PrintHeaderFlag field instance</summary>
		public static EntityField2 PrintHeaderFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(KpOptionGrpCopyJoinFieldIndex.PrintHeaderFlag);}
		}
		/// <summary>Creates a new KpOptionGrpCopyJoinEntity.SortCodeId field instance</summary>
		public static EntityField2 SortCodeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(KpOptionGrpCopyJoinFieldIndex.SortCodeId);}
		}
	}

	/// <summary>Field Creation Class for entity KpOptionGrpFormatJoinEntity</summary>
	public partial class KpOptionGrpFormatJoinFields
	{
		/// <summary>Creates a new KpOptionGrpFormatJoinEntity.AddLineFlag field instance</summary>
		public static EntityField2 AddLineFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(KpOptionGrpFormatJoinFieldIndex.AddLineFlag);}
		}
		/// <summary>Creates a new KpOptionGrpFormatJoinEntity.CharsizeCodeId field instance</summary>
		public static EntityField2 CharsizeCodeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(KpOptionGrpFormatJoinFieldIndex.CharsizeCodeId);}
		}
		/// <summary>Creates a new KpOptionGrpFormatJoinEntity.ColorCodeId field instance</summary>
		public static EntityField2 ColorCodeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(KpOptionGrpFormatJoinFieldIndex.ColorCodeId);}
		}
		/// <summary>Creates a new KpOptionGrpFormatJoinEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(KpOptionGrpFormatJoinFieldIndex.EntId);}
		}
		/// <summary>Creates a new KpOptionGrpFormatJoinEntity.FormatLabel field instance</summary>
		public static EntityField2 FormatLabel
		{
			get { return (EntityField2)EntityFieldFactory.Create(KpOptionGrpFormatJoinFieldIndex.FormatLabel);}
		}
		/// <summary>Creates a new KpOptionGrpFormatJoinEntity.KpFormatId field instance</summary>
		public static EntityField2 KpFormatId
		{
			get { return (EntityField2)EntityFieldFactory.Create(KpOptionGrpFormatJoinFieldIndex.KpFormatId);}
		}
		/// <summary>Creates a new KpOptionGrpFormatJoinEntity.KpOptionGrpId field instance</summary>
		public static EntityField2 KpOptionGrpId
		{
			get { return (EntityField2)EntityFieldFactory.Create(KpOptionGrpFormatJoinFieldIndex.KpOptionGrpId);}
		}
		/// <summary>Creates a new KpOptionGrpFormatJoinEntity.PrintFlag field instance</summary>
		public static EntityField2 PrintFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(KpOptionGrpFormatJoinFieldIndex.PrintFlag);}
		}
	}

	/// <summary>Field Creation Class for entity KpOptionGrpMasterEntity</summary>
	public partial class KpOptionGrpMasterFields
	{
		/// <summary>Creates a new KpOptionGrpMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(KpOptionGrpMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new KpOptionGrpMasterEntity.KpOptionGrpAbbr1 field instance</summary>
		public static EntityField2 KpOptionGrpAbbr1
		{
			get { return (EntityField2)EntityFieldFactory.Create(KpOptionGrpMasterFieldIndex.KpOptionGrpAbbr1);}
		}
		/// <summary>Creates a new KpOptionGrpMasterEntity.KpOptionGrpAbbr2 field instance</summary>
		public static EntityField2 KpOptionGrpAbbr2
		{
			get { return (EntityField2)EntityFieldFactory.Create(KpOptionGrpMasterFieldIndex.KpOptionGrpAbbr2);}
		}
		/// <summary>Creates a new KpOptionGrpMasterEntity.KpOptionGrpId field instance</summary>
		public static EntityField2 KpOptionGrpId
		{
			get { return (EntityField2)EntityFieldFactory.Create(KpOptionGrpMasterFieldIndex.KpOptionGrpId);}
		}
		/// <summary>Creates a new KpOptionGrpMasterEntity.KpOptionGrpName field instance</summary>
		public static EntityField2 KpOptionGrpName
		{
			get { return (EntityField2)EntityFieldFactory.Create(KpOptionGrpMasterFieldIndex.KpOptionGrpName);}
		}
		/// <summary>Creates a new KpOptionGrpMasterEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(KpOptionGrpMasterFieldIndex.StoreId);}
		}
	}

	/// <summary>Field Creation Class for entity KpPrinterMasterEntity</summary>
	public partial class KpPrinterMasterFields
	{
		/// <summary>Creates a new KpPrinterMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(KpPrinterMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new KpPrinterMasterEntity.KpPrinterId field instance</summary>
		public static EntityField2 KpPrinterId
		{
			get { return (EntityField2)EntityFieldFactory.Create(KpPrinterMasterFieldIndex.KpPrinterId);}
		}
		/// <summary>Creates a new KpPrinterMasterEntity.KpPrinterName field instance</summary>
		public static EntityField2 KpPrinterName
		{
			get { return (EntityField2)EntityFieldFactory.Create(KpPrinterMasterFieldIndex.KpPrinterName);}
		}
		/// <summary>Creates a new KpPrinterMasterEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(KpPrinterMasterFieldIndex.StoreId);}
		}
	}

	/// <summary>Field Creation Class for entity KpRouteMasterEntity</summary>
	public partial class KpRouteMasterFields
	{
		/// <summary>Creates a new KpRouteMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(KpRouteMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new KpRouteMasterEntity.KpRouteId field instance</summary>
		public static EntityField2 KpRouteId
		{
			get { return (EntityField2)EntityFieldFactory.Create(KpRouteMasterFieldIndex.KpRouteId);}
		}
		/// <summary>Creates a new KpRouteMasterEntity.KpRouteName field instance</summary>
		public static EntityField2 KpRouteName
		{
			get { return (EntityField2)EntityFieldFactory.Create(KpRouteMasterFieldIndex.KpRouteName);}
		}
		/// <summary>Creates a new KpRouteMasterEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(KpRouteMasterFieldIndex.StoreId);}
		}
	}

	/// <summary>Field Creation Class for entity KpRoutePrinterJoinEntity</summary>
	public partial class KpRoutePrinterJoinFields
	{
		/// <summary>Creates a new KpRoutePrinterJoinEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(KpRoutePrinterJoinFieldIndex.EntId);}
		}
		/// <summary>Creates a new KpRoutePrinterJoinEntity.KpDeviceId field instance</summary>
		public static EntityField2 KpDeviceId
		{
			get { return (EntityField2)EntityFieldFactory.Create(KpRoutePrinterJoinFieldIndex.KpDeviceId);}
		}
		/// <summary>Creates a new KpRoutePrinterJoinEntity.KpPrinterId field instance</summary>
		public static EntityField2 KpPrinterId
		{
			get { return (EntityField2)EntityFieldFactory.Create(KpRoutePrinterJoinFieldIndex.KpPrinterId);}
		}
		/// <summary>Creates a new KpRoutePrinterJoinEntity.KpRouteId field instance</summary>
		public static EntityField2 KpRouteId
		{
			get { return (EntityField2)EntityFieldFactory.Create(KpRoutePrinterJoinFieldIndex.KpRouteId);}
		}
	}

	/// <summary>Field Creation Class for entity MachineOptionsMasterEntity</summary>
	public partial class MachineOptionsMasterFields
	{
		/// <summary>Creates a new MachineOptionsMasterEntity.OptionName field instance</summary>
		public static EntityField2 OptionName
		{
			get { return (EntityField2)EntityFieldFactory.Create(MachineOptionsMasterFieldIndex.OptionName);}
		}
		/// <summary>Creates a new MachineOptionsMasterEntity.OptionValue field instance</summary>
		public static EntityField2 OptionValue
		{
			get { return (EntityField2)EntityFieldFactory.Create(MachineOptionsMasterFieldIndex.OptionValue);}
		}
	}

	/// <summary>Field Creation Class for entity MealPeriodMasterEntity</summary>
	public partial class MealPeriodMasterFields
	{
		/// <summary>Creates a new MealPeriodMasterEntity.DefaultCheckTypeId field instance</summary>
		public static EntityField2 DefaultCheckTypeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MealPeriodMasterFieldIndex.DefaultCheckTypeId);}
		}
		/// <summary>Creates a new MealPeriodMasterEntity.DefaultPriceLevelId field instance</summary>
		public static EntityField2 DefaultPriceLevelId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MealPeriodMasterFieldIndex.DefaultPriceLevelId);}
		}
		/// <summary>Creates a new MealPeriodMasterEntity.EntertainmentFlag field instance</summary>
		public static EntityField2 EntertainmentFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(MealPeriodMasterFieldIndex.EntertainmentFlag);}
		}
		/// <summary>Creates a new MealPeriodMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MealPeriodMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new MealPeriodMasterEntity.MealPeriodAbbr1 field instance</summary>
		public static EntityField2 MealPeriodAbbr1
		{
			get { return (EntityField2)EntityFieldFactory.Create(MealPeriodMasterFieldIndex.MealPeriodAbbr1);}
		}
		/// <summary>Creates a new MealPeriodMasterEntity.MealPeriodAbbr2 field instance</summary>
		public static EntityField2 MealPeriodAbbr2
		{
			get { return (EntityField2)EntityFieldFactory.Create(MealPeriodMasterFieldIndex.MealPeriodAbbr2);}
		}
		/// <summary>Creates a new MealPeriodMasterEntity.MealPeriodId field instance</summary>
		public static EntityField2 MealPeriodId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MealPeriodMasterFieldIndex.MealPeriodId);}
		}
		/// <summary>Creates a new MealPeriodMasterEntity.MealPeriodName field instance</summary>
		public static EntityField2 MealPeriodName
		{
			get { return (EntityField2)EntityFieldFactory.Create(MealPeriodMasterFieldIndex.MealPeriodName);}
		}
		/// <summary>Creates a new MealPeriodMasterEntity.MealPeriodSecId field instance</summary>
		public static EntityField2 MealPeriodSecId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MealPeriodMasterFieldIndex.MealPeriodSecId);}
		}
		/// <summary>Creates a new MealPeriodMasterEntity.ReceiptCode field instance</summary>
		public static EntityField2 ReceiptCode
		{
			get { return (EntityField2)EntityFieldFactory.Create(MealPeriodMasterFieldIndex.ReceiptCode);}
		}
		/// <summary>Creates a new MealPeriodMasterEntity.RowVersion field instance</summary>
		public static EntityField2 RowVersion
		{
			get { return (EntityField2)EntityFieldFactory.Create(MealPeriodMasterFieldIndex.RowVersion);}
		}
		/// <summary>Creates a new MealPeriodMasterEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MealPeriodMasterFieldIndex.StoreId);}
		}
	}

	/// <summary>Field Creation Class for entity MembershipMasterEntity</summary>
	public partial class MembershipMasterFields
	{
		/// <summary>Creates a new MembershipMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MembershipMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new MembershipMasterEntity.MembershipCode field instance</summary>
		public static EntityField2 MembershipCode
		{
			get { return (EntityField2)EntityFieldFactory.Create(MembershipMasterFieldIndex.MembershipCode);}
		}
		/// <summary>Creates a new MembershipMasterEntity.MembershipId field instance</summary>
		public static EntityField2 MembershipId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MembershipMasterFieldIndex.MembershipId);}
		}
		/// <summary>Creates a new MembershipMasterEntity.MembershipName field instance</summary>
		public static EntityField2 MembershipName
		{
			get { return (EntityField2)EntityFieldFactory.Create(MembershipMasterFieldIndex.MembershipName);}
		}
	}

	/// <summary>Field Creation Class for entity MembershipProfitCenterJoinEntity</summary>
	public partial class MembershipProfitCenterJoinFields
	{
		/// <summary>Creates a new MembershipProfitCenterJoinEntity.CheckTypeId field instance</summary>
		public static EntityField2 CheckTypeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MembershipProfitCenterJoinFieldIndex.CheckTypeId);}
		}
		/// <summary>Creates a new MembershipProfitCenterJoinEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MembershipProfitCenterJoinFieldIndex.EntId);}
		}
		/// <summary>Creates a new MembershipProfitCenterJoinEntity.MembershipId field instance</summary>
		public static EntityField2 MembershipId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MembershipProfitCenterJoinFieldIndex.MembershipId);}
		}
		/// <summary>Creates a new MembershipProfitCenterJoinEntity.ProfitCenterId field instance</summary>
		public static EntityField2 ProfitCenterId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MembershipProfitCenterJoinFieldIndex.ProfitCenterId);}
		}
	}

	/// <summary>Field Creation Class for entity MenuBttnObjJoinEntity</summary>
	public partial class MenuBttnObjJoinFields
	{
		/// <summary>Creates a new MenuBttnObjJoinEntity.ButtonBackcolor field instance</summary>
		public static EntityField2 ButtonBackcolor
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuBttnObjJoinFieldIndex.ButtonBackcolor);}
		}
		/// <summary>Creates a new MenuBttnObjJoinEntity.ButtonBorderStyle field instance</summary>
		public static EntityField2 ButtonBorderStyle
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuBttnObjJoinFieldIndex.ButtonBorderStyle);}
		}
		/// <summary>Creates a new MenuBttnObjJoinEntity.ButtonColumn field instance</summary>
		public static EntityField2 ButtonColumn
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuBttnObjJoinFieldIndex.ButtonColumn);}
		}
		/// <summary>Creates a new MenuBttnObjJoinEntity.ButtonFontId field instance</summary>
		public static EntityField2 ButtonFontId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuBttnObjJoinFieldIndex.ButtonFontId);}
		}
		/// <summary>Creates a new MenuBttnObjJoinEntity.ButtonForecolor field instance</summary>
		public static EntityField2 ButtonForecolor
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuBttnObjJoinFieldIndex.ButtonForecolor);}
		}
		/// <summary>Creates a new MenuBttnObjJoinEntity.ButtonHeight field instance</summary>
		public static EntityField2 ButtonHeight
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuBttnObjJoinFieldIndex.ButtonHeight);}
		}
		/// <summary>Creates a new MenuBttnObjJoinEntity.ButtonId field instance</summary>
		public static EntityField2 ButtonId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuBttnObjJoinFieldIndex.ButtonId);}
		}
		/// <summary>Creates a new MenuBttnObjJoinEntity.ButtonImageId field instance</summary>
		public static EntityField2 ButtonImageId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuBttnObjJoinFieldIndex.ButtonImageId);}
		}
		/// <summary>Creates a new MenuBttnObjJoinEntity.ButtonNextMenuId field instance</summary>
		public static EntityField2 ButtonNextMenuId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuBttnObjJoinFieldIndex.ButtonNextMenuId);}
		}
		/// <summary>Creates a new MenuBttnObjJoinEntity.ButtonRow field instance</summary>
		public static EntityField2 ButtonRow
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuBttnObjJoinFieldIndex.ButtonRow);}
		}
		/// <summary>Creates a new MenuBttnObjJoinEntity.ButtonShapeId field instance</summary>
		public static EntityField2 ButtonShapeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuBttnObjJoinFieldIndex.ButtonShapeId);}
		}
		/// <summary>Creates a new MenuBttnObjJoinEntity.ButtonWidth field instance</summary>
		public static EntityField2 ButtonWidth
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuBttnObjJoinFieldIndex.ButtonWidth);}
		}
		/// <summary>Creates a new MenuBttnObjJoinEntity.DisplayImageFlag field instance</summary>
		public static EntityField2 DisplayImageFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuBttnObjJoinFieldIndex.DisplayImageFlag);}
		}
		/// <summary>Creates a new MenuBttnObjJoinEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuBttnObjJoinFieldIndex.EntId);}
		}
		/// <summary>Creates a new MenuBttnObjJoinEntity.ErrorCodeId field instance</summary>
		public static EntityField2 ErrorCodeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuBttnObjJoinFieldIndex.ErrorCodeId);}
		}
		/// <summary>Creates a new MenuBttnObjJoinEntity.MenuId field instance</summary>
		public static EntityField2 MenuId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuBttnObjJoinFieldIndex.MenuId);}
		}
		/// <summary>Creates a new MenuBttnObjJoinEntity.ObjectId field instance</summary>
		public static EntityField2 ObjectId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuBttnObjJoinFieldIndex.ObjectId);}
		}
		/// <summary>Creates a new MenuBttnObjJoinEntity.ObjectTypeId field instance</summary>
		public static EntityField2 ObjectTypeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuBttnObjJoinFieldIndex.ObjectTypeId);}
		}
		/// <summary>Creates a new MenuBttnObjJoinEntity.ScreenId field instance</summary>
		public static EntityField2 ScreenId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuBttnObjJoinFieldIndex.ScreenId);}
		}
		/// <summary>Creates a new MenuBttnObjJoinEntity.StoreCreatedId field instance</summary>
		public static EntityField2 StoreCreatedId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuBttnObjJoinFieldIndex.StoreCreatedId);}
		}
		/// <summary>Creates a new MenuBttnObjJoinEntity.TerminalTypeId field instance</summary>
		public static EntityField2 TerminalTypeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuBttnObjJoinFieldIndex.TerminalTypeId);}
		}
	}

	/// <summary>Field Creation Class for entity MenuItemGroupMasterEntity</summary>
	public partial class MenuItemGroupMasterFields
	{
		/// <summary>Creates a new MenuItemGroupMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuItemGroupMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new MenuItemGroupMasterEntity.MenuItemGroupId field instance</summary>
		public static EntityField2 MenuItemGroupId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuItemGroupMasterFieldIndex.MenuItemGroupId);}
		}
		/// <summary>Creates a new MenuItemGroupMasterEntity.MenuItemGroupName field instance</summary>
		public static EntityField2 MenuItemGroupName
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuItemGroupMasterFieldIndex.MenuItemGroupName);}
		}
		/// <summary>Creates a new MenuItemGroupMasterEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuItemGroupMasterFieldIndex.StoreId);}
		}
	}

	/// <summary>Field Creation Class for entity MenuItemMasterEntity</summary>
	public partial class MenuItemMasterFields
	{
		/// <summary>Creates a new MenuItemMasterEntity.BargunId field instance</summary>
		public static EntityField2 BargunId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuItemMasterFieldIndex.BargunId);}
		}
		/// <summary>Creates a new MenuItemMasterEntity.Covers field instance</summary>
		public static EntityField2 Covers
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuItemMasterFieldIndex.Covers);}
		}
		/// <summary>Creates a new MenuItemMasterEntity.DataControlGroupId field instance</summary>
		public static EntityField2 DataControlGroupId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuItemMasterFieldIndex.DataControlGroupId);}
		}
		/// <summary>Creates a new MenuItemMasterEntity.DefaultImageId field instance</summary>
		public static EntityField2 DefaultImageId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuItemMasterFieldIndex.DefaultImageId);}
		}
		/// <summary>Creates a new MenuItemMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuItemMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new MenuItemMasterEntity.KdsCategoryId field instance</summary>
		public static EntityField2 KdsCategoryId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuItemMasterFieldIndex.KdsCategoryId);}
		}
		/// <summary>Creates a new MenuItemMasterEntity.KdsCookTime field instance</summary>
		public static EntityField2 KdsCookTime
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuItemMasterFieldIndex.KdsCookTime);}
		}
		/// <summary>Creates a new MenuItemMasterEntity.KdsVideoId field instance</summary>
		public static EntityField2 KdsVideoId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuItemMasterFieldIndex.KdsVideoId);}
		}
		/// <summary>Creates a new MenuItemMasterEntity.KdsVideoLabel field instance</summary>
		public static EntityField2 KdsVideoLabel
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuItemMasterFieldIndex.KdsVideoLabel);}
		}
		/// <summary>Creates a new MenuItemMasterEntity.MenuItemAbbr1 field instance</summary>
		public static EntityField2 MenuItemAbbr1
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuItemMasterFieldIndex.MenuItemAbbr1);}
		}
		/// <summary>Creates a new MenuItemMasterEntity.MenuItemAbbr2 field instance</summary>
		public static EntityField2 MenuItemAbbr2
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuItemMasterFieldIndex.MenuItemAbbr2);}
		}
		/// <summary>Creates a new MenuItemMasterEntity.MenuItemGroupId field instance</summary>
		public static EntityField2 MenuItemGroupId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuItemMasterFieldIndex.MenuItemGroupId);}
		}
		/// <summary>Creates a new MenuItemMasterEntity.MenuItemId field instance</summary>
		public static EntityField2 MenuItemId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuItemMasterFieldIndex.MenuItemId);}
		}
		/// <summary>Creates a new MenuItemMasterEntity.MenuItemName field instance</summary>
		public static EntityField2 MenuItemName
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuItemMasterFieldIndex.MenuItemName);}
		}
		/// <summary>Creates a new MenuItemMasterEntity.MiCostAmt field instance</summary>
		public static EntityField2 MiCostAmt
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuItemMasterFieldIndex.MiCostAmt);}
		}
		/// <summary>Creates a new MenuItemMasterEntity.MiDiscountableFlag field instance</summary>
		public static EntityField2 MiDiscountableFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuItemMasterFieldIndex.MiDiscountableFlag);}
		}
		/// <summary>Creates a new MenuItemMasterEntity.MiEmpDiscountableFlag field instance</summary>
		public static EntityField2 MiEmpDiscountableFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuItemMasterFieldIndex.MiEmpDiscountableFlag);}
		}
		/// <summary>Creates a new MenuItemMasterEntity.MiKpLabel field instance</summary>
		public static EntityField2 MiKpLabel
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuItemMasterFieldIndex.MiKpLabel);}
		}
		/// <summary>Creates a new MenuItemMasterEntity.MiNotActiveFlag field instance</summary>
		public static EntityField2 MiNotActiveFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuItemMasterFieldIndex.MiNotActiveFlag);}
		}
		/// <summary>Creates a new MenuItemMasterEntity.MiOpenPricePrompt field instance</summary>
		public static EntityField2 MiOpenPricePrompt
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuItemMasterFieldIndex.MiOpenPricePrompt);}
		}
		/// <summary>Creates a new MenuItemMasterEntity.MiPriceOverrideFlag field instance</summary>
		public static EntityField2 MiPriceOverrideFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuItemMasterFieldIndex.MiPriceOverrideFlag);}
		}
		/// <summary>Creates a new MenuItemMasterEntity.MiPrintFlag field instance</summary>
		public static EntityField2 MiPrintFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuItemMasterFieldIndex.MiPrintFlag);}
		}
		/// <summary>Creates a new MenuItemMasterEntity.MiReceiptLabel field instance</summary>
		public static EntityField2 MiReceiptLabel
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuItemMasterFieldIndex.MiReceiptLabel);}
		}
		/// <summary>Creates a new MenuItemMasterEntity.MiSecId field instance</summary>
		public static EntityField2 MiSecId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuItemMasterFieldIndex.MiSecId);}
		}
		/// <summary>Creates a new MenuItemMasterEntity.MiTaxInclFlag field instance</summary>
		public static EntityField2 MiTaxInclFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuItemMasterFieldIndex.MiTaxInclFlag);}
		}
		/// <summary>Creates a new MenuItemMasterEntity.MiVoidableFlag field instance</summary>
		public static EntityField2 MiVoidableFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuItemMasterFieldIndex.MiVoidableFlag);}
		}
		/// <summary>Creates a new MenuItemMasterEntity.MiWeightFlag field instance</summary>
		public static EntityField2 MiWeightFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuItemMasterFieldIndex.MiWeightFlag);}
		}
		/// <summary>Creates a new MenuItemMasterEntity.MiWeightTare field instance</summary>
		public static EntityField2 MiWeightTare
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuItemMasterFieldIndex.MiWeightTare);}
		}
		/// <summary>Creates a new MenuItemMasterEntity.OpenModifierFlag field instance</summary>
		public static EntityField2 OpenModifierFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuItemMasterFieldIndex.OpenModifierFlag);}
		}
		/// <summary>Creates a new MenuItemMasterEntity.ProdClassId field instance</summary>
		public static EntityField2 ProdClassId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuItemMasterFieldIndex.ProdClassId);}
		}
		/// <summary>Creates a new MenuItemMasterEntity.RevCatId field instance</summary>
		public static EntityField2 RevCatId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuItemMasterFieldIndex.RevCatId);}
		}
		/// <summary>Creates a new MenuItemMasterEntity.RowVersion field instance</summary>
		public static EntityField2 RowVersion
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuItemMasterFieldIndex.RowVersion);}
		}
		/// <summary>Creates a new MenuItemMasterEntity.RptCatId field instance</summary>
		public static EntityField2 RptCatId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuItemMasterFieldIndex.RptCatId);}
		}
		/// <summary>Creates a new MenuItemMasterEntity.SkuNo field instance</summary>
		public static EntityField2 SkuNo
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuItemMasterFieldIndex.SkuNo);}
		}
		/// <summary>Creates a new MenuItemMasterEntity.StoreCreatedId field instance</summary>
		public static EntityField2 StoreCreatedId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuItemMasterFieldIndex.StoreCreatedId);}
		}
		/// <summary>Creates a new MenuItemMasterEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuItemMasterFieldIndex.StoreId);}
		}
		/// <summary>Creates a new MenuItemMasterEntity.TaxGrpId field instance</summary>
		public static EntityField2 TaxGrpId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuItemMasterFieldIndex.TaxGrpId);}
		}
	}

	/// <summary>Field Creation Class for entity MenuMasterEntity</summary>
	public partial class MenuMasterFields
	{
		/// <summary>Creates a new MenuMasterEntity.AllowButtonOverlapFlag field instance</summary>
		public static EntityField2 AllowButtonOverlapFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuMasterFieldIndex.AllowButtonOverlapFlag);}
		}
		/// <summary>Creates a new MenuMasterEntity.ButtonPadding field instance</summary>
		public static EntityField2 ButtonPadding
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuMasterFieldIndex.ButtonPadding);}
		}
		/// <summary>Creates a new MenuMasterEntity.DataControlGroupId field instance</summary>
		public static EntityField2 DataControlGroupId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuMasterFieldIndex.DataControlGroupId);}
		}
		/// <summary>Creates a new MenuMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new MenuMasterEntity.GridSize field instance</summary>
		public static EntityField2 GridSize
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuMasterFieldIndex.GridSize);}
		}
		/// <summary>Creates a new MenuMasterEntity.MenuAbbr1 field instance</summary>
		public static EntityField2 MenuAbbr1
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuMasterFieldIndex.MenuAbbr1);}
		}
		/// <summary>Creates a new MenuMasterEntity.MenuAbbr2 field instance</summary>
		public static EntityField2 MenuAbbr2
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuMasterFieldIndex.MenuAbbr2);}
		}
		/// <summary>Creates a new MenuMasterEntity.MenuId field instance</summary>
		public static EntityField2 MenuId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuMasterFieldIndex.MenuId);}
		}
		/// <summary>Creates a new MenuMasterEntity.MenuName field instance</summary>
		public static EntityField2 MenuName
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuMasterFieldIndex.MenuName);}
		}
		/// <summary>Creates a new MenuMasterEntity.ScreenSizeId field instance</summary>
		public static EntityField2 ScreenSizeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuMasterFieldIndex.ScreenSizeId);}
		}
		/// <summary>Creates a new MenuMasterEntity.ScreenTypeId field instance</summary>
		public static EntityField2 ScreenTypeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuMasterFieldIndex.ScreenTypeId);}
		}
		/// <summary>Creates a new MenuMasterEntity.SnapToGridFlag field instance</summary>
		public static EntityField2 SnapToGridFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuMasterFieldIndex.SnapToGridFlag);}
		}
		/// <summary>Creates a new MenuMasterEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuMasterFieldIndex.StoreId);}
		}
		/// <summary>Creates a new MenuMasterEntity.SystemScreenFlag field instance</summary>
		public static EntityField2 SystemScreenFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuMasterFieldIndex.SystemScreenFlag);}
		}
		/// <summary>Creates a new MenuMasterEntity.ThemeId field instance</summary>
		public static EntityField2 ThemeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuMasterFieldIndex.ThemeId);}
		}
	}

	/// <summary>Field Creation Class for entity MenuMigrpJoinEntity</summary>
	public partial class MenuMigrpJoinFields
	{
		/// <summary>Creates a new MenuMigrpJoinEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuMigrpJoinFieldIndex.EntId);}
		}
		/// <summary>Creates a new MenuMigrpJoinEntity.MenuId field instance</summary>
		public static EntityField2 MenuId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuMigrpJoinFieldIndex.MenuId);}
		}
		/// <summary>Creates a new MenuMigrpJoinEntity.MenuItemGroupId field instance</summary>
		public static EntityField2 MenuItemGroupId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MenuMigrpJoinFieldIndex.MenuItemGroupId);}
		}
	}

	/// <summary>Field Creation Class for entity MiChoiceGrpJoinEntity</summary>
	public partial class MiChoiceGrpJoinFields
	{
		/// <summary>Creates a new MiChoiceGrpJoinEntity.ChoiceGroupId field instance</summary>
		public static EntityField2 ChoiceGroupId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MiChoiceGrpJoinFieldIndex.ChoiceGroupId);}
		}
		/// <summary>Creates a new MiChoiceGrpJoinEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MiChoiceGrpJoinFieldIndex.EntId);}
		}
		/// <summary>Creates a new MiChoiceGrpJoinEntity.MenuItemId field instance</summary>
		public static EntityField2 MenuItemId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MiChoiceGrpJoinFieldIndex.MenuItemId);}
		}
		/// <summary>Creates a new MiChoiceGrpJoinEntity.MiCgJoinId field instance</summary>
		public static EntityField2 MiCgJoinId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MiChoiceGrpJoinFieldIndex.MiCgJoinId);}
		}
		/// <summary>Creates a new MiChoiceGrpJoinEntity.MiChoicegrpSeq field instance</summary>
		public static EntityField2 MiChoicegrpSeq
		{
			get { return (EntityField2)EntityFieldFactory.Create(MiChoiceGrpJoinFieldIndex.MiChoicegrpSeq);}
		}
	}

	/// <summary>Field Creation Class for entity MiKpprinterJoinEntity</summary>
	public partial class MiKpprinterJoinFields
	{
		/// <summary>Creates a new MiKpprinterJoinEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MiKpprinterJoinFieldIndex.EntId);}
		}
		/// <summary>Creates a new MiKpprinterJoinEntity.KpPrinterId field instance</summary>
		public static EntityField2 KpPrinterId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MiKpprinterJoinFieldIndex.KpPrinterId);}
		}
		/// <summary>Creates a new MiKpprinterJoinEntity.MenuItemId field instance</summary>
		public static EntityField2 MenuItemId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MiKpprinterJoinFieldIndex.MenuItemId);}
		}
		/// <summary>Creates a new MiKpprinterJoinEntity.PrimaryPrinterFlag field instance</summary>
		public static EntityField2 PrimaryPrinterFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(MiKpprinterJoinFieldIndex.PrimaryPrinterFlag);}
		}
	}

	/// <summary>Field Creation Class for entity MiPriceJoinEntity</summary>
	public partial class MiPriceJoinFields
	{
		/// <summary>Creates a new MiPriceJoinEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MiPriceJoinFieldIndex.EntId);}
		}
		/// <summary>Creates a new MiPriceJoinEntity.MenuItemId field instance</summary>
		public static EntityField2 MenuItemId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MiPriceJoinFieldIndex.MenuItemId);}
		}
		/// <summary>Creates a new MiPriceJoinEntity.PriceLast field instance</summary>
		public static EntityField2 PriceLast
		{
			get { return (EntityField2)EntityFieldFactory.Create(MiPriceJoinFieldIndex.PriceLast);}
		}
		/// <summary>Creates a new MiPriceJoinEntity.PriceLevelId field instance</summary>
		public static EntityField2 PriceLevelId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MiPriceJoinFieldIndex.PriceLevelId);}
		}
		/// <summary>Creates a new MiPriceJoinEntity.PriceNet field instance</summary>
		public static EntityField2 PriceNet
		{
			get { return (EntityField2)EntityFieldFactory.Create(MiPriceJoinFieldIndex.PriceNet);}
		}
		/// <summary>Creates a new MiPriceJoinEntity.StoreCreatedId field instance</summary>
		public static EntityField2 StoreCreatedId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MiPriceJoinFieldIndex.StoreCreatedId);}
		}
	}

	/// <summary>Field Creation Class for entity MiSkuJoinEntity</summary>
	public partial class MiSkuJoinFields
	{
		/// <summary>Creates a new MiSkuJoinEntity.Description field instance</summary>
		public static EntityField2 Description
		{
			get { return (EntityField2)EntityFieldFactory.Create(MiSkuJoinFieldIndex.Description);}
		}
		/// <summary>Creates a new MiSkuJoinEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MiSkuJoinFieldIndex.EntId);}
		}
		/// <summary>Creates a new MiSkuJoinEntity.MenuItemId field instance</summary>
		public static EntityField2 MenuItemId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MiSkuJoinFieldIndex.MenuItemId);}
		}
		/// <summary>Creates a new MiSkuJoinEntity.SkuGenId field instance</summary>
		public static EntityField2 SkuGenId
		{
			get { return (EntityField2)EntityFieldFactory.Create(MiSkuJoinFieldIndex.SkuGenId);}
		}
		/// <summary>Creates a new MiSkuJoinEntity.SkuNo field instance</summary>
		public static EntityField2 SkuNo
		{
			get { return (EntityField2)EntityFieldFactory.Create(MiSkuJoinFieldIndex.SkuNo);}
		}
	}

	/// <summary>Field Creation Class for entity ModifierKpprinterJoinEntity</summary>
	public partial class ModifierKpprinterJoinFields
	{
		/// <summary>Creates a new ModifierKpprinterJoinEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ModifierKpprinterJoinFieldIndex.EntId);}
		}
		/// <summary>Creates a new ModifierKpprinterJoinEntity.KpPrinterId field instance</summary>
		public static EntityField2 KpPrinterId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ModifierKpprinterJoinFieldIndex.KpPrinterId);}
		}
		/// <summary>Creates a new ModifierKpprinterJoinEntity.ModifierId field instance</summary>
		public static EntityField2 ModifierId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ModifierKpprinterJoinFieldIndex.ModifierId);}
		}
		/// <summary>Creates a new ModifierKpprinterJoinEntity.PrimaryPrinterFlag field instance</summary>
		public static EntityField2 PrimaryPrinterFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(ModifierKpprinterJoinFieldIndex.PrimaryPrinterFlag);}
		}
	}

	/// <summary>Field Creation Class for entity ModifierMasterEntity</summary>
	public partial class ModifierMasterFields
	{
		/// <summary>Creates a new ModifierMasterEntity.DataControlGroupId field instance</summary>
		public static EntityField2 DataControlGroupId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ModifierMasterFieldIndex.DataControlGroupId);}
		}
		/// <summary>Creates a new ModifierMasterEntity.DiscoupId field instance</summary>
		public static EntityField2 DiscoupId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ModifierMasterFieldIndex.DiscoupId);}
		}
		/// <summary>Creates a new ModifierMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ModifierMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new ModifierMasterEntity.KdsVideoLabel field instance</summary>
		public static EntityField2 KdsVideoLabel
		{
			get { return (EntityField2)EntityFieldFactory.Create(ModifierMasterFieldIndex.KdsVideoLabel);}
		}
		/// <summary>Creates a new ModifierMasterEntity.ModifierAbbr1 field instance</summary>
		public static EntityField2 ModifierAbbr1
		{
			get { return (EntityField2)EntityFieldFactory.Create(ModifierMasterFieldIndex.ModifierAbbr1);}
		}
		/// <summary>Creates a new ModifierMasterEntity.ModifierAbbr2 field instance</summary>
		public static EntityField2 ModifierAbbr2
		{
			get { return (EntityField2)EntityFieldFactory.Create(ModifierMasterFieldIndex.ModifierAbbr2);}
		}
		/// <summary>Creates a new ModifierMasterEntity.ModifierExtraChoicegrpId field instance</summary>
		public static EntityField2 ModifierExtraChoicegrpId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ModifierMasterFieldIndex.ModifierExtraChoicegrpId);}
		}
		/// <summary>Creates a new ModifierMasterEntity.ModifierId field instance</summary>
		public static EntityField2 ModifierId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ModifierMasterFieldIndex.ModifierId);}
		}
		/// <summary>Creates a new ModifierMasterEntity.ModifierKpLabel field instance</summary>
		public static EntityField2 ModifierKpLabel
		{
			get { return (EntityField2)EntityFieldFactory.Create(ModifierMasterFieldIndex.ModifierKpLabel);}
		}
		/// <summary>Creates a new ModifierMasterEntity.ModifierMenuItemId field instance</summary>
		public static EntityField2 ModifierMenuItemId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ModifierMasterFieldIndex.ModifierMenuItemId);}
		}
		/// <summary>Creates a new ModifierMasterEntity.ModifierName field instance</summary>
		public static EntityField2 ModifierName
		{
			get { return (EntityField2)EntityFieldFactory.Create(ModifierMasterFieldIndex.ModifierName);}
		}
		/// <summary>Creates a new ModifierMasterEntity.ModifierPrintFlag field instance</summary>
		public static EntityField2 ModifierPrintFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(ModifierMasterFieldIndex.ModifierPrintFlag);}
		}
		/// <summary>Creates a new ModifierMasterEntity.ModifierSecId field instance</summary>
		public static EntityField2 ModifierSecId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ModifierMasterFieldIndex.ModifierSecId);}
		}
		/// <summary>Creates a new ModifierMasterEntity.ModifierWithMiFlag field instance</summary>
		public static EntityField2 ModifierWithMiFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(ModifierMasterFieldIndex.ModifierWithMiFlag);}
		}
		/// <summary>Creates a new ModifierMasterEntity.RowVersion field instance</summary>
		public static EntityField2 RowVersion
		{
			get { return (EntityField2)EntityFieldFactory.Create(ModifierMasterFieldIndex.RowVersion);}
		}
		/// <summary>Creates a new ModifierMasterEntity.StoreCreatedId field instance</summary>
		public static EntityField2 StoreCreatedId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ModifierMasterFieldIndex.StoreCreatedId);}
		}
		/// <summary>Creates a new ModifierMasterEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ModifierMasterFieldIndex.StoreId);}
		}
	}

	/// <summary>Field Creation Class for entity ModifierPriceJoinEntity</summary>
	public partial class ModifierPriceJoinFields
	{
		/// <summary>Creates a new ModifierPriceJoinEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ModifierPriceJoinFieldIndex.EntId);}
		}
		/// <summary>Creates a new ModifierPriceJoinEntity.ModifierId field instance</summary>
		public static EntityField2 ModifierId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ModifierPriceJoinFieldIndex.ModifierId);}
		}
		/// <summary>Creates a new ModifierPriceJoinEntity.PriceLast field instance</summary>
		public static EntityField2 PriceLast
		{
			get { return (EntityField2)EntityFieldFactory.Create(ModifierPriceJoinFieldIndex.PriceLast);}
		}
		/// <summary>Creates a new ModifierPriceJoinEntity.PriceLevelId field instance</summary>
		public static EntityField2 PriceLevelId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ModifierPriceJoinFieldIndex.PriceLevelId);}
		}
		/// <summary>Creates a new ModifierPriceJoinEntity.PriceNet field instance</summary>
		public static EntityField2 PriceNet
		{
			get { return (EntityField2)EntityFieldFactory.Create(ModifierPriceJoinFieldIndex.PriceNet);}
		}
	}

	/// <summary>Field Creation Class for entity OptionsMasterEntity</summary>
	public partial class OptionsMasterFields
	{
		/// <summary>Creates a new OptionsMasterEntity.DualFontName field instance</summary>
		public static EntityField2 DualFontName
		{
			get { return (EntityField2)EntityFieldFactory.Create(OptionsMasterFieldIndex.DualFontName);}
		}
		/// <summary>Creates a new OptionsMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(OptionsMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new OptionsMasterEntity.ThemeLanguageId field instance</summary>
		public static EntityField2 ThemeLanguageId
		{
			get { return (EntityField2)EntityFieldFactory.Create(OptionsMasterFieldIndex.ThemeLanguageId);}
		}
		/// <summary>Creates a new OptionsMasterEntity.UdUserFlag field instance</summary>
		public static EntityField2 UdUserFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(OptionsMasterFieldIndex.UdUserFlag);}
		}
		/// <summary>Creates a new OptionsMasterEntity.UnicodeFlag field instance</summary>
		public static EntityField2 UnicodeFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(OptionsMasterFieldIndex.UnicodeFlag);}
		}
	}

	/// <summary>Field Creation Class for entity PriceLevelMasterEntity</summary>
	public partial class PriceLevelMasterFields
	{
		/// <summary>Creates a new PriceLevelMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(PriceLevelMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new PriceLevelMasterEntity.PriceLevelId field instance</summary>
		public static EntityField2 PriceLevelId
		{
			get { return (EntityField2)EntityFieldFactory.Create(PriceLevelMasterFieldIndex.PriceLevelId);}
		}
		/// <summary>Creates a new PriceLevelMasterEntity.PriceLevelName field instance</summary>
		public static EntityField2 PriceLevelName
		{
			get { return (EntityField2)EntityFieldFactory.Create(PriceLevelMasterFieldIndex.PriceLevelName);}
		}
		/// <summary>Creates a new PriceLevelMasterEntity.RowVersion field instance</summary>
		public static EntityField2 RowVersion
		{
			get { return (EntityField2)EntityFieldFactory.Create(PriceLevelMasterFieldIndex.RowVersion);}
		}
		/// <summary>Creates a new PriceLevelMasterEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(PriceLevelMasterFieldIndex.StoreId);}
		}
	}

	/// <summary>Field Creation Class for entity ProcessMasterEntity</summary>
	public partial class ProcessMasterFields
	{
		/// <summary>Creates a new ProcessMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProcessMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new ProcessMasterEntity.InstalledServerId field instance</summary>
		public static EntityField2 InstalledServerId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProcessMasterFieldIndex.InstalledServerId);}
		}
		/// <summary>Creates a new ProcessMasterEntity.ProcessDesc field instance</summary>
		public static EntityField2 ProcessDesc
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProcessMasterFieldIndex.ProcessDesc);}
		}
		/// <summary>Creates a new ProcessMasterEntity.ProcessId field instance</summary>
		public static EntityField2 ProcessId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProcessMasterFieldIndex.ProcessId);}
		}
		/// <summary>Creates a new ProcessMasterEntity.ProcessName field instance</summary>
		public static EntityField2 ProcessName
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProcessMasterFieldIndex.ProcessName);}
		}
		/// <summary>Creates a new ProcessMasterEntity.ProcessTypeId field instance</summary>
		public static EntityField2 ProcessTypeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProcessMasterFieldIndex.ProcessTypeId);}
		}
		/// <summary>Creates a new ProcessMasterEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProcessMasterFieldIndex.StoreId);}
		}
	}

	/// <summary>Field Creation Class for entity ProductClassMasterEntity</summary>
	public partial class ProductClassMasterFields
	{
		/// <summary>Creates a new ProductClassMasterEntity.DefaultMenuItemGroupId field instance</summary>
		public static EntityField2 DefaultMenuItemGroupId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProductClassMasterFieldIndex.DefaultMenuItemGroupId);}
		}
		/// <summary>Creates a new ProductClassMasterEntity.DefaultRevCatId field instance</summary>
		public static EntityField2 DefaultRevCatId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProductClassMasterFieldIndex.DefaultRevCatId);}
		}
		/// <summary>Creates a new ProductClassMasterEntity.DefaultRptCatId field instance</summary>
		public static EntityField2 DefaultRptCatId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProductClassMasterFieldIndex.DefaultRptCatId);}
		}
		/// <summary>Creates a new ProductClassMasterEntity.DefaultSecId field instance</summary>
		public static EntityField2 DefaultSecId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProductClassMasterFieldIndex.DefaultSecId);}
		}
		/// <summary>Creates a new ProductClassMasterEntity.DefaultTaxGrpId field instance</summary>
		public static EntityField2 DefaultTaxGrpId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProductClassMasterFieldIndex.DefaultTaxGrpId);}
		}
		/// <summary>Creates a new ProductClassMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProductClassMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new ProductClassMasterEntity.ItemRestrictedFlag field instance</summary>
		public static EntityField2 ItemRestrictedFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProductClassMasterFieldIndex.ItemRestrictedFlag);}
		}
		/// <summary>Creates a new ProductClassMasterEntity.ProdClassId field instance</summary>
		public static EntityField2 ProdClassId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProductClassMasterFieldIndex.ProdClassId);}
		}
		/// <summary>Creates a new ProductClassMasterEntity.ProdClassName field instance</summary>
		public static EntityField2 ProdClassName
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProductClassMasterFieldIndex.ProdClassName);}
		}
		/// <summary>Creates a new ProductClassMasterEntity.RowVersion field instance</summary>
		public static EntityField2 RowVersion
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProductClassMasterFieldIndex.RowVersion);}
		}
		/// <summary>Creates a new ProductClassMasterEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProductClassMasterFieldIndex.StoreId);}
		}
	}

	/// <summary>Field Creation Class for entity ProfitCenterDayPartJoinEntity</summary>
	public partial class ProfitCenterDayPartJoinFields
	{
		/// <summary>Creates a new ProfitCenterDayPartJoinEntity.DayPartName field instance</summary>
		public static EntityField2 DayPartName
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterDayPartJoinFieldIndex.DayPartName);}
		}
		/// <summary>Creates a new ProfitCenterDayPartJoinEntity.DayPartStartTime field instance</summary>
		public static EntityField2 DayPartStartTime
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterDayPartJoinFieldIndex.DayPartStartTime);}
		}
		/// <summary>Creates a new ProfitCenterDayPartJoinEntity.ProfitCenterId field instance</summary>
		public static EntityField2 ProfitCenterId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterDayPartJoinFieldIndex.ProfitCenterId);}
		}
	}

	/// <summary>Field Creation Class for entity ProfitCenterGroupJoinEntity</summary>
	public partial class ProfitCenterGroupJoinFields
	{
		/// <summary>Creates a new ProfitCenterGroupJoinEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterGroupJoinFieldIndex.EntId);}
		}
		/// <summary>Creates a new ProfitCenterGroupJoinEntity.ProfitCenterId field instance</summary>
		public static EntityField2 ProfitCenterId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterGroupJoinFieldIndex.ProfitCenterId);}
		}
		/// <summary>Creates a new ProfitCenterGroupJoinEntity.ProfitCtrGrpId field instance</summary>
		public static EntityField2 ProfitCtrGrpId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterGroupJoinFieldIndex.ProfitCtrGrpId);}
		}
	}

	/// <summary>Field Creation Class for entity ProfitCenterGroupMasterEntity</summary>
	public partial class ProfitCenterGroupMasterFields
	{
		/// <summary>Creates a new ProfitCenterGroupMasterEntity.ChargingPattern field instance</summary>
		public static EntityField2 ChargingPattern
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterGroupMasterFieldIndex.ChargingPattern);}
		}
		/// <summary>Creates a new ProfitCenterGroupMasterEntity.DataControlGroupId field instance</summary>
		public static EntityField2 DataControlGroupId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterGroupMasterFieldIndex.DataControlGroupId);}
		}
		/// <summary>Creates a new ProfitCenterGroupMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterGroupMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new ProfitCenterGroupMasterEntity.ProfitCtrAutoUpdateFlag field instance</summary>
		public static EntityField2 ProfitCtrAutoUpdateFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterGroupMasterFieldIndex.ProfitCtrAutoUpdateFlag);}
		}
		/// <summary>Creates a new ProfitCenterGroupMasterEntity.ProfitCtrGrpAbbr1 field instance</summary>
		public static EntityField2 ProfitCtrGrpAbbr1
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterGroupMasterFieldIndex.ProfitCtrGrpAbbr1);}
		}
		/// <summary>Creates a new ProfitCenterGroupMasterEntity.ProfitCtrGrpAbbr2 field instance</summary>
		public static EntityField2 ProfitCtrGrpAbbr2
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterGroupMasterFieldIndex.ProfitCtrGrpAbbr2);}
		}
		/// <summary>Creates a new ProfitCenterGroupMasterEntity.ProfitCtrGrpId field instance</summary>
		public static EntityField2 ProfitCtrGrpId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterGroupMasterFieldIndex.ProfitCtrGrpId);}
		}
		/// <summary>Creates a new ProfitCenterGroupMasterEntity.ProfitCtrGrpName field instance</summary>
		public static EntityField2 ProfitCtrGrpName
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterGroupMasterFieldIndex.ProfitCtrGrpName);}
		}
		/// <summary>Creates a new ProfitCenterGroupMasterEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterGroupMasterFieldIndex.StoreId);}
		}
	}

	/// <summary>Field Creation Class for entity ProfitCenterMasterEntity</summary>
	public partial class ProfitCenterMasterFields
	{
		/// <summary>Creates a new ProfitCenterMasterEntity.BypassCcAgencyThresholdAmount field instance</summary>
		public static EntityField2 BypassCcAgencyThresholdAmount
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterMasterFieldIndex.BypassCcAgencyThresholdAmount);}
		}
		/// <summary>Creates a new ProfitCenterMasterEntity.BypassCcPrintingThresholdAmount field instance</summary>
		public static EntityField2 BypassCcPrintingThresholdAmount
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterMasterFieldIndex.BypassCcPrintingThresholdAmount);}
		}
		/// <summary>Creates a new ProfitCenterMasterEntity.BypassCcVoiceAuthThresholdAmount field instance</summary>
		public static EntityField2 BypassCcVoiceAuthThresholdAmount
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterMasterFieldIndex.BypassCcVoiceAuthThresholdAmount);}
		}
		/// <summary>Creates a new ProfitCenterMasterEntity.ChkFtrLine1 field instance</summary>
		public static EntityField2 ChkFtrLine1
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterMasterFieldIndex.ChkFtrLine1);}
		}
		/// <summary>Creates a new ProfitCenterMasterEntity.ChkFtrLine2 field instance</summary>
		public static EntityField2 ChkFtrLine2
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterMasterFieldIndex.ChkFtrLine2);}
		}
		/// <summary>Creates a new ProfitCenterMasterEntity.ChkFtrLine3 field instance</summary>
		public static EntityField2 ChkFtrLine3
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterMasterFieldIndex.ChkFtrLine3);}
		}
		/// <summary>Creates a new ProfitCenterMasterEntity.ChkHdrLine1 field instance</summary>
		public static EntityField2 ChkHdrLine1
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterMasterFieldIndex.ChkHdrLine1);}
		}
		/// <summary>Creates a new ProfitCenterMasterEntity.ChkHdrLine2 field instance</summary>
		public static EntityField2 ChkHdrLine2
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterMasterFieldIndex.ChkHdrLine2);}
		}
		/// <summary>Creates a new ProfitCenterMasterEntity.ChkHdrLine3 field instance</summary>
		public static EntityField2 ChkHdrLine3
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterMasterFieldIndex.ChkHdrLine3);}
		}
		/// <summary>Creates a new ProfitCenterMasterEntity.DataControlGroupId field instance</summary>
		public static EntityField2 DataControlGroupId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterMasterFieldIndex.DataControlGroupId);}
		}
		/// <summary>Creates a new ProfitCenterMasterEntity.DefaultTableLayoutId field instance</summary>
		public static EntityField2 DefaultTableLayoutId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterMasterFieldIndex.DefaultTableLayoutId);}
		}
		/// <summary>Creates a new ProfitCenterMasterEntity.DocLinesAdvance field instance</summary>
		public static EntityField2 DocLinesAdvance
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterMasterFieldIndex.DocLinesAdvance);}
		}
		/// <summary>Creates a new ProfitCenterMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new ProfitCenterMasterEntity.MaxDocLinesPage field instance</summary>
		public static EntityField2 MaxDocLinesPage
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterMasterFieldIndex.MaxDocLinesPage);}
		}
		/// <summary>Creates a new ProfitCenterMasterEntity.MerchantId field instance</summary>
		public static EntityField2 MerchantId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterMasterFieldIndex.MerchantId);}
		}
		/// <summary>Creates a new ProfitCenterMasterEntity.MinRcptLinesPage field instance</summary>
		public static EntityField2 MinRcptLinesPage
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterMasterFieldIndex.MinRcptLinesPage);}
		}
		/// <summary>Creates a new ProfitCenterMasterEntity.PoleDisplayClosed field instance</summary>
		public static EntityField2 PoleDisplayClosed
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterMasterFieldIndex.PoleDisplayClosed);}
		}
		/// <summary>Creates a new ProfitCenterMasterEntity.PoleDisplayOpen field instance</summary>
		public static EntityField2 PoleDisplayOpen
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterMasterFieldIndex.PoleDisplayOpen);}
		}
		/// <summary>Creates a new ProfitCenterMasterEntity.PrimaryLanguageId field instance</summary>
		public static EntityField2 PrimaryLanguageId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterMasterFieldIndex.PrimaryLanguageId);}
		}
		/// <summary>Creates a new ProfitCenterMasterEntity.PrintByRevCatFlag field instance</summary>
		public static EntityField2 PrintByRevCatFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterMasterFieldIndex.PrintByRevCatFlag);}
		}
		/// <summary>Creates a new ProfitCenterMasterEntity.ProfitCenterDesc field instance</summary>
		public static EntityField2 ProfitCenterDesc
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterMasterFieldIndex.ProfitCenterDesc);}
		}
		/// <summary>Creates a new ProfitCenterMasterEntity.ProfitCenterId field instance</summary>
		public static EntityField2 ProfitCenterId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterMasterFieldIndex.ProfitCenterId);}
		}
		/// <summary>Creates a new ProfitCenterMasterEntity.ProfitCenterName field instance</summary>
		public static EntityField2 ProfitCenterName
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterMasterFieldIndex.ProfitCenterName);}
		}
		/// <summary>Creates a new ProfitCenterMasterEntity.ProfitCtrAbbr1 field instance</summary>
		public static EntityField2 ProfitCtrAbbr1
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterMasterFieldIndex.ProfitCtrAbbr1);}
		}
		/// <summary>Creates a new ProfitCenterMasterEntity.ProfitCtrAbbr2 field instance</summary>
		public static EntityField2 ProfitCtrAbbr2
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterMasterFieldIndex.ProfitCtrAbbr2);}
		}
		/// <summary>Creates a new ProfitCenterMasterEntity.RowVersion field instance</summary>
		public static EntityField2 RowVersion
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterMasterFieldIndex.RowVersion);}
		}
		/// <summary>Creates a new ProfitCenterMasterEntity.SalesTippableFlag field instance</summary>
		public static EntityField2 SalesTippableFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterMasterFieldIndex.SalesTippableFlag);}
		}
		/// <summary>Creates a new ProfitCenterMasterEntity.SecondaryLanguageId field instance</summary>
		public static EntityField2 SecondaryLanguageId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterMasterFieldIndex.SecondaryLanguageId);}
		}
		/// <summary>Creates a new ProfitCenterMasterEntity.SourcePropertyCode field instance</summary>
		public static EntityField2 SourcePropertyCode
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterMasterFieldIndex.SourcePropertyCode);}
		}
		/// <summary>Creates a new ProfitCenterMasterEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterMasterFieldIndex.StoreId);}
		}
		/// <summary>Creates a new ProfitCenterMasterEntity.TipEnforcementCodeId field instance</summary>
		public static EntityField2 TipEnforcementCodeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterMasterFieldIndex.TipEnforcementCodeId);}
		}
		/// <summary>Creates a new ProfitCenterMasterEntity.TipMaxPercent field instance</summary>
		public static EntityField2 TipMaxPercent
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterMasterFieldIndex.TipMaxPercent);}
		}
	}

	/// <summary>Field Creation Class for entity ProfitCenterReceiptPrinterJoinEntity</summary>
	public partial class ProfitCenterReceiptPrinterJoinFields
	{
		/// <summary>Creates a new ProfitCenterReceiptPrinterJoinEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterReceiptPrinterJoinFieldIndex.EntId);}
		}
		/// <summary>Creates a new ProfitCenterReceiptPrinterJoinEntity.PrimaryFlag field instance</summary>
		public static EntityField2 PrimaryFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterReceiptPrinterJoinFieldIndex.PrimaryFlag);}
		}
		/// <summary>Creates a new ProfitCenterReceiptPrinterJoinEntity.ProfitCenterId field instance</summary>
		public static EntityField2 ProfitCenterId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterReceiptPrinterJoinFieldIndex.ProfitCenterId);}
		}
		/// <summary>Creates a new ProfitCenterReceiptPrinterJoinEntity.ReceiptPrinterId field instance</summary>
		public static EntityField2 ReceiptPrinterId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterReceiptPrinterJoinFieldIndex.ReceiptPrinterId);}
		}
	}

	/// <summary>Field Creation Class for entity ProfitCenterTableJoinEntity</summary>
	public partial class ProfitCenterTableJoinFields
	{
		/// <summary>Creates a new ProfitCenterTableJoinEntity.BoothFlag field instance</summary>
		public static EntityField2 BoothFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterTableJoinFieldIndex.BoothFlag);}
		}
		/// <summary>Creates a new ProfitCenterTableJoinEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterTableJoinFieldIndex.EntId);}
		}
		/// <summary>Creates a new ProfitCenterTableJoinEntity.MaxSeats field instance</summary>
		public static EntityField2 MaxSeats
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterTableJoinFieldIndex.MaxSeats);}
		}
		/// <summary>Creates a new ProfitCenterTableJoinEntity.OutsideFlag field instance</summary>
		public static EntityField2 OutsideFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterTableJoinFieldIndex.OutsideFlag);}
		}
		/// <summary>Creates a new ProfitCenterTableJoinEntity.PrivateFlag field instance</summary>
		public static EntityField2 PrivateFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterTableJoinFieldIndex.PrivateFlag);}
		}
		/// <summary>Creates a new ProfitCenterTableJoinEntity.ProfitCenterId field instance</summary>
		public static EntityField2 ProfitCenterId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterTableJoinFieldIndex.ProfitCenterId);}
		}
		/// <summary>Creates a new ProfitCenterTableJoinEntity.SecurityId field instance</summary>
		public static EntityField2 SecurityId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterTableJoinFieldIndex.SecurityId);}
		}
		/// <summary>Creates a new ProfitCenterTableJoinEntity.SmokingFlag field instance</summary>
		public static EntityField2 SmokingFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterTableJoinFieldIndex.SmokingFlag);}
		}
		/// <summary>Creates a new ProfitCenterTableJoinEntity.TableId field instance</summary>
		public static EntityField2 TableId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterTableJoinFieldIndex.TableId);}
		}
		/// <summary>Creates a new ProfitCenterTableJoinEntity.TableName field instance</summary>
		public static EntityField2 TableName
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterTableJoinFieldIndex.TableName);}
		}
		/// <summary>Creates a new ProfitCenterTableJoinEntity.TableNo field instance</summary>
		public static EntityField2 TableNo
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterTableJoinFieldIndex.TableNo);}
		}
		/// <summary>Creates a new ProfitCenterTableJoinEntity.ViewFlag field instance</summary>
		public static EntityField2 ViewFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterTableJoinFieldIndex.ViewFlag);}
		}
		/// <summary>Creates a new ProfitCenterTableJoinEntity.VipFlag field instance</summary>
		public static EntityField2 VipFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(ProfitCenterTableJoinFieldIndex.VipFlag);}
		}
	}

	/// <summary>Field Creation Class for entity QuickTenderMasterEntity</summary>
	public partial class QuickTenderMasterFields
	{
		/// <summary>Creates a new QuickTenderMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(QuickTenderMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new QuickTenderMasterEntity.PaymentAmount field instance</summary>
		public static EntityField2 PaymentAmount
		{
			get { return (EntityField2)EntityFieldFactory.Create(QuickTenderMasterFieldIndex.PaymentAmount);}
		}
		/// <summary>Creates a new QuickTenderMasterEntity.QuickTenderAbbr1 field instance</summary>
		public static EntityField2 QuickTenderAbbr1
		{
			get { return (EntityField2)EntityFieldFactory.Create(QuickTenderMasterFieldIndex.QuickTenderAbbr1);}
		}
		/// <summary>Creates a new QuickTenderMasterEntity.QuickTenderAbbr2 field instance</summary>
		public static EntityField2 QuickTenderAbbr2
		{
			get { return (EntityField2)EntityFieldFactory.Create(QuickTenderMasterFieldIndex.QuickTenderAbbr2);}
		}
		/// <summary>Creates a new QuickTenderMasterEntity.QuickTenderId field instance</summary>
		public static EntityField2 QuickTenderId
		{
			get { return (EntityField2)EntityFieldFactory.Create(QuickTenderMasterFieldIndex.QuickTenderId);}
		}
		/// <summary>Creates a new QuickTenderMasterEntity.QuickTenderName field instance</summary>
		public static EntityField2 QuickTenderName
		{
			get { return (EntityField2)EntityFieldFactory.Create(QuickTenderMasterFieldIndex.QuickTenderName);}
		}
		/// <summary>Creates a new QuickTenderMasterEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(QuickTenderMasterFieldIndex.StoreId);}
		}
		/// <summary>Creates a new QuickTenderMasterEntity.TenderId field instance</summary>
		public static EntityField2 TenderId
		{
			get { return (EntityField2)EntityFieldFactory.Create(QuickTenderMasterFieldIndex.TenderId);}
		}
	}

	/// <summary>Field Creation Class for entity ReceiptPrinterMasterEntity</summary>
	public partial class ReceiptPrinterMasterFields
	{
		/// <summary>Creates a new ReceiptPrinterMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ReceiptPrinterMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new ReceiptPrinterMasterEntity.IpAddress field instance</summary>
		public static EntityField2 IpAddress
		{
			get { return (EntityField2)EntityFieldFactory.Create(ReceiptPrinterMasterFieldIndex.IpAddress);}
		}
		/// <summary>Creates a new ReceiptPrinterMasterEntity.IpPrintingPort field instance</summary>
		public static EntityField2 IpPrintingPort
		{
			get { return (EntityField2)EntityFieldFactory.Create(ReceiptPrinterMasterFieldIndex.IpPrintingPort);}
		}
		/// <summary>Creates a new ReceiptPrinterMasterEntity.IpStatusPort field instance</summary>
		public static EntityField2 IpStatusPort
		{
			get { return (EntityField2)EntityFieldFactory.Create(ReceiptPrinterMasterFieldIndex.IpStatusPort);}
		}
		/// <summary>Creates a new ReceiptPrinterMasterEntity.ReceiptPrinterDescription field instance</summary>
		public static EntityField2 ReceiptPrinterDescription
		{
			get { return (EntityField2)EntityFieldFactory.Create(ReceiptPrinterMasterFieldIndex.ReceiptPrinterDescription);}
		}
		/// <summary>Creates a new ReceiptPrinterMasterEntity.ReceiptPrinterId field instance</summary>
		public static EntityField2 ReceiptPrinterId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ReceiptPrinterMasterFieldIndex.ReceiptPrinterId);}
		}
		/// <summary>Creates a new ReceiptPrinterMasterEntity.ReceiptPrinterName field instance</summary>
		public static EntityField2 ReceiptPrinterName
		{
			get { return (EntityField2)EntityFieldFactory.Create(ReceiptPrinterMasterFieldIndex.ReceiptPrinterName);}
		}
		/// <summary>Creates a new ReceiptPrinterMasterEntity.RpInvertedFlag field instance</summary>
		public static EntityField2 RpInvertedFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(ReceiptPrinterMasterFieldIndex.RpInvertedFlag);}
		}
		/// <summary>Creates a new ReceiptPrinterMasterEntity.RpLeftMarginOffset field instance</summary>
		public static EntityField2 RpLeftMarginOffset
		{
			get { return (EntityField2)EntityFieldFactory.Create(ReceiptPrinterMasterFieldIndex.RpLeftMarginOffset);}
		}
		/// <summary>Creates a new ReceiptPrinterMasterEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ReceiptPrinterMasterFieldIndex.StoreId);}
		}
		/// <summary>Creates a new ReceiptPrinterMasterEntity.UseDefaultPrinterFontFlag field instance</summary>
		public static EntityField2 UseDefaultPrinterFontFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(ReceiptPrinterMasterFieldIndex.UseDefaultPrinterFontFlag);}
		}
	}

	/// <summary>Field Creation Class for entity ReportCatMasterEntity</summary>
	public partial class ReportCatMasterFields
	{
		/// <summary>Creates a new ReportCatMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ReportCatMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new ReportCatMasterEntity.RowVersion field instance</summary>
		public static EntityField2 RowVersion
		{
			get { return (EntityField2)EntityFieldFactory.Create(ReportCatMasterFieldIndex.RowVersion);}
		}
		/// <summary>Creates a new ReportCatMasterEntity.RptCatAbbr1 field instance</summary>
		public static EntityField2 RptCatAbbr1
		{
			get { return (EntityField2)EntityFieldFactory.Create(ReportCatMasterFieldIndex.RptCatAbbr1);}
		}
		/// <summary>Creates a new ReportCatMasterEntity.RptCatAbbr2 field instance</summary>
		public static EntityField2 RptCatAbbr2
		{
			get { return (EntityField2)EntityFieldFactory.Create(ReportCatMasterFieldIndex.RptCatAbbr2);}
		}
		/// <summary>Creates a new ReportCatMasterEntity.RptCatId field instance</summary>
		public static EntityField2 RptCatId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ReportCatMasterFieldIndex.RptCatId);}
		}
		/// <summary>Creates a new ReportCatMasterEntity.RptCatName field instance</summary>
		public static EntityField2 RptCatName
		{
			get { return (EntityField2)EntityFieldFactory.Create(ReportCatMasterFieldIndex.RptCatName);}
		}
		/// <summary>Creates a new ReportCatMasterEntity.RptSummaryId field instance</summary>
		public static EntityField2 RptSummaryId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ReportCatMasterFieldIndex.RptSummaryId);}
		}
		/// <summary>Creates a new ReportCatMasterEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ReportCatMasterFieldIndex.StoreId);}
		}
	}

	/// <summary>Field Creation Class for entity RevenueCatMasterEntity</summary>
	public partial class RevenueCatMasterFields
	{
		/// <summary>Creates a new RevenueCatMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(RevenueCatMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new RevenueCatMasterEntity.LoyaltyEarnEligibleFlag field instance</summary>
		public static EntityField2 LoyaltyEarnEligibleFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(RevenueCatMasterFieldIndex.LoyaltyEarnEligibleFlag);}
		}
		/// <summary>Creates a new RevenueCatMasterEntity.LoyaltyRedeemEligibleFlag field instance</summary>
		public static EntityField2 LoyaltyRedeemEligibleFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(RevenueCatMasterFieldIndex.LoyaltyRedeemEligibleFlag);}
		}
		/// <summary>Creates a new RevenueCatMasterEntity.RevCatAbbr1 field instance</summary>
		public static EntityField2 RevCatAbbr1
		{
			get { return (EntityField2)EntityFieldFactory.Create(RevenueCatMasterFieldIndex.RevCatAbbr1);}
		}
		/// <summary>Creates a new RevenueCatMasterEntity.RevCatAbbr2 field instance</summary>
		public static EntityField2 RevCatAbbr2
		{
			get { return (EntityField2)EntityFieldFactory.Create(RevenueCatMasterFieldIndex.RevCatAbbr2);}
		}
		/// <summary>Creates a new RevenueCatMasterEntity.RevCatId field instance</summary>
		public static EntityField2 RevCatId
		{
			get { return (EntityField2)EntityFieldFactory.Create(RevenueCatMasterFieldIndex.RevCatId);}
		}
		/// <summary>Creates a new RevenueCatMasterEntity.RevCatName field instance</summary>
		public static EntityField2 RevCatName
		{
			get { return (EntityField2)EntityFieldFactory.Create(RevenueCatMasterFieldIndex.RevCatName);}
		}
		/// <summary>Creates a new RevenueCatMasterEntity.RevClassId field instance</summary>
		public static EntityField2 RevClassId
		{
			get { return (EntityField2)EntityFieldFactory.Create(RevenueCatMasterFieldIndex.RevClassId);}
		}
		/// <summary>Creates a new RevenueCatMasterEntity.RowVersion field instance</summary>
		public static EntityField2 RowVersion
		{
			get { return (EntityField2)EntityFieldFactory.Create(RevenueCatMasterFieldIndex.RowVersion);}
		}
		/// <summary>Creates a new RevenueCatMasterEntity.RptCatId field instance</summary>
		public static EntityField2 RptCatId
		{
			get { return (EntityField2)EntityFieldFactory.Create(RevenueCatMasterFieldIndex.RptCatId);}
		}
		/// <summary>Creates a new RevenueCatMasterEntity.SalesTippableFlag field instance</summary>
		public static EntityField2 SalesTippableFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(RevenueCatMasterFieldIndex.SalesTippableFlag);}
		}
		/// <summary>Creates a new RevenueCatMasterEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(RevenueCatMasterFieldIndex.StoreId);}
		}
		/// <summary>Creates a new RevenueCatMasterEntity.TenderRestrictionFlag field instance</summary>
		public static EntityField2 TenderRestrictionFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(RevenueCatMasterFieldIndex.TenderRestrictionFlag);}
		}
	}

	/// <summary>Field Creation Class for entity RevenueClassMasterEntity</summary>
	public partial class RevenueClassMasterFields
	{
		/// <summary>Creates a new RevenueClassMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(RevenueClassMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new RevenueClassMasterEntity.RevClassAlphaId field instance</summary>
		public static EntityField2 RevClassAlphaId
		{
			get { return (EntityField2)EntityFieldFactory.Create(RevenueClassMasterFieldIndex.RevClassAlphaId);}
		}
		/// <summary>Creates a new RevenueClassMasterEntity.RevClassId field instance</summary>
		public static EntityField2 RevClassId
		{
			get { return (EntityField2)EntityFieldFactory.Create(RevenueClassMasterFieldIndex.RevClassId);}
		}
		/// <summary>Creates a new RevenueClassMasterEntity.RevClassName field instance</summary>
		public static EntityField2 RevClassName
		{
			get { return (EntityField2)EntityFieldFactory.Create(RevenueClassMasterFieldIndex.RevClassName);}
		}
		/// <summary>Creates a new RevenueClassMasterEntity.RowVersion field instance</summary>
		public static EntityField2 RowVersion
		{
			get { return (EntityField2)EntityFieldFactory.Create(RevenueClassMasterFieldIndex.RowVersion);}
		}
		/// <summary>Creates a new RevenueClassMasterEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(RevenueClassMasterFieldIndex.StoreId);}
		}
	}

	/// <summary>Field Creation Class for entity RowLockMasterEntity</summary>
	public partial class RowLockMasterFields
	{
		/// <summary>Creates a new RowLockMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(RowLockMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new RowLockMasterEntity.LockTimeStamp field instance</summary>
		public static EntityField2 LockTimeStamp
		{
			get { return (EntityField2)EntityFieldFactory.Create(RowLockMasterFieldIndex.LockTimeStamp);}
		}
		/// <summary>Creates a new RowLockMasterEntity.TableId field instance</summary>
		public static EntityField2 TableId
		{
			get { return (EntityField2)EntityFieldFactory.Create(RowLockMasterFieldIndex.TableId);}
		}
		/// <summary>Creates a new RowLockMasterEntity.TableName field instance</summary>
		public static EntityField2 TableName
		{
			get { return (EntityField2)EntityFieldFactory.Create(RowLockMasterFieldIndex.TableName);}
		}
	}

	/// <summary>Field Creation Class for entity ScreenTemplateMasterEntity</summary>
	public partial class ScreenTemplateMasterFields
	{
		/// <summary>Creates a new ScreenTemplateMasterEntity.AllowButtonOverlapFlag field instance</summary>
		public static EntityField2 AllowButtonOverlapFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(ScreenTemplateMasterFieldIndex.AllowButtonOverlapFlag);}
		}
		/// <summary>Creates a new ScreenTemplateMasterEntity.ButtonPadding field instance</summary>
		public static EntityField2 ButtonPadding
		{
			get { return (EntityField2)EntityFieldFactory.Create(ScreenTemplateMasterFieldIndex.ButtonPadding);}
		}
		/// <summary>Creates a new ScreenTemplateMasterEntity.DefaultBackcolor field instance</summary>
		public static EntityField2 DefaultBackcolor
		{
			get { return (EntityField2)EntityFieldFactory.Create(ScreenTemplateMasterFieldIndex.DefaultBackcolor);}
		}
		/// <summary>Creates a new ScreenTemplateMasterEntity.DefaultHeight field instance</summary>
		public static EntityField2 DefaultHeight
		{
			get { return (EntityField2)EntityFieldFactory.Create(ScreenTemplateMasterFieldIndex.DefaultHeight);}
		}
		/// <summary>Creates a new ScreenTemplateMasterEntity.DefaultWidth field instance</summary>
		public static EntityField2 DefaultWidth
		{
			get { return (EntityField2)EntityFieldFactory.Create(ScreenTemplateMasterFieldIndex.DefaultWidth);}
		}
		/// <summary>Creates a new ScreenTemplateMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ScreenTemplateMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new ScreenTemplateMasterEntity.GridSize field instance</summary>
		public static EntityField2 GridSize
		{
			get { return (EntityField2)EntityFieldFactory.Create(ScreenTemplateMasterFieldIndex.GridSize);}
		}
		/// <summary>Creates a new ScreenTemplateMasterEntity.LockDownFlag field instance</summary>
		public static EntityField2 LockDownFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(ScreenTemplateMasterFieldIndex.LockDownFlag);}
		}
		/// <summary>Creates a new ScreenTemplateMasterEntity.ScreenTemplateId field instance</summary>
		public static EntityField2 ScreenTemplateId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ScreenTemplateMasterFieldIndex.ScreenTemplateId);}
		}
		/// <summary>Creates a new ScreenTemplateMasterEntity.ScreenTemplateName field instance</summary>
		public static EntityField2 ScreenTemplateName
		{
			get { return (EntityField2)EntityFieldFactory.Create(ScreenTemplateMasterFieldIndex.ScreenTemplateName);}
		}
		/// <summary>Creates a new ScreenTemplateMasterEntity.ScreenTemplateText field instance</summary>
		public static EntityField2 ScreenTemplateText
		{
			get { return (EntityField2)EntityFieldFactory.Create(ScreenTemplateMasterFieldIndex.ScreenTemplateText);}
		}
		/// <summary>Creates a new ScreenTemplateMasterEntity.SnapToGridFlag field instance</summary>
		public static EntityField2 SnapToGridFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(ScreenTemplateMasterFieldIndex.SnapToGridFlag);}
		}
		/// <summary>Creates a new ScreenTemplateMasterEntity.StoreFlag field instance</summary>
		public static EntityField2 StoreFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(ScreenTemplateMasterFieldIndex.StoreFlag);}
		}
		/// <summary>Creates a new ScreenTemplateMasterEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ScreenTemplateMasterFieldIndex.StoreId);}
		}
		/// <summary>Creates a new ScreenTemplateMasterEntity.ThemeId field instance</summary>
		public static EntityField2 ThemeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ScreenTemplateMasterFieldIndex.ThemeId);}
		}
	}

	/// <summary>Field Creation Class for entity ScreenTemplateMigrpJoinEntity</summary>
	public partial class ScreenTemplateMigrpJoinFields
	{
		/// <summary>Creates a new ScreenTemplateMigrpJoinEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ScreenTemplateMigrpJoinFieldIndex.EntId);}
		}
		/// <summary>Creates a new ScreenTemplateMigrpJoinEntity.MenuItemGroupId field instance</summary>
		public static EntityField2 MenuItemGroupId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ScreenTemplateMigrpJoinFieldIndex.MenuItemGroupId);}
		}
		/// <summary>Creates a new ScreenTemplateMigrpJoinEntity.ScreenTemplateId field instance</summary>
		public static EntityField2 ScreenTemplateId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ScreenTemplateMigrpJoinFieldIndex.ScreenTemplateId);}
		}
	}

	/// <summary>Field Creation Class for entity SecurityLevelMasterEntity</summary>
	public partial class SecurityLevelMasterFields
	{
		/// <summary>Creates a new SecurityLevelMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(SecurityLevelMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new SecurityLevelMasterEntity.SecurityId field instance</summary>
		public static EntityField2 SecurityId
		{
			get { return (EntityField2)EntityFieldFactory.Create(SecurityLevelMasterFieldIndex.SecurityId);}
		}
		/// <summary>Creates a new SecurityLevelMasterEntity.SecurityLevelName field instance</summary>
		public static EntityField2 SecurityLevelName
		{
			get { return (EntityField2)EntityFieldFactory.Create(SecurityLevelMasterFieldIndex.SecurityLevelName);}
		}
	}

	/// <summary>Field Creation Class for entity SelectionGroupItemJoinEntity</summary>
	public partial class SelectionGroupItemJoinFields
	{
		/// <summary>Creates a new SelectionGroupItemJoinEntity.AdjustedAmt field instance</summary>
		public static EntityField2 AdjustedAmt
		{
			get { return (EntityField2)EntityFieldFactory.Create(SelectionGroupItemJoinFieldIndex.AdjustedAmt);}
		}
		/// <summary>Creates a new SelectionGroupItemJoinEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(SelectionGroupItemJoinFieldIndex.EntId);}
		}
		/// <summary>Creates a new SelectionGroupItemJoinEntity.MenuItemId field instance</summary>
		public static EntityField2 MenuItemId
		{
			get { return (EntityField2)EntityFieldFactory.Create(SelectionGroupItemJoinFieldIndex.MenuItemId);}
		}
		/// <summary>Creates a new SelectionGroupItemJoinEntity.SelectionGroupId field instance</summary>
		public static EntityField2 SelectionGroupId
		{
			get { return (EntityField2)EntityFieldFactory.Create(SelectionGroupItemJoinFieldIndex.SelectionGroupId);}
		}
		/// <summary>Creates a new SelectionGroupItemJoinEntity.SequenceId field instance</summary>
		public static EntityField2 SequenceId
		{
			get { return (EntityField2)EntityFieldFactory.Create(SelectionGroupItemJoinFieldIndex.SequenceId);}
		}
	}

	/// <summary>Field Creation Class for entity SelectionGroupMasterEntity</summary>
	public partial class SelectionGroupMasterFields
	{
		/// <summary>Creates a new SelectionGroupMasterEntity.DivisionId field instance</summary>
		public static EntityField2 DivisionId
		{
			get { return (EntityField2)EntityFieldFactory.Create(SelectionGroupMasterFieldIndex.DivisionId);}
		}
		/// <summary>Creates a new SelectionGroupMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(SelectionGroupMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new SelectionGroupMasterEntity.RowVersion field instance</summary>
		public static EntityField2 RowVersion
		{
			get { return (EntityField2)EntityFieldFactory.Create(SelectionGroupMasterFieldIndex.RowVersion);}
		}
		/// <summary>Creates a new SelectionGroupMasterEntity.SelectionGroupId field instance</summary>
		public static EntityField2 SelectionGroupId
		{
			get { return (EntityField2)EntityFieldFactory.Create(SelectionGroupMasterFieldIndex.SelectionGroupId);}
		}
		/// <summary>Creates a new SelectionGroupMasterEntity.SelectionGroupKpLabel field instance</summary>
		public static EntityField2 SelectionGroupKpLabel
		{
			get { return (EntityField2)EntityFieldFactory.Create(SelectionGroupMasterFieldIndex.SelectionGroupKpLabel);}
		}
		/// <summary>Creates a new SelectionGroupMasterEntity.SelectionGroupName field instance</summary>
		public static EntityField2 SelectionGroupName
		{
			get { return (EntityField2)EntityFieldFactory.Create(SelectionGroupMasterFieldIndex.SelectionGroupName);}
		}
		/// <summary>Creates a new SelectionGroupMasterEntity.SelectionGroupReceiptLabel field instance</summary>
		public static EntityField2 SelectionGroupReceiptLabel
		{
			get { return (EntityField2)EntityFieldFactory.Create(SelectionGroupMasterFieldIndex.SelectionGroupReceiptLabel);}
		}
		/// <summary>Creates a new SelectionGroupMasterEntity.TerminalPendingText field instance</summary>
		public static EntityField2 TerminalPendingText
		{
			get { return (EntityField2)EntityFieldFactory.Create(SelectionGroupMasterFieldIndex.TerminalPendingText);}
		}
		/// <summary>Creates a new SelectionGroupMasterEntity.TerminalPrompt field instance</summary>
		public static EntityField2 TerminalPrompt
		{
			get { return (EntityField2)EntityFieldFactory.Create(SelectionGroupMasterFieldIndex.TerminalPrompt);}
		}
	}

	/// <summary>Field Creation Class for entity ServiceManagerProxyMasterEntity</summary>
	public partial class ServiceManagerProxyMasterFields
	{
		/// <summary>Creates a new ServiceManagerProxyMasterEntity.CampusCardDuration field instance</summary>
		public static EntityField2 CampusCardDuration
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServiceManagerProxyMasterFieldIndex.CampusCardDuration);}
		}
		/// <summary>Creates a new ServiceManagerProxyMasterEntity.CampusCardUrl field instance</summary>
		public static EntityField2 CampusCardUrl
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServiceManagerProxyMasterFieldIndex.CampusCardUrl);}
		}
		/// <summary>Creates a new ServiceManagerProxyMasterEntity.CampusCardVersion field instance</summary>
		public static EntityField2 CampusCardVersion
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServiceManagerProxyMasterFieldIndex.CampusCardVersion);}
		}
		/// <summary>Creates a new ServiceManagerProxyMasterEntity.CompCardDuration field instance</summary>
		public static EntityField2 CompCardDuration
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServiceManagerProxyMasterFieldIndex.CompCardDuration);}
		}
		/// <summary>Creates a new ServiceManagerProxyMasterEntity.CompCardUrl field instance</summary>
		public static EntityField2 CompCardUrl
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServiceManagerProxyMasterFieldIndex.CompCardUrl);}
		}
		/// <summary>Creates a new ServiceManagerProxyMasterEntity.CompCardVersion field instance</summary>
		public static EntityField2 CompCardVersion
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServiceManagerProxyMasterFieldIndex.CompCardVersion);}
		}
		/// <summary>Creates a new ServiceManagerProxyMasterEntity.CredentialModeType field instance</summary>
		public static EntityField2 CredentialModeType
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServiceManagerProxyMasterFieldIndex.CredentialModeType);}
		}
		/// <summary>Creates a new ServiceManagerProxyMasterEntity.CreditCardDuration field instance</summary>
		public static EntityField2 CreditCardDuration
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServiceManagerProxyMasterFieldIndex.CreditCardDuration);}
		}
		/// <summary>Creates a new ServiceManagerProxyMasterEntity.CreditCardUrl field instance</summary>
		public static EntityField2 CreditCardUrl
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServiceManagerProxyMasterFieldIndex.CreditCardUrl);}
		}
		/// <summary>Creates a new ServiceManagerProxyMasterEntity.CreditCardVersion field instance</summary>
		public static EntityField2 CreditCardVersion
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServiceManagerProxyMasterFieldIndex.CreditCardVersion);}
		}
		/// <summary>Creates a new ServiceManagerProxyMasterEntity.Description field instance</summary>
		public static EntityField2 Description
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServiceManagerProxyMasterFieldIndex.Description);}
		}
		/// <summary>Creates a new ServiceManagerProxyMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServiceManagerProxyMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new ServiceManagerProxyMasterEntity.GiftCardDuration field instance</summary>
		public static EntityField2 GiftCardDuration
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServiceManagerProxyMasterFieldIndex.GiftCardDuration);}
		}
		/// <summary>Creates a new ServiceManagerProxyMasterEntity.GiftCardUrl field instance</summary>
		public static EntityField2 GiftCardUrl
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServiceManagerProxyMasterFieldIndex.GiftCardUrl);}
		}
		/// <summary>Creates a new ServiceManagerProxyMasterEntity.GiftCardVersion field instance</summary>
		public static EntityField2 GiftCardVersion
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServiceManagerProxyMasterFieldIndex.GiftCardVersion);}
		}
		/// <summary>Creates a new ServiceManagerProxyMasterEntity.GuestInfoDuration field instance</summary>
		public static EntityField2 GuestInfoDuration
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServiceManagerProxyMasterFieldIndex.GuestInfoDuration);}
		}
		/// <summary>Creates a new ServiceManagerProxyMasterEntity.GuestInfoUrl field instance</summary>
		public static EntityField2 GuestInfoUrl
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServiceManagerProxyMasterFieldIndex.GuestInfoUrl);}
		}
		/// <summary>Creates a new ServiceManagerProxyMasterEntity.GuestInfoVersion field instance</summary>
		public static EntityField2 GuestInfoVersion
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServiceManagerProxyMasterFieldIndex.GuestInfoVersion);}
		}
		/// <summary>Creates a new ServiceManagerProxyMasterEntity.Hash field instance</summary>
		public static EntityField2 Hash
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServiceManagerProxyMasterFieldIndex.Hash);}
		}
		/// <summary>Creates a new ServiceManagerProxyMasterEntity.LoyaltyCardDuration field instance</summary>
		public static EntityField2 LoyaltyCardDuration
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServiceManagerProxyMasterFieldIndex.LoyaltyCardDuration);}
		}
		/// <summary>Creates a new ServiceManagerProxyMasterEntity.LoyaltyCardUrl field instance</summary>
		public static EntityField2 LoyaltyCardUrl
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServiceManagerProxyMasterFieldIndex.LoyaltyCardUrl);}
		}
		/// <summary>Creates a new ServiceManagerProxyMasterEntity.LoyaltyCardVersion field instance</summary>
		public static EntityField2 LoyaltyCardVersion
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServiceManagerProxyMasterFieldIndex.LoyaltyCardVersion);}
		}
		/// <summary>Creates a new ServiceManagerProxyMasterEntity.Password field instance</summary>
		public static EntityField2 Password
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServiceManagerProxyMasterFieldIndex.Password);}
		}
		/// <summary>Creates a new ServiceManagerProxyMasterEntity.ServiceManagerProxyId field instance</summary>
		public static EntityField2 ServiceManagerProxyId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServiceManagerProxyMasterFieldIndex.ServiceManagerProxyId);}
		}
		/// <summary>Creates a new ServiceManagerProxyMasterEntity.ServiceManagerProxyName field instance</summary>
		public static EntityField2 ServiceManagerProxyName
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServiceManagerProxyMasterFieldIndex.ServiceManagerProxyName);}
		}
		/// <summary>Creates a new ServiceManagerProxyMasterEntity.SignatureCaptureUrl field instance</summary>
		public static EntityField2 SignatureCaptureUrl
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServiceManagerProxyMasterFieldIndex.SignatureCaptureUrl);}
		}
		/// <summary>Creates a new ServiceManagerProxyMasterEntity.SignatureCaptureVersion field instance</summary>
		public static EntityField2 SignatureCaptureVersion
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServiceManagerProxyMasterFieldIndex.SignatureCaptureVersion);}
		}
		/// <summary>Creates a new ServiceManagerProxyMasterEntity.SiteId field instance</summary>
		public static EntityField2 SiteId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServiceManagerProxyMasterFieldIndex.SiteId);}
		}
		/// <summary>Creates a new ServiceManagerProxyMasterEntity.Username field instance</summary>
		public static EntityField2 Username
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServiceManagerProxyMasterFieldIndex.Username);}
		}
		/// <summary>Creates a new ServiceManagerProxyMasterEntity.UsingRguestPaymentDeviceFlag field instance</summary>
		public static EntityField2 UsingRguestPaymentDeviceFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(ServiceManagerProxyMasterFieldIndex.UsingRguestPaymentDeviceFlag);}
		}
	}

	/// <summary>Field Creation Class for entity SmuCommandMasterEntity</summary>
	public partial class SmuCommandMasterFields
	{
		/// <summary>Creates a new SmuCommandMasterEntity.CommandId field instance</summary>
		public static EntityField2 CommandId
		{
			get { return (EntityField2)EntityFieldFactory.Create(SmuCommandMasterFieldIndex.CommandId);}
		}
		/// <summary>Creates a new SmuCommandMasterEntity.CreatedUtcDateTime field instance</summary>
		public static EntityField2 CreatedUtcDateTime
		{
			get { return (EntityField2)EntityFieldFactory.Create(SmuCommandMasterFieldIndex.CreatedUtcDateTime);}
		}
		/// <summary>Creates a new SmuCommandMasterEntity.DeviceId field instance</summary>
		public static EntityField2 DeviceId
		{
			get { return (EntityField2)EntityFieldFactory.Create(SmuCommandMasterFieldIndex.DeviceId);}
		}
		/// <summary>Creates a new SmuCommandMasterEntity.DeviceTypeId field instance</summary>
		public static EntityField2 DeviceTypeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(SmuCommandMasterFieldIndex.DeviceTypeId);}
		}
		/// <summary>Creates a new SmuCommandMasterEntity.DivisionId field instance</summary>
		public static EntityField2 DivisionId
		{
			get { return (EntityField2)EntityFieldFactory.Create(SmuCommandMasterFieldIndex.DivisionId);}
		}
		/// <summary>Creates a new SmuCommandMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(SmuCommandMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new SmuCommandMasterEntity.ExtraData field instance</summary>
		public static EntityField2 ExtraData
		{
			get { return (EntityField2)EntityFieldFactory.Create(SmuCommandMasterFieldIndex.ExtraData);}
		}
		/// <summary>Creates a new SmuCommandMasterEntity.RecordId field instance</summary>
		public static EntityField2 RecordId
		{
			get { return (EntityField2)EntityFieldFactory.Create(SmuCommandMasterFieldIndex.RecordId);}
		}
		/// <summary>Creates a new SmuCommandMasterEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(SmuCommandMasterFieldIndex.StoreId);}
		}
	}

	/// <summary>Field Creation Class for entity SmuStatusMasterEntity</summary>
	public partial class SmuStatusMasterFields
	{
		/// <summary>Creates a new SmuStatusMasterEntity.ConfigurationLoadUtcTime field instance</summary>
		public static EntityField2 ConfigurationLoadUtcTime
		{
			get { return (EntityField2)EntityFieldFactory.Create(SmuStatusMasterFieldIndex.ConfigurationLoadUtcTime);}
		}
		/// <summary>Creates a new SmuStatusMasterEntity.CurrentClockUtcTime field instance</summary>
		public static EntityField2 CurrentClockUtcTime
		{
			get { return (EntityField2)EntityFieldFactory.Create(SmuStatusMasterFieldIndex.CurrentClockUtcTime);}
		}
		/// <summary>Creates a new SmuStatusMasterEntity.DeviceId field instance</summary>
		public static EntityField2 DeviceId
		{
			get { return (EntityField2)EntityFieldFactory.Create(SmuStatusMasterFieldIndex.DeviceId);}
		}
		/// <summary>Creates a new SmuStatusMasterEntity.DeviceIpAddress field instance</summary>
		public static EntityField2 DeviceIpAddress
		{
			get { return (EntityField2)EntityFieldFactory.Create(SmuStatusMasterFieldIndex.DeviceIpAddress);}
		}
		/// <summary>Creates a new SmuStatusMasterEntity.DeviceTypeId field instance</summary>
		public static EntityField2 DeviceTypeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(SmuStatusMasterFieldIndex.DeviceTypeId);}
		}
		/// <summary>Creates a new SmuStatusMasterEntity.DivisionId field instance</summary>
		public static EntityField2 DivisionId
		{
			get { return (EntityField2)EntityFieldFactory.Create(SmuStatusMasterFieldIndex.DivisionId);}
		}
		/// <summary>Creates a new SmuStatusMasterEntity.Drawer1EmpId field instance</summary>
		public static EntityField2 Drawer1EmpId
		{
			get { return (EntityField2)EntityFieldFactory.Create(SmuStatusMasterFieldIndex.Drawer1EmpId);}
		}
		/// <summary>Creates a new SmuStatusMasterEntity.Drawer2EmpId field instance</summary>
		public static EntityField2 Drawer2EmpId
		{
			get { return (EntityField2)EntityFieldFactory.Create(SmuStatusMasterFieldIndex.Drawer2EmpId);}
		}
		/// <summary>Creates a new SmuStatusMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(SmuStatusMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new SmuStatusMasterEntity.IsLtsInitSuccessFlag field instance</summary>
		public static EntityField2 IsLtsInitSuccessFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(SmuStatusMasterFieldIndex.IsLtsInitSuccessFlag);}
		}
		/// <summary>Creates a new SmuStatusMasterEntity.IsTerminalLockedFlag field instance</summary>
		public static EntityField2 IsTerminalLockedFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(SmuStatusMasterFieldIndex.IsTerminalLockedFlag);}
		}
		/// <summary>Creates a new SmuStatusMasterEntity.IsTerminalOnline field instance</summary>
		public static EntityField2 IsTerminalOnline
		{
			get { return (EntityField2)EntityFieldFactory.Create(SmuStatusMasterFieldIndex.IsTerminalOnline);}
		}
		/// <summary>Creates a new SmuStatusMasterEntity.IsTrainingModeFlag field instance</summary>
		public static EntityField2 IsTrainingModeFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(SmuStatusMasterFieldIndex.IsTrainingModeFlag);}
		}
		/// <summary>Creates a new SmuStatusMasterEntity.LastCheckNumber field instance</summary>
		public static EntityField2 LastCheckNumber
		{
			get { return (EntityField2)EntityFieldFactory.Create(SmuStatusMasterFieldIndex.LastCheckNumber);}
		}
		/// <summary>Creates a new SmuStatusMasterEntity.LastUpdateUtcDateTime field instance</summary>
		public static EntityField2 LastUpdateUtcDateTime
		{
			get { return (EntityField2)EntityFieldFactory.Create(SmuStatusMasterFieldIndex.LastUpdateUtcDateTime);}
		}
		/// <summary>Creates a new SmuStatusMasterEntity.LtsAvailabilityFlag field instance</summary>
		public static EntityField2 LtsAvailabilityFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(SmuStatusMasterFieldIndex.LtsAvailabilityFlag);}
		}
		/// <summary>Creates a new SmuStatusMasterEntity.LtsServerIpAddress field instance</summary>
		public static EntityField2 LtsServerIpAddress
		{
			get { return (EntityField2)EntityFieldFactory.Create(SmuStatusMasterFieldIndex.LtsServerIpAddress);}
		}
		/// <summary>Creates a new SmuStatusMasterEntity.MealPeriodId field instance</summary>
		public static EntityField2 MealPeriodId
		{
			get { return (EntityField2)EntityFieldFactory.Create(SmuStatusMasterFieldIndex.MealPeriodId);}
		}
		/// <summary>Creates a new SmuStatusMasterEntity.ProfitCenterId field instance</summary>
		public static EntityField2 ProfitCenterId
		{
			get { return (EntityField2)EntityFieldFactory.Create(SmuStatusMasterFieldIndex.ProfitCenterId);}
		}
		/// <summary>Creates a new SmuStatusMasterEntity.ProgramLoadUtcTime field instance</summary>
		public static EntityField2 ProgramLoadUtcTime
		{
			get { return (EntityField2)EntityFieldFactory.Create(SmuStatusMasterFieldIndex.ProgramLoadUtcTime);}
		}
		/// <summary>Creates a new SmuStatusMasterEntity.ScreenState field instance</summary>
		public static EntityField2 ScreenState
		{
			get { return (EntityField2)EntityFieldFactory.Create(SmuStatusMasterFieldIndex.ScreenState);}
		}
		/// <summary>Creates a new SmuStatusMasterEntity.SignonEmpId field instance</summary>
		public static EntityField2 SignonEmpId
		{
			get { return (EntityField2)EntityFieldFactory.Create(SmuStatusMasterFieldIndex.SignonEmpId);}
		}
		/// <summary>Creates a new SmuStatusMasterEntity.SignonJobcodeId field instance</summary>
		public static EntityField2 SignonJobcodeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(SmuStatusMasterFieldIndex.SignonJobcodeId);}
		}
		/// <summary>Creates a new SmuStatusMasterEntity.SmuStatusId field instance</summary>
		public static EntityField2 SmuStatusId
		{
			get { return (EntityField2)EntityFieldFactory.Create(SmuStatusMasterFieldIndex.SmuStatusId);}
		}
		/// <summary>Creates a new SmuStatusMasterEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(SmuStatusMasterFieldIndex.StoreId);}
		}
		/// <summary>Creates a new SmuStatusMasterEntity.TlogCheckCount field instance</summary>
		public static EntityField2 TlogCheckCount
		{
			get { return (EntityField2)EntityFieldFactory.Create(SmuStatusMasterFieldIndex.TlogCheckCount);}
		}
		/// <summary>Creates a new SmuStatusMasterEntity.VersionNumber field instance</summary>
		public static EntityField2 VersionNumber
		{
			get { return (EntityField2)EntityFieldFactory.Create(SmuStatusMasterFieldIndex.VersionNumber);}
		}
	}

	/// <summary>Field Creation Class for entity SpecialInstrMasterEntity</summary>
	public partial class SpecialInstrMasterFields
	{
		/// <summary>Creates a new SpecialInstrMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(SpecialInstrMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new SpecialInstrMasterEntity.RowVersion field instance</summary>
		public static EntityField2 RowVersion
		{
			get { return (EntityField2)EntityFieldFactory.Create(SpecialInstrMasterFieldIndex.RowVersion);}
		}
		/// <summary>Creates a new SpecialInstrMasterEntity.SiAbbr1 field instance</summary>
		public static EntityField2 SiAbbr1
		{
			get { return (EntityField2)EntityFieldFactory.Create(SpecialInstrMasterFieldIndex.SiAbbr1);}
		}
		/// <summary>Creates a new SpecialInstrMasterEntity.SiAbbr2 field instance</summary>
		public static EntityField2 SiAbbr2
		{
			get { return (EntityField2)EntityFieldFactory.Create(SpecialInstrMasterFieldIndex.SiAbbr2);}
		}
		/// <summary>Creates a new SpecialInstrMasterEntity.SiId field instance</summary>
		public static EntityField2 SiId
		{
			get { return (EntityField2)EntityFieldFactory.Create(SpecialInstrMasterFieldIndex.SiId);}
		}
		/// <summary>Creates a new SpecialInstrMasterEntity.SiName field instance</summary>
		public static EntityField2 SiName
		{
			get { return (EntityField2)EntityFieldFactory.Create(SpecialInstrMasterFieldIndex.SiName);}
		}
		/// <summary>Creates a new SpecialInstrMasterEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(SpecialInstrMasterFieldIndex.StoreId);}
		}
	}

	/// <summary>Field Creation Class for entity SystemConfigurationMasterEntity</summary>
	public partial class SystemConfigurationMasterFields
	{
		/// <summary>Creates a new SystemConfigurationMasterEntity.PropertyName field instance</summary>
		public static EntityField2 PropertyName
		{
			get { return (EntityField2)EntityFieldFactory.Create(SystemConfigurationMasterFieldIndex.PropertyName);}
		}
		/// <summary>Creates a new SystemConfigurationMasterEntity.PropertyValue field instance</summary>
		public static EntityField2 PropertyValue
		{
			get { return (EntityField2)EntityFieldFactory.Create(SystemConfigurationMasterFieldIndex.PropertyValue);}
		}
	}

	/// <summary>Field Creation Class for entity TableLayoutMasterEntity</summary>
	public partial class TableLayoutMasterFields
	{
		/// <summary>Creates a new TableLayoutMasterEntity.AllowButtonOverlapFlag field instance</summary>
		public static EntityField2 AllowButtonOverlapFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(TableLayoutMasterFieldIndex.AllowButtonOverlapFlag);}
		}
		/// <summary>Creates a new TableLayoutMasterEntity.ButtonPadding field instance</summary>
		public static EntityField2 ButtonPadding
		{
			get { return (EntityField2)EntityFieldFactory.Create(TableLayoutMasterFieldIndex.ButtonPadding);}
		}
		/// <summary>Creates a new TableLayoutMasterEntity.DataControlGroupId field instance</summary>
		public static EntityField2 DataControlGroupId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TableLayoutMasterFieldIndex.DataControlGroupId);}
		}
		/// <summary>Creates a new TableLayoutMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TableLayoutMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new TableLayoutMasterEntity.GridSize field instance</summary>
		public static EntityField2 GridSize
		{
			get { return (EntityField2)EntityFieldFactory.Create(TableLayoutMasterFieldIndex.GridSize);}
		}
		/// <summary>Creates a new TableLayoutMasterEntity.ScreenSizeId field instance</summary>
		public static EntityField2 ScreenSizeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TableLayoutMasterFieldIndex.ScreenSizeId);}
		}
		/// <summary>Creates a new TableLayoutMasterEntity.ScreenTypeId field instance</summary>
		public static EntityField2 ScreenTypeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TableLayoutMasterFieldIndex.ScreenTypeId);}
		}
		/// <summary>Creates a new TableLayoutMasterEntity.SnapToGridFlag field instance</summary>
		public static EntityField2 SnapToGridFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(TableLayoutMasterFieldIndex.SnapToGridFlag);}
		}
		/// <summary>Creates a new TableLayoutMasterEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TableLayoutMasterFieldIndex.StoreId);}
		}
		/// <summary>Creates a new TableLayoutMasterEntity.ThemeId field instance</summary>
		public static EntityField2 ThemeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TableLayoutMasterFieldIndex.ThemeId);}
		}
		/// <summary>Creates a new TableLayoutMasterEntity.TlAbbr1 field instance</summary>
		public static EntityField2 TlAbbr1
		{
			get { return (EntityField2)EntityFieldFactory.Create(TableLayoutMasterFieldIndex.TlAbbr1);}
		}
		/// <summary>Creates a new TableLayoutMasterEntity.TlAbbr2 field instance</summary>
		public static EntityField2 TlAbbr2
		{
			get { return (EntityField2)EntityFieldFactory.Create(TableLayoutMasterFieldIndex.TlAbbr2);}
		}
		/// <summary>Creates a new TableLayoutMasterEntity.TlId field instance</summary>
		public static EntityField2 TlId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TableLayoutMasterFieldIndex.TlId);}
		}
		/// <summary>Creates a new TableLayoutMasterEntity.TlName field instance</summary>
		public static EntityField2 TlName
		{
			get { return (EntityField2)EntityFieldFactory.Create(TableLayoutMasterFieldIndex.TlName);}
		}
	}

	/// <summary>Field Creation Class for entity TaxCatMasterEntity</summary>
	public partial class TaxCatMasterFields
	{
		/// <summary>Creates a new TaxCatMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TaxCatMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new TaxCatMasterEntity.RowVersion field instance</summary>
		public static EntityField2 RowVersion
		{
			get { return (EntityField2)EntityFieldFactory.Create(TaxCatMasterFieldIndex.RowVersion);}
		}
		/// <summary>Creates a new TaxCatMasterEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TaxCatMasterFieldIndex.StoreId);}
		}
		/// <summary>Creates a new TaxCatMasterEntity.TaxCatAbbr1 field instance</summary>
		public static EntityField2 TaxCatAbbr1
		{
			get { return (EntityField2)EntityFieldFactory.Create(TaxCatMasterFieldIndex.TaxCatAbbr1);}
		}
		/// <summary>Creates a new TaxCatMasterEntity.TaxCatAbbr2 field instance</summary>
		public static EntityField2 TaxCatAbbr2
		{
			get { return (EntityField2)EntityFieldFactory.Create(TaxCatMasterFieldIndex.TaxCatAbbr2);}
		}
		/// <summary>Creates a new TaxCatMasterEntity.TaxCatId field instance</summary>
		public static EntityField2 TaxCatId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TaxCatMasterFieldIndex.TaxCatId);}
		}
		/// <summary>Creates a new TaxCatMasterEntity.TaxCatName field instance</summary>
		public static EntityField2 TaxCatName
		{
			get { return (EntityField2)EntityFieldFactory.Create(TaxCatMasterFieldIndex.TaxCatName);}
		}
	}

	/// <summary>Field Creation Class for entity TaxGroupMasterEntity</summary>
	public partial class TaxGroupMasterFields
	{
		/// <summary>Creates a new TaxGroupMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TaxGroupMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new TaxGroupMasterEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TaxGroupMasterFieldIndex.StoreId);}
		}
		/// <summary>Creates a new TaxGroupMasterEntity.TaxGrpAbbr1 field instance</summary>
		public static EntityField2 TaxGrpAbbr1
		{
			get { return (EntityField2)EntityFieldFactory.Create(TaxGroupMasterFieldIndex.TaxGrpAbbr1);}
		}
		/// <summary>Creates a new TaxGroupMasterEntity.TaxGrpAbbr2 field instance</summary>
		public static EntityField2 TaxGrpAbbr2
		{
			get { return (EntityField2)EntityFieldFactory.Create(TaxGroupMasterFieldIndex.TaxGrpAbbr2);}
		}
		/// <summary>Creates a new TaxGroupMasterEntity.TaxGrpId field instance</summary>
		public static EntityField2 TaxGrpId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TaxGroupMasterFieldIndex.TaxGrpId);}
		}
		/// <summary>Creates a new TaxGroupMasterEntity.TaxGrpName field instance</summary>
		public static EntityField2 TaxGrpName
		{
			get { return (EntityField2)EntityFieldFactory.Create(TaxGroupMasterFieldIndex.TaxGrpName);}
		}
	}

	/// <summary>Field Creation Class for entity TaxGrpTaxJoinEntity</summary>
	public partial class TaxGrpTaxJoinFields
	{
		/// <summary>Creates a new TaxGrpTaxJoinEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TaxGrpTaxJoinFieldIndex.EntId);}
		}
		/// <summary>Creates a new TaxGrpTaxJoinEntity.TaxGrpId field instance</summary>
		public static EntityField2 TaxGrpId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TaxGrpTaxJoinFieldIndex.TaxGrpId);}
		}
		/// <summary>Creates a new TaxGrpTaxJoinEntity.TaxId field instance</summary>
		public static EntityField2 TaxId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TaxGrpTaxJoinFieldIndex.TaxId);}
		}
	}

	/// <summary>Field Creation Class for entity TaxMasterEntity</summary>
	public partial class TaxMasterFields
	{
		/// <summary>Creates a new TaxMasterEntity.CompoundLevelId field instance</summary>
		public static EntityField2 CompoundLevelId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TaxMasterFieldIndex.CompoundLevelId);}
		}
		/// <summary>Creates a new TaxMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TaxMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new TaxMasterEntity.LimitTaxReportingFlag field instance</summary>
		public static EntityField2 LimitTaxReportingFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(TaxMasterFieldIndex.LimitTaxReportingFlag);}
		}
		/// <summary>Creates a new TaxMasterEntity.LoyaltyEarnEligibleFlag field instance</summary>
		public static EntityField2 LoyaltyEarnEligibleFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(TaxMasterFieldIndex.LoyaltyEarnEligibleFlag);}
		}
		/// <summary>Creates a new TaxMasterEntity.LoyaltyRedeemEligibleFlag field instance</summary>
		public static EntityField2 LoyaltyRedeemEligibleFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(TaxMasterFieldIndex.LoyaltyRedeemEligibleFlag);}
		}
		/// <summary>Creates a new TaxMasterEntity.RoundBasis field instance</summary>
		public static EntityField2 RoundBasis
		{
			get { return (EntityField2)EntityFieldFactory.Create(TaxMasterFieldIndex.RoundBasis);}
		}
		/// <summary>Creates a new TaxMasterEntity.RoundTypeId field instance</summary>
		public static EntityField2 RoundTypeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TaxMasterFieldIndex.RoundTypeId);}
		}
		/// <summary>Creates a new TaxMasterEntity.RowVersion field instance</summary>
		public static EntityField2 RowVersion
		{
			get { return (EntityField2)EntityFieldFactory.Create(TaxMasterFieldIndex.RowVersion);}
		}
		/// <summary>Creates a new TaxMasterEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TaxMasterFieldIndex.StoreId);}
		}
		/// <summary>Creates a new TaxMasterEntity.TaxAbbr1 field instance</summary>
		public static EntityField2 TaxAbbr1
		{
			get { return (EntityField2)EntityFieldFactory.Create(TaxMasterFieldIndex.TaxAbbr1);}
		}
		/// <summary>Creates a new TaxMasterEntity.TaxAbbr2 field instance</summary>
		public static EntityField2 TaxAbbr2
		{
			get { return (EntityField2)EntityFieldFactory.Create(TaxMasterFieldIndex.TaxAbbr2);}
		}
		/// <summary>Creates a new TaxMasterEntity.TaxAmt field instance</summary>
		public static EntityField2 TaxAmt
		{
			get { return (EntityField2)EntityFieldFactory.Create(TaxMasterFieldIndex.TaxAmt);}
		}
		/// <summary>Creates a new TaxMasterEntity.TaxCatId field instance</summary>
		public static EntityField2 TaxCatId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TaxMasterFieldIndex.TaxCatId);}
		}
		/// <summary>Creates a new TaxMasterEntity.TaxId field instance</summary>
		public static EntityField2 TaxId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TaxMasterFieldIndex.TaxId);}
		}
		/// <summary>Creates a new TaxMasterEntity.TaxName field instance</summary>
		public static EntityField2 TaxName
		{
			get { return (EntityField2)EntityFieldFactory.Create(TaxMasterFieldIndex.TaxName);}
		}
		/// <summary>Creates a new TaxMasterEntity.TaxPercent field instance</summary>
		public static EntityField2 TaxPercent
		{
			get { return (EntityField2)EntityFieldFactory.Create(TaxMasterFieldIndex.TaxPercent);}
		}
		/// <summary>Creates a new TaxMasterEntity.TaxRemovableFlag field instance</summary>
		public static EntityField2 TaxRemovableFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(TaxMasterFieldIndex.TaxRemovableFlag);}
		}
		/// <summary>Creates a new TaxMasterEntity.ThreshholdAmt field instance</summary>
		public static EntityField2 ThreshholdAmt
		{
			get { return (EntityField2)EntityFieldFactory.Create(TaxMasterFieldIndex.ThreshholdAmt);}
		}
	}

	/// <summary>Field Creation Class for entity TaxRevCatJoinEntity</summary>
	public partial class TaxRevCatJoinFields
	{
		/// <summary>Creates a new TaxRevCatJoinEntity.EmpPercent field instance</summary>
		public static EntityField2 EmpPercent
		{
			get { return (EntityField2)EntityFieldFactory.Create(TaxRevCatJoinFieldIndex.EmpPercent);}
		}
		/// <summary>Creates a new TaxRevCatJoinEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TaxRevCatJoinFieldIndex.EntId);}
		}
		/// <summary>Creates a new TaxRevCatJoinEntity.PatronPercent field instance</summary>
		public static EntityField2 PatronPercent
		{
			get { return (EntityField2)EntityFieldFactory.Create(TaxRevCatJoinFieldIndex.PatronPercent);}
		}
		/// <summary>Creates a new TaxRevCatJoinEntity.RevCatId field instance</summary>
		public static EntityField2 RevCatId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TaxRevCatJoinFieldIndex.RevCatId);}
		}
		/// <summary>Creates a new TaxRevCatJoinEntity.TaxId field instance</summary>
		public static EntityField2 TaxId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TaxRevCatJoinFieldIndex.TaxId);}
		}
	}

	/// <summary>Field Creation Class for entity TenderClassMasterEntity</summary>
	public partial class TenderClassMasterFields
	{
		/// <summary>Creates a new TenderClassMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderClassMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new TenderClassMasterEntity.RowVersion field instance</summary>
		public static EntityField2 RowVersion
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderClassMasterFieldIndex.RowVersion);}
		}
		/// <summary>Creates a new TenderClassMasterEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderClassMasterFieldIndex.StoreId);}
		}
		/// <summary>Creates a new TenderClassMasterEntity.TenderClassId field instance</summary>
		public static EntityField2 TenderClassId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderClassMasterFieldIndex.TenderClassId);}
		}
		/// <summary>Creates a new TenderClassMasterEntity.TenderClassName field instance</summary>
		public static EntityField2 TenderClassName
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderClassMasterFieldIndex.TenderClassName);}
		}
	}

	/// <summary>Field Creation Class for entity TenderMasterEntity</summary>
	public partial class TenderMasterFields
	{
		/// <summary>Creates a new TenderMasterEntity.AdditionalCheckidCodeId field instance</summary>
		public static EntityField2 AdditionalCheckidCodeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.AdditionalCheckidCodeId);}
		}
		/// <summary>Creates a new TenderMasterEntity.AutoRemoveTaxFlag field instance</summary>
		public static EntityField2 AutoRemoveTaxFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.AutoRemoveTaxFlag);}
		}
		/// <summary>Creates a new TenderMasterEntity.BypassPdsFlag field instance</summary>
		public static EntityField2 BypassPdsFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.BypassPdsFlag);}
		}
		/// <summary>Creates a new TenderMasterEntity.CheckTypeId field instance</summary>
		public static EntityField2 CheckTypeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.CheckTypeId);}
		}
		/// <summary>Creates a new TenderMasterEntity.CompTenderFlag field instance</summary>
		public static EntityField2 CompTenderFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.CompTenderFlag);}
		}
		/// <summary>Creates a new TenderMasterEntity.DestinationPropertyCode field instance</summary>
		public static EntityField2 DestinationPropertyCode
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.DestinationPropertyCode);}
		}
		/// <summary>Creates a new TenderMasterEntity.DiscoupId field instance</summary>
		public static EntityField2 DiscoupId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.DiscoupId);}
		}
		/// <summary>Creates a new TenderMasterEntity.EmvCardTypeCode field instance</summary>
		public static EntityField2 EmvCardTypeCode
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.EmvCardTypeCode);}
		}
		/// <summary>Creates a new TenderMasterEntity.EnterTipPrompt field instance</summary>
		public static EntityField2 EnterTipPrompt
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.EnterTipPrompt);}
		}
		/// <summary>Creates a new TenderMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new TenderMasterEntity.FirstTenderFlag field instance</summary>
		public static EntityField2 FirstTenderFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.FirstTenderFlag);}
		}
		/// <summary>Creates a new TenderMasterEntity.FrankingCodeId field instance</summary>
		public static EntityField2 FrankingCodeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.FrankingCodeId);}
		}
		/// <summary>Creates a new TenderMasterEntity.IccDecimalPlaces field instance</summary>
		public static EntityField2 IccDecimalPlaces
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.IccDecimalPlaces);}
		}
		/// <summary>Creates a new TenderMasterEntity.IccRate field instance</summary>
		public static EntityField2 IccRate
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.IccRate);}
		}
		/// <summary>Creates a new TenderMasterEntity.LastTenderFlag field instance</summary>
		public static EntityField2 LastTenderFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.LastTenderFlag);}
		}
		/// <summary>Creates a new TenderMasterEntity.LoyaltyEarnEligibleFlag field instance</summary>
		public static EntityField2 LoyaltyEarnEligibleFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.LoyaltyEarnEligibleFlag);}
		}
		/// <summary>Creates a new TenderMasterEntity.NumReceiptsPrint field instance</summary>
		public static EntityField2 NumReceiptsPrint
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.NumReceiptsPrint);}
		}
		/// <summary>Creates a new TenderMasterEntity.OpenCashdrwrCodeId field instance</summary>
		public static EntityField2 OpenCashdrwrCodeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.OpenCashdrwrCodeId);}
		}
		/// <summary>Creates a new TenderMasterEntity.OvertenderCodeId field instance</summary>
		public static EntityField2 OvertenderCodeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.OvertenderCodeId);}
		}
		/// <summary>Creates a new TenderMasterEntity.PostAcctNo field instance</summary>
		public static EntityField2 PostAcctNo
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.PostAcctNo);}
		}
		/// <summary>Creates a new TenderMasterEntity.PostSiteId field instance</summary>
		public static EntityField2 PostSiteId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.PostSiteId);}
		}
		/// <summary>Creates a new TenderMasterEntity.PostSystem1Flag field instance</summary>
		public static EntityField2 PostSystem1Flag
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.PostSystem1Flag);}
		}
		/// <summary>Creates a new TenderMasterEntity.PostSystem2Flag field instance</summary>
		public static EntityField2 PostSystem2Flag
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.PostSystem2Flag);}
		}
		/// <summary>Creates a new TenderMasterEntity.PostSystem3Flag field instance</summary>
		public static EntityField2 PostSystem3Flag
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.PostSystem3Flag);}
		}
		/// <summary>Creates a new TenderMasterEntity.PostSystem4Flag field instance</summary>
		public static EntityField2 PostSystem4Flag
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.PostSystem4Flag);}
		}
		/// <summary>Creates a new TenderMasterEntity.PostSystem5Flag field instance</summary>
		public static EntityField2 PostSystem5Flag
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.PostSystem5Flag);}
		}
		/// <summary>Creates a new TenderMasterEntity.PostSystem6Flag field instance</summary>
		public static EntityField2 PostSystem6Flag
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.PostSystem6Flag);}
		}
		/// <summary>Creates a new TenderMasterEntity.PostSystem7Flag field instance</summary>
		public static EntityField2 PostSystem7Flag
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.PostSystem7Flag);}
		}
		/// <summary>Creates a new TenderMasterEntity.PostSystem8Flag field instance</summary>
		public static EntityField2 PostSystem8Flag
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.PostSystem8Flag);}
		}
		/// <summary>Creates a new TenderMasterEntity.PriceLevelId field instance</summary>
		public static EntityField2 PriceLevelId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.PriceLevelId);}
		}
		/// <summary>Creates a new TenderMasterEntity.PromptCvvFlag field instance</summary>
		public static EntityField2 PromptCvvFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.PromptCvvFlag);}
		}
		/// <summary>Creates a new TenderMasterEntity.PromptExtraAlphaFlag field instance</summary>
		public static EntityField2 PromptExtraAlphaFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.PromptExtraAlphaFlag);}
		}
		/// <summary>Creates a new TenderMasterEntity.PromptExtraDataFlag field instance</summary>
		public static EntityField2 PromptExtraDataFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.PromptExtraDataFlag);}
		}
		/// <summary>Creates a new TenderMasterEntity.PromptZipcodeFlag field instance</summary>
		public static EntityField2 PromptZipcodeFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.PromptZipcodeFlag);}
		}
		/// <summary>Creates a new TenderMasterEntity.RequireAmtFlag field instance</summary>
		public static EntityField2 RequireAmtFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.RequireAmtFlag);}
		}
		/// <summary>Creates a new TenderMasterEntity.RestrictedFlag field instance</summary>
		public static EntityField2 RestrictedFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.RestrictedFlag);}
		}
		/// <summary>Creates a new TenderMasterEntity.RowVersion field instance</summary>
		public static EntityField2 RowVersion
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.RowVersion);}
		}
		/// <summary>Creates a new TenderMasterEntity.SalesTippableFlag field instance</summary>
		public static EntityField2 SalesTippableFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.SalesTippableFlag);}
		}
		/// <summary>Creates a new TenderMasterEntity.SecurityId field instance</summary>
		public static EntityField2 SecurityId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.SecurityId);}
		}
		/// <summary>Creates a new TenderMasterEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.StoreId);}
		}
		/// <summary>Creates a new TenderMasterEntity.TaxCompCode field instance</summary>
		public static EntityField2 TaxCompCode
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.TaxCompCode);}
		}
		/// <summary>Creates a new TenderMasterEntity.TenderAbbr1 field instance</summary>
		public static EntityField2 TenderAbbr1
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.TenderAbbr1);}
		}
		/// <summary>Creates a new TenderMasterEntity.TenderAbbr2 field instance</summary>
		public static EntityField2 TenderAbbr2
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.TenderAbbr2);}
		}
		/// <summary>Creates a new TenderMasterEntity.TenderClassId field instance</summary>
		public static EntityField2 TenderClassId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.TenderClassId);}
		}
		/// <summary>Creates a new TenderMasterEntity.TenderId field instance</summary>
		public static EntityField2 TenderId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.TenderId);}
		}
		/// <summary>Creates a new TenderMasterEntity.TenderLimit field instance</summary>
		public static EntityField2 TenderLimit
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.TenderLimit);}
		}
		/// <summary>Creates a new TenderMasterEntity.TenderName field instance</summary>
		public static EntityField2 TenderName
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.TenderName);}
		}
		/// <summary>Creates a new TenderMasterEntity.UseArchiveFlag field instance</summary>
		public static EntityField2 UseArchiveFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.UseArchiveFlag);}
		}
		/// <summary>Creates a new TenderMasterEntity.UseSigcapFlag field instance</summary>
		public static EntityField2 UseSigcapFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.UseSigcapFlag);}
		}
		/// <summary>Creates a new TenderMasterEntity.VerificationCodeId field instance</summary>
		public static EntityField2 VerificationCodeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.VerificationCodeId);}
		}
		/// <summary>Creates a new TenderMasterEntity.VerificationManualEntryCodeId field instance</summary>
		public static EntityField2 VerificationManualEntryCodeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TenderMasterFieldIndex.VerificationManualEntryCodeId);}
		}
	}

	/// <summary>Field Creation Class for entity TermGrpChkTypeJoinEntity</summary>
	public partial class TermGrpChkTypeJoinFields
	{
		/// <summary>Creates a new TermGrpChkTypeJoinEntity.CheckPriceLevelId field instance</summary>
		public static EntityField2 CheckPriceLevelId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpChkTypeJoinFieldIndex.CheckPriceLevelId);}
		}
		/// <summary>Creates a new TermGrpChkTypeJoinEntity.CheckSecId field instance</summary>
		public static EntityField2 CheckSecId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpChkTypeJoinFieldIndex.CheckSecId);}
		}
		/// <summary>Creates a new TermGrpChkTypeJoinEntity.CheckTenderAllowed field instance</summary>
		public static EntityField2 CheckTenderAllowed
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpChkTypeJoinFieldIndex.CheckTenderAllowed);}
		}
		/// <summary>Creates a new TermGrpChkTypeJoinEntity.CheckTypeId field instance</summary>
		public static EntityField2 CheckTypeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpChkTypeJoinFieldIndex.CheckTypeId);}
		}
		/// <summary>Creates a new TermGrpChkTypeJoinEntity.CoversPrompt field instance</summary>
		public static EntityField2 CoversPrompt
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpChkTypeJoinFieldIndex.CoversPrompt);}
		}
		/// <summary>Creates a new TermGrpChkTypeJoinEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpChkTypeJoinFieldIndex.EntId);}
		}
		/// <summary>Creates a new TermGrpChkTypeJoinEntity.RoomNoPrompt field instance</summary>
		public static EntityField2 RoomNoPrompt
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpChkTypeJoinFieldIndex.RoomNoPrompt);}
		}
		/// <summary>Creates a new TermGrpChkTypeJoinEntity.ServerPrompt field instance</summary>
		public static EntityField2 ServerPrompt
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpChkTypeJoinFieldIndex.ServerPrompt);}
		}
		/// <summary>Creates a new TermGrpChkTypeJoinEntity.TablePrompt field instance</summary>
		public static EntityField2 TablePrompt
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpChkTypeJoinFieldIndex.TablePrompt);}
		}
		/// <summary>Creates a new TermGrpChkTypeJoinEntity.TaxOverride field instance</summary>
		public static EntityField2 TaxOverride
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpChkTypeJoinFieldIndex.TaxOverride);}
		}
		/// <summary>Creates a new TermGrpChkTypeJoinEntity.TermGrpId field instance</summary>
		public static EntityField2 TermGrpId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpChkTypeJoinFieldIndex.TermGrpId);}
		}
	}

	/// <summary>Field Creation Class for entity TermGrpJobcodeJoinEntity</summary>
	public partial class TermGrpJobcodeJoinFields
	{
		/// <summary>Creates a new TermGrpJobcodeJoinEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpJobcodeJoinFieldIndex.EntId);}
		}
		/// <summary>Creates a new TermGrpJobcodeJoinEntity.JobcodeId field instance</summary>
		public static EntityField2 JobcodeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpJobcodeJoinFieldIndex.JobcodeId);}
		}
		/// <summary>Creates a new TermGrpJobcodeJoinEntity.TermGrpId field instance</summary>
		public static EntityField2 TermGrpId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpJobcodeJoinFieldIndex.TermGrpId);}
		}
	}

	/// <summary>Field Creation Class for entity TermGrpMasterEntity</summary>
	public partial class TermGrpMasterFields
	{
		/// <summary>Creates a new TermGrpMasterEntity.CalendarPeriodId field instance</summary>
		public static EntityField2 CalendarPeriodId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpMasterFieldIndex.CalendarPeriodId);}
		}
		/// <summary>Creates a new TermGrpMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new TermGrpMasterEntity.ScreenScheduleId field instance</summary>
		public static EntityField2 ScreenScheduleId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpMasterFieldIndex.ScreenScheduleId);}
		}
		/// <summary>Creates a new TermGrpMasterEntity.SigcapButton1 field instance</summary>
		public static EntityField2 SigcapButton1
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpMasterFieldIndex.SigcapButton1);}
		}
		/// <summary>Creates a new TermGrpMasterEntity.SigcapButton2 field instance</summary>
		public static EntityField2 SigcapButton2
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpMasterFieldIndex.SigcapButton2);}
		}
		/// <summary>Creates a new TermGrpMasterEntity.SigcapButton3 field instance</summary>
		public static EntityField2 SigcapButton3
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpMasterFieldIndex.SigcapButton3);}
		}
		/// <summary>Creates a new TermGrpMasterEntity.SigcapButton4 field instance</summary>
		public static EntityField2 SigcapButton4
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpMasterFieldIndex.SigcapButton4);}
		}
		/// <summary>Creates a new TermGrpMasterEntity.SigcapTipPrompt field instance</summary>
		public static EntityField2 SigcapTipPrompt
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpMasterFieldIndex.SigcapTipPrompt);}
		}
		/// <summary>Creates a new TermGrpMasterEntity.SigcapTypeCode field instance</summary>
		public static EntityField2 SigcapTypeCode
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpMasterFieldIndex.SigcapTypeCode);}
		}
		/// <summary>Creates a new TermGrpMasterEntity.SiMenuId field instance</summary>
		public static EntityField2 SiMenuId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpMasterFieldIndex.SiMenuId);}
		}
		/// <summary>Creates a new TermGrpMasterEntity.StartTimeFlag field instance</summary>
		public static EntityField2 StartTimeFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpMasterFieldIndex.StartTimeFlag);}
		}
		/// <summary>Creates a new TermGrpMasterEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpMasterFieldIndex.StoreId);}
		}
		/// <summary>Creates a new TermGrpMasterEntity.TermGrpId field instance</summary>
		public static EntityField2 TermGrpId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpMasterFieldIndex.TermGrpId);}
		}
		/// <summary>Creates a new TermGrpMasterEntity.TermGrpName field instance</summary>
		public static EntityField2 TermGrpName
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpMasterFieldIndex.TermGrpName);}
		}
		/// <summary>Creates a new TermGrpMasterEntity.TerminalTextId field instance</summary>
		public static EntityField2 TerminalTextId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpMasterFieldIndex.TerminalTextId);}
		}
	}

	/// <summary>Field Creation Class for entity TermGrpMenuJoinEntity</summary>
	public partial class TermGrpMenuJoinFields
	{
		/// <summary>Creates a new TermGrpMenuJoinEntity.CheckFlag field instance</summary>
		public static EntityField2 CheckFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpMenuJoinFieldIndex.CheckFlag);}
		}
		/// <summary>Creates a new TermGrpMenuJoinEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpMenuJoinFieldIndex.EntId);}
		}
		/// <summary>Creates a new TermGrpMenuJoinEntity.MenuId field instance</summary>
		public static EntityField2 MenuId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpMenuJoinFieldIndex.MenuId);}
		}
		/// <summary>Creates a new TermGrpMenuJoinEntity.ParentMenuId field instance</summary>
		public static EntityField2 ParentMenuId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpMenuJoinFieldIndex.ParentMenuId);}
		}
		/// <summary>Creates a new TermGrpMenuJoinEntity.TermGrpId field instance</summary>
		public static EntityField2 TermGrpId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpMenuJoinFieldIndex.TermGrpId);}
		}
	}

	/// <summary>Field Creation Class for entity TermGrpMenuJoinCookerEntity</summary>
	public partial class TermGrpMenuJoinCookerFields
	{
		/// <summary>Creates a new TermGrpMenuJoinCookerEntity.CheckFlag field instance</summary>
		public static EntityField2 CheckFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpMenuJoinCookerFieldIndex.CheckFlag);}
		}
		/// <summary>Creates a new TermGrpMenuJoinCookerEntity.CookerHandle field instance</summary>
		public static EntityField2 CookerHandle
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpMenuJoinCookerFieldIndex.CookerHandle);}
		}
		/// <summary>Creates a new TermGrpMenuJoinCookerEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpMenuJoinCookerFieldIndex.EntId);}
		}
		/// <summary>Creates a new TermGrpMenuJoinCookerEntity.MenuId field instance</summary>
		public static EntityField2 MenuId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpMenuJoinCookerFieldIndex.MenuId);}
		}
		/// <summary>Creates a new TermGrpMenuJoinCookerEntity.ParentMenuId field instance</summary>
		public static EntityField2 ParentMenuId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpMenuJoinCookerFieldIndex.ParentMenuId);}
		}
		/// <summary>Creates a new TermGrpMenuJoinCookerEntity.TermGrpId field instance</summary>
		public static EntityField2 TermGrpId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpMenuJoinCookerFieldIndex.TermGrpId);}
		}
	}

	/// <summary>Field Creation Class for entity TermGrpMpJoinEntity</summary>
	public partial class TermGrpMpJoinFields
	{
		/// <summary>Creates a new TermGrpMpJoinEntity.AutoCheckTypeId field instance</summary>
		public static EntityField2 AutoCheckTypeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpMpJoinFieldIndex.AutoCheckTypeId);}
		}
		/// <summary>Creates a new TermGrpMpJoinEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpMpJoinFieldIndex.EntId);}
		}
		/// <summary>Creates a new TermGrpMpJoinEntity.MealPeriodId field instance</summary>
		public static EntityField2 MealPeriodId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpMpJoinFieldIndex.MealPeriodId);}
		}
		/// <summary>Creates a new TermGrpMpJoinEntity.MealPeriodStartTime1 field instance</summary>
		public static EntityField2 MealPeriodStartTime1
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpMpJoinFieldIndex.MealPeriodStartTime1);}
		}
		/// <summary>Creates a new TermGrpMpJoinEntity.MealPeriodStartTime2 field instance</summary>
		public static EntityField2 MealPeriodStartTime2
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpMpJoinFieldIndex.MealPeriodStartTime2);}
		}
		/// <summary>Creates a new TermGrpMpJoinEntity.MealPeriodStartTime3 field instance</summary>
		public static EntityField2 MealPeriodStartTime3
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpMpJoinFieldIndex.MealPeriodStartTime3);}
		}
		/// <summary>Creates a new TermGrpMpJoinEntity.MealPeriodStartTime4 field instance</summary>
		public static EntityField2 MealPeriodStartTime4
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpMpJoinFieldIndex.MealPeriodStartTime4);}
		}
		/// <summary>Creates a new TermGrpMpJoinEntity.MealPeriodStartTime5 field instance</summary>
		public static EntityField2 MealPeriodStartTime5
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpMpJoinFieldIndex.MealPeriodStartTime5);}
		}
		/// <summary>Creates a new TermGrpMpJoinEntity.MealPeriodStartTime6 field instance</summary>
		public static EntityField2 MealPeriodStartTime6
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpMpJoinFieldIndex.MealPeriodStartTime6);}
		}
		/// <summary>Creates a new TermGrpMpJoinEntity.MealPeriodStartTime7 field instance</summary>
		public static EntityField2 MealPeriodStartTime7
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpMpJoinFieldIndex.MealPeriodStartTime7);}
		}
		/// <summary>Creates a new TermGrpMpJoinEntity.PriceLevelId field instance</summary>
		public static EntityField2 PriceLevelId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpMpJoinFieldIndex.PriceLevelId);}
		}
		/// <summary>Creates a new TermGrpMpJoinEntity.TermGrpId field instance</summary>
		public static EntityField2 TermGrpId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpMpJoinFieldIndex.TermGrpId);}
		}
	}

	/// <summary>Field Creation Class for entity TermGrpMpmenuJoinEntity</summary>
	public partial class TermGrpMpmenuJoinFields
	{
		/// <summary>Creates a new TermGrpMpmenuJoinEntity.CalendarDaySeqId field instance</summary>
		public static EntityField2 CalendarDaySeqId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpMpmenuJoinFieldIndex.CalendarDaySeqId);}
		}
		/// <summary>Creates a new TermGrpMpmenuJoinEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpMpmenuJoinFieldIndex.EntId);}
		}
		/// <summary>Creates a new TermGrpMpmenuJoinEntity.MealPeriodId field instance</summary>
		public static EntityField2 MealPeriodId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpMpmenuJoinFieldIndex.MealPeriodId);}
		}
		/// <summary>Creates a new TermGrpMpmenuJoinEntity.MenuId field instance</summary>
		public static EntityField2 MenuId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpMpmenuJoinFieldIndex.MenuId);}
		}
		/// <summary>Creates a new TermGrpMpmenuJoinEntity.MenuSeq field instance</summary>
		public static EntityField2 MenuSeq
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpMpmenuJoinFieldIndex.MenuSeq);}
		}
		/// <summary>Creates a new TermGrpMpmenuJoinEntity.TermGrpId field instance</summary>
		public static EntityField2 TermGrpId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpMpmenuJoinFieldIndex.TermGrpId);}
		}
	}

	/// <summary>Field Creation Class for entity TermGrpTndrJoinEntity</summary>
	public partial class TermGrpTndrJoinFields
	{
		/// <summary>Creates a new TermGrpTndrJoinEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpTndrJoinFieldIndex.EntId);}
		}
		/// <summary>Creates a new TermGrpTndrJoinEntity.SeqNo field instance</summary>
		public static EntityField2 SeqNo
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpTndrJoinFieldIndex.SeqNo);}
		}
		/// <summary>Creates a new TermGrpTndrJoinEntity.TenderId field instance</summary>
		public static EntityField2 TenderId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpTndrJoinFieldIndex.TenderId);}
		}
		/// <summary>Creates a new TermGrpTndrJoinEntity.TermGrpId field instance</summary>
		public static EntityField2 TermGrpId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpTndrJoinFieldIndex.TermGrpId);}
		}
	}

	/// <summary>Field Creation Class for entity TermGrpTransitionJoinEntity</summary>
	public partial class TermGrpTransitionJoinFields
	{
		/// <summary>Creates a new TermGrpTransitionJoinEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpTransitionJoinFieldIndex.EntId);}
		}
		/// <summary>Creates a new TermGrpTransitionJoinEntity.ScreenId field instance</summary>
		public static EntityField2 ScreenId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpTransitionJoinFieldIndex.ScreenId);}
		}
		/// <summary>Creates a new TermGrpTransitionJoinEntity.TermGrpId field instance</summary>
		public static EntityField2 TermGrpId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpTransitionJoinFieldIndex.TermGrpId);}
		}
		/// <summary>Creates a new TermGrpTransitionJoinEntity.TransitionId field instance</summary>
		public static EntityField2 TransitionId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermGrpTransitionJoinFieldIndex.TransitionId);}
		}
	}

	/// <summary>Field Creation Class for entity TerminalMasterEntity</summary>
	public partial class TerminalMasterFields
	{
		/// <summary>Creates a new TerminalMasterEntity.AltBargunTermId field instance</summary>
		public static EntityField2 AltBargunTermId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TerminalMasterFieldIndex.AltBargunTermId);}
		}
		/// <summary>Creates a new TerminalMasterEntity.AltRcptTermId field instance</summary>
		public static EntityField2 AltRcptTermId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TerminalMasterFieldIndex.AltRcptTermId);}
		}
		/// <summary>Creates a new TerminalMasterEntity.BargunBumpOverrideFlag field instance</summary>
		public static EntityField2 BargunBumpOverrideFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(TerminalMasterFieldIndex.BargunBumpOverrideFlag);}
		}
		/// <summary>Creates a new TerminalMasterEntity.BargunPrintDrinksOnBumpFlag field instance</summary>
		public static EntityField2 BargunPrintDrinksOnBumpFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(TerminalMasterFieldIndex.BargunPrintDrinksOnBumpFlag);}
		}
		/// <summary>Creates a new TerminalMasterEntity.BargunTermFlag field instance</summary>
		public static EntityField2 BargunTermFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(TerminalMasterFieldIndex.BargunTermFlag);}
		}
		/// <summary>Creates a new TerminalMasterEntity.CurrentVersion field instance</summary>
		public static EntityField2 CurrentVersion
		{
			get { return (EntityField2)EntityFieldFactory.Create(TerminalMasterFieldIndex.CurrentVersion);}
		}
		/// <summary>Creates a new TerminalMasterEntity.DefaultTableLayoutId field instance</summary>
		public static EntityField2 DefaultTableLayoutId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TerminalMasterFieldIndex.DefaultTableLayoutId);}
		}
		/// <summary>Creates a new TerminalMasterEntity.FirstTableNo field instance</summary>
		public static EntityField2 FirstTableNo
		{
			get { return (EntityField2)EntityFieldFactory.Create(TerminalMasterFieldIndex.FirstTableNo);}
		}
		/// <summary>Creates a new TerminalMasterEntity.FolFileLoadFlag field instance</summary>
		public static EntityField2 FolFileLoadFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(TerminalMasterFieldIndex.FolFileLoadFlag);}
		}
		/// <summary>Creates a new TerminalMasterEntity.GaFileLoadFlag field instance</summary>
		public static EntityField2 GaFileLoadFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(TerminalMasterFieldIndex.GaFileLoadFlag);}
		}
		/// <summary>Creates a new TerminalMasterEntity.IpAddress field instance</summary>
		public static EntityField2 IpAddress
		{
			get { return (EntityField2)EntityFieldFactory.Create(TerminalMasterFieldIndex.IpAddress);}
		}
		/// <summary>Creates a new TerminalMasterEntity.LastPingUtcDateTime field instance</summary>
		public static EntityField2 LastPingUtcDateTime
		{
			get { return (EntityField2)EntityFieldFactory.Create(TerminalMasterFieldIndex.LastPingUtcDateTime);}
		}
		/// <summary>Creates a new TerminalMasterEntity.NumTables field instance</summary>
		public static EntityField2 NumTables
		{
			get { return (EntityField2)EntityFieldFactory.Create(TerminalMasterFieldIndex.NumTables);}
		}
		/// <summary>Creates a new TerminalMasterEntity.PedValue field instance</summary>
		public static EntityField2 PedValue
		{
			get { return (EntityField2)EntityFieldFactory.Create(TerminalMasterFieldIndex.PedValue);}
		}
		/// <summary>Creates a new TerminalMasterEntity.PrimaryProfitCenterId field instance</summary>
		public static EntityField2 PrimaryProfitCenterId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TerminalMasterFieldIndex.PrimaryProfitCenterId);}
		}
		/// <summary>Creates a new TerminalMasterEntity.ProfileId field instance</summary>
		public static EntityField2 ProfileId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TerminalMasterFieldIndex.ProfileId);}
		}
		/// <summary>Creates a new TerminalMasterEntity.ReceiptPrinterType field instance</summary>
		public static EntityField2 ReceiptPrinterType
		{
			get { return (EntityField2)EntityFieldFactory.Create(TerminalMasterFieldIndex.ReceiptPrinterType);}
		}
		/// <summary>Creates a new TerminalMasterEntity.RmsFileLoadFlag field instance</summary>
		public static EntityField2 RmsFileLoadFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(TerminalMasterFieldIndex.RmsFileLoadFlag);}
		}
		/// <summary>Creates a new TerminalMasterEntity.RowVersion field instance</summary>
		public static EntityField2 RowVersion
		{
			get { return (EntityField2)EntityFieldFactory.Create(TerminalMasterFieldIndex.RowVersion);}
		}
		/// <summary>Creates a new TerminalMasterEntity.StaticReceiptPrinterId field instance</summary>
		public static EntityField2 StaticReceiptPrinterId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TerminalMasterFieldIndex.StaticReceiptPrinterId);}
		}
		/// <summary>Creates a new TerminalMasterEntity.TermActiveFlag field instance</summary>
		public static EntityField2 TermActiveFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(TerminalMasterFieldIndex.TermActiveFlag);}
		}
		/// <summary>Creates a new TerminalMasterEntity.TermGrpId field instance</summary>
		public static EntityField2 TermGrpId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TerminalMasterFieldIndex.TermGrpId);}
		}
		/// <summary>Creates a new TerminalMasterEntity.TermId field instance</summary>
		public static EntityField2 TermId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TerminalMasterFieldIndex.TermId);}
		}
		/// <summary>Creates a new TerminalMasterEntity.TermName field instance</summary>
		public static EntityField2 TermName
		{
			get { return (EntityField2)EntityFieldFactory.Create(TerminalMasterFieldIndex.TermName);}
		}
		/// <summary>Creates a new TerminalMasterEntity.TermOptionGrpId field instance</summary>
		public static EntityField2 TermOptionGrpId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TerminalMasterFieldIndex.TermOptionGrpId);}
		}
		/// <summary>Creates a new TerminalMasterEntity.TermPrinterGrpId field instance</summary>
		public static EntityField2 TermPrinterGrpId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TerminalMasterFieldIndex.TermPrinterGrpId);}
		}
		/// <summary>Creates a new TerminalMasterEntity.TermReceiptInfo field instance</summary>
		public static EntityField2 TermReceiptInfo
		{
			get { return (EntityField2)EntityFieldFactory.Create(TerminalMasterFieldIndex.TermReceiptInfo);}
		}
		/// <summary>Creates a new TerminalMasterEntity.TermServiceGrpId field instance</summary>
		public static EntityField2 TermServiceGrpId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TerminalMasterFieldIndex.TermServiceGrpId);}
		}
		/// <summary>Creates a new TerminalMasterEntity.VirtualTermFlag field instance</summary>
		public static EntityField2 VirtualTermFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(TerminalMasterFieldIndex.VirtualTermFlag);}
		}
	}

	/// <summary>Field Creation Class for entity TerminalProfitCtrJoinEntity</summary>
	public partial class TerminalProfitCtrJoinFields
	{
		/// <summary>Creates a new TerminalProfitCtrJoinEntity.PrimaryProfitCenterFlag field instance</summary>
		public static EntityField2 PrimaryProfitCenterFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(TerminalProfitCtrJoinFieldIndex.PrimaryProfitCenterFlag);}
		}
		/// <summary>Creates a new TerminalProfitCtrJoinEntity.ProfitCenterId field instance</summary>
		public static EntityField2 ProfitCenterId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TerminalProfitCtrJoinFieldIndex.ProfitCenterId);}
		}
		/// <summary>Creates a new TerminalProfitCtrJoinEntity.ProfitCenterSeq field instance</summary>
		public static EntityField2 ProfitCenterSeq
		{
			get { return (EntityField2)EntityFieldFactory.Create(TerminalProfitCtrJoinFieldIndex.ProfitCenterSeq);}
		}
		/// <summary>Creates a new TerminalProfitCtrJoinEntity.TermId field instance</summary>
		public static EntityField2 TermId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TerminalProfitCtrJoinFieldIndex.TermId);}
		}
	}

	/// <summary>Field Creation Class for entity TerminalTextGuestInfoJoinEntity</summary>
	public partial class TerminalTextGuestInfoJoinFields
	{
		/// <summary>Creates a new TerminalTextGuestInfoJoinEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TerminalTextGuestInfoJoinFieldIndex.EntId);}
		}
		/// <summary>Creates a new TerminalTextGuestInfoJoinEntity.SequenceId field instance</summary>
		public static EntityField2 SequenceId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TerminalTextGuestInfoJoinFieldIndex.SequenceId);}
		}
		/// <summary>Creates a new TerminalTextGuestInfoJoinEntity.TerminalIntlId field instance</summary>
		public static EntityField2 TerminalIntlId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TerminalTextGuestInfoJoinFieldIndex.TerminalIntlId);}
		}
		/// <summary>Creates a new TerminalTextGuestInfoJoinEntity.TerminalIntlUserText field instance</summary>
		public static EntityField2 TerminalIntlUserText
		{
			get { return (EntityField2)EntityFieldFactory.Create(TerminalTextGuestInfoJoinFieldIndex.TerminalIntlUserText);}
		}
		/// <summary>Creates a new TerminalTextGuestInfoJoinEntity.TerminalTextId field instance</summary>
		public static EntityField2 TerminalTextId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TerminalTextGuestInfoJoinFieldIndex.TerminalTextId);}
		}
	}

	/// <summary>Field Creation Class for entity TerminalTextMasterEntity</summary>
	public partial class TerminalTextMasterFields
	{
		/// <summary>Creates a new TerminalTextMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TerminalTextMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new TerminalTextMasterEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TerminalTextMasterFieldIndex.StoreId);}
		}
		/// <summary>Creates a new TerminalTextMasterEntity.TerminalTextId field instance</summary>
		public static EntityField2 TerminalTextId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TerminalTextMasterFieldIndex.TerminalTextId);}
		}
		/// <summary>Creates a new TerminalTextMasterEntity.TerminalTextName field instance</summary>
		public static EntityField2 TerminalTextName
		{
			get { return (EntityField2)EntityFieldFactory.Create(TerminalTextMasterFieldIndex.TerminalTextName);}
		}
	}

	/// <summary>Field Creation Class for entity TermOptionGrpMasterEntity</summary>
	public partial class TermOptionGrpMasterFields
	{
		/// <summary>Creates a new TermOptionGrpMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermOptionGrpMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new TermOptionGrpMasterEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermOptionGrpMasterFieldIndex.StoreId);}
		}
		/// <summary>Creates a new TermOptionGrpMasterEntity.TermOptionGrpId field instance</summary>
		public static EntityField2 TermOptionGrpId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermOptionGrpMasterFieldIndex.TermOptionGrpId);}
		}
		/// <summary>Creates a new TermOptionGrpMasterEntity.TermOptionGrpName field instance</summary>
		public static EntityField2 TermOptionGrpName
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermOptionGrpMasterFieldIndex.TermOptionGrpName);}
		}
	}

	/// <summary>Field Creation Class for entity TermOptionGrpOptionJoinEntity</summary>
	public partial class TermOptionGrpOptionJoinFields
	{
		/// <summary>Creates a new TermOptionGrpOptionJoinEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermOptionGrpOptionJoinFieldIndex.EntId);}
		}
		/// <summary>Creates a new TermOptionGrpOptionJoinEntity.OptionFlag field instance</summary>
		public static EntityField2 OptionFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermOptionGrpOptionJoinFieldIndex.OptionFlag);}
		}
		/// <summary>Creates a new TermOptionGrpOptionJoinEntity.OptionId field instance</summary>
		public static EntityField2 OptionId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermOptionGrpOptionJoinFieldIndex.OptionId);}
		}
		/// <summary>Creates a new TermOptionGrpOptionJoinEntity.TermOptionGrpId field instance</summary>
		public static EntityField2 TermOptionGrpId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermOptionGrpOptionJoinFieldIndex.TermOptionGrpId);}
		}
	}

	/// <summary>Field Creation Class for entity TermPrinterGrpKpprntJoinEntity</summary>
	public partial class TermPrinterGrpKpprntJoinFields
	{
		/// <summary>Creates a new TermPrinterGrpKpprntJoinEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermPrinterGrpKpprntJoinFieldIndex.EntId);}
		}
		/// <summary>Creates a new TermPrinterGrpKpprntJoinEntity.KpAlternateDeviceId field instance</summary>
		public static EntityField2 KpAlternateDeviceId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermPrinterGrpKpprntJoinFieldIndex.KpAlternateDeviceId);}
		}
		/// <summary>Creates a new TermPrinterGrpKpprntJoinEntity.KpDeviceId field instance</summary>
		public static EntityField2 KpDeviceId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermPrinterGrpKpprntJoinFieldIndex.KpDeviceId);}
		}
		/// <summary>Creates a new TermPrinterGrpKpprntJoinEntity.KpPrinterId field instance</summary>
		public static EntityField2 KpPrinterId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermPrinterGrpKpprntJoinFieldIndex.KpPrinterId);}
		}
		/// <summary>Creates a new TermPrinterGrpKpprntJoinEntity.TermPrinterGrpId field instance</summary>
		public static EntityField2 TermPrinterGrpId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermPrinterGrpKpprntJoinFieldIndex.TermPrinterGrpId);}
		}
	}

	/// <summary>Field Creation Class for entity TermPrinterGrpMasterEntity</summary>
	public partial class TermPrinterGrpMasterFields
	{
		/// <summary>Creates a new TermPrinterGrpMasterEntity.AlternatePrintingCode field instance</summary>
		public static EntityField2 AlternatePrintingCode
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermPrinterGrpMasterFieldIndex.AlternatePrintingCode);}
		}
		/// <summary>Creates a new TermPrinterGrpMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermPrinterGrpMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new TermPrinterGrpMasterEntity.StatusCodeId field instance</summary>
		public static EntityField2 StatusCodeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermPrinterGrpMasterFieldIndex.StatusCodeId);}
		}
		/// <summary>Creates a new TermPrinterGrpMasterEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermPrinterGrpMasterFieldIndex.StoreId);}
		}
		/// <summary>Creates a new TermPrinterGrpMasterEntity.TermPrinterGrpAbbr1 field instance</summary>
		public static EntityField2 TermPrinterGrpAbbr1
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermPrinterGrpMasterFieldIndex.TermPrinterGrpAbbr1);}
		}
		/// <summary>Creates a new TermPrinterGrpMasterEntity.TermPrinterGrpAbbr2 field instance</summary>
		public static EntityField2 TermPrinterGrpAbbr2
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermPrinterGrpMasterFieldIndex.TermPrinterGrpAbbr2);}
		}
		/// <summary>Creates a new TermPrinterGrpMasterEntity.TermPrinterGrpId field instance</summary>
		public static EntityField2 TermPrinterGrpId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermPrinterGrpMasterFieldIndex.TermPrinterGrpId);}
		}
		/// <summary>Creates a new TermPrinterGrpMasterEntity.TermPrinterGrpName field instance</summary>
		public static EntityField2 TermPrinterGrpName
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermPrinterGrpMasterFieldIndex.TermPrinterGrpName);}
		}
		/// <summary>Creates a new TermPrinterGrpMasterEntity.UseAlternateFlag field instance</summary>
		public static EntityField2 UseAlternateFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermPrinterGrpMasterFieldIndex.UseAlternateFlag);}
		}
	}

	/// <summary>Field Creation Class for entity TermPrinterGrpRouteJoinEntity</summary>
	public partial class TermPrinterGrpRouteJoinFields
	{
		/// <summary>Creates a new TermPrinterGrpRouteJoinEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermPrinterGrpRouteJoinFieldIndex.EntId);}
		}
		/// <summary>Creates a new TermPrinterGrpRouteJoinEntity.GroupRouteGenId field instance</summary>
		public static EntityField2 GroupRouteGenId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermPrinterGrpRouteJoinFieldIndex.GroupRouteGenId);}
		}
		/// <summary>Creates a new TermPrinterGrpRouteJoinEntity.KpRouteId field instance</summary>
		public static EntityField2 KpRouteId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermPrinterGrpRouteJoinFieldIndex.KpRouteId);}
		}
		/// <summary>Creates a new TermPrinterGrpRouteJoinEntity.StartMinuteFri field instance</summary>
		public static EntityField2 StartMinuteFri
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermPrinterGrpRouteJoinFieldIndex.StartMinuteFri);}
		}
		/// <summary>Creates a new TermPrinterGrpRouteJoinEntity.StartMinuteMon field instance</summary>
		public static EntityField2 StartMinuteMon
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermPrinterGrpRouteJoinFieldIndex.StartMinuteMon);}
		}
		/// <summary>Creates a new TermPrinterGrpRouteJoinEntity.StartMinuteSat field instance</summary>
		public static EntityField2 StartMinuteSat
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermPrinterGrpRouteJoinFieldIndex.StartMinuteSat);}
		}
		/// <summary>Creates a new TermPrinterGrpRouteJoinEntity.StartMinuteSun field instance</summary>
		public static EntityField2 StartMinuteSun
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermPrinterGrpRouteJoinFieldIndex.StartMinuteSun);}
		}
		/// <summary>Creates a new TermPrinterGrpRouteJoinEntity.StartMinuteThu field instance</summary>
		public static EntityField2 StartMinuteThu
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermPrinterGrpRouteJoinFieldIndex.StartMinuteThu);}
		}
		/// <summary>Creates a new TermPrinterGrpRouteJoinEntity.StartMinuteTue field instance</summary>
		public static EntityField2 StartMinuteTue
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermPrinterGrpRouteJoinFieldIndex.StartMinuteTue);}
		}
		/// <summary>Creates a new TermPrinterGrpRouteJoinEntity.StartMinuteWed field instance</summary>
		public static EntityField2 StartMinuteWed
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermPrinterGrpRouteJoinFieldIndex.StartMinuteWed);}
		}
		/// <summary>Creates a new TermPrinterGrpRouteJoinEntity.TermPrinterGrpId field instance</summary>
		public static EntityField2 TermPrinterGrpId
		{
			get { return (EntityField2)EntityFieldFactory.Create(TermPrinterGrpRouteJoinFieldIndex.TermPrinterGrpId);}
		}
	}

	/// <summary>Field Creation Class for entity ThemeColorJoinEntity</summary>
	public partial class ThemeColorJoinFields
	{
		/// <summary>Creates a new ThemeColorJoinEntity.ColorCodeId field instance</summary>
		public static EntityField2 ColorCodeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ThemeColorJoinFieldIndex.ColorCodeId);}
		}
		/// <summary>Creates a new ThemeColorJoinEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ThemeColorJoinFieldIndex.EntId);}
		}
		/// <summary>Creates a new ThemeColorJoinEntity.RgbColor field instance</summary>
		public static EntityField2 RgbColor
		{
			get { return (EntityField2)EntityFieldFactory.Create(ThemeColorJoinFieldIndex.RgbColor);}
		}
		/// <summary>Creates a new ThemeColorJoinEntity.ThemeId field instance</summary>
		public static EntityField2 ThemeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ThemeColorJoinFieldIndex.ThemeId);}
		}
	}

	/// <summary>Field Creation Class for entity ThemeFontJoinEntity</summary>
	public partial class ThemeFontJoinFields
	{
		/// <summary>Creates a new ThemeFontJoinEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ThemeFontJoinFieldIndex.EntId);}
		}
		/// <summary>Creates a new ThemeFontJoinEntity.FontId field instance</summary>
		public static EntityField2 FontId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ThemeFontJoinFieldIndex.FontId);}
		}
		/// <summary>Creates a new ThemeFontJoinEntity.ThemeId field instance</summary>
		public static EntityField2 ThemeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ThemeFontJoinFieldIndex.ThemeId);}
		}
	}

	/// <summary>Field Creation Class for entity ThemeMasterEntity</summary>
	public partial class ThemeMasterFields
	{
		/// <summary>Creates a new ThemeMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ThemeMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new ThemeMasterEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ThemeMasterFieldIndex.StoreId);}
		}
		/// <summary>Creates a new ThemeMasterEntity.ThemeId field instance</summary>
		public static EntityField2 ThemeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ThemeMasterFieldIndex.ThemeId);}
		}
		/// <summary>Creates a new ThemeMasterEntity.ThemeName field instance</summary>
		public static EntityField2 ThemeName
		{
			get { return (EntityField2)EntityFieldFactory.Create(ThemeMasterFieldIndex.ThemeName);}
		}
	}

	/// <summary>Field Creation Class for entity ThemeObjectJoinEntity</summary>
	public partial class ThemeObjectJoinFields
	{
		/// <summary>Creates a new ThemeObjectJoinEntity.ButtonBackcolor field instance</summary>
		public static EntityField2 ButtonBackcolor
		{
			get { return (EntityField2)EntityFieldFactory.Create(ThemeObjectJoinFieldIndex.ButtonBackcolor);}
		}
		/// <summary>Creates a new ThemeObjectJoinEntity.ButtonColumn field instance</summary>
		public static EntityField2 ButtonColumn
		{
			get { return (EntityField2)EntityFieldFactory.Create(ThemeObjectJoinFieldIndex.ButtonColumn);}
		}
		/// <summary>Creates a new ThemeObjectJoinEntity.ButtonFontId field instance</summary>
		public static EntityField2 ButtonFontId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ThemeObjectJoinFieldIndex.ButtonFontId);}
		}
		/// <summary>Creates a new ThemeObjectJoinEntity.ButtonForecolor field instance</summary>
		public static EntityField2 ButtonForecolor
		{
			get { return (EntityField2)EntityFieldFactory.Create(ThemeObjectJoinFieldIndex.ButtonForecolor);}
		}
		/// <summary>Creates a new ThemeObjectJoinEntity.ButtonHeight field instance</summary>
		public static EntityField2 ButtonHeight
		{
			get { return (EntityField2)EntityFieldFactory.Create(ThemeObjectJoinFieldIndex.ButtonHeight);}
		}
		/// <summary>Creates a new ThemeObjectJoinEntity.ButtonImageId field instance</summary>
		public static EntityField2 ButtonImageId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ThemeObjectJoinFieldIndex.ButtonImageId);}
		}
		/// <summary>Creates a new ThemeObjectJoinEntity.ButtonNextMenuId field instance</summary>
		public static EntityField2 ButtonNextMenuId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ThemeObjectJoinFieldIndex.ButtonNextMenuId);}
		}
		/// <summary>Creates a new ThemeObjectJoinEntity.ButtonRow field instance</summary>
		public static EntityField2 ButtonRow
		{
			get { return (EntityField2)EntityFieldFactory.Create(ThemeObjectJoinFieldIndex.ButtonRow);}
		}
		/// <summary>Creates a new ThemeObjectJoinEntity.ButtonShapeId field instance</summary>
		public static EntityField2 ButtonShapeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ThemeObjectJoinFieldIndex.ButtonShapeId);}
		}
		/// <summary>Creates a new ThemeObjectJoinEntity.ButtonWidth field instance</summary>
		public static EntityField2 ButtonWidth
		{
			get { return (EntityField2)EntityFieldFactory.Create(ThemeObjectJoinFieldIndex.ButtonWidth);}
		}
		/// <summary>Creates a new ThemeObjectJoinEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ThemeObjectJoinFieldIndex.EntId);}
		}
		/// <summary>Creates a new ThemeObjectJoinEntity.ErrorCodeId field instance</summary>
		public static EntityField2 ErrorCodeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ThemeObjectJoinFieldIndex.ErrorCodeId);}
		}
		/// <summary>Creates a new ThemeObjectJoinEntity.ObjectTypeId field instance</summary>
		public static EntityField2 ObjectTypeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ThemeObjectJoinFieldIndex.ObjectTypeId);}
		}
		/// <summary>Creates a new ThemeObjectJoinEntity.ThemeId field instance</summary>
		public static EntityField2 ThemeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ThemeObjectJoinFieldIndex.ThemeId);}
		}
	}

	/// <summary>Field Creation Class for entity UdefDateRangeInstanceEntity</summary>
	public partial class UdefDateRangeInstanceFields
	{
		/// <summary>Creates a new UdefDateRangeInstanceEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(UdefDateRangeInstanceFieldIndex.EntId);}
		}
		/// <summary>Creates a new UdefDateRangeInstanceEntity.PeriodTypeId field instance</summary>
		public static EntityField2 PeriodTypeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(UdefDateRangeInstanceFieldIndex.PeriodTypeId);}
		}
		/// <summary>Creates a new UdefDateRangeInstanceEntity.RangeEndDate field instance</summary>
		public static EntityField2 RangeEndDate
		{
			get { return (EntityField2)EntityFieldFactory.Create(UdefDateRangeInstanceFieldIndex.RangeEndDate);}
		}
		/// <summary>Creates a new UdefDateRangeInstanceEntity.RangeEndTime field instance</summary>
		public static EntityField2 RangeEndTime
		{
			get { return (EntityField2)EntityFieldFactory.Create(UdefDateRangeInstanceFieldIndex.RangeEndTime);}
		}
		/// <summary>Creates a new UdefDateRangeInstanceEntity.RangeInstanceDesc field instance</summary>
		public static EntityField2 RangeInstanceDesc
		{
			get { return (EntityField2)EntityFieldFactory.Create(UdefDateRangeInstanceFieldIndex.RangeInstanceDesc);}
		}
		/// <summary>Creates a new UdefDateRangeInstanceEntity.RangeInstanceId field instance</summary>
		public static EntityField2 RangeInstanceId
		{
			get { return (EntityField2)EntityFieldFactory.Create(UdefDateRangeInstanceFieldIndex.RangeInstanceId);}
		}
		/// <summary>Creates a new UdefDateRangeInstanceEntity.RangeStartDate field instance</summary>
		public static EntityField2 RangeStartDate
		{
			get { return (EntityField2)EntityFieldFactory.Create(UdefDateRangeInstanceFieldIndex.RangeStartDate);}
		}
		/// <summary>Creates a new UdefDateRangeInstanceEntity.RangeStartTime field instance</summary>
		public static EntityField2 RangeStartTime
		{
			get { return (EntityField2)EntityFieldFactory.Create(UdefDateRangeInstanceFieldIndex.RangeStartTime);}
		}
		/// <summary>Creates a new UdefDateRangeInstanceEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(UdefDateRangeInstanceFieldIndex.StoreId);}
		}
	}

	/// <summary>Field Creation Class for entity UdefPeriodTypeMasterEntity</summary>
	public partial class UdefPeriodTypeMasterFields
	{
		/// <summary>Creates a new UdefPeriodTypeMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(UdefPeriodTypeMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new UdefPeriodTypeMasterEntity.PeriodTypeDesc field instance</summary>
		public static EntityField2 PeriodTypeDesc
		{
			get { return (EntityField2)EntityFieldFactory.Create(UdefPeriodTypeMasterFieldIndex.PeriodTypeDesc);}
		}
		/// <summary>Creates a new UdefPeriodTypeMasterEntity.PeriodTypeId field instance</summary>
		public static EntityField2 PeriodTypeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(UdefPeriodTypeMasterFieldIndex.PeriodTypeId);}
		}
		/// <summary>Creates a new UdefPeriodTypeMasterEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(UdefPeriodTypeMasterFieldIndex.StoreId);}
		}
	}

	/// <summary>Field Creation Class for entity VoidReasonMasterEntity</summary>
	public partial class VoidReasonMasterFields
	{
		/// <summary>Creates a new VoidReasonMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(VoidReasonMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new VoidReasonMasterEntity.KpFlag field instance</summary>
		public static EntityField2 KpFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(VoidReasonMasterFieldIndex.KpFlag);}
		}
		/// <summary>Creates a new VoidReasonMasterEntity.PmixFlag field instance</summary>
		public static EntityField2 PmixFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(VoidReasonMasterFieldIndex.PmixFlag);}
		}
		/// <summary>Creates a new VoidReasonMasterEntity.RefundCheckFlag field instance</summary>
		public static EntityField2 RefundCheckFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(VoidReasonMasterFieldIndex.RefundCheckFlag);}
		}
		/// <summary>Creates a new VoidReasonMasterEntity.RowVersion field instance</summary>
		public static EntityField2 RowVersion
		{
			get { return (EntityField2)EntityFieldFactory.Create(VoidReasonMasterFieldIndex.RowVersion);}
		}
		/// <summary>Creates a new VoidReasonMasterEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(VoidReasonMasterFieldIndex.StoreId);}
		}
		/// <summary>Creates a new VoidReasonMasterEntity.VoidItemFlag field instance</summary>
		public static EntityField2 VoidItemFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(VoidReasonMasterFieldIndex.VoidItemFlag);}
		}
		/// <summary>Creates a new VoidReasonMasterEntity.VoidOpenChkFlag field instance</summary>
		public static EntityField2 VoidOpenChkFlag
		{
			get { return (EntityField2)EntityFieldFactory.Create(VoidReasonMasterFieldIndex.VoidOpenChkFlag);}
		}
		/// <summary>Creates a new VoidReasonMasterEntity.VoidReasonAbbr1 field instance</summary>
		public static EntityField2 VoidReasonAbbr1
		{
			get { return (EntityField2)EntityFieldFactory.Create(VoidReasonMasterFieldIndex.VoidReasonAbbr1);}
		}
		/// <summary>Creates a new VoidReasonMasterEntity.VoidReasonAbbr2 field instance</summary>
		public static EntityField2 VoidReasonAbbr2
		{
			get { return (EntityField2)EntityFieldFactory.Create(VoidReasonMasterFieldIndex.VoidReasonAbbr2);}
		}
		/// <summary>Creates a new VoidReasonMasterEntity.VoidReasonId field instance</summary>
		public static EntityField2 VoidReasonId
		{
			get { return (EntityField2)EntityFieldFactory.Create(VoidReasonMasterFieldIndex.VoidReasonId);}
		}
		/// <summary>Creates a new VoidReasonMasterEntity.VoidReasonName field instance</summary>
		public static EntityField2 VoidReasonName
		{
			get { return (EntityField2)EntityFieldFactory.Create(VoidReasonMasterFieldIndex.VoidReasonName);}
		}
		/// <summary>Creates a new VoidReasonMasterEntity.VoidReasonSecId field instance</summary>
		public static EntityField2 VoidReasonSecId
		{
			get { return (EntityField2)EntityFieldFactory.Create(VoidReasonMasterFieldIndex.VoidReasonSecId);}
		}
	}

	/// <summary>Field Creation Class for entity ZooMasterEntity</summary>
	public partial class ZooMasterFields
	{
		/// <summary>Creates a new ZooMasterEntity.ConfigEventXml field instance</summary>
		public static EntityField2 ConfigEventXml
		{
			get { return (EntityField2)EntityFieldFactory.Create(ZooMasterFieldIndex.ConfigEventXml);}
		}
		/// <summary>Creates a new ZooMasterEntity.CustomerId field instance</summary>
		public static EntityField2 CustomerId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ZooMasterFieldIndex.CustomerId);}
		}
		/// <summary>Creates a new ZooMasterEntity.DivisionId field instance</summary>
		public static EntityField2 DivisionId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ZooMasterFieldIndex.DivisionId);}
		}
		/// <summary>Creates a new ZooMasterEntity.EntId field instance</summary>
		public static EntityField2 EntId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ZooMasterFieldIndex.EntId);}
		}
		/// <summary>Creates a new ZooMasterEntity.MasterObjectDisplayName field instance</summary>
		public static EntityField2 MasterObjectDisplayName
		{
			get { return (EntityField2)EntityFieldFactory.Create(ZooMasterFieldIndex.MasterObjectDisplayName);}
		}
		/// <summary>Creates a new ZooMasterEntity.MasterObjectId field instance</summary>
		public static EntityField2 MasterObjectId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ZooMasterFieldIndex.MasterObjectId);}
		}
		/// <summary>Creates a new ZooMasterEntity.ObjectDisplayName field instance</summary>
		public static EntityField2 ObjectDisplayName
		{
			get { return (EntityField2)EntityFieldFactory.Create(ZooMasterFieldIndex.ObjectDisplayName);}
		}
		/// <summary>Creates a new ZooMasterEntity.ObjectId field instance</summary>
		public static EntityField2 ObjectId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ZooMasterFieldIndex.ObjectId);}
		}
		/// <summary>Creates a new ZooMasterEntity.ObjectTypeId field instance</summary>
		public static EntityField2 ObjectTypeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ZooMasterFieldIndex.ObjectTypeId);}
		}
		/// <summary>Creates a new ZooMasterEntity.StoreId field instance</summary>
		public static EntityField2 StoreId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ZooMasterFieldIndex.StoreId);}
		}
		/// <summary>Creates a new ZooMasterEntity.TrackAction field instance</summary>
		public static EntityField2 TrackAction
		{
			get { return (EntityField2)EntityFieldFactory.Create(ZooMasterFieldIndex.TrackAction);}
		}
		/// <summary>Creates a new ZooMasterEntity.TrackDttime field instance</summary>
		public static EntityField2 TrackDttime
		{
			get { return (EntityField2)EntityFieldFactory.Create(ZooMasterFieldIndex.TrackDttime);}
		}
		/// <summary>Creates a new ZooMasterEntity.TrackGroupId field instance</summary>
		public static EntityField2 TrackGroupId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ZooMasterFieldIndex.TrackGroupId);}
		}
		/// <summary>Creates a new ZooMasterEntity.TrackSourceId field instance</summary>
		public static EntityField2 TrackSourceId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ZooMasterFieldIndex.TrackSourceId);}
		}
		/// <summary>Creates a new ZooMasterEntity.TrackUsername field instance</summary>
		public static EntityField2 TrackUsername
		{
			get { return (EntityField2)EntityFieldFactory.Create(ZooMasterFieldIndex.TrackUsername);}
		}
		/// <summary>Creates a new ZooMasterEntity.VirtualDatabaseName field instance</summary>
		public static EntityField2 VirtualDatabaseName
		{
			get { return (EntityField2)EntityFieldFactory.Create(ZooMasterFieldIndex.VirtualDatabaseName);}
		}
		/// <summary>Creates a new ZooMasterEntity.VirtualTableName field instance</summary>
		public static EntityField2 VirtualTableName
		{
			get { return (EntityField2)EntityFieldFactory.Create(ZooMasterFieldIndex.VirtualTableName);}
		}
		/// <summary>Creates a new ZooMasterEntity.ZooId field instance</summary>
		public static EntityField2 ZooId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ZooMasterFieldIndex.ZooId);}
		}
	}
	

}