﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace InfoGenesis.HelperClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	
	
	/// <summary>Singleton implementation of the FieldInfoProvider. This class is the singleton wrapper through which the actual instance is retrieved.</summary>
	/// <remarks>It uses a single instance of an internal class. The access isn't marked with locks as the FieldInfoProviderBase class is threadsafe.</remarks>
	internal static class FieldInfoProviderSingleton
	{
		#region Class Member Declarations
		private static readonly IFieldInfoProvider _providerInstance = new FieldInfoProviderCore();
		#endregion

		/// <summary>Dummy static constructor to make sure threadsafe initialization is performed.</summary>
		static FieldInfoProviderSingleton()
		{
		}

		/// <summary>Gets the singleton instance of the FieldInfoProviderCore</summary>
		/// <returns>Instance of the FieldInfoProvider.</returns>
		public static IFieldInfoProvider GetInstance()
		{
			return _providerInstance;
		}
	}

	/// <summary>Actual implementation of the FieldInfoProvider. Used by singleton wrapper.</summary>
	internal class FieldInfoProviderCore : FieldInfoProviderBase
	{
		/// <summary>Initializes a new instance of the <see cref="FieldInfoProviderCore"/> class.</summary>
		internal FieldInfoProviderCore()
		{
			Init();
		}

		/// <summary>Method which initializes the internal datastores.</summary>
		private void Init()
		{
			this.InitClass( (123 + 0));
			InitBonusCodeMasterEntityInfos();
			InitButtonAbbrJoinEntityInfos();
			InitCardTypeMasterEntityInfos();
			InitCardTypeRangeJoinEntityInfos();
			InitCfgVersionEntityInfos();
			InitCheckTypeMasterEntityInfos();
			InitChefMasterEntityInfos();
			InitChkTypeGratJoinEntityInfos();
			InitChkTypeGratRevJoinEntityInfos();
			InitChkTypeGratTaxJoinEntityInfos();
			InitChkTypeTaxGrpJoinEntityInfos();
			InitChkTypeTaxGrpTaxJoinEntityInfos();
			InitChkTypeVatGratJoinEntityInfos();
			InitChkTypeVatRevJoinEntityInfos();
			InitChoiceGroupMasterEntityInfos();
			InitChoiceGrpModJoinEntityInfos();
			InitComboItemJoinEntityInfos();
			InitComboMasterEntityInfos();
			InitConfigurationIdMasterEntityInfos();
			InitDeviceMasterEntityInfos();
			InitDiscoupMasterEntityInfos();
			InitEmpGroupMasterEntityInfos();
			InitEmpGrpEmpJoinEntityInfos();
			InitEmpJobcodeJoinEntityInfos();
			InitEmpMasterEntityInfos();
			InitFuncBttnTextJoinEntityInfos();
			InitGa4680ExportParmMasterEntityInfos();
			InitGa4680ImportParmMasterEntityInfos();
			InitGeneralLedgerMapMasterEntityInfos();
			InitGiftCardMasterEntityInfos();
			InitGratuityCatMasterEntityInfos();
			InitGratuityMasterEntityInfos();
			InitIdMasterEntityInfos();
			InitImageLibraryMasterEntityInfos();
			InitIntlDescriptorTextJoinEntityInfos();
			InitIpServerAttribMasterEntityInfos();
			InitJobcodeFuncBttnJoinEntityInfos();
			InitJobcodeFunctJoinEntityInfos();
			InitJobcodeMasterEntityInfos();
			InitKdsCategoryMasterEntityInfos();
			InitKdsVideoMasterEntityInfos();
			InitKpOptionGrpCopyJoinEntityInfos();
			InitKpOptionGrpFormatJoinEntityInfos();
			InitKpOptionGrpMasterEntityInfos();
			InitKpPrinterMasterEntityInfos();
			InitKpRouteMasterEntityInfos();
			InitKpRoutePrinterJoinEntityInfos();
			InitMachineOptionsMasterEntityInfos();
			InitMealPeriodMasterEntityInfos();
			InitMembershipMasterEntityInfos();
			InitMembershipProfitCenterJoinEntityInfos();
			InitMenuBttnObjJoinEntityInfos();
			InitMenuItemGroupMasterEntityInfos();
			InitMenuItemMasterEntityInfos();
			InitMenuMasterEntityInfos();
			InitMenuMigrpJoinEntityInfos();
			InitMiChoiceGrpJoinEntityInfos();
			InitMiKpprinterJoinEntityInfos();
			InitMiPriceJoinEntityInfos();
			InitMiSkuJoinEntityInfos();
			InitModifierKpprinterJoinEntityInfos();
			InitModifierMasterEntityInfos();
			InitModifierPriceJoinEntityInfos();
			InitOptionsMasterEntityInfos();
			InitPriceLevelMasterEntityInfos();
			InitProcessMasterEntityInfos();
			InitProductClassMasterEntityInfos();
			InitProfitCenterDayPartJoinEntityInfos();
			InitProfitCenterGroupJoinEntityInfos();
			InitProfitCenterGroupMasterEntityInfos();
			InitProfitCenterMasterEntityInfos();
			InitProfitCenterReceiptPrinterJoinEntityInfos();
			InitProfitCenterTableJoinEntityInfos();
			InitQuickTenderMasterEntityInfos();
			InitReceiptPrinterMasterEntityInfos();
			InitReportCatMasterEntityInfos();
			InitRevenueCatMasterEntityInfos();
			InitRevenueClassMasterEntityInfos();
			InitRowLockMasterEntityInfos();
			InitScreenTemplateMasterEntityInfos();
			InitScreenTemplateMigrpJoinEntityInfos();
			InitSecurityLevelMasterEntityInfos();
			InitSelectionGroupItemJoinEntityInfos();
			InitSelectionGroupMasterEntityInfos();
			InitServiceManagerProxyMasterEntityInfos();
			InitSmuCommandMasterEntityInfos();
			InitSmuStatusMasterEntityInfos();
			InitSpecialInstrMasterEntityInfos();
			InitSystemConfigurationMasterEntityInfos();
			InitTableLayoutMasterEntityInfos();
			InitTaxCatMasterEntityInfos();
			InitTaxGroupMasterEntityInfos();
			InitTaxGrpTaxJoinEntityInfos();
			InitTaxMasterEntityInfos();
			InitTaxRevCatJoinEntityInfos();
			InitTenderClassMasterEntityInfos();
			InitTenderMasterEntityInfos();
			InitTermGrpChkTypeJoinEntityInfos();
			InitTermGrpJobcodeJoinEntityInfos();
			InitTermGrpMasterEntityInfos();
			InitTermGrpMenuJoinEntityInfos();
			InitTermGrpMenuJoinCookerEntityInfos();
			InitTermGrpMpJoinEntityInfos();
			InitTermGrpMpmenuJoinEntityInfos();
			InitTermGrpTndrJoinEntityInfos();
			InitTermGrpTransitionJoinEntityInfos();
			InitTerminalMasterEntityInfos();
			InitTerminalProfitCtrJoinEntityInfos();
			InitTerminalTextGuestInfoJoinEntityInfos();
			InitTerminalTextMasterEntityInfos();
			InitTermOptionGrpMasterEntityInfos();
			InitTermOptionGrpOptionJoinEntityInfos();
			InitTermPrinterGrpKpprntJoinEntityInfos();
			InitTermPrinterGrpMasterEntityInfos();
			InitTermPrinterGrpRouteJoinEntityInfos();
			InitThemeColorJoinEntityInfos();
			InitThemeFontJoinEntityInfos();
			InitThemeMasterEntityInfos();
			InitThemeObjectJoinEntityInfos();
			InitUdefDateRangeInstanceEntityInfos();
			InitUdefPeriodTypeMasterEntityInfos();
			InitVoidReasonMasterEntityInfos();
			InitZooMasterEntityInfos();

			this.ConstructElementFieldStructures(InheritanceInfoProviderSingleton.GetInstance());
		}

		/// <summary>Inits BonusCodeMasterEntity's FieldInfo objects</summary>
		private void InitBonusCodeMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(BonusCodeMasterFieldIndex), "BonusCodeMasterEntity");
			this.AddElementFieldInfo("BonusCodeMasterEntity", "BonusCodeEndDate", typeof(System.String), false, false, false, false,  (int)BonusCodeMasterFieldIndex.BonusCodeEndDate, 8, 0, 0);
			this.AddElementFieldInfo("BonusCodeMasterEntity", "BonusCodeGenId", typeof(System.Int32), true, false, true, false,  (int)BonusCodeMasterFieldIndex.BonusCodeGenId, 0, 0, 10);
			this.AddElementFieldInfo("BonusCodeMasterEntity", "BonusCodeName", typeof(System.String), false, false, false, false,  (int)BonusCodeMasterFieldIndex.BonusCodeName, 30, 0, 0);
			this.AddElementFieldInfo("BonusCodeMasterEntity", "BonusCodeNo", typeof(System.String), false, false, false, false,  (int)BonusCodeMasterFieldIndex.BonusCodeNo, 10, 0, 0);
			this.AddElementFieldInfo("BonusCodeMasterEntity", "BonusCodeStartDate", typeof(System.String), false, false, false, false,  (int)BonusCodeMasterFieldIndex.BonusCodeStartDate, 8, 0, 0);
			this.AddElementFieldInfo("BonusCodeMasterEntity", "EntId", typeof(System.Int32), false, false, false, false,  (int)BonusCodeMasterFieldIndex.EntId, 0, 0, 10);
		}
		/// <summary>Inits ButtonAbbrJoinEntity's FieldInfo objects</summary>
		private void InitButtonAbbrJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ButtonAbbrJoinFieldIndex), "ButtonAbbrJoinEntity");
			this.AddElementFieldInfo("ButtonAbbrJoinEntity", "ButtonId", typeof(System.Int32), true, false, false, false,  (int)ButtonAbbrJoinFieldIndex.ButtonId, 0, 0, 10);
			this.AddElementFieldInfo("ButtonAbbrJoinEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)ButtonAbbrJoinFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("ButtonAbbrJoinEntity", "UserButtonAbbr1", typeof(System.String), false, false, false, true,  (int)ButtonAbbrJoinFieldIndex.UserButtonAbbr1, 7, 0, 0);
			this.AddElementFieldInfo("ButtonAbbrJoinEntity", "UserButtonAbbr2", typeof(System.String), false, false, false, true,  (int)ButtonAbbrJoinFieldIndex.UserButtonAbbr2, 7, 0, 0);
		}
		/// <summary>Inits CardTypeMasterEntity's FieldInfo objects</summary>
		private void InitCardTypeMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(CardTypeMasterFieldIndex), "CardTypeMasterEntity");
			this.AddElementFieldInfo("CardTypeMasterEntity", "CardTypeId", typeof(System.Int32), true, false, false, false,  (int)CardTypeMasterFieldIndex.CardTypeId, 0, 0, 10);
			this.AddElementFieldInfo("CardTypeMasterEntity", "CardTypeName", typeof(System.String), false, false, false, true,  (int)CardTypeMasterFieldIndex.CardTypeName, 50, 0, 0);
			this.AddElementFieldInfo("CardTypeMasterEntity", "CardTypeNumLength", typeof(Nullable<System.Int32>), false, false, false, true,  (int)CardTypeMasterFieldIndex.CardTypeNumLength, 0, 0, 10);
			this.AddElementFieldInfo("CardTypeMasterEntity", "CardTypePrefixLength", typeof(Nullable<System.Int32>), false, false, false, true,  (int)CardTypeMasterFieldIndex.CardTypePrefixLength, 0, 0, 10);
			this.AddElementFieldInfo("CardTypeMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)CardTypeMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("CardTypeMasterEntity", "StoreId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)CardTypeMasterFieldIndex.StoreId, 0, 0, 10);
			this.AddElementFieldInfo("CardTypeMasterEntity", "TenderId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)CardTypeMasterFieldIndex.TenderId, 0, 0, 10);
		}
		/// <summary>Inits CardTypeRangeJoinEntity's FieldInfo objects</summary>
		private void InitCardTypeRangeJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(CardTypeRangeJoinFieldIndex), "CardTypeRangeJoinEntity");
			this.AddElementFieldInfo("CardTypeRangeJoinEntity", "CardTypeId", typeof(System.Int32), true, false, false, false,  (int)CardTypeRangeJoinFieldIndex.CardTypeId, 0, 0, 10);
			this.AddElementFieldInfo("CardTypeRangeJoinEntity", "CardTypePrefixHigh", typeof(Nullable<System.Int32>), false, false, false, true,  (int)CardTypeRangeJoinFieldIndex.CardTypePrefixHigh, 0, 0, 10);
			this.AddElementFieldInfo("CardTypeRangeJoinEntity", "CardTypePrefixLow", typeof(Nullable<System.Int32>), false, false, false, true,  (int)CardTypeRangeJoinFieldIndex.CardTypePrefixLow, 0, 0, 10);
			this.AddElementFieldInfo("CardTypeRangeJoinEntity", "CardTypeRangeId", typeof(System.Int32), true, false, true, false,  (int)CardTypeRangeJoinFieldIndex.CardTypeRangeId, 0, 0, 10);
			this.AddElementFieldInfo("CardTypeRangeJoinEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)CardTypeRangeJoinFieldIndex.EntId, 0, 0, 10);
		}
		/// <summary>Inits CfgVersionEntity's FieldInfo objects</summary>
		private void InitCfgVersionEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(CfgVersionFieldIndex), "CfgVersionEntity");
			this.AddElementFieldInfo("CfgVersionEntity", "BuildVersion", typeof(Nullable<System.Int16>), false, false, false, true,  (int)CfgVersionFieldIndex.BuildVersion, 0, 0, 5);
			this.AddElementFieldInfo("CfgVersionEntity", "DbName", typeof(System.String), true, false, false, false,  (int)CfgVersionFieldIndex.DbName, 50, 0, 0);
			this.AddElementFieldInfo("CfgVersionEntity", "MajorVersion", typeof(System.Int16), true, false, false, false,  (int)CfgVersionFieldIndex.MajorVersion, 0, 0, 5);
			this.AddElementFieldInfo("CfgVersionEntity", "MinorVersion", typeof(System.Int16), true, false, false, false,  (int)CfgVersionFieldIndex.MinorVersion, 0, 0, 5);
			this.AddElementFieldInfo("CfgVersionEntity", "PatchVersion", typeof(System.Int16), true, false, false, false,  (int)CfgVersionFieldIndex.PatchVersion, 0, 0, 5);
		}
		/// <summary>Inits CheckTypeMasterEntity's FieldInfo objects</summary>
		private void InitCheckTypeMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(CheckTypeMasterFieldIndex), "CheckTypeMasterEntity");
			this.AddElementFieldInfo("CheckTypeMasterEntity", "CheckTypeAbbr1", typeof(System.String), false, false, false, true,  (int)CheckTypeMasterFieldIndex.CheckTypeAbbr1, 7, 0, 0);
			this.AddElementFieldInfo("CheckTypeMasterEntity", "CheckTypeAbbr2", typeof(System.String), false, false, false, true,  (int)CheckTypeMasterFieldIndex.CheckTypeAbbr2, 7, 0, 0);
			this.AddElementFieldInfo("CheckTypeMasterEntity", "CheckTypeId", typeof(System.Int32), true, false, false, false,  (int)CheckTypeMasterFieldIndex.CheckTypeId, 0, 0, 10);
			this.AddElementFieldInfo("CheckTypeMasterEntity", "CheckTypeName", typeof(System.String), false, false, false, true,  (int)CheckTypeMasterFieldIndex.CheckTypeName, 16, 0, 0);
			this.AddElementFieldInfo("CheckTypeMasterEntity", "DefaultPriceLevelId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)CheckTypeMasterFieldIndex.DefaultPriceLevelId, 0, 0, 10);
			this.AddElementFieldInfo("CheckTypeMasterEntity", "DefaultSecId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)CheckTypeMasterFieldIndex.DefaultSecId, 0, 0, 10);
			this.AddElementFieldInfo("CheckTypeMasterEntity", "DiscountId", typeof(System.Int32), false, false, false, false,  (int)CheckTypeMasterFieldIndex.DiscountId, 0, 0, 10);
			this.AddElementFieldInfo("CheckTypeMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)CheckTypeMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("CheckTypeMasterEntity", "RoundBasis", typeof(Nullable<System.Int32>), false, false, false, true,  (int)CheckTypeMasterFieldIndex.RoundBasis, 0, 0, 10);
			this.AddElementFieldInfo("CheckTypeMasterEntity", "RoundTypeId", typeof(Nullable<System.Int16>), false, false, false, true,  (int)CheckTypeMasterFieldIndex.RoundTypeId, 0, 0, 5);
			this.AddElementFieldInfo("CheckTypeMasterEntity", "RowVersion", typeof(System.Byte[]), false, false, true, false,  (int)CheckTypeMasterFieldIndex.RowVersion, 2147483647, 0, 0);
			this.AddElementFieldInfo("CheckTypeMasterEntity", "SalesTippableFlag", typeof(System.Boolean), false, false, false, false,  (int)CheckTypeMasterFieldIndex.SalesTippableFlag, 0, 0, 0);
			this.AddElementFieldInfo("CheckTypeMasterEntity", "StoreId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)CheckTypeMasterFieldIndex.StoreId, 0, 0, 10);
		}
		/// <summary>Inits ChefMasterEntity's FieldInfo objects</summary>
		private void InitChefMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ChefMasterFieldIndex), "ChefMasterEntity");
			this.AddElementFieldInfo("ChefMasterEntity", "CookedFlag", typeof(System.Boolean), false, false, false, false,  (int)ChefMasterFieldIndex.CookedFlag, 0, 0, 0);
			this.AddElementFieldInfo("ChefMasterEntity", "CookerHandle", typeof(Nullable<System.Decimal>), false, false, false, true,  (int)ChefMasterFieldIndex.CookerHandle, 0, 10, 20);
			this.AddElementFieldInfo("ChefMasterEntity", "CookerKeyId", typeof(System.Int64), true, false, true, false,  (int)ChefMasterFieldIndex.CookerKeyId, 0, 0, 19);
			this.AddElementFieldInfo("ChefMasterEntity", "CustomerId", typeof(System.Int32), false, false, false, false,  (int)ChefMasterFieldIndex.CustomerId, 0, 0, 10);
			this.AddElementFieldInfo("ChefMasterEntity", "DivisionId", typeof(System.Int32), false, false, false, false,  (int)ChefMasterFieldIndex.DivisionId, 0, 0, 10);
			this.AddElementFieldInfo("ChefMasterEntity", "EntId", typeof(System.Int32), false, false, false, false,  (int)ChefMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("ChefMasterEntity", "LsrName", typeof(System.String), false, false, false, true,  (int)ChefMasterFieldIndex.LsrName, 20, 0, 0);
			this.AddElementFieldInfo("ChefMasterEntity", "StoreId", typeof(System.Int32), false, false, false, false,  (int)ChefMasterFieldIndex.StoreId, 0, 0, 10);
		}
		/// <summary>Inits ChkTypeGratJoinEntity's FieldInfo objects</summary>
		private void InitChkTypeGratJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ChkTypeGratJoinFieldIndex), "ChkTypeGratJoinEntity");
			this.AddElementFieldInfo("ChkTypeGratJoinEntity", "CheckTypeId", typeof(System.Int32), true, false, false, false,  (int)ChkTypeGratJoinFieldIndex.CheckTypeId, 0, 0, 10);
			this.AddElementFieldInfo("ChkTypeGratJoinEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)ChkTypeGratJoinFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("ChkTypeGratJoinEntity", "GratId", typeof(System.Int32), true, false, false, false,  (int)ChkTypeGratJoinFieldIndex.GratId, 0, 0, 10);
			this.AddElementFieldInfo("ChkTypeGratJoinEntity", "RevCatCodeId", typeof(Nullable<System.Int16>), false, false, false, true,  (int)ChkTypeGratJoinFieldIndex.RevCatCodeId, 0, 0, 5);
			this.AddElementFieldInfo("ChkTypeGratJoinEntity", "TaxCodeId", typeof(Nullable<System.Int16>), false, false, false, true,  (int)ChkTypeGratJoinFieldIndex.TaxCodeId, 0, 0, 5);
		}
		/// <summary>Inits ChkTypeGratRevJoinEntity's FieldInfo objects</summary>
		private void InitChkTypeGratRevJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ChkTypeGratRevJoinFieldIndex), "ChkTypeGratRevJoinEntity");
			this.AddElementFieldInfo("ChkTypeGratRevJoinEntity", "CheckTypeId", typeof(System.Int32), true, false, false, false,  (int)ChkTypeGratRevJoinFieldIndex.CheckTypeId, 0, 0, 10);
			this.AddElementFieldInfo("ChkTypeGratRevJoinEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)ChkTypeGratRevJoinFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("ChkTypeGratRevJoinEntity", "GratId", typeof(System.Int32), true, false, false, false,  (int)ChkTypeGratRevJoinFieldIndex.GratId, 0, 0, 10);
			this.AddElementFieldInfo("ChkTypeGratRevJoinEntity", "RevCatId", typeof(System.Int32), true, false, false, false,  (int)ChkTypeGratRevJoinFieldIndex.RevCatId, 0, 0, 10);
		}
		/// <summary>Inits ChkTypeGratTaxJoinEntity's FieldInfo objects</summary>
		private void InitChkTypeGratTaxJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ChkTypeGratTaxJoinFieldIndex), "ChkTypeGratTaxJoinEntity");
			this.AddElementFieldInfo("ChkTypeGratTaxJoinEntity", "CheckTypeId", typeof(System.Int32), true, false, false, false,  (int)ChkTypeGratTaxJoinFieldIndex.CheckTypeId, 0, 0, 10);
			this.AddElementFieldInfo("ChkTypeGratTaxJoinEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)ChkTypeGratTaxJoinFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("ChkTypeGratTaxJoinEntity", "GratId", typeof(System.Int32), true, false, false, false,  (int)ChkTypeGratTaxJoinFieldIndex.GratId, 0, 0, 10);
			this.AddElementFieldInfo("ChkTypeGratTaxJoinEntity", "TaxId", typeof(System.Int32), true, false, false, false,  (int)ChkTypeGratTaxJoinFieldIndex.TaxId, 0, 0, 10);
		}
		/// <summary>Inits ChkTypeTaxGrpJoinEntity's FieldInfo objects</summary>
		private void InitChkTypeTaxGrpJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ChkTypeTaxGrpJoinFieldIndex), "ChkTypeTaxGrpJoinEntity");
			this.AddElementFieldInfo("ChkTypeTaxGrpJoinEntity", "CheckTypeId", typeof(System.Int32), true, false, false, false,  (int)ChkTypeTaxGrpJoinFieldIndex.CheckTypeId, 0, 0, 10);
			this.AddElementFieldInfo("ChkTypeTaxGrpJoinEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)ChkTypeTaxGrpJoinFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("ChkTypeTaxGrpJoinEntity", "TaxCodeId", typeof(Nullable<System.Int16>), false, false, false, true,  (int)ChkTypeTaxGrpJoinFieldIndex.TaxCodeId, 0, 0, 5);
			this.AddElementFieldInfo("ChkTypeTaxGrpJoinEntity", "TaxGrpId", typeof(System.Int32), true, false, false, false,  (int)ChkTypeTaxGrpJoinFieldIndex.TaxGrpId, 0, 0, 10);
		}
		/// <summary>Inits ChkTypeTaxGrpTaxJoinEntity's FieldInfo objects</summary>
		private void InitChkTypeTaxGrpTaxJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ChkTypeTaxGrpTaxJoinFieldIndex), "ChkTypeTaxGrpTaxJoinEntity");
			this.AddElementFieldInfo("ChkTypeTaxGrpTaxJoinEntity", "CheckTypeId", typeof(System.Int32), true, false, false, false,  (int)ChkTypeTaxGrpTaxJoinFieldIndex.CheckTypeId, 0, 0, 10);
			this.AddElementFieldInfo("ChkTypeTaxGrpTaxJoinEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)ChkTypeTaxGrpTaxJoinFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("ChkTypeTaxGrpTaxJoinEntity", "TaxGrpId", typeof(System.Int32), true, false, false, false,  (int)ChkTypeTaxGrpTaxJoinFieldIndex.TaxGrpId, 0, 0, 10);
			this.AddElementFieldInfo("ChkTypeTaxGrpTaxJoinEntity", "TaxId", typeof(System.Int32), true, false, false, false,  (int)ChkTypeTaxGrpTaxJoinFieldIndex.TaxId, 0, 0, 10);
		}
		/// <summary>Inits ChkTypeVatGratJoinEntity's FieldInfo objects</summary>
		private void InitChkTypeVatGratJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ChkTypeVatGratJoinFieldIndex), "ChkTypeVatGratJoinEntity");
			this.AddElementFieldInfo("ChkTypeVatGratJoinEntity", "CheckTypeId", typeof(System.Int32), true, false, false, false,  (int)ChkTypeVatGratJoinFieldIndex.CheckTypeId, 0, 0, 10);
			this.AddElementFieldInfo("ChkTypeVatGratJoinEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)ChkTypeVatGratJoinFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("ChkTypeVatGratJoinEntity", "GratId", typeof(System.Int32), true, false, false, false,  (int)ChkTypeVatGratJoinFieldIndex.GratId, 0, 0, 10);
			this.AddElementFieldInfo("ChkTypeVatGratJoinEntity", "TaxId", typeof(System.Int32), false, false, false, false,  (int)ChkTypeVatGratJoinFieldIndex.TaxId, 0, 0, 10);
		}
		/// <summary>Inits ChkTypeVatRevJoinEntity's FieldInfo objects</summary>
		private void InitChkTypeVatRevJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ChkTypeVatRevJoinFieldIndex), "ChkTypeVatRevJoinEntity");
			this.AddElementFieldInfo("ChkTypeVatRevJoinEntity", "CheckTypeId", typeof(System.Int32), true, false, false, false,  (int)ChkTypeVatRevJoinFieldIndex.CheckTypeId, 0, 0, 10);
			this.AddElementFieldInfo("ChkTypeVatRevJoinEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)ChkTypeVatRevJoinFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("ChkTypeVatRevJoinEntity", "RevCatId", typeof(System.Int32), true, false, false, false,  (int)ChkTypeVatRevJoinFieldIndex.RevCatId, 0, 0, 10);
			this.AddElementFieldInfo("ChkTypeVatRevJoinEntity", "TaxId", typeof(System.Int32), false, false, false, false,  (int)ChkTypeVatRevJoinFieldIndex.TaxId, 0, 0, 10);
		}
		/// <summary>Inits ChoiceGroupMasterEntity's FieldInfo objects</summary>
		private void InitChoiceGroupMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ChoiceGroupMasterFieldIndex), "ChoiceGroupMasterEntity");
			this.AddElementFieldInfo("ChoiceGroupMasterEntity", "ChoiceGroupAbbr1", typeof(System.String), false, false, false, true,  (int)ChoiceGroupMasterFieldIndex.ChoiceGroupAbbr1, 7, 0, 0);
			this.AddElementFieldInfo("ChoiceGroupMasterEntity", "ChoiceGroupAbbr2", typeof(System.String), false, false, false, true,  (int)ChoiceGroupMasterFieldIndex.ChoiceGroupAbbr2, 7, 0, 0);
			this.AddElementFieldInfo("ChoiceGroupMasterEntity", "ChoiceGroupId", typeof(System.Int32), true, false, false, false,  (int)ChoiceGroupMasterFieldIndex.ChoiceGroupId, 0, 0, 10);
			this.AddElementFieldInfo("ChoiceGroupMasterEntity", "ChoiceGroupLabel", typeof(System.String), false, false, false, true,  (int)ChoiceGroupMasterFieldIndex.ChoiceGroupLabel, 20, 0, 0);
			this.AddElementFieldInfo("ChoiceGroupMasterEntity", "ChoiceGroupName", typeof(System.String), false, false, false, true,  (int)ChoiceGroupMasterFieldIndex.ChoiceGroupName, 16, 0, 0);
			this.AddElementFieldInfo("ChoiceGroupMasterEntity", "DataControlGroupId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ChoiceGroupMasterFieldIndex.DataControlGroupId, 0, 0, 10);
			this.AddElementFieldInfo("ChoiceGroupMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)ChoiceGroupMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("ChoiceGroupMasterEntity", "MaxNumMods", typeof(Nullable<System.Int16>), false, false, false, true,  (int)ChoiceGroupMasterFieldIndex.MaxNumMods, 0, 0, 5);
			this.AddElementFieldInfo("ChoiceGroupMasterEntity", "MinNumMods", typeof(Nullable<System.Int16>), false, false, false, true,  (int)ChoiceGroupMasterFieldIndex.MinNumMods, 0, 0, 5);
			this.AddElementFieldInfo("ChoiceGroupMasterEntity", "NumFreeMods", typeof(Nullable<System.Int16>), false, false, false, true,  (int)ChoiceGroupMasterFieldIndex.NumFreeMods, 0, 0, 5);
			this.AddElementFieldInfo("ChoiceGroupMasterEntity", "StoreId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ChoiceGroupMasterFieldIndex.StoreId, 0, 0, 10);
		}
		/// <summary>Inits ChoiceGrpModJoinEntity's FieldInfo objects</summary>
		private void InitChoiceGrpModJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ChoiceGrpModJoinFieldIndex), "ChoiceGrpModJoinEntity");
			this.AddElementFieldInfo("ChoiceGrpModJoinEntity", "ChoiceGroupId", typeof(System.Int32), true, false, false, false,  (int)ChoiceGrpModJoinFieldIndex.ChoiceGroupId, 0, 0, 10);
			this.AddElementFieldInfo("ChoiceGrpModJoinEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)ChoiceGrpModJoinFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("ChoiceGrpModJoinEntity", "ModifierChoicegrpSeq", typeof(Nullable<System.Int16>), false, false, false, true,  (int)ChoiceGrpModJoinFieldIndex.ModifierChoicegrpSeq, 0, 0, 5);
			this.AddElementFieldInfo("ChoiceGrpModJoinEntity", "ModifierId", typeof(System.Int32), true, false, false, false,  (int)ChoiceGrpModJoinFieldIndex.ModifierId, 0, 0, 10);
		}
		/// <summary>Inits ComboItemJoinEntity's FieldInfo objects</summary>
		private void InitComboItemJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ComboItemJoinFieldIndex), "ComboItemJoinEntity");
			this.AddElementFieldInfo("ComboItemJoinEntity", "ComboId", typeof(System.Int32), true, false, false, false,  (int)ComboItemJoinFieldIndex.ComboId, 0, 0, 10);
			this.AddElementFieldInfo("ComboItemJoinEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)ComboItemJoinFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("ComboItemJoinEntity", "ItemPrice", typeof(System.Decimal), false, false, false, false,  (int)ComboItemJoinFieldIndex.ItemPrice, 0, 4, 19);
			this.AddElementFieldInfo("ComboItemJoinEntity", "ItemQty", typeof(System.Int16), false, false, false, false,  (int)ComboItemJoinFieldIndex.ItemQty, 0, 0, 5);
			this.AddElementFieldInfo("ComboItemJoinEntity", "KeyId", typeof(System.Int32), true, false, true, false,  (int)ComboItemJoinFieldIndex.KeyId, 0, 0, 10);
			this.AddElementFieldInfo("ComboItemJoinEntity", "ObjectId", typeof(System.Int32), true, false, false, false,  (int)ComboItemJoinFieldIndex.ObjectId, 0, 0, 10);
			this.AddElementFieldInfo("ComboItemJoinEntity", "ObjectTypeId", typeof(System.Byte), true, false, false, false,  (int)ComboItemJoinFieldIndex.ObjectTypeId, 0, 0, 3);
			this.AddElementFieldInfo("ComboItemJoinEntity", "PrintReceiptFlag", typeof(System.Boolean), false, false, false, false,  (int)ComboItemJoinFieldIndex.PrintReceiptFlag, 0, 0, 0);
			this.AddElementFieldInfo("ComboItemJoinEntity", "SequenceId", typeof(System.Int16), false, false, false, false,  (int)ComboItemJoinFieldIndex.SequenceId, 0, 0, 5);
		}
		/// <summary>Inits ComboMasterEntity's FieldInfo objects</summary>
		private void InitComboMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ComboMasterFieldIndex), "ComboMasterEntity");
			this.AddElementFieldInfo("ComboMasterEntity", "ComboAbbr1", typeof(System.String), false, false, false, false,  (int)ComboMasterFieldIndex.ComboAbbr1, 20, 0, 0);
			this.AddElementFieldInfo("ComboMasterEntity", "ComboAbbr2", typeof(System.String), false, false, false, true,  (int)ComboMasterFieldIndex.ComboAbbr2, 20, 0, 0);
			this.AddElementFieldInfo("ComboMasterEntity", "ComboId", typeof(System.Int32), true, false, false, false,  (int)ComboMasterFieldIndex.ComboId, 0, 0, 10);
			this.AddElementFieldInfo("ComboMasterEntity", "ComboKpLabel", typeof(System.String), false, false, false, false,  (int)ComboMasterFieldIndex.ComboKpLabel, 16, 0, 0);
			this.AddElementFieldInfo("ComboMasterEntity", "ComboMenuItemId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ComboMasterFieldIndex.ComboMenuItemId, 0, 0, 10);
			this.AddElementFieldInfo("ComboMasterEntity", "ComboName", typeof(System.String), false, false, false, false,  (int)ComboMasterFieldIndex.ComboName, 50, 0, 0);
			this.AddElementFieldInfo("ComboMasterEntity", "ComboReceiptLabel", typeof(System.String), false, false, false, false,  (int)ComboMasterFieldIndex.ComboReceiptLabel, 16, 0, 0);
			this.AddElementFieldInfo("ComboMasterEntity", "DivisionId", typeof(System.Int32), false, false, false, false,  (int)ComboMasterFieldIndex.DivisionId, 0, 0, 10);
			this.AddElementFieldInfo("ComboMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)ComboMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("ComboMasterEntity", "KdsVideoLabel", typeof(System.String), false, false, false, true,  (int)ComboMasterFieldIndex.KdsVideoLabel, 20, 0, 0);
			this.AddElementFieldInfo("ComboMasterEntity", "RowVersion", typeof(System.Byte[]), false, false, true, false,  (int)ComboMasterFieldIndex.RowVersion, 2147483647, 0, 0);
		}
		/// <summary>Inits ConfigurationIdMasterEntity's FieldInfo objects</summary>
		private void InitConfigurationIdMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ConfigurationIdMasterFieldIndex), "ConfigurationIdMasterEntity");
			this.AddElementFieldInfo("ConfigurationIdMasterEntity", "CustomerId", typeof(System.Int32), true, false, false, false,  (int)ConfigurationIdMasterFieldIndex.CustomerId, 0, 0, 10);
			this.AddElementFieldInfo("ConfigurationIdMasterEntity", "DivId", typeof(System.Int32), true, false, false, false,  (int)ConfigurationIdMasterFieldIndex.DivId, 0, 0, 10);
			this.AddElementFieldInfo("ConfigurationIdMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)ConfigurationIdMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("ConfigurationIdMasterEntity", "ReservedId", typeof(System.Int32), true, false, false, false,  (int)ConfigurationIdMasterFieldIndex.ReservedId, 0, 0, 10);
			this.AddElementFieldInfo("ConfigurationIdMasterEntity", "StoreId", typeof(System.Int32), true, false, false, false,  (int)ConfigurationIdMasterFieldIndex.StoreId, 0, 0, 10);
			this.AddElementFieldInfo("ConfigurationIdMasterEntity", "TableName", typeof(System.String), true, false, false, false,  (int)ConfigurationIdMasterFieldIndex.TableName, 255, 0, 0);
			this.AddElementFieldInfo("ConfigurationIdMasterEntity", "Timeout", typeof(System.DateTime), false, false, false, false,  (int)ConfigurationIdMasterFieldIndex.Timeout, 0, 0, 0);
		}
		/// <summary>Inits DeviceMasterEntity's FieldInfo objects</summary>
		private void InitDeviceMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(DeviceMasterFieldIndex), "DeviceMasterEntity");
			this.AddElementFieldInfo("DeviceMasterEntity", "BbArmingCharacter", typeof(Nullable<System.Int16>), false, false, false, true,  (int)DeviceMasterFieldIndex.BbArmingCharacter, 0, 0, 5);
			this.AddElementFieldInfo("DeviceMasterEntity", "BbOffsetId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)DeviceMasterFieldIndex.BbOffsetId, 0, 0, 10);
			this.AddElementFieldInfo("DeviceMasterEntity", "ConnectedDeviceId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)DeviceMasterFieldIndex.ConnectedDeviceId, 0, 0, 10);
			this.AddElementFieldInfo("DeviceMasterEntity", "ConnectionCodeId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)DeviceMasterFieldIndex.ConnectionCodeId, 0, 0, 10);
			this.AddElementFieldInfo("DeviceMasterEntity", "DeviceAbbr", typeof(System.String), false, false, false, true,  (int)DeviceMasterFieldIndex.DeviceAbbr, 4, 0, 0);
			this.AddElementFieldInfo("DeviceMasterEntity", "DeviceDesc", typeof(System.String), false, false, false, true,  (int)DeviceMasterFieldIndex.DeviceDesc, 255, 0, 0);
			this.AddElementFieldInfo("DeviceMasterEntity", "DeviceId", typeof(System.Int32), true, false, false, false,  (int)DeviceMasterFieldIndex.DeviceId, 0, 0, 10);
			this.AddElementFieldInfo("DeviceMasterEntity", "DeviceName", typeof(System.String), false, false, false, true,  (int)DeviceMasterFieldIndex.DeviceName, 20, 0, 0);
			this.AddElementFieldInfo("DeviceMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)DeviceMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("DeviceMasterEntity", "HardwareTypeId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)DeviceMasterFieldIndex.HardwareTypeId, 0, 0, 10);
			this.AddElementFieldInfo("DeviceMasterEntity", "IpAddress", typeof(System.String), false, false, false, true,  (int)DeviceMasterFieldIndex.IpAddress, 15, 0, 0);
			this.AddElementFieldInfo("DeviceMasterEntity", "IpPrintingPort", typeof(Nullable<System.Int16>), false, false, false, true,  (int)DeviceMasterFieldIndex.IpPrintingPort, 0, 0, 5);
			this.AddElementFieldInfo("DeviceMasterEntity", "IpStatusPort", typeof(Nullable<System.Int16>), false, false, false, true,  (int)DeviceMasterFieldIndex.IpStatusPort, 0, 0, 5);
			this.AddElementFieldInfo("DeviceMasterEntity", "KpBackupDeviceId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)DeviceMasterFieldIndex.KpBackupDeviceId, 0, 0, 10);
			this.AddElementFieldInfo("DeviceMasterEntity", "KpOptionGrpId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)DeviceMasterFieldIndex.KpOptionGrpId, 0, 0, 10);
			this.AddElementFieldInfo("DeviceMasterEntity", "ModelId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)DeviceMasterFieldIndex.ModelId, 0, 0, 10);
			this.AddElementFieldInfo("DeviceMasterEntity", "PortId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)DeviceMasterFieldIndex.PortId, 0, 0, 10);
			this.AddElementFieldInfo("DeviceMasterEntity", "PrimaryLanguageId", typeof(System.String), false, false, false, false,  (int)DeviceMasterFieldIndex.PrimaryLanguageId, 9, 0, 0);
			this.AddElementFieldInfo("DeviceMasterEntity", "SecondaryLanguageId", typeof(System.String), false, false, false, true,  (int)DeviceMasterFieldIndex.SecondaryLanguageId, 9, 0, 0);
			this.AddElementFieldInfo("DeviceMasterEntity", "StoreId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)DeviceMasterFieldIndex.StoreId, 0, 0, 10);
			this.AddElementFieldInfo("DeviceMasterEntity", "XferBaudRate", typeof(Nullable<System.Int32>), false, false, false, true,  (int)DeviceMasterFieldIndex.XferBaudRate, 0, 0, 10);
			this.AddElementFieldInfo("DeviceMasterEntity", "XferDataBits", typeof(Nullable<System.Int16>), false, false, false, true,  (int)DeviceMasterFieldIndex.XferDataBits, 0, 0, 5);
			this.AddElementFieldInfo("DeviceMasterEntity", "XferHandshakeId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)DeviceMasterFieldIndex.XferHandshakeId, 0, 0, 10);
			this.AddElementFieldInfo("DeviceMasterEntity", "XferParityId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)DeviceMasterFieldIndex.XferParityId, 0, 0, 10);
			this.AddElementFieldInfo("DeviceMasterEntity", "XferStopBits", typeof(Nullable<System.Int16>), false, false, false, true,  (int)DeviceMasterFieldIndex.XferStopBits, 0, 0, 5);
		}
		/// <summary>Inits DiscoupMasterEntity's FieldInfo objects</summary>
		private void InitDiscoupMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(DiscoupMasterFieldIndex), "DiscoupMasterEntity");
			this.AddElementFieldInfo("DiscoupMasterEntity", "AssocTenderId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)DiscoupMasterFieldIndex.AssocTenderId, 0, 0, 10);
			this.AddElementFieldInfo("DiscoupMasterEntity", "BevRevClassFlag", typeof(System.Boolean), false, false, false, false,  (int)DiscoupMasterFieldIndex.BevRevClassFlag, 0, 0, 0);
			this.AddElementFieldInfo("DiscoupMasterEntity", "DiscountExtraPromptCode", typeof(System.Byte), false, false, false, false,  (int)DiscoupMasterFieldIndex.DiscountExtraPromptCode, 0, 0, 3);
			this.AddElementFieldInfo("DiscoupMasterEntity", "DiscoupAbbr1", typeof(System.String), false, false, false, true,  (int)DiscoupMasterFieldIndex.DiscoupAbbr1, 7, 0, 0);
			this.AddElementFieldInfo("DiscoupMasterEntity", "DiscoupAbbr2", typeof(System.String), false, false, false, true,  (int)DiscoupMasterFieldIndex.DiscoupAbbr2, 7, 0, 0);
			this.AddElementFieldInfo("DiscoupMasterEntity", "DiscoupAmt", typeof(Nullable<System.Decimal>), false, false, false, true,  (int)DiscoupMasterFieldIndex.DiscoupAmt, 0, 4, 19);
			this.AddElementFieldInfo("DiscoupMasterEntity", "DiscoupId", typeof(System.Int32), true, false, false, false,  (int)DiscoupMasterFieldIndex.DiscoupId, 0, 0, 10);
			this.AddElementFieldInfo("DiscoupMasterEntity", "DiscoupItemLevelCodeId", typeof(Nullable<System.Int16>), false, false, false, true,  (int)DiscoupMasterFieldIndex.DiscoupItemLevelCodeId, 0, 0, 5);
			this.AddElementFieldInfo("DiscoupMasterEntity", "DiscoupMaxAmt", typeof(Nullable<System.Decimal>), false, false, false, true,  (int)DiscoupMasterFieldIndex.DiscoupMaxAmt, 0, 4, 19);
			this.AddElementFieldInfo("DiscoupMasterEntity", "DiscoupMaxPercent", typeof(Nullable<System.Double>), false, false, false, true,  (int)DiscoupMasterFieldIndex.DiscoupMaxPercent, 0, 0, 38);
			this.AddElementFieldInfo("DiscoupMasterEntity", "DiscoupName", typeof(System.String), false, false, false, true,  (int)DiscoupMasterFieldIndex.DiscoupName, 16, 0, 0);
			this.AddElementFieldInfo("DiscoupMasterEntity", "DiscoupOpenCodeId", typeof(Nullable<System.Int16>), false, false, false, true,  (int)DiscoupMasterFieldIndex.DiscoupOpenCodeId, 0, 0, 5);
			this.AddElementFieldInfo("DiscoupMasterEntity", "DiscoupPctAmtCodeId", typeof(Nullable<System.Int16>), false, false, false, true,  (int)DiscoupMasterFieldIndex.DiscoupPctAmtCodeId, 0, 0, 5);
			this.AddElementFieldInfo("DiscoupMasterEntity", "DiscoupPercent", typeof(Nullable<System.Double>), false, false, false, true,  (int)DiscoupMasterFieldIndex.DiscoupPercent, 0, 0, 38);
			this.AddElementFieldInfo("DiscoupMasterEntity", "DiscoupTypeCodeId", typeof(Nullable<System.Int16>), false, false, false, true,  (int)DiscoupMasterFieldIndex.DiscoupTypeCodeId, 0, 0, 5);
			this.AddElementFieldInfo("DiscoupMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)DiscoupMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("DiscoupMasterEntity", "ExclusiveFlag", typeof(System.Boolean), false, false, false, false,  (int)DiscoupMasterFieldIndex.ExclusiveFlag, 0, 0, 0);
			this.AddElementFieldInfo("DiscoupMasterEntity", "FoodRevClassFlag", typeof(System.Boolean), false, false, false, false,  (int)DiscoupMasterFieldIndex.FoodRevClassFlag, 0, 0, 0);
			this.AddElementFieldInfo("DiscoupMasterEntity", "OtherRevClassFlag", typeof(System.Boolean), false, false, false, false,  (int)DiscoupMasterFieldIndex.OtherRevClassFlag, 0, 0, 0);
			this.AddElementFieldInfo("DiscoupMasterEntity", "PostAcctNo", typeof(System.String), false, false, false, true,  (int)DiscoupMasterFieldIndex.PostAcctNo, 30, 0, 0);
			this.AddElementFieldInfo("DiscoupMasterEntity", "ProfitCtrGrpId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)DiscoupMasterFieldIndex.ProfitCtrGrpId, 0, 0, 10);
			this.AddElementFieldInfo("DiscoupMasterEntity", "PromptExtraDataFlag", typeof(System.Boolean), false, false, false, false,  (int)DiscoupMasterFieldIndex.PromptExtraDataFlag, 0, 0, 0);
			this.AddElementFieldInfo("DiscoupMasterEntity", "RoundBasis", typeof(Nullable<System.Int32>), false, false, false, true,  (int)DiscoupMasterFieldIndex.RoundBasis, 0, 0, 10);
			this.AddElementFieldInfo("DiscoupMasterEntity", "RoundTypeId", typeof(Nullable<System.Int16>), false, false, false, true,  (int)DiscoupMasterFieldIndex.RoundTypeId, 0, 0, 5);
			this.AddElementFieldInfo("DiscoupMasterEntity", "RowVersion", typeof(System.Byte[]), false, false, true, false,  (int)DiscoupMasterFieldIndex.RowVersion, 2147483647, 0, 0);
			this.AddElementFieldInfo("DiscoupMasterEntity", "SecurityId", typeof(System.Int32), false, false, false, false,  (int)DiscoupMasterFieldIndex.SecurityId, 0, 0, 10);
			this.AddElementFieldInfo("DiscoupMasterEntity", "SodaRevClassFlag", typeof(System.Boolean), false, false, false, false,  (int)DiscoupMasterFieldIndex.SodaRevClassFlag, 0, 0, 0);
			this.AddElementFieldInfo("DiscoupMasterEntity", "StoreId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)DiscoupMasterFieldIndex.StoreId, 0, 0, 10);
			this.AddElementFieldInfo("DiscoupMasterEntity", "ThreshholdAmt", typeof(Nullable<System.Decimal>), false, false, false, true,  (int)DiscoupMasterFieldIndex.ThreshholdAmt, 0, 4, 19);
		}
		/// <summary>Inits EmpGroupMasterEntity's FieldInfo objects</summary>
		private void InitEmpGroupMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(EmpGroupMasterFieldIndex), "EmpGroupMasterEntity");
			this.AddElementFieldInfo("EmpGroupMasterEntity", "EmpAutoUpdateFlag", typeof(System.Boolean), false, false, false, false,  (int)EmpGroupMasterFieldIndex.EmpAutoUpdateFlag, 0, 0, 0);
			this.AddElementFieldInfo("EmpGroupMasterEntity", "EmpGrpAbbr1", typeof(System.String), false, false, false, true,  (int)EmpGroupMasterFieldIndex.EmpGrpAbbr1, 7, 0, 0);
			this.AddElementFieldInfo("EmpGroupMasterEntity", "EmpGrpAbbr2", typeof(System.String), false, false, false, true,  (int)EmpGroupMasterFieldIndex.EmpGrpAbbr2, 7, 0, 0);
			this.AddElementFieldInfo("EmpGroupMasterEntity", "EmpGrpId", typeof(System.Int32), true, false, false, false,  (int)EmpGroupMasterFieldIndex.EmpGrpId, 0, 0, 10);
			this.AddElementFieldInfo("EmpGroupMasterEntity", "EmpGrpName", typeof(System.String), false, false, false, true,  (int)EmpGroupMasterFieldIndex.EmpGrpName, 30, 0, 0);
			this.AddElementFieldInfo("EmpGroupMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)EmpGroupMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("EmpGroupMasterEntity", "StoreId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)EmpGroupMasterFieldIndex.StoreId, 0, 0, 10);
		}
		/// <summary>Inits EmpGrpEmpJoinEntity's FieldInfo objects</summary>
		private void InitEmpGrpEmpJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(EmpGrpEmpJoinFieldIndex), "EmpGrpEmpJoinEntity");
			this.AddElementFieldInfo("EmpGrpEmpJoinEntity", "EmpGrpId", typeof(System.Int32), true, false, false, false,  (int)EmpGrpEmpJoinFieldIndex.EmpGrpId, 0, 0, 10);
			this.AddElementFieldInfo("EmpGrpEmpJoinEntity", "EmpId", typeof(System.Int32), true, false, false, false,  (int)EmpGrpEmpJoinFieldIndex.EmpId, 0, 0, 10);
			this.AddElementFieldInfo("EmpGrpEmpJoinEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)EmpGrpEmpJoinFieldIndex.EntId, 0, 0, 10);
		}
		/// <summary>Inits EmpJobcodeJoinEntity's FieldInfo objects</summary>
		private void InitEmpJobcodeJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(EmpJobcodeJoinFieldIndex), "EmpJobcodeJoinEntity");
			this.AddElementFieldInfo("EmpJobcodeJoinEntity", "EmpId", typeof(System.Int32), true, false, false, false,  (int)EmpJobcodeJoinFieldIndex.EmpId, 0, 0, 10);
			this.AddElementFieldInfo("EmpJobcodeJoinEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)EmpJobcodeJoinFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("EmpJobcodeJoinEntity", "JobcodeAddDt", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)EmpJobcodeJoinFieldIndex.JobcodeAddDt, 0, 0, 0);
			this.AddElementFieldInfo("EmpJobcodeJoinEntity", "JobcodeId", typeof(System.Int32), true, false, false, false,  (int)EmpJobcodeJoinFieldIndex.JobcodeId, 0, 0, 10);
			this.AddElementFieldInfo("EmpJobcodeJoinEntity", "JobcodeLaborRate", typeof(Nullable<System.Decimal>), false, false, false, true,  (int)EmpJobcodeJoinFieldIndex.JobcodeLaborRate, 0, 4, 19);
			this.AddElementFieldInfo("EmpJobcodeJoinEntity", "ManagerFlag", typeof(System.Boolean), false, false, false, false,  (int)EmpJobcodeJoinFieldIndex.ManagerFlag, 0, 0, 0);
			this.AddElementFieldInfo("EmpJobcodeJoinEntity", "MultipleSignonFlag", typeof(System.Boolean), false, false, false, false,  (int)EmpJobcodeJoinFieldIndex.MultipleSignonFlag, 0, 0, 0);
			this.AddElementFieldInfo("EmpJobcodeJoinEntity", "PrimaryJobcodeFlag", typeof(System.Boolean), false, false, false, false,  (int)EmpJobcodeJoinFieldIndex.PrimaryJobcodeFlag, 0, 0, 0);
		}
		/// <summary>Inits EmpMasterEntity's FieldInfo objects</summary>
		private void InitEmpMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(EmpMasterFieldIndex), "EmpMasterEntity");
			this.AddElementFieldInfo("EmpMasterEntity", "DataControlGroupId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)EmpMasterFieldIndex.DataControlGroupId, 0, 0, 10);
			this.AddElementFieldInfo("EmpMasterEntity", "DefaultLanguageId", typeof(System.String), false, false, false, false,  (int)EmpMasterFieldIndex.DefaultLanguageId, 9, 0, 0);
			this.AddElementFieldInfo("EmpMasterEntity", "EmpBirthdate", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)EmpMasterFieldIndex.EmpBirthdate, 0, 0, 0);
			this.AddElementFieldInfo("EmpMasterEntity", "EmpCardNo", typeof(Nullable<System.Int32>), false, false, false, true,  (int)EmpMasterFieldIndex.EmpCardNo, 0, 0, 10);
			this.AddElementFieldInfo("EmpMasterEntity", "EmpCity", typeof(System.String), false, false, false, true,  (int)EmpMasterFieldIndex.EmpCity, 50, 0, 0);
			this.AddElementFieldInfo("EmpMasterEntity", "EmpCountry", typeof(System.String), false, false, false, true,  (int)EmpMasterFieldIndex.EmpCountry, 50, 0, 0);
			this.AddElementFieldInfo("EmpMasterEntity", "EmpEmergencyTel", typeof(System.String), false, false, false, true,  (int)EmpMasterFieldIndex.EmpEmergencyTel, 16, 0, 0);
			this.AddElementFieldInfo("EmpMasterEntity", "EmpFirstName", typeof(System.String), false, false, false, true,  (int)EmpMasterFieldIndex.EmpFirstName, 16, 0, 0);
			this.AddElementFieldInfo("EmpMasterEntity", "EmpHireDt", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)EmpMasterFieldIndex.EmpHireDt, 0, 0, 0);
			this.AddElementFieldInfo("EmpMasterEntity", "EmpId", typeof(System.Int32), true, false, false, false,  (int)EmpMasterFieldIndex.EmpId, 0, 0, 10);
			this.AddElementFieldInfo("EmpMasterEntity", "EmpLastName", typeof(System.String), false, false, false, true,  (int)EmpMasterFieldIndex.EmpLastName, 50, 0, 0);
			this.AddElementFieldInfo("EmpMasterEntity", "EmpLastReviewDt", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)EmpMasterFieldIndex.EmpLastReviewDt, 0, 0, 0);
			this.AddElementFieldInfo("EmpMasterEntity", "EmpNextReviewDt", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)EmpMasterFieldIndex.EmpNextReviewDt, 0, 0, 0);
			this.AddElementFieldInfo("EmpMasterEntity", "EmpPassword", typeof(Nullable<System.Int32>), false, false, false, true,  (int)EmpMasterFieldIndex.EmpPassword, 0, 0, 10);
			this.AddElementFieldInfo("EmpMasterEntity", "EmpPosName", typeof(System.String), false, false, false, true,  (int)EmpMasterFieldIndex.EmpPosName, 16, 0, 0);
			this.AddElementFieldInfo("EmpMasterEntity", "EmpSsn", typeof(System.String), false, false, false, true,  (int)EmpMasterFieldIndex.EmpSsn, 12, 0, 0);
			this.AddElementFieldInfo("EmpMasterEntity", "EmpState", typeof(System.String), false, false, false, true,  (int)EmpMasterFieldIndex.EmpState, 50, 0, 0);
			this.AddElementFieldInfo("EmpMasterEntity", "EmpStreet1", typeof(System.String), false, false, false, true,  (int)EmpMasterFieldIndex.EmpStreet1, 50, 0, 0);
			this.AddElementFieldInfo("EmpMasterEntity", "EmpStreet2", typeof(System.String), false, false, false, true,  (int)EmpMasterFieldIndex.EmpStreet2, 50, 0, 0);
			this.AddElementFieldInfo("EmpMasterEntity", "EmpTel", typeof(System.String), false, false, false, true,  (int)EmpMasterFieldIndex.EmpTel, 16, 0, 0);
			this.AddElementFieldInfo("EmpMasterEntity", "EmpTerminateDt", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)EmpMasterFieldIndex.EmpTerminateDt, 0, 0, 0);
			this.AddElementFieldInfo("EmpMasterEntity", "EmpZip", typeof(System.String), false, false, false, true,  (int)EmpMasterFieldIndex.EmpZip, 16, 0, 0);
			this.AddElementFieldInfo("EmpMasterEntity", "EntId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)EmpMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("EmpMasterEntity", "RowVersion", typeof(System.Byte[]), false, false, true, false,  (int)EmpMasterFieldIndex.RowVersion, 2147483647, 0, 0);
			this.AddElementFieldInfo("EmpMasterEntity", "SecondaryId", typeof(System.String), false, false, false, true,  (int)EmpMasterFieldIndex.SecondaryId, 10, 0, 0);
			this.AddElementFieldInfo("EmpMasterEntity", "StoreId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)EmpMasterFieldIndex.StoreId, 0, 0, 10);
			this.AddElementFieldInfo("EmpMasterEntity", "SupervisorEmpId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)EmpMasterFieldIndex.SupervisorEmpId, 0, 0, 10);
		}
		/// <summary>Inits FuncBttnTextJoinEntity's FieldInfo objects</summary>
		private void InitFuncBttnTextJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(FuncBttnTextJoinFieldIndex), "FuncBttnTextJoinEntity");
			this.AddElementFieldInfo("FuncBttnTextJoinEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)FuncBttnTextJoinFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("FuncBttnTextJoinEntity", "FuncbttnId", typeof(System.Int32), true, false, false, false,  (int)FuncBttnTextJoinFieldIndex.FuncbttnId, 0, 0, 10);
			this.AddElementFieldInfo("FuncBttnTextJoinEntity", "FuncbttnText", typeof(System.String), false, false, false, true,  (int)FuncBttnTextJoinFieldIndex.FuncbttnText, 50, 0, 0);
		}
		/// <summary>Inits Ga4680ExportParmMasterEntity's FieldInfo objects</summary>
		private void InitGa4680ExportParmMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(Ga4680ExportParmMasterFieldIndex), "Ga4680ExportParmMasterEntity");
			this.AddElementFieldInfo("Ga4680ExportParmMasterEntity", "BusinessDayOffset", typeof(Nullable<System.Int16>), false, false, false, true,  (int)Ga4680ExportParmMasterFieldIndex.BusinessDayOffset, 0, 0, 5);
			this.AddElementFieldInfo("Ga4680ExportParmMasterEntity", "BusinessDayOptionId", typeof(Nullable<System.Int16>), false, false, false, true,  (int)Ga4680ExportParmMasterFieldIndex.BusinessDayOptionId, 0, 0, 5);
			this.AddElementFieldInfo("Ga4680ExportParmMasterEntity", "DelimiterValue", typeof(System.String), false, false, false, true,  (int)Ga4680ExportParmMasterFieldIndex.DelimiterValue, 3, 0, 0);
			this.AddElementFieldInfo("Ga4680ExportParmMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)Ga4680ExportParmMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("Ga4680ExportParmMasterEntity", "FileFormatId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)Ga4680ExportParmMasterFieldIndex.FileFormatId, 0, 0, 10);
			this.AddElementFieldInfo("Ga4680ExportParmMasterEntity", "Ga1ExportPath", typeof(System.String), false, false, false, true,  (int)Ga4680ExportParmMasterFieldIndex.Ga1ExportPath, 120, 0, 0);
			this.AddElementFieldInfo("Ga4680ExportParmMasterEntity", "Ga1RequiredFlag", typeof(System.Boolean), false, false, false, false,  (int)Ga4680ExportParmMasterFieldIndex.Ga1RequiredFlag, 0, 0, 0);
			this.AddElementFieldInfo("Ga4680ExportParmMasterEntity", "Ga2ExportPath", typeof(System.String), false, false, false, true,  (int)Ga4680ExportParmMasterFieldIndex.Ga2ExportPath, 120, 0, 0);
			this.AddElementFieldInfo("Ga4680ExportParmMasterEntity", "Ga2RequiredFlag", typeof(System.Boolean), false, false, false, false,  (int)Ga4680ExportParmMasterFieldIndex.Ga2RequiredFlag, 0, 0, 0);
			this.AddElementFieldInfo("Ga4680ExportParmMasterEntity", "Ga3ExportPath", typeof(System.String), false, false, false, true,  (int)Ga4680ExportParmMasterFieldIndex.Ga3ExportPath, 120, 0, 0);
			this.AddElementFieldInfo("Ga4680ExportParmMasterEntity", "Ga3RequiredFlag", typeof(System.Boolean), false, false, false, false,  (int)Ga4680ExportParmMasterFieldIndex.Ga3RequiredFlag, 0, 0, 0);
			this.AddElementFieldInfo("Ga4680ExportParmMasterEntity", "Ga4ExportPath", typeof(System.String), false, false, false, true,  (int)Ga4680ExportParmMasterFieldIndex.Ga4ExportPath, 120, 0, 0);
			this.AddElementFieldInfo("Ga4680ExportParmMasterEntity", "Ga4RequiredFlag", typeof(System.Boolean), false, false, false, false,  (int)Ga4680ExportParmMasterFieldIndex.Ga4RequiredFlag, 0, 0, 0);
			this.AddElementFieldInfo("Ga4680ExportParmMasterEntity", "Ga5ExportPath", typeof(System.String), false, false, false, true,  (int)Ga4680ExportParmMasterFieldIndex.Ga5ExportPath, 120, 0, 0);
			this.AddElementFieldInfo("Ga4680ExportParmMasterEntity", "Ga5RequiredFlag", typeof(System.Boolean), false, false, false, false,  (int)Ga4680ExportParmMasterFieldIndex.Ga5RequiredFlag, 0, 0, 0);
			this.AddElementFieldInfo("Ga4680ExportParmMasterEntity", "Ga6ExportPath", typeof(System.String), false, false, false, true,  (int)Ga4680ExportParmMasterFieldIndex.Ga6ExportPath, 120, 0, 0);
			this.AddElementFieldInfo("Ga4680ExportParmMasterEntity", "Ga6RequiredFlag", typeof(System.Boolean), false, false, false, false,  (int)Ga4680ExportParmMasterFieldIndex.Ga6RequiredFlag, 0, 0, 0);
			this.AddElementFieldInfo("Ga4680ExportParmMasterEntity", "Ga7ExportPath", typeof(System.String), false, false, false, true,  (int)Ga4680ExportParmMasterFieldIndex.Ga7ExportPath, 120, 0, 0);
			this.AddElementFieldInfo("Ga4680ExportParmMasterEntity", "Ga7RequiredFlag", typeof(System.Boolean), false, false, false, false,  (int)Ga4680ExportParmMasterFieldIndex.Ga7RequiredFlag, 0, 0, 0);
			this.AddElementFieldInfo("Ga4680ExportParmMasterEntity", "Ga8ExportPath", typeof(System.String), false, false, false, true,  (int)Ga4680ExportParmMasterFieldIndex.Ga8ExportPath, 120, 0, 0);
			this.AddElementFieldInfo("Ga4680ExportParmMasterEntity", "Ga8RequiredFlag", typeof(System.Boolean), false, false, false, false,  (int)Ga4680ExportParmMasterFieldIndex.Ga8RequiredFlag, 0, 0, 0);
			this.AddElementFieldInfo("Ga4680ExportParmMasterEntity", "Ga9ExportPath", typeof(System.String), false, false, false, true,  (int)Ga4680ExportParmMasterFieldIndex.Ga9ExportPath, 120, 0, 0);
			this.AddElementFieldInfo("Ga4680ExportParmMasterEntity", "Ga9RequiredFlag", typeof(System.Boolean), false, false, false, false,  (int)Ga4680ExportParmMasterFieldIndex.Ga9RequiredFlag, 0, 0, 0);
			this.AddElementFieldInfo("Ga4680ExportParmMasterEntity", "GaExportId", typeof(System.Int32), true, false, false, false,  (int)Ga4680ExportParmMasterFieldIndex.GaExportId, 0, 0, 10);
			this.AddElementFieldInfo("Ga4680ExportParmMasterEntity", "LocationCode", typeof(System.String), false, false, false, true,  (int)Ga4680ExportParmMasterFieldIndex.LocationCode, 2, 0, 0);
			this.AddElementFieldInfo("Ga4680ExportParmMasterEntity", "NumDays", typeof(Nullable<System.Int32>), false, false, false, true,  (int)Ga4680ExportParmMasterFieldIndex.NumDays, 0, 0, 10);
		}
		/// <summary>Inits Ga4680ImportParmMasterEntity's FieldInfo objects</summary>
		private void InitGa4680ImportParmMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(Ga4680ImportParmMasterFieldIndex), "Ga4680ImportParmMasterEntity");
			this.AddElementFieldInfo("Ga4680ImportParmMasterEntity", "DelimiterRequiredFlag", typeof(System.Boolean), false, false, false, false,  (int)Ga4680ImportParmMasterFieldIndex.DelimiterRequiredFlag, 0, 0, 0);
			this.AddElementFieldInfo("Ga4680ImportParmMasterEntity", "DelimiterValue", typeof(System.String), false, false, false, true,  (int)Ga4680ImportParmMasterFieldIndex.DelimiterValue, 3, 0, 0);
			this.AddElementFieldInfo("Ga4680ImportParmMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)Ga4680ImportParmMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("Ga4680ImportParmMasterEntity", "Ga1ImportPath", typeof(System.String), false, false, false, true,  (int)Ga4680ImportParmMasterFieldIndex.Ga1ImportPath, 120, 0, 0);
			this.AddElementFieldInfo("Ga4680ImportParmMasterEntity", "Ga1PreimportOptionId", typeof(Nullable<System.Int16>), false, false, false, true,  (int)Ga4680ImportParmMasterFieldIndex.Ga1PreimportOptionId, 0, 0, 5);
			this.AddElementFieldInfo("Ga4680ImportParmMasterEntity", "Ga1RequiredFlag", typeof(System.Boolean), false, false, false, false,  (int)Ga4680ImportParmMasterFieldIndex.Ga1RequiredFlag, 0, 0, 0);
			this.AddElementFieldInfo("Ga4680ImportParmMasterEntity", "Ga2ImportPath", typeof(System.String), false, false, false, true,  (int)Ga4680ImportParmMasterFieldIndex.Ga2ImportPath, 120, 0, 0);
			this.AddElementFieldInfo("Ga4680ImportParmMasterEntity", "Ga2PreimportOptionId", typeof(Nullable<System.Int16>), false, false, false, true,  (int)Ga4680ImportParmMasterFieldIndex.Ga2PreimportOptionId, 0, 0, 5);
			this.AddElementFieldInfo("Ga4680ImportParmMasterEntity", "Ga2RequiredFlag", typeof(System.Boolean), false, false, false, false,  (int)Ga4680ImportParmMasterFieldIndex.Ga2RequiredFlag, 0, 0, 0);
			this.AddElementFieldInfo("Ga4680ImportParmMasterEntity", "Ga3ImportPath", typeof(System.String), false, false, false, true,  (int)Ga4680ImportParmMasterFieldIndex.Ga3ImportPath, 120, 0, 0);
			this.AddElementFieldInfo("Ga4680ImportParmMasterEntity", "Ga3PreimportOptionId", typeof(Nullable<System.Int16>), false, false, false, true,  (int)Ga4680ImportParmMasterFieldIndex.Ga3PreimportOptionId, 0, 0, 5);
			this.AddElementFieldInfo("Ga4680ImportParmMasterEntity", "Ga3RequiredFlag", typeof(System.Boolean), false, false, false, false,  (int)Ga4680ImportParmMasterFieldIndex.Ga3RequiredFlag, 0, 0, 0);
			this.AddElementFieldInfo("Ga4680ImportParmMasterEntity", "Ga4ImportPath", typeof(System.String), false, false, false, true,  (int)Ga4680ImportParmMasterFieldIndex.Ga4ImportPath, 120, 0, 0);
			this.AddElementFieldInfo("Ga4680ImportParmMasterEntity", "Ga4PreimportOptionId", typeof(Nullable<System.Int16>), false, false, false, true,  (int)Ga4680ImportParmMasterFieldIndex.Ga4PreimportOptionId, 0, 0, 5);
			this.AddElementFieldInfo("Ga4680ImportParmMasterEntity", "Ga4RequiredFlag", typeof(System.Boolean), false, false, false, false,  (int)Ga4680ImportParmMasterFieldIndex.Ga4RequiredFlag, 0, 0, 0);
			this.AddElementFieldInfo("Ga4680ImportParmMasterEntity", "Ga5ImportPath", typeof(System.String), false, false, false, true,  (int)Ga4680ImportParmMasterFieldIndex.Ga5ImportPath, 120, 0, 0);
			this.AddElementFieldInfo("Ga4680ImportParmMasterEntity", "Ga5PreimportOptionId", typeof(Nullable<System.Int16>), false, false, false, true,  (int)Ga4680ImportParmMasterFieldIndex.Ga5PreimportOptionId, 0, 0, 5);
			this.AddElementFieldInfo("Ga4680ImportParmMasterEntity", "Ga5RequiredFlag", typeof(System.Boolean), false, false, false, false,  (int)Ga4680ImportParmMasterFieldIndex.Ga5RequiredFlag, 0, 0, 0);
			this.AddElementFieldInfo("Ga4680ImportParmMasterEntity", "Ga6ImportPath", typeof(System.String), false, false, false, true,  (int)Ga4680ImportParmMasterFieldIndex.Ga6ImportPath, 120, 0, 0);
			this.AddElementFieldInfo("Ga4680ImportParmMasterEntity", "Ga6PreimportOptionId", typeof(Nullable<System.Int16>), false, false, false, true,  (int)Ga4680ImportParmMasterFieldIndex.Ga6PreimportOptionId, 0, 0, 5);
			this.AddElementFieldInfo("Ga4680ImportParmMasterEntity", "Ga6RequiredFlag", typeof(System.Boolean), false, false, false, false,  (int)Ga4680ImportParmMasterFieldIndex.Ga6RequiredFlag, 0, 0, 0);
			this.AddElementFieldInfo("Ga4680ImportParmMasterEntity", "Ga7ImportPath", typeof(System.String), false, false, false, true,  (int)Ga4680ImportParmMasterFieldIndex.Ga7ImportPath, 120, 0, 0);
			this.AddElementFieldInfo("Ga4680ImportParmMasterEntity", "Ga7PreimportOptionId", typeof(Nullable<System.Int16>), false, false, false, true,  (int)Ga4680ImportParmMasterFieldIndex.Ga7PreimportOptionId, 0, 0, 5);
			this.AddElementFieldInfo("Ga4680ImportParmMasterEntity", "Ga7RequiredFlag", typeof(System.Boolean), false, false, false, false,  (int)Ga4680ImportParmMasterFieldIndex.Ga7RequiredFlag, 0, 0, 0);
			this.AddElementFieldInfo("Ga4680ImportParmMasterEntity", "Ga8ImportPath", typeof(System.String), false, false, false, true,  (int)Ga4680ImportParmMasterFieldIndex.Ga8ImportPath, 120, 0, 0);
			this.AddElementFieldInfo("Ga4680ImportParmMasterEntity", "Ga8PreimportOptionId", typeof(Nullable<System.Int16>), false, false, false, true,  (int)Ga4680ImportParmMasterFieldIndex.Ga8PreimportOptionId, 0, 0, 5);
			this.AddElementFieldInfo("Ga4680ImportParmMasterEntity", "Ga8RequiredFlag", typeof(System.Boolean), false, false, false, false,  (int)Ga4680ImportParmMasterFieldIndex.Ga8RequiredFlag, 0, 0, 0);
			this.AddElementFieldInfo("Ga4680ImportParmMasterEntity", "Ga9ImportPath", typeof(System.String), false, false, false, true,  (int)Ga4680ImportParmMasterFieldIndex.Ga9ImportPath, 120, 0, 0);
			this.AddElementFieldInfo("Ga4680ImportParmMasterEntity", "Ga9PreimportOptionId", typeof(Nullable<System.Int16>), false, false, false, true,  (int)Ga4680ImportParmMasterFieldIndex.Ga9PreimportOptionId, 0, 0, 5);
			this.AddElementFieldInfo("Ga4680ImportParmMasterEntity", "Ga9RequiredFlag", typeof(System.Boolean), false, false, false, false,  (int)Ga4680ImportParmMasterFieldIndex.Ga9RequiredFlag, 0, 0, 0);
			this.AddElementFieldInfo("Ga4680ImportParmMasterEntity", "GaImportId", typeof(System.Int32), true, false, false, false,  (int)Ga4680ImportParmMasterFieldIndex.GaImportId, 0, 0, 10);
			this.AddElementFieldInfo("Ga4680ImportParmMasterEntity", "PreimportOptionId", typeof(Nullable<System.Int16>), false, false, false, true,  (int)Ga4680ImportParmMasterFieldIndex.PreimportOptionId, 0, 0, 5);
		}
		/// <summary>Inits GeneralLedgerMapMasterEntity's FieldInfo objects</summary>
		private void InitGeneralLedgerMapMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(GeneralLedgerMapMasterFieldIndex), "GeneralLedgerMapMasterEntity");
			this.AddElementFieldInfo("GeneralLedgerMapMasterEntity", "EntId", typeof(System.Int32), false, false, false, false,  (int)GeneralLedgerMapMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("GeneralLedgerMapMasterEntity", "GlAccountNo", typeof(System.String), false, false, false, true,  (int)GeneralLedgerMapMasterFieldIndex.GlAccountNo, 20, 0, 0);
			this.AddElementFieldInfo("GeneralLedgerMapMasterEntity", "GlClassId", typeof(System.Int32), false, false, false, false,  (int)GeneralLedgerMapMasterFieldIndex.GlClassId, 0, 0, 10);
			this.AddElementFieldInfo("GeneralLedgerMapMasterEntity", "GlMapGenId", typeof(System.Int32), true, false, true, false,  (int)GeneralLedgerMapMasterFieldIndex.GlMapGenId, 0, 0, 10);
			this.AddElementFieldInfo("GeneralLedgerMapMasterEntity", "Parm1ObjectId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)GeneralLedgerMapMasterFieldIndex.Parm1ObjectId, 0, 0, 10);
			this.AddElementFieldInfo("GeneralLedgerMapMasterEntity", "Parm2ObjectId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)GeneralLedgerMapMasterFieldIndex.Parm2ObjectId, 0, 0, 10);
			this.AddElementFieldInfo("GeneralLedgerMapMasterEntity", "ProfitCenterId", typeof(System.Int32), false, false, false, false,  (int)GeneralLedgerMapMasterFieldIndex.ProfitCenterId, 0, 0, 10);
			this.AddElementFieldInfo("GeneralLedgerMapMasterEntity", "ValidPermutationFlag", typeof(Nullable<System.Boolean>), false, false, false, true,  (int)GeneralLedgerMapMasterFieldIndex.ValidPermutationFlag, 0, 0, 0);
		}
		/// <summary>Inits GiftCardMasterEntity's FieldInfo objects</summary>
		private void InitGiftCardMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(GiftCardMasterFieldIndex), "GiftCardMasterEntity");
			this.AddElementFieldInfo("GiftCardMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)GiftCardMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("GiftCardMasterEntity", "GiftCardAbbr1", typeof(System.String), false, false, false, false,  (int)GiftCardMasterFieldIndex.GiftCardAbbr1, 7, 0, 0);
			this.AddElementFieldInfo("GiftCardMasterEntity", "GiftCardAbbr2", typeof(System.String), false, false, false, true,  (int)GiftCardMasterFieldIndex.GiftCardAbbr2, 7, 0, 0);
			this.AddElementFieldInfo("GiftCardMasterEntity", "GiftCardAmt", typeof(Nullable<System.Decimal>), false, false, false, true,  (int)GiftCardMasterFieldIndex.GiftCardAmt, 0, 4, 19);
			this.AddElementFieldInfo("GiftCardMasterEntity", "GiftCardId", typeof(System.Int32), true, false, false, false,  (int)GiftCardMasterFieldIndex.GiftCardId, 0, 0, 10);
			this.AddElementFieldInfo("GiftCardMasterEntity", "GiftCardMaxAmt", typeof(Nullable<System.Decimal>), false, false, false, true,  (int)GiftCardMasterFieldIndex.GiftCardMaxAmt, 0, 4, 19);
			this.AddElementFieldInfo("GiftCardMasterEntity", "GiftCardMinAmt", typeof(Nullable<System.Decimal>), false, false, false, true,  (int)GiftCardMasterFieldIndex.GiftCardMinAmt, 0, 4, 19);
			this.AddElementFieldInfo("GiftCardMasterEntity", "GiftCardName", typeof(System.String), false, false, false, false,  (int)GiftCardMasterFieldIndex.GiftCardName, 16, 0, 0);
			this.AddElementFieldInfo("GiftCardMasterEntity", "GiftCardOpenCodeId", typeof(System.Int16), false, false, false, false,  (int)GiftCardMasterFieldIndex.GiftCardOpenCodeId, 0, 0, 5);
			this.AddElementFieldInfo("GiftCardMasterEntity", "MenuItemId", typeof(System.Int32), false, false, false, false,  (int)GiftCardMasterFieldIndex.MenuItemId, 0, 0, 10);
			this.AddElementFieldInfo("GiftCardMasterEntity", "NodeId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)GiftCardMasterFieldIndex.NodeId, 0, 0, 10);
		}
		/// <summary>Inits GratuityCatMasterEntity's FieldInfo objects</summary>
		private void InitGratuityCatMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(GratuityCatMasterFieldIndex), "GratuityCatMasterEntity");
			this.AddElementFieldInfo("GratuityCatMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)GratuityCatMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("GratuityCatMasterEntity", "GratCatAbbr1", typeof(System.String), false, false, false, true,  (int)GratuityCatMasterFieldIndex.GratCatAbbr1, 7, 0, 0);
			this.AddElementFieldInfo("GratuityCatMasterEntity", "GratCatAbbr2", typeof(System.String), false, false, false, true,  (int)GratuityCatMasterFieldIndex.GratCatAbbr2, 7, 0, 0);
			this.AddElementFieldInfo("GratuityCatMasterEntity", "GratCatId", typeof(System.Int32), true, false, false, false,  (int)GratuityCatMasterFieldIndex.GratCatId, 0, 0, 10);
			this.AddElementFieldInfo("GratuityCatMasterEntity", "GratCatName", typeof(System.String), false, false, false, true,  (int)GratuityCatMasterFieldIndex.GratCatName, 20, 0, 0);
			this.AddElementFieldInfo("GratuityCatMasterEntity", "GratCatSvsChgFlag", typeof(System.Boolean), false, false, false, false,  (int)GratuityCatMasterFieldIndex.GratCatSvsChgFlag, 0, 0, 0);
			this.AddElementFieldInfo("GratuityCatMasterEntity", "LoyaltyEarnEligibleFlag", typeof(System.Boolean), false, false, false, false,  (int)GratuityCatMasterFieldIndex.LoyaltyEarnEligibleFlag, 0, 0, 0);
			this.AddElementFieldInfo("GratuityCatMasterEntity", "LoyaltyRedeemEligibleFlag", typeof(System.Boolean), false, false, false, false,  (int)GratuityCatMasterFieldIndex.LoyaltyRedeemEligibleFlag, 0, 0, 0);
			this.AddElementFieldInfo("GratuityCatMasterEntity", "RowVersion", typeof(System.Byte[]), false, false, true, false,  (int)GratuityCatMasterFieldIndex.RowVersion, 2147483647, 0, 0);
			this.AddElementFieldInfo("GratuityCatMasterEntity", "StoreId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)GratuityCatMasterFieldIndex.StoreId, 0, 0, 10);
		}
		/// <summary>Inits GratuityMasterEntity's FieldInfo objects</summary>
		private void InitGratuityMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(GratuityMasterFieldIndex), "GratuityMasterEntity");
			this.AddElementFieldInfo("GratuityMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)GratuityMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("GratuityMasterEntity", "GratAbbr1", typeof(System.String), false, false, false, true,  (int)GratuityMasterFieldIndex.GratAbbr1, 7, 0, 0);
			this.AddElementFieldInfo("GratuityMasterEntity", "GratAbbr2", typeof(System.String), false, false, false, true,  (int)GratuityMasterFieldIndex.GratAbbr2, 7, 0, 0);
			this.AddElementFieldInfo("GratuityMasterEntity", "GratAmt", typeof(Nullable<System.Decimal>), false, false, false, true,  (int)GratuityMasterFieldIndex.GratAmt, 0, 4, 19);
			this.AddElementFieldInfo("GratuityMasterEntity", "GratCatId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)GratuityMasterFieldIndex.GratCatId, 0, 0, 10);
			this.AddElementFieldInfo("GratuityMasterEntity", "GratId", typeof(System.Int32), true, false, false, false,  (int)GratuityMasterFieldIndex.GratId, 0, 0, 10);
			this.AddElementFieldInfo("GratuityMasterEntity", "GratName", typeof(System.String), false, false, false, true,  (int)GratuityMasterFieldIndex.GratName, 20, 0, 0);
			this.AddElementFieldInfo("GratuityMasterEntity", "GratPercent", typeof(Nullable<System.Double>), false, false, false, true,  (int)GratuityMasterFieldIndex.GratPercent, 0, 0, 38);
			this.AddElementFieldInfo("GratuityMasterEntity", "GratPerCoverAmt", typeof(Nullable<System.Decimal>), false, false, false, true,  (int)GratuityMasterFieldIndex.GratPerCoverAmt, 0, 4, 19);
			this.AddElementFieldInfo("GratuityMasterEntity", "GratRecipientCodeId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)GratuityMasterFieldIndex.GratRecipientCodeId, 0, 0, 10);
			this.AddElementFieldInfo("GratuityMasterEntity", "GratRecipientEmpId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)GratuityMasterFieldIndex.GratRecipientEmpId, 0, 0, 10);
			this.AddElementFieldInfo("GratuityMasterEntity", "GratRemovableFlag", typeof(System.Boolean), false, false, false, false,  (int)GratuityMasterFieldIndex.GratRemovableFlag, 0, 0, 0);
			this.AddElementFieldInfo("GratuityMasterEntity", "GratTermPayFlag", typeof(System.Boolean), false, false, false, false,  (int)GratuityMasterFieldIndex.GratTermPayFlag, 0, 0, 0);
			this.AddElementFieldInfo("GratuityMasterEntity", "GratuityIsInclusiveFlag", typeof(System.Boolean), false, false, false, false,  (int)GratuityMasterFieldIndex.GratuityIsInclusiveFlag, 0, 0, 0);
			this.AddElementFieldInfo("GratuityMasterEntity", "InclusiveGratuityConvertedFlag", typeof(System.Boolean), false, false, false, false,  (int)GratuityMasterFieldIndex.InclusiveGratuityConvertedFlag, 0, 0, 0);
			this.AddElementFieldInfo("GratuityMasterEntity", "MaxCoverThreshhold", typeof(Nullable<System.Int16>), false, false, false, true,  (int)GratuityMasterFieldIndex.MaxCoverThreshhold, 0, 0, 5);
			this.AddElementFieldInfo("GratuityMasterEntity", "MinCoverThreshhold", typeof(Nullable<System.Int16>), false, false, false, true,  (int)GratuityMasterFieldIndex.MinCoverThreshhold, 0, 0, 5);
			this.AddElementFieldInfo("GratuityMasterEntity", "PercentOrAmountFlag", typeof(System.Boolean), false, false, false, false,  (int)GratuityMasterFieldIndex.PercentOrAmountFlag, 0, 0, 0);
			this.AddElementFieldInfo("GratuityMasterEntity", "RoundBasis", typeof(Nullable<System.Int32>), false, false, false, true,  (int)GratuityMasterFieldIndex.RoundBasis, 0, 0, 10);
			this.AddElementFieldInfo("GratuityMasterEntity", "RoundTypeId", typeof(Nullable<System.Int16>), false, false, false, true,  (int)GratuityMasterFieldIndex.RoundTypeId, 0, 0, 5);
			this.AddElementFieldInfo("GratuityMasterEntity", "RowVersion", typeof(System.Byte[]), false, false, true, false,  (int)GratuityMasterFieldIndex.RowVersion, 2147483647, 0, 0);
			this.AddElementFieldInfo("GratuityMasterEntity", "SalesCalculationOnNetOrGrossFlag", typeof(System.Boolean), false, false, false, false,  (int)GratuityMasterFieldIndex.SalesCalculationOnNetOrGrossFlag, 0, 0, 0);
			this.AddElementFieldInfo("GratuityMasterEntity", "StoreId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)GratuityMasterFieldIndex.StoreId, 0, 0, 10);
		}
		/// <summary>Inits IdMasterEntity's FieldInfo objects</summary>
		private void InitIdMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(IdMasterFieldIndex), "IdMasterEntity");
			this.AddElementFieldInfo("IdMasterEntity", "CorpId", typeof(System.Int32), false, false, false, false,  (int)IdMasterFieldIndex.CorpId, 0, 0, 10);
			this.AddElementFieldInfo("IdMasterEntity", "DivId", typeof(System.Int32), false, false, false, false,  (int)IdMasterFieldIndex.DivId, 0, 0, 10);
			this.AddElementFieldInfo("IdMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)IdMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("IdMasterEntity", "LastUsedId", typeof(System.Int32), false, false, false, false,  (int)IdMasterFieldIndex.LastUsedId, 0, 0, 10);
			this.AddElementFieldInfo("IdMasterEntity", "StoreId", typeof(System.Int32), false, false, false, false,  (int)IdMasterFieldIndex.StoreId, 0, 0, 10);
			this.AddElementFieldInfo("IdMasterEntity", "TableName", typeof(System.String), true, false, false, false,  (int)IdMasterFieldIndex.TableName, 50, 0, 0);
		}
		/// <summary>Inits ImageLibraryMasterEntity's FieldInfo objects</summary>
		private void InitImageLibraryMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ImageLibraryMasterFieldIndex), "ImageLibraryMasterEntity");
			this.AddElementFieldInfo("ImageLibraryMasterEntity", "EntId", typeof(System.Int32), false, false, false, false,  (int)ImageLibraryMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("ImageLibraryMasterEntity", "Image", typeof(System.Byte[]), false, false, false, false,  (int)ImageLibraryMasterFieldIndex.Image, 2147483647, 0, 0);
			this.AddElementFieldInfo("ImageLibraryMasterEntity", "ImageExtension", typeof(System.String), false, false, false, false,  (int)ImageLibraryMasterFieldIndex.ImageExtension, 50, 0, 0);
			this.AddElementFieldInfo("ImageLibraryMasterEntity", "ImageId", typeof(System.Int32), true, false, true, false,  (int)ImageLibraryMasterFieldIndex.ImageId, 0, 0, 10);
			this.AddElementFieldInfo("ImageLibraryMasterEntity", "ImageName", typeof(System.String), false, false, false, false,  (int)ImageLibraryMasterFieldIndex.ImageName, 128, 0, 0);
		}
		/// <summary>Inits IntlDescriptorTextJoinEntity's FieldInfo objects</summary>
		private void InitIntlDescriptorTextJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(IntlDescriptorTextJoinFieldIndex), "IntlDescriptorTextJoinEntity");
			this.AddElementFieldInfo("IntlDescriptorTextJoinEntity", "ControlId", typeof(System.Int32), true, false, false, false,  (int)IntlDescriptorTextJoinFieldIndex.ControlId, 0, 0, 10);
			this.AddElementFieldInfo("IntlDescriptorTextJoinEntity", "ControlText", typeof(System.String), false, false, false, true,  (int)IntlDescriptorTextJoinFieldIndex.ControlText, 255, 0, 0);
			this.AddElementFieldInfo("IntlDescriptorTextJoinEntity", "CustomerId", typeof(System.Int32), true, false, false, false,  (int)IntlDescriptorTextJoinFieldIndex.CustomerId, 0, 0, 10);
			this.AddElementFieldInfo("IntlDescriptorTextJoinEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)IntlDescriptorTextJoinFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("IntlDescriptorTextJoinEntity", "LanguageId", typeof(System.String), true, false, false, false,  (int)IntlDescriptorTextJoinFieldIndex.LanguageId, 9, 0, 0);
			this.AddElementFieldInfo("IntlDescriptorTextJoinEntity", "NodeId", typeof(System.Int32), true, false, false, false,  (int)IntlDescriptorTextJoinFieldIndex.NodeId, 0, 0, 10);
			this.AddElementFieldInfo("IntlDescriptorTextJoinEntity", "ObjectId", typeof(System.Int32), true, false, false, false,  (int)IntlDescriptorTextJoinFieldIndex.ObjectId, 0, 0, 10);
			this.AddElementFieldInfo("IntlDescriptorTextJoinEntity", "TranslatedFlag", typeof(System.Boolean), false, false, false, false,  (int)IntlDescriptorTextJoinFieldIndex.TranslatedFlag, 0, 0, 0);
		}
		/// <summary>Inits IpServerAttribMasterEntity's FieldInfo objects</summary>
		private void InitIpServerAttribMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(IpServerAttribMasterFieldIndex), "IpServerAttribMasterEntity");
			this.AddElementFieldInfo("IpServerAttribMasterEntity", "CurrentVersion", typeof(System.String), false, false, false, true,  (int)IpServerAttribMasterFieldIndex.CurrentVersion, 30, 0, 0);
			this.AddElementFieldInfo("IpServerAttribMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)IpServerAttribMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("IpServerAttribMasterEntity", "IpAddress", typeof(System.String), false, false, false, true,  (int)IpServerAttribMasterFieldIndex.IpAddress, 15, 0, 0);
			this.AddElementFieldInfo("IpServerAttribMasterEntity", "IpTypeCodeId", typeof(Nullable<System.Int16>), false, false, false, true,  (int)IpServerAttribMasterFieldIndex.IpTypeCodeId, 0, 0, 5);
			this.AddElementFieldInfo("IpServerAttribMasterEntity", "ServerAttribDesc", typeof(System.String), false, false, false, true,  (int)IpServerAttribMasterFieldIndex.ServerAttribDesc, 255, 0, 0);
			this.AddElementFieldInfo("IpServerAttribMasterEntity", "ServerAttribId", typeof(System.Int32), true, false, false, false,  (int)IpServerAttribMasterFieldIndex.ServerAttribId, 0, 0, 10);
			this.AddElementFieldInfo("IpServerAttribMasterEntity", "ServerHostName", typeof(System.String), false, false, false, true,  (int)IpServerAttribMasterFieldIndex.ServerHostName, 255, 0, 0);
			this.AddElementFieldInfo("IpServerAttribMasterEntity", "StoreId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)IpServerAttribMasterFieldIndex.StoreId, 0, 0, 10);
		}
		/// <summary>Inits JobcodeFuncBttnJoinEntity's FieldInfo objects</summary>
		private void InitJobcodeFuncBttnJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(JobcodeFuncBttnJoinFieldIndex), "JobcodeFuncBttnJoinEntity");
			this.AddElementFieldInfo("JobcodeFuncBttnJoinEntity", "EnabledFlag", typeof(System.Boolean), false, false, false, false,  (int)JobcodeFuncBttnJoinFieldIndex.EnabledFlag, 0, 0, 0);
			this.AddElementFieldInfo("JobcodeFuncBttnJoinEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)JobcodeFuncBttnJoinFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("JobcodeFuncBttnJoinEntity", "FuncbttnId", typeof(System.Int32), true, false, false, false,  (int)JobcodeFuncBttnJoinFieldIndex.FuncbttnId, 0, 0, 10);
			this.AddElementFieldInfo("JobcodeFuncBttnJoinEntity", "JobcodeId", typeof(System.Int32), true, false, false, false,  (int)JobcodeFuncBttnJoinFieldIndex.JobcodeId, 0, 0, 10);
		}
		/// <summary>Inits JobcodeFunctJoinEntity's FieldInfo objects</summary>
		private void InitJobcodeFunctJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(JobcodeFunctJoinFieldIndex), "JobcodeFunctJoinEntity");
			this.AddElementFieldInfo("JobcodeFunctJoinEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)JobcodeFunctJoinFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("JobcodeFunctJoinEntity", "FunctFlag", typeof(System.Boolean), false, false, false, false,  (int)JobcodeFunctJoinFieldIndex.FunctFlag, 0, 0, 0);
			this.AddElementFieldInfo("JobcodeFunctJoinEntity", "FunctId", typeof(System.Int32), true, false, false, false,  (int)JobcodeFunctJoinFieldIndex.FunctId, 0, 0, 10);
			this.AddElementFieldInfo("JobcodeFunctJoinEntity", "JobcodeId", typeof(System.Int32), true, false, false, false,  (int)JobcodeFunctJoinFieldIndex.JobcodeId, 0, 0, 10);
		}
		/// <summary>Inits JobcodeMasterEntity's FieldInfo objects</summary>
		private void InitJobcodeMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(JobcodeMasterFieldIndex), "JobcodeMasterEntity");
			this.AddElementFieldInfo("JobcodeMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)JobcodeMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("JobcodeMasterEntity", "JobcodeAbbr", typeof(System.String), false, false, false, true,  (int)JobcodeMasterFieldIndex.JobcodeAbbr, 7, 0, 0);
			this.AddElementFieldInfo("JobcodeMasterEntity", "JobcodeAbbr2", typeof(System.String), false, false, false, true,  (int)JobcodeMasterFieldIndex.JobcodeAbbr2, 7, 0, 0);
			this.AddElementFieldInfo("JobcodeMasterEntity", "JobcodeCarryoverTipsFlag", typeof(System.Boolean), false, false, false, false,  (int)JobcodeMasterFieldIndex.JobcodeCarryoverTipsFlag, 0, 0, 0);
			this.AddElementFieldInfo("JobcodeMasterEntity", "JobcodeId", typeof(System.Int32), true, false, false, false,  (int)JobcodeMasterFieldIndex.JobcodeId, 0, 0, 10);
			this.AddElementFieldInfo("JobcodeMasterEntity", "JobcodeLaborRate", typeof(Nullable<System.Decimal>), false, false, false, true,  (int)JobcodeMasterFieldIndex.JobcodeLaborRate, 0, 4, 19);
			this.AddElementFieldInfo("JobcodeMasterEntity", "JobcodeName", typeof(System.String), false, false, false, true,  (int)JobcodeMasterFieldIndex.JobcodeName, 16, 0, 0);
			this.AddElementFieldInfo("JobcodeMasterEntity", "JobcodePrintCashFlag", typeof(System.Boolean), false, false, false, false,  (int)JobcodeMasterFieldIndex.JobcodePrintCashFlag, 0, 0, 0);
			this.AddElementFieldInfo("JobcodeMasterEntity", "JobcodePrintCashierFlag", typeof(System.Boolean), false, false, false, false,  (int)JobcodeMasterFieldIndex.JobcodePrintCashierFlag, 0, 0, 0);
			this.AddElementFieldInfo("JobcodeMasterEntity", "JobcodePrintEmpIdFlag", typeof(System.Boolean), false, false, false, false,  (int)JobcodeMasterFieldIndex.JobcodePrintEmpIdFlag, 0, 0, 0);
			this.AddElementFieldInfo("JobcodeMasterEntity", "JobcodePrintServerFlag", typeof(System.Boolean), false, false, false, false,  (int)JobcodeMasterFieldIndex.JobcodePrintServerFlag, 0, 0, 0);
			this.AddElementFieldInfo("JobcodeMasterEntity", "JobcodeSecId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)JobcodeMasterFieldIndex.JobcodeSecId, 0, 0, 10);
			this.AddElementFieldInfo("JobcodeMasterEntity", "JobcodeTipPrompt", typeof(System.Boolean), false, false, false, false,  (int)JobcodeMasterFieldIndex.JobcodeTipPrompt, 0, 0, 0);
			this.AddElementFieldInfo("JobcodeMasterEntity", "RowVersion", typeof(System.Byte[]), false, false, true, false,  (int)JobcodeMasterFieldIndex.RowVersion, 2147483647, 0, 0);
			this.AddElementFieldInfo("JobcodeMasterEntity", "StoreId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)JobcodeMasterFieldIndex.StoreId, 0, 0, 10);
		}
		/// <summary>Inits KdsCategoryMasterEntity's FieldInfo objects</summary>
		private void InitKdsCategoryMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(KdsCategoryMasterFieldIndex), "KdsCategoryMasterEntity");
			this.AddElementFieldInfo("KdsCategoryMasterEntity", "DivisionId", typeof(System.Int32), false, false, false, false,  (int)KdsCategoryMasterFieldIndex.DivisionId, 0, 0, 10);
			this.AddElementFieldInfo("KdsCategoryMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)KdsCategoryMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("KdsCategoryMasterEntity", "KdsCategoryId", typeof(System.Int32), true, false, false, false,  (int)KdsCategoryMasterFieldIndex.KdsCategoryId, 0, 0, 10);
			this.AddElementFieldInfo("KdsCategoryMasterEntity", "KdsCategoryName", typeof(System.String), false, false, false, false,  (int)KdsCategoryMasterFieldIndex.KdsCategoryName, 40, 0, 0);
		}
		/// <summary>Inits KdsVideoMasterEntity's FieldInfo objects</summary>
		private void InitKdsVideoMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(KdsVideoMasterFieldIndex), "KdsVideoMasterEntity");
			this.AddElementFieldInfo("KdsVideoMasterEntity", "DivisionId", typeof(System.Int32), false, false, false, false,  (int)KdsVideoMasterFieldIndex.DivisionId, 0, 0, 10);
			this.AddElementFieldInfo("KdsVideoMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)KdsVideoMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("KdsVideoMasterEntity", "KdsVideoId", typeof(System.Int32), true, false, false, false,  (int)KdsVideoMasterFieldIndex.KdsVideoId, 0, 0, 10);
			this.AddElementFieldInfo("KdsVideoMasterEntity", "KdsVideoName", typeof(System.String), false, false, false, false,  (int)KdsVideoMasterFieldIndex.KdsVideoName, 50, 0, 0);
		}
		/// <summary>Inits KpOptionGrpCopyJoinEntity's FieldInfo objects</summary>
		private void InitKpOptionGrpCopyJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(KpOptionGrpCopyJoinFieldIndex), "KpOptionGrpCopyJoinEntity");
			this.AddElementFieldInfo("KpOptionGrpCopyJoinEntity", "BeepFlag", typeof(System.Boolean), false, false, false, false,  (int)KpOptionGrpCopyJoinFieldIndex.BeepFlag, 0, 0, 0);
			this.AddElementFieldInfo("KpOptionGrpCopyJoinEntity", "CutCopyFlag", typeof(System.Boolean), false, false, false, false,  (int)KpOptionGrpCopyJoinFieldIndex.CutCopyFlag, 0, 0, 0);
			this.AddElementFieldInfo("KpOptionGrpCopyJoinEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)KpOptionGrpCopyJoinFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("KpOptionGrpCopyJoinEntity", "ExtraCodeId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)KpOptionGrpCopyJoinFieldIndex.ExtraCodeId, 0, 0, 10);
			this.AddElementFieldInfo("KpOptionGrpCopyJoinEntity", "KpCopiesSeq", typeof(System.Int16), true, false, false, false,  (int)KpOptionGrpCopyJoinFieldIndex.KpCopiesSeq, 0, 0, 5);
			this.AddElementFieldInfo("KpOptionGrpCopyJoinEntity", "KpOptionGrpId", typeof(System.Int32), true, false, false, false,  (int)KpOptionGrpCopyJoinFieldIndex.KpOptionGrpId, 0, 0, 10);
			this.AddElementFieldInfo("KpOptionGrpCopyJoinEntity", "NumLinesAfter", typeof(Nullable<System.Int16>), false, false, false, true,  (int)KpOptionGrpCopyJoinFieldIndex.NumLinesAfter, 0, 0, 5);
			this.AddElementFieldInfo("KpOptionGrpCopyJoinEntity", "NumLinesBefore", typeof(Nullable<System.Int16>), false, false, false, true,  (int)KpOptionGrpCopyJoinFieldIndex.NumLinesBefore, 0, 0, 5);
			this.AddElementFieldInfo("KpOptionGrpCopyJoinEntity", "PrintHeaderFlag", typeof(System.Boolean), false, false, false, false,  (int)KpOptionGrpCopyJoinFieldIndex.PrintHeaderFlag, 0, 0, 0);
			this.AddElementFieldInfo("KpOptionGrpCopyJoinEntity", "SortCodeId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)KpOptionGrpCopyJoinFieldIndex.SortCodeId, 0, 0, 10);
		}
		/// <summary>Inits KpOptionGrpFormatJoinEntity's FieldInfo objects</summary>
		private void InitKpOptionGrpFormatJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(KpOptionGrpFormatJoinFieldIndex), "KpOptionGrpFormatJoinEntity");
			this.AddElementFieldInfo("KpOptionGrpFormatJoinEntity", "AddLineFlag", typeof(System.Boolean), false, false, false, false,  (int)KpOptionGrpFormatJoinFieldIndex.AddLineFlag, 0, 0, 0);
			this.AddElementFieldInfo("KpOptionGrpFormatJoinEntity", "CharsizeCodeId", typeof(Nullable<System.Int16>), false, false, false, true,  (int)KpOptionGrpFormatJoinFieldIndex.CharsizeCodeId, 0, 0, 5);
			this.AddElementFieldInfo("KpOptionGrpFormatJoinEntity", "ColorCodeId", typeof(Nullable<System.Int16>), false, false, false, true,  (int)KpOptionGrpFormatJoinFieldIndex.ColorCodeId, 0, 0, 5);
			this.AddElementFieldInfo("KpOptionGrpFormatJoinEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)KpOptionGrpFormatJoinFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("KpOptionGrpFormatJoinEntity", "FormatLabel", typeof(System.String), false, false, false, true,  (int)KpOptionGrpFormatJoinFieldIndex.FormatLabel, 30, 0, 0);
			this.AddElementFieldInfo("KpOptionGrpFormatJoinEntity", "KpFormatId", typeof(System.Int32), true, false, false, false,  (int)KpOptionGrpFormatJoinFieldIndex.KpFormatId, 0, 0, 10);
			this.AddElementFieldInfo("KpOptionGrpFormatJoinEntity", "KpOptionGrpId", typeof(System.Int32), true, false, false, false,  (int)KpOptionGrpFormatJoinFieldIndex.KpOptionGrpId, 0, 0, 10);
			this.AddElementFieldInfo("KpOptionGrpFormatJoinEntity", "PrintFlag", typeof(System.Boolean), false, false, false, false,  (int)KpOptionGrpFormatJoinFieldIndex.PrintFlag, 0, 0, 0);
		}
		/// <summary>Inits KpOptionGrpMasterEntity's FieldInfo objects</summary>
		private void InitKpOptionGrpMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(KpOptionGrpMasterFieldIndex), "KpOptionGrpMasterEntity");
			this.AddElementFieldInfo("KpOptionGrpMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)KpOptionGrpMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("KpOptionGrpMasterEntity", "KpOptionGrpAbbr1", typeof(System.String), false, false, false, true,  (int)KpOptionGrpMasterFieldIndex.KpOptionGrpAbbr1, 7, 0, 0);
			this.AddElementFieldInfo("KpOptionGrpMasterEntity", "KpOptionGrpAbbr2", typeof(System.String), false, false, false, true,  (int)KpOptionGrpMasterFieldIndex.KpOptionGrpAbbr2, 7, 0, 0);
			this.AddElementFieldInfo("KpOptionGrpMasterEntity", "KpOptionGrpId", typeof(System.Int32), true, false, false, false,  (int)KpOptionGrpMasterFieldIndex.KpOptionGrpId, 0, 0, 10);
			this.AddElementFieldInfo("KpOptionGrpMasterEntity", "KpOptionGrpName", typeof(System.String), false, false, false, true,  (int)KpOptionGrpMasterFieldIndex.KpOptionGrpName, 50, 0, 0);
			this.AddElementFieldInfo("KpOptionGrpMasterEntity", "StoreId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)KpOptionGrpMasterFieldIndex.StoreId, 0, 0, 10);
		}
		/// <summary>Inits KpPrinterMasterEntity's FieldInfo objects</summary>
		private void InitKpPrinterMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(KpPrinterMasterFieldIndex), "KpPrinterMasterEntity");
			this.AddElementFieldInfo("KpPrinterMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)KpPrinterMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("KpPrinterMasterEntity", "KpPrinterId", typeof(System.Int32), true, false, false, false,  (int)KpPrinterMasterFieldIndex.KpPrinterId, 0, 0, 10);
			this.AddElementFieldInfo("KpPrinterMasterEntity", "KpPrinterName", typeof(System.String), false, false, false, true,  (int)KpPrinterMasterFieldIndex.KpPrinterName, 50, 0, 0);
			this.AddElementFieldInfo("KpPrinterMasterEntity", "StoreId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)KpPrinterMasterFieldIndex.StoreId, 0, 0, 10);
		}
		/// <summary>Inits KpRouteMasterEntity's FieldInfo objects</summary>
		private void InitKpRouteMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(KpRouteMasterFieldIndex), "KpRouteMasterEntity");
			this.AddElementFieldInfo("KpRouteMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)KpRouteMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("KpRouteMasterEntity", "KpRouteId", typeof(System.Int32), true, false, false, false,  (int)KpRouteMasterFieldIndex.KpRouteId, 0, 0, 10);
			this.AddElementFieldInfo("KpRouteMasterEntity", "KpRouteName", typeof(System.String), false, false, false, false,  (int)KpRouteMasterFieldIndex.KpRouteName, 50, 0, 0);
			this.AddElementFieldInfo("KpRouteMasterEntity", "StoreId", typeof(System.Int32), false, false, false, false,  (int)KpRouteMasterFieldIndex.StoreId, 0, 0, 10);
		}
		/// <summary>Inits KpRoutePrinterJoinEntity's FieldInfo objects</summary>
		private void InitKpRoutePrinterJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(KpRoutePrinterJoinFieldIndex), "KpRoutePrinterJoinEntity");
			this.AddElementFieldInfo("KpRoutePrinterJoinEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)KpRoutePrinterJoinFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("KpRoutePrinterJoinEntity", "KpDeviceId", typeof(System.Int32), false, false, false, false,  (int)KpRoutePrinterJoinFieldIndex.KpDeviceId, 0, 0, 10);
			this.AddElementFieldInfo("KpRoutePrinterJoinEntity", "KpPrinterId", typeof(System.Int32), true, false, false, false,  (int)KpRoutePrinterJoinFieldIndex.KpPrinterId, 0, 0, 10);
			this.AddElementFieldInfo("KpRoutePrinterJoinEntity", "KpRouteId", typeof(System.Int32), true, false, false, false,  (int)KpRoutePrinterJoinFieldIndex.KpRouteId, 0, 0, 10);
		}
		/// <summary>Inits MachineOptionsMasterEntity's FieldInfo objects</summary>
		private void InitMachineOptionsMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(MachineOptionsMasterFieldIndex), "MachineOptionsMasterEntity");
			this.AddElementFieldInfo("MachineOptionsMasterEntity", "OptionName", typeof(System.String), true, false, false, false,  (int)MachineOptionsMasterFieldIndex.OptionName, 50, 0, 0);
			this.AddElementFieldInfo("MachineOptionsMasterEntity", "OptionValue", typeof(System.String), false, false, false, true,  (int)MachineOptionsMasterFieldIndex.OptionValue, 10, 0, 0);
		}
		/// <summary>Inits MealPeriodMasterEntity's FieldInfo objects</summary>
		private void InitMealPeriodMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(MealPeriodMasterFieldIndex), "MealPeriodMasterEntity");
			this.AddElementFieldInfo("MealPeriodMasterEntity", "DefaultCheckTypeId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)MealPeriodMasterFieldIndex.DefaultCheckTypeId, 0, 0, 10);
			this.AddElementFieldInfo("MealPeriodMasterEntity", "DefaultPriceLevelId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)MealPeriodMasterFieldIndex.DefaultPriceLevelId, 0, 0, 10);
			this.AddElementFieldInfo("MealPeriodMasterEntity", "EntertainmentFlag", typeof(System.Boolean), false, false, false, false,  (int)MealPeriodMasterFieldIndex.EntertainmentFlag, 0, 0, 0);
			this.AddElementFieldInfo("MealPeriodMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)MealPeriodMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("MealPeriodMasterEntity", "MealPeriodAbbr1", typeof(System.String), false, false, false, true,  (int)MealPeriodMasterFieldIndex.MealPeriodAbbr1, 7, 0, 0);
			this.AddElementFieldInfo("MealPeriodMasterEntity", "MealPeriodAbbr2", typeof(System.String), false, false, false, true,  (int)MealPeriodMasterFieldIndex.MealPeriodAbbr2, 7, 0, 0);
			this.AddElementFieldInfo("MealPeriodMasterEntity", "MealPeriodId", typeof(System.Int32), true, false, false, false,  (int)MealPeriodMasterFieldIndex.MealPeriodId, 0, 0, 10);
			this.AddElementFieldInfo("MealPeriodMasterEntity", "MealPeriodName", typeof(System.String), false, false, false, true,  (int)MealPeriodMasterFieldIndex.MealPeriodName, 16, 0, 0);
			this.AddElementFieldInfo("MealPeriodMasterEntity", "MealPeriodSecId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)MealPeriodMasterFieldIndex.MealPeriodSecId, 0, 0, 10);
			this.AddElementFieldInfo("MealPeriodMasterEntity", "ReceiptCode", typeof(System.String), false, false, false, true,  (int)MealPeriodMasterFieldIndex.ReceiptCode, 3, 0, 0);
			this.AddElementFieldInfo("MealPeriodMasterEntity", "RowVersion", typeof(System.Byte[]), false, false, true, false,  (int)MealPeriodMasterFieldIndex.RowVersion, 2147483647, 0, 0);
			this.AddElementFieldInfo("MealPeriodMasterEntity", "StoreId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)MealPeriodMasterFieldIndex.StoreId, 0, 0, 10);
		}
		/// <summary>Inits MembershipMasterEntity's FieldInfo objects</summary>
		private void InitMembershipMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(MembershipMasterFieldIndex), "MembershipMasterEntity");
			this.AddElementFieldInfo("MembershipMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)MembershipMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("MembershipMasterEntity", "MembershipCode", typeof(System.String), false, false, false, false,  (int)MembershipMasterFieldIndex.MembershipCode, 50, 0, 0);
			this.AddElementFieldInfo("MembershipMasterEntity", "MembershipId", typeof(System.Int32), true, false, false, false,  (int)MembershipMasterFieldIndex.MembershipId, 0, 0, 10);
			this.AddElementFieldInfo("MembershipMasterEntity", "MembershipName", typeof(System.String), false, false, false, false,  (int)MembershipMasterFieldIndex.MembershipName, 50, 0, 0);
		}
		/// <summary>Inits MembershipProfitCenterJoinEntity's FieldInfo objects</summary>
		private void InitMembershipProfitCenterJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(MembershipProfitCenterJoinFieldIndex), "MembershipProfitCenterJoinEntity");
			this.AddElementFieldInfo("MembershipProfitCenterJoinEntity", "CheckTypeId", typeof(System.Int32), false, false, false, false,  (int)MembershipProfitCenterJoinFieldIndex.CheckTypeId, 0, 0, 10);
			this.AddElementFieldInfo("MembershipProfitCenterJoinEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)MembershipProfitCenterJoinFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("MembershipProfitCenterJoinEntity", "MembershipId", typeof(System.Int32), true, false, false, false,  (int)MembershipProfitCenterJoinFieldIndex.MembershipId, 0, 0, 10);
			this.AddElementFieldInfo("MembershipProfitCenterJoinEntity", "ProfitCenterId", typeof(System.Int32), true, false, false, false,  (int)MembershipProfitCenterJoinFieldIndex.ProfitCenterId, 0, 0, 10);
		}
		/// <summary>Inits MenuBttnObjJoinEntity's FieldInfo objects</summary>
		private void InitMenuBttnObjJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(MenuBttnObjJoinFieldIndex), "MenuBttnObjJoinEntity");
			this.AddElementFieldInfo("MenuBttnObjJoinEntity", "ButtonBackcolor", typeof(Nullable<System.Int32>), false, false, false, true,  (int)MenuBttnObjJoinFieldIndex.ButtonBackcolor, 0, 0, 10);
			this.AddElementFieldInfo("MenuBttnObjJoinEntity", "ButtonBorderStyle", typeof(Nullable<System.Int32>), false, false, false, true,  (int)MenuBttnObjJoinFieldIndex.ButtonBorderStyle, 0, 0, 10);
			this.AddElementFieldInfo("MenuBttnObjJoinEntity", "ButtonColumn", typeof(Nullable<System.Int32>), false, false, false, true,  (int)MenuBttnObjJoinFieldIndex.ButtonColumn, 0, 0, 10);
			this.AddElementFieldInfo("MenuBttnObjJoinEntity", "ButtonFontId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)MenuBttnObjJoinFieldIndex.ButtonFontId, 0, 0, 10);
			this.AddElementFieldInfo("MenuBttnObjJoinEntity", "ButtonForecolor", typeof(Nullable<System.Int32>), false, false, false, true,  (int)MenuBttnObjJoinFieldIndex.ButtonForecolor, 0, 0, 10);
			this.AddElementFieldInfo("MenuBttnObjJoinEntity", "ButtonHeight", typeof(Nullable<System.Int16>), false, false, false, true,  (int)MenuBttnObjJoinFieldIndex.ButtonHeight, 0, 0, 5);
			this.AddElementFieldInfo("MenuBttnObjJoinEntity", "ButtonId", typeof(System.Int32), true, false, false, false,  (int)MenuBttnObjJoinFieldIndex.ButtonId, 0, 0, 10);
			this.AddElementFieldInfo("MenuBttnObjJoinEntity", "ButtonImageId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)MenuBttnObjJoinFieldIndex.ButtonImageId, 0, 0, 10);
			this.AddElementFieldInfo("MenuBttnObjJoinEntity", "ButtonNextMenuId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)MenuBttnObjJoinFieldIndex.ButtonNextMenuId, 0, 0, 10);
			this.AddElementFieldInfo("MenuBttnObjJoinEntity", "ButtonRow", typeof(Nullable<System.Int32>), false, false, false, true,  (int)MenuBttnObjJoinFieldIndex.ButtonRow, 0, 0, 10);
			this.AddElementFieldInfo("MenuBttnObjJoinEntity", "ButtonShapeId", typeof(Nullable<System.Int16>), false, false, false, true,  (int)MenuBttnObjJoinFieldIndex.ButtonShapeId, 0, 0, 5);
			this.AddElementFieldInfo("MenuBttnObjJoinEntity", "ButtonWidth", typeof(Nullable<System.Int16>), false, false, false, true,  (int)MenuBttnObjJoinFieldIndex.ButtonWidth, 0, 0, 5);
			this.AddElementFieldInfo("MenuBttnObjJoinEntity", "DisplayImageFlag", typeof(System.Boolean), false, false, false, false,  (int)MenuBttnObjJoinFieldIndex.DisplayImageFlag, 0, 0, 0);
			this.AddElementFieldInfo("MenuBttnObjJoinEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)MenuBttnObjJoinFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("MenuBttnObjJoinEntity", "ErrorCodeId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)MenuBttnObjJoinFieldIndex.ErrorCodeId, 0, 0, 10);
			this.AddElementFieldInfo("MenuBttnObjJoinEntity", "MenuId", typeof(System.Int32), true, false, false, false,  (int)MenuBttnObjJoinFieldIndex.MenuId, 0, 0, 10);
			this.AddElementFieldInfo("MenuBttnObjJoinEntity", "ObjectId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)MenuBttnObjJoinFieldIndex.ObjectId, 0, 0, 10);
			this.AddElementFieldInfo("MenuBttnObjJoinEntity", "ObjectTypeId", typeof(System.Int32), false, false, false, false,  (int)MenuBttnObjJoinFieldIndex.ObjectTypeId, 0, 0, 10);
			this.AddElementFieldInfo("MenuBttnObjJoinEntity", "ScreenId", typeof(System.Int32), true, false, false, false,  (int)MenuBttnObjJoinFieldIndex.ScreenId, 0, 0, 10);
			this.AddElementFieldInfo("MenuBttnObjJoinEntity", "StoreCreatedId", typeof(System.Int32), true, false, false, false,  (int)MenuBttnObjJoinFieldIndex.StoreCreatedId, 0, 0, 10);
			this.AddElementFieldInfo("MenuBttnObjJoinEntity", "TerminalTypeId", typeof(System.Int32), true, false, false, false,  (int)MenuBttnObjJoinFieldIndex.TerminalTypeId, 0, 0, 10);
		}
		/// <summary>Inits MenuItemGroupMasterEntity's FieldInfo objects</summary>
		private void InitMenuItemGroupMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(MenuItemGroupMasterFieldIndex), "MenuItemGroupMasterEntity");
			this.AddElementFieldInfo("MenuItemGroupMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)MenuItemGroupMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("MenuItemGroupMasterEntity", "MenuItemGroupId", typeof(System.Int32), true, false, false, false,  (int)MenuItemGroupMasterFieldIndex.MenuItemGroupId, 0, 0, 10);
			this.AddElementFieldInfo("MenuItemGroupMasterEntity", "MenuItemGroupName", typeof(System.String), false, false, false, true,  (int)MenuItemGroupMasterFieldIndex.MenuItemGroupName, 50, 0, 0);
			this.AddElementFieldInfo("MenuItemGroupMasterEntity", "StoreId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)MenuItemGroupMasterFieldIndex.StoreId, 0, 0, 10);
		}
		/// <summary>Inits MenuItemMasterEntity's FieldInfo objects</summary>
		private void InitMenuItemMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(MenuItemMasterFieldIndex), "MenuItemMasterEntity");
			this.AddElementFieldInfo("MenuItemMasterEntity", "BargunId", typeof(Nullable<System.Int16>), false, false, false, true,  (int)MenuItemMasterFieldIndex.BargunId, 0, 0, 5);
			this.AddElementFieldInfo("MenuItemMasterEntity", "Covers", typeof(System.Int16), false, false, false, false,  (int)MenuItemMasterFieldIndex.Covers, 0, 0, 5);
			this.AddElementFieldInfo("MenuItemMasterEntity", "DataControlGroupId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)MenuItemMasterFieldIndex.DataControlGroupId, 0, 0, 10);
			this.AddElementFieldInfo("MenuItemMasterEntity", "DefaultImageId", typeof(System.Int32), false, false, false, false,  (int)MenuItemMasterFieldIndex.DefaultImageId, 0, 0, 10);
			this.AddElementFieldInfo("MenuItemMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)MenuItemMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("MenuItemMasterEntity", "KdsCategoryId", typeof(System.Int32), false, false, false, false,  (int)MenuItemMasterFieldIndex.KdsCategoryId, 0, 0, 10);
			this.AddElementFieldInfo("MenuItemMasterEntity", "KdsCookTime", typeof(System.Int32), false, false, false, false,  (int)MenuItemMasterFieldIndex.KdsCookTime, 0, 0, 10);
			this.AddElementFieldInfo("MenuItemMasterEntity", "KdsVideoId", typeof(System.Int32), false, false, false, false,  (int)MenuItemMasterFieldIndex.KdsVideoId, 0, 0, 10);
			this.AddElementFieldInfo("MenuItemMasterEntity", "KdsVideoLabel", typeof(System.String), false, false, false, true,  (int)MenuItemMasterFieldIndex.KdsVideoLabel, 20, 0, 0);
			this.AddElementFieldInfo("MenuItemMasterEntity", "MenuItemAbbr1", typeof(System.String), false, false, false, true,  (int)MenuItemMasterFieldIndex.MenuItemAbbr1, 20, 0, 0);
			this.AddElementFieldInfo("MenuItemMasterEntity", "MenuItemAbbr2", typeof(System.String), false, false, false, true,  (int)MenuItemMasterFieldIndex.MenuItemAbbr2, 20, 0, 0);
			this.AddElementFieldInfo("MenuItemMasterEntity", "MenuItemGroupId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)MenuItemMasterFieldIndex.MenuItemGroupId, 0, 0, 10);
			this.AddElementFieldInfo("MenuItemMasterEntity", "MenuItemId", typeof(System.Int32), true, false, false, false,  (int)MenuItemMasterFieldIndex.MenuItemId, 0, 0, 10);
			this.AddElementFieldInfo("MenuItemMasterEntity", "MenuItemName", typeof(System.String), false, false, false, true,  (int)MenuItemMasterFieldIndex.MenuItemName, 50, 0, 0);
			this.AddElementFieldInfo("MenuItemMasterEntity", "MiCostAmt", typeof(Nullable<System.Decimal>), false, false, false, true,  (int)MenuItemMasterFieldIndex.MiCostAmt, 0, 4, 19);
			this.AddElementFieldInfo("MenuItemMasterEntity", "MiDiscountableFlag", typeof(System.Boolean), false, false, false, false,  (int)MenuItemMasterFieldIndex.MiDiscountableFlag, 0, 0, 0);
			this.AddElementFieldInfo("MenuItemMasterEntity", "MiEmpDiscountableFlag", typeof(System.Boolean), false, false, false, false,  (int)MenuItemMasterFieldIndex.MiEmpDiscountableFlag, 0, 0, 0);
			this.AddElementFieldInfo("MenuItemMasterEntity", "MiKpLabel", typeof(System.String), false, false, false, true,  (int)MenuItemMasterFieldIndex.MiKpLabel, 16, 0, 0);
			this.AddElementFieldInfo("MenuItemMasterEntity", "MiNotActiveFlag", typeof(System.Boolean), false, false, false, false,  (int)MenuItemMasterFieldIndex.MiNotActiveFlag, 0, 0, 0);
			this.AddElementFieldInfo("MenuItemMasterEntity", "MiOpenPricePrompt", typeof(System.Boolean), false, false, false, false,  (int)MenuItemMasterFieldIndex.MiOpenPricePrompt, 0, 0, 0);
			this.AddElementFieldInfo("MenuItemMasterEntity", "MiPriceOverrideFlag", typeof(System.Boolean), false, false, false, false,  (int)MenuItemMasterFieldIndex.MiPriceOverrideFlag, 0, 0, 0);
			this.AddElementFieldInfo("MenuItemMasterEntity", "MiPrintFlag", typeof(System.Boolean), false, false, false, false,  (int)MenuItemMasterFieldIndex.MiPrintFlag, 0, 0, 0);
			this.AddElementFieldInfo("MenuItemMasterEntity", "MiReceiptLabel", typeof(System.String), false, false, false, true,  (int)MenuItemMasterFieldIndex.MiReceiptLabel, 16, 0, 0);
			this.AddElementFieldInfo("MenuItemMasterEntity", "MiSecId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)MenuItemMasterFieldIndex.MiSecId, 0, 0, 10);
			this.AddElementFieldInfo("MenuItemMasterEntity", "MiTaxInclFlag", typeof(System.Boolean), false, false, false, false,  (int)MenuItemMasterFieldIndex.MiTaxInclFlag, 0, 0, 0);
			this.AddElementFieldInfo("MenuItemMasterEntity", "MiVoidableFlag", typeof(System.Boolean), false, false, false, false,  (int)MenuItemMasterFieldIndex.MiVoidableFlag, 0, 0, 0);
			this.AddElementFieldInfo("MenuItemMasterEntity", "MiWeightFlag", typeof(System.Boolean), false, false, false, false,  (int)MenuItemMasterFieldIndex.MiWeightFlag, 0, 0, 0);
			this.AddElementFieldInfo("MenuItemMasterEntity", "MiWeightTare", typeof(Nullable<System.Double>), false, false, false, true,  (int)MenuItemMasterFieldIndex.MiWeightTare, 0, 0, 38);
			this.AddElementFieldInfo("MenuItemMasterEntity", "OpenModifierFlag", typeof(System.Boolean), false, false, false, false,  (int)MenuItemMasterFieldIndex.OpenModifierFlag, 0, 0, 0);
			this.AddElementFieldInfo("MenuItemMasterEntity", "ProdClassId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)MenuItemMasterFieldIndex.ProdClassId, 0, 0, 10);
			this.AddElementFieldInfo("MenuItemMasterEntity", "RevCatId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)MenuItemMasterFieldIndex.RevCatId, 0, 0, 10);
			this.AddElementFieldInfo("MenuItemMasterEntity", "RowVersion", typeof(System.Byte[]), false, false, true, false,  (int)MenuItemMasterFieldIndex.RowVersion, 2147483647, 0, 0);
			this.AddElementFieldInfo("MenuItemMasterEntity", "RptCatId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)MenuItemMasterFieldIndex.RptCatId, 0, 0, 10);
			this.AddElementFieldInfo("MenuItemMasterEntity", "SkuNo", typeof(System.String), false, false, false, true,  (int)MenuItemMasterFieldIndex.SkuNo, 30, 0, 0);
			this.AddElementFieldInfo("MenuItemMasterEntity", "StoreCreatedId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)MenuItemMasterFieldIndex.StoreCreatedId, 0, 0, 10);
			this.AddElementFieldInfo("MenuItemMasterEntity", "StoreId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)MenuItemMasterFieldIndex.StoreId, 0, 0, 10);
			this.AddElementFieldInfo("MenuItemMasterEntity", "TaxGrpId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)MenuItemMasterFieldIndex.TaxGrpId, 0, 0, 10);
		}
		/// <summary>Inits MenuMasterEntity's FieldInfo objects</summary>
		private void InitMenuMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(MenuMasterFieldIndex), "MenuMasterEntity");
			this.AddElementFieldInfo("MenuMasterEntity", "AllowButtonOverlapFlag", typeof(Nullable<System.Boolean>), false, false, false, true,  (int)MenuMasterFieldIndex.AllowButtonOverlapFlag, 0, 0, 0);
			this.AddElementFieldInfo("MenuMasterEntity", "ButtonPadding", typeof(Nullable<System.Int32>), false, false, false, true,  (int)MenuMasterFieldIndex.ButtonPadding, 0, 0, 10);
			this.AddElementFieldInfo("MenuMasterEntity", "DataControlGroupId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)MenuMasterFieldIndex.DataControlGroupId, 0, 0, 10);
			this.AddElementFieldInfo("MenuMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)MenuMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("MenuMasterEntity", "GridSize", typeof(Nullable<System.Int32>), false, false, false, true,  (int)MenuMasterFieldIndex.GridSize, 0, 0, 10);
			this.AddElementFieldInfo("MenuMasterEntity", "MenuAbbr1", typeof(System.String), false, false, false, true,  (int)MenuMasterFieldIndex.MenuAbbr1, 7, 0, 0);
			this.AddElementFieldInfo("MenuMasterEntity", "MenuAbbr2", typeof(System.String), false, false, false, true,  (int)MenuMasterFieldIndex.MenuAbbr2, 7, 0, 0);
			this.AddElementFieldInfo("MenuMasterEntity", "MenuId", typeof(System.Int32), true, false, false, false,  (int)MenuMasterFieldIndex.MenuId, 0, 0, 10);
			this.AddElementFieldInfo("MenuMasterEntity", "MenuName", typeof(System.String), false, false, false, true,  (int)MenuMasterFieldIndex.MenuName, 20, 0, 0);
			this.AddElementFieldInfo("MenuMasterEntity", "ScreenSizeId", typeof(Nullable<System.Int16>), false, false, false, true,  (int)MenuMasterFieldIndex.ScreenSizeId, 0, 0, 5);
			this.AddElementFieldInfo("MenuMasterEntity", "ScreenTypeId", typeof(Nullable<System.Int16>), false, false, false, true,  (int)MenuMasterFieldIndex.ScreenTypeId, 0, 0, 5);
			this.AddElementFieldInfo("MenuMasterEntity", "SnapToGridFlag", typeof(Nullable<System.Boolean>), false, false, false, true,  (int)MenuMasterFieldIndex.SnapToGridFlag, 0, 0, 0);
			this.AddElementFieldInfo("MenuMasterEntity", "StoreId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)MenuMasterFieldIndex.StoreId, 0, 0, 10);
			this.AddElementFieldInfo("MenuMasterEntity", "SystemScreenFlag", typeof(Nullable<System.Boolean>), false, false, false, true,  (int)MenuMasterFieldIndex.SystemScreenFlag, 0, 0, 0);
			this.AddElementFieldInfo("MenuMasterEntity", "ThemeId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)MenuMasterFieldIndex.ThemeId, 0, 0, 10);
		}
		/// <summary>Inits MenuMigrpJoinEntity's FieldInfo objects</summary>
		private void InitMenuMigrpJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(MenuMigrpJoinFieldIndex), "MenuMigrpJoinEntity");
			this.AddElementFieldInfo("MenuMigrpJoinEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)MenuMigrpJoinFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("MenuMigrpJoinEntity", "MenuId", typeof(System.Int32), true, false, false, false,  (int)MenuMigrpJoinFieldIndex.MenuId, 0, 0, 10);
			this.AddElementFieldInfo("MenuMigrpJoinEntity", "MenuItemGroupId", typeof(System.Int32), true, false, false, false,  (int)MenuMigrpJoinFieldIndex.MenuItemGroupId, 0, 0, 10);
		}
		/// <summary>Inits MiChoiceGrpJoinEntity's FieldInfo objects</summary>
		private void InitMiChoiceGrpJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(MiChoiceGrpJoinFieldIndex), "MiChoiceGrpJoinEntity");
			this.AddElementFieldInfo("MiChoiceGrpJoinEntity", "ChoiceGroupId", typeof(System.Int32), false, false, false, false,  (int)MiChoiceGrpJoinFieldIndex.ChoiceGroupId, 0, 0, 10);
			this.AddElementFieldInfo("MiChoiceGrpJoinEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)MiChoiceGrpJoinFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("MiChoiceGrpJoinEntity", "MenuItemId", typeof(System.Int32), true, false, false, false,  (int)MiChoiceGrpJoinFieldIndex.MenuItemId, 0, 0, 10);
			this.AddElementFieldInfo("MiChoiceGrpJoinEntity", "MiCgJoinId", typeof(System.Int32), true, false, true, false,  (int)MiChoiceGrpJoinFieldIndex.MiCgJoinId, 0, 0, 10);
			this.AddElementFieldInfo("MiChoiceGrpJoinEntity", "MiChoicegrpSeq", typeof(Nullable<System.Int16>), false, false, false, true,  (int)MiChoiceGrpJoinFieldIndex.MiChoicegrpSeq, 0, 0, 5);
		}
		/// <summary>Inits MiKpprinterJoinEntity's FieldInfo objects</summary>
		private void InitMiKpprinterJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(MiKpprinterJoinFieldIndex), "MiKpprinterJoinEntity");
			this.AddElementFieldInfo("MiKpprinterJoinEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)MiKpprinterJoinFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("MiKpprinterJoinEntity", "KpPrinterId", typeof(System.Int32), true, false, false, false,  (int)MiKpprinterJoinFieldIndex.KpPrinterId, 0, 0, 10);
			this.AddElementFieldInfo("MiKpprinterJoinEntity", "MenuItemId", typeof(System.Int32), true, false, false, false,  (int)MiKpprinterJoinFieldIndex.MenuItemId, 0, 0, 10);
			this.AddElementFieldInfo("MiKpprinterJoinEntity", "PrimaryPrinterFlag", typeof(System.Boolean), false, false, false, false,  (int)MiKpprinterJoinFieldIndex.PrimaryPrinterFlag, 0, 0, 0);
		}
		/// <summary>Inits MiPriceJoinEntity's FieldInfo objects</summary>
		private void InitMiPriceJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(MiPriceJoinFieldIndex), "MiPriceJoinEntity");
			this.AddElementFieldInfo("MiPriceJoinEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)MiPriceJoinFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("MiPriceJoinEntity", "MenuItemId", typeof(System.Int32), true, false, false, false,  (int)MiPriceJoinFieldIndex.MenuItemId, 0, 0, 10);
			this.AddElementFieldInfo("MiPriceJoinEntity", "PriceLast", typeof(Nullable<System.Decimal>), false, false, false, true,  (int)MiPriceJoinFieldIndex.PriceLast, 0, 4, 19);
			this.AddElementFieldInfo("MiPriceJoinEntity", "PriceLevelId", typeof(System.Int32), true, false, false, false,  (int)MiPriceJoinFieldIndex.PriceLevelId, 0, 0, 10);
			this.AddElementFieldInfo("MiPriceJoinEntity", "PriceNet", typeof(Nullable<System.Decimal>), false, false, false, true,  (int)MiPriceJoinFieldIndex.PriceNet, 0, 4, 19);
			this.AddElementFieldInfo("MiPriceJoinEntity", "StoreCreatedId", typeof(System.Int32), true, false, false, false,  (int)MiPriceJoinFieldIndex.StoreCreatedId, 0, 0, 10);
		}
		/// <summary>Inits MiSkuJoinEntity's FieldInfo objects</summary>
		private void InitMiSkuJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(MiSkuJoinFieldIndex), "MiSkuJoinEntity");
			this.AddElementFieldInfo("MiSkuJoinEntity", "Description", typeof(System.String), false, false, false, false,  (int)MiSkuJoinFieldIndex.Description, 50, 0, 0);
			this.AddElementFieldInfo("MiSkuJoinEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)MiSkuJoinFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("MiSkuJoinEntity", "MenuItemId", typeof(System.Int32), true, false, false, false,  (int)MiSkuJoinFieldIndex.MenuItemId, 0, 0, 10);
			this.AddElementFieldInfo("MiSkuJoinEntity", "SkuGenId", typeof(System.Int32), true, false, true, false,  (int)MiSkuJoinFieldIndex.SkuGenId, 0, 0, 10);
			this.AddElementFieldInfo("MiSkuJoinEntity", "SkuNo", typeof(System.String), false, false, false, false,  (int)MiSkuJoinFieldIndex.SkuNo, 30, 0, 0);
		}
		/// <summary>Inits ModifierKpprinterJoinEntity's FieldInfo objects</summary>
		private void InitModifierKpprinterJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ModifierKpprinterJoinFieldIndex), "ModifierKpprinterJoinEntity");
			this.AddElementFieldInfo("ModifierKpprinterJoinEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)ModifierKpprinterJoinFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("ModifierKpprinterJoinEntity", "KpPrinterId", typeof(System.Int32), true, false, false, false,  (int)ModifierKpprinterJoinFieldIndex.KpPrinterId, 0, 0, 10);
			this.AddElementFieldInfo("ModifierKpprinterJoinEntity", "ModifierId", typeof(System.Int32), true, false, false, false,  (int)ModifierKpprinterJoinFieldIndex.ModifierId, 0, 0, 10);
			this.AddElementFieldInfo("ModifierKpprinterJoinEntity", "PrimaryPrinterFlag", typeof(System.Boolean), false, false, false, false,  (int)ModifierKpprinterJoinFieldIndex.PrimaryPrinterFlag, 0, 0, 0);
		}
		/// <summary>Inits ModifierMasterEntity's FieldInfo objects</summary>
		private void InitModifierMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ModifierMasterFieldIndex), "ModifierMasterEntity");
			this.AddElementFieldInfo("ModifierMasterEntity", "DataControlGroupId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ModifierMasterFieldIndex.DataControlGroupId, 0, 0, 10);
			this.AddElementFieldInfo("ModifierMasterEntity", "DiscoupId", typeof(System.Int32), false, false, false, false,  (int)ModifierMasterFieldIndex.DiscoupId, 0, 0, 10);
			this.AddElementFieldInfo("ModifierMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)ModifierMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("ModifierMasterEntity", "KdsVideoLabel", typeof(System.String), false, false, false, true,  (int)ModifierMasterFieldIndex.KdsVideoLabel, 20, 0, 0);
			this.AddElementFieldInfo("ModifierMasterEntity", "ModifierAbbr1", typeof(System.String), false, false, false, true,  (int)ModifierMasterFieldIndex.ModifierAbbr1, 7, 0, 0);
			this.AddElementFieldInfo("ModifierMasterEntity", "ModifierAbbr2", typeof(System.String), false, false, false, true,  (int)ModifierMasterFieldIndex.ModifierAbbr2, 7, 0, 0);
			this.AddElementFieldInfo("ModifierMasterEntity", "ModifierExtraChoicegrpId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ModifierMasterFieldIndex.ModifierExtraChoicegrpId, 0, 0, 10);
			this.AddElementFieldInfo("ModifierMasterEntity", "ModifierId", typeof(System.Int32), true, false, false, false,  (int)ModifierMasterFieldIndex.ModifierId, 0, 0, 10);
			this.AddElementFieldInfo("ModifierMasterEntity", "ModifierKpLabel", typeof(System.String), false, false, false, true,  (int)ModifierMasterFieldIndex.ModifierKpLabel, 16, 0, 0);
			this.AddElementFieldInfo("ModifierMasterEntity", "ModifierMenuItemId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ModifierMasterFieldIndex.ModifierMenuItemId, 0, 0, 10);
			this.AddElementFieldInfo("ModifierMasterEntity", "ModifierName", typeof(System.String), false, false, false, true,  (int)ModifierMasterFieldIndex.ModifierName, 16, 0, 0);
			this.AddElementFieldInfo("ModifierMasterEntity", "ModifierPrintFlag", typeof(System.Boolean), false, false, false, false,  (int)ModifierMasterFieldIndex.ModifierPrintFlag, 0, 0, 0);
			this.AddElementFieldInfo("ModifierMasterEntity", "ModifierSecId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ModifierMasterFieldIndex.ModifierSecId, 0, 0, 10);
			this.AddElementFieldInfo("ModifierMasterEntity", "ModifierWithMiFlag", typeof(System.Boolean), false, false, false, false,  (int)ModifierMasterFieldIndex.ModifierWithMiFlag, 0, 0, 0);
			this.AddElementFieldInfo("ModifierMasterEntity", "RowVersion", typeof(System.Byte[]), false, false, true, false,  (int)ModifierMasterFieldIndex.RowVersion, 2147483647, 0, 0);
			this.AddElementFieldInfo("ModifierMasterEntity", "StoreCreatedId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ModifierMasterFieldIndex.StoreCreatedId, 0, 0, 10);
			this.AddElementFieldInfo("ModifierMasterEntity", "StoreId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ModifierMasterFieldIndex.StoreId, 0, 0, 10);
		}
		/// <summary>Inits ModifierPriceJoinEntity's FieldInfo objects</summary>
		private void InitModifierPriceJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ModifierPriceJoinFieldIndex), "ModifierPriceJoinEntity");
			this.AddElementFieldInfo("ModifierPriceJoinEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)ModifierPriceJoinFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("ModifierPriceJoinEntity", "ModifierId", typeof(System.Int32), true, false, false, false,  (int)ModifierPriceJoinFieldIndex.ModifierId, 0, 0, 10);
			this.AddElementFieldInfo("ModifierPriceJoinEntity", "PriceLast", typeof(Nullable<System.Decimal>), false, false, false, true,  (int)ModifierPriceJoinFieldIndex.PriceLast, 0, 4, 19);
			this.AddElementFieldInfo("ModifierPriceJoinEntity", "PriceLevelId", typeof(System.Int32), true, false, false, false,  (int)ModifierPriceJoinFieldIndex.PriceLevelId, 0, 0, 10);
			this.AddElementFieldInfo("ModifierPriceJoinEntity", "PriceNet", typeof(Nullable<System.Decimal>), false, false, false, true,  (int)ModifierPriceJoinFieldIndex.PriceNet, 0, 4, 19);
		}
		/// <summary>Inits OptionsMasterEntity's FieldInfo objects</summary>
		private void InitOptionsMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(OptionsMasterFieldIndex), "OptionsMasterEntity");
			this.AddElementFieldInfo("OptionsMasterEntity", "DualFontName", typeof(System.String), false, false, false, true,  (int)OptionsMasterFieldIndex.DualFontName, 255, 0, 0);
			this.AddElementFieldInfo("OptionsMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)OptionsMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("OptionsMasterEntity", "ThemeLanguageId", typeof(System.String), false, false, false, true,  (int)OptionsMasterFieldIndex.ThemeLanguageId, 2, 0, 0);
			this.AddElementFieldInfo("OptionsMasterEntity", "UdUserFlag", typeof(System.Boolean), false, false, false, false,  (int)OptionsMasterFieldIndex.UdUserFlag, 0, 0, 0);
			this.AddElementFieldInfo("OptionsMasterEntity", "UnicodeFlag", typeof(System.Boolean), false, false, false, false,  (int)OptionsMasterFieldIndex.UnicodeFlag, 0, 0, 0);
		}
		/// <summary>Inits PriceLevelMasterEntity's FieldInfo objects</summary>
		private void InitPriceLevelMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(PriceLevelMasterFieldIndex), "PriceLevelMasterEntity");
			this.AddElementFieldInfo("PriceLevelMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)PriceLevelMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("PriceLevelMasterEntity", "PriceLevelId", typeof(System.Int32), true, false, false, false,  (int)PriceLevelMasterFieldIndex.PriceLevelId, 0, 0, 10);
			this.AddElementFieldInfo("PriceLevelMasterEntity", "PriceLevelName", typeof(System.String), false, false, false, true,  (int)PriceLevelMasterFieldIndex.PriceLevelName, 16, 0, 0);
			this.AddElementFieldInfo("PriceLevelMasterEntity", "RowVersion", typeof(System.Byte[]), false, false, true, false,  (int)PriceLevelMasterFieldIndex.RowVersion, 2147483647, 0, 0);
			this.AddElementFieldInfo("PriceLevelMasterEntity", "StoreId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)PriceLevelMasterFieldIndex.StoreId, 0, 0, 10);
		}
		/// <summary>Inits ProcessMasterEntity's FieldInfo objects</summary>
		private void InitProcessMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ProcessMasterFieldIndex), "ProcessMasterEntity");
			this.AddElementFieldInfo("ProcessMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)ProcessMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("ProcessMasterEntity", "InstalledServerId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ProcessMasterFieldIndex.InstalledServerId, 0, 0, 10);
			this.AddElementFieldInfo("ProcessMasterEntity", "ProcessDesc", typeof(System.String), false, false, false, true,  (int)ProcessMasterFieldIndex.ProcessDesc, 255, 0, 0);
			this.AddElementFieldInfo("ProcessMasterEntity", "ProcessId", typeof(System.Int32), true, false, false, false,  (int)ProcessMasterFieldIndex.ProcessId, 0, 0, 10);
			this.AddElementFieldInfo("ProcessMasterEntity", "ProcessName", typeof(System.String), false, false, false, true,  (int)ProcessMasterFieldIndex.ProcessName, 50, 0, 0);
			this.AddElementFieldInfo("ProcessMasterEntity", "ProcessTypeId", typeof(Nullable<System.Int16>), false, false, false, true,  (int)ProcessMasterFieldIndex.ProcessTypeId, 0, 0, 5);
			this.AddElementFieldInfo("ProcessMasterEntity", "StoreId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ProcessMasterFieldIndex.StoreId, 0, 0, 10);
		}
		/// <summary>Inits ProductClassMasterEntity's FieldInfo objects</summary>
		private void InitProductClassMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ProductClassMasterFieldIndex), "ProductClassMasterEntity");
			this.AddElementFieldInfo("ProductClassMasterEntity", "DefaultMenuItemGroupId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ProductClassMasterFieldIndex.DefaultMenuItemGroupId, 0, 0, 10);
			this.AddElementFieldInfo("ProductClassMasterEntity", "DefaultRevCatId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ProductClassMasterFieldIndex.DefaultRevCatId, 0, 0, 10);
			this.AddElementFieldInfo("ProductClassMasterEntity", "DefaultRptCatId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ProductClassMasterFieldIndex.DefaultRptCatId, 0, 0, 10);
			this.AddElementFieldInfo("ProductClassMasterEntity", "DefaultSecId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ProductClassMasterFieldIndex.DefaultSecId, 0, 0, 10);
			this.AddElementFieldInfo("ProductClassMasterEntity", "DefaultTaxGrpId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ProductClassMasterFieldIndex.DefaultTaxGrpId, 0, 0, 10);
			this.AddElementFieldInfo("ProductClassMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)ProductClassMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("ProductClassMasterEntity", "ItemRestrictedFlag", typeof(System.Boolean), false, false, false, false,  (int)ProductClassMasterFieldIndex.ItemRestrictedFlag, 0, 0, 0);
			this.AddElementFieldInfo("ProductClassMasterEntity", "ProdClassId", typeof(System.Int32), true, false, false, false,  (int)ProductClassMasterFieldIndex.ProdClassId, 0, 0, 10);
			this.AddElementFieldInfo("ProductClassMasterEntity", "ProdClassName", typeof(System.String), false, false, false, true,  (int)ProductClassMasterFieldIndex.ProdClassName, 16, 0, 0);
			this.AddElementFieldInfo("ProductClassMasterEntity", "RowVersion", typeof(System.Byte[]), false, false, true, false,  (int)ProductClassMasterFieldIndex.RowVersion, 2147483647, 0, 0);
			this.AddElementFieldInfo("ProductClassMasterEntity", "StoreId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ProductClassMasterFieldIndex.StoreId, 0, 0, 10);
		}
		/// <summary>Inits ProfitCenterDayPartJoinEntity's FieldInfo objects</summary>
		private void InitProfitCenterDayPartJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ProfitCenterDayPartJoinFieldIndex), "ProfitCenterDayPartJoinEntity");
			this.AddElementFieldInfo("ProfitCenterDayPartJoinEntity", "DayPartName", typeof(System.String), false, false, false, true,  (int)ProfitCenterDayPartJoinFieldIndex.DayPartName, 30, 0, 0);
			this.AddElementFieldInfo("ProfitCenterDayPartJoinEntity", "DayPartStartTime", typeof(System.String), true, false, false, false,  (int)ProfitCenterDayPartJoinFieldIndex.DayPartStartTime, 5, 0, 0);
			this.AddElementFieldInfo("ProfitCenterDayPartJoinEntity", "ProfitCenterId", typeof(System.Int32), true, false, false, false,  (int)ProfitCenterDayPartJoinFieldIndex.ProfitCenterId, 0, 0, 10);
		}
		/// <summary>Inits ProfitCenterGroupJoinEntity's FieldInfo objects</summary>
		private void InitProfitCenterGroupJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ProfitCenterGroupJoinFieldIndex), "ProfitCenterGroupJoinEntity");
			this.AddElementFieldInfo("ProfitCenterGroupJoinEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)ProfitCenterGroupJoinFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("ProfitCenterGroupJoinEntity", "ProfitCenterId", typeof(System.Int32), true, false, false, false,  (int)ProfitCenterGroupJoinFieldIndex.ProfitCenterId, 0, 0, 10);
			this.AddElementFieldInfo("ProfitCenterGroupJoinEntity", "ProfitCtrGrpId", typeof(System.Int32), true, false, false, false,  (int)ProfitCenterGroupJoinFieldIndex.ProfitCtrGrpId, 0, 0, 10);
		}
		/// <summary>Inits ProfitCenterGroupMasterEntity's FieldInfo objects</summary>
		private void InitProfitCenterGroupMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ProfitCenterGroupMasterFieldIndex), "ProfitCenterGroupMasterEntity");
			this.AddElementFieldInfo("ProfitCenterGroupMasterEntity", "ChargingPattern", typeof(System.String), false, false, false, true,  (int)ProfitCenterGroupMasterFieldIndex.ChargingPattern, 2, 0, 0);
			this.AddElementFieldInfo("ProfitCenterGroupMasterEntity", "DataControlGroupId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ProfitCenterGroupMasterFieldIndex.DataControlGroupId, 0, 0, 10);
			this.AddElementFieldInfo("ProfitCenterGroupMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)ProfitCenterGroupMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("ProfitCenterGroupMasterEntity", "ProfitCtrAutoUpdateFlag", typeof(System.Boolean), false, false, false, false,  (int)ProfitCenterGroupMasterFieldIndex.ProfitCtrAutoUpdateFlag, 0, 0, 0);
			this.AddElementFieldInfo("ProfitCenterGroupMasterEntity", "ProfitCtrGrpAbbr1", typeof(System.String), false, false, false, true,  (int)ProfitCenterGroupMasterFieldIndex.ProfitCtrGrpAbbr1, 7, 0, 0);
			this.AddElementFieldInfo("ProfitCenterGroupMasterEntity", "ProfitCtrGrpAbbr2", typeof(System.String), false, false, false, true,  (int)ProfitCenterGroupMasterFieldIndex.ProfitCtrGrpAbbr2, 7, 0, 0);
			this.AddElementFieldInfo("ProfitCenterGroupMasterEntity", "ProfitCtrGrpId", typeof(System.Int32), true, false, false, false,  (int)ProfitCenterGroupMasterFieldIndex.ProfitCtrGrpId, 0, 0, 10);
			this.AddElementFieldInfo("ProfitCenterGroupMasterEntity", "ProfitCtrGrpName", typeof(System.String), false, false, false, true,  (int)ProfitCenterGroupMasterFieldIndex.ProfitCtrGrpName, 30, 0, 0);
			this.AddElementFieldInfo("ProfitCenterGroupMasterEntity", "StoreId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ProfitCenterGroupMasterFieldIndex.StoreId, 0, 0, 10);
		}
		/// <summary>Inits ProfitCenterMasterEntity's FieldInfo objects</summary>
		private void InitProfitCenterMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ProfitCenterMasterFieldIndex), "ProfitCenterMasterEntity");
			this.AddElementFieldInfo("ProfitCenterMasterEntity", "BypassCcAgencyThresholdAmount", typeof(System.Decimal), false, false, false, false,  (int)ProfitCenterMasterFieldIndex.BypassCcAgencyThresholdAmount, 0, 4, 19);
			this.AddElementFieldInfo("ProfitCenterMasterEntity", "BypassCcPrintingThresholdAmount", typeof(System.Decimal), false, false, false, false,  (int)ProfitCenterMasterFieldIndex.BypassCcPrintingThresholdAmount, 0, 4, 19);
			this.AddElementFieldInfo("ProfitCenterMasterEntity", "BypassCcVoiceAuthThresholdAmount", typeof(System.Decimal), false, false, false, false,  (int)ProfitCenterMasterFieldIndex.BypassCcVoiceAuthThresholdAmount, 0, 4, 19);
			this.AddElementFieldInfo("ProfitCenterMasterEntity", "ChkFtrLine1", typeof(System.String), false, false, false, true,  (int)ProfitCenterMasterFieldIndex.ChkFtrLine1, 30, 0, 0);
			this.AddElementFieldInfo("ProfitCenterMasterEntity", "ChkFtrLine2", typeof(System.String), false, false, false, true,  (int)ProfitCenterMasterFieldIndex.ChkFtrLine2, 30, 0, 0);
			this.AddElementFieldInfo("ProfitCenterMasterEntity", "ChkFtrLine3", typeof(System.String), false, false, false, true,  (int)ProfitCenterMasterFieldIndex.ChkFtrLine3, 30, 0, 0);
			this.AddElementFieldInfo("ProfitCenterMasterEntity", "ChkHdrLine1", typeof(System.String), false, false, false, true,  (int)ProfitCenterMasterFieldIndex.ChkHdrLine1, 30, 0, 0);
			this.AddElementFieldInfo("ProfitCenterMasterEntity", "ChkHdrLine2", typeof(System.String), false, false, false, true,  (int)ProfitCenterMasterFieldIndex.ChkHdrLine2, 30, 0, 0);
			this.AddElementFieldInfo("ProfitCenterMasterEntity", "ChkHdrLine3", typeof(System.String), false, false, false, true,  (int)ProfitCenterMasterFieldIndex.ChkHdrLine3, 30, 0, 0);
			this.AddElementFieldInfo("ProfitCenterMasterEntity", "DataControlGroupId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ProfitCenterMasterFieldIndex.DataControlGroupId, 0, 0, 10);
			this.AddElementFieldInfo("ProfitCenterMasterEntity", "DefaultTableLayoutId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ProfitCenterMasterFieldIndex.DefaultTableLayoutId, 0, 0, 10);
			this.AddElementFieldInfo("ProfitCenterMasterEntity", "DocLinesAdvance", typeof(Nullable<System.Int16>), false, false, false, true,  (int)ProfitCenterMasterFieldIndex.DocLinesAdvance, 0, 0, 5);
			this.AddElementFieldInfo("ProfitCenterMasterEntity", "EntId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ProfitCenterMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("ProfitCenterMasterEntity", "MaxDocLinesPage", typeof(Nullable<System.Int16>), false, false, false, true,  (int)ProfitCenterMasterFieldIndex.MaxDocLinesPage, 0, 0, 5);
			this.AddElementFieldInfo("ProfitCenterMasterEntity", "MerchantId", typeof(System.String), false, false, false, true,  (int)ProfitCenterMasterFieldIndex.MerchantId, 50, 0, 0);
			this.AddElementFieldInfo("ProfitCenterMasterEntity", "MinRcptLinesPage", typeof(Nullable<System.Int16>), false, false, false, true,  (int)ProfitCenterMasterFieldIndex.MinRcptLinesPage, 0, 0, 5);
			this.AddElementFieldInfo("ProfitCenterMasterEntity", "PoleDisplayClosed", typeof(System.String), false, false, false, true,  (int)ProfitCenterMasterFieldIndex.PoleDisplayClosed, 20, 0, 0);
			this.AddElementFieldInfo("ProfitCenterMasterEntity", "PoleDisplayOpen", typeof(System.String), false, false, false, true,  (int)ProfitCenterMasterFieldIndex.PoleDisplayOpen, 20, 0, 0);
			this.AddElementFieldInfo("ProfitCenterMasterEntity", "PrimaryLanguageId", typeof(System.String), false, false, false, false,  (int)ProfitCenterMasterFieldIndex.PrimaryLanguageId, 9, 0, 0);
			this.AddElementFieldInfo("ProfitCenterMasterEntity", "PrintByRevCatFlag", typeof(System.Boolean), false, false, false, false,  (int)ProfitCenterMasterFieldIndex.PrintByRevCatFlag, 0, 0, 0);
			this.AddElementFieldInfo("ProfitCenterMasterEntity", "ProfitCenterDesc", typeof(System.String), false, false, false, true,  (int)ProfitCenterMasterFieldIndex.ProfitCenterDesc, 50, 0, 0);
			this.AddElementFieldInfo("ProfitCenterMasterEntity", "ProfitCenterId", typeof(System.Int32), true, false, false, false,  (int)ProfitCenterMasterFieldIndex.ProfitCenterId, 0, 0, 10);
			this.AddElementFieldInfo("ProfitCenterMasterEntity", "ProfitCenterName", typeof(System.String), false, false, false, true,  (int)ProfitCenterMasterFieldIndex.ProfitCenterName, 30, 0, 0);
			this.AddElementFieldInfo("ProfitCenterMasterEntity", "ProfitCtrAbbr1", typeof(System.String), false, false, false, true,  (int)ProfitCenterMasterFieldIndex.ProfitCtrAbbr1, 7, 0, 0);
			this.AddElementFieldInfo("ProfitCenterMasterEntity", "ProfitCtrAbbr2", typeof(System.String), false, false, false, true,  (int)ProfitCenterMasterFieldIndex.ProfitCtrAbbr2, 7, 0, 0);
			this.AddElementFieldInfo("ProfitCenterMasterEntity", "RowVersion", typeof(System.Byte[]), false, false, true, false,  (int)ProfitCenterMasterFieldIndex.RowVersion, 2147483647, 0, 0);
			this.AddElementFieldInfo("ProfitCenterMasterEntity", "SalesTippableFlag", typeof(System.Boolean), false, false, false, false,  (int)ProfitCenterMasterFieldIndex.SalesTippableFlag, 0, 0, 0);
			this.AddElementFieldInfo("ProfitCenterMasterEntity", "SecondaryLanguageId", typeof(System.String), false, false, false, true,  (int)ProfitCenterMasterFieldIndex.SecondaryLanguageId, 9, 0, 0);
			this.AddElementFieldInfo("ProfitCenterMasterEntity", "SourcePropertyCode", typeof(System.String), false, false, false, true,  (int)ProfitCenterMasterFieldIndex.SourcePropertyCode, 6, 0, 0);
			this.AddElementFieldInfo("ProfitCenterMasterEntity", "StoreId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ProfitCenterMasterFieldIndex.StoreId, 0, 0, 10);
			this.AddElementFieldInfo("ProfitCenterMasterEntity", "TipEnforcementCodeId", typeof(System.Int16), false, false, false, false,  (int)ProfitCenterMasterFieldIndex.TipEnforcementCodeId, 0, 0, 5);
			this.AddElementFieldInfo("ProfitCenterMasterEntity", "TipMaxPercent", typeof(Nullable<System.Int16>), false, false, false, true,  (int)ProfitCenterMasterFieldIndex.TipMaxPercent, 0, 0, 5);
		}
		/// <summary>Inits ProfitCenterReceiptPrinterJoinEntity's FieldInfo objects</summary>
		private void InitProfitCenterReceiptPrinterJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ProfitCenterReceiptPrinterJoinFieldIndex), "ProfitCenterReceiptPrinterJoinEntity");
			this.AddElementFieldInfo("ProfitCenterReceiptPrinterJoinEntity", "EntId", typeof(System.Int32), false, false, false, false,  (int)ProfitCenterReceiptPrinterJoinFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("ProfitCenterReceiptPrinterJoinEntity", "PrimaryFlag", typeof(System.Boolean), false, false, false, false,  (int)ProfitCenterReceiptPrinterJoinFieldIndex.PrimaryFlag, 0, 0, 0);
			this.AddElementFieldInfo("ProfitCenterReceiptPrinterJoinEntity", "ProfitCenterId", typeof(System.Int32), true, false, false, false,  (int)ProfitCenterReceiptPrinterJoinFieldIndex.ProfitCenterId, 0, 0, 10);
			this.AddElementFieldInfo("ProfitCenterReceiptPrinterJoinEntity", "ReceiptPrinterId", typeof(System.Int32), true, false, false, false,  (int)ProfitCenterReceiptPrinterJoinFieldIndex.ReceiptPrinterId, 0, 0, 10);
		}
		/// <summary>Inits ProfitCenterTableJoinEntity's FieldInfo objects</summary>
		private void InitProfitCenterTableJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ProfitCenterTableJoinFieldIndex), "ProfitCenterTableJoinEntity");
			this.AddElementFieldInfo("ProfitCenterTableJoinEntity", "BoothFlag", typeof(Nullable<System.Boolean>), false, false, false, true,  (int)ProfitCenterTableJoinFieldIndex.BoothFlag, 0, 0, 0);
			this.AddElementFieldInfo("ProfitCenterTableJoinEntity", "EntId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ProfitCenterTableJoinFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("ProfitCenterTableJoinEntity", "MaxSeats", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ProfitCenterTableJoinFieldIndex.MaxSeats, 0, 0, 10);
			this.AddElementFieldInfo("ProfitCenterTableJoinEntity", "OutsideFlag", typeof(Nullable<System.Boolean>), false, false, false, true,  (int)ProfitCenterTableJoinFieldIndex.OutsideFlag, 0, 0, 0);
			this.AddElementFieldInfo("ProfitCenterTableJoinEntity", "PrivateFlag", typeof(Nullable<System.Boolean>), false, false, false, true,  (int)ProfitCenterTableJoinFieldIndex.PrivateFlag, 0, 0, 0);
			this.AddElementFieldInfo("ProfitCenterTableJoinEntity", "ProfitCenterId", typeof(System.Int32), true, false, false, false,  (int)ProfitCenterTableJoinFieldIndex.ProfitCenterId, 0, 0, 10);
			this.AddElementFieldInfo("ProfitCenterTableJoinEntity", "SecurityId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ProfitCenterTableJoinFieldIndex.SecurityId, 0, 0, 10);
			this.AddElementFieldInfo("ProfitCenterTableJoinEntity", "SmokingFlag", typeof(Nullable<System.Boolean>), false, false, false, true,  (int)ProfitCenterTableJoinFieldIndex.SmokingFlag, 0, 0, 0);
			this.AddElementFieldInfo("ProfitCenterTableJoinEntity", "TableId", typeof(System.Int32), true, false, false, false,  (int)ProfitCenterTableJoinFieldIndex.TableId, 0, 0, 10);
			this.AddElementFieldInfo("ProfitCenterTableJoinEntity", "TableName", typeof(System.String), false, false, false, true,  (int)ProfitCenterTableJoinFieldIndex.TableName, 50, 0, 0);
			this.AddElementFieldInfo("ProfitCenterTableJoinEntity", "TableNo", typeof(System.String), false, false, false, true,  (int)ProfitCenterTableJoinFieldIndex.TableNo, 7, 0, 0);
			this.AddElementFieldInfo("ProfitCenterTableJoinEntity", "ViewFlag", typeof(Nullable<System.Boolean>), false, false, false, true,  (int)ProfitCenterTableJoinFieldIndex.ViewFlag, 0, 0, 0);
			this.AddElementFieldInfo("ProfitCenterTableJoinEntity", "VipFlag", typeof(Nullable<System.Boolean>), false, false, false, true,  (int)ProfitCenterTableJoinFieldIndex.VipFlag, 0, 0, 0);
		}
		/// <summary>Inits QuickTenderMasterEntity's FieldInfo objects</summary>
		private void InitQuickTenderMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(QuickTenderMasterFieldIndex), "QuickTenderMasterEntity");
			this.AddElementFieldInfo("QuickTenderMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)QuickTenderMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("QuickTenderMasterEntity", "PaymentAmount", typeof(Nullable<System.Decimal>), false, false, false, true,  (int)QuickTenderMasterFieldIndex.PaymentAmount, 0, 4, 19);
			this.AddElementFieldInfo("QuickTenderMasterEntity", "QuickTenderAbbr1", typeof(System.String), false, false, false, true,  (int)QuickTenderMasterFieldIndex.QuickTenderAbbr1, 7, 0, 0);
			this.AddElementFieldInfo("QuickTenderMasterEntity", "QuickTenderAbbr2", typeof(System.String), false, false, false, true,  (int)QuickTenderMasterFieldIndex.QuickTenderAbbr2, 7, 0, 0);
			this.AddElementFieldInfo("QuickTenderMasterEntity", "QuickTenderId", typeof(System.Int32), true, false, false, false,  (int)QuickTenderMasterFieldIndex.QuickTenderId, 0, 0, 10);
			this.AddElementFieldInfo("QuickTenderMasterEntity", "QuickTenderName", typeof(System.String), false, false, false, true,  (int)QuickTenderMasterFieldIndex.QuickTenderName, 50, 0, 0);
			this.AddElementFieldInfo("QuickTenderMasterEntity", "StoreId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)QuickTenderMasterFieldIndex.StoreId, 0, 0, 10);
			this.AddElementFieldInfo("QuickTenderMasterEntity", "TenderId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)QuickTenderMasterFieldIndex.TenderId, 0, 0, 10);
		}
		/// <summary>Inits ReceiptPrinterMasterEntity's FieldInfo objects</summary>
		private void InitReceiptPrinterMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ReceiptPrinterMasterFieldIndex), "ReceiptPrinterMasterEntity");
			this.AddElementFieldInfo("ReceiptPrinterMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)ReceiptPrinterMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("ReceiptPrinterMasterEntity", "IpAddress", typeof(System.String), false, false, false, false,  (int)ReceiptPrinterMasterFieldIndex.IpAddress, 40, 0, 0);
			this.AddElementFieldInfo("ReceiptPrinterMasterEntity", "IpPrintingPort", typeof(System.Int16), false, false, false, false,  (int)ReceiptPrinterMasterFieldIndex.IpPrintingPort, 0, 0, 5);
			this.AddElementFieldInfo("ReceiptPrinterMasterEntity", "IpStatusPort", typeof(System.Int16), false, false, false, false,  (int)ReceiptPrinterMasterFieldIndex.IpStatusPort, 0, 0, 5);
			this.AddElementFieldInfo("ReceiptPrinterMasterEntity", "ReceiptPrinterDescription", typeof(System.String), false, false, false, false,  (int)ReceiptPrinterMasterFieldIndex.ReceiptPrinterDescription, 255, 0, 0);
			this.AddElementFieldInfo("ReceiptPrinterMasterEntity", "ReceiptPrinterId", typeof(System.Int32), true, false, false, false,  (int)ReceiptPrinterMasterFieldIndex.ReceiptPrinterId, 0, 0, 10);
			this.AddElementFieldInfo("ReceiptPrinterMasterEntity", "ReceiptPrinterName", typeof(System.String), false, false, false, false,  (int)ReceiptPrinterMasterFieldIndex.ReceiptPrinterName, 50, 0, 0);
			this.AddElementFieldInfo("ReceiptPrinterMasterEntity", "RpInvertedFlag", typeof(System.Boolean), false, false, false, false,  (int)ReceiptPrinterMasterFieldIndex.RpInvertedFlag, 0, 0, 0);
			this.AddElementFieldInfo("ReceiptPrinterMasterEntity", "RpLeftMarginOffset", typeof(System.Byte), false, false, false, false,  (int)ReceiptPrinterMasterFieldIndex.RpLeftMarginOffset, 0, 0, 3);
			this.AddElementFieldInfo("ReceiptPrinterMasterEntity", "StoreId", typeof(System.Int32), false, false, false, false,  (int)ReceiptPrinterMasterFieldIndex.StoreId, 0, 0, 10);
			this.AddElementFieldInfo("ReceiptPrinterMasterEntity", "UseDefaultPrinterFontFlag", typeof(System.Boolean), false, false, false, false,  (int)ReceiptPrinterMasterFieldIndex.UseDefaultPrinterFontFlag, 0, 0, 0);
		}
		/// <summary>Inits ReportCatMasterEntity's FieldInfo objects</summary>
		private void InitReportCatMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ReportCatMasterFieldIndex), "ReportCatMasterEntity");
			this.AddElementFieldInfo("ReportCatMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)ReportCatMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("ReportCatMasterEntity", "RowVersion", typeof(System.Byte[]), false, false, true, false,  (int)ReportCatMasterFieldIndex.RowVersion, 2147483647, 0, 0);
			this.AddElementFieldInfo("ReportCatMasterEntity", "RptCatAbbr1", typeof(System.String), false, false, false, true,  (int)ReportCatMasterFieldIndex.RptCatAbbr1, 7, 0, 0);
			this.AddElementFieldInfo("ReportCatMasterEntity", "RptCatAbbr2", typeof(System.String), false, false, false, true,  (int)ReportCatMasterFieldIndex.RptCatAbbr2, 7, 0, 0);
			this.AddElementFieldInfo("ReportCatMasterEntity", "RptCatId", typeof(System.Int32), true, false, false, false,  (int)ReportCatMasterFieldIndex.RptCatId, 0, 0, 10);
			this.AddElementFieldInfo("ReportCatMasterEntity", "RptCatName", typeof(System.String), false, false, false, true,  (int)ReportCatMasterFieldIndex.RptCatName, 50, 0, 0);
			this.AddElementFieldInfo("ReportCatMasterEntity", "RptSummaryId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ReportCatMasterFieldIndex.RptSummaryId, 0, 0, 10);
			this.AddElementFieldInfo("ReportCatMasterEntity", "StoreId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ReportCatMasterFieldIndex.StoreId, 0, 0, 10);
		}
		/// <summary>Inits RevenueCatMasterEntity's FieldInfo objects</summary>
		private void InitRevenueCatMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(RevenueCatMasterFieldIndex), "RevenueCatMasterEntity");
			this.AddElementFieldInfo("RevenueCatMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)RevenueCatMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("RevenueCatMasterEntity", "LoyaltyEarnEligibleFlag", typeof(System.Boolean), false, false, false, false,  (int)RevenueCatMasterFieldIndex.LoyaltyEarnEligibleFlag, 0, 0, 0);
			this.AddElementFieldInfo("RevenueCatMasterEntity", "LoyaltyRedeemEligibleFlag", typeof(System.Boolean), false, false, false, false,  (int)RevenueCatMasterFieldIndex.LoyaltyRedeemEligibleFlag, 0, 0, 0);
			this.AddElementFieldInfo("RevenueCatMasterEntity", "RevCatAbbr1", typeof(System.String), false, false, false, true,  (int)RevenueCatMasterFieldIndex.RevCatAbbr1, 7, 0, 0);
			this.AddElementFieldInfo("RevenueCatMasterEntity", "RevCatAbbr2", typeof(System.String), false, false, false, true,  (int)RevenueCatMasterFieldIndex.RevCatAbbr2, 7, 0, 0);
			this.AddElementFieldInfo("RevenueCatMasterEntity", "RevCatId", typeof(System.Int32), true, false, false, false,  (int)RevenueCatMasterFieldIndex.RevCatId, 0, 0, 10);
			this.AddElementFieldInfo("RevenueCatMasterEntity", "RevCatName", typeof(System.String), false, false, false, true,  (int)RevenueCatMasterFieldIndex.RevCatName, 16, 0, 0);
			this.AddElementFieldInfo("RevenueCatMasterEntity", "RevClassId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)RevenueCatMasterFieldIndex.RevClassId, 0, 0, 10);
			this.AddElementFieldInfo("RevenueCatMasterEntity", "RowVersion", typeof(System.Byte[]), false, false, true, false,  (int)RevenueCatMasterFieldIndex.RowVersion, 2147483647, 0, 0);
			this.AddElementFieldInfo("RevenueCatMasterEntity", "RptCatId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)RevenueCatMasterFieldIndex.RptCatId, 0, 0, 10);
			this.AddElementFieldInfo("RevenueCatMasterEntity", "SalesTippableFlag", typeof(System.Boolean), false, false, false, false,  (int)RevenueCatMasterFieldIndex.SalesTippableFlag, 0, 0, 0);
			this.AddElementFieldInfo("RevenueCatMasterEntity", "StoreId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)RevenueCatMasterFieldIndex.StoreId, 0, 0, 10);
			this.AddElementFieldInfo("RevenueCatMasterEntity", "TenderRestrictionFlag", typeof(System.Boolean), false, false, false, false,  (int)RevenueCatMasterFieldIndex.TenderRestrictionFlag, 0, 0, 0);
		}
		/// <summary>Inits RevenueClassMasterEntity's FieldInfo objects</summary>
		private void InitRevenueClassMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(RevenueClassMasterFieldIndex), "RevenueClassMasterEntity");
			this.AddElementFieldInfo("RevenueClassMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)RevenueClassMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("RevenueClassMasterEntity", "RevClassAlphaId", typeof(System.String), false, false, false, true,  (int)RevenueClassMasterFieldIndex.RevClassAlphaId, 1, 0, 0);
			this.AddElementFieldInfo("RevenueClassMasterEntity", "RevClassId", typeof(System.Int32), true, false, false, false,  (int)RevenueClassMasterFieldIndex.RevClassId, 0, 0, 10);
			this.AddElementFieldInfo("RevenueClassMasterEntity", "RevClassName", typeof(System.String), false, false, false, true,  (int)RevenueClassMasterFieldIndex.RevClassName, 50, 0, 0);
			this.AddElementFieldInfo("RevenueClassMasterEntity", "RowVersion", typeof(System.Byte[]), false, false, true, false,  (int)RevenueClassMasterFieldIndex.RowVersion, 2147483647, 0, 0);
			this.AddElementFieldInfo("RevenueClassMasterEntity", "StoreId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)RevenueClassMasterFieldIndex.StoreId, 0, 0, 10);
		}
		/// <summary>Inits RowLockMasterEntity's FieldInfo objects</summary>
		private void InitRowLockMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(RowLockMasterFieldIndex), "RowLockMasterEntity");
			this.AddElementFieldInfo("RowLockMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)RowLockMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("RowLockMasterEntity", "LockTimeStamp", typeof(System.DateTime), false, false, false, false,  (int)RowLockMasterFieldIndex.LockTimeStamp, 0, 0, 0);
			this.AddElementFieldInfo("RowLockMasterEntity", "TableId", typeof(System.Int32), true, false, false, false,  (int)RowLockMasterFieldIndex.TableId, 0, 0, 10);
			this.AddElementFieldInfo("RowLockMasterEntity", "TableName", typeof(System.String), true, false, false, false,  (int)RowLockMasterFieldIndex.TableName, 50, 0, 0);
		}
		/// <summary>Inits ScreenTemplateMasterEntity's FieldInfo objects</summary>
		private void InitScreenTemplateMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ScreenTemplateMasterFieldIndex), "ScreenTemplateMasterEntity");
			this.AddElementFieldInfo("ScreenTemplateMasterEntity", "AllowButtonOverlapFlag", typeof(Nullable<System.Boolean>), false, false, false, true,  (int)ScreenTemplateMasterFieldIndex.AllowButtonOverlapFlag, 0, 0, 0);
			this.AddElementFieldInfo("ScreenTemplateMasterEntity", "ButtonPadding", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ScreenTemplateMasterFieldIndex.ButtonPadding, 0, 0, 10);
			this.AddElementFieldInfo("ScreenTemplateMasterEntity", "DefaultBackcolor", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ScreenTemplateMasterFieldIndex.DefaultBackcolor, 0, 0, 10);
			this.AddElementFieldInfo("ScreenTemplateMasterEntity", "DefaultHeight", typeof(Nullable<System.Int16>), false, false, false, true,  (int)ScreenTemplateMasterFieldIndex.DefaultHeight, 0, 0, 5);
			this.AddElementFieldInfo("ScreenTemplateMasterEntity", "DefaultWidth", typeof(Nullable<System.Int16>), false, false, false, true,  (int)ScreenTemplateMasterFieldIndex.DefaultWidth, 0, 0, 5);
			this.AddElementFieldInfo("ScreenTemplateMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)ScreenTemplateMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("ScreenTemplateMasterEntity", "GridSize", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ScreenTemplateMasterFieldIndex.GridSize, 0, 0, 10);
			this.AddElementFieldInfo("ScreenTemplateMasterEntity", "LockDownFlag", typeof(System.Boolean), false, false, false, false,  (int)ScreenTemplateMasterFieldIndex.LockDownFlag, 0, 0, 0);
			this.AddElementFieldInfo("ScreenTemplateMasterEntity", "ScreenTemplateId", typeof(System.Int32), true, false, false, false,  (int)ScreenTemplateMasterFieldIndex.ScreenTemplateId, 0, 0, 10);
			this.AddElementFieldInfo("ScreenTemplateMasterEntity", "ScreenTemplateName", typeof(System.String), false, false, false, true,  (int)ScreenTemplateMasterFieldIndex.ScreenTemplateName, 50, 0, 0);
			this.AddElementFieldInfo("ScreenTemplateMasterEntity", "ScreenTemplateText", typeof(System.String), false, false, false, true,  (int)ScreenTemplateMasterFieldIndex.ScreenTemplateText, 50, 0, 0);
			this.AddElementFieldInfo("ScreenTemplateMasterEntity", "SnapToGridFlag", typeof(Nullable<System.Boolean>), false, false, false, true,  (int)ScreenTemplateMasterFieldIndex.SnapToGridFlag, 0, 0, 0);
			this.AddElementFieldInfo("ScreenTemplateMasterEntity", "StoreFlag", typeof(System.Boolean), false, false, false, false,  (int)ScreenTemplateMasterFieldIndex.StoreFlag, 0, 0, 0);
			this.AddElementFieldInfo("ScreenTemplateMasterEntity", "StoreId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ScreenTemplateMasterFieldIndex.StoreId, 0, 0, 10);
			this.AddElementFieldInfo("ScreenTemplateMasterEntity", "ThemeId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ScreenTemplateMasterFieldIndex.ThemeId, 0, 0, 10);
		}
		/// <summary>Inits ScreenTemplateMigrpJoinEntity's FieldInfo objects</summary>
		private void InitScreenTemplateMigrpJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ScreenTemplateMigrpJoinFieldIndex), "ScreenTemplateMigrpJoinEntity");
			this.AddElementFieldInfo("ScreenTemplateMigrpJoinEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)ScreenTemplateMigrpJoinFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("ScreenTemplateMigrpJoinEntity", "MenuItemGroupId", typeof(System.Int32), true, false, false, false,  (int)ScreenTemplateMigrpJoinFieldIndex.MenuItemGroupId, 0, 0, 10);
			this.AddElementFieldInfo("ScreenTemplateMigrpJoinEntity", "ScreenTemplateId", typeof(System.Int32), true, false, false, false,  (int)ScreenTemplateMigrpJoinFieldIndex.ScreenTemplateId, 0, 0, 10);
		}
		/// <summary>Inits SecurityLevelMasterEntity's FieldInfo objects</summary>
		private void InitSecurityLevelMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(SecurityLevelMasterFieldIndex), "SecurityLevelMasterEntity");
			this.AddElementFieldInfo("SecurityLevelMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)SecurityLevelMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("SecurityLevelMasterEntity", "SecurityId", typeof(System.Int32), true, false, false, false,  (int)SecurityLevelMasterFieldIndex.SecurityId, 0, 0, 10);
			this.AddElementFieldInfo("SecurityLevelMasterEntity", "SecurityLevelName", typeof(System.String), false, false, false, true,  (int)SecurityLevelMasterFieldIndex.SecurityLevelName, 10, 0, 0);
		}
		/// <summary>Inits SelectionGroupItemJoinEntity's FieldInfo objects</summary>
		private void InitSelectionGroupItemJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(SelectionGroupItemJoinFieldIndex), "SelectionGroupItemJoinEntity");
			this.AddElementFieldInfo("SelectionGroupItemJoinEntity", "AdjustedAmt", typeof(System.Decimal), false, false, false, false,  (int)SelectionGroupItemJoinFieldIndex.AdjustedAmt, 0, 4, 19);
			this.AddElementFieldInfo("SelectionGroupItemJoinEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)SelectionGroupItemJoinFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("SelectionGroupItemJoinEntity", "MenuItemId", typeof(System.Int32), true, false, false, false,  (int)SelectionGroupItemJoinFieldIndex.MenuItemId, 0, 0, 10);
			this.AddElementFieldInfo("SelectionGroupItemJoinEntity", "SelectionGroupId", typeof(System.Int32), true, false, false, false,  (int)SelectionGroupItemJoinFieldIndex.SelectionGroupId, 0, 0, 10);
			this.AddElementFieldInfo("SelectionGroupItemJoinEntity", "SequenceId", typeof(System.Int16), false, false, false, false,  (int)SelectionGroupItemJoinFieldIndex.SequenceId, 0, 0, 5);
		}
		/// <summary>Inits SelectionGroupMasterEntity's FieldInfo objects</summary>
		private void InitSelectionGroupMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(SelectionGroupMasterFieldIndex), "SelectionGroupMasterEntity");
			this.AddElementFieldInfo("SelectionGroupMasterEntity", "DivisionId", typeof(System.Int32), false, false, false, false,  (int)SelectionGroupMasterFieldIndex.DivisionId, 0, 0, 10);
			this.AddElementFieldInfo("SelectionGroupMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)SelectionGroupMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("SelectionGroupMasterEntity", "RowVersion", typeof(System.Byte[]), false, false, true, false,  (int)SelectionGroupMasterFieldIndex.RowVersion, 2147483647, 0, 0);
			this.AddElementFieldInfo("SelectionGroupMasterEntity", "SelectionGroupId", typeof(System.Int32), true, false, false, false,  (int)SelectionGroupMasterFieldIndex.SelectionGroupId, 0, 0, 10);
			this.AddElementFieldInfo("SelectionGroupMasterEntity", "SelectionGroupKpLabel", typeof(System.String), false, false, false, false,  (int)SelectionGroupMasterFieldIndex.SelectionGroupKpLabel, 50, 0, 0);
			this.AddElementFieldInfo("SelectionGroupMasterEntity", "SelectionGroupName", typeof(System.String), false, false, false, false,  (int)SelectionGroupMasterFieldIndex.SelectionGroupName, 50, 0, 0);
			this.AddElementFieldInfo("SelectionGroupMasterEntity", "SelectionGroupReceiptLabel", typeof(System.String), false, false, false, false,  (int)SelectionGroupMasterFieldIndex.SelectionGroupReceiptLabel, 50, 0, 0);
			this.AddElementFieldInfo("SelectionGroupMasterEntity", "TerminalPendingText", typeof(System.String), false, false, false, false,  (int)SelectionGroupMasterFieldIndex.TerminalPendingText, 50, 0, 0);
			this.AddElementFieldInfo("SelectionGroupMasterEntity", "TerminalPrompt", typeof(System.String), false, false, false, false,  (int)SelectionGroupMasterFieldIndex.TerminalPrompt, 50, 0, 0);
		}
		/// <summary>Inits ServiceManagerProxyMasterEntity's FieldInfo objects</summary>
		private void InitServiceManagerProxyMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ServiceManagerProxyMasterFieldIndex), "ServiceManagerProxyMasterEntity");
			this.AddElementFieldInfo("ServiceManagerProxyMasterEntity", "CampusCardDuration", typeof(System.Byte), false, false, false, false,  (int)ServiceManagerProxyMasterFieldIndex.CampusCardDuration, 0, 0, 3);
			this.AddElementFieldInfo("ServiceManagerProxyMasterEntity", "CampusCardUrl", typeof(System.String), false, false, false, true,  (int)ServiceManagerProxyMasterFieldIndex.CampusCardUrl, 255, 0, 0);
			this.AddElementFieldInfo("ServiceManagerProxyMasterEntity", "CampusCardVersion", typeof(System.String), false, false, false, true,  (int)ServiceManagerProxyMasterFieldIndex.CampusCardVersion, 10, 0, 0);
			this.AddElementFieldInfo("ServiceManagerProxyMasterEntity", "CompCardDuration", typeof(System.Byte), false, false, false, false,  (int)ServiceManagerProxyMasterFieldIndex.CompCardDuration, 0, 0, 3);
			this.AddElementFieldInfo("ServiceManagerProxyMasterEntity", "CompCardUrl", typeof(System.String), false, false, false, true,  (int)ServiceManagerProxyMasterFieldIndex.CompCardUrl, 255, 0, 0);
			this.AddElementFieldInfo("ServiceManagerProxyMasterEntity", "CompCardVersion", typeof(System.String), false, false, false, true,  (int)ServiceManagerProxyMasterFieldIndex.CompCardVersion, 10, 0, 0);
			this.AddElementFieldInfo("ServiceManagerProxyMasterEntity", "CredentialModeType", typeof(System.Int16), false, false, false, false,  (int)ServiceManagerProxyMasterFieldIndex.CredentialModeType, 0, 0, 5);
			this.AddElementFieldInfo("ServiceManagerProxyMasterEntity", "CreditCardDuration", typeof(System.Byte), false, false, false, false,  (int)ServiceManagerProxyMasterFieldIndex.CreditCardDuration, 0, 0, 3);
			this.AddElementFieldInfo("ServiceManagerProxyMasterEntity", "CreditCardUrl", typeof(System.String), false, false, false, true,  (int)ServiceManagerProxyMasterFieldIndex.CreditCardUrl, 255, 0, 0);
			this.AddElementFieldInfo("ServiceManagerProxyMasterEntity", "CreditCardVersion", typeof(System.String), false, false, false, true,  (int)ServiceManagerProxyMasterFieldIndex.CreditCardVersion, 10, 0, 0);
			this.AddElementFieldInfo("ServiceManagerProxyMasterEntity", "Description", typeof(System.String), false, false, false, true,  (int)ServiceManagerProxyMasterFieldIndex.Description, 255, 0, 0);
			this.AddElementFieldInfo("ServiceManagerProxyMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)ServiceManagerProxyMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("ServiceManagerProxyMasterEntity", "GiftCardDuration", typeof(System.Byte), false, false, false, false,  (int)ServiceManagerProxyMasterFieldIndex.GiftCardDuration, 0, 0, 3);
			this.AddElementFieldInfo("ServiceManagerProxyMasterEntity", "GiftCardUrl", typeof(System.String), false, false, false, true,  (int)ServiceManagerProxyMasterFieldIndex.GiftCardUrl, 255, 0, 0);
			this.AddElementFieldInfo("ServiceManagerProxyMasterEntity", "GiftCardVersion", typeof(System.String), false, false, false, true,  (int)ServiceManagerProxyMasterFieldIndex.GiftCardVersion, 10, 0, 0);
			this.AddElementFieldInfo("ServiceManagerProxyMasterEntity", "GuestInfoDuration", typeof(System.Byte), false, false, false, false,  (int)ServiceManagerProxyMasterFieldIndex.GuestInfoDuration, 0, 0, 3);
			this.AddElementFieldInfo("ServiceManagerProxyMasterEntity", "GuestInfoUrl", typeof(System.String), false, false, false, true,  (int)ServiceManagerProxyMasterFieldIndex.GuestInfoUrl, 255, 0, 0);
			this.AddElementFieldInfo("ServiceManagerProxyMasterEntity", "GuestInfoVersion", typeof(System.String), false, false, false, true,  (int)ServiceManagerProxyMasterFieldIndex.GuestInfoVersion, 10, 0, 0);
			this.AddElementFieldInfo("ServiceManagerProxyMasterEntity", "Hash", typeof(System.Int64), false, false, false, false,  (int)ServiceManagerProxyMasterFieldIndex.Hash, 0, 0, 19);
			this.AddElementFieldInfo("ServiceManagerProxyMasterEntity", "LoyaltyCardDuration", typeof(System.Byte), false, false, false, false,  (int)ServiceManagerProxyMasterFieldIndex.LoyaltyCardDuration, 0, 0, 3);
			this.AddElementFieldInfo("ServiceManagerProxyMasterEntity", "LoyaltyCardUrl", typeof(System.String), false, false, false, true,  (int)ServiceManagerProxyMasterFieldIndex.LoyaltyCardUrl, 255, 0, 0);
			this.AddElementFieldInfo("ServiceManagerProxyMasterEntity", "LoyaltyCardVersion", typeof(System.String), false, false, false, true,  (int)ServiceManagerProxyMasterFieldIndex.LoyaltyCardVersion, 10, 0, 0);
			this.AddElementFieldInfo("ServiceManagerProxyMasterEntity", "Password", typeof(System.String), false, false, false, true,  (int)ServiceManagerProxyMasterFieldIndex.Password, 255, 0, 0);
			this.AddElementFieldInfo("ServiceManagerProxyMasterEntity", "ServiceManagerProxyId", typeof(System.Int32), true, false, false, false,  (int)ServiceManagerProxyMasterFieldIndex.ServiceManagerProxyId, 0, 0, 10);
			this.AddElementFieldInfo("ServiceManagerProxyMasterEntity", "ServiceManagerProxyName", typeof(System.String), false, false, false, false,  (int)ServiceManagerProxyMasterFieldIndex.ServiceManagerProxyName, 50, 0, 0);
			this.AddElementFieldInfo("ServiceManagerProxyMasterEntity", "SignatureCaptureUrl", typeof(System.String), false, false, false, true,  (int)ServiceManagerProxyMasterFieldIndex.SignatureCaptureUrl, 255, 0, 0);
			this.AddElementFieldInfo("ServiceManagerProxyMasterEntity", "SignatureCaptureVersion", typeof(System.String), false, false, false, true,  (int)ServiceManagerProxyMasterFieldIndex.SignatureCaptureVersion, 10, 0, 0);
			this.AddElementFieldInfo("ServiceManagerProxyMasterEntity", "SiteId", typeof(System.String), false, false, false, false,  (int)ServiceManagerProxyMasterFieldIndex.SiteId, 50, 0, 0);
			this.AddElementFieldInfo("ServiceManagerProxyMasterEntity", "Username", typeof(System.String), false, false, false, true,  (int)ServiceManagerProxyMasterFieldIndex.Username, 255, 0, 0);
			this.AddElementFieldInfo("ServiceManagerProxyMasterEntity", "UsingRguestPaymentDeviceFlag", typeof(System.Boolean), false, false, false, false,  (int)ServiceManagerProxyMasterFieldIndex.UsingRguestPaymentDeviceFlag, 0, 0, 0);
		}
		/// <summary>Inits SmuCommandMasterEntity's FieldInfo objects</summary>
		private void InitSmuCommandMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(SmuCommandMasterFieldIndex), "SmuCommandMasterEntity");
			this.AddElementFieldInfo("SmuCommandMasterEntity", "CommandId", typeof(System.Int32), false, false, false, false,  (int)SmuCommandMasterFieldIndex.CommandId, 0, 0, 10);
			this.AddElementFieldInfo("SmuCommandMasterEntity", "CreatedUtcDateTime", typeof(System.DateTime), false, false, false, false,  (int)SmuCommandMasterFieldIndex.CreatedUtcDateTime, 0, 0, 0);
			this.AddElementFieldInfo("SmuCommandMasterEntity", "DeviceId", typeof(System.Int32), false, false, false, false,  (int)SmuCommandMasterFieldIndex.DeviceId, 0, 0, 10);
			this.AddElementFieldInfo("SmuCommandMasterEntity", "DeviceTypeId", typeof(System.Int32), false, false, false, false,  (int)SmuCommandMasterFieldIndex.DeviceTypeId, 0, 0, 10);
			this.AddElementFieldInfo("SmuCommandMasterEntity", "DivisionId", typeof(System.Int32), false, false, false, false,  (int)SmuCommandMasterFieldIndex.DivisionId, 0, 0, 10);
			this.AddElementFieldInfo("SmuCommandMasterEntity", "EntId", typeof(System.Int32), false, false, false, false,  (int)SmuCommandMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("SmuCommandMasterEntity", "ExtraData", typeof(System.String), false, false, false, true,  (int)SmuCommandMasterFieldIndex.ExtraData, 1073741823, 0, 0);
			this.AddElementFieldInfo("SmuCommandMasterEntity", "RecordId", typeof(System.Guid), true, false, false, false,  (int)SmuCommandMasterFieldIndex.RecordId, 0, 0, 0);
			this.AddElementFieldInfo("SmuCommandMasterEntity", "StoreId", typeof(System.Int32), false, false, false, false,  (int)SmuCommandMasterFieldIndex.StoreId, 0, 0, 10);
		}
		/// <summary>Inits SmuStatusMasterEntity's FieldInfo objects</summary>
		private void InitSmuStatusMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(SmuStatusMasterFieldIndex), "SmuStatusMasterEntity");
			this.AddElementFieldInfo("SmuStatusMasterEntity", "ConfigurationLoadUtcTime", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)SmuStatusMasterFieldIndex.ConfigurationLoadUtcTime, 0, 0, 0);
			this.AddElementFieldInfo("SmuStatusMasterEntity", "CurrentClockUtcTime", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)SmuStatusMasterFieldIndex.CurrentClockUtcTime, 0, 0, 0);
			this.AddElementFieldInfo("SmuStatusMasterEntity", "DeviceId", typeof(System.Int32), false, false, false, false,  (int)SmuStatusMasterFieldIndex.DeviceId, 0, 0, 10);
			this.AddElementFieldInfo("SmuStatusMasterEntity", "DeviceIpAddress", typeof(System.String), false, false, false, true,  (int)SmuStatusMasterFieldIndex.DeviceIpAddress, 40, 0, 0);
			this.AddElementFieldInfo("SmuStatusMasterEntity", "DeviceTypeId", typeof(System.Int32), false, false, false, false,  (int)SmuStatusMasterFieldIndex.DeviceTypeId, 0, 0, 10);
			this.AddElementFieldInfo("SmuStatusMasterEntity", "DivisionId", typeof(System.Int32), false, false, false, false,  (int)SmuStatusMasterFieldIndex.DivisionId, 0, 0, 10);
			this.AddElementFieldInfo("SmuStatusMasterEntity", "Drawer1EmpId", typeof(System.Int32), false, false, false, false,  (int)SmuStatusMasterFieldIndex.Drawer1EmpId, 0, 0, 10);
			this.AddElementFieldInfo("SmuStatusMasterEntity", "Drawer2EmpId", typeof(System.Int32), false, false, false, false,  (int)SmuStatusMasterFieldIndex.Drawer2EmpId, 0, 0, 10);
			this.AddElementFieldInfo("SmuStatusMasterEntity", "EntId", typeof(System.Int32), false, false, false, false,  (int)SmuStatusMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("SmuStatusMasterEntity", "IsLtsInitSuccessFlag", typeof(Nullable<System.Boolean>), false, false, false, true,  (int)SmuStatusMasterFieldIndex.IsLtsInitSuccessFlag, 0, 0, 0);
			this.AddElementFieldInfo("SmuStatusMasterEntity", "IsTerminalLockedFlag", typeof(System.Boolean), false, false, false, false,  (int)SmuStatusMasterFieldIndex.IsTerminalLockedFlag, 0, 0, 0);
			this.AddElementFieldInfo("SmuStatusMasterEntity", "IsTerminalOnline", typeof(System.Boolean), false, false, false, false,  (int)SmuStatusMasterFieldIndex.IsTerminalOnline, 0, 0, 0);
			this.AddElementFieldInfo("SmuStatusMasterEntity", "IsTrainingModeFlag", typeof(System.Boolean), false, false, false, false,  (int)SmuStatusMasterFieldIndex.IsTrainingModeFlag, 0, 0, 0);
			this.AddElementFieldInfo("SmuStatusMasterEntity", "LastCheckNumber", typeof(System.Int32), false, false, false, false,  (int)SmuStatusMasterFieldIndex.LastCheckNumber, 0, 0, 10);
			this.AddElementFieldInfo("SmuStatusMasterEntity", "LastUpdateUtcDateTime", typeof(System.DateTime), false, false, false, false,  (int)SmuStatusMasterFieldIndex.LastUpdateUtcDateTime, 0, 0, 0);
			this.AddElementFieldInfo("SmuStatusMasterEntity", "LtsAvailabilityFlag", typeof(Nullable<System.Boolean>), false, false, false, true,  (int)SmuStatusMasterFieldIndex.LtsAvailabilityFlag, 0, 0, 0);
			this.AddElementFieldInfo("SmuStatusMasterEntity", "LtsServerIpAddress", typeof(System.String), false, false, false, true,  (int)SmuStatusMasterFieldIndex.LtsServerIpAddress, 40, 0, 0);
			this.AddElementFieldInfo("SmuStatusMasterEntity", "MealPeriodId", typeof(System.Int32), false, false, false, false,  (int)SmuStatusMasterFieldIndex.MealPeriodId, 0, 0, 10);
			this.AddElementFieldInfo("SmuStatusMasterEntity", "ProfitCenterId", typeof(System.Int32), false, false, false, false,  (int)SmuStatusMasterFieldIndex.ProfitCenterId, 0, 0, 10);
			this.AddElementFieldInfo("SmuStatusMasterEntity", "ProgramLoadUtcTime", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)SmuStatusMasterFieldIndex.ProgramLoadUtcTime, 0, 0, 0);
			this.AddElementFieldInfo("SmuStatusMasterEntity", "ScreenState", typeof(System.Int32), false, false, false, false,  (int)SmuStatusMasterFieldIndex.ScreenState, 0, 0, 10);
			this.AddElementFieldInfo("SmuStatusMasterEntity", "SignonEmpId", typeof(System.Int32), false, false, false, false,  (int)SmuStatusMasterFieldIndex.SignonEmpId, 0, 0, 10);
			this.AddElementFieldInfo("SmuStatusMasterEntity", "SignonJobcodeId", typeof(System.Int32), false, false, false, false,  (int)SmuStatusMasterFieldIndex.SignonJobcodeId, 0, 0, 10);
			this.AddElementFieldInfo("SmuStatusMasterEntity", "SmuStatusId", typeof(System.Guid), true, false, false, false,  (int)SmuStatusMasterFieldIndex.SmuStatusId, 0, 0, 0);
			this.AddElementFieldInfo("SmuStatusMasterEntity", "StoreId", typeof(System.Int32), false, false, false, false,  (int)SmuStatusMasterFieldIndex.StoreId, 0, 0, 10);
			this.AddElementFieldInfo("SmuStatusMasterEntity", "TlogCheckCount", typeof(System.Int32), false, false, false, false,  (int)SmuStatusMasterFieldIndex.TlogCheckCount, 0, 0, 10);
			this.AddElementFieldInfo("SmuStatusMasterEntity", "VersionNumber", typeof(System.String), false, false, false, true,  (int)SmuStatusMasterFieldIndex.VersionNumber, 20, 0, 0);
		}
		/// <summary>Inits SpecialInstrMasterEntity's FieldInfo objects</summary>
		private void InitSpecialInstrMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(SpecialInstrMasterFieldIndex), "SpecialInstrMasterEntity");
			this.AddElementFieldInfo("SpecialInstrMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)SpecialInstrMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("SpecialInstrMasterEntity", "RowVersion", typeof(System.Byte[]), false, false, true, false,  (int)SpecialInstrMasterFieldIndex.RowVersion, 2147483647, 0, 0);
			this.AddElementFieldInfo("SpecialInstrMasterEntity", "SiAbbr1", typeof(System.String), false, false, false, true,  (int)SpecialInstrMasterFieldIndex.SiAbbr1, 7, 0, 0);
			this.AddElementFieldInfo("SpecialInstrMasterEntity", "SiAbbr2", typeof(System.String), false, false, false, true,  (int)SpecialInstrMasterFieldIndex.SiAbbr2, 7, 0, 0);
			this.AddElementFieldInfo("SpecialInstrMasterEntity", "SiId", typeof(System.Int32), true, false, false, false,  (int)SpecialInstrMasterFieldIndex.SiId, 0, 0, 10);
			this.AddElementFieldInfo("SpecialInstrMasterEntity", "SiName", typeof(System.String), false, false, false, true,  (int)SpecialInstrMasterFieldIndex.SiName, 20, 0, 0);
			this.AddElementFieldInfo("SpecialInstrMasterEntity", "StoreId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)SpecialInstrMasterFieldIndex.StoreId, 0, 0, 10);
		}
		/// <summary>Inits SystemConfigurationMasterEntity's FieldInfo objects</summary>
		private void InitSystemConfigurationMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(SystemConfigurationMasterFieldIndex), "SystemConfigurationMasterEntity");
			this.AddElementFieldInfo("SystemConfigurationMasterEntity", "PropertyName", typeof(System.String), true, false, false, false,  (int)SystemConfigurationMasterFieldIndex.PropertyName, 50, 0, 0);
			this.AddElementFieldInfo("SystemConfigurationMasterEntity", "PropertyValue", typeof(System.String), false, false, false, true,  (int)SystemConfigurationMasterFieldIndex.PropertyValue, 256, 0, 0);
		}
		/// <summary>Inits TableLayoutMasterEntity's FieldInfo objects</summary>
		private void InitTableLayoutMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(TableLayoutMasterFieldIndex), "TableLayoutMasterEntity");
			this.AddElementFieldInfo("TableLayoutMasterEntity", "AllowButtonOverlapFlag", typeof(Nullable<System.Boolean>), false, false, false, true,  (int)TableLayoutMasterFieldIndex.AllowButtonOverlapFlag, 0, 0, 0);
			this.AddElementFieldInfo("TableLayoutMasterEntity", "ButtonPadding", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TableLayoutMasterFieldIndex.ButtonPadding, 0, 0, 10);
			this.AddElementFieldInfo("TableLayoutMasterEntity", "DataControlGroupId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TableLayoutMasterFieldIndex.DataControlGroupId, 0, 0, 10);
			this.AddElementFieldInfo("TableLayoutMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)TableLayoutMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("TableLayoutMasterEntity", "GridSize", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TableLayoutMasterFieldIndex.GridSize, 0, 0, 10);
			this.AddElementFieldInfo("TableLayoutMasterEntity", "ScreenSizeId", typeof(Nullable<System.Int16>), false, false, false, true,  (int)TableLayoutMasterFieldIndex.ScreenSizeId, 0, 0, 5);
			this.AddElementFieldInfo("TableLayoutMasterEntity", "ScreenTypeId", typeof(Nullable<System.Int16>), false, false, false, true,  (int)TableLayoutMasterFieldIndex.ScreenTypeId, 0, 0, 5);
			this.AddElementFieldInfo("TableLayoutMasterEntity", "SnapToGridFlag", typeof(Nullable<System.Boolean>), false, false, false, true,  (int)TableLayoutMasterFieldIndex.SnapToGridFlag, 0, 0, 0);
			this.AddElementFieldInfo("TableLayoutMasterEntity", "StoreId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TableLayoutMasterFieldIndex.StoreId, 0, 0, 10);
			this.AddElementFieldInfo("TableLayoutMasterEntity", "ThemeId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TableLayoutMasterFieldIndex.ThemeId, 0, 0, 10);
			this.AddElementFieldInfo("TableLayoutMasterEntity", "TlAbbr1", typeof(System.String), false, false, false, true,  (int)TableLayoutMasterFieldIndex.TlAbbr1, 7, 0, 0);
			this.AddElementFieldInfo("TableLayoutMasterEntity", "TlAbbr2", typeof(System.String), false, false, false, true,  (int)TableLayoutMasterFieldIndex.TlAbbr2, 7, 0, 0);
			this.AddElementFieldInfo("TableLayoutMasterEntity", "TlId", typeof(System.Int32), true, false, false, false,  (int)TableLayoutMasterFieldIndex.TlId, 0, 0, 10);
			this.AddElementFieldInfo("TableLayoutMasterEntity", "TlName", typeof(System.String), false, false, false, true,  (int)TableLayoutMasterFieldIndex.TlName, 20, 0, 0);
		}
		/// <summary>Inits TaxCatMasterEntity's FieldInfo objects</summary>
		private void InitTaxCatMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(TaxCatMasterFieldIndex), "TaxCatMasterEntity");
			this.AddElementFieldInfo("TaxCatMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)TaxCatMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("TaxCatMasterEntity", "RowVersion", typeof(System.Byte[]), false, false, true, false,  (int)TaxCatMasterFieldIndex.RowVersion, 2147483647, 0, 0);
			this.AddElementFieldInfo("TaxCatMasterEntity", "StoreId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TaxCatMasterFieldIndex.StoreId, 0, 0, 10);
			this.AddElementFieldInfo("TaxCatMasterEntity", "TaxCatAbbr1", typeof(System.String), false, false, false, true,  (int)TaxCatMasterFieldIndex.TaxCatAbbr1, 7, 0, 0);
			this.AddElementFieldInfo("TaxCatMasterEntity", "TaxCatAbbr2", typeof(System.String), false, false, false, true,  (int)TaxCatMasterFieldIndex.TaxCatAbbr2, 7, 0, 0);
			this.AddElementFieldInfo("TaxCatMasterEntity", "TaxCatId", typeof(System.Int32), true, false, false, false,  (int)TaxCatMasterFieldIndex.TaxCatId, 0, 0, 10);
			this.AddElementFieldInfo("TaxCatMasterEntity", "TaxCatName", typeof(System.String), false, false, false, true,  (int)TaxCatMasterFieldIndex.TaxCatName, 16, 0, 0);
		}
		/// <summary>Inits TaxGroupMasterEntity's FieldInfo objects</summary>
		private void InitTaxGroupMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(TaxGroupMasterFieldIndex), "TaxGroupMasterEntity");
			this.AddElementFieldInfo("TaxGroupMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)TaxGroupMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("TaxGroupMasterEntity", "StoreId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TaxGroupMasterFieldIndex.StoreId, 0, 0, 10);
			this.AddElementFieldInfo("TaxGroupMasterEntity", "TaxGrpAbbr1", typeof(System.String), false, false, false, true,  (int)TaxGroupMasterFieldIndex.TaxGrpAbbr1, 7, 0, 0);
			this.AddElementFieldInfo("TaxGroupMasterEntity", "TaxGrpAbbr2", typeof(System.String), false, false, false, true,  (int)TaxGroupMasterFieldIndex.TaxGrpAbbr2, 7, 0, 0);
			this.AddElementFieldInfo("TaxGroupMasterEntity", "TaxGrpId", typeof(System.Int32), true, false, false, false,  (int)TaxGroupMasterFieldIndex.TaxGrpId, 0, 0, 10);
			this.AddElementFieldInfo("TaxGroupMasterEntity", "TaxGrpName", typeof(System.String), false, false, false, true,  (int)TaxGroupMasterFieldIndex.TaxGrpName, 50, 0, 0);
		}
		/// <summary>Inits TaxGrpTaxJoinEntity's FieldInfo objects</summary>
		private void InitTaxGrpTaxJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(TaxGrpTaxJoinFieldIndex), "TaxGrpTaxJoinEntity");
			this.AddElementFieldInfo("TaxGrpTaxJoinEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)TaxGrpTaxJoinFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("TaxGrpTaxJoinEntity", "TaxGrpId", typeof(System.Int32), true, false, false, false,  (int)TaxGrpTaxJoinFieldIndex.TaxGrpId, 0, 0, 10);
			this.AddElementFieldInfo("TaxGrpTaxJoinEntity", "TaxId", typeof(System.Int32), true, false, false, false,  (int)TaxGrpTaxJoinFieldIndex.TaxId, 0, 0, 10);
		}
		/// <summary>Inits TaxMasterEntity's FieldInfo objects</summary>
		private void InitTaxMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(TaxMasterFieldIndex), "TaxMasterEntity");
			this.AddElementFieldInfo("TaxMasterEntity", "CompoundLevelId", typeof(Nullable<System.Int16>), false, false, false, true,  (int)TaxMasterFieldIndex.CompoundLevelId, 0, 0, 5);
			this.AddElementFieldInfo("TaxMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)TaxMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("TaxMasterEntity", "LimitTaxReportingFlag", typeof(Nullable<System.Boolean>), false, false, false, true,  (int)TaxMasterFieldIndex.LimitTaxReportingFlag, 0, 0, 0);
			this.AddElementFieldInfo("TaxMasterEntity", "LoyaltyEarnEligibleFlag", typeof(System.Boolean), false, false, false, false,  (int)TaxMasterFieldIndex.LoyaltyEarnEligibleFlag, 0, 0, 0);
			this.AddElementFieldInfo("TaxMasterEntity", "LoyaltyRedeemEligibleFlag", typeof(System.Boolean), false, false, false, false,  (int)TaxMasterFieldIndex.LoyaltyRedeemEligibleFlag, 0, 0, 0);
			this.AddElementFieldInfo("TaxMasterEntity", "RoundBasis", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TaxMasterFieldIndex.RoundBasis, 0, 0, 10);
			this.AddElementFieldInfo("TaxMasterEntity", "RoundTypeId", typeof(Nullable<System.Int16>), false, false, false, true,  (int)TaxMasterFieldIndex.RoundTypeId, 0, 0, 5);
			this.AddElementFieldInfo("TaxMasterEntity", "RowVersion", typeof(System.Byte[]), false, false, true, false,  (int)TaxMasterFieldIndex.RowVersion, 2147483647, 0, 0);
			this.AddElementFieldInfo("TaxMasterEntity", "StoreId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TaxMasterFieldIndex.StoreId, 0, 0, 10);
			this.AddElementFieldInfo("TaxMasterEntity", "TaxAbbr1", typeof(System.String), false, false, false, true,  (int)TaxMasterFieldIndex.TaxAbbr1, 7, 0, 0);
			this.AddElementFieldInfo("TaxMasterEntity", "TaxAbbr2", typeof(System.String), false, false, false, true,  (int)TaxMasterFieldIndex.TaxAbbr2, 7, 0, 0);
			this.AddElementFieldInfo("TaxMasterEntity", "TaxAmt", typeof(Nullable<System.Decimal>), false, false, false, true,  (int)TaxMasterFieldIndex.TaxAmt, 0, 4, 19);
			this.AddElementFieldInfo("TaxMasterEntity", "TaxCatId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TaxMasterFieldIndex.TaxCatId, 0, 0, 10);
			this.AddElementFieldInfo("TaxMasterEntity", "TaxId", typeof(System.Int32), true, false, false, false,  (int)TaxMasterFieldIndex.TaxId, 0, 0, 10);
			this.AddElementFieldInfo("TaxMasterEntity", "TaxName", typeof(System.String), false, false, false, true,  (int)TaxMasterFieldIndex.TaxName, 16, 0, 0);
			this.AddElementFieldInfo("TaxMasterEntity", "TaxPercent", typeof(Nullable<System.Double>), false, false, false, true,  (int)TaxMasterFieldIndex.TaxPercent, 0, 0, 38);
			this.AddElementFieldInfo("TaxMasterEntity", "TaxRemovableFlag", typeof(System.Boolean), false, false, false, false,  (int)TaxMasterFieldIndex.TaxRemovableFlag, 0, 0, 0);
			this.AddElementFieldInfo("TaxMasterEntity", "ThreshholdAmt", typeof(Nullable<System.Decimal>), false, false, false, true,  (int)TaxMasterFieldIndex.ThreshholdAmt, 0, 4, 19);
		}
		/// <summary>Inits TaxRevCatJoinEntity's FieldInfo objects</summary>
		private void InitTaxRevCatJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(TaxRevCatJoinFieldIndex), "TaxRevCatJoinEntity");
			this.AddElementFieldInfo("TaxRevCatJoinEntity", "EmpPercent", typeof(System.Int32), false, false, false, false,  (int)TaxRevCatJoinFieldIndex.EmpPercent, 0, 0, 10);
			this.AddElementFieldInfo("TaxRevCatJoinEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)TaxRevCatJoinFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("TaxRevCatJoinEntity", "PatronPercent", typeof(System.Int32), false, false, false, false,  (int)TaxRevCatJoinFieldIndex.PatronPercent, 0, 0, 10);
			this.AddElementFieldInfo("TaxRevCatJoinEntity", "RevCatId", typeof(System.Int32), true, false, false, false,  (int)TaxRevCatJoinFieldIndex.RevCatId, 0, 0, 10);
			this.AddElementFieldInfo("TaxRevCatJoinEntity", "TaxId", typeof(System.Int32), true, false, false, false,  (int)TaxRevCatJoinFieldIndex.TaxId, 0, 0, 10);
		}
		/// <summary>Inits TenderClassMasterEntity's FieldInfo objects</summary>
		private void InitTenderClassMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(TenderClassMasterFieldIndex), "TenderClassMasterEntity");
			this.AddElementFieldInfo("TenderClassMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)TenderClassMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("TenderClassMasterEntity", "RowVersion", typeof(System.Byte[]), false, false, true, false,  (int)TenderClassMasterFieldIndex.RowVersion, 2147483647, 0, 0);
			this.AddElementFieldInfo("TenderClassMasterEntity", "StoreId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TenderClassMasterFieldIndex.StoreId, 0, 0, 10);
			this.AddElementFieldInfo("TenderClassMasterEntity", "TenderClassId", typeof(System.Int32), true, false, false, false,  (int)TenderClassMasterFieldIndex.TenderClassId, 0, 0, 10);
			this.AddElementFieldInfo("TenderClassMasterEntity", "TenderClassName", typeof(System.String), false, false, false, true,  (int)TenderClassMasterFieldIndex.TenderClassName, 30, 0, 0);
		}
		/// <summary>Inits TenderMasterEntity's FieldInfo objects</summary>
		private void InitTenderMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(TenderMasterFieldIndex), "TenderMasterEntity");
			this.AddElementFieldInfo("TenderMasterEntity", "AdditionalCheckidCodeId", typeof(System.Byte), false, false, false, false,  (int)TenderMasterFieldIndex.AdditionalCheckidCodeId, 0, 0, 3);
			this.AddElementFieldInfo("TenderMasterEntity", "AutoRemoveTaxFlag", typeof(System.Boolean), false, false, false, false,  (int)TenderMasterFieldIndex.AutoRemoveTaxFlag, 0, 0, 0);
			this.AddElementFieldInfo("TenderMasterEntity", "BypassPdsFlag", typeof(System.Boolean), false, false, false, false,  (int)TenderMasterFieldIndex.BypassPdsFlag, 0, 0, 0);
			this.AddElementFieldInfo("TenderMasterEntity", "CheckTypeId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TenderMasterFieldIndex.CheckTypeId, 0, 0, 10);
			this.AddElementFieldInfo("TenderMasterEntity", "CompTenderFlag", typeof(System.Boolean), false, false, false, false,  (int)TenderMasterFieldIndex.CompTenderFlag, 0, 0, 0);
			this.AddElementFieldInfo("TenderMasterEntity", "DestinationPropertyCode", typeof(System.String), false, false, false, true,  (int)TenderMasterFieldIndex.DestinationPropertyCode, 6, 0, 0);
			this.AddElementFieldInfo("TenderMasterEntity", "DiscoupId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TenderMasterFieldIndex.DiscoupId, 0, 0, 10);
			this.AddElementFieldInfo("TenderMasterEntity", "EmvCardTypeCode", typeof(System.String), false, false, false, true,  (int)TenderMasterFieldIndex.EmvCardTypeCode, 20, 0, 0);
			this.AddElementFieldInfo("TenderMasterEntity", "EnterTipPrompt", typeof(System.Boolean), false, false, false, false,  (int)TenderMasterFieldIndex.EnterTipPrompt, 0, 0, 0);
			this.AddElementFieldInfo("TenderMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)TenderMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("TenderMasterEntity", "FirstTenderFlag", typeof(System.Boolean), false, false, false, false,  (int)TenderMasterFieldIndex.FirstTenderFlag, 0, 0, 0);
			this.AddElementFieldInfo("TenderMasterEntity", "FrankingCodeId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TenderMasterFieldIndex.FrankingCodeId, 0, 0, 10);
			this.AddElementFieldInfo("TenderMasterEntity", "IccDecimalPlaces", typeof(Nullable<System.Int16>), false, false, false, true,  (int)TenderMasterFieldIndex.IccDecimalPlaces, 0, 0, 5);
			this.AddElementFieldInfo("TenderMasterEntity", "IccRate", typeof(Nullable<System.Double>), false, false, false, true,  (int)TenderMasterFieldIndex.IccRate, 0, 0, 38);
			this.AddElementFieldInfo("TenderMasterEntity", "LastTenderFlag", typeof(System.Boolean), false, false, false, false,  (int)TenderMasterFieldIndex.LastTenderFlag, 0, 0, 0);
			this.AddElementFieldInfo("TenderMasterEntity", "LoyaltyEarnEligibleFlag", typeof(System.Boolean), false, false, false, false,  (int)TenderMasterFieldIndex.LoyaltyEarnEligibleFlag, 0, 0, 0);
			this.AddElementFieldInfo("TenderMasterEntity", "NumReceiptsPrint", typeof(Nullable<System.Int16>), false, false, false, true,  (int)TenderMasterFieldIndex.NumReceiptsPrint, 0, 0, 5);
			this.AddElementFieldInfo("TenderMasterEntity", "OpenCashdrwrCodeId", typeof(Nullable<System.Int16>), false, false, false, true,  (int)TenderMasterFieldIndex.OpenCashdrwrCodeId, 0, 0, 5);
			this.AddElementFieldInfo("TenderMasterEntity", "OvertenderCodeId", typeof(Nullable<System.Int16>), false, false, false, true,  (int)TenderMasterFieldIndex.OvertenderCodeId, 0, 0, 5);
			this.AddElementFieldInfo("TenderMasterEntity", "PostAcctNo", typeof(System.String), false, false, false, true,  (int)TenderMasterFieldIndex.PostAcctNo, 30, 0, 0);
			this.AddElementFieldInfo("TenderMasterEntity", "PostSiteId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TenderMasterFieldIndex.PostSiteId, 0, 0, 10);
			this.AddElementFieldInfo("TenderMasterEntity", "PostSystem1Flag", typeof(System.Boolean), false, false, false, false,  (int)TenderMasterFieldIndex.PostSystem1Flag, 0, 0, 0);
			this.AddElementFieldInfo("TenderMasterEntity", "PostSystem2Flag", typeof(System.Boolean), false, false, false, false,  (int)TenderMasterFieldIndex.PostSystem2Flag, 0, 0, 0);
			this.AddElementFieldInfo("TenderMasterEntity", "PostSystem3Flag", typeof(System.Boolean), false, false, false, false,  (int)TenderMasterFieldIndex.PostSystem3Flag, 0, 0, 0);
			this.AddElementFieldInfo("TenderMasterEntity", "PostSystem4Flag", typeof(System.Boolean), false, false, false, false,  (int)TenderMasterFieldIndex.PostSystem4Flag, 0, 0, 0);
			this.AddElementFieldInfo("TenderMasterEntity", "PostSystem5Flag", typeof(System.Boolean), false, false, false, false,  (int)TenderMasterFieldIndex.PostSystem5Flag, 0, 0, 0);
			this.AddElementFieldInfo("TenderMasterEntity", "PostSystem6Flag", typeof(System.Boolean), false, false, false, false,  (int)TenderMasterFieldIndex.PostSystem6Flag, 0, 0, 0);
			this.AddElementFieldInfo("TenderMasterEntity", "PostSystem7Flag", typeof(System.Boolean), false, false, false, false,  (int)TenderMasterFieldIndex.PostSystem7Flag, 0, 0, 0);
			this.AddElementFieldInfo("TenderMasterEntity", "PostSystem8Flag", typeof(System.Boolean), false, false, false, false,  (int)TenderMasterFieldIndex.PostSystem8Flag, 0, 0, 0);
			this.AddElementFieldInfo("TenderMasterEntity", "PriceLevelId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TenderMasterFieldIndex.PriceLevelId, 0, 0, 10);
			this.AddElementFieldInfo("TenderMasterEntity", "PromptCvvFlag", typeof(System.Boolean), false, false, false, false,  (int)TenderMasterFieldIndex.PromptCvvFlag, 0, 0, 0);
			this.AddElementFieldInfo("TenderMasterEntity", "PromptExtraAlphaFlag", typeof(System.Boolean), false, false, false, false,  (int)TenderMasterFieldIndex.PromptExtraAlphaFlag, 0, 0, 0);
			this.AddElementFieldInfo("TenderMasterEntity", "PromptExtraDataFlag", typeof(System.Boolean), false, false, false, false,  (int)TenderMasterFieldIndex.PromptExtraDataFlag, 0, 0, 0);
			this.AddElementFieldInfo("TenderMasterEntity", "PromptZipcodeFlag", typeof(System.Boolean), false, false, false, false,  (int)TenderMasterFieldIndex.PromptZipcodeFlag, 0, 0, 0);
			this.AddElementFieldInfo("TenderMasterEntity", "RequireAmtFlag", typeof(System.Boolean), false, false, false, false,  (int)TenderMasterFieldIndex.RequireAmtFlag, 0, 0, 0);
			this.AddElementFieldInfo("TenderMasterEntity", "RestrictedFlag", typeof(System.Boolean), false, false, false, false,  (int)TenderMasterFieldIndex.RestrictedFlag, 0, 0, 0);
			this.AddElementFieldInfo("TenderMasterEntity", "RowVersion", typeof(System.Byte[]), false, false, true, false,  (int)TenderMasterFieldIndex.RowVersion, 2147483647, 0, 0);
			this.AddElementFieldInfo("TenderMasterEntity", "SalesTippableFlag", typeof(System.Boolean), false, false, false, false,  (int)TenderMasterFieldIndex.SalesTippableFlag, 0, 0, 0);
			this.AddElementFieldInfo("TenderMasterEntity", "SecurityId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TenderMasterFieldIndex.SecurityId, 0, 0, 10);
			this.AddElementFieldInfo("TenderMasterEntity", "StoreId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TenderMasterFieldIndex.StoreId, 0, 0, 10);
			this.AddElementFieldInfo("TenderMasterEntity", "TaxCompCode", typeof(System.Byte), false, false, false, false,  (int)TenderMasterFieldIndex.TaxCompCode, 0, 0, 3);
			this.AddElementFieldInfo("TenderMasterEntity", "TenderAbbr1", typeof(System.String), false, false, false, true,  (int)TenderMasterFieldIndex.TenderAbbr1, 7, 0, 0);
			this.AddElementFieldInfo("TenderMasterEntity", "TenderAbbr2", typeof(System.String), false, false, false, true,  (int)TenderMasterFieldIndex.TenderAbbr2, 7, 0, 0);
			this.AddElementFieldInfo("TenderMasterEntity", "TenderClassId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TenderMasterFieldIndex.TenderClassId, 0, 0, 10);
			this.AddElementFieldInfo("TenderMasterEntity", "TenderId", typeof(System.Int32), true, false, false, false,  (int)TenderMasterFieldIndex.TenderId, 0, 0, 10);
			this.AddElementFieldInfo("TenderMasterEntity", "TenderLimit", typeof(Nullable<System.Decimal>), false, false, false, true,  (int)TenderMasterFieldIndex.TenderLimit, 0, 4, 19);
			this.AddElementFieldInfo("TenderMasterEntity", "TenderName", typeof(System.String), false, false, false, true,  (int)TenderMasterFieldIndex.TenderName, 16, 0, 0);
			this.AddElementFieldInfo("TenderMasterEntity", "UseArchiveFlag", typeof(System.Boolean), false, false, false, false,  (int)TenderMasterFieldIndex.UseArchiveFlag, 0, 0, 0);
			this.AddElementFieldInfo("TenderMasterEntity", "UseSigcapFlag", typeof(System.Boolean), false, false, false, false,  (int)TenderMasterFieldIndex.UseSigcapFlag, 0, 0, 0);
			this.AddElementFieldInfo("TenderMasterEntity", "VerificationCodeId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TenderMasterFieldIndex.VerificationCodeId, 0, 0, 10);
			this.AddElementFieldInfo("TenderMasterEntity", "VerificationManualEntryCodeId", typeof(System.Int16), false, false, false, false,  (int)TenderMasterFieldIndex.VerificationManualEntryCodeId, 0, 0, 5);
		}
		/// <summary>Inits TermGrpChkTypeJoinEntity's FieldInfo objects</summary>
		private void InitTermGrpChkTypeJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(TermGrpChkTypeJoinFieldIndex), "TermGrpChkTypeJoinEntity");
			this.AddElementFieldInfo("TermGrpChkTypeJoinEntity", "CheckPriceLevelId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TermGrpChkTypeJoinFieldIndex.CheckPriceLevelId, 0, 0, 10);
			this.AddElementFieldInfo("TermGrpChkTypeJoinEntity", "CheckSecId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TermGrpChkTypeJoinFieldIndex.CheckSecId, 0, 0, 10);
			this.AddElementFieldInfo("TermGrpChkTypeJoinEntity", "CheckTenderAllowed", typeof(Nullable<System.Int16>), false, false, false, true,  (int)TermGrpChkTypeJoinFieldIndex.CheckTenderAllowed, 0, 0, 5);
			this.AddElementFieldInfo("TermGrpChkTypeJoinEntity", "CheckTypeId", typeof(System.Int32), true, false, false, false,  (int)TermGrpChkTypeJoinFieldIndex.CheckTypeId, 0, 0, 10);
			this.AddElementFieldInfo("TermGrpChkTypeJoinEntity", "CoversPrompt", typeof(System.Boolean), false, false, false, false,  (int)TermGrpChkTypeJoinFieldIndex.CoversPrompt, 0, 0, 0);
			this.AddElementFieldInfo("TermGrpChkTypeJoinEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)TermGrpChkTypeJoinFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("TermGrpChkTypeJoinEntity", "RoomNoPrompt", typeof(System.Boolean), false, false, false, false,  (int)TermGrpChkTypeJoinFieldIndex.RoomNoPrompt, 0, 0, 0);
			this.AddElementFieldInfo("TermGrpChkTypeJoinEntity", "ServerPrompt", typeof(System.Boolean), false, false, false, false,  (int)TermGrpChkTypeJoinFieldIndex.ServerPrompt, 0, 0, 0);
			this.AddElementFieldInfo("TermGrpChkTypeJoinEntity", "TablePrompt", typeof(System.Boolean), false, false, false, false,  (int)TermGrpChkTypeJoinFieldIndex.TablePrompt, 0, 0, 0);
			this.AddElementFieldInfo("TermGrpChkTypeJoinEntity", "TaxOverride", typeof(Nullable<System.Int16>), false, false, false, true,  (int)TermGrpChkTypeJoinFieldIndex.TaxOverride, 0, 0, 5);
			this.AddElementFieldInfo("TermGrpChkTypeJoinEntity", "TermGrpId", typeof(System.Int32), true, false, false, false,  (int)TermGrpChkTypeJoinFieldIndex.TermGrpId, 0, 0, 10);
		}
		/// <summary>Inits TermGrpJobcodeJoinEntity's FieldInfo objects</summary>
		private void InitTermGrpJobcodeJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(TermGrpJobcodeJoinFieldIndex), "TermGrpJobcodeJoinEntity");
			this.AddElementFieldInfo("TermGrpJobcodeJoinEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)TermGrpJobcodeJoinFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("TermGrpJobcodeJoinEntity", "JobcodeId", typeof(System.Int32), true, false, false, false,  (int)TermGrpJobcodeJoinFieldIndex.JobcodeId, 0, 0, 10);
			this.AddElementFieldInfo("TermGrpJobcodeJoinEntity", "TermGrpId", typeof(System.Int32), true, false, false, false,  (int)TermGrpJobcodeJoinFieldIndex.TermGrpId, 0, 0, 10);
		}
		/// <summary>Inits TermGrpMasterEntity's FieldInfo objects</summary>
		private void InitTermGrpMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(TermGrpMasterFieldIndex), "TermGrpMasterEntity");
			this.AddElementFieldInfo("TermGrpMasterEntity", "CalendarPeriodId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TermGrpMasterFieldIndex.CalendarPeriodId, 0, 0, 10);
			this.AddElementFieldInfo("TermGrpMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)TermGrpMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("TermGrpMasterEntity", "ScreenScheduleId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TermGrpMasterFieldIndex.ScreenScheduleId, 0, 0, 10);
			this.AddElementFieldInfo("TermGrpMasterEntity", "SigcapButton1", typeof(System.Int32), false, false, false, false,  (int)TermGrpMasterFieldIndex.SigcapButton1, 0, 0, 10);
			this.AddElementFieldInfo("TermGrpMasterEntity", "SigcapButton2", typeof(System.Int32), false, false, false, false,  (int)TermGrpMasterFieldIndex.SigcapButton2, 0, 0, 10);
			this.AddElementFieldInfo("TermGrpMasterEntity", "SigcapButton3", typeof(System.Int32), false, false, false, false,  (int)TermGrpMasterFieldIndex.SigcapButton3, 0, 0, 10);
			this.AddElementFieldInfo("TermGrpMasterEntity", "SigcapButton4", typeof(System.Int32), false, false, false, false,  (int)TermGrpMasterFieldIndex.SigcapButton4, 0, 0, 10);
			this.AddElementFieldInfo("TermGrpMasterEntity", "SigcapTipPrompt", typeof(System.String), false, false, false, true,  (int)TermGrpMasterFieldIndex.SigcapTipPrompt, 255, 0, 0);
			this.AddElementFieldInfo("TermGrpMasterEntity", "SigcapTypeCode", typeof(System.Byte), false, false, false, false,  (int)TermGrpMasterFieldIndex.SigcapTypeCode, 0, 0, 3);
			this.AddElementFieldInfo("TermGrpMasterEntity", "SiMenuId", typeof(System.Int32), false, false, false, false,  (int)TermGrpMasterFieldIndex.SiMenuId, 0, 0, 10);
			this.AddElementFieldInfo("TermGrpMasterEntity", "StartTimeFlag", typeof(System.Boolean), false, false, false, false,  (int)TermGrpMasterFieldIndex.StartTimeFlag, 0, 0, 0);
			this.AddElementFieldInfo("TermGrpMasterEntity", "StoreId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TermGrpMasterFieldIndex.StoreId, 0, 0, 10);
			this.AddElementFieldInfo("TermGrpMasterEntity", "TermGrpId", typeof(System.Int32), true, false, false, false,  (int)TermGrpMasterFieldIndex.TermGrpId, 0, 0, 10);
			this.AddElementFieldInfo("TermGrpMasterEntity", "TermGrpName", typeof(System.String), false, false, false, true,  (int)TermGrpMasterFieldIndex.TermGrpName, 50, 0, 0);
			this.AddElementFieldInfo("TermGrpMasterEntity", "TerminalTextId", typeof(System.Int32), false, false, false, false,  (int)TermGrpMasterFieldIndex.TerminalTextId, 0, 0, 10);
		}
		/// <summary>Inits TermGrpMenuJoinEntity's FieldInfo objects</summary>
		private void InitTermGrpMenuJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(TermGrpMenuJoinFieldIndex), "TermGrpMenuJoinEntity");
			this.AddElementFieldInfo("TermGrpMenuJoinEntity", "CheckFlag", typeof(System.Boolean), false, false, false, false,  (int)TermGrpMenuJoinFieldIndex.CheckFlag, 0, 0, 0);
			this.AddElementFieldInfo("TermGrpMenuJoinEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)TermGrpMenuJoinFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("TermGrpMenuJoinEntity", "MenuId", typeof(System.Int32), true, false, false, false,  (int)TermGrpMenuJoinFieldIndex.MenuId, 0, 0, 10);
			this.AddElementFieldInfo("TermGrpMenuJoinEntity", "ParentMenuId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TermGrpMenuJoinFieldIndex.ParentMenuId, 0, 0, 10);
			this.AddElementFieldInfo("TermGrpMenuJoinEntity", "TermGrpId", typeof(System.Int32), true, false, false, false,  (int)TermGrpMenuJoinFieldIndex.TermGrpId, 0, 0, 10);
		}
		/// <summary>Inits TermGrpMenuJoinCookerEntity's FieldInfo objects</summary>
		private void InitTermGrpMenuJoinCookerEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(TermGrpMenuJoinCookerFieldIndex), "TermGrpMenuJoinCookerEntity");
			this.AddElementFieldInfo("TermGrpMenuJoinCookerEntity", "CheckFlag", typeof(System.Boolean), false, false, false, false,  (int)TermGrpMenuJoinCookerFieldIndex.CheckFlag, 0, 0, 0);
			this.AddElementFieldInfo("TermGrpMenuJoinCookerEntity", "CookerHandle", typeof(System.Decimal), true, false, false, false,  (int)TermGrpMenuJoinCookerFieldIndex.CookerHandle, 0, 10, 20);
			this.AddElementFieldInfo("TermGrpMenuJoinCookerEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)TermGrpMenuJoinCookerFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("TermGrpMenuJoinCookerEntity", "MenuId", typeof(System.Int32), true, false, false, false,  (int)TermGrpMenuJoinCookerFieldIndex.MenuId, 0, 0, 10);
			this.AddElementFieldInfo("TermGrpMenuJoinCookerEntity", "ParentMenuId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TermGrpMenuJoinCookerFieldIndex.ParentMenuId, 0, 0, 10);
			this.AddElementFieldInfo("TermGrpMenuJoinCookerEntity", "TermGrpId", typeof(System.Int32), true, false, false, false,  (int)TermGrpMenuJoinCookerFieldIndex.TermGrpId, 0, 0, 10);
		}
		/// <summary>Inits TermGrpMpJoinEntity's FieldInfo objects</summary>
		private void InitTermGrpMpJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(TermGrpMpJoinFieldIndex), "TermGrpMpJoinEntity");
			this.AddElementFieldInfo("TermGrpMpJoinEntity", "AutoCheckTypeId", typeof(System.Int32), false, false, false, false,  (int)TermGrpMpJoinFieldIndex.AutoCheckTypeId, 0, 0, 10);
			this.AddElementFieldInfo("TermGrpMpJoinEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)TermGrpMpJoinFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("TermGrpMpJoinEntity", "MealPeriodId", typeof(System.Int32), true, false, false, false,  (int)TermGrpMpJoinFieldIndex.MealPeriodId, 0, 0, 10);
			this.AddElementFieldInfo("TermGrpMpJoinEntity", "MealPeriodStartTime1", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)TermGrpMpJoinFieldIndex.MealPeriodStartTime1, 0, 0, 0);
			this.AddElementFieldInfo("TermGrpMpJoinEntity", "MealPeriodStartTime2", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)TermGrpMpJoinFieldIndex.MealPeriodStartTime2, 0, 0, 0);
			this.AddElementFieldInfo("TermGrpMpJoinEntity", "MealPeriodStartTime3", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)TermGrpMpJoinFieldIndex.MealPeriodStartTime3, 0, 0, 0);
			this.AddElementFieldInfo("TermGrpMpJoinEntity", "MealPeriodStartTime4", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)TermGrpMpJoinFieldIndex.MealPeriodStartTime4, 0, 0, 0);
			this.AddElementFieldInfo("TermGrpMpJoinEntity", "MealPeriodStartTime5", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)TermGrpMpJoinFieldIndex.MealPeriodStartTime5, 0, 0, 0);
			this.AddElementFieldInfo("TermGrpMpJoinEntity", "MealPeriodStartTime6", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)TermGrpMpJoinFieldIndex.MealPeriodStartTime6, 0, 0, 0);
			this.AddElementFieldInfo("TermGrpMpJoinEntity", "MealPeriodStartTime7", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)TermGrpMpJoinFieldIndex.MealPeriodStartTime7, 0, 0, 0);
			this.AddElementFieldInfo("TermGrpMpJoinEntity", "PriceLevelId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TermGrpMpJoinFieldIndex.PriceLevelId, 0, 0, 10);
			this.AddElementFieldInfo("TermGrpMpJoinEntity", "TermGrpId", typeof(System.Int32), true, false, false, false,  (int)TermGrpMpJoinFieldIndex.TermGrpId, 0, 0, 10);
		}
		/// <summary>Inits TermGrpMpmenuJoinEntity's FieldInfo objects</summary>
		private void InitTermGrpMpmenuJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(TermGrpMpmenuJoinFieldIndex), "TermGrpMpmenuJoinEntity");
			this.AddElementFieldInfo("TermGrpMpmenuJoinEntity", "CalendarDaySeqId", typeof(System.Int32), true, false, false, false,  (int)TermGrpMpmenuJoinFieldIndex.CalendarDaySeqId, 0, 0, 10);
			this.AddElementFieldInfo("TermGrpMpmenuJoinEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)TermGrpMpmenuJoinFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("TermGrpMpmenuJoinEntity", "MealPeriodId", typeof(System.Int32), true, false, false, false,  (int)TermGrpMpmenuJoinFieldIndex.MealPeriodId, 0, 0, 10);
			this.AddElementFieldInfo("TermGrpMpmenuJoinEntity", "MenuId", typeof(System.Int32), true, false, false, false,  (int)TermGrpMpmenuJoinFieldIndex.MenuId, 0, 0, 10);
			this.AddElementFieldInfo("TermGrpMpmenuJoinEntity", "MenuSeq", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TermGrpMpmenuJoinFieldIndex.MenuSeq, 0, 0, 10);
			this.AddElementFieldInfo("TermGrpMpmenuJoinEntity", "TermGrpId", typeof(System.Int32), true, false, false, false,  (int)TermGrpMpmenuJoinFieldIndex.TermGrpId, 0, 0, 10);
		}
		/// <summary>Inits TermGrpTndrJoinEntity's FieldInfo objects</summary>
		private void InitTermGrpTndrJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(TermGrpTndrJoinFieldIndex), "TermGrpTndrJoinEntity");
			this.AddElementFieldInfo("TermGrpTndrJoinEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)TermGrpTndrJoinFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("TermGrpTndrJoinEntity", "SeqNo", typeof(Nullable<System.Int16>), false, false, false, true,  (int)TermGrpTndrJoinFieldIndex.SeqNo, 0, 0, 5);
			this.AddElementFieldInfo("TermGrpTndrJoinEntity", "TenderId", typeof(System.Int32), true, false, false, false,  (int)TermGrpTndrJoinFieldIndex.TenderId, 0, 0, 10);
			this.AddElementFieldInfo("TermGrpTndrJoinEntity", "TermGrpId", typeof(System.Int32), true, false, false, false,  (int)TermGrpTndrJoinFieldIndex.TermGrpId, 0, 0, 10);
		}
		/// <summary>Inits TermGrpTransitionJoinEntity's FieldInfo objects</summary>
		private void InitTermGrpTransitionJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(TermGrpTransitionJoinFieldIndex), "TermGrpTransitionJoinEntity");
			this.AddElementFieldInfo("TermGrpTransitionJoinEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)TermGrpTransitionJoinFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("TermGrpTransitionJoinEntity", "ScreenId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TermGrpTransitionJoinFieldIndex.ScreenId, 0, 0, 10);
			this.AddElementFieldInfo("TermGrpTransitionJoinEntity", "TermGrpId", typeof(System.Int32), true, false, false, false,  (int)TermGrpTransitionJoinFieldIndex.TermGrpId, 0, 0, 10);
			this.AddElementFieldInfo("TermGrpTransitionJoinEntity", "TransitionId", typeof(System.Int32), true, false, false, false,  (int)TermGrpTransitionJoinFieldIndex.TransitionId, 0, 0, 10);
		}
		/// <summary>Inits TerminalMasterEntity's FieldInfo objects</summary>
		private void InitTerminalMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(TerminalMasterFieldIndex), "TerminalMasterEntity");
			this.AddElementFieldInfo("TerminalMasterEntity", "AltBargunTermId", typeof(System.Int32), false, false, false, false,  (int)TerminalMasterFieldIndex.AltBargunTermId, 0, 0, 10);
			this.AddElementFieldInfo("TerminalMasterEntity", "AltRcptTermId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TerminalMasterFieldIndex.AltRcptTermId, 0, 0, 10);
			this.AddElementFieldInfo("TerminalMasterEntity", "BargunBumpOverrideFlag", typeof(Nullable<System.Boolean>), false, false, false, true,  (int)TerminalMasterFieldIndex.BargunBumpOverrideFlag, 0, 0, 0);
			this.AddElementFieldInfo("TerminalMasterEntity", "BargunPrintDrinksOnBumpFlag", typeof(System.Boolean), false, false, false, false,  (int)TerminalMasterFieldIndex.BargunPrintDrinksOnBumpFlag, 0, 0, 0);
			this.AddElementFieldInfo("TerminalMasterEntity", "BargunTermFlag", typeof(System.Boolean), false, false, false, false,  (int)TerminalMasterFieldIndex.BargunTermFlag, 0, 0, 0);
			this.AddElementFieldInfo("TerminalMasterEntity", "CurrentVersion", typeof(System.String), false, false, false, true,  (int)TerminalMasterFieldIndex.CurrentVersion, 15, 0, 0);
			this.AddElementFieldInfo("TerminalMasterEntity", "DefaultTableLayoutId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TerminalMasterFieldIndex.DefaultTableLayoutId, 0, 0, 10);
			this.AddElementFieldInfo("TerminalMasterEntity", "FirstTableNo", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TerminalMasterFieldIndex.FirstTableNo, 0, 0, 10);
			this.AddElementFieldInfo("TerminalMasterEntity", "FolFileLoadFlag", typeof(System.Boolean), false, false, false, false,  (int)TerminalMasterFieldIndex.FolFileLoadFlag, 0, 0, 0);
			this.AddElementFieldInfo("TerminalMasterEntity", "GaFileLoadFlag", typeof(System.Boolean), false, false, false, false,  (int)TerminalMasterFieldIndex.GaFileLoadFlag, 0, 0, 0);
			this.AddElementFieldInfo("TerminalMasterEntity", "IpAddress", typeof(System.String), false, false, false, true,  (int)TerminalMasterFieldIndex.IpAddress, 15, 0, 0);
			this.AddElementFieldInfo("TerminalMasterEntity", "LastPingUtcDateTime", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)TerminalMasterFieldIndex.LastPingUtcDateTime, 0, 0, 0);
			this.AddElementFieldInfo("TerminalMasterEntity", "NumTables", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TerminalMasterFieldIndex.NumTables, 0, 0, 10);
			this.AddElementFieldInfo("TerminalMasterEntity", "PedValue", typeof(System.String), false, false, false, true,  (int)TerminalMasterFieldIndex.PedValue, 50, 0, 0);
			this.AddElementFieldInfo("TerminalMasterEntity", "PrimaryProfitCenterId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TerminalMasterFieldIndex.PrimaryProfitCenterId, 0, 0, 10);
			this.AddElementFieldInfo("TerminalMasterEntity", "ProfileId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TerminalMasterFieldIndex.ProfileId, 0, 0, 10);
			this.AddElementFieldInfo("TerminalMasterEntity", "ReceiptPrinterType", typeof(System.Byte), false, false, false, false,  (int)TerminalMasterFieldIndex.ReceiptPrinterType, 0, 0, 3);
			this.AddElementFieldInfo("TerminalMasterEntity", "RmsFileLoadFlag", typeof(System.Boolean), false, false, false, false,  (int)TerminalMasterFieldIndex.RmsFileLoadFlag, 0, 0, 0);
			this.AddElementFieldInfo("TerminalMasterEntity", "RowVersion", typeof(System.Byte[]), false, false, true, false,  (int)TerminalMasterFieldIndex.RowVersion, 2147483647, 0, 0);
			this.AddElementFieldInfo("TerminalMasterEntity", "StaticReceiptPrinterId", typeof(System.Int32), false, false, false, false,  (int)TerminalMasterFieldIndex.StaticReceiptPrinterId, 0, 0, 10);
			this.AddElementFieldInfo("TerminalMasterEntity", "TermActiveFlag", typeof(System.Boolean), false, false, false, false,  (int)TerminalMasterFieldIndex.TermActiveFlag, 0, 0, 0);
			this.AddElementFieldInfo("TerminalMasterEntity", "TermGrpId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TerminalMasterFieldIndex.TermGrpId, 0, 0, 10);
			this.AddElementFieldInfo("TerminalMasterEntity", "TermId", typeof(System.Int32), true, false, false, false,  (int)TerminalMasterFieldIndex.TermId, 0, 0, 10);
			this.AddElementFieldInfo("TerminalMasterEntity", "TermName", typeof(System.String), false, false, false, true,  (int)TerminalMasterFieldIndex.TermName, 50, 0, 0);
			this.AddElementFieldInfo("TerminalMasterEntity", "TermOptionGrpId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TerminalMasterFieldIndex.TermOptionGrpId, 0, 0, 10);
			this.AddElementFieldInfo("TerminalMasterEntity", "TermPrinterGrpId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TerminalMasterFieldIndex.TermPrinterGrpId, 0, 0, 10);
			this.AddElementFieldInfo("TerminalMasterEntity", "TermReceiptInfo", typeof(System.String), false, false, false, true,  (int)TerminalMasterFieldIndex.TermReceiptInfo, 50, 0, 0);
			this.AddElementFieldInfo("TerminalMasterEntity", "TermServiceGrpId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TerminalMasterFieldIndex.TermServiceGrpId, 0, 0, 10);
			this.AddElementFieldInfo("TerminalMasterEntity", "VirtualTermFlag", typeof(System.Boolean), false, false, false, false,  (int)TerminalMasterFieldIndex.VirtualTermFlag, 0, 0, 0);
		}
		/// <summary>Inits TerminalProfitCtrJoinEntity's FieldInfo objects</summary>
		private void InitTerminalProfitCtrJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(TerminalProfitCtrJoinFieldIndex), "TerminalProfitCtrJoinEntity");
			this.AddElementFieldInfo("TerminalProfitCtrJoinEntity", "PrimaryProfitCenterFlag", typeof(System.Boolean), false, false, false, false,  (int)TerminalProfitCtrJoinFieldIndex.PrimaryProfitCenterFlag, 0, 0, 0);
			this.AddElementFieldInfo("TerminalProfitCtrJoinEntity", "ProfitCenterId", typeof(System.Int32), true, false, false, false,  (int)TerminalProfitCtrJoinFieldIndex.ProfitCenterId, 0, 0, 10);
			this.AddElementFieldInfo("TerminalProfitCtrJoinEntity", "ProfitCenterSeq", typeof(Nullable<System.Int16>), false, false, false, true,  (int)TerminalProfitCtrJoinFieldIndex.ProfitCenterSeq, 0, 0, 5);
			this.AddElementFieldInfo("TerminalProfitCtrJoinEntity", "TermId", typeof(System.Int32), true, false, false, false,  (int)TerminalProfitCtrJoinFieldIndex.TermId, 0, 0, 10);
		}
		/// <summary>Inits TerminalTextGuestInfoJoinEntity's FieldInfo objects</summary>
		private void InitTerminalTextGuestInfoJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(TerminalTextGuestInfoJoinFieldIndex), "TerminalTextGuestInfoJoinEntity");
			this.AddElementFieldInfo("TerminalTextGuestInfoJoinEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)TerminalTextGuestInfoJoinFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("TerminalTextGuestInfoJoinEntity", "SequenceId", typeof(System.Int16), false, false, false, false,  (int)TerminalTextGuestInfoJoinFieldIndex.SequenceId, 0, 0, 5);
			this.AddElementFieldInfo("TerminalTextGuestInfoJoinEntity", "TerminalIntlId", typeof(System.Int32), true, false, false, false,  (int)TerminalTextGuestInfoJoinFieldIndex.TerminalIntlId, 0, 0, 10);
			this.AddElementFieldInfo("TerminalTextGuestInfoJoinEntity", "TerminalIntlUserText", typeof(System.String), false, false, false, false,  (int)TerminalTextGuestInfoJoinFieldIndex.TerminalIntlUserText, 255, 0, 0);
			this.AddElementFieldInfo("TerminalTextGuestInfoJoinEntity", "TerminalTextId", typeof(System.Int32), true, false, false, false,  (int)TerminalTextGuestInfoJoinFieldIndex.TerminalTextId, 0, 0, 10);
		}
		/// <summary>Inits TerminalTextMasterEntity's FieldInfo objects</summary>
		private void InitTerminalTextMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(TerminalTextMasterFieldIndex), "TerminalTextMasterEntity");
			this.AddElementFieldInfo("TerminalTextMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)TerminalTextMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("TerminalTextMasterEntity", "StoreId", typeof(System.Int32), false, false, false, false,  (int)TerminalTextMasterFieldIndex.StoreId, 0, 0, 10);
			this.AddElementFieldInfo("TerminalTextMasterEntity", "TerminalTextId", typeof(System.Int32), true, false, false, false,  (int)TerminalTextMasterFieldIndex.TerminalTextId, 0, 0, 10);
			this.AddElementFieldInfo("TerminalTextMasterEntity", "TerminalTextName", typeof(System.String), false, false, false, false,  (int)TerminalTextMasterFieldIndex.TerminalTextName, 50, 0, 0);
		}
		/// <summary>Inits TermOptionGrpMasterEntity's FieldInfo objects</summary>
		private void InitTermOptionGrpMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(TermOptionGrpMasterFieldIndex), "TermOptionGrpMasterEntity");
			this.AddElementFieldInfo("TermOptionGrpMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)TermOptionGrpMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("TermOptionGrpMasterEntity", "StoreId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TermOptionGrpMasterFieldIndex.StoreId, 0, 0, 10);
			this.AddElementFieldInfo("TermOptionGrpMasterEntity", "TermOptionGrpId", typeof(System.Int32), true, false, false, false,  (int)TermOptionGrpMasterFieldIndex.TermOptionGrpId, 0, 0, 10);
			this.AddElementFieldInfo("TermOptionGrpMasterEntity", "TermOptionGrpName", typeof(System.String), false, false, false, true,  (int)TermOptionGrpMasterFieldIndex.TermOptionGrpName, 50, 0, 0);
		}
		/// <summary>Inits TermOptionGrpOptionJoinEntity's FieldInfo objects</summary>
		private void InitTermOptionGrpOptionJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(TermOptionGrpOptionJoinFieldIndex), "TermOptionGrpOptionJoinEntity");
			this.AddElementFieldInfo("TermOptionGrpOptionJoinEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)TermOptionGrpOptionJoinFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("TermOptionGrpOptionJoinEntity", "OptionFlag", typeof(System.Boolean), false, false, false, false,  (int)TermOptionGrpOptionJoinFieldIndex.OptionFlag, 0, 0, 0);
			this.AddElementFieldInfo("TermOptionGrpOptionJoinEntity", "OptionId", typeof(System.Int32), true, false, false, false,  (int)TermOptionGrpOptionJoinFieldIndex.OptionId, 0, 0, 10);
			this.AddElementFieldInfo("TermOptionGrpOptionJoinEntity", "TermOptionGrpId", typeof(System.Int32), true, false, false, false,  (int)TermOptionGrpOptionJoinFieldIndex.TermOptionGrpId, 0, 0, 10);
		}
		/// <summary>Inits TermPrinterGrpKpprntJoinEntity's FieldInfo objects</summary>
		private void InitTermPrinterGrpKpprntJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(TermPrinterGrpKpprntJoinFieldIndex), "TermPrinterGrpKpprntJoinEntity");
			this.AddElementFieldInfo("TermPrinterGrpKpprntJoinEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)TermPrinterGrpKpprntJoinFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("TermPrinterGrpKpprntJoinEntity", "KpAlternateDeviceId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TermPrinterGrpKpprntJoinFieldIndex.KpAlternateDeviceId, 0, 0, 10);
			this.AddElementFieldInfo("TermPrinterGrpKpprntJoinEntity", "KpDeviceId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TermPrinterGrpKpprntJoinFieldIndex.KpDeviceId, 0, 0, 10);
			this.AddElementFieldInfo("TermPrinterGrpKpprntJoinEntity", "KpPrinterId", typeof(System.Int32), true, false, false, false,  (int)TermPrinterGrpKpprntJoinFieldIndex.KpPrinterId, 0, 0, 10);
			this.AddElementFieldInfo("TermPrinterGrpKpprntJoinEntity", "TermPrinterGrpId", typeof(System.Int32), true, false, false, false,  (int)TermPrinterGrpKpprntJoinFieldIndex.TermPrinterGrpId, 0, 0, 10);
		}
		/// <summary>Inits TermPrinterGrpMasterEntity's FieldInfo objects</summary>
		private void InitTermPrinterGrpMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(TermPrinterGrpMasterFieldIndex), "TermPrinterGrpMasterEntity");
			this.AddElementFieldInfo("TermPrinterGrpMasterEntity", "AlternatePrintingCode", typeof(System.Byte), false, false, false, false,  (int)TermPrinterGrpMasterFieldIndex.AlternatePrintingCode, 0, 0, 3);
			this.AddElementFieldInfo("TermPrinterGrpMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)TermPrinterGrpMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("TermPrinterGrpMasterEntity", "StatusCodeId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TermPrinterGrpMasterFieldIndex.StatusCodeId, 0, 0, 10);
			this.AddElementFieldInfo("TermPrinterGrpMasterEntity", "StoreId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TermPrinterGrpMasterFieldIndex.StoreId, 0, 0, 10);
			this.AddElementFieldInfo("TermPrinterGrpMasterEntity", "TermPrinterGrpAbbr1", typeof(System.String), false, false, false, true,  (int)TermPrinterGrpMasterFieldIndex.TermPrinterGrpAbbr1, 7, 0, 0);
			this.AddElementFieldInfo("TermPrinterGrpMasterEntity", "TermPrinterGrpAbbr2", typeof(System.String), false, false, false, true,  (int)TermPrinterGrpMasterFieldIndex.TermPrinterGrpAbbr2, 7, 0, 0);
			this.AddElementFieldInfo("TermPrinterGrpMasterEntity", "TermPrinterGrpId", typeof(System.Int32), true, false, false, false,  (int)TermPrinterGrpMasterFieldIndex.TermPrinterGrpId, 0, 0, 10);
			this.AddElementFieldInfo("TermPrinterGrpMasterEntity", "TermPrinterGrpName", typeof(System.String), false, false, false, true,  (int)TermPrinterGrpMasterFieldIndex.TermPrinterGrpName, 50, 0, 0);
			this.AddElementFieldInfo("TermPrinterGrpMasterEntity", "UseAlternateFlag", typeof(System.Boolean), false, false, false, false,  (int)TermPrinterGrpMasterFieldIndex.UseAlternateFlag, 0, 0, 0);
		}
		/// <summary>Inits TermPrinterGrpRouteJoinEntity's FieldInfo objects</summary>
		private void InitTermPrinterGrpRouteJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(TermPrinterGrpRouteJoinFieldIndex), "TermPrinterGrpRouteJoinEntity");
			this.AddElementFieldInfo("TermPrinterGrpRouteJoinEntity", "EntId", typeof(System.Int32), false, false, false, false,  (int)TermPrinterGrpRouteJoinFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("TermPrinterGrpRouteJoinEntity", "GroupRouteGenId", typeof(System.Int32), true, false, true, false,  (int)TermPrinterGrpRouteJoinFieldIndex.GroupRouteGenId, 0, 0, 10);
			this.AddElementFieldInfo("TermPrinterGrpRouteJoinEntity", "KpRouteId", typeof(System.Int32), false, false, false, false,  (int)TermPrinterGrpRouteJoinFieldIndex.KpRouteId, 0, 0, 10);
			this.AddElementFieldInfo("TermPrinterGrpRouteJoinEntity", "StartMinuteFri", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TermPrinterGrpRouteJoinFieldIndex.StartMinuteFri, 0, 0, 10);
			this.AddElementFieldInfo("TermPrinterGrpRouteJoinEntity", "StartMinuteMon", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TermPrinterGrpRouteJoinFieldIndex.StartMinuteMon, 0, 0, 10);
			this.AddElementFieldInfo("TermPrinterGrpRouteJoinEntity", "StartMinuteSat", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TermPrinterGrpRouteJoinFieldIndex.StartMinuteSat, 0, 0, 10);
			this.AddElementFieldInfo("TermPrinterGrpRouteJoinEntity", "StartMinuteSun", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TermPrinterGrpRouteJoinFieldIndex.StartMinuteSun, 0, 0, 10);
			this.AddElementFieldInfo("TermPrinterGrpRouteJoinEntity", "StartMinuteThu", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TermPrinterGrpRouteJoinFieldIndex.StartMinuteThu, 0, 0, 10);
			this.AddElementFieldInfo("TermPrinterGrpRouteJoinEntity", "StartMinuteTue", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TermPrinterGrpRouteJoinFieldIndex.StartMinuteTue, 0, 0, 10);
			this.AddElementFieldInfo("TermPrinterGrpRouteJoinEntity", "StartMinuteWed", typeof(Nullable<System.Int32>), false, false, false, true,  (int)TermPrinterGrpRouteJoinFieldIndex.StartMinuteWed, 0, 0, 10);
			this.AddElementFieldInfo("TermPrinterGrpRouteJoinEntity", "TermPrinterGrpId", typeof(System.Int32), false, false, false, false,  (int)TermPrinterGrpRouteJoinFieldIndex.TermPrinterGrpId, 0, 0, 10);
		}
		/// <summary>Inits ThemeColorJoinEntity's FieldInfo objects</summary>
		private void InitThemeColorJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ThemeColorJoinFieldIndex), "ThemeColorJoinEntity");
			this.AddElementFieldInfo("ThemeColorJoinEntity", "ColorCodeId", typeof(System.Int32), true, false, false, false,  (int)ThemeColorJoinFieldIndex.ColorCodeId, 0, 0, 10);
			this.AddElementFieldInfo("ThemeColorJoinEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)ThemeColorJoinFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("ThemeColorJoinEntity", "RgbColor", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ThemeColorJoinFieldIndex.RgbColor, 0, 0, 10);
			this.AddElementFieldInfo("ThemeColorJoinEntity", "ThemeId", typeof(System.Int32), true, false, false, false,  (int)ThemeColorJoinFieldIndex.ThemeId, 0, 0, 10);
		}
		/// <summary>Inits ThemeFontJoinEntity's FieldInfo objects</summary>
		private void InitThemeFontJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ThemeFontJoinFieldIndex), "ThemeFontJoinEntity");
			this.AddElementFieldInfo("ThemeFontJoinEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)ThemeFontJoinFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("ThemeFontJoinEntity", "FontId", typeof(System.Int32), true, false, false, false,  (int)ThemeFontJoinFieldIndex.FontId, 0, 0, 10);
			this.AddElementFieldInfo("ThemeFontJoinEntity", "ThemeId", typeof(System.Int32), true, false, false, false,  (int)ThemeFontJoinFieldIndex.ThemeId, 0, 0, 10);
		}
		/// <summary>Inits ThemeMasterEntity's FieldInfo objects</summary>
		private void InitThemeMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ThemeMasterFieldIndex), "ThemeMasterEntity");
			this.AddElementFieldInfo("ThemeMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)ThemeMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("ThemeMasterEntity", "StoreId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ThemeMasterFieldIndex.StoreId, 0, 0, 10);
			this.AddElementFieldInfo("ThemeMasterEntity", "ThemeId", typeof(System.Int32), true, false, false, false,  (int)ThemeMasterFieldIndex.ThemeId, 0, 0, 10);
			this.AddElementFieldInfo("ThemeMasterEntity", "ThemeName", typeof(System.String), false, false, false, true,  (int)ThemeMasterFieldIndex.ThemeName, 50, 0, 0);
		}
		/// <summary>Inits ThemeObjectJoinEntity's FieldInfo objects</summary>
		private void InitThemeObjectJoinEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ThemeObjectJoinFieldIndex), "ThemeObjectJoinEntity");
			this.AddElementFieldInfo("ThemeObjectJoinEntity", "ButtonBackcolor", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ThemeObjectJoinFieldIndex.ButtonBackcolor, 0, 0, 10);
			this.AddElementFieldInfo("ThemeObjectJoinEntity", "ButtonColumn", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ThemeObjectJoinFieldIndex.ButtonColumn, 0, 0, 10);
			this.AddElementFieldInfo("ThemeObjectJoinEntity", "ButtonFontId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ThemeObjectJoinFieldIndex.ButtonFontId, 0, 0, 10);
			this.AddElementFieldInfo("ThemeObjectJoinEntity", "ButtonForecolor", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ThemeObjectJoinFieldIndex.ButtonForecolor, 0, 0, 10);
			this.AddElementFieldInfo("ThemeObjectJoinEntity", "ButtonHeight", typeof(Nullable<System.Int16>), false, false, false, true,  (int)ThemeObjectJoinFieldIndex.ButtonHeight, 0, 0, 5);
			this.AddElementFieldInfo("ThemeObjectJoinEntity", "ButtonImageId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ThemeObjectJoinFieldIndex.ButtonImageId, 0, 0, 10);
			this.AddElementFieldInfo("ThemeObjectJoinEntity", "ButtonNextMenuId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ThemeObjectJoinFieldIndex.ButtonNextMenuId, 0, 0, 10);
			this.AddElementFieldInfo("ThemeObjectJoinEntity", "ButtonRow", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ThemeObjectJoinFieldIndex.ButtonRow, 0, 0, 10);
			this.AddElementFieldInfo("ThemeObjectJoinEntity", "ButtonShapeId", typeof(Nullable<System.Int16>), false, false, false, true,  (int)ThemeObjectJoinFieldIndex.ButtonShapeId, 0, 0, 5);
			this.AddElementFieldInfo("ThemeObjectJoinEntity", "ButtonWidth", typeof(Nullable<System.Int16>), false, false, false, true,  (int)ThemeObjectJoinFieldIndex.ButtonWidth, 0, 0, 5);
			this.AddElementFieldInfo("ThemeObjectJoinEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)ThemeObjectJoinFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("ThemeObjectJoinEntity", "ErrorCodeId", typeof(Nullable<System.Int16>), false, false, false, true,  (int)ThemeObjectJoinFieldIndex.ErrorCodeId, 0, 0, 5);
			this.AddElementFieldInfo("ThemeObjectJoinEntity", "ObjectTypeId", typeof(System.Int32), true, false, false, false,  (int)ThemeObjectJoinFieldIndex.ObjectTypeId, 0, 0, 10);
			this.AddElementFieldInfo("ThemeObjectJoinEntity", "ThemeId", typeof(System.Int32), true, false, false, false,  (int)ThemeObjectJoinFieldIndex.ThemeId, 0, 0, 10);
		}
		/// <summary>Inits UdefDateRangeInstanceEntity's FieldInfo objects</summary>
		private void InitUdefDateRangeInstanceEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(UdefDateRangeInstanceFieldIndex), "UdefDateRangeInstanceEntity");
			this.AddElementFieldInfo("UdefDateRangeInstanceEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)UdefDateRangeInstanceFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("UdefDateRangeInstanceEntity", "PeriodTypeId", typeof(System.Int32), true, false, false, false,  (int)UdefDateRangeInstanceFieldIndex.PeriodTypeId, 0, 0, 10);
			this.AddElementFieldInfo("UdefDateRangeInstanceEntity", "RangeEndDate", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)UdefDateRangeInstanceFieldIndex.RangeEndDate, 0, 0, 0);
			this.AddElementFieldInfo("UdefDateRangeInstanceEntity", "RangeEndTime", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)UdefDateRangeInstanceFieldIndex.RangeEndTime, 0, 0, 0);
			this.AddElementFieldInfo("UdefDateRangeInstanceEntity", "RangeInstanceDesc", typeof(System.String), false, false, false, true,  (int)UdefDateRangeInstanceFieldIndex.RangeInstanceDesc, 30, 0, 0);
			this.AddElementFieldInfo("UdefDateRangeInstanceEntity", "RangeInstanceId", typeof(System.Int32), true, false, true, false,  (int)UdefDateRangeInstanceFieldIndex.RangeInstanceId, 0, 0, 10);
			this.AddElementFieldInfo("UdefDateRangeInstanceEntity", "RangeStartDate", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)UdefDateRangeInstanceFieldIndex.RangeStartDate, 0, 0, 0);
			this.AddElementFieldInfo("UdefDateRangeInstanceEntity", "RangeStartTime", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)UdefDateRangeInstanceFieldIndex.RangeStartTime, 0, 0, 0);
			this.AddElementFieldInfo("UdefDateRangeInstanceEntity", "StoreId", typeof(System.Int32), false, false, false, false,  (int)UdefDateRangeInstanceFieldIndex.StoreId, 0, 0, 10);
		}
		/// <summary>Inits UdefPeriodTypeMasterEntity's FieldInfo objects</summary>
		private void InitUdefPeriodTypeMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(UdefPeriodTypeMasterFieldIndex), "UdefPeriodTypeMasterEntity");
			this.AddElementFieldInfo("UdefPeriodTypeMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)UdefPeriodTypeMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("UdefPeriodTypeMasterEntity", "PeriodTypeDesc", typeof(System.String), false, false, false, true,  (int)UdefPeriodTypeMasterFieldIndex.PeriodTypeDesc, 30, 0, 0);
			this.AddElementFieldInfo("UdefPeriodTypeMasterEntity", "PeriodTypeId", typeof(System.Int32), true, false, false, false,  (int)UdefPeriodTypeMasterFieldIndex.PeriodTypeId, 0, 0, 10);
			this.AddElementFieldInfo("UdefPeriodTypeMasterEntity", "StoreId", typeof(System.Int32), false, false, false, false,  (int)UdefPeriodTypeMasterFieldIndex.StoreId, 0, 0, 10);
		}
		/// <summary>Inits VoidReasonMasterEntity's FieldInfo objects</summary>
		private void InitVoidReasonMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(VoidReasonMasterFieldIndex), "VoidReasonMasterEntity");
			this.AddElementFieldInfo("VoidReasonMasterEntity", "EntId", typeof(System.Int32), true, false, false, false,  (int)VoidReasonMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("VoidReasonMasterEntity", "KpFlag", typeof(System.Boolean), false, false, false, false,  (int)VoidReasonMasterFieldIndex.KpFlag, 0, 0, 0);
			this.AddElementFieldInfo("VoidReasonMasterEntity", "PmixFlag", typeof(System.Boolean), false, false, false, false,  (int)VoidReasonMasterFieldIndex.PmixFlag, 0, 0, 0);
			this.AddElementFieldInfo("VoidReasonMasterEntity", "RefundCheckFlag", typeof(System.Boolean), false, false, false, false,  (int)VoidReasonMasterFieldIndex.RefundCheckFlag, 0, 0, 0);
			this.AddElementFieldInfo("VoidReasonMasterEntity", "RowVersion", typeof(System.Byte[]), false, false, true, false,  (int)VoidReasonMasterFieldIndex.RowVersion, 2147483647, 0, 0);
			this.AddElementFieldInfo("VoidReasonMasterEntity", "StoreId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)VoidReasonMasterFieldIndex.StoreId, 0, 0, 10);
			this.AddElementFieldInfo("VoidReasonMasterEntity", "VoidItemFlag", typeof(System.Boolean), false, false, false, false,  (int)VoidReasonMasterFieldIndex.VoidItemFlag, 0, 0, 0);
			this.AddElementFieldInfo("VoidReasonMasterEntity", "VoidOpenChkFlag", typeof(System.Boolean), false, false, false, false,  (int)VoidReasonMasterFieldIndex.VoidOpenChkFlag, 0, 0, 0);
			this.AddElementFieldInfo("VoidReasonMasterEntity", "VoidReasonAbbr1", typeof(System.String), false, false, false, true,  (int)VoidReasonMasterFieldIndex.VoidReasonAbbr1, 7, 0, 0);
			this.AddElementFieldInfo("VoidReasonMasterEntity", "VoidReasonAbbr2", typeof(System.String), false, false, false, true,  (int)VoidReasonMasterFieldIndex.VoidReasonAbbr2, 7, 0, 0);
			this.AddElementFieldInfo("VoidReasonMasterEntity", "VoidReasonId", typeof(System.Int32), true, false, false, false,  (int)VoidReasonMasterFieldIndex.VoidReasonId, 0, 0, 10);
			this.AddElementFieldInfo("VoidReasonMasterEntity", "VoidReasonName", typeof(System.String), false, false, false, true,  (int)VoidReasonMasterFieldIndex.VoidReasonName, 20, 0, 0);
			this.AddElementFieldInfo("VoidReasonMasterEntity", "VoidReasonSecId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)VoidReasonMasterFieldIndex.VoidReasonSecId, 0, 0, 10);
		}
		/// <summary>Inits ZooMasterEntity's FieldInfo objects</summary>
		private void InitZooMasterEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ZooMasterFieldIndex), "ZooMasterEntity");
			this.AddElementFieldInfo("ZooMasterEntity", "ConfigEventXml", typeof(System.String), false, false, false, false,  (int)ZooMasterFieldIndex.ConfigEventXml, 2147483647, 0, 0);
			this.AddElementFieldInfo("ZooMasterEntity", "CustomerId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ZooMasterFieldIndex.CustomerId, 0, 0, 10);
			this.AddElementFieldInfo("ZooMasterEntity", "DivisionId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ZooMasterFieldIndex.DivisionId, 0, 0, 10);
			this.AddElementFieldInfo("ZooMasterEntity", "EntId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ZooMasterFieldIndex.EntId, 0, 0, 10);
			this.AddElementFieldInfo("ZooMasterEntity", "MasterObjectDisplayName", typeof(System.String), false, false, false, true,  (int)ZooMasterFieldIndex.MasterObjectDisplayName, 50, 0, 0);
			this.AddElementFieldInfo("ZooMasterEntity", "MasterObjectId", typeof(System.String), false, false, false, true,  (int)ZooMasterFieldIndex.MasterObjectId, 50, 0, 0);
			this.AddElementFieldInfo("ZooMasterEntity", "ObjectDisplayName", typeof(System.String), false, false, false, true,  (int)ZooMasterFieldIndex.ObjectDisplayName, 50, 0, 0);
			this.AddElementFieldInfo("ZooMasterEntity", "ObjectId", typeof(System.String), false, false, false, true,  (int)ZooMasterFieldIndex.ObjectId, 50, 0, 0);
			this.AddElementFieldInfo("ZooMasterEntity", "ObjectTypeId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ZooMasterFieldIndex.ObjectTypeId, 0, 0, 10);
			this.AddElementFieldInfo("ZooMasterEntity", "StoreId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ZooMasterFieldIndex.StoreId, 0, 0, 10);
			this.AddElementFieldInfo("ZooMasterEntity", "TrackAction", typeof(System.String), false, false, false, false,  (int)ZooMasterFieldIndex.TrackAction, 1, 0, 0);
			this.AddElementFieldInfo("ZooMasterEntity", "TrackDttime", typeof(System.DateTime), false, false, false, false,  (int)ZooMasterFieldIndex.TrackDttime, 0, 0, 0);
			this.AddElementFieldInfo("ZooMasterEntity", "TrackGroupId", typeof(System.Int64), false, false, false, false,  (int)ZooMasterFieldIndex.TrackGroupId, 0, 0, 19);
			this.AddElementFieldInfo("ZooMasterEntity", "TrackSourceId", typeof(System.Byte), false, false, false, false,  (int)ZooMasterFieldIndex.TrackSourceId, 0, 0, 3);
			this.AddElementFieldInfo("ZooMasterEntity", "TrackUsername", typeof(System.String), false, false, false, false,  (int)ZooMasterFieldIndex.TrackUsername, 50, 0, 0);
			this.AddElementFieldInfo("ZooMasterEntity", "VirtualDatabaseName", typeof(System.String), false, false, false, false,  (int)ZooMasterFieldIndex.VirtualDatabaseName, 100, 0, 0);
			this.AddElementFieldInfo("ZooMasterEntity", "VirtualTableName", typeof(System.String), false, false, false, false,  (int)ZooMasterFieldIndex.VirtualTableName, 100, 0, 0);
			this.AddElementFieldInfo("ZooMasterEntity", "ZooId", typeof(System.Int64), true, false, true, false,  (int)ZooMasterFieldIndex.ZooId, 0, 0, 19);
		}
		
	}
}




