﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;

namespace InfoGenesis
{
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: BonusCodeMaster.</summary>
	public enum BonusCodeMasterFieldIndex
	{
		///<summary>BonusCodeEndDate. </summary>
		BonusCodeEndDate,
		///<summary>BonusCodeGenId. </summary>
		BonusCodeGenId,
		///<summary>BonusCodeName. </summary>
		BonusCodeName,
		///<summary>BonusCodeNo. </summary>
		BonusCodeNo,
		///<summary>BonusCodeStartDate. </summary>
		BonusCodeStartDate,
		///<summary>EntId. </summary>
		EntId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ButtonAbbrJoin.</summary>
	public enum ButtonAbbrJoinFieldIndex
	{
		///<summary>ButtonId. </summary>
		ButtonId,
		///<summary>EntId. </summary>
		EntId,
		///<summary>UserButtonAbbr1. </summary>
		UserButtonAbbr1,
		///<summary>UserButtonAbbr2. </summary>
		UserButtonAbbr2,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CardTypeMaster.</summary>
	public enum CardTypeMasterFieldIndex
	{
		///<summary>CardTypeId. </summary>
		CardTypeId,
		///<summary>CardTypeName. </summary>
		CardTypeName,
		///<summary>CardTypeNumLength. </summary>
		CardTypeNumLength,
		///<summary>CardTypePrefixLength. </summary>
		CardTypePrefixLength,
		///<summary>EntId. </summary>
		EntId,
		///<summary>StoreId. </summary>
		StoreId,
		///<summary>TenderId. </summary>
		TenderId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CardTypeRangeJoin.</summary>
	public enum CardTypeRangeJoinFieldIndex
	{
		///<summary>CardTypeId. </summary>
		CardTypeId,
		///<summary>CardTypePrefixHigh. </summary>
		CardTypePrefixHigh,
		///<summary>CardTypePrefixLow. </summary>
		CardTypePrefixLow,
		///<summary>CardTypeRangeId. </summary>
		CardTypeRangeId,
		///<summary>EntId. </summary>
		EntId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CfgVersion.</summary>
	public enum CfgVersionFieldIndex
	{
		///<summary>BuildVersion. </summary>
		BuildVersion,
		///<summary>DbName. </summary>
		DbName,
		///<summary>MajorVersion. </summary>
		MajorVersion,
		///<summary>MinorVersion. </summary>
		MinorVersion,
		///<summary>PatchVersion. </summary>
		PatchVersion,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CheckTypeMaster.</summary>
	public enum CheckTypeMasterFieldIndex
	{
		///<summary>CheckTypeAbbr1. </summary>
		CheckTypeAbbr1,
		///<summary>CheckTypeAbbr2. </summary>
		CheckTypeAbbr2,
		///<summary>CheckTypeId. </summary>
		CheckTypeId,
		///<summary>CheckTypeName. </summary>
		CheckTypeName,
		///<summary>DefaultPriceLevelId. </summary>
		DefaultPriceLevelId,
		///<summary>DefaultSecId. </summary>
		DefaultSecId,
		///<summary>DiscountId. </summary>
		DiscountId,
		///<summary>EntId. </summary>
		EntId,
		///<summary>RoundBasis. </summary>
		RoundBasis,
		///<summary>RoundTypeId. </summary>
		RoundTypeId,
		///<summary>RowVersion. </summary>
		RowVersion,
		///<summary>SalesTippableFlag. </summary>
		SalesTippableFlag,
		///<summary>StoreId. </summary>
		StoreId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ChefMaster.</summary>
	public enum ChefMasterFieldIndex
	{
		///<summary>CookedFlag. </summary>
		CookedFlag,
		///<summary>CookerHandle. </summary>
		CookerHandle,
		///<summary>CookerKeyId. </summary>
		CookerKeyId,
		///<summary>CustomerId. </summary>
		CustomerId,
		///<summary>DivisionId. </summary>
		DivisionId,
		///<summary>EntId. </summary>
		EntId,
		///<summary>LsrName. </summary>
		LsrName,
		///<summary>StoreId. </summary>
		StoreId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ChkTypeGratJoin.</summary>
	public enum ChkTypeGratJoinFieldIndex
	{
		///<summary>CheckTypeId. </summary>
		CheckTypeId,
		///<summary>EntId. </summary>
		EntId,
		///<summary>GratId. </summary>
		GratId,
		///<summary>RevCatCodeId. </summary>
		RevCatCodeId,
		///<summary>TaxCodeId. </summary>
		TaxCodeId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ChkTypeGratRevJoin.</summary>
	public enum ChkTypeGratRevJoinFieldIndex
	{
		///<summary>CheckTypeId. </summary>
		CheckTypeId,
		///<summary>EntId. </summary>
		EntId,
		///<summary>GratId. </summary>
		GratId,
		///<summary>RevCatId. </summary>
		RevCatId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ChkTypeGratTaxJoin.</summary>
	public enum ChkTypeGratTaxJoinFieldIndex
	{
		///<summary>CheckTypeId. </summary>
		CheckTypeId,
		///<summary>EntId. </summary>
		EntId,
		///<summary>GratId. </summary>
		GratId,
		///<summary>TaxId. </summary>
		TaxId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ChkTypeTaxGrpJoin.</summary>
	public enum ChkTypeTaxGrpJoinFieldIndex
	{
		///<summary>CheckTypeId. </summary>
		CheckTypeId,
		///<summary>EntId. </summary>
		EntId,
		///<summary>TaxCodeId. </summary>
		TaxCodeId,
		///<summary>TaxGrpId. </summary>
		TaxGrpId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ChkTypeTaxGrpTaxJoin.</summary>
	public enum ChkTypeTaxGrpTaxJoinFieldIndex
	{
		///<summary>CheckTypeId. </summary>
		CheckTypeId,
		///<summary>EntId. </summary>
		EntId,
		///<summary>TaxGrpId. </summary>
		TaxGrpId,
		///<summary>TaxId. </summary>
		TaxId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ChkTypeVatGratJoin.</summary>
	public enum ChkTypeVatGratJoinFieldIndex
	{
		///<summary>CheckTypeId. </summary>
		CheckTypeId,
		///<summary>EntId. </summary>
		EntId,
		///<summary>GratId. </summary>
		GratId,
		///<summary>TaxId. </summary>
		TaxId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ChkTypeVatRevJoin.</summary>
	public enum ChkTypeVatRevJoinFieldIndex
	{
		///<summary>CheckTypeId. </summary>
		CheckTypeId,
		///<summary>EntId. </summary>
		EntId,
		///<summary>RevCatId. </summary>
		RevCatId,
		///<summary>TaxId. </summary>
		TaxId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ChoiceGroupMaster.</summary>
	public enum ChoiceGroupMasterFieldIndex
	{
		///<summary>ChoiceGroupAbbr1. </summary>
		ChoiceGroupAbbr1,
		///<summary>ChoiceGroupAbbr2. </summary>
		ChoiceGroupAbbr2,
		///<summary>ChoiceGroupId. </summary>
		ChoiceGroupId,
		///<summary>ChoiceGroupLabel. </summary>
		ChoiceGroupLabel,
		///<summary>ChoiceGroupName. </summary>
		ChoiceGroupName,
		///<summary>DataControlGroupId. </summary>
		DataControlGroupId,
		///<summary>EntId. </summary>
		EntId,
		///<summary>MaxNumMods. </summary>
		MaxNumMods,
		///<summary>MinNumMods. </summary>
		MinNumMods,
		///<summary>NumFreeMods. </summary>
		NumFreeMods,
		///<summary>StoreId. </summary>
		StoreId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ChoiceGrpModJoin.</summary>
	public enum ChoiceGrpModJoinFieldIndex
	{
		///<summary>ChoiceGroupId. </summary>
		ChoiceGroupId,
		///<summary>EntId. </summary>
		EntId,
		///<summary>ModifierChoicegrpSeq. </summary>
		ModifierChoicegrpSeq,
		///<summary>ModifierId. </summary>
		ModifierId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ComboItemJoin.</summary>
	public enum ComboItemJoinFieldIndex
	{
		///<summary>ComboId. </summary>
		ComboId,
		///<summary>EntId. </summary>
		EntId,
		///<summary>ItemPrice. </summary>
		ItemPrice,
		///<summary>ItemQty. </summary>
		ItemQty,
		///<summary>KeyId. </summary>
		KeyId,
		///<summary>ObjectId. </summary>
		ObjectId,
		///<summary>ObjectTypeId. </summary>
		ObjectTypeId,
		///<summary>PrintReceiptFlag. </summary>
		PrintReceiptFlag,
		///<summary>SequenceId. </summary>
		SequenceId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ComboMaster.</summary>
	public enum ComboMasterFieldIndex
	{
		///<summary>ComboAbbr1. </summary>
		ComboAbbr1,
		///<summary>ComboAbbr2. </summary>
		ComboAbbr2,
		///<summary>ComboId. </summary>
		ComboId,
		///<summary>ComboKpLabel. </summary>
		ComboKpLabel,
		///<summary>ComboMenuItemId. </summary>
		ComboMenuItemId,
		///<summary>ComboName. </summary>
		ComboName,
		///<summary>ComboReceiptLabel. </summary>
		ComboReceiptLabel,
		///<summary>DivisionId. </summary>
		DivisionId,
		///<summary>EntId. </summary>
		EntId,
		///<summary>KdsVideoLabel. </summary>
		KdsVideoLabel,
		///<summary>RowVersion. </summary>
		RowVersion,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ConfigurationIdMaster.</summary>
	public enum ConfigurationIdMasterFieldIndex
	{
		///<summary>CustomerId. </summary>
		CustomerId,
		///<summary>DivId. </summary>
		DivId,
		///<summary>EntId. </summary>
		EntId,
		///<summary>ReservedId. </summary>
		ReservedId,
		///<summary>StoreId. </summary>
		StoreId,
		///<summary>TableName. </summary>
		TableName,
		///<summary>Timeout. </summary>
		Timeout,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: DeviceMaster.</summary>
	public enum DeviceMasterFieldIndex
	{
		///<summary>BbArmingCharacter. </summary>
		BbArmingCharacter,
		///<summary>BbOffsetId. </summary>
		BbOffsetId,
		///<summary>ConnectedDeviceId. </summary>
		ConnectedDeviceId,
		///<summary>ConnectionCodeId. </summary>
		ConnectionCodeId,
		///<summary>DeviceAbbr. </summary>
		DeviceAbbr,
		///<summary>DeviceDesc. </summary>
		DeviceDesc,
		///<summary>DeviceId. </summary>
		DeviceId,
		///<summary>DeviceName. </summary>
		DeviceName,
		///<summary>EntId. </summary>
		EntId,
		///<summary>HardwareTypeId. </summary>
		HardwareTypeId,
		///<summary>IpAddress. </summary>
		IpAddress,
		///<summary>IpPrintingPort. </summary>
		IpPrintingPort,
		///<summary>IpStatusPort. </summary>
		IpStatusPort,
		///<summary>KpBackupDeviceId. </summary>
		KpBackupDeviceId,
		///<summary>KpOptionGrpId. </summary>
		KpOptionGrpId,
		///<summary>ModelId. </summary>
		ModelId,
		///<summary>PortId. </summary>
		PortId,
		///<summary>PrimaryLanguageId. </summary>
		PrimaryLanguageId,
		///<summary>SecondaryLanguageId. </summary>
		SecondaryLanguageId,
		///<summary>StoreId. </summary>
		StoreId,
		///<summary>XferBaudRate. </summary>
		XferBaudRate,
		///<summary>XferDataBits. </summary>
		XferDataBits,
		///<summary>XferHandshakeId. </summary>
		XferHandshakeId,
		///<summary>XferParityId. </summary>
		XferParityId,
		///<summary>XferStopBits. </summary>
		XferStopBits,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: DiscoupMaster.</summary>
	public enum DiscoupMasterFieldIndex
	{
		///<summary>AssocTenderId. </summary>
		AssocTenderId,
		///<summary>BevRevClassFlag. </summary>
		BevRevClassFlag,
		///<summary>DiscountExtraPromptCode. </summary>
		DiscountExtraPromptCode,
		///<summary>DiscoupAbbr1. </summary>
		DiscoupAbbr1,
		///<summary>DiscoupAbbr2. </summary>
		DiscoupAbbr2,
		///<summary>DiscoupAmt. </summary>
		DiscoupAmt,
		///<summary>DiscoupId. </summary>
		DiscoupId,
		///<summary>DiscoupItemLevelCodeId. </summary>
		DiscoupItemLevelCodeId,
		///<summary>DiscoupMaxAmt. </summary>
		DiscoupMaxAmt,
		///<summary>DiscoupMaxPercent. </summary>
		DiscoupMaxPercent,
		///<summary>DiscoupName. </summary>
		DiscoupName,
		///<summary>DiscoupOpenCodeId. </summary>
		DiscoupOpenCodeId,
		///<summary>DiscoupPctAmtCodeId. </summary>
		DiscoupPctAmtCodeId,
		///<summary>DiscoupPercent. </summary>
		DiscoupPercent,
		///<summary>DiscoupTypeCodeId. </summary>
		DiscoupTypeCodeId,
		///<summary>EntId. </summary>
		EntId,
		///<summary>ExclusiveFlag. </summary>
		ExclusiveFlag,
		///<summary>FoodRevClassFlag. </summary>
		FoodRevClassFlag,
		///<summary>OtherRevClassFlag. </summary>
		OtherRevClassFlag,
		///<summary>PostAcctNo. </summary>
		PostAcctNo,
		///<summary>ProfitCtrGrpId. </summary>
		ProfitCtrGrpId,
		///<summary>PromptExtraDataFlag. </summary>
		PromptExtraDataFlag,
		///<summary>RoundBasis. </summary>
		RoundBasis,
		///<summary>RoundTypeId. </summary>
		RoundTypeId,
		///<summary>RowVersion. </summary>
		RowVersion,
		///<summary>SecurityId. </summary>
		SecurityId,
		///<summary>SodaRevClassFlag. </summary>
		SodaRevClassFlag,
		///<summary>StoreId. </summary>
		StoreId,
		///<summary>ThreshholdAmt. </summary>
		ThreshholdAmt,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: EmpGroupMaster.</summary>
	public enum EmpGroupMasterFieldIndex
	{
		///<summary>EmpAutoUpdateFlag. </summary>
		EmpAutoUpdateFlag,
		///<summary>EmpGrpAbbr1. </summary>
		EmpGrpAbbr1,
		///<summary>EmpGrpAbbr2. </summary>
		EmpGrpAbbr2,
		///<summary>EmpGrpId. </summary>
		EmpGrpId,
		///<summary>EmpGrpName. </summary>
		EmpGrpName,
		///<summary>EntId. </summary>
		EntId,
		///<summary>StoreId. </summary>
		StoreId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: EmpGrpEmpJoin.</summary>
	public enum EmpGrpEmpJoinFieldIndex
	{
		///<summary>EmpGrpId. </summary>
		EmpGrpId,
		///<summary>EmpId. </summary>
		EmpId,
		///<summary>EntId. </summary>
		EntId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: EmpJobcodeJoin.</summary>
	public enum EmpJobcodeJoinFieldIndex
	{
		///<summary>EmpId. </summary>
		EmpId,
		///<summary>EntId. </summary>
		EntId,
		///<summary>JobcodeAddDt. </summary>
		JobcodeAddDt,
		///<summary>JobcodeId. </summary>
		JobcodeId,
		///<summary>JobcodeLaborRate. </summary>
		JobcodeLaborRate,
		///<summary>ManagerFlag. </summary>
		ManagerFlag,
		///<summary>MultipleSignonFlag. </summary>
		MultipleSignonFlag,
		///<summary>PrimaryJobcodeFlag. </summary>
		PrimaryJobcodeFlag,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: EmpMaster.</summary>
	public enum EmpMasterFieldIndex
	{
		///<summary>DataControlGroupId. </summary>
		DataControlGroupId,
		///<summary>DefaultLanguageId. </summary>
		DefaultLanguageId,
		///<summary>EmpBirthdate. </summary>
		EmpBirthdate,
		///<summary>EmpCardNo. </summary>
		EmpCardNo,
		///<summary>EmpCity. </summary>
		EmpCity,
		///<summary>EmpCountry. </summary>
		EmpCountry,
		///<summary>EmpEmergencyTel. </summary>
		EmpEmergencyTel,
		///<summary>EmpFirstName. </summary>
		EmpFirstName,
		///<summary>EmpHireDt. </summary>
		EmpHireDt,
		///<summary>EmpId. </summary>
		EmpId,
		///<summary>EmpLastName. </summary>
		EmpLastName,
		///<summary>EmpLastReviewDt. </summary>
		EmpLastReviewDt,
		///<summary>EmpNextReviewDt. </summary>
		EmpNextReviewDt,
		///<summary>EmpPassword. </summary>
		EmpPassword,
		///<summary>EmpPosName. </summary>
		EmpPosName,
		///<summary>EmpSsn. </summary>
		EmpSsn,
		///<summary>EmpState. </summary>
		EmpState,
		///<summary>EmpStreet1. </summary>
		EmpStreet1,
		///<summary>EmpStreet2. </summary>
		EmpStreet2,
		///<summary>EmpTel. </summary>
		EmpTel,
		///<summary>EmpTerminateDt. </summary>
		EmpTerminateDt,
		///<summary>EmpZip. </summary>
		EmpZip,
		///<summary>EntId. </summary>
		EntId,
		///<summary>RowVersion. </summary>
		RowVersion,
		///<summary>SecondaryId. </summary>
		SecondaryId,
		///<summary>StoreId. </summary>
		StoreId,
		///<summary>SupervisorEmpId. </summary>
		SupervisorEmpId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: FuncBttnTextJoin.</summary>
	public enum FuncBttnTextJoinFieldIndex
	{
		///<summary>EntId. </summary>
		EntId,
		///<summary>FuncbttnId. </summary>
		FuncbttnId,
		///<summary>FuncbttnText. </summary>
		FuncbttnText,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Ga4680ExportParmMaster.</summary>
	public enum Ga4680ExportParmMasterFieldIndex
	{
		///<summary>BusinessDayOffset. </summary>
		BusinessDayOffset,
		///<summary>BusinessDayOptionId. </summary>
		BusinessDayOptionId,
		///<summary>DelimiterValue. </summary>
		DelimiterValue,
		///<summary>EntId. </summary>
		EntId,
		///<summary>FileFormatId. </summary>
		FileFormatId,
		///<summary>Ga1ExportPath. </summary>
		Ga1ExportPath,
		///<summary>Ga1RequiredFlag. </summary>
		Ga1RequiredFlag,
		///<summary>Ga2ExportPath. </summary>
		Ga2ExportPath,
		///<summary>Ga2RequiredFlag. </summary>
		Ga2RequiredFlag,
		///<summary>Ga3ExportPath. </summary>
		Ga3ExportPath,
		///<summary>Ga3RequiredFlag. </summary>
		Ga3RequiredFlag,
		///<summary>Ga4ExportPath. </summary>
		Ga4ExportPath,
		///<summary>Ga4RequiredFlag. </summary>
		Ga4RequiredFlag,
		///<summary>Ga5ExportPath. </summary>
		Ga5ExportPath,
		///<summary>Ga5RequiredFlag. </summary>
		Ga5RequiredFlag,
		///<summary>Ga6ExportPath. </summary>
		Ga6ExportPath,
		///<summary>Ga6RequiredFlag. </summary>
		Ga6RequiredFlag,
		///<summary>Ga7ExportPath. </summary>
		Ga7ExportPath,
		///<summary>Ga7RequiredFlag. </summary>
		Ga7RequiredFlag,
		///<summary>Ga8ExportPath. </summary>
		Ga8ExportPath,
		///<summary>Ga8RequiredFlag. </summary>
		Ga8RequiredFlag,
		///<summary>Ga9ExportPath. </summary>
		Ga9ExportPath,
		///<summary>Ga9RequiredFlag. </summary>
		Ga9RequiredFlag,
		///<summary>GaExportId. </summary>
		GaExportId,
		///<summary>LocationCode. </summary>
		LocationCode,
		///<summary>NumDays. </summary>
		NumDays,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Ga4680ImportParmMaster.</summary>
	public enum Ga4680ImportParmMasterFieldIndex
	{
		///<summary>DelimiterRequiredFlag. </summary>
		DelimiterRequiredFlag,
		///<summary>DelimiterValue. </summary>
		DelimiterValue,
		///<summary>EntId. </summary>
		EntId,
		///<summary>Ga1ImportPath. </summary>
		Ga1ImportPath,
		///<summary>Ga1PreimportOptionId. </summary>
		Ga1PreimportOptionId,
		///<summary>Ga1RequiredFlag. </summary>
		Ga1RequiredFlag,
		///<summary>Ga2ImportPath. </summary>
		Ga2ImportPath,
		///<summary>Ga2PreimportOptionId. </summary>
		Ga2PreimportOptionId,
		///<summary>Ga2RequiredFlag. </summary>
		Ga2RequiredFlag,
		///<summary>Ga3ImportPath. </summary>
		Ga3ImportPath,
		///<summary>Ga3PreimportOptionId. </summary>
		Ga3PreimportOptionId,
		///<summary>Ga3RequiredFlag. </summary>
		Ga3RequiredFlag,
		///<summary>Ga4ImportPath. </summary>
		Ga4ImportPath,
		///<summary>Ga4PreimportOptionId. </summary>
		Ga4PreimportOptionId,
		///<summary>Ga4RequiredFlag. </summary>
		Ga4RequiredFlag,
		///<summary>Ga5ImportPath. </summary>
		Ga5ImportPath,
		///<summary>Ga5PreimportOptionId. </summary>
		Ga5PreimportOptionId,
		///<summary>Ga5RequiredFlag. </summary>
		Ga5RequiredFlag,
		///<summary>Ga6ImportPath. </summary>
		Ga6ImportPath,
		///<summary>Ga6PreimportOptionId. </summary>
		Ga6PreimportOptionId,
		///<summary>Ga6RequiredFlag. </summary>
		Ga6RequiredFlag,
		///<summary>Ga7ImportPath. </summary>
		Ga7ImportPath,
		///<summary>Ga7PreimportOptionId. </summary>
		Ga7PreimportOptionId,
		///<summary>Ga7RequiredFlag. </summary>
		Ga7RequiredFlag,
		///<summary>Ga8ImportPath. </summary>
		Ga8ImportPath,
		///<summary>Ga8PreimportOptionId. </summary>
		Ga8PreimportOptionId,
		///<summary>Ga8RequiredFlag. </summary>
		Ga8RequiredFlag,
		///<summary>Ga9ImportPath. </summary>
		Ga9ImportPath,
		///<summary>Ga9PreimportOptionId. </summary>
		Ga9PreimportOptionId,
		///<summary>Ga9RequiredFlag. </summary>
		Ga9RequiredFlag,
		///<summary>GaImportId. </summary>
		GaImportId,
		///<summary>PreimportOptionId. </summary>
		PreimportOptionId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: GeneralLedgerMapMaster.</summary>
	public enum GeneralLedgerMapMasterFieldIndex
	{
		///<summary>EntId. </summary>
		EntId,
		///<summary>GlAccountNo. </summary>
		GlAccountNo,
		///<summary>GlClassId. </summary>
		GlClassId,
		///<summary>GlMapGenId. </summary>
		GlMapGenId,
		///<summary>Parm1ObjectId. </summary>
		Parm1ObjectId,
		///<summary>Parm2ObjectId. </summary>
		Parm2ObjectId,
		///<summary>ProfitCenterId. </summary>
		ProfitCenterId,
		///<summary>ValidPermutationFlag. </summary>
		ValidPermutationFlag,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: GiftCardMaster.</summary>
	public enum GiftCardMasterFieldIndex
	{
		///<summary>EntId. </summary>
		EntId,
		///<summary>GiftCardAbbr1. </summary>
		GiftCardAbbr1,
		///<summary>GiftCardAbbr2. </summary>
		GiftCardAbbr2,
		///<summary>GiftCardAmt. </summary>
		GiftCardAmt,
		///<summary>GiftCardId. </summary>
		GiftCardId,
		///<summary>GiftCardMaxAmt. </summary>
		GiftCardMaxAmt,
		///<summary>GiftCardMinAmt. </summary>
		GiftCardMinAmt,
		///<summary>GiftCardName. </summary>
		GiftCardName,
		///<summary>GiftCardOpenCodeId. </summary>
		GiftCardOpenCodeId,
		///<summary>MenuItemId. </summary>
		MenuItemId,
		///<summary>NodeId. </summary>
		NodeId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: GratuityCatMaster.</summary>
	public enum GratuityCatMasterFieldIndex
	{
		///<summary>EntId. </summary>
		EntId,
		///<summary>GratCatAbbr1. </summary>
		GratCatAbbr1,
		///<summary>GratCatAbbr2. </summary>
		GratCatAbbr2,
		///<summary>GratCatId. </summary>
		GratCatId,
		///<summary>GratCatName. </summary>
		GratCatName,
		///<summary>GratCatSvsChgFlag. </summary>
		GratCatSvsChgFlag,
		///<summary>LoyaltyEarnEligibleFlag. </summary>
		LoyaltyEarnEligibleFlag,
		///<summary>LoyaltyRedeemEligibleFlag. </summary>
		LoyaltyRedeemEligibleFlag,
		///<summary>RowVersion. </summary>
		RowVersion,
		///<summary>StoreId. </summary>
		StoreId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: GratuityMaster.</summary>
	public enum GratuityMasterFieldIndex
	{
		///<summary>EntId. </summary>
		EntId,
		///<summary>GratAbbr1. </summary>
		GratAbbr1,
		///<summary>GratAbbr2. </summary>
		GratAbbr2,
		///<summary>GratAmt. </summary>
		GratAmt,
		///<summary>GratCatId. </summary>
		GratCatId,
		///<summary>GratId. </summary>
		GratId,
		///<summary>GratName. </summary>
		GratName,
		///<summary>GratPercent. </summary>
		GratPercent,
		///<summary>GratPerCoverAmt. </summary>
		GratPerCoverAmt,
		///<summary>GratRecipientCodeId. </summary>
		GratRecipientCodeId,
		///<summary>GratRecipientEmpId. </summary>
		GratRecipientEmpId,
		///<summary>GratRemovableFlag. </summary>
		GratRemovableFlag,
		///<summary>GratTermPayFlag. </summary>
		GratTermPayFlag,
		///<summary>GratuityIsInclusiveFlag. </summary>
		GratuityIsInclusiveFlag,
		///<summary>InclusiveGratuityConvertedFlag. </summary>
		InclusiveGratuityConvertedFlag,
		///<summary>MaxCoverThreshhold. </summary>
		MaxCoverThreshhold,
		///<summary>MinCoverThreshhold. </summary>
		MinCoverThreshhold,
		///<summary>PercentOrAmountFlag. </summary>
		PercentOrAmountFlag,
		///<summary>RoundBasis. </summary>
		RoundBasis,
		///<summary>RoundTypeId. </summary>
		RoundTypeId,
		///<summary>RowVersion. </summary>
		RowVersion,
		///<summary>SalesCalculationOnNetOrGrossFlag. </summary>
		SalesCalculationOnNetOrGrossFlag,
		///<summary>StoreId. </summary>
		StoreId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: IdMaster.</summary>
	public enum IdMasterFieldIndex
	{
		///<summary>CorpId. </summary>
		CorpId,
		///<summary>DivId. </summary>
		DivId,
		///<summary>EntId. </summary>
		EntId,
		///<summary>LastUsedId. </summary>
		LastUsedId,
		///<summary>StoreId. </summary>
		StoreId,
		///<summary>TableName. </summary>
		TableName,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ImageLibraryMaster.</summary>
	public enum ImageLibraryMasterFieldIndex
	{
		///<summary>EntId. </summary>
		EntId,
		///<summary>Image. </summary>
		Image,
		///<summary>ImageExtension. </summary>
		ImageExtension,
		///<summary>ImageId. </summary>
		ImageId,
		///<summary>ImageName. </summary>
		ImageName,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: IntlDescriptorTextJoin.</summary>
	public enum IntlDescriptorTextJoinFieldIndex
	{
		///<summary>ControlId. </summary>
		ControlId,
		///<summary>ControlText. </summary>
		ControlText,
		///<summary>CustomerId. </summary>
		CustomerId,
		///<summary>EntId. </summary>
		EntId,
		///<summary>LanguageId. </summary>
		LanguageId,
		///<summary>NodeId. </summary>
		NodeId,
		///<summary>ObjectId. </summary>
		ObjectId,
		///<summary>TranslatedFlag. </summary>
		TranslatedFlag,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: IpServerAttribMaster.</summary>
	public enum IpServerAttribMasterFieldIndex
	{
		///<summary>CurrentVersion. </summary>
		CurrentVersion,
		///<summary>EntId. </summary>
		EntId,
		///<summary>IpAddress. </summary>
		IpAddress,
		///<summary>IpTypeCodeId. </summary>
		IpTypeCodeId,
		///<summary>ServerAttribDesc. </summary>
		ServerAttribDesc,
		///<summary>ServerAttribId. </summary>
		ServerAttribId,
		///<summary>ServerHostName. </summary>
		ServerHostName,
		///<summary>StoreId. </summary>
		StoreId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: JobcodeFuncBttnJoin.</summary>
	public enum JobcodeFuncBttnJoinFieldIndex
	{
		///<summary>EnabledFlag. </summary>
		EnabledFlag,
		///<summary>EntId. </summary>
		EntId,
		///<summary>FuncbttnId. </summary>
		FuncbttnId,
		///<summary>JobcodeId. </summary>
		JobcodeId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: JobcodeFunctJoin.</summary>
	public enum JobcodeFunctJoinFieldIndex
	{
		///<summary>EntId. </summary>
		EntId,
		///<summary>FunctFlag. </summary>
		FunctFlag,
		///<summary>FunctId. </summary>
		FunctId,
		///<summary>JobcodeId. </summary>
		JobcodeId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: JobcodeMaster.</summary>
	public enum JobcodeMasterFieldIndex
	{
		///<summary>EntId. </summary>
		EntId,
		///<summary>JobcodeAbbr. </summary>
		JobcodeAbbr,
		///<summary>JobcodeAbbr2. </summary>
		JobcodeAbbr2,
		///<summary>JobcodeCarryoverTipsFlag. </summary>
		JobcodeCarryoverTipsFlag,
		///<summary>JobcodeId. </summary>
		JobcodeId,
		///<summary>JobcodeLaborRate. </summary>
		JobcodeLaborRate,
		///<summary>JobcodeName. </summary>
		JobcodeName,
		///<summary>JobcodePrintCashFlag. </summary>
		JobcodePrintCashFlag,
		///<summary>JobcodePrintCashierFlag. </summary>
		JobcodePrintCashierFlag,
		///<summary>JobcodePrintEmpIdFlag. </summary>
		JobcodePrintEmpIdFlag,
		///<summary>JobcodePrintServerFlag. </summary>
		JobcodePrintServerFlag,
		///<summary>JobcodeSecId. </summary>
		JobcodeSecId,
		///<summary>JobcodeTipPrompt. </summary>
		JobcodeTipPrompt,
		///<summary>RowVersion. </summary>
		RowVersion,
		///<summary>StoreId. </summary>
		StoreId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: KdsCategoryMaster.</summary>
	public enum KdsCategoryMasterFieldIndex
	{
		///<summary>DivisionId. </summary>
		DivisionId,
		///<summary>EntId. </summary>
		EntId,
		///<summary>KdsCategoryId. </summary>
		KdsCategoryId,
		///<summary>KdsCategoryName. </summary>
		KdsCategoryName,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: KdsVideoMaster.</summary>
	public enum KdsVideoMasterFieldIndex
	{
		///<summary>DivisionId. </summary>
		DivisionId,
		///<summary>EntId. </summary>
		EntId,
		///<summary>KdsVideoId. </summary>
		KdsVideoId,
		///<summary>KdsVideoName. </summary>
		KdsVideoName,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: KpOptionGrpCopyJoin.</summary>
	public enum KpOptionGrpCopyJoinFieldIndex
	{
		///<summary>BeepFlag. </summary>
		BeepFlag,
		///<summary>CutCopyFlag. </summary>
		CutCopyFlag,
		///<summary>EntId. </summary>
		EntId,
		///<summary>ExtraCodeId. </summary>
		ExtraCodeId,
		///<summary>KpCopiesSeq. </summary>
		KpCopiesSeq,
		///<summary>KpOptionGrpId. </summary>
		KpOptionGrpId,
		///<summary>NumLinesAfter. </summary>
		NumLinesAfter,
		///<summary>NumLinesBefore. </summary>
		NumLinesBefore,
		///<summary>PrintHeaderFlag. </summary>
		PrintHeaderFlag,
		///<summary>SortCodeId. </summary>
		SortCodeId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: KpOptionGrpFormatJoin.</summary>
	public enum KpOptionGrpFormatJoinFieldIndex
	{
		///<summary>AddLineFlag. </summary>
		AddLineFlag,
		///<summary>CharsizeCodeId. </summary>
		CharsizeCodeId,
		///<summary>ColorCodeId. </summary>
		ColorCodeId,
		///<summary>EntId. </summary>
		EntId,
		///<summary>FormatLabel. </summary>
		FormatLabel,
		///<summary>KpFormatId. </summary>
		KpFormatId,
		///<summary>KpOptionGrpId. </summary>
		KpOptionGrpId,
		///<summary>PrintFlag. </summary>
		PrintFlag,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: KpOptionGrpMaster.</summary>
	public enum KpOptionGrpMasterFieldIndex
	{
		///<summary>EntId. </summary>
		EntId,
		///<summary>KpOptionGrpAbbr1. </summary>
		KpOptionGrpAbbr1,
		///<summary>KpOptionGrpAbbr2. </summary>
		KpOptionGrpAbbr2,
		///<summary>KpOptionGrpId. </summary>
		KpOptionGrpId,
		///<summary>KpOptionGrpName. </summary>
		KpOptionGrpName,
		///<summary>StoreId. </summary>
		StoreId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: KpPrinterMaster.</summary>
	public enum KpPrinterMasterFieldIndex
	{
		///<summary>EntId. </summary>
		EntId,
		///<summary>KpPrinterId. </summary>
		KpPrinterId,
		///<summary>KpPrinterName. </summary>
		KpPrinterName,
		///<summary>StoreId. </summary>
		StoreId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: KpRouteMaster.</summary>
	public enum KpRouteMasterFieldIndex
	{
		///<summary>EntId. </summary>
		EntId,
		///<summary>KpRouteId. </summary>
		KpRouteId,
		///<summary>KpRouteName. </summary>
		KpRouteName,
		///<summary>StoreId. </summary>
		StoreId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: KpRoutePrinterJoin.</summary>
	public enum KpRoutePrinterJoinFieldIndex
	{
		///<summary>EntId. </summary>
		EntId,
		///<summary>KpDeviceId. </summary>
		KpDeviceId,
		///<summary>KpPrinterId. </summary>
		KpPrinterId,
		///<summary>KpRouteId. </summary>
		KpRouteId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: MachineOptionsMaster.</summary>
	public enum MachineOptionsMasterFieldIndex
	{
		///<summary>OptionName. </summary>
		OptionName,
		///<summary>OptionValue. </summary>
		OptionValue,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: MealPeriodMaster.</summary>
	public enum MealPeriodMasterFieldIndex
	{
		///<summary>DefaultCheckTypeId. </summary>
		DefaultCheckTypeId,
		///<summary>DefaultPriceLevelId. </summary>
		DefaultPriceLevelId,
		///<summary>EntertainmentFlag. </summary>
		EntertainmentFlag,
		///<summary>EntId. </summary>
		EntId,
		///<summary>MealPeriodAbbr1. </summary>
		MealPeriodAbbr1,
		///<summary>MealPeriodAbbr2. </summary>
		MealPeriodAbbr2,
		///<summary>MealPeriodId. </summary>
		MealPeriodId,
		///<summary>MealPeriodName. </summary>
		MealPeriodName,
		///<summary>MealPeriodSecId. </summary>
		MealPeriodSecId,
		///<summary>ReceiptCode. </summary>
		ReceiptCode,
		///<summary>RowVersion. </summary>
		RowVersion,
		///<summary>StoreId. </summary>
		StoreId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: MembershipMaster.</summary>
	public enum MembershipMasterFieldIndex
	{
		///<summary>EntId. </summary>
		EntId,
		///<summary>MembershipCode. </summary>
		MembershipCode,
		///<summary>MembershipId. </summary>
		MembershipId,
		///<summary>MembershipName. </summary>
		MembershipName,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: MembershipProfitCenterJoin.</summary>
	public enum MembershipProfitCenterJoinFieldIndex
	{
		///<summary>CheckTypeId. </summary>
		CheckTypeId,
		///<summary>EntId. </summary>
		EntId,
		///<summary>MembershipId. </summary>
		MembershipId,
		///<summary>ProfitCenterId. </summary>
		ProfitCenterId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: MenuBttnObjJoin.</summary>
	public enum MenuBttnObjJoinFieldIndex
	{
		///<summary>ButtonBackcolor. </summary>
		ButtonBackcolor,
		///<summary>ButtonBorderStyle. </summary>
		ButtonBorderStyle,
		///<summary>ButtonColumn. </summary>
		ButtonColumn,
		///<summary>ButtonFontId. </summary>
		ButtonFontId,
		///<summary>ButtonForecolor. </summary>
		ButtonForecolor,
		///<summary>ButtonHeight. </summary>
		ButtonHeight,
		///<summary>ButtonId. </summary>
		ButtonId,
		///<summary>ButtonImageId. </summary>
		ButtonImageId,
		///<summary>ButtonNextMenuId. </summary>
		ButtonNextMenuId,
		///<summary>ButtonRow. </summary>
		ButtonRow,
		///<summary>ButtonShapeId. </summary>
		ButtonShapeId,
		///<summary>ButtonWidth. </summary>
		ButtonWidth,
		///<summary>DisplayImageFlag. </summary>
		DisplayImageFlag,
		///<summary>EntId. </summary>
		EntId,
		///<summary>ErrorCodeId. </summary>
		ErrorCodeId,
		///<summary>MenuId. </summary>
		MenuId,
		///<summary>ObjectId. </summary>
		ObjectId,
		///<summary>ObjectTypeId. </summary>
		ObjectTypeId,
		///<summary>ScreenId. </summary>
		ScreenId,
		///<summary>StoreCreatedId. </summary>
		StoreCreatedId,
		///<summary>TerminalTypeId. </summary>
		TerminalTypeId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: MenuItemGroupMaster.</summary>
	public enum MenuItemGroupMasterFieldIndex
	{
		///<summary>EntId. </summary>
		EntId,
		///<summary>MenuItemGroupId. </summary>
		MenuItemGroupId,
		///<summary>MenuItemGroupName. </summary>
		MenuItemGroupName,
		///<summary>StoreId. </summary>
		StoreId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: MenuItemMaster.</summary>
	public enum MenuItemMasterFieldIndex
	{
		///<summary>BargunId. </summary>
		BargunId,
		///<summary>Covers. </summary>
		Covers,
		///<summary>DataControlGroupId. </summary>
		DataControlGroupId,
		///<summary>DefaultImageId. </summary>
		DefaultImageId,
		///<summary>EntId. </summary>
		EntId,
		///<summary>KdsCategoryId. </summary>
		KdsCategoryId,
		///<summary>KdsCookTime. </summary>
		KdsCookTime,
		///<summary>KdsVideoId. </summary>
		KdsVideoId,
		///<summary>KdsVideoLabel. </summary>
		KdsVideoLabel,
		///<summary>MenuItemAbbr1. </summary>
		MenuItemAbbr1,
		///<summary>MenuItemAbbr2. </summary>
		MenuItemAbbr2,
		///<summary>MenuItemGroupId. </summary>
		MenuItemGroupId,
		///<summary>MenuItemId. </summary>
		MenuItemId,
		///<summary>MenuItemName. </summary>
		MenuItemName,
		///<summary>MiCostAmt. </summary>
		MiCostAmt,
		///<summary>MiDiscountableFlag. </summary>
		MiDiscountableFlag,
		///<summary>MiEmpDiscountableFlag. </summary>
		MiEmpDiscountableFlag,
		///<summary>MiKpLabel. </summary>
		MiKpLabel,
		///<summary>MiNotActiveFlag. </summary>
		MiNotActiveFlag,
		///<summary>MiOpenPricePrompt. </summary>
		MiOpenPricePrompt,
		///<summary>MiPriceOverrideFlag. </summary>
		MiPriceOverrideFlag,
		///<summary>MiPrintFlag. </summary>
		MiPrintFlag,
		///<summary>MiReceiptLabel. </summary>
		MiReceiptLabel,
		///<summary>MiSecId. </summary>
		MiSecId,
		///<summary>MiTaxInclFlag. </summary>
		MiTaxInclFlag,
		///<summary>MiVoidableFlag. </summary>
		MiVoidableFlag,
		///<summary>MiWeightFlag. </summary>
		MiWeightFlag,
		///<summary>MiWeightTare. </summary>
		MiWeightTare,
		///<summary>OpenModifierFlag. </summary>
		OpenModifierFlag,
		///<summary>ProdClassId. </summary>
		ProdClassId,
		///<summary>RevCatId. </summary>
		RevCatId,
		///<summary>RowVersion. </summary>
		RowVersion,
		///<summary>RptCatId. </summary>
		RptCatId,
		///<summary>SkuNo. </summary>
		SkuNo,
		///<summary>StoreCreatedId. </summary>
		StoreCreatedId,
		///<summary>StoreId. </summary>
		StoreId,
		///<summary>TaxGrpId. </summary>
		TaxGrpId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: MenuMaster.</summary>
	public enum MenuMasterFieldIndex
	{
		///<summary>AllowButtonOverlapFlag. </summary>
		AllowButtonOverlapFlag,
		///<summary>ButtonPadding. </summary>
		ButtonPadding,
		///<summary>DataControlGroupId. </summary>
		DataControlGroupId,
		///<summary>EntId. </summary>
		EntId,
		///<summary>GridSize. </summary>
		GridSize,
		///<summary>MenuAbbr1. </summary>
		MenuAbbr1,
		///<summary>MenuAbbr2. </summary>
		MenuAbbr2,
		///<summary>MenuId. </summary>
		MenuId,
		///<summary>MenuName. </summary>
		MenuName,
		///<summary>ScreenSizeId. </summary>
		ScreenSizeId,
		///<summary>ScreenTypeId. </summary>
		ScreenTypeId,
		///<summary>SnapToGridFlag. </summary>
		SnapToGridFlag,
		///<summary>StoreId. </summary>
		StoreId,
		///<summary>SystemScreenFlag. </summary>
		SystemScreenFlag,
		///<summary>ThemeId. </summary>
		ThemeId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: MenuMigrpJoin.</summary>
	public enum MenuMigrpJoinFieldIndex
	{
		///<summary>EntId. </summary>
		EntId,
		///<summary>MenuId. </summary>
		MenuId,
		///<summary>MenuItemGroupId. </summary>
		MenuItemGroupId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: MiChoiceGrpJoin.</summary>
	public enum MiChoiceGrpJoinFieldIndex
	{
		///<summary>ChoiceGroupId. </summary>
		ChoiceGroupId,
		///<summary>EntId. </summary>
		EntId,
		///<summary>MenuItemId. </summary>
		MenuItemId,
		///<summary>MiCgJoinId. </summary>
		MiCgJoinId,
		///<summary>MiChoicegrpSeq. </summary>
		MiChoicegrpSeq,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: MiKpprinterJoin.</summary>
	public enum MiKpprinterJoinFieldIndex
	{
		///<summary>EntId. </summary>
		EntId,
		///<summary>KpPrinterId. </summary>
		KpPrinterId,
		///<summary>MenuItemId. </summary>
		MenuItemId,
		///<summary>PrimaryPrinterFlag. </summary>
		PrimaryPrinterFlag,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: MiPriceJoin.</summary>
	public enum MiPriceJoinFieldIndex
	{
		///<summary>EntId. </summary>
		EntId,
		///<summary>MenuItemId. </summary>
		MenuItemId,
		///<summary>PriceLast. </summary>
		PriceLast,
		///<summary>PriceLevelId. </summary>
		PriceLevelId,
		///<summary>PriceNet. </summary>
		PriceNet,
		///<summary>StoreCreatedId. </summary>
		StoreCreatedId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: MiSkuJoin.</summary>
	public enum MiSkuJoinFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EntId. </summary>
		EntId,
		///<summary>MenuItemId. </summary>
		MenuItemId,
		///<summary>SkuGenId. </summary>
		SkuGenId,
		///<summary>SkuNo. </summary>
		SkuNo,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ModifierKpprinterJoin.</summary>
	public enum ModifierKpprinterJoinFieldIndex
	{
		///<summary>EntId. </summary>
		EntId,
		///<summary>KpPrinterId. </summary>
		KpPrinterId,
		///<summary>ModifierId. </summary>
		ModifierId,
		///<summary>PrimaryPrinterFlag. </summary>
		PrimaryPrinterFlag,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ModifierMaster.</summary>
	public enum ModifierMasterFieldIndex
	{
		///<summary>DataControlGroupId. </summary>
		DataControlGroupId,
		///<summary>DiscoupId. </summary>
		DiscoupId,
		///<summary>EntId. </summary>
		EntId,
		///<summary>KdsVideoLabel. </summary>
		KdsVideoLabel,
		///<summary>ModifierAbbr1. </summary>
		ModifierAbbr1,
		///<summary>ModifierAbbr2. </summary>
		ModifierAbbr2,
		///<summary>ModifierExtraChoicegrpId. </summary>
		ModifierExtraChoicegrpId,
		///<summary>ModifierId. </summary>
		ModifierId,
		///<summary>ModifierKpLabel. </summary>
		ModifierKpLabel,
		///<summary>ModifierMenuItemId. </summary>
		ModifierMenuItemId,
		///<summary>ModifierName. </summary>
		ModifierName,
		///<summary>ModifierPrintFlag. </summary>
		ModifierPrintFlag,
		///<summary>ModifierSecId. </summary>
		ModifierSecId,
		///<summary>ModifierWithMiFlag. </summary>
		ModifierWithMiFlag,
		///<summary>RowVersion. </summary>
		RowVersion,
		///<summary>StoreCreatedId. </summary>
		StoreCreatedId,
		///<summary>StoreId. </summary>
		StoreId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ModifierPriceJoin.</summary>
	public enum ModifierPriceJoinFieldIndex
	{
		///<summary>EntId. </summary>
		EntId,
		///<summary>ModifierId. </summary>
		ModifierId,
		///<summary>PriceLast. </summary>
		PriceLast,
		///<summary>PriceLevelId. </summary>
		PriceLevelId,
		///<summary>PriceNet. </summary>
		PriceNet,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: OptionsMaster.</summary>
	public enum OptionsMasterFieldIndex
	{
		///<summary>DualFontName. </summary>
		DualFontName,
		///<summary>EntId. </summary>
		EntId,
		///<summary>ThemeLanguageId. </summary>
		ThemeLanguageId,
		///<summary>UdUserFlag. </summary>
		UdUserFlag,
		///<summary>UnicodeFlag. </summary>
		UnicodeFlag,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PriceLevelMaster.</summary>
	public enum PriceLevelMasterFieldIndex
	{
		///<summary>EntId. </summary>
		EntId,
		///<summary>PriceLevelId. </summary>
		PriceLevelId,
		///<summary>PriceLevelName. </summary>
		PriceLevelName,
		///<summary>RowVersion. </summary>
		RowVersion,
		///<summary>StoreId. </summary>
		StoreId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ProcessMaster.</summary>
	public enum ProcessMasterFieldIndex
	{
		///<summary>EntId. </summary>
		EntId,
		///<summary>InstalledServerId. </summary>
		InstalledServerId,
		///<summary>ProcessDesc. </summary>
		ProcessDesc,
		///<summary>ProcessId. </summary>
		ProcessId,
		///<summary>ProcessName. </summary>
		ProcessName,
		///<summary>ProcessTypeId. </summary>
		ProcessTypeId,
		///<summary>StoreId. </summary>
		StoreId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ProductClassMaster.</summary>
	public enum ProductClassMasterFieldIndex
	{
		///<summary>DefaultMenuItemGroupId. </summary>
		DefaultMenuItemGroupId,
		///<summary>DefaultRevCatId. </summary>
		DefaultRevCatId,
		///<summary>DefaultRptCatId. </summary>
		DefaultRptCatId,
		///<summary>DefaultSecId. </summary>
		DefaultSecId,
		///<summary>DefaultTaxGrpId. </summary>
		DefaultTaxGrpId,
		///<summary>EntId. </summary>
		EntId,
		///<summary>ItemRestrictedFlag. </summary>
		ItemRestrictedFlag,
		///<summary>ProdClassId. </summary>
		ProdClassId,
		///<summary>ProdClassName. </summary>
		ProdClassName,
		///<summary>RowVersion. </summary>
		RowVersion,
		///<summary>StoreId. </summary>
		StoreId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ProfitCenterDayPartJoin.</summary>
	public enum ProfitCenterDayPartJoinFieldIndex
	{
		///<summary>DayPartName. </summary>
		DayPartName,
		///<summary>DayPartStartTime. </summary>
		DayPartStartTime,
		///<summary>ProfitCenterId. </summary>
		ProfitCenterId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ProfitCenterGroupJoin.</summary>
	public enum ProfitCenterGroupJoinFieldIndex
	{
		///<summary>EntId. </summary>
		EntId,
		///<summary>ProfitCenterId. </summary>
		ProfitCenterId,
		///<summary>ProfitCtrGrpId. </summary>
		ProfitCtrGrpId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ProfitCenterGroupMaster.</summary>
	public enum ProfitCenterGroupMasterFieldIndex
	{
		///<summary>ChargingPattern. </summary>
		ChargingPattern,
		///<summary>DataControlGroupId. </summary>
		DataControlGroupId,
		///<summary>EntId. </summary>
		EntId,
		///<summary>ProfitCtrAutoUpdateFlag. </summary>
		ProfitCtrAutoUpdateFlag,
		///<summary>ProfitCtrGrpAbbr1. </summary>
		ProfitCtrGrpAbbr1,
		///<summary>ProfitCtrGrpAbbr2. </summary>
		ProfitCtrGrpAbbr2,
		///<summary>ProfitCtrGrpId. </summary>
		ProfitCtrGrpId,
		///<summary>ProfitCtrGrpName. </summary>
		ProfitCtrGrpName,
		///<summary>StoreId. </summary>
		StoreId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ProfitCenterMaster.</summary>
	public enum ProfitCenterMasterFieldIndex
	{
		///<summary>BypassCcAgencyThresholdAmount. </summary>
		BypassCcAgencyThresholdAmount,
		///<summary>BypassCcPrintingThresholdAmount. </summary>
		BypassCcPrintingThresholdAmount,
		///<summary>BypassCcVoiceAuthThresholdAmount. </summary>
		BypassCcVoiceAuthThresholdAmount,
		///<summary>ChkFtrLine1. </summary>
		ChkFtrLine1,
		///<summary>ChkFtrLine2. </summary>
		ChkFtrLine2,
		///<summary>ChkFtrLine3. </summary>
		ChkFtrLine3,
		///<summary>ChkHdrLine1. </summary>
		ChkHdrLine1,
		///<summary>ChkHdrLine2. </summary>
		ChkHdrLine2,
		///<summary>ChkHdrLine3. </summary>
		ChkHdrLine3,
		///<summary>DataControlGroupId. </summary>
		DataControlGroupId,
		///<summary>DefaultTableLayoutId. </summary>
		DefaultTableLayoutId,
		///<summary>DocLinesAdvance. </summary>
		DocLinesAdvance,
		///<summary>EntId. </summary>
		EntId,
		///<summary>MaxDocLinesPage. </summary>
		MaxDocLinesPage,
		///<summary>MerchantId. </summary>
		MerchantId,
		///<summary>MinRcptLinesPage. </summary>
		MinRcptLinesPage,
		///<summary>PoleDisplayClosed. </summary>
		PoleDisplayClosed,
		///<summary>PoleDisplayOpen. </summary>
		PoleDisplayOpen,
		///<summary>PrimaryLanguageId. </summary>
		PrimaryLanguageId,
		///<summary>PrintByRevCatFlag. </summary>
		PrintByRevCatFlag,
		///<summary>ProfitCenterDesc. </summary>
		ProfitCenterDesc,
		///<summary>ProfitCenterId. </summary>
		ProfitCenterId,
		///<summary>ProfitCenterName. </summary>
		ProfitCenterName,
		///<summary>ProfitCtrAbbr1. </summary>
		ProfitCtrAbbr1,
		///<summary>ProfitCtrAbbr2. </summary>
		ProfitCtrAbbr2,
		///<summary>RowVersion. </summary>
		RowVersion,
		///<summary>SalesTippableFlag. </summary>
		SalesTippableFlag,
		///<summary>SecondaryLanguageId. </summary>
		SecondaryLanguageId,
		///<summary>SourcePropertyCode. </summary>
		SourcePropertyCode,
		///<summary>StoreId. </summary>
		StoreId,
		///<summary>TipEnforcementCodeId. </summary>
		TipEnforcementCodeId,
		///<summary>TipMaxPercent. </summary>
		TipMaxPercent,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ProfitCenterReceiptPrinterJoin.</summary>
	public enum ProfitCenterReceiptPrinterJoinFieldIndex
	{
		///<summary>EntId. </summary>
		EntId,
		///<summary>PrimaryFlag. </summary>
		PrimaryFlag,
		///<summary>ProfitCenterId. </summary>
		ProfitCenterId,
		///<summary>ReceiptPrinterId. </summary>
		ReceiptPrinterId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ProfitCenterTableJoin.</summary>
	public enum ProfitCenterTableJoinFieldIndex
	{
		///<summary>BoothFlag. </summary>
		BoothFlag,
		///<summary>EntId. </summary>
		EntId,
		///<summary>MaxSeats. </summary>
		MaxSeats,
		///<summary>OutsideFlag. </summary>
		OutsideFlag,
		///<summary>PrivateFlag. </summary>
		PrivateFlag,
		///<summary>ProfitCenterId. </summary>
		ProfitCenterId,
		///<summary>SecurityId. </summary>
		SecurityId,
		///<summary>SmokingFlag. </summary>
		SmokingFlag,
		///<summary>TableId. </summary>
		TableId,
		///<summary>TableName. </summary>
		TableName,
		///<summary>TableNo. </summary>
		TableNo,
		///<summary>ViewFlag. </summary>
		ViewFlag,
		///<summary>VipFlag. </summary>
		VipFlag,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: QuickTenderMaster.</summary>
	public enum QuickTenderMasterFieldIndex
	{
		///<summary>EntId. </summary>
		EntId,
		///<summary>PaymentAmount. </summary>
		PaymentAmount,
		///<summary>QuickTenderAbbr1. </summary>
		QuickTenderAbbr1,
		///<summary>QuickTenderAbbr2. </summary>
		QuickTenderAbbr2,
		///<summary>QuickTenderId. </summary>
		QuickTenderId,
		///<summary>QuickTenderName. </summary>
		QuickTenderName,
		///<summary>StoreId. </summary>
		StoreId,
		///<summary>TenderId. </summary>
		TenderId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ReceiptPrinterMaster.</summary>
	public enum ReceiptPrinterMasterFieldIndex
	{
		///<summary>EntId. </summary>
		EntId,
		///<summary>IpAddress. </summary>
		IpAddress,
		///<summary>IpPrintingPort. </summary>
		IpPrintingPort,
		///<summary>IpStatusPort. </summary>
		IpStatusPort,
		///<summary>ReceiptPrinterDescription. </summary>
		ReceiptPrinterDescription,
		///<summary>ReceiptPrinterId. </summary>
		ReceiptPrinterId,
		///<summary>ReceiptPrinterName. </summary>
		ReceiptPrinterName,
		///<summary>RpInvertedFlag. </summary>
		RpInvertedFlag,
		///<summary>RpLeftMarginOffset. </summary>
		RpLeftMarginOffset,
		///<summary>StoreId. </summary>
		StoreId,
		///<summary>UseDefaultPrinterFontFlag. </summary>
		UseDefaultPrinterFontFlag,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ReportCatMaster.</summary>
	public enum ReportCatMasterFieldIndex
	{
		///<summary>EntId. </summary>
		EntId,
		///<summary>RowVersion. </summary>
		RowVersion,
		///<summary>RptCatAbbr1. </summary>
		RptCatAbbr1,
		///<summary>RptCatAbbr2. </summary>
		RptCatAbbr2,
		///<summary>RptCatId. </summary>
		RptCatId,
		///<summary>RptCatName. </summary>
		RptCatName,
		///<summary>RptSummaryId. </summary>
		RptSummaryId,
		///<summary>StoreId. </summary>
		StoreId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: RevenueCatMaster.</summary>
	public enum RevenueCatMasterFieldIndex
	{
		///<summary>EntId. </summary>
		EntId,
		///<summary>LoyaltyEarnEligibleFlag. </summary>
		LoyaltyEarnEligibleFlag,
		///<summary>LoyaltyRedeemEligibleFlag. </summary>
		LoyaltyRedeemEligibleFlag,
		///<summary>RevCatAbbr1. </summary>
		RevCatAbbr1,
		///<summary>RevCatAbbr2. </summary>
		RevCatAbbr2,
		///<summary>RevCatId. </summary>
		RevCatId,
		///<summary>RevCatName. </summary>
		RevCatName,
		///<summary>RevClassId. </summary>
		RevClassId,
		///<summary>RowVersion. </summary>
		RowVersion,
		///<summary>RptCatId. </summary>
		RptCatId,
		///<summary>SalesTippableFlag. </summary>
		SalesTippableFlag,
		///<summary>StoreId. </summary>
		StoreId,
		///<summary>TenderRestrictionFlag. </summary>
		TenderRestrictionFlag,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: RevenueClassMaster.</summary>
	public enum RevenueClassMasterFieldIndex
	{
		///<summary>EntId. </summary>
		EntId,
		///<summary>RevClassAlphaId. </summary>
		RevClassAlphaId,
		///<summary>RevClassId. </summary>
		RevClassId,
		///<summary>RevClassName. </summary>
		RevClassName,
		///<summary>RowVersion. </summary>
		RowVersion,
		///<summary>StoreId. </summary>
		StoreId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: RowLockMaster.</summary>
	public enum RowLockMasterFieldIndex
	{
		///<summary>EntId. </summary>
		EntId,
		///<summary>LockTimeStamp. </summary>
		LockTimeStamp,
		///<summary>TableId. </summary>
		TableId,
		///<summary>TableName. </summary>
		TableName,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ScreenTemplateMaster.</summary>
	public enum ScreenTemplateMasterFieldIndex
	{
		///<summary>AllowButtonOverlapFlag. </summary>
		AllowButtonOverlapFlag,
		///<summary>ButtonPadding. </summary>
		ButtonPadding,
		///<summary>DefaultBackcolor. </summary>
		DefaultBackcolor,
		///<summary>DefaultHeight. </summary>
		DefaultHeight,
		///<summary>DefaultWidth. </summary>
		DefaultWidth,
		///<summary>EntId. </summary>
		EntId,
		///<summary>GridSize. </summary>
		GridSize,
		///<summary>LockDownFlag. </summary>
		LockDownFlag,
		///<summary>ScreenTemplateId. </summary>
		ScreenTemplateId,
		///<summary>ScreenTemplateName. </summary>
		ScreenTemplateName,
		///<summary>ScreenTemplateText. </summary>
		ScreenTemplateText,
		///<summary>SnapToGridFlag. </summary>
		SnapToGridFlag,
		///<summary>StoreFlag. </summary>
		StoreFlag,
		///<summary>StoreId. </summary>
		StoreId,
		///<summary>ThemeId. </summary>
		ThemeId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ScreenTemplateMigrpJoin.</summary>
	public enum ScreenTemplateMigrpJoinFieldIndex
	{
		///<summary>EntId. </summary>
		EntId,
		///<summary>MenuItemGroupId. </summary>
		MenuItemGroupId,
		///<summary>ScreenTemplateId. </summary>
		ScreenTemplateId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SecurityLevelMaster.</summary>
	public enum SecurityLevelMasterFieldIndex
	{
		///<summary>EntId. </summary>
		EntId,
		///<summary>SecurityId. </summary>
		SecurityId,
		///<summary>SecurityLevelName. </summary>
		SecurityLevelName,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SelectionGroupItemJoin.</summary>
	public enum SelectionGroupItemJoinFieldIndex
	{
		///<summary>AdjustedAmt. </summary>
		AdjustedAmt,
		///<summary>EntId. </summary>
		EntId,
		///<summary>MenuItemId. </summary>
		MenuItemId,
		///<summary>SelectionGroupId. </summary>
		SelectionGroupId,
		///<summary>SequenceId. </summary>
		SequenceId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SelectionGroupMaster.</summary>
	public enum SelectionGroupMasterFieldIndex
	{
		///<summary>DivisionId. </summary>
		DivisionId,
		///<summary>EntId. </summary>
		EntId,
		///<summary>RowVersion. </summary>
		RowVersion,
		///<summary>SelectionGroupId. </summary>
		SelectionGroupId,
		///<summary>SelectionGroupKpLabel. </summary>
		SelectionGroupKpLabel,
		///<summary>SelectionGroupName. </summary>
		SelectionGroupName,
		///<summary>SelectionGroupReceiptLabel. </summary>
		SelectionGroupReceiptLabel,
		///<summary>TerminalPendingText. </summary>
		TerminalPendingText,
		///<summary>TerminalPrompt. </summary>
		TerminalPrompt,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ServiceManagerProxyMaster.</summary>
	public enum ServiceManagerProxyMasterFieldIndex
	{
		///<summary>CampusCardDuration. </summary>
		CampusCardDuration,
		///<summary>CampusCardUrl. </summary>
		CampusCardUrl,
		///<summary>CampusCardVersion. </summary>
		CampusCardVersion,
		///<summary>CompCardDuration. </summary>
		CompCardDuration,
		///<summary>CompCardUrl. </summary>
		CompCardUrl,
		///<summary>CompCardVersion. </summary>
		CompCardVersion,
		///<summary>CredentialModeType. </summary>
		CredentialModeType,
		///<summary>CreditCardDuration. </summary>
		CreditCardDuration,
		///<summary>CreditCardUrl. </summary>
		CreditCardUrl,
		///<summary>CreditCardVersion. </summary>
		CreditCardVersion,
		///<summary>Description. </summary>
		Description,
		///<summary>EntId. </summary>
		EntId,
		///<summary>GiftCardDuration. </summary>
		GiftCardDuration,
		///<summary>GiftCardUrl. </summary>
		GiftCardUrl,
		///<summary>GiftCardVersion. </summary>
		GiftCardVersion,
		///<summary>GuestInfoDuration. </summary>
		GuestInfoDuration,
		///<summary>GuestInfoUrl. </summary>
		GuestInfoUrl,
		///<summary>GuestInfoVersion. </summary>
		GuestInfoVersion,
		///<summary>Hash. </summary>
		Hash,
		///<summary>LoyaltyCardDuration. </summary>
		LoyaltyCardDuration,
		///<summary>LoyaltyCardUrl. </summary>
		LoyaltyCardUrl,
		///<summary>LoyaltyCardVersion. </summary>
		LoyaltyCardVersion,
		///<summary>Password. </summary>
		Password,
		///<summary>ServiceManagerProxyId. </summary>
		ServiceManagerProxyId,
		///<summary>ServiceManagerProxyName. </summary>
		ServiceManagerProxyName,
		///<summary>SignatureCaptureUrl. </summary>
		SignatureCaptureUrl,
		///<summary>SignatureCaptureVersion. </summary>
		SignatureCaptureVersion,
		///<summary>SiteId. </summary>
		SiteId,
		///<summary>Username. </summary>
		Username,
		///<summary>UsingRguestPaymentDeviceFlag. </summary>
		UsingRguestPaymentDeviceFlag,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SmuCommandMaster.</summary>
	public enum SmuCommandMasterFieldIndex
	{
		///<summary>CommandId. </summary>
		CommandId,
		///<summary>CreatedUtcDateTime. </summary>
		CreatedUtcDateTime,
		///<summary>DeviceId. </summary>
		DeviceId,
		///<summary>DeviceTypeId. </summary>
		DeviceTypeId,
		///<summary>DivisionId. </summary>
		DivisionId,
		///<summary>EntId. </summary>
		EntId,
		///<summary>ExtraData. </summary>
		ExtraData,
		///<summary>RecordId. </summary>
		RecordId,
		///<summary>StoreId. </summary>
		StoreId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SmuStatusMaster.</summary>
	public enum SmuStatusMasterFieldIndex
	{
		///<summary>ConfigurationLoadUtcTime. </summary>
		ConfigurationLoadUtcTime,
		///<summary>CurrentClockUtcTime. </summary>
		CurrentClockUtcTime,
		///<summary>DeviceId. </summary>
		DeviceId,
		///<summary>DeviceIpAddress. </summary>
		DeviceIpAddress,
		///<summary>DeviceTypeId. </summary>
		DeviceTypeId,
		///<summary>DivisionId. </summary>
		DivisionId,
		///<summary>Drawer1EmpId. </summary>
		Drawer1EmpId,
		///<summary>Drawer2EmpId. </summary>
		Drawer2EmpId,
		///<summary>EntId. </summary>
		EntId,
		///<summary>IsLtsInitSuccessFlag. </summary>
		IsLtsInitSuccessFlag,
		///<summary>IsTerminalLockedFlag. </summary>
		IsTerminalLockedFlag,
		///<summary>IsTerminalOnline. </summary>
		IsTerminalOnline,
		///<summary>IsTrainingModeFlag. </summary>
		IsTrainingModeFlag,
		///<summary>LastCheckNumber. </summary>
		LastCheckNumber,
		///<summary>LastUpdateUtcDateTime. </summary>
		LastUpdateUtcDateTime,
		///<summary>LtsAvailabilityFlag. </summary>
		LtsAvailabilityFlag,
		///<summary>LtsServerIpAddress. </summary>
		LtsServerIpAddress,
		///<summary>MealPeriodId. </summary>
		MealPeriodId,
		///<summary>ProfitCenterId. </summary>
		ProfitCenterId,
		///<summary>ProgramLoadUtcTime. </summary>
		ProgramLoadUtcTime,
		///<summary>ScreenState. </summary>
		ScreenState,
		///<summary>SignonEmpId. </summary>
		SignonEmpId,
		///<summary>SignonJobcodeId. </summary>
		SignonJobcodeId,
		///<summary>SmuStatusId. </summary>
		SmuStatusId,
		///<summary>StoreId. </summary>
		StoreId,
		///<summary>TlogCheckCount. </summary>
		TlogCheckCount,
		///<summary>VersionNumber. </summary>
		VersionNumber,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SpecialInstrMaster.</summary>
	public enum SpecialInstrMasterFieldIndex
	{
		///<summary>EntId. </summary>
		EntId,
		///<summary>RowVersion. </summary>
		RowVersion,
		///<summary>SiAbbr1. </summary>
		SiAbbr1,
		///<summary>SiAbbr2. </summary>
		SiAbbr2,
		///<summary>SiId. </summary>
		SiId,
		///<summary>SiName. </summary>
		SiName,
		///<summary>StoreId. </summary>
		StoreId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SystemConfigurationMaster.</summary>
	public enum SystemConfigurationMasterFieldIndex
	{
		///<summary>PropertyName. </summary>
		PropertyName,
		///<summary>PropertyValue. </summary>
		PropertyValue,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TableLayoutMaster.</summary>
	public enum TableLayoutMasterFieldIndex
	{
		///<summary>AllowButtonOverlapFlag. </summary>
		AllowButtonOverlapFlag,
		///<summary>ButtonPadding. </summary>
		ButtonPadding,
		///<summary>DataControlGroupId. </summary>
		DataControlGroupId,
		///<summary>EntId. </summary>
		EntId,
		///<summary>GridSize. </summary>
		GridSize,
		///<summary>ScreenSizeId. </summary>
		ScreenSizeId,
		///<summary>ScreenTypeId. </summary>
		ScreenTypeId,
		///<summary>SnapToGridFlag. </summary>
		SnapToGridFlag,
		///<summary>StoreId. </summary>
		StoreId,
		///<summary>ThemeId. </summary>
		ThemeId,
		///<summary>TlAbbr1. </summary>
		TlAbbr1,
		///<summary>TlAbbr2. </summary>
		TlAbbr2,
		///<summary>TlId. </summary>
		TlId,
		///<summary>TlName. </summary>
		TlName,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TaxCatMaster.</summary>
	public enum TaxCatMasterFieldIndex
	{
		///<summary>EntId. </summary>
		EntId,
		///<summary>RowVersion. </summary>
		RowVersion,
		///<summary>StoreId. </summary>
		StoreId,
		///<summary>TaxCatAbbr1. </summary>
		TaxCatAbbr1,
		///<summary>TaxCatAbbr2. </summary>
		TaxCatAbbr2,
		///<summary>TaxCatId. </summary>
		TaxCatId,
		///<summary>TaxCatName. </summary>
		TaxCatName,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TaxGroupMaster.</summary>
	public enum TaxGroupMasterFieldIndex
	{
		///<summary>EntId. </summary>
		EntId,
		///<summary>StoreId. </summary>
		StoreId,
		///<summary>TaxGrpAbbr1. </summary>
		TaxGrpAbbr1,
		///<summary>TaxGrpAbbr2. </summary>
		TaxGrpAbbr2,
		///<summary>TaxGrpId. </summary>
		TaxGrpId,
		///<summary>TaxGrpName. </summary>
		TaxGrpName,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TaxGrpTaxJoin.</summary>
	public enum TaxGrpTaxJoinFieldIndex
	{
		///<summary>EntId. </summary>
		EntId,
		///<summary>TaxGrpId. </summary>
		TaxGrpId,
		///<summary>TaxId. </summary>
		TaxId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TaxMaster.</summary>
	public enum TaxMasterFieldIndex
	{
		///<summary>CompoundLevelId. </summary>
		CompoundLevelId,
		///<summary>EntId. </summary>
		EntId,
		///<summary>LimitTaxReportingFlag. </summary>
		LimitTaxReportingFlag,
		///<summary>LoyaltyEarnEligibleFlag. </summary>
		LoyaltyEarnEligibleFlag,
		///<summary>LoyaltyRedeemEligibleFlag. </summary>
		LoyaltyRedeemEligibleFlag,
		///<summary>RoundBasis. </summary>
		RoundBasis,
		///<summary>RoundTypeId. </summary>
		RoundTypeId,
		///<summary>RowVersion. </summary>
		RowVersion,
		///<summary>StoreId. </summary>
		StoreId,
		///<summary>TaxAbbr1. </summary>
		TaxAbbr1,
		///<summary>TaxAbbr2. </summary>
		TaxAbbr2,
		///<summary>TaxAmt. </summary>
		TaxAmt,
		///<summary>TaxCatId. </summary>
		TaxCatId,
		///<summary>TaxId. </summary>
		TaxId,
		///<summary>TaxName. </summary>
		TaxName,
		///<summary>TaxPercent. </summary>
		TaxPercent,
		///<summary>TaxRemovableFlag. </summary>
		TaxRemovableFlag,
		///<summary>ThreshholdAmt. </summary>
		ThreshholdAmt,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TaxRevCatJoin.</summary>
	public enum TaxRevCatJoinFieldIndex
	{
		///<summary>EmpPercent. </summary>
		EmpPercent,
		///<summary>EntId. </summary>
		EntId,
		///<summary>PatronPercent. </summary>
		PatronPercent,
		///<summary>RevCatId. </summary>
		RevCatId,
		///<summary>TaxId. </summary>
		TaxId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TenderClassMaster.</summary>
	public enum TenderClassMasterFieldIndex
	{
		///<summary>EntId. </summary>
		EntId,
		///<summary>RowVersion. </summary>
		RowVersion,
		///<summary>StoreId. </summary>
		StoreId,
		///<summary>TenderClassId. </summary>
		TenderClassId,
		///<summary>TenderClassName. </summary>
		TenderClassName,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TenderMaster.</summary>
	public enum TenderMasterFieldIndex
	{
		///<summary>AdditionalCheckidCodeId. </summary>
		AdditionalCheckidCodeId,
		///<summary>AutoRemoveTaxFlag. </summary>
		AutoRemoveTaxFlag,
		///<summary>BypassPdsFlag. </summary>
		BypassPdsFlag,
		///<summary>CheckTypeId. </summary>
		CheckTypeId,
		///<summary>CompTenderFlag. </summary>
		CompTenderFlag,
		///<summary>DestinationPropertyCode. </summary>
		DestinationPropertyCode,
		///<summary>DiscoupId. </summary>
		DiscoupId,
		///<summary>EmvCardTypeCode. </summary>
		EmvCardTypeCode,
		///<summary>EnterTipPrompt. </summary>
		EnterTipPrompt,
		///<summary>EntId. </summary>
		EntId,
		///<summary>FirstTenderFlag. </summary>
		FirstTenderFlag,
		///<summary>FrankingCodeId. </summary>
		FrankingCodeId,
		///<summary>IccDecimalPlaces. </summary>
		IccDecimalPlaces,
		///<summary>IccRate. </summary>
		IccRate,
		///<summary>LastTenderFlag. </summary>
		LastTenderFlag,
		///<summary>LoyaltyEarnEligibleFlag. </summary>
		LoyaltyEarnEligibleFlag,
		///<summary>NumReceiptsPrint. </summary>
		NumReceiptsPrint,
		///<summary>OpenCashdrwrCodeId. </summary>
		OpenCashdrwrCodeId,
		///<summary>OvertenderCodeId. </summary>
		OvertenderCodeId,
		///<summary>PostAcctNo. </summary>
		PostAcctNo,
		///<summary>PostSiteId. </summary>
		PostSiteId,
		///<summary>PostSystem1Flag. </summary>
		PostSystem1Flag,
		///<summary>PostSystem2Flag. </summary>
		PostSystem2Flag,
		///<summary>PostSystem3Flag. </summary>
		PostSystem3Flag,
		///<summary>PostSystem4Flag. </summary>
		PostSystem4Flag,
		///<summary>PostSystem5Flag. </summary>
		PostSystem5Flag,
		///<summary>PostSystem6Flag. </summary>
		PostSystem6Flag,
		///<summary>PostSystem7Flag. </summary>
		PostSystem7Flag,
		///<summary>PostSystem8Flag. </summary>
		PostSystem8Flag,
		///<summary>PriceLevelId. </summary>
		PriceLevelId,
		///<summary>PromptCvvFlag. </summary>
		PromptCvvFlag,
		///<summary>PromptExtraAlphaFlag. </summary>
		PromptExtraAlphaFlag,
		///<summary>PromptExtraDataFlag. </summary>
		PromptExtraDataFlag,
		///<summary>PromptZipcodeFlag. </summary>
		PromptZipcodeFlag,
		///<summary>RequireAmtFlag. </summary>
		RequireAmtFlag,
		///<summary>RestrictedFlag. </summary>
		RestrictedFlag,
		///<summary>RowVersion. </summary>
		RowVersion,
		///<summary>SalesTippableFlag. </summary>
		SalesTippableFlag,
		///<summary>SecurityId. </summary>
		SecurityId,
		///<summary>StoreId. </summary>
		StoreId,
		///<summary>TaxCompCode. </summary>
		TaxCompCode,
		///<summary>TenderAbbr1. </summary>
		TenderAbbr1,
		///<summary>TenderAbbr2. </summary>
		TenderAbbr2,
		///<summary>TenderClassId. </summary>
		TenderClassId,
		///<summary>TenderId. </summary>
		TenderId,
		///<summary>TenderLimit. </summary>
		TenderLimit,
		///<summary>TenderName. </summary>
		TenderName,
		///<summary>UseArchiveFlag. </summary>
		UseArchiveFlag,
		///<summary>UseSigcapFlag. </summary>
		UseSigcapFlag,
		///<summary>VerificationCodeId. </summary>
		VerificationCodeId,
		///<summary>VerificationManualEntryCodeId. </summary>
		VerificationManualEntryCodeId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TermGrpChkTypeJoin.</summary>
	public enum TermGrpChkTypeJoinFieldIndex
	{
		///<summary>CheckPriceLevelId. </summary>
		CheckPriceLevelId,
		///<summary>CheckSecId. </summary>
		CheckSecId,
		///<summary>CheckTenderAllowed. </summary>
		CheckTenderAllowed,
		///<summary>CheckTypeId. </summary>
		CheckTypeId,
		///<summary>CoversPrompt. </summary>
		CoversPrompt,
		///<summary>EntId. </summary>
		EntId,
		///<summary>RoomNoPrompt. </summary>
		RoomNoPrompt,
		///<summary>ServerPrompt. </summary>
		ServerPrompt,
		///<summary>TablePrompt. </summary>
		TablePrompt,
		///<summary>TaxOverride. </summary>
		TaxOverride,
		///<summary>TermGrpId. </summary>
		TermGrpId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TermGrpJobcodeJoin.</summary>
	public enum TermGrpJobcodeJoinFieldIndex
	{
		///<summary>EntId. </summary>
		EntId,
		///<summary>JobcodeId. </summary>
		JobcodeId,
		///<summary>TermGrpId. </summary>
		TermGrpId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TermGrpMaster.</summary>
	public enum TermGrpMasterFieldIndex
	{
		///<summary>CalendarPeriodId. </summary>
		CalendarPeriodId,
		///<summary>EntId. </summary>
		EntId,
		///<summary>ScreenScheduleId. </summary>
		ScreenScheduleId,
		///<summary>SigcapButton1. </summary>
		SigcapButton1,
		///<summary>SigcapButton2. </summary>
		SigcapButton2,
		///<summary>SigcapButton3. </summary>
		SigcapButton3,
		///<summary>SigcapButton4. </summary>
		SigcapButton4,
		///<summary>SigcapTipPrompt. </summary>
		SigcapTipPrompt,
		///<summary>SigcapTypeCode. </summary>
		SigcapTypeCode,
		///<summary>SiMenuId. </summary>
		SiMenuId,
		///<summary>StartTimeFlag. </summary>
		StartTimeFlag,
		///<summary>StoreId. </summary>
		StoreId,
		///<summary>TermGrpId. </summary>
		TermGrpId,
		///<summary>TermGrpName. </summary>
		TermGrpName,
		///<summary>TerminalTextId. </summary>
		TerminalTextId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TermGrpMenuJoin.</summary>
	public enum TermGrpMenuJoinFieldIndex
	{
		///<summary>CheckFlag. </summary>
		CheckFlag,
		///<summary>EntId. </summary>
		EntId,
		///<summary>MenuId. </summary>
		MenuId,
		///<summary>ParentMenuId. </summary>
		ParentMenuId,
		///<summary>TermGrpId. </summary>
		TermGrpId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TermGrpMenuJoinCooker.</summary>
	public enum TermGrpMenuJoinCookerFieldIndex
	{
		///<summary>CheckFlag. </summary>
		CheckFlag,
		///<summary>CookerHandle. </summary>
		CookerHandle,
		///<summary>EntId. </summary>
		EntId,
		///<summary>MenuId. </summary>
		MenuId,
		///<summary>ParentMenuId. </summary>
		ParentMenuId,
		///<summary>TermGrpId. </summary>
		TermGrpId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TermGrpMpJoin.</summary>
	public enum TermGrpMpJoinFieldIndex
	{
		///<summary>AutoCheckTypeId. </summary>
		AutoCheckTypeId,
		///<summary>EntId. </summary>
		EntId,
		///<summary>MealPeriodId. </summary>
		MealPeriodId,
		///<summary>MealPeriodStartTime1. </summary>
		MealPeriodStartTime1,
		///<summary>MealPeriodStartTime2. </summary>
		MealPeriodStartTime2,
		///<summary>MealPeriodStartTime3. </summary>
		MealPeriodStartTime3,
		///<summary>MealPeriodStartTime4. </summary>
		MealPeriodStartTime4,
		///<summary>MealPeriodStartTime5. </summary>
		MealPeriodStartTime5,
		///<summary>MealPeriodStartTime6. </summary>
		MealPeriodStartTime6,
		///<summary>MealPeriodStartTime7. </summary>
		MealPeriodStartTime7,
		///<summary>PriceLevelId. </summary>
		PriceLevelId,
		///<summary>TermGrpId. </summary>
		TermGrpId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TermGrpMpmenuJoin.</summary>
	public enum TermGrpMpmenuJoinFieldIndex
	{
		///<summary>CalendarDaySeqId. </summary>
		CalendarDaySeqId,
		///<summary>EntId. </summary>
		EntId,
		///<summary>MealPeriodId. </summary>
		MealPeriodId,
		///<summary>MenuId. </summary>
		MenuId,
		///<summary>MenuSeq. </summary>
		MenuSeq,
		///<summary>TermGrpId. </summary>
		TermGrpId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TermGrpTndrJoin.</summary>
	public enum TermGrpTndrJoinFieldIndex
	{
		///<summary>EntId. </summary>
		EntId,
		///<summary>SeqNo. </summary>
		SeqNo,
		///<summary>TenderId. </summary>
		TenderId,
		///<summary>TermGrpId. </summary>
		TermGrpId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TermGrpTransitionJoin.</summary>
	public enum TermGrpTransitionJoinFieldIndex
	{
		///<summary>EntId. </summary>
		EntId,
		///<summary>ScreenId. </summary>
		ScreenId,
		///<summary>TermGrpId. </summary>
		TermGrpId,
		///<summary>TransitionId. </summary>
		TransitionId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TerminalMaster.</summary>
	public enum TerminalMasterFieldIndex
	{
		///<summary>AltBargunTermId. </summary>
		AltBargunTermId,
		///<summary>AltRcptTermId. </summary>
		AltRcptTermId,
		///<summary>BargunBumpOverrideFlag. </summary>
		BargunBumpOverrideFlag,
		///<summary>BargunPrintDrinksOnBumpFlag. </summary>
		BargunPrintDrinksOnBumpFlag,
		///<summary>BargunTermFlag. </summary>
		BargunTermFlag,
		///<summary>CurrentVersion. </summary>
		CurrentVersion,
		///<summary>DefaultTableLayoutId. </summary>
		DefaultTableLayoutId,
		///<summary>FirstTableNo. </summary>
		FirstTableNo,
		///<summary>FolFileLoadFlag. </summary>
		FolFileLoadFlag,
		///<summary>GaFileLoadFlag. </summary>
		GaFileLoadFlag,
		///<summary>IpAddress. </summary>
		IpAddress,
		///<summary>LastPingUtcDateTime. </summary>
		LastPingUtcDateTime,
		///<summary>NumTables. </summary>
		NumTables,
		///<summary>PedValue. </summary>
		PedValue,
		///<summary>PrimaryProfitCenterId. </summary>
		PrimaryProfitCenterId,
		///<summary>ProfileId. </summary>
		ProfileId,
		///<summary>ReceiptPrinterType. </summary>
		ReceiptPrinterType,
		///<summary>RmsFileLoadFlag. </summary>
		RmsFileLoadFlag,
		///<summary>RowVersion. </summary>
		RowVersion,
		///<summary>StaticReceiptPrinterId. </summary>
		StaticReceiptPrinterId,
		///<summary>TermActiveFlag. </summary>
		TermActiveFlag,
		///<summary>TermGrpId. </summary>
		TermGrpId,
		///<summary>TermId. </summary>
		TermId,
		///<summary>TermName. </summary>
		TermName,
		///<summary>TermOptionGrpId. </summary>
		TermOptionGrpId,
		///<summary>TermPrinterGrpId. </summary>
		TermPrinterGrpId,
		///<summary>TermReceiptInfo. </summary>
		TermReceiptInfo,
		///<summary>TermServiceGrpId. </summary>
		TermServiceGrpId,
		///<summary>VirtualTermFlag. </summary>
		VirtualTermFlag,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TerminalProfitCtrJoin.</summary>
	public enum TerminalProfitCtrJoinFieldIndex
	{
		///<summary>PrimaryProfitCenterFlag. </summary>
		PrimaryProfitCenterFlag,
		///<summary>ProfitCenterId. </summary>
		ProfitCenterId,
		///<summary>ProfitCenterSeq. </summary>
		ProfitCenterSeq,
		///<summary>TermId. </summary>
		TermId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TerminalTextGuestInfoJoin.</summary>
	public enum TerminalTextGuestInfoJoinFieldIndex
	{
		///<summary>EntId. </summary>
		EntId,
		///<summary>SequenceId. </summary>
		SequenceId,
		///<summary>TerminalIntlId. </summary>
		TerminalIntlId,
		///<summary>TerminalIntlUserText. </summary>
		TerminalIntlUserText,
		///<summary>TerminalTextId. </summary>
		TerminalTextId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TerminalTextMaster.</summary>
	public enum TerminalTextMasterFieldIndex
	{
		///<summary>EntId. </summary>
		EntId,
		///<summary>StoreId. </summary>
		StoreId,
		///<summary>TerminalTextId. </summary>
		TerminalTextId,
		///<summary>TerminalTextName. </summary>
		TerminalTextName,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TermOptionGrpMaster.</summary>
	public enum TermOptionGrpMasterFieldIndex
	{
		///<summary>EntId. </summary>
		EntId,
		///<summary>StoreId. </summary>
		StoreId,
		///<summary>TermOptionGrpId. </summary>
		TermOptionGrpId,
		///<summary>TermOptionGrpName. </summary>
		TermOptionGrpName,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TermOptionGrpOptionJoin.</summary>
	public enum TermOptionGrpOptionJoinFieldIndex
	{
		///<summary>EntId. </summary>
		EntId,
		///<summary>OptionFlag. </summary>
		OptionFlag,
		///<summary>OptionId. </summary>
		OptionId,
		///<summary>TermOptionGrpId. </summary>
		TermOptionGrpId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TermPrinterGrpKpprntJoin.</summary>
	public enum TermPrinterGrpKpprntJoinFieldIndex
	{
		///<summary>EntId. </summary>
		EntId,
		///<summary>KpAlternateDeviceId. </summary>
		KpAlternateDeviceId,
		///<summary>KpDeviceId. </summary>
		KpDeviceId,
		///<summary>KpPrinterId. </summary>
		KpPrinterId,
		///<summary>TermPrinterGrpId. </summary>
		TermPrinterGrpId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TermPrinterGrpMaster.</summary>
	public enum TermPrinterGrpMasterFieldIndex
	{
		///<summary>AlternatePrintingCode. </summary>
		AlternatePrintingCode,
		///<summary>EntId. </summary>
		EntId,
		///<summary>StatusCodeId. </summary>
		StatusCodeId,
		///<summary>StoreId. </summary>
		StoreId,
		///<summary>TermPrinterGrpAbbr1. </summary>
		TermPrinterGrpAbbr1,
		///<summary>TermPrinterGrpAbbr2. </summary>
		TermPrinterGrpAbbr2,
		///<summary>TermPrinterGrpId. </summary>
		TermPrinterGrpId,
		///<summary>TermPrinterGrpName. </summary>
		TermPrinterGrpName,
		///<summary>UseAlternateFlag. </summary>
		UseAlternateFlag,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TermPrinterGrpRouteJoin.</summary>
	public enum TermPrinterGrpRouteJoinFieldIndex
	{
		///<summary>EntId. </summary>
		EntId,
		///<summary>GroupRouteGenId. </summary>
		GroupRouteGenId,
		///<summary>KpRouteId. </summary>
		KpRouteId,
		///<summary>StartMinuteFri. </summary>
		StartMinuteFri,
		///<summary>StartMinuteMon. </summary>
		StartMinuteMon,
		///<summary>StartMinuteSat. </summary>
		StartMinuteSat,
		///<summary>StartMinuteSun. </summary>
		StartMinuteSun,
		///<summary>StartMinuteThu. </summary>
		StartMinuteThu,
		///<summary>StartMinuteTue. </summary>
		StartMinuteTue,
		///<summary>StartMinuteWed. </summary>
		StartMinuteWed,
		///<summary>TermPrinterGrpId. </summary>
		TermPrinterGrpId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ThemeColorJoin.</summary>
	public enum ThemeColorJoinFieldIndex
	{
		///<summary>ColorCodeId. </summary>
		ColorCodeId,
		///<summary>EntId. </summary>
		EntId,
		///<summary>RgbColor. </summary>
		RgbColor,
		///<summary>ThemeId. </summary>
		ThemeId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ThemeFontJoin.</summary>
	public enum ThemeFontJoinFieldIndex
	{
		///<summary>EntId. </summary>
		EntId,
		///<summary>FontId. </summary>
		FontId,
		///<summary>ThemeId. </summary>
		ThemeId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ThemeMaster.</summary>
	public enum ThemeMasterFieldIndex
	{
		///<summary>EntId. </summary>
		EntId,
		///<summary>StoreId. </summary>
		StoreId,
		///<summary>ThemeId. </summary>
		ThemeId,
		///<summary>ThemeName. </summary>
		ThemeName,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ThemeObjectJoin.</summary>
	public enum ThemeObjectJoinFieldIndex
	{
		///<summary>ButtonBackcolor. </summary>
		ButtonBackcolor,
		///<summary>ButtonColumn. </summary>
		ButtonColumn,
		///<summary>ButtonFontId. </summary>
		ButtonFontId,
		///<summary>ButtonForecolor. </summary>
		ButtonForecolor,
		///<summary>ButtonHeight. </summary>
		ButtonHeight,
		///<summary>ButtonImageId. </summary>
		ButtonImageId,
		///<summary>ButtonNextMenuId. </summary>
		ButtonNextMenuId,
		///<summary>ButtonRow. </summary>
		ButtonRow,
		///<summary>ButtonShapeId. </summary>
		ButtonShapeId,
		///<summary>ButtonWidth. </summary>
		ButtonWidth,
		///<summary>EntId. </summary>
		EntId,
		///<summary>ErrorCodeId. </summary>
		ErrorCodeId,
		///<summary>ObjectTypeId. </summary>
		ObjectTypeId,
		///<summary>ThemeId. </summary>
		ThemeId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: UdefDateRangeInstance.</summary>
	public enum UdefDateRangeInstanceFieldIndex
	{
		///<summary>EntId. </summary>
		EntId,
		///<summary>PeriodTypeId. </summary>
		PeriodTypeId,
		///<summary>RangeEndDate. </summary>
		RangeEndDate,
		///<summary>RangeEndTime. </summary>
		RangeEndTime,
		///<summary>RangeInstanceDesc. </summary>
		RangeInstanceDesc,
		///<summary>RangeInstanceId. </summary>
		RangeInstanceId,
		///<summary>RangeStartDate. </summary>
		RangeStartDate,
		///<summary>RangeStartTime. </summary>
		RangeStartTime,
		///<summary>StoreId. </summary>
		StoreId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: UdefPeriodTypeMaster.</summary>
	public enum UdefPeriodTypeMasterFieldIndex
	{
		///<summary>EntId. </summary>
		EntId,
		///<summary>PeriodTypeDesc. </summary>
		PeriodTypeDesc,
		///<summary>PeriodTypeId. </summary>
		PeriodTypeId,
		///<summary>StoreId. </summary>
		StoreId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: VoidReasonMaster.</summary>
	public enum VoidReasonMasterFieldIndex
	{
		///<summary>EntId. </summary>
		EntId,
		///<summary>KpFlag. </summary>
		KpFlag,
		///<summary>PmixFlag. </summary>
		PmixFlag,
		///<summary>RefundCheckFlag. </summary>
		RefundCheckFlag,
		///<summary>RowVersion. </summary>
		RowVersion,
		///<summary>StoreId. </summary>
		StoreId,
		///<summary>VoidItemFlag. </summary>
		VoidItemFlag,
		///<summary>VoidOpenChkFlag. </summary>
		VoidOpenChkFlag,
		///<summary>VoidReasonAbbr1. </summary>
		VoidReasonAbbr1,
		///<summary>VoidReasonAbbr2. </summary>
		VoidReasonAbbr2,
		///<summary>VoidReasonId. </summary>
		VoidReasonId,
		///<summary>VoidReasonName. </summary>
		VoidReasonName,
		///<summary>VoidReasonSecId. </summary>
		VoidReasonSecId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ZooMaster.</summary>
	public enum ZooMasterFieldIndex
	{
		///<summary>ConfigEventXml. </summary>
		ConfigEventXml,
		///<summary>CustomerId. </summary>
		CustomerId,
		///<summary>DivisionId. </summary>
		DivisionId,
		///<summary>EntId. </summary>
		EntId,
		///<summary>MasterObjectDisplayName. </summary>
		MasterObjectDisplayName,
		///<summary>MasterObjectId. </summary>
		MasterObjectId,
		///<summary>ObjectDisplayName. </summary>
		ObjectDisplayName,
		///<summary>ObjectId. </summary>
		ObjectId,
		///<summary>ObjectTypeId. </summary>
		ObjectTypeId,
		///<summary>StoreId. </summary>
		StoreId,
		///<summary>TrackAction. </summary>
		TrackAction,
		///<summary>TrackDttime. </summary>
		TrackDttime,
		///<summary>TrackGroupId. </summary>
		TrackGroupId,
		///<summary>TrackSourceId. </summary>
		TrackSourceId,
		///<summary>TrackUsername. </summary>
		TrackUsername,
		///<summary>VirtualDatabaseName. </summary>
		VirtualDatabaseName,
		///<summary>VirtualTableName. </summary>
		VirtualTableName,
		///<summary>ZooId. </summary>
		ZooId,
		/// <summary></summary>
		AmountOfFields
	}



	/// <summary>Enum definition for all the entity types defined in this namespace. Used by the entityfields factory.</summary>
	public enum EntityType
	{
		///<summary>BonusCodeMaster</summary>
		BonusCodeMasterEntity,
		///<summary>ButtonAbbrJoin</summary>
		ButtonAbbrJoinEntity,
		///<summary>CardTypeMaster</summary>
		CardTypeMasterEntity,
		///<summary>CardTypeRangeJoin</summary>
		CardTypeRangeJoinEntity,
		///<summary>CfgVersion</summary>
		CfgVersionEntity,
		///<summary>CheckTypeMaster</summary>
		CheckTypeMasterEntity,
		///<summary>ChefMaster</summary>
		ChefMasterEntity,
		///<summary>ChkTypeGratJoin</summary>
		ChkTypeGratJoinEntity,
		///<summary>ChkTypeGratRevJoin</summary>
		ChkTypeGratRevJoinEntity,
		///<summary>ChkTypeGratTaxJoin</summary>
		ChkTypeGratTaxJoinEntity,
		///<summary>ChkTypeTaxGrpJoin</summary>
		ChkTypeTaxGrpJoinEntity,
		///<summary>ChkTypeTaxGrpTaxJoin</summary>
		ChkTypeTaxGrpTaxJoinEntity,
		///<summary>ChkTypeVatGratJoin</summary>
		ChkTypeVatGratJoinEntity,
		///<summary>ChkTypeVatRevJoin</summary>
		ChkTypeVatRevJoinEntity,
		///<summary>ChoiceGroupMaster</summary>
		ChoiceGroupMasterEntity,
		///<summary>ChoiceGrpModJoin</summary>
		ChoiceGrpModJoinEntity,
		///<summary>ComboItemJoin</summary>
		ComboItemJoinEntity,
		///<summary>ComboMaster</summary>
		ComboMasterEntity,
		///<summary>ConfigurationIdMaster</summary>
		ConfigurationIdMasterEntity,
		///<summary>DeviceMaster</summary>
		DeviceMasterEntity,
		///<summary>DiscoupMaster</summary>
		DiscoupMasterEntity,
		///<summary>EmpGroupMaster</summary>
		EmpGroupMasterEntity,
		///<summary>EmpGrpEmpJoin</summary>
		EmpGrpEmpJoinEntity,
		///<summary>EmpJobcodeJoin</summary>
		EmpJobcodeJoinEntity,
		///<summary>EmpMaster</summary>
		EmpMasterEntity,
		///<summary>FuncBttnTextJoin</summary>
		FuncBttnTextJoinEntity,
		///<summary>Ga4680ExportParmMaster</summary>
		Ga4680ExportParmMasterEntity,
		///<summary>Ga4680ImportParmMaster</summary>
		Ga4680ImportParmMasterEntity,
		///<summary>GeneralLedgerMapMaster</summary>
		GeneralLedgerMapMasterEntity,
		///<summary>GiftCardMaster</summary>
		GiftCardMasterEntity,
		///<summary>GratuityCatMaster</summary>
		GratuityCatMasterEntity,
		///<summary>GratuityMaster</summary>
		GratuityMasterEntity,
		///<summary>IdMaster</summary>
		IdMasterEntity,
		///<summary>ImageLibraryMaster</summary>
		ImageLibraryMasterEntity,
		///<summary>IntlDescriptorTextJoin</summary>
		IntlDescriptorTextJoinEntity,
		///<summary>IpServerAttribMaster</summary>
		IpServerAttribMasterEntity,
		///<summary>JobcodeFuncBttnJoin</summary>
		JobcodeFuncBttnJoinEntity,
		///<summary>JobcodeFunctJoin</summary>
		JobcodeFunctJoinEntity,
		///<summary>JobcodeMaster</summary>
		JobcodeMasterEntity,
		///<summary>KdsCategoryMaster</summary>
		KdsCategoryMasterEntity,
		///<summary>KdsVideoMaster</summary>
		KdsVideoMasterEntity,
		///<summary>KpOptionGrpCopyJoin</summary>
		KpOptionGrpCopyJoinEntity,
		///<summary>KpOptionGrpFormatJoin</summary>
		KpOptionGrpFormatJoinEntity,
		///<summary>KpOptionGrpMaster</summary>
		KpOptionGrpMasterEntity,
		///<summary>KpPrinterMaster</summary>
		KpPrinterMasterEntity,
		///<summary>KpRouteMaster</summary>
		KpRouteMasterEntity,
		///<summary>KpRoutePrinterJoin</summary>
		KpRoutePrinterJoinEntity,
		///<summary>MachineOptionsMaster</summary>
		MachineOptionsMasterEntity,
		///<summary>MealPeriodMaster</summary>
		MealPeriodMasterEntity,
		///<summary>MembershipMaster</summary>
		MembershipMasterEntity,
		///<summary>MembershipProfitCenterJoin</summary>
		MembershipProfitCenterJoinEntity,
		///<summary>MenuBttnObjJoin</summary>
		MenuBttnObjJoinEntity,
		///<summary>MenuItemGroupMaster</summary>
		MenuItemGroupMasterEntity,
		///<summary>MenuItemMaster</summary>
		MenuItemMasterEntity,
		///<summary>MenuMaster</summary>
		MenuMasterEntity,
		///<summary>MenuMigrpJoin</summary>
		MenuMigrpJoinEntity,
		///<summary>MiChoiceGrpJoin</summary>
		MiChoiceGrpJoinEntity,
		///<summary>MiKpprinterJoin</summary>
		MiKpprinterJoinEntity,
		///<summary>MiPriceJoin</summary>
		MiPriceJoinEntity,
		///<summary>MiSkuJoin</summary>
		MiSkuJoinEntity,
		///<summary>ModifierKpprinterJoin</summary>
		ModifierKpprinterJoinEntity,
		///<summary>ModifierMaster</summary>
		ModifierMasterEntity,
		///<summary>ModifierPriceJoin</summary>
		ModifierPriceJoinEntity,
		///<summary>OptionsMaster</summary>
		OptionsMasterEntity,
		///<summary>PriceLevelMaster</summary>
		PriceLevelMasterEntity,
		///<summary>ProcessMaster</summary>
		ProcessMasterEntity,
		///<summary>ProductClassMaster</summary>
		ProductClassMasterEntity,
		///<summary>ProfitCenterDayPartJoin</summary>
		ProfitCenterDayPartJoinEntity,
		///<summary>ProfitCenterGroupJoin</summary>
		ProfitCenterGroupJoinEntity,
		///<summary>ProfitCenterGroupMaster</summary>
		ProfitCenterGroupMasterEntity,
		///<summary>ProfitCenterMaster</summary>
		ProfitCenterMasterEntity,
		///<summary>ProfitCenterReceiptPrinterJoin</summary>
		ProfitCenterReceiptPrinterJoinEntity,
		///<summary>ProfitCenterTableJoin</summary>
		ProfitCenterTableJoinEntity,
		///<summary>QuickTenderMaster</summary>
		QuickTenderMasterEntity,
		///<summary>ReceiptPrinterMaster</summary>
		ReceiptPrinterMasterEntity,
		///<summary>ReportCatMaster</summary>
		ReportCatMasterEntity,
		///<summary>RevenueCatMaster</summary>
		RevenueCatMasterEntity,
		///<summary>RevenueClassMaster</summary>
		RevenueClassMasterEntity,
		///<summary>RowLockMaster</summary>
		RowLockMasterEntity,
		///<summary>ScreenTemplateMaster</summary>
		ScreenTemplateMasterEntity,
		///<summary>ScreenTemplateMigrpJoin</summary>
		ScreenTemplateMigrpJoinEntity,
		///<summary>SecurityLevelMaster</summary>
		SecurityLevelMasterEntity,
		///<summary>SelectionGroupItemJoin</summary>
		SelectionGroupItemJoinEntity,
		///<summary>SelectionGroupMaster</summary>
		SelectionGroupMasterEntity,
		///<summary>ServiceManagerProxyMaster</summary>
		ServiceManagerProxyMasterEntity,
		///<summary>SmuCommandMaster</summary>
		SmuCommandMasterEntity,
		///<summary>SmuStatusMaster</summary>
		SmuStatusMasterEntity,
		///<summary>SpecialInstrMaster</summary>
		SpecialInstrMasterEntity,
		///<summary>SystemConfigurationMaster</summary>
		SystemConfigurationMasterEntity,
		///<summary>TableLayoutMaster</summary>
		TableLayoutMasterEntity,
		///<summary>TaxCatMaster</summary>
		TaxCatMasterEntity,
		///<summary>TaxGroupMaster</summary>
		TaxGroupMasterEntity,
		///<summary>TaxGrpTaxJoin</summary>
		TaxGrpTaxJoinEntity,
		///<summary>TaxMaster</summary>
		TaxMasterEntity,
		///<summary>TaxRevCatJoin</summary>
		TaxRevCatJoinEntity,
		///<summary>TenderClassMaster</summary>
		TenderClassMasterEntity,
		///<summary>TenderMaster</summary>
		TenderMasterEntity,
		///<summary>TermGrpChkTypeJoin</summary>
		TermGrpChkTypeJoinEntity,
		///<summary>TermGrpJobcodeJoin</summary>
		TermGrpJobcodeJoinEntity,
		///<summary>TermGrpMaster</summary>
		TermGrpMasterEntity,
		///<summary>TermGrpMenuJoin</summary>
		TermGrpMenuJoinEntity,
		///<summary>TermGrpMenuJoinCooker</summary>
		TermGrpMenuJoinCookerEntity,
		///<summary>TermGrpMpJoin</summary>
		TermGrpMpJoinEntity,
		///<summary>TermGrpMpmenuJoin</summary>
		TermGrpMpmenuJoinEntity,
		///<summary>TermGrpTndrJoin</summary>
		TermGrpTndrJoinEntity,
		///<summary>TermGrpTransitionJoin</summary>
		TermGrpTransitionJoinEntity,
		///<summary>TerminalMaster</summary>
		TerminalMasterEntity,
		///<summary>TerminalProfitCtrJoin</summary>
		TerminalProfitCtrJoinEntity,
		///<summary>TerminalTextGuestInfoJoin</summary>
		TerminalTextGuestInfoJoinEntity,
		///<summary>TerminalTextMaster</summary>
		TerminalTextMasterEntity,
		///<summary>TermOptionGrpMaster</summary>
		TermOptionGrpMasterEntity,
		///<summary>TermOptionGrpOptionJoin</summary>
		TermOptionGrpOptionJoinEntity,
		///<summary>TermPrinterGrpKpprntJoin</summary>
		TermPrinterGrpKpprntJoinEntity,
		///<summary>TermPrinterGrpMaster</summary>
		TermPrinterGrpMasterEntity,
		///<summary>TermPrinterGrpRouteJoin</summary>
		TermPrinterGrpRouteJoinEntity,
		///<summary>ThemeColorJoin</summary>
		ThemeColorJoinEntity,
		///<summary>ThemeFontJoin</summary>
		ThemeFontJoinEntity,
		///<summary>ThemeMaster</summary>
		ThemeMasterEntity,
		///<summary>ThemeObjectJoin</summary>
		ThemeObjectJoinEntity,
		///<summary>UdefDateRangeInstance</summary>
		UdefDateRangeInstanceEntity,
		///<summary>UdefPeriodTypeMaster</summary>
		UdefPeriodTypeMasterEntity,
		///<summary>VoidReasonMaster</summary>
		VoidReasonMasterEntity,
		///<summary>ZooMaster</summary>
		ZooMasterEntity
	}


	#region Custom ConstantsEnums Code
	
	// __LLBLGENPRO_USER_CODE_REGION_START CustomUserConstants
	// __LLBLGENPRO_USER_CODE_REGION_END
	
	#endregion

	#region Included code

	#endregion
}

