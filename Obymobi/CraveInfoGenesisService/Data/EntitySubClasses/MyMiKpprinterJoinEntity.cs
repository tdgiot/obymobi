﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Xml.Serialization;
using InfoGenesis;
using InfoGenesis.HelperClasses;
using InfoGenesis.FactoryClasses;
using InfoGenesis.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace InfoGenesis.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	

	/// <summary>Entity class which represents the entity 'MiKpprinterJoin'.</summary>
	[Serializable]
	public partial class MyMiKpprinterJoinEntity : MiKpprinterJoinEntity, ISerializable
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
			
	{
		#region Class Member Declarations
		
		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		
		#endregion

		/// <summary> CTor</summary>
		public MyMiKpprinterJoinEntity()
		{
			InitClass();
		}

		/// <summary> CTor</summary>
		/// <remarks>For framework usage.</remarks>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public MyMiKpprinterJoinEntity(IEntityFields2 fields):base(fields)
		{
			InitClass();
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this MiKpprinterJoinEntity</param>
		public MyMiKpprinterJoinEntity(IValidator validator):base(validator)
		{
			InitClass();
		}
				
		/// <summary> CTor</summary>
		/// <param name="entId">PK value for MiKpprinterJoin which data should be fetched into this MiKpprinterJoin object</param>
		/// <param name="kpPrinterId">PK value for MiKpprinterJoin which data should be fetched into this MiKpprinterJoin object</param>
		/// <param name="menuItemId">PK value for MiKpprinterJoin which data should be fetched into this MiKpprinterJoin object</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public MyMiKpprinterJoinEntity(System.Int32 entId, System.Int32 kpPrinterId, System.Int32 menuItemId):base(entId, kpPrinterId, menuItemId)
		{
			InitClass();
		}

		/// <summary> CTor</summary>
		/// <param name="entId">PK value for MiKpprinterJoin which data should be fetched into this MiKpprinterJoin object</param>
		/// <param name="kpPrinterId">PK value for MiKpprinterJoin which data should be fetched into this MiKpprinterJoin object</param>
		/// <param name="menuItemId">PK value for MiKpprinterJoin which data should be fetched into this MiKpprinterJoin object</param>
		/// <param name="validator">The custom validator object for this MiKpprinterJoinEntity</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public MyMiKpprinterJoinEntity(System.Int32 entId, System.Int32 kpPrinterId, System.Int32 menuItemId, IValidator validator):base(entId, kpPrinterId, menuItemId, validator)
		{
			InitClass();
		}

		/// <summary> Protected CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected MyMiKpprinterJoinEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			InitClass();
		}
		
		/// <summary>Creates a new instance of the factory related to this entity</summary>
		protected override IEntityFactory2 CreateEntityFactory()
		{
			return new MyMiKpprinterJoinEntityFactory();
		}
		
		/// <summary>Initialization routine for class</summary>
		private void InitClass()
		{
			
			// __LLBLGENPRO_USER_CODE_REGION_START InitClass
			// __LLBLGENPRO_USER_CODE_REGION_END
			
		}
		
		#region Class Property Declarations








		#endregion

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		
		#endregion
	}
}
