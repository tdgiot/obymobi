﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Xml.Serialization;
using InfoGenesis;
using InfoGenesis.HelperClasses;
using InfoGenesis.FactoryClasses;
using InfoGenesis.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace InfoGenesis.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	

	/// <summary>Entity class which represents the entity 'IntlDescriptorTextJoin'.</summary>
	[Serializable]
	public partial class MyIntlDescriptorTextJoinEntity : IntlDescriptorTextJoinEntity, ISerializable
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
			
	{
		#region Class Member Declarations
		
		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		
		#endregion

		/// <summary> CTor</summary>
		public MyIntlDescriptorTextJoinEntity()
		{
			InitClass();
		}

		/// <summary> CTor</summary>
		/// <remarks>For framework usage.</remarks>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public MyIntlDescriptorTextJoinEntity(IEntityFields2 fields):base(fields)
		{
			InitClass();
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this IntlDescriptorTextJoinEntity</param>
		public MyIntlDescriptorTextJoinEntity(IValidator validator):base(validator)
		{
			InitClass();
		}
				
		/// <summary> CTor</summary>
		/// <param name="controlId">PK value for IntlDescriptorTextJoin which data should be fetched into this IntlDescriptorTextJoin object</param>
		/// <param name="customerId">PK value for IntlDescriptorTextJoin which data should be fetched into this IntlDescriptorTextJoin object</param>
		/// <param name="entId">PK value for IntlDescriptorTextJoin which data should be fetched into this IntlDescriptorTextJoin object</param>
		/// <param name="languageId">PK value for IntlDescriptorTextJoin which data should be fetched into this IntlDescriptorTextJoin object</param>
		/// <param name="nodeId">PK value for IntlDescriptorTextJoin which data should be fetched into this IntlDescriptorTextJoin object</param>
		/// <param name="objectId">PK value for IntlDescriptorTextJoin which data should be fetched into this IntlDescriptorTextJoin object</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public MyIntlDescriptorTextJoinEntity(System.Int32 controlId, System.Int32 customerId, System.Int32 entId, System.String languageId, System.Int32 nodeId, System.Int32 objectId):base(controlId, customerId, entId, languageId, nodeId, objectId)
		{
			InitClass();
		}

		/// <summary> CTor</summary>
		/// <param name="controlId">PK value for IntlDescriptorTextJoin which data should be fetched into this IntlDescriptorTextJoin object</param>
		/// <param name="customerId">PK value for IntlDescriptorTextJoin which data should be fetched into this IntlDescriptorTextJoin object</param>
		/// <param name="entId">PK value for IntlDescriptorTextJoin which data should be fetched into this IntlDescriptorTextJoin object</param>
		/// <param name="languageId">PK value for IntlDescriptorTextJoin which data should be fetched into this IntlDescriptorTextJoin object</param>
		/// <param name="nodeId">PK value for IntlDescriptorTextJoin which data should be fetched into this IntlDescriptorTextJoin object</param>
		/// <param name="objectId">PK value for IntlDescriptorTextJoin which data should be fetched into this IntlDescriptorTextJoin object</param>
		/// <param name="validator">The custom validator object for this IntlDescriptorTextJoinEntity</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public MyIntlDescriptorTextJoinEntity(System.Int32 controlId, System.Int32 customerId, System.Int32 entId, System.String languageId, System.Int32 nodeId, System.Int32 objectId, IValidator validator):base(controlId, customerId, entId, languageId, nodeId, objectId, validator)
		{
			InitClass();
		}

		/// <summary> Protected CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected MyIntlDescriptorTextJoinEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			InitClass();
		}
		
		/// <summary>Creates a new instance of the factory related to this entity</summary>
		protected override IEntityFactory2 CreateEntityFactory()
		{
			return new MyIntlDescriptorTextJoinEntityFactory();
		}
		
		/// <summary>Initialization routine for class</summary>
		private void InitClass()
		{
			
			// __LLBLGENPRO_USER_CODE_REGION_START InitClass
			// __LLBLGENPRO_USER_CODE_REGION_END
			
		}
		
		#region Class Property Declarations








		#endregion

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		
		#endregion
	}
}
