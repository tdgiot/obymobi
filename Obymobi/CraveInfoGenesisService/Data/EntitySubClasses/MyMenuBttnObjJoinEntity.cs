﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Xml.Serialization;
using InfoGenesis;
using InfoGenesis.HelperClasses;
using InfoGenesis.FactoryClasses;
using InfoGenesis.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace InfoGenesis.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	

	/// <summary>Entity class which represents the entity 'MenuBttnObjJoin'.</summary>
	[Serializable]
	public partial class MyMenuBttnObjJoinEntity : MenuBttnObjJoinEntity, ISerializable
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
			
	{
		#region Class Member Declarations
		
		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		
		#endregion

		/// <summary> CTor</summary>
		public MyMenuBttnObjJoinEntity()
		{
			InitClass();
		}

		/// <summary> CTor</summary>
		/// <remarks>For framework usage.</remarks>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public MyMenuBttnObjJoinEntity(IEntityFields2 fields):base(fields)
		{
			InitClass();
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this MenuBttnObjJoinEntity</param>
		public MyMenuBttnObjJoinEntity(IValidator validator):base(validator)
		{
			InitClass();
		}
				
		/// <summary> CTor</summary>
		/// <param name="buttonId">PK value for MenuBttnObjJoin which data should be fetched into this MenuBttnObjJoin object</param>
		/// <param name="entId">PK value for MenuBttnObjJoin which data should be fetched into this MenuBttnObjJoin object</param>
		/// <param name="menuId">PK value for MenuBttnObjJoin which data should be fetched into this MenuBttnObjJoin object</param>
		/// <param name="screenId">PK value for MenuBttnObjJoin which data should be fetched into this MenuBttnObjJoin object</param>
		/// <param name="storeCreatedId">PK value for MenuBttnObjJoin which data should be fetched into this MenuBttnObjJoin object</param>
		/// <param name="terminalTypeId">PK value for MenuBttnObjJoin which data should be fetched into this MenuBttnObjJoin object</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public MyMenuBttnObjJoinEntity(System.Int32 buttonId, System.Int32 entId, System.Int32 menuId, System.Int32 screenId, System.Int32 storeCreatedId, System.Int32 terminalTypeId):base(buttonId, entId, menuId, screenId, storeCreatedId, terminalTypeId)
		{
			InitClass();
		}

		/// <summary> CTor</summary>
		/// <param name="buttonId">PK value for MenuBttnObjJoin which data should be fetched into this MenuBttnObjJoin object</param>
		/// <param name="entId">PK value for MenuBttnObjJoin which data should be fetched into this MenuBttnObjJoin object</param>
		/// <param name="menuId">PK value for MenuBttnObjJoin which data should be fetched into this MenuBttnObjJoin object</param>
		/// <param name="screenId">PK value for MenuBttnObjJoin which data should be fetched into this MenuBttnObjJoin object</param>
		/// <param name="storeCreatedId">PK value for MenuBttnObjJoin which data should be fetched into this MenuBttnObjJoin object</param>
		/// <param name="terminalTypeId">PK value for MenuBttnObjJoin which data should be fetched into this MenuBttnObjJoin object</param>
		/// <param name="validator">The custom validator object for this MenuBttnObjJoinEntity</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public MyMenuBttnObjJoinEntity(System.Int32 buttonId, System.Int32 entId, System.Int32 menuId, System.Int32 screenId, System.Int32 storeCreatedId, System.Int32 terminalTypeId, IValidator validator):base(buttonId, entId, menuId, screenId, storeCreatedId, terminalTypeId, validator)
		{
			InitClass();
		}

		/// <summary> Protected CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected MyMenuBttnObjJoinEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			InitClass();
		}
		
		/// <summary>Creates a new instance of the factory related to this entity</summary>
		protected override IEntityFactory2 CreateEntityFactory()
		{
			return new MyMenuBttnObjJoinEntityFactory();
		}
		
		/// <summary>Initialization routine for class</summary>
		private void InitClass()
		{
			
			// __LLBLGENPRO_USER_CODE_REGION_START InitClass
			// __LLBLGENPRO_USER_CODE_REGION_END
			
		}
		
		#region Class Property Declarations








		#endregion

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		
		#endregion
	}
}
