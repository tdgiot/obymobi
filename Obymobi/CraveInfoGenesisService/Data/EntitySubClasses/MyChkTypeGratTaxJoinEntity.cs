﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Xml.Serialization;
using InfoGenesis;
using InfoGenesis.HelperClasses;
using InfoGenesis.FactoryClasses;
using InfoGenesis.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace InfoGenesis.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	

	/// <summary>Entity class which represents the entity 'ChkTypeGratTaxJoin'.</summary>
	[Serializable]
	public partial class MyChkTypeGratTaxJoinEntity : ChkTypeGratTaxJoinEntity, ISerializable
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
			
	{
		#region Class Member Declarations
		
		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		
		#endregion

		/// <summary> CTor</summary>
		public MyChkTypeGratTaxJoinEntity()
		{
			InitClass();
		}

		/// <summary> CTor</summary>
		/// <remarks>For framework usage.</remarks>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public MyChkTypeGratTaxJoinEntity(IEntityFields2 fields):base(fields)
		{
			InitClass();
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this ChkTypeGratTaxJoinEntity</param>
		public MyChkTypeGratTaxJoinEntity(IValidator validator):base(validator)
		{
			InitClass();
		}
				
		/// <summary> CTor</summary>
		/// <param name="checkTypeId">PK value for ChkTypeGratTaxJoin which data should be fetched into this ChkTypeGratTaxJoin object</param>
		/// <param name="entId">PK value for ChkTypeGratTaxJoin which data should be fetched into this ChkTypeGratTaxJoin object</param>
		/// <param name="gratId">PK value for ChkTypeGratTaxJoin which data should be fetched into this ChkTypeGratTaxJoin object</param>
		/// <param name="taxId">PK value for ChkTypeGratTaxJoin which data should be fetched into this ChkTypeGratTaxJoin object</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public MyChkTypeGratTaxJoinEntity(System.Int32 checkTypeId, System.Int32 entId, System.Int32 gratId, System.Int32 taxId):base(checkTypeId, entId, gratId, taxId)
		{
			InitClass();
		}

		/// <summary> CTor</summary>
		/// <param name="checkTypeId">PK value for ChkTypeGratTaxJoin which data should be fetched into this ChkTypeGratTaxJoin object</param>
		/// <param name="entId">PK value for ChkTypeGratTaxJoin which data should be fetched into this ChkTypeGratTaxJoin object</param>
		/// <param name="gratId">PK value for ChkTypeGratTaxJoin which data should be fetched into this ChkTypeGratTaxJoin object</param>
		/// <param name="taxId">PK value for ChkTypeGratTaxJoin which data should be fetched into this ChkTypeGratTaxJoin object</param>
		/// <param name="validator">The custom validator object for this ChkTypeGratTaxJoinEntity</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public MyChkTypeGratTaxJoinEntity(System.Int32 checkTypeId, System.Int32 entId, System.Int32 gratId, System.Int32 taxId, IValidator validator):base(checkTypeId, entId, gratId, taxId, validator)
		{
			InitClass();
		}

		/// <summary> Protected CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected MyChkTypeGratTaxJoinEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			InitClass();
		}
		
		/// <summary>Creates a new instance of the factory related to this entity</summary>
		protected override IEntityFactory2 CreateEntityFactory()
		{
			return new MyChkTypeGratTaxJoinEntityFactory();
		}
		
		/// <summary>Initialization routine for class</summary>
		private void InitClass()
		{
			
			// __LLBLGENPRO_USER_CODE_REGION_START InitClass
			// __LLBLGENPRO_USER_CODE_REGION_END
			
		}
		
		#region Class Property Declarations








		#endregion

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		
		#endregion
	}
}
