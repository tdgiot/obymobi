﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Xml.Serialization;
using InfoGenesis;
using InfoGenesis.HelperClasses;
using InfoGenesis.FactoryClasses;
using InfoGenesis.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace InfoGenesis.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	

	/// <summary>Entity class which represents the entity 'CfgVersion'.</summary>
	[Serializable]
	public partial class MyCfgVersionEntity : CfgVersionEntity, ISerializable
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
			
	{
		#region Class Member Declarations
		
		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		
		#endregion

		/// <summary> CTor</summary>
		public MyCfgVersionEntity()
		{
			InitClass();
		}

		/// <summary> CTor</summary>
		/// <remarks>For framework usage.</remarks>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public MyCfgVersionEntity(IEntityFields2 fields):base(fields)
		{
			InitClass();
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this CfgVersionEntity</param>
		public MyCfgVersionEntity(IValidator validator):base(validator)
		{
			InitClass();
		}
				
		/// <summary> CTor</summary>
		/// <param name="dbName">PK value for CfgVersion which data should be fetched into this CfgVersion object</param>
		/// <param name="majorVersion">PK value for CfgVersion which data should be fetched into this CfgVersion object</param>
		/// <param name="minorVersion">PK value for CfgVersion which data should be fetched into this CfgVersion object</param>
		/// <param name="patchVersion">PK value for CfgVersion which data should be fetched into this CfgVersion object</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public MyCfgVersionEntity(System.String dbName, System.Int16 majorVersion, System.Int16 minorVersion, System.Int16 patchVersion):base(dbName, majorVersion, minorVersion, patchVersion)
		{
			InitClass();
		}

		/// <summary> CTor</summary>
		/// <param name="dbName">PK value for CfgVersion which data should be fetched into this CfgVersion object</param>
		/// <param name="majorVersion">PK value for CfgVersion which data should be fetched into this CfgVersion object</param>
		/// <param name="minorVersion">PK value for CfgVersion which data should be fetched into this CfgVersion object</param>
		/// <param name="patchVersion">PK value for CfgVersion which data should be fetched into this CfgVersion object</param>
		/// <param name="validator">The custom validator object for this CfgVersionEntity</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public MyCfgVersionEntity(System.String dbName, System.Int16 majorVersion, System.Int16 minorVersion, System.Int16 patchVersion, IValidator validator):base(dbName, majorVersion, minorVersion, patchVersion, validator)
		{
			InitClass();
		}

		/// <summary> Protected CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected MyCfgVersionEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			InitClass();
		}
		
		/// <summary>Creates a new instance of the factory related to this entity</summary>
		protected override IEntityFactory2 CreateEntityFactory()
		{
			return new MyCfgVersionEntityFactory();
		}
		
		/// <summary>Initialization routine for class</summary>
		private void InitClass()
		{
			
			// __LLBLGENPRO_USER_CODE_REGION_START InitClass
			// __LLBLGENPRO_USER_CODE_REGION_END
			
		}
		
		#region Class Property Declarations








		#endregion

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		
		#endregion
	}
}
