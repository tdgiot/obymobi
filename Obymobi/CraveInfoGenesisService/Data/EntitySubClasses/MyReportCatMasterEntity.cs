﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Xml.Serialization;
using InfoGenesis;
using InfoGenesis.HelperClasses;
using InfoGenesis.FactoryClasses;
using InfoGenesis.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace InfoGenesis.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	

	/// <summary>Entity class which represents the entity 'ReportCatMaster'.</summary>
	[Serializable]
	public partial class MyReportCatMasterEntity : ReportCatMasterEntity, ISerializable
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
			
	{
		#region Class Member Declarations
		
		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		
		#endregion

		/// <summary> CTor</summary>
		public MyReportCatMasterEntity()
		{
			InitClass();
		}

		/// <summary> CTor</summary>
		/// <remarks>For framework usage.</remarks>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public MyReportCatMasterEntity(IEntityFields2 fields):base(fields)
		{
			InitClass();
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this ReportCatMasterEntity</param>
		public MyReportCatMasterEntity(IValidator validator):base(validator)
		{
			InitClass();
		}
				
		/// <summary> CTor</summary>
		/// <param name="entId">PK value for ReportCatMaster which data should be fetched into this ReportCatMaster object</param>
		/// <param name="rptCatId">PK value for ReportCatMaster which data should be fetched into this ReportCatMaster object</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public MyReportCatMasterEntity(System.Int32 entId, System.Int32 rptCatId):base(entId, rptCatId)
		{
			InitClass();
		}

		/// <summary> CTor</summary>
		/// <param name="entId">PK value for ReportCatMaster which data should be fetched into this ReportCatMaster object</param>
		/// <param name="rptCatId">PK value for ReportCatMaster which data should be fetched into this ReportCatMaster object</param>
		/// <param name="validator">The custom validator object for this ReportCatMasterEntity</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public MyReportCatMasterEntity(System.Int32 entId, System.Int32 rptCatId, IValidator validator):base(entId, rptCatId, validator)
		{
			InitClass();
		}

		/// <summary> Protected CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected MyReportCatMasterEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			InitClass();
		}
		
		/// <summary>Creates a new instance of the factory related to this entity</summary>
		protected override IEntityFactory2 CreateEntityFactory()
		{
			return new MyReportCatMasterEntityFactory();
		}
		
		/// <summary>Initialization routine for class</summary>
		private void InitClass()
		{
			
			// __LLBLGENPRO_USER_CODE_REGION_START InitClass
			// __LLBLGENPRO_USER_CODE_REGION_END
			
		}
		
		#region Class Property Declarations








		#endregion

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		
		#endregion
	}
}
