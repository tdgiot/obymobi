﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Xml.Serialization;
using InfoGenesis;
using InfoGenesis.HelperClasses;
using InfoGenesis.FactoryClasses;
using InfoGenesis.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace InfoGenesis.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	

	/// <summary>Entity class which represents the entity 'EmpGrpEmpJoin'.</summary>
	[Serializable]
	public partial class MyEmpGrpEmpJoinEntity : EmpGrpEmpJoinEntity, ISerializable
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
			
	{
		#region Class Member Declarations
		
		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		
		#endregion

		/// <summary> CTor</summary>
		public MyEmpGrpEmpJoinEntity()
		{
			InitClass();
		}

		/// <summary> CTor</summary>
		/// <remarks>For framework usage.</remarks>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public MyEmpGrpEmpJoinEntity(IEntityFields2 fields):base(fields)
		{
			InitClass();
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this EmpGrpEmpJoinEntity</param>
		public MyEmpGrpEmpJoinEntity(IValidator validator):base(validator)
		{
			InitClass();
		}
				
		/// <summary> CTor</summary>
		/// <param name="empGrpId">PK value for EmpGrpEmpJoin which data should be fetched into this EmpGrpEmpJoin object</param>
		/// <param name="empId">PK value for EmpGrpEmpJoin which data should be fetched into this EmpGrpEmpJoin object</param>
		/// <param name="entId">PK value for EmpGrpEmpJoin which data should be fetched into this EmpGrpEmpJoin object</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public MyEmpGrpEmpJoinEntity(System.Int32 empGrpId, System.Int32 empId, System.Int32 entId):base(empGrpId, empId, entId)
		{
			InitClass();
		}

		/// <summary> CTor</summary>
		/// <param name="empGrpId">PK value for EmpGrpEmpJoin which data should be fetched into this EmpGrpEmpJoin object</param>
		/// <param name="empId">PK value for EmpGrpEmpJoin which data should be fetched into this EmpGrpEmpJoin object</param>
		/// <param name="entId">PK value for EmpGrpEmpJoin which data should be fetched into this EmpGrpEmpJoin object</param>
		/// <param name="validator">The custom validator object for this EmpGrpEmpJoinEntity</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public MyEmpGrpEmpJoinEntity(System.Int32 empGrpId, System.Int32 empId, System.Int32 entId, IValidator validator):base(empGrpId, empId, entId, validator)
		{
			InitClass();
		}

		/// <summary> Protected CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected MyEmpGrpEmpJoinEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			InitClass();
		}
		
		/// <summary>Creates a new instance of the factory related to this entity</summary>
		protected override IEntityFactory2 CreateEntityFactory()
		{
			return new MyEmpGrpEmpJoinEntityFactory();
		}
		
		/// <summary>Initialization routine for class</summary>
		private void InitClass()
		{
			
			// __LLBLGENPRO_USER_CODE_REGION_START InitClass
			// __LLBLGENPRO_USER_CODE_REGION_END
			
		}
		
		#region Class Property Declarations








		#endregion

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		
		#endregion
	}
}
