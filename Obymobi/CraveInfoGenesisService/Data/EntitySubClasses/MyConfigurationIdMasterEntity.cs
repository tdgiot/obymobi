﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Xml.Serialization;
using InfoGenesis;
using InfoGenesis.HelperClasses;
using InfoGenesis.FactoryClasses;
using InfoGenesis.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace InfoGenesis.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	

	/// <summary>Entity class which represents the entity 'ConfigurationIdMaster'.</summary>
	[Serializable]
	public partial class MyConfigurationIdMasterEntity : ConfigurationIdMasterEntity, ISerializable
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
			
	{
		#region Class Member Declarations
		
		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		
		#endregion

		/// <summary> CTor</summary>
		public MyConfigurationIdMasterEntity()
		{
			InitClass();
		}

		/// <summary> CTor</summary>
		/// <remarks>For framework usage.</remarks>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public MyConfigurationIdMasterEntity(IEntityFields2 fields):base(fields)
		{
			InitClass();
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this ConfigurationIdMasterEntity</param>
		public MyConfigurationIdMasterEntity(IValidator validator):base(validator)
		{
			InitClass();
		}
				
		/// <summary> CTor</summary>
		/// <param name="customerId">PK value for ConfigurationIdMaster which data should be fetched into this ConfigurationIdMaster object</param>
		/// <param name="divId">PK value for ConfigurationIdMaster which data should be fetched into this ConfigurationIdMaster object</param>
		/// <param name="entId">PK value for ConfigurationIdMaster which data should be fetched into this ConfigurationIdMaster object</param>
		/// <param name="reservedId">PK value for ConfigurationIdMaster which data should be fetched into this ConfigurationIdMaster object</param>
		/// <param name="storeId">PK value for ConfigurationIdMaster which data should be fetched into this ConfigurationIdMaster object</param>
		/// <param name="tableName">PK value for ConfigurationIdMaster which data should be fetched into this ConfigurationIdMaster object</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public MyConfigurationIdMasterEntity(System.Int32 customerId, System.Int32 divId, System.Int32 entId, System.Int32 reservedId, System.Int32 storeId, System.String tableName):base(customerId, divId, entId, reservedId, storeId, tableName)
		{
			InitClass();
		}

		/// <summary> CTor</summary>
		/// <param name="customerId">PK value for ConfigurationIdMaster which data should be fetched into this ConfigurationIdMaster object</param>
		/// <param name="divId">PK value for ConfigurationIdMaster which data should be fetched into this ConfigurationIdMaster object</param>
		/// <param name="entId">PK value for ConfigurationIdMaster which data should be fetched into this ConfigurationIdMaster object</param>
		/// <param name="reservedId">PK value for ConfigurationIdMaster which data should be fetched into this ConfigurationIdMaster object</param>
		/// <param name="storeId">PK value for ConfigurationIdMaster which data should be fetched into this ConfigurationIdMaster object</param>
		/// <param name="tableName">PK value for ConfigurationIdMaster which data should be fetched into this ConfigurationIdMaster object</param>
		/// <param name="validator">The custom validator object for this ConfigurationIdMasterEntity</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public MyConfigurationIdMasterEntity(System.Int32 customerId, System.Int32 divId, System.Int32 entId, System.Int32 reservedId, System.Int32 storeId, System.String tableName, IValidator validator):base(customerId, divId, entId, reservedId, storeId, tableName, validator)
		{
			InitClass();
		}

		/// <summary> Protected CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected MyConfigurationIdMasterEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			InitClass();
		}
		
		/// <summary>Creates a new instance of the factory related to this entity</summary>
		protected override IEntityFactory2 CreateEntityFactory()
		{
			return new MyConfigurationIdMasterEntityFactory();
		}
		
		/// <summary>Initialization routine for class</summary>
		private void InitClass()
		{
			
			// __LLBLGENPRO_USER_CODE_REGION_START InitClass
			// __LLBLGENPRO_USER_CODE_REGION_END
			
		}
		
		#region Class Property Declarations








		#endregion

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		
		#endregion
	}
}
