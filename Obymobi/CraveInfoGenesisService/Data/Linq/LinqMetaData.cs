﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using SD.LLBLGen.Pro.LinqSupportClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

using InfoGenesis;
using InfoGenesis.EntityClasses;
using InfoGenesis.FactoryClasses;
using InfoGenesis.HelperClasses;
using InfoGenesis.RelationClasses;

namespace InfoGenesis.Linq
{
	/// <summary>Meta-data class for the construction of Linq queries which are to be executed using LLBLGen Pro code.</summary>
	public partial class LinqMetaData: ILinqMetaData
	{
		#region Class Member Declarations
		private IDataAccessAdapter _adapterToUse;
		private FunctionMappingStore _customFunctionMappings;
		private Context _contextToUse;
		#endregion
		
		/// <summary>CTor. Using this ctor will leave the IDataAccessAdapter object to use empty. To be able to execute the query, an IDataAccessAdapter instance
		/// is required, and has to be set on the LLBLGenProProvider2 object in the query to execute. </summary>
		public LinqMetaData() : this(null, null)
		{
		}
		
		/// <summary>CTor which accepts an IDataAccessAdapter implementing object, which will be used to execute queries created with this metadata class.</summary>
		/// <param name="adapterToUse">the IDataAccessAdapter to use in queries created with this meta data</param>
		/// <remarks> Be aware that the IDataAccessAdapter object set via this property is kept alive by the LLBLGenProQuery objects created with this meta data
		/// till they go out of scope.</remarks>
		public LinqMetaData(IDataAccessAdapter adapterToUse) : this (adapterToUse, null)
		{
		}

		/// <summary>CTor which accepts an IDataAccessAdapter implementing object, which will be used to execute queries created with this metadata class.</summary>
		/// <param name="adapterToUse">the IDataAccessAdapter to use in queries created with this meta data</param>
		/// <param name="customFunctionMappings">The custom function mappings to use. These take higher precedence than the ones in the DQE to use.</param>
		/// <remarks> Be aware that the IDataAccessAdapter object set via this property is kept alive by the LLBLGenProQuery objects created with this meta data
		/// till they go out of scope.</remarks>
		public LinqMetaData(IDataAccessAdapter adapterToUse, FunctionMappingStore customFunctionMappings)
		{
			_adapterToUse = adapterToUse;
			_customFunctionMappings = customFunctionMappings;
		}
	
		/// <summary>returns the datasource to use in a Linq query for the entity type specified</summary>
		/// <param name="typeOfEntity">the type of the entity to get the datasource for</param>
		/// <returns>the requested datasource</returns>
		public IDataSource GetQueryableForEntity(int typeOfEntity)
		{
			IDataSource toReturn = null;
			switch((InfoGenesis.EntityType)typeOfEntity)
			{
				case InfoGenesis.EntityType.BonusCodeMasterEntity:
					toReturn = this.BonusCodeMaster;
					break;
				case InfoGenesis.EntityType.ButtonAbbrJoinEntity:
					toReturn = this.ButtonAbbrJoin;
					break;
				case InfoGenesis.EntityType.CardTypeMasterEntity:
					toReturn = this.CardTypeMaster;
					break;
				case InfoGenesis.EntityType.CardTypeRangeJoinEntity:
					toReturn = this.CardTypeRangeJoin;
					break;
				case InfoGenesis.EntityType.CfgVersionEntity:
					toReturn = this.CfgVersion;
					break;
				case InfoGenesis.EntityType.CheckTypeMasterEntity:
					toReturn = this.CheckTypeMaster;
					break;
				case InfoGenesis.EntityType.ChefMasterEntity:
					toReturn = this.ChefMaster;
					break;
				case InfoGenesis.EntityType.ChkTypeGratJoinEntity:
					toReturn = this.ChkTypeGratJoin;
					break;
				case InfoGenesis.EntityType.ChkTypeGratRevJoinEntity:
					toReturn = this.ChkTypeGratRevJoin;
					break;
				case InfoGenesis.EntityType.ChkTypeGratTaxJoinEntity:
					toReturn = this.ChkTypeGratTaxJoin;
					break;
				case InfoGenesis.EntityType.ChkTypeTaxGrpJoinEntity:
					toReturn = this.ChkTypeTaxGrpJoin;
					break;
				case InfoGenesis.EntityType.ChkTypeTaxGrpTaxJoinEntity:
					toReturn = this.ChkTypeTaxGrpTaxJoin;
					break;
				case InfoGenesis.EntityType.ChkTypeVatGratJoinEntity:
					toReturn = this.ChkTypeVatGratJoin;
					break;
				case InfoGenesis.EntityType.ChkTypeVatRevJoinEntity:
					toReturn = this.ChkTypeVatRevJoin;
					break;
				case InfoGenesis.EntityType.ChoiceGroupMasterEntity:
					toReturn = this.ChoiceGroupMaster;
					break;
				case InfoGenesis.EntityType.ChoiceGrpModJoinEntity:
					toReturn = this.ChoiceGrpModJoin;
					break;
				case InfoGenesis.EntityType.ComboItemJoinEntity:
					toReturn = this.ComboItemJoin;
					break;
				case InfoGenesis.EntityType.ComboMasterEntity:
					toReturn = this.ComboMaster;
					break;
				case InfoGenesis.EntityType.ConfigurationIdMasterEntity:
					toReturn = this.ConfigurationIdMaster;
					break;
				case InfoGenesis.EntityType.DeviceMasterEntity:
					toReturn = this.DeviceMaster;
					break;
				case InfoGenesis.EntityType.DiscoupMasterEntity:
					toReturn = this.DiscoupMaster;
					break;
				case InfoGenesis.EntityType.EmpGroupMasterEntity:
					toReturn = this.EmpGroupMaster;
					break;
				case InfoGenesis.EntityType.EmpGrpEmpJoinEntity:
					toReturn = this.EmpGrpEmpJoin;
					break;
				case InfoGenesis.EntityType.EmpJobcodeJoinEntity:
					toReturn = this.EmpJobcodeJoin;
					break;
				case InfoGenesis.EntityType.EmpMasterEntity:
					toReturn = this.EmpMaster;
					break;
				case InfoGenesis.EntityType.FuncBttnTextJoinEntity:
					toReturn = this.FuncBttnTextJoin;
					break;
				case InfoGenesis.EntityType.Ga4680ExportParmMasterEntity:
					toReturn = this.Ga4680ExportParmMaster;
					break;
				case InfoGenesis.EntityType.Ga4680ImportParmMasterEntity:
					toReturn = this.Ga4680ImportParmMaster;
					break;
				case InfoGenesis.EntityType.GeneralLedgerMapMasterEntity:
					toReturn = this.GeneralLedgerMapMaster;
					break;
				case InfoGenesis.EntityType.GiftCardMasterEntity:
					toReturn = this.GiftCardMaster;
					break;
				case InfoGenesis.EntityType.GratuityCatMasterEntity:
					toReturn = this.GratuityCatMaster;
					break;
				case InfoGenesis.EntityType.GratuityMasterEntity:
					toReturn = this.GratuityMaster;
					break;
				case InfoGenesis.EntityType.IdMasterEntity:
					toReturn = this.IdMaster;
					break;
				case InfoGenesis.EntityType.ImageLibraryMasterEntity:
					toReturn = this.ImageLibraryMaster;
					break;
				case InfoGenesis.EntityType.IntlDescriptorTextJoinEntity:
					toReturn = this.IntlDescriptorTextJoin;
					break;
				case InfoGenesis.EntityType.IpServerAttribMasterEntity:
					toReturn = this.IpServerAttribMaster;
					break;
				case InfoGenesis.EntityType.JobcodeFuncBttnJoinEntity:
					toReturn = this.JobcodeFuncBttnJoin;
					break;
				case InfoGenesis.EntityType.JobcodeFunctJoinEntity:
					toReturn = this.JobcodeFunctJoin;
					break;
				case InfoGenesis.EntityType.JobcodeMasterEntity:
					toReturn = this.JobcodeMaster;
					break;
				case InfoGenesis.EntityType.KdsCategoryMasterEntity:
					toReturn = this.KdsCategoryMaster;
					break;
				case InfoGenesis.EntityType.KdsVideoMasterEntity:
					toReturn = this.KdsVideoMaster;
					break;
				case InfoGenesis.EntityType.KpOptionGrpCopyJoinEntity:
					toReturn = this.KpOptionGrpCopyJoin;
					break;
				case InfoGenesis.EntityType.KpOptionGrpFormatJoinEntity:
					toReturn = this.KpOptionGrpFormatJoin;
					break;
				case InfoGenesis.EntityType.KpOptionGrpMasterEntity:
					toReturn = this.KpOptionGrpMaster;
					break;
				case InfoGenesis.EntityType.KpPrinterMasterEntity:
					toReturn = this.KpPrinterMaster;
					break;
				case InfoGenesis.EntityType.KpRouteMasterEntity:
					toReturn = this.KpRouteMaster;
					break;
				case InfoGenesis.EntityType.KpRoutePrinterJoinEntity:
					toReturn = this.KpRoutePrinterJoin;
					break;
				case InfoGenesis.EntityType.MachineOptionsMasterEntity:
					toReturn = this.MachineOptionsMaster;
					break;
				case InfoGenesis.EntityType.MealPeriodMasterEntity:
					toReturn = this.MealPeriodMaster;
					break;
				case InfoGenesis.EntityType.MembershipMasterEntity:
					toReturn = this.MembershipMaster;
					break;
				case InfoGenesis.EntityType.MembershipProfitCenterJoinEntity:
					toReturn = this.MembershipProfitCenterJoin;
					break;
				case InfoGenesis.EntityType.MenuBttnObjJoinEntity:
					toReturn = this.MenuBttnObjJoin;
					break;
				case InfoGenesis.EntityType.MenuItemGroupMasterEntity:
					toReturn = this.MenuItemGroupMaster;
					break;
				case InfoGenesis.EntityType.MenuItemMasterEntity:
					toReturn = this.MenuItemMaster;
					break;
				case InfoGenesis.EntityType.MenuMasterEntity:
					toReturn = this.MenuMaster;
					break;
				case InfoGenesis.EntityType.MenuMigrpJoinEntity:
					toReturn = this.MenuMigrpJoin;
					break;
				case InfoGenesis.EntityType.MiChoiceGrpJoinEntity:
					toReturn = this.MiChoiceGrpJoin;
					break;
				case InfoGenesis.EntityType.MiKpprinterJoinEntity:
					toReturn = this.MiKpprinterJoin;
					break;
				case InfoGenesis.EntityType.MiPriceJoinEntity:
					toReturn = this.MiPriceJoin;
					break;
				case InfoGenesis.EntityType.MiSkuJoinEntity:
					toReturn = this.MiSkuJoin;
					break;
				case InfoGenesis.EntityType.ModifierKpprinterJoinEntity:
					toReturn = this.ModifierKpprinterJoin;
					break;
				case InfoGenesis.EntityType.ModifierMasterEntity:
					toReturn = this.ModifierMaster;
					break;
				case InfoGenesis.EntityType.ModifierPriceJoinEntity:
					toReturn = this.ModifierPriceJoin;
					break;
				case InfoGenesis.EntityType.OptionsMasterEntity:
					toReturn = this.OptionsMaster;
					break;
				case InfoGenesis.EntityType.PriceLevelMasterEntity:
					toReturn = this.PriceLevelMaster;
					break;
				case InfoGenesis.EntityType.ProcessMasterEntity:
					toReturn = this.ProcessMaster;
					break;
				case InfoGenesis.EntityType.ProductClassMasterEntity:
					toReturn = this.ProductClassMaster;
					break;
				case InfoGenesis.EntityType.ProfitCenterDayPartJoinEntity:
					toReturn = this.ProfitCenterDayPartJoin;
					break;
				case InfoGenesis.EntityType.ProfitCenterGroupJoinEntity:
					toReturn = this.ProfitCenterGroupJoin;
					break;
				case InfoGenesis.EntityType.ProfitCenterGroupMasterEntity:
					toReturn = this.ProfitCenterGroupMaster;
					break;
				case InfoGenesis.EntityType.ProfitCenterMasterEntity:
					toReturn = this.ProfitCenterMaster;
					break;
				case InfoGenesis.EntityType.ProfitCenterReceiptPrinterJoinEntity:
					toReturn = this.ProfitCenterReceiptPrinterJoin;
					break;
				case InfoGenesis.EntityType.ProfitCenterTableJoinEntity:
					toReturn = this.ProfitCenterTableJoin;
					break;
				case InfoGenesis.EntityType.QuickTenderMasterEntity:
					toReturn = this.QuickTenderMaster;
					break;
				case InfoGenesis.EntityType.ReceiptPrinterMasterEntity:
					toReturn = this.ReceiptPrinterMaster;
					break;
				case InfoGenesis.EntityType.ReportCatMasterEntity:
					toReturn = this.ReportCatMaster;
					break;
				case InfoGenesis.EntityType.RevenueCatMasterEntity:
					toReturn = this.RevenueCatMaster;
					break;
				case InfoGenesis.EntityType.RevenueClassMasterEntity:
					toReturn = this.RevenueClassMaster;
					break;
				case InfoGenesis.EntityType.RowLockMasterEntity:
					toReturn = this.RowLockMaster;
					break;
				case InfoGenesis.EntityType.ScreenTemplateMasterEntity:
					toReturn = this.ScreenTemplateMaster;
					break;
				case InfoGenesis.EntityType.ScreenTemplateMigrpJoinEntity:
					toReturn = this.ScreenTemplateMigrpJoin;
					break;
				case InfoGenesis.EntityType.SecurityLevelMasterEntity:
					toReturn = this.SecurityLevelMaster;
					break;
				case InfoGenesis.EntityType.SelectionGroupItemJoinEntity:
					toReturn = this.SelectionGroupItemJoin;
					break;
				case InfoGenesis.EntityType.SelectionGroupMasterEntity:
					toReturn = this.SelectionGroupMaster;
					break;
				case InfoGenesis.EntityType.ServiceManagerProxyMasterEntity:
					toReturn = this.ServiceManagerProxyMaster;
					break;
				case InfoGenesis.EntityType.SmuCommandMasterEntity:
					toReturn = this.SmuCommandMaster;
					break;
				case InfoGenesis.EntityType.SmuStatusMasterEntity:
					toReturn = this.SmuStatusMaster;
					break;
				case InfoGenesis.EntityType.SpecialInstrMasterEntity:
					toReturn = this.SpecialInstrMaster;
					break;
				case InfoGenesis.EntityType.SystemConfigurationMasterEntity:
					toReturn = this.SystemConfigurationMaster;
					break;
				case InfoGenesis.EntityType.TableLayoutMasterEntity:
					toReturn = this.TableLayoutMaster;
					break;
				case InfoGenesis.EntityType.TaxCatMasterEntity:
					toReturn = this.TaxCatMaster;
					break;
				case InfoGenesis.EntityType.TaxGroupMasterEntity:
					toReturn = this.TaxGroupMaster;
					break;
				case InfoGenesis.EntityType.TaxGrpTaxJoinEntity:
					toReturn = this.TaxGrpTaxJoin;
					break;
				case InfoGenesis.EntityType.TaxMasterEntity:
					toReturn = this.TaxMaster;
					break;
				case InfoGenesis.EntityType.TaxRevCatJoinEntity:
					toReturn = this.TaxRevCatJoin;
					break;
				case InfoGenesis.EntityType.TenderClassMasterEntity:
					toReturn = this.TenderClassMaster;
					break;
				case InfoGenesis.EntityType.TenderMasterEntity:
					toReturn = this.TenderMaster;
					break;
				case InfoGenesis.EntityType.TermGrpChkTypeJoinEntity:
					toReturn = this.TermGrpChkTypeJoin;
					break;
				case InfoGenesis.EntityType.TermGrpJobcodeJoinEntity:
					toReturn = this.TermGrpJobcodeJoin;
					break;
				case InfoGenesis.EntityType.TermGrpMasterEntity:
					toReturn = this.TermGrpMaster;
					break;
				case InfoGenesis.EntityType.TermGrpMenuJoinEntity:
					toReturn = this.TermGrpMenuJoin;
					break;
				case InfoGenesis.EntityType.TermGrpMenuJoinCookerEntity:
					toReturn = this.TermGrpMenuJoinCooker;
					break;
				case InfoGenesis.EntityType.TermGrpMpJoinEntity:
					toReturn = this.TermGrpMpJoin;
					break;
				case InfoGenesis.EntityType.TermGrpMpmenuJoinEntity:
					toReturn = this.TermGrpMpmenuJoin;
					break;
				case InfoGenesis.EntityType.TermGrpTndrJoinEntity:
					toReturn = this.TermGrpTndrJoin;
					break;
				case InfoGenesis.EntityType.TermGrpTransitionJoinEntity:
					toReturn = this.TermGrpTransitionJoin;
					break;
				case InfoGenesis.EntityType.TerminalMasterEntity:
					toReturn = this.TerminalMaster;
					break;
				case InfoGenesis.EntityType.TerminalProfitCtrJoinEntity:
					toReturn = this.TerminalProfitCtrJoin;
					break;
				case InfoGenesis.EntityType.TerminalTextGuestInfoJoinEntity:
					toReturn = this.TerminalTextGuestInfoJoin;
					break;
				case InfoGenesis.EntityType.TerminalTextMasterEntity:
					toReturn = this.TerminalTextMaster;
					break;
				case InfoGenesis.EntityType.TermOptionGrpMasterEntity:
					toReturn = this.TermOptionGrpMaster;
					break;
				case InfoGenesis.EntityType.TermOptionGrpOptionJoinEntity:
					toReturn = this.TermOptionGrpOptionJoin;
					break;
				case InfoGenesis.EntityType.TermPrinterGrpKpprntJoinEntity:
					toReturn = this.TermPrinterGrpKpprntJoin;
					break;
				case InfoGenesis.EntityType.TermPrinterGrpMasterEntity:
					toReturn = this.TermPrinterGrpMaster;
					break;
				case InfoGenesis.EntityType.TermPrinterGrpRouteJoinEntity:
					toReturn = this.TermPrinterGrpRouteJoin;
					break;
				case InfoGenesis.EntityType.ThemeColorJoinEntity:
					toReturn = this.ThemeColorJoin;
					break;
				case InfoGenesis.EntityType.ThemeFontJoinEntity:
					toReturn = this.ThemeFontJoin;
					break;
				case InfoGenesis.EntityType.ThemeMasterEntity:
					toReturn = this.ThemeMaster;
					break;
				case InfoGenesis.EntityType.ThemeObjectJoinEntity:
					toReturn = this.ThemeObjectJoin;
					break;
				case InfoGenesis.EntityType.UdefDateRangeInstanceEntity:
					toReturn = this.UdefDateRangeInstance;
					break;
				case InfoGenesis.EntityType.UdefPeriodTypeMasterEntity:
					toReturn = this.UdefPeriodTypeMaster;
					break;
				case InfoGenesis.EntityType.VoidReasonMasterEntity:
					toReturn = this.VoidReasonMaster;
					break;
				case InfoGenesis.EntityType.ZooMasterEntity:
					toReturn = this.ZooMaster;
					break;
				default:
					toReturn = null;
					break;
			}
			return toReturn;
		}

		/// <summary>returns the datasource to use in a Linq query when targeting MyBonusCodeMasterEntity instances in the database.</summary>
		public DataSource2<MyBonusCodeMasterEntity> BonusCodeMaster
		{
			get { return new DataSource2<MyBonusCodeMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyButtonAbbrJoinEntity instances in the database.</summary>
		public DataSource2<MyButtonAbbrJoinEntity> ButtonAbbrJoin
		{
			get { return new DataSource2<MyButtonAbbrJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyCardTypeMasterEntity instances in the database.</summary>
		public DataSource2<MyCardTypeMasterEntity> CardTypeMaster
		{
			get { return new DataSource2<MyCardTypeMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyCardTypeRangeJoinEntity instances in the database.</summary>
		public DataSource2<MyCardTypeRangeJoinEntity> CardTypeRangeJoin
		{
			get { return new DataSource2<MyCardTypeRangeJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyCfgVersionEntity instances in the database.</summary>
		public DataSource2<MyCfgVersionEntity> CfgVersion
		{
			get { return new DataSource2<MyCfgVersionEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyCheckTypeMasterEntity instances in the database.</summary>
		public DataSource2<MyCheckTypeMasterEntity> CheckTypeMaster
		{
			get { return new DataSource2<MyCheckTypeMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyChefMasterEntity instances in the database.</summary>
		public DataSource2<MyChefMasterEntity> ChefMaster
		{
			get { return new DataSource2<MyChefMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyChkTypeGratJoinEntity instances in the database.</summary>
		public DataSource2<MyChkTypeGratJoinEntity> ChkTypeGratJoin
		{
			get { return new DataSource2<MyChkTypeGratJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyChkTypeGratRevJoinEntity instances in the database.</summary>
		public DataSource2<MyChkTypeGratRevJoinEntity> ChkTypeGratRevJoin
		{
			get { return new DataSource2<MyChkTypeGratRevJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyChkTypeGratTaxJoinEntity instances in the database.</summary>
		public DataSource2<MyChkTypeGratTaxJoinEntity> ChkTypeGratTaxJoin
		{
			get { return new DataSource2<MyChkTypeGratTaxJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyChkTypeTaxGrpJoinEntity instances in the database.</summary>
		public DataSource2<MyChkTypeTaxGrpJoinEntity> ChkTypeTaxGrpJoin
		{
			get { return new DataSource2<MyChkTypeTaxGrpJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyChkTypeTaxGrpTaxJoinEntity instances in the database.</summary>
		public DataSource2<MyChkTypeTaxGrpTaxJoinEntity> ChkTypeTaxGrpTaxJoin
		{
			get { return new DataSource2<MyChkTypeTaxGrpTaxJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyChkTypeVatGratJoinEntity instances in the database.</summary>
		public DataSource2<MyChkTypeVatGratJoinEntity> ChkTypeVatGratJoin
		{
			get { return new DataSource2<MyChkTypeVatGratJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyChkTypeVatRevJoinEntity instances in the database.</summary>
		public DataSource2<MyChkTypeVatRevJoinEntity> ChkTypeVatRevJoin
		{
			get { return new DataSource2<MyChkTypeVatRevJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyChoiceGroupMasterEntity instances in the database.</summary>
		public DataSource2<MyChoiceGroupMasterEntity> ChoiceGroupMaster
		{
			get { return new DataSource2<MyChoiceGroupMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyChoiceGrpModJoinEntity instances in the database.</summary>
		public DataSource2<MyChoiceGrpModJoinEntity> ChoiceGrpModJoin
		{
			get { return new DataSource2<MyChoiceGrpModJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyComboItemJoinEntity instances in the database.</summary>
		public DataSource2<MyComboItemJoinEntity> ComboItemJoin
		{
			get { return new DataSource2<MyComboItemJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyComboMasterEntity instances in the database.</summary>
		public DataSource2<MyComboMasterEntity> ComboMaster
		{
			get { return new DataSource2<MyComboMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyConfigurationIdMasterEntity instances in the database.</summary>
		public DataSource2<MyConfigurationIdMasterEntity> ConfigurationIdMaster
		{
			get { return new DataSource2<MyConfigurationIdMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyDeviceMasterEntity instances in the database.</summary>
		public DataSource2<MyDeviceMasterEntity> DeviceMaster
		{
			get { return new DataSource2<MyDeviceMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyDiscoupMasterEntity instances in the database.</summary>
		public DataSource2<MyDiscoupMasterEntity> DiscoupMaster
		{
			get { return new DataSource2<MyDiscoupMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyEmpGroupMasterEntity instances in the database.</summary>
		public DataSource2<MyEmpGroupMasterEntity> EmpGroupMaster
		{
			get { return new DataSource2<MyEmpGroupMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyEmpGrpEmpJoinEntity instances in the database.</summary>
		public DataSource2<MyEmpGrpEmpJoinEntity> EmpGrpEmpJoin
		{
			get { return new DataSource2<MyEmpGrpEmpJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyEmpJobcodeJoinEntity instances in the database.</summary>
		public DataSource2<MyEmpJobcodeJoinEntity> EmpJobcodeJoin
		{
			get { return new DataSource2<MyEmpJobcodeJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyEmpMasterEntity instances in the database.</summary>
		public DataSource2<MyEmpMasterEntity> EmpMaster
		{
			get { return new DataSource2<MyEmpMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyFuncBttnTextJoinEntity instances in the database.</summary>
		public DataSource2<MyFuncBttnTextJoinEntity> FuncBttnTextJoin
		{
			get { return new DataSource2<MyFuncBttnTextJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyGa4680ExportParmMasterEntity instances in the database.</summary>
		public DataSource2<MyGa4680ExportParmMasterEntity> Ga4680ExportParmMaster
		{
			get { return new DataSource2<MyGa4680ExportParmMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyGa4680ImportParmMasterEntity instances in the database.</summary>
		public DataSource2<MyGa4680ImportParmMasterEntity> Ga4680ImportParmMaster
		{
			get { return new DataSource2<MyGa4680ImportParmMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyGeneralLedgerMapMasterEntity instances in the database.</summary>
		public DataSource2<MyGeneralLedgerMapMasterEntity> GeneralLedgerMapMaster
		{
			get { return new DataSource2<MyGeneralLedgerMapMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyGiftCardMasterEntity instances in the database.</summary>
		public DataSource2<MyGiftCardMasterEntity> GiftCardMaster
		{
			get { return new DataSource2<MyGiftCardMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyGratuityCatMasterEntity instances in the database.</summary>
		public DataSource2<MyGratuityCatMasterEntity> GratuityCatMaster
		{
			get { return new DataSource2<MyGratuityCatMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyGratuityMasterEntity instances in the database.</summary>
		public DataSource2<MyGratuityMasterEntity> GratuityMaster
		{
			get { return new DataSource2<MyGratuityMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyIdMasterEntity instances in the database.</summary>
		public DataSource2<MyIdMasterEntity> IdMaster
		{
			get { return new DataSource2<MyIdMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyImageLibraryMasterEntity instances in the database.</summary>
		public DataSource2<MyImageLibraryMasterEntity> ImageLibraryMaster
		{
			get { return new DataSource2<MyImageLibraryMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyIntlDescriptorTextJoinEntity instances in the database.</summary>
		public DataSource2<MyIntlDescriptorTextJoinEntity> IntlDescriptorTextJoin
		{
			get { return new DataSource2<MyIntlDescriptorTextJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyIpServerAttribMasterEntity instances in the database.</summary>
		public DataSource2<MyIpServerAttribMasterEntity> IpServerAttribMaster
		{
			get { return new DataSource2<MyIpServerAttribMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyJobcodeFuncBttnJoinEntity instances in the database.</summary>
		public DataSource2<MyJobcodeFuncBttnJoinEntity> JobcodeFuncBttnJoin
		{
			get { return new DataSource2<MyJobcodeFuncBttnJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyJobcodeFunctJoinEntity instances in the database.</summary>
		public DataSource2<MyJobcodeFunctJoinEntity> JobcodeFunctJoin
		{
			get { return new DataSource2<MyJobcodeFunctJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyJobcodeMasterEntity instances in the database.</summary>
		public DataSource2<MyJobcodeMasterEntity> JobcodeMaster
		{
			get { return new DataSource2<MyJobcodeMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyKdsCategoryMasterEntity instances in the database.</summary>
		public DataSource2<MyKdsCategoryMasterEntity> KdsCategoryMaster
		{
			get { return new DataSource2<MyKdsCategoryMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyKdsVideoMasterEntity instances in the database.</summary>
		public DataSource2<MyKdsVideoMasterEntity> KdsVideoMaster
		{
			get { return new DataSource2<MyKdsVideoMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyKpOptionGrpCopyJoinEntity instances in the database.</summary>
		public DataSource2<MyKpOptionGrpCopyJoinEntity> KpOptionGrpCopyJoin
		{
			get { return new DataSource2<MyKpOptionGrpCopyJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyKpOptionGrpFormatJoinEntity instances in the database.</summary>
		public DataSource2<MyKpOptionGrpFormatJoinEntity> KpOptionGrpFormatJoin
		{
			get { return new DataSource2<MyKpOptionGrpFormatJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyKpOptionGrpMasterEntity instances in the database.</summary>
		public DataSource2<MyKpOptionGrpMasterEntity> KpOptionGrpMaster
		{
			get { return new DataSource2<MyKpOptionGrpMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyKpPrinterMasterEntity instances in the database.</summary>
		public DataSource2<MyKpPrinterMasterEntity> KpPrinterMaster
		{
			get { return new DataSource2<MyKpPrinterMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyKpRouteMasterEntity instances in the database.</summary>
		public DataSource2<MyKpRouteMasterEntity> KpRouteMaster
		{
			get { return new DataSource2<MyKpRouteMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyKpRoutePrinterJoinEntity instances in the database.</summary>
		public DataSource2<MyKpRoutePrinterJoinEntity> KpRoutePrinterJoin
		{
			get { return new DataSource2<MyKpRoutePrinterJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyMachineOptionsMasterEntity instances in the database.</summary>
		public DataSource2<MyMachineOptionsMasterEntity> MachineOptionsMaster
		{
			get { return new DataSource2<MyMachineOptionsMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyMealPeriodMasterEntity instances in the database.</summary>
		public DataSource2<MyMealPeriodMasterEntity> MealPeriodMaster
		{
			get { return new DataSource2<MyMealPeriodMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyMembershipMasterEntity instances in the database.</summary>
		public DataSource2<MyMembershipMasterEntity> MembershipMaster
		{
			get { return new DataSource2<MyMembershipMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyMembershipProfitCenterJoinEntity instances in the database.</summary>
		public DataSource2<MyMembershipProfitCenterJoinEntity> MembershipProfitCenterJoin
		{
			get { return new DataSource2<MyMembershipProfitCenterJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyMenuBttnObjJoinEntity instances in the database.</summary>
		public DataSource2<MyMenuBttnObjJoinEntity> MenuBttnObjJoin
		{
			get { return new DataSource2<MyMenuBttnObjJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyMenuItemGroupMasterEntity instances in the database.</summary>
		public DataSource2<MyMenuItemGroupMasterEntity> MenuItemGroupMaster
		{
			get { return new DataSource2<MyMenuItemGroupMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyMenuItemMasterEntity instances in the database.</summary>
		public DataSource2<MyMenuItemMasterEntity> MenuItemMaster
		{
			get { return new DataSource2<MyMenuItemMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyMenuMasterEntity instances in the database.</summary>
		public DataSource2<MyMenuMasterEntity> MenuMaster
		{
			get { return new DataSource2<MyMenuMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyMenuMigrpJoinEntity instances in the database.</summary>
		public DataSource2<MyMenuMigrpJoinEntity> MenuMigrpJoin
		{
			get { return new DataSource2<MyMenuMigrpJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyMiChoiceGrpJoinEntity instances in the database.</summary>
		public DataSource2<MyMiChoiceGrpJoinEntity> MiChoiceGrpJoin
		{
			get { return new DataSource2<MyMiChoiceGrpJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyMiKpprinterJoinEntity instances in the database.</summary>
		public DataSource2<MyMiKpprinterJoinEntity> MiKpprinterJoin
		{
			get { return new DataSource2<MyMiKpprinterJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyMiPriceJoinEntity instances in the database.</summary>
		public DataSource2<MyMiPriceJoinEntity> MiPriceJoin
		{
			get { return new DataSource2<MyMiPriceJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyMiSkuJoinEntity instances in the database.</summary>
		public DataSource2<MyMiSkuJoinEntity> MiSkuJoin
		{
			get { return new DataSource2<MyMiSkuJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyModifierKpprinterJoinEntity instances in the database.</summary>
		public DataSource2<MyModifierKpprinterJoinEntity> ModifierKpprinterJoin
		{
			get { return new DataSource2<MyModifierKpprinterJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyModifierMasterEntity instances in the database.</summary>
		public DataSource2<MyModifierMasterEntity> ModifierMaster
		{
			get { return new DataSource2<MyModifierMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyModifierPriceJoinEntity instances in the database.</summary>
		public DataSource2<MyModifierPriceJoinEntity> ModifierPriceJoin
		{
			get { return new DataSource2<MyModifierPriceJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyOptionsMasterEntity instances in the database.</summary>
		public DataSource2<MyOptionsMasterEntity> OptionsMaster
		{
			get { return new DataSource2<MyOptionsMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyPriceLevelMasterEntity instances in the database.</summary>
		public DataSource2<MyPriceLevelMasterEntity> PriceLevelMaster
		{
			get { return new DataSource2<MyPriceLevelMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyProcessMasterEntity instances in the database.</summary>
		public DataSource2<MyProcessMasterEntity> ProcessMaster
		{
			get { return new DataSource2<MyProcessMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyProductClassMasterEntity instances in the database.</summary>
		public DataSource2<MyProductClassMasterEntity> ProductClassMaster
		{
			get { return new DataSource2<MyProductClassMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyProfitCenterDayPartJoinEntity instances in the database.</summary>
		public DataSource2<MyProfitCenterDayPartJoinEntity> ProfitCenterDayPartJoin
		{
			get { return new DataSource2<MyProfitCenterDayPartJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyProfitCenterGroupJoinEntity instances in the database.</summary>
		public DataSource2<MyProfitCenterGroupJoinEntity> ProfitCenterGroupJoin
		{
			get { return new DataSource2<MyProfitCenterGroupJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyProfitCenterGroupMasterEntity instances in the database.</summary>
		public DataSource2<MyProfitCenterGroupMasterEntity> ProfitCenterGroupMaster
		{
			get { return new DataSource2<MyProfitCenterGroupMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyProfitCenterMasterEntity instances in the database.</summary>
		public DataSource2<MyProfitCenterMasterEntity> ProfitCenterMaster
		{
			get { return new DataSource2<MyProfitCenterMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyProfitCenterReceiptPrinterJoinEntity instances in the database.</summary>
		public DataSource2<MyProfitCenterReceiptPrinterJoinEntity> ProfitCenterReceiptPrinterJoin
		{
			get { return new DataSource2<MyProfitCenterReceiptPrinterJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyProfitCenterTableJoinEntity instances in the database.</summary>
		public DataSource2<MyProfitCenterTableJoinEntity> ProfitCenterTableJoin
		{
			get { return new DataSource2<MyProfitCenterTableJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyQuickTenderMasterEntity instances in the database.</summary>
		public DataSource2<MyQuickTenderMasterEntity> QuickTenderMaster
		{
			get { return new DataSource2<MyQuickTenderMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyReceiptPrinterMasterEntity instances in the database.</summary>
		public DataSource2<MyReceiptPrinterMasterEntity> ReceiptPrinterMaster
		{
			get { return new DataSource2<MyReceiptPrinterMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyReportCatMasterEntity instances in the database.</summary>
		public DataSource2<MyReportCatMasterEntity> ReportCatMaster
		{
			get { return new DataSource2<MyReportCatMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyRevenueCatMasterEntity instances in the database.</summary>
		public DataSource2<MyRevenueCatMasterEntity> RevenueCatMaster
		{
			get { return new DataSource2<MyRevenueCatMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyRevenueClassMasterEntity instances in the database.</summary>
		public DataSource2<MyRevenueClassMasterEntity> RevenueClassMaster
		{
			get { return new DataSource2<MyRevenueClassMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyRowLockMasterEntity instances in the database.</summary>
		public DataSource2<MyRowLockMasterEntity> RowLockMaster
		{
			get { return new DataSource2<MyRowLockMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyScreenTemplateMasterEntity instances in the database.</summary>
		public DataSource2<MyScreenTemplateMasterEntity> ScreenTemplateMaster
		{
			get { return new DataSource2<MyScreenTemplateMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyScreenTemplateMigrpJoinEntity instances in the database.</summary>
		public DataSource2<MyScreenTemplateMigrpJoinEntity> ScreenTemplateMigrpJoin
		{
			get { return new DataSource2<MyScreenTemplateMigrpJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MySecurityLevelMasterEntity instances in the database.</summary>
		public DataSource2<MySecurityLevelMasterEntity> SecurityLevelMaster
		{
			get { return new DataSource2<MySecurityLevelMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MySelectionGroupItemJoinEntity instances in the database.</summary>
		public DataSource2<MySelectionGroupItemJoinEntity> SelectionGroupItemJoin
		{
			get { return new DataSource2<MySelectionGroupItemJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MySelectionGroupMasterEntity instances in the database.</summary>
		public DataSource2<MySelectionGroupMasterEntity> SelectionGroupMaster
		{
			get { return new DataSource2<MySelectionGroupMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyServiceManagerProxyMasterEntity instances in the database.</summary>
		public DataSource2<MyServiceManagerProxyMasterEntity> ServiceManagerProxyMaster
		{
			get { return new DataSource2<MyServiceManagerProxyMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MySmuCommandMasterEntity instances in the database.</summary>
		public DataSource2<MySmuCommandMasterEntity> SmuCommandMaster
		{
			get { return new DataSource2<MySmuCommandMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MySmuStatusMasterEntity instances in the database.</summary>
		public DataSource2<MySmuStatusMasterEntity> SmuStatusMaster
		{
			get { return new DataSource2<MySmuStatusMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MySpecialInstrMasterEntity instances in the database.</summary>
		public DataSource2<MySpecialInstrMasterEntity> SpecialInstrMaster
		{
			get { return new DataSource2<MySpecialInstrMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MySystemConfigurationMasterEntity instances in the database.</summary>
		public DataSource2<MySystemConfigurationMasterEntity> SystemConfigurationMaster
		{
			get { return new DataSource2<MySystemConfigurationMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyTableLayoutMasterEntity instances in the database.</summary>
		public DataSource2<MyTableLayoutMasterEntity> TableLayoutMaster
		{
			get { return new DataSource2<MyTableLayoutMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyTaxCatMasterEntity instances in the database.</summary>
		public DataSource2<MyTaxCatMasterEntity> TaxCatMaster
		{
			get { return new DataSource2<MyTaxCatMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyTaxGroupMasterEntity instances in the database.</summary>
		public DataSource2<MyTaxGroupMasterEntity> TaxGroupMaster
		{
			get { return new DataSource2<MyTaxGroupMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyTaxGrpTaxJoinEntity instances in the database.</summary>
		public DataSource2<MyTaxGrpTaxJoinEntity> TaxGrpTaxJoin
		{
			get { return new DataSource2<MyTaxGrpTaxJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyTaxMasterEntity instances in the database.</summary>
		public DataSource2<MyTaxMasterEntity> TaxMaster
		{
			get { return new DataSource2<MyTaxMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyTaxRevCatJoinEntity instances in the database.</summary>
		public DataSource2<MyTaxRevCatJoinEntity> TaxRevCatJoin
		{
			get { return new DataSource2<MyTaxRevCatJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyTenderClassMasterEntity instances in the database.</summary>
		public DataSource2<MyTenderClassMasterEntity> TenderClassMaster
		{
			get { return new DataSource2<MyTenderClassMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyTenderMasterEntity instances in the database.</summary>
		public DataSource2<MyTenderMasterEntity> TenderMaster
		{
			get { return new DataSource2<MyTenderMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyTermGrpChkTypeJoinEntity instances in the database.</summary>
		public DataSource2<MyTermGrpChkTypeJoinEntity> TermGrpChkTypeJoin
		{
			get { return new DataSource2<MyTermGrpChkTypeJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyTermGrpJobcodeJoinEntity instances in the database.</summary>
		public DataSource2<MyTermGrpJobcodeJoinEntity> TermGrpJobcodeJoin
		{
			get { return new DataSource2<MyTermGrpJobcodeJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyTermGrpMasterEntity instances in the database.</summary>
		public DataSource2<MyTermGrpMasterEntity> TermGrpMaster
		{
			get { return new DataSource2<MyTermGrpMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyTermGrpMenuJoinEntity instances in the database.</summary>
		public DataSource2<MyTermGrpMenuJoinEntity> TermGrpMenuJoin
		{
			get { return new DataSource2<MyTermGrpMenuJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyTermGrpMenuJoinCookerEntity instances in the database.</summary>
		public DataSource2<MyTermGrpMenuJoinCookerEntity> TermGrpMenuJoinCooker
		{
			get { return new DataSource2<MyTermGrpMenuJoinCookerEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyTermGrpMpJoinEntity instances in the database.</summary>
		public DataSource2<MyTermGrpMpJoinEntity> TermGrpMpJoin
		{
			get { return new DataSource2<MyTermGrpMpJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyTermGrpMpmenuJoinEntity instances in the database.</summary>
		public DataSource2<MyTermGrpMpmenuJoinEntity> TermGrpMpmenuJoin
		{
			get { return new DataSource2<MyTermGrpMpmenuJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyTermGrpTndrJoinEntity instances in the database.</summary>
		public DataSource2<MyTermGrpTndrJoinEntity> TermGrpTndrJoin
		{
			get { return new DataSource2<MyTermGrpTndrJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyTermGrpTransitionJoinEntity instances in the database.</summary>
		public DataSource2<MyTermGrpTransitionJoinEntity> TermGrpTransitionJoin
		{
			get { return new DataSource2<MyTermGrpTransitionJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyTerminalMasterEntity instances in the database.</summary>
		public DataSource2<MyTerminalMasterEntity> TerminalMaster
		{
			get { return new DataSource2<MyTerminalMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyTerminalProfitCtrJoinEntity instances in the database.</summary>
		public DataSource2<MyTerminalProfitCtrJoinEntity> TerminalProfitCtrJoin
		{
			get { return new DataSource2<MyTerminalProfitCtrJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyTerminalTextGuestInfoJoinEntity instances in the database.</summary>
		public DataSource2<MyTerminalTextGuestInfoJoinEntity> TerminalTextGuestInfoJoin
		{
			get { return new DataSource2<MyTerminalTextGuestInfoJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyTerminalTextMasterEntity instances in the database.</summary>
		public DataSource2<MyTerminalTextMasterEntity> TerminalTextMaster
		{
			get { return new DataSource2<MyTerminalTextMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyTermOptionGrpMasterEntity instances in the database.</summary>
		public DataSource2<MyTermOptionGrpMasterEntity> TermOptionGrpMaster
		{
			get { return new DataSource2<MyTermOptionGrpMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyTermOptionGrpOptionJoinEntity instances in the database.</summary>
		public DataSource2<MyTermOptionGrpOptionJoinEntity> TermOptionGrpOptionJoin
		{
			get { return new DataSource2<MyTermOptionGrpOptionJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyTermPrinterGrpKpprntJoinEntity instances in the database.</summary>
		public DataSource2<MyTermPrinterGrpKpprntJoinEntity> TermPrinterGrpKpprntJoin
		{
			get { return new DataSource2<MyTermPrinterGrpKpprntJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyTermPrinterGrpMasterEntity instances in the database.</summary>
		public DataSource2<MyTermPrinterGrpMasterEntity> TermPrinterGrpMaster
		{
			get { return new DataSource2<MyTermPrinterGrpMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyTermPrinterGrpRouteJoinEntity instances in the database.</summary>
		public DataSource2<MyTermPrinterGrpRouteJoinEntity> TermPrinterGrpRouteJoin
		{
			get { return new DataSource2<MyTermPrinterGrpRouteJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyThemeColorJoinEntity instances in the database.</summary>
		public DataSource2<MyThemeColorJoinEntity> ThemeColorJoin
		{
			get { return new DataSource2<MyThemeColorJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyThemeFontJoinEntity instances in the database.</summary>
		public DataSource2<MyThemeFontJoinEntity> ThemeFontJoin
		{
			get { return new DataSource2<MyThemeFontJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyThemeMasterEntity instances in the database.</summary>
		public DataSource2<MyThemeMasterEntity> ThemeMaster
		{
			get { return new DataSource2<MyThemeMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyThemeObjectJoinEntity instances in the database.</summary>
		public DataSource2<MyThemeObjectJoinEntity> ThemeObjectJoin
		{
			get { return new DataSource2<MyThemeObjectJoinEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyUdefDateRangeInstanceEntity instances in the database.</summary>
		public DataSource2<MyUdefDateRangeInstanceEntity> UdefDateRangeInstance
		{
			get { return new DataSource2<MyUdefDateRangeInstanceEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyUdefPeriodTypeMasterEntity instances in the database.</summary>
		public DataSource2<MyUdefPeriodTypeMasterEntity> UdefPeriodTypeMaster
		{
			get { return new DataSource2<MyUdefPeriodTypeMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyVoidReasonMasterEntity instances in the database.</summary>
		public DataSource2<MyVoidReasonMasterEntity> VoidReasonMaster
		{
			get { return new DataSource2<MyVoidReasonMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting MyZooMasterEntity instances in the database.</summary>
		public DataSource2<MyZooMasterEntity> ZooMaster
		{
			get { return new DataSource2<MyZooMasterEntity>(_adapterToUse, new MyElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		
		#region Class Property Declarations
		/// <summary> Gets / sets the IDataAccessAdapter to use for the queries created with this meta data object.</summary>
		/// <remarks> Be aware that the IDataAccessAdapter object set via this property is kept alive by the LLBLGenProQuery objects created with this meta data
		/// till they go out of scope.</remarks>
		public IDataAccessAdapter AdapterToUse
		{
			get { return _adapterToUse;}
			set { _adapterToUse = value;}
		}

		/// <summary>Gets or sets the custom function mappings to use. These take higher precedence than the ones in the DQE to use</summary>
		public FunctionMappingStore CustomFunctionMappings
		{
			get { return _customFunctionMappings; }
			set { _customFunctionMappings = value; }
		}
		
		/// <summary>Gets or sets the Context instance to use for entity fetches.</summary>
		public Context ContextToUse
		{
			get { return _contextToUse;}
			set { _contextToUse = value;}
		}
		#endregion
	}
}