﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Xml.Serialization;
using InfoGenesis;
using InfoGenesis.HelperClasses;
using InfoGenesis.FactoryClasses;
using InfoGenesis.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace InfoGenesis.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	

	/// <summary>Entity class which represents the entity 'MenuItemMaster'.<br/><br/></summary>
	[Serializable]
	public partial class MenuItemMasterEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
			
	{
		#region Class Member Declarations

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
		}
		#endregion
		
		/// <summary> Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static MenuItemMasterEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary> CTor</summary>
		public MenuItemMasterEntity():base("MenuItemMasterEntity")
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <remarks>For framework usage.</remarks>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public MenuItemMasterEntity(IEntityFields2 fields):base("MenuItemMasterEntity")
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this MenuItemMasterEntity</param>
		public MenuItemMasterEntity(IValidator validator):base("MenuItemMasterEntity")
		{
			InitClassEmpty(validator, null);
		}
				
		/// <summary> CTor</summary>
		/// <param name="entId">PK value for MenuItemMaster which data should be fetched into this MenuItemMaster object</param>
		/// <param name="menuItemId">PK value for MenuItemMaster which data should be fetched into this MenuItemMaster object</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public MenuItemMasterEntity(System.Int32 entId, System.Int32 menuItemId):base("MenuItemMasterEntity")
		{
			InitClassEmpty(null, null);
			this.EntId = entId;
			this.MenuItemId = menuItemId;
		}

		/// <summary> CTor</summary>
		/// <param name="entId">PK value for MenuItemMaster which data should be fetched into this MenuItemMaster object</param>
		/// <param name="menuItemId">PK value for MenuItemMaster which data should be fetched into this MenuItemMaster object</param>
		/// <param name="validator">The custom validator object for this MenuItemMasterEntity</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public MenuItemMasterEntity(System.Int32 entId, System.Int32 menuItemId, IValidator validator):base("MenuItemMasterEntity")
		{
			InitClassEmpty(validator, null);
			this.EntId = entId;
			this.MenuItemId = menuItemId;
		}

		/// <summary> Protected CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected MenuItemMasterEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			if(SerializationHelper.Optimization != SerializationOptimization.Fast) 
			{
				this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance());
			}
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
			
		}


		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}
		
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				default:
					break;				
			}
			return toReturn;
		}
#if !CF
		/// <summary>Checks if the relation mapped by the property with the name specified is a one way / single sided relation. If the passed in name is null, it/ will return true if the entity has any single-sided relation</summary>
		/// <param name="propertyName">Name of the property which is mapped onto the relation to check, or null to check if the entity has any relation/ which is single sided</param>
		/// <returns>true if the relation is single sided / one way (so the opposite relation isn't present), false otherwise</returns>
		protected override bool CheckOneWayRelations(string propertyName)
		{
			int numberOfOneWayRelations = 0;
			switch(propertyName)
			{
				case null:
					return ((numberOfOneWayRelations > 0) || base.CheckOneWayRelations(null));
				default:
					return base.CheckOneWayRelations(propertyName);
			}
		}
#endif
		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				default:
					break;
			}
		}

		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity2 objects, referenced by this entity</returns>
		protected override List<IEntity2> GetDependingRelatedEntities()
		{
			List<IEntity2> toReturn = new List<IEntity2>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These
		/// entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity2 objects, referenced by this entity</returns>
		protected override List<IEntity2> GetDependentRelatedEntities()
		{
			List<IEntity2> toReturn = new List<IEntity2>();
			return toReturn;
		}
		
		/// <summary>Gets a list of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection2 objects, referenced by this entity</returns>
		protected override List<IEntityCollection2> GetMemberEntityCollections()
		{
			List<IEntityCollection2> toReturn = new List<IEntityCollection2>();
			return toReturn;
		}

		/// <summary>ISerializable member. Does custom serialization so event handlers do not get serialized. Serializes members of this entity class and uses the base class' implementation to serialize the rest.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (SerializationHelper.Optimization != SerializationOptimization.Fast) 
			{
			}
			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			base.GetObjectData(info, context);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new MenuItemMasterRelations().GetAllRelations();
		}
		

		/// <summary>Creates a new instance of the factory related to this entity</summary>
		protected override IEntityFactory2 CreateEntityFactory()
		{
			return EntityFactoryCache2.GetEntityFactory(typeof(MenuItemMasterEntityFactory));
		}
#if !CF
		/// <summary>Adds the member collections to the collections queue (base first)</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		protected override void AddToMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue) 
		{
			base.AddToMemberEntityCollectionsQueue(collectionsQueue);
		}
		
		/// <summary>Gets the member collections queue from the queue (base first)</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		protected override void GetFromMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue)
		{
			base.GetFromMemberEntityCollectionsQueue(collectionsQueue);

		}
		
		/// <summary>Determines whether the entity has populated member collections</summary>
		/// <returns>true if the entity has populated member collections.</returns>
		protected override bool HasPopulatedMemberEntityCollections()
		{
			bool toReturn = false;
			return toReturn ? true : base.HasPopulatedMemberEntityCollections();
		}
		
		/// <summary>Creates the member entity collections queue.</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		/// <param name="requiredQueue">The required queue.</param>
		protected override void CreateMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue, Queue<bool> requiredQueue) 
		{
			base.CreateMemberEntityCollectionsQueue(collectionsQueue, requiredQueue);
		}
#endif
		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			return toReturn;
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			OnInitClassMembersComplete();
		}


		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BargunId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Covers", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DataControlGroupId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DefaultImageId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EntId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("KdsCategoryId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("KdsCookTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("KdsVideoId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("KdsVideoLabel", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MenuItemAbbr1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MenuItemAbbr2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MenuItemGroupId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MenuItemId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MenuItemName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MiCostAmt", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MiDiscountableFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MiEmpDiscountableFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MiKpLabel", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MiNotActiveFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MiOpenPricePrompt", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MiPriceOverrideFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MiPrintFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MiReceiptLabel", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MiSecId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MiTaxInclFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MiVoidableFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MiWeightFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MiWeightTare", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OpenModifierFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProdClassId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RevCatId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RowVersion", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RptCatId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SkuNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StoreCreatedId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StoreId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TaxGrpId", fieldHashtable);
		}
		#endregion

		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this MenuItemMasterEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END
			

			OnInitialized();

		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static MenuItemMasterRelations Relations
		{
			get	{ return new MenuItemMasterRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The BargunId property of the Entity MenuItemMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Item_Master"."bargun_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): SmallInt, 5, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> BargunId
		{
			get { return (Nullable<System.Int16>)GetValue((int)MenuItemMasterFieldIndex.BargunId, false); }
			set	{ SetValue((int)MenuItemMasterFieldIndex.BargunId, value); }
		}

		/// <summary> The Covers property of the Entity MenuItemMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Item_Master"."covers"<br/>
		/// Table field type characteristics (type, precision, scale, length): SmallInt, 5, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int16 Covers
		{
			get { return (System.Int16)GetValue((int)MenuItemMasterFieldIndex.Covers, true); }
			set	{ SetValue((int)MenuItemMasterFieldIndex.Covers, value); }
		}

		/// <summary> The DataControlGroupId property of the Entity MenuItemMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Item_Master"."data_control_group_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> DataControlGroupId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MenuItemMasterFieldIndex.DataControlGroupId, false); }
			set	{ SetValue((int)MenuItemMasterFieldIndex.DataControlGroupId, value); }
		}

		/// <summary> The DefaultImageId property of the Entity MenuItemMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Item_Master"."default_image_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DefaultImageId
		{
			get { return (System.Int32)GetValue((int)MenuItemMasterFieldIndex.DefaultImageId, true); }
			set	{ SetValue((int)MenuItemMasterFieldIndex.DefaultImageId, value); }
		}

		/// <summary> The EntId property of the Entity MenuItemMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Item_Master"."ent_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int32 EntId
		{
			get { return (System.Int32)GetValue((int)MenuItemMasterFieldIndex.EntId, true); }
			set	{ SetValue((int)MenuItemMasterFieldIndex.EntId, value); }
		}

		/// <summary> The KdsCategoryId property of the Entity MenuItemMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Item_Master"."kds_category_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 KdsCategoryId
		{
			get { return (System.Int32)GetValue((int)MenuItemMasterFieldIndex.KdsCategoryId, true); }
			set	{ SetValue((int)MenuItemMasterFieldIndex.KdsCategoryId, value); }
		}

		/// <summary> The KdsCookTime property of the Entity MenuItemMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Item_Master"."kds_cook_time"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 KdsCookTime
		{
			get { return (System.Int32)GetValue((int)MenuItemMasterFieldIndex.KdsCookTime, true); }
			set	{ SetValue((int)MenuItemMasterFieldIndex.KdsCookTime, value); }
		}

		/// <summary> The KdsVideoId property of the Entity MenuItemMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Item_Master"."kds_video_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 KdsVideoId
		{
			get { return (System.Int32)GetValue((int)MenuItemMasterFieldIndex.KdsVideoId, true); }
			set	{ SetValue((int)MenuItemMasterFieldIndex.KdsVideoId, value); }
		}

		/// <summary> The KdsVideoLabel property of the Entity MenuItemMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Item_Master"."kds_video_label"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String KdsVideoLabel
		{
			get { return (System.String)GetValue((int)MenuItemMasterFieldIndex.KdsVideoLabel, true); }
			set	{ SetValue((int)MenuItemMasterFieldIndex.KdsVideoLabel, value); }
		}

		/// <summary> The MenuItemAbbr1 property of the Entity MenuItemMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Item_Master"."menu_item_abbr1"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String MenuItemAbbr1
		{
			get { return (System.String)GetValue((int)MenuItemMasterFieldIndex.MenuItemAbbr1, true); }
			set	{ SetValue((int)MenuItemMasterFieldIndex.MenuItemAbbr1, value); }
		}

		/// <summary> The MenuItemAbbr2 property of the Entity MenuItemMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Item_Master"."menu_item_abbr2"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String MenuItemAbbr2
		{
			get { return (System.String)GetValue((int)MenuItemMasterFieldIndex.MenuItemAbbr2, true); }
			set	{ SetValue((int)MenuItemMasterFieldIndex.MenuItemAbbr2, value); }
		}

		/// <summary> The MenuItemGroupId property of the Entity MenuItemMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Item_Master"."menu_item_group_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> MenuItemGroupId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MenuItemMasterFieldIndex.MenuItemGroupId, false); }
			set	{ SetValue((int)MenuItemMasterFieldIndex.MenuItemGroupId, value); }
		}

		/// <summary> The MenuItemId property of the Entity MenuItemMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Item_Master"."menu_item_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int32 MenuItemId
		{
			get { return (System.Int32)GetValue((int)MenuItemMasterFieldIndex.MenuItemId, true); }
			set	{ SetValue((int)MenuItemMasterFieldIndex.MenuItemId, value); }
		}

		/// <summary> The MenuItemName property of the Entity MenuItemMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Item_Master"."menu_item_name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String MenuItemName
		{
			get { return (System.String)GetValue((int)MenuItemMasterFieldIndex.MenuItemName, true); }
			set	{ SetValue((int)MenuItemMasterFieldIndex.MenuItemName, value); }
		}

		/// <summary> The MiCostAmt property of the Entity MenuItemMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Item_Master"."mi_cost_amt"<br/>
		/// Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> MiCostAmt
		{
			get { return (Nullable<System.Decimal>)GetValue((int)MenuItemMasterFieldIndex.MiCostAmt, false); }
			set	{ SetValue((int)MenuItemMasterFieldIndex.MiCostAmt, value); }
		}

		/// <summary> The MiDiscountableFlag property of the Entity MenuItemMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Item_Master"."mi_discountable_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean MiDiscountableFlag
		{
			get { return (System.Boolean)GetValue((int)MenuItemMasterFieldIndex.MiDiscountableFlag, true); }
			set	{ SetValue((int)MenuItemMasterFieldIndex.MiDiscountableFlag, value); }
		}

		/// <summary> The MiEmpDiscountableFlag property of the Entity MenuItemMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Item_Master"."mi_emp_discountable_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean MiEmpDiscountableFlag
		{
			get { return (System.Boolean)GetValue((int)MenuItemMasterFieldIndex.MiEmpDiscountableFlag, true); }
			set	{ SetValue((int)MenuItemMasterFieldIndex.MiEmpDiscountableFlag, value); }
		}

		/// <summary> The MiKpLabel property of the Entity MenuItemMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Item_Master"."mi_KP_label"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 16<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String MiKpLabel
		{
			get { return (System.String)GetValue((int)MenuItemMasterFieldIndex.MiKpLabel, true); }
			set	{ SetValue((int)MenuItemMasterFieldIndex.MiKpLabel, value); }
		}

		/// <summary> The MiNotActiveFlag property of the Entity MenuItemMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Item_Master"."mi_not_active_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean MiNotActiveFlag
		{
			get { return (System.Boolean)GetValue((int)MenuItemMasterFieldIndex.MiNotActiveFlag, true); }
			set	{ SetValue((int)MenuItemMasterFieldIndex.MiNotActiveFlag, value); }
		}

		/// <summary> The MiOpenPricePrompt property of the Entity MenuItemMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Item_Master"."mi_open_price_prompt"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean MiOpenPricePrompt
		{
			get { return (System.Boolean)GetValue((int)MenuItemMasterFieldIndex.MiOpenPricePrompt, true); }
			set	{ SetValue((int)MenuItemMasterFieldIndex.MiOpenPricePrompt, value); }
		}

		/// <summary> The MiPriceOverrideFlag property of the Entity MenuItemMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Item_Master"."mi_price_override_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean MiPriceOverrideFlag
		{
			get { return (System.Boolean)GetValue((int)MenuItemMasterFieldIndex.MiPriceOverrideFlag, true); }
			set	{ SetValue((int)MenuItemMasterFieldIndex.MiPriceOverrideFlag, value); }
		}

		/// <summary> The MiPrintFlag property of the Entity MenuItemMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Item_Master"."mi_print_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean MiPrintFlag
		{
			get { return (System.Boolean)GetValue((int)MenuItemMasterFieldIndex.MiPrintFlag, true); }
			set	{ SetValue((int)MenuItemMasterFieldIndex.MiPrintFlag, value); }
		}

		/// <summary> The MiReceiptLabel property of the Entity MenuItemMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Item_Master"."mi_receipt_label"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 16<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String MiReceiptLabel
		{
			get { return (System.String)GetValue((int)MenuItemMasterFieldIndex.MiReceiptLabel, true); }
			set	{ SetValue((int)MenuItemMasterFieldIndex.MiReceiptLabel, value); }
		}

		/// <summary> The MiSecId property of the Entity MenuItemMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Item_Master"."mi_sec_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> MiSecId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MenuItemMasterFieldIndex.MiSecId, false); }
			set	{ SetValue((int)MenuItemMasterFieldIndex.MiSecId, value); }
		}

		/// <summary> The MiTaxInclFlag property of the Entity MenuItemMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Item_Master"."mi_tax_incl_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean MiTaxInclFlag
		{
			get { return (System.Boolean)GetValue((int)MenuItemMasterFieldIndex.MiTaxInclFlag, true); }
			set	{ SetValue((int)MenuItemMasterFieldIndex.MiTaxInclFlag, value); }
		}

		/// <summary> The MiVoidableFlag property of the Entity MenuItemMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Item_Master"."mi_voidable_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean MiVoidableFlag
		{
			get { return (System.Boolean)GetValue((int)MenuItemMasterFieldIndex.MiVoidableFlag, true); }
			set	{ SetValue((int)MenuItemMasterFieldIndex.MiVoidableFlag, value); }
		}

		/// <summary> The MiWeightFlag property of the Entity MenuItemMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Item_Master"."mi_weight_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean MiWeightFlag
		{
			get { return (System.Boolean)GetValue((int)MenuItemMasterFieldIndex.MiWeightFlag, true); }
			set	{ SetValue((int)MenuItemMasterFieldIndex.MiWeightFlag, value); }
		}

		/// <summary> The MiWeightTare property of the Entity MenuItemMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Item_Master"."mi_weight_tare"<br/>
		/// Table field type characteristics (type, precision, scale, length): Float, 38, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Double> MiWeightTare
		{
			get { return (Nullable<System.Double>)GetValue((int)MenuItemMasterFieldIndex.MiWeightTare, false); }
			set	{ SetValue((int)MenuItemMasterFieldIndex.MiWeightTare, value); }
		}

		/// <summary> The OpenModifierFlag property of the Entity MenuItemMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Item_Master"."open_modifier_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean OpenModifierFlag
		{
			get { return (System.Boolean)GetValue((int)MenuItemMasterFieldIndex.OpenModifierFlag, true); }
			set	{ SetValue((int)MenuItemMasterFieldIndex.OpenModifierFlag, value); }
		}

		/// <summary> The ProdClassId property of the Entity MenuItemMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Item_Master"."prod_class_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ProdClassId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MenuItemMasterFieldIndex.ProdClassId, false); }
			set	{ SetValue((int)MenuItemMasterFieldIndex.ProdClassId, value); }
		}

		/// <summary> The RevCatId property of the Entity MenuItemMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Item_Master"."rev_cat_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RevCatId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MenuItemMasterFieldIndex.RevCatId, false); }
			set	{ SetValue((int)MenuItemMasterFieldIndex.RevCatId, value); }
		}

		/// <summary> The RowVersion property of the Entity MenuItemMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Item_Master"."row_version"<br/>
		/// Table field type characteristics (type, precision, scale, length): Timestamp, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Byte[] RowVersion
		{
			get { return (System.Byte[])GetValue((int)MenuItemMasterFieldIndex.RowVersion, true); }

		}

		/// <summary> The RptCatId property of the Entity MenuItemMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Item_Master"."rpt_cat_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RptCatId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MenuItemMasterFieldIndex.RptCatId, false); }
			set	{ SetValue((int)MenuItemMasterFieldIndex.RptCatId, value); }
		}

		/// <summary> The SkuNo property of the Entity MenuItemMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Item_Master"."sku_no"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 30<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String SkuNo
		{
			get { return (System.String)GetValue((int)MenuItemMasterFieldIndex.SkuNo, true); }
			set	{ SetValue((int)MenuItemMasterFieldIndex.SkuNo, value); }
		}

		/// <summary> The StoreCreatedId property of the Entity MenuItemMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Item_Master"."store_created_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> StoreCreatedId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MenuItemMasterFieldIndex.StoreCreatedId, false); }
			set	{ SetValue((int)MenuItemMasterFieldIndex.StoreCreatedId, value); }
		}

		/// <summary> The StoreId property of the Entity MenuItemMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Item_Master"."store_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> StoreId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MenuItemMasterFieldIndex.StoreId, false); }
			set	{ SetValue((int)MenuItemMasterFieldIndex.StoreId, value); }
		}

		/// <summary> The TaxGrpId property of the Entity MenuItemMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Item_Master"."tax_grp_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TaxGrpId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MenuItemMasterFieldIndex.TaxGrpId, false); }
			set	{ SetValue((int)MenuItemMasterFieldIndex.TaxGrpId, value); }
		}
	
		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}
		
		/// <summary>Returns the InfoGenesis.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)InfoGenesis.EntityType.MenuItemMasterEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		
		#endregion

		#region Included code

		#endregion
	}
}

