﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Xml.Serialization;
using InfoGenesis;
using InfoGenesis.HelperClasses;
using InfoGenesis.FactoryClasses;
using InfoGenesis.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace InfoGenesis.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	

	/// <summary>Entity class which represents the entity 'SmuStatusMaster'.<br/><br/></summary>
	[Serializable]
	public partial class SmuStatusMasterEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
			
	{
		#region Class Member Declarations

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
		}
		#endregion
		
		/// <summary> Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static SmuStatusMasterEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary> CTor</summary>
		public SmuStatusMasterEntity():base("SmuStatusMasterEntity")
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <remarks>For framework usage.</remarks>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public SmuStatusMasterEntity(IEntityFields2 fields):base("SmuStatusMasterEntity")
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this SmuStatusMasterEntity</param>
		public SmuStatusMasterEntity(IValidator validator):base("SmuStatusMasterEntity")
		{
			InitClassEmpty(validator, null);
		}
				
		/// <summary> CTor</summary>
		/// <param name="smuStatusId">PK value for SmuStatusMaster which data should be fetched into this SmuStatusMaster object</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public SmuStatusMasterEntity(System.Guid smuStatusId):base("SmuStatusMasterEntity")
		{
			InitClassEmpty(null, null);
			this.SmuStatusId = smuStatusId;
		}

		/// <summary> CTor</summary>
		/// <param name="smuStatusId">PK value for SmuStatusMaster which data should be fetched into this SmuStatusMaster object</param>
		/// <param name="validator">The custom validator object for this SmuStatusMasterEntity</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public SmuStatusMasterEntity(System.Guid smuStatusId, IValidator validator):base("SmuStatusMasterEntity")
		{
			InitClassEmpty(validator, null);
			this.SmuStatusId = smuStatusId;
		}

		/// <summary> Protected CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected SmuStatusMasterEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			if(SerializationHelper.Optimization != SerializationOptimization.Fast) 
			{
				this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance());
			}
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
			
		}


		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}
		
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				default:
					break;				
			}
			return toReturn;
		}
#if !CF
		/// <summary>Checks if the relation mapped by the property with the name specified is a one way / single sided relation. If the passed in name is null, it/ will return true if the entity has any single-sided relation</summary>
		/// <param name="propertyName">Name of the property which is mapped onto the relation to check, or null to check if the entity has any relation/ which is single sided</param>
		/// <returns>true if the relation is single sided / one way (so the opposite relation isn't present), false otherwise</returns>
		protected override bool CheckOneWayRelations(string propertyName)
		{
			int numberOfOneWayRelations = 0;
			switch(propertyName)
			{
				case null:
					return ((numberOfOneWayRelations > 0) || base.CheckOneWayRelations(null));
				default:
					return base.CheckOneWayRelations(propertyName);
			}
		}
#endif
		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				default:
					break;
			}
		}

		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity2 objects, referenced by this entity</returns>
		protected override List<IEntity2> GetDependingRelatedEntities()
		{
			List<IEntity2> toReturn = new List<IEntity2>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These
		/// entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity2 objects, referenced by this entity</returns>
		protected override List<IEntity2> GetDependentRelatedEntities()
		{
			List<IEntity2> toReturn = new List<IEntity2>();
			return toReturn;
		}
		
		/// <summary>Gets a list of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection2 objects, referenced by this entity</returns>
		protected override List<IEntityCollection2> GetMemberEntityCollections()
		{
			List<IEntityCollection2> toReturn = new List<IEntityCollection2>();
			return toReturn;
		}

		/// <summary>ISerializable member. Does custom serialization so event handlers do not get serialized. Serializes members of this entity class and uses the base class' implementation to serialize the rest.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (SerializationHelper.Optimization != SerializationOptimization.Fast) 
			{
			}
			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			base.GetObjectData(info, context);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new SmuStatusMasterRelations().GetAllRelations();
		}
		

		/// <summary>Creates a new instance of the factory related to this entity</summary>
		protected override IEntityFactory2 CreateEntityFactory()
		{
			return EntityFactoryCache2.GetEntityFactory(typeof(SmuStatusMasterEntityFactory));
		}
#if !CF
		/// <summary>Adds the member collections to the collections queue (base first)</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		protected override void AddToMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue) 
		{
			base.AddToMemberEntityCollectionsQueue(collectionsQueue);
		}
		
		/// <summary>Gets the member collections queue from the queue (base first)</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		protected override void GetFromMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue)
		{
			base.GetFromMemberEntityCollectionsQueue(collectionsQueue);

		}
		
		/// <summary>Determines whether the entity has populated member collections</summary>
		/// <returns>true if the entity has populated member collections.</returns>
		protected override bool HasPopulatedMemberEntityCollections()
		{
			bool toReturn = false;
			return toReturn ? true : base.HasPopulatedMemberEntityCollections();
		}
		
		/// <summary>Creates the member entity collections queue.</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		/// <param name="requiredQueue">The required queue.</param>
		protected override void CreateMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue, Queue<bool> requiredQueue) 
		{
			base.CreateMemberEntityCollectionsQueue(collectionsQueue, requiredQueue);
		}
#endif
		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			return toReturn;
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			OnInitClassMembersComplete();
		}


		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ConfigurationLoadUtcTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CurrentClockUtcTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceIpAddress", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceTypeId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DivisionId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Drawer1EmpId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Drawer2EmpId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EntId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsLtsInitSuccessFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsTerminalLockedFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsTerminalOnline", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsTrainingModeFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastCheckNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUpdateUtcDateTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LtsAvailabilityFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LtsServerIpAddress", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MealPeriodId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProfitCenterId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProgramLoadUtcTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ScreenState", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SignonEmpId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SignonJobcodeId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SmuStatusId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StoreId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TlogCheckCount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VersionNumber", fieldHashtable);
		}
		#endregion

		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this SmuStatusMasterEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END
			

			OnInitialized();

		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static SmuStatusMasterRelations Relations
		{
			get	{ return new SmuStatusMasterRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ConfigurationLoadUtcTime property of the Entity SmuStatusMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SMU_Status_Master"."configuration_load_utc_time"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ConfigurationLoadUtcTime
		{
			get { return (Nullable<System.DateTime>)GetValue((int)SmuStatusMasterFieldIndex.ConfigurationLoadUtcTime, false); }
			set	{ SetValue((int)SmuStatusMasterFieldIndex.ConfigurationLoadUtcTime, value); }
		}

		/// <summary> The CurrentClockUtcTime property of the Entity SmuStatusMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SMU_Status_Master"."current_clock_utc_time"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CurrentClockUtcTime
		{
			get { return (Nullable<System.DateTime>)GetValue((int)SmuStatusMasterFieldIndex.CurrentClockUtcTime, false); }
			set	{ SetValue((int)SmuStatusMasterFieldIndex.CurrentClockUtcTime, value); }
		}

		/// <summary> The DeviceId property of the Entity SmuStatusMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SMU_Status_Master"."device_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DeviceId
		{
			get { return (System.Int32)GetValue((int)SmuStatusMasterFieldIndex.DeviceId, true); }
			set	{ SetValue((int)SmuStatusMasterFieldIndex.DeviceId, value); }
		}

		/// <summary> The DeviceIpAddress property of the Entity SmuStatusMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SMU_Status_Master"."device_ip_address"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 40<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DeviceIpAddress
		{
			get { return (System.String)GetValue((int)SmuStatusMasterFieldIndex.DeviceIpAddress, true); }
			set	{ SetValue((int)SmuStatusMasterFieldIndex.DeviceIpAddress, value); }
		}

		/// <summary> The DeviceTypeId property of the Entity SmuStatusMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SMU_Status_Master"."device_type_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DeviceTypeId
		{
			get { return (System.Int32)GetValue((int)SmuStatusMasterFieldIndex.DeviceTypeId, true); }
			set	{ SetValue((int)SmuStatusMasterFieldIndex.DeviceTypeId, value); }
		}

		/// <summary> The DivisionId property of the Entity SmuStatusMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SMU_Status_Master"."division_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DivisionId
		{
			get { return (System.Int32)GetValue((int)SmuStatusMasterFieldIndex.DivisionId, true); }
			set	{ SetValue((int)SmuStatusMasterFieldIndex.DivisionId, value); }
		}

		/// <summary> The Drawer1EmpId property of the Entity SmuStatusMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SMU_Status_Master"."drawer1_emp_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Drawer1EmpId
		{
			get { return (System.Int32)GetValue((int)SmuStatusMasterFieldIndex.Drawer1EmpId, true); }
			set	{ SetValue((int)SmuStatusMasterFieldIndex.Drawer1EmpId, value); }
		}

		/// <summary> The Drawer2EmpId property of the Entity SmuStatusMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SMU_Status_Master"."drawer2_emp_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Drawer2EmpId
		{
			get { return (System.Int32)GetValue((int)SmuStatusMasterFieldIndex.Drawer2EmpId, true); }
			set	{ SetValue((int)SmuStatusMasterFieldIndex.Drawer2EmpId, value); }
		}

		/// <summary> The EntId property of the Entity SmuStatusMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SMU_Status_Master"."ent_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 EntId
		{
			get { return (System.Int32)GetValue((int)SmuStatusMasterFieldIndex.EntId, true); }
			set	{ SetValue((int)SmuStatusMasterFieldIndex.EntId, value); }
		}

		/// <summary> The IsLtsInitSuccessFlag property of the Entity SmuStatusMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SMU_Status_Master"."is_lts_init_success_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> IsLtsInitSuccessFlag
		{
			get { return (Nullable<System.Boolean>)GetValue((int)SmuStatusMasterFieldIndex.IsLtsInitSuccessFlag, false); }
			set	{ SetValue((int)SmuStatusMasterFieldIndex.IsLtsInitSuccessFlag, value); }
		}

		/// <summary> The IsTerminalLockedFlag property of the Entity SmuStatusMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SMU_Status_Master"."is_terminal_locked_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsTerminalLockedFlag
		{
			get { return (System.Boolean)GetValue((int)SmuStatusMasterFieldIndex.IsTerminalLockedFlag, true); }
			set	{ SetValue((int)SmuStatusMasterFieldIndex.IsTerminalLockedFlag, value); }
		}

		/// <summary> The IsTerminalOnline property of the Entity SmuStatusMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SMU_Status_Master"."is_terminal_online"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsTerminalOnline
		{
			get { return (System.Boolean)GetValue((int)SmuStatusMasterFieldIndex.IsTerminalOnline, true); }
			set	{ SetValue((int)SmuStatusMasterFieldIndex.IsTerminalOnline, value); }
		}

		/// <summary> The IsTrainingModeFlag property of the Entity SmuStatusMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SMU_Status_Master"."is_training_mode_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsTrainingModeFlag
		{
			get { return (System.Boolean)GetValue((int)SmuStatusMasterFieldIndex.IsTrainingModeFlag, true); }
			set	{ SetValue((int)SmuStatusMasterFieldIndex.IsTrainingModeFlag, value); }
		}

		/// <summary> The LastCheckNumber property of the Entity SmuStatusMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SMU_Status_Master"."last_check_number"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 LastCheckNumber
		{
			get { return (System.Int32)GetValue((int)SmuStatusMasterFieldIndex.LastCheckNumber, true); }
			set	{ SetValue((int)SmuStatusMasterFieldIndex.LastCheckNumber, value); }
		}

		/// <summary> The LastUpdateUtcDateTime property of the Entity SmuStatusMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SMU_Status_Master"."last_update_utc_date_time"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastUpdateUtcDateTime
		{
			get { return (System.DateTime)GetValue((int)SmuStatusMasterFieldIndex.LastUpdateUtcDateTime, true); }
			set	{ SetValue((int)SmuStatusMasterFieldIndex.LastUpdateUtcDateTime, value); }
		}

		/// <summary> The LtsAvailabilityFlag property of the Entity SmuStatusMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SMU_Status_Master"."lts_availability_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> LtsAvailabilityFlag
		{
			get { return (Nullable<System.Boolean>)GetValue((int)SmuStatusMasterFieldIndex.LtsAvailabilityFlag, false); }
			set	{ SetValue((int)SmuStatusMasterFieldIndex.LtsAvailabilityFlag, value); }
		}

		/// <summary> The LtsServerIpAddress property of the Entity SmuStatusMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SMU_Status_Master"."lts_server_ip_address"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 40<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String LtsServerIpAddress
		{
			get { return (System.String)GetValue((int)SmuStatusMasterFieldIndex.LtsServerIpAddress, true); }
			set	{ SetValue((int)SmuStatusMasterFieldIndex.LtsServerIpAddress, value); }
		}

		/// <summary> The MealPeriodId property of the Entity SmuStatusMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SMU_Status_Master"."meal_period_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MealPeriodId
		{
			get { return (System.Int32)GetValue((int)SmuStatusMasterFieldIndex.MealPeriodId, true); }
			set	{ SetValue((int)SmuStatusMasterFieldIndex.MealPeriodId, value); }
		}

		/// <summary> The ProfitCenterId property of the Entity SmuStatusMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SMU_Status_Master"."profit_center_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ProfitCenterId
		{
			get { return (System.Int32)GetValue((int)SmuStatusMasterFieldIndex.ProfitCenterId, true); }
			set	{ SetValue((int)SmuStatusMasterFieldIndex.ProfitCenterId, value); }
		}

		/// <summary> The ProgramLoadUtcTime property of the Entity SmuStatusMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SMU_Status_Master"."program_load_utc_time"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ProgramLoadUtcTime
		{
			get { return (Nullable<System.DateTime>)GetValue((int)SmuStatusMasterFieldIndex.ProgramLoadUtcTime, false); }
			set	{ SetValue((int)SmuStatusMasterFieldIndex.ProgramLoadUtcTime, value); }
		}

		/// <summary> The ScreenState property of the Entity SmuStatusMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SMU_Status_Master"."screen_state"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ScreenState
		{
			get { return (System.Int32)GetValue((int)SmuStatusMasterFieldIndex.ScreenState, true); }
			set	{ SetValue((int)SmuStatusMasterFieldIndex.ScreenState, value); }
		}

		/// <summary> The SignonEmpId property of the Entity SmuStatusMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SMU_Status_Master"."signon_emp_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SignonEmpId
		{
			get { return (System.Int32)GetValue((int)SmuStatusMasterFieldIndex.SignonEmpId, true); }
			set	{ SetValue((int)SmuStatusMasterFieldIndex.SignonEmpId, value); }
		}

		/// <summary> The SignonJobcodeId property of the Entity SmuStatusMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SMU_Status_Master"."signon_jobcode_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SignonJobcodeId
		{
			get { return (System.Int32)GetValue((int)SmuStatusMasterFieldIndex.SignonJobcodeId, true); }
			set	{ SetValue((int)SmuStatusMasterFieldIndex.SignonJobcodeId, value); }
		}

		/// <summary> The SmuStatusId property of the Entity SmuStatusMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SMU_Status_Master"."smu_status_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): UniqueIdentifier, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Guid SmuStatusId
		{
			get { return (System.Guid)GetValue((int)SmuStatusMasterFieldIndex.SmuStatusId, true); }
			set	{ SetValue((int)SmuStatusMasterFieldIndex.SmuStatusId, value); }
		}

		/// <summary> The StoreId property of the Entity SmuStatusMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SMU_Status_Master"."store_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 StoreId
		{
			get { return (System.Int32)GetValue((int)SmuStatusMasterFieldIndex.StoreId, true); }
			set	{ SetValue((int)SmuStatusMasterFieldIndex.StoreId, value); }
		}

		/// <summary> The TlogCheckCount property of the Entity SmuStatusMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SMU_Status_Master"."tlog_check_count"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 TlogCheckCount
		{
			get { return (System.Int32)GetValue((int)SmuStatusMasterFieldIndex.TlogCheckCount, true); }
			set	{ SetValue((int)SmuStatusMasterFieldIndex.TlogCheckCount, value); }
		}

		/// <summary> The VersionNumber property of the Entity SmuStatusMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SMU_Status_Master"."version_number"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String VersionNumber
		{
			get { return (System.String)GetValue((int)SmuStatusMasterFieldIndex.VersionNumber, true); }
			set	{ SetValue((int)SmuStatusMasterFieldIndex.VersionNumber, value); }
		}
	
		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}
		
		/// <summary>Returns the InfoGenesis.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)InfoGenesis.EntityType.SmuStatusMasterEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		
		#endregion

		#region Included code

		#endregion
	}
}

