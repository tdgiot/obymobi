﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Xml.Serialization;
using InfoGenesis;
using InfoGenesis.HelperClasses;
using InfoGenesis.FactoryClasses;
using InfoGenesis.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace InfoGenesis.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	

	/// <summary>Entity class which represents the entity 'ProfitCenterMaster'.<br/><br/></summary>
	[Serializable]
	public partial class ProfitCenterMasterEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
			
	{
		#region Class Member Declarations

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
		}
		#endregion
		
		/// <summary> Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ProfitCenterMasterEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary> CTor</summary>
		public ProfitCenterMasterEntity():base("ProfitCenterMasterEntity")
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <remarks>For framework usage.</remarks>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public ProfitCenterMasterEntity(IEntityFields2 fields):base("ProfitCenterMasterEntity")
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this ProfitCenterMasterEntity</param>
		public ProfitCenterMasterEntity(IValidator validator):base("ProfitCenterMasterEntity")
		{
			InitClassEmpty(validator, null);
		}
				
		/// <summary> CTor</summary>
		/// <param name="profitCenterId">PK value for ProfitCenterMaster which data should be fetched into this ProfitCenterMaster object</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public ProfitCenterMasterEntity(System.Int32 profitCenterId):base("ProfitCenterMasterEntity")
		{
			InitClassEmpty(null, null);
			this.ProfitCenterId = profitCenterId;
		}

		/// <summary> CTor</summary>
		/// <param name="profitCenterId">PK value for ProfitCenterMaster which data should be fetched into this ProfitCenterMaster object</param>
		/// <param name="validator">The custom validator object for this ProfitCenterMasterEntity</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public ProfitCenterMasterEntity(System.Int32 profitCenterId, IValidator validator):base("ProfitCenterMasterEntity")
		{
			InitClassEmpty(validator, null);
			this.ProfitCenterId = profitCenterId;
		}

		/// <summary> Protected CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected ProfitCenterMasterEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			if(SerializationHelper.Optimization != SerializationOptimization.Fast) 
			{
				this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance());
			}
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
			
		}


		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}
		
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				default:
					break;				
			}
			return toReturn;
		}
#if !CF
		/// <summary>Checks if the relation mapped by the property with the name specified is a one way / single sided relation. If the passed in name is null, it/ will return true if the entity has any single-sided relation</summary>
		/// <param name="propertyName">Name of the property which is mapped onto the relation to check, or null to check if the entity has any relation/ which is single sided</param>
		/// <returns>true if the relation is single sided / one way (so the opposite relation isn't present), false otherwise</returns>
		protected override bool CheckOneWayRelations(string propertyName)
		{
			int numberOfOneWayRelations = 0;
			switch(propertyName)
			{
				case null:
					return ((numberOfOneWayRelations > 0) || base.CheckOneWayRelations(null));
				default:
					return base.CheckOneWayRelations(propertyName);
			}
		}
#endif
		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				default:
					break;
			}
		}

		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity2 objects, referenced by this entity</returns>
		protected override List<IEntity2> GetDependingRelatedEntities()
		{
			List<IEntity2> toReturn = new List<IEntity2>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These
		/// entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity2 objects, referenced by this entity</returns>
		protected override List<IEntity2> GetDependentRelatedEntities()
		{
			List<IEntity2> toReturn = new List<IEntity2>();
			return toReturn;
		}
		
		/// <summary>Gets a list of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection2 objects, referenced by this entity</returns>
		protected override List<IEntityCollection2> GetMemberEntityCollections()
		{
			List<IEntityCollection2> toReturn = new List<IEntityCollection2>();
			return toReturn;
		}

		/// <summary>ISerializable member. Does custom serialization so event handlers do not get serialized. Serializes members of this entity class and uses the base class' implementation to serialize the rest.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (SerializationHelper.Optimization != SerializationOptimization.Fast) 
			{
			}
			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			base.GetObjectData(info, context);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ProfitCenterMasterRelations().GetAllRelations();
		}
		

		/// <summary>Creates a new instance of the factory related to this entity</summary>
		protected override IEntityFactory2 CreateEntityFactory()
		{
			return EntityFactoryCache2.GetEntityFactory(typeof(ProfitCenterMasterEntityFactory));
		}
#if !CF
		/// <summary>Adds the member collections to the collections queue (base first)</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		protected override void AddToMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue) 
		{
			base.AddToMemberEntityCollectionsQueue(collectionsQueue);
		}
		
		/// <summary>Gets the member collections queue from the queue (base first)</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		protected override void GetFromMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue)
		{
			base.GetFromMemberEntityCollectionsQueue(collectionsQueue);

		}
		
		/// <summary>Determines whether the entity has populated member collections</summary>
		/// <returns>true if the entity has populated member collections.</returns>
		protected override bool HasPopulatedMemberEntityCollections()
		{
			bool toReturn = false;
			return toReturn ? true : base.HasPopulatedMemberEntityCollections();
		}
		
		/// <summary>Creates the member entity collections queue.</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		/// <param name="requiredQueue">The required queue.</param>
		protected override void CreateMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue, Queue<bool> requiredQueue) 
		{
			base.CreateMemberEntityCollectionsQueue(collectionsQueue, requiredQueue);
		}
#endif
		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			return toReturn;
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			OnInitClassMembersComplete();
		}


		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BypassCcAgencyThresholdAmount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BypassCcPrintingThresholdAmount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BypassCcVoiceAuthThresholdAmount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ChkFtrLine1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ChkFtrLine2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ChkFtrLine3", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ChkHdrLine1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ChkHdrLine2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ChkHdrLine3", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DataControlGroupId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DefaultTableLayoutId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DocLinesAdvance", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EntId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MaxDocLinesPage", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MerchantId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MinRcptLinesPage", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PoleDisplayClosed", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PoleDisplayOpen", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PrimaryLanguageId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PrintByRevCatFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProfitCenterDesc", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProfitCenterId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProfitCenterName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProfitCtrAbbr1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProfitCtrAbbr2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RowVersion", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SalesTippableFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SecondaryLanguageId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SourcePropertyCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StoreId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TipEnforcementCodeId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TipMaxPercent", fieldHashtable);
		}
		#endregion

		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this ProfitCenterMasterEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END
			

			OnInitialized();

		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ProfitCenterMasterRelations Relations
		{
			get	{ return new ProfitCenterMasterRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The BypassCcAgencyThresholdAmount property of the Entity ProfitCenterMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Profit_Center_Master"."bypass_CC_agency_threshold_amount"<br/>
		/// Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal BypassCcAgencyThresholdAmount
		{
			get { return (System.Decimal)GetValue((int)ProfitCenterMasterFieldIndex.BypassCcAgencyThresholdAmount, true); }
			set	{ SetValue((int)ProfitCenterMasterFieldIndex.BypassCcAgencyThresholdAmount, value); }
		}

		/// <summary> The BypassCcPrintingThresholdAmount property of the Entity ProfitCenterMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Profit_Center_Master"."bypass_CC_printing_threshold_amount"<br/>
		/// Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal BypassCcPrintingThresholdAmount
		{
			get { return (System.Decimal)GetValue((int)ProfitCenterMasterFieldIndex.BypassCcPrintingThresholdAmount, true); }
			set	{ SetValue((int)ProfitCenterMasterFieldIndex.BypassCcPrintingThresholdAmount, value); }
		}

		/// <summary> The BypassCcVoiceAuthThresholdAmount property of the Entity ProfitCenterMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Profit_Center_Master"."bypass_CC_voice_auth_threshold_amount"<br/>
		/// Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal BypassCcVoiceAuthThresholdAmount
		{
			get { return (System.Decimal)GetValue((int)ProfitCenterMasterFieldIndex.BypassCcVoiceAuthThresholdAmount, true); }
			set	{ SetValue((int)ProfitCenterMasterFieldIndex.BypassCcVoiceAuthThresholdAmount, value); }
		}

		/// <summary> The ChkFtrLine1 property of the Entity ProfitCenterMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Profit_Center_Master"."chk_ftr_line1"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 30<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ChkFtrLine1
		{
			get { return (System.String)GetValue((int)ProfitCenterMasterFieldIndex.ChkFtrLine1, true); }
			set	{ SetValue((int)ProfitCenterMasterFieldIndex.ChkFtrLine1, value); }
		}

		/// <summary> The ChkFtrLine2 property of the Entity ProfitCenterMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Profit_Center_Master"."chk_ftr_line2"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 30<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ChkFtrLine2
		{
			get { return (System.String)GetValue((int)ProfitCenterMasterFieldIndex.ChkFtrLine2, true); }
			set	{ SetValue((int)ProfitCenterMasterFieldIndex.ChkFtrLine2, value); }
		}

		/// <summary> The ChkFtrLine3 property of the Entity ProfitCenterMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Profit_Center_Master"."chk_ftr_line3"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 30<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ChkFtrLine3
		{
			get { return (System.String)GetValue((int)ProfitCenterMasterFieldIndex.ChkFtrLine3, true); }
			set	{ SetValue((int)ProfitCenterMasterFieldIndex.ChkFtrLine3, value); }
		}

		/// <summary> The ChkHdrLine1 property of the Entity ProfitCenterMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Profit_Center_Master"."chk_hdr_line1"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 30<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ChkHdrLine1
		{
			get { return (System.String)GetValue((int)ProfitCenterMasterFieldIndex.ChkHdrLine1, true); }
			set	{ SetValue((int)ProfitCenterMasterFieldIndex.ChkHdrLine1, value); }
		}

		/// <summary> The ChkHdrLine2 property of the Entity ProfitCenterMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Profit_Center_Master"."chk_hdr_line2"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 30<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ChkHdrLine2
		{
			get { return (System.String)GetValue((int)ProfitCenterMasterFieldIndex.ChkHdrLine2, true); }
			set	{ SetValue((int)ProfitCenterMasterFieldIndex.ChkHdrLine2, value); }
		}

		/// <summary> The ChkHdrLine3 property of the Entity ProfitCenterMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Profit_Center_Master"."chk_hdr_line3"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 30<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ChkHdrLine3
		{
			get { return (System.String)GetValue((int)ProfitCenterMasterFieldIndex.ChkHdrLine3, true); }
			set	{ SetValue((int)ProfitCenterMasterFieldIndex.ChkHdrLine3, value); }
		}

		/// <summary> The DataControlGroupId property of the Entity ProfitCenterMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Profit_Center_Master"."data_control_group_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> DataControlGroupId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ProfitCenterMasterFieldIndex.DataControlGroupId, false); }
			set	{ SetValue((int)ProfitCenterMasterFieldIndex.DataControlGroupId, value); }
		}

		/// <summary> The DefaultTableLayoutId property of the Entity ProfitCenterMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Profit_Center_Master"."default_table_layout_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> DefaultTableLayoutId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ProfitCenterMasterFieldIndex.DefaultTableLayoutId, false); }
			set	{ SetValue((int)ProfitCenterMasterFieldIndex.DefaultTableLayoutId, value); }
		}

		/// <summary> The DocLinesAdvance property of the Entity ProfitCenterMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Profit_Center_Master"."doc_lines_advance"<br/>
		/// Table field type characteristics (type, precision, scale, length): SmallInt, 5, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> DocLinesAdvance
		{
			get { return (Nullable<System.Int16>)GetValue((int)ProfitCenterMasterFieldIndex.DocLinesAdvance, false); }
			set	{ SetValue((int)ProfitCenterMasterFieldIndex.DocLinesAdvance, value); }
		}

		/// <summary> The EntId property of the Entity ProfitCenterMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Profit_Center_Master"."ent_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> EntId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ProfitCenterMasterFieldIndex.EntId, false); }
			set	{ SetValue((int)ProfitCenterMasterFieldIndex.EntId, value); }
		}

		/// <summary> The MaxDocLinesPage property of the Entity ProfitCenterMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Profit_Center_Master"."max_doc_lines_page"<br/>
		/// Table field type characteristics (type, precision, scale, length): SmallInt, 5, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> MaxDocLinesPage
		{
			get { return (Nullable<System.Int16>)GetValue((int)ProfitCenterMasterFieldIndex.MaxDocLinesPage, false); }
			set	{ SetValue((int)ProfitCenterMasterFieldIndex.MaxDocLinesPage, value); }
		}

		/// <summary> The MerchantId property of the Entity ProfitCenterMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Profit_Center_Master"."merchant_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String MerchantId
		{
			get { return (System.String)GetValue((int)ProfitCenterMasterFieldIndex.MerchantId, true); }
			set	{ SetValue((int)ProfitCenterMasterFieldIndex.MerchantId, value); }
		}

		/// <summary> The MinRcptLinesPage property of the Entity ProfitCenterMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Profit_Center_Master"."min_rcpt_lines_page"<br/>
		/// Table field type characteristics (type, precision, scale, length): SmallInt, 5, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> MinRcptLinesPage
		{
			get { return (Nullable<System.Int16>)GetValue((int)ProfitCenterMasterFieldIndex.MinRcptLinesPage, false); }
			set	{ SetValue((int)ProfitCenterMasterFieldIndex.MinRcptLinesPage, value); }
		}

		/// <summary> The PoleDisplayClosed property of the Entity ProfitCenterMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Profit_Center_Master"."pole_display_closed"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PoleDisplayClosed
		{
			get { return (System.String)GetValue((int)ProfitCenterMasterFieldIndex.PoleDisplayClosed, true); }
			set	{ SetValue((int)ProfitCenterMasterFieldIndex.PoleDisplayClosed, value); }
		}

		/// <summary> The PoleDisplayOpen property of the Entity ProfitCenterMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Profit_Center_Master"."pole_display_open"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PoleDisplayOpen
		{
			get { return (System.String)GetValue((int)ProfitCenterMasterFieldIndex.PoleDisplayOpen, true); }
			set	{ SetValue((int)ProfitCenterMasterFieldIndex.PoleDisplayOpen, value); }
		}

		/// <summary> The PrimaryLanguageId property of the Entity ProfitCenterMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Profit_Center_Master"."primary_language_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 9<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String PrimaryLanguageId
		{
			get { return (System.String)GetValue((int)ProfitCenterMasterFieldIndex.PrimaryLanguageId, true); }
			set	{ SetValue((int)ProfitCenterMasterFieldIndex.PrimaryLanguageId, value); }
		}

		/// <summary> The PrintByRevCatFlag property of the Entity ProfitCenterMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Profit_Center_Master"."print_by_rev_cat_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean PrintByRevCatFlag
		{
			get { return (System.Boolean)GetValue((int)ProfitCenterMasterFieldIndex.PrintByRevCatFlag, true); }
			set	{ SetValue((int)ProfitCenterMasterFieldIndex.PrintByRevCatFlag, value); }
		}

		/// <summary> The ProfitCenterDesc property of the Entity ProfitCenterMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Profit_Center_Master"."profit_center_desc"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ProfitCenterDesc
		{
			get { return (System.String)GetValue((int)ProfitCenterMasterFieldIndex.ProfitCenterDesc, true); }
			set	{ SetValue((int)ProfitCenterMasterFieldIndex.ProfitCenterDesc, value); }
		}

		/// <summary> The ProfitCenterId property of the Entity ProfitCenterMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Profit_Center_Master"."profit_center_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int32 ProfitCenterId
		{
			get { return (System.Int32)GetValue((int)ProfitCenterMasterFieldIndex.ProfitCenterId, true); }
			set	{ SetValue((int)ProfitCenterMasterFieldIndex.ProfitCenterId, value); }
		}

		/// <summary> The ProfitCenterName property of the Entity ProfitCenterMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Profit_Center_Master"."profit_center_name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 30<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ProfitCenterName
		{
			get { return (System.String)GetValue((int)ProfitCenterMasterFieldIndex.ProfitCenterName, true); }
			set	{ SetValue((int)ProfitCenterMasterFieldIndex.ProfitCenterName, value); }
		}

		/// <summary> The ProfitCtrAbbr1 property of the Entity ProfitCenterMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Profit_Center_Master"."profit_ctr_abbr1"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 7<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ProfitCtrAbbr1
		{
			get { return (System.String)GetValue((int)ProfitCenterMasterFieldIndex.ProfitCtrAbbr1, true); }
			set	{ SetValue((int)ProfitCenterMasterFieldIndex.ProfitCtrAbbr1, value); }
		}

		/// <summary> The ProfitCtrAbbr2 property of the Entity ProfitCenterMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Profit_Center_Master"."profit_ctr_abbr2"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 7<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ProfitCtrAbbr2
		{
			get { return (System.String)GetValue((int)ProfitCenterMasterFieldIndex.ProfitCtrAbbr2, true); }
			set	{ SetValue((int)ProfitCenterMasterFieldIndex.ProfitCtrAbbr2, value); }
		}

		/// <summary> The RowVersion property of the Entity ProfitCenterMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Profit_Center_Master"."row_version"<br/>
		/// Table field type characteristics (type, precision, scale, length): Timestamp, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Byte[] RowVersion
		{
			get { return (System.Byte[])GetValue((int)ProfitCenterMasterFieldIndex.RowVersion, true); }

		}

		/// <summary> The SalesTippableFlag property of the Entity ProfitCenterMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Profit_Center_Master"."sales_tippable_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean SalesTippableFlag
		{
			get { return (System.Boolean)GetValue((int)ProfitCenterMasterFieldIndex.SalesTippableFlag, true); }
			set	{ SetValue((int)ProfitCenterMasterFieldIndex.SalesTippableFlag, value); }
		}

		/// <summary> The SecondaryLanguageId property of the Entity ProfitCenterMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Profit_Center_Master"."secondary_language_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 9<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String SecondaryLanguageId
		{
			get { return (System.String)GetValue((int)ProfitCenterMasterFieldIndex.SecondaryLanguageId, true); }
			set	{ SetValue((int)ProfitCenterMasterFieldIndex.SecondaryLanguageId, value); }
		}

		/// <summary> The SourcePropertyCode property of the Entity ProfitCenterMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Profit_Center_Master"."source_property_code"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 6<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String SourcePropertyCode
		{
			get { return (System.String)GetValue((int)ProfitCenterMasterFieldIndex.SourcePropertyCode, true); }
			set	{ SetValue((int)ProfitCenterMasterFieldIndex.SourcePropertyCode, value); }
		}

		/// <summary> The StoreId property of the Entity ProfitCenterMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Profit_Center_Master"."store_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> StoreId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ProfitCenterMasterFieldIndex.StoreId, false); }
			set	{ SetValue((int)ProfitCenterMasterFieldIndex.StoreId, value); }
		}

		/// <summary> The TipEnforcementCodeId property of the Entity ProfitCenterMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Profit_Center_Master"."tip_enforcement_code_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): SmallInt, 5, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int16 TipEnforcementCodeId
		{
			get { return (System.Int16)GetValue((int)ProfitCenterMasterFieldIndex.TipEnforcementCodeId, true); }
			set	{ SetValue((int)ProfitCenterMasterFieldIndex.TipEnforcementCodeId, value); }
		}

		/// <summary> The TipMaxPercent property of the Entity ProfitCenterMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Profit_Center_Master"."tip_max_percent"<br/>
		/// Table field type characteristics (type, precision, scale, length): SmallInt, 5, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> TipMaxPercent
		{
			get { return (Nullable<System.Int16>)GetValue((int)ProfitCenterMasterFieldIndex.TipMaxPercent, false); }
			set	{ SetValue((int)ProfitCenterMasterFieldIndex.TipMaxPercent, value); }
		}
	
		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}
		
		/// <summary>Returns the InfoGenesis.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)InfoGenesis.EntityType.ProfitCenterMasterEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		
		#endregion

		#region Included code

		#endregion
	}
}

