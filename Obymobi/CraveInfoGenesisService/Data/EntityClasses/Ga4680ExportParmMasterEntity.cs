﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Xml.Serialization;
using InfoGenesis;
using InfoGenesis.HelperClasses;
using InfoGenesis.FactoryClasses;
using InfoGenesis.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace InfoGenesis.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	

	/// <summary>Entity class which represents the entity 'Ga4680ExportParmMaster'.<br/><br/></summary>
	[Serializable]
	public partial class Ga4680ExportParmMasterEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
			
	{
		#region Class Member Declarations

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
		}
		#endregion
		
		/// <summary> Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static Ga4680ExportParmMasterEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary> CTor</summary>
		public Ga4680ExportParmMasterEntity():base("Ga4680ExportParmMasterEntity")
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <remarks>For framework usage.</remarks>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public Ga4680ExportParmMasterEntity(IEntityFields2 fields):base("Ga4680ExportParmMasterEntity")
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this Ga4680ExportParmMasterEntity</param>
		public Ga4680ExportParmMasterEntity(IValidator validator):base("Ga4680ExportParmMasterEntity")
		{
			InitClassEmpty(validator, null);
		}
				
		/// <summary> CTor</summary>
		/// <param name="entId">PK value for Ga4680ExportParmMaster which data should be fetched into this Ga4680ExportParmMaster object</param>
		/// <param name="gaExportId">PK value for Ga4680ExportParmMaster which data should be fetched into this Ga4680ExportParmMaster object</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public Ga4680ExportParmMasterEntity(System.Int32 entId, System.Int32 gaExportId):base("Ga4680ExportParmMasterEntity")
		{
			InitClassEmpty(null, null);
			this.EntId = entId;
			this.GaExportId = gaExportId;
		}

		/// <summary> CTor</summary>
		/// <param name="entId">PK value for Ga4680ExportParmMaster which data should be fetched into this Ga4680ExportParmMaster object</param>
		/// <param name="gaExportId">PK value for Ga4680ExportParmMaster which data should be fetched into this Ga4680ExportParmMaster object</param>
		/// <param name="validator">The custom validator object for this Ga4680ExportParmMasterEntity</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public Ga4680ExportParmMasterEntity(System.Int32 entId, System.Int32 gaExportId, IValidator validator):base("Ga4680ExportParmMasterEntity")
		{
			InitClassEmpty(validator, null);
			this.EntId = entId;
			this.GaExportId = gaExportId;
		}

		/// <summary> Protected CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected Ga4680ExportParmMasterEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			if(SerializationHelper.Optimization != SerializationOptimization.Fast) 
			{
				this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance());
			}
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
			
		}


		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}
		
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				default:
					break;				
			}
			return toReturn;
		}
#if !CF
		/// <summary>Checks if the relation mapped by the property with the name specified is a one way / single sided relation. If the passed in name is null, it/ will return true if the entity has any single-sided relation</summary>
		/// <param name="propertyName">Name of the property which is mapped onto the relation to check, or null to check if the entity has any relation/ which is single sided</param>
		/// <returns>true if the relation is single sided / one way (so the opposite relation isn't present), false otherwise</returns>
		protected override bool CheckOneWayRelations(string propertyName)
		{
			int numberOfOneWayRelations = 0;
			switch(propertyName)
			{
				case null:
					return ((numberOfOneWayRelations > 0) || base.CheckOneWayRelations(null));
				default:
					return base.CheckOneWayRelations(propertyName);
			}
		}
#endif
		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				default:
					break;
			}
		}

		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity2 objects, referenced by this entity</returns>
		protected override List<IEntity2> GetDependingRelatedEntities()
		{
			List<IEntity2> toReturn = new List<IEntity2>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These
		/// entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity2 objects, referenced by this entity</returns>
		protected override List<IEntity2> GetDependentRelatedEntities()
		{
			List<IEntity2> toReturn = new List<IEntity2>();
			return toReturn;
		}
		
		/// <summary>Gets a list of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection2 objects, referenced by this entity</returns>
		protected override List<IEntityCollection2> GetMemberEntityCollections()
		{
			List<IEntityCollection2> toReturn = new List<IEntityCollection2>();
			return toReturn;
		}

		/// <summary>ISerializable member. Does custom serialization so event handlers do not get serialized. Serializes members of this entity class and uses the base class' implementation to serialize the rest.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (SerializationHelper.Optimization != SerializationOptimization.Fast) 
			{
			}
			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			base.GetObjectData(info, context);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new Ga4680ExportParmMasterRelations().GetAllRelations();
		}
		

		/// <summary>Creates a new instance of the factory related to this entity</summary>
		protected override IEntityFactory2 CreateEntityFactory()
		{
			return EntityFactoryCache2.GetEntityFactory(typeof(Ga4680ExportParmMasterEntityFactory));
		}
#if !CF
		/// <summary>Adds the member collections to the collections queue (base first)</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		protected override void AddToMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue) 
		{
			base.AddToMemberEntityCollectionsQueue(collectionsQueue);
		}
		
		/// <summary>Gets the member collections queue from the queue (base first)</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		protected override void GetFromMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue)
		{
			base.GetFromMemberEntityCollectionsQueue(collectionsQueue);

		}
		
		/// <summary>Determines whether the entity has populated member collections</summary>
		/// <returns>true if the entity has populated member collections.</returns>
		protected override bool HasPopulatedMemberEntityCollections()
		{
			bool toReturn = false;
			return toReturn ? true : base.HasPopulatedMemberEntityCollections();
		}
		
		/// <summary>Creates the member entity collections queue.</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		/// <param name="requiredQueue">The required queue.</param>
		protected override void CreateMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue, Queue<bool> requiredQueue) 
		{
			base.CreateMemberEntityCollectionsQueue(collectionsQueue, requiredQueue);
		}
#endif
		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			return toReturn;
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			OnInitClassMembersComplete();
		}


		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BusinessDayOffset", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BusinessDayOptionId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DelimiterValue", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EntId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FileFormatId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Ga1ExportPath", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Ga1RequiredFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Ga2ExportPath", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Ga2RequiredFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Ga3ExportPath", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Ga3RequiredFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Ga4ExportPath", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Ga4RequiredFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Ga5ExportPath", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Ga5RequiredFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Ga6ExportPath", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Ga6RequiredFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Ga7ExportPath", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Ga7RequiredFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Ga8ExportPath", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Ga8RequiredFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Ga9ExportPath", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Ga9RequiredFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GaExportId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LocationCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NumDays", fieldHashtable);
		}
		#endregion

		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this Ga4680ExportParmMasterEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END
			

			OnInitialized();

		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static Ga4680ExportParmMasterRelations Relations
		{
			get	{ return new Ga4680ExportParmMasterRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The BusinessDayOffset property of the Entity Ga4680ExportParmMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "GA_4680_Export_Parm_Master"."business_day_offset"<br/>
		/// Table field type characteristics (type, precision, scale, length): SmallInt, 5, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> BusinessDayOffset
		{
			get { return (Nullable<System.Int16>)GetValue((int)Ga4680ExportParmMasterFieldIndex.BusinessDayOffset, false); }
			set	{ SetValue((int)Ga4680ExportParmMasterFieldIndex.BusinessDayOffset, value); }
		}

		/// <summary> The BusinessDayOptionId property of the Entity Ga4680ExportParmMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "GA_4680_Export_Parm_Master"."business_day_option_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): SmallInt, 5, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> BusinessDayOptionId
		{
			get { return (Nullable<System.Int16>)GetValue((int)Ga4680ExportParmMasterFieldIndex.BusinessDayOptionId, false); }
			set	{ SetValue((int)Ga4680ExportParmMasterFieldIndex.BusinessDayOptionId, value); }
		}

		/// <summary> The DelimiterValue property of the Entity Ga4680ExportParmMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "GA_4680_Export_Parm_Master"."delimiter_value"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 3<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DelimiterValue
		{
			get { return (System.String)GetValue((int)Ga4680ExportParmMasterFieldIndex.DelimiterValue, true); }
			set	{ SetValue((int)Ga4680ExportParmMasterFieldIndex.DelimiterValue, value); }
		}

		/// <summary> The EntId property of the Entity Ga4680ExportParmMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "GA_4680_Export_Parm_Master"."ent_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int32 EntId
		{
			get { return (System.Int32)GetValue((int)Ga4680ExportParmMasterFieldIndex.EntId, true); }
			set	{ SetValue((int)Ga4680ExportParmMasterFieldIndex.EntId, value); }
		}

		/// <summary> The FileFormatId property of the Entity Ga4680ExportParmMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "GA_4680_Export_Parm_Master"."file_format_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> FileFormatId
		{
			get { return (Nullable<System.Int32>)GetValue((int)Ga4680ExportParmMasterFieldIndex.FileFormatId, false); }
			set	{ SetValue((int)Ga4680ExportParmMasterFieldIndex.FileFormatId, value); }
		}

		/// <summary> The Ga1ExportPath property of the Entity Ga4680ExportParmMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "GA_4680_Export_Parm_Master"."GA1_export_path"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 120<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Ga1ExportPath
		{
			get { return (System.String)GetValue((int)Ga4680ExportParmMasterFieldIndex.Ga1ExportPath, true); }
			set	{ SetValue((int)Ga4680ExportParmMasterFieldIndex.Ga1ExportPath, value); }
		}

		/// <summary> The Ga1RequiredFlag property of the Entity Ga4680ExportParmMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "GA_4680_Export_Parm_Master"."GA1_required_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Ga1RequiredFlag
		{
			get { return (System.Boolean)GetValue((int)Ga4680ExportParmMasterFieldIndex.Ga1RequiredFlag, true); }
			set	{ SetValue((int)Ga4680ExportParmMasterFieldIndex.Ga1RequiredFlag, value); }
		}

		/// <summary> The Ga2ExportPath property of the Entity Ga4680ExportParmMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "GA_4680_Export_Parm_Master"."GA2_export_path"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 120<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Ga2ExportPath
		{
			get { return (System.String)GetValue((int)Ga4680ExportParmMasterFieldIndex.Ga2ExportPath, true); }
			set	{ SetValue((int)Ga4680ExportParmMasterFieldIndex.Ga2ExportPath, value); }
		}

		/// <summary> The Ga2RequiredFlag property of the Entity Ga4680ExportParmMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "GA_4680_Export_Parm_Master"."GA2_required_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Ga2RequiredFlag
		{
			get { return (System.Boolean)GetValue((int)Ga4680ExportParmMasterFieldIndex.Ga2RequiredFlag, true); }
			set	{ SetValue((int)Ga4680ExportParmMasterFieldIndex.Ga2RequiredFlag, value); }
		}

		/// <summary> The Ga3ExportPath property of the Entity Ga4680ExportParmMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "GA_4680_Export_Parm_Master"."GA3_export_path"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 120<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Ga3ExportPath
		{
			get { return (System.String)GetValue((int)Ga4680ExportParmMasterFieldIndex.Ga3ExportPath, true); }
			set	{ SetValue((int)Ga4680ExportParmMasterFieldIndex.Ga3ExportPath, value); }
		}

		/// <summary> The Ga3RequiredFlag property of the Entity Ga4680ExportParmMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "GA_4680_Export_Parm_Master"."GA3_required_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Ga3RequiredFlag
		{
			get { return (System.Boolean)GetValue((int)Ga4680ExportParmMasterFieldIndex.Ga3RequiredFlag, true); }
			set	{ SetValue((int)Ga4680ExportParmMasterFieldIndex.Ga3RequiredFlag, value); }
		}

		/// <summary> The Ga4ExportPath property of the Entity Ga4680ExportParmMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "GA_4680_Export_Parm_Master"."GA4_export_path"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 120<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Ga4ExportPath
		{
			get { return (System.String)GetValue((int)Ga4680ExportParmMasterFieldIndex.Ga4ExportPath, true); }
			set	{ SetValue((int)Ga4680ExportParmMasterFieldIndex.Ga4ExportPath, value); }
		}

		/// <summary> The Ga4RequiredFlag property of the Entity Ga4680ExportParmMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "GA_4680_Export_Parm_Master"."GA4_required_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Ga4RequiredFlag
		{
			get { return (System.Boolean)GetValue((int)Ga4680ExportParmMasterFieldIndex.Ga4RequiredFlag, true); }
			set	{ SetValue((int)Ga4680ExportParmMasterFieldIndex.Ga4RequiredFlag, value); }
		}

		/// <summary> The Ga5ExportPath property of the Entity Ga4680ExportParmMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "GA_4680_Export_Parm_Master"."GA5_export_path"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 120<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Ga5ExportPath
		{
			get { return (System.String)GetValue((int)Ga4680ExportParmMasterFieldIndex.Ga5ExportPath, true); }
			set	{ SetValue((int)Ga4680ExportParmMasterFieldIndex.Ga5ExportPath, value); }
		}

		/// <summary> The Ga5RequiredFlag property of the Entity Ga4680ExportParmMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "GA_4680_Export_Parm_Master"."GA5_required_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Ga5RequiredFlag
		{
			get { return (System.Boolean)GetValue((int)Ga4680ExportParmMasterFieldIndex.Ga5RequiredFlag, true); }
			set	{ SetValue((int)Ga4680ExportParmMasterFieldIndex.Ga5RequiredFlag, value); }
		}

		/// <summary> The Ga6ExportPath property of the Entity Ga4680ExportParmMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "GA_4680_Export_Parm_Master"."GA6_export_path"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 120<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Ga6ExportPath
		{
			get { return (System.String)GetValue((int)Ga4680ExportParmMasterFieldIndex.Ga6ExportPath, true); }
			set	{ SetValue((int)Ga4680ExportParmMasterFieldIndex.Ga6ExportPath, value); }
		}

		/// <summary> The Ga6RequiredFlag property of the Entity Ga4680ExportParmMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "GA_4680_Export_Parm_Master"."GA6_required_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Ga6RequiredFlag
		{
			get { return (System.Boolean)GetValue((int)Ga4680ExportParmMasterFieldIndex.Ga6RequiredFlag, true); }
			set	{ SetValue((int)Ga4680ExportParmMasterFieldIndex.Ga6RequiredFlag, value); }
		}

		/// <summary> The Ga7ExportPath property of the Entity Ga4680ExportParmMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "GA_4680_Export_Parm_Master"."GA7_export_path"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 120<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Ga7ExportPath
		{
			get { return (System.String)GetValue((int)Ga4680ExportParmMasterFieldIndex.Ga7ExportPath, true); }
			set	{ SetValue((int)Ga4680ExportParmMasterFieldIndex.Ga7ExportPath, value); }
		}

		/// <summary> The Ga7RequiredFlag property of the Entity Ga4680ExportParmMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "GA_4680_Export_Parm_Master"."GA7_required_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Ga7RequiredFlag
		{
			get { return (System.Boolean)GetValue((int)Ga4680ExportParmMasterFieldIndex.Ga7RequiredFlag, true); }
			set	{ SetValue((int)Ga4680ExportParmMasterFieldIndex.Ga7RequiredFlag, value); }
		}

		/// <summary> The Ga8ExportPath property of the Entity Ga4680ExportParmMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "GA_4680_Export_Parm_Master"."GA8_export_path"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 120<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Ga8ExportPath
		{
			get { return (System.String)GetValue((int)Ga4680ExportParmMasterFieldIndex.Ga8ExportPath, true); }
			set	{ SetValue((int)Ga4680ExportParmMasterFieldIndex.Ga8ExportPath, value); }
		}

		/// <summary> The Ga8RequiredFlag property of the Entity Ga4680ExportParmMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "GA_4680_Export_Parm_Master"."GA8_required_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Ga8RequiredFlag
		{
			get { return (System.Boolean)GetValue((int)Ga4680ExportParmMasterFieldIndex.Ga8RequiredFlag, true); }
			set	{ SetValue((int)Ga4680ExportParmMasterFieldIndex.Ga8RequiredFlag, value); }
		}

		/// <summary> The Ga9ExportPath property of the Entity Ga4680ExportParmMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "GA_4680_Export_Parm_Master"."GA9_export_path"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 120<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Ga9ExportPath
		{
			get { return (System.String)GetValue((int)Ga4680ExportParmMasterFieldIndex.Ga9ExportPath, true); }
			set	{ SetValue((int)Ga4680ExportParmMasterFieldIndex.Ga9ExportPath, value); }
		}

		/// <summary> The Ga9RequiredFlag property of the Entity Ga4680ExportParmMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "GA_4680_Export_Parm_Master"."GA9_required_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Ga9RequiredFlag
		{
			get { return (System.Boolean)GetValue((int)Ga4680ExportParmMasterFieldIndex.Ga9RequiredFlag, true); }
			set	{ SetValue((int)Ga4680ExportParmMasterFieldIndex.Ga9RequiredFlag, value); }
		}

		/// <summary> The GaExportId property of the Entity Ga4680ExportParmMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "GA_4680_Export_Parm_Master"."GA_export_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int32 GaExportId
		{
			get { return (System.Int32)GetValue((int)Ga4680ExportParmMasterFieldIndex.GaExportId, true); }
			set	{ SetValue((int)Ga4680ExportParmMasterFieldIndex.GaExportId, value); }
		}

		/// <summary> The LocationCode property of the Entity Ga4680ExportParmMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "GA_4680_Export_Parm_Master"."location_code"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String LocationCode
		{
			get { return (System.String)GetValue((int)Ga4680ExportParmMasterFieldIndex.LocationCode, true); }
			set	{ SetValue((int)Ga4680ExportParmMasterFieldIndex.LocationCode, value); }
		}

		/// <summary> The NumDays property of the Entity Ga4680ExportParmMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "GA_4680_Export_Parm_Master"."num_days"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> NumDays
		{
			get { return (Nullable<System.Int32>)GetValue((int)Ga4680ExportParmMasterFieldIndex.NumDays, false); }
			set	{ SetValue((int)Ga4680ExportParmMasterFieldIndex.NumDays, value); }
		}
	
		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}
		
		/// <summary>Returns the InfoGenesis.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)InfoGenesis.EntityType.Ga4680ExportParmMasterEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		
		#endregion

		#region Included code

		#endregion
	}
}

