﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Xml.Serialization;
using InfoGenesis;
using InfoGenesis.HelperClasses;
using InfoGenesis.FactoryClasses;
using InfoGenesis.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace InfoGenesis.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	

	/// <summary>Entity class which represents the entity 'DiscoupMaster'.<br/><br/></summary>
	[Serializable]
	public partial class DiscoupMasterEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
			
	{
		#region Class Member Declarations

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
		}
		#endregion
		
		/// <summary> Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static DiscoupMasterEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary> CTor</summary>
		public DiscoupMasterEntity():base("DiscoupMasterEntity")
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <remarks>For framework usage.</remarks>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public DiscoupMasterEntity(IEntityFields2 fields):base("DiscoupMasterEntity")
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this DiscoupMasterEntity</param>
		public DiscoupMasterEntity(IValidator validator):base("DiscoupMasterEntity")
		{
			InitClassEmpty(validator, null);
		}
				
		/// <summary> CTor</summary>
		/// <param name="discoupId">PK value for DiscoupMaster which data should be fetched into this DiscoupMaster object</param>
		/// <param name="entId">PK value for DiscoupMaster which data should be fetched into this DiscoupMaster object</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public DiscoupMasterEntity(System.Int32 discoupId, System.Int32 entId):base("DiscoupMasterEntity")
		{
			InitClassEmpty(null, null);
			this.DiscoupId = discoupId;
			this.EntId = entId;
		}

		/// <summary> CTor</summary>
		/// <param name="discoupId">PK value for DiscoupMaster which data should be fetched into this DiscoupMaster object</param>
		/// <param name="entId">PK value for DiscoupMaster which data should be fetched into this DiscoupMaster object</param>
		/// <param name="validator">The custom validator object for this DiscoupMasterEntity</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public DiscoupMasterEntity(System.Int32 discoupId, System.Int32 entId, IValidator validator):base("DiscoupMasterEntity")
		{
			InitClassEmpty(validator, null);
			this.DiscoupId = discoupId;
			this.EntId = entId;
		}

		/// <summary> Protected CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected DiscoupMasterEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			if(SerializationHelper.Optimization != SerializationOptimization.Fast) 
			{
				this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance());
			}
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
			
		}


		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}
		
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				default:
					break;				
			}
			return toReturn;
		}
#if !CF
		/// <summary>Checks if the relation mapped by the property with the name specified is a one way / single sided relation. If the passed in name is null, it/ will return true if the entity has any single-sided relation</summary>
		/// <param name="propertyName">Name of the property which is mapped onto the relation to check, or null to check if the entity has any relation/ which is single sided</param>
		/// <returns>true if the relation is single sided / one way (so the opposite relation isn't present), false otherwise</returns>
		protected override bool CheckOneWayRelations(string propertyName)
		{
			int numberOfOneWayRelations = 0;
			switch(propertyName)
			{
				case null:
					return ((numberOfOneWayRelations > 0) || base.CheckOneWayRelations(null));
				default:
					return base.CheckOneWayRelations(propertyName);
			}
		}
#endif
		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				default:
					break;
			}
		}

		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity2 objects, referenced by this entity</returns>
		protected override List<IEntity2> GetDependingRelatedEntities()
		{
			List<IEntity2> toReturn = new List<IEntity2>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These
		/// entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity2 objects, referenced by this entity</returns>
		protected override List<IEntity2> GetDependentRelatedEntities()
		{
			List<IEntity2> toReturn = new List<IEntity2>();
			return toReturn;
		}
		
		/// <summary>Gets a list of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection2 objects, referenced by this entity</returns>
		protected override List<IEntityCollection2> GetMemberEntityCollections()
		{
			List<IEntityCollection2> toReturn = new List<IEntityCollection2>();
			return toReturn;
		}

		/// <summary>ISerializable member. Does custom serialization so event handlers do not get serialized. Serializes members of this entity class and uses the base class' implementation to serialize the rest.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (SerializationHelper.Optimization != SerializationOptimization.Fast) 
			{
			}
			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			base.GetObjectData(info, context);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new DiscoupMasterRelations().GetAllRelations();
		}
		

		/// <summary>Creates a new instance of the factory related to this entity</summary>
		protected override IEntityFactory2 CreateEntityFactory()
		{
			return EntityFactoryCache2.GetEntityFactory(typeof(DiscoupMasterEntityFactory));
		}
#if !CF
		/// <summary>Adds the member collections to the collections queue (base first)</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		protected override void AddToMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue) 
		{
			base.AddToMemberEntityCollectionsQueue(collectionsQueue);
		}
		
		/// <summary>Gets the member collections queue from the queue (base first)</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		protected override void GetFromMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue)
		{
			base.GetFromMemberEntityCollectionsQueue(collectionsQueue);

		}
		
		/// <summary>Determines whether the entity has populated member collections</summary>
		/// <returns>true if the entity has populated member collections.</returns>
		protected override bool HasPopulatedMemberEntityCollections()
		{
			bool toReturn = false;
			return toReturn ? true : base.HasPopulatedMemberEntityCollections();
		}
		
		/// <summary>Creates the member entity collections queue.</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		/// <param name="requiredQueue">The required queue.</param>
		protected override void CreateMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue, Queue<bool> requiredQueue) 
		{
			base.CreateMemberEntityCollectionsQueue(collectionsQueue, requiredQueue);
		}
#endif
		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			return toReturn;
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			OnInitClassMembersComplete();
		}


		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AssocTenderId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BevRevClassFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DiscountExtraPromptCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DiscoupAbbr1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DiscoupAbbr2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DiscoupAmt", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DiscoupId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DiscoupItemLevelCodeId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DiscoupMaxAmt", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DiscoupMaxPercent", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DiscoupName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DiscoupOpenCodeId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DiscoupPctAmtCodeId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DiscoupPercent", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DiscoupTypeCodeId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EntId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExclusiveFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FoodRevClassFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OtherRevClassFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PostAcctNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProfitCtrGrpId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PromptExtraDataFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoundBasis", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoundTypeId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RowVersion", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SecurityId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SodaRevClassFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StoreId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ThreshholdAmt", fieldHashtable);
		}
		#endregion

		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this DiscoupMasterEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END
			

			OnInitialized();

		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static DiscoupMasterRelations Relations
		{
			get	{ return new DiscoupMasterRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AssocTenderId property of the Entity DiscoupMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Discoup_Master"."assoc_tender_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> AssocTenderId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DiscoupMasterFieldIndex.AssocTenderId, false); }
			set	{ SetValue((int)DiscoupMasterFieldIndex.AssocTenderId, value); }
		}

		/// <summary> The BevRevClassFlag property of the Entity DiscoupMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Discoup_Master"."bev_rev_class_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean BevRevClassFlag
		{
			get { return (System.Boolean)GetValue((int)DiscoupMasterFieldIndex.BevRevClassFlag, true); }
			set	{ SetValue((int)DiscoupMasterFieldIndex.BevRevClassFlag, value); }
		}

		/// <summary> The DiscountExtraPromptCode property of the Entity DiscoupMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Discoup_Master"."discount_extra_prompt_code"<br/>
		/// Table field type characteristics (type, precision, scale, length): TinyInt, 3, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Byte DiscountExtraPromptCode
		{
			get { return (System.Byte)GetValue((int)DiscoupMasterFieldIndex.DiscountExtraPromptCode, true); }
			set	{ SetValue((int)DiscoupMasterFieldIndex.DiscountExtraPromptCode, value); }
		}

		/// <summary> The DiscoupAbbr1 property of the Entity DiscoupMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Discoup_Master"."discoup_abbr1"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 7<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DiscoupAbbr1
		{
			get { return (System.String)GetValue((int)DiscoupMasterFieldIndex.DiscoupAbbr1, true); }
			set	{ SetValue((int)DiscoupMasterFieldIndex.DiscoupAbbr1, value); }
		}

		/// <summary> The DiscoupAbbr2 property of the Entity DiscoupMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Discoup_Master"."discoup_abbr2"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 7<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DiscoupAbbr2
		{
			get { return (System.String)GetValue((int)DiscoupMasterFieldIndex.DiscoupAbbr2, true); }
			set	{ SetValue((int)DiscoupMasterFieldIndex.DiscoupAbbr2, value); }
		}

		/// <summary> The DiscoupAmt property of the Entity DiscoupMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Discoup_Master"."discoup_amt"<br/>
		/// Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> DiscoupAmt
		{
			get { return (Nullable<System.Decimal>)GetValue((int)DiscoupMasterFieldIndex.DiscoupAmt, false); }
			set	{ SetValue((int)DiscoupMasterFieldIndex.DiscoupAmt, value); }
		}

		/// <summary> The DiscoupId property of the Entity DiscoupMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Discoup_Master"."discoup_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int32 DiscoupId
		{
			get { return (System.Int32)GetValue((int)DiscoupMasterFieldIndex.DiscoupId, true); }
			set	{ SetValue((int)DiscoupMasterFieldIndex.DiscoupId, value); }
		}

		/// <summary> The DiscoupItemLevelCodeId property of the Entity DiscoupMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Discoup_Master"."discoup_item_level_code_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): SmallInt, 5, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> DiscoupItemLevelCodeId
		{
			get { return (Nullable<System.Int16>)GetValue((int)DiscoupMasterFieldIndex.DiscoupItemLevelCodeId, false); }
			set	{ SetValue((int)DiscoupMasterFieldIndex.DiscoupItemLevelCodeId, value); }
		}

		/// <summary> The DiscoupMaxAmt property of the Entity DiscoupMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Discoup_Master"."discoup_max_amt"<br/>
		/// Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> DiscoupMaxAmt
		{
			get { return (Nullable<System.Decimal>)GetValue((int)DiscoupMasterFieldIndex.DiscoupMaxAmt, false); }
			set	{ SetValue((int)DiscoupMasterFieldIndex.DiscoupMaxAmt, value); }
		}

		/// <summary> The DiscoupMaxPercent property of the Entity DiscoupMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Discoup_Master"."discoup_max_percent"<br/>
		/// Table field type characteristics (type, precision, scale, length): Float, 38, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Double> DiscoupMaxPercent
		{
			get { return (Nullable<System.Double>)GetValue((int)DiscoupMasterFieldIndex.DiscoupMaxPercent, false); }
			set	{ SetValue((int)DiscoupMasterFieldIndex.DiscoupMaxPercent, value); }
		}

		/// <summary> The DiscoupName property of the Entity DiscoupMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Discoup_Master"."discoup_name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 16<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DiscoupName
		{
			get { return (System.String)GetValue((int)DiscoupMasterFieldIndex.DiscoupName, true); }
			set	{ SetValue((int)DiscoupMasterFieldIndex.DiscoupName, value); }
		}

		/// <summary> The DiscoupOpenCodeId property of the Entity DiscoupMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Discoup_Master"."discoup_open_code_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): SmallInt, 5, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> DiscoupOpenCodeId
		{
			get { return (Nullable<System.Int16>)GetValue((int)DiscoupMasterFieldIndex.DiscoupOpenCodeId, false); }
			set	{ SetValue((int)DiscoupMasterFieldIndex.DiscoupOpenCodeId, value); }
		}

		/// <summary> The DiscoupPctAmtCodeId property of the Entity DiscoupMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Discoup_Master"."discoup_pct_amt_code_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): SmallInt, 5, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> DiscoupPctAmtCodeId
		{
			get { return (Nullable<System.Int16>)GetValue((int)DiscoupMasterFieldIndex.DiscoupPctAmtCodeId, false); }
			set	{ SetValue((int)DiscoupMasterFieldIndex.DiscoupPctAmtCodeId, value); }
		}

		/// <summary> The DiscoupPercent property of the Entity DiscoupMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Discoup_Master"."discoup_percent"<br/>
		/// Table field type characteristics (type, precision, scale, length): Float, 38, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Double> DiscoupPercent
		{
			get { return (Nullable<System.Double>)GetValue((int)DiscoupMasterFieldIndex.DiscoupPercent, false); }
			set	{ SetValue((int)DiscoupMasterFieldIndex.DiscoupPercent, value); }
		}

		/// <summary> The DiscoupTypeCodeId property of the Entity DiscoupMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Discoup_Master"."discoup_type_code_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): SmallInt, 5, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> DiscoupTypeCodeId
		{
			get { return (Nullable<System.Int16>)GetValue((int)DiscoupMasterFieldIndex.DiscoupTypeCodeId, false); }
			set	{ SetValue((int)DiscoupMasterFieldIndex.DiscoupTypeCodeId, value); }
		}

		/// <summary> The EntId property of the Entity DiscoupMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Discoup_Master"."ent_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int32 EntId
		{
			get { return (System.Int32)GetValue((int)DiscoupMasterFieldIndex.EntId, true); }
			set	{ SetValue((int)DiscoupMasterFieldIndex.EntId, value); }
		}

		/// <summary> The ExclusiveFlag property of the Entity DiscoupMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Discoup_Master"."exclusive_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ExclusiveFlag
		{
			get { return (System.Boolean)GetValue((int)DiscoupMasterFieldIndex.ExclusiveFlag, true); }
			set	{ SetValue((int)DiscoupMasterFieldIndex.ExclusiveFlag, value); }
		}

		/// <summary> The FoodRevClassFlag property of the Entity DiscoupMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Discoup_Master"."food_rev_class_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean FoodRevClassFlag
		{
			get { return (System.Boolean)GetValue((int)DiscoupMasterFieldIndex.FoodRevClassFlag, true); }
			set	{ SetValue((int)DiscoupMasterFieldIndex.FoodRevClassFlag, value); }
		}

		/// <summary> The OtherRevClassFlag property of the Entity DiscoupMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Discoup_Master"."other_rev_class_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean OtherRevClassFlag
		{
			get { return (System.Boolean)GetValue((int)DiscoupMasterFieldIndex.OtherRevClassFlag, true); }
			set	{ SetValue((int)DiscoupMasterFieldIndex.OtherRevClassFlag, value); }
		}

		/// <summary> The PostAcctNo property of the Entity DiscoupMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Discoup_Master"."post_acct_no"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 30<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PostAcctNo
		{
			get { return (System.String)GetValue((int)DiscoupMasterFieldIndex.PostAcctNo, true); }
			set	{ SetValue((int)DiscoupMasterFieldIndex.PostAcctNo, value); }
		}

		/// <summary> The ProfitCtrGrpId property of the Entity DiscoupMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Discoup_Master"."profit_ctr_grp_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ProfitCtrGrpId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DiscoupMasterFieldIndex.ProfitCtrGrpId, false); }
			set	{ SetValue((int)DiscoupMasterFieldIndex.ProfitCtrGrpId, value); }
		}

		/// <summary> The PromptExtraDataFlag property of the Entity DiscoupMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Discoup_Master"."prompt_extra_data_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean PromptExtraDataFlag
		{
			get { return (System.Boolean)GetValue((int)DiscoupMasterFieldIndex.PromptExtraDataFlag, true); }
			set	{ SetValue((int)DiscoupMasterFieldIndex.PromptExtraDataFlag, value); }
		}

		/// <summary> The RoundBasis property of the Entity DiscoupMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Discoup_Master"."round_basis"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RoundBasis
		{
			get { return (Nullable<System.Int32>)GetValue((int)DiscoupMasterFieldIndex.RoundBasis, false); }
			set	{ SetValue((int)DiscoupMasterFieldIndex.RoundBasis, value); }
		}

		/// <summary> The RoundTypeId property of the Entity DiscoupMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Discoup_Master"."round_type_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): SmallInt, 5, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> RoundTypeId
		{
			get { return (Nullable<System.Int16>)GetValue((int)DiscoupMasterFieldIndex.RoundTypeId, false); }
			set	{ SetValue((int)DiscoupMasterFieldIndex.RoundTypeId, value); }
		}

		/// <summary> The RowVersion property of the Entity DiscoupMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Discoup_Master"."row_version"<br/>
		/// Table field type characteristics (type, precision, scale, length): Timestamp, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Byte[] RowVersion
		{
			get { return (System.Byte[])GetValue((int)DiscoupMasterFieldIndex.RowVersion, true); }

		}

		/// <summary> The SecurityId property of the Entity DiscoupMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Discoup_Master"."security_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SecurityId
		{
			get { return (System.Int32)GetValue((int)DiscoupMasterFieldIndex.SecurityId, true); }
			set	{ SetValue((int)DiscoupMasterFieldIndex.SecurityId, value); }
		}

		/// <summary> The SodaRevClassFlag property of the Entity DiscoupMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Discoup_Master"."soda_rev_class_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean SodaRevClassFlag
		{
			get { return (System.Boolean)GetValue((int)DiscoupMasterFieldIndex.SodaRevClassFlag, true); }
			set	{ SetValue((int)DiscoupMasterFieldIndex.SodaRevClassFlag, value); }
		}

		/// <summary> The StoreId property of the Entity DiscoupMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Discoup_Master"."store_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> StoreId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DiscoupMasterFieldIndex.StoreId, false); }
			set	{ SetValue((int)DiscoupMasterFieldIndex.StoreId, value); }
		}

		/// <summary> The ThreshholdAmt property of the Entity DiscoupMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Discoup_Master"."threshhold_amt"<br/>
		/// Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> ThreshholdAmt
		{
			get { return (Nullable<System.Decimal>)GetValue((int)DiscoupMasterFieldIndex.ThreshholdAmt, false); }
			set	{ SetValue((int)DiscoupMasterFieldIndex.ThreshholdAmt, value); }
		}
	
		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}
		
		/// <summary>Returns the InfoGenesis.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)InfoGenesis.EntityType.DiscoupMasterEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		
		#endregion

		#region Included code

		#endregion
	}
}

