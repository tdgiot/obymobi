﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Xml.Serialization;
using InfoGenesis;
using InfoGenesis.HelperClasses;
using InfoGenesis.FactoryClasses;
using InfoGenesis.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace InfoGenesis.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	

	/// <summary>Entity class which represents the entity 'JobcodeMaster'.<br/><br/></summary>
	[Serializable]
	public partial class JobcodeMasterEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
			
	{
		#region Class Member Declarations

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
		}
		#endregion
		
		/// <summary> Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static JobcodeMasterEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary> CTor</summary>
		public JobcodeMasterEntity():base("JobcodeMasterEntity")
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <remarks>For framework usage.</remarks>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public JobcodeMasterEntity(IEntityFields2 fields):base("JobcodeMasterEntity")
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this JobcodeMasterEntity</param>
		public JobcodeMasterEntity(IValidator validator):base("JobcodeMasterEntity")
		{
			InitClassEmpty(validator, null);
		}
				
		/// <summary> CTor</summary>
		/// <param name="entId">PK value for JobcodeMaster which data should be fetched into this JobcodeMaster object</param>
		/// <param name="jobcodeId">PK value for JobcodeMaster which data should be fetched into this JobcodeMaster object</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public JobcodeMasterEntity(System.Int32 entId, System.Int32 jobcodeId):base("JobcodeMasterEntity")
		{
			InitClassEmpty(null, null);
			this.EntId = entId;
			this.JobcodeId = jobcodeId;
		}

		/// <summary> CTor</summary>
		/// <param name="entId">PK value for JobcodeMaster which data should be fetched into this JobcodeMaster object</param>
		/// <param name="jobcodeId">PK value for JobcodeMaster which data should be fetched into this JobcodeMaster object</param>
		/// <param name="validator">The custom validator object for this JobcodeMasterEntity</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public JobcodeMasterEntity(System.Int32 entId, System.Int32 jobcodeId, IValidator validator):base("JobcodeMasterEntity")
		{
			InitClassEmpty(validator, null);
			this.EntId = entId;
			this.JobcodeId = jobcodeId;
		}

		/// <summary> Protected CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected JobcodeMasterEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			if(SerializationHelper.Optimization != SerializationOptimization.Fast) 
			{
				this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance());
			}
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
			
		}


		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}
		
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				default:
					break;				
			}
			return toReturn;
		}
#if !CF
		/// <summary>Checks if the relation mapped by the property with the name specified is a one way / single sided relation. If the passed in name is null, it/ will return true if the entity has any single-sided relation</summary>
		/// <param name="propertyName">Name of the property which is mapped onto the relation to check, or null to check if the entity has any relation/ which is single sided</param>
		/// <returns>true if the relation is single sided / one way (so the opposite relation isn't present), false otherwise</returns>
		protected override bool CheckOneWayRelations(string propertyName)
		{
			int numberOfOneWayRelations = 0;
			switch(propertyName)
			{
				case null:
					return ((numberOfOneWayRelations > 0) || base.CheckOneWayRelations(null));
				default:
					return base.CheckOneWayRelations(propertyName);
			}
		}
#endif
		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				default:
					break;
			}
		}

		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity2 objects, referenced by this entity</returns>
		protected override List<IEntity2> GetDependingRelatedEntities()
		{
			List<IEntity2> toReturn = new List<IEntity2>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These
		/// entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity2 objects, referenced by this entity</returns>
		protected override List<IEntity2> GetDependentRelatedEntities()
		{
			List<IEntity2> toReturn = new List<IEntity2>();
			return toReturn;
		}
		
		/// <summary>Gets a list of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection2 objects, referenced by this entity</returns>
		protected override List<IEntityCollection2> GetMemberEntityCollections()
		{
			List<IEntityCollection2> toReturn = new List<IEntityCollection2>();
			return toReturn;
		}

		/// <summary>ISerializable member. Does custom serialization so event handlers do not get serialized. Serializes members of this entity class and uses the base class' implementation to serialize the rest.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (SerializationHelper.Optimization != SerializationOptimization.Fast) 
			{
			}
			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			base.GetObjectData(info, context);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new JobcodeMasterRelations().GetAllRelations();
		}
		

		/// <summary>Creates a new instance of the factory related to this entity</summary>
		protected override IEntityFactory2 CreateEntityFactory()
		{
			return EntityFactoryCache2.GetEntityFactory(typeof(JobcodeMasterEntityFactory));
		}
#if !CF
		/// <summary>Adds the member collections to the collections queue (base first)</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		protected override void AddToMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue) 
		{
			base.AddToMemberEntityCollectionsQueue(collectionsQueue);
		}
		
		/// <summary>Gets the member collections queue from the queue (base first)</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		protected override void GetFromMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue)
		{
			base.GetFromMemberEntityCollectionsQueue(collectionsQueue);

		}
		
		/// <summary>Determines whether the entity has populated member collections</summary>
		/// <returns>true if the entity has populated member collections.</returns>
		protected override bool HasPopulatedMemberEntityCollections()
		{
			bool toReturn = false;
			return toReturn ? true : base.HasPopulatedMemberEntityCollections();
		}
		
		/// <summary>Creates the member entity collections queue.</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		/// <param name="requiredQueue">The required queue.</param>
		protected override void CreateMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue, Queue<bool> requiredQueue) 
		{
			base.CreateMemberEntityCollectionsQueue(collectionsQueue, requiredQueue);
		}
#endif
		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			return toReturn;
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			OnInitClassMembersComplete();
		}


		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EntId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("JobcodeAbbr", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("JobcodeAbbr2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("JobcodeCarryoverTipsFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("JobcodeId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("JobcodeLaborRate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("JobcodeName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("JobcodePrintCashFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("JobcodePrintCashierFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("JobcodePrintEmpIdFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("JobcodePrintServerFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("JobcodeSecId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("JobcodeTipPrompt", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RowVersion", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StoreId", fieldHashtable);
		}
		#endregion

		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this JobcodeMasterEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END
			

			OnInitialized();

		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static JobcodeMasterRelations Relations
		{
			get	{ return new JobcodeMasterRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The EntId property of the Entity JobcodeMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Jobcode_Master"."ent_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int32 EntId
		{
			get { return (System.Int32)GetValue((int)JobcodeMasterFieldIndex.EntId, true); }
			set	{ SetValue((int)JobcodeMasterFieldIndex.EntId, value); }
		}

		/// <summary> The JobcodeAbbr property of the Entity JobcodeMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Jobcode_Master"."jobcode_abbr"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 7<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String JobcodeAbbr
		{
			get { return (System.String)GetValue((int)JobcodeMasterFieldIndex.JobcodeAbbr, true); }
			set	{ SetValue((int)JobcodeMasterFieldIndex.JobcodeAbbr, value); }
		}

		/// <summary> The JobcodeAbbr2 property of the Entity JobcodeMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Jobcode_Master"."jobcode_abbr2"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 7<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String JobcodeAbbr2
		{
			get { return (System.String)GetValue((int)JobcodeMasterFieldIndex.JobcodeAbbr2, true); }
			set	{ SetValue((int)JobcodeMasterFieldIndex.JobcodeAbbr2, value); }
		}

		/// <summary> The JobcodeCarryoverTipsFlag property of the Entity JobcodeMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Jobcode_Master"."jobcode_carryover_tips_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean JobcodeCarryoverTipsFlag
		{
			get { return (System.Boolean)GetValue((int)JobcodeMasterFieldIndex.JobcodeCarryoverTipsFlag, true); }
			set	{ SetValue((int)JobcodeMasterFieldIndex.JobcodeCarryoverTipsFlag, value); }
		}

		/// <summary> The JobcodeId property of the Entity JobcodeMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Jobcode_Master"."jobcode_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int32 JobcodeId
		{
			get { return (System.Int32)GetValue((int)JobcodeMasterFieldIndex.JobcodeId, true); }
			set	{ SetValue((int)JobcodeMasterFieldIndex.JobcodeId, value); }
		}

		/// <summary> The JobcodeLaborRate property of the Entity JobcodeMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Jobcode_Master"."jobcode_labor_rate"<br/>
		/// Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> JobcodeLaborRate
		{
			get { return (Nullable<System.Decimal>)GetValue((int)JobcodeMasterFieldIndex.JobcodeLaborRate, false); }
			set	{ SetValue((int)JobcodeMasterFieldIndex.JobcodeLaborRate, value); }
		}

		/// <summary> The JobcodeName property of the Entity JobcodeMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Jobcode_Master"."jobcode_name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 16<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String JobcodeName
		{
			get { return (System.String)GetValue((int)JobcodeMasterFieldIndex.JobcodeName, true); }
			set	{ SetValue((int)JobcodeMasterFieldIndex.JobcodeName, value); }
		}

		/// <summary> The JobcodePrintCashFlag property of the Entity JobcodeMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Jobcode_Master"."jobcode_print_cash_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean JobcodePrintCashFlag
		{
			get { return (System.Boolean)GetValue((int)JobcodeMasterFieldIndex.JobcodePrintCashFlag, true); }
			set	{ SetValue((int)JobcodeMasterFieldIndex.JobcodePrintCashFlag, value); }
		}

		/// <summary> The JobcodePrintCashierFlag property of the Entity JobcodeMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Jobcode_Master"."jobcode_print_cashier_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean JobcodePrintCashierFlag
		{
			get { return (System.Boolean)GetValue((int)JobcodeMasterFieldIndex.JobcodePrintCashierFlag, true); }
			set	{ SetValue((int)JobcodeMasterFieldIndex.JobcodePrintCashierFlag, value); }
		}

		/// <summary> The JobcodePrintEmpIdFlag property of the Entity JobcodeMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Jobcode_Master"."jobcode_print_emp_id_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean JobcodePrintEmpIdFlag
		{
			get { return (System.Boolean)GetValue((int)JobcodeMasterFieldIndex.JobcodePrintEmpIdFlag, true); }
			set	{ SetValue((int)JobcodeMasterFieldIndex.JobcodePrintEmpIdFlag, value); }
		}

		/// <summary> The JobcodePrintServerFlag property of the Entity JobcodeMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Jobcode_Master"."jobcode_print_server_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean JobcodePrintServerFlag
		{
			get { return (System.Boolean)GetValue((int)JobcodeMasterFieldIndex.JobcodePrintServerFlag, true); }
			set	{ SetValue((int)JobcodeMasterFieldIndex.JobcodePrintServerFlag, value); }
		}

		/// <summary> The JobcodeSecId property of the Entity JobcodeMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Jobcode_Master"."jobcode_sec_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> JobcodeSecId
		{
			get { return (Nullable<System.Int32>)GetValue((int)JobcodeMasterFieldIndex.JobcodeSecId, false); }
			set	{ SetValue((int)JobcodeMasterFieldIndex.JobcodeSecId, value); }
		}

		/// <summary> The JobcodeTipPrompt property of the Entity JobcodeMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Jobcode_Master"."jobcode_tip_prompt"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean JobcodeTipPrompt
		{
			get { return (System.Boolean)GetValue((int)JobcodeMasterFieldIndex.JobcodeTipPrompt, true); }
			set	{ SetValue((int)JobcodeMasterFieldIndex.JobcodeTipPrompt, value); }
		}

		/// <summary> The RowVersion property of the Entity JobcodeMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Jobcode_Master"."row_version"<br/>
		/// Table field type characteristics (type, precision, scale, length): Timestamp, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Byte[] RowVersion
		{
			get { return (System.Byte[])GetValue((int)JobcodeMasterFieldIndex.RowVersion, true); }

		}

		/// <summary> The StoreId property of the Entity JobcodeMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Jobcode_Master"."store_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> StoreId
		{
			get { return (Nullable<System.Int32>)GetValue((int)JobcodeMasterFieldIndex.StoreId, false); }
			set	{ SetValue((int)JobcodeMasterFieldIndex.StoreId, value); }
		}
	
		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}
		
		/// <summary>Returns the InfoGenesis.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)InfoGenesis.EntityType.JobcodeMasterEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		
		#endregion

		#region Included code

		#endregion
	}
}

