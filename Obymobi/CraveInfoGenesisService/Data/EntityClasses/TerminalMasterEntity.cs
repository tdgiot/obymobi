﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Xml.Serialization;
using InfoGenesis;
using InfoGenesis.HelperClasses;
using InfoGenesis.FactoryClasses;
using InfoGenesis.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace InfoGenesis.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	

	/// <summary>Entity class which represents the entity 'TerminalMaster'.<br/><br/></summary>
	[Serializable]
	public partial class TerminalMasterEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
			
	{
		#region Class Member Declarations

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
		}
		#endregion
		
		/// <summary> Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static TerminalMasterEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary> CTor</summary>
		public TerminalMasterEntity():base("TerminalMasterEntity")
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <remarks>For framework usage.</remarks>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public TerminalMasterEntity(IEntityFields2 fields):base("TerminalMasterEntity")
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this TerminalMasterEntity</param>
		public TerminalMasterEntity(IValidator validator):base("TerminalMasterEntity")
		{
			InitClassEmpty(validator, null);
		}
				
		/// <summary> CTor</summary>
		/// <param name="termId">PK value for TerminalMaster which data should be fetched into this TerminalMaster object</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public TerminalMasterEntity(System.Int32 termId):base("TerminalMasterEntity")
		{
			InitClassEmpty(null, null);
			this.TermId = termId;
		}

		/// <summary> CTor</summary>
		/// <param name="termId">PK value for TerminalMaster which data should be fetched into this TerminalMaster object</param>
		/// <param name="validator">The custom validator object for this TerminalMasterEntity</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public TerminalMasterEntity(System.Int32 termId, IValidator validator):base("TerminalMasterEntity")
		{
			InitClassEmpty(validator, null);
			this.TermId = termId;
		}

		/// <summary> Protected CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected TerminalMasterEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			if(SerializationHelper.Optimization != SerializationOptimization.Fast) 
			{
				this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance());
			}
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
			
		}


		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}
		
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				default:
					break;				
			}
			return toReturn;
		}
#if !CF
		/// <summary>Checks if the relation mapped by the property with the name specified is a one way / single sided relation. If the passed in name is null, it/ will return true if the entity has any single-sided relation</summary>
		/// <param name="propertyName">Name of the property which is mapped onto the relation to check, or null to check if the entity has any relation/ which is single sided</param>
		/// <returns>true if the relation is single sided / one way (so the opposite relation isn't present), false otherwise</returns>
		protected override bool CheckOneWayRelations(string propertyName)
		{
			int numberOfOneWayRelations = 0;
			switch(propertyName)
			{
				case null:
					return ((numberOfOneWayRelations > 0) || base.CheckOneWayRelations(null));
				default:
					return base.CheckOneWayRelations(propertyName);
			}
		}
#endif
		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				default:
					break;
			}
		}

		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity2 objects, referenced by this entity</returns>
		protected override List<IEntity2> GetDependingRelatedEntities()
		{
			List<IEntity2> toReturn = new List<IEntity2>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These
		/// entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity2 objects, referenced by this entity</returns>
		protected override List<IEntity2> GetDependentRelatedEntities()
		{
			List<IEntity2> toReturn = new List<IEntity2>();
			return toReturn;
		}
		
		/// <summary>Gets a list of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection2 objects, referenced by this entity</returns>
		protected override List<IEntityCollection2> GetMemberEntityCollections()
		{
			List<IEntityCollection2> toReturn = new List<IEntityCollection2>();
			return toReturn;
		}

		/// <summary>ISerializable member. Does custom serialization so event handlers do not get serialized. Serializes members of this entity class and uses the base class' implementation to serialize the rest.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (SerializationHelper.Optimization != SerializationOptimization.Fast) 
			{
			}
			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			base.GetObjectData(info, context);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new TerminalMasterRelations().GetAllRelations();
		}
		

		/// <summary>Creates a new instance of the factory related to this entity</summary>
		protected override IEntityFactory2 CreateEntityFactory()
		{
			return EntityFactoryCache2.GetEntityFactory(typeof(TerminalMasterEntityFactory));
		}
#if !CF
		/// <summary>Adds the member collections to the collections queue (base first)</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		protected override void AddToMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue) 
		{
			base.AddToMemberEntityCollectionsQueue(collectionsQueue);
		}
		
		/// <summary>Gets the member collections queue from the queue (base first)</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		protected override void GetFromMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue)
		{
			base.GetFromMemberEntityCollectionsQueue(collectionsQueue);

		}
		
		/// <summary>Determines whether the entity has populated member collections</summary>
		/// <returns>true if the entity has populated member collections.</returns>
		protected override bool HasPopulatedMemberEntityCollections()
		{
			bool toReturn = false;
			return toReturn ? true : base.HasPopulatedMemberEntityCollections();
		}
		
		/// <summary>Creates the member entity collections queue.</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		/// <param name="requiredQueue">The required queue.</param>
		protected override void CreateMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue, Queue<bool> requiredQueue) 
		{
			base.CreateMemberEntityCollectionsQueue(collectionsQueue, requiredQueue);
		}
#endif
		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			return toReturn;
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			OnInitClassMembersComplete();
		}


		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AltBargunTermId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AltRcptTermId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BargunBumpOverrideFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BargunPrintDrinksOnBumpFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BargunTermFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CurrentVersion", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DefaultTableLayoutId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FirstTableNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FolFileLoadFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GaFileLoadFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IpAddress", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastPingUtcDateTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NumTables", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PedValue", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PrimaryProfitCenterId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProfileId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReceiptPrinterType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RmsFileLoadFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RowVersion", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StaticReceiptPrinterId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TermActiveFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TermGrpId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TermId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TermName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TermOptionGrpId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TermPrinterGrpId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TermReceiptInfo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TermServiceGrpId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VirtualTermFlag", fieldHashtable);
		}
		#endregion

		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this TerminalMasterEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END
			

			OnInitialized();

		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static TerminalMasterRelations Relations
		{
			get	{ return new TerminalMasterRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AltBargunTermId property of the Entity TerminalMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal_Master"."alt_bargun_term_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 AltBargunTermId
		{
			get { return (System.Int32)GetValue((int)TerminalMasterFieldIndex.AltBargunTermId, true); }
			set	{ SetValue((int)TerminalMasterFieldIndex.AltBargunTermId, value); }
		}

		/// <summary> The AltRcptTermId property of the Entity TerminalMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal_Master"."alt_rcpt_term_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> AltRcptTermId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TerminalMasterFieldIndex.AltRcptTermId, false); }
			set	{ SetValue((int)TerminalMasterFieldIndex.AltRcptTermId, value); }
		}

		/// <summary> The BargunBumpOverrideFlag property of the Entity TerminalMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal_Master"."bargun_bump_override_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> BargunBumpOverrideFlag
		{
			get { return (Nullable<System.Boolean>)GetValue((int)TerminalMasterFieldIndex.BargunBumpOverrideFlag, false); }
			set	{ SetValue((int)TerminalMasterFieldIndex.BargunBumpOverrideFlag, value); }
		}

		/// <summary> The BargunPrintDrinksOnBumpFlag property of the Entity TerminalMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal_Master"."bargun_print_drinks_on_bump_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean BargunPrintDrinksOnBumpFlag
		{
			get { return (System.Boolean)GetValue((int)TerminalMasterFieldIndex.BargunPrintDrinksOnBumpFlag, true); }
			set	{ SetValue((int)TerminalMasterFieldIndex.BargunPrintDrinksOnBumpFlag, value); }
		}

		/// <summary> The BargunTermFlag property of the Entity TerminalMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal_Master"."bargun_term_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean BargunTermFlag
		{
			get { return (System.Boolean)GetValue((int)TerminalMasterFieldIndex.BargunTermFlag, true); }
			set	{ SetValue((int)TerminalMasterFieldIndex.BargunTermFlag, value); }
		}

		/// <summary> The CurrentVersion property of the Entity TerminalMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal_Master"."current_version"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 15<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CurrentVersion
		{
			get { return (System.String)GetValue((int)TerminalMasterFieldIndex.CurrentVersion, true); }
			set	{ SetValue((int)TerminalMasterFieldIndex.CurrentVersion, value); }
		}

		/// <summary> The DefaultTableLayoutId property of the Entity TerminalMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal_Master"."default_table_layout_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> DefaultTableLayoutId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TerminalMasterFieldIndex.DefaultTableLayoutId, false); }
			set	{ SetValue((int)TerminalMasterFieldIndex.DefaultTableLayoutId, value); }
		}

		/// <summary> The FirstTableNo property of the Entity TerminalMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal_Master"."first_table_no"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> FirstTableNo
		{
			get { return (Nullable<System.Int32>)GetValue((int)TerminalMasterFieldIndex.FirstTableNo, false); }
			set	{ SetValue((int)TerminalMasterFieldIndex.FirstTableNo, value); }
		}

		/// <summary> The FolFileLoadFlag property of the Entity TerminalMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal_Master"."FOL_file_load_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean FolFileLoadFlag
		{
			get { return (System.Boolean)GetValue((int)TerminalMasterFieldIndex.FolFileLoadFlag, true); }
			set	{ SetValue((int)TerminalMasterFieldIndex.FolFileLoadFlag, value); }
		}

		/// <summary> The GaFileLoadFlag property of the Entity TerminalMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal_Master"."GA_file_load_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean GaFileLoadFlag
		{
			get { return (System.Boolean)GetValue((int)TerminalMasterFieldIndex.GaFileLoadFlag, true); }
			set	{ SetValue((int)TerminalMasterFieldIndex.GaFileLoadFlag, value); }
		}

		/// <summary> The IpAddress property of the Entity TerminalMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal_Master"."IP_address"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 15<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String IpAddress
		{
			get { return (System.String)GetValue((int)TerminalMasterFieldIndex.IpAddress, true); }
			set	{ SetValue((int)TerminalMasterFieldIndex.IpAddress, value); }
		}

		/// <summary> The LastPingUtcDateTime property of the Entity TerminalMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal_Master"."last_ping_utc_date_time"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> LastPingUtcDateTime
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TerminalMasterFieldIndex.LastPingUtcDateTime, false); }
			set	{ SetValue((int)TerminalMasterFieldIndex.LastPingUtcDateTime, value); }
		}

		/// <summary> The NumTables property of the Entity TerminalMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal_Master"."num_tables"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> NumTables
		{
			get { return (Nullable<System.Int32>)GetValue((int)TerminalMasterFieldIndex.NumTables, false); }
			set	{ SetValue((int)TerminalMasterFieldIndex.NumTables, value); }
		}

		/// <summary> The PedValue property of the Entity TerminalMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal_Master"."ped_value"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PedValue
		{
			get { return (System.String)GetValue((int)TerminalMasterFieldIndex.PedValue, true); }
			set	{ SetValue((int)TerminalMasterFieldIndex.PedValue, value); }
		}

		/// <summary> The PrimaryProfitCenterId property of the Entity TerminalMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal_Master"."primary_profit_center_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PrimaryProfitCenterId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TerminalMasterFieldIndex.PrimaryProfitCenterId, false); }
			set	{ SetValue((int)TerminalMasterFieldIndex.PrimaryProfitCenterId, value); }
		}

		/// <summary> The ProfileId property of the Entity TerminalMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal_Master"."profile_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ProfileId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TerminalMasterFieldIndex.ProfileId, false); }
			set	{ SetValue((int)TerminalMasterFieldIndex.ProfileId, value); }
		}

		/// <summary> The ReceiptPrinterType property of the Entity TerminalMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal_Master"."receipt_printer_type"<br/>
		/// Table field type characteristics (type, precision, scale, length): TinyInt, 3, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Byte ReceiptPrinterType
		{
			get { return (System.Byte)GetValue((int)TerminalMasterFieldIndex.ReceiptPrinterType, true); }
			set	{ SetValue((int)TerminalMasterFieldIndex.ReceiptPrinterType, value); }
		}

		/// <summary> The RmsFileLoadFlag property of the Entity TerminalMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal_Master"."RMS_file_load_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean RmsFileLoadFlag
		{
			get { return (System.Boolean)GetValue((int)TerminalMasterFieldIndex.RmsFileLoadFlag, true); }
			set	{ SetValue((int)TerminalMasterFieldIndex.RmsFileLoadFlag, value); }
		}

		/// <summary> The RowVersion property of the Entity TerminalMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal_Master"."row_version"<br/>
		/// Table field type characteristics (type, precision, scale, length): Timestamp, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Byte[] RowVersion
		{
			get { return (System.Byte[])GetValue((int)TerminalMasterFieldIndex.RowVersion, true); }

		}

		/// <summary> The StaticReceiptPrinterId property of the Entity TerminalMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal_Master"."static_receipt_printer_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 StaticReceiptPrinterId
		{
			get { return (System.Int32)GetValue((int)TerminalMasterFieldIndex.StaticReceiptPrinterId, true); }
			set	{ SetValue((int)TerminalMasterFieldIndex.StaticReceiptPrinterId, value); }
		}

		/// <summary> The TermActiveFlag property of the Entity TerminalMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal_Master"."term_active_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean TermActiveFlag
		{
			get { return (System.Boolean)GetValue((int)TerminalMasterFieldIndex.TermActiveFlag, true); }
			set	{ SetValue((int)TerminalMasterFieldIndex.TermActiveFlag, value); }
		}

		/// <summary> The TermGrpId property of the Entity TerminalMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal_Master"."term_grp_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TermGrpId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TerminalMasterFieldIndex.TermGrpId, false); }
			set	{ SetValue((int)TerminalMasterFieldIndex.TermGrpId, value); }
		}

		/// <summary> The TermId property of the Entity TerminalMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal_Master"."term_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int32 TermId
		{
			get { return (System.Int32)GetValue((int)TerminalMasterFieldIndex.TermId, true); }
			set	{ SetValue((int)TerminalMasterFieldIndex.TermId, value); }
		}

		/// <summary> The TermName property of the Entity TerminalMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal_Master"."term_name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TermName
		{
			get { return (System.String)GetValue((int)TerminalMasterFieldIndex.TermName, true); }
			set	{ SetValue((int)TerminalMasterFieldIndex.TermName, value); }
		}

		/// <summary> The TermOptionGrpId property of the Entity TerminalMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal_Master"."term_option_grp_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TermOptionGrpId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TerminalMasterFieldIndex.TermOptionGrpId, false); }
			set	{ SetValue((int)TerminalMasterFieldIndex.TermOptionGrpId, value); }
		}

		/// <summary> The TermPrinterGrpId property of the Entity TerminalMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal_Master"."term_printer_grp_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TermPrinterGrpId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TerminalMasterFieldIndex.TermPrinterGrpId, false); }
			set	{ SetValue((int)TerminalMasterFieldIndex.TermPrinterGrpId, value); }
		}

		/// <summary> The TermReceiptInfo property of the Entity TerminalMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal_Master"."term_receipt_info"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TermReceiptInfo
		{
			get { return (System.String)GetValue((int)TerminalMasterFieldIndex.TermReceiptInfo, true); }
			set	{ SetValue((int)TerminalMasterFieldIndex.TermReceiptInfo, value); }
		}

		/// <summary> The TermServiceGrpId property of the Entity TerminalMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal_Master"."term_service_grp_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TermServiceGrpId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TerminalMasterFieldIndex.TermServiceGrpId, false); }
			set	{ SetValue((int)TerminalMasterFieldIndex.TermServiceGrpId, value); }
		}

		/// <summary> The VirtualTermFlag property of the Entity TerminalMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Terminal_Master"."virtual_term_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean VirtualTermFlag
		{
			get { return (System.Boolean)GetValue((int)TerminalMasterFieldIndex.VirtualTermFlag, true); }
			set	{ SetValue((int)TerminalMasterFieldIndex.VirtualTermFlag, value); }
		}
	
		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}
		
		/// <summary>Returns the InfoGenesis.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)InfoGenesis.EntityType.TerminalMasterEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		
		#endregion

		#region Included code

		#endregion
	}
}

