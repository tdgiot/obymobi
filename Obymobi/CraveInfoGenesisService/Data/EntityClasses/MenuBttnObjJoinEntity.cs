﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Xml.Serialization;
using InfoGenesis;
using InfoGenesis.HelperClasses;
using InfoGenesis.FactoryClasses;
using InfoGenesis.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace InfoGenesis.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	

	/// <summary>Entity class which represents the entity 'MenuBttnObjJoin'.<br/><br/></summary>
	[Serializable]
	public partial class MenuBttnObjJoinEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
			
	{
		#region Class Member Declarations

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
		}
		#endregion
		
		/// <summary> Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static MenuBttnObjJoinEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary> CTor</summary>
		public MenuBttnObjJoinEntity():base("MenuBttnObjJoinEntity")
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <remarks>For framework usage.</remarks>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public MenuBttnObjJoinEntity(IEntityFields2 fields):base("MenuBttnObjJoinEntity")
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this MenuBttnObjJoinEntity</param>
		public MenuBttnObjJoinEntity(IValidator validator):base("MenuBttnObjJoinEntity")
		{
			InitClassEmpty(validator, null);
		}
				
		/// <summary> CTor</summary>
		/// <param name="buttonId">PK value for MenuBttnObjJoin which data should be fetched into this MenuBttnObjJoin object</param>
		/// <param name="entId">PK value for MenuBttnObjJoin which data should be fetched into this MenuBttnObjJoin object</param>
		/// <param name="menuId">PK value for MenuBttnObjJoin which data should be fetched into this MenuBttnObjJoin object</param>
		/// <param name="screenId">PK value for MenuBttnObjJoin which data should be fetched into this MenuBttnObjJoin object</param>
		/// <param name="storeCreatedId">PK value for MenuBttnObjJoin which data should be fetched into this MenuBttnObjJoin object</param>
		/// <param name="terminalTypeId">PK value for MenuBttnObjJoin which data should be fetched into this MenuBttnObjJoin object</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public MenuBttnObjJoinEntity(System.Int32 buttonId, System.Int32 entId, System.Int32 menuId, System.Int32 screenId, System.Int32 storeCreatedId, System.Int32 terminalTypeId):base("MenuBttnObjJoinEntity")
		{
			InitClassEmpty(null, null);
			this.ButtonId = buttonId;
			this.EntId = entId;
			this.MenuId = menuId;
			this.ScreenId = screenId;
			this.StoreCreatedId = storeCreatedId;
			this.TerminalTypeId = terminalTypeId;
		}

		/// <summary> CTor</summary>
		/// <param name="buttonId">PK value for MenuBttnObjJoin which data should be fetched into this MenuBttnObjJoin object</param>
		/// <param name="entId">PK value for MenuBttnObjJoin which data should be fetched into this MenuBttnObjJoin object</param>
		/// <param name="menuId">PK value for MenuBttnObjJoin which data should be fetched into this MenuBttnObjJoin object</param>
		/// <param name="screenId">PK value for MenuBttnObjJoin which data should be fetched into this MenuBttnObjJoin object</param>
		/// <param name="storeCreatedId">PK value for MenuBttnObjJoin which data should be fetched into this MenuBttnObjJoin object</param>
		/// <param name="terminalTypeId">PK value for MenuBttnObjJoin which data should be fetched into this MenuBttnObjJoin object</param>
		/// <param name="validator">The custom validator object for this MenuBttnObjJoinEntity</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public MenuBttnObjJoinEntity(System.Int32 buttonId, System.Int32 entId, System.Int32 menuId, System.Int32 screenId, System.Int32 storeCreatedId, System.Int32 terminalTypeId, IValidator validator):base("MenuBttnObjJoinEntity")
		{
			InitClassEmpty(validator, null);
			this.ButtonId = buttonId;
			this.EntId = entId;
			this.MenuId = menuId;
			this.ScreenId = screenId;
			this.StoreCreatedId = storeCreatedId;
			this.TerminalTypeId = terminalTypeId;
		}

		/// <summary> Protected CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected MenuBttnObjJoinEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			if(SerializationHelper.Optimization != SerializationOptimization.Fast) 
			{
				this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance());
			}
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
			
		}


		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}
		
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				default:
					break;				
			}
			return toReturn;
		}
#if !CF
		/// <summary>Checks if the relation mapped by the property with the name specified is a one way / single sided relation. If the passed in name is null, it/ will return true if the entity has any single-sided relation</summary>
		/// <param name="propertyName">Name of the property which is mapped onto the relation to check, or null to check if the entity has any relation/ which is single sided</param>
		/// <returns>true if the relation is single sided / one way (so the opposite relation isn't present), false otherwise</returns>
		protected override bool CheckOneWayRelations(string propertyName)
		{
			int numberOfOneWayRelations = 0;
			switch(propertyName)
			{
				case null:
					return ((numberOfOneWayRelations > 0) || base.CheckOneWayRelations(null));
				default:
					return base.CheckOneWayRelations(propertyName);
			}
		}
#endif
		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				default:
					break;
			}
		}

		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity2 objects, referenced by this entity</returns>
		protected override List<IEntity2> GetDependingRelatedEntities()
		{
			List<IEntity2> toReturn = new List<IEntity2>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These
		/// entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity2 objects, referenced by this entity</returns>
		protected override List<IEntity2> GetDependentRelatedEntities()
		{
			List<IEntity2> toReturn = new List<IEntity2>();
			return toReturn;
		}
		
		/// <summary>Gets a list of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection2 objects, referenced by this entity</returns>
		protected override List<IEntityCollection2> GetMemberEntityCollections()
		{
			List<IEntityCollection2> toReturn = new List<IEntityCollection2>();
			return toReturn;
		}

		/// <summary>ISerializable member. Does custom serialization so event handlers do not get serialized. Serializes members of this entity class and uses the base class' implementation to serialize the rest.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (SerializationHelper.Optimization != SerializationOptimization.Fast) 
			{
			}
			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			base.GetObjectData(info, context);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new MenuBttnObjJoinRelations().GetAllRelations();
		}
		

		/// <summary>Creates a new instance of the factory related to this entity</summary>
		protected override IEntityFactory2 CreateEntityFactory()
		{
			return EntityFactoryCache2.GetEntityFactory(typeof(MenuBttnObjJoinEntityFactory));
		}
#if !CF
		/// <summary>Adds the member collections to the collections queue (base first)</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		protected override void AddToMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue) 
		{
			base.AddToMemberEntityCollectionsQueue(collectionsQueue);
		}
		
		/// <summary>Gets the member collections queue from the queue (base first)</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		protected override void GetFromMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue)
		{
			base.GetFromMemberEntityCollectionsQueue(collectionsQueue);

		}
		
		/// <summary>Determines whether the entity has populated member collections</summary>
		/// <returns>true if the entity has populated member collections.</returns>
		protected override bool HasPopulatedMemberEntityCollections()
		{
			bool toReturn = false;
			return toReturn ? true : base.HasPopulatedMemberEntityCollections();
		}
		
		/// <summary>Creates the member entity collections queue.</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		/// <param name="requiredQueue">The required queue.</param>
		protected override void CreateMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue, Queue<bool> requiredQueue) 
		{
			base.CreateMemberEntityCollectionsQueue(collectionsQueue, requiredQueue);
		}
#endif
		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			return toReturn;
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			OnInitClassMembersComplete();
		}


		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ButtonBackcolor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ButtonBorderStyle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ButtonColumn", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ButtonFontId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ButtonForecolor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ButtonHeight", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ButtonId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ButtonImageId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ButtonNextMenuId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ButtonRow", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ButtonShapeId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ButtonWidth", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DisplayImageFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EntId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ErrorCodeId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MenuId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ObjectId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ObjectTypeId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ScreenId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StoreCreatedId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TerminalTypeId", fieldHashtable);
		}
		#endregion

		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this MenuBttnObjJoinEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END
			

			OnInitialized();

		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static MenuBttnObjJoinRelations Relations
		{
			get	{ return new MenuBttnObjJoinRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ButtonBackcolor property of the Entity MenuBttnObjJoin<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Bttn_Obj_Join"."button_backcolor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ButtonBackcolor
		{
			get { return (Nullable<System.Int32>)GetValue((int)MenuBttnObjJoinFieldIndex.ButtonBackcolor, false); }
			set	{ SetValue((int)MenuBttnObjJoinFieldIndex.ButtonBackcolor, value); }
		}

		/// <summary> The ButtonBorderStyle property of the Entity MenuBttnObjJoin<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Bttn_Obj_Join"."button_border_style"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ButtonBorderStyle
		{
			get { return (Nullable<System.Int32>)GetValue((int)MenuBttnObjJoinFieldIndex.ButtonBorderStyle, false); }
			set	{ SetValue((int)MenuBttnObjJoinFieldIndex.ButtonBorderStyle, value); }
		}

		/// <summary> The ButtonColumn property of the Entity MenuBttnObjJoin<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Bttn_Obj_Join"."button_column"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ButtonColumn
		{
			get { return (Nullable<System.Int32>)GetValue((int)MenuBttnObjJoinFieldIndex.ButtonColumn, false); }
			set	{ SetValue((int)MenuBttnObjJoinFieldIndex.ButtonColumn, value); }
		}

		/// <summary> The ButtonFontId property of the Entity MenuBttnObjJoin<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Bttn_Obj_Join"."button_font_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ButtonFontId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MenuBttnObjJoinFieldIndex.ButtonFontId, false); }
			set	{ SetValue((int)MenuBttnObjJoinFieldIndex.ButtonFontId, value); }
		}

		/// <summary> The ButtonForecolor property of the Entity MenuBttnObjJoin<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Bttn_Obj_Join"."button_forecolor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ButtonForecolor
		{
			get { return (Nullable<System.Int32>)GetValue((int)MenuBttnObjJoinFieldIndex.ButtonForecolor, false); }
			set	{ SetValue((int)MenuBttnObjJoinFieldIndex.ButtonForecolor, value); }
		}

		/// <summary> The ButtonHeight property of the Entity MenuBttnObjJoin<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Bttn_Obj_Join"."button_height"<br/>
		/// Table field type characteristics (type, precision, scale, length): SmallInt, 5, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> ButtonHeight
		{
			get { return (Nullable<System.Int16>)GetValue((int)MenuBttnObjJoinFieldIndex.ButtonHeight, false); }
			set	{ SetValue((int)MenuBttnObjJoinFieldIndex.ButtonHeight, value); }
		}

		/// <summary> The ButtonId property of the Entity MenuBttnObjJoin<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Bttn_Obj_Join"."button_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int32 ButtonId
		{
			get { return (System.Int32)GetValue((int)MenuBttnObjJoinFieldIndex.ButtonId, true); }
			set	{ SetValue((int)MenuBttnObjJoinFieldIndex.ButtonId, value); }
		}

		/// <summary> The ButtonImageId property of the Entity MenuBttnObjJoin<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Bttn_Obj_Join"."button_image_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ButtonImageId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MenuBttnObjJoinFieldIndex.ButtonImageId, false); }
			set	{ SetValue((int)MenuBttnObjJoinFieldIndex.ButtonImageId, value); }
		}

		/// <summary> The ButtonNextMenuId property of the Entity MenuBttnObjJoin<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Bttn_Obj_Join"."button_next_menu_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ButtonNextMenuId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MenuBttnObjJoinFieldIndex.ButtonNextMenuId, false); }
			set	{ SetValue((int)MenuBttnObjJoinFieldIndex.ButtonNextMenuId, value); }
		}

		/// <summary> The ButtonRow property of the Entity MenuBttnObjJoin<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Bttn_Obj_Join"."button_row"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ButtonRow
		{
			get { return (Nullable<System.Int32>)GetValue((int)MenuBttnObjJoinFieldIndex.ButtonRow, false); }
			set	{ SetValue((int)MenuBttnObjJoinFieldIndex.ButtonRow, value); }
		}

		/// <summary> The ButtonShapeId property of the Entity MenuBttnObjJoin<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Bttn_Obj_Join"."button_shape_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): SmallInt, 5, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> ButtonShapeId
		{
			get { return (Nullable<System.Int16>)GetValue((int)MenuBttnObjJoinFieldIndex.ButtonShapeId, false); }
			set	{ SetValue((int)MenuBttnObjJoinFieldIndex.ButtonShapeId, value); }
		}

		/// <summary> The ButtonWidth property of the Entity MenuBttnObjJoin<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Bttn_Obj_Join"."button_width"<br/>
		/// Table field type characteristics (type, precision, scale, length): SmallInt, 5, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> ButtonWidth
		{
			get { return (Nullable<System.Int16>)GetValue((int)MenuBttnObjJoinFieldIndex.ButtonWidth, false); }
			set	{ SetValue((int)MenuBttnObjJoinFieldIndex.ButtonWidth, value); }
		}

		/// <summary> The DisplayImageFlag property of the Entity MenuBttnObjJoin<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Bttn_Obj_Join"."display_image_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean DisplayImageFlag
		{
			get { return (System.Boolean)GetValue((int)MenuBttnObjJoinFieldIndex.DisplayImageFlag, true); }
			set	{ SetValue((int)MenuBttnObjJoinFieldIndex.DisplayImageFlag, value); }
		}

		/// <summary> The EntId property of the Entity MenuBttnObjJoin<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Bttn_Obj_Join"."ent_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int32 EntId
		{
			get { return (System.Int32)GetValue((int)MenuBttnObjJoinFieldIndex.EntId, true); }
			set	{ SetValue((int)MenuBttnObjJoinFieldIndex.EntId, value); }
		}

		/// <summary> The ErrorCodeId property of the Entity MenuBttnObjJoin<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Bttn_Obj_Join"."error_code_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ErrorCodeId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MenuBttnObjJoinFieldIndex.ErrorCodeId, false); }
			set	{ SetValue((int)MenuBttnObjJoinFieldIndex.ErrorCodeId, value); }
		}

		/// <summary> The MenuId property of the Entity MenuBttnObjJoin<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Bttn_Obj_Join"."menu_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int32 MenuId
		{
			get { return (System.Int32)GetValue((int)MenuBttnObjJoinFieldIndex.MenuId, true); }
			set	{ SetValue((int)MenuBttnObjJoinFieldIndex.MenuId, value); }
		}

		/// <summary> The ObjectId property of the Entity MenuBttnObjJoin<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Bttn_Obj_Join"."object_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ObjectId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MenuBttnObjJoinFieldIndex.ObjectId, false); }
			set	{ SetValue((int)MenuBttnObjJoinFieldIndex.ObjectId, value); }
		}

		/// <summary> The ObjectTypeId property of the Entity MenuBttnObjJoin<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Bttn_Obj_Join"."object_type_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ObjectTypeId
		{
			get { return (System.Int32)GetValue((int)MenuBttnObjJoinFieldIndex.ObjectTypeId, true); }
			set	{ SetValue((int)MenuBttnObjJoinFieldIndex.ObjectTypeId, value); }
		}

		/// <summary> The ScreenId property of the Entity MenuBttnObjJoin<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Bttn_Obj_Join"."screen_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int32 ScreenId
		{
			get { return (System.Int32)GetValue((int)MenuBttnObjJoinFieldIndex.ScreenId, true); }
			set	{ SetValue((int)MenuBttnObjJoinFieldIndex.ScreenId, value); }
		}

		/// <summary> The StoreCreatedId property of the Entity MenuBttnObjJoin<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Bttn_Obj_Join"."store_created_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int32 StoreCreatedId
		{
			get { return (System.Int32)GetValue((int)MenuBttnObjJoinFieldIndex.StoreCreatedId, true); }
			set	{ SetValue((int)MenuBttnObjJoinFieldIndex.StoreCreatedId, value); }
		}

		/// <summary> The TerminalTypeId property of the Entity MenuBttnObjJoin<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Menu_Bttn_Obj_Join"."terminal_type_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int32 TerminalTypeId
		{
			get { return (System.Int32)GetValue((int)MenuBttnObjJoinFieldIndex.TerminalTypeId, true); }
			set	{ SetValue((int)MenuBttnObjJoinFieldIndex.TerminalTypeId, value); }
		}
	
		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}
		
		/// <summary>Returns the InfoGenesis.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)InfoGenesis.EntityType.MenuBttnObjJoinEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		
		#endregion

		#region Included code

		#endregion
	}
}

