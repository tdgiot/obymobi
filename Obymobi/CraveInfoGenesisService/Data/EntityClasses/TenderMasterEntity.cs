﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Xml.Serialization;
using InfoGenesis;
using InfoGenesis.HelperClasses;
using InfoGenesis.FactoryClasses;
using InfoGenesis.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace InfoGenesis.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	

	/// <summary>Entity class which represents the entity 'TenderMaster'.<br/><br/></summary>
	[Serializable]
	public partial class TenderMasterEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
			
	{
		#region Class Member Declarations

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
		}
		#endregion
		
		/// <summary> Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static TenderMasterEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary> CTor</summary>
		public TenderMasterEntity():base("TenderMasterEntity")
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <remarks>For framework usage.</remarks>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public TenderMasterEntity(IEntityFields2 fields):base("TenderMasterEntity")
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this TenderMasterEntity</param>
		public TenderMasterEntity(IValidator validator):base("TenderMasterEntity")
		{
			InitClassEmpty(validator, null);
		}
				
		/// <summary> CTor</summary>
		/// <param name="entId">PK value for TenderMaster which data should be fetched into this TenderMaster object</param>
		/// <param name="tenderId">PK value for TenderMaster which data should be fetched into this TenderMaster object</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public TenderMasterEntity(System.Int32 entId, System.Int32 tenderId):base("TenderMasterEntity")
		{
			InitClassEmpty(null, null);
			this.EntId = entId;
			this.TenderId = tenderId;
		}

		/// <summary> CTor</summary>
		/// <param name="entId">PK value for TenderMaster which data should be fetched into this TenderMaster object</param>
		/// <param name="tenderId">PK value for TenderMaster which data should be fetched into this TenderMaster object</param>
		/// <param name="validator">The custom validator object for this TenderMasterEntity</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public TenderMasterEntity(System.Int32 entId, System.Int32 tenderId, IValidator validator):base("TenderMasterEntity")
		{
			InitClassEmpty(validator, null);
			this.EntId = entId;
			this.TenderId = tenderId;
		}

		/// <summary> Protected CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected TenderMasterEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			if(SerializationHelper.Optimization != SerializationOptimization.Fast) 
			{
				this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance());
			}
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
			
		}


		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}
		
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				default:
					break;				
			}
			return toReturn;
		}
#if !CF
		/// <summary>Checks if the relation mapped by the property with the name specified is a one way / single sided relation. If the passed in name is null, it/ will return true if the entity has any single-sided relation</summary>
		/// <param name="propertyName">Name of the property which is mapped onto the relation to check, or null to check if the entity has any relation/ which is single sided</param>
		/// <returns>true if the relation is single sided / one way (so the opposite relation isn't present), false otherwise</returns>
		protected override bool CheckOneWayRelations(string propertyName)
		{
			int numberOfOneWayRelations = 0;
			switch(propertyName)
			{
				case null:
					return ((numberOfOneWayRelations > 0) || base.CheckOneWayRelations(null));
				default:
					return base.CheckOneWayRelations(propertyName);
			}
		}
#endif
		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				default:
					break;
			}
		}

		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity2 objects, referenced by this entity</returns>
		protected override List<IEntity2> GetDependingRelatedEntities()
		{
			List<IEntity2> toReturn = new List<IEntity2>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These
		/// entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity2 objects, referenced by this entity</returns>
		protected override List<IEntity2> GetDependentRelatedEntities()
		{
			List<IEntity2> toReturn = new List<IEntity2>();
			return toReturn;
		}
		
		/// <summary>Gets a list of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection2 objects, referenced by this entity</returns>
		protected override List<IEntityCollection2> GetMemberEntityCollections()
		{
			List<IEntityCollection2> toReturn = new List<IEntityCollection2>();
			return toReturn;
		}

		/// <summary>ISerializable member. Does custom serialization so event handlers do not get serialized. Serializes members of this entity class and uses the base class' implementation to serialize the rest.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (SerializationHelper.Optimization != SerializationOptimization.Fast) 
			{
			}
			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			base.GetObjectData(info, context);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new TenderMasterRelations().GetAllRelations();
		}
		

		/// <summary>Creates a new instance of the factory related to this entity</summary>
		protected override IEntityFactory2 CreateEntityFactory()
		{
			return EntityFactoryCache2.GetEntityFactory(typeof(TenderMasterEntityFactory));
		}
#if !CF
		/// <summary>Adds the member collections to the collections queue (base first)</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		protected override void AddToMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue) 
		{
			base.AddToMemberEntityCollectionsQueue(collectionsQueue);
		}
		
		/// <summary>Gets the member collections queue from the queue (base first)</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		protected override void GetFromMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue)
		{
			base.GetFromMemberEntityCollectionsQueue(collectionsQueue);

		}
		
		/// <summary>Determines whether the entity has populated member collections</summary>
		/// <returns>true if the entity has populated member collections.</returns>
		protected override bool HasPopulatedMemberEntityCollections()
		{
			bool toReturn = false;
			return toReturn ? true : base.HasPopulatedMemberEntityCollections();
		}
		
		/// <summary>Creates the member entity collections queue.</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		/// <param name="requiredQueue">The required queue.</param>
		protected override void CreateMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue, Queue<bool> requiredQueue) 
		{
			base.CreateMemberEntityCollectionsQueue(collectionsQueue, requiredQueue);
		}
#endif
		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			return toReturn;
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			OnInitClassMembersComplete();
		}


		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AdditionalCheckidCodeId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AutoRemoveTaxFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BypassPdsFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CheckTypeId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompTenderFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DestinationPropertyCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DiscoupId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EmvCardTypeCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EnterTipPrompt", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EntId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FirstTenderFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FrankingCodeId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IccDecimalPlaces", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IccRate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastTenderFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LoyaltyEarnEligibleFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NumReceiptsPrint", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OpenCashdrwrCodeId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OvertenderCodeId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PostAcctNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PostSiteId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PostSystem1Flag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PostSystem2Flag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PostSystem3Flag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PostSystem4Flag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PostSystem5Flag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PostSystem6Flag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PostSystem7Flag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PostSystem8Flag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PriceLevelId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PromptCvvFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PromptExtraAlphaFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PromptExtraDataFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PromptZipcodeFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RequireAmtFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RestrictedFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RowVersion", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SalesTippableFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SecurityId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StoreId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TaxCompCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TenderAbbr1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TenderAbbr2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TenderClassId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TenderId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TenderLimit", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TenderName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UseArchiveFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UseSigcapFlag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VerificationCodeId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VerificationManualEntryCodeId", fieldHashtable);
		}
		#endregion

		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this TenderMasterEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END
			

			OnInitialized();

		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static TenderMasterRelations Relations
		{
			get	{ return new TenderMasterRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AdditionalCheckidCodeId property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."additional_checkid_code_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): TinyInt, 3, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Byte AdditionalCheckidCodeId
		{
			get { return (System.Byte)GetValue((int)TenderMasterFieldIndex.AdditionalCheckidCodeId, true); }
			set	{ SetValue((int)TenderMasterFieldIndex.AdditionalCheckidCodeId, value); }
		}

		/// <summary> The AutoRemoveTaxFlag property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."auto_remove_tax_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean AutoRemoveTaxFlag
		{
			get { return (System.Boolean)GetValue((int)TenderMasterFieldIndex.AutoRemoveTaxFlag, true); }
			set	{ SetValue((int)TenderMasterFieldIndex.AutoRemoveTaxFlag, value); }
		}

		/// <summary> The BypassPdsFlag property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."bypass_pds_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean BypassPdsFlag
		{
			get { return (System.Boolean)GetValue((int)TenderMasterFieldIndex.BypassPdsFlag, true); }
			set	{ SetValue((int)TenderMasterFieldIndex.BypassPdsFlag, value); }
		}

		/// <summary> The CheckTypeId property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."check_type_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CheckTypeId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TenderMasterFieldIndex.CheckTypeId, false); }
			set	{ SetValue((int)TenderMasterFieldIndex.CheckTypeId, value); }
		}

		/// <summary> The CompTenderFlag property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."comp_tender_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean CompTenderFlag
		{
			get { return (System.Boolean)GetValue((int)TenderMasterFieldIndex.CompTenderFlag, true); }
			set	{ SetValue((int)TenderMasterFieldIndex.CompTenderFlag, value); }
		}

		/// <summary> The DestinationPropertyCode property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."destination_property_code"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 6<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DestinationPropertyCode
		{
			get { return (System.String)GetValue((int)TenderMasterFieldIndex.DestinationPropertyCode, true); }
			set	{ SetValue((int)TenderMasterFieldIndex.DestinationPropertyCode, value); }
		}

		/// <summary> The DiscoupId property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."discoup_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> DiscoupId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TenderMasterFieldIndex.DiscoupId, false); }
			set	{ SetValue((int)TenderMasterFieldIndex.DiscoupId, value); }
		}

		/// <summary> The EmvCardTypeCode property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."emv_card_type_code"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String EmvCardTypeCode
		{
			get { return (System.String)GetValue((int)TenderMasterFieldIndex.EmvCardTypeCode, true); }
			set	{ SetValue((int)TenderMasterFieldIndex.EmvCardTypeCode, value); }
		}

		/// <summary> The EnterTipPrompt property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."enter_tip_prompt"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean EnterTipPrompt
		{
			get { return (System.Boolean)GetValue((int)TenderMasterFieldIndex.EnterTipPrompt, true); }
			set	{ SetValue((int)TenderMasterFieldIndex.EnterTipPrompt, value); }
		}

		/// <summary> The EntId property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."ent_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int32 EntId
		{
			get { return (System.Int32)GetValue((int)TenderMasterFieldIndex.EntId, true); }
			set	{ SetValue((int)TenderMasterFieldIndex.EntId, value); }
		}

		/// <summary> The FirstTenderFlag property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."first_tender_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean FirstTenderFlag
		{
			get { return (System.Boolean)GetValue((int)TenderMasterFieldIndex.FirstTenderFlag, true); }
			set	{ SetValue((int)TenderMasterFieldIndex.FirstTenderFlag, value); }
		}

		/// <summary> The FrankingCodeId property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."franking_code_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> FrankingCodeId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TenderMasterFieldIndex.FrankingCodeId, false); }
			set	{ SetValue((int)TenderMasterFieldIndex.FrankingCodeId, value); }
		}

		/// <summary> The IccDecimalPlaces property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."icc_decimal_places"<br/>
		/// Table field type characteristics (type, precision, scale, length): SmallInt, 5, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> IccDecimalPlaces
		{
			get { return (Nullable<System.Int16>)GetValue((int)TenderMasterFieldIndex.IccDecimalPlaces, false); }
			set	{ SetValue((int)TenderMasterFieldIndex.IccDecimalPlaces, value); }
		}

		/// <summary> The IccRate property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."icc_rate"<br/>
		/// Table field type characteristics (type, precision, scale, length): Float, 38, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Double> IccRate
		{
			get { return (Nullable<System.Double>)GetValue((int)TenderMasterFieldIndex.IccRate, false); }
			set	{ SetValue((int)TenderMasterFieldIndex.IccRate, value); }
		}

		/// <summary> The LastTenderFlag property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."last_tender_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean LastTenderFlag
		{
			get { return (System.Boolean)GetValue((int)TenderMasterFieldIndex.LastTenderFlag, true); }
			set	{ SetValue((int)TenderMasterFieldIndex.LastTenderFlag, value); }
		}

		/// <summary> The LoyaltyEarnEligibleFlag property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."loyalty_earn_eligible_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean LoyaltyEarnEligibleFlag
		{
			get { return (System.Boolean)GetValue((int)TenderMasterFieldIndex.LoyaltyEarnEligibleFlag, true); }
			set	{ SetValue((int)TenderMasterFieldIndex.LoyaltyEarnEligibleFlag, value); }
		}

		/// <summary> The NumReceiptsPrint property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."num_receipts_print"<br/>
		/// Table field type characteristics (type, precision, scale, length): SmallInt, 5, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> NumReceiptsPrint
		{
			get { return (Nullable<System.Int16>)GetValue((int)TenderMasterFieldIndex.NumReceiptsPrint, false); }
			set	{ SetValue((int)TenderMasterFieldIndex.NumReceiptsPrint, value); }
		}

		/// <summary> The OpenCashdrwrCodeId property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."open_cashdrwr_code_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): SmallInt, 5, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> OpenCashdrwrCodeId
		{
			get { return (Nullable<System.Int16>)GetValue((int)TenderMasterFieldIndex.OpenCashdrwrCodeId, false); }
			set	{ SetValue((int)TenderMasterFieldIndex.OpenCashdrwrCodeId, value); }
		}

		/// <summary> The OvertenderCodeId property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."overtender_code_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): SmallInt, 5, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> OvertenderCodeId
		{
			get { return (Nullable<System.Int16>)GetValue((int)TenderMasterFieldIndex.OvertenderCodeId, false); }
			set	{ SetValue((int)TenderMasterFieldIndex.OvertenderCodeId, value); }
		}

		/// <summary> The PostAcctNo property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."post_acct_no"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 30<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PostAcctNo
		{
			get { return (System.String)GetValue((int)TenderMasterFieldIndex.PostAcctNo, true); }
			set	{ SetValue((int)TenderMasterFieldIndex.PostAcctNo, value); }
		}

		/// <summary> The PostSiteId property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."post_site_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PostSiteId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TenderMasterFieldIndex.PostSiteId, false); }
			set	{ SetValue((int)TenderMasterFieldIndex.PostSiteId, value); }
		}

		/// <summary> The PostSystem1Flag property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."post_system1_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean PostSystem1Flag
		{
			get { return (System.Boolean)GetValue((int)TenderMasterFieldIndex.PostSystem1Flag, true); }
			set	{ SetValue((int)TenderMasterFieldIndex.PostSystem1Flag, value); }
		}

		/// <summary> The PostSystem2Flag property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."post_system2_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean PostSystem2Flag
		{
			get { return (System.Boolean)GetValue((int)TenderMasterFieldIndex.PostSystem2Flag, true); }
			set	{ SetValue((int)TenderMasterFieldIndex.PostSystem2Flag, value); }
		}

		/// <summary> The PostSystem3Flag property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."post_system3_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean PostSystem3Flag
		{
			get { return (System.Boolean)GetValue((int)TenderMasterFieldIndex.PostSystem3Flag, true); }
			set	{ SetValue((int)TenderMasterFieldIndex.PostSystem3Flag, value); }
		}

		/// <summary> The PostSystem4Flag property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."post_system4_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean PostSystem4Flag
		{
			get { return (System.Boolean)GetValue((int)TenderMasterFieldIndex.PostSystem4Flag, true); }
			set	{ SetValue((int)TenderMasterFieldIndex.PostSystem4Flag, value); }
		}

		/// <summary> The PostSystem5Flag property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."post_system5_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean PostSystem5Flag
		{
			get { return (System.Boolean)GetValue((int)TenderMasterFieldIndex.PostSystem5Flag, true); }
			set	{ SetValue((int)TenderMasterFieldIndex.PostSystem5Flag, value); }
		}

		/// <summary> The PostSystem6Flag property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."post_system6_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean PostSystem6Flag
		{
			get { return (System.Boolean)GetValue((int)TenderMasterFieldIndex.PostSystem6Flag, true); }
			set	{ SetValue((int)TenderMasterFieldIndex.PostSystem6Flag, value); }
		}

		/// <summary> The PostSystem7Flag property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."post_system7_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean PostSystem7Flag
		{
			get { return (System.Boolean)GetValue((int)TenderMasterFieldIndex.PostSystem7Flag, true); }
			set	{ SetValue((int)TenderMasterFieldIndex.PostSystem7Flag, value); }
		}

		/// <summary> The PostSystem8Flag property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."post_system8_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean PostSystem8Flag
		{
			get { return (System.Boolean)GetValue((int)TenderMasterFieldIndex.PostSystem8Flag, true); }
			set	{ SetValue((int)TenderMasterFieldIndex.PostSystem8Flag, value); }
		}

		/// <summary> The PriceLevelId property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."price_level_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PriceLevelId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TenderMasterFieldIndex.PriceLevelId, false); }
			set	{ SetValue((int)TenderMasterFieldIndex.PriceLevelId, value); }
		}

		/// <summary> The PromptCvvFlag property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."prompt_cvv_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean PromptCvvFlag
		{
			get { return (System.Boolean)GetValue((int)TenderMasterFieldIndex.PromptCvvFlag, true); }
			set	{ SetValue((int)TenderMasterFieldIndex.PromptCvvFlag, value); }
		}

		/// <summary> The PromptExtraAlphaFlag property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."prompt_extra_alpha_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean PromptExtraAlphaFlag
		{
			get { return (System.Boolean)GetValue((int)TenderMasterFieldIndex.PromptExtraAlphaFlag, true); }
			set	{ SetValue((int)TenderMasterFieldIndex.PromptExtraAlphaFlag, value); }
		}

		/// <summary> The PromptExtraDataFlag property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."prompt_extra_data_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean PromptExtraDataFlag
		{
			get { return (System.Boolean)GetValue((int)TenderMasterFieldIndex.PromptExtraDataFlag, true); }
			set	{ SetValue((int)TenderMasterFieldIndex.PromptExtraDataFlag, value); }
		}

		/// <summary> The PromptZipcodeFlag property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."prompt_zipcode_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean PromptZipcodeFlag
		{
			get { return (System.Boolean)GetValue((int)TenderMasterFieldIndex.PromptZipcodeFlag, true); }
			set	{ SetValue((int)TenderMasterFieldIndex.PromptZipcodeFlag, value); }
		}

		/// <summary> The RequireAmtFlag property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."require_amt_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean RequireAmtFlag
		{
			get { return (System.Boolean)GetValue((int)TenderMasterFieldIndex.RequireAmtFlag, true); }
			set	{ SetValue((int)TenderMasterFieldIndex.RequireAmtFlag, value); }
		}

		/// <summary> The RestrictedFlag property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."restricted_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean RestrictedFlag
		{
			get { return (System.Boolean)GetValue((int)TenderMasterFieldIndex.RestrictedFlag, true); }
			set	{ SetValue((int)TenderMasterFieldIndex.RestrictedFlag, value); }
		}

		/// <summary> The RowVersion property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."row_version"<br/>
		/// Table field type characteristics (type, precision, scale, length): Timestamp, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Byte[] RowVersion
		{
			get { return (System.Byte[])GetValue((int)TenderMasterFieldIndex.RowVersion, true); }

		}

		/// <summary> The SalesTippableFlag property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."sales_tippable_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean SalesTippableFlag
		{
			get { return (System.Boolean)GetValue((int)TenderMasterFieldIndex.SalesTippableFlag, true); }
			set	{ SetValue((int)TenderMasterFieldIndex.SalesTippableFlag, value); }
		}

		/// <summary> The SecurityId property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."security_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SecurityId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TenderMasterFieldIndex.SecurityId, false); }
			set	{ SetValue((int)TenderMasterFieldIndex.SecurityId, value); }
		}

		/// <summary> The StoreId property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."store_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> StoreId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TenderMasterFieldIndex.StoreId, false); }
			set	{ SetValue((int)TenderMasterFieldIndex.StoreId, value); }
		}

		/// <summary> The TaxCompCode property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."tax_comp_code"<br/>
		/// Table field type characteristics (type, precision, scale, length): TinyInt, 3, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Byte TaxCompCode
		{
			get { return (System.Byte)GetValue((int)TenderMasterFieldIndex.TaxCompCode, true); }
			set	{ SetValue((int)TenderMasterFieldIndex.TaxCompCode, value); }
		}

		/// <summary> The TenderAbbr1 property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."tender_abbr1"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 7<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TenderAbbr1
		{
			get { return (System.String)GetValue((int)TenderMasterFieldIndex.TenderAbbr1, true); }
			set	{ SetValue((int)TenderMasterFieldIndex.TenderAbbr1, value); }
		}

		/// <summary> The TenderAbbr2 property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."tender_abbr2"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 7<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TenderAbbr2
		{
			get { return (System.String)GetValue((int)TenderMasterFieldIndex.TenderAbbr2, true); }
			set	{ SetValue((int)TenderMasterFieldIndex.TenderAbbr2, value); }
		}

		/// <summary> The TenderClassId property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."tender_class_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TenderClassId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TenderMasterFieldIndex.TenderClassId, false); }
			set	{ SetValue((int)TenderMasterFieldIndex.TenderClassId, value); }
		}

		/// <summary> The TenderId property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."tender_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int32 TenderId
		{
			get { return (System.Int32)GetValue((int)TenderMasterFieldIndex.TenderId, true); }
			set	{ SetValue((int)TenderMasterFieldIndex.TenderId, value); }
		}

		/// <summary> The TenderLimit property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."tender_limit"<br/>
		/// Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> TenderLimit
		{
			get { return (Nullable<System.Decimal>)GetValue((int)TenderMasterFieldIndex.TenderLimit, false); }
			set	{ SetValue((int)TenderMasterFieldIndex.TenderLimit, value); }
		}

		/// <summary> The TenderName property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."tender_name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 16<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TenderName
		{
			get { return (System.String)GetValue((int)TenderMasterFieldIndex.TenderName, true); }
			set	{ SetValue((int)TenderMasterFieldIndex.TenderName, value); }
		}

		/// <summary> The UseArchiveFlag property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."use_archive_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean UseArchiveFlag
		{
			get { return (System.Boolean)GetValue((int)TenderMasterFieldIndex.UseArchiveFlag, true); }
			set	{ SetValue((int)TenderMasterFieldIndex.UseArchiveFlag, value); }
		}

		/// <summary> The UseSigcapFlag property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."use_sigcap_flag"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean UseSigcapFlag
		{
			get { return (System.Boolean)GetValue((int)TenderMasterFieldIndex.UseSigcapFlag, true); }
			set	{ SetValue((int)TenderMasterFieldIndex.UseSigcapFlag, value); }
		}

		/// <summary> The VerificationCodeId property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."verification_code_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> VerificationCodeId
		{
			get { return (Nullable<System.Int32>)GetValue((int)TenderMasterFieldIndex.VerificationCodeId, false); }
			set	{ SetValue((int)TenderMasterFieldIndex.VerificationCodeId, value); }
		}

		/// <summary> The VerificationManualEntryCodeId property of the Entity TenderMaster<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Tender_Master"."verification_manual_entry_code_id"<br/>
		/// Table field type characteristics (type, precision, scale, length): SmallInt, 5, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int16 VerificationManualEntryCodeId
		{
			get { return (System.Int16)GetValue((int)TenderMasterFieldIndex.VerificationManualEntryCodeId, true); }
			set	{ SetValue((int)TenderMasterFieldIndex.VerificationManualEntryCodeId, value); }
		}
	
		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}
		
		/// <summary>Returns the InfoGenesis.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)InfoGenesis.EntityType.TenderMasterEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		
		#endregion

		#region Included code

		#endregion
	}
}

