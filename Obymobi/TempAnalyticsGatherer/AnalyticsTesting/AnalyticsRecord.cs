﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TempAnalyticsGatherer.AnalyticsTesting
{
    public class Totals
    {
        public string visits { get; set; }
        public string hits { get; set; }
        public string timeOnSite { get; set; }
        public string screenviews { get; set; }
        public string uniqueScreenviews { get; set; }
        public string timeOnScreen { get; set; }
    }

    public class AdwordsClickInfo
    {
    }

    public class TrafficSource
    {
        public string source { get; set; }
        public string medium { get; set; }
        public AdwordsClickInfo adwordsClickInfo { get; set; }
        public bool isTrueDirect { get; set; }
    }

    public class Device
    {
        public string browser { get; set; }
        public string browserVersion { get; set; }
        public string operatingSystem { get; set; }
        public string operatingSystemVersion { get; set; }
        public bool isMobile { get; set; }
        public string mobileDeviceInfo { get; set; }
        public string flashVersion { get; set; }
        public bool javaEnabled { get; set; }
        public string language { get; set; }
        public string screenColors { get; set; }
        public string screenResolution { get; set; }
        public string deviceCategory { get; set; }
        public string browserSize { get; set; }
    }

    public class GeoNetwork
    {
        public string continent { get; set; }
        public string subContinent { get; set; }
        public string country { get; set; }
        public string region { get; set; }
        public string metro { get; set; }
        public string city { get; set; }
        public string cityId { get; set; }
        public string networkDomain { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public string networkLocation { get; set; }
    }

    public class Page
    {
        public string pagePathLevel1 { get; set; }
        public string pagePathLevel2 { get; set; }
        public string pagePathLevel3 { get; set; }
        public string pagePathLevel4 { get; set; }
    }

    public class AppInfo
    {
        public string name { get; set; }
        public string version { get; set; }
        public string id { get; set; }
        public string installerId { get; set; }
        public string appInstallerId { get; set; }
        public string appName { get; set; }
        public string appVersion { get; set; }
        public string appId { get; set; }
        public string screenName { get; set; }
        public string landingScreenName { get; set; }
        public string exitScreenName { get; set; }
        public string screenDepth { get; set; }
    }

    public class ExceptionInfo
    {
        public bool isFatal { get; set; }
    }

    public class EventInfo
    {
        public string eventCategory { get; set; }
        public string eventAction { get; set; }
        public string eventLabel { get; set; }
        public string eventValue { get; set; }
    }

    public class CustomDimension
    {
        public string index { get; set; }
        public string value { get; set; }
    }

    public class Social
    {
        public string socialNetwork { get; set; }
        public string hasSocialSourceReferral { get; set; }
        public string socialInteractionNetworkAction { get; set; }
    }

    public class ECommerceAction
    {
        public string action_type { get; set; }
        public string step { get; set; }
    }

    public class RawHit
    {
        public string hitNumber { get; set; }
        public string time { get; set; }
        public string hour { get; set; }
        public string minute { get; set; }
        public bool isInteraction { get; set; }
        public Page page { get; set; }
        public AppInfo appInfo { get; set; }
        public ExceptionInfo exceptionInfo { get; set; }
        public EventInfo eventInfo { get; set; }
        public List<object> customVariables { get; set; }
        public List<CustomDimension> customDimensions { get; set; }
        public List<object> customMetrics { get; set; }
        public string type { get; set; }
        public Social social { get; set; }
        public List<object> product { get; set; }
        public List<object> promotion { get; set; }
        public ECommerceAction eCommerceAction { get; set; }
        public List<object> experiment { get; set; }
        public bool? isEntrance { get; set; }
        public bool? isExit { get; set; }
    }

    public class AnalyticsRecord
    {
        public string visitNumber { get; set; }
        public string visitId { get; set; }
        public string visitStartTime { get; set; }
        public string date { get; set; }
        public Totals totals { get; set; }
        public TrafficSource trafficSource { get; set; }
        public Device device { get; set; }
        public GeoNetwork geoNetwork { get; set; }
        public List<object> customDimensions { get; set; }
        public List<RawHit> hits { get; set; }
        public string fullVisitorId { get; set; }
        public string channelGrouping { get; set; }
        public string socialEngagementType { get; set; }
    }
}