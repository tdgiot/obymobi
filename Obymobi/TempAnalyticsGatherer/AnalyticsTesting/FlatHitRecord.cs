﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Obymobi.Analytics.Cache.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TempAnalyticsGatherer.AnalyticsTesting
{
    public class FlatHitRecord : IFlatHitRecord
    {
        [JsonProperty(PropertyName ="id")]
        public string Id { get; set; }
        public int VisitId { get; set; }
        public int VisitNumber { get; set; }
        public int VisitStartTime { get; set; }

        [JsonConverter(typeof(IsoDateTimeConverter))]
        [JsonProperty("date")]
        public DateTime Date { get; set; }
        public int HitNumber { get; set; }
        public int HitTime { get; set; }
        public int HitHour { get; set; }
        public int HitMinute { get; set; }
        public string HitType { get; set; }
        public string AppName { get; set; }
        public int Version { get; set; }
        public string ScreenName { get; set; }
        public string EventCategory { get; set; }
        public string EventAction { get; set; }
        public string EventLabel { get; set; }
        public int? EventValue { get; set; }
        public string ApplicationType { get; set; }
        public int TimeOnSite { get; set; }

        public string LegacyCustomDimension1 { get; set; }
        public string LegacyCustomDimension2 { get; set; }
        public string MacAddress { get; set; }
        public string DeviceType { get; set; }
        public int? TimeStamp { get; set; }
        public string ProductName { get; set; }
        public string AlterationName { get; set; }
        public string ServiceRequestName { get; set; }
        public string CategoryName { get; set; }
        public string EntertainmentName { get; set; }
        public string ApplicationName { get; set; }
        public string DeliverypointNumber { get; set; }
        public int? DeliverypointNumberInt { get; set; }
        public string DeliverypointGroupName { get; set; }
        public string PrimaryKeys { get; set; }
        public bool? IsTablet { get; set; }
        public string OperatingSystem { get; set; }
        public string CompanyName { get; set; }
        public string LegacyApplicationVersion { get; set; }
        public string NavigationSource { get; set; }
        public string LegacyApplicationName { get; set; }
        public string HitVersion { get; set; }
        public int? CompanyId { get; set; }
        public int? ProductId { get; set; }
        public int? CategoryId { get; set; }
        public int? EntertainmentId { get; set; }
        public int? DeliverypointId { get; set; }
        public string DeliverypointName { get; set; }
        public int? AdvertisementId { get; set; }
        public string AdvertisementName { get; set; }
        public string AdvertisementLocation { get; set; }
        public int? SiteId { get; set; }
        public string SiteName { get; set; }
        public int? PageId { get; set; }
        public string PageName { get; set; }
        public int? TabId { get; set; }
        public string TabName { get; set; }
        public int? MessageId { get; set; }
        public string MessageName { get; set; }
        public int? RoomControlAreaId { get; set; }
        public string RoomControlAreaName { get; set; }
        public int? WidgetId { get; set; }
        public string WidgetName { get; set; }
        public string WidgetCaption { get; set; }
        public int? DeliverypointGroupId { get; set; }
        public int? ClientId { get; set; }
        public string OrderSource { get; set; }
        public string Action { get; set; }
        public string AttachmentName { get; set; }
        public int? AttachmentId { get; set; }
        public int? RoomControlAreaSectionId { get; set; }
        public string RoomControlAreaSectionName { get; set; }
        public int? RoomControlAreaSectionItemId { get; set; }
        public string RoomControlAreaSectionItemName { get; set; }
        public string RoomControlSceneName { get; set; }
        public string RoomControlComponentName { get; set; }
        public int? RoomControlComponentId { get; set; }
        public int? RoomControlStationListingId { get; set; }
        public string RoomControlStationListingName { get; set; }
        public string RoomControlThermostatMode { get; set; }
        public string RoomControlThermostatScale { get; set; }
        public int? RoomControlThermostatTemperature { get; set; }
        public string RoomControlThermostatFanMode { get; set; }
        public int? RoomControlStationId { get; set; }
        public string RoomControlStationName { get; set; }
        public string RoomControlTvRemoteAction { get; set; }
        public int? MediaId { get; set; }
        public int? OrderId { get; set; }
        public int? OrderitemId { get; set; }
        public string TestPrefix { get; set; }
        public string MessageResponse { get; set; }
        public string ClientOperationMode { get; set; }
        public string OrderType { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
