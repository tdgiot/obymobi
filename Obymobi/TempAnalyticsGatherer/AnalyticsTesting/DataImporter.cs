﻿using Dionysos;
using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json;
using Obymobi.Analytics.Cache.Data;
using Obymobi.Analytics.Cache.Data.EntityClasses;
using Obymobi.Analytics.Google;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;

namespace TempAnalyticsGatherer.AnalyticsTesting
{
    public class DataImporter
    {
        public DataImporter()
        {

        }

        private const string EndpointUri = "https://gakrtestgahits.documents.azure.com:443/";
        private const string PrimaryKey = "0nxW48hudvkaJ3bEFlI3MWvrWHobf9QLmy7wXduyW7dIObkvdqAldWKNWQSmBG18j6NtlSLtkCZUSy1JqsXMWQ==";
        private DocumentClient client;

        private object lockO = new object();

        public void ImportMongoAndSql()
        {
            // Files
            var filePaths = new List<string>();
            filePaths = Directory.GetFiles(@"D:\Temp\GA Exports\Unpacked").ToList();
            //filePaths.Add(@"D:\Temp\GA Exports\Unpacked\20161001json-000000000000");

            int progress = 0;
            int fails = 0;

            // http://mongodb.github.io/mongo-csharp-driver/2.2/getting_started/quick_tour/
            var client = new MongoClient(); // Takes local instance automagiccall.
            var database = client.GetDatabase("test");

            try
            {
                database.CreateCollection("gahits");
            }
            catch
            {
            }

            try
            {
                database.CreateCollection("gaflathits");
            }
            catch
            {
            }
            //}
            //var nestedCollection = database.GetCollection<AnalyticsRecord>("gahits");
            var flatCollection = database.GetCollection<FlatHitRecord>("gaflathits");

            Stopwatch sw = new Stopwatch();
            sw.Start();
            foreach (var filePath in filePaths)
            {
                string line;
                using (StreamReader file = new StreamReader(filePath))
                {
                    while ((line = file.ReadLine()) != null)
                    {
                        var record = JsonConvert.DeserializeObject<AnalyticsRecord>(line);

                        // nestedCollection.InsertOne(record);

                        Parallel.ForEach(record.hits, source =>
                        {
                            try
                            {
                                var target = new FlatHitRecord();
                                WriteHit(source, record, target);
                                target.Id = "{0}.{1}.{2}.{3}.{4}".FormatSafe(target.Date.DateTimeToSimpleDateTimeStamp(), target.VisitId, target.VisitNumber, target.HitNumber, DateTime.Now.Ticks);
                                flatCollection.InsertOne(target);

                                var target2 = new HitEntity();
                                WriteHit(source, record, target2);
                                target2.Save();
                            }
                            catch (Exception ex)
                            {
                                Debug.WriteLine("Failed: {0}".FormatSafe(ex.Message));
                                fails++;
                            }

                            int localProgess = Interlocked.Increment(ref progress);
                            if (localProgess % 100000 == 0)
                            {
                                string progressText = "Mongo/Sql is now: {0} hits in {1} seconds (@ {2}) - Fails: {3} | Rate {4} .".FormatSafe(localProgess, sw.ElapsedMilliseconds / 1000, DateTime.Now, fails, Decimal.Divide(localProgess, sw.ElapsedMilliseconds / 1000));
                                File.AppendAllText("ImportLog.txt", progressText + "\r\n");
                                Debug.WriteLine(progressText);
                            }
                        }); // each hit
                    } // read file line
                } // using stream reader
            } // foreach file path

            sw.Stop();
            Debug.WriteLine("{0} hits in {1}".FormatSafe(progress, sw.Elapsed));
        }

        public void ImportMongoDbLocal()
        {
            // Files
            var filePaths = new List<string>();
            filePaths = Directory.GetFiles(@"D:\Temp\GA Exports\Unpacked").ToList();
            //filePaths.Add(@"D:\Temp\GA Exports\Unpacked\20161001json-000000000000");

            int progress = 0;
            int fails = 0;

            // http://mongodb.github.io/mongo-csharp-driver/2.2/getting_started/quick_tour/
            var client = new MongoClient(); // Takes local instance automagiccall.
            var database = client.GetDatabase("test");

            try
            {
                database.CreateCollection("gahits");
            }
            catch
            {
            }

            try
            {
                database.CreateCollection("gaflathits");
            }
            catch
            {
            }
            //}
            //var nestedCollection = database.GetCollection<AnalyticsRecord>("gahits");
            var flatCollection = database.GetCollection<FlatHitRecord>("gaflathits");            

            Stopwatch sw = new Stopwatch();
            sw.Start();
            foreach (var filePath in filePaths)
            {
                string line;
                using (StreamReader file = new StreamReader(filePath))
                {
                    while ((line = file.ReadLine()) != null)
                    {
                        var record = JsonConvert.DeserializeObject<AnalyticsRecord>(line);

                        // nestedCollection.InsertOne(record);

                        Parallel.ForEach(record.hits, source =>
                        {
                            try
                            {
                                var target = new FlatHitRecord();
                                WriteHit(source, record, target);
                                flatCollection.InsertOne(target);
                            }
                            catch (Exception ex)
                            {
                                Debug.WriteLine("Failed: {0}".FormatSafe(ex.Message));
                                fails++;
                            }

                            int localProgess = Interlocked.Increment(ref progress);
                            if (localProgess % 100000 == 0)
                            {
                                string progressText = "MongoDb is now: {0} hits in {1} seconds (@ {2}) - Fails: {3} | Rate {4} .".FormatSafe(localProgess, sw.ElapsedMilliseconds / 1000, DateTime.Now, fails, Decimal.Divide(localProgess, sw.ElapsedMilliseconds/1000));
                                File.AppendAllText("ImportLog.txt", progressText + "\r\n");                                
                                Debug.WriteLine(progressText);
                            }
                        }); // each hit
                    } // read file line
                } // using stream reader
            } // foreach file path

            sw.Stop();
            Debug.WriteLine("{0} hits in {1}".FormatSafe(progress, sw.Elapsed));

        }

        public async void ImportDocumentDb()
        {
            // Connect
            try
            {
                this.client = new DocumentClient(new Uri(EndpointUri), PrimaryKey);
            }
            catch (DocumentClientException de)
            {
                Exception baseException = de.GetBaseException();
                Debug.WriteLine("{0} error occurred: {1}, Message: {2}", de.StatusCode, de.Message, baseException.Message);
            }
            catch (Exception e)
            {
                Exception baseException = e.GetBaseException();
                Debug.WriteLine("Error: {0}, Message: {1}", e.Message, baseException.Message);
            }

            // Verify DB existence
            string databaseName = "flathits2";
            string collectionName = "flathitscol2";
            var dbColUri = UriFactory.CreateDocumentCollectionUri(databaseName, collectionName);
            var dbUri = UriFactory.CreateDatabaseUri(databaseName);

            bool redo = true;
            if (redo)
            {
                try
                {
                    var result = await this.client.DeleteDatabaseAsync(dbUri);
                    Debug.WriteLine("Delete 1: " + result.StatusCode);
                    var result2 = await this.client.DeleteDatabaseAsync(UriFactory.CreateDatabaseUri("flathits"));
                    Debug.WriteLine("Delete 2: " + result2.StatusCode);
                }
                catch
                {                    
                }
            }

            try
            {
                await this.client.ReadDatabaseAsync(UriFactory.CreateDatabaseUri(databaseName));
                Debug.WriteLine("Found {0}", databaseName, "");
            }
            catch (DocumentClientException de)
            {
                // If the database does not exist, create a new database
                if (de.StatusCode == HttpStatusCode.NotFound)
                {
                    await this.client.CreateDatabaseAsync(new Database { Id = databaseName });
                    Debug.WriteLine("Created {0}", databaseName, "");
                }
                else
                {
                    throw;
                }
            }

            // Verify Collection existence

            try
            {
                await this.client.ReadDocumentCollectionAsync(dbColUri);
                Debug.WriteLine("Found {0}", collectionName);
            }
            catch (DocumentClientException de)
            {
                // If the document collection does not exist, create a new collection
                if (de.StatusCode == HttpStatusCode.NotFound)
                {
                    DocumentCollection collectionInfo = new DocumentCollection();
                    collectionInfo.Id = collectionName;
                    collectionInfo.PartitionKey.Paths.Add("/date");

                    // Configure collections for maximum query flexibility including string range queries.
                    collectionInfo.IndexingPolicy = new IndexingPolicy(new RangeIndex(DataType.String) { Precision = -1 });

                    // Here we create a collection with 20000 RU/s.
                    await this.client.CreateDocumentCollectionAsync(UriFactory.CreateDatabaseUri(databaseName),collectionInfo,new RequestOptions { OfferThroughput = 20000 });

                    Debug.WriteLine("Created {0}", collectionName, "");
                }
                else
                {
                    throw;
                }
            }

            // Test  Query
            try
            {
                FeedOptions queryOptions = new FeedOptions { MaxItemCount = 1000 };
                IQueryable<FlatHitRecord> query = this.client.CreateDocumentQuery<FlatHitRecord>(dbColUri, queryOptions);
                var list = query.ToList();
            }
            catch
            {

            }            


            // Files
            var filePaths = new List<string>();
            filePaths = Directory.GetFiles(@"D:\Temp\GA Exports\Unpacked").ToList();
            //filePaths.Add(@"D:\Temp\GA Exports\Unpacked\20161001json-000000000000");

            int progress = 0;
            int fails = 0;

            Stopwatch sw = new Stopwatch();
            sw.Start();
            foreach (var filePath in filePaths)
            {
                string line;
                using (StreamReader file = new StreamReader(filePath))
                {                    
                    while ((line = file.ReadLine()) != null)
                    {
                        var record = JsonConvert.DeserializeObject<AnalyticsRecord>(line);

                        //Parallel.ForEach(record.hits, source =>
                        using (RateGate rg = new RateGate(750, TimeSpan.FromSeconds(1)))
                        {
                            foreach (var source in record.hits)
                            {
                                try
                                {
                                    rg.WaitToProceed();
                                    var target = new FlatHitRecord();
                                    WriteHit(source, record, target);
                                    target.Id = "{0}.{1}.{2}.{3}.{4}".FormatSafe(target.Date.DateTimeToSimpleDateTimeStamp(), target.VisitId, target.VisitNumber, target.HitNumber, DateTime.Now.Ticks);
                                    var result = this.client.CreateDocumentAsync(UriFactory.CreateDocumentCollectionUri(databaseName, collectionName), target).Result;
                                }
                                catch (Exception ex)
                                {
                                    Debug.WriteLine("Failed: {0}".FormatSafe(ex.Message));
                                    fails++;
                                }

                                int localProgess = Interlocked.Increment(ref progress);
                                if (localProgess % 100000 == 0)
                                {
                                    string progressText = "DocumentDb is now: {0} hits in {1} seconds (@ {2}) - Fails: {3}.".FormatSafe(localProgess, sw.ElapsedMilliseconds / 1000, DateTime.Now, fails);
                                    File.AppendAllText("ImportLog.txt", progressText + "\r\n");
                                    Debug.WriteLine(progressText);
                                }
                            } // each hit
                        }
                    } // read file line
                } // using stream reader
            } // foreach file path

            sw.Stop();
            Debug.WriteLine("{0} hits in {1}".FormatSafe(progress, sw.Elapsed));


        }

        public void ImportMsSql()
        {
            // Files
            var filePaths = new List<string>();
            filePaths = Directory.GetFiles(@"D:\Temp\GA Exports\Unpacked").ToList();
            //filePaths.Add(@"D:\Temp\GA Exports\Unpacked\20161001json-000000000000");

            int progress = 0;
            int fails = 0;

            Stopwatch sw = new Stopwatch();
            sw.Start();
            foreach (var filePath in filePaths)
            {
                string line;
                using (StreamReader file = new StreamReader(filePath))
                {
                    while ((line = file.ReadLine()) != null)
                    {
                        var record = JsonConvert.DeserializeObject<AnalyticsRecord>(line);

                        Parallel.ForEach(record.hits, source =>
                        {
                            try
                            {                               
                                for (int i = 0; i < 12; i++)
                                {
                                    var target = new HitEntity();
                                    WriteHit(source, record, target);
                                    target.Save();
                                }                                
                            }
                            catch (Exception ex)
                            {
                                Debug.WriteLine("Failed: {0}".FormatSafe(ex.Message));
                                fails++;
                            }
                            
                            int localProgess = Interlocked.Increment(ref progress);
                            if (localProgess % 100000 == 0)
                            {
                                localProgess = localProgess * 5;
                                string progressText = "Progress is now: {0} hits in {1} seconds (@ {2}) - Fails: {3} - Rate: {4} per second.".FormatSafe(localProgess, sw.ElapsedMilliseconds / 1000, DateTime.Now, fails, localProgess / (sw.ElapsedMilliseconds / 1000));
                                File.AppendAllText("ImportLog.txt", progressText + "\r\n");
                                Debug.WriteLine(progressText);
                            }
                        }); // each hit
                    } // read file line
                } // using stream reader
            } // foreach file path

            sw.Stop();
            Debug.WriteLine("{0} hits in {1}".FormatSafe(progress, sw.Elapsed));
        }        

        private void WriteHit(RawHit source, AnalyticsRecord record, IFlatHitRecord target)
        {         
            target.VisitId = ParseInt(record.visitId);
            target.VisitNumber = ParseInt(record.visitNumber);
            target.VisitStartTime = ParseInt(record.visitStartTime);
            target.Date = DateTimeUtil.SimpleDateStampToDateTime(record.date);
            target.TimeOnSite = ParseInt(record.totals.timeOnSite);

            target.HitNumber = ParseInt(source.hitNumber);
            target.HitTime = ParseInt(source.time);
            target.HitHour = ParseInt(source.hour);
            target.HitMinute = ParseInt(source.minute);
            target.HitType = ParseString(source.type);
            target.AppName = ParseString(source.appInfo.appName) ?? "NoName";
            target.Version = ParseInt(source.appInfo.appVersion);
            target.ScreenName = ParseString(source.appInfo.screenName);
            if (source.eventInfo != null)
            {
                target.EventCategory = ParseString(source.eventInfo.eventCategory);
                target.EventAction = ParseString(source.eventInfo.eventAction);
                target.EventLabel = ParseString(source.eventInfo.eventLabel);
                target.EventValue = ParseInt(source.eventInfo.eventValue);
            }

            foreach (var customDimension in source.customDimensions)
            {
                int dimensionIndex = ParseInt(customDimension.index);

                if (dimensionIndex <= 0)
                    continue;

                CustomDimensionDefinition dimension;
                if (!EnumUtil.TryParse(dimensionIndex, out dimension))
                    continue;

                string stringVal = ParseString(customDimension.value);
                if (stringVal == null)
                    continue;

                int intVal = ParseInt(customDimension.value);

                switch (dimension)
                {
                    case CustomDimensionDefinition.Unknown:
                        break;
                    case CustomDimensionDefinition.LegacyCustomDimension1:
                        target.LegacyCustomDimension1 = stringVal;
                        break;
                    case CustomDimensionDefinition.LegacyCustomDimension2:
                        target.LegacyCustomDimension2 = stringVal;
                        break;
                    case CustomDimensionDefinition.MacAddress:
                        target.MacAddress = stringVal;
                        break;
                    case CustomDimensionDefinition.DeviceType:
                        target.DeviceType = stringVal;
                        break;
                    case CustomDimensionDefinition.TimeStamp:
                        target.TimeStamp = intVal;
                        break;
                    case CustomDimensionDefinition.ProductName:
                        target.ProductName = stringVal;
                        break;
                    case CustomDimensionDefinition.AlterationName:
                        target.AlterationName = stringVal;
                        break;
                    case CustomDimensionDefinition.ServiceRequestName:
                        target.ServiceRequestName = stringVal;
                        break;
                    case CustomDimensionDefinition.CategoryName:
                        target.CategoryName = stringVal;
                        break;
                    case CustomDimensionDefinition.EntertainmentName:
                        target.EntertainmentName = stringVal;
                        break;
                    case CustomDimensionDefinition.ApplicationType:
                        target.ApplicationType = stringVal;
                        break;
                    case CustomDimensionDefinition.DeliverypointNumber:
                        target.DeliverypointNumber = stringVal;
                        target.DeliverypointNumberInt = intVal;
                        break;
                    case CustomDimensionDefinition.DeliverypointGroupName:
                        target.DeliverypointGroupName = stringVal;
                        break;
                    case CustomDimensionDefinition.PrimaryKeys:
                        target.PrimaryKeys = stringVal;
                        break;
                    case CustomDimensionDefinition.IsTablet:
                        target.IsTablet = (intVal > 0);
                        break;
                    case CustomDimensionDefinition.OperatingSystem:
                        target.OperatingSystem = stringVal;
                        break;
                    case CustomDimensionDefinition.CompanyName:
                        target.CompanyName = stringVal;
                        break;
                    case CustomDimensionDefinition.LegacyApplicationVersion:
                        target.LegacyApplicationVersion = stringVal;
                        break;
                    case CustomDimensionDefinition.NavigationSource:
                        target.NavigationSource = stringVal;
                        break;
                    case CustomDimensionDefinition.LegacyApplicationName:
                        target.LegacyApplicationName = stringVal;
                        break;
                    case CustomDimensionDefinition.HitVersion:
                        target.HitVersion = stringVal;
                        break;
                    case CustomDimensionDefinition.CompanyId:
                        target.CompanyId = intVal;
                        break;
                    case CustomDimensionDefinition.ProductId:
                        target.ProductId = intVal;
                        break;
                    case CustomDimensionDefinition.CategoryId:
                        target.CategoryId = intVal;
                        break;
                    case CustomDimensionDefinition.EntertainmentId:
                        target.EntertainmentId = intVal;
                        break;
                    case CustomDimensionDefinition.DeliverypointId:
                        target.DeliverypointId = intVal;
                        break;
                    case CustomDimensionDefinition.DeliverypointName:
                        target.DeliverypointName = stringVal;
                        break;
                    case CustomDimensionDefinition.AdvertisementId:
                        target.AdvertisementId = intVal;
                        break;
                    case CustomDimensionDefinition.AdvertisementName:
                        target.AdvertisementName = stringVal;
                        break;
                    case CustomDimensionDefinition.AdvertisementLocation:
                        target.AdvertisementLocation = stringVal;
                        break;
                    case CustomDimensionDefinition.SiteId:
                        target.SiteId = intVal;
                        break;
                    case CustomDimensionDefinition.SiteName:
                        target.SiteName = stringVal;
                        break;
                    case CustomDimensionDefinition.PageId:
                        target.PageId = intVal;
                        break;
                    case CustomDimensionDefinition.PageName:
                        target.PageName = stringVal;
                        break;
                    case CustomDimensionDefinition.TabId:
                        target.TabId = intVal;
                        break;
                    case CustomDimensionDefinition.TabName:
                        target.TabName = stringVal;
                        break;
                    case CustomDimensionDefinition.MessageId:
                        target.MessageId = intVal;
                        break;
                    case CustomDimensionDefinition.MessageName:
                        target.MessageName = stringVal;
                        break;
                    case CustomDimensionDefinition.RoomControlAreaId:
                        target.RoomControlAreaId = intVal;
                        break;
                    case CustomDimensionDefinition.RoomControlAreaName:
                        target.RoomControlAreaName = stringVal;
                        break;
                    case CustomDimensionDefinition.WidgetId:
                        target.WidgetId = intVal;
                        break;
                    case CustomDimensionDefinition.WidgetName:
                        target.WidgetName = stringVal;
                        break;
                    case CustomDimensionDefinition.WidgetCaption:
                        target.WidgetCaption = stringVal;
                        break;
                    case CustomDimensionDefinition.DeliverypointGroupId:
                        target.DeliverypointGroupId = intVal;
                        break;
                    case CustomDimensionDefinition.ClientId:
                        target.ClientId = intVal;
                        break;
                    case CustomDimensionDefinition.OrderSource:
                        target.OrderSource = stringVal;
                        break;
                    case CustomDimensionDefinition.Action:
                        target.Action = stringVal;
                        break;
                    case CustomDimensionDefinition.AttachmentName:
                        target.AttachmentName = stringVal;
                        break;
                    case CustomDimensionDefinition.AttachmentId:
                        target.AttachmentId = intVal;
                        break;
                    case CustomDimensionDefinition.RoomControlAreaSectionId:
                        target.RoomControlAreaSectionId = intVal;
                        break;
                    case CustomDimensionDefinition.RoomControlAreaSectionName:
                        target.RoomControlAreaSectionName = stringVal;
                        break;
                    case CustomDimensionDefinition.RoomControlAreaSectionItemId:
                        target.RoomControlAreaSectionItemId = intVal;
                        break;
                    case CustomDimensionDefinition.RoomControlAreaSectionItemName:
                        target.RoomControlAreaSectionItemName = stringVal;
                        break;
                    case CustomDimensionDefinition.RoomControlSceneName:
                        target.RoomControlSceneName = stringVal;
                        break;
                    case CustomDimensionDefinition.RoomControlComponentName:
                        target.RoomControlComponentName = stringVal;
                        break;
                    case CustomDimensionDefinition.RoomControlComponentId:
                        target.RoomControlComponentId = intVal;
                        break;
                    case CustomDimensionDefinition.RoomControlStationListingId:
                        target.RoomControlStationListingId = intVal;
                        break;
                    case CustomDimensionDefinition.RoomControlStationListingName:
                        target.RoomControlStationListingName = stringVal;
                        break;
                    case CustomDimensionDefinition.RoomControlThermostatMode:
                        target.RoomControlThermostatMode = stringVal;
                        break;
                    case CustomDimensionDefinition.RoomControlThermostatScale:
                        target.RoomControlThermostatScale = stringVal;
                        break;
                    case CustomDimensionDefinition.RoomControlThermostatTemperature:
                        target.RoomControlThermostatTemperature = intVal;
                        break;
                    case CustomDimensionDefinition.RoomControlThermostatFanMode:
                        target.RoomControlThermostatFanMode = stringVal;
                        break;
                    case CustomDimensionDefinition.RoomControlStationId:
                        target.RoomControlStationId = intVal;
                        break;
                    case CustomDimensionDefinition.RoomControlStationName:
                        target.RoomControlStationName = stringVal;
                        break;
                    case CustomDimensionDefinition.RoomControlTvRemoteAction:
                        target.RoomControlTvRemoteAction = stringVal;
                        break;
                    case CustomDimensionDefinition.MediaId:
                        target.MediaId = intVal;
                        break;
                    case CustomDimensionDefinition.OrderId:
                        target.OrderId = intVal;
                        break;
                    case CustomDimensionDefinition.OrderitemId:
                        target.OrderitemId = intVal;
                        break;
                    case CustomDimensionDefinition.TestPrefix:
                        target.TestPrefix = stringVal;
                        break;
                    case CustomDimensionDefinition.MessageResponse:
                        target.MessageResponse = stringVal;
                        break;
                    case CustomDimensionDefinition.ClientOperationMode:
                        target.ClientOperationMode = stringVal;
                        break;
                    case CustomDimensionDefinition.OrderType:
                        target.OrderType = stringVal;
                        break;
                    default:
                        throw new NotImplementedException(dimension.ToString());
                }
            } // each custom dimenion            
        }

        private int ParseInt(string value)
        {
            int toReturn;
            if (!int.TryParse(value, out toReturn))
                toReturn = -1;
            return toReturn;
        }

        private string ParseString(string value)
        {
            if (value.IsNullOrWhiteSpace() || value.Equals("N/A"))
                return null;
            else
                return value;
        }
    }
}
/*
 create table #t
(
  name nvarchar(128),
  rows varchar(50),
  reserved varchar(50),
  data varchar(50),
  index_size varchar(50),
  unused varchar(50)
)

declare @id nvarchar(128)
declare c cursor for
select name from sysobjects where xtype='U'

open c
fetch c into @id

while @@fetch_status = 0 begin

  insert into #t
  exec sp_spaceused @id

  fetch c into @id
end

close c
deallocate c

select * from #t
order by convert(int, substring(data, 1, len(data)-3)) desc

drop table #t
 * 
 */
