﻿namespace TempAnalyticsGatherer
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.cbTimeZone = new System.Windows.Forms.ComboBox();
            this.btClearKpisToRetrieve = new System.Windows.Forms.Button();
            this.lblExcelPath = new System.Windows.Forms.LinkLabel();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.cbCulture = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cbGaView = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cbGaProperty = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cbPerformancConsistencyChecks = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbKpis = new System.Windows.Forms.TextBox();
            this.tbOutput = new System.Windows.Forms.TextBox();
            this.cbGaAccount = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dtFrom = new System.Windows.Forms.DateTimePicker();
            this.dtTill = new System.Windows.Forms.DateTimePicker();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btTestBatches = new System.Windows.Forms.Button();
            this.btSubmitHit = new System.Windows.Forms.Button();
            this.tbConfiguratorLog = new System.Windows.Forms.TextBox();
            this.btFixAll = new System.Windows.Forms.Button();
            this.lblDimensionsValid = new System.Windows.Forms.Label();
            this.lblInvalidDimensions = new System.Windows.Forms.Label();
            this.btLoadAccounts = new System.Windows.Forms.Button();
            this.btFixProperty = new System.Windows.Forms.Button();
            this.btValidateProperty = new System.Windows.Forms.Button();
            this.cbProperty = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cbAccount = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btAnalytics = new System.Windows.Forms.Button();
            this.tabPage1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btAnalytics);
            this.tabPage1.Controls.Add(this.cbTimeZone);
            this.tabPage1.Controls.Add(this.btClearKpisToRetrieve);
            this.tabPage1.Controls.Add(this.lblExcelPath);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.cbCulture);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.cbGaView);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.cbGaProperty);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.cbPerformancConsistencyChecks);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.tbKpis);
            this.tabPage1.Controls.Add(this.tbOutput);
            this.tabPage1.Controls.Add(this.cbGaAccount);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.dtFrom);
            this.tabPage1.Controls.Add(this.dtTill);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1088, 768);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Analytics";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // cbTimeZone
            // 
            this.cbTimeZone.FormattingEnabled = true;
            this.cbTimeZone.Location = new System.Drawing.Point(602, 10);
            this.cbTimeZone.Name = "cbTimeZone";
            this.cbTimeZone.Size = new System.Drawing.Size(164, 21);
            this.cbTimeZone.TabIndex = 24;
            // 
            // btClearKpisToRetrieve
            // 
            this.btClearKpisToRetrieve.Location = new System.Drawing.Point(6, 111);
            this.btClearKpisToRetrieve.Name = "btClearKpisToRetrieve";
            this.btClearKpisToRetrieve.Size = new System.Drawing.Size(75, 23);
            this.btClearKpisToRetrieve.TabIndex = 23;
            this.btClearKpisToRetrieve.Text = "Clear";
            this.btClearKpisToRetrieve.UseVisualStyleBackColor = true;
            this.btClearKpisToRetrieve.Click += new System.EventHandler(this.btClearKpisToRetrieve_Click);
            // 
            // lblExcelPath
            // 
            this.lblExcelPath.AutoSize = true;
            this.lblExcelPath.Location = new System.Drawing.Point(85, 67);
            this.lblExcelPath.Name = "lblExcelPath";
            this.lblExcelPath.Size = new System.Drawing.Size(76, 13);
            this.lblExcelPath.TabIndex = 22;
            this.lblExcelPath.TabStop = true;
            this.lblExcelPath.Text = "None selected";
            this.lblExcelPath.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblExcelPath_LinkClicked);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 67);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(33, 13);
            this.label11.TabIndex = 20;
            this.label11.Text = "Excel";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(526, 13);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(70, 13);
            this.label10.TabIndex = 19;
            this.label10.Text = "In Time Zone";
            // 
            // cbCulture
            // 
            this.cbCulture.AccessibleRole = System.Windows.Forms.AccessibleRole.WhiteSpace;
            this.cbCulture.DisplayMember = "Name";
            this.cbCulture.FormattingEnabled = true;
            this.cbCulture.Items.AddRange(new object[] {
            "UK",
            "US",
            "NL"});
            this.cbCulture.Location = new System.Drawing.Point(843, 9);
            this.cbCulture.Name = "cbCulture";
            this.cbCulture.Size = new System.Drawing.Size(97, 21);
            this.cbCulture.TabIndex = 18;
            this.cbCulture.ValueMember = "Id";
            this.cbCulture.SelectedIndexChanged += new System.EventHandler(this.cbCulture_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(797, 13);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(40, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "Culture";
            // 
            // cbGaView
            // 
            this.cbGaView.AccessibleRole = System.Windows.Forms.AccessibleRole.WhiteSpace;
            this.cbGaView.DisplayMember = "Name";
            this.cbGaView.FormattingEnabled = true;
            this.cbGaView.Items.AddRange(new object[] {
            "Development",
            "Test",
            "Demo",
            "Production"});
            this.cbGaView.Location = new System.Drawing.Point(562, 36);
            this.cbGaView.Name = "cbGaView";
            this.cbGaView.Size = new System.Drawing.Size(204, 21);
            this.cbGaView.TabIndex = 15;
            this.cbGaView.ValueMember = "Id";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(526, 40);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(30, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "View";
            // 
            // cbGaProperty
            // 
            this.cbGaProperty.AccessibleRole = System.Windows.Forms.AccessibleRole.WhiteSpace;
            this.cbGaProperty.DisplayMember = "Name";
            this.cbGaProperty.FormattingEnabled = true;
            this.cbGaProperty.Items.AddRange(new object[] {
            "Development",
            "Test",
            "Demo",
            "Production"});
            this.cbGaProperty.Location = new System.Drawing.Point(337, 36);
            this.cbGaProperty.Name = "cbGaProperty";
            this.cbGaProperty.Size = new System.Drawing.Size(178, 21);
            this.cbGaProperty.TabIndex = 13;
            this.cbGaProperty.ValueMember = "Id";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(285, 40);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Property";
            // 
            // cbPerformancConsistencyChecks
            // 
            this.cbPerformancConsistencyChecks.AutoSize = true;
            this.cbPerformancConsistencyChecks.Location = new System.Drawing.Point(946, 36);
            this.cbPerformancConsistencyChecks.Name = "cbPerformancConsistencyChecks";
            this.cbPerformancConsistencyChecks.Size = new System.Drawing.Size(131, 17);
            this.cbPerformancConsistencyChecks.TabIndex = 11;
            this.cbPerformancConsistencyChecks.Text = "Consistency Checking";
            this.cbPerformancConsistencyChecks.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 42);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Analytics Account";
            // 
            // tbKpis
            // 
            this.tbKpis.Font = new System.Drawing.Font("Courier New", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.tbKpis.Location = new System.Drawing.Point(88, 92);
            this.tbKpis.Multiline = true;
            this.tbKpis.Name = "tbKpis";
            this.tbKpis.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbKpis.Size = new System.Drawing.Size(989, 245);
            this.tbKpis.TabIndex = 0;
            this.tbKpis.Text = resources.GetString("tbKpis.Text");
            this.tbKpis.WordWrap = false;
            // 
            // tbOutput
            // 
            this.tbOutput.Font = new System.Drawing.Font("Courier New", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.tbOutput.Location = new System.Drawing.Point(88, 343);
            this.tbOutput.Multiline = true;
            this.tbOutput.Name = "tbOutput";
            this.tbOutput.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbOutput.Size = new System.Drawing.Size(989, 349);
            this.tbOutput.TabIndex = 7;
            this.tbOutput.WordWrap = false;
            // 
            // cbGaAccount
            // 
            this.cbGaAccount.AccessibleRole = System.Windows.Forms.AccessibleRole.WhiteSpace;
            this.cbGaAccount.DisplayMember = "Name";
            this.cbGaAccount.FormattingEnabled = true;
            this.cbGaAccount.Items.AddRange(new object[] {
            "Development",
            "Test",
            "Demo",
            "Production"});
            this.cbGaAccount.Location = new System.Drawing.Point(109, 36);
            this.cbGaAccount.Name = "cbGaAccount";
            this.cbGaAccount.Size = new System.Drawing.Size(170, 21);
            this.cbGaAccount.TabIndex = 9;
            this.cbGaAccount.ValueMember = "Id";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 95);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "KPIs to retrieve";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(946, 7);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Go Gather!";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Timespan";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 291);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Output";
            // 
            // dtFrom
            // 
            this.dtFrom.Location = new System.Drawing.Point(109, 10);
            this.dtFrom.Name = "dtFrom";
            this.dtFrom.Size = new System.Drawing.Size(200, 20);
            this.dtFrom.TabIndex = 4;
            this.dtFrom.Value = new System.DateTime(2016, 8, 9, 0, 0, 0, 0);
            this.dtFrom.ValueChanged += new System.EventHandler(this.dtFrom_ValueChanged);
            // 
            // dtTill
            // 
            this.dtTill.Location = new System.Drawing.Point(315, 10);
            this.dtTill.Name = "dtTill";
            this.dtTill.Size = new System.Drawing.Size(200, 20);
            this.dtTill.TabIndex = 5;
            this.dtTill.Value = new System.DateTime(2016, 3, 5, 0, 0, 0, 0);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1096, 794);
            this.tabControl1.TabIndex = 11;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btTestBatches);
            this.tabPage2.Controls.Add(this.btSubmitHit);
            this.tabPage2.Controls.Add(this.tbConfiguratorLog);
            this.tabPage2.Controls.Add(this.btFixAll);
            this.tabPage2.Controls.Add(this.lblDimensionsValid);
            this.tabPage2.Controls.Add(this.lblInvalidDimensions);
            this.tabPage2.Controls.Add(this.btLoadAccounts);
            this.tabPage2.Controls.Add(this.btFixProperty);
            this.tabPage2.Controls.Add(this.btValidateProperty);
            this.tabPage2.Controls.Add(this.cbProperty);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.cbAccount);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1088, 768);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Configurator";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btTestBatches
            // 
            this.btTestBatches.Location = new System.Drawing.Point(170, 450);
            this.btTestBatches.Name = "btTestBatches";
            this.btTestBatches.Size = new System.Drawing.Size(75, 23);
            this.btTestBatches.TabIndex = 12;
            this.btTestBatches.Text = "button2";
            this.btTestBatches.UseVisualStyleBackColor = true;
            this.btTestBatches.Click += new System.EventHandler(this.btTestBatches_Click);
            // 
            // btSubmitHit
            // 
            this.btSubmitHit.Location = new System.Drawing.Point(89, 450);
            this.btSubmitHit.Name = "btSubmitHit";
            this.btSubmitHit.Size = new System.Drawing.Size(75, 23);
            this.btSubmitHit.TabIndex = 11;
            this.btSubmitHit.Text = "SubmitHit";
            this.btSubmitHit.UseVisualStyleBackColor = true;
            this.btSubmitHit.Click += new System.EventHandler(this.btSubmitHit_Click);
            // 
            // tbConfiguratorLog
            // 
            this.tbConfiguratorLog.Font = new System.Drawing.Font("Courier New", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.tbConfiguratorLog.Location = new System.Drawing.Point(8, 95);
            this.tbConfiguratorLog.Multiline = true;
            this.tbConfiguratorLog.Name = "tbConfiguratorLog";
            this.tbConfiguratorLog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbConfiguratorLog.Size = new System.Drawing.Size(1074, 349);
            this.tbConfiguratorLog.TabIndex = 10;
            this.tbConfiguratorLog.Visible = false;
            this.tbConfiguratorLog.WordWrap = false;
            // 
            // btFixAll
            // 
            this.btFixAll.Location = new System.Drawing.Point(8, 450);
            this.btFixAll.Name = "btFixAll";
            this.btFixAll.Size = new System.Drawing.Size(75, 23);
            this.btFixAll.TabIndex = 9;
            this.btFixAll.Text = "Fix All";
            this.btFixAll.UseVisualStyleBackColor = true;
            this.btFixAll.Visible = false;
            this.btFixAll.Click += new System.EventHandler(this.button2_Click);
            // 
            // lblDimensionsValid
            // 
            this.lblDimensionsValid.AutoSize = true;
            this.lblDimensionsValid.ForeColor = System.Drawing.Color.Green;
            this.lblDimensionsValid.Location = new System.Drawing.Point(422, 42);
            this.lblDimensionsValid.Name = "lblDimensionsValid";
            this.lblDimensionsValid.Size = new System.Drawing.Size(87, 13);
            this.lblDimensionsValid.TabIndex = 8;
            this.lblDimensionsValid.Text = "Dimensions Valid";
            this.lblDimensionsValid.Visible = false;
            // 
            // lblInvalidDimensions
            // 
            this.lblInvalidDimensions.AutoSize = true;
            this.lblInvalidDimensions.ForeColor = System.Drawing.Color.Red;
            this.lblInvalidDimensions.Location = new System.Drawing.Point(422, 42);
            this.lblInvalidDimensions.Name = "lblInvalidDimensions";
            this.lblInvalidDimensions.Size = new System.Drawing.Size(107, 13);
            this.lblInvalidDimensions.TabIndex = 7;
            this.lblInvalidDimensions.Text = "Dimensions Not Valid";
            this.lblInvalidDimensions.Visible = false;
            // 
            // btLoadAccounts
            // 
            this.btLoadAccounts.Location = new System.Drawing.Point(422, 12);
            this.btLoadAccounts.Name = "btLoadAccounts";
            this.btLoadAccounts.Size = new System.Drawing.Size(75, 23);
            this.btLoadAccounts.TabIndex = 6;
            this.btLoadAccounts.Text = "Load";
            this.btLoadAccounts.UseVisualStyleBackColor = true;
            this.btLoadAccounts.Click += new System.EventHandler(this.btLoadAccounts_Click);
            // 
            // btFixProperty
            // 
            this.btFixProperty.Location = new System.Drawing.Point(185, 66);
            this.btFixProperty.Name = "btFixProperty";
            this.btFixProperty.Size = new System.Drawing.Size(75, 23);
            this.btFixProperty.TabIndex = 5;
            this.btFixProperty.Text = "Fix";
            this.btFixProperty.UseVisualStyleBackColor = true;
            this.btFixProperty.Click += new System.EventHandler(this.btFixProperty_Click);
            // 
            // btValidateProperty
            // 
            this.btValidateProperty.Location = new System.Drawing.Point(104, 66);
            this.btValidateProperty.Name = "btValidateProperty";
            this.btValidateProperty.Size = new System.Drawing.Size(75, 23);
            this.btValidateProperty.TabIndex = 4;
            this.btValidateProperty.Text = "Validate";
            this.btValidateProperty.UseVisualStyleBackColor = true;
            this.btValidateProperty.Click += new System.EventHandler(this.btValidateProperty_Click);
            // 
            // cbProperty
            // 
            this.cbProperty.Enabled = false;
            this.cbProperty.FormattingEnabled = true;
            this.cbProperty.Location = new System.Drawing.Point(104, 39);
            this.cbProperty.Name = "cbProperty";
            this.cbProperty.Size = new System.Drawing.Size(312, 21);
            this.cbProperty.TabIndex = 3;
            this.cbProperty.SelectedIndexChanged += new System.EventHandler(this.cbProperty_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(17, 42);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Property";
            // 
            // cbAccount
            // 
            this.cbAccount.Enabled = false;
            this.cbAccount.FormattingEnabled = true;
            this.cbAccount.Location = new System.Drawing.Point(104, 12);
            this.cbAccount.Name = "cbAccount";
            this.cbAccount.Size = new System.Drawing.Size(312, 21);
            this.cbAccount.TabIndex = 1;
            this.cbAccount.SelectedValueChanged += new System.EventHandler(this.cbAccount_SelectedValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(17, 15);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Account";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // btAnalytics
            // 
            this.btAnalytics.Location = new System.Drawing.Point(946, 707);
            this.btAnalytics.Name = "btAnalytics";
            this.btAnalytics.Size = new System.Drawing.Size(131, 23);
            this.btAnalytics.TabIndex = 25;
            this.btAnalytics.Text = "Analytics!";
            this.btAnalytics.UseVisualStyleBackColor = true;
            this.btAnalytics.Click += new System.EventHandler(this.btAnalytics_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1096, 794);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Temp Analytics Gatherer";
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbKpis;
        private System.Windows.Forms.TextBox tbOutput;
        private System.Windows.Forms.ComboBox cbGaAccount;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dtFrom;
        private System.Windows.Forms.DateTimePicker dtTill;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ComboBox cbAccount;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbProperty;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btFixProperty;
        private System.Windows.Forms.Button btValidateProperty;
        private System.Windows.Forms.Button btLoadAccounts;
        private System.Windows.Forms.Label lblInvalidDimensions;
        private System.Windows.Forms.Label lblDimensionsValid;
        private System.Windows.Forms.CheckBox cbPerformancConsistencyChecks;
        private System.Windows.Forms.TextBox tbConfiguratorLog;
        private System.Windows.Forms.Button btFixAll;
        private System.Windows.Forms.ComboBox cbGaView;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cbGaProperty;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cbCulture;
        private System.Windows.Forms.LinkLabel lblExcelPath;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button btSubmitHit;
        private System.Windows.Forms.Button btClearKpisToRetrieve;
        private System.Windows.Forms.ComboBox cbTimeZone;
        private System.Windows.Forms.Button btTestBatches;
        private System.Windows.Forms.Button btAnalytics;
    }
}

