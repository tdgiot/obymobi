﻿using Dionysos;
using Obymobi.Analytics;
using Obymobi.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Reflection;
using System.Data.SqlClient;
using System.Collections.Specialized;
using System.Security.Principal;
using Obymobi.Data.EntityClasses;
using Google.Apis.Analytics.v3.Data;
using Obymobi.Analytics.Data;
using Obymobi.Analytics.Google;
using Obymobi.Analytics.Reports;
using Obymobi.Analytics.KpiFunctions;
using System.Globalization;
using System.Threading;
using System.IO;
using Newtonsoft.Json;
using Obymobi.Analytics.Google.Recording;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using System.Diagnostics;
//using Obymobi.Analytics.Google.Bigquery;

namespace TempAnalyticsGatherer
{
    public partial class Form1 : Form
    {
        private const string AppSettingConnectionString = "ObymobiData.ConnectionString";
        private const string AppSettingGoogleToken = "GoogleApisJsonWebTokenBase64";

        public Form1()
        {
            InitializeComponent();
            this.Focus();            

            this.tbConfiguratorLog.Visible = TestUtil.IsPcGabriel;
            this.btFixAll.Visible = TestUtil.IsPcGabriel;                            
            this.btSubmitHit.Visible = TestUtil.IsPcGabriel;

            this.dtFrom.Value = DateTimeUtil.GetNextWeekday(DateTime.Now, DayOfWeek.Monday).AddDays(-14);
            this.dtTill.Value = DateTimeUtil.GetNextWeekday(this.dtFrom.Value, DayOfWeek.Sunday);

            var timezones = new TimeZoneCollection();
            timezones.GetMulti(null, 0, new SortExpression(TimeZoneFields.Name | SortOperator.Ascending));

            // Make Europe, Vegas & No transformation top 3
            var utcZone = new TimeZoneEntity { Name = "Don't apply", NameDotNet = TimeZoneInfo.Utc.DisplayName };
            timezones.Insert(0, utcZone);

            var toMoveToTheTop = timezones.FirstOrDefault(x => x.Name.Equals("Europe/London"));
            if (toMoveToTheTop != null)
            {
                timezones.Remove(toMoveToTheTop);
                timezones.Insert(0, toMoveToTheTop);
            }

            toMoveToTheTop = timezones.FirstOrDefault(x => x.Name.Equals("US/Pacific"));
            if (toMoveToTheTop != null)
            {
                timezones.Remove(toMoveToTheTop);
                timezones.Insert(0, toMoveToTheTop);
            }



            this.cbTimeZone.ValueMember = "NameDotNet";
            this.cbTimeZone.DisplayMember = "Name";
            this.cbTimeZone.DataSource = timezones;

            if (TestUtil.IsPcGabriel && false)
            {
                StringBuilder sbKpis = new StringBuilder();
                //sbKpis.AppendLine("OrderedQuantity(CompanyIds[343]; CategoryIds[]; ProductIds[139924,82478,146186,146264]; OrderTypes[]; FriendlyName[Ordered Product Count for 4 Products])");
                //sbKpis.AppendLine("OrderedQuantity(CompanyIds[343]; CategoryIds[]; ExcludeProductIds[139924,82478,146186,146264]; FriendlyName[Ordered Product Count for all but 4 Products])");
                //sbKpis.AppendLine("OrderedQuantity(CompanyIds[343]; CategoryIds[]; OrderTypes[]; FriendlyName[Ordered Product Count for all Products])");
                sbKpis.AppendLine("OrderedQuantity(CompanyIds[343]; CategoryIds[24529,24530,24531,24532];)");
                sbKpis.AppendLine("OrderedQuantity(CompanyIds[343]; CategoryIds[0,24525];)");
                sbKpis.AppendLine("OrderedQuantity(CompanyIds[343]; ExcludeCategoryIds[24529,24530,24531,24532];)");
                sbKpis.AppendLine("OrderedQuantity(CompanyIds[343]; ExcludeCategoryIds[0,24525];)");
                sbKpis.AppendLine("OrderedQuantity(CompanyIds[343];)");
                this.tbKpis.Text = sbKpis.ToString();
            }
            else if (TestUtil.IsPcGabriel && false)
            {
                StringBuilder sbKpis = new StringBuilder();
                sbKpis.AppendLine("ScreenViewCount(CompanyIds[343]; SiteIds[]; PageIds[]; TabIds[]; ScreenNames[Aria]; CategoryIds[]; ProductIds[]; FriendlyName[All Aria Pages Views])");
                sbKpis.AppendLine("EventCount(CompanyIds[343]; EventCategories[]; EventActions[Added first product to basket]; EventLabels[]; SiteIds[]; PageIds[]; TabIds[]; ScreenNames[]; CategoryIds[]; ProductIds[]; FriendlyName[People adding a first product to the basket])");
                this.tbKpis.Text = sbKpis.ToString();
            }
            else if (TestUtil.IsPcGabriel && false)
            {
                StringBuilder sbKpis = new StringBuilder();
                sbKpis.AppendLine("TopXEventCount(CompanyIds[]; Dimensions[eventCategory, eventAction])");
                sbKpis.AppendLine("TopXEventCount(PrimaryKeys[C-343]; Dimensions[eventCategory, eventAction, CompanyName, PrimaryKeys])");
                sbKpis.AppendLine("TopXEventCount(ExcludePrimaryKeys[C-343]; Dimensions[eventCategory, eventAction, CompanyName, PrimaryKeys])");
                sbKpis.AppendLine("TopXScreenCount(ScreenNames[Breakfast]; Dimensions[screenName, DeliverypointGroupName, PrimaryKeys]; NoOfResults[5])");
                sbKpis.AppendLine("TopXScreenCount(ScreenNames[Breakfast]; ExcludeScreenNames[web]; Dimensions[screenName, DeliverypointGroupName, PrimaryKeys]; NoOfResults[5])");
                this.tbKpis.Text = sbKpis.ToString();
            }
            else if (TestUtil.IsPcGabriel)
            {
                StringBuilder sbKpis = new StringBuilder();
                sbKpis.AppendLine("DailyOrderedQuantity(CompanyIds[343]; CategoryIds[24529,24530,24531,24532];)");
                sbKpis.AppendLine("DailyOrderedQuantity(CompanyIds[343]; CategoryIds[0,24525];)");
                sbKpis.AppendLine("OrderedQuantity(CompanyIds[343]; CategoryIds[24529,24530,24531,24532];)");
                sbKpis.AppendLine("OrderedQuantity(CompanyIds[343]; CategoryIds[0,24525];)");
                this.tbKpis.Text = sbKpis.ToString();
                this.dtFrom.Value = new DateTime(2016, 9, 10);
                this.dtTill.Value = new DateTime(2016, 9, 12);
            }
            else if (TestUtil.IsPcGabriel)
            {
                StringBuilder sbKpis = new StringBuilder();
                sbKpis.AppendLine("StartDate(CompanyIds[343]; CategoryIds[24529,24530,24531,24532];)");
                sbKpis.AppendLine("StartDate(CompanyIds[343]; CategoryIds[0,24525];)");
                //sbKpis.AppendLine("DailyOrderedQuantity(CompanyIds[343]; CategoryIds[24529,24530,24531,24532];)");
                //sbKpis.AppendLine("DailyOrderedQuantity(CompanyIds[343]; CategoryIds[0,24525];)");
                //sbKpis.AppendLine("OrderedQuantity(CompanyIds[343]; CategoryIds[24529,24530,24531,24532];)");
                //sbKpis.AppendLine("OrderedQuantity(CompanyIds[343]; CategoryIds[0,24525];)");
                this.tbKpis.Text = sbKpis.ToString();
                this.dtFrom.Value = new DateTime(2016, 9, 10);
                this.dtTill.Value = new DateTime(2016, 9, 12);
            }

            this.btAnalytics.Visible = TestUtil.IsPcGabriel;

            this.SetupGui();

            //string json;
            //try
            //{
            //    json = Encoding.UTF8.GetString(Convert.FromBase64String(System.Configuration.ConfigurationManager.AppSettings.Get("GoogleApisJsonWebTokenBase64")));
            //}
            //catch (Exception ex)
            //{
            //    throw new Exception("Non valid Json Web Token in AppSettings[\"GoogleApisJsonWebTokenBase64\"]", ex);
            //}
        }

        private async void SetupGui()
        {
            //this.cbCulture.SelectedIndex = 1;

            // Load Initial Google Configuration
            this.cbGaAccount.DataSource = await this.GetManagementApiHelper().GetAccountsAsync();

            // Try to find Crave Interactive Production
            for (int i = 0; i < this.cbGaAccount.Items.Count; i++)
            {
                var account = (Account)this.cbGaAccount.Items[i];
                if (account.Name.Equals("Crave Interactive Production"))
                {
                    this.cbGaAccount.SelectedIndex = i;
                    this.cbGaProperty.DataSource = await this.GetManagementApiHelper().GetPropertiesAsync(account);

                    for (int j = 0; j < this.cbGaProperty.Items.Count; j++)
                    {
                        var property = (Webproperty)this.cbGaProperty.Items[j];
                        if (property.Name.Equals("Crave World Live"))
                        {
                            this.cbGaProperty.SelectedIndex = j;
                            this.cbGaView.DataSource = await this.GetManagementApiHelper().GetViewsAsync(property);

                            for (int k = 0; k < this.cbGaView.Items.Count; k++)
                            {
                                var view = (Profile)this.cbGaView.Items[k];                                
                                if (view.Name.Equals("All Mobile App Data"))
                                    this.cbGaView.SelectedIndex = k;                                
                            }
                        }
                    }
                }
            }

            this.cbGaAccount.SelectedIndexChanged += CbGaAccount_SelectedIndexChanged;
            this.cbGaProperty.SelectedIndexChanged += CbGaProperty_SelectedIndexChanged;
            //this.cbGaView.SelectedIndexChanged += CbGaView_SelectedIndexChanged;
        }

        private async void CbGaProperty_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.cbGaView.DataSource = null;
            this.cbGaView.DisplayMember = "Name";
            this.cbGaView.ValueMember = "Id";
            this.cbGaView.DataSource = await this.GetManagementApiHelper().GetViewsAsync((Webproperty)this.cbGaProperty.SelectedItem);
        }

        private async void CbGaAccount_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.cbProperty.DataSource = null;
            this.cbGaView.DataSource = null;
            this.cbGaProperty.DataSource = await this.GetManagementApiHelper().GetPropertiesAsync((Account)this.cbGaAccount.SelectedItem);
        }

        public async void GoGather()
        {
            this.tbOutput.Clear();
            this.Enabled = false;
            this.UseWaitCursor = true;                        
            
            DateTime fromTimeZoned = this.dtFrom.Value.MakeBeginOfDay();
            DateTime tillTimeZoned = this.dtTill.Value.MakeEndOfDay();

            var timeZone = TimeZoneInfo.FindSystemTimeZoneById(this.cbTimeZone.SelectedValue.ToString());

            var fromUtc = TimeZoneInfo.ConvertTimeToUtc(fromTimeZoned, timeZone);
            var tillUtc = TimeZoneInfo.ConvertTimeToUtc(tillTimeZoned, timeZone);
            int utcOffset = Convert.ToInt32((fromTimeZoned - fromUtc).TotalHours);

            this.tbOutput.AppendText(string.Format("Selection Period for Database (UTC): {0} - {1} (Target Timezone offset: {2}) \r\n", fromUtc, tillUtc, utcOffset));

            // Excel Processing
            if (File.Exists(this.lblExcelPath.Text))
            {
                this.tbOutput.AppendText("Excel is being processed - this can take a while. Have a coffee! Or watch some Cat movies.\r\n");
                this.GoGatherUsingExcel(fromUtc, tillUtc, utcOffset);
                this.tbOutput.AppendText("Excel is done.\r\n");

                this.Enabled = true;
                this.UseWaitCursor = false;

                return;
            }

            ChildCategoryFetcher categoryFetcher = new ChildCategoryFetcher();

            // Transaction
            Obymobi.Analytics.Data.TransactionsKpiRetriever transactionKpiRetriever = new TransactionsKpiRetriever(this.cbPerformancConsistencyChecks.Checked);
            Obymobi.Analytics.Data.KpiFunctionProcessor transactionKpiProcessor = new Obymobi.Analytics.Data.KpiFunctionProcessor(transactionKpiRetriever, categoryFetcher);

            // Google
            var googleAnalyticsKpiRetriever = new GoogleAnalyticsKpiRetriever(this.GetGoogleJwt(), ((Profile)this.cbGaView.SelectedItem).Id);
            var googleKpiProcessor = new Obymobi.Analytics.Google.GoogleAnalyticsKpiFunctionProcessor(googleAnalyticsKpiRetriever, categoryFetcher);

            //var bigQueryKpiRetriever = new BigqueryKpiRetriever(this.GetGoogleJwt(), "crave-interactive-live", "79296192.ga_sessions_");
            //var bigQueryKpiProcessor = new Obymobi.Analytics.Google.GoogleAnalyticsKpiFunctionProcessor(

            // Append both processors
            var processors = new List<IKpiFunctionProcessor>();
            processors.Add(transactionKpiProcessor);
            processors.Add(googleKpiProcessor);

            KpiFunctionHelper functionHelper = new KpiFunctionHelper(processors);            
            var kpis = this.tbKpis.Lines;

            foreach (var kpi in kpis)
            {
                if (kpi.IsNullOrWhiteSpace())
                    continue;

                try
                {
                    this.tbOutput.AppendText(kpi + "\r\n");
                    string actualFilter;
                    
                    List<object[]> results = functionHelper.GetValue(kpi, fromUtc, tillUtc, fromTimeZoned, tillTimeZoned, out actualFilter);
                    foreach (var result in results)
                    {
                        List<string> cultured = new List<string>();
                        for (int i = 0; i < result.Length; i++)
                        {
                            if (utcOffset != 0 && result[i] is DateTime)
                            {
                                DateTime offsetted = ((DateTime)result[i]).AddHours(utcOffset);
                                cultured.Add(string.Format("{0}", offsetted)); // Apply culturing)
                            }
                            else
                                cultured.Add(string.Format("{0}", result[i])); // Apply culturing
                        }                                                
                        this.tbOutput.AppendText(string.Join("|", cultured) + "\r\n");
                    }

                    this.tbOutput.AppendText(actualFilter + "\r\n");
                }
                catch (Exception ex)
                {                    
                    this.tbOutput.AppendText("Failure:" + ex.GetAllMessages(false) + "\r\n");
                }
                finally
                {
                    this.tbOutput.AppendText("----------------\r\n");
                }
            }

            this.Enabled = true;
            this.UseWaitCursor = false;
        }

        private void GoGatherUsingExcel(DateTime from, DateTime till, int utcOffset)
        {
            var metadata = new Obymobi.Analytics.Reports.KpiReportMetadata();            
            metadata.IncludeActualFilters = false;
            metadata.IncludeStackTracesWithErrors = true;
            metadata.UtcOffset = utcOffset;
            metadata.GoogleAnalyticsViewId = ((Profile)this.cbGaView.SelectedItem).Id;

            ReportProcessingTaskEntity task = new ReportProcessingTaskEntity();
            task.FromUTC = from;
            task.TillUTC = till;            
            task.CompanyId = -1;
            task.AnalyticsReportType = AnalyticsReportType.Kpi;
            task.Filter = JsonConvert.SerializeObject(metadata);

            ReportProcessingTaskTemplateEntity template = new ReportProcessingTaskTemplateEntity();

            // Read while it's open!
            using (FileStream fs = File.Open(this.lblExcelPath.Text, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            using (MemoryStream ms = new MemoryStream())
            {
                fs.CopyTo(ms);
                template.TemplateFile = ms.ToArray();
            }

            //template.TemplateFile = File.ReadAllBytes(this.lblExcelPath.Text);
            // We don't want to go to this collection via DB.
            task.AlreadyFetchedReportProcessingTaskTemplateCollection = true;
            task.AlwaysFetchReportProcessingTaskTemplateCollection = false;
            task.ReportProcessingTaskTemplateCollection.Add(template);

            var kpireport = new Obymobi.Analytics.Reports.KpiReport(task, this.GetGoogleJwt());

            // Direct processing
            var output = kpireport.RunReport(false);

            // String Path
            FileInfo fi = new FileInfo(this.lblExcelPath.Text);
            string newFileName = string.Format("{0}\\{1}-{2}.xlsx", fi.Directory, Path.GetFileNameWithoutExtension(this.lblExcelPath.Text), DateTime.Now.Ticks);
            output.SaveAs(newFileName);

            System.Diagnostics.Process.Start(newFileName);
        }

        private static void AddValuesToList(string values, List<int> list)
        {
            if (values.Length == 0)
                return;

            list.AddRange(values.Split(new char[] { ',' }).Select(x => Convert.ToInt32(x)));
        }

        private static void AddValuesToList(string values, List<string> list)
        {
            if (values.Length == 0)
                return;

            list.AddRange(values.Split(new char[] { ',' }).Select(x => x));
        }

        private static void AddValuesToList(string values, List<OrderType> list)
        {
            if (values.Length == 0)
                return;

            list.AddRange(values.Split(new char[] { ',' }).Select(x => (OrderType)Enum.Parse(typeof(OrderType), x))); // Don't you love one-liners?
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.GoGather();
        }

        private void UpdateAppSetting(string key, string value)
        {
            Configuration configuration = System.Configuration.ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().Location);
            configuration.AppSettings.Settings[key].Value = value;
            configuration.Save();

            System.Configuration.ConfigurationManager.RefreshSection("appSettings");
        }

        public static bool IsAdministrator()
        {
            var identity = WindowsIdentity.GetCurrent();
            var principal = new WindowsPrincipal(identity);
            return principal.IsInRole(WindowsBuiltInRole.Administrator);
        }

        private async void btLoadAccounts_Click(object sender, EventArgs e)
        {
            var accounts = await this.GetManagementApiHelper().GetAccountsAsync();
            this.cbAccount.DisplayMember = "Name";
            this.cbAccount.ValueMember = "Id";
            this.cbAccount.DataSource = accounts;
            this.cbAccount.Enabled = true;
        }

        private ManagementApiHelper managementApiHelper = null;
        private ManagementApiHelper GetManagementApiHelper()
        {
            if (this.managementApiHelper == null)
                this.managementApiHelper = new ManagementApiHelper(this.GetGoogleJwt());

            return this.managementApiHelper;
        }

        private string GetGoogleJwt()
        {
            string json;
            try
            {
                json = Encoding.UTF8.GetString(Convert.FromBase64String(System.Configuration.ConfigurationManager.AppSettings.Get(AppSettingGoogleToken)));
            }
            catch (Exception ex)
            {
                throw new Exception("Non valid Json Web Token in AppSettings[\"GoogleApisJsonWebTokenBase64\"]", ex);
            }
            return json;
        }

        private async void cbAccount_SelectedValueChanged(object sender, EventArgs e)
        {
            if (this.cbAccount.SelectedValue == null)
            {
                this.cbProperty.DataSource = null;
                this.cbProperty.Items.Clear();
                return;
            }

            this.cbAccount.Enabled = false;
            this.cbProperty.Enabled = false;
            this.btValidateProperty.Enabled = false;
            this.btFixProperty.Enabled = false;

            var properties = await this.GetManagementApiHelper().GetPropertiesAsync((Account)this.cbAccount.SelectedItem);
            this.cbProperty.DisplayMember = "Name";
            this.cbProperty.ValueMember = "Id";
            this.cbProperty.DataSource = properties;
            this.lblInvalidDimensions.Visible = false;
            this.lblDimensionsValid.Visible = false;

            this.cbAccount.Enabled = true;
            this.cbProperty.Enabled = true;
            this.btValidateProperty.Enabled = true;
            this.btFixProperty.Enabled = true;
        }

        private void btValidateProperty_Click(object sender, EventArgs e)
        {
            this.ValidateCustomDimensions(false);
        }

        private void btFixProperty_Click(object sender, EventArgs e)
        {
            this.ValidateCustomDimensions(true);
        }

        private async void ValidateCustomDimensions(bool fix)
        {
            this.cbAccount.Enabled = false;
            this.cbProperty.Enabled = false;
            this.btValidateProperty.Enabled = false;
            this.btFixProperty.Enabled = false;
            this.btLoadAccounts.Enabled = false;

            var success = await this.GetManagementApiHelper().ValidateCustomDimensions((Account)this.cbAccount.SelectedItem, (Webproperty)this.cbProperty.SelectedItem, fix);

            if (!fix)
            {
                this.lblInvalidDimensions.Visible = !success;
                this.lblDimensionsValid.Visible = success;
            }
            else
            {
                this.lblDimensionsValid.Visible = true;
                this.lblInvalidDimensions.Visible = false;
            }

            this.cbAccount.Enabled = true;
            this.cbProperty.Enabled = true;
            this.btValidateProperty.Enabled = true;
            this.btFixProperty.Enabled = true;
            this.btLoadAccounts.Enabled = true;
        }

        private void cbProperty_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.lblDimensionsValid.Visible = false;
            this.lblInvalidDimensions.Visible = false;
        }

        private async void button2_Click(object sender, EventArgs e)
        {
            // https://developers.google.com/analytics/devguides/config/mgmt/v3/limits-quotas#general_quota_limits
            this.tbConfiguratorLog.AppendText("\r\nLoad the accounts");
            var accounts = await this.GetManagementApiHelper().GetAccountsAsync();
            this.tbConfiguratorLog.AppendText("\r\nLoaded Accounts: " + accounts.Count);
            foreach(var account in accounts)
            {
                if (account.Name != "Crave Interactive Production")
                    continue;

                var properties = await this.GetManagementApiHelper().GetPropertiesAsync(account);
                this.tbConfiguratorLog.AppendText("\r\nLoaded Properties: " + properties.Count);
                foreach (var property in properties)
                {
                    this.tbConfiguratorLog.AppendText("\r\nValidating /fixing: " + property.Name);
                    var valid = await this.GetManagementApiHelper().ValidateCustomDimensions(account, property, false);
                    if (valid)
                    {
                        this.tbConfiguratorLog.AppendText("\r\nVALID: " + property.Name);
                    }
                    else
                    {
                        this.tbConfiguratorLog.AppendText("\r\nNOW FIXING: " + property.Name);
                        try
                        {
                            valid = await this.GetManagementApiHelper().ValidateCustomDimensions(account, property, true);
                            this.tbConfiguratorLog.AppendText("\r\nVALID: " + property.Name);
                        }
                        catch (Exception ex)
                        {
                            this.tbConfiguratorLog.AppendText("\r\nFailed to fix: " + property.Name + " - " + ex.Message);
                        }
                        
                    }
                }
                
            }

            this.tbConfiguratorLog.AppendText("\r\nDone - Sleep 90s.");
            Thread.Sleep(90);
            
        }

        private void cbCulture_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.cbCulture.SelectedIndex == 1)
            {
                // US
                CultureInfo.DefaultThreadCurrentCulture = CultureInfo.GetCultureInfo("en-US");
            }
            if (this.cbCulture.SelectedIndex == 2)
            {
                // US
                CultureInfo.DefaultThreadCurrentCulture = CultureInfo.GetCultureInfo("nl-NL");
            }
            else
            {
                // UK
                CultureInfo.DefaultThreadCurrentCulture = CultureInfo.GetCultureInfo("en-UK");
            }
        }

        private void lblExcelPath_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            int size = -1;
            openFileDialog1.CheckFileExists = true;
            openFileDialog1.DefaultExt = "xlsx";
            DialogResult result = openFileDialog1.ShowDialog(); // Show the dialog.
            if (result == DialogResult.OK) // Test result.
            {
                this.lblExcelPath.Text = openFileDialog1.FileName;
            }
            else
            {
                this.lblExcelPath.Text = "None selected";
            }
        }

        private void btSubmitHit_Click(object sender, EventArgs e)
        {
            Hit h = new Hit(HitType.Event);
            h.AnalyticsClientId = "ClientIdGatherer." + TestUtil.MachineName;            
            h.EventCategory = EventCategory.Technical;
            h.EventAction = "Test Hit From Gatherer";
            h.EventLabel = DateTime.Now.ToString();            
            h.UserId = h.AnalyticsClientId + Environment.UserName;
            h.HitVersion = "1.0";

            AsyncHitRecorder.RecordHit(h, ((Webproperty)this.cbGaProperty.SelectedItem).Id);
        }

        private void btClearKpisToRetrieve_Click(object sender, EventArgs e)
        {
            this.tbKpis.Text = string.Empty;                 
        }

        private void dtFrom_ValueChanged(object sender, EventArgs e)
        {
            if (this.dtFrom.Value > this.dtTill.Value)
                this.dtTill.Value = this.dtFrom.Value;
        }

        private void btTestBatches_Click(object sender, EventArgs e)
        {
            if (TestUtil.IsPcGabriel)
            {
                var categoryFetcher = new ChildCategoryFetcher();
                var googleAnalyticsKpiRetriever = new GoogleAnalyticsKpiRetriever(this.GetGoogleJwt(), ((Profile)this.cbGaView.SelectedItem).Id);
                var bigQueryKpiRetriever = new BigqueryKpiRetriever(this.GetGoogleJwt(), "crave-interactive-live", "crave-interactive-live:79296192.ga_sessions_", false);
                
                var googleKpiProcessor = new Obymobi.Analytics.Google.GoogleAnalyticsKpiFunctionProcessor(googleAnalyticsKpiRetriever, categoryFetcher);
                KpiFunctionHelper kpiFunctionHelper = new KpiFunctionHelper(new List<IKpiFunctionProcessor> { googleKpiProcessor });
                var fromUtc = this.dtFrom.Value.MakeBeginOfDay();
                var tillUtc = this.dtTill.Value.MakeEndOfDay();
                var intervalType = ReportingInterval.None; // None == 1 interval of all.

                List<string> kpiFunctions = new List<string>();
                kpiFunctions.Add("EventCount(EventActions[Company tab viewed, Product viewed, Category Details Shown, Attachment viewed, Native page viewed, Entertainment viewed]; CompanyIds[343]);");
                kpiFunctions.Add("EventCount(CompanyIds[343]; EventCategories[Room Controls];)");
                kpiFunctions.Add("EventCount(CompanyIds[343]; EventCategories[Widgets]; EventActions[Scene clicked]; EventLabels[Scene: 'All Off'])");
                kpiFunctions.Add("EventCount(CompanyIds[343]; EventCategories[Features]; EventActions[Estimated delivery time viewed];)");
                kpiFunctions.Add("EventCount(CompanyIds[343]; EventCategories[Features]; EventActions[Disable privacy dialog shown];)");
                kpiFunctions.Add("EventCount(EventActions[Section Viewed, Area changed]; CompanyIds[343]);");
                kpiFunctions.Add("EventCount(EventActions[Product Viewed, Category Details shown]; CategoryIds[0, 22586]; CompanyIds[343])");
                kpiFunctions.Add("EventCount(EventActions[Company tab viewed]; EventLabels[Home, Cart]; CompanyIds[343])");
                kpiFunctions.Add("EventCount(CompanyIds[343]; EventCategories[Widgets]; EventActions[Link clicked, Scene clicked];)");
                kpiFunctions.Add("EventCount(CompanyIds[343]; EventCategories[Advertising]; EventActions[Advertisement clicked];)");
                kpiFunctions.Add("EventCount(CompanyIds[343]; EventCategories[Room Controls]; EventActions[Wake up timer changed]; EventLabels[Enabled])");
                kpiFunctions.Add("EventCount(CompanyIds[343]; EventCategories[Room Controls]; EventActions[Wake up timer changed]; EventLabels[Disabled])");
                kpiFunctions.Add("EventCount(CompanyIds[343]; EventCategories[Room Controls]; EventActions[Wake up timer ran];)");
                kpiFunctions.Add("EventCount(EventActions[Company tab viewed]; CompanyIds[343])");
                kpiFunctions.Add("EventCount(EventActions[Product Viewed, Category Details Shown]; CompanyIds[343])");
                kpiFunctions.Add("EventCount(EventActions[Section Viewed, Area changed]; CompanyIds[343])");
                kpiFunctions.Add("EventCount(EventActions[Advertisement viewed]; CompanyIds[343])");
                kpiFunctions.Add("EventCount(EventActions[Attachment viewed]; CompanyIds[343])");
                kpiFunctions.Add("EventCount(EventActions[Message viewed]; CompanyIds[343])");
                kpiFunctions.Add("EventCount(EventActions[Browser view]; CompanyIds[343])");
                kpiFunctions.Add("EventCount(EventActions[Native page viewed]; CompanyIds[343]);");
                kpiFunctions.Add("EventCount(EventActions[Entertainment viewed]; CompanyIds[343]);");
                kpiFunctions.Add("EventCount(EventActions[Company tab viewed, Product viewed, Category Details Shown, Section Viewed, Area changed, Advertisement viewed, Attachment viewed, Message viewed, Browser view, Native page viewed, Entertainment viewed]; CompanyIds[343]);");

                this.tbOutput.Clear();
                foreach (var function in kpiFunctions)
                {
                    this.tbOutput.AppendText(function + "\n");
                    var filter = kpiFunctionHelper.GetFilter(function,fromUtc,tillUtc) as Obymobi.Analytics.Google.Filter;
                    List<Task> tasks = new List<Task>();

                    Stopwatch sw = new Stopwatch();
                    sw.Start();
                    var count = googleAnalyticsKpiRetriever.GetEventCount(filter);
                    sw.Stop();
                    this.tbOutput.AppendText("Google Analytics Result: {0:n} in {1:n} milliseconds\n".FormatSafe(count, sw.ElapsedMilliseconds));

                    sw.Reset();
                    sw.Start();
                    count = bigQueryKpiRetriever.GetEventCount(filter);
                    sw.Stop();
                    this.tbOutput.AppendText("Bigquery Result: {0:n} in {1:n} milliseconds\n".FormatSafe(count, sw.ElapsedMilliseconds));

                    this.tbOutput.AppendText("-----------------------------------\n");
                }                

                //var fEvents = new Obymobi.Analytics.Google.Filter();
                //fEvents.CompanyIds.Add(343);
                //fEvents.FromUtc = new DateTime(2016, 9, 10).MakeBeginOfDay();
                //fEvents.TillUtc = new DateTime(2016, 9, 12).MakeEndOfDay();
                //fEvents.EventCategories.Add("Room Controls");
                //int eventCount = retriever.GetEventCount(fEvents);

                //fEvents = new Obymobi.Analytics.Google.Filter();
                //fEvents.CompanyIds.Add(343);
                //fEvents.FromUtc = new DateTime(2016, 9, 10).MakeBeginOfDay();
                //fEvents.TillUtc = new DateTime(2016, 9, 10).MakeEndOfDay();
                //fEvents.EventCategories.Add("Room Controls");
                //eventCount = retriever.GetEventCount(fEvents);

                //fEvents = new Obymobi.Analytics.Google.Filter();
                //fEvents.CompanyIds.Add(343);
                //fEvents.FromUtc = new DateTime(2016, 9, 10).MakeBeginOfDay();
                //fEvents.TillUtc = new DateTime(2016, 9, 12).MakeEndOfDay();
                //fEvents.EventCategories.Add("Room Controls");
                //eventCount = retriever.GetEventCount(fEvents);

                // TransactionsKpiRetriever retriever = new TransactionsKpiRetriever(false);

                // Get Aria Revenue for February & March in different intervals (not taking in account Timezones)

                //// Transactions Examples
                //DateTime from = new DateTime(2016, 2, 1);
                //DateTime till = new DateTime(2016, 4, 1);

                //TransactionsKpiRetriever.Filter filter = new TransactionsKpiRetriever.Filter();
                //filter.FromUtc = from;
                //filter.TillUtc = till;
                //filter.CompanyIds.Add(343);

                //List<ReportingInterval> intervals = new List<ReportingInterval>();
                //intervals.Add(ReportingInterval.Daily);
                //intervals.Add(ReportingInterval.Weekly);
                //intervals.Add(ReportingInterval.Monthly);
                //intervals.Add(ReportingInterval.None);

                //foreach (var interval in intervals)
                //{
                //    Stopwatch sw = new Stopwatch();
                //    sw.Start();                    
                //    var results = retriever.GetRevenue(filter, interval);
                //    sw.Stop();
                //    Debug.WriteLine("{0} took: {1}ms".FormatSafe(intervals, sw.ElapsedMilliseconds));

                //    Debug.WriteLine("Results for: {0}".FormatSafe(intervals));
                //    foreach (var result in results)
                //        Debug.WriteLine("{0}-{1}: {2}", result.FromUtc, result.TillUtc, result.Result);
                //}

                //// Google Analytics Examples
                //var googleAnalyticsKpiRetriever = new GoogleAnalyticsKpiRetriever(this.GetGoogleJwt(), ((Profile)this.cbGaView.SelectedItem).Id);
                //Obymobi.Analytics.Google.Filter googleFilter = new Obymobi.Analytics.Google.Filter();
                //googleFilter.FromUtc = from;
                //googleFilter.TillUtc = till;
                //googleFilter.CompanyIds.Add(343);
                //googleFilter.EventCategories.Add("RoomControls");

                //foreach (var interval in intervals)
                //{
                //    Stopwatch sw = new Stopwatch();
                //    sw.Start();
                //    var results = googleAnalyticsKpiRetriever.GetEventCount(googleFilter, interval);
                //    sw.Stop();
                //    Debug.WriteLine("{0} took: {1}ms".FormatSafe(intervals, sw.ElapsedMilliseconds));

                //    Debug.WriteLine("Results for: {0}".FormatSafe(intervals));
                //    foreach (var result in results)
                //        Debug.WriteLine("{0}-{1}: {2}", result.FromUtc, result.TillUtc, result.Result);
                //}

                //TransactionsKpiRetriever.Filter filter = new TransactionsKpiRetriever.Filter();
                //filter.FromUtc = from;
                //filter.TillUtc = till;
                //filter.CompanyIds.Add(343);

                //List<ReportingInterval> intervals = new List<ReportingInterval>();
                //intervals.Add(ReportingInterval.Daily);
                //intervals.Add(ReportingInterval.Weekly);
                //intervals.Add(ReportingInterval.Monthly);
                //intervals.Add(ReportingInterval.None);

                //foreach (var interval in intervals)
                //{
                //    Stopwatch sw = new Stopwatch();
                //    sw.Start();                    
                //    var results = retriever.GetRevenue(filter, interval);
                //    sw.Stop();
                //    Debug.WriteLine("{0} took: {1}ms".FormatSafe(intervals, sw.ElapsedMilliseconds));

                //    Debug.WriteLine("Results for: {0}".FormatSafe(intervals));
                //    foreach (var result in results)
                //        Debug.WriteLine("{0}-{1}: {2}", result.FromUtc, result.TillUtc, result.Result);
                //}

                //// Google Analytics Examples
                //var googleAnalyticsKpiRetriever = new GoogleAnalyticsKpiRetriever(this.GetGoogleJwt(), ((Profile)this.cbGaView.SelectedItem).Id);
                //Obymobi.Analytics.Google.Filter googleFilter = new Obymobi.Analytics.Google.Filter();
                //googleFilter.FromUtc = from;
                //googleFilter.TillUtc = till;
                //googleFilter.CompanyIds.Add(343);
                //googleFilter.EventCategories.Add("RoomControls");

                //foreach (var interval in intervals)
                //{
                //    Stopwatch sw = new Stopwatch();
                //    sw.Start();
                //    var results = googleAnalyticsKpiRetriever.GetEventCount(googleFilter, interval);
                //    sw.Stop();
                //    Debug.WriteLine("{0} took: {1}ms".FormatSafe(intervals, sw.ElapsedMilliseconds));

                //    Debug.WriteLine("Results for: {0}".FormatSafe(intervals));
                //    foreach (var result in results)
                //        Debug.WriteLine("{0}-{1}: {2}", result.FromUtc, result.TillUtc, result.Result);
                //}

                // ====

                //// 3 month period in intervals
                //var testFilter = new Obymobi.Analytics.Google.Filter();
                //testFilter.FromUtc = DateTime.UtcNow;
                //testFilter.TillUtc = testFilter.FromUtc.AddMonths(3);

                //Debug.WriteLine("Interval - None");
                //var intervals = IntervalCreator.GetIntervals(testFilter, ReportingInterval.None);
                //foreach (var value in intervals)
                //    Debug.WriteLine("{0}-{1}", value.Item1, value.Item2);

                //Debug.WriteLine("Interval - Monthly");
                //intervals = IntervalCreator.GetIntervals(testFilter, ReportingInterval.Monthly);
                //foreach (var value in intervals)
                //    Debug.WriteLine("{0}-{1}", value.Item1, value.Item2);

                //Debug.WriteLine("Interval - Weekly");
                //intervals = IntervalCreator.GetIntervals(testFilter, ReportingInterval.Weekly);
                //foreach (var value in intervals)
                //    Debug.WriteLine("{0}-{1}", value.Item1, value.Item2);

                //testFilter = new Obymobi.Analytics.Google.Filter();
                //testFilter.FromUtc = DateTime.UtcNow;
                //testFilter.TillUtc = testFilter.FromUtc.AddMonths(1);

                //Debug.WriteLine("Interval - Daily");
                //intervals = IntervalCreator.GetIntervals(testFilter, ReportingInterval.Daily);
                //foreach (var value in intervals)
                //    Debug.WriteLine("{0}-{1}", value.Item1, value.Item2);

                //testFilter = new Obymobi.Analytics.Google.Filter();
                //testFilter.FromUtc = DateTime.UtcNow;
                //testFilter.TillUtc = testFilter.FromUtc.AddDays(3);

                //Debug.WriteLine("Interval - Hourly");
                //intervals = IntervalCreator.GetIntervals(testFilter, ReportingInterval.Hourly);
                //foreach (var value in intervals)
                //    Debug.WriteLine("{0}-{1}", value.Item1, value.Item2);

                //// Fetch regulary
                //Stopwatch sw = new Stopwatch();
                //sw.Start();
                //DateTime fromUtc = new DateTime(2016, 3, 1);
                //DateTime tillUtc = new DateTime(2016, 3, 30);

                //TransactionsKpiRetriever.Filter f = new TransactionsKpiRetriever.Filter();
                //f.CompanyIds.Add(343);
                //f.FromUtc = fromUtc;
                //f.TillUtc = tillUtc;
                //var advancedResults = retriever.GetRevenue(f, ReportingInterval.Daily);

                //DateTime currentDate = fromUtc;
                //List<IntervalResult<decimal>> simpleResults = new List<IntervalResult<decimal>>();
                //while (currentDate <= tillUtc)
                //{
                //    TransactionsKpiRetriever.Filter fLocal = new TransactionsKpiRetriever.Filter();
                //    fLocal.CompanyIds.Add(343);
                //    fLocal.FromUtc = currentDate.MakeBeginOfDay();
                //    fLocal.TillUtc = currentDate.MakeEndOfDay();

                //    simpleResults.Add(new IntervalResult<decimal>(currentDate, currentDate, retriever.GetRevenue(fLocal)));

                //    currentDate = currentDate.AddDays(1);
                //}                

                int i = 9;



                //var googleAnalyticsKpiRetriever = new GoogleAnalyticsKpiRetriever(this.GetGoogleJwt(), ((Profile)this.cbGaView.SelectedItem).Id);
                //googleAnalyticsKpiRetriever.ThisShouldNeverExist();
            }
            else
            {
                MessageBox.Show("For Mr G's Eye's Only");
            }
        }

        private void btAnalytics_Click(object sender, EventArgs e)
        {
            var importer = new AnalyticsTesting.DataImporter();
            importer.ImportMsSql();
            //importer.ImportMongoDbLocal();
            //Task.Factory.StartNew(() => importer.ImportMongoAndSql());
            //Task.Factory.StartNew(() => importer.ImportDocumentDb());
        }
    }
}


