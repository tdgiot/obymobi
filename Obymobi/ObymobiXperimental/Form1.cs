﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dionysos;
using Obymobi.Configuration;
using Obymobi.Constants;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using Obymobi.Logic.Status;
using Obymobi.Security;
using Obymobi.Web.Weather;
using ObymobiXperimental.Mitel;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Integrations.PMS;
using Obymobi.Integrations.PMS.Configuration;
using Obymobi.Integrations.PMS.Tigertms;
using Obymobi.Integrations.PMS.Innsist;
using Obymobi.Integrations.Service.Hyatt;
using Obymobi.Integrations.PMS.Tigertms.Messages;
using Obymobi.Integrations.Service.HotSOS.MTechAPI;

namespace ObymobiXperimental
{
    public partial class Form1 : System.Windows.Forms.Form
    {
        [DllImport("User32.dll", CharSet = CharSet.Auto, EntryPoint = "SendMessage")]
        static extern IntPtr SendMessage(IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam);

        const int WM_VSCROLL = 277;
        const int SB_BOTTOM = 7;

        private PmsConnector pmsConnector = null;

        #region Constructor & logging

        public Form1()
        {
            InitializeComponent();

            //this.GenerateTestScript();

            if (TestUtil.IsPcBattleStationDanny)
            {
                /*var sb = new StringBuilder();
                IEnumerable<string> lines = File.ReadLines("D:\\Battlezone\\Logs\\CraveEmenu_20140329.txt");
                foreach (var line in lines)
                {
                    sb.Append(line.Replace("\n", "").Replace("\r", ""));
                }
                lines = File.ReadLines("D:\\Battlezone\\Logs\\CraveEmenu_20140330.txt");
                foreach (var line in lines)
                {
                    sb.Append(line.Replace("\n", "").Replace("\r", ""));
                }*/

                this.tbHashTimestamp.Text = DateTime.Now.ToUnixTime().ToString();
                this.tbHashMacAddress.Text = "98:52:b1:32:5c:9e";
                /*this.tbHashParam1.Text = "608";
                this.tbHashParam2.Text = sb.ToString();
                this.tbHashParam3.Text = long.MaxValue.ToString();*/
                this.tbHashSalt.Text = "pgveyykptbyikvvjkpirdjcpjkldnmxaphestypsvuxlpldu";

                //Clipboard.SetText(sb.ToString());
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // Load config from config file
            this.cbPmsTigerEmulateMessageInService.Checked = Dionysos.ConfigurationManager.GetBool(PmsConfigurationConstants.TigertmsWebserviceTestMode);
            this.tbPmsTigerMessageInServiceUrl.Text = Dionysos.ConfigurationManager.GetString(PmsConfigurationConstants.TigertmsWebserviceToCallToUrl);
            this.tbPmsTigerExternalWebserviceUrl.Text = Dionysos.ConfigurationManager.GetString(PmsConfigurationConstants.TigertmsWebserviceToBeCalledOnUrl);
            this.tbPmsTigerUserKey.Text = Dionysos.ConfigurationManager.GetString(PmsConfigurationConstants.TigertmsWebserviceUserkey);

            this.tbPmsComtrolHost.Text = Dionysos.ConfigurationManager.GetString(PmsConfigurationConstants.ComtrolHostAddress);
            this.tbPmsComtrolPort.Text = Dionysos.ConfigurationManager.GetString(PmsConfigurationConstants.ComtrolPort);

            if (this.tbPmsComtrolHost.Text.IsNullOrWhiteSpace())
                this.tbPmsComtrolHost.Text = this.GetLocalIp();

            this.tbMitelServerListenerIp.Text = this.GetLocalIp();
            this.tbMitelClientConnectToIp.Text = this.GetLocalIp();
            this.tbMitelClientListenerIp.Text = this.GetLocalIp();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Stop any running servers
            if (this.tigerConnector != null)
                this.tigerConnector.StopReceiving();

            this.pmsConnector = null;

            btnKillSpawns_Click(this, null);
        }

        private string GetLocalIp()
        {
            string result = string.Empty;

            IPHostEntry host;
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    string localIP = ip.ToString();
                    if (localIP.StartsWith("192.168."))
                    {
                        result = localIP;
                        break;
                    }
                }
            }

            return result;
        }

        delegate void AddToLogDelegate(string text);
        private void LogPms(string format, params object[] args)
        {
            if (this.tbPmsLog.InvokeRequired)
            {
                AddToLogDelegate d = new AddToLogDelegate(AddToLogPms);
                try
                {
                    this.Invoke(d, new object[] { string.Format(format, args) });
                }
                catch
                {
                    string text = string.Format("Format: {0}, Args: {1}", format, StringUtil.CombineWithCommaSpace(args));
                }
            }
            else
            {
                if (this.tbPmsLog.Text.IsNullOrWhiteSpace())
                    this.tbPmsLog.Text = string.Empty;
                this.tbPmsLog.Text = string.Format(format, args) + Environment.NewLine + this.tbPmsLog.Text;
            }
        }

        private void AddToLogPms(string text)
        {
            this.LogPms(text);
        }

        private void LogPms(Obymobi.Logic.Model.GuestInformation guestInformation)
        {
            this.LogPms("Account: '{0}'", guestInformation.AccountNumber);
            this.LogPms("Group: '{0}' (Reference: '{1}')", guestInformation.GroupName, guestInformation.GroupReference);
            this.LogPms("Company: '{0}'", guestInformation.Company);
            this.LogPms("Name: First: '{0}', Middle: '{1}', Last: '{2}'", guestInformation.CustomerFirstname, guestInformation.CustomerLastnamePrefix, guestInformation.CustomerLastname);
            this.LogPms("Zipcode: '{0}'", guestInformation.Zipcode);
            this.LogPms("Language: '{0}'", guestInformation.LanguageCode);
            this.LogPms("Password: '{0}'", guestInformation.Password);
            this.LogPms("Error: '{0}'", guestInformation.Error);

            if (guestInformation.AllowFolioPosting)
            {
                this.LogPms("Folio posting: Allowed, Credit Limit: '{0}'", guestInformation.CreditLimit.ToString("N2"));
            }
            else
                this.LogPms("Folio posting: Not allowed");
        }

        private void LogMitel(string format, params object[] args)
        {
            Debug.WriteLine(string.Format(format, args));
            if (this.tbMitelLog.InvokeRequired)
            {
                AddToLogDelegate d = new AddToLogDelegate(AddToLogMitel);
                try
                {
                    this.Invoke(d, new object[] { string.Format(format, args) });
                }
                catch
                {
                    string text = string.Format("Format: {0}, Args: {1}", format, StringUtil.CombineWithCommaSpace(args));
                }
            }
            else
            {
                if (this.tbMitelLog.Text.IsNullOrWhiteSpace())
                    this.tbMitelLog.Text = string.Empty;
                this.tbMitelLog.Text = string.Format(format, args) + Environment.NewLine + this.tbMitelLog.Text;
            }
        }

        private void AddToLogMitel(string text)
        {
            this.LogMitel(text);
        }

        #endregion

        #region Comtrol

        //private ComtrolConnector comtrolConnector;

        private void btComtrolInit_Click(object sender, EventArgs e)
        {
            Dionysos.ConfigurationManager.SetValue(PmsConfigurationConstants.ComtrolHostAddress, this.tbPmsComtrolHost.Text);
            Dionysos.ConfigurationManager.SetValue(PmsConfigurationConstants.ComtrolPort, this.tbPmsComtrolPort.Text);

            //this.comtrolConnector = new ComtrolConnector();
            this.HookUpComtrolEvents();
            this.lblStripConnectionState.Text = "Connected";
        }

        void HookUpComtrolEvents()
        {
            // Receive incoming messages
            //this.comtrolConnector.CheckedIn += new PmsConnector.CheckInHandler(connector_CheckedIn);
            //this.comtrolConnector.CheckedOut += new Obymobi.Integrations.PMS.PmsConnector.CheckOutHandler(connector_CheckedOut);
            //this.comtrolConnector.CreditLimitUpdated += new Obymobi.Integrations.PMS.PmsConnector.CreditLimitUpdateHandler(connector_CreditLimitUpdated);
            //this.comtrolConnector.DoNotDisturbStateUpdated += new Obymobi.Integrations.PMS.PmsConnector.DoNotDisturbUpdateHandler(connector_DoNotDisturbStateUpdated);
            //this.comtrolConnector.GuestInformationUpdated += new Obymobi.Integrations.PMS.PmsConnector.GuestInformationUpdateHandler(connector_GuestInformationUpdated);
            //this.comtrolConnector.Moved += new PmsConnector.MoveHandler(connector_Moved);
            //this.comtrolConnector.RoomStateUpdated += new Obymobi.Integrations.PMS.PmsConnector.RoomStateUpdateHandler(connector_RoomStateUpdated);
        }

        private void btPmsStopTigertms_Click(object sender, EventArgs e)
        {
            if (this.tigerConnector != null)
                this.tigerConnector.StopReceiving();

            this.btPmsInitializeTigerTms.Enabled = true;
            this.btPmsStopTigertms.Enabled = false;

            this.lblStripConnectionState.Text = "Not connected...";
        }

        #endregion

        #region TigerTMS

        TigertmsConnector tigerConnector = null;

        private ServiceHost host = null;
        private TigertmsToBeCalledOn.ExternalWebservice tigerTmsExternalWebservice = null;
        private void btPmsInitializeTigerTms_Click(object sender, EventArgs e)
        {
            this.btPmsInitializeTigerTms.Enabled = false;
            this.btPmsStopTigertms.Enabled = true;

            // Set if we emulate the MessageIn service (which is the one on which we call tiger)
            Dionysos.ConfigurationManager.SetValue(PmsConfigurationConstants.TigertmsWebserviceTestMode, this.cbPmsTigerEmulateMessageInService.Checked);

            // Set url of the MessageIn service (which is the one on which we call tiger)            
            Dionysos.ConfigurationManager.SetValue(PmsConfigurationConstants.TigertmsWebserviceToCallToUrl, this.tbPmsTigerMessageInServiceUrl.Text);

            // USer key
            Dionysos.ConfigurationManager.SetValue(PmsConfigurationConstants.TigertmsWebserviceUserkey, this.tbPmsTigerUserKey.Text);

            // Set the url of the ExternalWebservice, which is the one on which we will be called.
            if (!this.cbPmsTigerEmulateExternalWebservice.Checked)
            {
                // ObymobiXperimental webservice will not be used to receive messages, therefore we init Tiger with a useless ExternalWs url
                Dionysos.ConfigurationManager.SetValue(PmsConfigurationConstants.TigertmsWebserviceToBeCalledOnUrl, "http://localhost:5348/NotToBeUSed");

                this.tigerConnector = new TigertmsConnector();

                // For future start ups set the url to the Textbox value
                Dionysos.ConfigurationManager.SetValue(PmsConfigurationConstants.TigertmsWebserviceToBeCalledOnUrl, this.tbPmsTigerExternalWebserviceUrl.Text);
            }
            else
            {
                Dionysos.ConfigurationManager.SetValue(PmsConfigurationConstants.TigertmsWebserviceToBeCalledOnUrl, this.tbPmsTigerExternalWebserviceUrl.Text);
                this.tigerConnector = new TigertmsConnector();
            }

            // Init the Webservice calling code to to call to ExternalWebservice
            // So we're not initing a ExternalWebservice itself.
            this.tigerTmsExternalWebservice = new TigertmsToBeCalledOn.ExternalWebservice();
            this.tigerTmsExternalWebservice.Url = this.tbPmsTigerExternalWebserviceUrl.Text;

            this.tigerConnector.CheckedInForDevelopmentMode = this.cbPmsToCraveCheckedIn.Checked;

            // Receive incoming messages
            this.tigerConnector.CheckedIn += new PmsConnector.CheckInHandler(connector_CheckedIn);
            this.tigerConnector.CheckedOut += new Obymobi.Integrations.PMS.PmsConnector.CheckOutHandler(connector_CheckedOut);
            this.tigerConnector.CreditLimitUpdated += new Obymobi.Integrations.PMS.PmsConnector.CreditLimitUpdateHandler(connector_CreditLimitUpdated);
            this.tigerConnector.DoNotDisturbStateUpdated += new Obymobi.Integrations.PMS.PmsConnector.DoNotDisturbUpdateHandler(connector_DoNotDisturbStateUpdated);
            this.tigerConnector.GuestInformationUpdated += new Obymobi.Integrations.PMS.PmsConnector.GuestInformationUpdateHandler(connector_GuestInformationUpdated);
            this.tigerConnector.Moved += new PmsConnector.MoveHandler(connector_Moved);
            this.tigerConnector.RoomStateUpdated += new Obymobi.Integrations.PMS.PmsConnector.RoomStateUpdateHandler(connector_RoomStateUpdated);

            this.tigerConnector.Logged += new LoggingClassBase.LogHandler(tigerConnector_Logged);

            this.lblStripConnectionState.Text = "TigerTMS intialized...";

            // Set as the default PMS connector
            this.pmsConnector = this.tigerConnector;
        }

        void tigerConnector_Logged(object sender, LoggingLevel level, string log)
        {
            this.LogPms(log);
        }

        #endregion

        #region Innsist PMS

        private TCAPosConnector innsistConnector;
        private InnsistToBeCalledOn.ExternalWebservice innsistExternalWebservice;

        private void btnInnsistInitialize_Click(object sender, EventArgs e)
        {
            this.btnInnsistInitialize.Enabled = false;
            this.btnInnsistStop.Enabled = true;

            TCAPosConfgurationAdapter config = new TCAPosConfgurationAdapter
                                               {
                                                   FromSystemId = this.tbInnsistFromId.Text,
                                                   Password = this.tbInnsistPassword.Text,
                                                   ToSystemId = this.tbInnsistToId.Text,
                                                   Username = this.tbInnsistUsername.Text,
                                                   WakeUpCode = this.tbInnsistWakeUpCode.Text,
                                                   WebserviceToBeCalledOnUrl = this.tbInnsistIncommingUrl.Text,
                                                   WebserviceUrl = this.tbInnsistOutgoingUrl.Text
                                               };
            this.innsistConnector = new TCAPosConnector(config);
            this.innsistConnector.CheckedIn += connector_CheckedIn;
            this.innsistConnector.CheckedOut += connector_CheckedOut;
            this.innsistConnector.CreditLimitUpdated += connector_CreditLimitUpdated;
            this.innsistConnector.DoNotDisturbStateUpdated += connector_DoNotDisturbStateUpdated;
            this.innsistConnector.GuestInformationUpdated += connector_GuestInformationUpdated;
            this.innsistConnector.Moved += connector_Moved;
            this.innsistConnector.RoomStateUpdated += connector_RoomStateUpdated;

            this.innsistConnector.Logged += tigerConnector_Logged;

            this.innsistExternalWebservice = new InnsistToBeCalledOn.ExternalWebservice();
            this.innsistExternalWebservice.Url = config.WebserviceToBeCalledOnUrl;

            this.lblStripConnectionState.Text = "Innsist PMS intialized...";
            this.pmsConnector = this.innsistConnector;
        }

        private void btnInnsistStop_Click(object sender, EventArgs e)
        {
            if (this.pmsConnector != null)
            {
                this.pmsConnector.StopReceiving();
            }

            this.btnInnsistInitialize.Enabled = true;
            this.btnInnsistStop.Enabled = false;
        }

        #endregion

        #region Events to be received

        void connector_RoomStateUpdated(Obymobi.Integrations.PMS.PmsConnector sender, string deliverypointNumber, Obymobi.Integrations.PMS.RoomState state)
        {
            this.LogPms("--------------------------");
            this.LogPms("RoomStateUpdated - Room: {0}, New state: {1}", deliverypointNumber, state.ToString());
            this.LogPms("--------------------------");
        }

        void connector_Moved(PmsConnector sender, string fromDeliverypointNumber, string fromStationNumber, string toDeliverypointNumber, string toStationNumber)
        {
            this.LogPms("--------------------------");
            this.LogPms("RoomMoved - From: {0} (Station: {1}), To: {2} (Station: {3})", fromDeliverypointNumber, fromStationNumber, toDeliverypointNumber, toDeliverypointNumber, toStationNumber);
            this.LogPms("--------------------------");
        }


        void connector_GuestInformationUpdated(Obymobi.Integrations.PMS.PmsConnector sender, Obymobi.Logic.Model.GuestInformation guestInformation)
        {
            this.LogPms("--------------------------");
            this.LogPms("GuestInformation Updated for Room: {0}", guestInformation.DeliverypointNumber);
            this.LogPms(guestInformation);
            this.LogPms("--------------------------");
        }

        void connector_DoNotDisturbStateUpdated(Obymobi.Integrations.PMS.PmsConnector sender, string deliverypointNumber, Obymobi.Integrations.PMS.DoNotDisturbState state)
        {
            this.LogPms("--------------------------");
            this.LogPms("Do not disturb updated: Room '{0}' to state '{1}'", deliverypointNumber, state);
            this.LogPms("--------------------------");
        }

        void connector_CreditLimitUpdated(Obymobi.Integrations.PMS.PmsConnector sender, string deliverypointNumber, decimal limit)
        {
            this.LogPms("--------------------------");
            this.LogPms("Credit limit updated: Room '{0}' to state '{1:N2}'", deliverypointNumber, limit);
            this.LogPms("--------------------------");
        }

        void connector_CheckedOut(Obymobi.Integrations.PMS.PmsConnector sender, GuestInformation guestInformation)
        {
            this.LogPms("--------------------------");
            this.LogPms("Room checked out: Room '{0}'", guestInformation.DeliverypointNumber);
            this.LogPms("--------------------------");
        }

        void connector_CheckedIn(PmsConnector sender, Obymobi.Logic.Model.GuestInformation guestInformation)
        {
            this.LogPms("--------------------------");
            this.LogPms("Room checked in: Room '{0}'", guestInformation.DeliverypointNumber);
            this.LogPms(guestInformation);
            this.LogPms("--------------------------");
        }

        void connector_Logged(PmsConnector sender, LoggingLevel level, string log)
        {
            this.AddToLogPms(string.Format("[{0}] {1}", level.ToString(), log));
        }

        #endregion

        #region Generic Crave to PMS Functionality

        private void btCraveToPmsCheckout_Click(object sender, EventArgs e)
        {
            if (this.pmsConnector == null)
            {
                MessageBox.Show("No PMS Connector active");
            }
            else
            {
                Checkout checkoutRequest = new Checkout();
                checkoutRequest.DeliverypointNumber = this.tbCraveToPmsRoom.Text;
                checkoutRequest.ChargePriceIn = NumericsUtil.ParseInvariantCulture(this.tbCraveToPmsCheckoutBalance.Text);

                //bool result = this.pmsConnector.Checkout(checkoutRequest);

                Task.Factory.StartNew<bool>(() =>
                {
                    return this.pmsConnector.Checkout(checkoutRequest);
                }).ContinueWith((t) =>
                {
                    if (!t.IsFaulted)
                    {
                        this.LogPms("--------------------------");
                        if (t.Result)
                        {
                            this.LogPms("Express Checkout succeeded.");
                        }
                        else
                        {
                            this.LogPms("Express Checkout failed.");
                        }
                        this.LogPms("--------------------------");
                    }
                    else
                    {
                        if (t.Exception != null)
                            this.LogPms("Express Checkout failed: {0}", t.Exception.Message);
                        else
                            this.LogPms("Express Checkout failed: Exception unknown", t.Exception.Message);
                    }
                }, TaskScheduler.FromCurrentSynchronizationContext());
            }
        }

        private void btCraveToPmsGetFolio_Click(object sender, EventArgs e)
        {
            if (this.pmsConnector == null)
            {
                MessageBox.Show("No PMS Connector active");
            }
            else
            {
                //// Keep GUI active, run on seperate thread
                Task.Factory.StartNew<List<Folio>>(() =>
                {
                    return this.pmsConnector.GetFolio(this.tbCraveToPmsRoom.Text);
                }).ContinueWith((t) =>
                {
                    this.LogPms("--------------------------");
                    if (!t.IsFaulted)
                    {
                        if (t.Result != null)
                        {
                            foreach (var folio in t.Result)
                            {

                                this.LogPms("Folio received: Room '{0}'", folio.DeliverypointNumber);
                                if (!folio.Error.IsNullOrWhiteSpace())
                                    this.LogPms("Error: '{0}'", folio.Error);
                                else
                                {
                                    foreach (var item in folio.FolioItems)
                                    {
                                        this.LogPms("Folio item: '{0}', '{1}', '{2}'",
                                            item.Description, item.PriceIn, item.Created);
                                    }
                                }
                            }
                        }
                        else
                        {
                            this.LogPms("Folio received: No result.");
                        }
                    }
                    else
                    {
                        if (t.Exception != null)
                            this.LogPms("GetFolio failed: {0}", t.Exception.Message);
                        else
                            this.LogPms("GetFolio failed: Exception unknown", t.Exception.Message);
                    }
                    this.LogPms("--------------------------");
                }, TaskScheduler.FromCurrentSynchronizationContext());
            }
        }

        private async void btCraveToPmsGetGuestInformation_Click(object sender, EventArgs e)
        {
            if (this.pmsConnector == null)
            {
                MessageBox.Show("No PMS Connector active");
            }
            else
            {
                Task.Factory.StartNew<List<GuestInformation>>(() =>
                {
                    return this.pmsConnector.GetGuestInformation(this.tbCraveToPmsRoom.Text);
                }).ContinueWith((t) =>
                {
                    if (!t.IsFaulted)
                    {
                        this.LogPms("--------------------------");
                        if (t.Result != null)
                        {
                            foreach (var guestInfo in t.Result)
                                this.LogPms(guestInfo);
                        }
                        else
                        {
                            this.LogPms("GetGuestInformation returned nothing.");
                        }
                        this.LogPms("--------------------------");
                    }
                    else
                    {
                        if (t.Exception != null)
                            this.LogPms("GetGuestInformation failed: {0}", t.Exception.Message);
                        else
                            this.LogPms("GetGuestInformation failed: Exception unknown", t.Exception.Message);
                    }
                }, TaskScheduler.FromCurrentSynchronizationContext());
            }
        }

        private void btCraveToPmsAddToFolio_Click(object sender, EventArgs e)
        {
            if (this.pmsConnector == null)
            {
                MessageBox.Show("No PMS Connector active");
            }
            else
            {
                FolioItem fi = new FolioItem();
                fi.Created = DateTime.Now;
                fi.Description = this.tbCraveToPmsAddToFolioDescription.Text.ToString();
                fi.PriceIn = NumericsUtil.ParseInvariantCulture(this.tbCraveToPmsAddToFolioPrice.Text);
                var folioItems = new List<FolioItem> { fi };

                //bool result = this.pmsConnector.Checkout(checkoutRequest);

                Task.Factory.StartNew<bool>(() =>
                {
                    return this.pmsConnector.AddToFolio(this.tbCraveToPmsRoom.Text, folioItems);
                }).ContinueWith((t) =>
                {
                    if (!t.IsFaulted)
                    {
                        this.LogPms("--------------------------");
                        if (t.Result)
                        {
                            this.LogPms("Add to Folio succeeded.");
                        }
                        else
                        {
                            this.LogPms("Add to Folio failed.");
                        }
                        this.LogPms("--------------------------");
                    }
                    else
                    {
                        if (t.Exception != null)
                            this.LogPms("Add to Folio failed: {0}", t.Exception.Message);
                        else
                            this.LogPms("Add to folio failed: Exception unknown", t.Exception.Message);
                    }
                }, TaskScheduler.FromCurrentSynchronizationContext());
            }
        }

        #endregion

        #region Generic PMS to Crave Functionality

        private void btPmsToCraveCheckIn_Click(object sender, EventArgs e)
        {
            if (this.pmsConnector == null)
            {
                MessageBox.Show("No PMS Connector active");
            }
            else if (this.pmsConnector is TigertmsConnector)
            {
                this.cbPmsToCraveCheckedIn.Checked = true;

                Checkinresults checkinresults = new Checkinresults();
                checkinresults.Title = "Mr.";
                checkinresults.Room = this.tbPmsToCraveRoomNumber.Text;
                checkinresults.First = this.tbPmsToCraveFirstName.Text;
                checkinresults.Last = this.tbPmsToCraveLastname.Text;
                checkinresults.Viewbill = this.cbPmsToCraveAllowViewBill.Checked.ToString();
                checkinresults.ExpressCheckout = this.cbPmsToCraveAllowExpressCheckout.Checked.ToString();
                checkinresults.Group = this.tbPmsGroup.Text;
                checkinresults.Arrival = this.dpPmsCheckin.Text;
                checkinresults.Departure = this.dpPmsCheckout.Text;

                Task.Factory.StartNew(() =>
                {
                    this.tigerTmsExternalWebservice.SendMessageToExternalInterface(checkinresults.ToXml());
                });
            }
            else if (this.pmsConnector is TCAPosConnector)
            {
                this.cbPmsToCraveCheckedIn.Checked = true;
            }
            else
            {
                MessageBox.Show("PMS Simulation not implemented for PmsConnector: " + this.pmsConnector.GetType().Name);
            }
        }

        private void btPmsToCraveCheckout_Click(object sender, EventArgs e)
        {
            if (this.pmsConnector == null)
            {
                MessageBox.Show("No PMS Connector active");
            }
            else if (this.pmsConnector is TigertmsConnector)
            {
                this.cbPmsToCraveCheckedIn.Checked = false;

                Checkoutresults checkoutresults = new Checkoutresults();
                checkoutresults.Room = this.tbPmsToCraveRoomNumber.Text;

                // Send to OSS (we act like we are the TigerTms system)
                Task.Factory.StartNew(() =>
                {
                    this.tigerTmsExternalWebservice.SendMessageToExternalInterface(checkoutresults.ToXml());
                });
            }
            else
            {
                MessageBox.Show("PMS Simulation not implemented for PmsConnector: " + this.pmsConnector.GetType().Name);
            }
        }

        private void btPmsToCraveRoomMove_Click(object sender, EventArgs e)
        {
            if (this.pmsConnector == null)
            {
                MessageBox.Show("No PMS Connector active");
            }
            else if (this.pmsConnector is TigertmsConnector)
            {
                Roommoveresults roommove = new Roommoveresults();
                roommove.RoomOld = this.tbPmsToCraveOldRoomNumber.Text;
                roommove.Room = this.tbPmsToCraveRoomNumber.Text;

                // Send to OSS (we act like we are the TigerTms system)
                Task.Factory.StartNew(() =>
                {
                    this.tigerTmsExternalWebservice.SendMessageToExternalInterface(roommove.ToXml());
                });
            }
            else
            {
                MessageBox.Show("PMS Simulation not implemented for PmsConnector: " + this.pmsConnector.GetType().Name);
            }
        }        

        private void cbPmsToCraveCheckedIn_CheckedChanged(object sender, EventArgs e)
        {
            if (this.tigerConnector != null)
            {
                this.tigerConnector.CheckedInForDevelopmentMode = this.cbPmsToCraveCheckedIn.Checked;
            }
        }

        #endregion

        #region Mitel

        TcpServer mitelServer;
        MitelAopClient mitelClient;

        private void btMitelClientConnect_Click(object sender, EventArgs e)
        {
            if (this.mitelClient != null)
                this.mitelClient.Disconnect();

            string connectToIp = this.tbMitelClientConnectToIp.Text;
            int connectToPort = Convert.ToInt32(this.tbMitelClientConnectToPort.Text);
            string receiveOnIp = this.tbMitelClientListenerIp.Text;
            int receiveOnPort = Convert.ToInt32(this.tbMitelClientListenerPort.Text);

            this.mitelClient = new MitelAopClient(connectToIp, connectToPort, receiveOnIp, receiveOnPort);
            this.mitelClient.Logged += mitelClient_Logged;

            this.btMitelClientConnect.Enabled = false;
            this.btMitelClientDisconnect.Enabled = true;
        }

        void mitelClient_Logged(object sender, LoggingLevel level, string log)
        {
            this.LogMitel(log);
        }

        private void btMitelClientDisconnect_Click(object sender, EventArgs e)
        {
            if (this.mitelClient != null)
            {
                this.mitelClient.Disconnect();
                this.mitelClient = null;
            }
            this.btMitelClientConnect.Enabled = true;
            this.btMitelClientDisconnect.Enabled = false;
        }

        private void btMitelSend_Click(object sender, EventArgs e)
        {
            MitelAopRequest request = new MitelAopRequest(this.GetLocalIp(), Convert.ToInt32(this.tbMitelClientListenerPort.Text));
            request.To = this.tbMitelSendTo.Text;
            request.SenderIp = this.GetLocalIp();
            request.SenderPort = Convert.ToInt32(this.tbMitelClientListenerPort.Text);
            request.Subject = this.tbMitelSendSubject.Text;
            request.BodyText = this.tbMitelSendBody.Text;
            request.Priority = Convert.ToInt32(this.tbMitelSendPriority.Text);

            if (this.mitelClient == null)
                MessageBox.Show("First connect the Mitel Client");
            else
            {
                this.mitelClient.QueueMessage(request);
            }
        }

        private void btMitelServerStart_Click(object sender, EventArgs e)
        {
            if (this.mitelServer != null)
            {
                this.mitelServer.Stop();                
            }

            MitelAopServer server = new MitelAopServer();
            this.mitelServer = new TcpServer(server, this.tbMitelServerListenerIp.Text,  Convert.ToInt32(this.tbMitelServerListenerPort.Text));
            this.mitelServer.Start();

            server.Logged += server_Logged;            

            this.btMitelServerStart.Enabled = false;
            this.btMitelServerStop.Enabled = true;
        }

        void server_Logged(object sender, LoggingLevel level, string log)
        {
            this.LogMitel(log);
        }

        private void btMitelServerStop_Click(object sender, EventArgs e)
        {
            if (this.mitelServer != null)
            {
                this.mitelServer.Stop();
            }

            this.btMitelServerStart.Enabled = false;
            this.btMitelServerStop.Enabled = true;
        }

        #endregion

        #region Stresser

        private void btStresserStart_Click(object sender, EventArgs e)
        {                       
            // username,password,companyid,terminalid,clientid,deliverypointgroupId,
            /* string parameters = "Chewg,Treehouse,199,0,1646,255";
             ClientStatus status = new ClientStatus();
             status.ClientOperationMode = (int)ClientOperationMode.Ordering;
             status.BatteryLevel = 50;
             status.ClientStatusCode = (int)ClientStatusCode.OK;
             status.DeliverypointNumber = "10";            
             string statusXml = XmlHelper.Serialize(status);
             this.btStresserStart.Enabled = false;
             Parallel.For(0, Convert.ToInt32(this.tbStresserSeconds.Text), i =>
             {
                 CraveService6.CraveService cs = new CraveService6.CraveService();
                 cs.Url = this.tbStresserWebserviceUrl.Text;

                 string[] parametersSplit = StringUtil.Split(parameters, ',', StringSplitOptions.RemoveEmptyEntries);
                 string username = parametersSplit[0];
                 string password = parametersSplit[1];
                 int companyId = Convert.ToInt32(parametersSplit[2]);
                 int terminalId = Convert.ToInt32(parametersSplit[3]);
                 int clientId = Convert.ToInt32(parametersSplit[4]);
                 int dpgId = Convert.ToInt32(parametersSplit[5]);

                 StringBuilder sbResult = new StringBuilder();
                 sbResult.AppendFormat("Run {0}", i);

                 if (this.cbStressGetCompany.Checked)
                 {
                     if (this.cbAsync.Checked)
                     {
                         cs.GetCompanyAsync(0, username, password, companyId, terminalId, clientId);
                     }
                     else
                     {
                         sbResult.AppendFormat("GetCompany: {0}, ", cs.GetCompany(username, password, companyId, terminalId, clientId).ResultCode);
                     }
                 }

                 if (this.cbStressGetDeliverypoints.Checked)
                 {
                     if (this.cbAsync.Checked)
                     {
                         cs.GetDeliverypointsAsync(username, password, companyId, 0, clientId, dpgId);
                     }
                     else
                     {
                         sbResult.AppendFormat("GetDeliverypoints: {0}, ", cs.GetDeliverypoints(username, password, companyId, 0, clientId, dpgId).ResultCode);
                     }
                 }

                 if (this.cbStressGetMenu.Checked)
                 {
                     if (this.cbAsync.Checked)
                     {
                         cs.GetMenuAsync(username, password, companyId, terminalId, clientId);
                     }
                     else
                         sbResult.AppendFormat("GetMenu: {0}, ", cs.GetMenu(username, password, companyId, terminalId, clientId).ResultCode);                
                 }

                 //if (this.cbStressGetOrders.Checked)
                 //{ 
                 //    cs.GetOrders(username,password, companyId, terminalId, 
                 //}

                 if (this.cbStressGetReleaseAndDownloadLocation.Checked)
                 {                    
                     if (this.cbAsync.Checked)
                     {
                     cs.GetReleaseAndDownloadLocationAsync(username, password, companyId, "CraveConsoleAndroid");
                     }
                     else
                     {
                         sbResult.AppendFormat("GetRelease: {0}, ", cs.GetReleaseAndDownloadLocation(username, password, companyId, "CraveConsoleAndroid"));
                     }
                 }

                 if (this.cbStressGetSetClientStatus.Checked)
                 {   if (this.cbAsync.Checked)
                     {
                         cs.GetSetClientStatusAsync(username, password, terminalId, clientId, statusXml);
                     }
                     else
                     {
                         sbResult.AppendFormat("GetSetClientStatus: {0}, ", cs.GetSetClientStatus(username, password, terminalId, clientId, statusXml).ResultCode);
                     }
                 }

                 if (this.cbStressUpdateLastRequest.Checked)
                 {   
                     if (this.cbAsync.Checked)
                     {
                         cs.UpdateLastRequestAsync(username, password, terminalId, clientId);
                     }
                     else
                     {
                         sbResult.AppendFormat("UpdatLastRequest: {0}, ", cs.UpdateLastRequest(username, password, terminalId, clientId).ResultCode);
                     }
                 }
                 cs.Dispose();
                 Debug.WriteLine(sbResult.ToString());
                 sbResult.Clear();
                
             });

             this.btStresserStart.Enabled = true;*/
        }

        #endregion

        private void tbMitelServerListenerPort_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnGetLastState_Click(object sender, EventArgs e)
        {
            ConfigurationManager.SetValue(CraveCloudConfigConstants.CloudEnvironment, CloudEnvironment.ProductionPrimary);

            var status = new OnsiteServerStatus();
            status.Refresh(false);

            Debug.WriteLine("Finished executing...");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var timestamp = DateTime.Now.ToUnixTime();
            var mac = "98:52:b1:32:5d:ba";

            var queryString = "GetMedia.aspx";
            queryString += "?timestamp=" + timestamp;
            queryString += "&macAddress=" + mac;
            queryString += "&from=0";

            String hash = Hasher.GetHashFromParameters("p5kicqdn0vxebruj86tz13amoh7lsw4y29fg", timestamp, mac, 0);

            queryString += "&hash=" + hash;

            Process.Start("http://localhost/api/" + queryString);
        }

        private void btSerialisationTest_Click(object sender, EventArgs e)
        {
            Obymobi.Mobile.Data.CollectionClasses.CompanyCollection companies = new Obymobi.Mobile.Data.CollectionClasses.CompanyCollection();

            Obymobi.Mobile.Data.EntityClasses.CompanyEntity company = new Obymobi.Mobile.Data.EntityClasses.CompanyEntity();
            company.Name = "Heer";

            companies.Add(company);

            string path = @"d:\temp\comp.bin";
            this.SaveObject(path, companies);
            this.TryLoadObject(path, out companies);            
        }

        private static BinaryFormatter binaryFormatter = new BinaryFormatter(); // Slow to create, so only do once and also used as lock so static
        public bool SaveObject(string path, object data)
        {
            bool success = false;

            if (data == null)
            {
                //Log.Warning(ClassName, "SaveObject", "Path: {0}, ObjectType: null, Failed: Object is null", path, data.GetType().Name);
                return false;
            }

            lock (binaryFormatter)
            {
                try
                {
                    

                    using (Stream stream = File.Open(path, FileMode.Create))
                    {
                        binaryFormatter.Serialize(stream, data);
                    }
                    success = true;
                }
                catch (Exception ex)
                {
                    //Log.Warning(false, ClassName, "SaveObject", "Path: {0}, ObjectType: {1}, Failed: {2}\n{3}", path, data.GetType().Name, ex.Message, ex.StackTrace);
                }
            }

            return success;
        }

        public bool TryLoadObject<T>(string path, out T data)
        {
            bool success = false;
            data = default(T);

            lock (binaryFormatter)
            {
                try
                {                    
                    lock (binaryFormatter)
                    {
                        using (Stream stream = File.Open(path, FileMode.Open))
                        {
                            data = (T)binaryFormatter.Deserialize(stream);
                        }
                    }                 
                }
                catch (Exception ex)
                {
                    //Log.Warning(false, ClassName, "TryLoadObject", "Path: {0}, ObjectType: {1}, Failed: {2}\n{3}", path, typeof(T).Name, ex.Message, ex.StackTrace);
                }
            }

            if (!success)
                data = default(T);

            return success;
        }

        #region Comet Load Test

        readonly Dictionary<int, Process> spawnedClientProcesses = new Dictionary<int, Process>(); 

        private void btnSpawnCometClients_Click(object sender, EventArgs e)
        {
            if (numClients.Value == 0)
            {
                MessageBox.Show(@"Number of clients is zero", @"Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var filter = new PredicateExpression();
            filter.Add(ClientFields.DeviceId != DBNull.Value);
            filter.Add(ClientFields.ClientId != spawnedClientProcesses.Keys.ToList());

            var relations = new RelationCollection();
            relations.Add(ClientEntityBase.Relations.DeviceEntityUsingDeviceId);
            relations.Add(ClientEntityBase.Relations.CompanyEntityUsingCompanyId);

            var clientCollection = new ClientCollection();
            clientCollection.GetMulti(filter, (long)numClients.Value, null, relations);

            if (clientCollection.Count < numClients.Value)
            {
                MessageBox.Show(@"Number of clients fetched from database is less then specified", @"Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            for (int i = 0; i < numClients.Value; i++)
            {
                int id = i;
                Task.Factory.StartNew(() =>
                    {
                        var clientEntity = clientCollection[id];

                        var startInfo = new ProcessStartInfo();
                        startInfo.FileName = "ObymobiXperimental.Comet.exe";
                        startInfo.Arguments = string.Format("{0} {1} {2} {3} {4} {5}", tbPokeInUrl.Text, clientEntity.MacAddress, clientEntity.CompanyEntity.Salt, clientEntity.ClientId, clientEntity.DeliverypointGroupId.GetValueOrDefault(0), clientEntity.CompanyId);
                        startInfo.WorkingDirectory = Application.StartupPath;
                        startInfo.UseShellExecute = false;
                        startInfo.RedirectStandardOutput = true;
                        startInfo.CreateNoWindow = true;

                        try
                        {
                            Process clientProcess;
                            // Start the process with the info we specified.
                            // Call WaitForExit and then the using statement will close.
                            using (clientProcess = Process.Start(startInfo))
                            {
                                lock (logLock)
                                {
                                    spawnedClientProcesses.Add(clientEntity.ClientId, clientProcess);
                                }

                                using (var reader = clientProcess.StandardOutput)
                                {
                                    while (!clientProcess.HasExited)
                                    {
                                        string str = reader.ReadLine();
                                        loadClient_OnLog(str);
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            loadClient_OnLog(ex.Message);
                        }
                    });
            }
        }

        private void btnKillSpawns_Click(object sender, EventArgs e)
        {
            foreach (var cometLoadClient in spawnedClientProcesses)
            {
                try
                {
                    if (cometLoadClient.Value != null && !cometLoadClient.Value.HasExited)
                    {
                        cometLoadClient.Value.Kill();
                    }
                }
                catch
                {
                }

                loadClient_OnLog("Killed process for client id: " + cometLoadClient.Key);
            }

            spawnedClientProcesses.Clear();
        }

        readonly object logLock = new object();
        void loadClient_OnLog(string log)
        {
            if (tbEventLog.InvokeRequired)
            {
                tbEventLog.Invoke((MethodInvoker)(() => loadClient_OnLog(log)));
            }
            else
            {
                lock(logLock)
                    tbEventLog.AppendText(log + "\n");
            }
        }

        private void tbEventLog_TextChanged(object sender, EventArgs e)
        {
            var ptrWparam = new IntPtr(SB_BOTTOM);
            var ptrLparam = new IntPtr(0);
            SendMessage(this.tbEventLog.Handle, WM_VSCROLL, ptrWparam, ptrLparam);
        }

        #endregion

        private void btnGenerateHash_Click(object sender, EventArgs e)
        {
            var parameters = new object[]
                {
                    this.tbHashTimestamp.Text,
                    this.tbHashMacAddress.Text,
                    this.tbHashParam1.Text,
                    this.tbHashParam2.Text,
                    this.tbHashParam3.Text,
                    this.tbHashParam4.Text,
                    this.tbHashParam5.Text
                };
            var hash = Hasher.GetHashFromParameters(this.tbHashSalt.Text, parameters);
            this.tbHashOutput.Text = hash;
        }

        private void btnSaveClientLog_Click(object sender, EventArgs e)
        {
            btnGenerateHash_Click(this, null);

            using (var client = new WebClient())
            {
                var reqparm = new NameValueCollection();
                reqparm.Add("timestamp", this.tbHashTimestamp.Text);
                reqparm.Add("macAddress", this.tbHashMacAddress.Text);
                reqparm.Add("logType", this.tbHashParam1.Text);
                reqparm.Add("log", this.tbHashParam2.Text);
                reqparm.Add("fileDate", this.tbHashParam3.Text);
                reqparm.Add("hash", this.tbHashOutput.Text);

                var responseBytes = client.UploadValues("http://test.crave-emenu.com/api/14.0/CraveService.asmx/SaveClientLog", "POST", reqparm);
                tbRandomOutput.Text = Encoding.UTF8.GetString(responseBytes);
            }
            
        }

        private void btnHashTimestampRefresh_Click(object sender, EventArgs e)
        {
            this.tbHashTimestamp.Text = DateTime.Now.ToUnixTime().ToString();
        }

        private void btnOWMCurrent_Click(object sender, EventArgs e)
        {
            try
            {
                var provider = new OpenWeatherMapProvider(ObymobiConstants.GetOpenWeatherMapAppId(CloudEnvironment.Manual));
                var weather = provider.GetCurrentWeatherForCity("Naaldwijk,NL");

                Debug.WriteLine(weather);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }

        private void btnOWMForecast_Click(object sender, EventArgs e)
        {
            try
            {
                var provider = new OpenWeatherMapProvider(ObymobiConstants.GetOpenWeatherMapAppId(CloudEnvironment.Manual));
                var weather = provider.GetForecastForCity("Naaldwijk,NL", 5);

                Debug.WriteLine(weather);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }

        private void btnYahooWeatherCurrent_Click(object sender, EventArgs e)
        {
            try
            {
                var provider = new YahooWeatherProvider();
                var weather = provider.GetCurrentWeatherForCity("Naaldwijk,NL");

                Debug.WriteLine(weather);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }

        private void btnYahooWeatherForecast_Click(object sender, EventArgs e)
        {
            try
            {
                var provider = new YahooWeatherProvider();
                var weather = provider.GetForecastForCity("Naaldwijk,NL", 5);

                Debug.WriteLine(weather);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }     

        private string WakeUpDateSent;
        private string WakeUpTimeSent;

        private void btnSetWakeUp_Click(object sender, EventArgs e)
        {
            if (this.pmsConnector == null)
            {
                MessageBox.Show("No PMS Connector active");
            }
            else if (this.pmsConnector is TigertmsConnector)
            {
                DateTime today = DateTime.Now.AddMinutes(3); // 3 minutes ahead so we have time to clear it, if necessary

                Wakeupsetresults wakeupsetresults = new Wakeupsetresults();
                wakeupsetresults.Room = this.tbPmsToCraveRoomNumber.Text;
                wakeupsetresults.WakeUpDate = string.Format("{0:dd/MM/yyyy}", today).Replace("-", "/"); // Format: 07/04/2010. I know, strange replace but took too long, fuck the police
                wakeupsetresults.WakeUpTime = string.Format("{0:HH:mm:ss}", today); // Format: 07:30:00

                this.WakeUpDateSent = wakeupsetresults.WakeUpDate;
                this.WakeUpTimeSent = wakeupsetresults.WakeUpTime;

                // Send to OSS (we act like we are the TigerTms system)
                Task.Factory.StartNew(() =>
                {
                    this.tigerTmsExternalWebservice.SendMessageToExternalInterface(wakeupsetresults.ToXml());
                });
            }
            else
            {
                MessageBox.Show("PMS Simulation not implemented for PmsConnector: " + this.pmsConnector.GetType().Name);
            }
        }

        private void btnClearWakeUp_Click(object sender, EventArgs e)
        {
            if (this.pmsConnector == null)
            {
                MessageBox.Show("No PMS Connector active");
            }
            else if (this.WakeUpDateSent.IsNullOrWhiteSpace() || this.WakeUpTimeSent.IsNullOrWhiteSpace())
            {
                MessageBox.Show("You must set a wake-up first before you can clear it");
            }
            else if (this.pmsConnector is TigertmsConnector)
            {
                Wakeupclearresults wakeupclearresults = new Wakeupclearresults();
                wakeupclearresults.Room = this.tbPmsToCraveRoomNumber.Text;
                wakeupclearresults.WakeUpDate = this.WakeUpDateSent; // 07/04/2010
                wakeupclearresults.WakeUpTime = this.WakeUpTimeSent; // 07:30:00

                this.WakeUpDateSent = "";
                this.WakeUpTimeSent = "";

                // Send to OSS (we act like we are the TigerTms system)
                Task.Factory.StartNew(() =>
                {
                    this.tigerTmsExternalWebservice.SendMessageToExternalInterface(wakeupclearresults.ToXml());
                });
            }
            else
            {
                MessageBox.Show("PMS Simulation not implemented for PmsConnector: " + this.pmsConnector.GetType().Name);
            }
        }

        List<string> productNames = new List<string> { "Club Sandwich", "Cuba Libre Cocktail", "Champagne Afternoon Tea", "Continental Breakfast", "60 Minute Massage", "Skin Radiance Facials", "Chauffeur Hire" };

        private void btnCreateOrder_Click(object sender, EventArgs e)
        {
            if (this.pmsConnector == null)
            {
                MessageBox.Show("No PMS Connector active");
            }
            else if (this.pmsConnector is TigertmsConnector)
            {
                Roombillresults roombillresults = new Roombillresults();
                roombillresults.Room = this.tbPmsToCraveRoomNumber.Text;

                var numberOfProducts = RandomUtil.GetRandomNumber(10) + 2;
                roombillresults.Items = new List<BillItem>();
                decimal balance = 0;
                for (int i = 1; i <= numberOfProducts; i++)
                {
                    decimal price = ((RandomUtil.GetRandomNumber(1000) + 100) / 100);
                    balance += price;

                    BillItem bi1 = new BillItem();
                    bi1.Charge = price.ToStringInvariantCulture("N2");
                    bi1.Code = "CODE" + i;
                    bi1.Description = string.Format("Charge - {0}", productNames.Pick());
                    bi1.DateTime = MessageBase.DateTimeToString(DateTime.Now.AddDays(0 - RandomUtil.GetRandomNumber(8)).AddMinutes(RandomUtil.GetRandomNumber(1440)));

                    roombillresults.Items.Add(bi1);
                }
                roombillresults.Balance = balance.ToStringInvariantCulture("N2");

                // Send to OSS (we act like we are the TigerTms system)
                Task.Factory.StartNew(() =>
                {
                    this.tigerTmsExternalWebservice.SendMessageToExternalInterface(roombillresults.ToXml());
                });
            }
            else
            {
                MessageBox.Show("PMS Simulation not implemented for PmsConnector: " + this.pmsConnector.GetType().Name);
            }
        }

        private void btnEditGuestInformation_Click(object sender, EventArgs e)
        {
            if (this.pmsConnector == null)
            {
                MessageBox.Show("No PMS Connector active");
            }
            else if (this.pmsConnector is TigertmsConnector)
            {
                Editguestresults editguestresults = new Editguestresults();
                editguestresults.Room = this.tbPmsToCraveRoomNumber.Text;
                editguestresults.Title = "Mr.";
                editguestresults.First = this.tbPmsToCraveFirstName.Text;
                editguestresults.Last = this.tbPmsToCraveLastname.Text;
                editguestresults.Viewbill = this.cbPmsToCraveAllowViewBill.Checked.ToString();
                editguestresults.ExpressCheckout = this.cbPmsToCraveAllowExpressCheckout.Checked.ToString();
                editguestresults.Group = this.tbPmsGroup.Text;
                editguestresults.Arrival = this.dpPmsCheckin.Text;
                editguestresults.Departure = this.dpPmsCheckout.Text;

                // Send to OSS (we act like we are the TigerTms system)
                Task.Factory.StartNew(() =>
                {
                    this.tigerTmsExternalWebservice.SendMessageToExternalInterface(editguestresults.ToXml());
                });
            }
            else
            {
                MessageBox.Show("PMS Simulation not implemented for PmsConnector: " + this.pmsConnector.GetType().Name);
            }
        }

        private void btnEditRoomData_Click(object sender, EventArgs e)
        {
            if (this.pmsConnector == null)
            {
                MessageBox.Show("No PMS Connector active");
            }
            else if (this.pmsConnector is TigertmsConnector)
            {
                Roomdataresults roomdataResults = new Roomdataresults();
                roomdataResults.Room = this.tbPmsToCraveRoomNumber.Text;
                roomdataResults.MessageWaitingLamp = this.cbHasVoicemail.Checked ? "Y" : "N";

                // Send to OSS (we act like we are the TigerTms system)
                Task.Factory.StartNew(() =>
                {
                    this.tigerTmsExternalWebservice.SendMessageToExternalInterface(roomdataResults.ToXml());
                });
            }
            else
            {
                MessageBox.Show("PMS Simulation not implemented for PmsConnector: " + this.pmsConnector.GetType().Name);
            }
        }

        private void btCreateServiceOrder_Click(object sender, EventArgs e)
        {
            MTechAPIClient client = new MTechAPIClient();
            client.ClientCredentials.UserName.UserName = tbHotSOSUsername.Text;
            client.ClientCredentials.UserName.Password = tbHotSOSPassword.Text;
            client.Endpoint.Address = new EndpointAddress(tbHotSOSUrl.Text);

            Obymobi.Integrations.Service.HotSOS.MTechAPI.ServiceOrder serviceOrder = null;

            try
            {
                Obymobi.Integrations.Service.HotSOS.MTechAPI.Issue issue = new Obymobi.Integrations.Service.HotSOS.MTechAPI.Issue();
                issue.ID = "1";

                Room room = new Obymobi.Integrations.Service.HotSOS.MTechAPI.Room();
                room.ID = "1";

                serviceOrder = new Obymobi.Integrations.Service.HotSOS.MTechAPI.ServiceOrder();
                serviceOrder.Issue = issue;
                serviceOrder.Location = room;

                serviceOrder = client.send(serviceOrder) as Obymobi.Integrations.Service.HotSOS.MTechAPI.ServiceOrder;

                tbHotSOSResponse.Text = $"id: {serviceOrder.ID} status:{serviceOrder.Status}";
            }
            catch (Exception exception)
            {
                tbHotSOSResponse.Text = exception.ToString();
            }
        }
    }
}
