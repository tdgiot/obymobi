﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Obymobi.Configuration;
using System.IO;
using Obymobi.Integrations.PMS.Configuration;
using Obymobi.Integrations.POS;

namespace ObymobiXperimental
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            // Set the data providers
            Dionysos.DataFactory.EntityCollectionFactory = new Dionysos.Data.LLBLGen.LLBLGenEntityCollectionFactory();
            Dionysos.DataFactory.EntityFactory = new Dionysos.Data.LLBLGen.LLBLGenEntityFactory();
            Dionysos.DataFactory.EntityUtil = new Dionysos.Data.LLBLGen.LLBLGenEntityUtil();

            // Set the assembly information
            Dionysos.Global.AssemblyInfo.Add(Path.Combine(Application.StartupPath, "Obymobi.Data.dll"), "Data");

            var provider = new Dionysos.Configuration.XmlConfigurationProvider { ManualConfigFilePath = Path.Combine(Path.GetDirectoryName(Application.StartupPath), "ObymobiExperimental.config") };

            Dionysos.Global.ApplicationInfo = new Dionysos.ServiceProcess.ServiceInfo();
            Dionysos.Global.ApplicationInfo.ApplicationName = "Xperimental";

            Dionysos.Global.ConfigurationProvider = provider;
            Dionysos.Global.ConfigurationInfo.Add(new Dionysos.DionysosConfigurationInfo());
            Dionysos.Global.ConfigurationInfo.Add(new Obymobi.ObymobiConfigInfo());
            Dionysos.Global.ConfigurationInfo.Add(new CraveOnSiteServerConfigInfo());
            Dionysos.Global.ConfigurationInfo.Add(new POSConfigurationInfo());
            Dionysos.Global.ConfigurationInfo.Add(new PmsConfigurationInfo());
            Dionysos.Global.ConfigurationInfo.Add(new CraveCloudConfigInfo());

            Application.Run(new Form1());
        }
    }
}
