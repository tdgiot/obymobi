﻿using Dionysos;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ObymobiXperimental
{
    public class TcpTestServer : LoggingClassBase
    {
        private string ipAddress;
        private int port;
        private string logPrefix = "TcpTestServer";
        
        
        public TcpTestServer(string ipAddress = "127.0.0.1", int port = 13000)
        {
            this.ipAddress = ipAddress;
            this.port = port;
        }

        public void Start()
        {
            try
            {
                this.logPrefix += "-" + ipAddress + ":" + port;

                // Set the TcpListener on port 13000.                
                IPAddress localAddr = IPAddress.Parse(ipAddress);

                // TcpListener server = new TcpListener(port);
                TcpListener server = new TcpListener(localAddr, port);

                // Start listening for client requests.
                server.Start();

                // Enter the listening loop.
                while (true)
                {
                    // this.LogVerbose("Waiting for a connection... ");

                    // Perform a blocking call to accept requests.
                    // You could also user server.AcceptSocket() here.
                    TcpClient client = server.AcceptTcpClient();

                    //this.LogVerbose("Connected - Local Endpoint {0}:{1} - Remote Endpoint: {2}:{3}",
                    //    ((IPEndPoint)client.Client.LocalEndPoint).Address,
                    //    ((IPEndPoint)client.Client.LocalEndPoint).Port,
                    //    ((IPEndPoint)client.Client.RemoteEndPoint).Address,
                    //    ((IPEndPoint)client.Client.RemoteEndPoint).Port);

                    Thread t = new Thread(() => TcpTestServer.StartClientHandler(client, this));
                    t.Start();
                }
            }
            catch (SocketException e)
            {
                this.LogVerbose(string.Format("SocketException: {0}", e));
            }
            catch (Exception e)
            {
                this.LogVerbose(string.Format("Exception: {0}", e));
            }

            this.LogVerbose("\nHit enter to continue...");
        }

        public static void StartClientHandler(TcpClient client, TcpTestServer server)
        {
            ClientHandler handler = new ClientHandler();
            handler.MaintainClientConnection(client, server);   
        }

        public class ClientHandler
        {
            private string tuple;
            private string logPrefix = "Server";
            public void MaintainClientConnection(TcpClient client, TcpTestServer server)
            {
                // Buffer for reading data
                Byte[] bytes = new Byte[256];
                String data = null;

                this.tuple = string.Format("{0}:{1}-{2}:{3}",
                    ((IPEndPoint)client.Client.LocalEndPoint).Address,
                    ((IPEndPoint)client.Client.LocalEndPoint).Port,
                    ((IPEndPoint)client.Client.RemoteEndPoint).Address,
                    ((IPEndPoint)client.Client.RemoteEndPoint).Port);

                this.tuple = ((IPEndPoint)client.Client.RemoteEndPoint).Port.ToString();
                this.logPrefix = "Server-"+this.tuple;

                data = null;

                // Loop to receive all the data sent by the client.
                try
                {
                    // Get a stream object for reading and writing                    
                    int i;
                    while (true)
                    {                        
                        while (true)
                        {
                            NetworkStream stream = client.GetStream();                            
                            bytes = new Byte[256];
                            data = string.Empty;
                            i = stream.Read(bytes, 0, bytes.Length);
                            if(i != 0)
                            {
                                // Translate data bytes to a ASCII string.
                                data = System.Text.Encoding.ASCII.GetString(bytes, 0, i);
                                //this.LogVerbose(String.Format("Server Received: {0}", data));
                            }

                            data = data.Replace("\0", string.Empty);
                            server.LogVerbose("{0} - Received: {1}", this.logPrefix, data);

                            if(!data.IsNullOrWhiteSpace())
                            {
                                // Process the data sent by the client.
                                data = this.tuple + "- Server to Client - " + DateTime.Now.ToString();                                
                                byte[] msg = System.Text.Encoding.ASCII.GetBytes(data);

                                // Send back a response.
                                stream.Write(msg, 0, msg.Length);
                                //this.LogVerbose(String.Format("Server Sent: {0}", data));
                                //Debug.WriteLine("Read: " + data);
                            }
                            else
                                data = "NO DATA";

                            Thread.Sleep(4000);

                            
                        }                        
                    }
                }
                catch (Exception ex)
                {
                    server.LogVerbose(string.Format("MaintainClientConnection.SocketException: {0}", ex));
                }

                // Shutdown and end connection
                // client.Close();
            }
        }



        public override string LogPrefix()
        {
            return string.Empty;
        }
    }
}
