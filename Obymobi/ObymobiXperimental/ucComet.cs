﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Dionysos;
using Obymobi.Enums;
using Obymobi.Logic.Comet;
using Obymobi.Logic.Comet.Interfaces;
using Obymobi.Logic.Comet.Netmessages;
using Obymobi.Logic.Model;

namespace ObymobiXperimental
{
	public partial class UcComet : UserControl
	{
		[DllImport("User32.dll", CharSet = CharSet.Auto, EntryPoint = "SendMessage")]
		static extern IntPtr SendMessage(IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam);

		const int WM_VSCROLL = 277;
		const int SB_BOTTOM = 7;

		private CometClient cometClient;

		private readonly Connection connection = new Connection();
		private readonly Netmessage message = new Netmessage();
		private readonly NetmessageSetClientType messageClientType = new NetmessageSetClientType();

		public UcComet()
		{
			InitializeComponent();

			// Connection tab
			this.tbHost.DataBindings.Add("Text", connection, "Host");
			this.tbIdentifier.DataBindings.Add("Text", connection, "Identifier");
			this.tbSalt.DataBindings.Add("Text", connection, "Salt");

			this.cmbClientType.DataSource = Enum.GetNames(typeof (NetmessageClientType));

			this.tbClientTypeClientId.DataBindings.Add("Text", messageClientType, "ClientId");
			this.tbClientTypeCompanyId.DataBindings.Add("Text", messageClientType, "CompanyId");
			this.tbClientTypeDeliverypointId.DataBindings.Add("Text", messageClientType, "DeliverypointId");
			this.tbClientTypeDeliverypointgroupId.DataBindings.Add("Text", messageClientType, "DeliverypointgroupId");
			this.tbClientTypeTerminalId.DataBindings.Add("Text", messageClientType, "TerminalId");
			this.cbClientTypeMasterConsole.DataBindings.Add("Checked", messageClientType, "IsMasterConsole");

			// Message tab
			this.cmbMessageType.DataSource = Enum.GetNames(typeof (NetmessageType));
			this.tbReceiverClientId.DataBindings.Add("Text", message, "ReceiverClientId");
			this.tbReceiverCompanyId.DataBindings.Add("Text", message, "ReceiverCompanyId");
			this.tbReceiverDeliverypointId.DataBindings.Add("Text", message, "ReceiverDeliverypointId");
			this.tbReceiverTerminalId.DataBindings.Add("Text", message, "ReceiverTerminalId");
			this.tbSenderClientId.DataBindings.Add("Text", message, "SenderClientId");
			this.tbSenderCompanyId.DataBindings.Add("Text", message, "SenderCompanyId");
			this.tbSenderDeliverypointId.DataBindings.Add("Text", message, "SenderDeliverypointId");
			this.tbSenderTerminalId.DataBindings.Add("Text", message, "SenderTerminalId");
			this.tbFieldValue1.DataBindings.Add("Text", message, "FieldValue1");
			this.tbFieldValue2.DataBindings.Add("Text", message, "FieldValue2");
			this.tbFieldValue3.DataBindings.Add("Text", message, "FieldValue3");
			this.tbFieldValue4.DataBindings.Add("Text", message, "FieldValue4");
			this.tbFieldValue5.DataBindings.Add("Text", message, "FieldValue5");
			this.tbFieldValue6.DataBindings.Add("Text", message, "FieldValue6");
			this.tbFieldValue7.DataBindings.Add("Text", message, "FieldValue7");
			this.tbFieldValue8.DataBindings.Add("Text", message, "FieldValue8");
			this.tbFieldValue9.DataBindings.Add("Text", message, "FieldValue9");
			this.tbFieldValue10.DataBindings.Add("Text", message, "FieldValue10");

			SetDeveloperSettings();
		}

		private void SetDeveloperSettings()
		{
            connection.Host = "http://localhost/messaging/signalr";

			if (TestUtil.IsPcBattleStationDanny)
			{	
				//connection.Salt = "k4bxdpgic5l2hf6zta8r0yojwsun9vq7em31";
				//connection.Identifier = "98:52:b1:32:5c:9e";
				connection.Identifier = CometConstants.CometIdentifierWebservice;
				connection.Salt = CometConstants.CometInternalSalt;
			}
		}

		#region Models

		class CometEventListener : ICometClientEventListener
		{
			private readonly UcComet parent;

			public CometEventListener(UcComet p)
			{
				parent = p;
			}

			public void OnConnectFailed()
			{
				parent.WriteToLog("Connection Failed!");
				parent.OnDisconnect();
			}

			public void OnConnectionChanged(bool isConnected)
			{
				if (isConnected)
				{
					parent.WriteToLog("Connected!");
					parent.Authenticate();
				}
				else
				{
					parent.WriteToLog("Connection Lost!");
					parent.OnDisconnect();
				}
			}

			public void OnNetmessageReceived(Netmessage netmessage)
			{
				parent.WriteToLog("Received Netmessage: {0}", netmessage.GetMessageType());

				if (netmessage.MessageType == NetmessageType.AuthenticateResult)
				{
					var message = netmessage.ConvertTo<NetmessageAuthenticateResult>();
					parent.cometClient.IsAuthenticated = message.IsAuthenticated;
					if (message.IsAuthenticated)
					{
						parent.SetClientType();
						parent.WriteToLog("Authenticated, ready to send messages now!");
						parent.OnConnected();
					}
					else
					{
						parent.WriteToLog("Failed to authenticate. Error: {0}", message.AuthenticateErrorMessage);
						parent.OnDisconnect();
					}
				}
				else if (netmessage.MessageType == NetmessageType.Ping)
				{
					parent.Pong();
				}
				else if (netmessage.MessageType == NetmessageType.Disconnect)
				{
					OnConnectionChanged(false);
				}
			}

		    public void OnEventLog(string message, params object[] args)
		    {
                parent.WriteToLog(message, args);
		    }
		}

		class Connection
		{
			public string Host { get; set; }
			public string Identifier { get; set; }
			public string Salt { get; set; }

			public NetmessageClientType ClientType { private get; set; }

			public bool Validate()
			{
				return !(Host.IsNullOrWhiteSpace() ||
				         Identifier.IsNullOrWhiteSpace() ||
				         Salt.IsNullOrWhiteSpace() ||
				         ClientType == NetmessageClientType.Unknown);
			}
		}

		#endregion

		private void ConnectToServer()
		{
			if (connection.Validate())
			{
				btnConnect.Enabled = false;
				gbConnection.Enabled = false;
				gbClientInfo.Enabled = false;

                WriteToLog("Connecting to Comet host: {0}", connection.Host);
                cometClient = new SignalRClient(new CometEventListener(this), connection.Host, connection.Identifier, connection.Salt);
                //cometClient = new PokeInClient(new CometEventListener(this), connection.Host, connection.Identifier, connection.Salt);
                cometClient.Connect();
			}
			else
			{
				WriteToLog("Missing essentials parameters. Check if all fields are filled in.");
			}
		}

		private void Authenticate()
		{
			if (!cometClient.IsAuthenticated)
			{
				WriteToLog("Sending authentication request. Identifier: {0} | Salt: {1}", connection.Identifier, connection.Salt);
				cometClient.Authenticate(connection.Identifier, connection.Salt);
			}
			else
			{
				WriteToLog("Already authenticated. Ready to send messages!");
			}
		}

		private void SetClientType()
		{
			if (cometClient.IsAuthenticated)
			{
				cometClient.SendMessage(messageClientType);
			}
		}

		private void OnConnected()
		{
			if (btnConnect.InvokeRequired)
			{
				this.Invoke((MethodInvoker)OnConnected);
			}
			else
			{
				btnConnect.Enabled = true;
				btnConnect.Text = @"Disconnect";
			}
		}

		private void OnDisconnect()
		{
			if (btnConnect.InvokeRequired)
			{
				this.Invoke((MethodInvoker)OnDisconnect);
			}
			else
			{
				gbConnection.Enabled = true;
				gbClientInfo.Enabled = true;
				btnConnect.Text = @"Connect";
			    btnConnect.Enabled = true;
			}
			
		}

		private void Pong()
		{
			cometClient.Pong();
		}

		public void WriteToLog(string log, params object[] args)
		{
			if (this.tbEventLog.InvokeRequired)
			{
				this.tbEventLog.Invoke((MethodInvoker) (() => this.WriteToLog(log, args)));
			}
			else
			{
				this.tbEventLog.AppendText(log.FormatSafe(args) + "\r\n");
			}
		}

		#region Events

		private void btnConnect_Click(object sender, EventArgs e)
		{
			if (cometClient == null || !cometClient.IsConnected())
			{
				ConnectToServer();
			}
			else
			{
				cometClient.Disconnect();
			}
		}

		private void btnSendMessage_Click(object sender, EventArgs e)
		{
			WriteToLog("Sending message '{0}'", message.MessageType);

			if (cometClient.IsAuthenticated)
			{
				cometClient.SendMessage(message);
			}
		}

		private void tbEventLog_TextChanged(object sender, EventArgs e)
		{
			var ptrWparam = new IntPtr(SB_BOTTOM);
			var ptrLparam = new IntPtr(0);
			SendMessage(this.tbEventLog.Handle, WM_VSCROLL, ptrWparam, ptrLparam);
		}

		private void cmbMessageType_SelectedIndexChanged(object sender, EventArgs e)
		{
			var value = Enum.Parse(typeof(NetmessageType), cmbMessageType.SelectedValue.ToString());
			message.SetMessageType((NetmessageType)value);
		}

		private void cbClientType_SelectedIndexChanged(object sender, EventArgs e)
		{
			bool clientId = false;
			bool companyId = false;
			bool deliverypointId = false;
			bool deliverypointgroupId = false;
			bool terminalId = false;
			bool masterConsole = false;

			var value = (NetmessageClientType)Enum.Parse(typeof(NetmessageClientType), cmbClientType.SelectedValue.ToString());
			messageClientType.ClientType = value;
			connection.ClientType = value;
			switch (value)
			{
				case NetmessageClientType.Unknown:
					break;
				case NetmessageClientType.Emenu:
					clientId = true;
					companyId = true;
					deliverypointId = true;
					deliverypointgroupId = true;
					break;
				case NetmessageClientType.Console:
					terminalId = true;
					masterConsole = true;
					break;
				case NetmessageClientType.OnsiteServer:
					terminalId = true;
					break;
				case NetmessageClientType.SupportTools:
					break;
				case NetmessageClientType.Cms:
					break;
				case NetmessageClientType.Webservice:
					break;
                case NetmessageClientType.MobileNoc:
                    break;
				default:
					throw new ArgumentOutOfRangeException();
			}

			tbClientTypeClientId.Enabled = clientId;
			tbClientTypeCompanyId.Enabled = companyId;
			tbClientTypeDeliverypointId.Enabled = deliverypointId;
			tbClientTypeDeliverypointgroupId.Enabled = deliverypointgroupId;
			tbClientTypeTerminalId.Enabled = terminalId;
			cbClientTypeMasterConsole.Enabled = masterConsole;
		}

		#endregion
	}
}
