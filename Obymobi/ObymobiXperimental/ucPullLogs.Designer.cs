﻿namespace ObymobiXperimental
{
    partial class ucPullLogs
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbEventLog = new System.Windows.Forms.TextBox();
            this.btnFetchLogs = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cbCompany = new System.Windows.Forms.ComboBox();
            this.btnStop = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tbEventLog
            // 
            this.tbEventLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbEventLog.Location = new System.Drawing.Point(3, 59);
            this.tbEventLog.Multiline = true;
            this.tbEventLog.Name = "tbEventLog";
            this.tbEventLog.ReadOnly = true;
            this.tbEventLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbEventLog.Size = new System.Drawing.Size(740, 480);
            this.tbEventLog.TabIndex = 0;
            // 
            // btnFetchLogs
            // 
            this.btnFetchLogs.Location = new System.Drawing.Point(60, 30);
            this.btnFetchLogs.Name = "btnFetchLogs";
            this.btnFetchLogs.Size = new System.Drawing.Size(107, 23);
            this.btnFetchLogs.TabIndex = 1;
            this.btnFetchLogs.Text = "Fetch Logs";
            this.btnFetchLogs.UseVisualStyleBackColor = true;
            this.btnFetchLogs.Click += new System.EventHandler(this.btnFetchLogs_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Company";
            // 
            // cbCompany
            // 
            this.cbCompany.FormattingEnabled = true;
            this.cbCompany.Location = new System.Drawing.Point(60, 3);
            this.cbCompany.Name = "cbCompany";
            this.cbCompany.Size = new System.Drawing.Size(233, 21);
            this.cbCompany.TabIndex = 3;
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(173, 30);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(75, 23);
            this.btnStop.TabIndex = 4;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // ucPullLogs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.cbCompany);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnFetchLogs);
            this.Controls.Add(this.tbEventLog);
            this.Name = "ucPullLogs";
            this.Size = new System.Drawing.Size(746, 542);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbEventLog;
        private System.Windows.Forms.Button btnFetchLogs;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbCompany;
        private System.Windows.Forms.Button btnStop;
    }
}
