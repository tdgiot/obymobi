﻿using Dionysos;
using Dionysos.Diagnostics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace ObymobiXperimental
{
    public class TcpTestClient : LoggingClassBase
    {
        private string logPrefix = "Client";
        string server;
        int port;
        string message;
        string tuple;
        public TcpTestClient(String server, int port, string message)
        {
            this.server = server;
            this.port = port;
            this.message = message;
              
        }

        public void Start()
        {
            int step = 0;
            try
            {                
                this.logPrefix += "-" + server + ":" + port;

                step = 10;

                // Create a TcpClient.
                // Note, for this client to work you need to have a TcpServer 
                // connected to the same address as specified by the server, port
                // combination.                
                TcpClient client = new TcpClient(server, port);
                step = 20;
                // Translate the passed message into ASCII and store it as a Byte array.
                this.tuple = string.Format("{0}:{1}-{2}:{3}",
                    ((IPEndPoint)client.Client.LocalEndPoint).Address,
                    ((IPEndPoint)client.Client.LocalEndPoint).Port,
                    ((IPEndPoint)client.Client.RemoteEndPoint).Address,
                    ((IPEndPoint)client.Client.RemoteEndPoint).Port);
                this.tuple = ((IPEndPoint)client.Client.LocalEndPoint).Port.ToString();
                this.logPrefix = " - Client-" + this.tuple;
                step = 30;

                // Get a client stream for reading and writing.                
                while (true)
                {
                    NetworkStream stream = client.GetStream();
                    stream.ReadTimeout = 120000;
                    step = 35;
                    // Send the message to the connected TcpServer. 
                    string textToSend = this.tuple + "- Client to Server - " + DateTime.Now.ToString();                                
                    Byte[] data = System.Text.Encoding.ASCII.GetBytes(textToSend);
                    stream.Write(data, 0, data.Length);
                    step = 40;
                    //this.LogVerbose("Client Sent: {0}", message);

                    // Receive the TcpServer.response.

                    // Buffer to store the response bytes.
                    data = new Byte[256];
                    step = 50;
                    // String to store the response ASCII representation.
                    String responseData = String.Empty;

                    // Read the first batch of the TcpServer response bytes.
                    Int32 bytes = stream.Read(data, 0, data.Length);
                    step = 60;
                    responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
                    step = 70;
                    //this.LogVerbose("Client Received: {0}", responseData);
                    step = 80;

                    // Debug.WriteLine("Completed action for: " + this.message);

                    this.LogVerbose("Received: {0}", responseData);

                    Thread.Sleep(4000);
                }
                
   
                // Close everything.
                // client.Close();
            }
            catch (ArgumentNullException e)
            {
                this.LogVerbose(string.Format("ArgumentNullException: {0} - at Step {1}", e, step));
            }
            catch (SocketException e)
            {
                this.LogVerbose(string.Format("SocketException: {0} - at Step {1}", e, step));
            }
            catch (Exception e)
            {
                this.LogVerbose(string.Format("Exception: {0} - at Step {1}", e, step));
            }

            this.LogVerbose("\n Client finished: " + this.message);            
        }

        public override string LogPrefix()
        {
            return this.logPrefix;
        }
    }
}
