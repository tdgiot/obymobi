﻿namespace ObymobiXperimental
{
    partial class ucPorts
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbServerPort = new System.Windows.Forms.TextBox();
            this.cbServerIp = new System.Windows.Forms.ComboBox();
            this.btStartServer = new System.Windows.Forms.Button();
            this.btStopServer = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tbClientPort = new System.Windows.Forms.TextBox();
            this.tbClientIp = new System.Windows.Forms.TextBox();
            this.btStartClient = new System.Windows.Forms.Button();
            this.tbLog = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbClientInstances = new System.Windows.Forms.TextBox();
            this.cbCloseClientConnection = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblOpenedByServer = new System.Windows.Forms.Label();
            this.lblOpenedTotal = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Server IP";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Server Port";
            // 
            // tbServerPort
            // 
            this.tbServerPort.Location = new System.Drawing.Point(84, 36);
            this.tbServerPort.Name = "tbServerPort";
            this.tbServerPort.Size = new System.Drawing.Size(121, 20);
            this.tbServerPort.TabIndex = 3;
            this.tbServerPort.Text = "13000";
            // 
            // cbServerIp
            // 
            this.cbServerIp.FormattingEnabled = true;
            this.cbServerIp.Location = new System.Drawing.Point(84, 7);
            this.cbServerIp.Name = "cbServerIp";
            this.cbServerIp.Size = new System.Drawing.Size(121, 21);
            this.cbServerIp.TabIndex = 4;
            this.cbServerIp.TextChanged += new System.EventHandler(this.cbServerIp_TextChanged);
            // 
            // btStartServer
            // 
            this.btStartServer.Location = new System.Drawing.Point(21, 74);
            this.btStartServer.Name = "btStartServer";
            this.btStartServer.Size = new System.Drawing.Size(75, 23);
            this.btStartServer.TabIndex = 5;
            this.btStartServer.Text = "Start";
            this.btStartServer.UseVisualStyleBackColor = true;
            this.btStartServer.Click += new System.EventHandler(this.btStartServer_Click);
            // 
            // btStopServer
            // 
            this.btStopServer.Location = new System.Drawing.Point(130, 74);
            this.btStopServer.Name = "btStopServer";
            this.btStopServer.Size = new System.Drawing.Size(75, 23);
            this.btStopServer.TabIndex = 6;
            this.btStopServer.Text = "Stop";
            this.btStopServer.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(396, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Client Connect IP";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(396, 35);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Client Port";
            // 
            // tbClientPort
            // 
            this.tbClientPort.Location = new System.Drawing.Point(500, 32);
            this.tbClientPort.Name = "tbClientPort";
            this.tbClientPort.Size = new System.Drawing.Size(121, 20);
            this.tbClientPort.TabIndex = 9;
            this.tbClientPort.Text = "13000";
            // 
            // tbClientIp
            // 
            this.tbClientIp.Location = new System.Drawing.Point(500, 7);
            this.tbClientIp.Name = "tbClientIp";
            this.tbClientIp.Size = new System.Drawing.Size(121, 20);
            this.tbClientIp.TabIndex = 10;
            // 
            // btStartClient
            // 
            this.btStartClient.Location = new System.Drawing.Point(500, 113);
            this.btStartClient.Name = "btStartClient";
            this.btStartClient.Size = new System.Drawing.Size(75, 23);
            this.btStartClient.TabIndex = 11;
            this.btStartClient.Text = "Start";
            this.btStartClient.UseVisualStyleBackColor = true;
            this.btStartClient.Click += new System.EventHandler(this.btStartClient_Click);
            // 
            // tbLog
            // 
            this.tbLog.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbLog.Location = new System.Drawing.Point(2, 177);
            this.tbLog.Multiline = true;
            this.tbLog.Name = "tbLog";
            this.tbLog.Size = new System.Drawing.Size(944, 418);
            this.tbLog.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(396, 61);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Instances";
            // 
            // tbClientInstances
            // 
            this.tbClientInstances.Location = new System.Drawing.Point(500, 58);
            this.tbClientInstances.Name = "tbClientInstances";
            this.tbClientInstances.Size = new System.Drawing.Size(121, 20);
            this.tbClientInstances.TabIndex = 14;
            this.tbClientInstances.Text = "1";
            // 
            // cbCloseClientConnection
            // 
            this.cbCloseClientConnection.AutoSize = true;
            this.cbCloseClientConnection.Checked = true;
            this.cbCloseClientConnection.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbCloseClientConnection.Location = new System.Drawing.Point(500, 87);
            this.cbCloseClientConnection.Name = "cbCloseClientConnection";
            this.cbCloseClientConnection.Size = new System.Drawing.Size(108, 17);
            this.cbCloseClientConnection.TabIndex = 16;
            this.cbCloseClientConnection.Text = "Close connection";
            this.cbCloseClientConnection.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(18, 118);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(116, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "Ports opened by server";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(18, 140);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(93, 13);
            this.label7.TabIndex = 18;
            this.label7.Text = "Ports opened total";
            // 
            // lblOpenedByServer
            // 
            this.lblOpenedByServer.AutoSize = true;
            this.lblOpenedByServer.Location = new System.Drawing.Point(151, 118);
            this.lblOpenedByServer.Name = "lblOpenedByServer";
            this.lblOpenedByServer.Size = new System.Drawing.Size(13, 13);
            this.lblOpenedByServer.TabIndex = 19;
            this.lblOpenedByServer.Text = "0";
            // 
            // lblOpenedTotal
            // 
            this.lblOpenedTotal.AutoSize = true;
            this.lblOpenedTotal.Location = new System.Drawing.Point(151, 140);
            this.lblOpenedTotal.Name = "lblOpenedTotal";
            this.lblOpenedTotal.Size = new System.Drawing.Size(13, 13);
            this.lblOpenedTotal.TabIndex = 20;
            this.lblOpenedTotal.Text = "0";
            // 
            // ucPorts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblOpenedTotal);
            this.Controls.Add(this.lblOpenedByServer);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cbCloseClientConnection);
            this.Controls.Add(this.tbClientInstances);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tbLog);
            this.Controls.Add(this.btStartClient);
            this.Controls.Add(this.tbClientIp);
            this.Controls.Add(this.tbClientPort);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btStopServer);
            this.Controls.Add(this.btStartServer);
            this.Controls.Add(this.cbServerIp);
            this.Controls.Add(this.tbServerPort);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "ucPorts";
            this.Size = new System.Drawing.Size(950, 600);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbServerPort;
        private System.Windows.Forms.ComboBox cbServerIp;
        private System.Windows.Forms.Button btStartServer;
        private System.Windows.Forms.Button btStopServer;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbClientPort;
        private System.Windows.Forms.TextBox tbClientIp;
        private System.Windows.Forms.Button btStartClient;
        private System.Windows.Forms.TextBox tbLog;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbClientInstances;
        private System.Windows.Forms.CheckBox cbCloseClientConnection;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblOpenedByServer;
        private System.Windows.Forms.Label lblOpenedTotal;
    }
}
