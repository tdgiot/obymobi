﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dionysos;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using Obymobi.Security;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace ObymobiXperimental
{
    public partial class ucWebserviceLoadTester : UserControl
    {
        bool GetNetmessages { get; set; }
        bool GetRelease { get; set; }
        bool GetSetClientStatus { get; set; }
        bool GetClientStatuses { get; set; }
        bool GetReleaseAndDownloadLocation { get; set; }

        private int runningClients = 0;
        private readonly object runningClientsLock = new object();

        readonly Dictionary<int, Task> spawnedClients = new Dictionary<int, Task>();

        public ucWebserviceLoadTester()
        {
            InitializeComponent();
        }

        private void UpdateRunningClients(int v)
        {

            if (this.lblRunningClients.InvokeRequired)
            {
                this.lblRunningClients.Invoke((MethodInvoker) (() => UpdateRunningClients(v)));
            }
            else
            {
                lock (runningClientsLock)
                {
                    runningClients += v;
                    this.lblRunningClients.Text = runningClients.ToString(CultureInfo.InvariantCulture);

                    if (runningClients == 0)
                    {
                        this.btnStressTest.Enabled = true;
                        this.gbWebserviceCalls.Enabled = true;
                        this.numOfCalls.Enabled = true;
                        this.numOfClients.Enabled = true;
                    }
                }
            }
        }

        private void btnStressTest_Click(object sender, EventArgs e)
        {
            if (this.tbWebserviceURL.Text.IsNullOrWhiteSpace())
            {
                MessageBox.Show("Webservice url can not be empty.");
                return;
            }

            this.btnStressTest.Enabled = false;
            this.gbWebserviceCalls.Enabled = false;
            this.numOfCalls.Enabled = false;
            this.numOfClients.Enabled = false;

            var filter = new PredicateExpression();
            filter.Add(ClientFields.DeviceId != DBNull.Value);
            //filter.Add(ClientFields.ClientId != spawnedClientProcesses.Keys.ToList());

            var relations = new RelationCollection();
            relations.Add(ClientEntityBase.Relations.DeviceEntityUsingDeviceId);
            relations.Add(ClientEntityBase.Relations.CompanyEntityUsingCompanyId);

            var clientCollection = new ClientCollection();
            clientCollection.GetMulti(filter, (long)numOfClients.Value, null, relations);

            var terminalFilter = new PredicateExpression();
            terminalFilter.Add(TerminalFields.DeviceId != DBNull.Value);

            var terminalRelations = new RelationCollection();
            relations.Add(TerminalEntityBase.Relations.DeviceEntityUsingDeviceId);
            relations.Add(TerminalEntityBase.Relations.CompanyEntityUsingCompanyId);

            var terminalCollection = new TerminalCollection();
            terminalCollection.GetMulti(terminalFilter, terminalRelations);

            for (int i = 0; i < this.numOfClients.Value; i++)
            {
                int id = i;

                UpdateRunningClients(1);

                var task = Task.Factory.StartNew(() =>
                    {
                        var clientEntity = clientCollection[id];

                        var webservice = new CraveService21.CraveService();
                        webservice.Url = this.tbWebserviceURL.Text;

                        for (int j = 0; j < this.numOfCalls.Value; j++)
                        {
                            Thread.Sleep(RandomUtil.GetRandomNumber(5000));

                            long timestamp = DateTime.UtcNow.ToUnixTime();

                            if (GetNetmessages)
                            {
                                string hash = Hasher.GetHashFromParameters(clientEntity.CompanyEntity.Salt, timestamp, clientEntity.MacAddress, 0, clientEntity.ClientId, clientEntity.CompanyId, clientEntity.DeliverypointId.GetValueOrDefault(0), 0);
                                var result = webservice.GetNetmessages(timestamp, clientEntity.MacAddress, 0, clientEntity.ClientId, clientEntity.CompanyId, clientEntity.DeliverypointId.GetValueOrDefault(0), 0, hash);
                                if (result != null)
                                {
                                    Debug.WriteLine("GetNetmessage Result: {0}", result.ResultCode);
                                }
                            }

                            if (GetRelease)
                            {
                                string hash = Hasher.GetHashFromParameters(clientEntity.CompanyEntity.Salt, timestamp, clientEntity.MacAddress, "CraveEmenuAndroidTablet");
                                var result = webservice.GetRelease(timestamp, clientEntity.MacAddress, "CraveEmenuAndroidTablet", hash);
                                if (result != null)
                                {
                                    Debug.WriteLine("GetRelease Result: {0}", result.ResultCode);
                                }
                            }

                            if (GetSetClientStatus)
                            {
                                var status = new ClientStatus();
                                status.ApplicationVersion = "Load Tester " + RandomUtil.GetRandomNumber(1337);
                                status.BatteryLevel = RandomUtil.GetRandomNumber(100);
                                status.AgentIsRunning = true;
                                status.IsCharging = true;
                                status.IpAddress = "127.0.0." + RandomUtil.GetRandomNumber(254);
                                status.ClientOperationMode = (int)ClientOperationMode.Ordering;
                                status.PendingOrderIdsOnEmenu = new int[0];

                                var statusXml = status.toXml();
                                statusXml = statusXml.Replace("<?xml version=\"1.0\"?>\r\n", "");
                                statusXml = statusXml.Replace("<ClientStatus xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">", "<ClientStatus>");
                                statusXml = statusXml.Replace("\r\n  ", "");

                                string hash = Hasher.GetHashFromParameters(clientEntity.CompanyEntity.Salt, timestamp, clientEntity.MacAddress, statusXml);
                                var result = webservice.GetSetClientStatus(timestamp, clientEntity.MacAddress, statusXml, hash);
                                if (result != null)
                                {
                                    Debug.WriteLine("GetSetClientStatus Result: {0}", result.ResultCode);
                                }
                            }

                            if (GetClientStatuses)
                            {
                                // Get random terminal
                                var terminalEntity = terminalCollection[RandomUtil.GetRandomNumber(terminalCollection.Count - 1)];

                                var hash = Hasher.GetHashFromParameters(terminalEntity.CompanyEntity.Salt, timestamp, terminalEntity.DeviceEntity.Identifier);
                                var result = webservice.GetClientStatuses(timestamp, terminalEntity.DeviceEntity.Identifier, hash);
                                if (result != null)
                                {
                                    Debug.WriteLine("GetClientStatuses Result: {0}", result.ResultCode);
                                }
                            }

                            if (GetReleaseAndDownloadLocation)
                            {
                                var hash = Hasher.GetHashFromParameters(clientEntity.CompanyEntity.Salt, clientEntity.MacAddress, "CraveEmenuAndroidTablet");
                                var result = webservice.GetReleaseAndDownloadLocation(timestamp, clientEntity.MacAddress, "CraveEmenuAndroidTablet", hash);
                                Debug.WriteLine("GetReleaseAndDownloadLocation Result: {0}", result);
                            }
                        }

                        UpdateRunningClients(-1);

                        spawnedClients.Remove(id);
                    });

                spawnedClients.Add(id, task);
            }
        }

        private void cbCalls_CheckedChanged(object sender, EventArgs e)
        {
            if (sender.Equals(this.cbGetNetmessages))
            {
                GetNetmessages = this.cbGetNetmessages.Checked;
            }
            else if (sender.Equals(this.cbGetRelease))
            {
                GetRelease = this.cbGetRelease.Checked;
            }
            else if (sender.Equals(this.cbGetSetClientStatus))
            {
                GetSetClientStatus = this.cbGetSetClientStatus.Checked;
            }
            else if (sender.Equals(this.cbGetClientStatuses))
            {
                GetClientStatuses = this.cbGetClientStatuses.Checked;
            }
            else if (sender.Equals(this.cbGetReleaseAndDownloadLocation))
            {
                GetReleaseAndDownloadLocation = this.cbGetReleaseAndDownloadLocation.Checked;
            }
        }

        private void btnCancelWork_Click(object sender, EventArgs e)
        {

        }
    }
}
