﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Mobile.Data.SupportClasses
{
    [Serializable]
    public class EntityBase
    {
        public T Clone<T>() where T : EntityBase
        {
            // PILS Implement
            return (T)this;
        }
    }
}
