﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Mobile.Data.SupportClasses
{
    [Serializable]
    public class EntityCollection<T> : List<T> where T : EntityBase
    {       
    }
}
