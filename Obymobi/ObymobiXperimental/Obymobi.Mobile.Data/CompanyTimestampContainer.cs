﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Mobile.Data
{
    [Serializable]
    public class CompanyTimestampContainer
    {
        public CompanyTimestampContainer()
        {
            this.CompanySavedVersionTicks = 0;
        }

        public long CompanySavedVersionTicks { get; set; }
    }
}
