﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Mobile.Data
{
    [Serializable]
    public class ApplicationTimestampContainer
    {
        public ApplicationTimestampContainer()
        {
            this.CompanyListSavedVersionTicks = 0;
        }

        public long CompanyListSavedVersionTicks { get; set; }
    }
}
