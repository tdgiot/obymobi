﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Mobile.Data
{
    [Serializable]
    public class MenuTimestampContainer
    {
        public MenuTimestampContainer()
        {
            this.MenuSavedVersionTicks = 0;
        }

        public long MenuSavedVersionTicks { get; set; }
    }
}
