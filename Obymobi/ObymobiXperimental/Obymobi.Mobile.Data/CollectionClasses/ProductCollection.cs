using System;
using Obymobi.Mobile.Data.SupportClasses;
using Obymobi.Mobile.Data.EntityClasses;

namespace Obymobi.Mobile.Data.CollectionClasses
{
    [Serializable]
	public class ProductCollection : EntityCollection<ProductEntity>
	{
		#region Constructors

		public ProductCollection()
		{
		}

		#endregion
	}
}
