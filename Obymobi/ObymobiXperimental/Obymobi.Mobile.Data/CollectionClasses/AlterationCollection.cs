using System;
using Obymobi.Mobile.Data.SupportClasses;
using Obymobi.Mobile.Data.EntityClasses;

namespace Obymobi.Mobile.Data.CollectionClasses
{
    [Serializable]
	public class AlterationCollection : EntityCollection<AlterationEntity>
	{
		#region Constructors

		public AlterationCollection()
		{
		}

		#endregion
	}
}
