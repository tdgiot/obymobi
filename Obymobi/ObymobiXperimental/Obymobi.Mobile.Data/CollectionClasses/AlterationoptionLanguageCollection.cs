using System;
using Obymobi.Mobile.Data.SupportClasses;
using Obymobi.Mobile.Data.EntityClasses;

namespace Obymobi.Mobile.Data.CollectionClasses
{
    [Serializable]
	public class AlterationoptionLanguageCollection : EntityCollection<AlterationoptionLanguageEntity>
	{
		#region Constructors

		public AlterationoptionLanguageCollection()
		{
		}

		#endregion
	}
}
