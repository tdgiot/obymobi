using System;
using Obymobi.Mobile.Data.SupportClasses;
using Obymobi.Mobile.Data.EntityClasses;

namespace Obymobi.Mobile.Data.CollectionClasses
{
    [Serializable]
	public class AlterationoptionCollection : EntityCollection<AlterationoptionEntity>
	{
		#region Constructors

		public AlterationoptionCollection()
		{
		}

		#endregion
	}
}
