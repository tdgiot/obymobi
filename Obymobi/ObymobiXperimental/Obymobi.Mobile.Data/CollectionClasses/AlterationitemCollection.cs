using System;
using Obymobi.Mobile.Data.SupportClasses;
using Obymobi.Mobile.Data.EntityClasses;

namespace Obymobi.Mobile.Data.CollectionClasses
{
    [Serializable]
	public class AlterationitemCollection : EntityCollection<AlterationitemEntity>
	{
		#region Constructors

		public AlterationitemCollection()
		{
		}

		#endregion
	}
}
