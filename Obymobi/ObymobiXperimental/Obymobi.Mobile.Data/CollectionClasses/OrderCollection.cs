using System;
using Obymobi.Mobile.Data.SupportClasses;
using Obymobi.Mobile.Data.EntityClasses;

namespace Obymobi.Mobile.Data.CollectionClasses
{
    [Serializable]
	public class OrderCollection : EntityCollection<OrderEntity>
	{
		#region Constructors

		public OrderCollection()
		{
		}

		#endregion
	}
}
