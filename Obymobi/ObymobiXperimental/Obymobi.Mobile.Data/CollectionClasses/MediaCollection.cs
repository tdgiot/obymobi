using System;
using Obymobi.Mobile.Data.SupportClasses;
using Obymobi.Mobile.Data.EntityClasses;

namespace Obymobi.Mobile.Data.CollectionClasses
{
    [Serializable]
	public class MediaCollection : EntityCollection<MediaEntity>
	{
		#region Constructors

		public MediaCollection()
		{
		}

		#endregion
	}
}
