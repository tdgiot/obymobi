using System;
using Obymobi.Mobile.Data.SupportClasses;
using Obymobi.Mobile.Data.EntityClasses;

namespace Obymobi.Mobile.Data.CollectionClasses
{
    [Serializable]
	public class LanguageCollection : EntityCollection<LanguageEntity>
	{
		#region Constructors

		public LanguageCollection()
		{
		}

		#endregion
	}
}
