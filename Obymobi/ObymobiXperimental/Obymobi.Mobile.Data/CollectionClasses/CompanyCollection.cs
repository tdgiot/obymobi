using System;
using Obymobi.Mobile.Data.SupportClasses;
using Obymobi.Mobile.Data.EntityClasses;

namespace Obymobi.Mobile.Data.CollectionClasses
{
    [Serializable]
	public class CompanyCollection : EntityCollection<CompanyEntity>
	{
		#region Constructors

		public CompanyCollection()
		{
		}

		#endregion
	}
}
