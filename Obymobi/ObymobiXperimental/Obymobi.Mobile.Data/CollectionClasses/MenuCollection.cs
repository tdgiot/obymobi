using System;
using Obymobi.Mobile.Data.SupportClasses;
using Obymobi.Mobile.Data.EntityClasses;

namespace Obymobi.Mobile.Data.CollectionClasses
{
    [Serializable]
	public class MenuCollection : EntityCollection<MenuEntity>
	{
		#region Constructors

		public MenuCollection()
		{
		}

		#endregion
	}
}
