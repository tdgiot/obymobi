using System;
using Obymobi.Mobile.Data.SupportClasses;
using Obymobi.Mobile.Data.EntityClasses;

namespace Obymobi.Mobile.Data.CollectionClasses
{
    [Serializable]
	public class DeliverypointgroupCollection : EntityCollection<DeliverypointgroupEntity>
	{
		#region Constructors

		public DeliverypointgroupCollection()
		{
		}

		#endregion
	}
}
