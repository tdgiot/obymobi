using System;
using Obymobi.Mobile.Data.SupportClasses;
using Obymobi.Mobile.Data.EntityClasses;

namespace Obymobi.Mobile.Data.CollectionClasses
{
    [Serializable]
	public class MediaRatioTypeMediaCollection : EntityCollection<MediaRatioTypeMediaEntity>
	{
		#region Constructors

		public MediaRatioTypeMediaCollection()
		{
		}

		#endregion
	}
}
