using System;
using Obymobi.Mobile.Data.SupportClasses;
using Obymobi.Mobile.Data.EntityClasses;

namespace Obymobi.Mobile.Data.CollectionClasses
{
    [Serializable]
	public class DeliverypointCollection : EntityCollection<DeliverypointEntity>
	{
		#region Constructors

		public DeliverypointCollection()
		{
		}

		#endregion
	}
}
