using System;
using Obymobi.Mobile.Data.SupportClasses;
using Obymobi.Mobile.Data.EntityClasses;

namespace Obymobi.Mobile.Data.CollectionClasses
{
    [Serializable]
	public class CategoryCollection : EntityCollection<CategoryEntity>
	{
		#region Constructors

		public CategoryCollection()
		{
		}

		#endregion
	}
}
