using System;
using Obymobi.Mobile.Data.SupportClasses;
using Obymobi.Mobile.Data.EntityClasses;

namespace Obymobi.Mobile.Data.CollectionClasses
{
    [Serializable]
	public class CategoryLanguageCollection : EntityCollection<CategoryLanguageEntity>
	{
		#region Constructors

		public CategoryLanguageCollection()
		{
		}

		#endregion
	}
}
