using System;
using Obymobi.Mobile.Data.SupportClasses;
using Obymobi.Mobile.Data.EntityClasses;

namespace Obymobi.Mobile.Data.CollectionClasses
{
    [Serializable]
	public class ProductLanguageCollection : EntityCollection<ProductLanguageEntity>
	{
		#region Constructors

		public ProductLanguageCollection()
		{
		}

		#endregion
	}
}
