using System;
using Obymobi.Mobile.Data.EntityBaseClasses;
using Obymobi.Mobile.Data.CollectionClasses;
using Obymobi.Enums;

namespace Obymobi.Mobile.Data.EntityClasses
{
    [Serializable]
	public class AlterationEntity : AlterationEntityBase
	{
        public enum AlterationEntityResult
        {
            TypeValueIsNotValidForAlterationTypeEnum = 200
        }

		#region Constructors

		public AlterationEntity()
		{
		}

		#endregion

        #region Properties

        public AlterationType TypeAsEnum
        {
            get
            { 
                AlterationType toReturn = AlterationType.Options;
                if (!EnumUtil.TryParse(this.Type, out toReturn))
                {
                    throw new ObymobiException(AlterationEntityResult.TypeValueIsNotValidForAlterationTypeEnum, "AlterationEntity.Type '{0}': Is not a valid value for AlterationType enum", this.Type);
                }
                return toReturn;
            }
        }

        #endregion
    }
}
