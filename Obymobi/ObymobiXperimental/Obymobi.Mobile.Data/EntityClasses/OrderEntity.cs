using System;
using Obymobi.Mobile.Data.EntityBaseClasses;
using Obymobi.Mobile.Data.CollectionClasses;
using Dionysos;

namespace Obymobi.Mobile.Data.EntityClasses
{
    [Serializable]
	public class OrderEntity : OrderEntityBase
	{
		#region Constructors

		public OrderEntity()
		{
            this.Guid = GuidUtil.CreateGuid();
            this.Orderitems = new OrderitemCollection();
		}

		#endregion

        internal void CompleteOrder()
        { 

        }

        public int DeliverypointgroupId { get; set; }
	}
}
