using System;
using Obymobi.Mobile.Data.EntityBaseClasses;
using Obymobi.Mobile.Data.CollectionClasses;
using Dionysos;

namespace Obymobi.Mobile.Data.EntityClasses
{
    [Serializable]
	public class AlterationitemEntity : AlterationitemEntityBase
	{
		#region Constructors

		public AlterationitemEntity()
		{
            this.Guid = GuidUtil.CreateGuid();
		}

		#endregion

        #region Properties

        // PILS In generator toevoegen
        public string AlterationoptionName { get; set; }

        #endregion
    }
}
