using System;
using Obymobi.Mobile.Data.EntityBaseClasses;
using Obymobi.Mobile.Data.CollectionClasses;
using System.Collections.Generic;

namespace Obymobi.Mobile.Data.EntityClasses
{
    [Serializable]
	public class MenuEntity : MenuEntityBase
	{
		#region Constructors

		public MenuEntity()
		{
		}

		#endregion

        private Dictionary<string, ProductEntity> productLookup = null;
        private Dictionary<int, CategoryEntity> categoryLookup = null;

        public ProductEntity GetProduct(int productId, int categoryId)
        {
            if (this.productLookup == null)
                this.BuildLookups();
            
            string id = productId + categoryId + "";

            ProductEntity toReturn = null;
            if (!this.productLookup.TryGetValue(id, out toReturn))
                toReturn = null;

            return toReturn;
        }

        public CategoryEntity GetCategory(int categoryId)
        {
            if (this.categoryLookup == null)
                this.BuildLookups();

            CategoryEntity toReturn = null;
            
            if (!this.categoryLookup.TryGetValue(categoryId, out toReturn))
                toReturn = null;

            return toReturn;
        }

        private void BuildLookups() // PILS maybe call this when laoding the menu, so it's always ready
        {
            this.productLookup = new Dictionary<string, ProductEntity>();
            this.categoryLookup = new Dictionary<int, CategoryEntity>();

            foreach (var category in this.Categories ?? new CategoryCollection()) // PILS should always be intied, remove when that's the case.
            {
                this.BuildLookupRecursive(category);
            }
        }        

        private void BuildLookupRecursive(CategoryEntity category)
        {
            // Add the Category
            if (this.categoryLookup.ContainsKey(category.CategoryId))
            {
                // PILS: Reactivate:
                // Log.Warning("MenuEntity", "BuildLookupRecursive", "Duplicate CategoryId found: {0}", category.CategoryId);
            }
            else
                this.categoryLookup.Add(category.CategoryId, category);

            foreach (var product in category.Products ?? new ProductCollection()) // PILS should always be intied, remove when that's the case.
            {
                string key = product.CategoryId + product.ProductId + "";
                if (this.productLookup.ContainsKey(key))
                {
                    // PILS: Reactivate:
                    // Log.Warning("MenuEntity", "BuildLookupRecursive", "Duplicate CategoryId/ProductId found: {0}/{1}", product.CategoryId, product.ProductId);
                }
                else
                    this.productLookup.Add(key, product);
            }

            foreach (var subCategory in category.Categories ?? new CategoryCollection()) // PILS should always be intied, remove when that's the case.
            {
                this.BuildLookupRecursive(subCategory);
            }            
        }

        // PILS needs to be the real ticks
        public long Version
        {
            get
            {
                return -1;
            }
        }
	}
}
