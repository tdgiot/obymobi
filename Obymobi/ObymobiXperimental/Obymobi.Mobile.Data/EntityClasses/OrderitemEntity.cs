using System;
using System.Linq;
using Obymobi.Mobile.Data.EntityBaseClasses;
using Obymobi.Mobile.Data.CollectionClasses;
//using Obymobi.Mobile.Managers;
using Dionysos;

namespace Obymobi.Mobile.Data.EntityClasses
{
    [Serializable]
	public class OrderitemEntity : OrderitemEntityBase
	{
		#region Constructors

		public OrderitemEntity()
		{
            this.Guid = GuidUtil.CreateGuid();
		}

		#endregion

        #region Methods

        ///// <summary>
        ///// Completes the orderitem (setting the Product, Alteration fields (i.e. Name, Price, etc.) to the local fields on the Orderitem)
        ///// This method assumes orderitem has been validated!
        ///// </summary>
        ///// <param name="orderitem">The orderitem.</param>
        //internal void CompleteOrderitem()
        //{ 
        //    // You should only throw in validated orderitems.

        //    // Product
        //    var product = CompanyManager.Menu.GetProduct(this.ProductId, this.CategoryId);
        //    if (product != null)
        //    {
        //        this.ProductName = product.Name;
        //        this.ProductPriceIn = product.PriceIn;
        //    }
        //    else
        //        Log.Warning("OrderitemEntity", "CompleteOrderitem", "Product could not be found: '{0}'", this.ProductId);


        //    foreach (var alterationitem in this.Alterationitems ?? new AlterationitemCollection())
        //    { 
        //        // Get Alteration
        //        var alteration = product.Alterations.FirstOrDefault(a => a.AlterationId == alterationitem.AlterationId);
        //        // Get AlterationOption
        //        var alterationoption = alteration.Alterationoptions.FirstOrDefault(a => a.AlterationoptionId == alterationitem.AlterationoptionId);

        //        if (alteration != null && alterationoption != null)
        //        {
        //            alterationitem.AlterationName = alteration.Name;
        //            alterationitem.AlterationType = alteration.Type;
        //            alterationitem.AlterationoptionPriceIn = alterationoption.PriceIn;                    
        //            alterationitem.AlterationoptionName = alterationoption.Name;
        //        }
        //        else if (alteration == null)
        //        {
        //            Log.Warning("OrderitemEntity", "CompleteOrderitem", "Alteration could not be found: '{0}'", alterationitem.AlterationId);
        //        }
        //        else if (alterationoption == null)
        //        {
        //            Log.Warning("OrderitemEntity", "CompleteOrderitem", "Alterationtoption could not be found: '{0}'", alterationitem.AlterationoptionId);
        //        }
        //    }                                       
        //}

        #endregion
    }
}
