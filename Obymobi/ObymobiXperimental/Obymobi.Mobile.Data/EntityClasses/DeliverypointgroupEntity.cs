using System;
using Obymobi.Mobile.Data.EntityBaseClasses;
using Obymobi.Mobile.Data.CollectionClasses;

namespace Obymobi.Mobile.Data.EntityClasses
{
    [Serializable]
	public class DeliverypointgroupEntity : DeliverypointgroupEntityBase
	{
		#region Constructors

		public DeliverypointgroupEntity()
		{
		}

		#endregion

        #region Properties

        // PILS Make sure we actually get Deliverypoints on the Deliverypointgroup Entity
        public DeliverypointCollection Deliverypoints
        {
            get;
            set;
        }

        #endregion
    }
}
