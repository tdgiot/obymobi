using System;
using Obymobi.Mobile.Data.CollectionClasses;
using Obymobi.Mobile.Data.SupportClasses;

namespace Obymobi.Mobile.Data.EntityBaseClasses
{
    [Serializable]
	public class DeliverypointEntityBase : EntityBase
	{
		#region Constructors

		public DeliverypointEntityBase()
		{
		}

		#endregion

		#region Properties

		public int DeliverypointId
		{ get; set; }

		public int Number
		{ get; set; }

		public int CompanyId
		{ get; set; }

		public string Name
		{ get; set; }

		public int DeliverypointgroupId
		{ get; set; }

		#endregion
	}
}
