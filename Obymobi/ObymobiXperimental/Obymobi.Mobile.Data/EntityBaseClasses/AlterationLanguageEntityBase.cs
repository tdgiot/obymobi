using System;
using Obymobi.Mobile.Data.CollectionClasses;
using Obymobi.Mobile.Data.SupportClasses;

namespace Obymobi.Mobile.Data.EntityBaseClasses
{
    [Serializable]
	public class AlterationLanguageEntityBase : EntityBase
	{
		#region Constructors

		public AlterationLanguageEntityBase()
		{
		}

		#endregion

		#region Properties

		public int AlterationLanguageId
		{ get; set; }

		public int AlterationId
		{ get; set; }

		public string LanguageCode
		{ get; set; }

		public string Name
		{ get; set; }

		#endregion
	}
}
