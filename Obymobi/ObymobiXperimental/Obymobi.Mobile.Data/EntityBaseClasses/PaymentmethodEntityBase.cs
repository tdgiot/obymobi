using System;
using Obymobi.Mobile.Data.CollectionClasses;
using Obymobi.Mobile.Data.SupportClasses;

namespace Obymobi.Mobile.Data.EntityBaseClasses
{
    [Serializable]
	public class PaymentmethodEntityBase : EntityBase
	{
		#region Constructors

		public PaymentmethodEntityBase()
		{
		}

		#endregion

		#region Properties

		public int PaymentmethodId
		{ get; set; }

		public string Name
		{ get; set; }

		public int PaymentmethodType
		{ get; set; }

		public int CompanyId
		{ get; set; }

		public string ThankTitle
		{ get; set; }

		public string ThankMessage
		{ get; set; }

		public decimal MinimumLimit
		{ get; set; }

		#endregion
	}
}
