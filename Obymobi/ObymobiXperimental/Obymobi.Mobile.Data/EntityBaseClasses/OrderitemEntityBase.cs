using System;
using Obymobi.Mobile.Data.CollectionClasses;
using Obymobi.Mobile.Data.SupportClasses;

namespace Obymobi.Mobile.Data.EntityBaseClasses
{
    [Serializable]
	public class OrderitemEntityBase : EntityBase
	{
		#region Constructors

		public OrderitemEntityBase()
		{
		}

		#endregion

		#region Properties

		public int OrderitemId
		{ get; set; }

		public int OrderId
		{ get; set; }

		public string Guid
		{ get; set; }

		public int CategoryId
		{ get; set; }

		public int ProductId
		{ get; set; }

		public string ProductName
		{ get; set; }

		public int Quantity
		{ get; set; }

		public decimal ProductPriceIn
		{ get; set; }

		public string Notes
		{ get; set; }

		public AlterationitemCollection Alterationitems
		{ get; set; }

		#endregion
	}
}
