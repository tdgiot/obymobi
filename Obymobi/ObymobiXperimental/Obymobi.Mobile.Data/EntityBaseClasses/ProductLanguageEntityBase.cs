using System;
using Obymobi.Mobile.Data.CollectionClasses;
using Obymobi.Mobile.Data.SupportClasses;

namespace Obymobi.Mobile.Data.EntityBaseClasses
{
    [Serializable]
	public class ProductLanguageEntityBase : EntityBase
	{
		#region Constructors

		public ProductLanguageEntityBase()
		{
		}

		#endregion

		#region Properties

		public int ProductLanguageId
		{ get; set; }

		public int ProductId
		{ get; set; }

		public string LanguageCode
		{ get; set; }

		public string Name
		{ get; set; }

		public string Description
		{ get; set; }

		#endregion
	}
}
