using System;
using Obymobi.Mobile.Data.CollectionClasses;
using Obymobi.Mobile.Data.SupportClasses;

namespace Obymobi.Mobile.Data.EntityBaseClasses
{
    [Serializable]
	public class CompanyEntityBase : EntityBase
	{
		#region Constructors

		public CompanyEntityBase()
		{
		}

		#endregion

		#region Properties

		public int CompanyId
		{ get; set; }

		public string Currency
		{ get; set; }

		public string Name
		{ get; set; }

		public string Code
		{ get; set; }

		public string Addressline1
		{ get; set; }

		public string Addressline2
		{ get; set; }

		public string Addressline3
		{ get; set; }

		public string Zipcode
		{ get; set; }

		public string City
		{ get; set; }

		public string Country
		{ get; set; }

		public double? Latitude
		{ get; set; }

		public double? Longitude
		{ get; set; }

		public string LanguageCode
		{ get; set; }

		public string Telephone
		{ get; set; }

		public string Description
		{ get; set; }

		public string GoogleAnalyticsId
		{ get; set; }

		public long CompanyDataLastModifiedTicks
		{ get; set; }

		public long CompanyMediaLastModifiedTicks
		{ get; set; }

		public long MenuDataLastModifiedTicks
		{ get; set; }

		public long MenuMediaLastModifiedTicks
		{ get; set; }

		public long DeliverypointDataLastModifiedTicks
		{ get; set; }

		public bool FreeText
		{ get; set; }

		public string Salt
		{ get; set; }

		public MediaCollection Media
		{ get; set; }

		public LanguageCollection Languages
		{ get; set; }

		public DeliverypointgroupCollection Deliverypointgroups
		{ get; set; }

		public PaymentmethodCollection Paymentmethods
		{ get; set; }

		#endregion
	}
}
