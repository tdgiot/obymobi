using System;
using Obymobi.Mobile.Data.CollectionClasses;
using Obymobi.Mobile.Data.SupportClasses;

namespace Obymobi.Mobile.Data.EntityBaseClasses
{
    [Serializable]
	public class CategoryEntityBase : EntityBase
	{
		#region Constructors

		public CategoryEntityBase()
		{
		}

		#endregion

		#region Properties

		public int CategoryId
		{ get; set; }

		public int ParentCategoryId
		{ get; set; }

		public string Name
		{ get; set; }

		public int SortOrder
		{ get; set; }

		public CategoryCollection Categories
		{ get; set; }

		public ProductCollection Products
		{ get; set; }

		public MediaCollection Media
		{ get; set; }

		public CategoryLanguageCollection CategoryLanguages
		{ get; set; }

		public int Type
		{ get; set; }

		public bool HidePrices
		{ get; set; }

		#endregion
	}
}
