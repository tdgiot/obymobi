using System;
using Obymobi.Mobile.Data.CollectionClasses;
using Obymobi.Mobile.Data.SupportClasses;

namespace Obymobi.Mobile.Data.EntityBaseClasses
{
    [Serializable]
	public class MenuEntityBase : EntityBase
	{
		#region Constructors

		public MenuEntityBase()
		{
		}

		#endregion

		#region Properties

		public int MenuId
		{ get; set; }

		public string Name
		{ get; set; }

		public CategoryCollection Categories
		{ get; set; }

		#endregion
	}
}
