using System;
using Obymobi.Mobile.Data.CollectionClasses;
using Obymobi.Mobile.Data.SupportClasses;

namespace Obymobi.Mobile.Data.EntityBaseClasses
{
    [Serializable]
	public class LanguageEntityBase : EntityBase
	{
		#region Constructors

		public LanguageEntityBase()
		{
		}

		#endregion

		#region Properties

		public string Name
		{ get; set; }

		public string Code
		{ get; set; }

		public string LocalizedName
		{ get; set; }

		public string Description
		{ get; set; }

		public string DeliverypointCaption
		{ get; set; }

		#endregion
	}
}
