using System;
using Obymobi.Mobile.Data.CollectionClasses;
using Obymobi.Mobile.Data.SupportClasses;

namespace Obymobi.Mobile.Data.EntityBaseClasses
{
    [Serializable]
	public class AlterationoptionLanguageEntityBase : EntityBase
	{
		#region Constructors

		public AlterationoptionLanguageEntityBase()
		{
		}

		#endregion

		#region Properties

		public int AlterationoptionLanguageId
		{ get; set; }

		public int AlterationoptionId
		{ get; set; }

		public string LanguageCode
		{ get; set; }

		public string Name
		{ get; set; }

		#endregion
	}
}
