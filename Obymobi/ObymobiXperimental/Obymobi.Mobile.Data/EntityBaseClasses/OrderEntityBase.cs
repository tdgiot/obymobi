using System;
using Obymobi.Mobile.Data.CollectionClasses;
using Obymobi.Mobile.Data.SupportClasses;

namespace Obymobi.Mobile.Data.EntityBaseClasses
{
    [Serializable]
	public class OrderEntityBase : EntityBase
	{
		#region Constructors

		public OrderEntityBase()
		{
		}

		#endregion

		#region Properties

		public int OrderId
		{ get; set; }

		public string Guid
		{ get; set; }

		public int CustomerId
		{ get; set; }

		public int CompanyId
		{ get; set; }

		public int PaymentmethodId
		{ get; set; }

		public int DeliverypointId
		{ get; set; }

		public int DeliverypointNumber
		{ get; set; }

		public string DeliverypointName
		{ get; set; }

		public int Status
		{ get; set; }

		public string StatusText
		{ get; set; }

		public int ErrorCode
		{ get; set; }

		public string Notes
		{ get; set; }

		public string CustomerNameFull
		{ get; set; }

		public string CustomerPhonenumber
		{ get; set; }

		public int Type
		{ get; set; }

		public int PaymentState
		{ get; set; }

		public string PendingPaymentmethodAndTransactionId
		{ get; set; }

		public string Created
		{ get; set; }

		public OrderitemCollection Orderitems
		{ get; set; }

		#endregion
	}
}
