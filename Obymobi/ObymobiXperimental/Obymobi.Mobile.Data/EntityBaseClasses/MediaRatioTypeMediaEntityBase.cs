using System;
using Obymobi.Mobile.Data.CollectionClasses;
using Obymobi.Mobile.Data.SupportClasses;

namespace Obymobi.Mobile.Data.EntityBaseClasses
{
    [Serializable]
	public class MediaRatioTypeMediaEntityBase : EntityBase
	{
		#region Constructors

		public MediaRatioTypeMediaEntityBase()
		{
		}

		#endregion

		#region Properties

		public int MediaRatioTypeMediaId
		{ get; set; }

		public int MediaId
		{ get; set; }

		public string MediaRatioTypeSystemName
		{ get; set; }

		public int Width
		{ get; set; }

		public int Height
		{ get; set; }

		public int MediaType
		{ get; set; }

		#endregion
	}
}
