using System;
using Obymobi.Mobile.Data.CollectionClasses;
using Obymobi.Mobile.Data.SupportClasses;

namespace Obymobi.Mobile.Data.EntityBaseClasses
{
    [Serializable]
	public class AlterationEntityBase : EntityBase
	{
		#region Constructors

		public AlterationEntityBase()
		{
		}

		#endregion

		#region Properties

		public int AlterationId
		{ get; set; }

		public string Name
		{ get; set; }

		public int Type
		{ get; set; }

		public int DefaultAlterationoptionId
		{ get; set; }

		public int MinOptions
		{ get; set; }

		public int MaxOptions
		{ get; set; }

		public string StartTime
		{ get; set; }

		public string EndTime
		{ get; set; }

		public int MinLeadMinutes
		{ get; set; }

		public int MaxLeadHours
		{ get; set; }

		public int IntervalMinutes
		{ get; set; }

		public bool ShowDatePicker
		{ get; set; }

		public AlterationoptionCollection Alterationoptions
		{ get; set; }

		public AlterationLanguageCollection AlterationLanguages
		{ get; set; }

		public MediaCollection Media
		{ get; set; }

		#endregion
	}
}
