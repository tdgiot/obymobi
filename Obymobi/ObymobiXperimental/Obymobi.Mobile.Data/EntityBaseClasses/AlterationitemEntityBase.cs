using System;
using Obymobi.Mobile.Data.CollectionClasses;
using Obymobi.Mobile.Data.SupportClasses;

namespace Obymobi.Mobile.Data.EntityBaseClasses
{
    [Serializable]
	public class AlterationitemEntityBase : EntityBase
	{
		#region Constructors

		public AlterationitemEntityBase()
		{
		}

		#endregion

		#region Properties

		public int AlterationitemId
		{ get; set; }

		public int AlterationId
		{ get; set; }

		public string Guid
		{ get; set; }

		public string AlterationName
		{ get; set; }

		public int AlterationType
		{ get; set; }

		public int AlterationoptionId
		{ get; set; }

		public string AlterationoptionName
		{ get; set; }

		public decimal AlterationoptionPriceIn
		{ get; set; }

		public bool SelectedOnDefault
		{ get; set; }

		public int SortOrder
		{ get; set; }

		public string Time
		{ get; set; }

		#endregion
	}
}
