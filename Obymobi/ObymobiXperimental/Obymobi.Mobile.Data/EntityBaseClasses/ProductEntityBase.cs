using System;
using Obymobi.Mobile.Data.CollectionClasses;
using Obymobi.Mobile.Data.SupportClasses;

namespace Obymobi.Mobile.Data.EntityBaseClasses
{
    [Serializable]
	public class ProductEntityBase : EntityBase
	{
		#region Constructors

		public ProductEntityBase()
		{
		}

		#endregion

		#region Properties

		public int ProductId
		{ get; set; }

		public int CategoryId
		{ get; set; }

		public string Name
		{ get; set; }

		public string Description
		{ get; set; }

		public int Type
		{ get; set; }

		public decimal PriceIn
		{ get; set; }

		public int VatPercentage
		{ get; set; }

		public int SortOrder
		{ get; set; }

		public AlterationCollection Alterations
		{ get; set; }

		public MediaCollection Media
		{ get; set; }

		public ProductLanguageCollection ProductLanguages
		{ get; set; }

		#endregion
	}
}
