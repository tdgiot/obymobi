using System;
using Obymobi.Mobile.Data.CollectionClasses;
using Obymobi.Mobile.Data.SupportClasses;

namespace Obymobi.Mobile.Data.EntityBaseClasses
{
    [Serializable]
	public class DeliverypointgroupEntityBase : EntityBase
	{
		#region Constructors

		public DeliverypointgroupEntityBase()
		{
		}

		#endregion

		#region Properties

		public int DeliverypointgroupId
		{ get; set; }

		public int CompanyId
		{ get; set; }

		public string Name
		{ get; set; }

		public bool OrderHistoryDialogEnabled
		{ get; set; }

		public int MenuId
		{ get; set; }

		public DeliverypointCollection Deliverypoints
		{ get; set; }

		#endregion
	}
}
