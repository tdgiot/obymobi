using System;
using Obymobi.Mobile.Data.CollectionClasses;
using Obymobi.Mobile.Data.SupportClasses;

namespace Obymobi.Mobile.Data.EntityBaseClasses
{
    [Serializable]
	public class MediaEntityBase : EntityBase
	{
		#region Constructors

		public MediaEntityBase()
		{
		}

		#endregion

		#region Properties

		public int MediaId
		{ get; set; }

		public string Name
		{ get; set; }

		public string FilePathRelativeToMediaPath
		{ get; set; }

		public int MediaType
		{ get; set; }

		public MediaRatioTypeMediaCollection MediaRatioTypeMedias
		{ get; set; }

		#endregion
	}
}
