using System;
using Obymobi.Mobile.Data.CollectionClasses;
using Obymobi.Mobile.Data.SupportClasses;

namespace Obymobi.Mobile.Data.EntityBaseClasses
{
    [Serializable]
	public class CategoryLanguageEntityBase : EntityBase
	{
		#region Constructors

		public CategoryLanguageEntityBase()
		{
		}

		#endregion

		#region Properties

		public int CategoryLanguageId
		{ get; set; }

		public int CategoryId
		{ get; set; }

		public string LanguageCode
		{ get; set; }

		public string Name
		{ get; set; }

		#endregion
	}
}
