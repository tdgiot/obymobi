using System;
using Obymobi.Mobile.Data.CollectionClasses;
using Obymobi.Mobile.Data.SupportClasses;

namespace Obymobi.Mobile.Data.EntityBaseClasses
{
    [Serializable]
	public class AlterationoptionEntityBase : EntityBase
	{
		#region Constructors

		public AlterationoptionEntityBase()
		{
		}

		#endregion

		#region Properties

		public int AlterationoptionId
		{ get; set; }

		public string Name
		{ get; set; }

		public decimal PriceIn
		{ get; set; }

		public string Description
		{ get; set; }

		public AlterationoptionLanguageCollection AlterationoptionLanguages
		{ get; set; }

		#endregion
	}
}
