﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Obymobi.Logic.Analytics;
using Dionysos;
using System.Threading.Tasks;
using System.Diagnostics;

namespace ObymobiXperimental
{
    public partial class ucMetrics : UserControl
    {        

        public ucMetrics()
        {
            InitializeComponent();
            //AnalyticsService.Identifier = this.tbIdentity.Text;
        }

        private void btIdentifyA_Click(object sender, EventArgs e)
        {
            this.tbIdentity.Text = "pretty-fly-guy@crave-emenu.com";            
        }

        private void btIdentifyB_Click(object sender, EventArgs e)
        {
            this.tbIdentity.Text = "lady-in-pink@crave-emenu.com";
        }

        private void btIdentifyC_Click(object sender, EventArgs e)
        {
            this.tbIdentity.Text = "sponge-bob@crave-emenu.com";
        }

        private void btIdentifyD_Click(object sender, EventArgs e)
        {
            this.tbIdentity.Text = "scuba-steve@crave-emenu.com";
        }

        private void tbIdentity_TextChanged(object sender, EventArgs e)
        {
            //AnalyticsService.Identifier = this.tbIdentity.Text;
        }

        private void btReportVisistedMobileLandingPage_Click(object sender, EventArgs e)
        {
            //AnalyticsService.Track.VisitedMobileLandingPage(this.tbAquisitionSource.Text);
        }

        private void btReportWentToAppStore_Click(object sender, EventArgs e)
        {
            //AnalyticsService.Track.WentToAppStore(this.tbAquisitionSource2.Text);
        }

        private void btSetOS_Click(object sender, EventArgs e)
        {
            //AnalyticsService.Profile.SetOS(this.tbOS.Text);
        }

        private void btSetPhoneModel_Click(object sender, EventArgs e)
        {
            //AnalyticsService.Profile.SetPhoneModel(this.tbPhoneModel.Text);
        }

        private void btSetCountry_Click(object sender, EventArgs e)
        {
            //AnalyticsService.Profile.SetCountry(this.tbCountry.Text);
        }

        private void btReportFirstLaunchOfApplication_Click(object sender, EventArgs e)
        {
            //AnalyticsService.Track.FirstLaunchOfApplication(this.tbAquisitionSource2.Text);
        }

        private void btReportApplicationGotFocus_Click(object sender, EventArgs e)
        {
            //AnalyticsService.Track.ApplicationGotFocus(this.tbVersion.Text);
        }

        private void btReportApplicationLostFocus_Click(object sender, EventArgs e)
        {
            //AnalyticsService.Track.ApplicationLostFocus(Convert.ToInt32(this.tbSecondsOfFocus.Text));
        }

        private void btReportAddedProductToBasket_Click(object sender, EventArgs e)
        {
            //AnalyticsService.Track.AddedProductToBasket();
        }

        private void btReportStartedCheckOut_Click(object sender, EventArgs e)
        {
            //AnalyticsService.Track.StartedCheckout();
        }

        private void btReportCompletedCheckout_Click(object sender, EventArgs e)
        {
            //AnalyticsService.Track.CompletedCheckout(Convert.ToDouble(this.tbOrderValue.Text));
        }

        private void btReportStartedServiceRequest_Click(object sender, EventArgs e)
        {
            //AnalyticsService.Track.StartedServiceRequest();
        }

        private void btReportCompletedServiceRequest_Click(object sender, EventArgs e)
        {
            //AnalyticsService.Track.CompletedServiceRequest();
        }


        public static bool keepSimulating = false;

        private void GenerateStatistics()
        { 
            // Generate for a period of 2 months (1st of Oct till 30th of Nov)
            // Average 1 attempt per 30-60 minutes            

            // Generate 250 user
            DateTime minDateTime = DateTimeUtil.GetFirstDayOfMonth(DateTime.UtcNow.AddMonths(-2));
            DateTime maxDateTime = DateTimeUtil.GetLastDayOfMonth(DateTime.UtcNow);
            List<string> osChoices = new List<string> { "Android", "Android", "Android", "iOS", "iOS", "Windows" };
            List<string> versionChoices = new List<string> { "1.0", "2.0", "2.0", "2.0", "2.0", "2.0", "3.0" };
            List<string> aquisitionChoices = new List<string> { "Google", "Google", "Google", "Google", "Google", "In-Room", "In-Room", "In-Room", "AppStore" };
            List<string> countryChoices = new List<string> { "NL", "NL", "UK", "UK", "UK", "UK", "UK", "UK", };
            List<string> modelChoices = new List<string> { "iPad 2", "iPad 2 Retina", "iPad 3", "iPhone 4", "iPhone 5", "Xperia Z1", "Galaxy S2", "Galaxy S3", "Tab 2" };
            for(int iUser = 0; iUser <= 251; iUser++)
            {
                Debug.WriteLine("Simulating User: {0}".FormatSafe(iUser));

                if (!ucMetrics.keepSimulating)
                    break;

                // Start ups between 1 and 40 in that period
                // Period = 1440 hours (60 * 24)
                // Minimum interval is 8 hours
                // Maximum interval is 24 hours
                DateTime dateTime = RandomUtil.Next(minDateTime, maxDateTime);
                string identity = string.Format("{0}-{1}-{2}-{3}@crave-test.com", iUser, dateTime.Month, dateTime.Day, RandomUtil.GetRandomLowerCaseString(6));
                //AnalyticsService.Identifier = identity;

                // Set Profile                
                //AnalyticsService.Profile.SetCountry(RandomUtil.Pick(countryChoices));
                //AnalyticsService.Profile.SetOS(RandomUtil.Pick(osChoices));
                //AnalyticsService.Profile.SetPhoneModel(RandomUtil.Pick(modelChoices));
                string version = RandomUtil.Pick(versionChoices);
                string source = RandomUtil.Pick(aquisitionChoices);
                                
                // Days of usage (10% 1, 15% 7 days, 50% 14 days, 25% infinite)
                int daysOfUsageRandom = RandomUtil.Next(1,100);
                int daysOfUsage;
                if(daysOfUsageRandom <= 10)
                    daysOfUsage = 1;
                else if(daysOfUsageRandom <= 25) 
                    daysOfUsage = RandomUtil.Next(2,7);
                else if(daysOfUsageRandom <= 75)
                    daysOfUsage = RandomUtil.Next(8,14);
                else
                    daysOfUsage = 60;                    

                // Interval between usage (10% 4-12, 15% 13-24, 50% 24-36, 25% 36-100)
                int intervalsRandom = RandomUtil.Next(1,100);
                int intervalHours;
                if(intervalsRandom <= 10)
                    intervalHours = RandomUtil.Next(4,12);
                else if(intervalsRandom <= 25)
                    intervalHours = RandomUtil.Next(13,24);
                else if(intervalsRandom <= 75)
                    intervalHours = RandomUtil.Next(25,36);
                else
                    intervalHours = RandomUtil.Next(36,100);

                // Track first launch
                MixPanelConnector.importHistory = false;
                //AnalyticsService.Track.VisitedMobileLandingPage(RandomUtil.Pick(aquisitionChoices));
                MixPanelConnector.importHistory = true;
                //AnalyticsService.Track.FirstLaunchOfApplication(RandomUtil.Pick(aquisitionChoices), dateTime);

                int startups = 0;
                DateTime maxDateTimeUser = dateTime.AddDays(daysOfUsage);
                while(dateTime < maxDateTime && startups < 120 && dateTime < maxDateTimeUser)
                {
                    if (!ucMetrics.keepSimulating)
                        break;

                    startups++;

                    // Increase time
                    dateTime = dateTime.AddHours(RandomUtil.Next(1, intervalHours));

                    // Got Focus
                    //AnalyticsService.Track.ApplicationGotFocus(version, dateTime);
                    dateTime = dateTime.AddMilliseconds(RandomUtil.Next(1000, 30000));

                    // Of the start ups 35% put something in the basket
                    if (Dionysos.RandomUtil.Next(1, 100) < 35)
                    {
                        // Add between 1 and 5 products
                        int products = RandomUtil.Next(1, 5);

                        for (int iProduct = 0; iProduct < products; iProduct++)
                        {
                            //AnalyticsService.Track.AddedProductToBasket(dateTime);
                            dateTime = dateTime.AddMilliseconds(RandomUtil.Next(1000, 30000));
                        }

                        // Of the put in pasket 65% proceeds to checkout 
                        if (Dionysos.RandomUtil.Next(1, 100) < 65)
                        {
                            //AnalyticsService.Track.StartedCheckout(dateTime);
                            dateTime = dateTime.AddMilliseconds(RandomUtil.Next(1000, 30000));

                            // of the the started checkouts 8% completes the order
                            if (Dionysos.RandomUtil.Next(1, 100) < 85)
                            {
                                //AnalyticsService.Track.CompletedCheckout(RandomUtil.Next(5,125), dateTime);
                                dateTime = dateTime.AddMilliseconds(RandomUtil.Next(1000, 30000));
                            }
                        }                        
                    }

                    // of the stat ups 10% start a service request
                    if (Dionysos.RandomUtil.Next(1, 100) < 10)
                    {
                        //AnalyticsService.Track.StartedServiceRequest(dateTime);
                        dateTime = dateTime.AddMilliseconds(RandomUtil.Next(1000, 30000));

                        if (Dionysos.RandomUtil.Next(1, 100) < 95)
                        {
                            //AnalyticsService.Track.CompletedServiceRequest(dateTime);
                            dateTime = dateTime.AddMilliseconds(RandomUtil.Next(1000, 30000));
                        }
                    }                    

                    // the app is now closed after between 20 and 240s of usage.
                    //AnalyticsService.Track.ApplicationLostFocus(RandomUtil.Next(10, 480), dateTime);
                    dateTime = dateTime.AddMilliseconds(RandomUtil.Next(1000, 30000));
                }

                this.lblPersons.Invoke(new UpdateSimulatedUsersCallBack(this.UpdateUsers), new object[] { iUser });
            }            
        }

        private delegate void UpdateSimulatedUsersCallBack(int i);

        private void UpdateUsers(int i)
        {
            this.lblPersons.Text = i.ToString();
        }
        

        private void btStartSimulator_Click(object sender, EventArgs e)
        {
            Task.Factory.StartNew(() =>
            {
                ucMetrics.keepSimulating = true;
                this.GenerateStatistics();
            });
        }

        private void btStopSimulator_Click(object sender, EventArgs e)
        {
            ucMetrics.keepSimulating = false;
        }

        private void btScreenHomepage_Click(object sender, EventArgs e)
        {
            //AnalyticsService.Track.HomepageViewed();
        }

        private void btScreenCategory_Click(object sender, EventArgs e)
        {
            //AnalyticsService.Track.CategoryViewed(10, "Xenia", 73, "Deserts");
        }

        private void btScreenCompany_Click(object sender, EventArgs e)
        {
            //AnalyticsService.Track.CompanyPageViewed(10, "Xenia");
        }

        private void btScreenProduct_Click(object sender, EventArgs e)
        {
            //AnalyticsService.Track.ProductViewed(10, "Xenia", 73, "Deserts", 124, "Ice Cream");
        }

        private void btScreenSite1_Click(object sender, EventArgs e)
        {
            //AnalyticsService.Track.MicrositeViewed("www.crave-emenu.com/chewtonglenn/");
        }

        private void btScreenSite2_Click(object sender, EventArgs e)
        {
            //AnalyticsService.Track.MicrositeViewed("support.google.com/analytics/answer/");
        }

        private void btScreenSite11_Click(object sender, EventArgs e)
        {
            //AnalyticsService.Track.MicrositeViewed("www.crave-emenu.com/chewtonglenn/thingstodo.html");
        }

        private void btScreenSite21_Click(object sender, EventArgs e)
        {
            //AnalyticsService.Track.MicrositeViewed("support.google.com/analytics/answer/1638635");
        }
    }
}
