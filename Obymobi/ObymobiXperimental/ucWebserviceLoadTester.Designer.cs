﻿namespace ObymobiXperimental
{
    partial class ucWebserviceLoadTester
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tbWebserviceURL = new System.Windows.Forms.TextBox();
            this.gbWebserviceCalls = new System.Windows.Forms.GroupBox();
            this.cbGetSetClientStatus = new System.Windows.Forms.CheckBox();
            this.cbGetRelease = new System.Windows.Forms.CheckBox();
            this.cbGetNetmessages = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.numOfClients = new System.Windows.Forms.NumericUpDown();
            this.btnStressTest = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.numOfCalls = new System.Windows.Forms.NumericUpDown();
            this.btnCancelWork = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.lblRunningClients = new System.Windows.Forms.Label();
            this.cbGetClientStatuses = new System.Windows.Forms.CheckBox();
            this.cbGetReleaseAndDownloadLocation = new System.Windows.Forms.CheckBox();
            this.gbWebserviceCalls.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numOfClients)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numOfCalls)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Webservice URL";
            // 
            // tbWebserviceURL
            // 
            this.tbWebserviceURL.Location = new System.Drawing.Point(98, 3);
            this.tbWebserviceURL.Name = "tbWebserviceURL";
            this.tbWebserviceURL.Size = new System.Drawing.Size(264, 20);
            this.tbWebserviceURL.TabIndex = 1;
            // 
            // gbWebserviceCalls
            // 
            this.gbWebserviceCalls.Controls.Add(this.cbGetReleaseAndDownloadLocation);
            this.gbWebserviceCalls.Controls.Add(this.cbGetClientStatuses);
            this.gbWebserviceCalls.Controls.Add(this.cbGetSetClientStatus);
            this.gbWebserviceCalls.Controls.Add(this.cbGetRelease);
            this.gbWebserviceCalls.Controls.Add(this.cbGetNetmessages);
            this.gbWebserviceCalls.Location = new System.Drawing.Point(6, 29);
            this.gbWebserviceCalls.Name = "gbWebserviceCalls";
            this.gbWebserviceCalls.Size = new System.Drawing.Size(356, 92);
            this.gbWebserviceCalls.TabIndex = 2;
            this.gbWebserviceCalls.TabStop = false;
            this.gbWebserviceCalls.Text = "Calls";
            // 
            // cbGetSetClientStatus
            // 
            this.cbGetSetClientStatus.AutoSize = true;
            this.cbGetSetClientStatus.Location = new System.Drawing.Point(6, 65);
            this.cbGetSetClientStatus.Name = "cbGetSetClientStatus";
            this.cbGetSetClientStatus.Size = new System.Drawing.Size(115, 17);
            this.cbGetSetClientStatus.TabIndex = 2;
            this.cbGetSetClientStatus.Text = "GetSetClientStatus";
            this.cbGetSetClientStatus.UseVisualStyleBackColor = true;
            this.cbGetSetClientStatus.CheckedChanged += new System.EventHandler(this.cbCalls_CheckedChanged);
            // 
            // cbGetRelease
            // 
            this.cbGetRelease.AutoSize = true;
            this.cbGetRelease.Location = new System.Drawing.Point(6, 42);
            this.cbGetRelease.Name = "cbGetRelease";
            this.cbGetRelease.Size = new System.Drawing.Size(82, 17);
            this.cbGetRelease.TabIndex = 1;
            this.cbGetRelease.Text = "GetRelease";
            this.cbGetRelease.UseVisualStyleBackColor = true;
            this.cbGetRelease.CheckedChanged += new System.EventHandler(this.cbCalls_CheckedChanged);
            // 
            // cbGetNetmessages
            // 
            this.cbGetNetmessages.AutoSize = true;
            this.cbGetNetmessages.Location = new System.Drawing.Point(6, 19);
            this.cbGetNetmessages.Name = "cbGetNetmessages";
            this.cbGetNetmessages.Size = new System.Drawing.Size(107, 17);
            this.cbGetNetmessages.TabIndex = 0;
            this.cbGetNetmessages.Text = "GetNetmessages";
            this.cbGetNetmessages.UseVisualStyleBackColor = true;
            this.cbGetNetmessages.CheckedChanged += new System.EventHandler(this.cbCalls_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 129);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "# of connections";
            // 
            // numOfClients
            // 
            this.numOfClients.Location = new System.Drawing.Point(98, 127);
            this.numOfClients.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numOfClients.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numOfClients.Name = "numOfClients";
            this.numOfClients.Size = new System.Drawing.Size(72, 20);
            this.numOfClients.TabIndex = 5;
            this.numOfClients.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // btnStressTest
            // 
            this.btnStressTest.Location = new System.Drawing.Point(6, 193);
            this.btnStressTest.Name = "btnStressTest";
            this.btnStressTest.Size = new System.Drawing.Size(75, 23);
            this.btnStressTest.TabIndex = 6;
            this.btnStressTest.Text = "Stress Test!";
            this.btnStressTest.UseVisualStyleBackColor = true;
            this.btnStressTest.Click += new System.EventHandler(this.btnStressTest_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 155);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "# of calls";
            // 
            // numOfCalls
            // 
            this.numOfCalls.Location = new System.Drawing.Point(98, 153);
            this.numOfCalls.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numOfCalls.Name = "numOfCalls";
            this.numOfCalls.Size = new System.Drawing.Size(72, 20);
            this.numOfCalls.TabIndex = 8;
            this.numOfCalls.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // btnCancelWork
            // 
            this.btnCancelWork.Location = new System.Drawing.Point(87, 193);
            this.btnCancelWork.Name = "btnCancelWork";
            this.btnCancelWork.Size = new System.Drawing.Size(75, 23);
            this.btnCancelWork.TabIndex = 9;
            this.btnCancelWork.Text = "Cancel work";
            this.btnCancelWork.UseVisualStyleBackColor = true;
            this.btnCancelWork.Click += new System.EventHandler(this.btnCancelWork_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 239);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Running clients:";
            // 
            // lblRunningClients
            // 
            this.lblRunningClients.AutoSize = true;
            this.lblRunningClients.Location = new System.Drawing.Point(84, 239);
            this.lblRunningClients.Name = "lblRunningClients";
            this.lblRunningClients.Size = new System.Drawing.Size(13, 13);
            this.lblRunningClients.TabIndex = 11;
            this.lblRunningClients.Text = "0";
            // 
            // cbGetClientStatuses
            // 
            this.cbGetClientStatuses.AutoSize = true;
            this.cbGetClientStatuses.Location = new System.Drawing.Point(159, 19);
            this.cbGetClientStatuses.Name = "cbGetClientStatuses";
            this.cbGetClientStatuses.Size = new System.Drawing.Size(110, 17);
            this.cbGetClientStatuses.TabIndex = 12;
            this.cbGetClientStatuses.Text = "GetClientStatuses";
            this.cbGetClientStatuses.UseVisualStyleBackColor = true;
            this.cbGetClientStatuses.CheckedChanged += new System.EventHandler(this.cbCalls_CheckedChanged);
            // 
            // cbGetReleaseAndDownloadLocation
            // 
            this.cbGetReleaseAndDownloadLocation.AutoSize = true;
            this.cbGetReleaseAndDownloadLocation.Location = new System.Drawing.Point(159, 42);
            this.cbGetReleaseAndDownloadLocation.Name = "cbGetReleaseAndDownloadLocation";
            this.cbGetReleaseAndDownloadLocation.Size = new System.Drawing.Size(190, 17);
            this.cbGetReleaseAndDownloadLocation.TabIndex = 12;
            this.cbGetReleaseAndDownloadLocation.Text = "GetReleaseAndDownloadLocation";
            this.cbGetReleaseAndDownloadLocation.UseVisualStyleBackColor = true;
            this.cbGetReleaseAndDownloadLocation.CheckedChanged += new System.EventHandler(this.cbCalls_CheckedChanged);
            // 
            // ucWebserviceLoadTester
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblRunningClients);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnCancelWork);
            this.Controls.Add(this.numOfCalls);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnStressTest);
            this.Controls.Add(this.numOfClients);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.gbWebserviceCalls);
            this.Controls.Add(this.tbWebserviceURL);
            this.Controls.Add(this.label1);
            this.Name = "ucWebserviceLoadTester";
            this.Size = new System.Drawing.Size(606, 430);
            this.gbWebserviceCalls.ResumeLayout(false);
            this.gbWebserviceCalls.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numOfClients)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numOfCalls)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbWebserviceURL;
        private System.Windows.Forms.GroupBox gbWebserviceCalls;
        private System.Windows.Forms.CheckBox cbGetSetClientStatus;
        private System.Windows.Forms.CheckBox cbGetRelease;
        private System.Windows.Forms.CheckBox cbGetNetmessages;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numOfClients;
        private System.Windows.Forms.Button btnStressTest;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numOfCalls;
        private System.Windows.Forms.Button btnCancelWork;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblRunningClients;
        private System.Windows.Forms.CheckBox cbGetReleaseAndDownloadLocation;
        private System.Windows.Forms.CheckBox cbGetClientStatuses;
    }
}
