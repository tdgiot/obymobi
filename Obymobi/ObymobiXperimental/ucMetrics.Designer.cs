﻿namespace ObymobiXperimental
{
    partial class ucMetrics
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btIdentifyA = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btIdentifyB = new System.Windows.Forms.Button();
            this.btIdentifyC = new System.Windows.Forms.Button();
            this.btIdentifyD = new System.Windows.Forms.Button();
            this.tbIdentity = new System.Windows.Forms.TextBox();
            this.btReportVisistedMobileLandingPage = new System.Windows.Forms.Button();
            this.btReportWentToAppStore = new System.Windows.Forms.Button();
            this.btReportFirstLaunchOfApplication = new System.Windows.Forms.Button();
            this.btReportApplicationGotFocus = new System.Windows.Forms.Button();
            this.btReportApplicationLostFocus = new System.Windows.Forms.Button();
            this.btReportAddedProductToBasket = new System.Windows.Forms.Button();
            this.btReportStartedCheckOut = new System.Windows.Forms.Button();
            this.btReportCompletedCheckout = new System.Windows.Forms.Button();
            this.btReportStartedServiceRequest = new System.Windows.Forms.Button();
            this.btReportCompletedServiceRequest = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tbAquisitionSource = new System.Windows.Forms.TextBox();
            this.tbAquisitionSource2 = new System.Windows.Forms.TextBox();
            this.tbVersion = new System.Windows.Forms.TextBox();
            this.tbSecondsOfFocus = new System.Windows.Forms.TextBox();
            this.tbOrderValue = new System.Windows.Forms.TextBox();
            this.tbServiceRequestValue = new System.Windows.Forms.TextBox();
            this.tbStartupTime = new System.Windows.Forms.TextBox();
            this.btSetOS = new System.Windows.Forms.Button();
            this.btSetPhoneModel = new System.Windows.Forms.Button();
            this.btSetCountry = new System.Windows.Forms.Button();
            this.tbOS = new System.Windows.Forms.TextBox();
            this.tbPhoneModel = new System.Windows.Forms.TextBox();
            this.tbCountry = new System.Windows.Forms.TextBox();
            this.btStartSimulator = new System.Windows.Forms.Button();
            this.btStopSimulator = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.lblPersons = new System.Windows.Forms.Label();
            this.btScreenHomepage = new System.Windows.Forms.Button();
            this.btScreenCompany = new System.Windows.Forms.Button();
            this.btScreenProduct = new System.Windows.Forms.Button();
            this.btScreenCategory = new System.Windows.Forms.Button();
            this.btScreenSite11 = new System.Windows.Forms.Button();
            this.btScreenSite1 = new System.Windows.Forms.Button();
            this.btScreenSite21 = new System.Windows.Forms.Button();
            this.btScreenSite2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btIdentifyA
            // 
            this.btIdentifyA.Location = new System.Drawing.Point(284, 6);
            this.btIdentifyA.Name = "btIdentifyA";
            this.btIdentifyA.Size = new System.Drawing.Size(75, 23);
            this.btIdentifyA.TabIndex = 0;
            this.btIdentifyA.Text = "Identity A";
            this.btIdentifyA.UseVisualStyleBackColor = true;
            this.btIdentifyA.Click += new System.EventHandler(this.btIdentifyA_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Identifier";
            // 
            // btIdentifyB
            // 
            this.btIdentifyB.Location = new System.Drawing.Point(365, 6);
            this.btIdentifyB.Name = "btIdentifyB";
            this.btIdentifyB.Size = new System.Drawing.Size(75, 23);
            this.btIdentifyB.TabIndex = 2;
            this.btIdentifyB.Text = "Identity B";
            this.btIdentifyB.UseVisualStyleBackColor = true;
            this.btIdentifyB.Click += new System.EventHandler(this.btIdentifyB_Click);
            // 
            // btIdentifyC
            // 
            this.btIdentifyC.Location = new System.Drawing.Point(446, 6);
            this.btIdentifyC.Name = "btIdentifyC";
            this.btIdentifyC.Size = new System.Drawing.Size(75, 23);
            this.btIdentifyC.TabIndex = 3;
            this.btIdentifyC.Text = "Identity C";
            this.btIdentifyC.UseVisualStyleBackColor = true;
            this.btIdentifyC.Click += new System.EventHandler(this.btIdentifyC_Click);
            // 
            // btIdentifyD
            // 
            this.btIdentifyD.Location = new System.Drawing.Point(527, 6);
            this.btIdentifyD.Name = "btIdentifyD";
            this.btIdentifyD.Size = new System.Drawing.Size(75, 23);
            this.btIdentifyD.TabIndex = 4;
            this.btIdentifyD.Text = "Identity D";
            this.btIdentifyD.UseVisualStyleBackColor = true;
            this.btIdentifyD.Click += new System.EventHandler(this.btIdentifyD_Click);
            // 
            // tbIdentity
            // 
            this.tbIdentity.Location = new System.Drawing.Point(67, 8);
            this.tbIdentity.Name = "tbIdentity";
            this.tbIdentity.Size = new System.Drawing.Size(211, 20);
            this.tbIdentity.TabIndex = 5;
            this.tbIdentity.Text = "testpilot@crave-emenu.com";
            this.tbIdentity.TextChanged += new System.EventHandler(this.tbIdentity_TextChanged);
            // 
            // btReportVisistedMobileLandingPage
            // 
            this.btReportVisistedMobileLandingPage.Location = new System.Drawing.Point(17, 43);
            this.btReportVisistedMobileLandingPage.Name = "btReportVisistedMobileLandingPage";
            this.btReportVisistedMobileLandingPage.Size = new System.Drawing.Size(261, 23);
            this.btReportVisistedMobileLandingPage.TabIndex = 6;
            this.btReportVisistedMobileLandingPage.Text = "Report Visited Mobile Landing Page";
            this.btReportVisistedMobileLandingPage.UseVisualStyleBackColor = true;
            this.btReportVisistedMobileLandingPage.Click += new System.EventHandler(this.btReportVisistedMobileLandingPage_Click);
            // 
            // btReportWentToAppStore
            // 
            this.btReportWentToAppStore.Location = new System.Drawing.Point(17, 72);
            this.btReportWentToAppStore.Name = "btReportWentToAppStore";
            this.btReportWentToAppStore.Size = new System.Drawing.Size(261, 23);
            this.btReportWentToAppStore.TabIndex = 7;
            this.btReportWentToAppStore.Text = "Report Went to App Store";
            this.btReportWentToAppStore.UseVisualStyleBackColor = true;
            this.btReportWentToAppStore.Click += new System.EventHandler(this.btReportWentToAppStore_Click);
            // 
            // btReportFirstLaunchOfApplication
            // 
            this.btReportFirstLaunchOfApplication.Location = new System.Drawing.Point(17, 110);
            this.btReportFirstLaunchOfApplication.Name = "btReportFirstLaunchOfApplication";
            this.btReportFirstLaunchOfApplication.Size = new System.Drawing.Size(261, 23);
            this.btReportFirstLaunchOfApplication.TabIndex = 8;
            this.btReportFirstLaunchOfApplication.Text = "Report First Launch of Application";
            this.btReportFirstLaunchOfApplication.UseVisualStyleBackColor = true;
            this.btReportFirstLaunchOfApplication.Click += new System.EventHandler(this.btReportFirstLaunchOfApplication_Click);
            // 
            // btReportApplicationGotFocus
            // 
            this.btReportApplicationGotFocus.Location = new System.Drawing.Point(17, 139);
            this.btReportApplicationGotFocus.Name = "btReportApplicationGotFocus";
            this.btReportApplicationGotFocus.Size = new System.Drawing.Size(261, 23);
            this.btReportApplicationGotFocus.TabIndex = 9;
            this.btReportApplicationGotFocus.Text = "Report Application Got Focus";
            this.btReportApplicationGotFocus.UseVisualStyleBackColor = true;
            this.btReportApplicationGotFocus.Click += new System.EventHandler(this.btReportApplicationGotFocus_Click);
            // 
            // btReportApplicationLostFocus
            // 
            this.btReportApplicationLostFocus.Location = new System.Drawing.Point(17, 168);
            this.btReportApplicationLostFocus.Name = "btReportApplicationLostFocus";
            this.btReportApplicationLostFocus.Size = new System.Drawing.Size(261, 23);
            this.btReportApplicationLostFocus.TabIndex = 10;
            this.btReportApplicationLostFocus.Text = "Report Application Lost Focus";
            this.btReportApplicationLostFocus.UseVisualStyleBackColor = true;
            this.btReportApplicationLostFocus.Click += new System.EventHandler(this.btReportApplicationLostFocus_Click);
            // 
            // btReportAddedProductToBasket
            // 
            this.btReportAddedProductToBasket.Location = new System.Drawing.Point(17, 208);
            this.btReportAddedProductToBasket.Name = "btReportAddedProductToBasket";
            this.btReportAddedProductToBasket.Size = new System.Drawing.Size(261, 23);
            this.btReportAddedProductToBasket.TabIndex = 11;
            this.btReportAddedProductToBasket.Text = "Report Added Product to Basket";
            this.btReportAddedProductToBasket.UseVisualStyleBackColor = true;
            this.btReportAddedProductToBasket.Click += new System.EventHandler(this.btReportAddedProductToBasket_Click);
            // 
            // btReportStartedCheckOut
            // 
            this.btReportStartedCheckOut.Location = new System.Drawing.Point(17, 237);
            this.btReportStartedCheckOut.Name = "btReportStartedCheckOut";
            this.btReportStartedCheckOut.Size = new System.Drawing.Size(261, 23);
            this.btReportStartedCheckOut.TabIndex = 12;
            this.btReportStartedCheckOut.Text = "Report Started Checkout";
            this.btReportStartedCheckOut.UseVisualStyleBackColor = true;
            this.btReportStartedCheckOut.Click += new System.EventHandler(this.btReportStartedCheckOut_Click);
            // 
            // btReportCompletedCheckout
            // 
            this.btReportCompletedCheckout.Location = new System.Drawing.Point(17, 266);
            this.btReportCompletedCheckout.Name = "btReportCompletedCheckout";
            this.btReportCompletedCheckout.Size = new System.Drawing.Size(261, 23);
            this.btReportCompletedCheckout.TabIndex = 13;
            this.btReportCompletedCheckout.Text = "Report Completed Check Out";
            this.btReportCompletedCheckout.UseVisualStyleBackColor = true;
            this.btReportCompletedCheckout.Click += new System.EventHandler(this.btReportCompletedCheckout_Click);
            // 
            // btReportStartedServiceRequest
            // 
            this.btReportStartedServiceRequest.Location = new System.Drawing.Point(17, 306);
            this.btReportStartedServiceRequest.Name = "btReportStartedServiceRequest";
            this.btReportStartedServiceRequest.Size = new System.Drawing.Size(261, 23);
            this.btReportStartedServiceRequest.TabIndex = 14;
            this.btReportStartedServiceRequest.Text = "Report Started Service Request";
            this.btReportStartedServiceRequest.UseVisualStyleBackColor = true;
            this.btReportStartedServiceRequest.Click += new System.EventHandler(this.btReportStartedServiceRequest_Click);
            // 
            // btReportCompletedServiceRequest
            // 
            this.btReportCompletedServiceRequest.Location = new System.Drawing.Point(17, 335);
            this.btReportCompletedServiceRequest.Name = "btReportCompletedServiceRequest";
            this.btReportCompletedServiceRequest.Size = new System.Drawing.Size(261, 23);
            this.btReportCompletedServiceRequest.TabIndex = 15;
            this.btReportCompletedServiceRequest.Text = "Report Completed Service Request";
            this.btReportCompletedServiceRequest.UseVisualStyleBackColor = true;
            this.btReportCompletedServiceRequest.Click += new System.EventHandler(this.btReportCompletedServiceRequest_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(284, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 13);
            this.label2.TabIndex = 16;
            this.label2.Text = "Aquisition Source";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(284, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "Aquisition Source";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(284, 144);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "Version";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(284, 173);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(93, 13);
            this.label5.TabIndex = 19;
            this.label5.Text = "Seconds of Focus";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(284, 271);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 13);
            this.label6.TabIndex = 20;
            this.label6.Text = "Order Value";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(466, 144);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 13);
            this.label7.TabIndex = 21;
            this.label7.Text = "Start up Time";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(284, 340);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 13);
            this.label8.TabIndex = 22;
            this.label8.Text = "Order Value";
            // 
            // tbAquisitionSource
            // 
            this.tbAquisitionSource.Location = new System.Drawing.Point(379, 45);
            this.tbAquisitionSource.Name = "tbAquisitionSource";
            this.tbAquisitionSource.Size = new System.Drawing.Size(211, 20);
            this.tbAquisitionSource.TabIndex = 23;
            this.tbAquisitionSource.Text = "SMS";
            // 
            // tbAquisitionSource2
            // 
            this.tbAquisitionSource2.Location = new System.Drawing.Point(379, 71);
            this.tbAquisitionSource2.Name = "tbAquisitionSource2";
            this.tbAquisitionSource2.Size = new System.Drawing.Size(211, 20);
            this.tbAquisitionSource2.TabIndex = 24;
            this.tbAquisitionSource2.Text = "SMS";
            // 
            // tbVersion
            // 
            this.tbVersion.Location = new System.Drawing.Point(332, 141);
            this.tbVersion.Name = "tbVersion";
            this.tbVersion.Size = new System.Drawing.Size(108, 20);
            this.tbVersion.TabIndex = 25;
            this.tbVersion.Text = "1.0";
            // 
            // tbSecondsOfFocus
            // 
            this.tbSecondsOfFocus.Location = new System.Drawing.Point(379, 170);
            this.tbSecondsOfFocus.Name = "tbSecondsOfFocus";
            this.tbSecondsOfFocus.Size = new System.Drawing.Size(211, 20);
            this.tbSecondsOfFocus.TabIndex = 26;
            this.tbSecondsOfFocus.Text = "75";
            // 
            // tbOrderValue
            // 
            this.tbOrderValue.Location = new System.Drawing.Point(379, 268);
            this.tbOrderValue.Name = "tbOrderValue";
            this.tbOrderValue.Size = new System.Drawing.Size(79, 20);
            this.tbOrderValue.TabIndex = 27;
            this.tbOrderValue.Text = "10.00";
            // 
            // tbServiceRequestValue
            // 
            this.tbServiceRequestValue.Location = new System.Drawing.Point(379, 337);
            this.tbServiceRequestValue.Name = "tbServiceRequestValue";
            this.tbServiceRequestValue.Size = new System.Drawing.Size(79, 20);
            this.tbServiceRequestValue.TabIndex = 28;
            this.tbServiceRequestValue.Text = "75.00";
            // 
            // tbStartupTime
            // 
            this.tbStartupTime.Location = new System.Drawing.Point(542, 141);
            this.tbStartupTime.Name = "tbStartupTime";
            this.tbStartupTime.Size = new System.Drawing.Size(108, 20);
            this.tbStartupTime.TabIndex = 29;
            this.tbStartupTime.Text = "50";
            // 
            // btSetOS
            // 
            this.btSetOS.Location = new System.Drawing.Point(17, 439);
            this.btSetOS.Name = "btSetOS";
            this.btSetOS.Size = new System.Drawing.Size(261, 23);
            this.btSetOS.TabIndex = 30;
            this.btSetOS.Text = "Set OS";
            this.btSetOS.UseVisualStyleBackColor = true;
            this.btSetOS.Click += new System.EventHandler(this.btSetOS_Click);
            // 
            // btSetPhoneModel
            // 
            this.btSetPhoneModel.Location = new System.Drawing.Point(17, 468);
            this.btSetPhoneModel.Name = "btSetPhoneModel";
            this.btSetPhoneModel.Size = new System.Drawing.Size(261, 23);
            this.btSetPhoneModel.TabIndex = 31;
            this.btSetPhoneModel.Text = "Set Phone Model";
            this.btSetPhoneModel.UseVisualStyleBackColor = true;
            this.btSetPhoneModel.Click += new System.EventHandler(this.btSetPhoneModel_Click);
            // 
            // btSetCountry
            // 
            this.btSetCountry.Location = new System.Drawing.Point(17, 497);
            this.btSetCountry.Name = "btSetCountry";
            this.btSetCountry.Size = new System.Drawing.Size(261, 23);
            this.btSetCountry.TabIndex = 32;
            this.btSetCountry.Text = "Set Country";
            this.btSetCountry.UseVisualStyleBackColor = true;
            this.btSetCountry.Click += new System.EventHandler(this.btSetCountry_Click);
            // 
            // tbOS
            // 
            this.tbOS.Location = new System.Drawing.Point(287, 441);
            this.tbOS.Name = "tbOS";
            this.tbOS.Size = new System.Drawing.Size(79, 20);
            this.tbOS.TabIndex = 33;
            this.tbOS.Text = "iOS";
            // 
            // tbPhoneModel
            // 
            this.tbPhoneModel.Location = new System.Drawing.Point(287, 470);
            this.tbPhoneModel.Name = "tbPhoneModel";
            this.tbPhoneModel.Size = new System.Drawing.Size(79, 20);
            this.tbPhoneModel.TabIndex = 34;
            this.tbPhoneModel.Text = "iPhone 5S";
            // 
            // tbCountry
            // 
            this.tbCountry.Location = new System.Drawing.Point(287, 499);
            this.tbCountry.Name = "tbCountry";
            this.tbCountry.Size = new System.Drawing.Size(79, 20);
            this.tbCountry.TabIndex = 35;
            this.tbCountry.Text = "NL";
            // 
            // btStartSimulator
            // 
            this.btStartSimulator.Location = new System.Drawing.Point(457, 441);
            this.btStartSimulator.Name = "btStartSimulator";
            this.btStartSimulator.Size = new System.Drawing.Size(123, 23);
            this.btStartSimulator.TabIndex = 36;
            this.btStartSimulator.Text = "Start Simulator";
            this.btStartSimulator.UseVisualStyleBackColor = true;
            this.btStartSimulator.Click += new System.EventHandler(this.btStartSimulator_Click);
            // 
            // btStopSimulator
            // 
            this.btStopSimulator.Location = new System.Drawing.Point(586, 441);
            this.btStopSimulator.Name = "btStopSimulator";
            this.btStopSimulator.Size = new System.Drawing.Size(123, 23);
            this.btStopSimulator.TabIndex = 37;
            this.btStopSimulator.Text = "Stop Simulator";
            this.btStopSimulator.UseVisualStyleBackColor = true;
            this.btStopSimulator.Click += new System.EventHandler(this.btStopSimulator_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(458, 473);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(94, 13);
            this.label9.TabIndex = 38;
            this.label9.Text = "Persons Simulated";
            // 
            // lblPersons
            // 
            this.lblPersons.AutoSize = true;
            this.lblPersons.Location = new System.Drawing.Point(583, 473);
            this.lblPersons.Name = "lblPersons";
            this.lblPersons.Size = new System.Drawing.Size(13, 13);
            this.lblPersons.TabIndex = 40;
            this.lblPersons.Text = "0";
            // 
            // btScreenHomepage
            // 
            this.btScreenHomepage.Location = new System.Drawing.Point(17, 364);
            this.btScreenHomepage.Name = "btScreenHomepage";
            this.btScreenHomepage.Size = new System.Drawing.Size(126, 23);
            this.btScreenHomepage.TabIndex = 41;
            this.btScreenHomepage.Text = "Screen: Homepage";
            this.btScreenHomepage.UseVisualStyleBackColor = true;
            this.btScreenHomepage.Click += new System.EventHandler(this.btScreenHomepage_Click);
            // 
            // btScreenCompany
            // 
            this.btScreenCompany.Location = new System.Drawing.Point(149, 364);
            this.btScreenCompany.Name = "btScreenCompany";
            this.btScreenCompany.Size = new System.Drawing.Size(126, 23);
            this.btScreenCompany.TabIndex = 42;
            this.btScreenCompany.Text = "Screen: Company View";
            this.btScreenCompany.UseVisualStyleBackColor = true;
            this.btScreenCompany.Click += new System.EventHandler(this.btScreenCompany_Click);
            // 
            // btScreenProduct
            // 
            this.btScreenProduct.Location = new System.Drawing.Point(149, 393);
            this.btScreenProduct.Name = "btScreenProduct";
            this.btScreenProduct.Size = new System.Drawing.Size(126, 23);
            this.btScreenProduct.TabIndex = 44;
            this.btScreenProduct.Text = "Screen: Product View";
            this.btScreenProduct.UseVisualStyleBackColor = true;
            this.btScreenProduct.Click += new System.EventHandler(this.btScreenProduct_Click);
            // 
            // btScreenCategory
            // 
            this.btScreenCategory.Location = new System.Drawing.Point(17, 393);
            this.btScreenCategory.Name = "btScreenCategory";
            this.btScreenCategory.Size = new System.Drawing.Size(126, 23);
            this.btScreenCategory.TabIndex = 43;
            this.btScreenCategory.Text = "Screen: Category View";
            this.btScreenCategory.UseVisualStyleBackColor = true;
            this.btScreenCategory.Click += new System.EventHandler(this.btScreenCategory_Click);
            // 
            // btScreenSite11
            // 
            this.btScreenSite11.Location = new System.Drawing.Point(413, 363);
            this.btScreenSite11.Name = "btScreenSite11";
            this.btScreenSite11.Size = new System.Drawing.Size(126, 23);
            this.btScreenSite11.TabIndex = 46;
            this.btScreenSite11.Text = "Screen: Microsite 1.1";
            this.btScreenSite11.UseVisualStyleBackColor = true;
            this.btScreenSite11.Click += new System.EventHandler(this.btScreenSite11_Click);
            // 
            // btScreenSite1
            // 
            this.btScreenSite1.Location = new System.Drawing.Point(281, 363);
            this.btScreenSite1.Name = "btScreenSite1";
            this.btScreenSite1.Size = new System.Drawing.Size(126, 23);
            this.btScreenSite1.TabIndex = 45;
            this.btScreenSite1.Text = "Screen: Microsite 1";
            this.btScreenSite1.UseVisualStyleBackColor = true;
            this.btScreenSite1.Click += new System.EventHandler(this.btScreenSite1_Click);
            // 
            // btScreenSite21
            // 
            this.btScreenSite21.Location = new System.Drawing.Point(413, 393);
            this.btScreenSite21.Name = "btScreenSite21";
            this.btScreenSite21.Size = new System.Drawing.Size(126, 23);
            this.btScreenSite21.TabIndex = 48;
            this.btScreenSite21.Text = "Screen: Microsite 2.1";
            this.btScreenSite21.UseVisualStyleBackColor = true;
            this.btScreenSite21.Click += new System.EventHandler(this.btScreenSite21_Click);
            // 
            // btScreenSite2
            // 
            this.btScreenSite2.Location = new System.Drawing.Point(281, 393);
            this.btScreenSite2.Name = "btScreenSite2";
            this.btScreenSite2.Size = new System.Drawing.Size(126, 23);
            this.btScreenSite2.TabIndex = 47;
            this.btScreenSite2.Text = "Screen: Microsite 2";
            this.btScreenSite2.UseVisualStyleBackColor = true;
            this.btScreenSite2.Click += new System.EventHandler(this.btScreenSite2_Click);
            // 
            // ucMetrics
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btScreenSite21);
            this.Controls.Add(this.btScreenSite2);
            this.Controls.Add(this.btScreenSite11);
            this.Controls.Add(this.btScreenSite1);
            this.Controls.Add(this.btScreenProduct);
            this.Controls.Add(this.btScreenCategory);
            this.Controls.Add(this.btScreenCompany);
            this.Controls.Add(this.btScreenHomepage);
            this.Controls.Add(this.lblPersons);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.btStopSimulator);
            this.Controls.Add(this.btStartSimulator);
            this.Controls.Add(this.tbCountry);
            this.Controls.Add(this.tbPhoneModel);
            this.Controls.Add(this.tbOS);
            this.Controls.Add(this.btSetCountry);
            this.Controls.Add(this.btSetPhoneModel);
            this.Controls.Add(this.btSetOS);
            this.Controls.Add(this.tbStartupTime);
            this.Controls.Add(this.tbServiceRequestValue);
            this.Controls.Add(this.tbOrderValue);
            this.Controls.Add(this.tbSecondsOfFocus);
            this.Controls.Add(this.tbVersion);
            this.Controls.Add(this.tbAquisitionSource2);
            this.Controls.Add(this.tbAquisitionSource);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btReportCompletedServiceRequest);
            this.Controls.Add(this.btReportStartedServiceRequest);
            this.Controls.Add(this.btReportCompletedCheckout);
            this.Controls.Add(this.btReportStartedCheckOut);
            this.Controls.Add(this.btReportAddedProductToBasket);
            this.Controls.Add(this.btReportApplicationLostFocus);
            this.Controls.Add(this.btReportApplicationGotFocus);
            this.Controls.Add(this.btReportFirstLaunchOfApplication);
            this.Controls.Add(this.btReportWentToAppStore);
            this.Controls.Add(this.btReportVisistedMobileLandingPage);
            this.Controls.Add(this.tbIdentity);
            this.Controls.Add(this.btIdentifyD);
            this.Controls.Add(this.btIdentifyC);
            this.Controls.Add(this.btIdentifyB);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btIdentifyA);
            this.Name = "ucMetrics";
            this.Size = new System.Drawing.Size(729, 532);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btIdentifyA;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btIdentifyB;
        private System.Windows.Forms.Button btIdentifyC;
        private System.Windows.Forms.Button btIdentifyD;
        private System.Windows.Forms.TextBox tbIdentity;
        private System.Windows.Forms.Button btReportVisistedMobileLandingPage;
        private System.Windows.Forms.Button btReportWentToAppStore;
        private System.Windows.Forms.Button btReportFirstLaunchOfApplication;
        private System.Windows.Forms.Button btReportApplicationGotFocus;
        private System.Windows.Forms.Button btReportApplicationLostFocus;
        private System.Windows.Forms.Button btReportAddedProductToBasket;
        private System.Windows.Forms.Button btReportStartedCheckOut;
        private System.Windows.Forms.Button btReportCompletedCheckout;
        private System.Windows.Forms.Button btReportStartedServiceRequest;
        private System.Windows.Forms.Button btReportCompletedServiceRequest;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbAquisitionSource;
        private System.Windows.Forms.TextBox tbAquisitionSource2;
        private System.Windows.Forms.TextBox tbVersion;
        private System.Windows.Forms.TextBox tbSecondsOfFocus;
        private System.Windows.Forms.TextBox tbOrderValue;
        private System.Windows.Forms.TextBox tbServiceRequestValue;
        private System.Windows.Forms.TextBox tbStartupTime;
        private System.Windows.Forms.Button btSetOS;
        private System.Windows.Forms.Button btSetPhoneModel;
        private System.Windows.Forms.Button btSetCountry;
        private System.Windows.Forms.TextBox tbOS;
        private System.Windows.Forms.TextBox tbPhoneModel;
        private System.Windows.Forms.TextBox tbCountry;
        private System.Windows.Forms.Button btStartSimulator;
        private System.Windows.Forms.Button btStopSimulator;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblPersons;
        private System.Windows.Forms.Button btScreenHomepage;
        private System.Windows.Forms.Button btScreenCompany;
        private System.Windows.Forms.Button btScreenProduct;
        private System.Windows.Forms.Button btScreenCategory;
        private System.Windows.Forms.Button btScreenSite11;
        private System.Windows.Forms.Button btScreenSite1;
        private System.Windows.Forms.Button btScreenSite21;
        private System.Windows.Forms.Button btScreenSite2;
    }
}
