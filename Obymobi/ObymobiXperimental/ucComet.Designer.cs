﻿namespace ObymobiXperimental
{
	partial class UcComet
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.btnConnect = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lblConnectionStatus = new System.Windows.Forms.Label();
            this.tbHost = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbEventLog = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbFieldValue5 = new System.Windows.Forms.TextBox();
            this.tbFieldValue4 = new System.Windows.Forms.TextBox();
            this.tbFieldValue3 = new System.Windows.Forms.TextBox();
            this.tbFieldValue2 = new System.Windows.Forms.TextBox();
            this.tbFieldValue1 = new System.Windows.Forms.TextBox();
            this.tbFieldValue6 = new System.Windows.Forms.TextBox();
            this.tbFieldValue7 = new System.Windows.Forms.TextBox();
            this.tbFieldValue8 = new System.Windows.Forms.TextBox();
            this.tbFieldValue9 = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.tbFieldValue10 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tbSenderTerminalId = new System.Windows.Forms.TextBox();
            this.tbSenderDeliverypointId = new System.Windows.Forms.TextBox();
            this.tbSenderCompanyId = new System.Windows.Forms.TextBox();
            this.tbSenderClientId = new System.Windows.Forms.TextBox();
            this.btnSendMessage = new System.Windows.Forms.Button();
            this.tbReceiverTerminalId = new System.Windows.Forms.TextBox();
            this.tbReceiverCompanyId = new System.Windows.Forms.TextBox();
            this.tbReceiverClientId = new System.Windows.Forms.TextBox();
            this.tbReceiverDeliverypointId = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbMessageType = new System.Windows.Forms.ComboBox();
            this.netmessageBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.gbConnection = new System.Windows.Forms.GroupBox();
            this.cmbClientType = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.tbSalt = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.tbIdentifier = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.gbClientInfo = new System.Windows.Forms.GroupBox();
            this.cbClientTypeMasterConsole = new System.Windows.Forms.CheckBox();
            this.tbClientTypeTerminalId = new System.Windows.Forms.TextBox();
            this.tbClientTypeDeliverypointgroupId = new System.Windows.Forms.TextBox();
            this.tbClientTypeDeliverypointId = new System.Windows.Forms.TextBox();
            this.tbClientTypeCompanyId = new System.Windows.Forms.TextBox();
            this.tbClientTypeClientId = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.netmessageBindingSource)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.gbConnection.SuspendLayout();
            this.gbClientInfo.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(10, 162);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(129, 23);
            this.btnConnect.TabIndex = 0;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(10, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Status";
            // 
            // lblConnectionStatus
            // 
            this.lblConnectionStatus.AutoSize = true;
            this.lblConnectionStatus.Location = new System.Drawing.Point(84, 10);
            this.lblConnectionStatus.Name = "lblConnectionStatus";
            this.lblConnectionStatus.Size = new System.Drawing.Size(73, 13);
            this.lblConnectionStatus.TabIndex = 2;
            this.lblConnectionStatus.Text = "Disconnected";
            // 
            // tbHost
            // 
            this.tbHost.Location = new System.Drawing.Point(85, 19);
            this.tbHost.Name = "tbHost";
            this.tbHost.Size = new System.Drawing.Size(214, 20);
            this.tbHost.TabIndex = 3;
            this.tbHost.Text = "http://localhost/messaging/host.PokeIn";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Host";
            // 
            // tbEventLog
            // 
            this.tbEventLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbEventLog.Location = new System.Drawing.Point(7, 380);
            this.tbEventLog.Multiline = true;
            this.tbEventLog.Name = "tbEventLog";
            this.tbEventLog.ReadOnly = true;
            this.tbEventLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbEventLog.Size = new System.Drawing.Size(634, 189);
            this.tbEventLog.TabIndex = 5;
            this.tbEventLog.TextChanged += new System.EventHandler(this.tbEventLog_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 364);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Event Log";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.tbFieldValue5);
            this.groupBox1.Controls.Add(this.tbFieldValue4);
            this.groupBox1.Controls.Add(this.tbFieldValue3);
            this.groupBox1.Controls.Add(this.tbFieldValue2);
            this.groupBox1.Controls.Add(this.tbFieldValue1);
            this.groupBox1.Controls.Add(this.tbFieldValue6);
            this.groupBox1.Controls.Add(this.tbFieldValue7);
            this.groupBox1.Controls.Add(this.tbFieldValue8);
            this.groupBox1.Controls.Add(this.tbFieldValue9);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.tbFieldValue10);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.tbSenderTerminalId);
            this.groupBox1.Controls.Add(this.tbSenderDeliverypointId);
            this.groupBox1.Controls.Add(this.tbSenderCompanyId);
            this.groupBox1.Controls.Add(this.tbSenderClientId);
            this.groupBox1.Controls.Add(this.btnSendMessage);
            this.groupBox1.Controls.Add(this.tbReceiverTerminalId);
            this.groupBox1.Controls.Add(this.tbReceiverCompanyId);
            this.groupBox1.Controls.Add(this.tbReceiverClientId);
            this.groupBox1.Controls.Add(this.tbReceiverDeliverypointId);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.cmbMessageType);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(618, 314);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Send Message";
            // 
            // tbFieldValue5
            // 
            this.tbFieldValue5.Location = new System.Drawing.Point(436, 124);
            this.tbFieldValue5.Name = "tbFieldValue5";
            this.tbFieldValue5.Size = new System.Drawing.Size(150, 20);
            this.tbFieldValue5.TabIndex = 38;
            // 
            // tbFieldValue4
            // 
            this.tbFieldValue4.Location = new System.Drawing.Point(436, 98);
            this.tbFieldValue4.Name = "tbFieldValue4";
            this.tbFieldValue4.Size = new System.Drawing.Size(150, 20);
            this.tbFieldValue4.TabIndex = 37;
            // 
            // tbFieldValue3
            // 
            this.tbFieldValue3.Location = new System.Drawing.Point(436, 72);
            this.tbFieldValue3.Name = "tbFieldValue3";
            this.tbFieldValue3.Size = new System.Drawing.Size(150, 20);
            this.tbFieldValue3.TabIndex = 36;
            // 
            // tbFieldValue2
            // 
            this.tbFieldValue2.Location = new System.Drawing.Point(436, 46);
            this.tbFieldValue2.Name = "tbFieldValue2";
            this.tbFieldValue2.Size = new System.Drawing.Size(150, 20);
            this.tbFieldValue2.TabIndex = 35;
            // 
            // tbFieldValue1
            // 
            this.tbFieldValue1.Location = new System.Drawing.Point(436, 19);
            this.tbFieldValue1.Name = "tbFieldValue1";
            this.tbFieldValue1.Size = new System.Drawing.Size(150, 20);
            this.tbFieldValue1.TabIndex = 34;
            // 
            // tbFieldValue6
            // 
            this.tbFieldValue6.Location = new System.Drawing.Point(436, 150);
            this.tbFieldValue6.Name = "tbFieldValue6";
            this.tbFieldValue6.Size = new System.Drawing.Size(150, 20);
            this.tbFieldValue6.TabIndex = 33;
            // 
            // tbFieldValue7
            // 
            this.tbFieldValue7.Location = new System.Drawing.Point(436, 176);
            this.tbFieldValue7.Name = "tbFieldValue7";
            this.tbFieldValue7.Size = new System.Drawing.Size(150, 20);
            this.tbFieldValue7.TabIndex = 32;
            // 
            // tbFieldValue8
            // 
            this.tbFieldValue8.Location = new System.Drawing.Point(436, 202);
            this.tbFieldValue8.Name = "tbFieldValue8";
            this.tbFieldValue8.Size = new System.Drawing.Size(150, 20);
            this.tbFieldValue8.TabIndex = 31;
            // 
            // tbFieldValue9
            // 
            this.tbFieldValue9.Location = new System.Drawing.Point(436, 228);
            this.tbFieldValue9.Name = "tbFieldValue9";
            this.tbFieldValue9.Size = new System.Drawing.Size(150, 20);
            this.tbFieldValue9.TabIndex = 30;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(350, 257);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(68, 13);
            this.label22.TabIndex = 29;
            this.label22.Text = "FieldValue10";
            // 
            // tbFieldValue10
            // 
            this.tbFieldValue10.Location = new System.Drawing.Point(436, 254);
            this.tbFieldValue10.Name = "tbFieldValue10";
            this.tbFieldValue10.Size = new System.Drawing.Size(150, 20);
            this.tbFieldValue10.TabIndex = 28;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(350, 231);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(62, 13);
            this.label21.TabIndex = 27;
            this.label21.Text = "FieldValue9";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(350, 205);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(62, 13);
            this.label20.TabIndex = 26;
            this.label20.Text = "FieldValue8";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(350, 179);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(62, 13);
            this.label19.TabIndex = 25;
            this.label19.Text = "FieldValue7";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(350, 154);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(62, 13);
            this.label18.TabIndex = 24;
            this.label18.Text = "FieldValue6";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(350, 127);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(62, 13);
            this.label17.TabIndex = 23;
            this.label17.Text = "FieldValue5";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(350, 101);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(62, 13);
            this.label16.TabIndex = 22;
            this.label16.Text = "FieldValue4";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(350, 75);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(62, 13);
            this.label15.TabIndex = 21;
            this.label15.Text = "FieldValue3";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(350, 49);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(62, 13);
            this.label14.TabIndex = 20;
            this.label14.Text = "FieldValue2";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(350, 22);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(62, 13);
            this.label13.TabIndex = 19;
            this.label13.Text = "FieldValue1";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(6, 234);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(93, 13);
            this.label12.TabIndex = 18;
            this.label12.Text = "Sender TerminalId";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(6, 207);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(114, 13);
            this.label11.TabIndex = 17;
            this.label11.Text = "Sender DeliverypointId";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(6, 180);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(97, 13);
            this.label10.TabIndex = 16;
            this.label10.Text = "Sender CompanyId";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(6, 153);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(79, 13);
            this.label9.TabIndex = 15;
            this.label9.Text = "Sender ClientId";
            // 
            // tbSenderTerminalId
            // 
            this.tbSenderTerminalId.Location = new System.Drawing.Point(159, 227);
            this.tbSenderTerminalId.Name = "tbSenderTerminalId";
            this.tbSenderTerminalId.Size = new System.Drawing.Size(100, 20);
            this.tbSenderTerminalId.TabIndex = 14;
            this.tbSenderTerminalId.Text = "0";
            // 
            // tbSenderDeliverypointId
            // 
            this.tbSenderDeliverypointId.Location = new System.Drawing.Point(159, 201);
            this.tbSenderDeliverypointId.Name = "tbSenderDeliverypointId";
            this.tbSenderDeliverypointId.Size = new System.Drawing.Size(100, 20);
            this.tbSenderDeliverypointId.TabIndex = 13;
            this.tbSenderDeliverypointId.Text = "0";
            // 
            // tbSenderCompanyId
            // 
            this.tbSenderCompanyId.Location = new System.Drawing.Point(159, 175);
            this.tbSenderCompanyId.Name = "tbSenderCompanyId";
            this.tbSenderCompanyId.Size = new System.Drawing.Size(100, 20);
            this.tbSenderCompanyId.TabIndex = 12;
            this.tbSenderCompanyId.Text = "0";
            // 
            // tbSenderClientId
            // 
            this.tbSenderClientId.Location = new System.Drawing.Point(159, 149);
            this.tbSenderClientId.Name = "tbSenderClientId";
            this.tbSenderClientId.Size = new System.Drawing.Size(100, 20);
            this.tbSenderClientId.TabIndex = 11;
            this.tbSenderClientId.Text = "0";
            // 
            // btnSendMessage
            // 
            this.btnSendMessage.Location = new System.Drawing.Point(83, 275);
            this.btnSendMessage.Name = "btnSendMessage";
            this.btnSendMessage.Size = new System.Drawing.Size(176, 23);
            this.btnSendMessage.TabIndex = 10;
            this.btnSendMessage.Text = "Send Message";
            this.btnSendMessage.UseVisualStyleBackColor = true;
            this.btnSendMessage.Click += new System.EventHandler(this.btnSendMessage_Click);
            // 
            // tbReceiverTerminalId
            // 
            this.tbReceiverTerminalId.Location = new System.Drawing.Point(159, 124);
            this.tbReceiverTerminalId.Name = "tbReceiverTerminalId";
            this.tbReceiverTerminalId.Size = new System.Drawing.Size(100, 20);
            this.tbReceiverTerminalId.TabIndex = 9;
            this.tbReceiverTerminalId.Text = "0";
            // 
            // tbReceiverCompanyId
            // 
            this.tbReceiverCompanyId.Location = new System.Drawing.Point(159, 72);
            this.tbReceiverCompanyId.Name = "tbReceiverCompanyId";
            this.tbReceiverCompanyId.Size = new System.Drawing.Size(100, 20);
            this.tbReceiverCompanyId.TabIndex = 8;
            this.tbReceiverCompanyId.Text = "0";
            // 
            // tbReceiverClientId
            // 
            this.tbReceiverClientId.Location = new System.Drawing.Point(159, 46);
            this.tbReceiverClientId.Name = "tbReceiverClientId";
            this.tbReceiverClientId.Size = new System.Drawing.Size(100, 20);
            this.tbReceiverClientId.TabIndex = 7;
            this.tbReceiverClientId.Text = "0";
            // 
            // tbReceiverDeliverypointId
            // 
            this.tbReceiverDeliverypointId.Location = new System.Drawing.Point(159, 98);
            this.tbReceiverDeliverypointId.Name = "tbReceiverDeliverypointId";
            this.tbReceiverDeliverypointId.Size = new System.Drawing.Size(100, 20);
            this.tbReceiverDeliverypointId.TabIndex = 6;
            this.tbReceiverDeliverypointId.Text = "0";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(6, 75);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(106, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = "Receiver CompanyId";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(6, 49);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(88, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Receiver ClientId";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 127);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(102, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Receiver TerminalId";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(6, 101);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(123, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Receiver DeliverypointId";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "MessageType";
            // 
            // cmbMessageType
            // 
            this.cmbMessageType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMessageType.FormattingEnabled = true;
            this.cmbMessageType.Location = new System.Drawing.Point(159, 19);
            this.cmbMessageType.Name = "cmbMessageType";
            this.cmbMessageType.Size = new System.Drawing.Size(174, 21);
            this.cmbMessageType.TabIndex = 0;
            this.cmbMessageType.SelectedIndexChanged += new System.EventHandler(this.cmbMessageType_SelectedIndexChanged);
            // 
            // netmessageBindingSource
            // 
            this.netmessageBindingSource.DataSource = typeof(Obymobi.Logic.Model.Netmessage);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(3, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(638, 352);
            this.tabControl1.TabIndex = 8;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.gbConnection);
            this.tabPage1.Controls.Add(this.gbClientInfo);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.btnConnect);
            this.tabPage1.Controls.Add(this.lblConnectionStatus);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(630, 326);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Connect";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // gbConnection
            // 
            this.gbConnection.Controls.Add(this.tbHost);
            this.gbConnection.Controls.Add(this.label2);
            this.gbConnection.Controls.Add(this.cmbClientType);
            this.gbConnection.Controls.Add(this.label23);
            this.gbConnection.Controls.Add(this.tbSalt);
            this.gbConnection.Controls.Add(this.label24);
            this.gbConnection.Controls.Add(this.tbIdentifier);
            this.gbConnection.Controls.Add(this.label25);
            this.gbConnection.Location = new System.Drawing.Point(10, 26);
            this.gbConnection.Name = "gbConnection";
            this.gbConnection.Size = new System.Drawing.Size(313, 130);
            this.gbConnection.TabIndex = 12;
            this.gbConnection.TabStop = false;
            this.gbConnection.Text = "Connection";
            // 
            // cmbClientType
            // 
            this.cmbClientType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbClientType.FormattingEnabled = true;
            this.cmbClientType.Location = new System.Drawing.Point(85, 97);
            this.cmbClientType.Name = "cmbClientType";
            this.cmbClientType.Size = new System.Drawing.Size(214, 21);
            this.cmbClientType.TabIndex = 10;
            this.cmbClientType.SelectedIndexChanged += new System.EventHandler(this.cbClientType_SelectedIndexChanged);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(8, 48);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(47, 13);
            this.label23.TabIndex = 5;
            this.label23.Text = "Identifier";
            // 
            // tbSalt
            // 
            this.tbSalt.Location = new System.Drawing.Point(85, 71);
            this.tbSalt.Name = "tbSalt";
            this.tbSalt.Size = new System.Drawing.Size(214, 20);
            this.tbSalt.TabIndex = 9;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(8, 74);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(25, 13);
            this.label24.TabIndex = 6;
            this.label24.Text = "Salt";
            // 
            // tbIdentifier
            // 
            this.tbIdentifier.Location = new System.Drawing.Point(85, 45);
            this.tbIdentifier.Name = "tbIdentifier";
            this.tbIdentifier.Size = new System.Drawing.Size(214, 20);
            this.tbIdentifier.TabIndex = 8;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(8, 100);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(60, 13);
            this.label25.TabIndex = 7;
            this.label25.Text = "Client Type";
            // 
            // gbClientInfo
            // 
            this.gbClientInfo.Controls.Add(this.cbClientTypeMasterConsole);
            this.gbClientInfo.Controls.Add(this.tbClientTypeTerminalId);
            this.gbClientInfo.Controls.Add(this.tbClientTypeDeliverypointgroupId);
            this.gbClientInfo.Controls.Add(this.tbClientTypeDeliverypointId);
            this.gbClientInfo.Controls.Add(this.tbClientTypeCompanyId);
            this.gbClientInfo.Controls.Add(this.tbClientTypeClientId);
            this.gbClientInfo.Controls.Add(this.label30);
            this.gbClientInfo.Controls.Add(this.label29);
            this.gbClientInfo.Controls.Add(this.label28);
            this.gbClientInfo.Controls.Add(this.label27);
            this.gbClientInfo.Controls.Add(this.label26);
            this.gbClientInfo.Location = new System.Drawing.Point(329, 6);
            this.gbClientInfo.Name = "gbClientInfo";
            this.gbClientInfo.Size = new System.Drawing.Size(295, 172);
            this.gbClientInfo.TabIndex = 11;
            this.gbClientInfo.TabStop = false;
            this.gbClientInfo.Text = "Client Info";
            // 
            // cbClientTypeMasterConsole
            // 
            this.cbClientTypeMasterConsole.AutoSize = true;
            this.cbClientTypeMasterConsole.Location = new System.Drawing.Point(136, 149);
            this.cbClientTypeMasterConsole.Name = "cbClientTypeMasterConsole";
            this.cbClientTypeMasterConsole.Size = new System.Drawing.Size(99, 17);
            this.cbClientTypeMasterConsole.TabIndex = 10;
            this.cbClientTypeMasterConsole.Text = "Master Console";
            this.cbClientTypeMasterConsole.UseVisualStyleBackColor = true;
            // 
            // tbClientTypeTerminalId
            // 
            this.tbClientTypeTerminalId.Location = new System.Drawing.Point(136, 123);
            this.tbClientTypeTerminalId.Name = "tbClientTypeTerminalId";
            this.tbClientTypeTerminalId.Size = new System.Drawing.Size(100, 20);
            this.tbClientTypeTerminalId.TabIndex = 9;
            this.tbClientTypeTerminalId.Text = "0";
            // 
            // tbClientTypeDeliverypointgroupId
            // 
            this.tbClientTypeDeliverypointgroupId.Location = new System.Drawing.Point(136, 97);
            this.tbClientTypeDeliverypointgroupId.Name = "tbClientTypeDeliverypointgroupId";
            this.tbClientTypeDeliverypointgroupId.Size = new System.Drawing.Size(100, 20);
            this.tbClientTypeDeliverypointgroupId.TabIndex = 8;
            this.tbClientTypeDeliverypointgroupId.Text = "0";
            // 
            // tbClientTypeDeliverypointId
            // 
            this.tbClientTypeDeliverypointId.Location = new System.Drawing.Point(136, 71);
            this.tbClientTypeDeliverypointId.Name = "tbClientTypeDeliverypointId";
            this.tbClientTypeDeliverypointId.Size = new System.Drawing.Size(100, 20);
            this.tbClientTypeDeliverypointId.TabIndex = 7;
            this.tbClientTypeDeliverypointId.Text = "0";
            // 
            // tbClientTypeCompanyId
            // 
            this.tbClientTypeCompanyId.Location = new System.Drawing.Point(136, 45);
            this.tbClientTypeCompanyId.Name = "tbClientTypeCompanyId";
            this.tbClientTypeCompanyId.Size = new System.Drawing.Size(100, 20);
            this.tbClientTypeCompanyId.TabIndex = 6;
            this.tbClientTypeCompanyId.Text = "0";
            // 
            // tbClientTypeClientId
            // 
            this.tbClientTypeClientId.Location = new System.Drawing.Point(136, 19);
            this.tbClientTypeClientId.Name = "tbClientTypeClientId";
            this.tbClientTypeClientId.Size = new System.Drawing.Size(100, 20);
            this.tbClientTypeClientId.TabIndex = 5;
            this.tbClientTypeClientId.Text = "0";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(6, 126);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(56, 13);
            this.label30.TabIndex = 4;
            this.label30.Text = "TerminalId";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(6, 100);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(104, 13);
            this.label29.TabIndex = 3;
            this.label29.Text = "DeliverypointgroupId";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(6, 74);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(77, 13);
            this.label28.TabIndex = 2;
            this.label28.Text = "DeliverypointId";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(6, 48);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(60, 13);
            this.label27.TabIndex = 1;
            this.label27.Text = "CompanyId";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(6, 22);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(45, 13);
            this.label26.TabIndex = 0;
            this.label26.Text = "Client Id";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(630, 326);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Message";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // UcComet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbEventLog);
            this.MinimumSize = new System.Drawing.Size(644, 558);
            this.Name = "UcComet";
            this.Size = new System.Drawing.Size(644, 572);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.netmessageBindingSource)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.gbConnection.ResumeLayout(false);
            this.gbConnection.PerformLayout();
            this.gbClientInfo.ResumeLayout(false);
            this.gbClientInfo.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button btnConnect;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label lblConnectionStatus;
		private System.Windows.Forms.TextBox tbHost;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox tbEventLog;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.TextBox tbReceiverTerminalId;
		private System.Windows.Forms.TextBox tbReceiverCompanyId;
		private System.Windows.Forms.TextBox tbReceiverClientId;
		private System.Windows.Forms.TextBox tbReceiverDeliverypointId;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.ComboBox cmbMessageType;
		private System.Windows.Forms.BindingSource netmessageBindingSource;
		private System.Windows.Forms.Button btnSendMessage;
		private System.Windows.Forms.TextBox tbFieldValue5;
		private System.Windows.Forms.TextBox tbFieldValue4;
		private System.Windows.Forms.TextBox tbFieldValue3;
		private System.Windows.Forms.TextBox tbFieldValue2;
		private System.Windows.Forms.TextBox tbFieldValue1;
		private System.Windows.Forms.TextBox tbFieldValue6;
		private System.Windows.Forms.TextBox tbFieldValue7;
		private System.Windows.Forms.TextBox tbFieldValue8;
		private System.Windows.Forms.TextBox tbFieldValue9;
		private System.Windows.Forms.Label label22;
		private System.Windows.Forms.TextBox tbFieldValue10;
		private System.Windows.Forms.Label label21;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox tbSenderTerminalId;
		private System.Windows.Forms.TextBox tbSenderDeliverypointId;
		private System.Windows.Forms.TextBox tbSenderCompanyId;
		private System.Windows.Forms.TextBox tbSenderClientId;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.GroupBox gbClientInfo;
		private System.Windows.Forms.CheckBox cbClientTypeMasterConsole;
		private System.Windows.Forms.TextBox tbClientTypeTerminalId;
		private System.Windows.Forms.TextBox tbClientTypeDeliverypointgroupId;
		private System.Windows.Forms.TextBox tbClientTypeDeliverypointId;
		private System.Windows.Forms.TextBox tbClientTypeCompanyId;
		private System.Windows.Forms.TextBox tbClientTypeClientId;
		private System.Windows.Forms.Label label30;
		private System.Windows.Forms.Label label29;
		private System.Windows.Forms.Label label28;
		private System.Windows.Forms.Label label27;
		private System.Windows.Forms.Label label26;
		private System.Windows.Forms.ComboBox cmbClientType;
		private System.Windows.Forms.TextBox tbSalt;
		private System.Windows.Forms.TextBox tbIdentifier;
		private System.Windows.Forms.Label label25;
		private System.Windows.Forms.Label label24;
		private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.GroupBox gbConnection;
	}
}
