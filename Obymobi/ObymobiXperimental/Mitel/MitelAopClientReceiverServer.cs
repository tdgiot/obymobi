﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ObymobiXperimental.Mitel
{
    public class MitelAopClientReceiverServer : TcpServiceProvider
    {
        private MitelAopClient client;
        private Encoding iso8859 = System.Text.Encoding.GetEncoding("iso-8859-1");

        public MitelAopClientReceiverServer(MitelAopClient client)
        {
            this.client = client;
            this.Logged += MitelAopClientReceiverServer_Logged;
        }

        void MitelAopClientReceiverServer_Logged(object sender, Dionysos.LoggingLevel level, string log)
        {
            this.client.LogDebug(log);
        }

        public override object Clone()
        {
            return new MitelAopClientReceiverServer(this.client);
        }

        public override void OnAcceptConnection(ConnectionState state)
        {
            this.client.LogDebug("Connection accepted: {0}", state.RemoteEndPoint.ToString());
        }

        public override void OnReceiveData(ConnectionState state)
        {
            string _receivedStr = "";
            byte[] buffer = new byte[1024];
            this.client.LogDebug("Starting to receive response from: {0}", state.RemoteEndPoint.ToString());
            while (state.AvailableData > 0)
            {
                int readBytes = state.Read(buffer, 0, 1024);
                if (readBytes > 0)
                {
                    _receivedStr += iso8859.GetString(buffer, 0, readBytes);
                    if (_receivedStr.IndexOf("</OAP-response>") >= 0)
                    {
                        this.client.LogDebug("Response received: {0}", _receivedStr);
                        _receivedStr = "";
                    }
                }
                else 
                    state.EndConnection();                
            }
        }

        public override void OnDropConnection(ConnectionState state)
        {
            this.client.LogDebug("Connection dropped: {0}", state.RemoteEndPoint.ToString());
        }

        public override string LogPrefix()
        {
            return "MitelAopClientReceiverServer";
        }
    }
}
