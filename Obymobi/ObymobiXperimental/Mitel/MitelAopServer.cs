﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dionysos;

namespace ObymobiXperimental.Mitel
{
    public class MitelAopServer : TcpServiceProvider
    {
        private string _receivedStr;
        private Encoding iso8859 = System.Text.Encoding.GetEncoding("iso-8859-1");            

        public override object Clone()
        {
            return new MitelAopServer();
        }

        public override void
               OnAcceptConnection(ConnectionState state)
        {
            this.LogDebug("Connection accepted: {0}", state.RemoteEndPoint.ToString());
            //_receivedStr = "";
            //if (!state.Write(Encoding.UTF8.GetBytes(
            //                "MitelAopServer reportin' in.\r\n"), 0, 14))
            //    state.EndConnection();
            //if write fails... then close connection
        }


        public override void OnReceiveData(ConnectionState state)
        {
            byte[] buffer = new byte[1024];
            while (state.AvailableData > 0)
            {
                int readBytes = state.Read(buffer, 0, 1024);
                if (readBytes > 0)
                {
                    _receivedStr += iso8859.GetString(buffer, 0, readBytes);
                    if (_receivedStr.IndexOf("</OAP-request>") >= 0)
                    {
                        // Request is now finished 
                        // Directly report back in 3 seconds
                        
                        // Get IP
                        Regex regexIp = new Regex("<IP>(.*?)</IP>", RegexOptions.IgnoreCase);
                        var ipMatches = regexIp.Match(_receivedStr);
                        string ip = ipMatches.Groups[1].ToString();

                        // Get Port
                        Regex regexPort = new Regex("<Port>(.*?)</Port>", RegexOptions.IgnoreCase);
                        var portMatches = regexPort.Match(_receivedStr);
                        int port = Convert.ToInt32(portMatches.Groups[1].ToString());

                        // Get TransactionId
                        Regex regexTransactionId = new Regex("id=\"(.*?)\"", RegexOptions.IgnoreCase);
                        var transactionMatches = regexTransactionId.Match(_receivedStr);
                        int transactionId = Convert.ToInt32(transactionMatches.Groups[1].ToString());                                                

                        StringBuilder sb = new StringBuilder();
                        sb.AppendFormatLine("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
                        sb.AppendFormatLine("<OAP-response version=\"3.0\" id=\"{0}\">",transactionId);
                        //sb.AppendFormatLine("    <Status service=\"service_name\">");
                        sb.AppendFormatLine("        <Type>1</Type>");
                        sb.AppendFormatLine("        <Code>200</Code>");
                        //sb.AppendFormatLine("        <Text>Descriptive text</Text>");
                        //sb.AppendFormatLine("        <TypeSpecific>Elements that are specific for the status type.</TypeSpecific>");
                        sb.AppendFormatLine("    </Status>");
                        sb.AppendFormatLine("</OAP-response>");

                        Thread.Sleep(2000);

                        var bytesToReturn = iso8859.GetBytes(sb.ToString());

                        state.Write(bytesToReturn, 0, bytesToReturn.Length);
                        this.LogVerbose("Received from {0}: '{1}'", state.RemoteEndPoint.ToString(), _receivedStr);
                        _receivedStr = "";
                        
                        Task.Factory.StartDelayed(2000, () => returnDeliveryReport(transactionId, ip, port));
                    }
                }
                else state.EndConnection();
                //If read fails then close connection
            }
        }

        public void returnDeliveryReport(int transactionId, string ip, int port)
        {
            TcpClient tcpclient = new TcpClient();
            tcpclient.Connect(IPAddress.Parse(ip), port);

            StringBuilder sb = new StringBuilder();
            sb.AppendFormatLine("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
            sb.AppendFormatLine("<OAP-response version=\"3.0\" id=\"{0}\">", transactionId);
            sb.AppendFormatLine("    <Status>");
            sb.AppendFormatLine("        <Type>3</Type>");
            sb.AppendFormatLine("        <Code>200</Code>");
            sb.AppendFormatLine("    </Status>");
            sb.AppendFormatLine("</OAP-response>");
            var load = iso8859.GetBytes(sb.ToString());

            Stream stream = tcpclient.GetStream();
            stream.Write(load, 0, load.Length);
            stream.Flush();
            tcpclient.Close();

            Task.Factory.StartDelayed(2000, () => returnAcceptanceReport(transactionId, ip, port));
        }

        public void returnAcceptanceReport(int transactionId, string ip, int port)
        {
            TcpClient tcpclient = new TcpClient();
            tcpclient.Connect(IPAddress.Parse(ip), port);

            StringBuilder sb = new StringBuilder();
            sb.AppendFormatLine("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
            sb.AppendFormatLine("<OAP-response version=\"3.0\" id=\"{0}\">", transactionId);
            sb.AppendFormatLine("    <Status>");
            sb.AppendFormatLine("        <Type>1</Type>");
            sb.AppendFormatLine("        <Code>200</Code>");
            sb.AppendFormatLine("    </Status>");
            sb.AppendFormatLine("</OAP-response>");
            var load = iso8859.GetBytes(sb.ToString());

            Stream stream = tcpclient.GetStream();
            stream.Write(load, 0, load.Length);
            stream.Flush();
            tcpclient.Close();
        }

        public override void OnDropConnection(ConnectionState state)
        {
            //Nothing to clean here
        }

        public override string LogPrefix()
        {
            return "MitelAopServer";
        }
    }
}
