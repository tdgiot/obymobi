﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace ObymobiXperimental.Mitel
{
    public class MitelAopRequest
    {
        public MitelAopRequest(string senderIp, int senderPort)
        {
            this.SenderIp = senderIp;
            this.SenderPort = senderPort;
            this.TransactionId = -1;
            this.Sent = false;
        }

        public int TransactionId;
        public string To;
        public string Subject;
        public string BodyText;
        public int Priority;
        public bool Sent;
        public string SenderIp;
        public int SenderPort;

        public string ToXmlRequest()
        { 
            StringBuilder sb = new StringBuilder();
            sb.AppendFormatLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            sb.AppendFormatLine("<OAP-request version=\"3.0\" id=\"{0}\">", this.TransactionId);
            sb.AppendFormatLine("   <Header>");
            sb.AppendFormatLine("      <IP>{0}</IP>", this.SenderIp);
            sb.AppendFormatLine("      <Port>{0}</Port>", this.SenderPort);
            sb.AppendFormatLine("   </Header>");
            sb.AppendFormatLine("   <Body service=\"message\">");
            sb.AppendFormatLine("      <To>{0}</To>", this.To);
            sb.AppendFormatLine("      <SubjectText>{0}</SubjectText>", this.Subject);
            sb.AppendFormatLine("      <BodyText>{0}</BodyText>", this.BodyText);
            sb.AppendFormatLine("      <Priority>{0}</Priority>", this.Priority);
            sb.AppendFormatLine("      <BeepCode>2</BeepCode>");
//          sb.AppendFormatLine("      <AvailabilityStatus />");
            sb.AppendFormatLine("      <DeliveryStatus />");
            sb.AppendFormatLine("      <AllowErase />");
            sb.AppendFormatLine("      <ManualAckStatus>");
            sb.AppendFormatLine("         <Mode>AcceptAndReject</Mode>");
            sb.AppendFormatLine("      </ManualAckStatus>");
            sb.AppendFormatLine("   </Body>");
            sb.AppendFormatLine("</OAP-request>");

            return sb.ToString();
        }
    }
}
