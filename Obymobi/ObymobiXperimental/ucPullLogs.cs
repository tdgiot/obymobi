﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dionysos;
using Google.Apis.Manual.Util;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Comet;
using Obymobi.Logic.Comet.Interfaces;
using Obymobi.Logic.Comet.Netmessages;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace ObymobiXperimental
{
    public partial class ucPullLogs : UserControl
    {
        private SignalRClient cometClient;

        private const int LIMIT = 10;

        private CancellationTokenSource cts;
        private volatile bool isConnected = false;

        private readonly ConcurrentDictionary<string, AgentContainer> agentResponse = new ConcurrentDictionary<string, AgentContainer>();

        public ucPullLogs()
        {
            InitializeComponent();

            if (!DesignMode)
            {
                CompanyCollection companyCollection = new CompanyCollection();
                companyCollection.SortClauses = new SortExpression(CompanyFields.Name | SortOperator.Ascending);

                IncludeFieldsList fields = new IncludeFieldsList(CompanyFields.CompanyId, CompanyFields.Name);

                companyCollection.GetMulti(null, fields, 0);

                this.cbCompany.ValueMember = "CompanyId";
                this.cbCompany.DisplayMember = "Name";
                this.cbCompany.DataSource = companyCollection;
            }
        }

        private void btnFetchLogs_Click(object sender, EventArgs e)
        {
            int companyId = (int)this.cbCompany.SelectedValue;
            Task.Factory.StartNew(() => FetchLogs(companyId));
        }

        private void FetchLogs(int companyId)
        {
            agentResponse.Clear();

            cts = new CancellationTokenSource();

            List<string> addresses = LoadDeviceMacAddresses(companyId);
            WriteToLog("Number of devices: {0}", addresses.Count);

            cometClient = new SignalRClient(new CometEventListener(this), "https://app.crave-emenu.com/messaging/signalr", CometConstants.CometIdentifierServices, CometConstants.CometInternalSalt);
            cometClient.Connect();

            while (!isConnected && !cts.IsCancellationRequested)
            {
                Thread.Sleep(500);
            }

            Thread.Sleep(1000);

            foreach (string address in addresses)
            {
                if (address.IsNullOrEmpty())
                {
                    continue;
                }

                while (true)
                {
                    if (cts.IsCancellationRequested || agentResponse.Count < LIMIT)
                    {
                        break;
                    }

                    DateTime now = DateTime.UtcNow;
                    IEnumerable<KeyValuePair<string, AgentContainer>> t = agentResponse.Where(pair => pair.Value.Timeout < now);
                    foreach (KeyValuePair<string, AgentContainer> keyValuePair in t)
                    {
                        AgentContainer dummy;
                        agentResponse.TryRemove(keyValuePair.Key, out dummy);
                    }

                    Thread.Sleep(1000);
                }

                if (cts.IsCancellationRequested) break;

                agentResponse.TryAdd(address, new AgentContainer
                                              {
                                                  Timeout = DateTime.UtcNow.AddSeconds(20)
                                              });

                // Connect
                ConnectToAgent(address);
            }

            while (agentResponse.Count > 0)
            {
                if (cts.IsCancellationRequested)
                {
                    break;
                }

                Thread.Sleep(1000);
            }

            WriteToLog("All Done!");
        }

        private List<string> LoadDeviceMacAddresses(int companyId)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ClientFields.CompanyId == companyId);
            filter.Add(DeviceFields.LastRequestUTC > DateTime.UtcNow.AddMinutes(-3));
            
            RelationCollection relation = new RelationCollection();
            relation.Add(DeviceEntityBase.Relations.ClientEntityUsingDeviceId, JoinHint.Right);

            DeviceCollection collection = new DeviceCollection();
            collection.GetMulti(filter, 0, null, relation);

            return collection.Select(d => d.Identifier).ToList();
        }

        private void ConnectToAgent(string deviceMac)
        {
            NetmessageConnectToAgent netmessage = new NetmessageConnectToAgent();
            netmessage.AgentMacAddress = deviceMac;

            cometClient.SendMessage(netmessage);
        }

        private void RequestLog(string deviceMac)
        {
            NetmessageAgentCommandRequest netmessage = new NetmessageAgentCommandRequest();
            netmessage.Guid = GuidUtil.CreateGuid();
            netmessage.AgentMacAddress = deviceMac;
            netmessage.Command = "getlog";

            cometClient.SendMessage(netmessage);
        }

        private void OnConnected()
        {
            isConnected = true;
            if (btnFetchLogs.InvokeRequired)
            {
                this.Invoke((MethodInvoker)OnConnected);
            }
            else
            {
                btnFetchLogs.Enabled = false;
                btnStop.Enabled = true;
            }
        }

        private void OnDisconnect()
        {
            isConnected = false;
            if (btnFetchLogs.InvokeRequired)
            {
                this.Invoke((MethodInvoker)OnDisconnect);
            }
            else
            {
                btnFetchLogs.Enabled = true;
                btnStop.Enabled = false;
            }

        }

        private void SetClientType()
        {
            cometClient.SetAuthenticated(true);

            NetmessageSetClientType messageClientType = new NetmessageSetClientType();
            messageClientType.ClientType = NetmessageClientType.Cms;
            cometClient.SendMessage(messageClientType);            
        }

        private void Pong()
        {
            cometClient.Pong();
        }

        public void WriteToLog(string log, params object[] args)
        {
            if (this.tbEventLog.InvokeRequired)
            {
                this.tbEventLog.Invoke((MethodInvoker)(() => this.WriteToLog(log, args)));
            }
            else
            {
                this.tbEventLog.AppendText(log.FormatSafe(args) + "\r\n");
            }
        }

        protected void SetAgentResponse(string mac, string message)
        {
            AgentContainer container;
            if (agentResponse.TryGetValue(mac, out container))
            {
                if (!container.Connected)
                {
                    if (message.Contains("NOTICE"))
                    {
                        container.Timeout = DateTime.UtcNow.AddMinutes(1);
                        container.Connected = true;
                        agentResponse.AddOrUpdate(mac, container, (s, agentContainer) => container);

                        RequestLog(mac);
                    }
                    else
                    {
                        agentResponse.TryRemove(mac, out container);
                    }
                }
                else if (message.Contains("Log:"))
                {
                    WriteToLog("[{0}] {1}", mac, message);
                    agentResponse.TryRemove(mac, out container);
                }
            }
        }

        class CometEventListener : ICometClientEventListener
        {
            private readonly ucPullLogs parent;

            public CometEventListener(ucPullLogs p)
            {
                parent = p;
            }

            public void OnConnectFailed()
            {
                parent.WriteToLog("Connection Failed!");
                parent.OnDisconnect();
            }

            public void OnConnectionChanged(bool isConnected)
            {
                if (isConnected)
                {
                    parent.WriteToLog("Connected!");
                }
                else
                {
                    parent.WriteToLog("Connection Lost!");
                    parent.OnDisconnect();
                }
            }

            public void OnNetmessageReceived(Netmessage netmessage)
            {
                if (netmessage.MessageType != NetmessageType.AgentCommandResponse)
                {
                    parent.WriteToLog("Received Netmessage: {0}", netmessage.GetMessageType());
                }

                if (netmessage.MessageType == NetmessageType.AuthenticateResult)
                {
                    var message = netmessage.ConvertTo<NetmessageAuthenticateResult>();
                    parent.cometClient.IsAuthenticated = message.IsAuthenticated;
                    if (message.IsAuthenticated)
                    {
                        parent.SetClientType();
                        parent.WriteToLog("Authenticated, ready to send messages now!");
                        parent.OnConnected();
                    }
                    else
                    {
                        parent.WriteToLog("Failed to authenticate. Error: {0}", message.AuthenticateErrorMessage);
                        parent.OnDisconnect();
                    }
                }
                else if (netmessage.MessageType == NetmessageType.Ping)
                {
                    parent.Pong();
                }
                else if (netmessage.MessageType == NetmessageType.Disconnect)
                {
                    OnConnectionChanged(false);
                }
                else if (netmessage.MessageType == NetmessageType.AgentCommandResponse)
                {
                    string mac = netmessage.SenderIdentifier ?? netmessage.FieldValue2;
                    if (!mac.IsNullOrEmpty())
                    {
                        //parent.WriteToLog("Mac: {0} | Value: {1}", mac, netmessage.FieldValue1);
                        parent.SetAgentResponse(mac, netmessage.FieldValue1);
                    }
                }
            }

            public void OnEventLog(string message, params object[] args)
            {
                //parent.WriteToLog(message, args);
            }
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            cometClient.Disconnect();
            cometClient.Dispose();
        }

        private class AgentContainer
        {
            public DateTime Timeout { get; set; }
            public bool Connected { get; set; }
            //public string Response { get; set; }
        }
    }
}
