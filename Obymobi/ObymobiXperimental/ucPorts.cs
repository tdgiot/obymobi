﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Dionysos;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;

namespace ObymobiXperimental
{
    public partial class ucPorts : UserControl
    {
        System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();

        public ucPorts()
        {
            InitializeComponent();

            var ips = Dionysos.Net.NetUtil.GetLocalIps();
            foreach (var ip in ips)
                this.cbServerIp.Items.Add(ip);

            timer.Interval = 2500;
            timer.Tick += timer_Tick;
            timer.Start();
        }

        int maxServerPorts = 0;

        void timer_Tick(object sender, EventArgs e)
        {
            //Create process
            System.Diagnostics.Process pProcess = new System.Diagnostics.Process();

            //strCommand is path and file name of command to run
            pProcess.StartInfo.FileName = "netstat";

            //strCommandParameters are parameters to pass to program
            pProcess.StartInfo.Arguments = "-ano";// | findstr TCP";

            pProcess.StartInfo.UseShellExecute = false;

            //Set output of program to be written to process output stream
            pProcess.StartInfo.RedirectStandardOutput = true;

            //Optional
            // pProcess.StartInfo.WorkingDirectory = strWorkingDirectory;
            pProcess.StartInfo.CreateNoWindow = true;
            pProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;

            //Start the process
            pProcess.Start();

            //Get program output
            string strOutput = pProcess.StandardOutput.ReadToEnd();
            
            //Wait for process to finish
            pProcess.WaitForExit();

            this.lblOpenedTotal.Text = strOutput.CountOccurencesOfString("TCP").ToString();
            int serverports = strOutput.CountOccurencesOfString(this.cbServerIp.Text + ":" + this.tbServerPort.Text.ToString()) / 2;            

            if(serverports > this.maxServerPorts)
                this.maxServerPorts = serverports;

            this.lblOpenedByServer.Text = "{0} (Max: {1})".FormatSafe(serverports, this.maxServerPorts);
        }

        private void cbServerIp_TextChanged(object sender, EventArgs e)
        {
            if (this.tbClientIp.Text.IsNullOrWhiteSpace())
            {
                this.tbClientIp.Text = this.cbServerIp.Text;
            }
        }

        TcpTestServer server = null;
        private void btStartServer_Click(object sender, EventArgs e)
        {
            if (server == null)
            {
                string serverIp = this.cbServerIp.Text;
                int port = Convert.ToInt32(this.tbServerPort.Text);
                Task.Factory.StartNew(() =>
                {
                    server = new TcpTestServer(serverIp, port);
                    server.Logged += server_Logged;
                    server.Start();
                });
            }
        }

        void server_Logged(object sender, LoggingLevel level, string log)
        {
            this.WriteLog(log);            
        }

        private void btStartClient_Click(object sender, EventArgs e)
        {
            string serverIp = this.tbClientIp.Text;
            int port = Convert.ToInt32(this.tbClientPort.Text);
            int instances = Convert.ToInt32(this.tbClientInstances.Text);
            for (int i = 0; i < instances; i++)
            {
                int j = i;                
                Thread t = new Thread(() => CreatClient(j, serverIp, port, this));
                t.Start();                 
            }
        }

        private static void CreatClient(int j, string serverIp, int port, ucPorts uc)
        {            
            TcpTestClient client = new TcpTestClient(serverIp, port, "Test Message - " + j.ToString() + " - " + DateTime.Now.Ticks);
            client.Logged += uc.client_Logged;
            client.Start();
        }

        void client_Logged(object sender, LoggingLevel level, string log)
        {
            this.WriteLog(log);            
        }

        int logsWritten = 0;
        delegate void AddToLogDelegate(string text);
        private void WriteLog(string format, params object[] args)
        {
            if (this.tbLog.InvokeRequired)
            {
                AddToLogDelegate d = new AddToLogDelegate(WriteLogLoopie);
                try
                {
                    this.Invoke(d, new object[] { string.Format(format, args) });
                }
                catch
                {
                    string text = string.Format("Format: {0}, Args: {1}", format, StringUtil.CombineWithCommaSpace(args));
                }
            }
            else
            {
                if (this.tbLog.Text.IsNullOrWhiteSpace())
                    this.tbLog.Text = string.Empty;
                this.tbLog.Text = string.Format(format, args) + Environment.NewLine + this.tbLog.Text;
            }
            this.logsWritten++;

            if (logsWritten >= 100)
            {
                this.tbLog.Text = string.Empty;
                this.logsWritten = 0;
            }
        }

        private void WriteLogLoopie(string text)
        {
            this.WriteLog(text);
        }
    }
}
