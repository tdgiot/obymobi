﻿using System;
using System.Data;
using System.Threading.Tasks;
using System.Windows.Forms;
using Google.Apis.Manual.Analytics.v3.Data;
using Obymobi.Web.Google;

namespace ObymobiXperimental
{
    public partial class ucAnalytics : UserControl
    {
        public ucAnalytics()
        {
            InitializeComponent();
        }

        private void btnGetData_Click(object sender, EventArgs e)
        {
            this.btnGetData.Enabled = false;
            Task.Factory.StartNew(() => GetGoogleData(dateTimeFrom.Value, dateTimeTo.Value));
        }

        private void GetGoogleData(DateTime dateFrom, DateTime dateTo)
        {
            var google = new GoogleAPI();
            if (google.SetProfile("UA-45902422-1"))
            {
                var globalViews = google.Get("ga:screenviews", "ga:screenName", null, "-ga:screenViews", dateFrom, dateTo);
                AddToGrid(globalViews);
            }
            else
            {
                AddToGrid(null);
            }
        }

        private void AddToGrid(GaData data)
        {
            if (this.dataGridViewResult.InvokeRequired)
            {
                this.dataGridViewResult.Invoke(new Action(() => AddToGrid(data)));
                return;
            }

            this.btnGetData.Enabled = true;

            if (data == null)
            {
                MessageBox.Show("Google ID not found!");
            }
            else if (data.TotalResults > 0)
            {
                var table = new DataTable();
                for (int i =0; i < data.Rows[0].Count; i++)
                {
                    table.Columns.Add(i.ToString());
                }

                for (int i = 0; i < data.TotalResults; i++)
                {
                    DataRow row = table.NewRow();
                    var gaRow = data.Rows[i];

                    for (int j = 0; j < gaRow.Count; j++)
                    {
                        row[j] = gaRow[j];
                    }

                    table.Rows.Add(row);
                }

                this.dataGridViewResult.DataSource = table;
                this.dataGridViewResult.Refresh();
            }
        }
    }
}
