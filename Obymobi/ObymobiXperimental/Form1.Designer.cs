﻿namespace ObymobiXperimental
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPms = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPmsSimulateCrave = new System.Windows.Forms.TabPage();
            this.label13 = new System.Windows.Forms.Label();
            this.btCraveToPmsGetGuestInformation = new System.Windows.Forms.Button();
            this.btCraveToPmsGetFolio = new System.Windows.Forms.Button();
            this.tbCraveToPmsRoom = new System.Windows.Forms.MaskedTextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tbCraveToPmsCheckoutBalance = new System.Windows.Forms.MaskedTextBox();
            this.btCraveToPmsCheckout = new System.Windows.Forms.Button();
            this.tbCraveToPmsAddToFolioPrice = new System.Windows.Forms.MaskedTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tbCraveToPmsAddToFolioDescription = new System.Windows.Forms.TextBox();
            this.btCraveToPmsAddToFolio = new System.Windows.Forms.Button();
            this.tabPmsSimulatePms = new System.Windows.Forms.TabPage();
            this.label36 = new System.Windows.Forms.Label();
            this.tbPmsGroup = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.dpPmsCheckout = new System.Windows.Forms.DateTimePicker();
            this.dpPmsCheckin = new System.Windows.Forms.DateTimePicker();
            this.btnEditRoomData = new System.Windows.Forms.Button();
            this.btnCreateOrder = new System.Windows.Forms.Button();
            this.btnClearWakeUp = new System.Windows.Forms.Button();
            this.btnSetWakeUp = new System.Windows.Forms.Button();
            this.btnEditGuestInformation = new System.Windows.Forms.Button();
            this.cbHasVoicemail = new System.Windows.Forms.CheckBox();
            this.cbPmsToCraveCheckedIn = new System.Windows.Forms.CheckBox();
            this.tbPmsToCraveLastname = new System.Windows.Forms.TextBox();
            this.tbPmsToCraveFirstName = new System.Windows.Forms.TextBox();
            this.cbPmsToCraveAllowExpressCheckout = new System.Windows.Forms.CheckBox();
            this.cbPmsToCraveAllowViewBill = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbPmsToCraveOldRoomNumber = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btPmsToCraveRoomMove = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.tbPmsToCraveRoomNumber = new System.Windows.Forms.TextBox();
            this.btPmsToCraveCheckIn = new System.Windows.Forms.Button();
            this.btPmsToCraveCheckout = new System.Windows.Forms.Button();
            this.tabPmsComtrol = new System.Windows.Forms.TabPage();
            this.tbPmsComtrolPort = new System.Windows.Forms.TextBox();
            this.lblComtrolHost = new System.Windows.Forms.Label();
            this.tbPmsComtrolHost = new System.Windows.Forms.TextBox();
            this.lblComtrolPort = new System.Windows.Forms.Label();
            this.btComtrolInit = new System.Windows.Forms.Button();
            this.tabPmsTiger = new System.Windows.Forms.TabPage();
            this.cbPmsTigerEmulateExternalWebservice = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbPmsTigerUserKey = new System.Windows.Forms.TextBox();
            this.cbPmsTigerEmulateMessageInService = new System.Windows.Forms.CheckBox();
            this.label12 = new System.Windows.Forms.Label();
            this.tbPmsTigerExternalWebserviceUrl = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.tbPmsTigerMessageInServiceUrl = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btPmsInitializeTigerTms = new System.Windows.Forms.Button();
            this.btPmsStopTigertms = new System.Windows.Forms.Button();
            this.tabPmsInnsist = new System.Windows.Forms.TabPage();
            this.btnInnsistStop = new System.Windows.Forms.Button();
            this.btnInnsistInitialize = new System.Windows.Forms.Button();
            this.label45 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.tbInnsistWakeUpCode = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.tbInnsistToId = new System.Windows.Forms.TextBox();
            this.tbInnsistFromId = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.tbInnsistPassword = new System.Windows.Forms.TextBox();
            this.tbInnsistUsername = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.tbInnsistOutgoingUrl = new System.Windows.Forms.TextBox();
            this.tbInnsistIncommingUrl = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.tbPmsLog = new System.Windows.Forms.TextBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lblStripConnectionState = new System.Windows.Forms.ToolStripStatusLabel();
            this.tabMitel = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tbMitelLog = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btMitelSend = new System.Windows.Forms.Button();
            this.tbMitelSendPriority = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.tbMitelSendBody = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.tbMitelSendSubject = new System.Windows.Forms.TextBox();
            this.tbMitelSendTo = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tbMitelClientListenerIp = new System.Windows.Forms.TextBox();
            this.btMitelClientDisconnect = new System.Windows.Forms.Button();
            this.btMitelClientConnect = new System.Windows.Forms.Button();
            this.tbMitelClientListenerPort = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.tbMitelClientConnectToPort = new System.Windows.Forms.TextBox();
            this.tbMitelClientConnectToIp = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label27 = new System.Windows.Forms.Label();
            this.tbMitelServerListenerIp = new System.Windows.Forms.TextBox();
            this.btMitelServerStop = new System.Windows.Forms.Button();
            this.btMitelServerStart = new System.Windows.Forms.Button();
            this.tbMitelServerListenerPort = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.tabStresser = new System.Windows.Forms.TabPage();
            this.cbAsync = new System.Windows.Forms.CheckBox();
            this.label29 = new System.Windows.Forms.Label();
            this.tbStresserWebserviceUrl = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.tbStresserSeconds = new System.Windows.Forms.TextBox();
            this.btStresserStart = new System.Windows.Forms.Button();
            this.cbStressGetCompany = new System.Windows.Forms.CheckBox();
            this.cbGetEntertainment = new System.Windows.Forms.CheckBox();
            this.cbStressGetDeliverypoints = new System.Windows.Forms.CheckBox();
            this.cbStressUpdateLastRequest = new System.Windows.Forms.CheckBox();
            this.cbStressGetMenu = new System.Windows.Forms.CheckBox();
            this.cbStressGetOrders = new System.Windows.Forms.CheckBox();
            this.cbStressGetReleaseAndDownloadLocation = new System.Windows.Forms.CheckBox();
            this.cbStressGetSetClientStatus = new System.Windows.Forms.CheckBox();
            this.tabComet = new System.Windows.Forms.TabPage();
            this.ucComet2 = new ObymobiXperimental.UcComet();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tbRandomOutput = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.btnHashTimestampRefresh = new System.Windows.Forms.Button();
            this.btnSaveClientLog = new System.Windows.Forms.Button();
            this.label35 = new System.Windows.Forms.Label();
            this.tbHashParam5 = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.tbHashParam2 = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.tbHashParam4 = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.tbHashParam3 = new System.Windows.Forms.TextBox();
            this.tbHashOutput = new System.Windows.Forms.TextBox();
            this.btnGenerateHash = new System.Windows.Forms.Button();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.tbHashSalt = new System.Windows.Forms.TextBox();
            this.tbHashMacAddress = new System.Windows.Forms.TextBox();
            this.tbHashParam1 = new System.Windows.Forms.TextBox();
            this.tbHashTimestamp = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.btSerialisationTest = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnGetLastState = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnKillSpawns = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.tbPokeInUrl = new System.Windows.Forms.TextBox();
            this.tbEventLog = new System.Windows.Forms.TextBox();
            this.btnSpawnCometClients = new System.Windows.Forms.Button();
            this.numClients = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.ucWebserviceLoadTester1 = new ObymobiXperimental.ucWebserviceLoadTester();
            this.tabMetrics = new System.Windows.Forms.TabPage();
            this.ucMetrics1 = new ObymobiXperimental.ucMetrics();
            this.tabPorts = new System.Windows.Forms.TabPage();
            this.ucPorts1 = new ObymobiXperimental.ucPorts();
            this.button2 = new System.Windows.Forms.Button();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.ucAnalytics1 = new ObymobiXperimental.ucAnalytics();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.btnYahooWeatherForecast = new System.Windows.Forms.Button();
            this.btnYahooWeatherCurrent = new System.Windows.Forms.Button();
            this.btnOWMForecast = new System.Windows.Forms.Button();
            this.btnOWMCurrent = new System.Windows.Forms.Button();
            this.process1 = new System.Diagnostics.Process();
            this.ucComet1 = new ObymobiXperimental.UcComet();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.btCreateServiceOrder = new System.Windows.Forms.Button();
            this.tbHotSOSUrl = new System.Windows.Forms.TextBox();
            this.tbHotSOSUsername = new System.Windows.Forms.TextBox();
            this.tbHotSOSPassword = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.tbHotSOSResponse = new System.Windows.Forms.TextBox();
            this.tabControl1.SuspendLayout();
            this.tabPms.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPmsSimulateCrave.SuspendLayout();
            this.tabPmsSimulatePms.SuspendLayout();
            this.tabPmsComtrol.SuspendLayout();
            this.tabPmsTiger.SuspendLayout();
            this.tabPmsInnsist.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.tabMitel.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabStresser.SuspendLayout();
            this.tabComet.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numClients)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.tabMetrics.SuspendLayout();
            this.tabPorts.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPms);
            this.tabControl1.Controls.Add(this.tabMitel);
            this.tabControl1.Controls.Add(this.tabStresser);
            this.tabControl1.Controls.Add(this.tabComet);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabMetrics);
            this.tabControl1.Controls.Add(this.tabPorts);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1514, 1132);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPms
            // 
            this.tabPms.Controls.Add(this.splitContainer1);
            this.tabPms.Controls.Add(this.statusStrip1);
            this.tabPms.Location = new System.Drawing.Point(4, 29);
            this.tabPms.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPms.Name = "tabPms";
            this.tabPms.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPms.Size = new System.Drawing.Size(1494, 1052);
            this.tabPms.TabIndex = 0;
            this.tabPms.Text = "PMS";
            this.tabPms.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(4, 5);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tabControl2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tbPmsLog);
            this.splitContainer1.Size = new System.Drawing.Size(1486, 1010);
            this.splitContainer1.SplitterDistance = 231;
            this.splitContainer1.SplitterWidth = 6;
            this.splitContainer1.TabIndex = 15;
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPmsSimulateCrave);
            this.tabControl2.Controls.Add(this.tabPmsSimulatePms);
            this.tabControl2.Controls.Add(this.tabPmsComtrol);
            this.tabControl2.Controls.Add(this.tabPmsTiger);
            this.tabControl2.Controls.Add(this.tabPmsInnsist);
            this.tabControl2.Location = new System.Drawing.Point(0, 0);
            this.tabControl2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(1480, 229);
            this.tabControl2.TabIndex = 14;
            // 
            // tabPmsSimulateCrave
            // 
            this.tabPmsSimulateCrave.Controls.Add(this.label13);
            this.tabPmsSimulateCrave.Controls.Add(this.btCraveToPmsGetGuestInformation);
            this.tabPmsSimulateCrave.Controls.Add(this.btCraveToPmsGetFolio);
            this.tabPmsSimulateCrave.Controls.Add(this.tbCraveToPmsRoom);
            this.tabPmsSimulateCrave.Controls.Add(this.label9);
            this.tabPmsSimulateCrave.Controls.Add(this.tbCraveToPmsCheckoutBalance);
            this.tabPmsSimulateCrave.Controls.Add(this.btCraveToPmsCheckout);
            this.tabPmsSimulateCrave.Controls.Add(this.tbCraveToPmsAddToFolioPrice);
            this.tabPmsSimulateCrave.Controls.Add(this.label3);
            this.tabPmsSimulateCrave.Controls.Add(this.label4);
            this.tabPmsSimulateCrave.Controls.Add(this.tbCraveToPmsAddToFolioDescription);
            this.tabPmsSimulateCrave.Controls.Add(this.btCraveToPmsAddToFolio);
            this.tabPmsSimulateCrave.Location = new System.Drawing.Point(4, 29);
            this.tabPmsSimulateCrave.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPmsSimulateCrave.Name = "tabPmsSimulateCrave";
            this.tabPmsSimulateCrave.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPmsSimulateCrave.Size = new System.Drawing.Size(1472, 196);
            this.tabPmsSimulateCrave.TabIndex = 3;
            this.tabPmsSimulateCrave.Text = "Simulate Crave";
            this.tabPmsSimulateCrave.UseVisualStyleBackColor = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(9, 54);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(137, 20);
            this.label13.TabIndex = 22;
            this.label13.Text = "Checkout balance";
            // 
            // btCraveToPmsGetGuestInformation
            // 
            this.btCraveToPmsGetGuestInformation.Location = new System.Drawing.Point(363, 95);
            this.btCraveToPmsGetGuestInformation.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btCraveToPmsGetGuestInformation.Name = "btCraveToPmsGetGuestInformation";
            this.btCraveToPmsGetGuestInformation.Size = new System.Drawing.Size(182, 35);
            this.btCraveToPmsGetGuestInformation.TabIndex = 21;
            this.btCraveToPmsGetGuestInformation.Text = "Get Guest Info";
            this.btCraveToPmsGetGuestInformation.UseVisualStyleBackColor = true;
            this.btCraveToPmsGetGuestInformation.Click += new System.EventHandler(this.btCraveToPmsGetGuestInformation_Click);
            // 
            // btCraveToPmsGetFolio
            // 
            this.btCraveToPmsGetFolio.Location = new System.Drawing.Point(363, 49);
            this.btCraveToPmsGetFolio.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btCraveToPmsGetFolio.Name = "btCraveToPmsGetFolio";
            this.btCraveToPmsGetFolio.Size = new System.Drawing.Size(182, 35);
            this.btCraveToPmsGetFolio.TabIndex = 20;
            this.btCraveToPmsGetFolio.Text = "Get Folio";
            this.btCraveToPmsGetFolio.UseVisualStyleBackColor = true;
            this.btCraveToPmsGetFolio.Click += new System.EventHandler(this.btCraveToPmsGetFolio_Click);
            // 
            // tbCraveToPmsRoom
            // 
            this.tbCraveToPmsRoom.Location = new System.Drawing.Point(159, 11);
            this.tbCraveToPmsRoom.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbCraveToPmsRoom.Name = "tbCraveToPmsRoom";
            this.tbCraveToPmsRoom.Size = new System.Drawing.Size(58, 26);
            this.tbCraveToPmsRoom.TabIndex = 19;
            this.tbCraveToPmsRoom.Text = "100";
            this.tbCraveToPmsRoom.ValidatingType = typeof(int);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 14);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 20);
            this.label9.TabIndex = 18;
            this.label9.Text = "Room #";
            // 
            // tbCraveToPmsCheckoutBalance
            // 
            this.tbCraveToPmsCheckoutBalance.Location = new System.Drawing.Point(159, 49);
            this.tbCraveToPmsCheckoutBalance.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbCraveToPmsCheckoutBalance.Name = "tbCraveToPmsCheckoutBalance";
            this.tbCraveToPmsCheckoutBalance.Size = new System.Drawing.Size(98, 26);
            this.tbCraveToPmsCheckoutBalance.TabIndex = 16;
            this.tbCraveToPmsCheckoutBalance.Text = "10.00";
            this.tbCraveToPmsCheckoutBalance.ValidatingType = typeof(int);
            // 
            // btCraveToPmsCheckout
            // 
            this.btCraveToPmsCheckout.Location = new System.Drawing.Point(363, 6);
            this.btCraveToPmsCheckout.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btCraveToPmsCheckout.Name = "btCraveToPmsCheckout";
            this.btCraveToPmsCheckout.Size = new System.Drawing.Size(182, 35);
            this.btCraveToPmsCheckout.TabIndex = 15;
            this.btCraveToPmsCheckout.Text = "Request Check-out";
            this.btCraveToPmsCheckout.UseVisualStyleBackColor = true;
            this.btCraveToPmsCheckout.Click += new System.EventHandler(this.btCraveToPmsCheckout_Click);
            // 
            // tbCraveToPmsAddToFolioPrice
            // 
            this.tbCraveToPmsAddToFolioPrice.Location = new System.Drawing.Point(711, 49);
            this.tbCraveToPmsAddToFolioPrice.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbCraveToPmsAddToFolioPrice.Name = "tbCraveToPmsAddToFolioPrice";
            this.tbCraveToPmsAddToFolioPrice.Size = new System.Drawing.Size(148, 26);
            this.tbCraveToPmsAddToFolioPrice.TabIndex = 14;
            this.tbCraveToPmsAddToFolioPrice.Text = "10.00";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(612, 14);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 20);
            this.label3.TabIndex = 11;
            this.label3.Text = "Description";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(612, 54);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 20);
            this.label4.TabIndex = 12;
            this.label4.Text = "Value";
            // 
            // tbCraveToPmsAddToFolioDescription
            // 
            this.tbCraveToPmsAddToFolioDescription.Location = new System.Drawing.Point(711, 9);
            this.tbCraveToPmsAddToFolioDescription.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbCraveToPmsAddToFolioDescription.Name = "tbCraveToPmsAddToFolioDescription";
            this.tbCraveToPmsAddToFolioDescription.Size = new System.Drawing.Size(338, 26);
            this.tbCraveToPmsAddToFolioDescription.TabIndex = 3;
            this.tbCraveToPmsAddToFolioDescription.Text = "Continental breakfast";
            // 
            // btCraveToPmsAddToFolio
            // 
            this.btCraveToPmsAddToFolio.Location = new System.Drawing.Point(870, 49);
            this.btCraveToPmsAddToFolio.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btCraveToPmsAddToFolio.Name = "btCraveToPmsAddToFolio";
            this.btCraveToPmsAddToFolio.Size = new System.Drawing.Size(182, 35);
            this.btCraveToPmsAddToFolio.TabIndex = 10;
            this.btCraveToPmsAddToFolio.Text = "Add to Folio";
            this.btCraveToPmsAddToFolio.UseVisualStyleBackColor = true;
            this.btCraveToPmsAddToFolio.Click += new System.EventHandler(this.btCraveToPmsAddToFolio_Click);
            // 
            // tabPmsSimulatePms
            // 
            this.tabPmsSimulatePms.Controls.Add(this.label36);
            this.tabPmsSimulatePms.Controls.Add(this.tbPmsGroup);
            this.tabPmsSimulatePms.Controls.Add(this.label46);
            this.tabPmsSimulatePms.Controls.Add(this.label14);
            this.tabPmsSimulatePms.Controls.Add(this.dpPmsCheckout);
            this.tabPmsSimulatePms.Controls.Add(this.dpPmsCheckin);
            this.tabPmsSimulatePms.Controls.Add(this.btnEditRoomData);
            this.tabPmsSimulatePms.Controls.Add(this.btnCreateOrder);
            this.tabPmsSimulatePms.Controls.Add(this.btnClearWakeUp);
            this.tabPmsSimulatePms.Controls.Add(this.btnSetWakeUp);
            this.tabPmsSimulatePms.Controls.Add(this.btnEditGuestInformation);
            this.tabPmsSimulatePms.Controls.Add(this.cbHasVoicemail);
            this.tabPmsSimulatePms.Controls.Add(this.cbPmsToCraveCheckedIn);
            this.tabPmsSimulatePms.Controls.Add(this.tbPmsToCraveLastname);
            this.tabPmsSimulatePms.Controls.Add(this.tbPmsToCraveFirstName);
            this.tabPmsSimulatePms.Controls.Add(this.cbPmsToCraveAllowExpressCheckout);
            this.tabPmsSimulatePms.Controls.Add(this.cbPmsToCraveAllowViewBill);
            this.tabPmsSimulatePms.Controls.Add(this.label2);
            this.tabPmsSimulatePms.Controls.Add(this.label1);
            this.tabPmsSimulatePms.Controls.Add(this.tbPmsToCraveOldRoomNumber);
            this.tabPmsSimulatePms.Controls.Add(this.label7);
            this.tabPmsSimulatePms.Controls.Add(this.btPmsToCraveRoomMove);
            this.tabPmsSimulatePms.Controls.Add(this.label6);
            this.tabPmsSimulatePms.Controls.Add(this.tbPmsToCraveRoomNumber);
            this.tabPmsSimulatePms.Controls.Add(this.btPmsToCraveCheckIn);
            this.tabPmsSimulatePms.Controls.Add(this.btPmsToCraveCheckout);
            this.tabPmsSimulatePms.Location = new System.Drawing.Point(4, 29);
            this.tabPmsSimulatePms.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPmsSimulatePms.Name = "tabPmsSimulatePms";
            this.tabPmsSimulatePms.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPmsSimulatePms.Size = new System.Drawing.Size(1472, 196);
            this.tabPmsSimulatePms.TabIndex = 2;
            this.tabPmsSimulatePms.Text = "Simulate PMS";
            this.tabPmsSimulatePms.UseVisualStyleBackColor = true;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(855, 142);
            this.label36.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(54, 20);
            this.label36.TabIndex = 45;
            this.label36.Text = "Group";
            // 
            // tbPmsGroup
            // 
            this.tbPmsGroup.Location = new System.Drawing.Point(918, 137);
            this.tbPmsGroup.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbPmsGroup.Name = "tbPmsGroup";
            this.tbPmsGroup.Size = new System.Drawing.Size(150, 26);
            this.tbPmsGroup.TabIndex = 44;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(446, 146);
            this.label46.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(77, 20);
            this.label46.TabIndex = 43;
            this.label46.Text = "Checkout";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(446, 106);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(66, 20);
            this.label14.TabIndex = 42;
            this.label14.Text = "Checkin";
            // 
            // dpPmsCheckout
            // 
            this.dpPmsCheckout.CustomFormat = "dd/MM/yyyy";
            this.dpPmsCheckout.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dpPmsCheckout.Location = new System.Drawing.Point(532, 137);
            this.dpPmsCheckout.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dpPmsCheckout.Name = "dpPmsCheckout";
            this.dpPmsCheckout.Size = new System.Drawing.Size(310, 26);
            this.dpPmsCheckout.TabIndex = 41;
            // 
            // dpPmsCheckin
            // 
            this.dpPmsCheckin.CustomFormat = "dd/MM/yyyy";
            this.dpPmsCheckin.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dpPmsCheckin.Location = new System.Drawing.Point(532, 97);
            this.dpPmsCheckin.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dpPmsCheckin.Name = "dpPmsCheckin";
            this.dpPmsCheckin.Size = new System.Drawing.Size(310, 26);
            this.dpPmsCheckin.TabIndex = 40;
            // 
            // btnEditRoomData
            // 
            this.btnEditRoomData.Location = new System.Drawing.Point(1269, 134);
            this.btnEditRoomData.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnEditRoomData.Name = "btnEditRoomData";
            this.btnEditRoomData.Size = new System.Drawing.Size(182, 35);
            this.btnEditRoomData.TabIndex = 38;
            this.btnEditRoomData.Text = "Edit Room Data";
            this.btnEditRoomData.UseVisualStyleBackColor = true;
            this.btnEditRoomData.Click += new System.EventHandler(this.btnEditRoomData_Click);
            // 
            // btnCreateOrder
            // 
            this.btnCreateOrder.Location = new System.Drawing.Point(1078, 58);
            this.btnCreateOrder.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnCreateOrder.Name = "btnCreateOrder";
            this.btnCreateOrder.Size = new System.Drawing.Size(182, 35);
            this.btnCreateOrder.TabIndex = 37;
            this.btnCreateOrder.Text = "Create Order\r\n";
            this.btnCreateOrder.UseVisualStyleBackColor = true;
            this.btnCreateOrder.Click += new System.EventHandler(this.btnCreateOrder_Click);
            // 
            // btnClearWakeUp
            // 
            this.btnClearWakeUp.Location = new System.Drawing.Point(1269, 58);
            this.btnClearWakeUp.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnClearWakeUp.Name = "btnClearWakeUp";
            this.btnClearWakeUp.Size = new System.Drawing.Size(182, 35);
            this.btnClearWakeUp.TabIndex = 36;
            this.btnClearWakeUp.Text = "Clear Wake-up";
            this.btnClearWakeUp.UseVisualStyleBackColor = true;
            this.btnClearWakeUp.Click += new System.EventHandler(this.btnClearWakeUp_Click);
            // 
            // btnSetWakeUp
            // 
            this.btnSetWakeUp.Location = new System.Drawing.Point(1269, 14);
            this.btnSetWakeUp.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnSetWakeUp.Name = "btnSetWakeUp";
            this.btnSetWakeUp.Size = new System.Drawing.Size(182, 35);
            this.btnSetWakeUp.TabIndex = 35;
            this.btnSetWakeUp.Text = "Set Wake-up";
            this.btnSetWakeUp.UseVisualStyleBackColor = true;
            this.btnSetWakeUp.Click += new System.EventHandler(this.btnSetWakeUp_Click);
            // 
            // btnEditGuestInformation
            // 
            this.btnEditGuestInformation.Location = new System.Drawing.Point(1078, 14);
            this.btnEditGuestInformation.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnEditGuestInformation.Name = "btnEditGuestInformation";
            this.btnEditGuestInformation.Size = new System.Drawing.Size(182, 35);
            this.btnEditGuestInformation.TabIndex = 34;
            this.btnEditGuestInformation.Text = "Edit Guest Info";
            this.btnEditGuestInformation.UseVisualStyleBackColor = true;
            this.btnEditGuestInformation.Click += new System.EventHandler(this.btnEditGuestInformation_Click);
            // 
            // cbHasVoicemail
            // 
            this.cbHasVoicemail.AutoSize = true;
            this.cbHasVoicemail.Location = new System.Drawing.Point(1086, 140);
            this.cbHasVoicemail.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbHasVoicemail.Name = "cbHasVoicemail";
            this.cbHasVoicemail.Size = new System.Drawing.Size(172, 24);
            this.cbHasVoicemail.TabIndex = 33;
            this.cbHasVoicemail.Text = "Voicemail message";
            this.cbHasVoicemail.UseVisualStyleBackColor = true;
            // 
            // cbPmsToCraveCheckedIn
            // 
            this.cbPmsToCraveCheckedIn.AutoSize = true;
            this.cbPmsToCraveCheckedIn.Location = new System.Drawing.Point(860, 23);
            this.cbPmsToCraveCheckedIn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbPmsToCraveCheckedIn.Name = "cbPmsToCraveCheckedIn";
            this.cbPmsToCraveCheckedIn.Size = new System.Drawing.Size(116, 24);
            this.cbPmsToCraveCheckedIn.TabIndex = 32;
            this.cbPmsToCraveCheckedIn.Text = "Checked In";
            this.cbPmsToCraveCheckedIn.UseVisualStyleBackColor = true;
            this.cbPmsToCraveCheckedIn.CheckedChanged += new System.EventHandler(this.cbPmsToCraveCheckedIn_CheckedChanged);
            // 
            // tbPmsToCraveLastname
            // 
            this.tbPmsToCraveLastname.Location = new System.Drawing.Point(532, 57);
            this.tbPmsToCraveLastname.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbPmsToCraveLastname.Name = "tbPmsToCraveLastname";
            this.tbPmsToCraveLastname.Size = new System.Drawing.Size(310, 26);
            this.tbPmsToCraveLastname.TabIndex = 30;
            this.tbPmsToCraveLastname.Text = "Norris";
            // 
            // tbPmsToCraveFirstName
            // 
            this.tbPmsToCraveFirstName.Location = new System.Drawing.Point(532, 17);
            this.tbPmsToCraveFirstName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbPmsToCraveFirstName.Name = "tbPmsToCraveFirstName";
            this.tbPmsToCraveFirstName.Size = new System.Drawing.Size(310, 26);
            this.tbPmsToCraveFirstName.TabIndex = 29;
            this.tbPmsToCraveFirstName.Text = "Chuck";
            // 
            // cbPmsToCraveAllowExpressCheckout
            // 
            this.cbPmsToCraveAllowExpressCheckout.AutoSize = true;
            this.cbPmsToCraveAllowExpressCheckout.Checked = true;
            this.cbPmsToCraveAllowExpressCheckout.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbPmsToCraveAllowExpressCheckout.Location = new System.Drawing.Point(860, 91);
            this.cbPmsToCraveAllowExpressCheckout.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbPmsToCraveAllowExpressCheckout.Name = "cbPmsToCraveAllowExpressCheckout";
            this.cbPmsToCraveAllowExpressCheckout.Size = new System.Drawing.Size(205, 24);
            this.cbPmsToCraveAllowExpressCheckout.TabIndex = 28;
            this.cbPmsToCraveAllowExpressCheckout.Text = "Allow Express Checkout";
            this.cbPmsToCraveAllowExpressCheckout.UseVisualStyleBackColor = true;
            // 
            // cbPmsToCraveAllowViewBill
            // 
            this.cbPmsToCraveAllowViewBill.AutoSize = true;
            this.cbPmsToCraveAllowViewBill.Checked = true;
            this.cbPmsToCraveAllowViewBill.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbPmsToCraveAllowViewBill.Location = new System.Drawing.Point(860, 57);
            this.cbPmsToCraveAllowViewBill.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbPmsToCraveAllowViewBill.Name = "cbPmsToCraveAllowViewBill";
            this.cbPmsToCraveAllowViewBill.Size = new System.Drawing.Size(134, 24);
            this.cbPmsToCraveAllowViewBill.TabIndex = 27;
            this.cbPmsToCraveAllowViewBill.Text = "Allow View Bill";
            this.cbPmsToCraveAllowViewBill.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(446, 62);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 20);
            this.label2.TabIndex = 19;
            this.label2.Text = "Lastname";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(446, 22);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 20);
            this.label1.TabIndex = 18;
            this.label1.Text = "Firstname";
            // 
            // tbPmsToCraveOldRoomNumber
            // 
            this.tbPmsToCraveOldRoomNumber.Location = new System.Drawing.Point(118, 54);
            this.tbPmsToCraveOldRoomNumber.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbPmsToCraveOldRoomNumber.Name = "tbPmsToCraveOldRoomNumber";
            this.tbPmsToCraveOldRoomNumber.Size = new System.Drawing.Size(80, 26);
            this.tbPmsToCraveOldRoomNumber.TabIndex = 17;
            this.tbPmsToCraveOldRoomNumber.Text = "15";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(15, 58);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(86, 20);
            this.label7.TabIndex = 16;
            this.label7.Text = "Old room #";
            // 
            // btPmsToCraveRoomMove
            // 
            this.btPmsToCraveRoomMove.Location = new System.Drawing.Point(242, 103);
            this.btPmsToCraveRoomMove.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btPmsToCraveRoomMove.Name = "btPmsToCraveRoomMove";
            this.btPmsToCraveRoomMove.Size = new System.Drawing.Size(182, 35);
            this.btPmsToCraveRoomMove.TabIndex = 15;
            this.btPmsToCraveRoomMove.Text = "Room Move";
            this.btPmsToCraveRoomMove.UseVisualStyleBackColor = true;
            this.btPmsToCraveRoomMove.Click += new System.EventHandler(this.btPmsToCraveRoomMove_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 22);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 20);
            this.label6.TabIndex = 12;
            this.label6.Text = "Room #";
            // 
            // tbPmsToCraveRoomNumber
            // 
            this.tbPmsToCraveRoomNumber.Location = new System.Drawing.Point(118, 14);
            this.tbPmsToCraveRoomNumber.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbPmsToCraveRoomNumber.Name = "tbPmsToCraveRoomNumber";
            this.tbPmsToCraveRoomNumber.Size = new System.Drawing.Size(80, 26);
            this.tbPmsToCraveRoomNumber.TabIndex = 11;
            this.tbPmsToCraveRoomNumber.Text = "1";
            // 
            // btPmsToCraveCheckIn
            // 
            this.btPmsToCraveCheckIn.Location = new System.Drawing.Point(242, 14);
            this.btPmsToCraveCheckIn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btPmsToCraveCheckIn.Name = "btPmsToCraveCheckIn";
            this.btPmsToCraveCheckIn.Size = new System.Drawing.Size(182, 35);
            this.btPmsToCraveCheckIn.TabIndex = 10;
            this.btPmsToCraveCheckIn.Text = "Check-in";
            this.btPmsToCraveCheckIn.UseVisualStyleBackColor = true;
            this.btPmsToCraveCheckIn.Click += new System.EventHandler(this.btPmsToCraveCheckIn_Click);
            // 
            // btPmsToCraveCheckout
            // 
            this.btPmsToCraveCheckout.Location = new System.Drawing.Point(242, 58);
            this.btPmsToCraveCheckout.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btPmsToCraveCheckout.Name = "btPmsToCraveCheckout";
            this.btPmsToCraveCheckout.Size = new System.Drawing.Size(182, 35);
            this.btPmsToCraveCheckout.TabIndex = 6;
            this.btPmsToCraveCheckout.Text = "Check-out";
            this.btPmsToCraveCheckout.UseVisualStyleBackColor = true;
            this.btPmsToCraveCheckout.Click += new System.EventHandler(this.btPmsToCraveCheckout_Click);
            // 
            // tabPmsComtrol
            // 
            this.tabPmsComtrol.Controls.Add(this.tbPmsComtrolPort);
            this.tabPmsComtrol.Controls.Add(this.lblComtrolHost);
            this.tabPmsComtrol.Controls.Add(this.tbPmsComtrolHost);
            this.tabPmsComtrol.Controls.Add(this.lblComtrolPort);
            this.tabPmsComtrol.Controls.Add(this.btComtrolInit);
            this.tabPmsComtrol.Location = new System.Drawing.Point(4, 29);
            this.tabPmsComtrol.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPmsComtrol.Name = "tabPmsComtrol";
            this.tabPmsComtrol.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPmsComtrol.Size = new System.Drawing.Size(1472, 196);
            this.tabPmsComtrol.TabIndex = 0;
            this.tabPmsComtrol.Text = "Comtrol";
            this.tabPmsComtrol.UseVisualStyleBackColor = true;
            // 
            // tbPmsComtrolPort
            // 
            this.tbPmsComtrolPort.Location = new System.Drawing.Point(84, 49);
            this.tbPmsComtrolPort.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbPmsComtrolPort.Name = "tbPmsComtrolPort";
            this.tbPmsComtrolPort.Size = new System.Drawing.Size(194, 26);
            this.tbPmsComtrolPort.TabIndex = 10;
            // 
            // lblComtrolHost
            // 
            this.lblComtrolHost.AutoSize = true;
            this.lblComtrolHost.Location = new System.Drawing.Point(9, 14);
            this.lblComtrolHost.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblComtrolHost.Name = "lblComtrolHost";
            this.lblComtrolHost.Size = new System.Drawing.Size(43, 20);
            this.lblComtrolHost.TabIndex = 0;
            this.lblComtrolHost.Text = "Host";
            // 
            // tbPmsComtrolHost
            // 
            this.tbPmsComtrolHost.Location = new System.Drawing.Point(84, 9);
            this.tbPmsComtrolHost.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbPmsComtrolHost.Name = "tbPmsComtrolHost";
            this.tbPmsComtrolHost.Size = new System.Drawing.Size(194, 26);
            this.tbPmsComtrolHost.TabIndex = 2;
            // 
            // lblComtrolPort
            // 
            this.lblComtrolPort.AutoSize = true;
            this.lblComtrolPort.Location = new System.Drawing.Point(9, 55);
            this.lblComtrolPort.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblComtrolPort.Name = "lblComtrolPort";
            this.lblComtrolPort.Size = new System.Drawing.Size(38, 20);
            this.lblComtrolPort.TabIndex = 1;
            this.lblComtrolPort.Text = "Port";
            // 
            // btComtrolInit
            // 
            this.btComtrolInit.Location = new System.Drawing.Point(290, 48);
            this.btComtrolInit.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btComtrolInit.Name = "btComtrolInit";
            this.btComtrolInit.Size = new System.Drawing.Size(112, 35);
            this.btComtrolInit.TabIndex = 4;
            this.btComtrolInit.Text = "Initialize";
            this.btComtrolInit.UseVisualStyleBackColor = true;
            this.btComtrolInit.Click += new System.EventHandler(this.btComtrolInit_Click);
            // 
            // tabPmsTiger
            // 
            this.tabPmsTiger.Controls.Add(this.cbPmsTigerEmulateExternalWebservice);
            this.tabPmsTiger.Controls.Add(this.label5);
            this.tabPmsTiger.Controls.Add(this.tbPmsTigerUserKey);
            this.tabPmsTiger.Controls.Add(this.cbPmsTigerEmulateMessageInService);
            this.tabPmsTiger.Controls.Add(this.label12);
            this.tabPmsTiger.Controls.Add(this.tbPmsTigerExternalWebserviceUrl);
            this.tabPmsTiger.Controls.Add(this.label11);
            this.tabPmsTiger.Controls.Add(this.label10);
            this.tabPmsTiger.Controls.Add(this.tbPmsTigerMessageInServiceUrl);
            this.tabPmsTiger.Controls.Add(this.label8);
            this.tabPmsTiger.Controls.Add(this.btPmsInitializeTigerTms);
            this.tabPmsTiger.Controls.Add(this.btPmsStopTigertms);
            this.tabPmsTiger.Location = new System.Drawing.Point(4, 29);
            this.tabPmsTiger.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPmsTiger.Name = "tabPmsTiger";
            this.tabPmsTiger.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPmsTiger.Size = new System.Drawing.Size(1472, 196);
            this.tabPmsTiger.TabIndex = 1;
            this.tabPmsTiger.Text = "Tiger";
            this.tabPmsTiger.UseVisualStyleBackColor = true;
            // 
            // cbPmsTigerEmulateExternalWebservice
            // 
            this.cbPmsTigerEmulateExternalWebservice.AutoSize = true;
            this.cbPmsTigerEmulateExternalWebservice.Checked = true;
            this.cbPmsTigerEmulateExternalWebservice.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbPmsTigerEmulateExternalWebservice.Location = new System.Drawing.Point(664, 17);
            this.cbPmsTigerEmulateExternalWebservice.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbPmsTigerEmulateExternalWebservice.Name = "cbPmsTigerEmulateExternalWebservice";
            this.cbPmsTigerEmulateExternalWebservice.Size = new System.Drawing.Size(238, 24);
            this.cbPmsTigerEmulateExternalWebservice.TabIndex = 26;
            this.cbPmsTigerEmulateExternalWebservice.Text = "Emulate ExternalWebservice";
            this.cbPmsTigerEmulateExternalWebservice.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 102);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 20);
            this.label5.TabIndex = 25;
            this.label5.Text = "User key";
            // 
            // tbPmsTigerUserKey
            // 
            this.tbPmsTigerUserKey.Location = new System.Drawing.Point(250, 97);
            this.tbPmsTigerUserKey.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbPmsTigerUserKey.Name = "tbPmsTigerUserKey";
            this.tbPmsTigerUserKey.Size = new System.Drawing.Size(394, 26);
            this.tbPmsTigerUserKey.TabIndex = 24;
            this.tbPmsTigerUserKey.Text = "USERKEY";
            // 
            // cbPmsTigerEmulateMessageInService
            // 
            this.cbPmsTigerEmulateMessageInService.AutoSize = true;
            this.cbPmsTigerEmulateMessageInService.Checked = true;
            this.cbPmsTigerEmulateMessageInService.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbPmsTigerEmulateMessageInService.Location = new System.Drawing.Point(664, 60);
            this.cbPmsTigerEmulateMessageInService.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbPmsTigerEmulateMessageInService.Name = "cbPmsTigerEmulateMessageInService";
            this.cbPmsTigerEmulateMessageInService.Size = new System.Drawing.Size(233, 24);
            this.cbPmsTigerEmulateMessageInService.TabIndex = 23;
            this.cbPmsTigerEmulateMessageInService.Text = "Emulate MessageIn Service";
            this.cbPmsTigerEmulateMessageInService.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(926, 18);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(246, 20);
            this.label12.TabIndex = 21;
            this.label12.Text = "(Url for TigerTMS to call Crave on)";
            // 
            // tbPmsTigerExternalWebserviceUrl
            // 
            this.tbPmsTigerExternalWebserviceUrl.Location = new System.Drawing.Point(250, 14);
            this.tbPmsTigerExternalWebserviceUrl.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbPmsTigerExternalWebserviceUrl.Name = "tbPmsTigerExternalWebserviceUrl";
            this.tbPmsTigerExternalWebserviceUrl.Size = new System.Drawing.Size(394, 26);
            this.tbPmsTigerExternalWebserviceUrl.TabIndex = 20;
            this.tbPmsTigerExternalWebserviceUrl.Text = "http://localhost:9654/externalWebservice";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(9, 18);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(191, 20);
            this.label11.TabIndex = 19;
            this.label11.Text = "Url of ExternalWebservice";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(926, 62);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(246, 20);
            this.label10.TabIndex = 18;
            this.label10.Text = "(Url for Crave to call TigerTMS on)";
            // 
            // tbPmsTigerMessageInServiceUrl
            // 
            this.tbPmsTigerMessageInServiceUrl.Location = new System.Drawing.Point(250, 57);
            this.tbPmsTigerMessageInServiceUrl.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbPmsTigerMessageInServiceUrl.Name = "tbPmsTigerMessageInServiceUrl";
            this.tbPmsTigerMessageInServiceUrl.Size = new System.Drawing.Size(394, 26);
            this.tbPmsTigerMessageInServiceUrl.TabIndex = 17;
            this.tbPmsTigerMessageInServiceUrl.Text = "http://localhost:9655/messageInService";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 62);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(186, 20);
            this.label8.TabIndex = 16;
            this.label8.Text = "Url of MessageIn Service";
            // 
            // btPmsInitializeTigerTms
            // 
            this.btPmsInitializeTigerTms.Location = new System.Drawing.Point(250, 131);
            this.btPmsInitializeTigerTms.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btPmsInitializeTigerTms.Name = "btPmsInitializeTigerTms";
            this.btPmsInitializeTigerTms.Size = new System.Drawing.Size(194, 35);
            this.btPmsInitializeTigerTms.TabIndex = 12;
            this.btPmsInitializeTigerTms.Text = "Initialize";
            this.btPmsInitializeTigerTms.UseVisualStyleBackColor = true;
            this.btPmsInitializeTigerTms.Click += new System.EventHandler(this.btPmsInitializeTigerTms_Click);
            // 
            // btPmsStopTigertms
            // 
            this.btPmsStopTigertms.Enabled = false;
            this.btPmsStopTigertms.Location = new System.Drawing.Point(453, 131);
            this.btPmsStopTigertms.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btPmsStopTigertms.Name = "btPmsStopTigertms";
            this.btPmsStopTigertms.Size = new System.Drawing.Size(194, 35);
            this.btPmsStopTigertms.TabIndex = 13;
            this.btPmsStopTigertms.Text = "Stop";
            this.btPmsStopTigertms.UseVisualStyleBackColor = true;
            this.btPmsStopTigertms.Click += new System.EventHandler(this.btPmsStopTigertms_Click);
            // 
            // tabPmsInnsist
            // 
            this.tabPmsInnsist.Controls.Add(this.btnInnsistStop);
            this.tabPmsInnsist.Controls.Add(this.btnInnsistInitialize);
            this.tabPmsInnsist.Controls.Add(this.label45);
            this.tabPmsInnsist.Controls.Add(this.label44);
            this.tabPmsInnsist.Controls.Add(this.label43);
            this.tabPmsInnsist.Controls.Add(this.tbInnsistWakeUpCode);
            this.tabPmsInnsist.Controls.Add(this.label42);
            this.tabPmsInnsist.Controls.Add(this.tbInnsistToId);
            this.tabPmsInnsist.Controls.Add(this.tbInnsistFromId);
            this.tabPmsInnsist.Controls.Add(this.label41);
            this.tabPmsInnsist.Controls.Add(this.tbInnsistPassword);
            this.tabPmsInnsist.Controls.Add(this.tbInnsistUsername);
            this.tabPmsInnsist.Controls.Add(this.label40);
            this.tabPmsInnsist.Controls.Add(this.label39);
            this.tabPmsInnsist.Controls.Add(this.tbInnsistOutgoingUrl);
            this.tabPmsInnsist.Controls.Add(this.tbInnsistIncommingUrl);
            this.tabPmsInnsist.Controls.Add(this.label38);
            this.tabPmsInnsist.Controls.Add(this.label37);
            this.tabPmsInnsist.Location = new System.Drawing.Point(4, 29);
            this.tabPmsInnsist.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPmsInnsist.Name = "tabPmsInnsist";
            this.tabPmsInnsist.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPmsInnsist.Size = new System.Drawing.Size(1472, 196);
            this.tabPmsInnsist.TabIndex = 4;
            this.tabPmsInnsist.Text = "Innsist";
            this.tabPmsInnsist.UseVisualStyleBackColor = true;
            // 
            // btnInnsistStop
            // 
            this.btnInnsistStop.Enabled = false;
            this.btnInnsistStop.Location = new System.Drawing.Point(870, 118);
            this.btnInnsistStop.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnInnsistStop.Name = "btnInnsistStop";
            this.btnInnsistStop.Size = new System.Drawing.Size(112, 35);
            this.btnInnsistStop.TabIndex = 21;
            this.btnInnsistStop.Text = "Stop";
            this.btnInnsistStop.UseVisualStyleBackColor = true;
            this.btnInnsistStop.Click += new System.EventHandler(this.btnInnsistStop_Click);
            // 
            // btnInnsistInitialize
            // 
            this.btnInnsistInitialize.Location = new System.Drawing.Point(664, 118);
            this.btnInnsistInitialize.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnInnsistInitialize.Name = "btnInnsistInitialize";
            this.btnInnsistInitialize.Size = new System.Drawing.Size(196, 35);
            this.btnInnsistInitialize.TabIndex = 20;
            this.btnInnsistInitialize.Text = "Initialize PMS";
            this.btnInnsistInitialize.UseVisualStyleBackColor = true;
            this.btnInnsistInitialize.Click += new System.EventHandler(this.btnInnsistInitialize_Click);
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(1030, 55);
            this.label45.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(116, 20);
            this.label45.TabIndex = 19;
            this.label45.Text = "(PMS -> Crave)";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(1030, 15);
            this.label44.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(116, 20);
            this.label44.TabIndex = 18;
            this.label44.Text = "(Crave -> PMS)";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(260, 95);
            this.label43.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(113, 20);
            this.label43.TabIndex = 17;
            this.label43.Text = "WakeUp Code";
            // 
            // tbInnsistWakeUpCode
            // 
            this.tbInnsistWakeUpCode.Location = new System.Drawing.Point(386, 91);
            this.tbInnsistWakeUpCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbInnsistWakeUpCode.Name = "tbInnsistWakeUpCode";
            this.tbInnsistWakeUpCode.Size = new System.Drawing.Size(148, 26);
            this.tbInnsistWakeUpCode.TabIndex = 16;
            this.tbInnsistWakeUpCode.Text = "C1";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(260, 55);
            this.label42.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(48, 20);
            this.label42.TabIndex = 15;
            this.label42.Text = "To ID";
            // 
            // tbInnsistToId
            // 
            this.tbInnsistToId.Location = new System.Drawing.Point(386, 51);
            this.tbInnsistToId.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbInnsistToId.Name = "tbInnsistToId";
            this.tbInnsistToId.Size = new System.Drawing.Size(148, 26);
            this.tbInnsistToId.TabIndex = 14;
            this.tbInnsistToId.Text = "001";
            // 
            // tbInnsistFromId
            // 
            this.tbInnsistFromId.Location = new System.Drawing.Point(386, 11);
            this.tbInnsistFromId.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbInnsistFromId.Name = "tbInnsistFromId";
            this.tbInnsistFromId.Size = new System.Drawing.Size(148, 26);
            this.tbInnsistFromId.TabIndex = 13;
            this.tbInnsistFromId.Text = "ACME";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(260, 15);
            this.label41.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(67, 20);
            this.label41.TabIndex = 12;
            this.label41.Text = "From ID";
            // 
            // tbInnsistPassword
            // 
            this.tbInnsistPassword.Location = new System.Drawing.Point(100, 51);
            this.tbInnsistPassword.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbInnsistPassword.Name = "tbInnsistPassword";
            this.tbInnsistPassword.Size = new System.Drawing.Size(148, 26);
            this.tbInnsistPassword.TabIndex = 11;
            this.tbInnsistPassword.Text = "WEEkpNJPhAwK";
            // 
            // tbInnsistUsername
            // 
            this.tbInnsistUsername.Location = new System.Drawing.Point(100, 11);
            this.tbInnsistUsername.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbInnsistUsername.Name = "tbInnsistUsername";
            this.tbInnsistUsername.Size = new System.Drawing.Size(148, 26);
            this.tbInnsistUsername.TabIndex = 10;
            this.tbInnsistUsername.Text = "SandBox";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(12, 55);
            this.label40.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(78, 20);
            this.label40.TabIndex = 5;
            this.label40.Text = "Password";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(9, 15);
            this.label39.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(83, 20);
            this.label39.TabIndex = 4;
            this.label39.Text = "Username";
            // 
            // tbInnsistOutgoingUrl
            // 
            this.tbInnsistOutgoingUrl.Location = new System.Drawing.Point(794, 11);
            this.tbInnsistOutgoingUrl.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbInnsistOutgoingUrl.Name = "tbInnsistOutgoingUrl";
            this.tbInnsistOutgoingUrl.Size = new System.Drawing.Size(226, 26);
            this.tbInnsistOutgoingUrl.TabIndex = 3;
            this.tbInnsistOutgoingUrl.Text = "http://216.87.172.219:8587/";
            // 
            // tbInnsistIncommingUrl
            // 
            this.tbInnsistIncommingUrl.Location = new System.Drawing.Point(794, 51);
            this.tbInnsistIncommingUrl.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbInnsistIncommingUrl.Name = "tbInnsistIncommingUrl";
            this.tbInnsistIncommingUrl.Size = new System.Drawing.Size(226, 26);
            this.tbInnsistIncommingUrl.TabIndex = 2;
            this.tbInnsistIncommingUrl.Text = "http://localhost:8586/";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(660, 55);
            this.label38.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(124, 20);
            this.label38.TabIndex = 1;
            this.label38.Text = "Incomming URL";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(660, 15);
            this.label37.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(111, 20);
            this.label37.TabIndex = 0;
            this.label37.Text = "Outgoing URL";
            // 
            // tbPmsLog
            // 
            this.tbPmsLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbPmsLog.Location = new System.Drawing.Point(0, 0);
            this.tbPmsLog.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbPmsLog.Multiline = true;
            this.tbPmsLog.Name = "tbPmsLog";
            this.tbPmsLog.ReadOnly = true;
            this.tbPmsLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbPmsLog.Size = new System.Drawing.Size(1486, 773);
            this.tbPmsLog.TabIndex = 9;
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStripConnectionState});
            this.statusStrip1.Location = new System.Drawing.Point(4, 1015);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(2, 0, 21, 0);
            this.statusStrip1.Size = new System.Drawing.Size(1486, 32);
            this.statusStrip1.TabIndex = 8;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lblStripConnectionState
            // 
            this.lblStripConnectionState.Name = "lblStripConnectionState";
            this.lblStripConnectionState.Size = new System.Drawing.Size(129, 25);
            this.lblStripConnectionState.Text = "Not connected";
            // 
            // tabMitel
            // 
            this.tabMitel.Controls.Add(this.groupBox4);
            this.tabMitel.Controls.Add(this.groupBox3);
            this.tabMitel.Controls.Add(this.groupBox2);
            this.tabMitel.Controls.Add(this.groupBox1);
            this.tabMitel.Location = new System.Drawing.Point(4, 29);
            this.tabMitel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabMitel.Name = "tabMitel";
            this.tabMitel.Size = new System.Drawing.Size(1494, 1052);
            this.tabMitel.TabIndex = 2;
            this.tabMitel.Text = "Mitel";
            this.tabMitel.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.tbMitelLog);
            this.groupBox4.Location = new System.Drawing.Point(12, 415);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox4.Size = new System.Drawing.Size(1424, 557);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Log";
            // 
            // tbMitelLog
            // 
            this.tbMitelLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbMitelLog.Location = new System.Drawing.Point(4, 24);
            this.tbMitelLog.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbMitelLog.Multiline = true;
            this.tbMitelLog.Name = "tbMitelLog";
            this.tbMitelLog.ReadOnly = true;
            this.tbMitelLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbMitelLog.Size = new System.Drawing.Size(1416, 528);
            this.tbMitelLog.TabIndex = 10;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btMitelSend);
            this.groupBox3.Controls.Add(this.tbMitelSendPriority);
            this.groupBox3.Controls.Add(this.label26);
            this.groupBox3.Controls.Add(this.tbMitelSendBody);
            this.groupBox3.Controls.Add(this.label22);
            this.groupBox3.Controls.Add(this.label24);
            this.groupBox3.Controls.Add(this.tbMitelSendSubject);
            this.groupBox3.Controls.Add(this.tbMitelSendTo);
            this.groupBox3.Controls.Add(this.label25);
            this.groupBox3.Location = new System.Drawing.Point(12, 174);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox3.Size = new System.Drawing.Size(711, 232);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Client Requests";
            // 
            // btMitelSend
            // 
            this.btMitelSend.Location = new System.Drawing.Point(172, 185);
            this.btMitelSend.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btMitelSend.Name = "btMitelSend";
            this.btMitelSend.Size = new System.Drawing.Size(234, 35);
            this.btMitelSend.TabIndex = 9;
            this.btMitelSend.Text = "Send Message";
            this.btMitelSend.UseVisualStyleBackColor = true;
            this.btMitelSend.Click += new System.EventHandler(this.btMitelSend_Click);
            // 
            // tbMitelSendPriority
            // 
            this.tbMitelSendPriority.Location = new System.Drawing.Point(172, 145);
            this.tbMitelSendPriority.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbMitelSendPriority.Name = "tbMitelSendPriority";
            this.tbMitelSendPriority.Size = new System.Drawing.Size(110, 26);
            this.tbMitelSendPriority.TabIndex = 8;
            this.tbMitelSendPriority.Text = "2";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(9, 149);
            this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(101, 20);
            this.label26.TabIndex = 7;
            this.label26.Text = "Priority (1 - 9)";
            // 
            // tbMitelSendBody
            // 
            this.tbMitelSendBody.Location = new System.Drawing.Point(172, 106);
            this.tbMitelSendBody.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbMitelSendBody.Name = "tbMitelSendBody";
            this.tbMitelSendBody.Size = new System.Drawing.Size(528, 26);
            this.tbMitelSendBody.TabIndex = 6;
            this.tbMitelSendBody.Text = "Test Message Body";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(9, 111);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(74, 20);
            this.label22.TabIndex = 5;
            this.label22.Text = "Message";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(9, 74);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(63, 20);
            this.label24.TabIndex = 4;
            this.label24.Text = "Subject";
            // 
            // tbMitelSendSubject
            // 
            this.tbMitelSendSubject.Location = new System.Drawing.Point(172, 69);
            this.tbMitelSendSubject.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbMitelSendSubject.Name = "tbMitelSendSubject";
            this.tbMitelSendSubject.Size = new System.Drawing.Size(528, 26);
            this.tbMitelSendSubject.TabIndex = 3;
            this.tbMitelSendSubject.Text = "Test Message Title";
            // 
            // tbMitelSendTo
            // 
            this.tbMitelSendTo.Location = new System.Drawing.Point(172, 29);
            this.tbMitelSendTo.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbMitelSendTo.Name = "tbMitelSendTo";
            this.tbMitelSendTo.Size = new System.Drawing.Size(232, 26);
            this.tbMitelSendTo.TabIndex = 1;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(9, 34);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(120, 20);
            this.label25.TabIndex = 0;
            this.label25.Text = "Portable Device";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tbMitelClientListenerIp);
            this.groupBox2.Controls.Add(this.btMitelClientDisconnect);
            this.groupBox2.Controls.Add(this.btMitelClientConnect);
            this.groupBox2.Controls.Add(this.tbMitelClientListenerPort);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.tbMitelClientConnectToPort);
            this.groupBox2.Controls.Add(this.tbMitelClientConnectToIp);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Location = new System.Drawing.Point(12, 5);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox2.Size = new System.Drawing.Size(711, 160);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Client Configuration ( = Crave OSS)";
            // 
            // tbMitelClientListenerIp
            // 
            this.tbMitelClientListenerIp.Location = new System.Drawing.Point(172, 69);
            this.tbMitelClientListenerIp.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbMitelClientListenerIp.Name = "tbMitelClientListenerIp";
            this.tbMitelClientListenerIp.Size = new System.Drawing.Size(232, 26);
            this.tbMitelClientListenerIp.TabIndex = 9;
            this.tbMitelClientListenerIp.Text = "0.0.0.0";
            // 
            // btMitelClientDisconnect
            // 
            this.btMitelClientDisconnect.Enabled = false;
            this.btMitelClientDisconnect.Location = new System.Drawing.Point(294, 109);
            this.btMitelClientDisconnect.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btMitelClientDisconnect.Name = "btMitelClientDisconnect";
            this.btMitelClientDisconnect.Size = new System.Drawing.Size(112, 35);
            this.btMitelClientDisconnect.TabIndex = 8;
            this.btMitelClientDisconnect.Text = "Disconnect";
            this.btMitelClientDisconnect.UseVisualStyleBackColor = true;
            this.btMitelClientDisconnect.Click += new System.EventHandler(this.btMitelClientDisconnect_Click);
            // 
            // btMitelClientConnect
            // 
            this.btMitelClientConnect.Location = new System.Drawing.Point(172, 109);
            this.btMitelClientConnect.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btMitelClientConnect.Name = "btMitelClientConnect";
            this.btMitelClientConnect.Size = new System.Drawing.Size(112, 35);
            this.btMitelClientConnect.TabIndex = 7;
            this.btMitelClientConnect.Text = "Connect";
            this.btMitelClientConnect.UseVisualStyleBackColor = true;
            this.btMitelClientConnect.Click += new System.EventHandler(this.btMitelClientConnect_Click);
            // 
            // tbMitelClientListenerPort
            // 
            this.tbMitelClientListenerPort.Location = new System.Drawing.Point(416, 69);
            this.tbMitelClientListenerPort.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbMitelClientListenerPort.Name = "tbMitelClientListenerPort";
            this.tbMitelClientListenerPort.Size = new System.Drawing.Size(110, 26);
            this.tbMitelClientListenerPort.TabIndex = 6;
            this.tbMitelClientListenerPort.Text = "7667";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(9, 74);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(152, 20);
            this.label20.TabIndex = 5;
            this.label20.Text = "IP / Port to Listen on";
            // 
            // tbMitelClientConnectToPort
            // 
            this.tbMitelClientConnectToPort.Location = new System.Drawing.Point(416, 29);
            this.tbMitelClientConnectToPort.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbMitelClientConnectToPort.Name = "tbMitelClientConnectToPort";
            this.tbMitelClientConnectToPort.Size = new System.Drawing.Size(110, 26);
            this.tbMitelClientConnectToPort.TabIndex = 3;
            this.tbMitelClientConnectToPort.Text = "1321";
            // 
            // tbMitelClientConnectToIp
            // 
            this.tbMitelClientConnectToIp.Location = new System.Drawing.Point(172, 29);
            this.tbMitelClientConnectToIp.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbMitelClientConnectToIp.Name = "tbMitelClientConnectToIp";
            this.tbMitelClientConnectToIp.Size = new System.Drawing.Size(232, 26);
            this.tbMitelClientConnectToIp.TabIndex = 1;
            this.tbMitelClientConnectToIp.Text = "0.0.0.0";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(9, 34);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(164, 20);
            this.label19.TabIndex = 0;
            this.label19.Text = "IP / port to Connect to";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Controls.Add(this.tbMitelServerListenerIp);
            this.groupBox1.Controls.Add(this.btMitelServerStop);
            this.groupBox1.Controls.Add(this.btMitelServerStart);
            this.groupBox1.Controls.Add(this.tbMitelServerListenerPort);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.Location = new System.Drawing.Point(732, 5);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Size = new System.Drawing.Size(711, 160);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Server Configuration ( = Mitel AOP Server)";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(9, 38);
            this.label27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(111, 20);
            this.label27.TabIndex = 11;
            this.label27.Text = "IP to Listen on";
            // 
            // tbMitelServerListenerIp
            // 
            this.tbMitelServerListenerIp.Location = new System.Drawing.Point(172, 34);
            this.tbMitelServerListenerIp.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbMitelServerListenerIp.Name = "tbMitelServerListenerIp";
            this.tbMitelServerListenerIp.Size = new System.Drawing.Size(232, 26);
            this.tbMitelServerListenerIp.TabIndex = 10;
            this.tbMitelServerListenerIp.Text = "0.0.0.0";
            // 
            // btMitelServerStop
            // 
            this.btMitelServerStop.Location = new System.Drawing.Point(294, 109);
            this.btMitelServerStop.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btMitelServerStop.Name = "btMitelServerStop";
            this.btMitelServerStop.Size = new System.Drawing.Size(112, 35);
            this.btMitelServerStop.TabIndex = 9;
            this.btMitelServerStop.Text = "Stop";
            this.btMitelServerStop.UseVisualStyleBackColor = true;
            this.btMitelServerStop.Click += new System.EventHandler(this.btMitelServerStop_Click);
            // 
            // btMitelServerStart
            // 
            this.btMitelServerStart.Location = new System.Drawing.Point(172, 109);
            this.btMitelServerStart.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btMitelServerStart.Name = "btMitelServerStart";
            this.btMitelServerStart.Size = new System.Drawing.Size(112, 35);
            this.btMitelServerStart.TabIndex = 8;
            this.btMitelServerStart.Text = "Start";
            this.btMitelServerStart.UseVisualStyleBackColor = true;
            this.btMitelServerStart.Click += new System.EventHandler(this.btMitelServerStart_Click);
            // 
            // tbMitelServerListenerPort
            // 
            this.tbMitelServerListenerPort.Location = new System.Drawing.Point(172, 69);
            this.tbMitelServerListenerPort.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbMitelServerListenerPort.Name = "tbMitelServerListenerPort";
            this.tbMitelServerListenerPort.Size = new System.Drawing.Size(232, 26);
            this.tbMitelServerListenerPort.TabIndex = 6;
            this.tbMitelServerListenerPort.Text = "1321";
            this.tbMitelServerListenerPort.TextChanged += new System.EventHandler(this.tbMitelServerListenerPort_TextChanged);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(9, 74);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(125, 20);
            this.label23.TabIndex = 5;
            this.label23.Text = "Port to Listen on";
            // 
            // tabStresser
            // 
            this.tabStresser.Controls.Add(this.cbAsync);
            this.tabStresser.Controls.Add(this.label29);
            this.tabStresser.Controls.Add(this.tbStresserWebserviceUrl);
            this.tabStresser.Controls.Add(this.label28);
            this.tabStresser.Controls.Add(this.tbStresserSeconds);
            this.tabStresser.Controls.Add(this.btStresserStart);
            this.tabStresser.Controls.Add(this.cbStressGetCompany);
            this.tabStresser.Controls.Add(this.cbGetEntertainment);
            this.tabStresser.Controls.Add(this.cbStressGetDeliverypoints);
            this.tabStresser.Controls.Add(this.cbStressUpdateLastRequest);
            this.tabStresser.Controls.Add(this.cbStressGetMenu);
            this.tabStresser.Controls.Add(this.cbStressGetOrders);
            this.tabStresser.Controls.Add(this.cbStressGetReleaseAndDownloadLocation);
            this.tabStresser.Controls.Add(this.cbStressGetSetClientStatus);
            this.tabStresser.Location = new System.Drawing.Point(4, 29);
            this.tabStresser.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabStresser.Name = "tabStresser";
            this.tabStresser.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabStresser.Size = new System.Drawing.Size(1494, 1052);
            this.tabStresser.TabIndex = 3;
            this.tabStresser.Text = "Stresser";
            this.tabStresser.UseVisualStyleBackColor = true;
            // 
            // cbAsync
            // 
            this.cbAsync.AutoSize = true;
            this.cbAsync.Checked = true;
            this.cbAsync.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbAsync.Location = new System.Drawing.Point(68, 20);
            this.cbAsync.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbAsync.Name = "cbAsync";
            this.cbAsync.Size = new System.Drawing.Size(78, 24);
            this.cbAsync.TabIndex = 13;
            this.cbAsync.Text = "Async";
            this.cbAsync.UseVisualStyleBackColor = true;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(429, 62);
            this.label29.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(128, 20);
            this.label29.TabIndex = 12;
            this.label29.Text = "Webservice URL";
            // 
            // tbStresserWebserviceUrl
            // 
            this.tbStresserWebserviceUrl.Location = new System.Drawing.Point(572, 57);
            this.tbStresserWebserviceUrl.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbStresserWebserviceUrl.Name = "tbStresserWebserviceUrl";
            this.tbStresserWebserviceUrl.Size = new System.Drawing.Size(544, 26);
            this.tbStresserWebserviceUrl.TabIndex = 11;
            this.tbStresserWebserviceUrl.Text = "http://192.168.57.30/api/6.0/CraveService.asmx";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(26, 343);
            this.label28.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(48, 20);
            this.label28.TabIndex = 10;
            this.label28.Text = "Tests";
            // 
            // tbStresserSeconds
            // 
            this.tbStresserSeconds.Location = new System.Drawing.Point(108, 338);
            this.tbStresserSeconds.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbStresserSeconds.Name = "tbStresserSeconds";
            this.tbStresserSeconds.Size = new System.Drawing.Size(148, 26);
            this.tbStresserSeconds.TabIndex = 9;
            this.tbStresserSeconds.Text = "10000000";
            // 
            // btStresserStart
            // 
            this.btStresserStart.Location = new System.Drawing.Point(68, 386);
            this.btStresserStart.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btStresserStart.Name = "btStresserStart";
            this.btStresserStart.Size = new System.Drawing.Size(112, 35);
            this.btStresserStart.TabIndex = 8;
            this.btStresserStart.Text = "Start";
            this.btStresserStart.UseVisualStyleBackColor = true;
            this.btStresserStart.Click += new System.EventHandler(this.btStresserStart_Click);
            // 
            // cbStressGetCompany
            // 
            this.cbStressGetCompany.AutoSize = true;
            this.cbStressGetCompany.Checked = true;
            this.cbStressGetCompany.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbStressGetCompany.Location = new System.Drawing.Point(68, 303);
            this.cbStressGetCompany.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbStressGetCompany.Name = "cbStressGetCompany";
            this.cbStressGetCompany.Size = new System.Drawing.Size(129, 24);
            this.cbStressGetCompany.TabIndex = 7;
            this.cbStressGetCompany.Text = "GetCompany";
            this.cbStressGetCompany.UseVisualStyleBackColor = true;
            // 
            // cbGetEntertainment
            // 
            this.cbGetEntertainment.AutoSize = true;
            this.cbGetEntertainment.Checked = true;
            this.cbGetEntertainment.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbGetEntertainment.Location = new System.Drawing.Point(68, 268);
            this.cbGetEntertainment.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbGetEntertainment.Name = "cbGetEntertainment";
            this.cbGetEntertainment.Size = new System.Drawing.Size(163, 24);
            this.cbGetEntertainment.TabIndex = 6;
            this.cbGetEntertainment.Text = "GetEntertainment";
            this.cbGetEntertainment.UseVisualStyleBackColor = true;
            // 
            // cbStressGetDeliverypoints
            // 
            this.cbStressGetDeliverypoints.AutoSize = true;
            this.cbStressGetDeliverypoints.Checked = true;
            this.cbStressGetDeliverypoints.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbStressGetDeliverypoints.Location = new System.Drawing.Point(68, 232);
            this.cbStressGetDeliverypoints.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbStressGetDeliverypoints.Name = "cbStressGetDeliverypoints";
            this.cbStressGetDeliverypoints.Size = new System.Drawing.Size(160, 24);
            this.cbStressGetDeliverypoints.TabIndex = 5;
            this.cbStressGetDeliverypoints.Text = "GetDeliverypoints";
            this.cbStressGetDeliverypoints.UseVisualStyleBackColor = true;
            // 
            // cbStressUpdateLastRequest
            // 
            this.cbStressUpdateLastRequest.AutoSize = true;
            this.cbStressUpdateLastRequest.Checked = true;
            this.cbStressUpdateLastRequest.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbStressUpdateLastRequest.Location = new System.Drawing.Point(68, 197);
            this.cbStressUpdateLastRequest.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbStressUpdateLastRequest.Name = "cbStressUpdateLastRequest";
            this.cbStressUpdateLastRequest.Size = new System.Drawing.Size(180, 24);
            this.cbStressUpdateLastRequest.TabIndex = 4;
            this.cbStressUpdateLastRequest.Text = "UpdateLastRequest";
            this.cbStressUpdateLastRequest.UseVisualStyleBackColor = true;
            // 
            // cbStressGetMenu
            // 
            this.cbStressGetMenu.AutoSize = true;
            this.cbStressGetMenu.Checked = true;
            this.cbStressGetMenu.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbStressGetMenu.Location = new System.Drawing.Point(68, 162);
            this.cbStressGetMenu.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbStressGetMenu.Name = "cbStressGetMenu";
            this.cbStressGetMenu.Size = new System.Drawing.Size(102, 24);
            this.cbStressGetMenu.TabIndex = 3;
            this.cbStressGetMenu.Text = "GetMenu";
            this.cbStressGetMenu.UseVisualStyleBackColor = true;
            // 
            // cbStressGetOrders
            // 
            this.cbStressGetOrders.AutoSize = true;
            this.cbStressGetOrders.Checked = true;
            this.cbStressGetOrders.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbStressGetOrders.Location = new System.Drawing.Point(68, 126);
            this.cbStressGetOrders.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbStressGetOrders.Name = "cbStressGetOrders";
            this.cbStressGetOrders.Size = new System.Drawing.Size(110, 24);
            this.cbStressGetOrders.TabIndex = 2;
            this.cbStressGetOrders.Text = "GetOrders";
            this.cbStressGetOrders.UseVisualStyleBackColor = true;
            // 
            // cbStressGetReleaseAndDownloadLocation
            // 
            this.cbStressGetReleaseAndDownloadLocation.AutoSize = true;
            this.cbStressGetReleaseAndDownloadLocation.Checked = true;
            this.cbStressGetReleaseAndDownloadLocation.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbStressGetReleaseAndDownloadLocation.Location = new System.Drawing.Point(68, 91);
            this.cbStressGetReleaseAndDownloadLocation.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbStressGetReleaseAndDownloadLocation.Name = "cbStressGetReleaseAndDownloadLocation";
            this.cbStressGetReleaseAndDownloadLocation.Size = new System.Drawing.Size(282, 24);
            this.cbStressGetReleaseAndDownloadLocation.TabIndex = 1;
            this.cbStressGetReleaseAndDownloadLocation.Text = "GetReleaseAndDownloadLocation";
            this.cbStressGetReleaseAndDownloadLocation.UseVisualStyleBackColor = true;
            // 
            // cbStressGetSetClientStatus
            // 
            this.cbStressGetSetClientStatus.AutoSize = true;
            this.cbStressGetSetClientStatus.Checked = true;
            this.cbStressGetSetClientStatus.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbStressGetSetClientStatus.Location = new System.Drawing.Point(68, 55);
            this.cbStressGetSetClientStatus.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbStressGetSetClientStatus.Name = "cbStressGetSetClientStatus";
            this.cbStressGetSetClientStatus.Size = new System.Drawing.Size(174, 24);
            this.cbStressGetSetClientStatus.TabIndex = 0;
            this.cbStressGetSetClientStatus.Text = "GetSetClientStatus";
            this.cbStressGetSetClientStatus.UseVisualStyleBackColor = true;
            // 
            // tabComet
            // 
            this.tabComet.Controls.Add(this.ucComet2);
            this.tabComet.Location = new System.Drawing.Point(4, 29);
            this.tabComet.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabComet.Name = "tabComet";
            this.tabComet.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabComet.Size = new System.Drawing.Size(1494, 1052);
            this.tabComet.TabIndex = 4;
            this.tabComet.Text = "Comet (WIP)";
            this.tabComet.UseVisualStyleBackColor = true;
            // 
            // ucComet2
            // 
            this.ucComet2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucComet2.Location = new System.Drawing.Point(4, 5);
            this.ucComet2.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.ucComet2.MinimumSize = new System.Drawing.Size(966, 858);
            this.ucComet2.Name = "ucComet2";
            this.ucComet2.Size = new System.Drawing.Size(1486, 1042);
            this.ucComet2.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.tbRandomOutput);
            this.tabPage1.Controls.Add(this.groupBox5);
            this.tabPage1.Controls.Add(this.btSerialisationTest);
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.btnGetLastState);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPage1.Size = new System.Drawing.Size(1494, 1052);
            this.tabPage1.TabIndex = 5;
            this.tabPage1.Text = "Random Stuff";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tbRandomOutput
            // 
            this.tbRandomOutput.Location = new System.Drawing.Point(12, 643);
            this.tbRandomOutput.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbRandomOutput.Multiline = true;
            this.tbRandomOutput.Name = "tbRandomOutput";
            this.tbRandomOutput.ReadOnly = true;
            this.tbRandomOutput.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbRandomOutput.Size = new System.Drawing.Size(1466, 387);
            this.tbRandomOutput.TabIndex = 19;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.btnHashTimestampRefresh);
            this.groupBox5.Controls.Add(this.btnSaveClientLog);
            this.groupBox5.Controls.Add(this.label35);
            this.groupBox5.Controls.Add(this.tbHashParam5);
            this.groupBox5.Controls.Add(this.label34);
            this.groupBox5.Controls.Add(this.tbHashParam2);
            this.groupBox5.Controls.Add(this.label33);
            this.groupBox5.Controls.Add(this.tbHashParam4);
            this.groupBox5.Controls.Add(this.label32);
            this.groupBox5.Controls.Add(this.tbHashParam3);
            this.groupBox5.Controls.Add(this.tbHashOutput);
            this.groupBox5.Controls.Add(this.btnGenerateHash);
            this.groupBox5.Controls.Add(this.label31);
            this.groupBox5.Controls.Add(this.label30);
            this.groupBox5.Controls.Add(this.label21);
            this.groupBox5.Controls.Add(this.tbHashSalt);
            this.groupBox5.Controls.Add(this.tbHashMacAddress);
            this.groupBox5.Controls.Add(this.tbHashParam1);
            this.groupBox5.Controls.Add(this.tbHashTimestamp);
            this.groupBox5.Controls.Add(this.label18);
            this.groupBox5.Location = new System.Drawing.Point(12, 169);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox5.Size = new System.Drawing.Size(700, 465);
            this.groupBox5.TabIndex = 3;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Generate Hash";
            // 
            // btnHashTimestampRefresh
            // 
            this.btnHashTimestampRefresh.Location = new System.Drawing.Point(561, 26);
            this.btnHashTimestampRefresh.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnHashTimestampRefresh.Name = "btnHashTimestampRefresh";
            this.btnHashTimestampRefresh.Size = new System.Drawing.Size(112, 35);
            this.btnHashTimestampRefresh.TabIndex = 19;
            this.btnHashTimestampRefresh.Text = "Refresh";
            this.btnHashTimestampRefresh.UseVisualStyleBackColor = true;
            this.btnHashTimestampRefresh.Click += new System.EventHandler(this.btnHashTimestampRefresh_Click);
            // 
            // btnSaveClientLog
            // 
            this.btnSaveClientLog.Location = new System.Drawing.Point(304, 389);
            this.btnSaveClientLog.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnSaveClientLog.Name = "btnSaveClientLog";
            this.btnSaveClientLog.Size = new System.Drawing.Size(148, 35);
            this.btnSaveClientLog.TabIndex = 18;
            this.btnSaveClientLog.Text = "SaveClientLog";
            this.btnSaveClientLog.UseVisualStyleBackColor = true;
            this.btnSaveClientLog.Click += new System.EventHandler(this.btnSaveClientLog_Click);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(9, 274);
            this.label35.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(68, 20);
            this.label35.TabIndex = 17;
            this.label35.Text = "Param 5";
            // 
            // tbHashParam5
            // 
            this.tbHashParam5.Location = new System.Drawing.Point(135, 269);
            this.tbHashParam5.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbHashParam5.Name = "tbHashParam5";
            this.tbHashParam5.Size = new System.Drawing.Size(415, 26);
            this.tbHashParam5.TabIndex = 16;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(9, 154);
            this.label34.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(68, 20);
            this.label34.TabIndex = 15;
            this.label34.Text = "Param 2";
            // 
            // tbHashParam2
            // 
            this.tbHashParam2.Location = new System.Drawing.Point(135, 149);
            this.tbHashParam2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbHashParam2.MaxLength = 0;
            this.tbHashParam2.Name = "tbHashParam2";
            this.tbHashParam2.Size = new System.Drawing.Size(415, 26);
            this.tbHashParam2.TabIndex = 14;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(9, 234);
            this.label33.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(68, 20);
            this.label33.TabIndex = 13;
            this.label33.Text = "Param 4";
            // 
            // tbHashParam4
            // 
            this.tbHashParam4.Location = new System.Drawing.Point(135, 229);
            this.tbHashParam4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbHashParam4.Name = "tbHashParam4";
            this.tbHashParam4.Size = new System.Drawing.Size(415, 26);
            this.tbHashParam4.TabIndex = 12;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(9, 194);
            this.label32.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(68, 20);
            this.label32.TabIndex = 11;
            this.label32.Text = "Param 3";
            // 
            // tbHashParam3
            // 
            this.tbHashParam3.Location = new System.Drawing.Point(135, 189);
            this.tbHashParam3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbHashParam3.MaxLength = 0;
            this.tbHashParam3.Name = "tbHashParam3";
            this.tbHashParam3.Size = new System.Drawing.Size(415, 26);
            this.tbHashParam3.TabIndex = 10;
            // 
            // tbHashOutput
            // 
            this.tbHashOutput.Location = new System.Drawing.Point(135, 349);
            this.tbHashOutput.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbHashOutput.Name = "tbHashOutput";
            this.tbHashOutput.ReadOnly = true;
            this.tbHashOutput.Size = new System.Drawing.Size(415, 26);
            this.tbHashOutput.TabIndex = 9;
            // 
            // btnGenerateHash
            // 
            this.btnGenerateHash.Location = new System.Drawing.Point(14, 346);
            this.btnGenerateHash.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnGenerateHash.Name = "btnGenerateHash";
            this.btnGenerateHash.Size = new System.Drawing.Size(112, 35);
            this.btnGenerateHash.TabIndex = 8;
            this.btnGenerateHash.Text = "Generate";
            this.btnGenerateHash.UseVisualStyleBackColor = true;
            this.btnGenerateHash.Click += new System.EventHandler(this.btnGenerateHash_Click);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(9, 314);
            this.label31.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(37, 20);
            this.label31.TabIndex = 7;
            this.label31.Text = "Salt";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(9, 74);
            this.label30.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(102, 20);
            this.label30.TabIndex = 6;
            this.label30.Text = "Mac Address";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(9, 114);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(68, 20);
            this.label21.TabIndex = 5;
            this.label21.Text = "Param 1";
            // 
            // tbHashSalt
            // 
            this.tbHashSalt.Location = new System.Drawing.Point(135, 309);
            this.tbHashSalt.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbHashSalt.Name = "tbHashSalt";
            this.tbHashSalt.Size = new System.Drawing.Size(415, 26);
            this.tbHashSalt.TabIndex = 4;
            // 
            // tbHashMacAddress
            // 
            this.tbHashMacAddress.Location = new System.Drawing.Point(135, 69);
            this.tbHashMacAddress.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbHashMacAddress.Name = "tbHashMacAddress";
            this.tbHashMacAddress.Size = new System.Drawing.Size(415, 26);
            this.tbHashMacAddress.TabIndex = 3;
            // 
            // tbHashParam1
            // 
            this.tbHashParam1.Location = new System.Drawing.Point(135, 109);
            this.tbHashParam1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbHashParam1.Name = "tbHashParam1";
            this.tbHashParam1.Size = new System.Drawing.Size(415, 26);
            this.tbHashParam1.TabIndex = 2;
            // 
            // tbHashTimestamp
            // 
            this.tbHashTimestamp.Location = new System.Drawing.Point(135, 29);
            this.tbHashTimestamp.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbHashTimestamp.Name = "tbHashTimestamp";
            this.tbHashTimestamp.Size = new System.Drawing.Size(415, 26);
            this.tbHashTimestamp.TabIndex = 1;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(9, 34);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(87, 20);
            this.label18.TabIndex = 0;
            this.label18.Text = "Timestamp";
            // 
            // btSerialisationTest
            // 
            this.btSerialisationTest.Location = new System.Drawing.Point(12, 98);
            this.btSerialisationTest.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btSerialisationTest.Name = "btSerialisationTest";
            this.btSerialisationTest.Size = new System.Drawing.Size(176, 35);
            this.btSerialisationTest.TabIndex = 2;
            this.btSerialisationTest.Text = "Download Media";
            this.btSerialisationTest.UseVisualStyleBackColor = true;
            this.btSerialisationTest.Click += new System.EventHandler(this.btSerialisationTest_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 54);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(176, 35);
            this.button1.TabIndex = 1;
            this.button1.Text = "Download Media";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnGetLastState
            // 
            this.btnGetLastState.Location = new System.Drawing.Point(12, 9);
            this.btnGetLastState.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnGetLastState.Name = "btnGetLastState";
            this.btnGetLastState.Size = new System.Drawing.Size(148, 35);
            this.btnGetLastState.TabIndex = 0;
            this.btnGetLastState.Text = "Get last-state.txt";
            this.btnGetLastState.UseVisualStyleBackColor = true;
            this.btnGetLastState.Click += new System.EventHandler(this.btnGetLastState_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btnKillSpawns);
            this.tabPage2.Controls.Add(this.label17);
            this.tabPage2.Controls.Add(this.label16);
            this.tabPage2.Controls.Add(this.tbPokeInUrl);
            this.tabPage2.Controls.Add(this.tbEventLog);
            this.tabPage2.Controls.Add(this.btnSpawnCometClients);
            this.tabPage2.Controls.Add(this.numClients);
            this.tabPage2.Controls.Add(this.label15);
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPage2.Size = new System.Drawing.Size(1494, 1052);
            this.tabPage2.TabIndex = 6;
            this.tabPage2.Text = "Comet Load Test";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btnKillSpawns
            // 
            this.btnKillSpawns.Location = new System.Drawing.Point(246, 106);
            this.btnKillSpawns.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnKillSpawns.Name = "btnKillSpawns";
            this.btnKillSpawns.Size = new System.Drawing.Size(112, 35);
            this.btnKillSpawns.TabIndex = 10;
            this.btnKillSpawns.Text = "Kill Spawns";
            this.btnKillSpawns.UseVisualStyleBackColor = true;
            this.btnKillSpawns.Click += new System.EventHandler(this.btnKillSpawns_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(446, 32);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(98, 20);
            this.label17.TabIndex = 9;
            this.label17.Text = "/host.PokeIn";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(27, 31);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(42, 20);
            this.label16.TabIndex = 8;
            this.label16.Text = "URL";
            // 
            // tbPokeInUrl
            // 
            this.tbPokeInUrl.Location = new System.Drawing.Point(124, 26);
            this.tbPokeInUrl.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbPokeInUrl.Name = "tbPokeInUrl";
            this.tbPokeInUrl.Size = new System.Drawing.Size(316, 26);
            this.tbPokeInUrl.TabIndex = 7;
            this.tbPokeInUrl.Text = "http://localhost/trunk/messaging";
            // 
            // tbEventLog
            // 
            this.tbEventLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbEventLog.Location = new System.Drawing.Point(12, 151);
            this.tbEventLog.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbEventLog.Multiline = true;
            this.tbEventLog.Name = "tbEventLog";
            this.tbEventLog.ReadOnly = true;
            this.tbEventLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbEventLog.Size = new System.Drawing.Size(0, 0);
            this.tbEventLog.TabIndex = 6;
            this.tbEventLog.TextChanged += new System.EventHandler(this.tbEventLog_TextChanged);
            // 
            // btnSpawnCometClients
            // 
            this.btnSpawnCometClients.Location = new System.Drawing.Point(124, 106);
            this.btnSpawnCometClients.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnSpawnCometClients.Name = "btnSpawnCometClients";
            this.btnSpawnCometClients.Size = new System.Drawing.Size(112, 35);
            this.btnSpawnCometClients.TabIndex = 3;
            this.btnSpawnCometClients.Text = "Spawn";
            this.btnSpawnCometClients.UseVisualStyleBackColor = true;
            this.btnSpawnCometClients.Click += new System.EventHandler(this.btnSpawnCometClients_Click);
            // 
            // numClients
            // 
            this.numClients.Location = new System.Drawing.Point(124, 66);
            this.numClients.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.numClients.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numClients.Name = "numClients";
            this.numClients.Size = new System.Drawing.Size(112, 26);
            this.numClients.TabIndex = 2;
            this.numClients.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(27, 69);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(85, 20);
            this.label15.TabIndex = 0;
            this.label15.Text = "# of clients";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.ucWebserviceLoadTester1);
            this.tabPage3.Location = new System.Drawing.Point(4, 29);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPage3.Size = new System.Drawing.Size(1494, 1052);
            this.tabPage3.TabIndex = 7;
            this.tabPage3.Text = "Webservice Stresser";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // ucWebserviceLoadTester1
            // 
            this.ucWebserviceLoadTester1.Location = new System.Drawing.Point(9, 9);
            this.ucWebserviceLoadTester1.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.ucWebserviceLoadTester1.Name = "ucWebserviceLoadTester1";
            this.ucWebserviceLoadTester1.Size = new System.Drawing.Size(909, 662);
            this.ucWebserviceLoadTester1.TabIndex = 0;
            // 
            // tabMetrics
            // 
            this.tabMetrics.Controls.Add(this.ucMetrics1);
            this.tabMetrics.Location = new System.Drawing.Point(4, 29);
            this.tabMetrics.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabMetrics.Name = "tabMetrics";
            this.tabMetrics.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabMetrics.Size = new System.Drawing.Size(1494, 1052);
            this.tabMetrics.TabIndex = 8;
            this.tabMetrics.Text = "Metrics";
            this.tabMetrics.UseVisualStyleBackColor = true;
            // 
            // ucMetrics1
            // 
            this.ucMetrics1.Location = new System.Drawing.Point(9, 9);
            this.ucMetrics1.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.ucMetrics1.Name = "ucMetrics1";
            this.ucMetrics1.Size = new System.Drawing.Size(1094, 831);
            this.ucMetrics1.TabIndex = 0;
            // 
            // tabPorts
            // 
            this.tabPorts.Controls.Add(this.ucPorts1);
            this.tabPorts.Controls.Add(this.button2);
            this.tabPorts.Location = new System.Drawing.Point(4, 29);
            this.tabPorts.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPorts.Name = "tabPorts";
            this.tabPorts.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPorts.Size = new System.Drawing.Size(1494, 1052);
            this.tabPorts.TabIndex = 9;
            this.tabPorts.Text = "Ports";
            this.tabPorts.UseVisualStyleBackColor = true;
            // 
            // ucPorts1
            // 
            this.ucPorts1.Location = new System.Drawing.Point(32, 60);
            this.ucPorts1.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.ucPorts1.Name = "ucPorts1";
            this.ucPorts1.Size = new System.Drawing.Size(1425, 923);
            this.ucPorts1.TabIndex = 2;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(12, 9);
            this.button2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(112, 35);
            this.button2.TabIndex = 0;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.ucAnalytics1);
            this.tabPage4.Location = new System.Drawing.Point(4, 29);
            this.tabPage4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPage4.Size = new System.Drawing.Size(1494, 1052);
            this.tabPage4.TabIndex = 10;
            this.tabPage4.Text = "tabPage4";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // ucAnalytics1
            // 
            this.ucAnalytics1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucAnalytics1.Location = new System.Drawing.Point(4, 5);
            this.ucAnalytics1.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.ucAnalytics1.Name = "ucAnalytics1";
            this.ucAnalytics1.Size = new System.Drawing.Size(1486, 1042);
            this.ucAnalytics1.TabIndex = 0;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.btnYahooWeatherForecast);
            this.tabPage5.Controls.Add(this.btnYahooWeatherCurrent);
            this.tabPage5.Controls.Add(this.btnOWMForecast);
            this.tabPage5.Controls.Add(this.btnOWMCurrent);
            this.tabPage5.Location = new System.Drawing.Point(4, 29);
            this.tabPage5.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPage5.Size = new System.Drawing.Size(1494, 1052);
            this.tabPage5.TabIndex = 11;
            this.tabPage5.Text = "tabPage5";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // btnYahooWeatherForecast
            // 
            this.btnYahooWeatherForecast.Location = new System.Drawing.Point(254, 423);
            this.btnYahooWeatherForecast.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnYahooWeatherForecast.Name = "btnYahooWeatherForecast";
            this.btnYahooWeatherForecast.Size = new System.Drawing.Size(284, 35);
            this.btnYahooWeatherForecast.TabIndex = 3;
            this.btnYahooWeatherForecast.Text = "Yahoo Weather Forecast";
            this.btnYahooWeatherForecast.UseVisualStyleBackColor = true;
            this.btnYahooWeatherForecast.Click += new System.EventHandler(this.btnYahooWeatherForecast_Click);
            // 
            // btnYahooWeatherCurrent
            // 
            this.btnYahooWeatherCurrent.Location = new System.Drawing.Point(254, 378);
            this.btnYahooWeatherCurrent.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnYahooWeatherCurrent.Name = "btnYahooWeatherCurrent";
            this.btnYahooWeatherCurrent.Size = new System.Drawing.Size(284, 35);
            this.btnYahooWeatherCurrent.TabIndex = 2;
            this.btnYahooWeatherCurrent.Text = "Yahoo Weather Current";
            this.btnYahooWeatherCurrent.UseVisualStyleBackColor = true;
            this.btnYahooWeatherCurrent.Click += new System.EventHandler(this.btnYahooWeatherCurrent_Click);
            // 
            // btnOWMForecast
            // 
            this.btnOWMForecast.Location = new System.Drawing.Point(254, 300);
            this.btnOWMForecast.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnOWMForecast.Name = "btnOWMForecast";
            this.btnOWMForecast.Size = new System.Drawing.Size(284, 35);
            this.btnOWMForecast.TabIndex = 1;
            this.btnOWMForecast.Text = "OpenWeatherMap Forecast";
            this.btnOWMForecast.UseVisualStyleBackColor = true;
            this.btnOWMForecast.Click += new System.EventHandler(this.btnOWMForecast_Click);
            // 
            // btnOWMCurrent
            // 
            this.btnOWMCurrent.Location = new System.Drawing.Point(254, 255);
            this.btnOWMCurrent.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnOWMCurrent.Name = "btnOWMCurrent";
            this.btnOWMCurrent.Size = new System.Drawing.Size(284, 35);
            this.btnOWMCurrent.TabIndex = 0;
            this.btnOWMCurrent.Text = "OpenWeatherMap Current";
            this.btnOWMCurrent.UseVisualStyleBackColor = true;
            this.btnOWMCurrent.Click += new System.EventHandler(this.btnOWMCurrent_Click);
            // 
            // process1
            // 
            this.process1.StartInfo.Domain = "";
            this.process1.StartInfo.LoadUserProfile = false;
            this.process1.StartInfo.Password = null;
            this.process1.StartInfo.StandardErrorEncoding = null;
            this.process1.StartInfo.StandardOutputEncoding = null;
            this.process1.StartInfo.UserName = "";
            this.process1.SynchronizingObject = this;
            // 
            // ucComet1
            // 
            this.ucComet1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucComet1.Location = new System.Drawing.Point(3, 3);
            this.ucComet1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ucComet1.MinimumSize = new System.Drawing.Size(644, 558);
            this.ucComet1.Name = "ucComet1";
            this.ucComet1.Size = new System.Drawing.Size(959, 632);
            this.ucComet1.TabIndex = 0;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.tbHotSOSResponse);
            this.tabPage6.Controls.Add(this.label49);
            this.tabPage6.Controls.Add(this.label48);
            this.tabPage6.Controls.Add(this.label47);
            this.tabPage6.Controls.Add(this.tbHotSOSPassword);
            this.tabPage6.Controls.Add(this.tbHotSOSUsername);
            this.tabPage6.Controls.Add(this.tbHotSOSUrl);
            this.tabPage6.Controls.Add(this.btCreateServiceOrder);
            this.tabPage6.Location = new System.Drawing.Point(4, 29);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(1506, 1099);
            this.tabPage6.TabIndex = 12;
            this.tabPage6.Text = "HotSOS";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // btCreateServiceOrder
            // 
            this.btCreateServiceOrder.Location = new System.Drawing.Point(130, 220);
            this.btCreateServiceOrder.Name = "btCreateServiceOrder";
            this.btCreateServiceOrder.Size = new System.Drawing.Size(275, 32);
            this.btCreateServiceOrder.TabIndex = 0;
            this.btCreateServiceOrder.Text = "Create service order";
            this.btCreateServiceOrder.UseVisualStyleBackColor = true;
            this.btCreateServiceOrder.Click += new System.EventHandler(this.btCreateServiceOrder_Click);
            // 
            // tbHotSOSUrl
            // 
            this.tbHotSOSUrl.Location = new System.Drawing.Point(130, 49);
            this.tbHotSOSUrl.Name = "tbHotSOSUrl";
            this.tbHotSOSUrl.Size = new System.Drawing.Size(315, 26);
            this.tbHotSOSUrl.TabIndex = 1;
            // 
            // tbHotSOSUsername
            // 
            this.tbHotSOSUsername.Location = new System.Drawing.Point(130, 105);
            this.tbHotSOSUsername.Name = "tbHotSOSUsername";
            this.tbHotSOSUsername.Size = new System.Drawing.Size(100, 26);
            this.tbHotSOSUsername.TabIndex = 2;
            // 
            // tbHotSOSPassword
            // 
            this.tbHotSOSPassword.Location = new System.Drawing.Point(130, 161);
            this.tbHotSOSPassword.Name = "tbHotSOSPassword";
            this.tbHotSOSPassword.Size = new System.Drawing.Size(100, 26);
            this.tbHotSOSPassword.TabIndex = 3;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(24, 55);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(90, 20);
            this.label47.TabIndex = 4;
            this.label47.Text = "HotSOS url";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(31, 105);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(83, 20);
            this.label48.TabIndex = 5;
            this.label48.Text = "Username";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(36, 161);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(78, 20);
            this.label49.TabIndex = 6;
            this.label49.Text = "Password";
            // 
            // tbHotSOSResponse
            // 
            this.tbHotSOSResponse.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbHotSOSResponse.Location = new System.Drawing.Point(28, 292);
            this.tbHotSOSResponse.Multiline = true;
            this.tbHotSOSResponse.Name = "tbHotSOSResponse";
            this.tbHotSOSResponse.Size = new System.Drawing.Size(1456, 799);
            this.tbHotSOSResponse.TabIndex = 7;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1514, 1132);
            this.Controls.Add(this.tabControl1);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Form1";
            this.Text = "ObymobiXperimental - Test App";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPms.ResumeLayout(false);
            this.tabPms.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPmsSimulateCrave.ResumeLayout(false);
            this.tabPmsSimulateCrave.PerformLayout();
            this.tabPmsSimulatePms.ResumeLayout(false);
            this.tabPmsSimulatePms.PerformLayout();
            this.tabPmsComtrol.ResumeLayout(false);
            this.tabPmsComtrol.PerformLayout();
            this.tabPmsTiger.ResumeLayout(false);
            this.tabPmsTiger.PerformLayout();
            this.tabPmsInnsist.ResumeLayout(false);
            this.tabPmsInnsist.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tabMitel.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabStresser.ResumeLayout(false);
            this.tabStresser.PerformLayout();
            this.tabComet.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numClients)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabMetrics.ResumeLayout(false);
            this.tabPorts.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPms;
        private System.Windows.Forms.TextBox tbCraveToPmsAddToFolioDescription;
        private System.Windows.Forms.TextBox tbPmsLog;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Button btPmsToCraveCheckout;
        private System.Windows.Forms.ToolStripStatusLabel lblStripConnectionState;
        private System.Windows.Forms.Button btPmsToCraveCheckIn;
        private System.Windows.Forms.Button btCraveToPmsAddToFolio;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.MaskedTextBox tbCraveToPmsAddToFolioPrice;
        private System.Windows.Forms.Button btPmsInitializeTigerTms;
        private System.Windows.Forms.Button btPmsStopTigertms;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPmsTiger;
        private System.Windows.Forms.TabPage tabPmsSimulatePms;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbPmsToCraveRoomNumber;
        private System.Windows.Forms.TabPage tabPmsSimulateCrave;
        private System.Windows.Forms.TextBox tbPmsTigerMessageInServiceUrl;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.MaskedTextBox tbCraveToPmsCheckoutBalance;
        private System.Windows.Forms.Button btCraveToPmsCheckout;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.MaskedTextBox tbCraveToPmsRoom;
        private System.Windows.Forms.Button btCraveToPmsGetGuestInformation;
        private System.Windows.Forms.Button btCraveToPmsGetFolio;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbPmsTigerExternalWebserviceUrl;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.CheckBox cbPmsTigerEmulateMessageInService;
        private System.Windows.Forms.Button btPmsToCraveRoomMove;
        private System.Windows.Forms.TextBox tbPmsToCraveOldRoomNumber;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbPmsTigerUserKey;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TabPage tabPmsComtrol;
        private System.Windows.Forms.TextBox tbPmsComtrolPort;
        private System.Windows.Forms.Label lblComtrolHost;
        private System.Windows.Forms.TextBox tbPmsComtrolHost;
        private System.Windows.Forms.Label lblComtrolPort;
        private System.Windows.Forms.Button btComtrolInit;
        private System.Windows.Forms.CheckBox cbPmsTigerEmulateExternalWebservice;
        private System.Windows.Forms.TextBox tbPmsToCraveLastname;
        private System.Windows.Forms.TextBox tbPmsToCraveFirstName;
        private System.Windows.Forms.CheckBox cbPmsToCraveAllowExpressCheckout;
        private System.Windows.Forms.CheckBox cbPmsToCraveAllowViewBill;
        private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox cbPmsToCraveCheckedIn;
        private System.Windows.Forms.TabPage tabMitel;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btMitelSend;
        private System.Windows.Forms.TextBox tbMitelSendPriority;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox tbMitelSendBody;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox tbMitelSendSubject;
        private System.Windows.Forms.TextBox tbMitelSendTo;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btMitelClientDisconnect;
        private System.Windows.Forms.Button btMitelClientConnect;
        private System.Windows.Forms.TextBox tbMitelClientListenerPort;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox tbMitelClientConnectToPort;
        private System.Windows.Forms.TextBox tbMitelClientConnectToIp;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btMitelServerStop;
        private System.Windows.Forms.Button btMitelServerStart;
        private System.Windows.Forms.TextBox tbMitelServerListenerPort;
        private System.Windows.Forms.Label label23;
        private System.Diagnostics.Process process1;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox tbMitelServerListenerIp;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox tbMitelLog;
		private System.Windows.Forms.TextBox tbMitelClientListenerIp;
        private System.Windows.Forms.TabPage tabStresser;
        private System.Windows.Forms.CheckBox cbStressGetCompany;
        private System.Windows.Forms.CheckBox cbGetEntertainment;
        private System.Windows.Forms.CheckBox cbStressGetDeliverypoints;
        private System.Windows.Forms.CheckBox cbStressUpdateLastRequest;
        private System.Windows.Forms.CheckBox cbStressGetMenu;
        private System.Windows.Forms.CheckBox cbStressGetOrders;
        private System.Windows.Forms.CheckBox cbStressGetReleaseAndDownloadLocation;
        private System.Windows.Forms.CheckBox cbStressGetSetClientStatus;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox tbStresserSeconds;
        private System.Windows.Forms.Button btStresserStart;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox tbStresserWebserviceUrl;
        private System.Windows.Forms.CheckBox cbAsync;
		private System.Windows.Forms.TabPage tabComet;
		private UcComet ucComet1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button btnGetLastState;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btSerialisationTest;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button btnSpawnCometClients;
        private System.Windows.Forms.NumericUpDown numClients;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox tbEventLog;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox tbPokeInUrl;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button btnKillSpawns;
        private System.Windows.Forms.TabPage tabPage3;
        private ucWebserviceLoadTester ucWebserviceLoadTester1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox tbHashOutput;
        private System.Windows.Forms.Button btnGenerateHash;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox tbHashSalt;
        private System.Windows.Forms.TextBox tbHashMacAddress;
        private System.Windows.Forms.TextBox tbHashParam1;
        private System.Windows.Forms.TextBox tbHashTimestamp;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox tbHashParam5;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox tbHashParam2;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox tbHashParam4;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox tbHashParam3;
        private System.Windows.Forms.TabPage tabMetrics;
        private ucMetrics ucMetrics1;
        private System.Windows.Forms.TabPage tabPorts;
        private ucPorts ucPorts1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TabPage tabPage4;
        private UcComet ucComet2;
        private System.Windows.Forms.Button btnSaveClientLog;
        private System.Windows.Forms.TextBox tbRandomOutput;
        private ucAnalytics ucAnalytics1;
        private System.Windows.Forms.Button btnHashTimestampRefresh;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Button btnOWMForecast;
        private System.Windows.Forms.Button btnOWMCurrent;
        private System.Windows.Forms.Button btnYahooWeatherForecast;
        private System.Windows.Forms.Button btnYahooWeatherCurrent;
        private System.Windows.Forms.CheckBox cbHasVoicemail;
        private System.Windows.Forms.Button btnEditGuestInformation;
        private System.Windows.Forms.Button btnClearWakeUp;
        private System.Windows.Forms.Button btnSetWakeUp;
        private System.Windows.Forms.Button btnCreateOrder;
        private System.Windows.Forms.Button btnEditRoomData;
        private System.Windows.Forms.TabPage tabPmsInnsist;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox tbInnsistToId;
        private System.Windows.Forms.TextBox tbInnsistFromId;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox tbInnsistPassword;
        private System.Windows.Forms.TextBox tbInnsistUsername;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox tbInnsistOutgoingUrl;
        private System.Windows.Forms.TextBox tbInnsistIncommingUrl;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Button btnInnsistStop;
        private System.Windows.Forms.Button btnInnsistInitialize;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox tbInnsistWakeUpCode;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox tbPmsGroup;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DateTimePicker dpPmsCheckout;
        private System.Windows.Forms.DateTimePicker dpPmsCheckin;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.Button btCreateServiceOrder;
        private System.Windows.Forms.TextBox tbHotSOSPassword;
        private System.Windows.Forms.TextBox tbHotSOSUsername;
        private System.Windows.Forms.TextBox tbHotSOSUrl;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox tbHotSOSResponse;
    }
}

