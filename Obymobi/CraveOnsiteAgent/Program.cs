using CraveOnsiteAgent.Configuration;
using Dionysos;
using Dionysos.Logging;
using Microsoft.Win32;
using Obymobi;
using Obymobi.Configuration;
using Obymobi.Logging;
using System;
using System.Diagnostics;
using System.IO;
using System.ServiceProcess;

namespace CraveOnsiteAgent
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
#if (DEBUG)
            if (TestUtil.IsPcDeveloper && Environment.CommandLine.Contains("-debug"))
            {
                DesktopLogger.SetLoggingLevel(LoggingLevel.Debug);

                // Launch debugger
                Debugger.Launch();
            }
#endif
            // Begin with this
            InitializeGlobals();

            if (Environment.CommandLine.Contains("-console"))
	        {
		        new CraveOnsiteAgent().Start(args);
	        }
	        else
	        {
		        ServiceBase[] ServicesToRun;
		        ServicesToRun = new ServiceBase[]
			        {
				        new CraveOnsiteAgent()
			        };
		        ServiceBase.Run(ServicesToRun);
	        }
        }

        private static void InitializeGlobals()
        {
            // Set the configuration provider
            Dionysos.Configuration.XmlConfigurationProvider provider = new Dionysos.Configuration.XmlConfigurationProvider();
            provider.ManualConfigFilePath = Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location), "CraveOnsiteAgent.config");

            Dionysos.Global.ConfigurationProvider = provider;

            // Set the application information
            Dionysos.Global.ApplicationInfo = new Dionysos.ServiceProcess.ServiceInfo();
            Dionysos.Global.ApplicationInfo.ApplicationName = "CraveOnsiteAgent";
            Dionysos.Global.ApplicationInfo.ApplicationVersion = "1.2019021901";            
            Dionysos.Global.ApplicationInfo.BasePath = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            Dionysos.GlobalLight.LoggingProvider = new AsyncLoggingProvider(Path.Combine(Global.ApplicationInfo.BasePath, "Logs"), "CloudStatus", 14, "CraveOnsiteAgent");

            // Set the configuration definitions
            Dionysos.Global.ConfigurationInfo.Add(new ObymobiConfigInfo());
            Dionysos.Global.ConfigurationInfo.Add(new CraveOnSiteAgentConfigInfo());
            Dionysos.Global.ConfigurationInfo.Add(new CraveCloudConfigInfo());
            Dionysos.Global.ConfigurationInfo.Add(new CraveOnSiteServerConfigInfo());

            // Try to write the application details to the registry
            try
            {
                RegistryKey craveKey = Registry.LocalMachine.CreateSubKey(@"Software\Crave");
                using (RegistryKey reqsrvKey = craveKey.CreateSubKey(Dionysos.Global.ApplicationInfo.ApplicationName))
                {
                    reqsrvKey.SetValue("Name", Dionysos.Global.ApplicationInfo.ApplicationName);
                    reqsrvKey.SetValue("Version", Dionysos.Global.ApplicationInfo.ApplicationVersion);
                    reqsrvKey.SetValue("BasePath", Dionysos.Global.ApplicationInfo.BasePath);
                }
            }
            catch
            {
            }
        }
    }
}
