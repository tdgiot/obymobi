﻿using CraveOnsiteAgent.Configuration;
using CraveOnsiteAgent.Models;
using Dionysos;
using Dionysos.IO;
using Dionysos.ServiceProcess.HelperClasses;
using Microsoft.Win32;
using Obymobi.Configuration;
using Obymobi.Constants;
using Obymobi.Enums;
using Obymobi.Logging;
using Obymobi.Logic.Status;
using Obymobi.Security;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.ServiceProcess;
using System.Threading;
using System.Timers;

namespace CraveOnsiteAgent
{
    public partial class CraveOnsiteAgent : ServiceBase
    {
        #region Fields

        System.Timers.Timer timer;
        System.Timers.Timer aortaTimer;

        private string username = string.Empty;
        private string password = string.Empty;
        private int companyId = -1;
        private readonly CloudStatus cloudStatus = new CloudStatus();
        private CraveService.CraveService craveTypedWebservice;
        private readonly Dictionary<string, DateTime> serviceRestartTimes = new Dictionary<string,DateTime>();

        private int requestInterval = 30;

        private string updatesDir = string.Empty;

        private DateTime? doPollStarted;        
        private DateTime? lastBytesReceivedDownloadingApplication;

        //private LoggingLevel loggingLevel = LoggingLevel.Debug;
        private const int MAX_DAYS_LOG_HISTORY = 30;

        private bool updating;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CraveOnsiteAgent"/> class.
        /// </summary>
        public CraveOnsiteAgent()
        {
            InitializeComponent();
        }

        #endregion

        #region Methods

		/// <summary>
		/// Should only be called if the app is running in console mode
		/// </summary>
		/// <param name="args"></param>
		public void Start(string[] args)
		{
			OnStart(args);
		}

        protected override void OnStart(string[] args)
        {
            try
            {
                // Write the start to the log file
                this.WriteToLogVerbose("-------------------------------------------------------------");
                this.WriteToLogVerbose("Starting Crave On-site Agent " + Global.ApplicationInfo.ApplicationVersion);

                // Load the configuration
                this.LoadConfiguration();

                if (this.username.IsNullOrWhiteSpace() || this.password.IsNullOrWhiteSpace() || this.companyId <= 0)
                {
                    this.WriteToLogWarning("Stopped Crave On-site Agent: Username, password or companyId is missing.");
                    this.Stop();
                }

                // Hookup the events
                this.HookupEvents();

                // Initialize the vars
                this.WriteToLogDebug("Starting Crave On-site Agent - Initializing");
                if (this.Initialize())
                {
                    // Cleanup historical log files
                    this.WriteToLogDebug("Starting Crave On-site Agent - Clean up");
                    this.Cleanup();

                    // Execute a status diagnose
                    this.WriteToLogDebug("Starting Crave On-site Agent - Refresh Cloud Status");
                    this.cloudStatus.Refresh();

                    this.WriteToLogDebug("Sending Agent application version to webservice");
                    this.SendApplicationVerionToWebservice();

                    // Start the main timer and diagnose timer
                    this.WriteToLogDebug("Starting Crave On-site Agent - Start timers");
                    this.StartTimers();
                }
            }
            catch (Exception ex)
            {
                this.LogException(ex, "An exception was thrown while trying to start the Crave On-site Agent.");
                this.Stop();
            }
        }

        /// <summary>
        /// Load the configuration values from the configuration file
        /// </summary>
        private void LoadConfiguration()
        {
            try
            {
                this.WriteToLogVerbose("Loading configuration.");

                // Check with which cloud environment we should work.
                // Upgrade for backwards compability with the new CloudEnvironment & AlternativeWebserviceUrl
                if (ConfigurationManager.GetString(CraveCloudConfigConstants.CloudEnvironment) == "NOTSET")
                {
                    this.WriteToLogDebug("Upgrading to CloudEnvironment Configuration");

                    // Old installation, upgrade!
                    string webserviceUrl = ConfigurationManager.GetString(CraveOnSiteAgentConfigConstants.WebserviceBaseUrl1);

                    if (webserviceUrl.Contains("app.", StringComparison.InvariantCultureIgnoreCase))
                        ConfigurationManager.SetValue(CraveCloudConfigConstants.CloudEnvironment, CloudEnvironment.ProductionPrimary.ToString());
                    else if (webserviceUrl.Contains("dev.", StringComparison.InvariantCultureIgnoreCase))
                        ConfigurationManager.SetValue(CraveCloudConfigConstants.CloudEnvironment, CloudEnvironment.Development.ToString());
                    else if (webserviceUrl.Contains("test.", StringComparison.InvariantCultureIgnoreCase))
                        ConfigurationManager.SetValue(CraveCloudConfigConstants.CloudEnvironment, CloudEnvironment.Test.ToString());
                    else
                    {
                        ConfigurationManager.SetValue(CraveCloudConfigConstants.CloudEnvironment, CloudEnvironment.Manual.ToString());
                        ConfigurationManager.SetValue(CraveCloudConfigConstants.ManualWebserviceBaseUrl, webserviceUrl);
                    }                    

                    this.WriteToLogDebug("CloudEnvironment Configuration upgraded to: {0}", ConfigurationManager.GetString(CraveCloudConfigConstants.CloudEnvironment));
                }           

                this.username = ConfigurationManager.GetString(CraveOnSiteAgentConfigConstants.Username);
                this.password = ConfigurationManager.GetString(CraveOnSiteAgentConfigConstants.Password);
                this.companyId = ConfigurationManager.GetInt(CraveOnSiteAgentConfigConstants.CompanyId);                

                this.requestInterval = ConfigurationManager.GetInt(CraveOnSiteAgentConfigConstants.RequestInterval);

                this.WriteToLogDebug(string.Format("Username: {0}", this.username));
                this.WriteToLogDebug(string.Format("Company ID: {0}", this.companyId));

                this.WriteToLogDebug(string.Format("Request interval: {0}", this.requestInterval));

                this.WriteToLogDebug(string.Format("Cloud Environment: {0}", this.cloudStatus.CloudEnvironment.ToString()));
                //this.WriteToLogDebug(string.Format("Cloud Base Url: {0}", this.cloudStatus.WebserviceBaseUrl));                            
            }
            catch (Exception ex)
            {
                this.LogException(ex, "An exception occurred while trying to load the configuration.");
            }
        }

        /// <summary>
        /// Hookups the events to the corresponding event handler
        /// </summary>
        private void HookupEvents()
        {
            this.WriteToLogVerbose("Hooking up events.");

            this.cloudStatus.Logged += cloudStatus_Logged;
        }

        /// <summary>
        /// Initializes the needed variables
        /// </summary>
        private bool Initialize()
        {
            bool success = true;

            this.WriteToLogVerbose("Initializing.");

            try
            {
                this.updatesDir = this.CreateDirectory("Updates");                
            }
            catch (Exception ex)
            {
                success = false;

                this.WriteToLogInfo("Initializing failed. {0}", ex.Message);
                this.LogException(ex, "An error occurred while the Crave On-site Agent was initializing.");
            }

            return success;
        }
        
        private void SendApplicationVerionToWebservice()
        {
            var provider = GetOnsiteServerConfigurationProvider();

            if (provider != null)
            {
                string ossMacAddress = provider.GetString("MacAddress");
                string ossSalt = Dionysos.Security.Cryptography.Cryptographer.DecryptStringUsingRijndael(provider.GetString("Salt"));
                long timestamp = DateTime.UtcNow.ToUnixTime();
                string hash = Hasher.GetHashFromParameters(ossSalt, timestamp, ossMacAddress, ApplicationCode.Agent, Global.ApplicationInfo.ApplicationVersion);

                try
                {
                    var result = this.Webservice.SetApplicationVersion(timestamp, ossMacAddress, ApplicationCode.Agent, Global.ApplicationInfo.ApplicationVersion, hash);
                    if (result.ResultCode != 100)
                    {
                        this.WriteToLogError("Failed to set application version. Webservic result: {0} - {1}", result.ResultCode, result.ResultMessage);
                    }
                }
                catch (Exception ex)
                {
                    this.WriteToLogError("Failed to call webservice method SetApplicationVersion. Exception: {0}", ex.Message);
                }
            }
        }

        /// <summary>
        /// Start the main timer and the diagnose timer
        /// </summary>
        private void StartTimers()
        {
            // Set the main timer
            if (this.timer == null)
            {
                this.timer = new System.Timers.Timer();                            
                this.timer.Elapsed += (TimerElapsed);
                this.timer.AutoReset = false;
                this.timer.Interval = this.requestInterval * 1000;
            }

            this.timer.Start();  
          
            // Set the Aorta Timer
            if (this.aortaTimer == null)
            {
                this.aortaTimer = new System.Timers.Timer();
                this.aortaTimer.Elapsed += aortaTimer_Elapsed;
                this.aortaTimer.AutoReset = true;
                this.aortaTimer.Interval = this.timer.Interval * 5;
            }

            this.aortaTimer.Start();            
        }

        private void DoPoll()
        {
            // Stop timer, start again when done.
            if (this.timer != null)
            {
                this.timer.Stop();
            }            
            this.doPollStarted = DateTime.Now;

            try
            {
                this.WriteToLogDebug("-----------------------------------------------------------------");

                if (this.updating)
                {
                    this.WriteToLogDebug("Currently updating, skipping check for updates...");
                }
                else
                {
                    this.WriteToLogDebug("Checking for updates...");

                    // Check which applications are installed
                    List<Application> apps = this.GetInstalledApplications();

                    if (apps.Count == 0)
                    {
                        this.WriteToLogDebug("No installed apps could be found!");
                    }
                    else
                    {
                        this.WriteToLogDebug("{0} installed apps found!", apps.Count);

                        // Update Cloud Status
                        this.cloudStatus.Refresh();

                        // Check for each application if there are updates AND if it is still running.
                        foreach (Application application in apps)
                        {
                            // GK Changed to first checking for updates, because otherwise the CheckApplicationAlive might start the application,
                            // then the update applicatio ncomes, wants to kill it, which it can't until it's fully started, but succeeds any way
                            // which ends in a loop.

                            // Check if there are updates, if not restart.
                            try
                            {
                                if (!this.UpdateApplication(application))
                                {
                                    this.WriteToLogWarning("Update Application Failed!");
                                }
                            }
                            catch(Exception ex)
                            {
                                this.WriteToLogError("UpdateApplication failed: {0}", ex.Message);
                            }

                            // Check if application is still running, if not, restart                            
                            this.CheckApplicationAlive(application);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.WriteToLogInfo("Error doing DoPoll: {0}", ex.GetAllMessages());
                this.WriteToLogInfo("Error doing DoPoll: {0}", ex.ProcessStackTrace());
            }
            finally
            { 
                // Restart timer                
                this.StartTimers();
            }
        }

        private void CheckApplicationAlive(Application application)
        {
            // Don't check self
            if (application.Name.Contains("agent", StringComparison.OrdinalIgnoreCase))
                return;

            var sc = new ServiceController(application.ServiceName);
            string serviceState;
            switch (sc.Status)
            {
                case ServiceControllerStatus.Running:
                    serviceState = "Running";
                    break;
                case ServiceControllerStatus.Stopped:
                    serviceState = "Stopped";
                    break;
                case ServiceControllerStatus.Paused:
                    serviceState = "Paused";
                    break;
                case ServiceControllerStatus.StopPending:
                    serviceState = "Stopping";
                    break;
                case ServiceControllerStatus.StartPending:
                    serviceState = "Starting";
                    break;
                default:
                    serviceState = "Status Changing";
                    break;
            }

            if (sc.Status != ServiceControllerStatus.Running &&
                sc.Status != ServiceControllerStatus.StartPending)
            {
                this.WriteToLogWarning("Process '{0}' was not running (it was: '{1}'), starting it.", application.Name, serviceState);

                // Keek track of last restart attempts
                if (this.serviceRestartTimes.ContainsKey(application.Name))
                    this.serviceRestartTimes[application.Name] = DateTime.Now;
                else
                    this.serviceRestartTimes.Add(application.Name, DateTime.Now);

                sc.Start();
                if (!ServiceHelper.StartService(application.ServiceName, 60000))                
                    this.WriteToLogWarning("Process '{0}' was not running (it was: '{1}'), start failed.", application.Name, serviceState);                
                else
                    this.WriteToLogWarning("Process '{0}' was not running (it was: '{1}'), start succeeded.", application.Name, serviceState);
            }
            else
            { 
                // Check latest addition to the log file, it it's older than 3 minutes - restart
                // Might crash a POS sync or media download... Exciting! We'll see that then (build in the 15 minute retry interval to prevent looping).

                // Get the log file path.
                string fileName = Path.GetFileName(DesktopLogger.GetFilename());
                string logFilePath = string.Format("{0}/Logs/{1}", Path.GetDirectoryName(application.Path), fileName);

                // Be graceful first 2 minutes of the day, might just no have written the file yet.
                if (DateTime.Now.Hour == 0 && DateTime.Now.Minute < 2)
                {
                    // Midnight, grace.
                }
                else if(!File.Exists(logFilePath) || (DateTime.Now - new FileInfo(logFilePath).LastWriteTime).TotalMinutes > 3)
                {
                    if (this.serviceRestartTimes.ContainsKey(application.Name) &&
                        (DateTime.Now - this.serviceRestartTimes[application.Name]).TotalMinutes < 15)
                    {
                        this.WriteToLogWarning("NOT Restarting service: '{0}' due to inactivity of log file for 3 minutes - Already restarted at '{1}'.", application.Name, this.serviceRestartTimes[application.Name]);                        
                    }
                    else
                    {
                        this.WriteToLogWarning("Restarting service: '{0}' due to inactivity of log file for 3 minutes.", application.Name);
                        this.WriteToLogWarning("Log file: {0}", logFilePath);

                        // Stop the service
                        if (!Dionysos.Diagnostics.Process.KillProcess(application.ServiceName))
                        {
                            this.WriteToLogWarning("Stop service: '{0}' failed - Can't restart, try again in next iteration.", application.Name);
                        }
                        else
                        {
                            // Service is now stopped

                            // Append to it's log file: 'THIS SERVICE WAS RESTARTED BY THE AGENT DUE TO 5 MINUTES OF INACTIVITY'
                            try
                            {
                                File.WriteAllText(logFilePath, @"\r\nTHIS SERVICE IS NOW BEING RESTARTED BY THE AGENT DUE TO 3 MINUTES OF INACTIVITY OF THE LOG FILE\r\n");
                            }
                            catch (Exception)
                            {
                                Debug.WriteLine("Failed to write to log, but service is restarting");
                            }

                            // Start the Service
                            if (!ServiceHelper.StartService(application.ServiceName, 60000))
                                this.WriteToLogWarning("Process '{0}' was not active (no activity in log), start failed.", application.Name);
                            else
                                this.WriteToLogWarning("Process '{0}' was not active (no activity in log), start succeeded.", application.Name);                    

                            // Log the latest restart (attempt), don't do another restart in 15 minutes (to prevent looping)
                            if (this.serviceRestartTimes.ContainsKey(application.Name))
                                this.serviceRestartTimes[application.Name] = DateTime.Now;
                            else
                                this.serviceRestartTimes.Add(application.Name, DateTime.Now);
                        }
                    }
                }                
            }
        }

        private Dionysos.Configuration.XmlConfigurationProvider GetOnsiteServerConfigurationProvider()
        {
            string path = @"C:\Program Files\Crave\Crave Onsite Server\CraveOnSiteServer.config";
            Dionysos.Configuration.XmlConfigurationProvider provider = null;
            if (File.Exists(path))
            {
                provider = new Dionysos.Configuration.XmlConfigurationProvider();
                provider.ManualConfigFilePath = path;
            }
            else
            {
                this.WriteToLogError("CraveOnSiteServer.config could not be found at '{0}'.", path);
            }

            return provider;
        }

        private bool UpdateApplication(Application application)
        {           
            bool updatesInstalled = false;
            var provider = GetOnsiteServerConfigurationProvider();
            if (provider != null)
            {
                string ossMacAddress = provider.GetString("MacAddress");
                string ossSalt = Dionysos.Security.Cryptography.Cryptographer.DecryptStringUsingRijndael(provider.GetString("Salt"));
                long timestamp = DateTime.UtcNow.ToUnixTime();
                string hash = Hasher.GetHashFromParameters(ossSalt, timestamp, ossMacAddress, application.Name);

                string latestVersionAndDownloadLocation = this.Webservice.GetReleaseAndDownloadLocation(timestamp, ossMacAddress, application.Name, hash);
                if (!latestVersionAndDownloadLocation.IsNullOrWhiteSpace())
                {
                    // GK Shouldn't you've used Regex? ;) - Although I approve of simple logic, of course.
                    // ES "Some people, when confronted with a problem, think “I know, I'll use regular expressions.” Now they have two problems."
                    // Remove the leading [
                    if (latestVersionAndDownloadLocation.StartsWith("["))
                        latestVersionAndDownloadLocation = latestVersionAndDownloadLocation.Substring(1);

                    // Remove the trailing ]
                    if (latestVersionAndDownloadLocation.EndsWith("]"))
                        latestVersionAndDownloadLocation = latestVersionAndDownloadLocation.Substring(0, latestVersionAndDownloadLocation.Length - 1);

                    // Check whether the string contains a |
                    if (latestVersionAndDownloadLocation.Contains("|"))
                    {
                        // Split the string on the |
                        string[] parts = latestVersionAndDownloadLocation.Split('|');
                        if (parts.Length == 2)
                        {
                            // Get the latest version and download location
                            string latestVersion = parts[0];
                            string downloadLocation = parts[1];

                            // Get the version of the installed application
                            string installedVersion = string.Empty;
                            try
                            {
                                RegistryKey craveKey = Registry.LocalMachine.CreateSubKey(@"Software\Crave");
                                if (craveKey != null)
                                {
                                    foreach (string subKeyName in craveKey.GetSubKeyNames())
                                    {
                                        if (subKeyName.Equals(application.Name, StringComparison.InvariantCultureIgnoreCase))
                                        {
                                            using (RegistryKey appKey = craveKey.OpenSubKey(subKeyName))
                                            {
                                                if (appKey != null)
                                                {
                                                    foreach (string valueName in appKey.GetValueNames())
                                                    {
                                                        if (valueName.Equals("Version", StringComparison.InvariantCultureIgnoreCase))
                                                        {
                                                            installedVersion = appKey.GetValue(valueName).ToString();
                                                            if (installedVersion.StartsWith("1."))
                                                                installedVersion = installedVersion.Substring(2);

                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        if (installedVersion.Length > 0)
                                            break;
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                this.WriteToLogError("Exception: " + ex.Message);
                            }

                            bool update = false;

                            if (installedVersion.Length == 0)
                            {
                                this.WriteToLogWarning("Installed version could not be retrieved from registry for application '{0}'.", application.Name);
                                update = true;
                            }
                            else
                            {
                                this.WriteToLogDebug("Comparing installed version '{0}' with latest version '{1}' for application '{2}'.", installedVersion, latestVersion, application.Name);

                                // Compare the installed version with the latest version
                                int latestVersionInt;
                                int installedVersionInt;

                                if (!int.TryParse(latestVersion, out latestVersionInt))
                                    this.WriteToLogError("Latest version '{0}' for application '{1}' could not be parsed to an integer!", latestVersion, application.Name);
                                else if (!int.TryParse(installedVersion, out installedVersionInt))
                                    this.WriteToLogError("Installed version '{0}' for application '{1}' could not be parsed to an integer!", installedVersion, application.Name);
                                else if (latestVersionInt <= installedVersionInt)
                                    this.WriteToLogError("Installed version '{0}' for application '{2}' is up-to-date compared to latest version '{1}'.", installedVersion, latestVersion, application.Name);
                                else
                                    update = true;
                            }

                            if (update)
                            {
                                // An update is needed
                                if (!this.UpdateApplication(application, latestVersion, downloadLocation))
                                {
                                    this.WriteToLogError("Update of {0} from version {1} to {2} has failed!", application, latestVersion, downloadLocation);
                                }
                                else
                                    updatesInstalled = true;
                            }
                        }
                    }
                }
                else
                {
                    this.WriteToLogError("Latest version and download location for application '{0}' could not be retrieved from the ObymobiTypedWebservice.", application.Name);
                }
            }

            return updatesInstalled;
        }

        private bool UpdateApplication(Application application, string version, string downloadLocation)
        {
            bool success = false;

            string url = string.Empty;
            foreach (string cdnUrl in this.cloudStatus.CdnBaseUrls)
            {
                try
                {
                    if (!success)
                    {
                        // Postfix the base url with / if it has to
                        string baseUrl = cdnUrl.EndsWith("/") ? cdnUrl : cdnUrl + "/";
                        url = baseUrl + downloadLocation;

                        // Create the application directory
                        string applicationDirectory = Path.Combine(this.updatesDir, application.Name);
                        this.CreateDirectory(applicationDirectory);

                        // Create the version directory
                        string versionDirectory = Path.Combine(applicationDirectory, version);
                        this.CreateDirectory(versionDirectory);

                        // Create the path to the update file
                        const string filename = "update.zip";
                        string updateFilePath = Path.Combine(versionDirectory, filename);

                        // Download update if not already exists
                        bool downloaded = this.DownloadApplication(application, url, updateFilePath);
                        if (downloaded)
                        {
                            this.updating = true;

                            // Get the process name
                            string processName = Path.GetFileNameWithoutExtension(application.Path);

                            // Check whether is an update for the CraveOnsiteAgent or another application
                            if (application.Name.Equals("CraveOnsiteAgent"))
                            {
                                // Create a directory to unpack the files to
                                string unpackedDir = Path.Combine(applicationDirectory, "unpacked");
                                if (Directory.Exists(unpackedDir))
                                    Directory.Delete(unpackedDir, true);
                                this.CreateDirectory(unpackedDir);

                                // Unzip the update file to the unpacked directory
                                if (ZipUtil.Unzip(updateFilePath, unpackedDir, true))
                                {
                                    this.WriteToLogVerbose(@"Update file '{0}' unzipped to directory '{1}'.", updateFilePath, unpackedDir);

                                    // Check whether the updater executable exists
                                    string updaterExePath = @"C:\Program Files\Crave\Crave Onsite Agent\CraveOnsiteAgentUpdater.exe";
                                    if (!File.Exists(updaterExePath))
                                    {
                                        this.WriteToLogVerbose(@"Updater executable '{0}' does not exist!", updaterExePath);
                                    }
                                    else
                                    {
                                        this.WriteToLogVerbose(@"Starting updater executable '{0}'.", updaterExePath);

                                        // Start the updater executable
                                        success = true;
                                        Process.Start(updaterExePath);
                                    }
                                }
                            }
                            else
                            {
                                this.WriteToLogVerbose(@"Trying to kill process '{0}'.", processName);

                                DateTime killingAttemptsStarted = DateTime.Now;

                                int killAttempt = 1;
                                while ((DateTime.Now - killingAttemptsStarted).TotalSeconds < 60)
                                {
                                    this.WriteToLogVerbose(@"Trying to kill process attempt '{0}' - Max 60 seconds.", killAttempt);
                                    if (Dionysos.Diagnostics.Process.KillProcess(processName))
                                    {
                                        break;
                                    }

                                    Thread.Sleep(5000);
                                    killAttempt++;
                                }

                                // Get the log file path.
                                string fileName = Path.GetFileName(DesktopLogger.GetFilename());
                                string logFilePath = string.Format("{0}/Logs/{1}", Path.GetDirectoryName(application.Path), fileName);
                                File.WriteAllText(logFilePath, @"\r\nTHIS SERVICE IS NOW BEING RESTARTED BY THE AGENT DUE TO AN UPDATE\r\n");

                                // Stop the service or application (kill the process)
                                if (Dionysos.Diagnostics.Process.KillProcess(processName))
                                {
                                    // Get the directory to the application
                                    string appDir = Path.GetDirectoryName(application.Path) ?? "C:/";

                                    // Check whether the update file exists
                                    if (File.Exists(updateFilePath))
                                    {
                                        // Unzip the update file
                                        if (ZipUtil.Unzip(updateFilePath, appDir, true))
                                        {
                                            this.WriteToLogVerbose(@"Update file '{0}' unzipped to directory '{1}'.", updateFilePath, appDir);

                                            // Check whether a post-update batch file exists
                                            string postUpdateBatchFile = Path.Combine(appDir, "postupdate.bat");
                                            if (File.Exists(postUpdateBatchFile))
                                            {
                                                this.WriteToLogVerbose(@"Starting post-update batch file '{0}'.", postUpdateBatchFile);

                                                // Start the batch file
                                                Process.Start(postUpdateBatchFile);

                                                this.WriteToLogVerbose(@"Post-update batch file '{0}' started. Wait for 5 seconds to execute the batch file.", postUpdateBatchFile);

                                                // Wait for 5 seconds
                                                Thread.Sleep(5000);

                                                this.WriteToLogVerbose(@"Deleting post-update batch file '{0}'.", postUpdateBatchFile);
                                            }

                                            // Start the service or application again
                                            if (!ServiceHelper.StartService(application.ServiceName, 60000))
                                            {
                                                this.WriteToLogVerbose(@"Service '{0}' could not be started!", application.ServiceName);

                                            }
                                            else
                                            {
                                                this.WriteToLogVerbose(@"Service '{0}' started.", application.ServiceName);
                                                success = true;
                                            }
                                        }
                                        else
                                            this.WriteToLogVerbose(@"Update file '{0}' could not be unzipped!", updateFilePath);
                                    }
                                }
                                else
                                    this.WriteToLogVerbose(@"Process '{0}' could not be killed!", processName);
                            }
                        }                        
                    }
                }
                catch (WebException)
                {
                    success = false;
                    this.WriteToLogError(@"Failed to download update for application '{0}' from url '{1}'.", application.Name, url);
                }
                catch (Exception ex)
                {
                    success = false;
                    this.WriteToLogError(@"An error occurred while updating application '{0}' from url '{1}' with message '{2}'.", application.Name, url, ex.ToString());
                }
            }

            // Reset the updating flag
            this.updating = false;

            return success;
        }

        private bool DownloadApplication(Application application, string url, string updateFilePath)
        {
            bool success = false;
            try
            {
                if (!File.Exists(updateFilePath))
                {
                    this.WriteToLogDebug(@"Downloading update for application '{0}' from url '{1}'.", application.Name, url);

                    WebRequest req = WebRequest.Create(url);
                    req.Proxy = null;
                    WebResponse response = req.GetResponse();

                    Stream stream = response.GetResponseStream();

                    if (stream != null)
                    {
                        // Download in chunks
                        var buffer = new byte[1024];
                        //double downloadProgress = 0;
                        //var dataLength = (double)response.ContentLength;

                        var memStream = new FileStream(updateFilePath, FileMode.OpenOrCreate);

                        while (true)
                        {
                            // Read data from stream
                            int bytesRead = stream.Read(buffer, 0, buffer.Length);
                            if (bytesRead == 0)
                            {
                                break; // Finished downloading
                            }

                            memStream.Write(buffer, 0, bytesRead);
                            lastBytesReceivedDownloadingApplication = DateTime.Now;
                            //downloadProgress += bytesRead;
                        }

                        // Clean up
                        stream.Flush();
                        stream.Close();
                        memStream.Flush();
                        memStream.Close();
                    }
                    success = true;
                    this.WriteToLogVerbose(@"Finished downloading update for application '{0}' from url '{1}'.", application.Name, url);
                }
                else
                {
                    this.WriteToLogWarning(@"Update for '{0}' already exists.", application.Name, url);
                }
            }
            catch (Exception ex)
            {
                success = false;
                this.WriteToLogWarning(@"Exception in DownloadApplication for '{0}' exception '{1}'.", application.Name, ex.Message);
            }
            return success;
        }

        public static System.Security.SecureString ReadPassword(string password)
        {
            var secPass = new System.Security.SecureString();
            foreach (char t in password)
                secPass.AppendChar(t);
            return secPass;
        }

        private List<Application> GetInstalledApplications()
        {
            List<Application> apps = new List<Application>();

            string path = @"C:\Program Files\Grenos\Obymobi Request Service\ObymobiRequestService.exe";
            if (File.Exists(path))
                apps.Add(new Application("ObymobiRequestService", path, "ObymobiRequestService"));

            path = @"C:\Program Files\Crave\Crave Onsite Server\CraveOnsiteServer.exe";
            if (File.Exists(path))
                apps.Add(new Application("CraveOnsiteServer", path, "CraveOnsiteServer"));

            path = @"C:\Program Files\Crave\Crave Onsite Agent\CraveOnsiteAgent.exe";
            if (File.Exists(path))
                apps.Add(new Application("CraveOnsiteAgent", path, "Crave On-site Agent"));

            path = @"C:\Program Files\Crave\Crave Onsite SupportTools\CraveOnsiteSupportTools.exe";
            if (File.Exists(path))
                apps.Add(new Application("CraveOnsiteSupportTools", path, "CraveOnsiteSupportTools"));

            return apps;
        }

        protected override void OnStop()
        {
        }

        /// <summary>
        /// Writes the specified contents to the log file
        /// </summary>
        /// <param name="contents">The contents to write to the logfile</param>
        /// <param name="args">Arguements</param>
        private void WriteToLogInfo(string contents, params object[] args)
        {
            DesktopLogger.Info(contents, args);
        }

        /// <summary>
        /// Writes the specified contents to the log file
        /// </summary>
        /// <param name="contents">The contents to write to the logfile</param>
        /// <param name="args">Arguements</param>
        private void WriteToLogDebug(string contents, params object[] args)
        {
            DesktopLogger.Debug(contents, args);
        }

        /// <summary>
        /// Writes the specified contents to the log file
        /// </summary>
        /// <param name="contents">The contents to write to the logfile</param>
        /// <param name="args">Arguements</param>
        private void WriteToLogVerbose(string contents, params object[] args)
        {
            DesktopLogger.Verbose(contents, args);
        }

        /// <summary>
        /// Writes the specified contents to the log file
        /// </summary>
        /// <param name="contents">The contents to write to the logfile</param>
        /// <param name="args">Arguements</param>
        private void WriteToLogWarning(string contents, params object[] args)
        {
            DesktopLogger.Warning(contents, args);
        }

        /// <summary>
        /// Writes the specified contents to the log file
        /// </summary>
        /// <param name="contents">The contents to write to the logfile</param>
        /// <param name="args">Arguements</param>
        private void WriteToLogError(string contents, params object[] args)
        {
            DesktopLogger.Error(contents, args);
        }


        /// <summary>
        /// Creates a directory relative to the application directory
        /// </summary>
        /// <param name="relativeDir">The directory relative to the application directory</param>
        /// <returns>The full path to the relative directory</returns>
        private string CreateDirectory(string relativeDir)
        {
            string dir = Path.Combine(this.ApplicationDirectory, relativeDir);
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }

            return dir;
        }

        /// <summary>
        /// Logs an exception to the exception server or alternatively to a log file
        /// </summary>
        /// <param name="ex">The Exception instance to log</param>
        /// <param name="message">The message to write to the log file</param>
        private void LogException(Exception ex, string message)
        {
            if (ex is Obymobi.ObymobiException)
            {
                var wex = ex as Obymobi.ObymobiException;
                this.WriteToLogError("{0} {1}", message, wex.ErrorEnumValue.ToString());

                try
                {
                    this.SaveExceptionLog(wex);
                }
                catch
                {
                }
            }
            else
            {
                this.WriteToLogError("{0} {1} {2}", message, ex.Message, ex.StackTrace);

                try
                {
                    this.SaveExceptionLog(ex);
                }
                catch
                {
                }
            }
        }

        /// <summary>
        /// Saves a log for an exception to disk
        /// </summary>
        /// <param name="ex">The exception to save to disk</param>
        private void SaveExceptionLog(Exception ex)
        {
            // Create the Exceptions directory if it doesn't exist yet
            string dir = this.CreateDirectory("Exceptions");

            // Write the exception log to disk
            File.WriteAllText(Path.Combine(dir, string.Format("{0}.log", TimeStamp.CreateTimeStamp())), string.Format("{0}\r\n\r\n{1}", ex.Message, ex.StackTrace));
        }

        /// <summary>
        /// Cleanup old logs files
        /// </summary>
        private void Cleanup()
        {
            try
            {
                // Clean up log files
                string now = string.Format("{0}{1}{2}", DateTime.Now.Year.ToString("0000"), DateTime.Now.Month.ToString("00"), DateTime.Now.Day.ToString("00"));
                int nowNumber;
                if (Int32.TryParse(now, out nowNumber))
                {
                    string logFileDir = Path.Combine(this.ApplicationDirectory, "Logs");
                    if (Directory.Exists(logFileDir))
                    {
                        string[] logFiles = Directory.GetFiles(logFileDir);
                        for (int j = 0; j < logFiles.Length; j++)
                        {
                            string logFilePath = logFiles[j];
                            string logFileName = Path.GetFileNameWithoutExtension(logFiles[j]);
                            int logFileNumber;
                            if (Int32.TryParse(logFileName, out logFileNumber))
                            {
                                if ((nowNumber - logFileNumber) > MAX_DAYS_LOG_HISTORY)
                                {
                                    if (File.Exists(logFilePath))
                                    {
                                        File.Delete(logFilePath);
                                        this.WriteToLogVerbose("Historical log file '{0}' deleted.", logFilePath);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // Log the exception
                this.LogException(ex, "An exception occurred while trying to cleanup the historical log files.");
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the directory where the application is installed
        /// </summary>
        public string ApplicationDirectory
        {
            get
            {
                return Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            }
        }

        /// <summary>
        /// Gets the currently active webservice
        /// </summary>
        public CraveService.CraveService Webservice
        {
            get
            {
                // Check if we have no Webservice instance, or the url has changed
                if (this.craveTypedWebservice == null || this.craveTypedWebservice.Url != this.cloudStatus.WebserviceBaseUrl)
                {
                    this.craveTypedWebservice = new CraveService.CraveService();
                    this.craveTypedWebservice.Url = this.cloudStatus.CraveServiceUrl;
                }

                return this.craveTypedWebservice;
            }
        }        

        #endregion

        #region Event handlers

        private void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            this.DoPoll();            
        }

        void aortaTimer_Elapsed(object sender, ElapsedEventArgs e)
        {            
            double secondsSinceLastReceivedBytesFromDownload = double.MaxValue;
            if(this.lastBytesReceivedDownloadingApplication.HasValue)
                secondsSinceLastReceivedBytesFromDownload = (DateTime.Now - this.lastBytesReceivedDownloadingApplication.Value).TotalSeconds;

            double secondsSinceCurrentPollWasStarted = double.MaxValue;
            if(this.doPollStarted.HasValue)
                secondsSinceCurrentPollWasStarted = (DateTime.Now - this.doPollStarted.Value).TotalSeconds;            

            // Check if the Polling hasn't stalled.
            if (secondsSinceLastReceivedBytesFromDownload < 60)
            { 
                // Ok, we received bytes in the last minute 
                // (so if a poll takes > 10 minutes because of downloading it won't fail).
            }
            else if (secondsSinceCurrentPollWasStarted < 600)
            { 
                // Ok, we allow up to 10 minutes for a poll to run again.                
            }
            else 
            { 
                // Not ok, reset timer. 
                this.timer.Stop();
                this.timer.Dispose();
                this.timer = null;
                this.StartTimers();
            }
        }

        void cloudStatus_Logged(object sender, LoggingLevel level, string log)
        {
            switch (level)
            {
                case LoggingLevel.Verbose:
                    this.WriteToLogVerbose(log);
                    break;
                case LoggingLevel.Debug:
                    this.WriteToLogDebug(log);
                    break;
                case LoggingLevel.Info:
                    this.WriteToLogInfo(log);
                    break;
                case LoggingLevel.Warning:
                    this.WriteToLogWarning(log);
                    break;
                case LoggingLevel.Error:
                    this.WriteToLogError(log);
                    break;
                default:
                    this.WriteToLogVerbose(log);
                    break;
            }
        }

        #endregion
    }
}
