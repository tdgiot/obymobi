﻿namespace CraveOnsiteAgent
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CraveOnsiteAgentProcessInstaller1 = new System.ServiceProcess.ServiceProcessInstaller();
            this.CraveOnsiteAgentServiceInstaller1 = new System.ServiceProcess.ServiceInstaller();
            // 
            // CraveOnsiteAgentProcessInstaller1
            // 
            this.CraveOnsiteAgentProcessInstaller1.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.CraveOnsiteAgentProcessInstaller1.Password = null;
            this.CraveOnsiteAgentProcessInstaller1.Username = null;
            // 
            // CraveOnsiteAgentServiceInstaller1
            // 
            this.CraveOnsiteAgentServiceInstaller1.Description = "Checks whether updates are available, downloads updates and installs them";
            this.CraveOnsiteAgentServiceInstaller1.DisplayName = "Crave On-site Agent";
            this.CraveOnsiteAgentServiceInstaller1.ServiceName = "CraveOnsiteAgent";
            this.CraveOnsiteAgentServiceInstaller1.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.CraveOnsiteAgentProcessInstaller1,
            this.CraveOnsiteAgentServiceInstaller1});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller CraveOnsiteAgentProcessInstaller1;
        private System.ServiceProcess.ServiceInstaller CraveOnsiteAgentServiceInstaller1;
    }
}