﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using Obymobi.Enums;
using Obymobi.Integrations.POS.Simphony.Models.Requests;
using Obymobi.Integrations.POS.Simphony.Models.Responses;
using Obymobi.Integrations.POS.Simphony.simphonyposapiweb;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;

namespace Obymobi.Integrations.POS.Simphony
{
    public class SimphonyConnectorMock
    {
        private readonly string employeeObjectNum;
        private readonly List<int> revenueCenters;
        private readonly bool useOldPostTransactionMethod;
        private readonly bool syncInStockProductsOnly;

        private readonly SimphonyPosAPIWeb testLab;

        private const int MaxCondimentGroups = 256;

        public SimphonyConnectorMock(string webServiceUrl, string employeeObjectNum, string revenueCentersAsString, bool useOldPostTransactionMethod, bool syncInStockProductsOnly)
        {
            this.employeeObjectNum = employeeObjectNum;
            this.revenueCenters = GetRevenueCenters(revenueCentersAsString);
            this.useOldPostTransactionMethod = useOldPostTransactionMethod;
            this.syncInStockProductsOnly = syncInStockProductsOnly;

            testLab = new SimphonyPosAPIWeb();

            testLab.Url = webServiceUrl;
        }

        public Poscategory[] GetPoscategories()
        {
            List<Poscategory> posCategories = new List<Poscategory>();

            foreach (int revenueCenter in revenueCenters)
            {
                List<FamilyGroups.DbFamilyGroup> familyGroups = GetFamilyGroups(revenueCenter);
                List<MajorGroups.DbMajorGroup> majorGroups = GetMajorGroups(revenueCenter);

                posCategories.AddRange(familyGroups.Select(c => new Poscategory(int.Parse(c.FamGrpID), revenueCenter, c.ObjectNumber, c.Name.StringText)));
                posCategories.AddRange(majorGroups.Select(c => new Poscategory(int.Parse(c.MajGrpID), revenueCenter, c.ObjectNumber, c.Name.StringText)));
            }

            return posCategories.ToArray();
        }

        public Posdeliverypoint[] GetPosdeliverypoints()
        {
            List<Posdeliverypoint> posdeliverypoints = new List<Posdeliverypoint>();

            foreach (int revenueCenter in revenueCenters)
            {
                List<Tables.DbDiningTable> diningTables = GetDiningTables(revenueCenter);

                posdeliverypoints.AddRange(diningTables.Select(d => new Posdeliverypoint
                {
                    ExternalId = d.DiningTableID,
                    Number = d.ObjectNumber,
                    Name = d.Name.StringText,
                    RevenueCenter = revenueCenter.ToString()
                }));
            }

            return posdeliverypoints.ToArray();
        }

        public Posorder GetPosorder(string deliverypointNumber)
        {
            throw new NotImplementedException();
        }

        public OrderProcessingError SaveOrder(Posorder posorder)
        {
            try
            {
                foreach (int revenueCenter in revenueCenters)
                {
                    if (useOldPostTransactionMethod)
                    {
                        PostTransactionExRequest request = BuildPostTransactionExRequest(posorder, revenueCenter);

                        var ppMenuItems = request.ppMenuItems;
                        var pGuestCheck = request.pGuestCheck;
                        var ppComboMeals = request.ppComboMeals;
                        var pServiceChg = request.pServiceChg;
                        var pSubTotalDiscount = request.pSubTotalDiscount;
                        var pTmedDetail = request.pTmedDetail;
                        var pTotalsResponse = request.pTotalsResponse;
                        var ppCheckPrintLines = request.ppCheckPrintLines;
                        var ppVoucherOutput = request.ppVoucherOutput;

                        testLab.PostTransactionEx("OPN", ref pGuestCheck, ref ppMenuItems, ref ppComboMeals,
                            ref pServiceChg, ref pSubTotalDiscount, ref pTmedDetail, ref pTotalsResponse,
                            ref ppCheckPrintLines, ref ppVoucherOutput);

                        string responseXml = XmlHelper.Serialize(pTotalsResponse);

                        return ProcessOrderOperationalResult(responseXml,
                            pTotalsResponse.OperationalResult, posorder);

                    }
                    else
                    {
                        PostTransactionEx2Request request = BuildPostTransactionEx2Request(posorder, revenueCenter);

                        var ppMenuItems = request.ppMenuItemsEx;
                        var pGuestCheck = request.pGuestCheck;
                        var ppComboMeals = request.ppComboMealsEx;
                        var pServiceChg = request.pSvcChargeEx;
                        var pSubTotalDiscount = request.pSubTotalDiscountEx;
                        var pTmedDetail = request.pTmedDetailEx2;
                        var pTotalsResponse = request.pTotalsResponseEx;
                        var ppCheckPrintLines = request.ppCheckPrintLines;
                        var ppVoucherOutput = request.ppVoucherOutput;

                        testLab.PostTransactionEx2(ref pGuestCheck, ref ppMenuItems, ref ppComboMeals,
                            ref pServiceChg, ref pSubTotalDiscount, ref pTmedDetail, ref pTotalsResponse,
                            ref ppCheckPrintLines, ref ppVoucherOutput, new SimphonyPosApi_Extensibility[] { });

                        string responseXml = XmlHelper.Serialize(pTotalsResponse);

                        return ProcessOrderOperationalResult(responseXml,
                            pTotalsResponse.OperationalResult, posorder);
                    }
                }
            }
            catch (System.Net.WebException e)
            {
                posorder.ErrorMessage = e.Message;
                return OrderProcessingError.PosErrorConnectivityProblem;
            }

            //No revenue centers configured
            return OrderProcessingError.PosErrorConfigurationIncorrect;
        }

        public Posproduct[] GetPosproducts()
        {
            List<Posproduct> posproducts = new List<Posproduct>();

            foreach (int revenueCenter in revenueCenters)
            {
                List<DbMenuItemDefinition> menuItemDefinitions = GetMenuItemDefinitions(revenueCenter);
                List<DbMenuItemClass> menuItemClasses = GetMenuItemClasses(revenueCenter);
                List<DbMenuItemPrice> menuItemPrices = GetMenuItemPrices(revenueCenter);

                //Alteration Items
                List<DbMenuItemClass> condimentMenuItemClasses = menuItemClasses.Where(m => BinaryStringHasBitSet(m.MemberOfCondiments)).ToList();

                foreach (DbMenuItemDefinition menuItemDefinition in menuItemDefinitions)
                {
                    if (syncInStockProductsOnly &&
                        bool.TryParse(menuItemDefinition.OutOfMenuItem, out bool outOfSock) && outOfSock)
                    {
                        continue;
                    }

                    DbMenuItemClass menuItemClass = menuItemClasses.SingleOrDefault(c =>
                        c.ObjectNumber == menuItemDefinition.MenuItemClassObjNum);

                    //MenuItemDefinition is a Product if its linked MenuItemClass has a binary string with bits set for RequiredCondiments (If the MenuItemDefinition is an Alteration then the MemberOfCondiments binary will have bits set instead)
                    if (menuItemClass != null && BinaryStringHasBitSet(menuItemDefinition.MainLevels) &&
                        !BinaryStringHasBitSet(menuItemClass.MemberOfCondiments))
                    {
                        Posproduct posproduct = new Posproduct
                        {
                            ExternalId = menuItemDefinition.MiMasterObjNum, //Used for adding this item to a Simphony order
                            CompanyId = revenueCenter,
                            Name = menuItemDefinition.Name1.StringText,
                            Description = menuItemDefinition.LongDescriptor.StringText,
                            PriceIn = GetPriceForProduct(menuItemDefinition.MenuItemDefID,
                                menuItemDefinition.SequenceNum, menuItemPrices),

                            /* PRODUCT ALTERATION CONVERSION
                            
                            1. Find all the MenuItemClass where RequiredCondiments or AllowedCondiments has a 1 set which match the bit set for the MemberOfCondiments field for the MenuItemClass beloning to the Product's MenuItemDefinition
                            1.1. Convert these PosAlteration
                            
                            2. Find all MenuItemClass where MemberOfCondiment flag corresponds with step 1 flag
                            
                            3. Find MenuItemDefinitions where MenuItemClassObjNum equals step 2 MenuItemClass.ObjectNumber
                            3.1. Convert to PosAlterationoption
                            3.2. Set fieldvalue1 to say if required or not (from model of step 2)

                            */

                            Posalterations = GetPosalterationsForProduct(
                                menuItemDefinition,
                                menuItemClass,
                                menuItemDefinitions,
                                condimentMenuItemClasses,
                                revenueCenter),
                            VatTariff = 1,
                            RevenueCenter = revenueCenter.ToString()

                        };

                        if (menuItemDefinition.MenuLevelEntries != null &&
                            menuItemDefinition.MenuLevelEntries.DbMenuItemMenuLevelEntry.Any())
                        {
                            //ToDo: Some assumptions being made here: the example data suggests that MenuLevelEntries is either empty or has two entries. I'm assuming that the first one is used for the main level and the second for the sublevel when making an order.
                            //ToDo: Should we store EntryIndex or ObjectNumber here? Example data suggests both always fall under range 0-8... Chose ObjectNumber as it always seems to be in ascending order
                            posproduct.FieldValue2 = menuItemDefinition.MenuLevelEntries.DbMenuItemMenuLevelEntry[0].MenuLevelObjNum;
                            posproduct.FieldValue3 = menuItemDefinition.MenuLevelEntries.DbMenuItemMenuLevelEntry[1].MenuLevelObjNum;
                        }

                        posproducts.Add(posproduct);
                    }
                }
            }

            return posproducts.ToArray();
        }

        private List<int> GetRevenueCenters(string revenueCentersAsString)
        {
            string[] revenueCenters = revenueCentersAsString.Split(',');

            return revenueCenters.Select(r => int.Parse(r)).ToList();
        }

        private OrderProcessingError ProcessOrderOperationalResult(string responseXml, SimphonyPosApi_OperationalResult operationalResult, Posorder posorder)
        {
            if (!operationalResult.Success)
            {
                if (operationalResult.ErrorMessage != null)
                {
                    if (operationalResult.ErrorMessage.Contains("Object reference not set to an instance of an object"))
                    {
                        posorder.ErrorMessage = "Attempted to place an order with either an unknown product or tender media or the chosen product was out of stock";
                    }
                    else
                    {
                        posorder.ErrorMessage = $"{operationalResult.ErrorMessage}\n";
                    }
                }
                else
                {
                    posorder.ErrorMessage = "No error message from POS";
                }

                return OrderProcessingError.PosErrorSaveOrderFailed;
            }

            return OrderProcessingError.None;
        }

        private PostTransactionExRequest BuildPostTransactionExRequest(Posorder posorder, int revenueCenter)
        {
            PostTransactionExRequest request = new PostTransactionExRequest();

            List<SimphonyPosApi_MenuItemDefinition> products = new List<SimphonyPosApi_MenuItemDefinition>();
            Dictionary<int, SimphonyPosApi_MenuItemDefinition> alterations = new Dictionary<int, SimphonyPosApi_MenuItemDefinition>();

            List<string> orderNotes = new List<string>();

            List<SimphonyPosApi_MenuItem> ppMenuItems = new List<SimphonyPosApi_MenuItem>();

            request.pGuestCheck = new SimphonyPosApi_GuestCheck
            {
                CheckGuestCount = posorder.GuestCount,
                CheckEmployeeObjectNum = int.Parse(employeeObjectNum),
                CheckRevenueCenterID = revenueCenter,
                CheckTableObjectNum = int.Parse(posorder.PosdeliverypointExternalId),
                PPrintJobIds = new int[] { }
            };

            request.ppMenuItems = new SimphonyPosApi_MenuItem[]{};

            request.pTmedDetail = new SimphonyPosApi_TmedDetailItemEx()
            {
                TmedObjectNum = int.Parse(posorder.PospaymentmethodExternalId)
            };

            foreach (Posorderitem orderitem in posorder.Posorderitems)
            {
                if (!string.IsNullOrWhiteSpace(orderitem.Notes))
                {
                    orderNotes.Add(orderitem.Notes);
                }

                products.Add(new SimphonyPosApi_MenuItemDefinition
                {
                    MiObjectNum = int.Parse(orderitem.FieldValue1),
                    MiReference = orderitem.Description
                });

                if (orderitem.Posalterationitems != null && orderitem.Posalterationitems.Any())
                {
                    foreach (Posalterationitem alteration in orderitem.Posalterationitems)
                    {
                        alterations.Add(int.Parse(orderitem.FieldValue1), new SimphonyPosApi_MenuItemDefinition()
                        {
                            MiObjectNum = int.Parse(alteration.ExternalPosalterationId),
                            MiReference = alteration.FieldValue1
                        });
                    }
                }
            }

            for (int i = 0; i < products.Count; i++)
            {
                ppMenuItems.Add(new SimphonyPosApi_MenuItem()
                {
                    MenuItem = products[i],
                    Condiments = alterations.Where(a => a.Key == products[i].MiObjectNum).Select(e => e.Value)
                        .ToArray()
                });
            }

            request.ppMenuItems = ppMenuItems.ToArray();
            request.ppCheckPrintLines = orderNotes.ToArray();

            return request;
        }

        private PostTransactionEx2Request BuildPostTransactionEx2Request(Posorder posorder, int revenueCenter)
        {
            PostTransactionEx2Request request = new PostTransactionEx2Request();

            List<SimphonyPosApi_MenuItemDefinitionEx> products = new List<SimphonyPosApi_MenuItemDefinitionEx>();
            Dictionary<int, SimphonyPosApi_MenuItemDefinitionEx> alterations = new Dictionary<int, SimphonyPosApi_MenuItemDefinitionEx>();

            List<string> orderNotes = new List<string>();

            request.pGuestCheck = new SimphonyPosApi_GuestCheck
            {
                CheckGuestCount = posorder.GuestCount,
                CheckEmployeeObjectNum = int.Parse(employeeObjectNum),
                CheckRevenueCenterID = revenueCenter,
                CheckTableObjectNum = int.Parse(posorder.PosdeliverypointExternalId),
                PPrintJobIds = new int[] { }
            };

            request.pTmedDetailEx2 = new SimphonyPosApi_TmedDetailItemEx2[]
            {
                new SimphonyPosApi_TmedDetailItemEx2
                {
                    TmedObjectNum = int.Parse(posorder.PospaymentmethodExternalId)
                }
            };

            foreach (Posorderitem orderitem in posorder.Posorderitems)
            {
                if (!string.IsNullOrWhiteSpace(orderitem.Notes))
                {
                    orderNotes.Add(orderitem.Notes);
                }

                products.Add(new SimphonyPosApi_MenuItemDefinitionEx
                {
                    MiObjectNum = int.Parse(orderitem.FieldValue1),
                    MiReference = orderitem.Description
                });

                if (orderitem.Posalterationitems != null && orderitem.Posalterationitems.Any())
                {
                    foreach (Posalterationitem alteration in orderitem.Posalterationitems)
                    {
                        alterations.Add(int.Parse(orderitem.FieldValue1), new SimphonyPosApi_MenuItemDefinitionEx()
                        {
                            MiObjectNum = int.Parse(alteration.ExternalPosalterationId),
                            MiReference = alteration.FieldValue1
                        });
                    }
                }
            }

            for (int i = 0; i < products.Count; i++)
            {
                request.ppMenuItemsEx[i] = new SimphonyPosApi_MenuItemEx
                {
                    Condiments = alterations.Where(a => a.Key == products[i].MiObjectNum).Select(e => e.Value).ToArray(),
                    MenuItem = products[i]
                };
            }

            request.ppCheckPrintLines = orderNotes.ToArray();

            return request;
        }

        /// <summary>
        /// Maps MenuItemDefinitions (Alterations) if the MenuItemClass has a condiment group bit set which corresponds to the bit set for the MenuItemDefinition's RequiredCondiment bit.
        /// Once we have determined this, get all MenuItemDefinitions relating to this MenuItemClass as these contain the information we need to map to a POS Alteration
        /// </summary>
        /// <param name="requiredCondimentsForProduct">The binary string indicating which condiments are required for the product</param>
        /// <returns></returns>
        private Posalteration[] GetPosalterationsForProduct(
            DbMenuItemDefinition productMenuItemDefinition,
            DbMenuItemClass menuItemClass,
            List<DbMenuItemDefinition> menuItemDefinitions,
            List<DbMenuItemClass> menuItemClasses,
            int revenueCenter)
        {
            if (!BinaryStringHasBitSet(menuItemClass.RequiredCondiments) && !BinaryStringHasBitSet(menuItemClass.AllowedCondiments))
            {
                return new Posalteration[] { };
            }

            List<Posalteration> posalterations = new List<Posalteration>();

            foreach (DbMenuItemClass alteration in menuItemClasses)
            {
                char[] alterationMemberOfCondimentBits = alteration.MemberOfCondiments.ToCharArray();
                char[] menuItemClassRequiredCondimentsBits = menuItemClass.RequiredCondiments.ToCharArray();
                char[] menuItemClassAllowedCondimentsBits = menuItemClass.AllowedCondiments.ToCharArray();

                for (int i = 0; i < MaxCondimentGroups; i++)
                {
                    if ((alterationMemberOfCondimentBits[i] == menuItemClassRequiredCondimentsBits[i] && menuItemClassRequiredCondimentsBits[i] == '1') ||
                        (alterationMemberOfCondimentBits[i] == menuItemClassAllowedCondimentsBits[i] && menuItemClassAllowedCondimentsBits[i] == '1'))
                    {
                        posalterations.Add(new Posalteration
                        {
                            ExternalId = alteration.MenuItemClassID,
                            CompanyId = revenueCenter,
                            FieldValue1 = alteration.Name.StringText,
                            RevenueCenter = revenueCenter.ToString(),
                            Posalterationoptions = GetPosalterationoptionsForProduct(alteration, menuItemDefinitions, productMenuItemDefinition.MiMasterObjNum, menuItemClassRequiredCondimentsBits[i] == '1', revenueCenter)
                        });
                    }
                }
            }

            return posalterations.GroupBy(p => p.ExternalId).Select(grp => grp.FirstOrDefault()).ToArray();
        }

        private Posalterationoption[] GetPosalterationoptionsForProduct(DbMenuItemClass alteration, List<DbMenuItemDefinition> menuItemDefinitions, string posProductExternalId, bool requiredCondiment, int revenueCenter)
        {
            var alterationoptionMenuItemDefinitions = menuItemDefinitions.Where(m => m.MenuItemClassObjNum == alteration.ObjectNumber);

            List<Posalterationoption> posalterationoptions = new List<Posalterationoption>();

            posalterationoptions.AddRange(alterationoptionMenuItemDefinitions.Select(a => new Posalterationoption
            {
                ExternalId = a.MiMasterObjNum,
                PosproductExternalId = posProductExternalId,
                PosalterationoptionId = int.Parse(a.MenuItemDefID),
                Name = a.Name1.StringText,
                RevenueCenter = revenueCenter.ToString(),
                FieldValue1 = requiredCondiment == true ? "Required" : "Optional"
            }));

            return posalterationoptions.ToArray();
        }

        private SimphonyPosApi_ConfigInfoRequest GetSimphonyRequest(int[] configurationItemTypes, int revenueCenter)
        {
            return new SimphonyPosApi_ConfigInfoRequest
            {
                EmployeeObjectNumber = int.Parse(employeeObjectNum),
                RVCObjectNumber = revenueCenter,
                ConfigurationInfo = GetConfigurationItemTypes(configurationItemTypes)
            };
        }

        private SimphonyPosApi_ConfigInfo[] GetConfigurationItemTypes(int[] configurationItemTypes)
        {
            IList<SimphonyPosApi_ConfigInfo> result = new List<SimphonyPosApi_ConfigInfo>();

            foreach (int configurationItemType in configurationItemTypes.ToList())
            {
                switch (configurationItemType)
                {
                    case 1:
                        result.Add(new SimphonyPosApi_ConfigInfo { ConfigurationInfoTypeID = EConfigurationInfoType.MENUITEMDEFINITIONS });
                        break;
                    case 2:
                        result.Add(new SimphonyPosApi_ConfigInfo { ConfigurationInfoTypeID = EConfigurationInfoType.MENUITEMPRICE });
                        break;
                    case 3:
                        result.Add(new SimphonyPosApi_ConfigInfo { ConfigurationInfoTypeID = EConfigurationInfoType.MENUITEMCLASS });
                        break;
                    case 8:
                        result.Add(new SimphonyPosApi_ConfigInfo { ConfigurationInfoTypeID = EConfigurationInfoType.FAMILYGROUP });
                        break;
                    case 9:
                        result.Add(new SimphonyPosApi_ConfigInfo { ConfigurationInfoTypeID = EConfigurationInfoType.MAJORGROUP });
                        break;
                    case 18:
                        result.Add(new SimphonyPosApi_ConfigInfo { ConfigurationInfoTypeID = EConfigurationInfoType.TABLES });
                        break;

                }
            }

            return result.ToArray();
        }

        private List<Tables.DbDiningTable> GetDiningTables(int revenueCenter)
        {
            SimphonyPosApi_ConfigInfoRequest request = GetSimphonyRequest(new[] { 18 }, revenueCenter);

            var response = new SimphonyPosApi_ConfigInfoResponse();

            testLab.GetConfigurationInfoEx(request, ref response);

            if (string.IsNullOrWhiteSpace(response.MenuItemClass))
            {
                return new List<Tables.DbDiningTable>();
            }

            return MapXmlToEntity<Tables.ArrayOfDbDiningTable>(response.Tables).DbDiningTable;
        }

        private List<MajorGroups.DbMajorGroup> GetMajorGroups(int revenueCenter)
        {
            SimphonyPosApi_ConfigInfoRequest request = GetSimphonyRequest(new[] { 9 }, revenueCenter);

            var response = new SimphonyPosApi_ConfigInfoResponse();

            testLab.GetConfigurationInfoEx(request, ref response);

            if (string.IsNullOrWhiteSpace(response.MenuItemClass))
            {
                return new List<MajorGroups.DbMajorGroup>();
            }

            return MapXmlToEntity<MajorGroups.ArrayOfDbMajorGroup>(response.MajorGroup).DbMajorGroup;
        }

        private List<FamilyGroups.DbFamilyGroup> GetFamilyGroups(int revenueCenter)
        {
            SimphonyPosApi_ConfigInfoRequest request = GetSimphonyRequest(new[] { 8 }, revenueCenter);

            var response = new SimphonyPosApi_ConfigInfoResponse();

            testLab.GetConfigurationInfoEx(request, ref response);

            if (string.IsNullOrWhiteSpace(response.MenuItemClass))
            {
                return new List<FamilyGroups.DbFamilyGroup>();
            }

            return MapXmlToEntity<FamilyGroups.ArrayOfDbFamilyGroup>(response.FamilyGroups).DbFamilyGroup;
        }

        private List<DbMenuItemDefinition> GetMenuItemDefinitions(int revenueCenter)
        {
            SimphonyPosApi_ConfigInfoRequest request = GetSimphonyRequest(new[] { 1 }, revenueCenter);

            var response = new SimphonyPosApi_ConfigInfoResponse();

            testLab.GetConfigurationInfoEx(request, ref response);

            if (string.IsNullOrWhiteSpace(response.MenuItemDefinitions))
            {
                return new List<DbMenuItemDefinition>();
            }

            return MapXmlToEntity<ArrayOfDbMenuItemDefinition>(response.MenuItemDefinitions).DbMenuItemDefinition;
        }

        private List<DbMenuItemClass> GetMenuItemClasses(int revenueCenter)
        {
            SimphonyPosApi_ConfigInfoRequest request = GetSimphonyRequest(new[] { 3 }, revenueCenter);

            var response = new SimphonyPosApi_ConfigInfoResponse();

            testLab.GetConfigurationInfoEx(request, ref response);

            if (string.IsNullOrWhiteSpace(response.MenuItemClass))
            {
                return new List<DbMenuItemClass>();
            }

            return MapXmlToEntity<ArrayOfDbMenuItemClass>(response.MenuItemClass).DbMenuItemClass;
        }

        private List<DbMenuItemPrice> GetMenuItemPrices(int revenueCenter)
        {
            SimphonyPosApi_ConfigInfoRequest request = GetSimphonyRequest(new[] { 2 }, revenueCenter);

            var response = new SimphonyPosApi_ConfigInfoResponse();

            testLab.GetConfigurationInfoEx(request, ref response);

            if (string.IsNullOrWhiteSpace(response.MenuItemPrice))
            {
                return new List<DbMenuItemPrice>();
            }

            return MapXmlToEntity<ArrayOfDbMenuItemPrice>(response.MenuItemPrice).DbMenuItemPrice;
        }

        //ToDo: Making an assumption that a Product MenuItemDefinition can only ever have two associated MenuItemPrices, one with a TaxClassObjNum of 0 and one of non 0. Potential bug
        private decimal GetPriceForProduct(string menuItemDefinitionId, string sequenceNumber, IEnumerable<DbMenuItemPrice> menuItemPrices)
        {
            string price = "0.00";

            //In certain cases, a menu item definition has multiple prices. Only the menu level index and sequence number fields differ between the two. How do we determine which price to use? Using sequence number but not sure if that's correct
            IEnumerable<DbMenuItemPrice> pricesForProduct = menuItemPrices.Where(p => p.MenuItemDefID == menuItemDefinitionId && p.TaxClassObjNum == "0").ToList();

            if (pricesForProduct.Count() == 1)
            {
                price = pricesForProduct.Single().Price;
            }
            else if (pricesForProduct.Count() > 1)
            {
                price = pricesForProduct.Single(p => p.SequenceNum == sequenceNumber).Price;
            }

            return Math.Round(decimal.Parse(price, CultureInfo.InvariantCulture), 2,
                MidpointRounding.AwayFromZero);
        }

        private bool BinaryStringHasBitSet(string binaryString)
        {
            List<char> binaryAsCharList = binaryString.ToCharArray().ToList();

            return binaryAsCharList.Any(b => b == '1');
        }

        /// <summary>
        /// Checks if the MemberOfCondiments binary value has the same bit set as the RequiredCondimentsBinary. We use this to map Alterations to a Product
        /// </summary>
        /// <param name="memberOfCondimentsBinary">MemberOfCondiments binary string from the MenuItemClass (Alteration)</param>
        /// <param name="requiredOrAllowedCondimentsBinary">RequiredCondiments Or AllowedCondiments binary string from the MenuItemDefinition (Product)</param>
        /// <returns></returns>
        private bool MenuItemClassIsAnAlterationForProduct(string memberOfCondimentsBinary, string requiredOrAllowedCondimentsBinary)
        {
            char[] memberOfCondimentsBinaryArray = memberOfCondimentsBinary.ToCharArray();
            char[] requiredOrAllowedCondimentsBinaryArray = requiredOrAllowedCondimentsBinary.ToCharArray();

            for (int i = 0; i < MaxCondimentGroups; i++)
            {
                if (memberOfCondimentsBinaryArray[i] == '1')
                {
                    if (memberOfCondimentsBinaryArray[i] == requiredOrAllowedCondimentsBinaryArray[i])
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private T MapXmlToEntity<T>(string xml) where T : class
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));

            using (StringReader sr = new StringReader(xml))
            {
                return (T)xmlSerializer.Deserialize(sr);
            }
        }
    }
}
