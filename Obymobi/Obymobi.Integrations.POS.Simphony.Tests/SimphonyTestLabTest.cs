﻿using System;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using Obymobi.Enums;
using Obymobi.Integrations.POS.Simphony.Models.Responses;
using Obymobi.Integrations.POS.Simphony.simphonyposapiweb;
using Obymobi.Logic.Model;
using Xunit;

namespace Obymobi.Integrations.POS.Simphony.Tests
{
    public class SimphonyTestLabTests
    {
        private readonly SimphonyPosAPIWeb testLab;

        private SimphonyConnectorMock mock;

        private const int MenuItemDefinitionConfigTypeId = 1;
        private const int MenuItemPriceConfigTypeId = 2;
        private const int MenuItemClassConfigTypeId = 3;
        private const int FamilyGroupConfigTypeId = 8;
        private const int MajorGroupConfigTypeId = 9;
        private const int DiningTableConfigTypeId = 18;

        private const string WebServiceUrl = "http://192.168.58.39:8080/egateway/simphonyposapiweb.asmx";
        private const int RevenueCenterId = 11;
        private const int EmployeeNumber = 4;
        private const bool UseOldPostMethod = true;
        private const bool OnlySyncAvailableProducts = false;

        private const int DeliverypointId = 5;
        private const int PaymentMethodId = 101;
        private const int PaymentMethodWithTipsId = 907;

        private const string RevenueCenters = "11";


        public SimphonyTestLabTests()
        {
            this.testLab = new SimphonyPosAPIWeb
            {
                Url = WebServiceUrl
            };

            this.mock = new SimphonyConnectorMock(WebServiceUrl, EmployeeNumber.ToString(), RevenueCenters, UseOldPostMethod,
                OnlySyncAvailableProducts);
        }

        #region GetConfigurationInfo

        [Fact]
        public void MenuItemDefinitions()
        {
            SimphonyPosApi_ConfigInfoResponse response = new SimphonyPosApi_ConfigInfoResponse();

            this.testLab.GetConfigurationInfo(string.Empty, EmployeeNumber, new int[] { MenuItemDefinitionConfigTypeId }, RevenueCenterId, ref response);

            Assert.NotNull(response);

            ArrayOfDbMenuItemDefinition mappedResponse = MapXmlToEntity<ArrayOfDbMenuItemDefinition>(response.MenuItemDefinitions);

            Assert.NotNull(mappedResponse.DbMenuItemDefinition);
            Assert.NotEmpty(mappedResponse.DbMenuItemDefinition);
        }

        [Fact]
        public void MenuItemDefinitions_MultipleRevenueCenters()
        {
            this.mock = new SimphonyConnectorMock(WebServiceUrl, EmployeeNumber.ToString(), "11, 21, 31", UseOldPostMethod,
                OnlySyncAvailableProducts);

            Posproduct[] products = this.mock.GetPosproducts();

            Assert.NotNull(products);
            Assert.NotEmpty(products);
        }

        [Fact]
        public void RequiredCondiments()
        {
            Posproduct[] products = this.mock.GetPosproducts();

            Assert.NotNull(products);
            Assert.NotEmpty(products);

            Posproduct ribeye = products.Single(p => p.ExternalId == "61427");
            Assert.NotEmpty(ribeye.Posalterations);
        }

        [Fact]
        public void OutOfMenuItems()
        {
            SimphonyPosApi_ConfigInfoResponse response = new SimphonyPosApi_ConfigInfoResponse();

            this.testLab.GetConfigurationInfo(string.Empty, EmployeeNumber, new int[] { MenuItemDefinitionConfigTypeId }, RevenueCenterId, ref response);

            Assert.NotNull(response);

            ArrayOfDbMenuItemDefinition mappedResponse = MapXmlToEntity<ArrayOfDbMenuItemDefinition>(response.MenuItemDefinitions);

            DbMenuItemDefinition outOfStockMenuItemDefintion =
                mappedResponse.DbMenuItemDefinition.FirstOrDefault(d => d.OutOfMenuItem == "true");

            Assert.NotNull(outOfStockMenuItemDefintion);

            Posproduct[] products = this.mock.GetPosproducts();

            Assert.NotNull(products);
            Assert.NotEmpty(products);

            Posproduct outOfStockPosProduct = products.SingleOrDefault(p => p.ExternalId == outOfStockMenuItemDefintion.MenuItemDefID);

            Assert.NotNull(outOfStockPosProduct);
        }

        [Fact]
        public void MenuItemPrices()
        {
            SimphonyPosApi_ConfigInfoResponse response = new SimphonyPosApi_ConfigInfoResponse();

            this.testLab.GetConfigurationInfo(string.Empty, EmployeeNumber, new int[] { MenuItemPriceConfigTypeId }, RevenueCenterId, ref response);

            Assert.NotNull(response);

            ArrayOfDbMenuItemPrice mappedResponse = MapXmlToEntity<ArrayOfDbMenuItemPrice>(response.MenuItemPrice);

            Assert.NotNull(mappedResponse.DbMenuItemPrice);
            Assert.NotEmpty(mappedResponse.DbMenuItemPrice);
        }

        [Fact]
        public void MenuItemClasses()
        {
            SimphonyPosApi_ConfigInfoResponse response = new SimphonyPosApi_ConfigInfoResponse();

            this.testLab.GetConfigurationInfo(string.Empty, EmployeeNumber, new int[] { MenuItemClassConfigTypeId }, RevenueCenterId, ref response);

            Assert.NotNull(response);

            ArrayOfDbMenuItemClass mappedResponse = MapXmlToEntity<ArrayOfDbMenuItemClass>(response.MenuItemClass);

            Assert.NotNull(mappedResponse.DbMenuItemClass);
            Assert.NotEmpty(mappedResponse.DbMenuItemClass);
        }

        [Fact]
        public void FamilyGroups()
        {
            SimphonyPosApi_ConfigInfoResponse response = new SimphonyPosApi_ConfigInfoResponse();

            this.testLab.GetConfigurationInfo(string.Empty, EmployeeNumber, new int[] { FamilyGroupConfigTypeId }, RevenueCenterId, ref response);

            Assert.NotNull(response);

            FamilyGroups.ArrayOfDbFamilyGroup mappedResponse = MapXmlToEntity<FamilyGroups.ArrayOfDbFamilyGroup>(response.FamilyGroups);

            Assert.NotNull(mappedResponse.DbFamilyGroup);
            Assert.NotEmpty(mappedResponse.DbFamilyGroup);
        }

        [Fact]
        public void MajorGroups()
        {
            SimphonyPosApi_ConfigInfoResponse response = new SimphonyPosApi_ConfigInfoResponse();

            this.testLab.GetConfigurationInfo(string.Empty, EmployeeNumber, new int[] { MajorGroupConfigTypeId }, RevenueCenterId, ref response);

            Assert.NotNull(response);

            MajorGroups.ArrayOfDbMajorGroup mappedResponse = MapXmlToEntity<MajorGroups.ArrayOfDbMajorGroup>(response.MajorGroup);

            Assert.NotNull(mappedResponse.DbMajorGroup);
            Assert.NotEmpty(mappedResponse.DbMajorGroup);
        }

        [Fact]
        public void DiningTables()
        {
            SimphonyPosApi_ConfigInfoResponse response = new SimphonyPosApi_ConfigInfoResponse();

            this.testLab.GetConfigurationInfo(string.Empty, EmployeeNumber, new int[] { DiningTableConfigTypeId }, RevenueCenterId, ref response);

            Assert.NotNull(response);

            Tables.ArrayOfDbDiningTable mappedResponse = MapXmlToEntity<Tables.ArrayOfDbDiningTable>(response.Tables);

            Assert.NotNull(mappedResponse.DbDiningTable);
            Assert.NotEmpty(mappedResponse.DbDiningTable);
        }

        #endregion

        #region PostTransactionEx

        [Fact]
        public void PostTransactionEx_BeginCheckAddItemServiceTotal_OpenCheck()
        {
            SimphonyPosApi_GuestCheck check = new SimphonyPosApi_GuestCheck()
            {
                CheckGuestCount = 2,
                CheckDateToFire = DateTime.Now,
                CheckEmployeeObjectNum = 4,
                CheckOrderType = 3,
                CheckRevenueCenterID = 11,
                CheckTableObjectNum = 4,
                PCheckInfoLines = new string[] { "These are the order notes", ".. and so is this", "even more notes!" },
                PPrintJobIds = new int[] { },
                EventObjectNum = 0
            };

            SimphonyPosApi_MenuItem[] refMenuItems = new SimphonyPosApi_MenuItem[]
            {
                new SimphonyPosApi_MenuItem
                {
                    Condiments = new SimphonyPosApi_MenuItemDefinition[] {},
                    MenuItem = new SimphonyPosApi_MenuItemDefinition()
                    {

                        MiMenuLevel = 0,
                        MiObjectNum = 103000006, //MI Master from MenuItemDef
                        //MiReference = "TestMangoPie",
                        MiSubLevel = 0
                    }
                }
            };

            SimphonyPosApi_ComboMeal[] comboMeals = new SimphonyPosApi_ComboMeal[] { };
            SimphonyPosApi_SvcCharge serviceCharge = new SimphonyPosApi_SvcCharge() { };
            SimphonyPosApi_Discount discount = new SimphonyPosApi_Discount() { };

            SimphonyPosApi_TmedDetailItemEx tMed = new SimphonyPosApi_TmedDetailItemEx()
            {
                TmedEPayment = new SimphonyPosApi_EPayment(),
                TmedObjectNum = 901
            };

            SimphonyPosApi_TotalsResponse totals = new SimphonyPosApi_TotalsResponse();
            string[] checkPrintLines = new string[] { };
            string[] voucherOutput = new string[] { };

            this.testLab.PostTransactionEx("OPN", ref check, ref refMenuItems, ref comboMeals, ref serviceCharge, ref discount, ref tMed, ref totals, ref checkPrintLines, ref voucherOutput);

            Assert.NotNull(totals.OperationalResult);
            Assert.True(totals.OperationalResult.Success);
        }

        [Fact]
        public void PostTransactionEx_BeginCheckAddItemServiceTotal_ClosedCheck()
        {
            SimphonyPosApi_GuestCheck check = new SimphonyPosApi_GuestCheck()
            {
                CheckGuestCount = 3,
                CheckDateToFire = DateTime.Now,
                CheckEmployeeObjectNum = 4,
                CheckOrderType = 1,
                CheckRevenueCenterID = 11,
                CheckTableObjectNum = 5,
                PCheckInfoLines = new string[] { },
                PPrintJobIds = new int[] { },
                EventObjectNum = 0
            };

            SimphonyPosApi_MenuItem[] refMenuItems = new SimphonyPosApi_MenuItem[]
            {
                new SimphonyPosApi_MenuItem
                {
                    Condiments = new SimphonyPosApi_MenuItemDefinition[] {},
                    MenuItem = new SimphonyPosApi_MenuItemDefinition()
                    {
                        ItemDiscount = new SimphonyPosApi_Discount()
                        {
                            DiscAmountOrPercent = "0.0M"
                        },
                        MiMenuLevel = 0,
                        MiObjectNum = 103000006, //MI Master from MenuItemDef
                        MiReference = "TestMangoPie",
                        MiSubLevel = 0
                    }
                }
            };

            SimphonyPosApi_ComboMeal[] comboMeals = new SimphonyPosApi_ComboMeal[] { };

            SimphonyPosApi_SvcCharge serviceCharge = new SimphonyPosApi_SvcCharge()
            {
                SvcChgAmountOrPercent = "1.2"
            };

            SimphonyPosApi_Discount discount = new SimphonyPosApi_Discount()
            {
                DiscAmountOrPercent = "0.0"
            };

            SimphonyPosApi_TmedDetailItemEx tMed = new SimphonyPosApi_TmedDetailItemEx()
            {
                TmedEPayment = new SimphonyPosApi_EPayment(),
                TmedObjectNum = 101
            };

            SimphonyPosApi_TotalsResponse totals = new SimphonyPosApi_TotalsResponse();
            string[] checkPrintLines = new string[] { };
            string[] voucherOutput = new string[] { };

            this.testLab.PostTransactionEx("OPN", ref check, ref refMenuItems, ref comboMeals, ref serviceCharge, ref discount, ref tMed, ref totals, ref checkPrintLines, ref voucherOutput);

            Assert.NotNull(totals.OperationalResult);
            Assert.True(totals.OperationalResult.Success);
        }

        [Fact]
        public void PostTransactionEx_BeginCheckAddItemWithCondimentServiceTotal()
        {
            Posorder posorder = new Posorder() { Posorderitems = new Posorderitem[] { }, GuestCount = 2 };

            SimphonyPosApi_GuestCheck check = new SimphonyPosApi_GuestCheck()
            {
                CheckGuestCount = posorder.GuestCount,
                CheckEmployeeObjectNum = 4,
                CheckRevenueCenterID = 21,
                CheckTableObjectNum = 9,
                PCheckInfoLines = new string[] { },
                PPrintJobIds = new int[] { },
            };

            SimphonyPosApi_MenuItem[] refMenuItems = new SimphonyPosApi_MenuItem[]
            {
                new SimphonyPosApi_MenuItem
                {
                    Condiments = new SimphonyPosApi_MenuItemDefinition[]
                    {
                        new SimphonyPosApi_MenuItemDefinition()
                        {
                            MiObjectNum = 199006003,
                            MiReference = "TestBlueCheese",
                        }
                    },
                    MenuItem = new SimphonyPosApi_MenuItemDefinition()
                    {
                        MiObjectNum = 101000001, //MI Master from MenuItemDef
                        MiReference = "TestFries",
                    }
                }
            };

            SimphonyPosApi_ComboMeal[] comboMeals = new SimphonyPosApi_ComboMeal[] { };

            SimphonyPosApi_SvcCharge serviceCharge = new SimphonyPosApi_SvcCharge()
            {
                SvcChgAmountOrPercent = "1.2"
            };

            SimphonyPosApi_Discount discount = new SimphonyPosApi_Discount()
            {
            };

            SimphonyPosApi_TmedDetailItemEx tMed = new SimphonyPosApi_TmedDetailItemEx()
            {
                TmedEPayment = new SimphonyPosApi_EPayment(),
                TmedObjectNum = 101
            };

            SimphonyPosApi_TotalsResponse totals = new SimphonyPosApi_TotalsResponse();
            string[] checkPrintLines = new string[] { };
            string[] voucherOutput = new string[] { };

            this.testLab.PostTransactionEx("OPN", ref check, ref refMenuItems, ref comboMeals, ref serviceCharge, ref discount, ref tMed, ref totals, ref checkPrintLines, ref voucherOutput);

            Assert.NotNull(totals.OperationalResult);
            Assert.True(totals.OperationalResult.Success);
        }

        #endregion

        #region PosTransactionEx2

        [Fact]
        public void PostTransactionEx2_BeginCheckAddItemServiceTotal()
        {
            SimphonyPosApi_GuestCheck check = new SimphonyPosApi_GuestCheck()
            {
                CheckDateToFire = DateTime.Now,
                CheckEmployeeObjectNum = 4,
                CheckOrderType = 1,
                CheckRevenueCenterID = 21,
                CheckTableObjectNum = 9,
                PCheckInfoLines = new string[] { },
                PPrintJobIds = new int[] { },
                EventObjectNum = 0
            };

            SimphonyPosApi_MenuItemEx[] refMenuItems = new SimphonyPosApi_MenuItemEx[]
            {
                new SimphonyPosApi_MenuItemEx
                {
                    Condiments = new SimphonyPosApi_MenuItemDefinitionEx[] {},
                    MenuItem = new SimphonyPosApi_MenuItemDefinitionEx()
                    {
                        ItemDiscount = new SimphonyPosApi_DiscountEx[]
                        {
                            new SimphonyPosApi_DiscountEx
                            {
                                DiscAmountOrPercent = "0.0M"
                            }
                        },
                        MiMenuLevel = 0,
                        MiObjectNum = 101000001, //MI Master from MenuItemDef
                        MiReference = "TestFries",
                        MiSubLevel = 0
                    }
                }
            };

            SimphonyPosApi_ComboMealEx[] comboMeals = new SimphonyPosApi_ComboMealEx[] { };

            SimphonyPosApi_SvcChargeEx serviceCharge = new SimphonyPosApi_SvcChargeEx()
            {
                SvcChgAmountOrPercent = "1.2"
            };

            SimphonyPosApi_DiscountEx[] discount = new SimphonyPosApi_DiscountEx[]
            {
                new SimphonyPosApi_DiscountEx()
                {
                    DiscAmountOrPercent = "0.0"
                }
            };

            SimphonyPosApi_TmedDetailItemEx2[] tMed = new SimphonyPosApi_TmedDetailItemEx2[]
            {
                new SimphonyPosApi_TmedDetailItemEx2()
                {
                    TmedEPayment = new SimphonyPosApi_EPaymentEx(),
                    TmedObjectNum = 101
                }
            };

            SimphonyPosApi_TotalsResponseEx totals = new SimphonyPosApi_TotalsResponseEx();
            string[] checkPrintLines = new string[] { };
            string[] voucherOutput = new string[] { };

            this.testLab.PostTransactionEx2(ref check, ref refMenuItems, ref comboMeals, ref serviceCharge, ref discount, ref tMed, ref totals, ref checkPrintLines, ref voucherOutput, new SimphonyPosApi_Extensibility[] { });

            Assert.NotNull(totals.OperationalResult);
            Assert.True(totals.OperationalResult.Success);
        }

        [Fact]
        public void PostTransactionEx2_BeginCheckAddItemWithCondimentServiceTotal()
        {
            SimphonyPosApi_GuestCheck check = new SimphonyPosApi_GuestCheck()
            {
                CheckDateToFire = DateTime.Now,
                CheckEmployeeObjectNum = 4,
                CheckOrderType = 1,
                CheckRevenueCenterID = 11,
                CheckTableObjectNum = 5,
                PCheckInfoLines = new string[] { },
                PPrintJobIds = new int[] { },
                EventObjectNum = 0
            };

            SimphonyPosApi_MenuItemEx[] refMenuItems = new SimphonyPosApi_MenuItemEx[]
            {
                new SimphonyPosApi_MenuItemEx
                {
                    Condiments = new SimphonyPosApi_MenuItemDefinitionEx[]
                    {
                        new SimphonyPosApi_MenuItemDefinitionEx()
                        {
                            MiObjectNum = 199006003,
                            MiReference = "TestBlueCheese",
                        }
                    },
                    MenuItem = new SimphonyPosApi_MenuItemDefinitionEx()
                    {
                        ItemDiscount = new SimphonyPosApi_DiscountEx[]
                        {
                            new SimphonyPosApi_DiscountEx
                            {
                                DiscAmountOrPercent = "0.0M"
                            }
                        },
                        MiMenuLevel = 0,
                        MiObjectNum = 101000001, //MI Master from MenuItemDef
                        MiReference = "TestFries",
                        MiSubLevel = 0
                    }
                }
            };

            SimphonyPosApi_ComboMealEx[] comboMeals = new SimphonyPosApi_ComboMealEx[] { };

            SimphonyPosApi_SvcChargeEx serviceCharge = new SimphonyPosApi_SvcChargeEx()
            {
                SvcChgAmountOrPercent = "1.2"
            };

            SimphonyPosApi_DiscountEx[] discount = new SimphonyPosApi_DiscountEx[]
            {
                new SimphonyPosApi_DiscountEx()
                {
                    DiscAmountOrPercent = "0.0"
                }
            };

            SimphonyPosApi_TmedDetailItemEx2[] tMed = new SimphonyPosApi_TmedDetailItemEx2[]
            {
                new SimphonyPosApi_TmedDetailItemEx2()
                {
                    TmedEPayment = new SimphonyPosApi_EPaymentEx(),
                    TmedObjectNum = 101
                }
            };

            SimphonyPosApi_TotalsResponseEx totals = new SimphonyPosApi_TotalsResponseEx();
            string[] checkPrintLines = new string[] { };
            string[] voucherOutput = new string[] { };

            this.testLab.PostTransactionEx2(ref check, ref refMenuItems, ref comboMeals, ref serviceCharge, ref discount, ref tMed, ref totals, ref checkPrintLines, ref voucherOutput, new SimphonyPosApi_Extensibility[] { });

            Assert.NotNull(totals.OperationalResult);
            Assert.True(totals.OperationalResult.Success);
        }

        [Fact]
        public void PostTransactionEx2_BeginCheckAddItemWithCondiments_AndTips_ServiceTotal()
        {
            SimphonyPosApi_GuestCheck check = new SimphonyPosApi_GuestCheck()
            {
                CheckDateToFire = DateTime.Now,
                CheckEmployeeObjectNum = 4,
                CheckOrderType = 1,
                CheckRevenueCenterID = 11,
                CheckTableObjectNum = 5,
                PCheckInfoLines = new string[] { },
                PPrintJobIds = new int[] { },
                EventObjectNum = 0
            };

            SimphonyPosApi_MenuItemEx[] refMenuItems = new SimphonyPosApi_MenuItemEx[]
            {
                new SimphonyPosApi_MenuItemEx
                {
                    Condiments = new SimphonyPosApi_MenuItemDefinitionEx[]
                    {
                        new SimphonyPosApi_MenuItemDefinitionEx()
                        {
                            MiObjectNum = 199006003,
                            MiReference = "TestBlueCheese",
                        }
                    },
                    MenuItem = new SimphonyPosApi_MenuItemDefinitionEx()
                    {
                        ItemDiscount = new SimphonyPosApi_DiscountEx[]
                        {
                            new SimphonyPosApi_DiscountEx
                            {
                                DiscAmountOrPercent = "0.0M"
                            }
                        },
                        MiMenuLevel = 0,
                        MiObjectNum = 101000001, //MI Master from MenuItemDef
                        MiReference = "TestFries",
                        MiSubLevel = 0
                    }
                }
            };

            SimphonyPosApi_ComboMealEx[] comboMeals = new SimphonyPosApi_ComboMealEx[] { };

            SimphonyPosApi_SvcChargeEx serviceCharge = new SimphonyPosApi_SvcChargeEx()
            {
                SvcChgAmountOrPercent = "10.00",
                SvcChgObjectNum = 102
            };

            SimphonyPosApi_DiscountEx[] discount = new SimphonyPosApi_DiscountEx[]
            {
                new SimphonyPosApi_DiscountEx()
                {
                    DiscAmountOrPercent = "0.0"
                }
            };

            SimphonyPosApi_TmedDetailItemEx2[] tMed = new SimphonyPosApi_TmedDetailItemEx2[]
            {
                new SimphonyPosApi_TmedDetailItemEx2()
                {
                    TmedEPayment = new SimphonyPosApi_EPaymentEx(),
                    TmedObjectNum = PaymentMethodWithTipsId
                }
            };

            SimphonyPosApi_TotalsResponseEx totals = new SimphonyPosApi_TotalsResponseEx();
            string[] checkPrintLines = new string[] { };
            string[] voucherOutput = new string[] { };

            this.testLab.PostTransactionEx2(ref check, ref refMenuItems, ref comboMeals, ref serviceCharge, ref discount, ref tMed, ref totals, ref checkPrintLines, ref voucherOutput, new SimphonyPosApi_Extensibility[] { });

            Assert.NotNull(totals.OperationalResult);
            Assert.True(totals.OperationalResult.Success);
            Assert.Equal(serviceCharge.SvcChgAmountOrPercent, totals.TotalsOtherTotals);
        }

        #endregion

        #region Validation

        [Fact]
        public void ApiOffline()
        {
            this.mock = new SimphonyConnectorMock("http://192.168.57.39:8080/egateway/simphonyposapiweb.asmx", EmployeeNumber.ToString(), RevenueCenters, UseOldPostMethod,
                OnlySyncAvailableProducts);

            Posorder order = new Posorder()
            {
                PosdeliverypointExternalId = DeliverypointId.ToString(),
                PospaymentmethodExternalId = PaymentMethodId.ToString(),
                Posorderitems = new Posorderitem[]
                {
                    new Posorderitem
                    {
                        FieldValue1 = "106000001",
                        Description = "Ribeye",
                    }
                }
            };

            OrderProcessingError response = this.mock.SaveOrder(order);

            Assert.Equal(OrderProcessingError.PosErrorConnectivityProblem, response);
            Assert.Equal("Unable to connect to the remote server", order.ErrorMessage);
        }

        [Fact]
        public void InvalidMenuItemOrdered()
        {
            Posorder order = new Posorder()
            {
                PosdeliverypointExternalId = DeliverypointId.ToString(),
                PospaymentmethodExternalId = PaymentMethodId.ToString(),
                Posorderitems = new Posorderitem[]
                {
                    new Posorderitem
                    {
                        FieldValue1 = "1",
                        Description = "Ribeye",
                    }
                }
            };

            OrderProcessingError response = this.mock.SaveOrder(order);

            Assert.Equal(OrderProcessingError.PosErrorSaveOrderFailed, response);
            Assert.Equal("Attempted to place an order with either an unknown product or tender media or the chosen product was out of stock", order.ErrorMessage);
        }

        [Fact]
        public void OutOfStockMenuItemOrdered()
        {
            Posorder order = new Posorder()
            {
                PosdeliverypointExternalId = DeliverypointId.ToString(),
                PospaymentmethodExternalId = PaymentMethodId.ToString(),
                Posorderitems = new Posorderitem[]
                {
                    new Posorderitem
                    {
                        FieldValue1 = "102000001",
                        Description = "OystersHalfDozen",
                    }
                }
            };

            OrderProcessingError response = this.mock.SaveOrder(order);

            Assert.Equal(OrderProcessingError.PosErrorSaveOrderFailed, response);
            Assert.Equal("Attempted to place an order with either an unknown product or tender media or the chosen product was out of stock", order.ErrorMessage);
        }

        [Fact]
        public void InvalidTenderMediaApplied()
        {
            Posorder order = new Posorder()
            {
                PosdeliverypointExternalId = DeliverypointId.ToString(),
                PospaymentmethodExternalId = "1",
                Posorderitems = new Posorderitem[]
                {
                    new Posorderitem
                    {
                        FieldValue1 = "106000001",
                        Description = "Ribeye",
                    }
                }
            };

            OrderProcessingError response = this.mock.SaveOrder(order);

            Assert.Equal(OrderProcessingError.PosErrorSaveOrderFailed, response);
            Assert.Equal("Attempted to place an order with either an unknown product or tender media or the chosen product was out of stock", order.ErrorMessage);
        }

        #endregion


        #region Helpers

        private T MapXmlToEntity<T>(string xml) where T : class
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));

            using (StringReader sr = new StringReader(xml))
            {
                return (T)xmlSerializer.Deserialize(sr);
            }
        }

        #endregion
    }
}
