﻿using Obymobi.Logic.Model;
using System.Linq;
using Xunit;

namespace Obymobi.Integrations.POS.Simphony.Tests
{
    public class SimphonyConnectorMockTests
    {
        [Fact]
        public void GetPosProducts_MapsPosProductsCorrectly()
        {
            var mock = new SimphonyConnectorMock("http://192.168.58.39:8080/egateway/simphonyposapiweb.asmx", "4", "11", false, false);
            var posProducts = mock.GetPosproducts();

            Assert.NotNull(posProducts);
            Assert.NotEmpty(posProducts);

            var productsWithAlterations = posProducts.Where(p => p.Posalterations.Any());

            Posproduct beefBurger = posProducts.SingleOrDefault(p => p.ExternalId == "61465");

            Assert.NotNull(beefBurger);

            Assert.NotNull(beefBurger.Posalterations);
            Assert.NotEmpty(beefBurger.Posalterations);

            Assert.NotNull(beefBurger.Posalterations.First().Posalterationoptions);
            Assert.NotEmpty(beefBurger.Posalterations.First().Posalterationoptions);
        }
    }
}
