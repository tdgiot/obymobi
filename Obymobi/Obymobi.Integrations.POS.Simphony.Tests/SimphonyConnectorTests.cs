﻿using Obymobi.Integrations.POS.Simphony.Models;
using System;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using Xunit;

namespace Obymobi.Integrations.POS.Simphony.Tests
{
    public class SimphonyConnectorTests
    {
        [Fact]
        public void GetPoscategories_MapsCorrectly()
        {
            SimphonyGetConfigurationInfoResponse xmlResult = MapXml();

            Assert.NotNull(xmlResult);
            Assert.NotNull(xmlResult.ArrayOfConfigInfoResponse.SingleOrDefault(c => c.ConfigInfoType.EConfigurationInfoType == "MAJORGROUPS"));
            Assert.NotNull(xmlResult.ArrayOfConfigInfoResponse.SingleOrDefault(c => c.ConfigInfoType.EConfigurationInfoType == "FAMILYGROUPS"));

            ConfigInfoResponse majorGroupsResponse = xmlResult.ArrayOfConfigInfoResponse.Single(c => c.ConfigInfoType.EConfigurationInfoType == "MAJORGROUPS");
            Assert.NotNull(majorGroupsResponse);
            Assert.NotNull(majorGroupsResponse.MajorGroups);
            Assert.NotNull(majorGroupsResponse.MajorGroups.ArrayOfDbMajorGroup);

            ConfigInfoResponse familyGroupsResponse = xmlResult.ArrayOfConfigInfoResponse.Single(c => c.ConfigInfoType.EConfigurationInfoType == "FAMILYGROUPS");
            Assert.NotNull(familyGroupsResponse);
            Assert.NotNull(familyGroupsResponse.FamilyGroups);
            Assert.NotNull(familyGroupsResponse.FamilyGroups.ArrayOfDbFamilyGroup);

            SimphonyConnectorMock sut = new SimphonyConnectorMock(xmlResult);

            var result = sut.GetPoscategories();

            Assert.Equal(4, result.Length);

            Assert.Equal("Food", result[0].Name);
            Assert.Equal(72, result[0].PoscategoryId);
            Assert.Equal(689, result[0].CompanyId);
            Assert.Equal("1", result[0].ExternalId);

            Assert.Equal("Drink", result[1].Name);
            Assert.Equal(73, result[1].PoscategoryId);
            Assert.Equal(689, result[1].CompanyId);
            Assert.Equal("2", result[1].ExternalId);

            Assert.Equal("Mains", result[2].Name);
            Assert.Equal(237, result[2].PoscategoryId);
            Assert.Equal(700, result[2].CompanyId);
            Assert.Equal("11112", result[2].ExternalId);

            Assert.Equal("Desserts", result[3].Name);
            Assert.Equal(238, result[3].PoscategoryId);
            Assert.Equal(700, result[3].CompanyId);
            Assert.Equal("11113", result[3].ExternalId);
        }

        [Fact]
        public void GetPosdeliverypoints_MapsCorrectly()
        {
            SimphonyGetConfigurationInfoResponse xmlResult = MapXml();

            Assert.NotNull(xmlResult);

            Assert.NotNull(xmlResult.ArrayOfConfigInfoResponse.SingleOrDefault(c => c.ConfigInfoType.EConfigurationInfoType == "TABLES"));

            ConfigInfoResponse diningTablesResponse = xmlResult.ArrayOfConfigInfoResponse.Single(c => c.ConfigInfoType.EConfigurationInfoType == "TABLES");
            Assert.NotNull(diningTablesResponse);
            Assert.NotNull(diningTablesResponse.Tables);
            Assert.NotNull(diningTablesResponse.Tables.ArrayOfDbDiningTable);

            SimphonyConnectorMock sut = new SimphonyConnectorMock(xmlResult);

            var result = sut.GetPosdeliverypoints();

            Assert.Equal(2, result.Length);
            Assert.Equal("6533", result[0].ExternalId);
            Assert.Equal("10", result[0].Number);
            Assert.Equal("Table 10", result[0].Name);

            Assert.Equal("6534", result[1].ExternalId);
            Assert.Equal("11", result[1].Number);
            Assert.Equal("Table 11", result[1].Name);
        }

        [Fact]
        public void GetPosproducts_MapsCorrectly()
        {
            SimphonyGetConfigurationInfoResponse xmlResult = MapXml();

            Assert.NotNull(xmlResult);

            Assert.NotNull(xmlResult.ArrayOfConfigInfoResponse.SingleOrDefault(c => c.ConfigInfoType.EConfigurationInfoType == "MENUITEMDEFINITIONS"));
            Assert.NotNull(xmlResult.ArrayOfConfigInfoResponse.SingleOrDefault(c => c.ConfigInfoType.EConfigurationInfoType == "MENUITEMPRICES"));
            Assert.NotNull(xmlResult.ArrayOfConfigInfoResponse.SingleOrDefault(c => c.ConfigInfoType.EConfigurationInfoType == "MENUITEMMASTERS"));
            Assert.NotNull(xmlResult.ArrayOfConfigInfoResponse.SingleOrDefault(c => c.ConfigInfoType.EConfigurationInfoType == "MENUITEMCLASSES"));

            ConfigInfoResponse menuItemDefinitionsResponse = xmlResult.ArrayOfConfigInfoResponse.Single(c => c.ConfigInfoType.EConfigurationInfoType == "MENUITEMDEFINITIONS");
            Assert.NotNull(menuItemDefinitionsResponse);
            Assert.NotNull(menuItemDefinitionsResponse.MenuItemDefinitions);
            Assert.NotNull(menuItemDefinitionsResponse.MenuItemDefinitions.ArrayOfDbMenuItemDefinition);

            ConfigInfoResponse menuItemPricesResponse = xmlResult.ArrayOfConfigInfoResponse.Single(c => c.ConfigInfoType.EConfigurationInfoType == "MENUITEMPRICES");
            Assert.NotNull(menuItemPricesResponse);
            Assert.NotNull(menuItemPricesResponse.MenuItemPrices);
            Assert.NotNull(menuItemPricesResponse.MenuItemPrices.ArrayOfDbMenuItemPrice);

            ConfigInfoResponse menuItemMastersResponse = xmlResult.ArrayOfConfigInfoResponse.Single(c => c.ConfigInfoType.EConfigurationInfoType == "MENUITEMMASTERS");
            Assert.NotNull(menuItemMastersResponse);
            Assert.NotNull(menuItemMastersResponse.MenuItemMasters);
            Assert.NotNull(menuItemMastersResponse.MenuItemMasters.ArrayOfDbMenuItemMaster);

            ConfigInfoResponse menuItemClassesResponse = xmlResult.ArrayOfConfigInfoResponse.Single(c => c.ConfigInfoType.EConfigurationInfoType == "MENUITEMCLASSES");
            Assert.NotNull(menuItemClassesResponse);
            Assert.NotNull(menuItemClassesResponse.MenuItemClasses);
            Assert.NotNull(menuItemClassesResponse.MenuItemClasses.ArrayOfDbMenuItemClass);

            SimphonyConnectorMock sut = new SimphonyConnectorMock(xmlResult);

            var result = sut.GetPosproducts();

            Assert.Single(result);
            Assert.Equal("4916", result[0].ExternalId);
            Assert.Equal("8oz Sirloin Steak", result[0].Name);
            Assert.Equal(15.95M, result[0].PriceIn);
            Assert.Equal(1, result[0].VatTariff);
            Assert.Equal("Lovely lovely steak", result[0].Description);
            Assert.Equal("3", result[0].FieldValue1);
            Assert.Equal("4", result[0].FieldValue2);

            Assert.Equal(3, result[0].Posalterations.Length);
            Assert.Equal("1396", result[0].Posalterations[0].ExternalId);
            Assert.Equal("Mem Meat Temp", result[0].Posalterations[0].FieldValue1);

            Assert.Equal("1400", result[0].Posalterations[1].ExternalId);
            Assert.Equal("Mem Potato Choice", result[0].Posalterations[1].FieldValue1);

            Assert.Equal("1401", result[0].Posalterations[2].ExternalId);
            Assert.Equal("Mem Sauce Choice", result[0].Posalterations[2].FieldValue1);

            Assert.Single(result[0].Posalterations[0].Posalterationoptions);
            Assert.Equal("Medium Rare", result[0].Posalterations[0].Posalterationoptions[0].Description);
            Assert.Equal("5353", result[0].Posalterations[0].Posalterationoptions[0].ExternalId);
            Assert.Equal("Med Rare", result[0].Posalterations[0].Posalterationoptions[0].FieldValue1);
            Assert.Equal(0.0M, result[0].Posalterations[0].Posalterationoptions[0].PriceIn);

            Assert.Single(result[0].Posalterations[1].Posalterationoptions);
            Assert.Equal(string.Empty, result[0].Posalterations[1].Posalterationoptions[0].Description);
            Assert.Equal("5376", result[0].Posalterations[1].Posalterationoptions[0].ExternalId);
            Assert.Equal("Mashed Potato", result[0].Posalterations[1].Posalterationoptions[0].FieldValue1);
            Assert.Equal(1.40M, result[0].Posalterations[1].Posalterationoptions[0].PriceIn);

            Assert.Single(result[0].Posalterations[2].Posalterationoptions);
            Assert.Equal(string.Empty, result[0].Posalterations[2].Posalterationoptions[0].Description);
            Assert.Equal("5382", result[0].Posalterations[2].Posalterationoptions[0].ExternalId);
            Assert.Equal("Peppercorn Sauce", result[0].Posalterations[2].Posalterationoptions[0].FieldValue1);
            Assert.Equal(0.8M, result[0].Posalterations[2].Posalterationoptions[0].PriceIn);
        }

        private SimphonyGetConfigurationInfoResponse MapXml()
        {
            string xml = File.ReadAllText(Environment.CurrentDirectory + "\\SimphonyTestData.xml");

            XmlSerializer xmlSerializer = new XmlSerializer(typeof(SimphonyGetConfigurationInfoResponse));

            using (StringReader sr = new StringReader(xml))
            {
                return (SimphonyGetConfigurationInfoResponse)xmlSerializer.Deserialize(sr);
            }
        }
    }
}
