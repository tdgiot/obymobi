﻿using Dionysos;
using Newtonsoft.Json.Linq;
using Obymobi.Cms.Logic.HelperClasses;
using System;

namespace Obymobi.Cms.WebApplication
{
    public partial class CognitoOAuth : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string oAuthUrl = System.Configuration.ConfigurationManager.AppSettings[DionysosConfigurationConstants.CognitoBaseUrl] + "/oauth2/authorize";
            string oAuthTokenUrl = System.Configuration.ConfigurationManager.AppSettings[DionysosConfigurationConstants.CognitoBaseUrl] + "/oauth2/token";
            string clientId = System.Configuration.ConfigurationManager.AppSettings["CognitoClientId"];
            string clientSecret = System.Configuration.ConfigurationManager.AppSettings["CognitoSecret"];
            string scope = "profile+openid";

            if (Request["code"] != null)
            {
                //Build the form request from the parameters
                string formData = "client_id=" + clientId +
                    "&client_secret=" + clientSecret +
                    "&redirect_uri=" + Request.Url.GetLeftPart(UriPartial.Path) +
                    "&grant_type=authorization_code" +
                    "&code=" + Request["code"];

                //Exchange code for access token
                System.Net.WebClient webClient = new System.Net.WebClient();
                webClient.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                string results = webClient.UploadString(new Uri(oAuthTokenUrl), formData);
                window.InnerText = "Logged in successfully, close this tab and click Refresh on the payment integration configuration page";

                JObject tokenData = JObject.Parse(results);
                CognitoHelper.IdToken = tokenData["id_token"].ToString();
            }
            else //no "code" detected, redirect to OAuth service
            {
                string url = oAuthUrl + "" +
                    "?client_id=" + clientId +
                    "&scope=" + scope +
                    "&redirect_uri=" + Request.Url.GetLeftPart(UriPartial.Path) +
                    "&response_type=code";

                Response.Write($"<script>window.open('{url}','_self');</script>");
            }
        }
    }
}