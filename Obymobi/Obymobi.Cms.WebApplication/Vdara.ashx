﻿<%@ WebHandler Language="C#" Class="Vdara" %>

using System;
using System.Text;
using System.Web;
using Dionysos;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

public class Vdara : IHttpHandler {

    public void ProcessRequest (HttpContext context)
    {
        int companyId = 350;
        string[] values = context.Request.QueryString.GetValues("companyId");
        if (values != null && values.Length > 0)
        {
            if (!int.TryParse(values[0], out companyId))
            {
                companyId = 350;
            }
        }

        PredicateExpression filter = new PredicateExpression();
        filter.Add(ClientFields.CompanyId == companyId);

        RelationCollection relation = new RelationCollection();
        relation.Add(ClientEntity.Relations.DeliverypointEntityUsingDeliverypointId);
        relation.Add(ClientEntity.Relations.DeviceEntityUsingDeviceId);
        relation.Add(ClientEntity.Relations.DeliverypointgroupEntityUsingDeliverypointGroupId);

        /*
         [ClientId]
      ,Client.[LastRequestUTC]
      ,[LastIsCharging]
      ,[LastBatteryLevel]
      ,Client.[DeliverypointId]
      ,Deliverypoint.Number
      ,[LoadedSuccessfully]
      ,Device.WifiStrength
         */
        IncludeFieldsList fields = new IncludeFieldsList();
        fields.Add(ClientFields.ClientId);
        fields.Add(DeviceFields.LastRequestUTC);
        fields.Add(DeviceFields.BatteryLevel);
        fields.Add(ClientFields.DeliverypointId);
        fields.Add(DeliverypointFields.Number);
        fields.Add(ClientFields.LoadedSuccessfully);
        fields.Add(ClientFields.LastWifiSsid);
        fields.Add(DeviceFields.WifiStrength);
        fields.Add(DeviceFields.IsCharging);
        fields.Add(DeliverypointgroupFields.Name);
        fields.Add(DeviceFields.Identifier);
        fields.Add(DeviceFields.LastRequestUTC);
        fields.Add(DeviceFields.PublicIpAddress);

        PrefetchPath prefetch = new PrefetchPath(EntityType.ClientEntity);
        prefetch.Add(ClientEntityBase.PrefetchPathDeviceEntity);

        SortExpression sort = new SortExpression();
        sort.Add(DeliverypointFields.Number | SortOperator.Ascending);

        ClientCollection collection = new ClientCollection();
        collection.GetMulti(filter, 0, sort, relation, prefetch, fields, 0, 0);

        StringBuilder sb = new StringBuilder();
        sb.Append("<table border=1 cellpadding=4 style='border-style:1px solid black; border-collapse: collapse'>");

        sb.AppendFormat("<tr><td><b>{0}</b></td><td><b>{1}</b></td><td><b>{2}</b></td><td><b>{3}</b></td><td><b>{4}</b></td><td><b>{5}</b></td><td><b>{6}</b></td><td><b>{7}</b></td><td><b>{8}</b></td><td><b>{9}</b></td><td><b>{10}</b></td><td><b>{11}</b></td>",
            "ClientId", "Last Request (UTC)", "Battery", "Charging", "RoomControlConnected", "DPG", "DP ID", "DP Num", "Loaded", "Wifi", "Mac", "Public IP", "SSID");

        foreach (ClientEntity clientEntity in collection)
        {
            string deliverypointNumber = "";

            try
            {
                deliverypointNumber = clientEntity.DeliverypointEntity.Number;
            }
            catch (Exception ex)
            {

            }

            sb.AppendFormat("<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td><td>{6}</td><td>{7}</td><td>{8}</td><td>{9}</td><td>{10}</td><td>{11}</td><td>{12}</td>",
                clientEntity.ClientId,
                clientEntity.DeviceEntity.LastRequestUTC,
                clientEntity.DeviceEntity.BatteryLevel,
                clientEntity.DeviceEntity.IsCharging,
                clientEntity.RoomControlConnected,
                clientEntity.DeliverypointgroupEntity.Name,
                clientEntity.DeliverypointId,
                deliverypointNumber,
                clientEntity.Loaded,
                clientEntity.DeviceEntity.WifiStrength,
                clientEntity.DeviceEntity.Identifier,
                clientEntity.DeviceEntity.PublicIpAddress,
				clientEntity.LastWifiSsid);
        }
        sb.Append("</table>");

        context.Response.ContentType = "text/html";
        context.Response.Write(sb.ToString());
    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}