﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Generic.Publishing" Codebehind="Publishing.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
    <span style="padding-left: 4px"></span>
    <X:ToolBarButton runat="server" ID="btnRePublish" CommandName="RePublish" Text="Re-publish all" Image-Url="~/Images/Icons/arrow_refresh.png" PreSubmitWarning="Are you sure you want to re-publish all?" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
    <style type="text/css">
        .cdnTable {            
        }

        .cdnTable td {            
            padding: 2px;            
            border:1px dotted #bed4dc;
            border-left:0;
            border-top:0;
        }

        .cdnTable th {
            padding: 4px 6px 6px 4px;
            border:1px dotted #bed4dc;
            border-left:0;
            border-top:0;
        }

            .cdnTable td a {
                display:block;
                /*width:100%;*/
                height:20px;
                text-decoration:none;             
            }

            .cdnTable td.NotAvailable {
                background:url("../images/icons/cross.png") no-repeat center;
            } 

            .cdnTable td.OutOfDate {
                background:url("../images/icons/cross.png") no-repeat center;
            } 

            .cdnTable td.UpToDate {
                background:url("../images/icons/tick.png") no-repeat center;
            } 

            .cdnTable td.NotApplicable {
                background:url("../images/icons/bullet_orange.png") no-repeat center;
            }
            
            .cdnTable td.refresh {
                background:url("../images/icons/arrow_refresh.png") no-repeat center;
            }             

    </style>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
                    <D:PlaceHolder runat="server">
					    <table class="dataformV2">
						    <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblPublishRequest">What to publish</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
                                    <D:Label runat="server" id="lblPublishRequestValue" LocalizeText="false"></D:Label>
							    </td>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblPublishedUTC">Published</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
                                    <D:Label runat="server" id="lblPublishedValue" LocalizeText="false"></D:Label>
							    </td>
						    </tr>
						    <tr>
                                <td class="label">
                                    <D:Label runat="server" id="lblStatus">Status</D:Label>
							    </td>
							    <td class="control">
                                    <D:Label runat="server" id="lblStatusValue" LocalizeText="false"></D:Label>
							    </td>
							    <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblUserId">User</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
                                    <D:Label runat="server" id="lblUserName" LocalizeText="false"></D:Label>
							    </td>
						    </tr>
						    <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblLog">Log</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control" colspan="3">
                                    <D:TextBoxMultiLine ID="tbLog" runat="server" ReadOnly="true" />
							    </td>
						    </tr>
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblApiOperations">Operations</D:LabelEntityFieldInfo>
							    </td>
                                <td class="control" colspan="2">
                                    <table cellpadding="0" cellspacing="0" class="cdnTable" width="100%">
                                        <tr>
                                            <th><D:LabelTextOnly runat="server" LocalizeText="true" ID="lblApiOperation">API operation</D:LabelTextOnly></th>
                                            <th style="text-align: center; width: 100px;"><D:LabelTextOnly runat="server" LocalizeText="true" ID="lblApiOperationStatus">Status</D:LabelTextOnly></th>
                                            <th style="text-align: center; width: 85px;"><D:LabelTextOnly runat="server" LocalizeText="true" ID="lblCdnAmazon">Amazon</D:LabelTextOnly></th>
                                            <th style="text-align: center; width: 85px;"><D:LabelTextOnly runat="server" LocalizeText="true" ID="lblRepublish">Re-publish</D:LabelTextOnly></th>
                                        </tr>
                                        <D:PlaceHolder runat="server" ID="plhApiOperations"></D:PlaceHolder>
                                    </table>                        
                                </td>
							    <td class="control">
							    </td>
                            </tr>
					     </table>		
                    </D:PlaceHolder>
				</Controls>
			</X:TabPage>						
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>

