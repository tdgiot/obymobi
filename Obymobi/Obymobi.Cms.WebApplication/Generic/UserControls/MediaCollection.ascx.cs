﻿using System;
using System.Collections;
using System.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data;
using Dionysos.Web.UI;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System.Linq;
using Dionysos;
using Obymobi.Logic.HelperClasses;
using Obymobi.Enums;
using System.Collections.Generic;
using System.IO;
using CloudStorage;
using Dionysos.Web;
using DevExpress.Web.ASPxTreeList;
using Dionysos.Delegates.Web;
using Obymobi.Constants;

namespace Obymobi.ObymobiCms.Generic.UserControls
{
    public partial class MediaCollection : Dionysos.Web.UI.WebControls.SubPanelLLBLGenEntityCollection
    {
        #region Fields

        private string translationKeyPrefix = "Obymobi.ObymobiCms.Generic.UserControls.MediaCollection.";

        private const string queryStringParameterInformator = "inf";
        private const string queryStringParameterResultText = "rest";

        private string pageEntityName;
        private List<MediaType> mediaTypes;

        private Dictionary<int, Generic.SubPanels.MediaRatioTypesPanel> mediaTypesPanels = new Dictionary<int, SubPanels.MediaRatioTypesPanel>();

        private bool supportMultipleImages = true;
        private bool supportCultureSpecificImages = true;

        #endregion

        #region Methods        

        protected void HookUpEvents()
        {
            this.Page.PreRenderComplete += Page_PreRenderComplete;

            this.btAdd.Click += btAdd_Click;
            this.btUploadPdf.Click += btUploadPdf_Click;
            this.btDeleteSelectedTop.Click += btDeleteSelectedTop_Click;

            this.hlExpandAll.NavigateUrl = "javascript:tlMedia.ExpandAll()";
            this.hlCollapseAll.NavigateUrl = "javascript:tlMedia.CollapseAll()";
            this.hlRefresh.NavigateUrl = "javascript:tlMedia.PerformCallback('media-refresh')";

            this.PageAsPageLLBLGenEntity.DataSourceLoaded += PageAsPageLLBLGenEntity_DataSourceLoaded;
        }

        private void SetGui()
        {
            int informatorInt;
            string informatorText;
            if (!this.IsPostBack &&
                QueryStringHelper.TryGetValue(queryStringParameterInformator, out informatorInt) &&
                QueryStringHelper.TryGetValue(queryStringParameterResultText, out informatorText))
            {
                InformatorType type = informatorInt.ToEnum<InformatorType>();
                this.PageAsPageLLBLGenEntity.AddInformator(type, informatorText.HtmlDecode().HtmlEncode());
                this.PageAsPageLLBLGenEntity.Validate();
            }

            // Hide Add buttons + show informator if it's a Product Attachment with a Pdf alread added
            if (this.PageAsPageLLBLGenEntity.DataSource.LLBLGenProEntityTypeValue == (int)EntityType.AttachmentEntity &&
                this.DataSourceAsMediaCollection.Any(x => x.IsPdf()))
            {
                this.plhAddButtons.Visible = false;
                string cantUploadMoreMediaBecauseOfPdf = Dionysos.Global.TranslationProvider.GetTranslation("MediaCollectionPanel.CantUploadMoreMediaBecauseOfPdf", this.CurrentThreadLanguageCode,
                    "Er kunnen geen extra afbeeldingen / PDFs worden toegevoegd omdat er maar één PDF per Product Bijlage is toegestaan.", true);
                this.PageAsPageLLBLGenEntity.AddInformatorInfo(cantUploadMoreMediaBecauseOfPdf);
                this.PageAsPageLLBLGenEntity.Validate();
            }

            this.tlMedia.DataBind();

            this.plhMedia.Visible = !this.InheritFromBrand;

            if (this.pnlInheritedMedia.Visible)
            {
                this.tlInheritedMedia.DataBind();
            }
        }

        private void CreateTreelist()
        {
            // Create treelist
            this.tlMedia.SettingsEditing.Mode = TreeListEditMode.Inline;
            this.tlMedia.SettingsBehavior.AllowSort = false;
            this.tlMedia.SettingsBehavior.AllowDragDrop = false;
            this.tlMedia.ClientInstanceName = "tlMedia";
            this.tlMedia.AutoGenerateColumns = false;
            this.tlMedia.KeyFieldName = "MediaId";
            this.tlMedia.ParentFieldName = "AgnosticMediaId";
            this.tlMedia.SettingsBehavior.AutoExpandAllNodes = true;
            this.tlMedia.SettingsEditing.AllowNodeDragDrop = true;
            this.tlMedia.Settings.GridLines = GridLines.Both;
            this.tlMedia.SettingsSelection.AllowSelectAll = true;
            this.tlMedia.ClientSideEvents.EndDragNode = @"  function(s, e) {                                                                        
	                                                                    if(e.htmlEvent.shiftKey || document.getElementById('" + this.cbSortingMode.ClientID + @"').checked){                                                                            
		                                                                    e.cancel = true;
		                                                                    var key = s.GetNodeKeyByRow(e.targetElement);                                                                         
		                                                                    tlMedia.PerformCustomCallback('media-swap:' + e.nodeKey + ':' + key);
	                                                                    }                                                                        
                                                                    }";
            this.tlMedia.ClientSideEvents.NodeDblClick = @" function(s, e) {
                                                                        tlMedia.PerformCustomCallback('media-navigate:' + e.nodeKey);
		                                                                e.cancel = true;	                                                                    
                                                                    }";
            this.tlMedia.ClientSideEvents.CustomButtonClick = @"function (s ,e) { window.location.href = 'Media.aspx?id=' + e.nodeKey; }";
            this.tlMedia.ClientSideEvents.EndCallback = @"function(){ preloadImages(); }";
            this.tlMedia.ClientSideEvents.Init = "function(s, e) { preloadImages(); }";

            // Name
            var nameColumn = new TreeListTextColumn();
            nameColumn.Caption = "Name";
            nameColumn.FieldName = "Name";
            nameColumn.Width = Unit.Pixel(160);
            nameColumn.VisibleIndex = 1;
            this.tlMedia.Columns.Add(nameColumn);

            // Cultures
            if (this.supportCultureSpecificImages)
            {
                var languagesColumn = new TreeListTextColumn();
                languagesColumn.Caption = "Cultures";
                languagesColumn.FieldName = "MediaCountriesString";
                languagesColumn.Width = Unit.Pixel(0);
                languagesColumn.VisibleIndex = 10;
                languagesColumn.Visible = false;
                this.tlMedia.Columns.Add(languagesColumn);
            }

            // In-line Edit Column
            var editCommandColumn = new TreeListCommandColumn();
            editCommandColumn.VisibleIndex = 6;
            editCommandColumn.Width = Unit.Pixel(20);
            editCommandColumn.DeleteButton.Visible = true;
            editCommandColumn.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            editCommandColumn.ButtonType = ButtonType.Image;
            editCommandColumn.CellStyle.CssClass = "command";
            editCommandColumn.DeleteButton.Image.Url = this.ResolveUrl("~/images/icons/delete.png");
            editCommandColumn.CancelButton.Image.Url = this.ResolveUrl("~/images/icons/cross_grey.png");
            editCommandColumn.UpdateButton.Image.Url = this.ResolveUrl("~/images/icons/disk.png");
            this.tlMedia.Columns.Add(editCommandColumn);

            // Edit Page Column
            var editPageColumn = new TreeListHyperLinkColumn();
            editPageColumn.VisibleIndex = 7;
            editPageColumn.Caption = "&nbsp;";
            editPageColumn.FieldName = "MediaId";
            editPageColumn.Width = Unit.Pixel(15);
            editPageColumn.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            editPageColumn.PropertiesHyperLink.ImageUrl = "~/images/icons/page_edit.png";
            editPageColumn.PropertiesHyperLink.NavigateUrlFormatString = "~/Generic/Media.aspx?id={0}";
            editPageColumn.EditCellStyle.CssClass = "hidden";
            this.tlMedia.Columns.Add(editPageColumn);

            // Events
            this.tlMedia.CustomCallback += tlMedia_CustomCallback;
            this.tlMedia.HtmlDataCellPrepared += tlMedia_HtmlDataCellPrepared;
            this.tlMedia.NodeDeleting += tlMedia_NodeDeleting;

            // Data Source must be set in OnInit: http://documentation.devexpress.com/#AspNet/DevExpressWebASPxTreeListASPxTreeList_DataSourcetopic
            this.tlMedia.DataSource = this.DataSourceAsMediaCollection.OrderBy(m => m.SortOrder);
        }

        private void CreateTreeListInherited()
        {
            // Create treelist
            //this.tlInheritedMedia.SettingsEditing.Mode = TreeListEditMode.Inline;
            this.tlInheritedMedia.SettingsBehavior.AllowSort = false;
            this.tlInheritedMedia.SettingsBehavior.AllowDragDrop = false;
            this.tlInheritedMedia.ClientInstanceName = "tlInheritedMedia";
            this.tlInheritedMedia.AutoGenerateColumns = false;
            this.tlInheritedMedia.KeyFieldName = "MediaId";
            this.tlInheritedMedia.ParentFieldName = "AgnosticMediaId";
            this.tlInheritedMedia.SettingsBehavior.AutoExpandAllNodes = true;
            this.tlInheritedMedia.SettingsEditing.AllowNodeDragDrop = false;
            this.tlInheritedMedia.Settings.GridLines = GridLines.Both;
            this.tlInheritedMedia.ClientSideEvents.EndCallback = @"function(){ preloadImages(); }";
            this.tlInheritedMedia.ClientSideEvents.Init = "function(s, e) { preloadImages(); }";

            // Name
            var nameColumn = new TreeListTextColumn();
            nameColumn.Caption = "Name";
            nameColumn.FieldName = "Name";
            nameColumn.Width = Unit.Pixel(160);
            nameColumn.VisibleIndex = 1;
            this.tlInheritedMedia.Columns.Add(nameColumn);

            // Cultures
            var languagesColumn = new TreeListTextColumn();
            languagesColumn.Caption = "Cultures";
            languagesColumn.FieldName = "MediaCountriesString";
            languagesColumn.Width = Unit.Pixel(0);
            languagesColumn.VisibleIndex = 10;
            languagesColumn.Visible = false;
            this.tlInheritedMedia.Columns.Add(languagesColumn);

            this.tlInheritedMedia.HtmlDataCellPrepared += tlMediaInherited_HtmlDataCellPrepared;

            IMediaContainingEntity mediaContainingEntity = this.DataSourceAsBrandRelatedChildEntity.BrandParent as IMediaContainingEntity;
            if (mediaContainingEntity != null && mediaContainingEntity.MediaCollection.Count > 0)
            {
                this.tlInheritedMedia.DataSource = mediaContainingEntity.MediaCollection.OrderBy(m => m.SortOrder);
                this.pnlInheritedMedia.Visible = true;
            }
        }

        public override bool Add()
        {
            if (this.PageAsPageLLBLGenEntity.SaveCommand())
            {
                string relatedEntityType = this.pageEntityName.Replace("Entity", string.Empty);
                string url = string.Format("~/Generic/Media.aspx?mode=add&RelatedEntityType={0}&RelatedEntityId={1}",
                                        relatedEntityType, this.PageAsPageLLBLGenEntity.EntityId, true);

                // Include the media types if they're set
                if (this.MediaTypes != null && this.MediaTypes.Count > 0)
                {
                    string mediaTypesStr = "";

                    for (int i = 0; i < this.MediaTypes.Count; i++)
                    {
                        if (i == this.MediaTypes.Count - 1)
                            mediaTypesStr += (int)this.MediaTypes[i];
                        else
                            mediaTypesStr += (int)this.MediaTypes[i] + ",";
                    }

                    url = url + string.Format("&MediaTypes={0}", mediaTypesStr);
                }

                if (!this.supportCultureSpecificImages)
                {
                    url += $"&{Media.NoCulturesQueryParameter}=true";
                }

                this.Response.Redirect(url);
            }

            return true;
        }

        protected void RegisterClientSideCallBackHandlers()
        {
            // Generate JS for PDF uploading
            string pdfScript = string.Empty;
            string translatedProcessingPdf = Dionysos.Global.TranslationProvider.GetTranslation("MediaCollectionPanel.ProcessingPdf", this.CurrentThreadLanguageCode, "PDF verwerken...", true).Replace("'", "\\'");
            string translationNoFile = Dionysos.Global.TranslationProvider.GetTranslation("MediaCollectionPanel.NoFileSelected", this.CurrentThreadLanguageCode, "U heeft geen '.pdf' bestand gekozen om toe te voegen.", true).Replace("'", "\\'");
            string translationNotAPdf = Dionysos.Global.TranslationProvider.GetTranslation("MediaCollectionPanel.NotAPdfFileAlert", this.CurrentThreadLanguageCode, "Het gekozen bestand moet een '.pdf' bestand zijn.", true).Replace("'", "\\'");
            pdfScript += "\n function togglePdfUpload()";
            pdfScript += "\n	{";
            pdfScript += "\n	    if (!fileToggled)";
            pdfScript += "\n	    {";
            pdfScript += "\n	        $('" + this.fuPdf.ClientID + "').onchange = function () {";
            pdfScript += "\n	            if (this.value)";
            pdfScript += "\n	            {";
            pdfScript += "\n	                if (this.value.toLowerCase().match(\"pdf$\") == \"pdf\") {";
            pdfScript += "\n	                    Mediabox.open([[ '";
            pdfScript += this.ResolveUrl("~/images/ajax-loader.gif"); // "http://bradsknutson.com/wp-content/uploads/2013/04/page-loader.gif"
            pdfScript += "', '::" + translatedProcessingPdf + "' ]], 0, { 'initialWidth': 50, 'initialHeight': 50, 'closeable': false, 'imageLoadedCallback': function() { $('" + this.btUploadPdf.ClientID + "').click(); } });";
            pdfScript += "\n	                }";
            pdfScript += "\n	                else {";
            pdfScript += "\n	                    alert('" + translationNotAPdf + "');";
            pdfScript += "\n	                    this.val('')";
            pdfScript += "\n	                }";
            pdfScript += "\n	            }";
            pdfScript += "\n                else";
            pdfScript += "\n                {";
            pdfScript += "\n                    alert('" + translationNoFile + "');";
            pdfScript += "\n                }";
            pdfScript += "\n	        }";
            pdfScript += "\n	        fileToggled = true;";
            pdfScript += "\n	    }";
            pdfScript += "\n	    $('" + this.fuPdf.ClientID + "').click();";
            pdfScript += "\n	}";

            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "pdfUpload", pdfScript, true);
        }

        public void UploadPdf()
        {
            if (!this.fuPdf.HasFile)
            {
                this.PageAsPageLLBLGenEntity.AddInformator(InformatorType.Warning, Dionysos.Global.TranslationProvider.GetTranslation("MediaCollectionPanel.NoFileSelected", this.CurrentThreadLanguageCode, "Er is geen PDF bestand gekozen om te verwerken.", true));
            }

            string resultText = "";
            InformatorType informatorType = InformatorType.Hidden;
            try
            {
                int pages;

                List<MediaRatioType> defaultMediaRatioTypes = new List<MediaRatioType>();
                if (this.PageAsPageLLBLGenEntity.DataSource.LLBLGenProEntityTypeValue == (int)EntityType.AttachmentEntity)
                {
                    defaultMediaRatioTypes.Add(MediaRatioTypes.GetMediaRatioType(MediaType.AttachmentImage));
                }
                else if (this.PageAsPageLLBLGenEntity.DataSource.LLBLGenProEntityTypeValue == (int)EntityType.UIWidgetEntity)
                {
                    defaultMediaRatioTypes.Add(MediaRatioTypes.GetMediaRatioType(MediaType.WidgetPdf1280x800));
                }

                byte[] fileBytes;

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    this.fuPdf.PostedFile.InputStream.CopyTo(memoryStream);
                    fileBytes = memoryStream.ToArray();
                }

                PdfHelper.Instance.AttachPdfAsMedia((IMediaContainingEntity)this.Parent.DataSource,
                                                    this.fuPdf.FileName,
                                                    fileBytes,
                                                    defaultMediaRatioTypes,
                                                    out pages,
                                                    s => CloudUploader.Delete(ObymobiConstants.CloudContainerMediaFiles, s, false),
                                                    (mediaEnt, path, bytes) =>
                                                    {
                                                        CloudUploader.UploadMedia(mediaEnt, ObymobiConstants.CloudContainerMediaFiles, path, bytes);
                                                        ThumbnailGeneratorUtil.CleanUpTempFilesForMedia(mediaEnt.MediaId);
                                                    });
                if (pages == 0)
                {
                    // Empty PDF (?)
                    resultText = Dionysos.Global.TranslationProvider.GetTranslation("MediaCollectionPanel.PdfHasBeenAddedProcessedZeroPages", this.CurrentThreadLanguageCode, "Het gekozen PDF bestand '{0}' bevat geen (verwerkbare) pagina's.", true);
                    resultText = resultText.FormatSafe(this.fuPdf.FileName.HtmlEncode());
                    informatorType = InformatorType.Warning;
                }
                else if (pages == 1)
                {
                    // Single Page
                    resultText = Dionysos.Global.TranslationProvider.GetTranslation("MediaCollectionPanel.PdfHasBeenAddedProcessedSingle", this.CurrentThreadLanguageCode, "De pagina uit het PDF bestand '{0}' is toegevoegd als Media, u kunt deze nu bewerken en de juiste uitsneden kiezen.", true);
                    resultText = resultText.FormatSafe(this.fuPdf.FileName.HtmlEncode());
                    informatorType = InformatorType.Information;
                }
                else
                {
                    // Multiple Pages           
                    resultText = Dionysos.Global.TranslationProvider.GetTranslation("MediaCollectionPanel.PdfHasBeenAddedProcessedPlural", this.CurrentThreadLanguageCode, "De pagina's uit het PDF bestand '{0}' zijn toegevoegd als Media, u kunt deze nu bewerken en de juiste uitsneden kiezen.", true);
                    resultText = resultText.FormatSafe(this.fuPdf.FileName.HtmlEncode());
                    informatorType = InformatorType.Information;
                }
            }
            catch (Exception e)
            {
                File.AppendAllText(Server.MapPath("~/App_Data/Exception.txt"), e.ProcessStackTrace());

                resultText = Dionysos.Global.TranslationProvider.GetTranslation("MediaCollectionPanel.PdfCantBeProcessed", this.CurrentThreadLanguageCode, "Het gekozen PDF bestand '{0}' kan niet door het CMS worden verwerkt.", true);
                resultText = resultText.FormatSafe(this.fuPdf.FileName.HtmlEncode());
                informatorType = InformatorType.Warning;
            }

            bool devExHasThePageControlBug = true;

            if (devExHasThePageControlBug)
            {
                // Required postback
                QueryStringHelper qs = new QueryStringHelper();
                qs.AddItem(queryStringParameterInformator, (int)informatorType);
                qs.AddItem(queryStringParameterResultText, resultText);
                qs.AddItem(Dionysos.Web.UI.DevExControls.PageControl.queryStringElementToActiveTab, "Media");
                this.Response.Redirect(qs.MergeQuerystringWithRawUrl(), true);
            }
            else
            {
                // Redo to directly show the added images.
                this.PageAsPageLLBLGenEntity.AddInformator(informatorType, resultText);
                this.InitializeDataBindings();
            }
        }

        private void SwapNodes(int draggedMediaId, int draggedUponMediaId)
        {
            // Find both media
            var mediaDragged = this.DataSourceAsMediaCollection.FirstOrDefault(x => x.MediaId == draggedMediaId);
            var mediaDraggedUpon = this.DataSourceAsMediaCollection.FirstOrDefault(x => x.MediaId == draggedUponMediaId);

            bool draggedDown = false;
            // Sorting within a group of childeren is a bit more advanced, so 
            // that both up and down dragging can be used. 
            // (Instead of always putting the dragged item above the item it was dragged upon)
            if (mediaDragged.AgnosticMediaId == mediaDraggedUpon.AgnosticMediaId)
            {
                if (mediaDragged.SortOrder < mediaDraggedUpon.SortOrder)
                {
                    // It was above it, so it's dragged down
                    draggedDown = true;
                }

                // Put the items in the correct order - then renumber the whole lot
                // Get all medias 
                var medias = this.DataSourceAsMediaCollection.Where(x => x.AgnosticMediaId == mediaDragged.AgnosticMediaId).OrderBy(x => x.SortOrder).ToList();

                // Remove the dragged media from the list (we're going to insert that again on it's 
                // correct new place, and then re-number all sort orders.a
                medias.Remove(mediaDragged);

                for (int i = 0; i < medias.Count; i++)
                {
                    if (medias[i] == mediaDraggedUpon)
                    {
                        if (draggedDown)
                        {
                            // Place it below this item
                            medias.Insert(i + 1, mediaDragged);
                        }
                        else
                        {
                            // Place it in front of this item.
                            medias.Insert(i, mediaDragged);
                        }
                        break;
                    }
                }

                int orderNo = 1;
                foreach (var media in medias)
                {
                    media.SortOrder = orderNo;
                    media.Save();
                    orderNo++;
                }
            }
            else
            {
                mediaDragged.AgnosticMediaId = mediaDraggedUpon.MediaId;
                mediaDragged.SortOrder = mediaDraggedUpon.SortOrder;
                mediaDragged.Save();

                // Get other childeren to sort
                var childerenToSort = this.DataSourceAsMediaCollection.Where(x => x.AgnosticMediaId == mediaDraggedUpon.AgnosticMediaId && x.SortOrder >= mediaDragged.SortOrder && x.MediaId != draggedMediaId).OrderBy(x => x.SortOrder).ToList();
                int currentSortOrder = (int)mediaDragged.SortOrder + 1;
                for (int i = 0; i < childerenToSort.Count; i++)
                {
                    if (i == 0 && draggedDown)
                        childerenToSort[i].SortOrder = currentSortOrder - 2; // Dragged down with same parent, needs to be placed above it.
                    else
                        childerenToSort[i].SortOrder = currentSortOrder;

                    childerenToSort[i].Save();
                    currentSortOrder++;
                }
            }

            this.Refresh();
        }

        private void Refresh()
        {
            this.tlMedia.DataBind();
            this.tlMedia.ExpandAll();

            if (this.pnlInheritedMedia.Visible)
            {
                this.tlInheritedMedia.DataBind();
                this.tlInheritedMedia.ExpandAll();
            }
        }

        private void DeleteSelectedMedia()
        {
            List<TreeListNode> selectedNodes = new List<TreeListNode>();
            this.RetrieveSelectedNodes(ref selectedNodes, null);

            this.DeleteSelectedNodes(selectedNodes);
        }

        private void DeleteSelectedNodes(List<TreeListNode> nodesToDelete)
        {
            if (nodesToDelete.Count <= 0)
            {
                this.PageAsPageLLBLGenEntity.MultiValidatorDefault.AddError(this.PageAsPageLLBLGenEntity.Translate(this.translationKeyPrefix + "Error.NoMediaSelected", "Er is geen afbeelding geselecteerd om te verwijderen.", true));
                this.PageAsPageLLBLGenEntity.Validate();
            }
            else
            {
                EntityView<MediaEntity> mediaView = this.DataSourceAsMediaCollection.DefaultView;

                var mediaToBeRemoved = new Obymobi.Data.CollectionClasses.MediaCollection();
                foreach (TreeListNode node in nodesToDelete)
                {
                    int mediaId = Int32.Parse(node.Key);
                    mediaView.Filter = new PredicateExpression(MediaFields.MediaId == mediaId);
                    if (mediaView.Count > 0)
                    {
                        MediaEntity selectedMedia = mediaView[0];
                        mediaToBeRemoved.Add(selectedMedia);
                    }
                }
                mediaView.Filter = null;

                foreach (MediaEntity media in mediaToBeRemoved)
                {
                    media.Delete();
                    mediaView.RelatedCollection.Remove(media);
                }

                this.tlMedia.DataBind();

                if (nodesToDelete.Count == 1)
                    this.PageAsPageLLBLGenEntity.AddInformatorInfo(this.PageAsPageLLBLGenEntity.Translate(this.translationKeyPrefix + "Success.MediaDeleted", "De geselecteerde afbeelding is succesvol verwijderd.", true));
                else
                    this.PageAsPageLLBLGenEntity.AddInformatorInfo(this.PageAsPageLLBLGenEntity.Translate(this.translationKeyPrefix + "Success.MediasDeleted", "De geselecteerde afbeeldingen zijn succesvol verwijderd.", true));

                this.PageAsPageLLBLGenEntity.Validate();
            }
        }

        private void RetrieveSelectedNodes(ref List<TreeListNode> selectedNodes, TreeListNode parent)
        {
            TreeListNodeCollection nodes;

            if (parent != null)
                nodes = parent.ChildNodes;
            else
                nodes = this.tlMedia.Nodes;

            foreach (TreeListNode node in nodes)
            {
                if (node.Selected || node.ParentNode.Selected)
                {
                    if (node.ParentNode.Selected)
                        node.Selected = true;

                    selectedNodes.Add(node);
                }

                if (node.HasChildren)
                    this.RetrieveSelectedNodes(ref selectedNodes, node);
            }
        }

        private void RequeueMedia(int mediaId)
        {
            MediaEntity media = this.DataSourceAsMediaCollection.SingleOrDefault(m => m.MediaId == mediaId);
            if (media != null)
            {
                foreach (var mediaratioTypeMedia in media.MediaRatioTypeMediaCollection)
                {
                    MediaHelper.QueueMediaRatioTypeMediaFileTask(mediaratioTypeMedia, MediaProcessingTaskEntity.ProcessingAction.Upload, true, null, null);
                }

                string text = this.PageAsPageLLBLGenEntity.Translate("RequeuedAllForCdn", "Alle items zijn is opnieuw klaar gezet om te verzenden naar het CDN.");
                this.PageAsPageLLBLGenEntity.AddInformatorInfo(text);
                this.PageAsPageLLBLGenEntity.Validate();
            }
        }

        #endregion

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            this.HookUpEvents();

            this.EntityName = "MediaEntity";
            this.EntityPageUrl = "~/Generic/Media.aspx";

            this.CollectionOnParentDataSource = "MediaCollection";
        }

        protected void PageAsPageLLBLGenEntity_DataSourceLoaded(object sender)
        {
            this.pageEntityName = this.PageAsPageLLBLGenEntity.DataSource.LLBLGenProEntityName;

            this.CreateTreelist();

            if (this.DataSourceAsBrandRelatedChildEntity != null)
            {
                this.CreateTreeListInherited();
            }
        }

        private void Page_PreRenderComplete(object sender, EventArgs e)
        {
            // Only allow PDF attachment for IMediaContainingEntities
            if (this.PageAsPageLLBLGenEntity.DataSource as IMediaContainingEntity == null)
            {
                this.btConvertPdf.Visible = false;
            }

            // We don't have a license on the Secondary Server
            if (WebEnvironmentHelper.CloudEnvironment == CloudEnvironment.ProductionSecondary)
            {
                this.btConvertPdf.Enabled = false;
                string toolTipText = Dionysos.Global.TranslationProvider.GetTranslation("MediaCollectionPanel.PdfConversionTemporarilyNotAvailable", this.CurrentThreadLanguageCode, "Op dit moment is het converteren van PDFs niet beschikbaar", true);
                this.btConvertPdf.ToolTip = toolTipText;
            }

            if (!this.supportMultipleImages)
            {
                this.btAdd.Enabled = this.DataSourceAsMediaCollection.Count == 0;
                this.btAdd.ToolTip = "Only a single image is supported for this type of media";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.RegisterClientSideCallBackHandlers();
            this.SetGui();
        }

        private void btUploadPdf_Click(object sender, EventArgs e)
        {
            this.UploadPdf();
        }

        private void btAdd_Click(object sender, EventArgs e)
        {
            this.Add();
        }

        private void tlMedia_CustomCallback(object sender, TreeListCustomCallbackEventArgs e)
        {
            if (e.Argument.Contains("media-refresh", StringComparison.InvariantCultureIgnoreCase))
            {
                this.Refresh();
            }
            else if (e.Argument.Contains("media-requeue", StringComparison.InvariantCultureIgnoreCase))
            {
                string[] values = e.Argument.Split(new char[] { '-' });
                this.RequeueMedia(int.Parse(values[2]));
            }
            else if (e.Argument.Contains("media-navigate", StringComparison.InvariantCultureIgnoreCase))
            {
                string[] values = e.Argument.Split(new char[] { ':' });
                this.Response.RedirectLocation = this.ResolveUrl("~/Generic/Media.aspx?id={0}".FormatSafe(values[1]));
            }
            else if (e.Argument.Contains("media-swap", StringComparison.InvariantCultureIgnoreCase))
            {
                string[] values = e.Argument.Split(new char[] { ':' });
                this.SwapNodes(Convert.ToInt32(values[1]), Convert.ToInt32(values[2]));
            }
        }

        private void tlMedia_HtmlDataCellPrepared(object sender, TreeListHtmlDataCellEventArgs e)
        {
            if (e.Column.FieldName.Equals("ThumbnailImage"))
            {
                // Set the image of the media as a thumbnail                
                var hlImage = (Dionysos.Web.UI.WebControls.HyperLink)this.tlMedia.FindDataCellTemplateControl(e.NodeKey, e.Column, "hlImage");
                if (hlImage != null)
                {
                    hlImage.NavigateUrl = this.ResolveUrl("~/Generic/ThumbnailGenerator.ashx?mediaId={0}&width={1}&height={2}&fitInDimensions=true".FormatSafe(e.NodeKey, 600, 330));
                    hlImage.CssClass = "mb media_loading_gif";
                    hlImage.Attributes.Add("title", "Image");
                    hlImage.Attributes.Add("rel", "lightbox");
                }
            }
            else if (e.Column.FieldName.Equals("MediaCultures"))
            {
                // Change text to flags
                var plhImages = (Dionysos.Web.UI.WebControls.PlaceHolder)this.tlMedia.FindDataCellTemplateControl(e.NodeKey, e.Column, "plhCountryImages");
                string obj = (string)e.GetValue("MediaCountriesString");
                if (plhImages != null && !obj.IsNullOrWhiteSpace())
                {
                    string[] countries = obj.Split(',');
                    foreach (string country in countries)
                    {
                        string countryCode = country.Trim().ToLowerInvariant();
                        string url = this.ResolveUrl("~/Images/Icons/flags/flag_{0}.png", countryCode);
                        string html = string.Format("<img src=\"{0}\"></img>", url);
                        plhImages.AddHtml(html);
                    }
                }
            }
            else if (e.Column.FieldName.Equals("MediaRatioTypeString"))
            {
                var lblMediaRatioType = (Dionysos.Web.UI.WebControls.Label)this.tlMedia.FindDataCellTemplateControl(e.NodeKey, e.Column, "lblMediaRatioType");
                if (lblMediaRatioType != null && lblMediaRatioType.Text.IsNullOrWhiteSpace())
                {
                    MediaEntity media = this.DataSourceAsMediaCollection.SingleOrDefault(m => m.MediaId == Int32.Parse(e.NodeKey));
                    lblMediaRatioType.Text = media.MediaRatioTypeString;
                }
            }
            else if (e.Column.FieldName.Equals("Reupload"))
            {
                var imgRequeue = (Dionysos.Web.UI.WebControls.Image)this.tlMedia.FindDataCellTemplateControl(e.NodeKey, e.Column, "imgRequeue");
                if (imgRequeue != null)
                {
                    imgRequeue.AlternateText = e.NodeKey;
                }
            }
            else if (e.Column.FieldName.Equals("Amazon"))
            {
                MediaEntity media = this.DataSourceAsMediaCollection.SingleOrDefault(m => m.MediaId == Int32.Parse(e.NodeKey));
                int uploaded = media.MediaRatioTypeMediaCollection.Count(m => m.GetAmazonCdnStatus() == MediaRatioTypeMediaEntity.CdnStatus.UpToDate);

                e.Cell.Text = string.Format("{0}/{1}", uploaded, media.MediaRatioTypeMediaCollection.Count);
            }
        }

        private void btDeleteSelectedTop_Click(object sender, EventArgs e)
        {
            this.DeleteSelectedMedia();
        }

        private void tlMedia_NodeDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            e.Cancel = true;

            List<TreeListNode> nodesToDelete = new List<TreeListNode>();
            foreach (DictionaryEntry key in e.Keys)
            {
                TreeListNode node = this.tlMedia.FindNodeByKeyValue(key.Value.ToString());
                if (node != null)
                    nodesToDelete.Add(node);
            }

            this.DeleteSelectedNodes(nodesToDelete);
        }

        private Obymobi.Data.CollectionClasses.MediaCollection FilterOutMedia(MediaType type, Obymobi.Data.CollectionClasses.MediaCollection mediaCollection)
        {
            // Filter out thumbnail media, there are displayed on a separate tab for attachments
            List<MediaEntity> filterOut = new List<MediaEntity>();
            foreach (var media in mediaCollection)
            {
                foreach (var mrtm in media.MediaRatioTypeMediaCollection)
                {
                    if (mrtm.MediaType == (int)type)
                    {
                        filterOut.Add(media);
                        break;
                    }
                }
            }

            foreach (MediaEntity media in filterOut)
                mediaCollection.Remove(media);

            return mediaCollection;
        }

        #endregion

        #region Inherited Media

        private void tlMediaInherited_HtmlDataCellPrepared(object sender, TreeListHtmlDataCellEventArgs e)
        {
            if (e.Column.FieldName.Equals("ThumbnailImage"))
            {
                // Set the image of the media as a thumbnail                
                var hlImage = (Dionysos.Web.UI.WebControls.HyperLink)this.tlInheritedMedia.FindDataCellTemplateControl(e.NodeKey, e.Column, "hlImageInherited");
                if (hlImage != null)
                {
                    hlImage.NavigateUrl = this.ResolveUrl("~/Generic/ThumbnailGenerator.ashx?mediaId={0}&width={1}&height={2}&fitInDimensions=true".FormatSafe(e.NodeKey, 600, 330));
                    hlImage.CssClass = "mb media_loading_gif";
                    hlImage.Attributes.Add("title", "Image");
                    hlImage.Attributes.Add("rel", "lightbox");
                }
            }
            else if (e.Column.FieldName.Equals("MediaCultures"))
            {
                // Change text to flags
                var plhImages = (Dionysos.Web.UI.WebControls.PlaceHolder)this.tlInheritedMedia.FindDataCellTemplateControl(e.NodeKey, e.Column, "plhCountryImagesInherited");
                string obj = (string)e.GetValue("MediaCountriesString");
                if (plhImages != null && !obj.IsNullOrWhiteSpace())
                {
                    string[] countries = obj.Split(',');
                    foreach (string country in countries)
                    {
                        string countryCode = country.Trim().ToLowerInvariant();
                        string url = this.ResolveUrl("~/Images/Icons/flags/flag_{0}.png", countryCode);
                        string html = string.Format("<img src=\"{0}\"></img>", url);
                        plhImages.AddHtml(html);
                    }
                }
            }
            else if (e.Column.FieldName.Equals("MediaRatioTypeString"))
            {
                var lblMediaRatioType = (Dionysos.Web.UI.WebControls.Label)this.tlInheritedMedia.FindDataCellTemplateControl(e.NodeKey, e.Column, "lblMediaRatioTypeInherited");
                if (lblMediaRatioType != null && lblMediaRatioType.Text.IsNullOrWhiteSpace())
                {
                    IMediaContainingEntity mediaContainingEntity = this.DataSourceAsBrandRelatedChildEntity.BrandParent as IMediaContainingEntity;
                    MediaEntity media = mediaContainingEntity.MediaCollection.SingleOrDefault(m => m.MediaId == Int32.Parse(e.NodeKey));
                    lblMediaRatioType.Text = media.MediaRatioTypeString;
                }
            }
        }

        public INullableBrandRelatedChildEntity DataSourceAsBrandRelatedChildEntity
        {
            get
            {
                if (this.Page == null)
                {
                    return null;
                }

                return this.Parent.DataSource as INullableBrandRelatedChildEntity;
            }
        }

        #endregion

        #region Properties

        public List<MediaType> MediaTypes
        {
            set
            {
                this.mediaTypes = value;
            }
            get
            {
                return this.mediaTypes;
            }
        }

        private Obymobi.Data.CollectionClasses.MediaCollection DataSourceAsMediaCollection
        {
            get
            {
                if (this.pageEntityName.Contains("Attachment", StringComparison.InvariantCultureIgnoreCase))
                    return this.FilterOutMedia(MediaType.AttachmentThumbnail, this.DataSource as Obymobi.Data.CollectionClasses.MediaCollection);
                else
                    return this.DataSource as Obymobi.Data.CollectionClasses.MediaCollection;
            }
        }

        public bool InheritFromBrand
        {
            get => this.cbInheritMedia.Checked;
            set => this.cbInheritMedia.Checked = value;
        }

        // GK It's crap I use this, there wasn't a proper overload for GetTranslation with just key and translation value that also add non-found
        // translations, there for this paramter for that long overload :( But didn't feel I needed to changed to Dionysos functionality now.
        // If you do think I should, please help me in how to judge when YES and when NO to refactor if it's not a core bit of your feature.
        private string CurrentThreadLanguageCode
        {
            get
            {
                return System.Threading.Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName;
            }
        }

        public void SetImageConstraints(bool? supportMultipleImages = null, bool? supportCultureSpecificImages = null)
        {
            if (supportMultipleImages.HasValue)
            {
                this.supportMultipleImages = supportMultipleImages.Value;
            }

            if (supportCultureSpecificImages.HasValue)
            {
                this.supportCultureSpecificImages = supportCultureSpecificImages.Value;
            }
        }

        #endregion
    }
}

