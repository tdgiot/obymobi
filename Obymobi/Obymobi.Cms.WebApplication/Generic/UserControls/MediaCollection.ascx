﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Generic.UserControls.MediaCollection" Codebehind="MediaCollection.ascx.cs" %>
<%@ Register Assembly="Dionysos.Web" Namespace="Dionysos.Web.UI.WebControls" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v20.1" Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dxwtl" %>
<%@ Reference VirtualPath="~/Generic/SubPanels/MediaRatioTypesPanel.ascx" %>
<script src="../Js/mediabox-1.1.1-nc.js" type="text/javascript"></script>

<script type="text/javascript">
    var fileToggled = false;

    function requeueMedia(element) {
        var imgElement = element.getElementsByClassName("imgRequeue");
        if (imgElement.length > 0) {
            var mediaId = imgElement[0].getAttribute("alt");
            tlMedia.PerformCallback('media-requeue-' + mediaId);
        }
    }

    function changeThumbnailSize(url, width, height) {
        return url.replace('width=600', 'width=' + width).replace('height=330', 'height=' + height);
    }

    function preloadImages() {
        var images = $$('a[tag=thumbnail]');
        var count = images.length;
        for (var i = 0; i < images.length; i++) {
            var url = changeThumbnailSize(images[i].toString(), 25, 25);

            var request = new Request({
                url: url,
                async: true,
                onSuccess: function (data) {
                    var newUrl = changeThumbnailSize(this.toString(), 25, 25);
                    this.childNodes[0].src = newUrl;
                    this.className = 'mb';

                    count--;
                    if (count == 0) {
                        // The mediabox stops working on a callback (sorting)
                        Mediabox.scanPage();
                    }
                }.bind(images[i])
            });
            request.send();
        }
    }

</script>
<D:PlaceHolder runat="server" ID="plhMedia" Visible="true">
<D:PlaceHolder runat="server" ID="plhPanel" Visible="false">
<table class="dataformV2">
	<tr>
		<td class="control float">
			<div>
				<X:ComboBox runat="server" ID="ddlFilter" DisplayEmptyItem="False" SelectedIndex="0">
					<Items>			
						<dxe:ListEditItem Text="Alle bestanden tonen" Value="sadf" />
					</Items>
				</X:ComboBox>
			</div>						
			<div></div>					
		</td>	
	</tr>
</table>
</D:PlaceHolder>
<D:PlaceHolder runat="server" ID="plhAddButtons">
<div style="padding-bottom: 0px;">
	<D:CommandButton runat="server" ID="btAdd" CommandName="Add" Text="Toevoegen" ToolTip="Toevoegen" />&nbsp;    
    <D:Button runat="server" ID="btConvertPdf" Text="PDF als afbeelding(en) toevoegen" OnClientClick="togglePdfUpload(); return false;" />&nbsp;
    <D:Button runat="server" ID="btDeleteSelectedTop" Text="Verwijder geselecteerde(n)" />
    <span id="pdfUploadPanel" style="visibility:hidden;">
        <D:FileUpload runat="server" ID="fuPdf" ClientIDMode="Static" notdirty="true" Text="Pdf als afbeeldingen toevoegen" />
        <D:Button runat="server" ID="btUploadPdf" ClientIDMode="Static" />
    </span>    
</div>
</D:PlaceHolder>
<div id="mediaCollection" class="media-treelist">
    <div class="actions">
        <span class="nobold">
            <D:HyperLink runat="server" ID="hlExpandAll">Alles uitklappen</D:HyperLink> | <D:HyperLink runat="server" ID="hlCollapseAll">Alles inklappen</D:HyperLink> | <D:HyperLink runat="server" ID="hlRefresh">Verversen</D:HyperLink> | <D:CheckBox runat="server" ID="cbSortingMode" ClientIDMode="Static" Text="Sorteren" notdirty="true" /> <D:LabelTextOnly runat="server" ID="lblSortingWithShift">(of houd shift ingedrukt tijdens het slepen)</D:LabelTextOnly>
        </span>                                    
    </div>
    <dxwtl:ASPxTreeList ID="tlMedia" runat="server">
        <SettingsSelection Enabled="true" Recursive="false" />        
        <Columns>
            <dxwtl:TreeListTextColumn FieldName="ThumbnailImage" Caption="Image" VisibleIndex="0" Width="70" CellStyle-CssClass="center">
                <DataCellTemplate>                        
                    <D:HyperLink ID="hlImage" runat="server" ImageUrl="~/Images/mediabox/loading.gif" Tag="thumbnail">
                    </D:HyperLink>
                </DataCellTemplate>
            </dxwtl:TreeListTextColumn>
            <dxwtl:TreeListTextColumn FieldName="MediaRatioTypeString" Caption="Media Types" VisibleIndex="2" Width="290" CellStyle-CssClass="mediatypes">
                <DataCellTemplate>                        
                    <D:Label ID="lblMediaRatioType" runat="server" EnableViewState="false" />
                </DataCellTemplate>
            </dxwtl:TreeListTextColumn>            
            <dxwtl:TreeListTextColumn FieldName="MediaCultures" Caption="Cultures" VisibleIndex="3" Width="75">
                <DataCellTemplate>                        
                    <D:PlaceHolder ID="plhCountryImages" runat="server" />
                </DataCellTemplate>
            </dxwtl:TreeListTextColumn>
            <dxwtl:TreeListTextColumn FieldName="Amazon" Caption="Amazon" VisibleIndex="5" Width="44" CellStyle-CssClass="center">                
            </dxwtl:TreeListTextColumn>
            <dxwtl:TreeListTextColumn FieldName="Reupload" Caption="CDN" VisibleIndex="6" Width="24"  CellStyle-CssClass="center requeue">
                <DataCellTemplate>                                      
                    <span onclick="requeueMedia(this);">
                        <D:Image ID="imgRequeue" CssClass="imgRequeue" runat="server" Width="16" ImageUrl="~/Images/Icons/arrow_refresh.png" style="cursor:pointer;" />
                    </span>
                </DataCellTemplate>
            </dxwtl:TreeListTextColumn>
        </Columns>
    </dxwtl:ASPxTreeList>
</div>
</D:PlaceHolder>
<D:Panel ID="pnlInheritedMedia" runat="server" GroupingText="Inherited media" Visible="False" LocalizeText="False">
    <div class="media-treelist">
        <table class="dataformV2">
            <tr>
                <td class="control"><D:CheckBox ID="cbInheritMedia" ClientIDMode="Static" runat="server" Text="Inherit from brand" LocalizeText="False" /></td>
            </tr>
            <tr>
                <td>
                    <dxwtl:ASPxTreeList ID="tlInheritedMedia" runat="server">
                        <Columns>
                            <dxwtl:TreeListTextColumn FieldName="ThumbnailImage" Caption="Image" VisibleIndex="0" Width="70" CellStyle-CssClass="center">
                                <DataCellTemplate>                        
                                    <D:HyperLink ID="hlImageInherited" runat="server" ImageUrl="~/Images/mediabox/loading.gif" Tag="thumbnail">
                                    </D:HyperLink>
                                </DataCellTemplate>
                            </dxwtl:TreeListTextColumn>
                            <dxwtl:TreeListTextColumn FieldName="MediaRatioTypeString" Caption="Media Types" VisibleIndex="2" Width="290" CellStyle-CssClass="mediatypes">
                                <DataCellTemplate>                        
                                    <D:Label ID="lblMediaRatioTypeInherited" runat="server" EnableViewState="false" />                    
                                </DataCellTemplate>
                            </dxwtl:TreeListTextColumn>            
                            <dxwtl:TreeListTextColumn FieldName="MediaCultures" Caption="Cultures" VisibleIndex="3" Width="75">
                                <DataCellTemplate>                        
                                    <D:PlaceHolder ID="plhCountryImagesInherited" runat="server" />
                                </DataCellTemplate>
                            </dxwtl:TreeListTextColumn>
                        </Columns>
                    </dxwtl:ASPxTreeList> 
                </td>
            </tr>
        </table>
        </div>
</D:Panel>
