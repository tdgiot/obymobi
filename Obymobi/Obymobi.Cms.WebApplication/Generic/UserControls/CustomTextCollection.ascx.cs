using System;
using System.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System.Linq;
using Dionysos;
using Obymobi.Logic.HelperClasses;
using Obymobi.Enums;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using Dionysos.Web.UI.DevExControls;
using DevExpress.Web;
using Dionysos.Reflection;
using Dionysos.Web.UI.WebControls;
using Obymobi.Attributes;
using MarkdownSharp.Extensions;
using TabPage = Dionysos.Web.UI.DevExControls.TabPage;

namespace Obymobi.ObymobiCms.Generic.UserControls
{
    public partial class CustomTextCollection : SubPanelLLBLGenEntity
    {
        #region Fields

        private readonly Dictionary<string, TabPage> tabs = new Dictionary<string, TabPage>();
        private readonly List<TextBoxString> textBoxes = new List<TextBoxString>();
        private CustomTextType[] excludedCustomTextTypes;
        private CustomTextType[] disabledCustomTextTypes;

        #endregion

        #region Methods

        private void DataBindCultures(List<string> cultureCodesToExclude)
        {
            IEnumerable<Obymobi.Culture> cultures = Obymobi.Culture.Mappings.Values.OrderBy(x => x.ToString());

            ddlCultureCode.DataSource = cultures.Where(x => !cultureCodesToExclude.Contains(x.Code));
            ddlCultureCode.DataBind();
        }

        protected void HookUpEvents()
        {
            btnAdd.Click += btnAdd_Click;
        }        

        private void SetGui()
        {
            textBoxes.Clear();

            if (IsUITabRelatedEntity(CustomTextContainingEntity) && ((UITabEntity)CustomTextContainingEntity).UIModeEntity.PointOfInterestId.HasValue)
            {
                // UI tab (culture codes from PointOfInterest entity)
                RenderTabs(((UITabEntity)CustomTextContainingEntity).UIModeEntity.PointOfInterestEntity.CustomTextCollection.Select(x => x.CultureCode).Distinct().ToList(), CustomTextHelper.DefaultCulture.Code);
            }
            else if (IsUIWidgetRelatedEntity(CustomTextContainingEntity) && ((UIWidgetEntity)CustomTextContainingEntity).UITabEntity.UIModeEntity.PointOfInterestId.HasValue)
            {
                // UI Widget (culture codes from PointOfInterest entity)
                RenderTabs(((UIWidgetEntity)CustomTextContainingEntity).UITabEntity.UIModeEntity.PointOfInterestEntity.CustomTextCollection.Select(x => x.CultureCode).Distinct().ToList(), CustomTextHelper.DefaultCulture.Code);
            }
            else if (IsSiteRelatedEntity(CustomTextContainingEntity))
            {
                // Site related
                RenderSiteCultures((SiteEntity)CustomTextContainingEntity);                
            }            
            else if (IsSiteTemplateRelatedEntity(CustomTextContainingEntity))
            {
                // Site template related
                RenderSiteTemplateCultures((SiteTemplateEntity)CustomTextContainingEntity);
            }
            else if (CustomTextContainingEntity is AttachmentEntity && ((AttachmentEntity)CustomTextContainingEntity).PageId.HasValue)
            {
                // Attachment related to page entity
                RenderSiteCultures(((AttachmentEntity)CustomTextContainingEntity).PageEntity.SiteEntity);
            }
            else if (CustomTextContainingEntity is AttachmentEntity && ((AttachmentEntity)CustomTextContainingEntity).PageTemplateId.HasValue)
            {
                // Attachment related to page template entity
                RenderSiteTemplateCultures(((AttachmentEntity)CustomTextContainingEntity).PageTemplateEntity.SiteTemplateEntity);
            }
            else if (IsBrandRelatedEntity(CustomTextContainingEntity))
            {
                // Brand related
                RenderBrandCultures();
            }
            else if (IsCompanyRelatedEntity(CustomTextContainingEntity))
            {
                // Company related
                RenderCompanyCultures();
            }
            else
            {
                // Generic
                RenderGenericCultures();
            }
        }    
            
        private void RenderSiteCultures(SiteEntity siteEntity)
        {
            RenderTabs(((SiteEntity)siteEntity).SiteCultureCollection.Select(x => x.CultureCode).ToList(), CustomTextHelper.DefaultCulture.Code);
        }        

        private void RenderSiteTemplateCultures(SiteTemplateEntity siteTemplateEntity)
        {
            RenderTabs(((SiteTemplateEntity)siteTemplateEntity).SiteTemplateCultureCollection.Select(x => x.CultureCode).ToList(), CustomTextHelper.DefaultCulture.Code);
        }

        private void RenderCompanyCultures()
        {
            List<string> brandCultures = null;
            INullableBrandRelatedChildEntity brandRelatedChildEntity = CustomTextContainingEntity as INullableBrandRelatedChildEntity;
            if (brandRelatedChildEntity != null && brandRelatedChildEntity.ParentBrandId.HasValue)
            {
                CustomTextBrandRelatedChildEntity = brandRelatedChildEntity;
                brandCultures = brandRelatedChildEntity.BrandParentBrandEntity.BrandCultureCollection.Select(x => x.CultureCode).ToList();
            }

            RenderTabs(CmsSessionHelper.Cultures.Select(x => x.CultureCode).ToList(), CmsSessionHelper.DefaultCultureCodeForCompany, brandCultures);
        }

        private void RenderBrandCultures()
        {
            INullableBrandRelatedEntity brandRelatedEntity = (INullableBrandRelatedEntity)CustomTextContainingEntity;
            RenderTabs(brandRelatedEntity.BrandEntity.BrandCultureCollection.Select(x => x.CultureCode).ToList(), brandRelatedEntity.BrandEntity.CultureCode);
        }

        private void RenderGenericCultures()
        {
            plhAddCulture.Visible = true;
            List<string> cultureCodes = CustomTextContainingEntity.CustomTextCollection.Where(x => !string.IsNullOrWhiteSpace(x.CultureCode)).Select(x => x.CultureCode).Distinct().ToList();
            if (!IsPostBack)
            {
                DataBindCultures(cultureCodes);
            }
            RenderTabs(cultureCodes, CustomTextHelper.DefaultCulture.Code);
        }

        private void RenderTabs(List<string> cultures, string defaultCultureCode, List<string> brandCultures = null)
        {
            PageControl page = CreatePage();

            DefaultCulture = defaultCultureCode;

            cultures = cultures.OrderBy(x => x).ToList();

            if (!defaultCultureCode.IsNullOrWhiteSpace() && cultures.Contains(defaultCultureCode, StringComparison.InvariantCultureIgnoreCase))
            {
                List<string> toAdd = new List<string>();
                foreach (string culture in cultures)
                {
                    if (!culture.Equals(defaultCultureCode, StringComparison.InvariantCultureIgnoreCase))
                    {
                        toAdd.Add(culture);
                    }
                }
                toAdd.Insert(0, defaultCultureCode);
                cultures = toAdd;
            }

            tabs.Clear();
            foreach (string cultureCode in cultures)
            {
                if (cultureCode.IsNullOrWhiteSpace())
                    continue;

                bool isDefaultCulture = !defaultCultureCode.IsNullOrWhiteSpace() && cultureCode.Equals(defaultCultureCode, StringComparison.InvariantCultureIgnoreCase);
                bool isBrandCulture = brandCultures != null && brandCultures.Contains(cultureCode);

                TabPage tab = CreateTab(cultureCode, isDefaultCulture);
                RenderControls(tab, cultureCode, defaultCultureCode, isBrandCulture);

                tabs.Add(cultureCode, tab);
                page.TabPages.Add(tab);
            }

            plhCultures.Controls.Add(page);
        }

        private void RenderControls(TabPage tab, string cultureCode, string defaultCultureCode, bool isBrandCulture)
        {
            List<CustomTextType> types = CustomTextHelper.GetCustomTextTypesForEntity(CustomTextContainingEntity.GetType().Name, excludedCustomTextTypes);
            if (types.Count == 0)
            {
                return;
            }

            Dictionary<CustomTextType, string> brandCultureCodes = new Dictionary<CustomTextType, string>();
            Dictionary<CustomTextType, string> cultureCodes = CustomTextContainingEntity.CustomTextCollection.ToCultureCodeSpecificDictionary(cultureCode);
            if (CustomTextBrandRelatedChildEntity != null && CustomTextBrandRelatedChildEntity.BrandParent is ICustomTextContainingEntity)
            {
                brandCultureCodes = ((ICustomTextContainingEntity)CustomTextBrandRelatedChildEntity.BrandParent).CustomTextCollection.ToCultureCodeSpecificDictionary(cultureCode);
            }

            bool isDefaultCulture = cultureCode.Equals(defaultCultureCode, StringComparison.InvariantCultureIgnoreCase);

            Table table = new Table();
            table.Width = Unit.Percentage(100);
            table.CssClass = "dataformV2";

            TableRow row = null;

            if (!isDefaultCulture && isBrandCulture)
            {
                row = AddRow(ref table);

                TableCell brandWarningCell = new TableCell();
                brandWarningCell.CssClass = "control";
                brandWarningCell.ColumnSpan = 2;
                row.Cells.Add(brandWarningCell);

                ASPxLabel lblBrandWarning = AddLabel(ref brandWarningCell);
                lblBrandWarning.Text = "This translation is managed on the linked brand product and can not be edited on this page.";
            }

            row = AddRow(ref table);

            TableCell tcLabel = AddLabelCell(ref row);
            TableCell tcControl = AddControlCell(ref row);

            ASPxLabel lblCultureCaption = AddLabel(ref tcLabel);
            lblCultureCaption.Text = "Culture";

            TextBoxString tbCultureName = AddTextBox(ref tcControl);
            tbCultureName.Enabled = false;
            tbCultureName.Text = Culture.Mappings[cultureCode].NameAndCultureCode;

            AddLabelCell(ref row);
            AddControlCell(ref row);

            for (int i = 0; i < types.Count; i++)
            {
                CustomTextType type = types[i];

                if (EnumUtil.GetAttribute<DisableRendering>(type) != null)
                { continue; }
                    
                string text;
                if (!isDefaultCulture && isBrandCulture)
                {
                    text = brandCultureCodes.GetValueOrDefault(type);
                }
                else
                {
                    text = cultureCodes.GetValueOrDefault(type);
                    if (isDefaultCulture && text.IsNullOrWhiteSpace())
                    {
                        text = brandCultureCodes.GetValueOrDefault(type);
                    }
                }

                if (i % 2 == 0 || FullWidth)
                {
                    row = AddRow(ref table);
                }

                tcLabel = AddLabelCell(ref row);
                tcControl = AddControlCell(ref row);
                tcControl.ColumnSpan = FullWidth ? 3 : 1;

                ASPxLabel lblCulture = AddLabel(ref tcLabel);
                lblCulture.Text = EnumUtil.GetStringValue((Enum)type);

                TextBoxString tbCulture = AddTextBox(ref tcControl);
                tbCulture.ID = string.Format("Culture_{0}_{1}", cultureCode, (int)type);
                tbCulture.Enabled = !isDefaultCulture;

                string customTextValue = text.IsNullOrWhiteSpace() ? string.Empty : text;

                if (EnumUtil.GetAttribute<MultiLineTextBox>(type) != null)
                {
                    tbCulture.Rows = 5;
                    tbCulture.TextMode = TextBoxMode.MultiLine;
                }

                if (EnumUtil.GetAttribute<EnableEditingOnDefaultTab>(type) != null)
                {
                    tbCulture.Enabled = true;
                }

                if (EnumUtil.GetAttribute<Markdown>(type) != null)
                {
                    tbCulture.Text = MarkdownHelper.RemoveMarkdown(customTextValue);
                }
                else
                {
                    tbCulture.Text = customTextValue;
                }

                if (isBrandCulture)
                {
                    tbCulture.Enabled = false;
                }

                tbCulture.Enabled = IsCustomTextTypeEnabled(type);

                Labels.Add(lblCulture);
                textBoxes.Add(tbCulture);

                if (i == types.Count - 1 && !FullWidth)
                {
                    AddLabelCell(ref row);
                    AddControlCell(ref row);
                }
            }

            tab.Controls.Add(table);
        }

        private bool IsCustomTextTypeEnabled(CustomTextType customTextType)
        {
            if (disabledCustomTextTypes == null)
            {
                return true;
            }

            return disabledCustomTextTypes.All(x => x != customTextType);
        }

        public override bool Save()
        {
            ITransaction transaction = GetOrCreateTransaction();
            try
            {
                SaveCustomTexts(transaction);
                transaction.Commit();

                return true;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                throw ex;
            }
            finally
            {
                transaction.Dispose();
            }
        }
        
        public void ExcludeCustomTextTypes(params CustomTextType[] customTextTypes)
        {
            excludedCustomTextTypes = customTextTypes;
        }

        public void DisableCustomTextTypes(params CustomTextType[] customTextTypes)
        {
            disabledCustomTextTypes = customTextTypes;
        }

        private ITransaction GetOrCreateTransaction()
        {
            ITransaction transaction = TransactionFromCustomTextContainingEntity;
            if (transaction == null)
            {
                transaction = new Transaction(IsolationLevel.ReadUncommitted, "SaveCustomTexts");
            }

            return transaction;
        }

        private ITransaction TransactionFromCustomTextContainingEntity => CustomTextContainingEntity?.GetCurrentTransaction();

        private void SaveCustomTexts(ITransaction transaction)
        {
            EntityView<CustomTextEntity> entityView = CustomTextContainingEntity.CustomTextCollection.DefaultView;

            List<string> brandCultures = new List<string>();
            if (CustomTextBrandRelatedChildEntity != null)
            {
                brandCultures = CustomTextBrandRelatedChildEntity.BrandParentBrandEntity.BrandCultureCollection.Select(x => x.CultureCode).ToList();
            }

            foreach (TextBoxString tbCulture in textBoxes)
            {
                string[] values = tbCulture.ID.GetAllAfterFirstOccurenceOf("Culture_").Split('_');

                string cultureCode = values[0];
                if (cultureCode.Equals(DefaultCulture, StringComparison.InvariantCultureIgnoreCase) && !tbCulture.Enabled)
                {
                    // Skip default culture
                    continue;
                }

                if (brandCultures.Contains(cultureCode, StringComparison.InvariantCultureIgnoreCase) && !tbCulture.Enabled)
                {
                    // Skip cultures which are handled on the linked brand
                    continue;
                }

                if (!tbCulture.Enabled)
                {
                    continue;
                }

                CustomTextType customTextType = values[1].ToEnum<CustomTextType>();
                if (!IsCustomTextTypeEnabled(customTextType))
                {
                    continue;
                }

                if (!cultureCode.IsNullOrWhiteSpace())
                {
                    PredicateExpression filter = new PredicateExpression(CustomTextFields.CultureCode == cultureCode);
                    filter.Add(CustomTextFields.Type == customTextType);

                    entityView.Filter = filter;

                    if (tbCulture.Text.IsNullOrWhiteSpace() && entityView.Count > 0)
                    {
                        // No value
                        entityView[0].AddToTransaction(transaction);
                        entityView[0].Delete();
                        entityView.RelatedCollection.Remove(entityView[0]);
                    }
                    else if (!tbCulture.Text.IsNullOrWhiteSpace())
                    {
                        // Has value
                        CustomTextEntity entity;
                        if (entityView.Count > 0)
                        {
                            entity = entityView[0];
                        }
                        else
                        {
                            string foreignKey = CustomTextContainingEntity.PrimaryKeyFields[0].Name.ToString();
                            int foreignKeyValue = int.Parse(CustomTextContainingEntity.PrimaryKeyFields[0].CurrentValue.ToString());
                            entity = CustomTextHelper.CreateEntity(customTextType, cultureCode, tbCulture.Text, foreignKey, foreignKeyValue);
                        }

                        if (EnumUtil.GetAttribute<Markdown>(customTextType) != null)
                        {
                            entity.Text = MarkdownHelper.AddMarkdown(tbCulture.Text);
                        }
                        else
                        {
                            entity.Text = tbCulture.Text;
                        }

                        entity.AddToTransaction(transaction);
                        entity.Save();
                    }
                }
            }
        }

        private void CreateEmptyCustomTexts(string cultureCode)
        {
            if (CustomTextContainingEntity.CustomTextCollection.Any(x => x.CultureCode == cultureCode))
                return;

            Transaction transaction = new Transaction(IsolationLevel.ReadUncommitted, "CreateEmptyCustomTexts");
            try
            {
                List<CustomTextType> types = CustomTextHelper.GetCustomTextTypesForEntity(CustomTextContainingEntity.GetType().Name);

                foreach (CustomTextType type in types)
                {
                    string foreignKey = CustomTextContainingEntity.LLBLGenProEntityName.Replace("Entity", "Id");
                    int foreignKeyValue = int.Parse(CustomTextContainingEntity.PrimaryKeyFields[0].CurrentValue.ToString());

                    CustomTextEntity entity = new CustomTextEntity();
                    entity.CultureCode = cultureCode;
                    entity.Text = " ";
                    entity.Type = type;
                    entity.SetNewFieldValue(foreignKey, foreignKeyValue);
                    entity.AddToTransaction(transaction);
                    entity.Save();
                }

                transaction.Commit();
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                throw ex;
            }
            finally
            {
                transaction.Dispose();
            }
        }

        private PageControl CreatePage()
        {
            PageControl page = new PageControl();
            page.ID = $"pc{nameof(CustomTextCollection)}";
            page.Width = Unit.Percentage(100);
            return page;
        }

        private TabPage CreateTab(string cultureCode, bool defaultTab)
        {
            TabPage tab = new TabPage();
            tab.Name = string.Format("tab-{0}-{1}", cultureCode, CmsSessionHelper.CurrentCompanyId);
            tab.Text = cultureCode.ToUpperInvariant();
            tab.LocalizeText = false;
            if (defaultTab)
            {
                tab.Text += " (default)";
            }
            return tab;
        }

        private TableRow AddRow(ref Table table)
        {
            TableRow row = new TableRow();
            table.Rows.Add(row);
            return row;
        }

        private TableCell AddLabelCell(ref TableRow row)
        {
            TableCell label = new TableCell();
            label.CssClass = "label";
            row.Cells.Add(label);
            return label;
        }

        private TableCell AddControlCell(ref TableRow row)
        {
            TableCell control = new TableCell();
            control.CssClass = "control";
            row.Cells.Add(control);
            return control;
        }

        private ASPxLabel AddLabel(ref TableCell cell)
        {
            ASPxLabel label = new ASPxLabel();
            cell.Controls.Add(label);
            return label;
        }

        private TextBoxString AddTextBox(ref TableCell cell)
        {
            TextBoxString textbox = new TextBoxString();
            cell.Controls.Add(textbox);
            return textbox;
        }

        private bool IsSiteRelatedEntity(ICustomTextContainingEntity entity) { return entity is SiteEntity; }
        private bool IsSiteTemplateRelatedEntity(ICustomTextContainingEntity entity) { return entity is SiteTemplateEntity; }
        private bool IsUITabRelatedEntity(ICustomTextContainingEntity entity) { return entity is UITabEntity; }
        private bool IsUIWidgetRelatedEntity(ICustomTextContainingEntity entity) { return entity is UIWidgetEntity; }

        private bool IsBrandRelatedEntity(ICustomTextContainingEntity entity)
        {
            if (entity is INullableBrandRelatedEntity)
            {
                object value = Member.GetMemberValue(entity, "BrandId");
                return (value != null && (int)value > 0);
            }
            return false;
        }

        private bool IsCompanyRelatedEntity(ICustomTextContainingEntity entity)
        {
            return entity is ICompanyRelatedEntity ||
                entity is ICompanyRelatedChildEntity ||
                entity is ICompanyRelatedChildEntityOrNullableCompanyRelatedChildEntity ||
                (entity is INullableCompanyRelatedEntity && ((INullableCompanyRelatedEntity)entity).CompanyId.GetValueOrDefault(0) > 0) ||
                (entity is INullableCompanyRelatedChildEntity && ((INullableCompanyRelatedChildEntity)entity).ParentCompanyId.GetValueOrDefault(0) > 0);
        }

        public void Initialize()
        {
            SetGui();
            Initialized = true;
        }

        public void AddControl(Control control, string cultureCode)
        {
            if (tabs.ContainsKey(cultureCode))
            {
                TabPage tab = tabs[cultureCode];
                tab.Controls.Add(control);
            }
        }

        #endregion

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {            
            base.OnInit(e);            
            EntityName = "CustomTextEntity";
        }        

        protected void Page_Load(object sender, EventArgs e)
        {
            HookUpEvents();
            if (!Initialized)
            {
                SetGui(); 
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (ddlCultureCode.Value != null && !ddlCultureCode.Value.ToString().IsNullOrWhiteSpace())
            {
                CreateEmptyCustomTexts(ddlCultureCode.Value.ToString());                
                Response.Redirect(Request.RawUrl);
            }                        
        }        

        #endregion		        

        #region Properties

        private ICustomTextContainingEntity CustomTextContainingEntity
        {
            get
            {
                return PageAsPageLLBLGenEntity.DataSource as ICustomTextContainingEntity;
            }
        }

        private INullableBrandRelatedChildEntity CustomTextBrandRelatedChildEntity { get; set; }

        private string DefaultCulture { get; set; }

        private bool Initialized { get; set; }

        public bool FullWidth { get; set; }

        public ICollection<ASPxLabel> Labels { get; } = new List<ASPxLabel>();
        
        #endregion
    }
}

