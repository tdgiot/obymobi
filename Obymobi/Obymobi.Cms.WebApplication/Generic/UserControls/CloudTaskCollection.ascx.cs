﻿using Obymobi.Constants;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.ObymobiCms.UI;
using System;
using System.Data;
using System.Web.UI;

namespace Obymobi.ObymobiCms.Generic.UserControls
{
    public partial class CloudTaskCollection : UserControl
    {
        readonly DateTime failedTime = DateTime.MinValue;

        protected void Page_Load(object sender, EventArgs e)
        {
            this.taskGrid.Columns["AmazonUrl"].HeaderStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Center;
            this.taskGrid.Columns["AmazonUrl"].CellStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Center;

            LoadCdnData();
        }

        private void LoadCdnData()
        {
            var taskCollection = this.DataSourceAsCloudProcessingTaskCollection;
            if (taskCollection == null || taskCollection.Count == 0)
                return;

            // Build custom DataSource
            var tasksDataSource = new DataTable();
            var createdColumn = new DataColumn("Created");
            tasksDataSource.Columns.Add(createdColumn);
            var typeColumn = new DataColumn("Type");
            tasksDataSource.Columns.Add(typeColumn);
            var filenameColumn = new DataColumn("Filename");
            tasksDataSource.Columns.Add(filenameColumn);
            var actionColumn = new DataColumn("Action");
            tasksDataSource.Columns.Add(actionColumn);
            var amazonColumn = new DataColumn("Amazon");
            tasksDataSource.Columns.Add(amazonColumn);
            var amazonUrlColumn = new DataColumn("AmazonUrl");
            tasksDataSource.Columns.Add(amazonUrlColumn);
            var attemptsColumn = new DataColumn("Attempts");
            tasksDataSource.Columns.Add(attemptsColumn);
            var nextAttemptColumn = new DataColumn("NextAttempt");
            tasksDataSource.Columns.Add(nextAttemptColumn);

            DataRow lastRow = null;
            foreach (var entity in taskCollection)
            {
                var row = tasksDataSource.NewRow();
                row[createdColumn] = entity.CreatedUTC;
                row[typeColumn] = entity.TypeAsEnum.ToString();
                row[filenameColumn] = entity.PathFormat;
                row[actionColumn] = entity.ActionAsEnum.ToString();

                row[amazonColumn] = GetImageForTask(entity.CompletedOnAmazonUTC);

                row[amazonUrlColumn] = "";

                if (entity.ActionAsEnum == CloudProcessingTaskEntity.ProcessingAction.Upload)
                {
                    string filePath = string.Format("{0}/{1}", entity.Container, entity.PathFormat);

                    if (entity.CompletedOnAmazonUTC.GetValueOrDefault() > failedTime)
                        row[amazonUrlColumn] = string.Format(ObymobiConstants.AmazonBlobStorageUrlFormat, WebEnvironmentHelper.GetAmazonRootContainer(), filePath);
                }
                else if (entity.ActionAsEnum == CloudProcessingTaskEntity.ProcessingAction.Delete && lastRow != null)
                {
                    // Get previous row and empty download links
                    lastRow[amazonUrlColumn] = "";
                }

                row[attemptsColumn] = entity.Attempts;

                if (IsCompleted(entity))
                    row[nextAttemptColumn] = "";
                else
                    row[nextAttemptColumn] = entity.NextAttemptUTC;

                lastRow = row;
                tasksDataSource.Rows.Add(row);
            }

            taskGrid.DataSource = tasksDataSource;
            taskGrid.DataBind();
        }

        string GetImageForTask(DateTime? dateTimeValue)
        {
            if (dateTimeValue.HasValue)
            {
                if (dateTimeValue.Value > failedTime)
                {
                    // Success
                    return "~/Images/Icons/true.png";
                }

                // Failed
                return "~/Images/Icons/false.png";
            }

            return "~/Images/Icons/time.png";
        }

        bool IsCompleted(CloudProcessingTaskEntity entity)
        {
            return (entity.CompletedOnAmazonUTC.GetValueOrDefault() > failedTime);
        }

        public CloudProcessingTaskCollection DataSourceAsCloudProcessingTaskCollection
        {
            get
            {
                CloudProcessingTaskCollection collection = null;
                PageLLBLGenEntityCms pageEntity = (this.Page as PageLLBLGenEntityCms);
                if (pageEntity != null && pageEntity.DataSource != null)
                {
                    if (pageEntity.DataSource is EntertainmentEntity)
                        collection = ((EntertainmentEntity)pageEntity.DataSource).CloudProcessingTaskCollection;
                    else if (pageEntity.DataSource is ReleaseEntity)
                        collection = ((ReleaseEntity)pageEntity.DataSource).CloudProcessingTaskCollection;
                }

                return collection;
            }
        }
    }
}