﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Generic.UserControls.CloudTaskCollection" Codebehind="CloudTaskCollection.ascx.cs" %>
<D:Button ID="refreshTasksGrid" Text="Refresh" runat="server" /><br /><br />
<X:GridView ID="taskGrid" ClientInstanceName="tasksGrid" runat="server" Width="100%">
    <SettingsBehavior AutoFilterRowInputDelay="350" />                            
    <SettingsPager PageSize="30"></SettingsPager>
    <SettingsBehavior AllowGroup="false" AllowDragDrop="false" />
    <Columns>
        <dxwgv:GridViewDataDateColumn FieldName="Created" VisibleIndex="0" SortIndex="0"></dxwgv:GridViewDataDateColumn>
        <dxwgv:GridViewDataTextColumn FieldName="Type" VisibleIndex="1"></dxwgv:GridViewDataTextColumn> 
        <dxwgv:GridViewDataTextColumn FieldName="Filename" VisibleIndex="2"></dxwgv:GridViewDataTextColumn> 
        <dxwgv:GridViewDataTextColumn FieldName="Action" VisibleIndex="3"></dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataHyperLinkColumn FieldName="AmazonUrl" Caption="Amazon" VisibleIndex="5">
            <PropertiesHyperLinkEdit ImageUrlField="Amazon" Target="_blank" />
        </dxwgv:GridViewDataHyperLinkColumn>
        <dxwgv:GridViewDataTextColumn FieldName="Attempts" VisibleIndex="6"></dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataDateColumn FieldName="NextAttempt" VisibleIndex="7"></dxwgv:GridViewDataDateColumn>
    </Columns>
</X:GridView>