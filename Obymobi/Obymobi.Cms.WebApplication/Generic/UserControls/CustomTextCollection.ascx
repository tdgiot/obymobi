﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Generic.UserControls.CustomTextCollection" Codebehind="CustomTextCollection.ascx.cs" %>
<%@ Register Assembly="Dionysos.Web" Namespace="Dionysos.Web.UI.WebControls" TagPrefix="cc1" %>

<D:PlaceHolder runat="server" ID="plhAddCulture" Visible="false">
<table class="dataformV2">
	<tr>
		<td class="label">
		    <D:Label runat="server" id="lblCultureCode">Culture</D:Label>		
		</td>	
        <td class="control">
            <X:ComboBox runat="server" ID="ddlCultureCode" TextField="Name" ValueField="Code" style="float: left; width: 90%; margin-right: 4px;" notdirty="true"/>
            <D:Button runat="server" id="btnAdd" Text="Add" style="float:left;"/>
		</td>	
        <td class="label">            
        </td>
        <td class="control">
        </td>
	</tr>
</table>
</D:PlaceHolder>
<D:PlaceHolder runat="server" ID="plhCultures" Visible="true">
</D:PlaceHolder>