﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPageEntityCollection.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Generic.Entertainments" Title="Producten" Codebehind="Entertainments.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
     <X:ToolBarButton runat="server" ID="btChangeSettings" CommandName="Refresh" Text="Verversen" Image-Url="~/Images/Icons/table_refresh.png" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPrePageContent" Runat="Server">
    <table cellpadding="0" cellspacing="0" style="margin-bottom: 14px">
        <tr>
            <td>
                <D:Label runat="server" ID="lblOnlyCompanySpecific">Alleen bedrijfsspecifieke amusementen</D:Label>
            </td>
            <td style="padding-left: 4px">
                <D:CheckBox runat="server" ID="cbCompanySpecific" />
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cplhPostPageContent" Runat="Server">
</asp:Content>
