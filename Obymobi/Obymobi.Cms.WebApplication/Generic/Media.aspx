<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master"  EnableSessionState="ReadOnly" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Generic.Media" Title="Document" Codebehind="Media.aspx.cs" %>
<%@ Reference VirtualPath="~/Generic/SubPanels/MediaRatioPanel.ascx" %>
<%@ Reference VirtualPath="~/Generic/SubPanels/MediaRatioTypesPanel.ascx" %>

<asp:Content runat="server" ContentPlaceHolderID="cplhTitle"><D:Label runat="server" ID="lblTitleDocument">Afbeelding</D:Label> - <D:Label runat="server" ID="lblTitleEdit">Bewerken</D:Label> - <D:Label runat="server" ID="lblTitleMediaName" LocalizeText="false">Nieuw</D:Label></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
    <X:ToolBarButton runat="server" ID="btPush" CommandName="Push" Text="Push" Visible="false" />
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="cplhPageContent">
    <script type="text/javascript">
        window.onload = function() {
            var count = SelectedMediaLanguage();
            ShowHideAgnosticImages(count > 0);
        };

        function ChangeState(checkboxlist, state) {
            var cboxes = checkboxlist.getElementsByTagName("input");
            for (var i = 0; i < cboxes.length; i++) {
                cboxes[i].checked = state;
            }
        }

        function CheckAll(cbox, cboxlistId) {                     
            var cbList = document.getElementsByName(cboxlistId);
            if (cbList.length > 0) {
                ChangeState(cbList[0], cbox.checked);
            }
        }

        function MediaLanguageAgnosticClick(cbox) {
            if (cbox.checked) {
                if (lbMediaCultureCultures != null) {
                    lbMediaCultureCultures.UnselectAll();
                }

                ShowHideAgnosticImages(false);
            } else {
                var selectedCount = SelectedMediaLanguage();
                if (selectedCount == 0) {
                    cbox.checked = true;
                    ShowHideAgnosticImages(false);
                }
            }
        }

        function MediaLanguageClick() {            
            var selectedCount = SelectedMediaLanguage();            
            var convertedId = "ctl00_ctl00_cplhContentHolder_cplhPageContent_tabsMain_cbMediaCultureAgnostic";
            var cbAgnostic = document.getElementById(convertedId);
            cbAgnostic.checked = (selectedCount == 0);

            if (selectedCount > 0) {
                ShowHideAgnosticImages(true);
            } else {
                ShowHideAgnosticImages(false);
            }
        }

        function SelectedMediaLanguage() {
            var selectedCount = 0;
            if (lbMediaCultureCultures != null) {
                selectedCount = lbMediaCultureCultures.GetSelectedValues().length;
            }
            return selectedCount;
        }

        function ShowHideAgnosticImages(visible) {            
            var phlAgnosticImages = document.getElementById("ctl00_ctl00_cplhContentHolder_cplhPageContent_tabsMain_agnoticImagesPlaceholder");
            if (phlAgnosticImages != null) {
                phlAgnosticImages.style.display = visible ? "block" : "none";
            }
        }

        function HideElement(id)
        {            
            var element = document.getElementById(id);
            if (element != null)
            {                
                element.style.visibility = "hidden";
                element.style.display = "none";
            }                
        }

    </script>
    <style type="text/css">
        .cdnTable {            
        }

        .cdnTable td {            
            padding: 2px;            
            border:1px dotted #bed4dc;
            border-left:0;
            border-top:0;
        }

        .cdnTable th {
            width:85px;
            border:1px dotted #bed4dc;
            border-left:0;
            border-top:0;
        }

            .cdnTable td a {
                display:block;
                width:100%;
                height:20px;
                text-decoration:none;             
            }

            .cdnTable td.NotAvailable {
                background:url("../images/icons/cross.png") no-repeat center;
            } 

            .cdnTable td.OutOfDate {
                background:url("../images/icons/cross.png") no-repeat center;
            } 

            .cdnTable td.UpToDate {
                background:url("../images/icons/tick.png") no-repeat center;
            } 

            .cdnTable td.NotApplicable {
                background:url("../images/icons/bullet_orange.png") no-repeat center;
            }
            
            .cdnTable td.refresh {
                background:url("../images/icons/arrow_refresh.png") no-repeat center;
            }             

    </style>
    <X:PageControl runat="server" ID="tabsMain" Width="100%" EnableViewState="true">
        <ClientSideEvents ActiveTabChanged="function(s ,e) { initCropTools() }"  />
        <TabPages>
            <X:TabPage Text="Bronbestand" Name="Generic">
                <Controls>
                    <div class="divMediaLeft">                        
                        <table class="dataformV2">
                            <tr>
						        <td class="label">
							        <D:Label runat="server" ID="lblDocument">Afbeelding</D:Label>
						        </td>
						        <td class="control">
							        <D:FileUpload runat="server" ID="fuDocument" IsRequired="true" /> 
							        <D:PlaceHolder runat="server" ID="plhDeleteDocument">
								        <D:LinkButton runat="server" ID="hlDeleteDocument" PreSubmitWarning="Weet u zeker dat u de huidige afbeelding wilt verwijderen?">Huidige bestand verwijderen</D:LinkButton>
							        </D:PlaceHolder>                       
						        </td>         
					        </tr>
					        <tr>
                                <td class="label">
							        <D:Label runat="server" ID="lblRelatedEntityName">Gekoppeld aan</D:Label>
						        </td>
						        <td class="control">
                                    <span><D:HyperLink runat="server" ID="hlAttachToEntity" LocalizeText="false">Bekijken</D:HyperLink></span>
						        </td>        
					        </tr>	
                            <D:PlaceHolder runat="server" ID="plhAgnosticMedia" Visible="false">
                            <tr>
                                <td class="label"> 
                                    <D:Label runat="server" ID="lblAgnosticMedia">Linked to agnostic media</D:Label>
                                </td>
                                <td class="control"> 
                                    <span><D:HyperLink runat="server" ID="hlAgnosticMedia" LocalizeText="false">Bekijken</D:HyperLink></span>
                                </td>
                            </tr>
                            </D:PlaceHolder>
					        <tr>
                                <td class="label">
							        <D:Label runat="server" ID="lblJpgQuality">JPG kwaliteit</D:Label>
						        </td>
						        <td class="control">
                                    <D:TextBoxInt runat="server" ID="tbJpgQuality" Width="50" MinimalValue="5" MaximalValue="100" ThousandsSeperators="false" UseValidation="true"></D:TextBoxInt>
                                    <span><D:HyperLink runat="server" ID="hlJpgQuality" Visible="false" LocalizeText="false" style="font-weight: bold;"></D:HyperLink></span>
						        </td>        
					        </tr>	
                            <D:PlaceHolder runat="server" ID="plhZoomInformation" Visible="false">
					        <tr>
                                <td class="label">
							        <D:Label runat="server" ID="lblSizeMode">Standaard Zoom</D:Label>
						        </td>
						        <td class="control">
                                    <X:ComboBoxEnum runat="server" ID="cbSizeMode" Type="Obymobi.Enums.MediaSizeMode, Obymobi" Width="200px"></X:ComboBoxEnum>                                    
						        </td>        
					        </tr>
					        <tr>
                                <td class="label">
							        <D:Label runat="server" ID="lblZoomLevel">Zoom niveau</D:Label>
						        </td>
						        <td class="control">
                                    <D:TextBoxInt runat="server" ID="tbZoomLevel" MinimalValue="0" MaximalValue="100" Width="25"></D:TextBoxInt>
						        </td>        
					        </tr>  
                            </D:PlaceHolder>                          
                            <D:PlaceHolder runat="server" ID="plhMediaRatioTypeInformation" />
                        </table>                              
                        <div id="pnlMediaRatioTypes">
                            <D:PlaceHolder runat="server" ID="plhMediaRatioTypes">
                            </D:PlaceHolder>
                        </div>
                    </div>
                    <div class="divMediaRight">
                        <D:Image ID="imgMedia" runat="server" /><br />
                        <D:LinkButton runat="server" ID="hlDownloadOriginalImage">Download original image</D:LinkButton>                        
                    </div>
			    </Controls>
            </X:TabPage>
            <X:TabPage Text="Cultures" Name="Cultures">
                <Controls>
                    <div class="divMediaLeft">
				        <table class="dataformV2">
					        <tr>
						        <td class="label">
                                    <D:Label runat="server" ID="lblMediaCultures">Cultures</D:Label>
                                </td>
                                <td class="control">
                                    <D:CheckBox ID="cbMediaCultureAgnostic" runat="server" CssClass="checkbox-checkall" onclick="MediaLanguageAgnosticClick(this, 'lbMediaCultureCultures');" LocalizeText="false"/>
                                    <X:ListBox runat="server" ID="lbMediaCultureCultures" ClientInstanceName="lbMediaCultureCultures" SelectionMode="CheckColumn" TextField="Name" ValueField="Code">
                                        <ClientSideEvents SelectedIndexChanged="MediaLanguageClick"></ClientSideEvents>
                                    </X:ListBox>
                                    <br/>
                                    <D:Label runat="server" TranslationTag="MediaCultureNote" CssClass="nobold">(*) = Company or Brand enabled culture</D:Label>
                                </td>       						        
					        </tr>
                        </table>
                    </div>
                    <div class="divMediaRight" id="agnoticImagesPlaceholder">
                        <D:PlaceHolder runat="server" ID="plhAgnosticImages"></D:PlaceHolder>
                    </div>
                </Controls>
            </X:TabPage>
            <X:TabPage Text="Action" Name="Action">
                <Controls>
				    <table class="dataformV2">
					    <tr>
						    <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblActionProductId">Product</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlActionProductId" IncrementalFilteringMode="StartsWith" EntityName="Product" TextField="Name" ValueField="ProductId" PreventEntityCollectionInitialization="true" />
                            </td>       
						    <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblActionCategoryId">Category</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlActionCategoryId" IncrementalFilteringMode="StartsWith" EntityName="Category" TextField="Name" ValueField="CategoryId" PreventEntityCollectionInitialization="true" />
                            </td>       
					    </tr>                        
                        <tr>
						    <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblActionEntertainmentId">Entertainment</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlActionEntertainmentId" IncrementalFilteringMode="StartsWith" EntityName="Entertainment" TextField="Name" ValueField="EntertainmentId" PreventEntityCollectionInitialization="true" />
                            </td>       
						    <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblActionEntertainmentcategoryId">Entertainmentcategory</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlActionEntertainmentcategoryId" IncrementalFilteringMode="StartsWith" EntityName="Entertainmentcategory" TextField="Name" ValueField="EntertainmentcategoryId" PreventEntityCollectionInitialization="true" />
                            </td>       
					    </tr>
                        <tr>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" ID="lblActionSiteId">Site</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlActionSiteId" IncrementalFilteringMode="StartsWith" EntityName="Site" TextField="Name" ValueField="SiteId" PreventEntityCollectionInitialization="true">
                                    <ClientSideEvents SelectedIndexChanged="function(s, e) {ddlActionPageId.PerformCallback('1');}" />
                                </X:ComboBoxLLBLGenEntityCollection>
                            </td>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" ID="lblActionPageId">Site pagina</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlActionPageId" ClientInstanceName="ddlActionPageId" OnCallback="ddlActionPageId_OnCallback" IncrementalFilteringMode="StartsWith" EntityName="Page" TextField="Name" ValueField="PageId" PreventEntityCollectionInitialization="true" />
                            </td>
                        </tr>                        
                        <tr>
						    <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblActionUrl">URL</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <D:TextBoxString ID="tbActionUrl" runat="server" IsRequired="false"></D:TextBoxString>
                            </td>       
						    <td class="label">
						    </td>
						    <td class="control">
						    </td>        
					    </tr>
				    </table>   
			    </Controls>
            </X:TabPage>
            <X:TabPage Text="CDN" Name="CDN">
                <Controls>
                    <D:PlaceHolder runat="server">                    
                    <table cellpadding="0" cellspacing="0" class="cdnTable">
                        <tr>
                            <td><D:LinkButton ID="lbRefresh" runat="server" Text="Refresh" style="text-decoration: underline;"></D:LinkButton></td>
                            <th><D:LabelTextOnly runat="server" LocalizeText="true" ID="lblCdnAmazon">Amazon</D:LabelTextOnly></th>
                            <th><D:LabelTextOnly runat="server" LocalizeText="true" ID="lblCdnRequeue">Requeue</D:LabelTextOnly></th>
                        </tr>
                        <tr>
                            <td><D:LabelTextOnly runat="server" LocalizeText="true" ID="lblRequeueAll">Requeue all</D:LabelTextOnly></td>
                            <td></td>                                                        
                            <td class="refresh"><D:LinkButton runat="server" ID="btRefreshAllCdn"></D:LinkButton></td>
                        </tr>
                        <D:PlaceHolder runat="server" ID="plhCdnStatus"></D:PlaceHolder>       
                        <tr>
                            <td></td>
                            <td colspan="3" style="text-align: center;">
                                <D:Button ID="btGetMediaProcessingQueueStatus" runat="server" Text="Get Media Processing Queue Status" OnClientClick="aspnetForm.target='_blank';" />
                            </td>
                        </tr>
                    </table>
                    </D:PlaceHolder>
                </Controls>
            </X:TabPage>
        </TabPages>
    </X:PageControl>
    <script type="text/javascript">
        window.addEvent('load', function () { initCropTools(); });
    </script>

</asp:Content>
