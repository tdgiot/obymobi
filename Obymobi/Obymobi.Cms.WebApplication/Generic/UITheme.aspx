﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Generic.UITheme" Codebehind="UITheme.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblName">Name</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
							</td>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblCompanyId">Company</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlCompanyId" IncrementalFilteringMode="StartsWith" EntityName="Company" TextField="Name" ValueField="CompanyId" PreventEntityCollectionInitialization="true" />
                            </td>
						</tr>
                    </table>		
                    
                    <D:Placeholder runat="server" ID="plhControls" />

				</controls>
			</X:TabPage>
            <X:TabPage Text="Text sizes" Name="TextSizes">
                <Controls>
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:Label runat="server" id="lblSetDefaultTextSizes">Set default text sizes</D:Label>
							</td>
							<td class="control">
                                <D:Button ID="btnSetT2DefaultTextSizes" runat="server" Text="T2" LocalizeText="false" PreSubmitWarning="Are you sure that you want to overwrite ALL text sizes with the T2 defaults?" />                    								
                                &nbsp;
                                <D:Button ID="btnSetTminiDefaultTextSizes" runat="server" Text="T-mini" LocalizeText="false" PreSubmitWarning="Are you sure that you want to overwrite ALL text sizes with the T-mini defaults?" />                    								
							</td>
                            <td class="label">
                            </td>
                            <td class="control">
                            </td>
						</tr>
                    </table>
                    <D:Placeholder runat="server" ID="plhTextSizes" />
                </Controls>
            </X:TabPage>

        </TabPages>
	</X:PageControl>
</div>
</asp:Content>