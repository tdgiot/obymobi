﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Crave.Api.Logic.Requests.Publishing;
using Crave.Api.Logic.Timestamps;
using Crave.Api.Logic.UseCases;
using DevExpress.Web;
using Dionysos;
using Dionysos.Web.UI;
using Obymobi.Cms.Logic.HelperClasses;
using Obymobi.Constants;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Comet;
using Obymobi.ObymobiCms.UI;
using Obymobi.Security;
using Obymobi.Web.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Generic
{
    public partial class Map : PageLLBLGenEntityCms
    {
        #region Methods

        private void SetGui()
        {
            List<int> pointOfInterestIds = this.DataSourceAsMapEntity.MapPointOfInterestCollection.Select(x => x.PointOfInterestId).ToList();

            this.lbPois.DataSource = PointOfInterestHelper.GetPointOfInterests().OrderByDescending(x => pointOfInterestIds.Contains(x.PointOfInterestId)).ThenBy(x => x.Name);
            this.lbPois.DataBind();

            foreach (ListEditItem item in this.lbPois.Items)
            {
                if (pointOfInterestIds.Contains(int.Parse(item.Value.ToString())))
                {
                    item.Selected = true;
                }
            }
        }

        private void SavePointOfInterests(ListEditItemCollection items)
        {
            foreach (ListEditItem item in items)
            {
                int pointOfInterestId = int.Parse(item.Value.ToString());

                MapPointOfInterestEntity mapPointOfInterestEntity = this.DataSourceAsMapEntity.MapPointOfInterestCollection.FirstOrDefault(x => x.PointOfInterestId == pointOfInterestId);
                if (item.Selected && mapPointOfInterestEntity == null)
                {
                    mapPointOfInterestEntity = new MapPointOfInterestEntity();
                    mapPointOfInterestEntity.MapId = this.DataSourceAsMapEntity.MapId;
                    mapPointOfInterestEntity.PointOfInterestId = pointOfInterestId;
                    mapPointOfInterestEntity.Save();                    
                }
                else if (!item.Selected && mapPointOfInterestEntity != null)
                {
                    mapPointOfInterestEntity.Delete();
                    this.DataSourceAsMapEntity.MapPointOfInterestCollection.Remove(mapPointOfInterestEntity);
                }
            }
        }

        private void LoadUserControls()
        {
            this.cbMapProvider.DataBindEnum<MapProvider>();
            this.cbMapType.DataBindEnum<MapType>();

            CompanyCollection companies = new CompanyCollection();

            if (CmsSessionHelper.CurrentCompanyId > 0)
            {
                companies.GetMulti(CompanyFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            }
            else
            {
                companies.GetMulti(null);
            }

            if (CmsSessionHelper.CurrentRole < Role.Administrator)
            {
                this.ddlCompanyId.DisplayEmptyItem = false;
                this.ddlCompanyId.SelectedIndex = 0;
                this.ddlCompanyId.Enabled = false;
            }

            this.ddlCompanyId.DataSource = companies;
            this.ddlCompanyId.DataBind();
        }

        private void CopyMap()
        {
            if (this.DataSourceAsMapEntity.IsNew)
            {
                this.AddInformator(InformatorType.Warning, "Please save the map first before creating a copy.");
            }
            else if (this.tbNewMapName.Text.IsNullOrWhiteSpace())
            {
                this.AddInformator(InformatorType.Warning, "Please enter the new map name.");
            }
            else
            {
                MapEntity newMapEntity = MapHelper.Copy(this.DataSourceAsMapEntity, this.tbNewMapName.Text);
                if (newMapEntity == null)
                {
                    this.AddInformator(InformatorType.Warning, "Something went wrong trying to copy the map. Contact a developer.");
                }
                else
                {
                    newMapEntity.Save(true);
                    this.AddInformatorInfo("New map created: " + "<a href=\"{0}\">{1}</a>".FormatSafe(this.ResolveUrl("~/Generic/Map.aspx?id={0}".FormatSafe(newMapEntity.MapId)), newMapEntity.Name));
                }
            }

            this.Validate();
        }

        private void PushToClients(bool isTest)
        {
            if (!this.PublishToApi())
            {
                this.AddInformator(InformatorType.Warning, "Failed to push the map to one or more api's.");
            }
            else
            {
                ClientCollection clients = this.GetClientsToNotify(isTest);
                this.NotifyClients(clients);

                if (!isTest)
                {
                    this.AddInformator(InformatorType.Information, "Map has been pushed successfully. Refreshing {0} live clients.", clients.Count);
                }
                else
                {
                    this.AddInformator(InformatorType.Information, "Map has been pushed successfully. Refreshing {0} clients running in preview mode.", clients.Count);
                }
            }

            this.Validate();
        }

        private bool PublishToApi()
        {
            bool success = true;

            PublishHelper.Publish(CmsSessionHelper.CurrentCompanyId, (service, timestamp, mac, salt) =>
            {
                try
                {

                    File.AppendAllText(Server.MapPath("~/App_Data/Push.txt"), string.Format("{0} - {1} - {2} - Start map: {3}\n", DateTime.Now, "Generic", CmsSessionHelper.CurrentUser.Username, this.DataSourceAsMapEntity.Name));

                    string hash = Hasher.GetHashFromParameters(salt, timestamp, mac, this.DataSourceAsMapEntity.MapId);

                    var result = service.PublishMap(timestamp, mac, this.DataSourceAsMapEntity.MapId, hash);
                    if (result.ModelCollection.Length <= 0 || !result.ModelCollection[0].Succes)
                    {
                        File.AppendAllText(Server.MapPath("~/App_Data/Push.txt"), string.Format("{0} - {1} - {2} - PublishMap call failed: {3}\n", DateTime.Now, "Generic", CmsSessionHelper.CurrentUser.Username, this.DataSourceAsMapEntity.Name));
                        success = false;
                    }

                    result = service.PublishMapTimestamp(timestamp, mac, this.DataSourceAsMapEntity.MapId, hash);
                    if (result.ModelCollection.Length <= 0 || !result.ModelCollection[0].Succes)
                    {
                        File.AppendAllText(Server.MapPath("~/App_Data/Push.txt"), string.Format("{0} - {1} - {2} - PublishMapTImestamp call failed: {3}\n", DateTime.Now, "Generic", CmsSessionHelper.CurrentUser.Username, this.DataSourceAsMapEntity.Name));
                        success = false;
                    }

                    File.AppendAllText(Server.MapPath("~/App_Data/Push.txt"), string.Format("{0} - {1} - {2} - End map: {3}\n", DateTime.Now, "Generic", CmsSessionHelper.CurrentUser.Username, this.DataSourceAsMapEntity.Name));
                }
                catch (Exception ex)
                {
                    File.AppendAllText(Server.MapPath("~/App_Data/Push.txt"), string.Format("{0} - {1} - {2} - Exception: {3}\n", DateTime.Now, "Generic", CmsSessionHelper.CurrentUser.Username, ex.ToString()));
                }
            });

            return success;
        }

        private ClientCollection GetClientsToNotify(bool toTest)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(UITabFields.MapId == this.DataSourceAsMapEntity.MapId);
            filter.Add(ClientFields.IsTestClient == toTest);
            filter.Add(DeviceFields.LastRequestUTC >= DateTime.UtcNow.AddMilliseconds(ObymobiIntervals.DEVICE_OFFLINE_THRESHOLD * -1));

            RelationCollection relations = new RelationCollection();
            relations.Add(ClientEntityBase.Relations.DeviceEntityUsingDeviceId);
            relations.Add(ClientEntityBase.Relations.DeliverypointgroupEntityUsingDeliverypointGroupId);
            relations.Add(DeliverypointgroupEntityBase.Relations.UIModeEntityUsingUIModeId);
            relations.Add(UIModeEntityBase.Relations.UITabEntityUsingUIModeId);

            ClientCollection clients = new ClientCollection();
            clients.GetMulti(filter, relations);

            return clients;
        }

        private void NotifyClients(ClientCollection clients)
        {
            foreach (ClientEntity client in clients)
            {
                CometHelper.RefreshContent(client.ClientId);
            }
        }

        #endregion

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            this.DataSourceLoaded += Map_DataSourceLoaded;
            base.OnInit(e);
        }

        private void Map_DataSourceLoaded(object sender)
        {
            this.SetGui();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.DataSourceAsMapEntity.IsNew)
            {
                this.tbZoomLevel.Text = "12";
            }
            this.btPushToTestClients.Click += BtPushToTestClients_Click;
            this.btPushToLiveClients.Click += BtPushToLiveClients_Click;
            this.btnCopyMap.Click += BtnCopyMap_Click;
        }

        private void BtnCopyMap_Click(object sender, EventArgs e)
        {
            this.CopyMap();
        }

        private void BtPushToTestClients_Click(object sender, EventArgs e)
        {
            this.PushToClients(true);
        }

        private void BtPushToLiveClients_Click(object sender, EventArgs e)
        {
            this.PushToClients(false);
        }

        public override bool Save()
        {
            if (base.Save())
            {
                this.SavePointOfInterests(this.lbPois.Items);
            }
            return true;
        }

        #endregion

        #region Properties

        public MapEntity DataSourceAsMapEntity
        {
            get
            {
                return this.DataSource as MapEntity;
            }
        }

		#endregion
    }
}
