﻿using System;
using System.IO;
using System.Linq;
using Dionysos;
using Newtonsoft.Json;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Obymobi.ObymobiCms.Generic.SubPanels;
using Obymobi.ObymobiCms.UI;

namespace Obymobi.ObymobiCms.Generic
{
    public partial class InfraredConfiguration : PageLLBLGenEntityCms
    {
        #region Fields

        private static InfraredCommandType[] customButtonTypes = { InfraredCommandType.CustomButton1, InfraredCommandType.CustomButton2, InfraredCommandType.CustomButton3 };

        private InfraredCommandsPanel panel = null;

        #endregion

        private void LoadUserControls()
        {
            this.panel = (InfraredCommandsPanel)this.LoadControl("~/Generic/SubPanels/InfraredCommandsPanel.ascx");
            this.plhCommandsPanel.Controls.Add(this.panel);

            this.btnCopy.Click += BtnCopy_Click;

            if (this.PageMode == Dionysos.Web.PageMode.Add)
            {
                this.tabsMain.TabPages.FindByName("Import").Visible = true;
                this.tabsMain.TabPages.FindByName("Copy").Visible = false;
            }
            else
            {
                this.tabsMain.TabPages.FindByName("Import").Visible = false;
                this.tabsMain.TabPages.FindByName("Copy").Visible = true;
            }
        }

        private void BtnImport_Click(object sender, EventArgs e)
        {
            if (!this.DataSourceAsInfraredConfigurationEntity.IsNew)
            {
                this.MultiValidatorDefault.AddError("Only new configurations can be created from .json files.");
            }
            else if (!this.fuDocument.HasFile)
            {
                this.MultiValidatorDefault.AddError("No .json file selected.");
            }
            else if (System.IO.Path.GetExtension(this.fuDocument.FileName).ToLower() != ".json")
            {
                this.MultiValidatorDefault.AddError("Only .json files allowed.");
            }
            else
            {
                try
                {
                    string json;
                    using (StreamReader reader = new StreamReader(this.fuDocument.FileContent))
                    {
                        json = reader.ReadToEnd();
                    }

                    Obymobi.Logic.Model.InfraredConfiguration model = JsonConvert.DeserializeObject<Obymobi.Logic.Model.InfraredConfiguration>(json);
                    InfraredConfigurationEntity entity = InfraredConfigurationHelper.ConvertInfraredConfigurationModelToEntity(model);
                    entity.Save(true);

                    this.Response.Redirect("~/Generic/InfraredConfiguration.aspx?id=" + entity.InfraredConfigurationId, false);                    
                }
                catch (Exception ex)
                {
                    this.MultiValidatorDefault.AddError("Something went wrong deserializing the .json file: " + ex.Message);
                }                
            }
            this.Validate();
        }

        private void BtnCopy_Click(object sender, EventArgs e)
        {
            if (this.DataSourceAsInfraredConfigurationEntity.IsNew)
            {
                this.MultiValidatorDefault.AddError("The configuration has to be saved first before creating a copy.");
            }
            else if (this.tbCopyName.Text.IsNullOrWhiteSpace())
            {
                this.MultiValidatorDefault.AddError("The configuration needs to have a name.");
            }
            else
            {
                InfraredConfigurationEntity newInfraredConfigurationEntity = new InfraredConfigurationEntity();
                newInfraredConfigurationEntity.Name = this.tbCopyName.Text;
                newInfraredConfigurationEntity.MillisecondsBetweenCommands = this.DataSourceAsInfraredConfigurationEntity.MillisecondsBetweenCommands;

                foreach (InfraredCommandEntity oldCommand in this.DataSourceAsInfraredConfigurationEntity.InfraredCommandCollection)
                {
                    InfraredCommandEntity newCommand = new InfraredCommandEntity();
                    newCommand.Name = oldCommand.Name;
                    newCommand.Type = oldCommand.Type;
                    newCommand.Hex = oldCommand.Hex;
                    newInfraredConfigurationEntity.InfraredCommandCollection.Add(newCommand);
                }

                newInfraredConfigurationEntity.Save(true);
                newInfraredConfigurationEntity.Refetch();


                this.AddInformatorInfo("The configuration is created: <a href=\"{0}\">{1}</a>".FormatSafe(this.ResolveUrl("~/Generic/InfraredConfiguration.aspx?id={0}".FormatSafe(newInfraredConfigurationEntity.InfraredConfigurationId)), newInfraredConfigurationEntity.Name));
            }
            this.Validate();
        }

        private void SetGui()
        {
            if (this.panel != null && !this.DataSource.IsNew)
            {
                this.panel.Load(this.DataSourceAsInfraredConfigurationEntity.InfraredConfigurationId, true);
                this.btnAddCustomCommand.Visible = true;
                this.btnCreateCommandsForRemote.Visible = true;
            }

            if (this.DataSource.IsNew)
            {
                this.lblMillisecondsBetweenCommands.Visible = false;
                this.tbMillisecondsBetweenCommands.IsRequired = false;
                this.tbMillisecondsBetweenCommands.Visible = false;
            }

            this.btnAddCustomCommand.Visible = !InfraredConfiguration.customButtonTypes.All(x => this.DataSourceAsInfraredConfigurationEntity.InfraredCommandCollection.Any(y => y.Type == x));
        }

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            this.DataSourceLoaded += this.InfraredConfiguration_DataSourceLoaded;
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.btnAddCustomCommand.Click += btnAddCustomCommand_Click;
            this.btnCreateCommandsForRemote.Click += btnCreateCommandsForRemote_Click;
            this.btnImport.Click += BtnImport_Click;
        }        

        private void btnAddCustomCommand_Click(object sender, EventArgs e)
        {
            foreach (InfraredCommandType customType in InfraredConfiguration.customButtonTypes)
            {
                if (this.DataSourceAsInfraredConfigurationEntity.InfraredCommandCollection.Any(x => x.Type == customType))
                    continue;

                InfraredCommandEntity command = new InfraredCommandEntity();
                command.InfraredConfigurationId = this.DataSourceAsInfraredConfigurationEntity.InfraredConfigurationId;
                command.Type = customType;
                command.Save();
                break;
            }

            this.Response.Redirect(this.Request.RawUrl);
        }

        private void btnCreateCommandsForRemote_Click(object sender, EventArgs e)
        {
            InfraredCommandType[] types = EnumUtil.ToArray<InfraredCommandType>();

            foreach (InfraredCommandType type in types.Where(ict => ict != InfraredCommandType.Custom))
            {
                if (!this.DataSourceAsInfraredConfigurationEntity.InfraredCommandCollection.Any(ict => ict.Type == type))
                {
                    InfraredCommandEntity command = new InfraredCommandEntity();
                    command.InfraredConfigurationId = this.DataSourceAsInfraredConfigurationEntity.InfraredConfigurationId;
                    command.Type = type;
                    command.Save();
                }
            }

            this.Response.Redirect(this.Request.RawUrl);
        }

        private void InfraredConfiguration_DataSourceLoaded(object sender)
        {
            this.SetGui();
        }

        public InfraredConfigurationEntity DataSourceAsInfraredConfigurationEntity
        {
            get { return (this.DataSource as InfraredConfigurationEntity); }
        }        
    }
}