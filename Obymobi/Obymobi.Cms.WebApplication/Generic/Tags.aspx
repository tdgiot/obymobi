﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPageEntityCollection.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Generic.Tags" Title="Tags" Codebehind="Tags.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPrePageContent" Runat="Server">
    <D:PlaceHolder runat="server" ID="gridFilter">
	    <table cellpadding="0" cellspacing="0" style="margin-bottom: 14px">
		    <tr>
			    <td>
				    <D:Label runat="server" ID="lblTagFilter">Display:  </D:Label>
			    </td>
			    <td style="padding-left: 10px">
				    <D:CheckBox runat="server" ID="cbShowCompanyTags"  AutoPostBack="true" Checked="true" />
				    <D:Label runat="server" ID="lblShowCompanyTags" style="padding-left: 5px">Company  </D:Label>
			    </td>
			    <td style="padding-left: 10px">
				    <D:CheckBox runat="server" ID="cbShowSystemTags" AutoPostBack="true" />
				    <D:Label runat="server" ID="lblShowSystemTags" style="padding-left: 5px">System  </D:Label>
			    </td>
		    </tr>
	    </table>
    </D:PlaceHolder>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cplhPostPageContent" Runat="Server">
</asp:Content>
