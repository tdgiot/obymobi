﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Generic.MessageTemplate" Codebehind="MessageTemplate.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" runat="Server">
    <div>
        <X:PageControl ID="tabsMain" runat="server" Width="100%">
            <TabPages>
                <X:TabPage Text="Algemeen" Name="Generic">
                    <controls>
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblTitle">Titel</D:LabelEntityFieldInfo>
							</td>
							<td class="control" >
								<D:TextBoxString ID="tbTitle" runat="server" IsRequired="true"></D:TextBoxString>
							</td>
                        </tr>
                        <tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblMessage">Tekst</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxMultiLine runat="server" ID="tbMessage"></D:TextBoxMultiLine>
							</td>
					    </tr>	
                    </table>
                    </controls>
                </X:TabPage>
            </TabPages>
        </X:PageControl>
    </div>
</asp:Content>
