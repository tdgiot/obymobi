﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Web.UI.WebControls;
using Obymobi.Enums;
using Obymobi.ObymobiCms.UI;
using Obymobi.Logic.Cms;
using Dionysos.Web.UI.DevExControls;
using DevExpress.Web;
using Dionysos.Data.LLBLGen;
using Obymobi.Logic.HelperClasses;
using Dionysos.Web.Google.Geocoding;
using Dionysos;
using Obymobi.Data;
using System.Drawing;
using Obymobi.Logic.Extensions;

namespace Obymobi.ObymobiCms.Generic
{
    public partial class AccessCode : PageLLBLGenEntityCms
    {
		#region Methods

		protected override void OnInit(EventArgs e)
		{
            this.LoadUserControls();
            this.DataSourceLoaded += AccessCode_DataSourceLoaded;
			base.OnInit(e);			
		}

        private void LoadUserControls()
        {
            this.LoadCompanies();
            this.LoadCompanyCities();
            this.LoadPointOfInterestCities();
        }

        private void LoadCompanies()
        {
            CompanyCollection companyCollection = EntityCollection.GetMulti<CompanyCollection>(null, null, CompanyFields.Name);
            foreach (CompanyEntity companyEntity in companyCollection)
            {
                if (!CmsSessionHelper.ShouldHideCompany(companyEntity))
                {
                    this.cblCompanies.Items.Add(companyEntity.Name, companyEntity.CompanyId);
                }
            }
        }

        private void LoadCompanyCities()
        {
            CompanyCollection companyCollection = EntityCollection.GetMulti<CompanyCollection>(null, null, CompanyFields.City);
            List<string> companyCityList = new List<string>();
            foreach (CompanyEntity companyEntity in companyCollection)
            {
                string city = companyEntity.City.Trim();
                if (!city.IsNullOrWhiteSpace() && !companyCityList.Contains(city, true))
                    companyCityList.Add(city);
            }
            companyCityList.Sort();

            this.cbAddCompaniesFromCity.Items.Clear();
            this.cbAddCompaniesFromCity.Items.Add("Select a city ...");
            foreach (string city in companyCityList)
            {
                this.cbAddCompaniesFromCity.Items.Add(city);
            }
            this.cbAddCompaniesFromCity.SelectedIndex = 0;
        }

        private void LoadPointOfInterestCities()
        {
            PointOfInterestCollection poiCollection = EntityCollection.GetMulti<PointOfInterestCollection>(null, null, PointOfInterestFields.City);
            List<string> poiCityList = new List<string>();
            foreach (PointOfInterestEntity poiEntity in poiCollection)
            {
                string city = poiEntity.City.Trim();
                if (!city.IsNullOrWhiteSpace() && !poiCityList.Contains(city, true))
                    poiCityList.Add(city);
            }
            poiCityList.Sort();

            this.cbAddPointsOfInterestFromCity.Items.Clear();
            this.cbAddPointsOfInterestFromCity.Items.Add("Select a city ...");
            foreach (string city in poiCityList)
            {
                this.cbAddPointsOfInterestFromCity.Items.Add(city);
            }
            this.cbAddPointsOfInterestFromCity.SelectedIndex = 0;
        }

        private void RenderHeroCompanies()
        {
            var filter = new PredicateExpression(AccessCodeCompanyFields.AccessCodeId == this.DataSourceAsAccessCodeEntity.AccessCodeId);
            var relations = new RelationCollection(AccessCodeCompanyEntity.Relations.CompanyEntityUsingCompanyId);
            var sort = new SortExpression(CompanyFields.Name | SortOperator.Ascending);
            var prefetch = new PrefetchPath(EntityType.AccessCodeCompanyEntity);
            prefetch.Add(AccessCodeCompanyEntity.PrefetchPathCompanyEntity);

            AccessCodeCompanyCollection accessCodeCompanyCollection = EntityCollection.GetMulti<AccessCodeCompanyCollection>(filter, sort, relations, prefetch, null);

            this.cblShowOnCarrouselCompanies.Items.Clear();
            foreach (AccessCodeCompanyEntity accessCodeCompanyEntity in accessCodeCompanyCollection)
            {
                ListEditItem listEditItem = new ListEditItem(accessCodeCompanyEntity.CompanyEntity.Name, accessCodeCompanyEntity.AccessCodeCompanyId);
                listEditItem.Selected = accessCodeCompanyEntity.IsHero;
                this.cblShowOnCarrouselCompanies.Items.Add(listEditItem);
            }
        }

        private void RenderHeroPointsOfInterest()
        {
            var filter = new PredicateExpression(AccessCodePointOfInterestFields.AccessCodeId == this.DataSourceAsAccessCodeEntity.AccessCodeId);
            var relations = new RelationCollection(AccessCodePointOfInterestEntity.Relations.PointOfInterestEntityUsingPointOfInterestId);
            var sort = new SortExpression(PointOfInterestFields.Name | SortOperator.Ascending);
            var prefetch = new PrefetchPath(EntityType.AccessCodePointOfInterestEntity);
            prefetch.Add(AccessCodePointOfInterestEntity.PrefetchPathPointOfInterestEntity);

            AccessCodePointOfInterestCollection accessCodePointOfInterestCollection = EntityCollection.GetMulti<AccessCodePointOfInterestCollection>(filter, sort, relations, prefetch, null);

            this.cblShowOnCarrouselPointsOfInterest.Items.Clear();
            foreach (AccessCodePointOfInterestEntity accessCodePointOfInterestEntity in accessCodePointOfInterestCollection)
            {
                ListEditItem listEditItem = new ListEditItem(accessCodePointOfInterestEntity.PointOfInterestEntity.Name, accessCodePointOfInterestEntity.AccessCodePointOfInterestId);
                listEditItem.Selected = accessCodePointOfInterestEntity.IsHero;
                this.cblShowOnCarrouselPointsOfInterest.Items.Add(listEditItem);
            }
        }

        private void RenderSelection()
        {
            // Select the companies
            List<int> companyIds = this.DataSourceAsAccessCodeEntity.AccessCodeCompanyCollection.Select(company => company.CompanyId).ToList();
            foreach (ListEditItem item in this.cblCompanies.Items)
            {
                if (companyIds.Contains((int)item.Value))
                    item.Selected = true;
            }

            // Select the POIs
            List<int> poiIds = this.DataSourceAsAccessCodeEntity.AccessCodePointOfInterestCollection.Select(poi => poi.PointOfInterestId).ToList();
            foreach (ListEditItem item in this.cblPointsOfInterest.Items)
            {
                if (poiIds.Contains((int)item.Value))
                    item.Selected = true;
            }
        }

        public override bool Save()
        {
            this.Validate();

            if (this.DataSourceAsAccessCodeEntity.IsNew && this.IsValid)
            {
                this.DataSourceAsAccessCodeEntity.Code = this.tbCode.Value;
                this.DataSourceAsAccessCodeEntity.Type = (AccessCodeType)this.cbType.Value.Value;
            }

            bool success = base.Save();

            // Save the selected companies for this access code
            this.SaveCompaniesListBox(this.cblCompanies.Items);

            // Save the selected points of interest for this access code
            this.SavePointsOfInterestListBox(this.cblPointsOfInterest.Items);

            // Save the hero flags for the selected companies
            this.SaveHeroCompaniesListBox(this.cblShowOnCarrouselCompanies.Items);

            // Save the hero flags for the selected points of interest
            this.SaveHeroPointsOfInterestListBox(this.cblShowOnCarrouselPointsOfInterest.Items);

            return success;
        }

        /// <summary>
        /// Loop through a listbox looking for changes in selected/unselected items
        /// </summary>
        /// <param name="items">The items of a listbox where to loop through</param>
        private void SaveCompaniesListBox(ListEditItemCollection items)
        {
            foreach (ListEditItem item in items)
            {
                int companyId = (int)item.Value;

                var accessCodeCompany = this.DataSourceAsAccessCodeEntity.AccessCodeCompanyCollection.FirstOrDefault(x => x.CompanyId == companyId);

                if (item.Selected && accessCodeCompany == null)
                {
                    // Create the access code company entity
                    AccessCodeCompanyEntity acc = new AccessCodeCompanyEntity();
                    acc.CompanyId = companyId;
                    acc.AccessCodeId = this.DataSourceAsAccessCodeEntity.AccessCodeId;
                    acc.Save();
                }
                else if (!item.Selected && accessCodeCompany != null)
                {
                    // Delete the record
                    accessCodeCompany.Delete();
                    this.DataSourceAsAccessCodeEntity.AccessCodeCompanyCollection.Remove(accessCodeCompany);
                }
            }
        }

        /// <summary>
        /// Loop through a listbox looking for changes in selected/unselected items
        /// </summary>
        /// <param name="items">The items of a listbox where to loop through</param>
        private void SavePointsOfInterestListBox(ListEditItemCollection items)
        {
            foreach (ListEditItem item in items)
            {
                int poiId = (int)item.Value;

                var accessCodePoi = this.DataSourceAsAccessCodeEntity.AccessCodePointOfInterestCollection.FirstOrDefault(x => x.PointOfInterestId == poiId);

                if (item.Selected && accessCodePoi == null)
                {
                    // Create the access code point of interest entity
                    AccessCodePointOfInterestEntity acpoi = new AccessCodePointOfInterestEntity();
                    acpoi.PointOfInterestId = poiId;
                    acpoi.AccessCodeId = this.DataSourceAsAccessCodeEntity.AccessCodeId;
                    acpoi.Save();
                }
                else if (!item.Selected && accessCodePoi != null)
                {
                    // Delete the record
                    accessCodePoi.Delete();
                    this.DataSourceAsAccessCodeEntity.AccessCodePointOfInterestCollection.Remove(accessCodePoi);
                }
            }
        }

        /// <summary>
        /// Loop through a listbox looking for changes in selected/unselected items
        /// </summary>
        /// <param name="items">The items of a listbox where to loop through</param>
        private void SaveHeroCompaniesListBox(ListEditItemCollection items)
        {
            foreach (ListEditItem item in items)
            {
                int accessCodeCompanyId = Convert.ToInt32(item.Value);
                var accessCodeCompanyEntity = this.DataSourceAsAccessCodeEntity.AccessCodeCompanyCollection.FirstOrDefault(x => x.AccessCodeCompanyId == accessCodeCompanyId);

                if (accessCodeCompanyEntity == null)
                {
                    // Company was removed from access code
                }
                else
                {
                    accessCodeCompanyEntity.IsHero = item.Selected;
                    accessCodeCompanyEntity.Save();
                }
            }
        }

        /// <summary>
        /// Loop through a listbox looking for changes in selected/unselected items
        /// </summary>
        /// <param name="items">The items of a listbox where to loop through</param>
        private void SaveHeroPointsOfInterestListBox(ListEditItemCollection items)
        {
            foreach (ListEditItem item in items)
            {
                int accessCodePointOfInterestId = Convert.ToInt32(item.Value);
                var accessCodePointOfInterestEntity = this.DataSourceAsAccessCodeEntity.AccessCodePointOfInterestCollection.FirstOrDefault(x => x.AccessCodePointOfInterestId == accessCodePointOfInterestId);

                if (accessCodePointOfInterestEntity == null)
                {
                    // PointOfInterest was removed from access code
                }
                else
                {
                    accessCodePointOfInterestEntity.IsHero = item.Selected;
                    accessCodePointOfInterestEntity.Save();
                }
            }
        }

        private void SetWarnings()
        {
            if (!this.DataSourceAsAccessCodeEntity.IsNew)
            {
                if (this.DataSourceAsAccessCodeEntity.Code.StartsWith("Crave.", StringComparison.InvariantCultureIgnoreCase))
                {
                    PredicateExpression companyFilter = new PredicateExpression();
                    companyFilter.Add(CompanyVenueCategoryFields.CompanyVenueCategoryId == DBNull.Value);
                    companyFilter.Add(AccessCodeCompanyFields.AccessCodeId == this.DataSourceAsAccessCodeEntity.AccessCodeId);

                    RelationCollection companyRelations = new RelationCollection();
                    companyRelations.Add(CompanyEntity.Relations.CompanyVenueCategoryEntityUsingCompanyId, JoinHint.Left);
                    companyRelations.Add(CompanyEntity.Relations.AccessCodeCompanyEntityUsingCompanyId);

                    CompanyCollection companies = new CompanyCollection();
                    companies.GetMulti(companyFilter, companyRelations);

                    foreach (CompanyEntity company in companies)
                    {
                        this.AddInformator(Dionysos.Web.UI.InformatorType.Warning, this.Translate("CompanyHasNoVenueCategory-" + company.CompanyId, string.Format("Bedrijf '{0}' is niet gekoppeld aan een categorie en wordt daarom niet getoond in de exclusieve app modus.", company.Name)));
                    }

                    PredicateExpression poiFilter = new PredicateExpression();
                    poiFilter.Add(PointOfInterestVenueCategoryFields.PointOfInterestVenueCategoryId == DBNull.Value);
                    poiFilter.Add(AccessCodePointOfInterestFields.AccessCodeId == this.DataSourceAsAccessCodeEntity.AccessCodeId);

                    RelationCollection poiRelations = new RelationCollection();
                    poiRelations.Add(PointOfInterestEntity.Relations.PointOfInterestVenueCategoryEntityUsingPointOfInterestId, JoinHint.Left);
                    poiRelations.Add(PointOfInterestEntity.Relations.AccessCodePointOfInterestEntityUsingPointOfInterestId);

                    PointOfInterestCollection pois = new PointOfInterestCollection();
                    pois.GetMulti(poiFilter, poiRelations);

                    foreach (PointOfInterestEntity poi in pois)
                    {
                        this.AddInformator(Dionysos.Web.UI.InformatorType.Warning, this.Translate("PointOfInterestHasNoVenueCategory-" + poi.PointOfInterestId, string.Format("Point-of-interest '{0}' is niet gekoppeld aan een categorie en wordt daarom niet getoond in de exclusieve app modus.", poi.Name)));
                    }

                    this.Validate();
                }
            }
        }

        private void HookupEvents()
        {
            this.btnAddCompaniesFromCity.Click += btnAddCompaniesFromCity_Click;
            this.btnAddPointsOfInterestFromCity.Click += btnAddPointsOfInterestFromCity_Click;
        }

        private void AddCompaniesFromCity()
        {
            if (this.cbAddCompaniesFromCity.SelectedIndex == 0)
            {
                this.MultiValidatorDefault.AddError(this.Translate("no-city-selected-to-add-companies", "Please select a city from the list to add companies to this access code."));
                this.Validate();
            }
            else
            {
                // Get the companies for the selected city
                CompanyCollection companyCollection = EntityCollection.GetMulti<CompanyCollection>(CompanyFields.City == this.cbAddCompaniesFromCity.Text, null, CompanyFields.City);

                // Get the companies already added to this access code
                EntityView<AccessCodeCompanyEntity> accessCodeCompanyView = this.DataSourceAsAccessCodeEntity.AccessCodeCompanyCollection.DefaultView;
                PredicateExpression filter = null;

                // Walk through the companies and add them to the access code
                foreach (CompanyEntity companyEntity in companyCollection)
                {
                    if (CmsSessionHelper.ShouldHideCompany(companyEntity))
                    {
                        continue;
                    }

                    filter = new PredicateExpression();
                    filter.Add(AccessCodeCompanyFields.AccessCodeId == this.DataSourceAsAccessCodeEntity.AccessCodeId);
                    filter.Add(AccessCodeCompanyFields.CompanyId == companyEntity.CompanyId);
                    accessCodeCompanyView.Filter = filter;

                    if (accessCodeCompanyView.Count == 0)
                    {
                        AccessCodeCompanyEntity accessCodeCompanyEntity = new AccessCodeCompanyEntity();
                        accessCodeCompanyEntity.AccessCodeId = this.DataSourceAsAccessCodeEntity.AccessCodeId;
                        accessCodeCompanyEntity.CompanyId = companyEntity.CompanyId;
                        accessCodeCompanyEntity.Save();
                    }
                }

                // Reload the page
                Response.Redirect(Request.RawUrl);
            }
        }

        private void AddPointsOfInterestFromCity()
        {
            if (this.cbAddPointsOfInterestFromCity.SelectedIndex == 0)
            {
                this.MultiValidatorDefault.AddError(this.Translate("no-city-selected-to-add-pois", "Please select a city from the list to add points of interest to this access code."));
                this.Validate();
            }
            else
            {
                // Get the points of interest for the selected city
                PointOfInterestCollection pointOfInterestCollection = EntityCollection.GetMulti<PointOfInterestCollection>(PointOfInterestFields.City == this.cbAddPointsOfInterestFromCity.Text, null, PointOfInterestFields.City);

                // Get the points of interest already added to this access code
                EntityView<AccessCodePointOfInterestEntity> accessCodePointOfInterestView = this.DataSourceAsAccessCodeEntity.AccessCodePointOfInterestCollection.DefaultView;
                PredicateExpression filter = null;

                // Walk through the points of interest and add them to the access code
                foreach (PointOfInterestEntity pointOfInterestEntity in pointOfInterestCollection)
                {
                    filter = new PredicateExpression();
                    filter.Add(AccessCodePointOfInterestFields.AccessCodeId == this.DataSourceAsAccessCodeEntity.AccessCodeId);
                    filter.Add(AccessCodePointOfInterestFields.PointOfInterestId == pointOfInterestEntity.PointOfInterestId);
                    accessCodePointOfInterestView.Filter = filter;

                    if (accessCodePointOfInterestView.Count == 0)
                    {
                        AccessCodePointOfInterestEntity accessCodePointOfInterestEntity = new AccessCodePointOfInterestEntity();
                        accessCodePointOfInterestEntity.AccessCodeId = this.DataSourceAsAccessCodeEntity.AccessCodeId;
                        accessCodePointOfInterestEntity.PointOfInterestId = pointOfInterestEntity.PointOfInterestId;
                        accessCodePointOfInterestEntity.Save();
                    }
                }

                // Reload the page
                Response.Redirect(Request.RawUrl);
            }
        }

		#endregion

		#region Event Handlers
    
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
                this.SetWarnings();

            this.HookupEvents();
		}

        private void AccessCode_DataSourceLoaded(object sender)
        {
            if (!this.DataSourceAsAccessCodeEntity.IsNew)
            {
                this.tbCode.Visible = false;
                this.tbCode.IsRequired = false;
                this.cbType.Visible = false;
                this.cbType.IsRequired = false;

                this.lblCodeValue.Visible = true;
                this.lblCodeValue.Text = this.DataSourceAsAccessCodeEntity.Code;
                this.lblTypeValue.Visible = true;
                this.lblTypeValue.Text = this.DataSourceAsAccessCodeEntity.TypeText;

                this.RenderHeroCompanies();
                this.RenderHeroPointsOfInterest();

                // Select the items
                this.RenderSelection();
            }
        }

        private void btnAddCompaniesFromCity_Click(object sender, EventArgs e)
        {
            this.AddCompaniesFromCity();
        }

        private void btnAddPointsOfInterestFromCity_Click(object sender, EventArgs e)
        {
            this.AddPointsOfInterestFromCity();
        }

		#endregion

		#region Properties

        public AccessCodeEntity DataSourceAsAccessCodeEntity
        {
            get
            {
                return this.DataSource as AccessCodeEntity;
            }
        }

		#endregion
    }
}
