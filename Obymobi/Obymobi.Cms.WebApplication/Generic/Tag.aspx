﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Generic.Tag" Title="Tags" Codebehind="Tag.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" runat="Server">
</asp:Content>
<asp:Content ID="Conent2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" runat="Server">
    <div>
        <X:PageControl ID="tabsMain" runat="server" Width="100%">
            <TabPages>
                <X:TabPage Text="Generic" Name="Generic">
                    <Controls>
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblName">Name</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString id="tbName" runat="server" IsRequired="true"></D:TextBoxString>
							</td>
                            <td class="label"></td>
                            <td class="control"></td>
						</tr>
						<tr>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblTagType">Level</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <X:ComboBoxEnum runat="server" ID="cbTagType" DisplayEmptyItem="false" UseDataBinding="false" Type="Obymobi.Enums.TagType, Obymobi" />
                            </td>
                            <td class="label"></td>
                            <td class="control"></td>
                        </tr>
					 </table>					
				    </Controls>
                </X:TabPage>                       		
            </TabPages>
        </X:PageControl>
    </div>
</asp:Content>
