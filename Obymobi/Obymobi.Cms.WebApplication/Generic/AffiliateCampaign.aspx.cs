﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Dionysos;
using Dionysos.Web.UI.WebControls;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.ObymobiCms.UI;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Generic
{
    public partial class AffiliateCampaign : PageLLBLGenEntityCms
    {
        private AffiliatePartnerCollection Partners = new AffiliatePartnerCollection();
        public AffiliateCampaignEntity DataSourceAsAffiliateCampaignEntity => DataSource as AffiliateCampaignEntity;

        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            SetGui();
            base.OnInit(e);
            BuildPartnerCodeControls();
        }

        #endregion

        public override bool Save()
        {
            if (!IsCodeValid(tbCode.Value))
            {
                MultiValidatorDefault.AddError("The affiliate code must only contain alphanumeric characters without spaces.");
                Validate();
                return false;
            }

            if (!IsAffiliateCodeUnique(EntityId, tbCode.Value))
            {
                MultiValidatorDefault.AddError("The affiliate code is already used by another campaign.");
                Validate();
                return false;
            }

            if (!IsPartnerCodesValid())
            { return false; }

            if (pnlPartnerCodes.Visible)
            { PopulateAffiliateCampaignPartners(); }

            return Save(true);
        }

        private void SetGui() => pnlPartnerCodes.Visible = CmsSessionHelper.CurrentUser.HasFeature(FeatureToggle.MultipleCampaignCodes);

        private static bool IsCodeValid(string code) => Regex.IsMatch(code, "^[a-zA-Z0-9_-]*$");

        private string GetCode(int affiliatePartnerId) => ((TextBoxString)plhPartnerCodes.FindControl("tb-" + affiliatePartnerId))?.Text;

        private void PopulateAffiliateCampaignPartners()
        {
            List<AffiliateCampaignAffiliatePartnerEntity> campaignPartners = new List<AffiliateCampaignAffiliatePartnerEntity>();
            List<int> idsToDelete = new List<int>();

            foreach (int affiliatePartnerId in Partners.Select(x => x.AffiliatePartnerId))
            {
                string newCampaignCode = GetCode(affiliatePartnerId);
                AffiliateCampaignAffiliatePartnerEntity existingPartner = DataSourceAsAffiliateCampaignEntity.AffiliateCampaignAffiliatePartnerCollection.FirstOrDefault(x => x.AffiliatePartnerId == affiliatePartnerId);

                if (string.IsNullOrEmpty(newCampaignCode))
                {
                    if (existingPartner != null)
                    { idsToDelete.Add(existingPartner.AffiliateCampaignAffiliatePartnerId); }

                    continue;
                }

                AffiliateCampaignAffiliatePartnerEntity campaignPartner = existingPartner ?? new AffiliateCampaignAffiliatePartnerEntity();
                campaignPartner.AffiliatePartnerId = affiliatePartnerId;
                campaignPartner.AffiliateCampaignId = DataSourceAsAffiliateCampaignEntity.AffiliateCampaignId;
                campaignPartner.PartnerCampaignCode = newCampaignCode;

                if (!campaignPartner.IsNew && campaignPartner.IsDirty)
                { campaignPartner.UpdatedUTC = DateTime.UtcNow; }

                campaignPartners.Add(campaignPartner);
            }

            DataSourceAsAffiliateCampaignEntity.AffiliateCampaignAffiliatePartnerCollection.Clear();
            DataSourceAsAffiliateCampaignEntity.AffiliateCampaignAffiliatePartnerCollection.AddRange(campaignPartners);
            DeleteCampaignPartners(idsToDelete);
        }

        private void DeleteCampaignPartners(List<int> ids)
        {
            AffiliateCampaignAffiliatePartnerCollection deleteCollection = new AffiliateCampaignAffiliatePartnerCollection();
            deleteCollection.AddToTransaction(DataSourceAsAffiliateCampaignEntity);

            PredicateExpression filter = new PredicateExpression();
            filter.Add(new FieldCompareRangePredicate(AffiliateCampaignAffiliatePartnerFields.AffiliateCampaignAffiliatePartnerId, null, ids));
            deleteCollection.DeleteMulti(filter);
        }

        private static bool IsAffiliateCodeUnique(int affiliateCampaignId, string affiliateCode)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(AffiliateCampaignFields.AffiliateCampaignId != affiliateCampaignId);
            filter.Add(AffiliateCampaignFields.Code == affiliateCode);

            AffiliateCampaignCollection affiliateCampaignCollection = new AffiliateCampaignCollection();
            return affiliateCampaignCollection.GetDbCount(filter) <= 0;
        }

        private bool IsPartnerCodesValid()
        {
            bool isValid = true;
            foreach (AffiliatePartnerEntity affiliatePartner in Partners)
            {
                string partnerCode = GetCode(affiliatePartner.AffiliatePartnerId);

                if (!string.IsNullOrEmpty(partnerCode) && !IsCodeValid(partnerCode))
                {
                    isValid = false;
                    MultiValidatorDefault.AddError($"The Affiliate Partner code for {affiliatePartner.Name} must only contain alphanumeric characters without spaces.");
                }
            }

            Validate();
            return isValid;
        }

        private static AffiliatePartnerCollection GetAffiliatePartners()
        {
            AffiliatePartnerCollection partners = new AffiliatePartnerCollection();
            SortExpression sort = new SortExpression(new SortClause(AffiliatePartnerFields.Name, SortOperator.Ascending));

            partners.GetMulti(null, 0, sort);
            return partners;
        }

        private void BuildPartnerCodeControls()
        {
            if (!pnlPartnerCodes.Visible)
            { return; }

            Partners = GetAffiliatePartners();

            for (int i = 0; i < Partners.Count; i++)
            {
                if (i % 2 != 0)
                { continue; }

                plhPartnerCodes.AddHtml("<tr>");
                CreateAffiliateCampaignPartner(Partners[i]);

                if (i + 1 < Partners.Count)
                { CreateAffiliateCampaignPartner(Partners[i + 1]); }

                plhPartnerCodes.AddHtml("</tr>");
            }
        }

        private void CreateAffiliateCampaignPartner(AffiliatePartnerEntity partner)
        {
            AffiliateCampaignAffiliatePartnerEntity campaignPartner = DataSourceAsAffiliateCampaignEntity.AffiliateCampaignAffiliatePartnerCollection.FirstOrDefault(x => x.AffiliatePartnerId == partner.AffiliatePartnerId);

            plhPartnerCodes.AddHtml("<td class=\"label\">");

            Label label = CreatePartnerLabel(partner);

            plhPartnerCodes.Controls.Add(label);
            plhPartnerCodes.AddHtml("</td>");

            plhPartnerCodes.AddHtml("<td class=\"control\">");

            TextBoxString textBox = CreatePartnerTextBox(partner, campaignPartner?.PartnerCampaignCode);

            plhPartnerCodes.Controls.Add(textBox);
            plhPartnerCodes.AddHtml("</td>");
        }

        private Label CreatePartnerLabel(AffiliatePartnerEntity partner)
        {
            return new Label
            {
                ID = "lb-" + partner.AffiliatePartnerId,
                Text = partner.Name,
                LocalizeText = false
            };
        }

        private TextBoxString CreatePartnerTextBox(AffiliatePartnerEntity partner, string campaignCode)
        {
            return new TextBoxString
            {
                ID = "tb-" + partner.AffiliatePartnerId,
                UseDataBinding = false,
                Value = campaignCode,
                Placeholder = DataSourceAsAffiliateCampaignEntity.Code
            };
        }
    }
}
