﻿using System;
using System.Diagnostics;
using System.Web.UI.WebControls;
using DevExpress.Web;
using DevExpress.Web.ASPxTreeList;
using Dionysos;
using Dionysos.Web.UI.WebControls;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Cms;
using Obymobi.Logic.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Generic.SubPanels.Sms
{
    public partial class SmsKeywordPanel : SubPanelEntity
    {
        private const string TRANSLATION_KEY_PREFIX = "Obymobi.ObymobiCms.Generic.SubPanels.Sms.KeywordPanel.";

        private BaseDataSource<SmsKeywordManager> smsKeywordDataSource;

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            this.DataSourceLoaded += SmsKeywordPanel_DataSourceLoaded;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.tlSmsKeywords.DataBind();
        }

        public override bool Save()
        {
            return true;
        }

        public override bool Validate()
        {
            return true;
        }

        public override bool InitializeEntity(bool pageInitComplete)
        {
            return true;
        }

        private void CreateTreeList()
        {
            // Create Tree View
            this.tlSmsKeywords.SettingsEditing.Mode = TreeListEditMode.Inline;
            this.tlSmsKeywords.SettingsBehavior.AllowSort = true;
            this.tlSmsKeywords.SettingsBehavior.AllowDragDrop = false;
            this.tlSmsKeywords.ClientInstanceName = "treelist";
            this.tlSmsKeywords.AutoGenerateColumns = false;
            this.tlSmsKeywords.KeyFieldName = "SmsKeywordId";
            //this.tlSmsKeywords.ParentFieldName = "ParentPageId";
            this.tlSmsKeywords.SettingsBehavior.AutoExpandAllNodes = true;
            this.tlSmsKeywords.SettingsEditing.AllowNodeDragDrop = false;
            this.tlSmsKeywords.Settings.GridLines = GridLines.Both;
            this.tlSmsKeywords.ClientSideEvents.NodeDblClick = @" function(s, e) {
	                                                                    treelist.StartEdit(e.nodeKey);
		                                                                e.cancel = true;	                                                                    
                                                                    }";

            // Keyword Column
            var nameColumn = new TreeListTextColumn();
            nameColumn.Caption = "Keyword";
            nameColumn.FieldName = "Keyword";
            nameColumn.Width = Unit.Empty;
            nameColumn.VisibleIndex = 0;
            this.tlSmsKeywords.Columns.Add(nameColumn);


            var companyColumn = new TreeListComboBoxColumn();
            companyColumn.Caption = "Company";
            companyColumn.FieldName = "CompanyId";
            companyColumn.Width = Unit.Pixel(250);
            companyColumn.VisibleIndex = 1;
            companyColumn.CellStyle.HorizontalAlign = HorizontalAlign.Left;
            companyColumn.PropertiesEdit.Style.HorizontalAlign = HorizontalAlign.Left;
            companyColumn.PropertiesComboBox.IncrementalFilteringMode = DevExpress.Web.IncrementalFilteringMode.Contains;

            // Get company list
            if (CmsSessionHelper.CurrentRole >= Role.Administrator)
            {
                var sort = new SortExpression();
                sort.Add(CompanyFields.Name | SortOperator.Ascending);

                var companies = new CompanyCollection();
                companies.GetMulti(null, 0, sort);

                foreach (var company in companies)
                {
                    companyColumn.PropertiesComboBox.Items.Add(company.Name, company.CompanyId);
                }
            }
            else
            {
                var company = CompanyHelper.GetCompanyEntityByCompanyId(CmsSessionHelper.CurrentCompanyId, false);
                companyColumn.PropertiesComboBox.Items.Add(company.Name, company.CompanyId);
            }

            companyColumn.PropertiesComboBox.ValueType = typeof(Int32);
            this.tlSmsKeywords.Columns.Add(companyColumn);

            // In-line Edit Column
            var editCommandColumn = new TreeListCommandColumn();
            editCommandColumn.VisibleIndex = 2;
            editCommandColumn.Width = Unit.Pixel(50);
            editCommandColumn.ShowNewButtonInHeader = true;
            editCommandColumn.EditButton.Visible = true;
            editCommandColumn.DeleteButton.Visible = true;
            editCommandColumn.ButtonType = ButtonType.Image;
            editCommandColumn.EditButton.Image.Url = this.ResolveUrl("~/images/icons/pencil.png");
            editCommandColumn.DeleteButton.Image.Url = this.ResolveUrl("~/images/icons/delete.png");
            editCommandColumn.CancelButton.Image.Url = this.ResolveUrl("~/images/icons/cross_grey.png");
            editCommandColumn.UpdateButton.Image.Url = this.ResolveUrl("~/images/icons/disk.png");
            editCommandColumn.NewButton.Image.Url = this.ResolveUrl("~/images/icons/add2.png");
            this.tlSmsKeywords.Columns.Add(editCommandColumn);

            // Events
            this.tlSmsKeywords.SettingsEditing.Mode = DevExpress.Web.ASPxTreeList.TreeListEditMode.Inline;
            this.tlSmsKeywords.NodeValidating += tlSmsKeywords_NodeValidating;
            this.tlSmsKeywords.CustomErrorText += tlSmsKeywords_CustomErrorText;

            // Data Source must be set in OnInit: http://documentation.devexpress.com/#AspNet/DevExpressWebASPxTreeListASPxTreeList_DataSourcetopic
            this.InitDataSource();

            this.tlSmsKeywords.Width = Unit.Pixel(650);
        }

        void tlSmsKeywords_CustomErrorText(object sender, TreeListCustomErrorTextEventArgs e)
        {
            if (e.Exception.InnerException != null)
            {
                var exception = e.Exception.InnerException as ObymobiException;
                if (exception != null)
                {
                    e.ErrorText = exception.AdditionalMessage;
                }
                else
                {
                    e.ErrorText = e.Exception.InnerException.Message;
                }
            }
        }

        private void InitDataSource()
        {
            var parentEntity = (IEntity)this.Parent.DataSource;
            var primaryKey = (int)parentEntity.PrimaryKeyFields[0].CurrentValue;

            var dataManager = new SmsKeywordManager(primaryKey);
            this.smsKeywordDataSource = new BaseDataSource<SmsKeywordManager>(dataManager);
            this.tlSmsKeywords.DataSource = this.smsKeywordDataSource;
        }

        #region Events
        
        void SmsKeywordPanel_DataSourceLoaded(object sender)
        {
            if (!((IEntity)this.PageAsPageEntity.DataSource).IsNew)
            {
                this.CreateTreeList();
            }
        }

        void tlSmsKeywords_NodeValidating(object sender, TreeListNodeValidationEventArgs e)
        {
            if (((string)e.NewValues["Keyword"]).IsNullOrWhiteSpace())
                e.NodeError = this.PageAsPageEntity.Translate(TRANSLATION_KEY_PREFIX + "Error.KeywordNotEntered", "Er moet een 'Keyword' worden opgegeven.", true);
        }

        #endregion
    }
}