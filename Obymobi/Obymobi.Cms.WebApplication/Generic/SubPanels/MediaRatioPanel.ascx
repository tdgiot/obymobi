﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Generic.SubPanels.MediaRatioPanel" Codebehind="MediaRatioPanel.ascx.cs" %>
<table class="dataformV2">
    <tr>
        <td class="label">
            <D:Label runat="server" ID="lblNameLabel">Naam</D:Label>
        </td>
        <td class="control">
            <D:Label runat="server" ID="lblName" LocalizeText="false" />
        </td>
        <td class="label">
            <D:Label runat="server" ID="lblRelatedEntityNameLabel">Specifiek voor</D:Label>
        </td>
        <td class="control">
            <D:Label runat="server" ID="lblRelatedEntityName" LocalizeText="false" />
        </td>
    </tr>
     <tr>
        <td class="label">
            <D:Label runat="server" ID="lblMinimalWidthLabel">Minimale breedte</D:Label>
        </td>
        <td class="control">
            <D:Label runat="server" ID="lblMinimalWidth" LocalizeText="false" />
        </td>
        <td class="label">
            <D:Label runat="server" ID="lblWidthLabel">Afbeelding breedte</D:Label>
        </td>
        <td class="control">
            <D:Label runat="server" ID="lblWidth" LocalizeText="false" />
        </td>
    </tr>
    <tr>
        <td class="label">
            <D:Label runat="server" ID="lblMinimalHeightLabel">Minimale hoogte</D:Label>
        </td>
        <td class="control">
            <D:Label runat="server" ID="lblMinimalHeight" LocalizeText="false" />
        </td>
        <td class="label">
            <D:Label runat="server" ID="lblHeightLabel">Afbeelding hoogte</D:Label>
        </td>
        <td class="control">
            <D:Label runat="server" ID="lblHeight" LocalizeText="false" />
        </td>
    </tr>
    <tr>
        <td colspan="4">
            <D:PlaceHolder runat="server" ID="plhCropper">
                <D:PlaceHolder runat="server" ID="plhCropTool" />
                <asp:HiddenField runat="server" ID="hfTop" />
                <asp:HiddenField runat="server" ID="hfLeft" />
                <asp:HiddenField runat="server" ID="hfWidth" />
            <asp:HiddenField runat="server" ID="hfHeight" />
            </D:PlaceHolder>
            <D:PlaceHolder runat="server" ID="plhNoCropper" Visible="false">
                 <p><D:Label runat="server" ID="lblWarningImageToSmall">De afmetingen van de afbeelding zijn te klein om een uitsnede te maken.</D:Label></p>
            </D:PlaceHolder>
        </td>
    </tr>
</table>