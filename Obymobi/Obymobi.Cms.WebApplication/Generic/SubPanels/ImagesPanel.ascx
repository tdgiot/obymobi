﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Generic.SubPanels.ImagesPanel" Codebehind="ImagesPanel.ascx.cs" %>                       
<%@ Reference VirtualPath="~/Cms/SubPanels/PageTypeElementEditors/ImageEditor.ascx" %>
<table class="dataformV2 pageContent">
    <tr>
	    <td class="label">
		    <D:Label runat="server" ID="lblImage">Afbeelding</D:Label>
	    </td>
	    <td class="control" colspan="3">                                								
            <D:PlaceHolder runat="server" ID="plhNoImage">
                <span class="nobold">
                    <D:LinkButton runat="server" ID="hlUploadImage" Text="Afbeelding uploaden"></D:LinkButton>
                </span>
            </D:PlaceHolder> 								                               
	    </td>                            
    </tr>	        
    <D:PlaceHolder runat="server" ID="plhMedia"></D:PlaceHolder>    
	<tr>
		<td class="label">
			&nbsp;
		</td>
		<td class="control">
			&nbsp;
		</td>
        <td class="label">
            &nbsp;
        </td>
        <td class="control">
            &nbsp;
        </td>
	</tr>						                   
</table>  

                        
                        