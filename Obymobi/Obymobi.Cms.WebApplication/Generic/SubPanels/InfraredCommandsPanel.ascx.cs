﻿using System;
using System.Web.UI.WebControls;
using DevExpress.Utils;
using DevExpress.Web;
using DevExpress.Web.ASPxTreeList;
using Dionysos.Web.UI.WebControls;
using DevExpress.Web.Internal;
using System.Web.UI;
using Dionysos;
using DevExpress.Web;
using DevExpress.Web.Data;
using Obymobi.Data.EntityClasses;

namespace Obymobi.ObymobiCms.Generic.SubPanels
{
    public partial class InfraredCommandsPanel : SubPanelCustomDataSource
    {
        #region  Fields
        
        private InfraredCommandDataSource datasource;

        #endregion

        #region Properties

        private string EndDragNodeMethod
        {
            get
            {
                return @" function(s, e) {                                                                
                            e.cancel = true;
		                    var key = s.GetNodeKeyByRow(e.targetElement);
		                    " + this.TreelistId + @".PerformCustomCallback(e.nodeKey + ':' + key);
	                        }";
            }
        }

        private string NodeDblClickMethod
        {
            get { return @" function(s, e) {
	                            " + this.TreelistId + @".StartEdit(e.nodeKey);
		                        e.cancel = true;	                                                                    
                            }"; }
        }

        #endregion

        #region Methods

        private void CreateTreeList()
        {
            this.tlCommands.SettingsEditing.Mode = TreeListEditMode.EditForm;
            this.tlCommands.SettingsPopupEditForm.Width = 700;
            this.tlCommands.SettingsBehavior.ColumnResizeMode = ColumnResizeMode.Disabled;
            this.tlCommands.SettingsBehavior.AllowSort = false;
            this.tlCommands.SettingsBehavior.AllowDragDrop = false;
            this.tlCommands.ClientInstanceName = this.TreelistId;
            this.tlCommands.AutoGenerateColumns = false;
            this.tlCommands.SettingsEditing.AllowNodeDragDrop = true;
            this.tlCommands.SettingsEditing.AllowRecursiveDelete = false;
            this.tlCommands.Settings.GridLines = GridLines.Both;
            this.tlCommands.ClientSideEvents.EndDragNode = this.EndDragNodeMethod;
            this.tlCommands.ClientSideEvents.NodeDblClick = this.NodeDblClickMethod;            
            this.tlCommands.CustomErrorText += this.tlCommands_CustomErrorText;
            this.tlCommands.Width = Unit.Pixel(736);
            this.tlCommands.KeyFieldName = "InfraredCommandId";              
            this.tlCommands.SettingsBehavior.ColumnResizeMode = ColumnResizeMode.Control;

            TreeListTextColumn typeColumn = new TreeListTextColumn();
            typeColumn.Caption = "Type";
            typeColumn.FieldName = "TypeStringValue";
            typeColumn.VisibleIndex = 1;
            typeColumn.CellStyle.Wrap = DefaultBoolean.True;            
            this.tlCommands.Columns.Add(typeColumn);

            TreeListTextColumn nameColumn = new TreeListTextColumn();
            nameColumn.Caption = "Name";
            nameColumn.FieldName = "Name";
            nameColumn.VisibleIndex = 2;
            nameColumn.CellStyle.Wrap = DefaultBoolean.True;
            this.tlCommands.Columns.Add(nameColumn);

            TreeListTextColumn hexColumn = new TreeListTextColumn();
            hexColumn.Caption = "Hex code";
            hexColumn.FieldName = "Hex";
            hexColumn.VisibleIndex = 3;
            hexColumn.Width = Unit.Pixel(380);
            hexColumn.CellStyle.CssClass = "irHexClm";
            this.tlCommands.Columns.Add(hexColumn);

            TreeListCommandColumn editCommandColumn = new TreeListCommandColumn();
            editCommandColumn.VisibleIndex = 4;
            editCommandColumn.Width = Unit.Pixel(40);
            editCommandColumn.ShowNewButtonInHeader = false;
            editCommandColumn.EditButton.Visible = true;
            editCommandColumn.NewButton.Visible = false;
            editCommandColumn.DeleteButton.Visible = true;
            editCommandColumn.ButtonType = ButtonType.Image;
            editCommandColumn.EditButton.Image.Url = this.ResolveUrl("~/images/icons/pencil.png");
            editCommandColumn.DeleteButton.Image.Url = this.ResolveUrl("~/images/icons/delete.png");
            editCommandColumn.CancelButton.Image.Url = this.ResolveUrl("~/images/icons/cross_grey.png");
            editCommandColumn.NewButton.Image.Url = this.ResolveUrl("~/images/icons/add2.png");
            editCommandColumn.UpdateButton.Image.Url = this.ResolveUrl("~/images/icons/disk.png");
            this.tlCommands.Columns.Add(editCommandColumn);

            TreeListHyperLinkColumn editPageColumn = new TreeListHyperLinkColumn();
            editPageColumn.Name = "EditCommand";
            editPageColumn.Caption = " ";
            editPageColumn.VisibleIndex = 5;
            editPageColumn.Width = Unit.Pixel(20);
            editPageColumn.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            editPageColumn.PropertiesHyperLink.ImageUrl = "~/images/icons/page_edit.png";
            editPageColumn.EditCellStyle.CssClass = "hidden";
            editPageColumn.FieldName = "InfraredCommandId";
            editPageColumn.EditFormSettings.VisibleIndex = 5;
            editPageColumn.EditFormCaptionStyle.CssClass = "hidden";
            this.tlCommands.Columns.Add(editPageColumn);
            
            this.tlCommands.NodeValidating += this.tlCommands_NodeValidating;
            this.tlCommands.HtmlDataCellPrepared += this.tlCommands_HtmlDataCellPrepared;
            this.tlCommands.HtmlRowPrepared += this.tlCommands_HtmlRowPrepared;
            this.tlCommands.NodeDeleted += this.tlCommands_OnNodeDeleted;
        }

        private void tlCommands_HtmlRowPrepared(object sender, TreeListHtmlRowEventArgs e)
        {
            if (!e.NodeKey.IsNullOrWhiteSpace())
            {
                TreeListNode node = this.tlCommands.FindNodeByKeyValue(e.NodeKey);
                InfraredCommandEntity infraredCommandEntity = (InfraredCommandEntity)node.DataItem;

                if (infraredCommandEntity.Hex.IsNullOrWhiteSpace())
                {
                    e.Row.ForeColor = System.Drawing.Color.Red;
                }                
                else if (infraredCommandEntity.Type == Enums.InfraredCommandType.Custom && infraredCommandEntity.Name.IsNullOrWhiteSpace())
                {
                    e.Row.ForeColor = System.Drawing.Color.Red;
                }
            }
        }
       
        private void tlCommands_HtmlDataCellPrepared(object sender, TreeListHtmlDataCellEventArgs e)
        {
            if (e.Column.Name == "EditCommand" && !e.NodeKey.IsNullOrWhiteSpace())
            {
                string url = string.Format("~/Generic/InfraredCommand.aspx?id={0}", e.NodeKey.ToString());

                foreach (Control item in e.Cell.Controls)
                {
                    HyperLinkDisplayControl link = item as HyperLinkDisplayControl;
                    if (link != null)
                    {
                        link.NavigateUrl = url;
                        break;
                    }
                }
            }
        }

        private void tlCommands_OnNodeDeleted(object sender, ASPxDataDeletedEventArgs e)
        {
            ASPxWebControl.RedirectOnCallback(this.Request.RawUrl);
        }

        public new void Load(int infraredConfigurationId, bool bindAndExpand)
        {
            this.datasource = new InfraredCommandDataSource(infraredConfigurationId);
            this.tlCommands.DataSource = this.datasource;

            if (bindAndExpand)
            {
                this.tlCommands.DataBind();
                this.tlCommands.ExpandAll();
            }
        }

        private void LoadUserControls()
        {
            
        }

        #endregion

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            base.OnInit(e);
            this.CreateTreeList();
        }

        private void tlCommands_CustomErrorText(object sender, TreeListCustomErrorTextEventArgs e)
        {
            if (e.Exception.InnerException != null)
            {
                ObymobiException exception = e.Exception.InnerException as ObymobiException;
                if (exception != null)
                {
                    e.ErrorText = exception.AdditionalMessage;
                }
                else
                {
                    e.ErrorText = e.Exception.InnerException.Message;
                }
            }
        }

        private void tlCommands_NodeValidating(object sender, TreeListNodeValidationEventArgs e)
        {
            if (e.HasErrors)
            {
                e.NodeError = "Please correct the errors.";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.tlCommands.DataBind();
        }

        private string TreelistId
        {
            get { return string.Format("{0}_treelist", this.ID); }
        }

        #endregion
    }
}