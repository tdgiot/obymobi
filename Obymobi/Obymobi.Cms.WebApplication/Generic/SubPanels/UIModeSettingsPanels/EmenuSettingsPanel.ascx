<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Generic.Subpanels.UIModeSettingsPanels.EmenuSettingsPanel" Codebehind="EmenuSettingsPanel.ascx.cs" %>
<tr>
	<td class="label">
        <D:Label runat="server" id="lblDefaultUITabId">Default UI Tab</D:Label>
    </td>
    <td class="control">
        <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlDefaultUITabId" IncrementalFilteringMode="StartsWith" EntityName="UITab" TextField="CaptionOrType" ValueField="UITabId" PreventEntityCollectionInitialization="true"></X:ComboBoxLLBLGenEntityCollection>
    </td>
	<td class="label">
        <D:LabelEntityFieldInfo runat="server" id="lblShowRequestBill">Laat rekening vragen zien</D:LabelEntityFieldInfo>
    </td>
    <td class="control">
        <D:CheckBox runat ="server" ID="cbShowRequestBill" />
    </td>
</tr>						    
<tr>
	<td class="label">
        <D:LabelEntityFieldInfo runat="server" id="lblShowDeliverypoint">Laat tafel zien</D:LabelEntityFieldInfo>
    </td>
    <td class="control">
        <D:CheckBox runat ="server" ID="cbShowDeliverypoint" />
    </td>
	<td class="label">
        <D:LabelEntityFieldInfo runat="server" id="lblShowClock">Laat klok zien</D:LabelEntityFieldInfo>
    </td>
    <td class="control">
        <D:CheckBox runat ="server" ID="cbShowClock" />
    </td>
</tr>						    
<tr>
	<td class="label">
        <D:LabelEntityFieldInfo runat="server" id="lblShowBattery">Laat batterij zien</D:LabelEntityFieldInfo>
    </td>
    <td class="control">
        <D:CheckBox runat ="server" ID="cbShowBattery" />
    </td>
	<td class="label">
        <D:LabelEntityFieldInfo runat="server" id="lblShowBrightness">Laat helderheid zien</D:LabelEntityFieldInfo>
    </td>
    <td class="control">
        <D:CheckBox runat ="server" ID="cbShowBrightness" />
    </td>
</tr>						    
<tr>
	<td class="label">
        <D:LabelEntityFieldInfo runat="server" id="lblShowFullscreenEyecatcher">Laat eyecatcher op volledig scherm zien</D:LabelEntityFieldInfo>
    </td>
    <td class="control">
        <D:CheckBox runat ="server" ID="cbShowFullscreenEyecatcher" />
    </td>
	<td class="label">
        <D:LabelEntityFieldInfo runat="server" id="lblShowServiceOptions">Laat service opties zien</D:LabelEntityFieldInfo>
    </td>
    <td class="control">
        <D:CheckBox runat ="server" ID="cbShowServiceOptions" />
    </td>
</tr>	