using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;

namespace Obymobi.ObymobiCms.Generic.Subpanels.UIModeSettingsPanels
{
    public partial class EmenuSettingsPanel : System.Web.UI.UserControl, Dionysos.Interfaces.ISaveableControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private void SetGui()
        {
            // Get the UI tabs from the database for most accurate data
            if (this.UIModeEntity != null && this.UIModeEntity.UIModeId > 0)
            {
                var excludeTypes = new List<int>() { (int)UITabType.Placeholder };

                var filter = new PredicateExpression();
                filter.Add(UITabFields.UIModeId == this.UIModeEntity.UIModeId);
                filter.AddWithAnd(UITabFields.Type != excludeTypes);

                var sort = new SortExpression();
                sort.Add(new SortClause(UITabFields.SortOrder, SortOperator.Ascending));

                var uiTabs = new UITabCollection();
                uiTabs.GetMulti(filter, 0, sort);

                this.ddlDefaultUITabId.DataSource = uiTabs;
                this.ddlDefaultUITabId.DataBind();
            }
        }

        public bool Save()
        {
            return true;
        }

        #region Properties

        private UIModeEntity uiModeEntity = null;
        public UIModeEntity UIModeEntity
        {
            get
            {
                return this.uiModeEntity;
            }
            set
            {
                this.uiModeEntity = value;
                this.SetGui();
            }
        }

        #endregion
    }
}