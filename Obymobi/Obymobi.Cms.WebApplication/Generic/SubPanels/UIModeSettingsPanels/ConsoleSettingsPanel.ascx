<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Generic.Subpanels.UIModeSettingsPanels.ConsoleSettingsPanel" Codebehind="ConsoleSettingsPanel.ascx.cs" %>
<tr>
	<td class="label">
        <D:Label runat="server" id="lblDefaultUITabId">Default UI Tab</D:Label>
    </td>
    <td class="control">
        <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlDefaultUITabId" IncrementalFilteringMode="StartsWith" EntityName="UITab" TextField="CaptionOrType" ValueField="UITabId" PreventEntityCollectionInitialization="true" />
    </td>
</tr>