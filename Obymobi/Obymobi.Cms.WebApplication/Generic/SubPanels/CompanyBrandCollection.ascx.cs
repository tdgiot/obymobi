﻿using System;
using System.ComponentModel;
using System.Linq;
using Dionysos.Data.LLBLGen;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Generic.Subpanels
{
    public partial class CompanyBrandCollection : Dionysos.Web.UI.DevExControls.SubPanelLLBLOverviewDataSourceCollection
    {
        #region Methods

        private void HookUpEvents()
        {
            //this.MainGridView.CustomColumnSort += new DevExpress.Web.ASPxGridViewCustomColumnSortEventHandler(MainGridView_CustomColumnSort);
        }

        #endregion

        #region Events

        protected override void OnInit(EventArgs e)
        {
            this.Filter = new FieldCompareRangePredicate(CompanyBrandFields.CompanyId, null, CmsSessionHelper.CompanyIdsForUser.ToArray());

            this.DataSourceLoaded += CompanyBrandCollection_DataSourceLoaded;
            base.OnInit(e);
            
            this.EntityName = "CompanyBrand";
            this.EntityPageUrl = "~/Company/CompanyBrand.aspx";
        }

        void CompanyBrandCollection_DataSourceLoaded(object sender)
        {
            BrandRole userBrandRole = CmsSessionHelper.GetUserRoleForBrand(this.DataSourceAsBrandEntity.BrandId).GetValueOrDefault(BrandRole.Viewer);

            this.phlAddCompany.Visible = userBrandRole == BrandRole.Owner;
            this.MainGridView.ShowDeleteHyperlinkColumn = userBrandRole == BrandRole.Owner;
            this.MainGridView.ShowEditHyperlinkColumn = userBrandRole == BrandRole.Owner;
            this.MainGridView.EnableRowDoubleClick = userBrandRole == BrandRole.Owner;

            CompanyCollection companyCollection = CmsSessionHelper.CompanyCollectionForUser;
            companyCollection.Sort(CompanyFields.Name.FieldIndex, ListSortDirection.Ascending);

            this.ddlCompanyId.DataSource = companyCollection;
            this.ddlCompanyId.DataBind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.HookUpEvents();

            this.btnAdd.Click += btnAdd_Click;
            this.MainGridView.LoadComplete += MainGridView_LoadComplete;
        }

        void MainGridView_LoadComplete(object sender, EventArgs e)
        {
            if (this.MainGridView.Columns["Priority"] != null)
            {
                this.MainGridView.Columns["Priority"].Visible = false;
            }
            if (this.MainGridView.Columns["BrandName"] != null)
            {
                this.MainGridView.Columns["BrandName"].Visible = false;
            }
        }

        void btnAdd_Click(object sender, EventArgs e)
        {
            if (this.ddlCompanyId.ValidId > 0)
            {
                bool wasNew = this.ParentDataSource.IsNew;
                if (wasNew)
                    this.ParentDataSource.Save();

                // Check if connection doesn't already exists
                PredicateExpression filter = new PredicateExpression();
                filter.Add(CompanyBrandFields.BrandId == this.ParentPrimaryKeyFieldValue);
                filter.Add(CompanyBrandFields.CompanyId == this.ddlCompanyId.Value);

                if (EntityCollection.GetDbCount<Data.CollectionClasses.CompanyBrandCollection>(filter) == 0)
                {
                    // Get highest priority
                    PredicateExpression filter2 = new PredicateExpression(CompanyBrandFields.CompanyId == this.ddlCompanyId.Value);
                    SortExpression sort = new SortExpression(CompanyBrandFields.Priority | SortOperator.Descending);
                    
                    Data.CollectionClasses.CompanyBrandCollection collection = new Data.CollectionClasses.CompanyBrandCollection();
                    collection.GetMulti(filter2, 1, sort);

                    int priority = 100; // Default
                    if (collection.Count > 0)
                    {
                        priority = collection[0].Priority + 100;
                    }

                    CompanyBrandEntity companyBrandEntity = new CompanyBrandEntity();
                    companyBrandEntity.BrandId = this.ParentPrimaryKeyFieldValue;
                    companyBrandEntity.CompanyId = this.ddlCompanyId.ValidId;
                    companyBrandEntity.Priority = priority;
                    companyBrandEntity.Save();
                }

                if (wasNew)
                    this.Parent.Save();
            } 
        }

        #endregion

        private BrandEntity DataSourceAsBrandEntity
        {
            get
            {
                return this.ParentDataSource as BrandEntity;
            }
        }
    }
}