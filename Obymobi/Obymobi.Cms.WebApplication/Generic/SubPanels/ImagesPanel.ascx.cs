﻿using Obymobi.Logic.Cms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dionysos;
using Obymobi.Data.EntityClasses;
using Obymobi.ObymobiCms.Cms.SubPanels.PageTypeElementEditors;
using Dionysos.Interfaces;
using Obymobi.Enums;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;

namespace Obymobi.ObymobiCms.Generic.SubPanels
{
    public partial class ImagesPanel : Dionysos.Web.UI.WebControls.SubPanelLLBLGenEntityCollection
    {
        #region Methods

        private void RenderEditors()
        {
            MediaCollection mediaCollection;

            if (this.exclusiveMediaType != MediaType.NoMediaTypeSpecified)
            {
                mediaCollection = this.CustomMediaCollection;
                
                // Filter media so it only displays media with the exclude media type
                //var filteredMedia = new MediaCollection();
                //foreach (MediaEntity media in this.DataSourceAsMediaCollection)
                //{
                //    foreach (MediaRatioTypeMediaEntity mrtm in media.MediaRatioTypeMediaCollection)
                //    {
                //        if (mrtm.MediaType == (int)this.exclusiveMediaType)
                //        {
                //            filteredMedia.Add(media);
                //            break;
                //        }
                //    }
                //}
                //mediaCollection = filteredMedia;
            }
            else
            {
                mediaCollection = this.DataSourceAsMediaCollection;
            }

            var twoMedia = new MediaCollection();
            for (int i = 0; i < mediaCollection.Count; i++)
            {
                twoMedia.Add(mediaCollection[i]);

                if (twoMedia.Count == 2 || ((i + 1) == mediaCollection.Count))
                {
                    var control = this.LoadControl("~/Cms/SubPanels/PageTypeElementEditors/ImageEditor.ascx") as ImageEditor;
                    control.ExclusiveMediaType = this.exclusiveMediaType;
                    control.MediaCollection = twoMedia;                    
                    this.plhMedia.Controls.Add(control);
                    
                    twoMedia.Clear();
                }                                    
            }                            
        }

        #endregion

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            this.CollectionOnParentDataSource = "MediaCollection";
        }

        protected void Page_Load(object sender, EventArgs e)
        {            
            this.hlUploadImage.Click += hlUploadImage_Click;            
            this.hlUploadImage.OnClientClick = "pageIsDirty = false;";
        }

        protected override void CreateChildControls()
        {
            this.RenderEditors();
        }        

        private void hlUploadImage_Click(object sender, EventArgs e)
        {
            this.PageAsPageLLBLGenEntity.Validate();
            if (this.PageAsPageLLBLGenEntity.IsValid && this.PageAsPageLLBLGenEntity.Save())
            {
                string url = this.ResolveUrl("~/Generic/Media.aspx?mode=add&RelatedEntityType={0}&RelatedEntityId={1}".FormatSafe(this.PageAsPageLLBLGenEntity.EntityName, this.PageAsPageLLBLGenEntity.EntityId));
                
                if (this.exclusiveMediaType != MediaType.NoMediaTypeSpecified)
                    url += string.Format("&MediaType={0}", (int)this.exclusiveMediaType);
                
                this.Response.Redirect(url);
            }
        }

        #endregion

        #region Properties

        private Obymobi.Data.CollectionClasses.MediaCollection DataSourceAsMediaCollection
        {
            get
            {
                return this.DataSource as Obymobi.Data.CollectionClasses.MediaCollection;
            }
        }

        private MediaCollection customMediaCollection;

        public MediaCollection CustomMediaCollection
        { 
            get
            {
                return this.customMediaCollection;
            }
            set
            {
                this.customMediaCollection = value;
            }
        }

        private MediaType exclusiveMediaType = MediaType.NoMediaTypeSpecified;

        public MediaType ExclusiveMediaType
        {
            set
            {
                this.exclusiveMediaType = value;
            }
        }

        #endregion
    }
}