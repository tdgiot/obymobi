﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Data;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Cms;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Generic.SubPanels
{
    public partial class MediaRatioTypesPanel : UserControl
    {
        /// <summary>
        /// List of media ratio types divided by their device type
        /// </summary>
        private List<MediaRatioType> archosMediaRatioTypes = new List<MediaRatioType>();
        private List<MediaRatioType> inRoomTabletMediaRatioTypes = new List<MediaRatioType>();
        private List<MediaRatioType> tvMediaRatioTypes = new List<MediaRatioType>();
        private List<MediaRatioType> otouchoMediaRatioTypes = new List<MediaRatioType>();
        private List<MediaRatioType> mobileAndroidMediaRatioTypes = new List<MediaRatioType>();
        private List<MediaRatioType> mobileIphoneMediaRatioTypes = new List<MediaRatioType>();
        private List<MediaRatioType> ipadMediaRatioTypes = new List<MediaRatioType>();
        private List<MediaRatioType> applessMediaRatioTypes = new List<MediaRatioType>();
        private List<MediaRatioType> miscMediaRatioTypes = new List<MediaRatioType>();

        private void RenderMediaRatioTypes(DeviceCategory deviceCategory, List<MediaRatioType> mediaRatioTypes, int mediaType)
        {
            // Get the media ratio types connected to this media item
            EntityView<MediaRatioTypeMediaEntity> mediaRatioTypeMediaView = this.mediaEntity.MediaRatioTypeMediaCollection.DefaultView;

            foreach (var mediaRatioType in mediaRatioTypes)
            {
                // If a MediaRatioTypeMedia entity exists for the current MediaRatioType 
                // and Media combination, add the corresponding tab and check the checkbox
                ListItem listItem = new ListItem(mediaRatioType.Name, ((int)mediaRatioType.MediaType).ToString());

                PredicateExpression mediaFilter = new PredicateExpression();
                mediaFilter.Add(MediaRatioTypeMediaFields.MediaId == this.mediaEntity.MediaId);
                mediaFilter.Add(MediaRatioTypeMediaFields.MediaType == mediaRatioType.MediaType);

                mediaRatioTypeMediaView.Filter = mediaFilter;

                if (mediaRatioTypeMediaView.Count > 0 || (this.mediaEntity.IsNew && mediaRatioType.IsSelectedByDefault))
                    listItem.Selected = true;

                // Add the item to the specific checkboxlist
                if (deviceCategory == DeviceCategory.Archos)
                    this.cblMediaRatioTypesArchos.Items.Add(listItem);
                else if (deviceCategory == DeviceCategory.InRoomTablet)
                    this.cblMediaRatioTypesInRoomTablet.Items.Add(listItem);
                else if (deviceCategory == DeviceCategory.TvBox)
                    this.cblMediaRatioTypesAndroidTVBox.Items.Add(listItem);
                else if (deviceCategory == DeviceCategory.Otoucho)
                    this.cblMediaRatioTypesOtoucho.Items.Add(listItem);
                else if (deviceCategory == DeviceCategory.MobileAndroid)
                    this.cblMediaRatioTypesMobileAndroid.Items.Add(listItem);
                else if (deviceCategory == DeviceCategory.MobileIphone)
                    this.cblMediaRatioTypesMobileIphone.Items.Add(listItem);
                else if (deviceCategory == DeviceCategory.Ipad)
                    this.cblMediaRatioTypesIpad.Items.Add(listItem);
                else if (deviceCategory == DeviceCategory.AppLess)
                    this.cblMediaRatioTypesAppLess.Items.Add(listItem);
                else if (deviceCategory == DeviceCategory.Misc)
                    this.cblMediaRatioTypesMisc.Items.Add(listItem);
            }

            // Not sure if this is really necesarry, but I didnt want to break anything
            if (mediaType > 0)
            {
                if (deviceCategory == DeviceCategory.Archos && this.cblMediaRatioTypesArchos.Items.Count > 0)
                    this.cblMediaRatioTypesArchos.SelectedIndex = 0;
                else if (deviceCategory == DeviceCategory.InRoomTablet && this.cblMediaRatioTypesInRoomTablet.Items.Count > 0)
                    this.cblMediaRatioTypesInRoomTablet.SelectedIndex = 0;
                else if (deviceCategory == DeviceCategory.TvBox && this.cblMediaRatioTypesAndroidTVBox.Items.Count > 0)
                    this.cblMediaRatioTypesAndroidTVBox.SelectedIndex = 0;
                else if (deviceCategory == DeviceCategory.Otoucho && this.cblMediaRatioTypesOtoucho.Items.Count > 0)
                    this.cblMediaRatioTypesOtoucho.SelectedIndex = 0;
                else if (deviceCategory == DeviceCategory.MobileAndroid && this.cblMediaRatioTypesMobileAndroid.Items.Count > 0)
                    this.cblMediaRatioTypesMobileAndroid.SelectedIndex = 0;
                else if (deviceCategory == DeviceCategory.MobileIphone && this.cblMediaRatioTypesMobileIphone.Items.Count > 0)
                    this.cblMediaRatioTypesMobileIphone.SelectedIndex = 0;
                else if (deviceCategory == DeviceCategory.Ipad && this.cblMediaRatioTypesIpad.Items.Count > 0)
                    this.cblMediaRatioTypesIpad.SelectedIndex = 0;
                else if (deviceCategory == DeviceCategory.AppLess && this.cblMediaRatioTypesAppLess.Items.Count > 0)
                    this.cblMediaRatioTypesAppLess.SelectedIndex = 0;
                else if (deviceCategory == DeviceCategory.Misc && this.cblMediaRatioTypesMisc.Items.Count > 0)
                    this.cblMediaRatioTypesMisc.SelectedIndex = 0;
            }
        }

        public ListItemCollection RetrieveListItemsByDeviceCategory(DeviceCategory category)
        {
            if (category == DeviceCategory.Archos)
                return this.cblMediaRatioTypesArchos.Items;
            else if (category == DeviceCategory.Misc)
                return this.cblMediaRatioTypesMisc.Items;
            else if (category == DeviceCategory.MobileAndroid)
                return this.cblMediaRatioTypesMobileAndroid.Items;
            else if (category == DeviceCategory.Otoucho)
                return this.cblMediaRatioTypesOtoucho.Items;
            else if (category == DeviceCategory.InRoomTablet)
                return this.cblMediaRatioTypesInRoomTablet.Items;
            else if (category == DeviceCategory.TvBox)
                return this.cblMediaRatioTypesAndroidTVBox.Items;
            else if (category == DeviceCategory.Ipad)
                return this.cblMediaRatioTypesIpad.Items;
            else if (category == DeviceCategory.MobileIphone)
                return this.cblMediaRatioTypesMobileIphone.Items;
            else if (category == DeviceCategory.AppLess)
                return this.cblMediaRatioTypesAppLess.Items;
            else
                return null;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        public void Initialize()
        {
            // Enable the related MediaTypes automatically.
            List<string> mediaTypes = new List<string>();
            if (this.relatedEntityType == EntityType.PageElementEntity || this.relatedEntityType == EntityType.PageTemplateElementEntity)
            {
                ImageElement imageElement;
                if (this.relatedEntityType == EntityType.PageElementEntity)
                {
                    var pageElement = new PageElementEntity(this.relatedEntityId);
                    imageElement = PageTypeHelper.GetPageElement<ImageElement>(pageElement.PageEntity.PageTypeAsEnum, pageElement.SystemName);
                }
                else
                {
                    var pageTemplateElement = new PageTemplateElementEntity(this.relatedEntityId);
                    imageElement = PageTypeHelper.GetPageElement<ImageElement>(pageTemplateElement.PageTemplateEntity.PageType.ToEnum<PageType>(), pageTemplateElement.SystemName);
                }

                foreach (var mediaTypeGroup in imageElement.GetMediaTypeGroups())
                {
                    var types = MediaRatioTypes.GetMediaRatioTypes(mediaTypeGroup, this.relatedEntityType.ToString());
                    if (types.Count > 0)
                    {
                        mediaTypes.AddRange(types.Select(x => x.MediaType.ToIntString()));
                    }
                }

                foreach (MediaType elementMediaType in imageElement.GetMediaTypes())
                {
                    mediaTypes.Add(elementMediaType.ToIntString());
                }
            }

            List<MediaRatioType> mediaRatioTypes = null;

            int mediaType = 0;

            // Check whether we have a related entity type specified
            if (Dionysos.Web.QueryStringHelper.GetInt("mediaType", out mediaType))
            {
                mediaRatioTypes = new List<MediaRatioType>();

                MediaRatioType mediaRatioType = MediaRatioTypes.GetMediaRatioType((MediaType)mediaType);
                if (mediaRatioType != null)
                    mediaRatioTypes.Add(mediaRatioType);
            }
            else if (Dionysos.Web.QueryStringHelper.HasValue<string>("RelatedEntityType"))
            {
                mediaRatioTypes = MediaRatioTypes.GetMediaRatioTypes(this.relatedEntityType.ToString());

                // Check if we need a particular set of MediaTypes
                string mediaTypesStr;
                if (Dionysos.Web.QueryStringHelper.TryGetValue("MediaTypes", out mediaTypesStr))
                {
                    string[] mediaTypesString = mediaTypesStr.Split(',');

                    List<MediaRatioType> mediaRatioTypesParams = new List<MediaRatioType>();

                    foreach (string mediaTypeStr in mediaTypesString)
                    {
                        int mediaTypeInt = Int32.Parse(mediaTypeStr);
                        var type = mediaRatioTypes.SingleOrDefault(mrt => (int)mrt.MediaType == mediaTypeInt);

                        if (type != null)
                            mediaRatioTypesParams.Add(type);
                    }

                    mediaRatioTypes = mediaRatioTypesParams;
                }
            }
            else
            { mediaRatioTypes = this.mediaEntity.MediaRatioTypesForRelatedEntity; }


            // Initialize new lists
            this.archosMediaRatioTypes = new List<MediaRatioType>();
            this.inRoomTabletMediaRatioTypes = new List<MediaRatioType>();
            this.tvMediaRatioTypes = new List<MediaRatioType>();
            this.otouchoMediaRatioTypes = new List<MediaRatioType>();
            this.mobileAndroidMediaRatioTypes = new List<MediaRatioType>();
            this.mobileIphoneMediaRatioTypes = new List<MediaRatioType>();
            this.ipadMediaRatioTypes = new List<MediaRatioType>();
            this.applessMediaRatioTypes = new List<MediaRatioType>();
            this.miscMediaRatioTypes = new List<MediaRatioType>();

            CompanyEntity currentCompany = null;
            if (CmsSessionHelper.CurrentCompanyId > 0)
                currentCompany = new CompanyEntity(CmsSessionHelper.CurrentCompanyId);

            if (currentCompany != null)
            {
                // Seperate the media ratio types per device
                foreach (MediaRatioType mediaRatioType in mediaRatioTypes)
                {
                    // Only display generic media ratio types or for the proper systemtype (Otoucho or Crave)
                    if (mediaRatioType.MediaType.ToString().ToLower().Contains("generic") || (!currentCompany.IsNew && currentCompany.SystemType == mediaRatioType.SystemType))
                    {
                        if (mediaRatioType.Name.ToLower().Contains("800x480"))
                            this.archosMediaRatioTypes.Add(mediaRatioType);
                        else if (mediaRatioType.Name.ToLower().Contains("1280x800"))
                            this.inRoomTabletMediaRatioTypes.Add(mediaRatioType);
                        else if (mediaRatioType.Name.ToLower().Contains("1280x720"))
                            this.tvMediaRatioTypes.Add(mediaRatioType);
                        else if (mediaRatioType.Name.ToLower().Contains("otoucho"))
                            this.otouchoMediaRatioTypes.Add(mediaRatioType);
                        else if (mediaRatioType.Name.ToLower().Contains("iphone"))
                            this.mobileIphoneMediaRatioTypes.Add(mediaRatioType);
                        else if (mediaRatioType.Name.ToLower().EndsWith("(phone)") || mediaRatioType.Name.ToLower().EndsWith("(tablet)"))
                            this.mobileAndroidMediaRatioTypes.Add(mediaRatioType);
                        else if (mediaRatioType.Name.ToLower().Contains("ipad"))
                            this.ipadMediaRatioTypes.Add(mediaRatioType);
                        else if (mediaRatioType.Name.ToLower().Contains("appless"))
                            this.applessMediaRatioTypes.Add(mediaRatioType);
                        else
                            this.miscMediaRatioTypes.Add(mediaRatioType);
                    }
                }
            }

            // Render the checkbox lists
            this.RenderMediaRatioTypes(DeviceCategory.Archos, this.archosMediaRatioTypes, mediaType);
            this.RenderMediaRatioTypes(DeviceCategory.InRoomTablet, this.inRoomTabletMediaRatioTypes, mediaType);
            this.RenderMediaRatioTypes(DeviceCategory.TvBox, this.tvMediaRatioTypes, mediaType);
            this.RenderMediaRatioTypes(DeviceCategory.Otoucho, this.otouchoMediaRatioTypes, mediaType);
            this.RenderMediaRatioTypes(DeviceCategory.MobileAndroid, this.mobileAndroidMediaRatioTypes, mediaType);
            this.RenderMediaRatioTypes(DeviceCategory.MobileIphone, this.mobileIphoneMediaRatioTypes, mediaType);
            this.RenderMediaRatioTypes(DeviceCategory.Ipad, this.ipadMediaRatioTypes, mediaType);
            this.RenderMediaRatioTypes(DeviceCategory.Misc, this.miscMediaRatioTypes, mediaType);
            this.RenderMediaRatioTypes(DeviceCategory.AppLess, this.applessMediaRatioTypes, mediaType);

            // Hide the panels if there aren't any dimensions
            if (this.cblMediaRatioTypesArchos.Items.Count <= 0)
                this.pnlArchos.Visible = false;

            if (this.cblMediaRatioTypesInRoomTablet.Items.Count <= 0)
                this.pnlInRoomTablet.Visible = false;

            if (this.cblMediaRatioTypesAndroidTVBox.Items.Count <= 0)
                this.pnlAndroidTvBox.Visible = false;

            if (this.cblMediaRatioTypesOtoucho.Items.Count <= 0)
                this.pnlOtoucho.Visible = false;

            if (this.cblMediaRatioTypesMisc.Items.Count <= 0)
                this.pnlMisc.Visible = false;

            if (this.cblMediaRatioTypesMobileAndroid.Items.Count <= 0)
                this.pnlMobileAndroid.Visible = false;

            if (this.cblMediaRatioTypesMobileIphone.Items.Count <= 0)
                this.pnlMobileIphone.Visible = false;

            if (this.cblMediaRatioTypesIpad.Items.Count <= 0)
                this.pnlIpad.Visible = false;

            if (this.cblMediaRatioTypesAppLess.Items.Count <= 0)
                this.pnlAppLess.Visible = false;

            List<CheckBoxList> checkboxLists = new List<CheckBoxList> { this.cblMediaRatioTypesAndroidTVBox, this.cblMediaRatioTypesArchos, this.cblMediaRatioTypesMisc, this.cblMediaRatioTypesMobileAndroid, this.cblMediaRatioTypesOtoucho, this.cblMediaRatioTypesInRoomTablet, this.cblMediaRatioTypesIpad, this.cblMediaRatioTypesMobileIphone, this.cblMediaRatioTypesAppLess };

            foreach (var checkboxList in checkboxLists)
            {
                foreach (ListItem item in checkboxList.Items)
                {
                    if (mediaTypes.Contains(item.Value))
                        item.Selected = true;
                }
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (!IsPostBack)
            {
                this.Initialize();                
            }

            base.OnPreRender(e);
        }

        private MediaEntity mediaEntity = null;

        public MediaEntity MediaEntity
        {
            set { this.mediaEntity = value; }
        }

        private EntityType relatedEntityType;

        public EntityType RelatedEntityType
        {
            set { relatedEntityType = value; }
        }

        private int relatedEntityId;

        public int RelatedEntityId
        {
            set { this.relatedEntityId = value; }
        }
    }
}