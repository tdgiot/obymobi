﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Crave.Api.Logic.Requests.Publishing;
using Dionysos;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Generic.SubPanels
{
    public partial class PublishingCollection : Dionysos.Web.UI.DevExControls.SubPanelLLBLOverviewDataSourceCollection
    {
        #region Properties

        public PublishRequestBase PublishRequest { get; set; }

        #endregion

        #region Methods

        private void HookupEvents()
        {
            this.gvcsPublishingCollection.GridView.DataBound += GridView_DataBound;
            this.gvcsPublishingCollection.GridView.HtmlDataCellPrepared += GridView_HtmlDataCellPrepared;
            this.gvcsPublishingCollection.GridView.HtmlRowPrepared += GridView_HtmlRowPrepared;
        }

        private void SetDataSourceFilter()
        {
            LLBLGenProDataSource datasource = this.DataSource as LLBLGenProDataSource;
            if (datasource != null)
            {
                string publishRequestFullTypeName = this.PublishRequest.GetType().FullName;

                PredicateExpression filter = new PredicateExpression();
                filter.Add(PublishingFields.PublishRequest == this.PublishRequest.GetType().FullName);

                Dictionary<string, object> parameterNamesAndValues = this.PublishRequest.Timestamp.ParameterNamesAndValues;
                int counter = 1;

                foreach (string parameterName in parameterNamesAndValues.Keys)
                {
                    if (counter == 1)
                    {
                        filter.Add(PublishingFields.Parameter1Name == parameterName);
                        filter.Add(PublishingFields.Parameter1Value == parameterNamesAndValues[parameterName].ToString());
                    }
                    else if (counter == 2)
                    {
                        filter.Add(PublishingFields.Parameter2Name == parameterName);
                        filter.Add(PublishingFields.Parameter2Value == parameterNamesAndValues[parameterName].ToString());
                    }
                    counter++;
                }
                datasource.FilterToUse = filter;
            }
        }

        public void SetGui()
        {
            DateTime? lastModifiedUtc = this.PublishRequest.Timestamp.GetLastModifiedUtc();
            if (lastModifiedUtc.HasValue)
                this.lblLastModifiedValue.Text = lastModifiedUtc.Value.UtcToLocalTime(CmsSessionHelper.CurrentUser.TimeZoneInfo).ToString();
            else
                this.lblLastModifiedValue.Text = "Not modified yet";

            DateTime? lastPublishedUtc = this.PublishRequest.Timestamp.GetLastPublishedUtc();
            if (lastPublishedUtc.HasValue)
                this.lblLastPublishValue.Text = lastPublishedUtc.Value.UtcToLocalTime(CmsSessionHelper.CurrentUser.TimeZoneInfo).ToString();
            else
                this.lblLastPublishValue.Text = "Not published yet";

            if (this.PublishRequest.Timestamp.HasUnpublishedChanges())
            {
                this.lblLastPublish.ForeColor = Color.Red;
                this.lblLastPublishValue.ForeColor = Color.Red;
            }
            else
            {
                this.lblLastPublish.ForeColor = Color.Black;
                this.lblLastPublishValue.ForeColor = Color.Black;
            }
        }

        #endregion

        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.EntityName = "Publishing";
            this.EntityPageUrl = "~/Generic/Publishing.aspx";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.HookupEvents();
            this.SetDataSourceFilter();
            this.SetGui();

            this.MainGridView.ShowDeleteColumn = false;
            this.MainGridView.ShowDeleteHyperlinkColumn = false;
        }

        private void GridView_HtmlRowPrepared(object sender, DevExpress.Web.ASPxGridViewTableRowEventArgs e)
        {
            string statusText = e.GetValue("StatusText") as string;
            if (!statusText.IsNullOrWhiteSpace())
            {
                if (statusText == "Failure")
                    e.Row.ForeColor = Color.Red;
                else if (statusText == "Nothing To Publish")
                    e.Row.ForeColor = Color.Orange;
            }
        }

        private void GridView_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.DataColumn.FieldName.Equals("PublishedUTC"))
            {
                object value = e.GetValue("PublishedUTC");
                if (value != null)
                {
                    DateTime publishedUTC = (DateTime)value;
                    e.Cell.Text = publishedUTC.UtcToLocalTime(CmsSessionHelper.CurrentUser.TimeZoneInfo).ToString();
                }
            }
        }

        private void GridView_DataBound(object sender, EventArgs e)
        {
            this.gvcsPublishingCollection.GridView.SortBy(this.gvcsPublishingCollection.GridView.Columns[1], DevExpress.Data.ColumnSortOrder.Descending);
        }

        #endregion
    }
}