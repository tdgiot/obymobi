using Dionysos;
using Dionysos.Reflection;
using Dionysos.Web.UI;
using Obymobi.Data;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.ObymobiCms.Generic.SubPanels
{
    public partial class AttachmentCollection : Dionysos.Web.UI.DevExControls.SubPanelLLBLOverviewDataSourceCollection, IAttachmentCollection
    {
        #region Fields

        private readonly AttachmentType[] nonDuplicateAttachmentTypes = new[] { AttachmentType.AffiliatePrices, AttachmentType.AffiliateVideoAgent };

        #endregion

        #region Methods

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            this.EntityName = "Attachment";
            this.EntityPageUrl = "~/Generic/Attachment.aspx";

            this.Parent.DataSourceLoaded += Parent_DataSourceLoaded;
        }

        void Parent_DataSourceLoaded(object sender)
        {
            this.SetGui();
        }

        protected void SetGui()
        {
            this.plhAttachment.Visible = !this.InheritFromBrand;

            this.RenderAttachmentType();
            this.RenderBrandAttachments();

            this.btAdd.Click += btAdd_Click;
        }

        protected void RenderAttachmentType()
        {
            foreach (AttachmentType attachmentType in this.GetAllowedAttachmentTypes())
            {
                if (attachmentType == AttachmentType.AffiliateBookOnline)
                {
                    continue;
                }

                this.ddlAttachmentType.Items.Add(attachmentType.GetStringValue(), (int)attachmentType);
            }
        }

        protected void RenderBrandAttachments()
        {
            if (this.DataSourceAsBrandRelatedChildEntity?.BrandParent == null)
            {
                return;
            }

            BrandRole? userBrandRole = CmsSessionHelper.GetUserRoleForBrand(this.DataSourceAsBrandRelatedChildEntity.ParentBrandId.GetValueOrDefault());
            if (userBrandRole != null)
            {
                ICollection<AttachmentEntity> brandAttachmentCollection = Member.GetMemberValue(this.DataSourceAsBrandRelatedChildEntity.BrandParent, "AttachmentCollection") as ICollection<AttachmentEntity>;
                if (brandAttachmentCollection != null && brandAttachmentCollection.Count > 0)
                {
                    this.pnlInheritedAttachments.Visible = true;

                    StringBuilder sb = new StringBuilder();
                    sb.Append("<table width='50%'>");
                    sb.Append("<tr>");
                    sb.AppendFormat("<td class='control'><strong>Name</strong></td>");
                    sb.AppendFormat("<td class='control'><strong>Type</strong></td>");
                    sb.AppendFormat("<td class='control'><strong>Sort order</strong></td>");
                    sb.Append("</tr>");
                    foreach (AttachmentEntity attachmentEntity in brandAttachmentCollection)
                    {
                        sb.Append("<tr>");
                        if (userBrandRole >= BrandRole.Editor)
                        {
                            sb.AppendFormat("<td class='control'><a href='{0}?id={1}' _target='_blank'>{2}</a></td>", this.ResolveUrl(this.EntityPageUrl), attachmentEntity.AttachmentId, attachmentEntity.Name);
                        }
                        else
                        {
                            sb.AppendFormat("<td class='control'>{0}</td>", attachmentEntity.Name);
                        }
                        sb.AppendFormat("<td class='control'>{0}</td>", attachmentEntity.TypeText);
                        sb.AppendFormat("<td class='control'>{0}</td>", attachmentEntity.SortOrder);
                        sb.Append("</tr>");
                    }
                    sb.Append("</table>");

                    this.plhBrandAttachments.AddHtml(sb.ToString());
                }
            }
        }

        protected void btAdd_Click(object sender, EventArgs e)
        {
            if (!this.ddlAttachmentType.Value.HasValue || this.ddlAttachmentType.Value <= 0)
            {
                this.PageAsPageLLBLGenEntity.AddInformator(InformatorType.Warning, this.PageAsPageLLBLGenEntity.Translate("ChooseAnAttachmentTypeToAdd", "Kies een attachment type om toe te voegen."));
            }
            else
            {
                AttachmentType selectedAttachmentType = (AttachmentType)this.ddlAttachmentType.Value;
                IEnumerable<AttachmentType> allowedAttachmentTypesToAdd = this.GetAllowedAttachmentTypes();

                if (!allowedAttachmentTypesToAdd.Contains(selectedAttachmentType))
                {
                    this.PageAsPageLLBLGenEntity.AddInformator(InformatorType.Warning, "The attachment type may be added only once");
                }
                else
                {
                    this.Response.Redirect(this.ResolveUrl("~/Generic/Attachment.aspx") + "?mode=add&{0}={1}&type={2}".FormatSafe(this.ParentDataSource.PrimaryKeyFields[0].Name, this.ParentDataSource.PrimaryKeyFields[0].CurrentValue.ToString(), this.ddlAttachmentType.Value));
                }
            }
        }

        private IEnumerable<AttachmentType> GetAllowedAttachmentTypes()
        {
            IEnumerable<AttachmentType> alreadyAddedAttachmentTypes = new List<AttachmentType>();
            if (this.ParentDataSource is ProductEntity productEntity)
            {
                alreadyAddedAttachmentTypes = productEntity.AttachmentCollection.Select(a => a.Type);
            }

            List<AttachmentType> results = new List<AttachmentType>();
            foreach (AttachmentType type in Enum.GetValues(typeof(AttachmentType)))
            {
                bool mayBeAddedOnlyOnce = this.nonDuplicateAttachmentTypes.Contains(type);
                bool existInCollection = alreadyAddedAttachmentTypes.Contains(type);

                if (type == AttachmentType.NotSet || (mayBeAddedOnlyOnce && existInCollection))
                {
                    continue;
                }

                results.Add(type);
            }

            return results;
        }

        #endregion

        public bool InheritFromBrand
        {
            get => this.chkInheritAttachments.Checked;
            set => this.chkInheritAttachments.Checked = value;
        }

        public new PageLLBLGenEntity Page
        {
            get
            {
                return base.Page as PageLLBLGenEntity;
            }
        }

        public INullableBrandRelatedChildEntity DataSourceAsBrandRelatedChildEntity
        {
            get
            {
                if (this.Page == null)
                {
                    return null;
                }

                return this.Page.DataSource as INullableBrandRelatedChildEntity;
            }
        }
    }
}