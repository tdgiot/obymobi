﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Dionysos.Web.UI;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Web.UI.DevExControls;
using Obymobi.Logic.HelperClasses;
using Dionysos.Web.UI.WebControls;

namespace Obymobi.ObymobiCms.Generic.SubPanels
{
    public partial class BusinesshourPanel : System.Web.UI.UserControl, Dionysos.Interfaces.ISaveableControl
    {
        #region Fields

        private int? pointOfInterestId = null;
        
        #endregion

        #region Methods

        protected override void OnInit(EventArgs e)
        {
            Dionysos.Web.UI.MasterPage.LinkedCssFiles.Add("~/css/catalog.css", 1000);
            base.OnInit(e);            
            this.Page.DataSourceLoaded += new Dionysos.Delegates.Web.DataSourceLoadedHandler(Page_DataSourceLoaded);
        }

        BusinesshoursCollection GetRelatedBusinesshours()
        {
            PredicateExpression filter = new PredicateExpression();

            if (this.pointOfInterestId.HasValue)
                filter.Add(BusinesshoursFields.PointOfInterestId == this.pointOfInterestId);

            SortExpression sort = new SortExpression();
            sort.Add(BusinesshoursFields.DayOfWeekAndTime | SortOperator.Ascending);

            BusinesshoursCollection businessHours = new BusinesshoursCollection();
            businessHours.GetMulti(filter, 0, sort);

            return businessHours;
        }

        public bool Save()
        {
            BusinesshoursCollection existingBusinesshours = new BusinesshoursCollection();
            existingBusinesshours.GetMulti(new PredicateExpression(BusinesshoursFields.PointOfInterestId == this.pointOfInterestId));
            foreach (BusinesshoursEntity hours in existingBusinesshours)
            {
                hours.Delete();
            }

            if ((teDayAllBegin1.Value.HasValue && teDayAllEnd1.Value.HasValue && teDayAllBegin1.Value != teDayAllEnd1.Value) ||
                (teDayAllBegin2.Value.HasValue && teDayAllEnd2.Value.HasValue && teDayAllBegin2.Value != teDayAllEnd2.Value) ||
                (teDayAllBegin3.Value.HasValue && teDayAllEnd3.Value.HasValue && teDayAllBegin3.Value != teDayAllEnd3.Value))
            {
                // Values for all days
                for (int iInterval = 1; iInterval < 4; iInterval++)
                {
                    string controlNameBegin = string.Format("teDayAllBegin{0}", iInterval);
                    string controlNameEnd = string.Format("teDayAllEnd{0}", iInterval);

                    TimePicker2 teBegin = this.FindControl(controlNameBegin) as TimePicker2;
                    TimePicker2 teEnd = this.FindControl(controlNameEnd) as TimePicker2;

                    if (teBegin.Value.HasValue && teEnd.Value.HasValue && teBegin.Value != teEnd.Value && teBegin.Value < teEnd.Value)
                    {
                        for (int iDay = 0; iDay < 7; iDay++)
                        {
                            BusinesshoursEntity opening = new BusinesshoursEntity();
                            opening.PointOfInterestId = this.pointOfInterestId;
                            opening.Opening = true;
                            opening.DayOfWeekAndTime = string.Format("{0}{1:00}{2:00}", iDay, teBegin.Value.Value.Hour, teBegin.Value.Value.Minute);
                            opening.Save();

                            BusinesshoursEntity closing = new BusinesshoursEntity();
                            closing.PointOfInterestId = this.pointOfInterestId;
                            closing.Opening = false;
                            closing.DayOfWeekAndTime = string.Format("{0}{1:00}{2:00}", iDay, teEnd.Value.Value.Hour, teEnd.Value.Value.Minute);
                            closing.Save();
                        }
                    }
                }
            }
            else
            {
                // Values for each individual day
                for (int iDay = 0; iDay < 7; iDay++)
                {
                    for (int iInterval = 1; iInterval < 4; iInterval++)
                    {
                        string controlNameBegin = string.Format("teDay{0}Begin{1}", iDay, iInterval);
                        string controlNameEnd = string.Format("teDay{0}End{1}", iDay, iInterval);

                        TimePicker2 teBegin = this.FindControl(controlNameBegin) as TimePicker2;
                        TimePicker2 teEnd = this.FindControl(controlNameEnd) as TimePicker2;

                        if (teBegin.Value.HasValue && teEnd.Value.HasValue && teBegin.Value != teEnd.Value && teBegin.Value < teEnd.Value)
                        {
                            BusinesshoursEntity opening = new BusinesshoursEntity();
                            opening.PointOfInterestId = this.pointOfInterestId;
                            opening.Opening = true;
                            opening.DayOfWeekAndTime = string.Format("{0}{1:00}{2:00}", iDay, teBegin.Value.Value.Hour, teBegin.Value.Value.Minute);
                            opening.Save();

                            BusinesshoursEntity closing = new BusinesshoursEntity();
                            closing.PointOfInterestId = this.pointOfInterestId;
                            closing.Opening = false;
                            closing.DayOfWeekAndTime = string.Format("{0}{1:00}{2:00}", iDay, teEnd.Value.Value.Hour, teEnd.Value.Value.Minute);
                            closing.Save();
                        }
                    }
                }
            }
            
            return true;
        }

        void LoadValues()
        {
            var businesshourView = this.GetRelatedBusinesshours().DefaultView;

            // Check if all days are the same
            bool allDays = true;
            string dayValue = string.Empty;
            for (int iDay = 0; iDay < 7; iDay++)
            {
                PredicateExpression filter = new PredicateExpression();
                filter.Add(new FieldLikePredicate(BusinesshoursFields.DayOfWeekAndTime, string.Format("{0}%", iDay)));

                businesshourView.Filter = filter;

                string compoundValue = string.Empty;
                foreach(BusinesshoursEntity businesshours in businesshourView)
                {
                    compoundValue += businesshours.DayOfWeekAndTime.Substring(1, 4);
                }

                if(iDay == 0)
                {
                    dayValue = compoundValue;
                }
                else if(compoundValue != dayValue)
                {
                    allDays = false;
                    break;
                }
            }

            if (allDays)
            {
                // All days

                // All days are the same so we can just use the values for sunday
                PredicateExpression filter = new PredicateExpression();
                filter.Add(new FieldLikePredicate(BusinesshoursFields.DayOfWeekAndTime, "0%"));
                businesshourView.Filter = filter;

                int openingInterval = 1;
                int closingInterval = 1;
                foreach(BusinesshoursEntity businesshours in businesshourView)
                {
                    string controlName = string.Empty;
                    if (businesshours.Opening)
                    {
                        controlName = string.Format("teDayAllBegin{0}", openingInterval++);
                    }
                    else
                    {
                        controlName = string.Format("teDayAllEnd{0}", closingInterval++);
                    }

                    TimePicker2 control = this.FindControl(controlName) as TimePicker2;

                    int hours = Convert.ToInt32(businesshours.DayOfWeekAndTime.Substring(1, 2));
                    int minutes = Convert.ToInt32(businesshours.DayOfWeekAndTime.Substring(3, 2));

                    control.Value = new DateTime(2000, 1, 1, hours, minutes, 0);

                    Label lblAvailable = this.FindControl("lblDayAllAvailable") as Label;
                    if (businesshours.Opening && openingInterval > 2)
                    {
                        lblAvailable.Text += ", ";
                    }
                    else if(!businesshours.Opening)
                    {
                        lblAvailable.Text += " - ";
                    }
                    lblAvailable.Text += businesshours.TimeString;
                }
            }
            else
            {
                // Individual days
                businesshourView.Filter = new PredicateExpression();

                int[] openingIntervals = new int[] {1, 1, 1, 1, 1, 1, 1};
                int[] closingIntervals = new int[] {1, 1, 1, 1, 1, 1, 1};
                foreach (BusinesshoursEntity businesshours in businesshourView)
                {
                    int day = Convert.ToInt32(businesshours.DayOfWeekAndTime.Substring(0, 1));
                    int hours = Convert.ToInt32(businesshours.DayOfWeekAndTime.Substring(1, 2));
                    int minutes = Convert.ToInt32(businesshours.DayOfWeekAndTime.Substring(3, 2));

                    string controlName = string.Empty;
                    if (businesshours.Opening)
                    {
                        controlName = string.Format("teDay{0}Begin{1}", day, openingIntervals[day]++);
                    }
                    else
                    {
                        controlName = string.Format("teDay{0}End{1}", day, closingIntervals[day]++);
                    }

                    TimePicker2 control = this.FindControl(controlName) as TimePicker2;
                    control.Value = new DateTime(2000, 1, 1, hours, minutes, 0);

                    string controlAvailable = string.Format("lblDay{0}Available", day);
                    Label lblAvailable = this.FindControl(controlAvailable) as Label;
                    if (businesshours.Opening && openingIntervals[day] > 2)
                    {
                        lblAvailable.Text += ", ";
                    }
                    else if (!businesshours.Opening)
                    {
                        lblAvailable.Text += " - ";
                    }
                    lblAvailable.Text += businesshours.TimeString;
                }
            }
        }

        #endregion

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        void Page_DataSourceLoaded(object sender)
        {
            if (this.Page.DataSource is PointOfInterestEntity)
                this.pointOfInterestId = ((PointOfInterestEntity)this.Page.DataSource).PointOfInterestId;
            else
                throw new NotImplementedException();

            this.LoadValues();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the pointOfInterestId
        /// </summary>
        public int? PointOfInterestId
        {
            get
            {
                return this.pointOfInterestId;
            }
            set
            {
                this.pointOfInterestId = value;
            }
        }

        public new PageLLBLGenEntity Page
        {
            get
            {
                return base.Page as PageLLBLGenEntity;
            }
        }

        #endregion

    }
}