﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Generic.Subpanels.CompanyBrandCollection" Codebehind="CompanyBrandCollection.ascx.cs" %>
<D:PlaceHolder runat="server" ID="phlAddCompany">
<div style="overflow:hidden;margin-bottom:10px;">
Company: <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlCompanyId" UseDataBinding="true" EntityName="Company" TextField="Name" PreventEntityCollectionInitialization="true" Width="250px" notdirty="true"></X:ComboBoxLLBLGenEntityCollection><br />
<D:Button runat="server" ID="btnAdd" Text="Add brand to company" ToolTip="Add" />   
</div>
</D:PlaceHolder>
<X:GridViewColumnSelector runat="server" ID="gvcsBrand">
	<Columns />
</X:GridViewColumnSelector>

