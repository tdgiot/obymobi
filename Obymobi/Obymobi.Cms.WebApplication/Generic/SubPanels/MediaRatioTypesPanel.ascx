﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Generic.SubPanels.MediaRatioTypesPanel" Codebehind="MediaRatioTypesPanel.ascx.cs" %>
<D:Panel ID="pnlArchos" runat="server" GroupingText="Archos dimensions" Visible="false">
    <table class="dataformV2">
        <tr>
            <td class="label">
				<D:Label runat="server" ID="lblMediaRatioTypesArchos">Archos</D:Label>
			</td>
            <td class="label">
                                        
            </td>
			<td class="control">
                <D:CheckBox ID="cbArchosAllNone" runat="server" CssClass="checkbox-checkall" onclick="CheckAll(this, 'cblMediaRatioTypesArchos');" Text="Check all/none"/>
				<D:CheckBoxList ID="cblMediaRatioTypesArchos" runat="server" EnableCheckAll="true" onclick="CheckAll(this);" Name="cblMediaRatioTypesArchos"></D:CheckBoxList>
			</td>         
        </tr>
    </table>
</D:Panel>      
<D:Panel ID="pnlOtoucho" runat="server" GroupingText="Otoucho dimensions">
        <table class="dataformV2">
            <tr>
                <td class="label">
					<D:Label runat="server" ID="lblMediaRatioTypesOtoucho">Otoucho</D:Label>
				</td>
                <td class="label">
                </td>
				<td class="control">
                    <D:CheckBox ID="cbOtouchoAllNone" runat="server" CssClass="checkbox-checkall" onclick="CheckAll(this, 'cblMediaRatioTypesOtoucho');" Text="Check all/none"/>
					<D:CheckBoxList ID="cblMediaRatioTypesOtoucho" runat="server" EnableCheckAll="true" onclick="CheckAll(this);" Name="cblMediaRatioTypesOtoucho"></D:CheckBoxList>
				</td>         
            </tr>
        </table>
</D:Panel>
<D:Panel ID="pnlInRoomTablet" runat="server" GroupingText="In-Room Tablet dimensions">
        <table class="dataformV2">
            <tr>
                <td class="label">
					<D:Label runat="server" ID="lblMediaRatioTypesInRoomTablet">In-Room Tablet</D:Label>
				</td>
                <td class="label">
                </td>
				<td class="control">
                    <D:CheckBox ID="cbSamsungAllNone" runat="server" CssClass="checkbox-checkall" onclick="CheckAll(this, 'cblMediaRatioTypesInRoomTablet');" Text="Check all/none"/>
					<D:CheckBoxList ID="cblMediaRatioTypesInRoomTablet" runat="server" EnableCheckAll="true" onclick="CheckAll(this);" Name="cblMediaRatioTypesInRoomTablet"></D:CheckBoxList>
				</td>         
            </tr>
        </table>
</D:Panel>
<D:Panel ID="pnlAndroidTvBox" runat="server" GroupingText="Android TV box dimensions" Visible="false">
        <table class="dataformV2">
            <tr>
                <td class="label">
					<D:Label runat="server" ID="lblMediaRatioTypesAndroidTVBox">Android TV Box</D:Label>
				</td>
                <td class="label">
                </td>
				<td class="control">
                    <D:CheckBox ID="cbTvBoxAllNone" runat="server" CssClass="checkbox-checkall" onclick="CheckAll(this, 'cblMediaRatioTypesAndroidTVBox');" Text="Check all/none"/>
					<D:CheckBoxList ID="cblMediaRatioTypesAndroidTVBox" runat="server" EnableCheckAll="true" onclick="CheckAll(this);" Name="cblMediaRatioTypesAndroidTVBox"></D:CheckBoxList>
				</td>         
            </tr>
        </table>
</D:Panel>
<D:Panel ID="pnlMobileAndroid" runat="server" GroupingText="Mobile (Android) dimensions">
        <table class="dataformV2">
            <tr>
                <td class="label">
					<D:Label runat="server" ID="lblMediaRatioTypesMobileAndroid">Mobile (android)</D:Label>
				</td>
                <td class="label">
                </td>
				<td class="control">
                    <D:CheckBox ID="cbMobileAndroidAllNone" runat="server" CssClass="checkbox-checkall" onclick="CheckAll(this, 'cblMediaRatioTypesMobileAndroid');" Text="Check all/none"/>
					<D:CheckBoxList ID="cblMediaRatioTypesMobileAndroid" runat="server" EnableCheckAll="true" onclick="CheckAll(this);" Name="cblMediaRatioTypesMobileAndroid"></D:CheckBoxList>
				</td>         
            </tr>
        </table>
</D:Panel>
<D:Panel ID="pnlMobileIphone" runat="server" GroupingText="Mobile (Iphone) dimensions">
        <table class="dataformV2">
            <tr>
                <td class="label">
					<D:Label runat="server" ID="lblMediaRatioTypesMobileIphone">Mobile (iphone)</D:Label>
				</td>
                <td class="label">
                </td>
				<td class="control">
                    <D:CheckBox ID="cbMobileIphoneAllNone" runat="server" CssClass="checkbox-checkall" onclick="CheckAll(this, 'cblMediaRatioTypesMobileIphone');" Text="Check all/none"/>
					<D:CheckBoxList ID="cblMediaRatioTypesMobileIphone" runat="server" EnableCheckAll="true" onclick="CheckAll(this);" Name="cblMediaRatioTypesMobileIphone"></D:CheckBoxList>
				</td>         
            </tr>
        </table>
</D:Panel>
<D:Panel ID="pnlIpad" runat="server" GroupingText="Ipad dimensions">
        <table class="dataformV2">
            <tr>
                <td class="label">
					<D:Label runat="server" ID="lblMediaRatioTypesIpad">Ipad</D:Label>
				</td>
                <td class="label">
                </td>
				<td class="control">
                    <D:CheckBox ID="cbIpadAllNone" runat="server" CssClass="checkbox-checkall" onclick="CheckAll(this, 'cblMediaRatioTypesIpad');" Text="Check all/none"/>
					<D:CheckBoxList ID="cblMediaRatioTypesIpad" runat="server" EnableCheckAll="true" onclick="CheckAll(this);" Name="cblMediaRatioTypesIpad"></D:CheckBoxList>
				</td>         
            </tr>
        </table>
</D:Panel>
<D:Panel ID="pnlAppLess" runat="server" GroupingText="AppLess dimensions">
    <table class="dataformV2">
        <tr>
            <td class="label">
				<D:Label runat="server" ID="lblAppLess">AppLess</D:Label>
			</td>
            <td class="label">
            </td>
			<td class="control">
                <D:CheckBox ID="cbAppLessAllNone" runat="server" CssClass="checkbox-checkall" onclick="CheckAll(this, 'cblMediaRatioTypesAppLess');" Text="Check all/none"/>
				<D:CheckBoxList ID="cblMediaRatioTypesAppLess" runat="server" EnableCheckAll="true" onclick="CheckAll(this);" Name="cblMediaRatioTypesAppLess"></D:CheckBoxList>
			</td>         
        </tr>
    </table>
</D:Panel>
<D:Panel ID="pnlMisc" runat="server" GroupingText="Misc dimensions">
    <table class="dataformV2">
        <tr>
            <td class="label">
				<D:Label runat="server" ID="lblMediaRatioTypesMisc">Misc</D:Label>
			</td>
            <td class="label">
            </td>
			<td class="control">
                <D:CheckBox ID="cbMiscAllNone" runat="server" CssClass="checkbox-checkall" onclick="CheckAll(this, 'cblMediaRatioTypesMisc');" Text="Check all/none"/>
				<D:CheckBoxList ID="cblMediaRatioTypesMisc" runat="server" EnableCheckAll="true" onclick="CheckAll(this);" Name="cblMediaRatioTypesMisc"></D:CheckBoxList>
			</td>         
        </tr>
    </table>
</D:Panel>