﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Generic.SubPanels.AttachmentCollection" Codebehind="AttachmentCollection.ascx.cs" %>

<D:PlaceHolder runat="server" ID="plhAttachment" Visible="true">
<div style="overflow: hidden;margin-bottom: 10px;">
	<div style="float: left; margin-right: 10px">
		<X:ComboBoxInt ID="ddlAttachmentType" runat="server" UseDataBinding="true" Width="150px" notdirty="true"></X:ComboBoxInt>
	</div>
	<div style="float: left;">
		<D:Button runat="server" ID="btAdd" CommandName="Add" Text="Add attachment" ToolTip="Add" />
	</div>
</div>
<X:GridViewColumnSelector runat="server" ID="gvcsProductAttachmentCollection">
    <columns />
</X:GridViewColumnSelector>
<br/>
</D:PlaceHolder>
<D:Panel ID="pnlInheritedAttachments" runat="server" GroupingText="Inherited attachments" Visible="False" LocalizeText="False">
    <table class="dataformV2">
        <tr>
            <td class="control"><D:CheckBox runat="server" ID="chkInheritAttachments" Text="Inherit from brand" LocalizeText="False"/></td>
        </tr>
        <tr>
            <td>
                <D:PlaceHolder runat="server" ID="plhBrandAttachments" />
            </td>
        </tr>
    </table>
</D:Panel>
