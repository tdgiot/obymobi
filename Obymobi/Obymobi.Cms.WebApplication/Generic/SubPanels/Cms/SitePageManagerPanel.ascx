﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Generic.SubPanels.Cms.SitePageManagerPanel" Codebehind="SitePageManagerPanel.ascx.cs" %>
<%@ Reference VirtualPath="~/Generic/SubPanels/Cms/PageManagerPanel.ascx" %>

<div style="overflow:hidden;margin-bottom:10px;">
   <div style="float: left; margin-top: 5px; margin-bottom: 10px;">
       <label id="lblSiteCaption" runat="server" style="float: left; margin-top: 4px;margin-right: 10px">Site: </label><X:ComboBoxLLBLGenEntityCollection ID="ddlCopySiteId" runat="server" EntityName="Site" TextField="Name" PreventEntityCollectionInitialization="true" Width="350px" notdirty="true" style="float: left;"></X:ComboBoxLLBLGenEntityCollection>
       <D:Button ID="btnRefresh" Text="Refresh" ClientIDMode="Static" runat="server" style="float: left; margin-left: 4px; visibility: hidden;" notdirty="true" />
   </div>
    <div style="float: left;">
        <table class="dataformV2">
            <tr>
                <td class="control" style="padding-left: 0;">
                    <D:PlaceHolder runat="server" ID="plhPageManagerControl"/>                
                </td>            
            </tr>            
        </table>
    </div>   
</div>
