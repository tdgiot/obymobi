﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Generic.SubPanels.Cms.CopySitePagesPanel" Codebehind="CopySitePagesPanel.ascx.cs" %>
<%@ Reference VirtualPath="~/Generic/SubPanels/Cms/SitePageManagerPanel.ascx" %>
<div style="min-width: 1180px;">
    <div style="float: left; width: 552px;">
        <D:PlaceHolder ID="plhLeftSite" runat="server"/>
    </div>
    
    <div style="float: left; width: 45px; padding: 5px 5px 5px 5px; margin-top: 95px;">            
        <D:Button ID="btnCopyLeftToRight" runat="server" Text=">>>" style="width: 100%;" />
        <D:Button ID="btnCopyRightToLeft" runat="server" Text="<<<" style="width: 100%; margin-top:4px;" /> 
    </div>

    <div style="float: left; width: 552px;">
        <D:PlaceHolder ID="plhRightSite" runat="server"/>
    </div>    
</div>