﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Web.UI.WebControls;
using DevExpress.Utils;
using DevExpress.Web;
using DevExpress.Web.ASPxTreeList;
using Dionysos;
using Dionysos.Web.UI.WebControls;
using Obymobi.Cms.Logic.Sites;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logic.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using PageDataSource = Obymobi.Cms.Logic.Sites.PageDataSource;

namespace Obymobi.ObymobiCms.Generic.SubPanels.Cms
{
    public partial class PageManagerPanel : SubPanelCustomDataSource
    {
        #region Properties

        private const string TRANSLATION_KEY_PREFIX = "Obymobi.ObymobiCms.Generic.SubPanels.Cms.PageManager.";

        private PageDataSource pageDataSource;
        private EntityType entityType;
        private SiteTemplateEntity siteTemplate;
        private SiteEntity site;
        private TreeListComboBoxColumn pageTypeColumn;

        #endregion

        #region Methods

        private void SetGUI()
        {
            
        }

        private void DeleteSelected()
        {            
            List<TreeListNode> selectedNodes = new List<TreeListNode>();
            this.RetrieveSelectedNodes(ref selectedNodes, null);

            if (selectedNodes.Count <= 0)
            {
                this.PageAsPageDefault.MultiValidatorDefault.AddError(this.PageAsPageDefault.Translate(TRANSLATION_KEY_PREFIX + "Error.NoPageSelected", "Er is geen pagina geselecteerd om te verwijderen.", true));
                this.PageAsPageDefault.Validate();
            }
            else 
            {
                if (this.DataSource is SiteEntity)
                {
                    this.DeletePages(selectedNodes);
                }
                else
                {
                    this.DeletePageTemplates(selectedNodes);
                }
                
                this.tlSiteStructure.DataBind();

                if (selectedNodes.Count == 1)
                    this.PageAsPageDefault.AddInformatorInfo(this.PageAsPageDefault.Translate(TRANSLATION_KEY_PREFIX + "Success.PageDeleted", "De geselecteerde pagina is succesvol verwijderd.", true));
                else
                    this.PageAsPageDefault.AddInformatorInfo(this.PageAsPageDefault.Translate(TRANSLATION_KEY_PREFIX + "Success.PagesDeleted", "De geselecteerde pagina's zijn succesvol verwijderd.", true));

                this.PageAsPageDefault.Validate();                            
            }            
        }

        private void DeletePages(List<TreeListNode> selectedNodes)
        {            
            EntityView<Obymobi.Data.EntityClasses.PageEntity> pagesView = ((SiteEntity)this.DataSource).PageCollection.DefaultView;

            PageCollection pagesToBeRemoved = new PageCollection();
            foreach (TreeListNode node in selectedNodes)
            {                
                int pageId = Int32.Parse(node.Key);
                pagesView.Filter = new PredicateExpression(PageFields.PageId == pageId);
                if (pagesView.Count > 0)
                {
                    Obymobi.Data.EntityClasses.PageEntity selectedPage = pagesView[0];                    
                    pagesToBeRemoved.Add(selectedPage);                    
                    pagesView.RelatedCollection.Remove(selectedPage);                                       
                }                
            }
            pagesView.Filter = null;

            foreach (var page in pagesToBeRemoved)
            {
                page.Delete();
            }                       

            // Update site timestamp
            if (this.site == null)
                this.site = this.DataSource as SiteEntity;

            this.site.LastModifiedUTC = DateTime.UtcNow;
            this.site.Save();

            this.DataSource = Obymobi.Cms.Logic.Sites.SiteHelper.GetSite(this.site.SiteId, true);
            this.InitDataSource();
        }

        private void DeletePageTemplates(List<TreeListNode> selectedNodes)
        {
            EntityView<PageTemplateEntity> pagesView = ((SiteTemplateEntity)this.DataSource).PageTemplateCollection.DefaultView;

            PageTemplateCollection pageTemplatesToBeRemoved = new PageTemplateCollection();
            foreach (TreeListNode node in selectedNodes)
            {
                int pageTemplateId = Int32.Parse(node.Key);
                pagesView.Filter = new PredicateExpression(PageTemplateFields.PageTemplateId == pageTemplateId);
                if (pagesView.Count > 0)
                {
                    Obymobi.Data.EntityClasses.PageTemplateEntity selectedPageTemplate = pagesView[0];
                    pageTemplatesToBeRemoved.Add(selectedPageTemplate);
                    pagesView.RelatedCollection.Remove(selectedPageTemplate);
                }
            }
            pagesView.Filter = null;

            foreach (var page in pageTemplatesToBeRemoved)
            {
                page.Delete();
            }

            this.DataSource = new SiteTemplateEntity(((SiteTemplateEntity)this.DataSource).SiteTemplateId);
            this.InitDataSource();
        }

        private void RetrieveSelectedNodes(ref List<TreeListNode> selectedNodes, TreeListNode parent, bool includeChildren = true)
        {
            TreeListNodeCollection nodes;
            
            if (parent == null)
                nodes = this.tlSiteStructure.Nodes;
            else
                nodes = parent.ChildNodes;                

            foreach (TreeListNode node in nodes)
            {
                if (includeChildren)
                {
                    if (node.Selected || node.ParentNode.Selected)
                    {
                        if (node.ParentNode.Selected)
                            node.Selected = true;

                        selectedNodes.Add(node);
                    }
                }
                else
                {
                    if (node.Selected && !node.ParentNode.Selected)
                        selectedNodes.Add(node);
                }

                if (node.HasChildren)                
                    this.RetrieveSelectedNodes(ref selectedNodes, node, includeChildren);
            }            
        }

        private void InitDataSource()
        {
            if (this.DataSource != null)
            {
                IEntity parentEntity = (IEntity)this.DataSource;
                int primaryKey = (int)parentEntity.PrimaryKeyFields[0].CurrentValue;

                var datasource = new List<Obymobi.Logic.Cms.IPage>();
                if (this.DataSource is SiteEntity)
                {                    
                    datasource.AddRange(((SiteEntity)this.DataSource).PageCollection);
                    this.pageDataSource = new PageDataSource(EntityType.PageEntity, primaryKey, datasource);           
                }
                else
                {
                    datasource.AddRange(((SiteTemplateEntity)this.DataSource).PageTemplateCollection);
                    this.pageDataSource = new PageDataSource(EntityType.PageTemplateEntity, primaryKey, datasource);
                }
                this.tlSiteStructure.DataSource = this.pageDataSource;
            }
        }

        private void CreateTreeList()
        {
            // Create Tree View
            this.tlSiteStructure.SettingsEditing.Mode = TreeListEditMode.Inline;
            this.tlSiteStructure.SettingsBehavior.ColumnResizeMode = ColumnResizeMode.Disabled;            
            this.tlSiteStructure.SettingsBehavior.AllowSort = false;
            this.tlSiteStructure.SettingsBehavior.AllowDragDrop = false;
            this.tlSiteStructure.ClientInstanceName = this.TreelistId;
            this.tlSiteStructure.AutoGenerateColumns = false;            
            this.tlSiteStructure.SettingsEditing.AllowNodeDragDrop = true;            
            this.tlSiteStructure.SettingsBehavior.AutoExpandAllNodes = true;
            this.tlSiteStructure.Settings.GridLines = GridLines.Both;
            this.tlSiteStructure.ClientSideEvents.EndDragNode = this.endDragNodeMethod;
            this.tlSiteStructure.ClientSideEvents.NodeDblClick = this.nodeDblClickMethod;
            this.tlSiteStructure.ClientSideEvents.CustomButtonClick = this.customButtonClickMethod;
            this.tlSiteStructure.CustomErrorText += this.tlSiteStructure_CustomErrorText;

            if (this.entityType == EntityType.SiteEntity)
                this.tlSiteStructure.KeyFieldName = "PageId";
            else
                this.tlSiteStructure.KeyFieldName = "PageTemplateId";

            this.tlSiteStructure.ParentFieldName = "ParentPageId";

            // Name Column
            var nameColumn = new TreeListTextColumn();
            nameColumn.Caption = "Page";            
            nameColumn.Width = Unit.Pixel(335);
            nameColumn.VisibleIndex = 0;
            nameColumn.FieldName = "Name";
            nameColumn.CellStyle.Wrap = DefaultBoolean.True;
            this.tlSiteStructure.Columns.Add(nameColumn);

            // Page Type Column
            this.RefreshPageTypeColumn();            
            
            // In-line Edit Column
            var editCommandColumn = new TreeListCommandColumn();
            editCommandColumn.VisibleIndex = 4;
            editCommandColumn.Width = Unit.Pixel(60);
            editCommandColumn.ShowNewButtonInHeader = true;
            editCommandColumn.EditButton.Visible = true;
            editCommandColumn.NewButton.Visible = true;
            editCommandColumn.DeleteButton.Visible = true;
            editCommandColumn.ButtonType = ButtonType.Image;
            editCommandColumn.EditButton.Image.Url = this.ResolveUrl("~/images/icons/pencil.png");
            editCommandColumn.DeleteButton.Image.Url = this.ResolveUrl("~/images/icons/delete.png");
            editCommandColumn.CancelButton.Image.Url = this.ResolveUrl("~/images/icons/cross_grey.png");
            editCommandColumn.NewButton.Image.Url = this.ResolveUrl("~/images/icons/add2.png");
            editCommandColumn.UpdateButton.Image.Url = this.ResolveUrl("~/images/icons/disk.png");
            
            this.tlSiteStructure.Columns.Add(editCommandColumn);

            // Edit Page Colum
            var editPageColumn = new TreeListHyperLinkColumn();
            editPageColumn.VisibleIndex = 5;
            editPageColumn.Caption = "&nbsp;";
            editPageColumn.Width = Unit.Pixel(15);
            editPageColumn.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            editPageColumn.PropertiesHyperLink.ImageUrl = "~/images/icons/page_edit.png";
            editPageColumn.EditCellStyle.CssClass = "hidden";
            if (this.entityType == EntityType.SiteEntity)
            {                
                editPageColumn.FieldName = "PageId";
                editPageColumn.PropertiesHyperLink.NavigateUrlFormatString = "~/Cms/Page.aspx?id={0}";
            }
            else
            {
                editPageColumn.FieldName = "PageTemplateId";
                editPageColumn.PropertiesHyperLink.NavigateUrlFormatString = "~/Cms/PageTemplate.aspx?id={0}";
            }
            this.tlSiteStructure.Columns.Add(editPageColumn);

            // Events
            this.tlSiteStructure.CustomCallback += tlSiteStructure_CustomCallback;
            this.tlSiteStructure.SettingsEditing.Mode = TreeListEditMode.Inline;
            this.tlSiteStructure.NodeValidating += tlSiteStructure_NodeValidating;            
            this.tlSiteStructure.HtmlDataCellPrepared += tlSiteStructure_HtmlDataCellPrepared;
            this.tlSiteStructure.NodeUpdating += tlSiteStructure_NodeUpdating;

            // Data Source must be set in OnInit: http://documentation.devexpress.com/#AspNet/DevExpressWebASPxTreeListASPxTreeList_DataSourcetopic
            this.InitDataSource();

            // Width based on a 1280 width screen (fair requirement for CMS use?)
            // http://www.w3schools.com/browsers/browsers_display.asp | http://gs.statcounter.com/#desktop-resolution-ww-monthly-201212-201312            
            this.tlSiteStructure.Width = Unit.Pixel(550);            
        }

        private void RefreshPageTypeColumn()
        {
            // Get Page Types for SiteType
            Obymobi.Logic.Cms.SiteType siteType = Obymobi.Logic.Cms.SiteType.Unknown;
            if (this.site != null)
                siteType = this.site.SiteTypeAsEnum;
            else if (this.siteTemplate != null)
                siteType = this.siteTemplate.SiteTypeAsEnum; // Didnt'check for null because it's checked a parent datasource loaded event
            else if (this.DataSource != null && this.DataSource is SiteEntity)
                siteType = ((SiteEntity)this.DataSource).SiteTypeAsEnum;

            if (siteType != Obymobi.Logic.Cms.SiteType.Unknown)
            {
                var column = this.tlSiteStructure.Columns["PageType"];
                if (column == null)
                {
                    this.pageTypeColumn = new TreeListComboBoxColumn();
                    this.pageTypeColumn.Caption = "Page Type";
                    this.pageTypeColumn.FieldName = "PageType";
                    this.pageTypeColumn.Width = Unit.Pixel(160);
                    this.pageTypeColumn.VisibleIndex = 1;
                    this.pageTypeColumn.PropertiesComboBox.ValueType = typeof(Int32);
                    this.pageTypeColumn.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                    this.pageTypeColumn.PropertiesEdit.Style.HorizontalAlign = HorizontalAlign.Left;
                    this.pageTypeColumn.CellStyle.Wrap = DefaultBoolean.True;
                    this.pageTypeColumn.PropertiesComboBox.IncrementalFilteringMode = DevExpress.Web.IncrementalFilteringMode.Contains;
                    this.pageTypeColumn.PropertiesComboBox.Items.Clear();

                    var pageTypes = Obymobi.Logic.Cms.PageTypeHelper.GetPageTypes(siteType);
                    foreach (var pageType in pageTypes)
                    {
                        //if (this.entityType == EntityType.SiteEntity || pageType.PageType != Obymobi.Logic.Cms.PageType.PageLink)
                            this.pageTypeColumn.PropertiesComboBox.Items.Add(pageType.Name, (int)pageType.PageType);
                    }

                    this.tlSiteStructure.Columns.Add(this.pageTypeColumn);     
                }                           
            }
        }

        private void tlSiteStructure_NodeUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            Debug.WriteLine(string.Format("Old: {0} | New: {1} | Keys: {2}", e.OldValues, e.NewValues, e.Keys));
        }

        public void Refresh(bool expandAll = true)
        {
            this.InitDataSource();
            this.tlSiteStructure.DataBind();
            if (expandAll)
                this.tlSiteStructure.ExpandAll();
        }        

        #endregion

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            // Init the toolbar
            this.hlExpandAll.NavigateUrl = string.Format("javascript:{0}.ExpandAll()", this.TreelistId);
            this.hlCollapseAll.NavigateUrl = string.Format("javascript:{0}.CollapseAll()", this.TreelistId);                                   
        }

        public void Init(EntityType entityType)
        {
            this.entityType = entityType;
            this.CreateTreeList();
            this.SetGUI();
        }

        public void RefreshDataSource(IEntity datasource)
        {
            this.DataSource = datasource;
            if (this.DataSource != null && !((IEntity)this.DataSource).IsNew)
            {
                if (this.DataSource is SiteEntity)
                {                    
                    this.site = (SiteEntity)this.DataSource;
                }
                else if (this.DataSource is SiteTemplateEntity)
                {                    
                    this.siteTemplate = (SiteTemplateEntity)this.DataSource;
                }
                else
                    throw new NotImplementedException("Not implemented for EntityType: " + ((IEntity)this.DataSource).LLBLGenProEntityName);

                this.RefreshPageTypeColumn();
                this.Refresh();
            }            
        }

        private void tlSiteStructure_CustomErrorText(object sender, TreeListCustomErrorTextEventArgs e)
        {
            if (e.Exception.InnerException != null)
            {
                var exception = e.Exception.InnerException as ObymobiException;
                if (exception != null)
                {
                    e.ErrorText = exception.AdditionalMessage;
                }
                else
                {
                    e.ErrorText = e.Exception.InnerException.Message;
                }
            }
        }

        private void tlSiteStructure_HtmlDataCellPrepared(object sender, TreeListHtmlDataCellEventArgs e)
        {
            object obj = e.GetValue("Visible");
            if (obj != null)
            {
                bool visible = (bool)obj;
                if (!visible)
                    e.Cell.ForeColor = Color.Gray;
            }
        }        

        private void tlSiteStructure_NodeValidating(object sender, TreeListNodeValidationEventArgs e)
        {
            if (((string)e.NewValues["Name"]).IsNullOrWhiteSpace())
                e.Errors["Name"] = this.PageAsPageDefault.Translate(TRANSLATION_KEY_PREFIX + "Error.PageNameNotEntered", "Er moet een 'Naam' worden opgegeven voor de pagina.", true);
            if (e.NewValues["PageType"] == null || /* Null */
                (e.NewValues["PageType"] is int && (int)e.NewValues["PageType"] == 0) || /* int and zero */
                (e.NewValues["PageType"] is string && (((string)e.NewValues["PageType"]).IsNullOrWhiteSpace() || ((string)e.NewValues["PageType"]).Equals("0")))) /* string and empty or 0 */
                e.Errors["PageType"] = this.PageAsPageDefault.Translate(TRANSLATION_KEY_PREFIX + "Error.PageTypeNotSelected", "Er moet een 'Page Type' worden opgegeven voor de pagina.", true);
            else if (e.OldValues["PageType"] != null && !e.OldValues["PageType"].Equals(e.NewValues["PageType"]))
            {
                // Check if new PageType is compatible
                try
                {
                    Obymobi.Logic.Cms.PageType oldPageType;
                    if (!EnumUtil.TryParse(Convert.ToInt32(e.OldValues["PageType"]), out oldPageType))
                        throw new NotImplementedException("Unsupported Old Pagetype");
                    Obymobi.Logic.Cms.PageType newPageType;
                    if (!EnumUtil.TryParse(Convert.ToInt32(e.NewValues["PageType"]), out newPageType))
                        throw new NotImplementedException("Unsupported New Pagetype");

                    var compatibleTypes = PageHelper.PageTypesCompatibleWithPageAndSite(oldPageType, this.site.SiteTypeAsEnum);
                    if (!compatibleTypes.Contains(newPageType))
                        e.Errors["PageType"] = this.PageAsPageDefault.Translate(TRANSLATION_KEY_PREFIX + "Error.IncompatiblePageType", "Het gekozen Pagina Type van de pagina is niet compatibel met het huidige Pagina Type.", true);
                }
                catch
                {

                }
            }
            if (e.HasErrors)
                e.NodeError = this.PageAsPageDefault.Translate(TRANSLATION_KEY_PREFIX + "PleaseCorrectErrors", "Controleer uw invoer.", true);
        }

        // http://www.devexpress.com/Support/Center/Example/Details/E866
        private void tlSiteStructure_CustomCallback(object sender, TreeListCustomCallbackEventArgs e)
        {            
            // Node dragged callback
            string[] nodes = e.Argument.Split(new char[] { ':' });
            
            int draggedPageId = Convert.ToInt32(nodes[0]);
            int draggedUponPageId = Convert.ToInt32(nodes[1]);
            
            this.pageDataSource.PageManager.DraggedToSort(draggedPageId, draggedUponPageId);
            
            this.tlSiteStructure.DataBind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.tlSiteStructure.DataBind();

            var message = this.PageAsPageDefault.Translate(TRANSLATION_KEY_PREFIX + "Warning.PreSubmit", "Weet u zeker dat u deze paginas wilt verwijderen? Subpaginas worden automatisch verwijderd als de hoofdpagina geselecteerd is.", true);
                
            this.btDeleteSelected.PreSubmitWarning = message;
            this.btDeleteSelectedTop.PreSubmitWarning = message;

            this.btDeleteSelected.Click += btDeleteSelected_Click;
            this.btDeleteSelectedTop.Click += btDeleteSelectedTop_Click;
        }

        public void btDeleteSelectedTop_Click(object sender, EventArgs e)
        {
            this.DeleteSelected();
        }

        public void btDeleteSelected_Click(object sender, EventArgs e)
        {
            this.DeleteSelected();
        }
        
        public List<int> SelectedPageIds
        {
            get
            {
                List<TreeListNode> selectedNodes = new List<TreeListNode>();
                this.RetrieveSelectedNodes(ref selectedNodes, null, false);

                var selectedKeys = selectedNodes.Select(nodes => nodes.Key).ToList();

                return selectedKeys.ToArray().Select(int.Parse).ToList();
            }
        }

        public int SiteId
        {
            get
            {
                int siteId = -1;                
                if (this.DataSource is SiteEntity)
                    siteId = ((SiteEntity)this.DataSource).SiteId;
                else if (this.site != null)
                    siteId = this.site.SiteId;
                return siteId;
            }             
        }        

        private string TreelistId { get { return string.Format("{0}_treelist", this.ID); } }

        #endregion      

        #region Javascript Methods

        private string endDragNodeMethod
        {
            get
            {
                return @"  function(s, e) {                                                                             
                                if(e.htmlEvent.shiftKey || document.getElementById('cbSortingMode').checked){
                                    e.cancel = true;
		                            var key = s.GetNodeKeyByRow(e.targetElement);
		                            " + this.TreelistId + @".PerformCustomCallback(e.nodeKey + ':' + key);
	                            }                                
                            }";
            }
        }

        private string nodeDblClickMethod
        {
            get
            {
                return @" function(s, e) {
	                            " + this.TreelistId + @".StartEdit(e.nodeKey);
		                        e.cancel = true;	                                                                    
                            }";
            }
        }

        private string customButtonClickMethod
        {
            get { return @"function (s ,e) { window.location.href = 'Page.aspx?id=' + e.nodeKey; }"; }
        }

        #endregion
    }
}