﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using Obymobi.Cms.Logic.Sites;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Generic.SubPanels.Cms
{
    public partial class SitePageManagerPanel : UserControl
    {
        #region Fields

        private PageManagerPanel pageManagerPanel;

        #endregion

        #region Methods

        private void LoadUserControls()
        {
            // Site ddl
            var filter = new PredicateExpression(SiteFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

            if (CmsSessionHelper.CurrentRole >= Role.Administrator)
                filter.AddWithOr(SiteFields.CompanyId == DBNull.Value);

            var includeFieldsList = new IncludeFieldsList();
            includeFieldsList.Add(SiteFields.Name);

            var sort = new SortExpression(new SortClause(SiteFields.Name, SortOperator.Ascending));

            var sites = new SiteCollection();
            sites.GetMulti(filter, 0, sort, null, null, includeFieldsList, 0, 0);

            this.ddlCopySiteId.DataSource = sites;
            this.ddlCopySiteId.DataBind();

            var newDdlSiteId = string.Format("{0}_ddlSite", this.ID);
            this.ddlCopySiteId.ID = newDdlSiteId;

            // Page manager panel
            this.pageManagerPanel = (PageManagerPanel)this.LoadControl("~/Generic/SubPanels/Cms/PageManagerPanel.ascx");
            this.pageManagerPanel.ID = string.Format("{0}_pageManagerPanel", this.ID);
            this.pageManagerPanel.Init(EntityType.SiteEntity);
            plhPageManagerControl.Controls.Add(pageManagerPanel);            
        }

        private void RefreshCache(int siteId)
        {
            if (siteId > 0)
            {
                this.SiteId = siteId;
                this.LoadSite(siteId, true);                
            }
        }

        private void RefreshSite()
        {
            if (this.pageManagerPanel.DataSource == null && this.ddlCopySiteId.ValidId > 0 && this.ddlCopySiteId.ValidId == this.SiteId)
            {
                this.SiteId = this.ddlCopySiteId.ValidId;
                this.LoadSite(this.SiteId, false);                
            }            
        }

        public void Refresh(bool forceReload)
        {
            SiteEntity currentSite = this.pageManagerPanel.DataSource as SiteEntity;

            if (currentSite != null && forceReload)
                this.LoadSite(currentSite.SiteId, true);
        }

        private void LoadSite(int siteId, bool refreshSite)
        {
            if (siteId <= 0)
                return;

            this.pageManagerPanel.RefreshDataSource(SiteHelper.GetSite(siteId, refreshSite));            
        }

        #endregion

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {            
            this.LoadUserControls();
            base.OnInit(e);            
        }        

        protected void Page_Load(object sender, EventArgs e)
        {
            this.RefreshSite();

            var newBtnId = string.Format("{0}_btnRefresh", this.ID);
            this.btnRefresh.ID = newBtnId;

            this.ddlCopySiteId.ClientSideEvents.SelectedIndexChanged = "function(s, e) { document.getElementById('" + newBtnId + "').click(); return false; } ";            

            this.btnRefresh.Click += btnRefresh_Click;
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            this.RefreshCache(this.ddlCopySiteId.ValidId);
        }                        

        #endregion

        #region Properties

        public PageCollection SelectedPageCollection
        {
            get
            {
                var pageCollection = new PageCollection();
                List<int> selectedPageIds = this.pageManagerPanel.SelectedPageIds;
                if (selectedPageIds.Count > 0 && this.pageManagerPanel.DataSource != null)
                {
                    pageCollection.AddRange(((SiteEntity)this.pageManagerPanel.DataSource).PageCollection.Where(p => selectedPageIds.Contains(p.PageId)).ToList());
                }
                return pageCollection;
            }
        }

        public int SiteId
        {
            get
            {
                if (this.ViewState[this.ID] == null)
                    this.ViewState[this.ID] = -1;

                return (int)this.ViewState[this.ID];   
            }
            set
            {
                this.ViewState[this.ID] = value;
            }
        }                

        #endregion
    }
}