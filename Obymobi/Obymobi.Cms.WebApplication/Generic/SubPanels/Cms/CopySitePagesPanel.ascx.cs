﻿using System;
using Dionysos.Web.UI.WebControls;
using Obymobi.Cms.Logic.Sites;
using Obymobi.Data.CollectionClasses;
using Obymobi.Web.Media;
using PageEntity = Obymobi.Data.EntityClasses.PageEntity;

namespace Obymobi.ObymobiCms.Generic.SubPanels.Cms
{
    public partial class CopySitePagesPanel : SubPanelCustomDataSource
    {
        #region Fields

        private SitePageManagerPanel leftSitePageManagerPanel;
        private SitePageManagerPanel rightSitePageManagerPanel;        

        #endregion

        #region Methods

        private void LoadUserControls()
        {
            this.leftSitePageManagerPanel = (SitePageManagerPanel)this.LoadControl("~/Generic/SubPanels/Cms/SitePageManagerPanel.ascx");
            this.leftSitePageManagerPanel.ID = "leftSite";                        
            this.plhLeftSite.Controls.Add(this.leftSitePageManagerPanel);

            this.rightSitePageManagerPanel = (SitePageManagerPanel)this.LoadControl("~/Generic/SubPanels/Cms/SitePageManagerPanel.ascx");
            this.rightSitePageManagerPanel.ID = "rightSite";                        
            this.plhRightSite.Controls.Add(this.rightSitePageManagerPanel);
        }

        private void CopyPages(SitePageManagerPanel sourcePageManager, SitePageManagerPanel destPageManager)
        {
            var pagesToCopy = sourcePageManager.SelectedPageCollection;
            if (pagesToCopy.Count <= 0)
            {
                this.PageAsPageDefault.MultiValidatorDefault.AddError(this.PageAsPageDefault.Translate("CopySitePagesPanel.NoPageSelected", "Er is geen pagina geselecteerd om te kopiëren.", true));
                this.PageAsPageDefault.Validate();
            }
            else if (destPageManager.SiteId <= 0)
            {
                this.PageAsPageDefault.MultiValidatorDefault.AddError(this.PageAsPageDefault.Translate("CopySitePagesPanel.NoDestSiteSelected", "Er is geen site geselecteerd om naar toe te kopiëren.", true));
                this.PageAsPageDefault.Validate();
            }
            else
            {
                PageCollection newPageCollection = SiteHelper.CopyPages(pagesToCopy, destPageManager.SiteId, null, true, null, MediaCloudHelper.CopyMediaOnCdn);
                foreach (PageEntity page in newPageCollection)
                {
                    page.Save(true);                    
                }
                
                sourcePageManager.Refresh(false);
                destPageManager.Refresh(true);
            }
        }        

        #endregion

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            base.OnInit(e);            
        }        

        protected void Page_Load(object sender, EventArgs e)
        {
            var message = this.PageAsPageDefault.Translate("Warning.PreSubmit", "Weet u zeker dat u deze paginas wilt kopiëren? Subpaginas worden automatisch mee gekopieerd als de hoofdpagina geselecteerd is.", true);

            this.btnCopyLeftToRight.PreSubmitWarning = message;
            this.btnCopyRightToLeft.PreSubmitWarning = message;

            this.btnCopyLeftToRight.Click += btnCopyLeftToRight_Click;
            this.btnCopyRightToLeft.Click += btnCopyRightToLeft_Click;
        }        

        private void btnCopyRightToLeft_Click(object sender, EventArgs e)
        {
            this.CopyPages(this.rightSitePageManagerPanel, this.leftSitePageManagerPanel);
        }

        private void btnCopyLeftToRight_Click(object sender, EventArgs e)
        {
            this.CopyPages(this.leftSitePageManagerPanel, this.rightSitePageManagerPanel);
        }        

        #endregion        
    }
}