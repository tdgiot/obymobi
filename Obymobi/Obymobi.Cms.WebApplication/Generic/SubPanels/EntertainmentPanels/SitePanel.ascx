﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Generic.SubPanels.EntertainmentPanels.SitePanel" Codebehind="SitePanel.ascx.cs" %>
<table class="dataformV2">
    <tr>
        <td class="label">
			<D:Label runat="server" id="lblSite">Site</D:Label>
		</td>
		<td class="control">
			<X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlSiteId" UseDataBinding="true" EntityName="Site" IsRequired="false" PreventEntityCollectionInitialization="true"></X:ComboBoxLLBLGenEntityCollection>            
		</td>
		<td class="label">
            
		</td>
        <td class="control">
            
		</td>  
	</tr>	    		
</table>
