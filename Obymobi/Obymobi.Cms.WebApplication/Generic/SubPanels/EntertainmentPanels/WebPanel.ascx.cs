﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using Dionysos.Web.UI;
using Obymobi.ObymobiCms.UI;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Generic.SubPanels.EntertainmentPanels
{
    public partial class WebPanel : System.Web.UI.UserControl
    {
        protected override void OnInit(EventArgs e)
        {
            // Set the datasource of the entertainment urls
            Obymobi.Data.CollectionClasses.EntertainmenturlCollection entertainmentUrls = new Obymobi.Data.CollectionClasses.EntertainmenturlCollection();

            PredicateExpression filter = new PredicateExpression();
            filter.Add(EntertainmenturlFields.ParentCompanyId == CmsSessionHelper.CurrentCompanyId);
            filter.Add(EntertainmenturlFields.EntertainmentId == this.DataSourceAsEntertainmentEntity.EntertainmentId);

            entertainmentUrls.GetMulti(filter);

            this.ddlDefaultEntertainmenturlId.DataSource = entertainmentUrls;
            this.ddlDefaultEntertainmenturlId.DataBind();

            base.OnInit(e);
        }

        public EntertainmentEntity DataSourceAsEntertainmentEntity
        {
            get
            {
                return (this.Page as PageLLBLGenEntityCms).DataSource as EntertainmentEntity;
            }
        }

    }
}