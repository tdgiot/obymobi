﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using Dionysos.Web.UI;
using Obymobi.ObymobiCms.UI;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Generic.SubPanels.EntertainmentPanels
{
    public partial class SitePanel : System.Web.UI.UserControl
    {
        protected override void OnInit(EventArgs e)
        {
            // Set the datasource of the site ddl
            var filter = new PredicateExpression();
            filter.Add(SiteFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            filter.AddWithOr(SiteFields.CompanyId == DBNull.Value);

            var sort = new SortExpression();
            sort.Add(new SortClause(SiteFields.Name, SortOperator.Ascending));

            SiteCollection sites = new SiteCollection();
            sites.GetMulti(filter, 0, sort);

            this.ddlSiteId.DataSource = sites;
            this.ddlSiteId.DataBind();

            base.OnInit(e);
        }        
    }
}