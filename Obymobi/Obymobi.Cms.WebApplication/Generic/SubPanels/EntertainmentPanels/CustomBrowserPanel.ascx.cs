﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using Dionysos.Web.UI;
using Obymobi.ObymobiCms.UI;
using Obymobi.Data.HelperClasses;

namespace Obymobi.ObymobiCms.Generic.SubPanels.EntertainmentPanels
{
    public partial class CustomBrowserPanel : System.Web.UI.UserControl
    {
        protected override void OnInit(EventArgs e)
        {
            

            base.OnInit(e);
        }

        public EntertainmentEntity DataSourceAsEntertainmentEntity
        {
            get
            {
                return (this.Page as PageLLBLGenEntityCms).DataSource as EntertainmentEntity;
            }
        }

    }
}