﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Generic.SubPanels.EntertainmentPanels.AndroidPanel" Codebehind="AndroidPanel.ascx.cs" %>
<table class="dataformV2">
    <tr>
		<td class="label">
			<D:LabelEntityFieldInfo runat="server" id="lblPackageName">Package</D:LabelEntityFieldInfo>
		</td>
		<td class="control">
			<D:TextBoxString ID="tbPackageName" runat="server"></D:TextBoxString>
		</td>
		<td class="label">
			<D:LabelEntityFieldInfo runat="server" id="lblClassName">Class</D:LabelEntityFieldInfo>
		</td>
		<td class="control">
			<D:TextBoxString ID="tbClassName" runat="server"></D:TextBoxString>
		</td>
	</tr>
    <tr>
        <td class="label">
			<D:LabelEntityFieldInfo runat="server" id="lblApk">APK</D:LabelEntityFieldInfo>
		</td>
		<td class="control">
			<D:FileUpload runat="server" ID="fuApk" IsRequired="true" /> 
		</td>
		<td class="label">
		    <D:LabelEntityFieldInfo runat="server" id="lblApkFilename">Current APK</D:LabelEntityFieldInfo>
		</td>
		<td class="control">
		    <D:TextBoxString ID="tbApkFilename" runat="server" ReadOnly="true">None</D:TextBoxString>
            <D:PlaceHolder ID="phlRemoveApk" runat="server">
                <br /><D:Button ID="btnRemoveApk" Text="Remove APK" runat="server" PreSubmitWarning="Are you sure you want to remove the APK from this entertainment?" />
            </D:PlaceHolder>
		</td>
    </tr>
    <tr>
        <td class="label">
            <D:LabelEntityFieldInfo runat="server" id="lblAppDataClearInterval">AppData Clear Interval</D:LabelEntityFieldInfo>
        </td>
        <td class="control">
            <X:ComboBoxInt runat="server" ID="ddlAppDataClearInterval" DisplayEmptyItem="false"></X:ComboBoxInt>
        </td>
        <td class="label">
            <D:LabelEntityFieldInfo runat="server" id="lblAppCloseInterval">App Close Interval</D:LabelEntityFieldInfo>
        </td>
        <td class="control">
            <X:ComboBoxInt runat="server" ID="ddlAppCloseInterval" DisplayEmptyItem="false"></X:ComboBoxInt>
        </td>
    </tr>
</table>
