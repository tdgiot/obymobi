﻿using System;
using Dionysos.Interfaces;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.ObymobiCms.UI;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.ObymobiCms.Generic.SubPanels.EntertainmentPanels
{
    public partial class AndroidPanel : System.Web.UI.UserControl, ISaveableControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            btnRemoveApk.Click += btnRemoveApk_Click;
            phlRemoveApk.Visible = DataSourceAsEntertainmentEntity.EntertainmentFileId.HasValue;

            if (DataSourceAsEntertainmentEntity.EntertainmentFileId.HasValue)
            {
                tbApkFilename.Text = DataSourceAsEntertainmentEntity.EntertainmentFileEntity.Filename;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            this.ddlAppDataClearInterval.DataBindEnum<EntertainmentDataClearInterval>();
            this.ddlAppCloseInterval.DataBindEnum<EntertainmentCloseInterval>();
        }

        void btnRemoveApk_Click(object sender, EventArgs e)
        {
            RemoveApk();

            (this.Page as PageLLBLGenEntityCms).Refresh();
        }

        public EntertainmentEntity DataSourceAsEntertainmentEntity
        {
            get
            {
                return (this.Page as PageLLBLGenEntityCms).DataSource as EntertainmentEntity;
            }
        }

        public bool Save()
        {
            if (fuApk.HasFile)
            {
                if (!fuApk.FileName.EndsWith(".apk"))
                {
                    (this.Page as PageLLBLGenEntityCms).MultiValidatorDefault.AddError("You can only upload APK files");
                    return false;
                }

                // Remove APK if there is one
                RemoveApk();

                // Increment version number
                DataSourceAsEntertainmentEntity.EntertainmentFileVersion++;

                // Create new entertainment file
                var entertainmentFileEntity = new EntertainmentFileEntity();
                entertainmentFileEntity.Filename = string.Format("{0}-{1}-{2}", DataSourceAsEntertainmentEntity.EntertainmentId, DataSourceAsEntertainmentEntity.EntertainmentFileVersion, fuApk.FileName);
                entertainmentFileEntity.Blob = fuApk.FileBytes;
                entertainmentFileEntity.Save();

                // Assign file to entertainment
                DataSourceAsEntertainmentEntity.EntertainmentFileId = entertainmentFileEntity.EntertainmentFileId;
                DataSourceAsEntertainmentEntity.Save();

                CloudTaskHelper.UploadEntertainmentFile(DataSourceAsEntertainmentEntity.EntertainmentId, entertainmentFileEntity);
            }

            return true;
        }

        private void RemoveApk()
        {
            if (DataSourceAsEntertainmentEntity.EntertainmentFileId.HasValue)
            {
                CloudTaskHelper.DeleteEntertainmentFile(DataSourceAsEntertainmentEntity.EntertainmentId, DataSourceAsEntertainmentEntity.EntertainmentFileEntity);
                DataSourceAsEntertainmentEntity.EntertainmentFileId = null;
                DataSourceAsEntertainmentEntity.Save();
            }
        }
    }
}