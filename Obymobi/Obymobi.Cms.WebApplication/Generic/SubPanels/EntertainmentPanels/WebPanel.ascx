﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Generic.SubPanels.EntertainmentPanels.WebPanel" Codebehind="WebPanel.ascx.cs" %>
<table class="dataformV2">
    <tr>
        <td class="label">
			<D:Label runat="server" id="lblEntertainmenturlId">Default URL</D:Label>
		</td>
		<td class="control">
			<X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlDefaultEntertainmenturlId" UseDataBinding="true" EntityName="Entertainmenturl" IsRequired="false" PreventEntityCollectionInitialization="true"></X:ComboBoxLLBLGenEntityCollection>
		</td>
		<td class="label">
            
		</td>
        <td class="control">
            
		</td>  
	</tr>	
    <tr>
        <td class="label">
            <D:LabelEntityFieldInfo runat="server" id="lblPreventCaching">Data niet cachen</D:LabelEntityFieldInfo>
        </td>
        <td class="control">
            <D:CheckBox runat="server" id="cbPreventCaching" />
        </td>
        <td class="label">
            <D:LabelEntityFieldInfo runat="server" id="lblPostfixWithTimestamp">Postfix met timestamp</D:LabelEntityFieldInfo>
        </td>
        <td class="control">
            <D:CheckBox runat="server" id="cbPostfixWithTimestamp" />
        </td>
    </tr>			
</table>
