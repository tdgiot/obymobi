﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Generic.SubPanels.EntertainmentPanels.CustomBrowserPanel" Codebehind="CustomBrowserPanel.ascx.cs" %>
<table class="dataformV2">
    <tr>
        <td class="label">
            <D:LabelEntityFieldInfo runat="server" id="lblMenuContainer">Menu container tonen</D:LabelEntityFieldInfo>
        </td>
        <td class="control">
            <D:CheckBox runat="server" id="cbMenuContainer" />
        </td>
        <td class="label">
            <D:LabelEntityFieldInfo runat="server" id="lblNavigationBar">Navigatie bar tonen</D:LabelEntityFieldInfo>
        </td>
        <td class="control">
            <D:CheckBox runat="server" id="cbNavigationBar" />
        </td>        
    </tr>			
    <tr>
        <td class="label">
            <D:LabelEntityFieldInfo runat="server" id="lblBackButton">Terug knop tonen</D:LabelEntityFieldInfo>
        </td>
        <td class="control">
            <D:CheckBox runat="server" id="cbBackButton" />
        </td>
        <td class="label">
            <D:LabelEntityFieldInfo runat="server" id="lblTitleAsHeader">Website titel tonen</D:LabelEntityFieldInfo>
        </td>
        <td class="control">
            <D:CheckBox runat="server" id="cbTitleAsHeader" />
        </td>
    </tr>
    <tr>
         <td class="label">
            <D:LabelEntityFieldInfo runat="server" id="lblNavigateButton">Navigatie knop tonen</D:LabelEntityFieldInfo>
        </td>
        <td class="control">
            <D:CheckBox runat="server" id="cbNavigateButton" />
        </td>
        <td class="label">
            <D:LabelEntityFieldInfo runat="server" id="lblRestrictedAccess">Url requests controleren</D:LabelEntityFieldInfo>
        </td>
        <td class="control">
            <D:CheckBox runat="server" id="cbRestrictedAccess" />
        </td>
    </tr>
    <tr>
        <td class="label">
            <D:LabelEntityFieldInfo runat="server" id="lblHomeButton">Home knop tonen</D:LabelEntityFieldInfo>
        </td>
        <td class="control">
            <D:CheckBox runat="server" id="cbHomeButton" />
        </td>
        <td class="label">

        </td>
        <td class="control">

        </td>
    </tr>
</table>
