﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
          
namespace Obymobi.ObymobiCms.Generic.SubPanels
{
    public partial class VenueCategoryCollection : Dionysos.Web.UI.DevExControls.SubPanelLLBLOverviewDataSourceCollection
    {        
        #region Methods

        protected override void OnInit(EventArgs e)
        {            
            base.OnInit(e);
			
            this.EntityName = "VenueCategory";
            this.EntityPageUrl = "~/Generic/VenueCategory.aspx";
            this.ForeignKeyFieldName = "ParentVenueCategoryId";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.btAdd.Click += new EventHandler(btAdd_Click);
        }

        void btAdd_Click(object sender, EventArgs e)
        {
            this.Response.Redirect(string.Format("~/Generic/VenueCategory.aspx?mode=add&ParentVenueCategoryId={0}", this.ParentPrimaryKeyFieldValue));
        }

        #endregion
      
    }
}