﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Utils;
using DevExpress.Web;
using DevExpress.Web.ASPxTreeList;
using DevExpress.Web.ASPxTreeList.Internal;
using DevExpress.Web.Internal;
using Dionysos;
using Dionysos.Data.LLBLGen;
using Dionysos.Web.UI.WebControls;
using Obymobi.Cms.Logic.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Generic.SubPanels
{
    public partial class OptionPanel : SubPanelCustomDataSource
    {
        #region  Fields
        
        private OptionDataSource datasource;

        #endregion

        #region Properties

        private string EndDragNodeMethod
        {
            get
            {
                return @" function(s, e) {                                                                
                            e.cancel = true;
		                    var key = s.GetNodeKeyByRow(e.targetElement);
		                    " + this.TreelistId + @".PerformCustomCallback(e.nodeKey + ':' + key);
	                        }";
            }
        }

        private string NodeDblClickMethod
        {
            get { return @" function(s, e) {
	                            " + this.TreelistId + @".StartEdit(e.nodeKey);
		                        e.cancel = true;	                                                                    
                            }"; }
        }

        #endregion

        #region Methods

        public void SetParentDataSource(IEntity ds)
        {
            this.DataSource = ds;
        }

        private void RetrieveSelectedNodes(ref List<TreeListNode> selectedNodes, TreeListNode parent)
        {
            TreeListNodeCollection nodes = parent == null ? this.tlOptions.Nodes : parent.ChildNodes;
            foreach (TreeListNode node in nodes)
            {
                if (node.Selected && !node.ParentNode.Selected)
                {
                    selectedNodes.Add(node);
                }

                if (node.HasChildren)
                {
                    this.RetrieveSelectedNodes(ref selectedNodes, node);
                }
            }
        }

        private void CreateTreeList()
        {
            this.tlOptions.SettingsEditing.Mode = TreeListEditMode.EditForm;
            this.tlOptions.SettingsPopupEditForm.Width = 600;
            this.tlOptions.SettingsBehavior.ColumnResizeMode = ColumnResizeMode.Disabled;
            this.tlOptions.SettingsBehavior.AllowSort = false;
            this.tlOptions.SettingsBehavior.AllowDragDrop = false;
            this.tlOptions.ClientInstanceName = this.TreelistId;
            this.tlOptions.AutoGenerateColumns = false;
            this.tlOptions.SettingsEditing.AllowNodeDragDrop = true;
            this.tlOptions.SettingsEditing.AllowRecursiveDelete = false;
            this.tlOptions.Settings.GridLines = GridLines.Both;
            this.tlOptions.ClientSideEvents.EndDragNode = this.EndDragNodeMethod;
            this.tlOptions.ClientSideEvents.NodeDblClick = this.NodeDblClickMethod;            
            this.tlOptions.CustomErrorText += this.tlOptions_CustomErrorText;
            this.tlOptions.Width = Unit.Pixel(736);

            this.tlOptions.KeyFieldName = "OptionId";
            this.tlOptions.ParentFieldName = "ParentOptionId";            

            TreeListTextColumn nameColumn = new TreeListTextColumn();
            nameColumn.Caption = "Name";                        
            nameColumn.FieldName = "Name";
            nameColumn.VisibleIndex = 1;
            nameColumn.CellStyle.Wrap = DefaultBoolean.True;            
            this.tlOptions.Columns.Add(nameColumn);

            TreeListComboBoxColumn typeColumn = new TreeListComboBoxColumn();
            typeColumn.Caption = "Type";
            typeColumn.FieldName = "Type";
            typeColumn.VisibleIndex = 2;
            typeColumn.Width = Unit.Pixel(110);
            typeColumn.CellStyle.Wrap = DefaultBoolean.True;
            typeColumn.EditFormSettings.VisibleIndex = 2;
            this.tlOptions.Columns.Add(typeColumn);

            TreeListComboBoxColumn subTypeAsStringColumn = new TreeListComboBoxColumn();
            subTypeAsStringColumn.Caption = "Sub Type";
            subTypeAsStringColumn.FieldName = "SubTypeAsString";
            subTypeAsStringColumn.VisibleIndex = 3;
            subTypeAsStringColumn.Width = Unit.Pixel(80);
            subTypeAsStringColumn.CellStyle.Wrap = DefaultBoolean.True;
            subTypeAsStringColumn.EditFormSettings.VisibleIndex = 3;
            this.tlOptions.Columns.Add(subTypeAsStringColumn);

            TreeListComboBoxColumn subTypeColumn = new TreeListComboBoxColumn();
            subTypeColumn.Caption = "Sub Type";
            subTypeColumn.FieldName = "SubType";
            subTypeColumn.VisibleIndex = 4;
            subTypeColumn.Width = Unit.Pixel(80);
            subTypeColumn.CellStyle.Wrap = DefaultBoolean.True;
            subTypeColumn.Visible = false;            
            this.tlOptions.Columns.Add(subTypeColumn);

            TreeListCommandColumn editCommandColumn = new TreeListCommandColumn();
            editCommandColumn.VisibleIndex = 4;
            editCommandColumn.Width = Unit.Pixel(60);
            editCommandColumn.ShowNewButtonInHeader = true;
            editCommandColumn.EditButton.Visible = true;
            editCommandColumn.NewButton.Visible = true;
            editCommandColumn.DeleteButton.Visible = true;
            editCommandColumn.ButtonType = ButtonType.Image;
            editCommandColumn.EditButton.Image.Url = this.ResolveUrl("~/images/icons/pencil.png");
            editCommandColumn.DeleteButton.Image.Url = this.ResolveUrl("~/images/icons/delete.png");
            editCommandColumn.CancelButton.Image.Url = this.ResolveUrl("~/images/icons/cross_grey.png");
            editCommandColumn.NewButton.Image.Url = this.ResolveUrl("~/images/icons/add2.png");
            editCommandColumn.UpdateButton.Image.Url = this.ResolveUrl("~/images/icons/disk.png");            
            this.tlOptions.Columns.Add(editCommandColumn);

            TreeListHyperLinkColumn editPageColumn = new TreeListHyperLinkColumn();
            editPageColumn.Name = "EditPage";
            editPageColumn.Caption = " ";
            editPageColumn.VisibleIndex = 5;
            editPageColumn.Width = Unit.Pixel(15);
            editPageColumn.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            editPageColumn.PropertiesHyperLink.ImageUrl = "~/images/icons/page_edit.png";
            editPageColumn.EditCellStyle.CssClass = "hidden";
            editPageColumn.FieldName = "OptionId";            
            editPageColumn.EditFormSettings.VisibleIndex = 5;
            editPageColumn.EditFormCaptionStyle.CssClass = "hidden";
            this.tlOptions.Columns.Add(editPageColumn);
            
            this.tlOptions.NodeValidating += this.tlOptions_NodeValidating;
            this.tlOptions.HtmlDataCellPrepared += this.tlOptions_HtmlDataCellPrepared;
            this.tlOptions.HtmlRowPrepared += this.tlOptions_HtmlRowPrepared;
            this.tlOptions.CustomCallback += this.tlOptions_CustomCallback;
            this.tlOptions.NodeInserting += this.tlOptions_NodeInserting;
            this.tlOptions.NodeUpdating += this.tlOptions_NodeUpdating;            
            this.tlOptions.CommandColumnButtonInitialize += this.tlComponents_CommandColumnButtonInitialize;
        }        

        private void tlComponents_CommandColumnButtonInitialize(object sender, TreeListCommandColumnButtonEventArgs e)
        {
            if (!e.NodeKey.IsNullOrWhiteSpace() && (e.ButtonType == TreeListCommandColumnButtonType.New || e.ButtonType == TreeListCommandColumnButtonType.Delete))
            {
                TreeListNode node = this.tlOptions.FindNodeByKeyValue(e.NodeKey);
                Option option = (Option)node.DataItem;

                if (e.ButtonType == TreeListCommandColumnButtonType.New)
                {
                    if (option.Type.Equals(OptionType.Alteration, StringComparison.InvariantCultureIgnoreCase))
                    {
                        AlterationEntity alteration = (AlterationEntity)option.Entity;

                        if (alteration.Type == AlterationType.Date || alteration.Type == AlterationType.DateTime || alteration.Type == AlterationType.Image ||
                            alteration.Type == AlterationType.Text || alteration.Type == AlterationType.Summary || alteration.Type == AlterationType.Notes ||
                            alteration.Type == AlterationType.Email || alteration.Type == AlterationType.Instruction || alteration.Type == AlterationType.Numeric)
                        {
                            e.Visible = DefaultBoolean.False;
                        }
                    }
                    else if (option.Type.Equals(OptionType.Alterationoption, StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (this.datasource.Version == 1)
                        {
                            e.Visible = DefaultBoolean.False;
                        }
                    }
                }
                else if (e.ButtonType == TreeListCommandColumnButtonType.Delete)
                {
                    if (option.HasChildren)
                    {
                        e.Visible = DefaultBoolean.False;
                    }
                }
            }
        }

        private void tlOptions_HtmlRowPrepared(object sender, TreeListHtmlRowEventArgs e)
        {
            if (!e.NodeKey.IsNullOrWhiteSpace())
            {
                TreeListNode node = this.tlOptions.FindNodeByKeyValue(e.NodeKey);
                if (((Option)node.DataItem).LinkedMultipleEntities)
                {
                    e.Row.ForeColor = System.Drawing.Color.Orange;
                }                
            }
        }

        private void tlOptions_NodeInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            ASPxPageControl pageControl = this.tlOptions.FindEditFormTemplateControl("tabs") as ASPxPageControl;
            if (pageControl.ActiveTabIndex == 0) // New tab
            {
                string name, type;
                int subType;
                this.ExtractNewValues(pageControl.TabPages[pageControl.ActiveTabIndex], out name, out type, out subType);
                
                e.NewValues["Name"] = name;
                e.NewValues["Type"] = type;
                e.NewValues["SubType"] = subType;                
            }
            else // Existing tab
            {
                int foreignKey;
                string type;
                this.ExtractExistingValues(pageControl.TabPages[pageControl.ActiveTabIndex], out foreignKey, out type);

                e.NewValues["ForeignKey"] = foreignKey;
                e.NewValues["Type"] = type;
            }

            if (!this.tlOptions.NewNodeParentKey.IsNullOrWhiteSpace())
            {
                e.NewValues["ParentOptionId"] = this.tlOptions.NewNodeParentKey;
            }
        }

        private void tlOptions_NodeUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            ASPxPageControl pageControl = this.tlOptions.FindEditFormTemplateControl("tabs") as ASPxPageControl;
            if (pageControl.ActiveTabIndex == 0) // New tab
            {
                string name, type;
                int subType;
                this.ExtractNewValues(pageControl.TabPages[pageControl.ActiveTabIndex], out name, out type, out subType);

                e.NewValues["Name"] = name;
                e.NewValues["SubType"] = subType;
            }            
        }

        private void ExtractNewValues(TabPage tabPage, out string name, out string type, out int subType)
        {
            ASPxTextBox tbName = tabPage.FindControl("tbNewName") as ASPxTextBox;
            ASPxComboBox cbType = tabPage.FindControl("cbNewType") as ASPxComboBox;
            ASPxComboBox cbSubType = tabPage.FindControl("cbNewSubType") as ASPxComboBox;

            int intSubType;
            name = tbName.Text;
            type = cbType.Value.ToString();
            subType = cbSubType.Value != null ? int.TryParse(cbSubType.Value.ToString(), out intSubType) ? intSubType : -1 : -1;
        }

        private void ExtractExistingValues(TabPage tabPage, out int foreignKey, out string type)
        {
            ASPxComboBox cbExistingName = tabPage.FindControl("cbExistingName") as ASPxComboBox;
            ASPxComboBox cbType = tabPage.FindControl("cbExistingType") as ASPxComboBox;

            foreignKey = int.Parse(cbExistingName.Value.ToString());
            type = cbType.Value.ToString();
        }

        private void tlOptions_HtmlDataCellPrepared(object sender, TreeListHtmlDataCellEventArgs e)
        {
            if (e.Column.Name == "EditPage" && e.NodeKey != null)
            {
                Option option = this.GetOptionByNodeKey(e.NodeKey);
                if (option != null)
                {
                    string url = string.Format("~/Catalog/{0}.aspx?id={1}", option.Type, option.Entity.PrimaryKeyFields[0].CurrentValue);

                    if (option.Entity is AlterationEntity && this.datasource.Version > 1)
                    {
                        url = string.Format("~/Catalog/AlterationV3.aspx?id={0}", option.Entity.PrimaryKeyFields[0].CurrentValue);
                    }
                    else if (option.LinkedEntity is AlterationitemEntity)
                    {
                        if (this.datasource.Version > 1)
                        {
                            url = string.Format("~/Catalog/AlterationitemV3.aspx?id={0}", option.LinkedEntity.PrimaryKeyFields[0].CurrentValue);
                        }
                        else
                        {
                            url = string.Format("~/Catalog/Alterationitem.aspx?id={0}", option.LinkedEntity.PrimaryKeyFields[0].CurrentValue);
                        }
                    }

                    foreach (Control item in e.Cell.Controls)
                    {
                        if (item is HyperLinkDisplayControl link)
                        {
                            link.NavigateUrl = url;
                            break;
                        }
                    }
                }
            }
        }                

        protected void Type_Init(object sender, EventArgs e)
        {
            ASPxComboBox cbType = (ASPxComboBox)sender;
            this.BindType(cbType, "cbNewSubType");
        }                

        protected void SubType_Init(object sender, EventArgs e)
        {
            ASPxComboBox cbSubType = (ASPxComboBox)sender;
            this.BindSubType(cbSubType);            
        }

        protected void ExistingType_Init(object sender, EventArgs e)
        {
            ASPxComboBox cbType = (ASPxComboBox)sender;
            this.BindType(cbType, "cbExistingName");
        }

        protected void cbNewSubType_Callback(object sender, CallbackEventArgsBase e)
        {
            ASPxComboBox cbType = sender as ASPxComboBox;
            this.BindSubType(cbType, e.Parameter);
        }

        private void BindType(ASPxComboBox cbBox, string selectedIndexChangedCallback)
        {
            IEntity parentEntity = null;

            cbBox.ClientSideEvents.SelectedIndexChanged = "function (s, e) { { OnSelectedIndexChanged(s, e, " + selectedIndexChangedCallback + "); }}";

            ASPxPageControl pageControl = cbBox.NamingContainer as ASPxPageControl;
            TreeListEditFormTemplateContainer container = pageControl.NamingContainer as TreeListEditFormTemplateContainer;
            if (container.TreeList.IsNewNodeEditing)
            {
                // New node
                string parentNodeKey = container.TreeList.NewNodeParentKey;
                if (!parentNodeKey.IsNullOrWhiteSpace())
                {
                    // Existing parent node
                    TreeListNode parentNode = this.tlOptions.FindNodeByKeyValue(parentNodeKey);
                    Option parentOption = (Option)parentNode.DataItem;
                    parentEntity = parentOption.Entity;
                }
            }
            else
            {
                cbBox.Enabled = false;
                pageControl.TabPages[0].Text = "Edit";
                pageControl.TabPages[1].ClientVisible = false;
            }

            if (parentEntity == null)
            {
                if (this.datasource.ProductId > 0)
                {
                    if (this.datasource.ProductEntity.ProductgroupId.GetValueOrDefault(0) <= 0 && this.datasource.Version == 2)
                    {
                        this.DataBindEnumValuesToCombBox(cbBox, OptionType.Productgroup, OptionType.Alteration);
                    }
                    else
                    {
                        this.DataBindEnumValuesToCombBox(cbBox, OptionType.Alteration);
                    }
                }
                else if (this.datasource.AlterationId > 0)
                {
                    this.DataBindEnumValuesToCombBox(cbBox, OptionType.Alterationoption);
                }
                else if (this.datasource.AlterationitemId > 0)
                {
                    this.DataBindEnumValuesToCombBox(cbBox, OptionType.Alteration);
                }
                else if (this.datasource.ProductgroupId > 0)
                {
                    this.DataBindEnumValuesToCombBox(cbBox, OptionType.Productgroup, OptionType.Product);
                }
            }
            else if (parentEntity is ProductEntity)
            {
                ProductEntity product = (ProductEntity)parentEntity;
                if (product.ProductgroupId.GetValueOrDefault(0) <= 0 && this.datasource.Version == 2)
                {
                    this.DataBindEnumValuesToCombBox(cbBox, OptionType.Productgroup, OptionType.Alteration);
                }
                else
                {
                    this.DataBindEnumValuesToCombBox(cbBox, OptionType.Alteration);
                }
            }
            else if (parentEntity is ProductgroupEntity)
            {
                ProductgroupEntity productgroup = (ProductgroupEntity)parentEntity;
                this.DataBindEnumValuesToCombBox(cbBox, OptionType.Productgroup, OptionType.Product);
            }
            else if (parentEntity is AlterationEntity)
            {
                if (this.datasource.Version == 2)
                {
                    this.DataBindEnumValuesToCombBox(cbBox, OptionType.Alterationoption);
                }
                else
                {
                    AlterationEntity parentAlterationEntity = (AlterationEntity)parentEntity;
                    if (parentAlterationEntity.Type == AlterationType.Options)
                    {
                        this.DataBindEnumValuesToCombBox(cbBox, OptionType.Alterationoption);
                    }
                    else if (parentAlterationEntity.Type == AlterationType.Package || parentAlterationEntity.Type == AlterationType.Page)
                    {
                        this.DataBindEnumValuesToCombBox(cbBox, OptionType.Alteration);
                    }
                    else if (parentAlterationEntity.Type == AlterationType.Category)
                    {
                        this.DataBindEnumValuesToCombBox(cbBox, OptionType.Alteration, OptionType.Product);
                    }
                }
            }
            else if (parentEntity is AlterationoptionEntity)
            {
                this.DataBindEnumValuesToCombBox(cbBox, OptionType.Alteration);
            }
        }                

        private void BindSubType(ASPxComboBox cbBox, string type = "")
        {
            IEntity parentEntity = null;

            ASPxPageControl pageControl = cbBox.NamingContainer as ASPxPageControl;
            TreeListEditFormTemplateContainer container = pageControl.NamingContainer as TreeListEditFormTemplateContainer;

            if (!container.TreeList.IsNewNodeEditing && type.IsNullOrWhiteSpace())
            {
                // Existing node
                type = System.Web.UI.DataBinder.Eval(container.DataItem, "Type").ToString();
            }

            string parentNodeKey = container.TreeList.NewNodeParentKey;
            if (!parentNodeKey.IsNullOrWhiteSpace())
            {
                TreeListNode parentNode = this.tlOptions.FindNodeByKeyValue(parentNodeKey);
                parentEntity = ((Option)parentNode.DataItem).Entity;
            }
            else
            {
                parentEntity = this.datasource.AlterationEntity;
            }

            if (type.Equals(OptionType.Product))
            {
                cbBox.DataBindEnum<ProductType>(new ProductType[] { ProductType.Product });
            }
            else if (type.Equals(OptionType.Alteration))
            {
                if (this.datasource.Version == 2)
                {
                    List<AlterationType> supportedTypes = AlterationHelper.GetSupportedAlterationTypes(AlterationDialogMode.v3);
                    cbBox.DataBindEnum<AlterationType>(supportedTypes.ToArray());
                }
                else if (parentEntity != null && parentEntity is AlterationEntity)
                {
                    List<AlterationType> supportedTypes = AlterationoptionHelper.GetSupportedAlterationoptionTypesForAlteration(((AlterationEntity)parentEntity).Type);
                    cbBox.DataBindEnum<AlterationType>(supportedTypes.ToArray());
                }
                else
                {
                    List<AlterationType> supportedTypes = AlterationHelper.GetSupportedAlterationTypes(AlterationDialogMode.v2);
                    cbBox.DataBindEnum<AlterationType>(supportedTypes.ToArray());
                }
            }
            else if (type.Equals(OptionType.Alterationoption))
            {
                if (parentEntity == null || this.datasource.Version < 2)
                {
                    List<AlterationType> supportedTypes = AlterationoptionHelper.GetSupportedAlterationoptionTypes(this.datasource.Version == 2 ? AlterationDialogMode.v3 : AlterationDialogMode.v2);
                    cbBox.DataBindEnum<AlterationType>(supportedTypes.ToArray());
                }
                else if (parentEntity is AlterationEntity)
                {
                    List<AlterationType> supportedTypes = AlterationoptionHelper.GetSupportedAlterationoptionTypesForAlteration(((AlterationEntity)parentEntity).Type);
                    cbBox.DataBindEnum<AlterationType>(supportedTypes.ToArray());
                }                
            }
        }

        protected void cbExistingName_Callback(object sender, CallbackEventArgsBase e)
        {
            ASPxComboBox cbExistingName = sender as ASPxComboBox;
            ASPxPageControl pageControl = cbExistingName.NamingContainer as ASPxPageControl;
            TreeListEditFormTemplateContainer container = pageControl.NamingContainer as TreeListEditFormTemplateContainer;
            
            List<TreeListNode> recursiveParentNodes = new List<TreeListNode>();
            string parentNodeKey = container.TreeList.NewNodeParentKey;
            if (!parentNodeKey.IsNullOrWhiteSpace())
            {
                TreeListNode parentNode = this.tlOptions.FindNodeByKeyValue(parentNodeKey);
                recursiveParentNodes = this.GetParentNodesRecursive(parentNode);
            }

            List<Option> parentOptions = recursiveParentNodes.Select(x => (Option)x.DataItem).ToList();

            string type = e.Parameter;
            if (type.Equals(OptionType.Productgroup))
            {
                List<int> parentProductgroupIds = parentOptions.Where(x => x.Entity is ProductgroupEntity).Select(x => ((ProductgroupEntity)x.Entity).ProductgroupId).ToList();
                cbExistingName.ValueField = "ProductgroupId";
                cbExistingName.DataSource = this.GetProductgroups(parentProductgroupIds);
            }
            else if (type.Equals(OptionType.Product))
            {
                List<int> parentProductIds = parentOptions.Where(x => x.Entity is ProductEntity).Select(x => ((ProductEntity)x.Entity).ProductId).ToList();
                cbExistingName.ValueField = "ProductId";
                cbExistingName.DataSource = this.GetProducts(parentProductIds);
            }
            else if (type.Equals(OptionType.Alteration))
            {
                List<int> parentAlterationIds = parentOptions.Where(x => x.Entity is AlterationEntity).Select(x => ((AlterationEntity)x.Entity).AlterationId).ToList();
                cbExistingName.ValueField = "AlterationId";
                cbExistingName.DataSource = this.GetAlterations(parentAlterationIds);
            }
            else if (type.Equals(OptionType.Alterationoption))
            {
                List<int> parentAlterationoptionIds = parentOptions.Where(x => x.Entity is AlterationoptionEntity).Select(x => ((AlterationoptionEntity)x.Entity).AlterationoptionId).ToList();
                cbExistingName.ValueField = "AlterationoptionId";
                cbExistingName.DataSource = this.GetAlterationoptions(parentAlterationoptionIds);
            }

            cbExistingName.TextField = "Name";
            cbExistingName.DataBind();
        }

        private void tlOptions_CustomCallback(object sender, TreeListCustomCallbackEventArgs e)
        {
            // Node dragged callback
            string[] nodes = e.Argument.Split(new char[] { ':' });

            string draggedId = nodes[0];
            string draggedUponId = nodes[1];

            TreeListNode node = this.tlOptions.FindNodeByKeyValue(draggedId);
            if (!(node.DataItem is Option draggedOption))
            {
                return;
            }

            TreeListNode parentNode = this.tlOptions.FindNodeByKeyValue(draggedUponId);
            List<TreeListNode> recursiveParentNodes = this.GetParentNodesRecursive(parentNode);

            List<Option> parentOptions = recursiveParentNodes.Select(x => (Option)x.DataItem).ToList();

            bool isRecursive = parentOptions.Any(x => x.SubType == draggedOption.SubType && x.Type == draggedOption.Type && x.EntityId == draggedOption.EntityId);
            if (isRecursive)
            {
                return;
            }

            if (this.datasource != null)
            {
                this.datasource.DraggedToSort(draggedId, draggedUponId);
                this.tlOptions.DataBind();
            }
        }        

        public new void Load(int version, int productId, int alterationId, int alterationitemId, int productgroupId, bool bindAndExpand)
        {
            this.datasource = new OptionDataSource(version, productId, alterationId, alterationitemId, productgroupId);
            this.tlOptions.DataSource = this.datasource;

            if (bindAndExpand)
            {
                this.tlOptions.DataBind();
                this.tlOptions.ExpandAll();
            }
        }

        private void DeleteSelected()
        {
            List<TreeListNode> selectedNodes = new List<TreeListNode>();
            this.RetrieveSelectedNodes(ref selectedNodes, null);

            if (selectedNodes.Count > 0)
            {
                this.DeleteItems(selectedNodes);                
                this.DataBind();
            }
        }

        private void DeleteItems(List<TreeListNode> selectedNodes)
        {
            if (this.datasource == null)
                return;

            foreach (TreeListNode node in selectedNodes)
            {
                node.Selected = false;
                this.datasource.DeleteParameters.Clear();
                this.datasource.DeleteParameters.Add("OptionId", node.Key);
                this.datasource.Delete();
            }

            this.Load(this.datasource.Version, this.datasource.ProductId, this.datasource.AlterationId, this.datasource.AlterationitemId, this.datasource.ProductgroupId, false);
        }

        private void LoadUserControls()
        {
            this.hlExpandAll.NavigateUrl = string.Format("javascript:{0}.ExpandAll()", this.TreelistId);
            this.hlCollapseAll.NavigateUrl = string.Format("javascript:{0}.CollapseAll()", this.TreelistId);
        }

        #endregion

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            base.OnInit(e);
            this.CreateTreeList();
        }

        private void tlOptions_CustomErrorText(object sender, TreeListCustomErrorTextEventArgs e)
        {
            if (e.Exception?.InnerException is ObymobiException obymobiException)
            {
                e.ErrorText = obymobiException.AdditionalMessage;
            }
            else if (e.Exception?.InnerException is Exception exception)
            {
                e.ErrorText = exception.Message;
            }
        }

        private void tlOptions_NodeValidating(object sender, TreeListNodeValidationEventArgs e)
        {
            if (e.HasErrors)
            {
                e.NodeError = "Please correct the errors.";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.tlOptions.DataBind();

            string deleteMessage = "Are you sure you want to delete these items? Child items will be automatically removed if the parent item is selected.";

            this.btDeleteItemsTop.PreSubmitWarning = deleteMessage;
            this.btDeleteItemsBottom.PreSubmitWarning = deleteMessage;

            this.btDeleteItemsTop.Click += this.btDeleteSelected_Click;
            this.btDeleteItemsBottom.Click += this.btDeleteSelected_Click;
        }

        private void btDeleteSelected_Click(object sender, EventArgs e)
        {
            this.DeleteSelected();
        }

        private string TreelistId
        {
            get { return string.Format("{0}_treelist", this.ID); }
        }

        private Option GetOptionByNodeKey(string nodeKey)
        {
            Option option = null;
            TreeListNode currentNode = this.tlOptions.FindNodeByKeyValue(nodeKey);
            if (currentNode != null && currentNode.DataItem is Option)
            {
                option = (Option)currentNode.DataItem;
            }
            return option;
        }

        private void DataBindEnumValuesToCombBox(ASPxComboBox combobox, params object[] values)
        {
            combobox.Items.Clear();
            foreach (object value in values)
            {
                combobox.Items.Add(value.ToString(), value);
            }            
        }

        private List<TreeListNode> GetParentNodesRecursive(TreeListNode node)
        {
            List<TreeListNode> nodes = new List<TreeListNode>();
            if (node != null && !(node is TreeListRootNode))
            {
                nodes.Add(node);
                nodes.AddRange(this.GetParentNodesRecursive(node.ParentNode));
            }            
            return nodes;
        }

        private ProductgroupCollection GetProductgroups(List<int> productgroupIdsToExclude)
        {
            PredicateExpression filter = new PredicateExpression(ProductgroupFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            filter.Add(ProductgroupFields.ProductgroupId != productgroupIdsToExclude);
            SortExpression sort = new SortExpression(new SortClause(ProductgroupFields.Name, SortOperator.Ascending));
            return EntityCollection.GetMulti<ProductgroupCollection>(filter, sort, null, null, new IncludeFieldsList(ProductgroupFields.Name));
        }

        private ProductCollection GetProducts(List<int> productIdsToExclude)
        {
            PredicateExpression filter = new PredicateExpression(ProductFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            filter.Add(ProductFields.ProductId != productIdsToExclude);
            SortExpression sort = new SortExpression(new SortClause(ProductFields.Name, SortOperator.Ascending));
            return EntityCollection.GetMulti<ProductCollection>(filter, sort, null, null, new IncludeFieldsList(ProductFields.Name));
        }

        private AlterationCollection GetAlterations(List<int> alterationIdsToExclude)
        {
            PredicateExpression filter = new PredicateExpression(AlterationFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            filter.Add(AlterationFields.Version == this.datasource.Version);
            filter.Add(AlterationFields.AlterationId != alterationIdsToExclude);
            SortExpression sort = new SortExpression(new SortClause(AlterationFields.Name, SortOperator.Ascending));
            return EntityCollection.GetMulti<AlterationCollection>(filter, sort, null, null, new IncludeFieldsList(AlterationFields.Name));
        }

        private AlterationoptionCollection GetAlterationoptions(List<int> alterationoptionIdsToExclude)
        {
            PredicateExpression filter = new PredicateExpression();

            if (this.ParentDataSourceAsAlterationEntity.BrandId.HasValue)
            {
                filter.Add(AlterationoptionFields.BrandId == this.ParentDataSourceAsAlterationEntity.BrandId);
            }
            else
            {
                filter.Add(AlterationoptionFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            }

            filter.Add(AlterationoptionFields.Version == this.datasource.Version);
            filter.Add(AlterationoptionFields.AlterationoptionId != alterationoptionIdsToExclude);
            SortExpression sort = new SortExpression(new SortClause(AlterationoptionFields.Name, SortOperator.Ascending));
            return EntityCollection.GetMulti<AlterationoptionCollection>(filter, sort, null, null, new IncludeFieldsList(AlterationoptionFields.Name));
        }
        
        #endregion

        private AlterationEntity ParentDataSourceAsAlterationEntity
        {
            get { return (AlterationEntity)this.DataSource; }
        }
    }
}