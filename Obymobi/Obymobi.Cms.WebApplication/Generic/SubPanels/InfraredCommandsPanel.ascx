﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Generic.SubPanels.InfraredCommandsPanel" Codebehind="InfraredCommandsPanel.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v20.1" Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dxwtl" %>
<script type="text/javascript">

    function OnInit(s, e) {        
        ASPxClientUtils.AttachEventToElement(s.GetMainElement(), "keypress",
        function (evt) {
            switch (evt.keyCode) {
                    //ENTER
                case 13:
                    if (s.IsEditing()) {
                        s.UpdateEdit();
                        evt.stopPropagation();
                    }
                    break;

                    //ESC               
                case 27:
                    if (s.IsEditing()) {
                        s.CancelEdit();
                        evt.stopPropagation();
                    }
                    break;
            }
        });
    }    

</script>
<div style="width: 736px;">    
    <dxwtl:ASPxTreeList ID="tlCommands" ClientInstanceName="tlCommands" runat="server" EnableViewState="false" AutoGenerateColumns="False">
        <SettingsSelection Enabled="true" Recursive="false" />        
        <ClientSideEvents Init="OnInit" />
        <Templates>
            <EditForm>
                <table width="100%">
                    <tr>
                        <td style="color: black;">Type</td>
                        <td>
                            <dxe:ASPxTextBox ID="tbType" Enabled="false" style="width: 100%" runat="server" Value='<%# Bind("TypeStringValue") %>' />
                        </td>                                                                                                   
                        <td style="color: black;">Name</td>
                        <td>
                            <dxe:ASPxTextBox ID="tbName" Enabled="true" style="width: 100%" runat="server" Value='<%# Bind("Name") %>' />
                        </td>                                                                                                   
                    </tr>
                    <tr>
                        <td style="color: black;">Hex</td>
                        <td colspan="3">
                            <D:TextBoxMultiLine ID="tbHex" runat="server" Rows="10" UseDataBinding="true" Value='<%# Bind("Hex") %>'></D:TextBoxMultiLine>                            
                        </td>
                    </tr>                                    
                </table>                
                <div style="text-align: right; padding-top: 8px">
                    <dxwtl:ASPxTreeListTemplateReplacement runat="server" ReplacementType="UpdateButton"  />
                    <dxwtl:ASPxTreeListTemplateReplacement runat="server" ReplacementType="CancelButton" />
                </div>
            </EditForm>
        </Templates>
    </dxwtl:ASPxTreeList>    
</div>