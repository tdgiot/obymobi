﻿using System;
using System.Drawing;
using Dionysos;

namespace Obymobi.ObymobiCms.Generic.SubPanels
{
    public partial class PublishingItemCollection : Dionysos.Web.UI.DevExControls.SubPanelLLBLOverviewDataSourceCollection
    {
        #region Methods

        private void HookupEvents()
        {
            this.gvcsPublishingItemCollection.GridView.HtmlDataCellPrepared += GridView_HtmlDataCellPrepared;
        }

        private void GridView_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.DataColumn.FieldName.Equals("StatusText"))
            {
                string statusText = e.GetValue("StatusText") as string;
                if (statusText.IsNullOrWhiteSpace())
                {
                    if (statusText == "Success")
                        e.Cell.ForeColor = Color.Green;
                    else if (statusText == "Failure")
                        e.Cell.ForeColor = Color.Red;
                }
            }
        }

        #endregion

        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.EntityName = "PublishingItem";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.HookupEvents();

            this.MainGridView.ShowDeleteColumn = false;
            this.MainGridView.ShowDeleteHyperlinkColumn = false;
        }

        #endregion
    }
}