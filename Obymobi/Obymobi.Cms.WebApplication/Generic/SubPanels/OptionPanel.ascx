﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Generic.SubPanels.OptionPanel" Codebehind="OptionPanel.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v20.1" Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dxwtl" %>
<%@ Register TagPrefix="dxwtl" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<script type="text/javascript">

    function OnInit(s, e) {        
        ASPxClientUtils.AttachEventToElement(s.GetMainElement(), "keypress",
        function (evt) {
            switch (evt.keyCode) {
                    //ENTER
                case 13:
                    if (s.IsEditing()) {
                        s.UpdateEdit();
                        evt.stopPropagation();
                    }
                    break;

                    //ESC               
                case 27:
                    if (s.IsEditing()) {
                        s.CancelEdit();
                        evt.stopPropagation();
                    }
                    break;
            }
        });
    }

    function OnSelectedIndexChanged(s, e, cb) {
        if (cb != null) {
            var type = s.GetValue().toString();
            cb.PerformCallback(type);
        }
    }    

</script>
<div style="width: 736px;">
    <span class="nobold" style="width: 100%; height: 28px; margin-left: 0;">
        <table >
            <tr>
                <td><D:HyperLink runat="server" ID="hlExpandAll">Expand all</D:HyperLink> | </td>
                <td><D:HyperLink runat="server" ID="hlCollapseAll">Collapse all</D:HyperLink> | </td>
                <td style="padding-right: 5px;"> Legend:</td>                
                <td style="color: orange; padding-right: 5px;">&#9632; Changes will affect other products</td>
                <td style="float: right;width:341px"><D:Button runat="server" ID="btDeleteItemsTop" style="float:right;" Text="Delete" /></td>
            </tr>
        </table>                
    </span>                                    
    <dxwtl:ASPxTreeList ID="tlOptions" ClientInstanceName="tlOptions" runat="server" EnableViewState="false" AutoGenerateColumns="False">
        <SettingsSelection Enabled="true" Recursive="false" />        
        <ClientSideEvents Init="OnInit" />
        <Templates>
            <EditForm>
                <dxwtl:ASPxPageControl ID="tabs" runat="server" ActiveTabIndex="0" Width="100%">
                    <TabPages>
                        <dxwtl:TabPage Text="New">
                            <ContentCollection>
                                 <dxwtl:ContentControl runat="server">
                                    <table class="dataformV2">
                                        <tr>
                                            <td class="label">Type</td>
                                            <td class="control">
                                                <dxe:ASPxComboBox ID="cbNewType" ClientInstanceName="cbNewType" runat="server" IncrementalFilteringMode="Contains" Value='<%# Bind("Type") %>' OnInit="Type_Init" />
                                            </td>
                                            <td class="label">Sub Type</td>
                                            <td class="control"> 
                                                <dxe:ASPxComboBox ID="cbNewSubType" ClientInstanceName="cbNewSubType" runat="server" IncrementalFilteringMode="Contains" OnCallback="cbNewSubType_Callback" Value='<%# Bind("SubTypeAsString") %>' OnInit="SubType_Init" />
                                            </td>
                                        </tr>    
                                        <tr>
                                            <td class="label">Name</td>
                                            <td class="control" colspan="3">
                                                <dxe:ASPxTextBox ID="tbNewName" style="width: 100%" runat="server" Value='<%# Bind("Name") %>' />
                                            </td>                                                                           
                                        </tr>                                    
                                    </table>
                                </dxwtl:ContentControl>
                            </ContentCollection>
                        </dxwtl:TabPage>
                        <dxwtl:TabPage Text="Existing">
                            <ContentCollection>
                                <dxwtl:ContentControl runat="server">                                    
                                    <table class="dataformV2">
                                        <tr>
                                            <td class="label">Type</td>
                                            <td class="control">
                                                <dxe:ASPxComboBox ID="cbExistingType" ClientInstanceName="cbExistingType" runat="server" IncrementalFilteringMode="Contains" OnInit="ExistingType_Init" />
                                            </td>
                                            <td class="label"></td>
                                            <td class="control"></td>
                                        </tr>   
                                        <tr>
                                            <td class="label">Name</td>
                                            <td class="control" colspan="3">
                                                <dxe:ASPxComboBox ID="cbExistingName" ClientInstanceName="cbExistingName" style="width: 100%" runat="server" OnCallback="cbExistingName_Callback" IncrementalFilteringMode="Contains" EnableCallbackMode="True" CallbackPageSize="10" />
                                            </td>
                                        </tr>                                     
                                    </table>
                                </dxwtl:ContentControl>
                            </ContentCollection>
                        </dxwtl:TabPage>
                    </TabPages>
                </dxwtl:ASPxPageControl>
                <div style="text-align: right; padding-top: 8px">
                    <dxwtl:ASPxTreeListTemplateReplacement runat="server" ReplacementType="UpdateButton"  />
                    <dxwtl:ASPxTreeListTemplateReplacement runat="server" ReplacementType="CancelButton" />
                </div>
            </EditForm>
        </Templates>
    </dxwtl:ASPxTreeList>
    <div id="divDeletePages" style="margin-top: 4px;">
        <D:Button runat="server" ID="btDeleteItemsBottom" style="float:right;" Text="Delete" />        
    </div> 
</div>