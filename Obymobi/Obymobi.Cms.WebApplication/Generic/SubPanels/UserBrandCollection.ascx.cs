﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using Dionysos.Web.UI;
using Dionysos.Web.UI.DevExControls;
using Dionysos.Web;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Dionysos;
using Obymobi.Enums;

namespace Obymobi.ObymobiCms.Generic.Subpanels
{
    public partial class UserBrandCollection : Dionysos.Web.UI.DevExControls.SubPanelLLBLOverviewDataSourceCollection
    {
        #region Methods

        private void HookUpEvents()
        {
        }

        #endregion

        #region Events

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            
            this.EntityName = "UserBrand";
            this.EntityPageUrl = "~/Configuration/Users/UserBrand.aspx";
            
            this.MainGridView.ShowColumnSelectorButton = false;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.HookUpEvents();

            this.MainGridView.LoadComplete += MainGridView_LoadComplete;
        }

        void MainGridView_LoadComplete(object sender, EventArgs e)
        {
            if (this.MainGridView.Columns["Brand"] != null)
            {
                this.MainGridView.Columns["Brand"].Visible = false;
            }
        }

        protected override void OnParentDataSourceLoaded()
        {
            bool allowedToEdit = false;

            if (CmsSessionHelper.CurrentRole >= Role.Administrator)
            {
                allowedToEdit = true;
            }
            else
            {
                UserBrandEntity currentUserBrand = this.ParentDataSourceAsBrandEntity.UserBrandCollection.FirstOrDefault(a => a.UserId == CmsSessionHelper.CurrentUser.UserId);
                if (currentUserBrand != null && currentUserBrand.Role == BrandRole.Owner)
                {
                    allowedToEdit = true;
                }
            }

            this.MainGridView.ShowEditHyperlinkColumn = allowedToEdit;
            this.MainGridView.EnableRowDoubleClick = allowedToEdit;
            this.MainGridView.ShowDeleteHyperlinkColumn = allowedToEdit;

            base.OnParentDataSourceLoaded();
        }

        #endregion

        private BrandEntity ParentDataSourceAsBrandEntity
        {
            get
            {
                return this.PageAsPageLLBLGenEntity.DataSource as BrandEntity;
            }
        }
    }
}