﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Generic.SubPanels.PublishingCollection" Codebehind="PublishingCollection.ascx.cs" %>
    <table class="dataformV2">
	    <tr>
		    <td class="label">
                <D:Label runat="server" id="lblLastPublish">Last publish</D:Label>
		    </td>
		    <td class="control">
                <D:Label runat="server" id="lblLastPublishValue" LocalizeText="false"></D:Label>
		    </td>
		    <td class="label">
			    <D:Label runat="server" id="lblLastModified">Last modified</D:Label>
		    </td>
		    <td class="control">
			    <D:Label runat="server" id="lblLastModifiedValue" LocalizeText="false"></D:Label>
		    </td>
	    </tr>    
    </table>
    <br />
    <X:GridViewColumnSelector runat="server" ID="gvcsPublishingCollection">
	    <Columns />
    </X:GridViewColumnSelector>


