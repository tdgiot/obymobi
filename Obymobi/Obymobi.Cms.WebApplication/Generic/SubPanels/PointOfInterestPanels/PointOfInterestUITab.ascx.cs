﻿using System;
using System.Web.UI;
using Obymobi.Data.EntityClasses;
using Dionysos.Interfaces;
using Obymobi.Enums;
using Dionysos;

namespace Obymobi.ObymobiCms.Generic.SubPanels.PointOfInterestPanels
{
    public partial class PointOfInterestUITab : UserControl, ISaveableControl, System.Web.UI.IValidator
    {
        #region Methods

        protected override void OnInit(EventArgs e)
        {            
            this.LoadUserControls();
            base.OnInit(e);
        }

        private void LoadUserControls()
        {
            
        }
   
        public void Initialize()
        {
            if (this.UITab != null)
            {
                if (!this.UITab.Caption.IsNullOrWhiteSpace())
                {
                    this.pnlUITab.GroupingText = this.UITab.Caption;
                    this.tbDefaultCaption.Text = this.UITab.Caption;
                }

                if (this.CraveUITabEntity != null)
                {
                    CustomTextRenderer craveRenderer = new CustomTextRenderer(this.CraveUITabEntity, this.CultureCode);
                    craveRenderer.RenderCustomText(this.tbCaption, CustomTextType.UITabCaption);
                    craveRenderer.RenderCustomText(this.tbTabletUrl, CustomTextType.UITabUrl);
                }
                if (this.MobileUITabEntity != null)                   
                {
                    CustomTextRenderer mobileRenderer = new CustomTextRenderer(this.MobileUITabEntity, this.CultureCode);
                    mobileRenderer.RenderCustomText(this.tbCaption, CustomTextType.UITabCaption);
                    mobileRenderer.RenderCustomText(this.tbMobileUrl, CustomTextType.UITabUrl);
                }
            }

            // Hide the tablet url textfield if we dont have a Crave UItab entity
            this.tbTabletUrl.Visible = (this.CraveUITabEntity != null);
            this.lblTabletUrl.Visible = (this.CraveUITabEntity != null);

            // Hide the mobile url textfield if we dont have a Mobile UItab entity
            this.tbMobileUrl.Visible = (this.MobileUITabEntity != null);
            this.lblMobileUrl.Visible = (this.MobileUITabEntity != null);

            this.tbCaption.Enabled = !this.IsDefaultCultureCode;
            this.tbMobileUrl.Enabled = !this.IsDefaultCultureCode;
            this.tbTabletUrl.Enabled = !this.IsDefaultCultureCode;
        }

        public bool Save()
        {
            if (this.CraveUITabEntity != null)
            {
                CustomTextRenderer craveRenderer = new CustomTextRenderer(this.CraveUITabEntity, this.CultureCode);
                craveRenderer.SaveCustomText(this.tbCaption, CustomTextType.UITabCaption);
                craveRenderer.SaveCustomText(this.tbTabletUrl, CustomTextType.UITabUrl);
            }
            if (this.MobileUITabEntity != null)
            {
                CustomTextRenderer mobileRenderer = new CustomTextRenderer(this.MobileUITabEntity, this.CultureCode);
                mobileRenderer.SaveCustomText(this.tbCaption, CustomTextType.UITabCaption);
                mobileRenderer.SaveCustomText(this.tbMobileUrl, CustomTextType.UITabUrl);
            }
            return true;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        #endregion

        #region Properties

        public string CultureCode { get; set; }
        public bool IsDefaultCultureCode { get; set; }

        private UITabEntity UITab
        {
            get { return this.MobileUITabEntity ?? this.CraveUITabEntity; }
        }

        public UITabEntity MobileUITabEntity { get; set; }
        public UITabEntity CraveUITabEntity { get; set; }

        #endregion

        bool isValid = true;
        string errorMessage = string.Empty;

        public string ErrorMessage
        {
            get
            {
                return this.errorMessage;
            }
            set
            {
                this.errorMessage = value;
            }
        }

        public bool IsValid
        {
            get
            {
                return this.isValid;
            }
            set
            {
                this.isValid = value;
            }
        }

        public void Validate()
        {
            
        }

        public int SortOrder { get; set; }        

    }
}