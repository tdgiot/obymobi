﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Generic.SubPanels.PointOfInterestPanels.PointOfInterestUITab" Codebehind="PointOfInterestUITab.ascx.cs" %>
<D:Panel ID="pnlUITab" runat="server" GroupingText="New tab" LocalizeText="false">
    <table class="dataformV2">
        <tr>
            <td class="label">
			    <D:Label runat="server" id="lblDefaultCaption">Default caption</D:Label>
		    </td>
		    <td class="control">
			    <D:TextBoxString ID="tbDefaultCaption" runat="server" Enabled="false"></D:TextBoxString>          
		    </td>            
            <td class="label">
                <D:Label runat="server" id="lblMobileUrl">Mobile URL</D:Label>
		    </td>
            <td class="control">
                <D:TextBoxString ID="tbMobileUrl" runat="server"></D:TextBoxString>
		    </td>             		    
	    </tr>			
        <tr>            
            <td class="label">

			    <D:Label runat="server" id="lblCaption">Caption</D:Label>
		    </td>
		    <td class="control">
			    <D:TextBoxString ID="tbCaption" runat="server"></D:TextBoxString>          
		    </td>                     
            <td class="label">
                <D:Label runat="server" id="lblTabletUrl">Tablet URL</D:Label>
            </td>
            <td class="control">
                <D:TextBoxString ID="tbTabletUrl" runat="server"></D:TextBoxString>          
            </td>            
        </tr>                
    </table>
</D:Panel>
