﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Generic.SubPanels.PointOfInterestPanels.PointOfInterestTabPanel" Codebehind="PointOfInterestTabPanel.ascx.cs" %>
<D:Panel ID="pnlPoiTab" runat="server" GroupingText="New tab" LocalizeText="false">
    <table class="dataformV2">
        <tr>
            <td class="label">
			    <D:Label runat="server" id="lblCaption">Name</D:Label>
		    </td>
		    <td class="control">
			    <D:TextBoxString ID="tbCaption" runat="server"></D:TextBoxString>          
		    </td>            
		    <td class="label">
                <D:Label runat="server" id="lblMobileUrl">Mobile URL</D:Label>
		    </td>
            <td class="control">
                <D:TextBoxString ID="tbMobileUrl" runat="server"></D:TextBoxString>
		    </td>              
	    </tr>			
        <tr>            
            <td class="label">

            </td>
            <td class="control">
                <D:Button ID="btnDeleteTab" runat="server" Text="Remove" />
            </td>                        
            <td class="label">
                <D:Label runat="server" id="lblTabletUrl">Tablet URL</D:Label>
            </td>
            <td class="control">
                <D:TextBoxString ID="tbTabletUrl" runat="server"></D:TextBoxString>          
            </td>            
        </tr>        
        <tr>            
            <td class="label">
                &nbsp;
            </td>
            <td class="control">
                &nbsp;
            </td>
		    <td class="label">
                <D:Label runat="server" id="lblSiteId1">Site</D:Label>
		    </td>
            <td class="control">
                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlSiteId1" EntityName="Site" TextField="Name" ValueField="SiteId" PreventEntityCollectionInitialization="true" />
		    </td>              
        </tr>        
    </table>
</D:Panel>
