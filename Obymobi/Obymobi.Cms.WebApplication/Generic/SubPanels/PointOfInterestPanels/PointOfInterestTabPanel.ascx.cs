﻿using System;
using System.Web.UI;
using Dionysos;
using Dionysos.Interfaces;
using Dionysos.Web.UI;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;

namespace Obymobi.ObymobiCms.Generic.SubPanels.PointOfInterestPanels
{
    public partial class PointOfInterestTabPanel : UserControl, ISaveableControl, System.Web.UI.IValidator
    {
        #region Methods

        private bool sitesLoaded = false;

        protected override void OnInit(EventArgs e)
        {            
            this.LoadUserControls();
            base.OnInit(e);
        }

        private void LoadUserControls()
        {
            this.LoadSites();
        }
   
        public void Initialize()
        {
            this.LoadSites();

            if (this.MobileUITab != null)
            {
                this.pnlPoiTab.GroupingText = this.MobileUITab.Caption;
                this.tbCaption.Text = this.MobileUITab.Caption;
                this.tbMobileUrl.Text = this.MobileUITab.URL;
                this.ddlSiteId1.Value = this.MobileUITab.SiteId;
            }

            if (this.TabletUITab != null)
            {
                this.pnlPoiTab.GroupingText = this.TabletUITab.Caption;
                this.tbCaption.Text = this.TabletUITab.Caption;
                this.tbTabletUrl.Text = this.TabletUITab.URL;
                if (this.ddlSiteId1.Value == null && this.TabletUITab.SiteId.HasValue)
                    this.ddlSiteId1.Value = this.TabletUITab.SiteId;
            }                                              
        }

        public bool Save()
        {
            bool saved = false;

            if (!StringUtil.IsNullOrWhiteSpace(this.tbCaption.Text))
            {
                if (!StringUtil.IsNullOrWhiteSpace(this.tbMobileUrl.Text) || this.ddlSiteId1.ValidId > 0)
                {
                    if (this.MobileUITab == null)
                    {
                        this.MobileUITab = new UITabEntity();
                        this.MobileUITab.UIModeId = this.MobileUIModeId;
                        this.MobileUITab.SortOrder = this.SortOrder;
                    }

                    this.MobileUITab.Caption = this.tbCaption.Text;

                    if (this.ddlSiteId1.ValidId > 0)
                    {
                        this.MobileUITab.Type = (int)UITabType.Site;
                        this.MobileUITab.URL = string.Empty;
                        this.MobileUITab.SiteId = this.ddlSiteId1.ValidId;
                    }
                    else
                    {
                        this.MobileUITab.Type = (int)UITabType.Web;
                        this.MobileUITab.URL = this.tbMobileUrl.Text;
                        this.MobileUITab.SiteId = null;
                    }                                        
                    
                    this.MobileUITab.Save();
                    saved = true;
                }
                else if (this.MobileUITab != null)
                    this.MobileUITab.Delete();

                if (!StringUtil.IsNullOrWhiteSpace(this.tbTabletUrl.Text) || this.ddlSiteId1.ValidId > 0)
                {
                    if (this.TabletUITab == null)
                    {
                        this.TabletUITab = new UITabEntity();
                        this.TabletUITab.UIModeId = this.TabletUIModeId;
                        this.TabletUITab.SortOrder = this.SortOrder;
                    }

                    this.TabletUITab.Caption = this.tbCaption.Text;

                    if (this.ddlSiteId1.ValidId > 0)
                    {
                        this.TabletUITab.Type = (int)UITabType.Site;
                        this.TabletUITab.URL = string.Empty;                        
                        this.TabletUITab.SiteId = this.ddlSiteId1.ValidId;
                    }
                    else
                    {
                        this.TabletUITab.Type = (int)UITabType.Web;
                        this.TabletUITab.URL = this.tbTabletUrl.Text;                        
                        this.TabletUITab.SiteId = null;
                    }

                    this.TabletUITab.Save();
                    saved = true;
                }
                else if (this.TabletUITab != null)
                    this.TabletUITab.Delete();
            }

            return saved;
        }

        private void LoadSites()
        {
            if (!this.sitesLoaded)
            {
                this.ddlSiteId1.ValueFieldIndex = (int)SiteFieldIndex.SiteId;
                this.ddlSiteId1.TextFieldIndex = (int)SiteFieldIndex.Name;
                this.ddlSiteId1.DataSource = this.Sites;
                this.ddlSiteId1.DataBind();              

                this.sitesLoaded = true;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.btnDeleteTab.Click += btnDeleteTab_Click;
            this.btnDeleteTab.Visible = (this.MobileUITab != null || this.TabletUITab != null);            
        }

        void btnDeleteTab_Click(object sender, EventArgs e)
        {
            if (this.TabletUITab != null)
                this.TabletUITab.Delete();

            if (this.MobileUITab != null)
                this.MobileUITab.Delete();

            Response.Redirect(Request.RawUrl); 
        }

        #endregion

        #region Properties

        public int TabletUIModeId { get; set; }
        public int MobileUIModeId { get; set; }

        public UITabEntity TabletUITab { get; set; }
        public UITabEntity MobileUITab { get; set; }

        #endregion

        bool isValid = true;
        string errorMessage = string.Empty;

        public string ErrorMessage
        {
            get
            {
                return this.errorMessage;
            }
            set
            {
                this.errorMessage = value;
            }
        }

        public bool IsValid
        {
            get
            {
                return this.isValid;
            }
            set
            {
                this.isValid = value;
            }
        }

        public void Validate()
        {
            if (!this.tbMobileUrl.Text.IsNullOrWhiteSpace() || !this.tbTabletUrl.Text.IsNullOrWhiteSpace() || this.ddlSiteId1.ValidId > 0)
            {
                if ((!this.tbMobileUrl.Text.IsNullOrWhiteSpace() || !this.tbTabletUrl.Text.IsNullOrWhiteSpace()) && this.ddlSiteId1.ValidId > 0)
                {
                    string errorText = ((PageDefault)this.Page).Translate("Tabs.Error.CantSelectUrlsAndSiteId", "Voor één of meerdere tabs is zijn zowel een Site als Url's opgegeven, dit is niet mogelijk.");
                    ((PageDefault)this.Page).MultiValidatorDefault.AddError(errorText);
                }

                if (this.tbCaption.Text.IsNullOrWhiteSpace())
                {
                    string errorText = ((PageDefault)this.Page).Translate("Tabs.Error.CaptionIsRequired", "Er dient een naam te worden opgegeven voor elke Tab met Url(s) of een Site.");
                    ((PageDefault)this.Page).MultiValidatorDefault.AddError(errorText);
                }
            }
            else if(!this.tbCaption.Text.IsNullOrWhiteSpace())
            {
                string errorText = ((PageDefault)this.Page).Translate("Tabs.Error.UrlOrSiteHasToBeChosenToCreateATab", "Er dient een URL of Site te worden gekozen om een Tab aan te kunnen maken.");
                ((PageDefault)this.Page).MultiValidatorDefault.AddError(errorText);
            }
        }

        public int SortOrder { get; set; }

        public SiteCollection Sites { get; set; }

    }
}