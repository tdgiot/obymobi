﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Constants;
using Obymobi.Data.EntityClasses;
using Dionysos.Data;
using Dionysos.Interfaces.Data;
using Dionysos.Drawing;
using System.Drawing;
using System.IO;
using Obymobi.Enums;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logic.HelperClasses;
using System.Text;
using Dionysos;
using Obymobi.Data.Injectables.Authorizers;
using Obymobi.Data.Injectables.Validators;
using Obymobi.Web.CloudStorage;

namespace Obymobi.ObymobiCms.Generic.SubPanels
{
    public partial class MediaRatioPanel : Dionysos.Web.UI.WebControls.SubPanelLLBLGenEntity
    {        
        #region Fields

        public int mediaId;
        public MediaType mediaType;

        public List<int> mediaRatioTypeMediaIds = new List<int>();
        public Size orignalImageSize = new Size(0,0);

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the DataSource as a MediaRatioEntity which is used as the main one in the case of Groups of Media
        /// </summary>
        private MediaRatioTypeMediaEntity DataSourceAsMediaRatioTypeMediaEntity
        {
            get
            {
                return this.DataSource as MediaRatioTypeMediaEntity;
            }
            set
            {
                this.DataSource = value;
            }
        }
    
        #endregion

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            this.EntityName = "MediaRatioTypeMedia";
        }

        void PrepareCropTool()
        {
            // Determine Size
            int cropToolWidth = 0;
            int cropToolHeight = 0;
            this.DetermineCropToolSize(ref cropToolWidth, ref cropToolHeight);
            string htmlId = "croptool" + this.DataSourceAsMediaRatioTypeMediaEntity.MediaRatioTypeMediaId;

            // Image Url
            string imageUrl = this.ResolveUrl("~/Generic/ThumbnailGenerator.ashx?mediaId={0}&width={1}&height={2}".FormatSafe(this.mediaId, cropToolWidth, cropToolHeight));

            // Html Component
            string html = "<div style=\"position:relative;width:{0}px;height:{1}px\" id=\"{2}\" onclick=\"javascript:init{2}();\"><img src=\"{3}\"></div>".FormatSafe(cropToolWidth, cropToolHeight, htmlId, imageUrl);
            this.plhCropTool.AddHtml(html);

            // Initialize the cropObject
            StringBuilder script = new StringBuilder();
            script.AppendFormatLine("<script type=\"text/javascript\">");// window.addEvent('domready', function(){{");
            script.AppendFormatLine("var initComplete{0} = false;", htmlId);
            script.AppendFormatLine("function init{0}() {{ ", htmlId);
            script.AppendFormatLine("			if(initComplete{0}) return;", htmlId);
            //script.AppendFormatLine("			console.log('initAttempt{0}');", htmlId);
            script.AppendFormatLine("			if($('{0}').clientWidth > 0) initComplete{0} = true; else return;", htmlId);// problem is that until as tab is shown it's display:none and therefore has no dimensions.
            script.AppendFormatLine("           {0} = new DG.ImageCrop('{0}', {{", htmlId);	
            script.AppendFormatLine("			lazyShadows : true,");		
            script.AppendFormatLine("			resizeConfig: {");
            script.AppendFormatLine("				preserveAspectRatio : true");
            script.AppendFormatLine("			},");
            script.AppendFormatLine("			moveConfig : {");
            script.AppendFormatLine("				keyNavEnabled : true");	
            script.AppendFormatLine("			},");
            script.AppendFormatLine("			initialCoordinates : {");
            script.AppendFormatLine("				left : {0},", this.DataSourceAsMediaRatioTypeMediaEntity.Left);
            script.AppendFormatLine("				top : {0},", this.DataSourceAsMediaRatioTypeMediaEntity.Top);
            script.AppendFormatLine("				width: {0},", this.DataSourceAsMediaRatioTypeMediaEntity.Width);
            script.AppendFormatLine("				height: {0}", this.DataSourceAsMediaRatioTypeMediaEntity.Height);
            script.AppendFormatLine("			},");
            script.AppendFormatLine("			originalCoordinates : {");
            script.AppendFormatLine("				width: 	{0},", this.orignalImageSize.Width);
            script.AppendFormatLine("				height : {0}", this.orignalImageSize.Height);
            script.AppendFormatLine("			},");
            script.AppendFormatLine("			previewCoordinates : {");
            script.AppendFormatLine("				width: 	{0},", cropToolWidth);
            script.AppendFormatLine("				height : {0}", cropToolHeight);
            script.AppendFormatLine("			},");
            script.AppendFormatLine("			listeners : {");
            script.AppendFormatLine("				render : function() {"); // This could be imporved by storing the looked-up text fields in a variable
            script.AppendFormatLine("					var coordinates = this.getCoordinates();");
//            script.AppendFormatLine("					console.log('render w' + coordinates.width + ' h ' + coordinates.height + ' x ' + coordinates.left + ' y ' + coordinates.top);");
            script.AppendFormatLine("					$('{0}').value = coordinates.width;", this.hfWidth.ClientID);
            script.AppendFormatLine("					$('{0}').value = coordinates.height;", this.hfHeight.ClientID);
            script.AppendFormatLine("					$('{0}').value = coordinates.left;", this.hfLeft.ClientID);
            script.AppendFormatLine("					$('{0}').value = coordinates.top;", this.hfTop.ClientID);
            script.AppendFormatLine("				},");
            script.AppendFormatLine("				crop : function() {");
            script.AppendFormatLine("					var coordinates = this.getCoordinates();");
//            script.AppendFormatLine("					console.log('crop w' + coordinates.width + ' h ' + coordinates.height + ' x ' + coordinates.left + ' y ' + coordinates.top);");
            script.AppendFormatLine("					$('{0}').value = coordinates.width;", this.hfWidth.ClientID);
            script.AppendFormatLine("					$('{0}').value = coordinates.height;", this.hfHeight.ClientID);
            script.AppendFormatLine("					$('{0}').value = coordinates.left;", this.hfLeft.ClientID);
            script.AppendFormatLine("					$('{0}').value = coordinates.top;", this.hfTop.ClientID);
            script.AppendFormatLine("				}");									
            script.AppendFormatLine("			}");			
            script.AppendFormatLine("		});");
            script.AppendFormatLine("		}");
            script.AppendFormatLine("   cropToolInitializers[cropToolInitializers.length] = init{0};", htmlId); // Media.aspx defines this variable.
            //script.AppendFormatLine("}});");
            script.AppendFormatLine("</script>");

            //this.hfLeft.Value = this.DataSourceAsMediaRatioTypeMediaEntity.Left.ToString();
            //this.hfTop.Value = this.DataSourceAsMediaRatioTypeMediaEntity.Top.ToString();
            //this.hfWidth.Value = DataSourceAsMediaRatioTypeMediaEntity.Width.ToString();
            //this.hfHeight.Value =  this.DataSourceAsMediaRatioTypeMediaEntity.Height.ToString();

            this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), htmlId, script.ToString());
        }

        private void DetermineCropToolSize(ref int cropToolWidth, ref int cropToolHeight)
        {
            // Max size of the Cropping Tool: 1100 * 520
            int maxCropToolWidth = 1100;
            int maxCropToolHeight = 520;
            if (this.orignalImageSize.Width <= maxCropToolWidth && this.orignalImageSize.Height <= maxCropToolHeight)
            {
                // Good, nothing to do, already fits.
                cropToolWidth = this.orignalImageSize.Width;
                cropToolHeight = this.orignalImageSize.Height;
            }
            else
            {
                // Check both resize requirements to determinate the size
                decimal resizeFactor = 0;
                if (this.orignalImageSize.Width > maxCropToolWidth)
                {
                    resizeFactor = Decimal.Divide(maxCropToolWidth, this.orignalImageSize.Width);
                }

                if (this.orignalImageSize.Height > maxCropToolHeight)
                {
                    decimal heightResize = Decimal.Divide(maxCropToolHeight, this.orignalImageSize.Height);
                    if (resizeFactor == 0 || heightResize < resizeFactor)
                        resizeFactor = heightResize;
                }

                cropToolWidth = Convert.ToInt32(this.orignalImageSize.Width * resizeFactor);
                cropToolHeight = Convert.ToInt32(this.orignalImageSize.Height * resizeFactor);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // Validate image size & decide on Cropper
            this.plhCropper.Visible = true;
            this.plhNoCropper.Visible = false;
            this.PrepareCropTool();

            // Set labels
            this.lblName.Text = this.DataSourceAsMediaRatioTypeMediaEntity.MediaRatioType.Name;

            if (String.IsNullOrEmpty(this.DataSourceAsMediaRatioTypeMediaEntity.MediaRatioType.EntityType))
            {
                this.lblRelatedEntityName.Text = "Geen";
            }
            else
            {
                this.lblRelatedEntityName.Text = EntityInformationUtil.GetFriendlyNamePlural(this.DataSourceAsMediaRatioTypeMediaEntity.MediaRatioType.EntityType);
            }

            this.lblMinimalWidth.Text = this.DataSourceAsMediaRatioTypeMediaEntity.MediaRatioType.Width.ToString();
            this.lblMinimalHeight.Text = this.DataSourceAsMediaRatioTypeMediaEntity.MediaRatioType.Height.ToString();

            this.lblWidth.Text = this.orignalImageSize.Width.ToString();
            this.lblHeight.Text = this.orignalImageSize.Height.ToString();

            MediaEntity media = this.DataSourceAsMediaRatioTypeMediaEntity.MediaEntity;
            this.mediaId = media.MediaId;

        }

        protected override void OnParentDataSourceLoaded()
        {
            base.OnParentDataSourceLoaded();
        }

        public override bool Save()
        {
            // Re-retrieve the MRTM with Media and file prefetched
            PredicateExpression filter = new PredicateExpression();
            filter.Add(MediaRatioTypeMediaFields.MediaRatioTypeMediaId == this.mediaRatioTypeMediaIds);
            //filter.Add(MediaRatioTypeMediaFields.MediaType == this.mediaType);

            PrefetchPath path = new PrefetchPath(Obymobi.Data.EntityType.MediaRatioTypeMediaEntity);
            path.Add(MediaRatioTypeMediaEntity.PrefetchPathMediaEntity);

            MediaRatioTypeMediaCollection mediaRatioTypeMediaCollection = new MediaRatioTypeMediaCollection();
            mediaRatioTypeMediaCollection.GetMulti(filter, path);

            if (mediaRatioTypeMediaCollection.Count > 0)
            {                
                if (this.DataSourceAsMediaRatioTypeMediaEntity != null)
                {
                    // Store the values
                    float top, left, width, height;

                    this.hfTop.Value = this.hfTop.Value.Replace(".", ",");
                    this.hfLeft.Value = this.hfLeft.Value.Replace(".", ",");
                    this.hfWidth.Value = this.hfWidth.Value.Replace(".", ",");
                    this.hfHeight.Value = this.hfHeight.Value.Replace(".", ",");

                    if (float.TryParse(this.hfTop.Value, out top) &&
                        float.TryParse(this.hfLeft.Value, out left) &&
                        float.TryParse(this.hfWidth.Value, out width) &&
                        float.TryParse(this.hfHeight.Value, out height))
                    {
                        foreach (var mediaRatioTypeMediaEntity in mediaRatioTypeMediaCollection)
                        {                                                        
                            mediaRatioTypeMediaEntity.Top = (int)top;
                            mediaRatioTypeMediaEntity.Left = (int)left;
                            mediaRatioTypeMediaEntity.Width = (int)width;
                            mediaRatioTypeMediaEntity.Height = (int)height;

                            MediaEntity mediaEntity = mediaRatioTypeMediaEntity.MediaEntity;

                            if (mediaRatioTypeMediaEntity.Save() && mediaEntity.LastDistributedVersionTicks.HasValue)
                            {                                
                                MediaRatioTypeMediaEntity entity = mediaRatioTypeMediaEntity;
                                var httpContext = HttpContext.Current;
                                Task.Factory.StartNew(() =>
                                                      {
                                                          GeneralAuthorizer.SetThreadGodMode(true);

                                                          HttpContext.Current = httpContext;
                                                          Thread.SetData(Thread.GetNamedDataSlot(ObymobiConstants.GeneralValidatorDisableUpdateVersions), true);

                                                          entity.MediaEntity.FileDownloadDelegate = () => entity.MediaEntity.Download();
                                                          entity.ResizeAndPublishFile(entity.MediaEntity, true);
                                                      }, TaskCreationOptions.LongRunning);
                            }
                            else if (!mediaRatioTypeMediaEntity.MediaRatioTypeMediaFileEntity.IsNew)
                            {
                                // Media has no file, so reset this one as well.                                             
                                mediaRatioTypeMediaEntity.MediaRatioTypeMediaFileEntity.FileMd5 = string.Empty;
                                mediaRatioTypeMediaEntity.MediaRatioTypeMediaFileEntity.Save();
                            }                        
                        }
                    }
                }
            }
            
            return true;
        }

        #endregion
    }
}