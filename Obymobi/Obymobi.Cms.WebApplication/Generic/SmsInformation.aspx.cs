﻿using System;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.Injectables.Validators;
using Obymobi.Enums;
using Obymobi.Logic.Cms;
using Obymobi.ObymobiCms.UI;
using Obymobi.ObymobiCms.Generic.SubPanels.Sms;

namespace Obymobi.ObymobiCms.Generic
{
    public partial class SmsInformation : PageLLBLGenEntityCms
    {
        public string AlertMessageDefault = "";

        protected override void OnInit(EventArgs e)
        {
            this.AlertMessageDefault = this.Translate("AlertMessageIsDefault", "Weet u zeker dat u deze sms informatie als standaard wilt instellen? Alle sleutelwoorden zullen worden verwijderd!");

            this.LoadUserControls();

            DataSourceLoaded += SmsInformation_DataSourceLoaded;
            
            base.OnInit(e);
        }

        void SmsInformation_DataSourceLoaded(object sender)
        {
            SetGui();

            this.lblCount.Text = string.Format(this.Translate("CharactersRemaining", "Characters remaining: {0}"), (this.tbBodyText.MaxLength - DataSourceAsSmsInformationEntity.BodyText.Length));
        }

        private void LoadUserControls()
        {
            bool addMode = (this.PageMode == Dionysos.Web.PageMode.Add);
            if (!addMode)
            {
                var smsKeywordPanel = (SmsKeywordPanel)this.LoadControl("~/Generic/SubPanels/Sms/SmsKeywordPanel.ascx");
                smsKeywordPanel.Parent = this;
                this.pnlSmsKeywords.Controls.Add(smsKeywordPanel);
            }

            this.pnlSmsKeywords.Visible = !addMode;
        }

        private void SetGui()
        {
            // Disable default checkbox if user is not admin. Keywords have been assigned or if it's already default.
            this.cbIsDefault.Enabled = (CmsSessionHelper.CurrentRole >= Role.Administrator) && !this.DataSourceAsSmsInformationEntity.IsDefault;
            if (DataSourceAsSmsInformationEntity.SmsKeywordCollection.Count > 0)
            {
                this.cbIsDefault.Enabled = false;
            }

            if (DataSourceAsSmsInformationEntity.IsDefault)
                this.pnlSmsKeywords.Visible = false;
        }

        public SmsInformationEntity DataSourceAsSmsInformationEntity
        {
            get { return (this.DataSource as SmsInformationEntity); }
        }

        public override bool Delete()
        {
            try
            {
                return base.Delete();
            }
            catch (ObymobiException ex)
            {
                if (ex.ErrorEnumValue.Equals(SmsInformationValidator.SmsInformationError.CannotDeleteDefault))
                {
                    this.MultiValidatorDefault.AddError(this.Translate("CannotDeleteDefault", ex.AdditionalMessage));
                }
                else
                {
                    this.MultiValidatorDefault.AddError(ex.AdditionalMessage);
                }

                this.Validate();
            }

            return false;
        }
    }
}