﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Dionysos.Web;
using System.Drawing;

namespace Obymobi.ObymobiCms.Generic
{
	public partial class PointOfInterests : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.HookupEvents();
        }

        private void MainGridView_HtmlRowPrepared(object sender, DevExpress.Web.ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType != DevExpress.Web.GridViewRowType.Data) return;
            bool isImported = (bool)e.GetValue("IsImported");
            if (isImported)
                e.Row.BackColor = Color.FromArgb(223, 250, 226);
        }   

        #endregion

        #region Methods

        private void HookupEvents()
        {
            this.MainGridView.HtmlRowPrepared += MainGridView_HtmlRowPrepared;
        }

        #endregion
    }
}
