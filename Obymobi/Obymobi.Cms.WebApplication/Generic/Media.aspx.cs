using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Hosting;
using System.Web.UI;
using System.Web.UI.WebControls;
using CloudStorage;
using DevExpress.Web;
using DevExpress.Web;
using Dionysos;
using Dionysos.Data;
using Dionysos.Data.LLBLGen;
using Dionysos.Drawing;
using Dionysos.Interfaces.Data;
using Dionysos.Web;
using Dionysos.Web.UI;
using Obymobi.Constants;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.Injectables.Authorizers;
using Obymobi.Enums;
using Obymobi.Logic.Cms;
using Obymobi.Logic.HelperClasses;
using Obymobi.ObymobiCms.Generic.SubPanels;
using Obymobi.Web.Caching;
using Obymobi.Web.CloudStorage;
using SD.LLBLGen.Pro.ORMSupportClasses;
using DionysosWeb = Dionysos.Web;

namespace Obymobi.ObymobiCms.Generic
{
    public partial class Media : PageLLBLGenEntity
    {
        #region Fields

        internal const string NoCulturesQueryParameter = "NoCultures";

        /// <summary>
        /// Bool indicates if the page should reload after first save.
        /// </summary>
        private bool ReloadOnFirstSave = true;

        private Dictionary<string, MediaRatioPanel> mediaRatioPanelByMediaTypeGroup = new Dictionary<string, MediaRatioPanel>();

        // Panels
        private Generic.SubPanels.MediaRatioTypesPanel pnlMediaTypes = null;

        #endregion

        #region Methods

        private EntityType GetEntityTypeFromQueryString()
        {
            string entityTypeString = DionysosWeb.QueryStringHelper.GetString("RelatedEntityType");
            if (!entityTypeString.EndsWith("Entity"))
                entityTypeString += "Entity";
            return entityTypeString.ToEnum<EntityType>();
        }

        private void VerifyValidPageRequest()
        {
            if (!DionysosWeb.QueryStringHelper.HasValue("id"))
            {
                // Check if EntityType and EntityId are in the QueryString
                if (!QueryStringHelper.HasValue("RelatedEntityType") || !QueryStringHelper.HasValue("RelatedEntityId"))
                { return; }

                this.RelatedEntityType = GetEntityTypeFromQueryString();
                this.RelatedEntityId = DionysosWeb.QueryStringHelper.GetInt("RelatedEntityId");

                IEntity entityToInsert = DataFactory.EntityFactory.GetEntity(LLBLGenUtil.GetEntityName(this.RelatedEntityType.ToString())) as IEntity;
                string idFieldName = entityToInsert.PrimaryKeyFields[0].Name.ToString();
                this.DataSource.SetNewFieldValue(idFieldName, this.RelatedEntityId);
            }
            else
            {
                if (this.PageMode != DionysosWeb.PageMode.Delete && this.DataSourceAsMediaEntity.RelatedEntity != null)
                {
                    // Retrieve related EntityType and EntityId from the DataSource
                    this.RelatedEntityType = this.DataSourceAsMediaEntity.RelatedEntity.LLBLGenProEntityTypeValue.ToEnum<EntityType>();
                    this.RelatedEntityId = (int)this.DataSourceAsMediaEntity.RelatedEntity.PrimaryKeyFields[0].CurrentValue;
                }
            }
        }

        private void LoadUserControls()
        {
            // Add js file            
            DionysosWeb.UI.MasterPage.LinkedJsFiles.Add("~/js/dgcrop/effects/effect-element.js", 1000);
            DionysosWeb.UI.MasterPage.LinkedJsFiles.Add("~/js/dgcrop/effects/movable.js", 1100);
            DionysosWeb.UI.MasterPage.LinkedJsFiles.Add("~/js/dgcrop/effects/resizable.js", 1200);
            DionysosWeb.UI.MasterPage.LinkedJsFiles.Add("~/js/dgcrop/dg-crop.js", 1300);

            // Initialize the entity (DataSource), because we need to add SubPanels in the OnInit, so we don't get OnParentDataSourceLoaded errors
            this.InitializeEntity();

            List<MediaRatioType> mediaRatioTypes = new List<MediaRatioType>(); ;
            if (QueryStringHelper.GetInt("mediaType", out int mediaType))
            {
                MediaRatioType mediaRatioType = MediaRatioTypes.GetMediaRatioType((MediaType)mediaType);
                if (mediaRatioType != null) 
                { mediaRatioTypes.Add(mediaRatioType); }
            }
            else if (QueryStringHelper.HasValue<string>("RelatedEntityType"))
            {
                var entityType = GetEntityTypeFromQueryString();
                mediaRatioTypes.AddRange(MediaRatioTypes.GetMediaRatioTypes(entityType.ToString()));
            }
            else if (DataSourceAsMediaEntity.RelatedEntity != null)
            {
                mediaRatioTypes.AddRange(DataSourceAsMediaEntity.MediaRatioTypesForRelatedEntity);
                RelatedEntityType = DataSourceAsMediaEntity.RelatedEntity.LLBLGenProEntityTypeValue.ToEnum<EntityType>();
            }

            this.fuDocument.IsRequired = this.PageMode == DionysosWeb.PageMode.Add;
            string defaultCulture = Obymobi.Culture.Mappings.Values.FirstOrDefault(x => x.Code == CmsSessionHelper.DefaultCultureCodeForCompany)?.NameAndCultureCode;
            cbMediaCultureAgnostic.Text = $"Culture Agnostic ({defaultCulture})";

            ConfigureLimitedImageFileTypes(mediaRatioTypes);

            this.hlJpgQuality.Enabled = this.RelatedEntityType != EntityType.AttachmentEntity;

            // Add the media ratio types panel (with all the checkboxes yes)
            this.pnlMediaTypes = (Generic.SubPanels.MediaRatioTypesPanel)this.LoadControl("~/Generic/SubPanels/MediaRatioTypesPanel.ascx");
            this.plhMediaRatioTypes.Controls.Add(this.pnlMediaTypes);

            bool actionTabVisible = false;

            // Prepare javascript for the initialization of the Crop Tools
            string initializersJavascript = "<script type=\"text/javascript\">var cropToolInitializers = new Array(); function initCropTools() { for(i = 0; i < cropToolInitializers.length; i++) { cropToolInitializers[i](); } }</script>";
            this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "cropToolInitializers", initializersJavascript);

            // Create groups based on the MediaTypeGroups + double check their ratio's - if they are not equal, don't group, but do it seperately.                        
            Dictionary<MediaTypeGroup, Dictionary<string, List<MediaRatioType>>> groupedMediaTypes = new Dictionary<MediaTypeGroup, Dictionary<string, List<MediaRatioType>>>();
            foreach (MediaRatioType mediaRatioType in mediaRatioTypes)
            {
                // We need to place it in a Group and also in a Subgroup based on it's ratio,
                // Tricky part: Ratio's are allowed to be off by ca. 2% due to rounding errors
                if (!groupedMediaTypes.ContainsKey(mediaRatioType.MediaTypeGroup))
                {
                    groupedMediaTypes.Add(mediaRatioType.MediaTypeGroup, new Dictionary<string, List<MediaRatioType>>());
                }

                if (MediaRatioTypes.TryGetGetMediaTypeGroupRatioKey(mediaRatioType.MediaType, out string mediaGroupRatioName))
                {
                    if (!groupedMediaTypes[mediaRatioType.MediaTypeGroup].ContainsKey(mediaGroupRatioName))
                    {
                        groupedMediaTypes[mediaRatioType.MediaTypeGroup].Add(mediaGroupRatioName, new List<MediaRatioType>());
                    }

                    groupedMediaTypes[mediaRatioType.MediaTypeGroup][mediaGroupRatioName].Add(mediaRatioType);
                }
                else if (mediaRatioType.MediaTypeGroup != MediaTypeGroup.None)
                {
                    throw new NotImplementedException("MediaGroupRatioKey not implemented for media ratio type: " + mediaRatioType.Name + " - MediaType: " + mediaRatioType.MediaType);
                }
            }

            foreach (KeyValuePair<MediaTypeGroup, Dictionary<string, List<MediaRatioType>>> pair in groupedMediaTypes)
            {
                // We don't show these panels for Product Attachments as we keep them at their original size.
                if (this.RelatedEntityType == EntityType.AttachmentEntity)
                    break;

                bool showActionTab = false;

                if (pair.Key == MediaTypeGroup.None)
                {
                    // None, it's non-grouped MediaRatioType.                   
                    foreach (KeyValuePair<string, List<MediaRatioType>> subPair in pair.Value)
                    {
                        foreach (MediaRatioType mrt in subPair.Value)
                        {
                            if (mrt.MediaType == MediaType.WidgetPdf1280x800)
                                continue;

                            this.RenderMediaRatioPanel(mrt, null, string.Empty, false, out showActionTab);

                            if (showActionTab)
                                actionTabVisible = true;
                        }
                    }
                }
                else
                {
                    // It's grouped, supply the largest MediaRatioType (we downscale the others)                    
                    // Iterate over the collection per Ratio
                    bool showRatio = (pair.Value.Count > 1);
                    foreach (KeyValuePair<string, List<MediaRatioType>> subPair in pair.Value)
                    {
                        this.RenderMediaRatioPanel(null, subPair.Value, subPair.Key, showRatio, out showActionTab);

                        if (showActionTab)
                            actionTabVisible = true;
                    }
                }
            }

            // Set CDN status
            if (!this.DataSourceAsMediaEntity.IsNew)
                this.RenderCdnStatus();
            else
                this.tabsMain.GetTabPageByName("CDN").Visible = false;

            if (DionysosWeb.QueryStringHelper.HasValue(NoCulturesQueryParameter) || IsAppLessMediaRatioTypeMedia)
            {
                this.tabsMain.GetTabPageByName("Cultures").Visible = false;
            }

            this.RenderCompanyCultures();
            this.RenderAgnosticImages();

            this.SetActionTab(actionTabVisible);
        }

        private void ConfigureLimitedImageFileTypes(List<MediaRatioType> mediaRatioTypes)
        {
            IEnumerable<string> limitedSupportedExtensions = mediaRatioTypes.Where(m => m.SupportedImageExtensions != null).SelectMany(m => m.SupportedImageExtensions).Distinct();
            if (limitedSupportedExtensions.Any())
            {
                IEnumerable<string> mimeTypes = limitedSupportedExtensions.Select(e => MimeTypeHelper.GetMimeType(e));
                string mimeTypeString = string.Join(";", mimeTypes);

                // Limited list of file types in dialog
                this.fuDocument.Accept = mimeTypeString;

                // Validation after upload
                this.fuDocument.AllowedContentTypes = mimeTypeString;
                this.fuDocument.UseValidation = true;
            }
        }

        private void RenderCdnStatus()
        {
            string cdnPathOrignal = this.DataSourceAsMediaEntity.GetMediaPath(FileNameType.Cdn);
            string amazonUrlOrignal = WebEnvironmentHelper.GetCdnBaseUrlMedia(CloudStorageProviderIdentifier.Amazon, cdnPathOrignal, true);

            this.plhCdnStatus.AddHtml("<tr><td>Original</td>");
            this.plhCdnStatus.AddHtml(this.GetCdnTableCell(this.DataSourceAsMediaEntity.GetAmazonCdnStatus(), amazonUrlOrignal));

            this.plhCdnStatus.AddHtml("<td class=\"\">&nbsp;</td></tr>");

            foreach (MediaRatioTypeMediaEntity mediaratioTypeMedia in this.DataSourceAsMediaEntity.MediaRatioTypeMediaCollection.OrderBy(x => (int)x.MediaRatioType.MediaType))
            {
                MediaRatioTypeMediaEntity.CdnStatus amazonCdnStatus = mediaratioTypeMedia.GetAmazonCdnStatus();

                string cdnPath = MediaHelper.GetMediaRatioTypeMediaPath(mediaratioTypeMedia);
                string amazonUrl = WebEnvironmentHelper.GetCdnBaseUrlMedia(CloudStorageProviderIdentifier.Amazon, cdnPath, true);

                this.plhCdnStatus.AddHtml("<tr><td>{0}</td>", mediaratioTypeMedia.MediaRatioType.Name);
                this.plhCdnStatus.AddHtml(this.GetCdnTableCell(amazonCdnStatus, amazonUrl));
                this.plhCdnStatus.AddHtml("<td class=\"refresh\">");
                LinkButton btRefreshCdn = new LinkButton();
                btRefreshCdn.ID = "refreshCdn-" + mediaratioTypeMedia.MediaRatioTypeMediaId;
                btRefreshCdn.Click += btRefreshCdn_Click;
                this.plhCdnStatus.Controls.Add(btRefreshCdn);
                this.plhCdnStatus.AddHtml("</td></tr>");
            }
        }

        private void RenderCompanyCultures()
        {
            cbMediaCultureAgnostic.Checked = DataSourceAsMediaEntity.MediaCultureCollection.Count == 0;

            CompanyCultureCollection companyCultures = null;
            BrandCultureCollection brandCultureCollection = null;
            string defaultCultureCode = "derp";

            if (DataSourceAsMediaEntity.GetRelatedBrandId().HasValue)
            {
                brandCultureCollection = new BrandCultureCollection();
                brandCultureCollection.GetMulti(new PredicateExpression(BrandCultureFields.BrandId == DataSourceAsMediaEntity.GetRelatedBrandId()));
            }
            else
            {
                companyCultures = CompanyCultureHelper.GetCompanyCulturesForCompany(CmsSessionHelper.CurrentCompanyId);
                defaultCultureCode = CmsSessionHelper.DefaultCultureCodeForCompany;
            }

            List<ListEditItem> listitems = new List<ListEditItem>();
            List<ListEditItem> companyCultureItems = new List<ListEditItem>();

            foreach (Culture culture in Obymobi.Culture.Mappings.Values.OrderBy(x => x.ToString()))
            {
                // agnostic culture == default culture code
                if (culture.Code.Equals(defaultCultureCode, StringComparison.InvariantCultureIgnoreCase))
                {
                    continue;
                } 

                string cultureName = culture.ToString();

                bool isCompanyCulture = false;

                if (brandCultureCollection != null)
                {
                    isCompanyCulture = brandCultureCollection.Any(x => x.CultureCode.Equals(culture.Code, StringComparison.InvariantCultureIgnoreCase));
                }
                else if (companyCultures != null)
                {
                    isCompanyCulture = companyCultures.Any(x => x.CultureCode.Equals(culture.Code, StringComparison.InvariantCultureIgnoreCase));
                }

                if (isCompanyCulture)
                {
                    companyCultureItems.Add(new ListEditItem($"{cultureName} (*)", culture.Code));
                }
                else
                {
                    listitems.Add(new ListEditItem(cultureName, culture.Code));
                }
            }

            lbMediaCultureCultures.Items.AddRange(companyCultureItems);
            lbMediaCultureCultures.Items.AddRange(listitems);
        }

        private void SelectCultures()
        {
            bool isAgnostic = this.DataSourceAsMediaEntity.MediaCultureCollection.Count == 0;
            if (!isAgnostic)
            {
                List<string> cultureCodes = this.DataSourceAsMediaEntity.MediaCultureCollection.Select(x => x.CultureCode).ToList();
                foreach (ListEditItem item in this.lbMediaCultureCultures.Items)
                {
                    if (cultureCodes.Contains(item.Value))
                    {
                        item.Selected = true;
                    }
                }
            }
        }

        private void RenderAgnosticImages()
        {
            string agnRelatedEntityType;
            int agnRelatedEntityId;
            if (this.DataSourceAsMediaEntity.IsNew)
            {
                agnRelatedEntityType = DionysosWeb.QueryStringHelper.GetString("RelatedEntityType");
                DionysosWeb.QueryStringHelper.GetInt("RelatedEntityId", out agnRelatedEntityId);
            }
            else
            {
                agnRelatedEntityType = this.DataSourceAsMediaEntity.RelatedEntityName.Replace("Entity", string.Empty);
                agnRelatedEntityId = this.DataSourceAsMediaEntity.RelatedEntityId.GetValueOrDefault(0);
            }

            if (agnRelatedEntityType == null || agnRelatedEntityId == 0)
            { return; }

            MediaCollection mediaCollection = new MediaCollection();
            IEntity relatedEntity = DataFactory.EntityFactory.GetEntity(agnRelatedEntityType, agnRelatedEntityId) as IEntity;

            string entityColumn = relatedEntity.PrimaryKeyFields[0].SourceColumnName;
            IEntityField entityField = null;

            MediaEntity entity = new MediaEntity();
            foreach (IEntityField field in entity.Fields)
            {
                if (field.SourceColumnName.Equals(entityColumn))
                {
                    entityField = field;
                }
            }

            PredicateExpression filter = new PredicateExpression();
            filter.Add(new FieldCompareValuePredicate(entityField, ComparisonOperator.Equal, agnRelatedEntityId));
            filter.Add(MediaFields.MediaId != this.DataSourceAsMediaEntity.MediaId);
            filter.Add(MediaFields.AgnosticMediaId == DBNull.Value);

            if (relatedEntity is AttachmentEntity)
            {
                string mediaType;
                if (DionysosWeb.QueryStringHelper.TryGetValue("MediaType", out mediaType))
                {
                    filter.Add(MediaRatioTypeMediaFields.MediaType == int.Parse(mediaType));
                }
                else
                {
                    filter.Add(MediaRatioTypeMediaFields.MediaType != (int)MediaType.AttachmentThumbnail);
                }

                RelationCollection relations = new RelationCollection();
                relations.Add(MediaEntity.Relations.MediaRatioTypeMediaEntityUsingMediaId);

                mediaCollection.GetMulti(filter, relations);
            }
            else
            {
                mediaCollection.GetMulti(filter);
            }


            StringBuilder sb = new StringBuilder();
            this.plhAgnosticImages.AddHtml("<table border='0' cellpadding='0' cellspacing='10' align='center' width='100%'>");
            this.plhAgnosticImages.AddHtml("<tr><td colspan='2'>{0}</td></tr><tr>", this.Translate("PickAgnosticImageToLink", "Pick an agnostic image to link culture specific image to:"));

            int count = 0;
            foreach (MediaEntity mediaEntity in mediaCollection)
            {
                if (count > 0 && count % 2 == 0)
                {
                    this.plhAgnosticImages.AddHtml("</tr><tr>");
                }

                string imageUrl = this.ResolveUrl("~/Generic/ThumbnailGenerator.ashx?mediaId={0}&width={1}&height={2}&fitInDimensions=true".FormatSafe(mediaEntity.MediaId, 250, 250));

                this.plhAgnosticImages.AddHtml("<td align='center'><label for='ctl00_ctl00_cplhContentHolder_cplhPageContent_tabsMain_cbAgnosticImage{1}'><img src='{0}' alt=''></label><br/><br/>", imageUrl, mediaEntity.MediaId);

                RadioButton rb = new RadioButton();
                rb.ID = "cbAgnosticImage" + mediaEntity.MediaId;
                rb.GroupName = "cbAgnosticImages";
                rb.Checked = (mediaEntity.MediaId == this.DataSourceAsMediaEntity.AgnosticMediaId);
                this.plhAgnosticImages.Controls.Add(rb);

                this.plhAgnosticImages.AddHtml("</td>");

                count++;
            }
            this.plhAgnosticImages.AddHtml("<tr><td colspan='2'>{0}</td></tr><tr>", this.Translate("AgnosticOverwriteWarning", "Warning: linking to an agnostic image will overwrite the current dimensions"));
            this.plhAgnosticImages.AddHtml("</tr></table>");
        }

        private void RenderMediaRatioPanel(MediaRatioType mediaRatioType, List<MediaRatioType> mediaRatioTypesInGroup, string mediaGroupRatioName, bool showRatio, out bool showActionTab)
        {
            showActionTab = false;

            // If a MediaRatioTypeMedia entity exists for the current MediaRatioType 
            // and Media combination, add the corresponding tab and check the checkbox
            EntityView<MediaRatioTypeMediaEntity> mediaRatioTypeMediaView = this.DataSourceAsMediaEntity.MediaRatioTypeMediaCollection.DefaultView;
            PredicateExpression mediaFilter = new PredicateExpression();
            mediaFilter.Add(MediaRatioTypeMediaFields.MediaId == this.DataSourceAsMediaEntity.MediaId);

            if (mediaRatioType != null)
                mediaFilter.Add(MediaRatioTypeMediaFields.MediaType == mediaRatioType.MediaType);
            else
                mediaFilter.Add(MediaRatioTypeMediaFields.MediaType == mediaRatioTypesInGroup.Select(x => (int)x.MediaType).ToList());

            mediaRatioTypeMediaView.Filter = mediaFilter;

            if (mediaRatioTypeMediaView.Count > 0)
            {
                // Select the largest available to work with on the panel (in case of a group the others will be scaled down from this)
                MediaRatioTypeMediaEntity mediaRatioTypeEntity = mediaRatioTypeMediaView.OrderByDescending(x => x.MediaRatioType.Width).First();

                string tabText = mediaRatioTypeEntity.MediaRatioType.Name;
                string tabSystemName = mediaRatioTypeEntity.MediaRatioType.MediaType.ToIntString();
                if (mediaRatioTypesInGroup != null)
                {
                    tabText = mediaRatioTypesInGroup.FirstOrDefault(x => x.MediaTypeGroup == mediaRatioTypeEntity.MediaRatioType?.MediaTypeGroup).Name ?? tabText;
                    tabSystemName = mediaRatioTypeEntity.MediaRatioType.MediaTypeGroup.ToIntString();

                    // Fix tab text for grouped images
                    int index = tabText.IndexOf("(", StringComparison.InvariantCulture);
                    if (index > 0)
                    {
                        tabText = tabText.Remove((index - 1));
                    }
                }

                if (!mediaGroupRatioName.IsNullOrWhiteSpace())
                    tabSystemName = mediaGroupRatioName;

                // GK This is custom shit to make sure the page doesn't get too wide... :S 
                // Let's hope the psychopath is not going to check this code (http://goo.gl/wyEQK4).
                // Only lame excuse for this: We're going to rewrite this at some point - STaring point could be display names for MediaTypeGroups and/or grouping them
                if (tabText.StartsWith("Site"))
                    tabText = tabText.Substring(4);
                if (tabText.EndsWith("Image"))
                    tabText = tabText.Substring(0, tabText.Length - 5);

                if (showRatio)
                {
                    tabText += " ({0:N1})".FormatSafe(Decimal.Divide(mediaRatioTypeEntity.MediaRatioType.Width, mediaRatioTypeEntity.MediaRatioType.Height));
                }

                MediaRatioPanel mediaRatioPanel = this.tabsMain.AddTabPage(tabText, tabSystemName, "~/Generic/SubPanels/MediaRatioPanel.ascx", false) as MediaRatioPanel;

                if (mediaRatioTypeEntity.MediaRatioType.MediaTypeGroup != MediaTypeGroup.None && !mediaGroupRatioName.IsNullOrWhiteSpace())
                {
                    this.mediaRatioPanelByMediaTypeGroup.Add(mediaGroupRatioName, mediaRatioPanel);
                }

                // Set DataSource on SubPanel
                mediaRatioPanel.DataSource = mediaRatioTypeEntity;
                mediaRatioPanel.mediaId = this.DataSourceAsMediaEntity.MediaId;
                mediaRatioPanel.mediaRatioTypeMediaIds = mediaRatioTypeMediaView.Select(x => x.MediaRatioTypeMediaId).ToList();
                mediaRatioPanel.mediaType = mediaRatioTypeEntity.MediaRatioType.MediaType;
                mediaRatioPanel.orignalImageSize = MediaHelper.GetImageSize(this.DataSourceAsMediaEntity);

                List<MediaType> mediaTypesWithAction = new List<MediaType>()
                                                       {
                                                           MediaType.Homepage800x480,
                                                           MediaType.Homepage1280x800,
                                                           MediaType.Homepage1280x720,
                                                           MediaType.HomepageFullscreen800x480,
                                                           MediaType.HomepageFullscreen1280x800,
                                                           MediaType.HomepageFullscreen1280x720,
                                                           MediaType.HomepagePhoneSmall,
                                                           MediaType.HomepagePhoneNormal,
                                                           MediaType.HomepagePhoneLarge,
                                                           MediaType.HomepagePhoneXLarge,
                                                           MediaType.HomepageTabletSmall,
                                                           MediaType.HomepageTabletNormal,
                                                           MediaType.HomepageTabletLarge,
                                                           MediaType.HomepageTabletXLarge,
                                                           MediaType.HomepageiPhone,
                                                           MediaType.HomepageiPhoneRetina,
                                                           MediaType.HomepageiPhoneRetinaHD,
                                                           MediaType.HomepageiPhoneRetinaHDPlus,
                                                           MediaType.HomepageiPad,
                                                           MediaType.Homepage2iPad,
                                                           MediaType.HomepageAdvertiPad,
                                                           MediaType.HomepageiPadRetina,
                                                           MediaType.Homepage2iPadRetina,
                                                           MediaType.HomepageAdvertiPadRetina,
                                                           MediaType.ProductAdded800x480,
                                                           MediaType.ProductAdded1280x720,
                                                           MediaType.ProductAddedSmall1280x800,
                                                           MediaType.ProductAdded1280x800,
                                                           MediaType.CategoryPagePhotoHorizontal1280x800,
                                                           MediaType.Homepage3iPad,
                                                           MediaType.Homepage3iPadRetina
                                                       };

                // Show the action panel for the homepage media
                if (mediaTypesWithAction.Contains(mediaRatioTypeEntity.MediaTypeAsEnum) || mediaRatioTypeEntity.MediaTypeAsEnum.ToString().StartsWith("SiteBranding") || mediaRatioTypeEntity.MediaTypeAsEnum.ToString().StartsWith("CategoryBranding"))
                {
                    showActionTab = true;
                }
            }
        }

        private void SetActionTab(bool actionTabVisible)
        {
            this.tabsMain.GetTabPageByName("Action").Visible = actionTabVisible;

            if (actionTabVisible)
            {
                ProductCollection productCollection = new ProductCollection();
                productCollection.GetMulti(new PredicateExpression(ProductFields.CompanyId == CmsSessionHelper.CurrentCompanyId));
                this.ddlActionProductId.DataSource = productCollection;
                this.ddlActionProductId.DataBind();

                CategoryCollection categoryCollection = new CategoryCollection();
                categoryCollection.GetMulti(new PredicateExpression(CategoryFields.CompanyId == CmsSessionHelper.CurrentCompanyId));
                this.ddlActionCategoryId.DataSource = categoryCollection;
                this.ddlActionCategoryId.DataBind();

                EntertainmentCollection entertainmentCollection = new EntertainmentCollection();
                RelationCollection entertainmentRelations = new RelationCollection();
                entertainmentRelations.Add(EntertainmentEntity.Relations.DeliverypointgroupEntertainmentEntityUsingEntertainmentId, JoinHint.Left);
                entertainmentRelations.Add(DeliverypointgroupEntertainmentEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId);
                PredicateExpression entertainmentFilter = new PredicateExpression();
                entertainmentFilter.Add(DeliverypointgroupFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
                entertainmentCollection.GetMulti(entertainmentFilter, entertainmentRelations);
                this.ddlActionEntertainmentId.DataSource = entertainmentCollection;
                this.ddlActionEntertainmentId.DataBind();

                EntertainmentcategoryCollection entertainmentcategoryCollection = new EntertainmentcategoryCollection();
                entertainmentcategoryCollection.GetMulti(new PredicateExpression());
                this.ddlActionEntertainmentcategoryId.DataSource = entertainmentcategoryCollection;
                this.ddlActionEntertainmentcategoryId.DataBind();

                // Sites
                SortExpression sort = new SortExpression(new SortClause(SiteFields.Name, SortOperator.Ascending));
                PredicateExpression sitesFilter = new PredicateExpression();
                sitesFilter.Add(UIModeFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

                RelationCollection sitesRelations = new RelationCollection();
                sitesRelations.Add(SiteEntity.Relations.UITabEntityUsingSiteId);
                sitesRelations.Add(UITabEntity.Relations.UIModeEntityUsingUIModeId);

                SiteCollection sites = new SiteCollection();
                sites.GetMulti(sitesFilter, 0, sort, sitesRelations);
                this.ddlActionSiteId.DataSource = sites;
                this.ddlActionSiteId.DataBind();

                // Pages
                if (this.DataSourceAsMediaEntity.ActionSiteId.HasValue)
                {
                    PageCollection pages = this.LoadPages(this.DataSourceAsMediaEntity.ActionSiteId.Value);
                    this.ddlActionPageId.DataSource = pages;
                    this.ddlActionPageId.DataBind();
                }
            }
        }

        private PageCollection LoadPages(int siteId)
        {
            // Filter
            PredicateExpression filter = new PredicateExpression(PageFields.SiteId == siteId);

            // Sort
            SortExpression sort = new SortExpression(new SortClause(PageFields.Name, SortOperator.Ascending));

            // Get results
            PageCollection pages = new PageCollection();
            pages.GetMulti(filter, 0, sort);

            // Return results
            return pages;
        }

        private void SetGui()
        {
            if (!this.IsPostBack)
            {
                if (this.PageMode != DionysosWeb.PageMode.Add)
                {
                    // Edit Mode
                    string imageUrl = this.ResolveUrl("~/Generic/ThumbnailGenerator.ashx?mediaId={0}&width={1}&height={2}&fitInDimensions=true".FormatSafe(this.DataSourceAsMediaEntity.MediaId, 300, 300));
                    this.imgMedia.ImageUrl = imageUrl;
                    this.plhDeleteDocument.Visible = true;

                    this.lblTitleEdit.Visible = false;
                    this.hlDownloadOriginalImage.Visible = true;
                    this.btGetMediaProcessingQueueStatus.Visible = true;
                }
                else
                {
                    // Add Mode
                    this.lblTitleMediaName.Text = this.DataSourceAsMediaEntity.Name;
                    this.plhDeleteDocument.Visible = false;
                    this.hlDownloadOriginalImage.Visible = false;
                    this.btGetMediaProcessingQueueStatus.Visible = false;
                }

                // Set the link                
                if (this.DataSourceAsMediaEntity.RelatedEntity != null)
                {
                    IEntityInformation entityInfo = EntityInformationUtil.GetEntityInformation(this.DataSourceAsMediaEntity.RelatedEntity);
                    this.hlAttachToEntity.Text = this.DataSourceAsMediaEntity.RelatedEntityNameAndShowFieldValue;
                    if (!String.IsNullOrEmpty(entityInfo.DefaultEntityEditPage))
                    {
                        this.hlAttachToEntity.NavigateUrl = String.Format("{0}?id={1}", this.ResolveUrl(entityInfo.DefaultEntityEditPage), this.RelatedEntityId);
                    }
                    else
                    {
                        this.hlAttachToEntity.RenderAsText = true;
                    }
                }
                else
                {
                    this.lblRelatedEntityName.Visible = false;
                    this.hlAttachToEntity.Visible = false;
                }

                // Set the agnostic media
                if (this.DataSourceAsMediaEntity.IsCultureSpecific)
                {
                    this.hlAgnosticMedia.Text = this.DataSourceAsMediaEntity.MediaEntity.Name;
                    this.hlAgnosticMedia.NavigateUrl = ResolveUrl(string.Format("~/Generic/Media.aspx?id={0}", this.DataSourceAsMediaEntity.AgnosticMediaId));
                    this.plhAgnosticMedia.Visible = true;
                }
            }

            DionysosWeb.QueryStringHelper qs = new DionysosWeb.QueryStringHelper();
            qs.AddItem("JpgQuality", 1);
            this.hlJpgQuality.NavigateUrl = qs.MergeQuerystringWithRawUrl();

            if (!DionysosWeb.QueryStringHelper.HasValue("JpgQuality"))
            {
                this.tbJpgQuality.Visible = false;
                this.tbJpgQuality.UseValidation = false;

                if (!this.DataSourceAsMediaEntity.JpgQuality.HasValue)
                {
                    this.hlJpgQuality.Visible = true;
                    this.hlJpgQuality.Text = string.Format("{0}% (default)", ObymobiConstants.JpgQualityMedia);
                }
                else
                {
                    this.hlJpgQuality.Visible = true;
                    this.hlJpgQuality.Text = string.Format("{0}%", this.DataSourceAsMediaEntity.JpgQuality.Value);
                }
            }
        }

        private string GetCdnTableCell(MediaRatioTypeMediaEntity.CdnStatus status, string url)
        {
            switch (status)
            {
                case MediaRatioTypeMediaEntity.CdnStatus.NotAvailable:
                    return "<td class=\"NotAvailable\"><span></span></td>";
                case MediaRatioTypeMediaEntity.CdnStatus.OutOfdate:
                    return "<td class=\"OutOfDate\"><span></span></td>";
                case MediaRatioTypeMediaEntity.CdnStatus.UpToDate:
                    return "<td class=\"UpToDate\"><a href=\"{0}\" target=\"_blank\">&nbsp;</a></td>".FormatSafe(url);
                case MediaRatioTypeMediaEntity.CdnStatus.NotApplicable:
                    return "<td class=\"NotApplicable\"></td>".FormatSafe(url);
                default:
                    throw new NotImplementedException("CdnStatus: " + status);
            }
        }

        private void DownloadOriginalImage()
        {
            if (this.DataSourceAsMediaEntity != null &&
                (this.DataSourceAsMediaEntity.LastDistributedVersionTicks.HasValue ||
                (this.DataSourceAsMediaEntity.FileBytes != null && this.DataSourceAsMediaEntity.FileBytes.Length > 0)))
            {
                if (this.DataSourceAsMediaEntity.FileDownloadDelegate == null && this.DataSourceAsMediaEntity.FileDownloadDelegateAsync == null)
                {
                    this.DataSourceAsMediaEntity.FileDownloadDelegate = () => this.DataSourceAsMediaEntity.Download();
                }

                Response.Clear();
                Response.Buffer = false;

                string fileName = this.DataSourceAsMediaEntity.Name.Replace(' ', '_').Replace(",", string.Empty);
                if (this.DataSourceAsMediaEntity.IsPdf())
                {
                    if (!fileName.EndsWith(".pdf", StringComparison.InvariantCultureIgnoreCase))
                        fileName += ".pdf";
                    Response.AppendHeader("Content-Type", "application/pdf");
                }
                else
                {
                    if (!fileName.EndsWith(".jpg", StringComparison.InvariantCultureIgnoreCase))
                        fileName += ".jpg";
                    Response.AppendHeader("Content-Type", "image/jpeg");
                }

                Response.AppendHeader("Content-Disposition", string.Format("attachment; filename={0}", fileName));

                byte[] fileBytes = this.DataSourceAsMediaEntity.FileBytes;
                string length = fileBytes.Length.ToString();

                Response.AppendHeader("Content-Length", length);
                Response.BinaryWrite(fileBytes);
                Response.End();
            }
        }

        private void HookupEvents()
        {
            this.hlDeleteDocument.Click += new EventHandler(hlDeleteDocument_Click);
            this.hlDownloadOriginalImage.Click += new EventHandler(hlDownloadOriginalImage_Click);
            this.btRefreshAllCdn.Click += btRefreshAllCdn_Click;
            this.btGetMediaProcessingQueueStatus.Click += btGetMediaProcessingQueueStatus_Click;
        }

        private void SaveMediaRatioType(ListItem item, EntityView<MediaRatioTypeMediaEntity> mediaRatioView, EntityView<MediaRatioTypeMediaEntity> mediaRatioTypeMediaView, bool newFileUpload, MediaRatioTypeMediaCollection mediaRatioTypeMediaToUpload)
        {
            int mediaType = -1;
            if (!int.TryParse(item.Value, out mediaType))
                return;

            PredicateExpression mediaFilter = new PredicateExpression();
            mediaFilter.Add(MediaRatioTypeMediaFields.MediaId == this.DataSourceAsMediaEntity.MediaId);
            mediaFilter.Add(MediaRatioTypeMediaFields.MediaType == mediaType);
            mediaRatioTypeMediaView.Filter = mediaFilter;

            // Check whether there are MediaRatioTypeMedia (cutouts) which have to be deleted
            if (mediaRatioTypeMediaView.Count > 0 && !item.Selected)
            {
                this.DeleteMediaRatioTypeMedia(mediaRatioTypeMediaView);
            }
            else if (item.Selected && !this.DataSourceAsMediaEntity.IsNew)
            {
                // GK I assumed the code below was made to ensure and image is ALWAYS created for a Ratio. The waste
                // of this was that if you already had the image as a ratio it would be saved twice:
                // - Once here
                // - Once on the subpanel
                // If this happened to have caused a bug again, then don't just revert but REALLY solve that bug.                    
                if (mediaRatioTypeMediaView.Count > 0 && !newFileUpload)
                    return; // Will be dealt with on the subpanel

                MediaRatioTypeMediaEntity mediaRatioTypeMediaEntity;
                if (mediaRatioTypeMediaView.Count > 0)
                {
                    mediaRatioTypeMediaEntity = mediaRatioTypeMediaView[0];
                }
                else
                {
                    mediaRatioTypeMediaEntity = new MediaRatioTypeMediaEntity();
                    mediaRatioTypeMediaEntity.MediaId = this.DataSourceAsMediaEntity.MediaId;
                    mediaRatioTypeMediaEntity.MediaType = mediaType;
                }

                // Get the original image (if there is one)
                if ((this.DataSourceAsMediaEntity.SizeHeight.HasValue && this.DataSourceAsMediaEntity.SizeWidth.HasValue) ||
                    this.DataSourceAsMediaEntity.FileBytes.Length > 0)
                {
                    if (this.DataSourceAsMediaEntity.IsPdf())
                    {
                        // No resizing!
                        mediaRatioTypeMediaEntity.Left = 0;
                        mediaRatioTypeMediaEntity.Top = 0;
                        mediaRatioTypeMediaEntity.Width = 0;
                        mediaRatioTypeMediaEntity.Height = 0;
                    }
                    else
                    {
                        MediaHelper.SetDefaultCutout(this.DataSourceAsMediaEntity, mediaRatioTypeMediaEntity);
                    }

                    // Save the MediaRatioTypeMedia (cutout)
                    bool wasNew = mediaRatioTypeMediaEntity.IsNew;
                    if (wasNew)
                    {
                        mediaRatioTypeMediaEntity.ManuallyVerified = true;
                    }

                    mediaRatioTypeMediaEntity.AddToTransaction(this.DataSourceAsMediaEntity);
                    if (mediaRatioTypeMediaEntity.Save())
                    {
                        mediaRatioTypeMediaEntity.Refetch();
                        mediaRatioTypeMediaView.RelatedCollection.Add(mediaRatioTypeMediaEntity);
                        mediaRatioTypeMediaToUpload.Add(mediaRatioTypeMediaEntity);

                        if (wasNew && this.DataSourceAsMediaEntity.MediaCollection.Count > 0)
                        {
                            // Update the child agnostic media
                            this.UpdateChildAgnosticMedia(mediaRatioTypeMediaEntity, UpdateType.Add);
                        }
                    }

                    // If this MediaRatioType is part of a MediaTypeGroup, check if there's a bigger one already existing, if so
                    // add it to that panel, so it will receive that crop. 
                    // Potential bug, you just choose to add a smaller MediaType and unchecked the larger on. For now OK!
                    if (mediaRatioTypeMediaEntity.MediaRatioType.MediaTypeGroup != MediaTypeGroup.None)
                    {
                        string panelKey;
                        if (MediaRatioTypes.TryGetGetMediaTypeGroupRatioKey(mediaRatioTypeMediaEntity.MediaTypeAsEnum, out panelKey))
                        {
                            if (this.mediaRatioPanelByMediaTypeGroup.ContainsKey(panelKey))
                                this.mediaRatioPanelByMediaTypeGroup[panelKey].mediaRatioTypeMediaIds.Add(mediaRatioTypeMediaEntity.MediaRatioTypeMediaId);
                        }
                    }
                }
                else
                {
                    // Just save the entity 
                    mediaRatioTypeMediaEntity.AddToTransaction(this.DataSourceAsMediaEntity);
                    mediaRatioTypeMediaEntity.Save();
                }
            }

            PredicateExpression mediaRatioFilter = new PredicateExpression();
            mediaRatioFilter.Add(MediaRatioTypeMediaFields.MediaId == this.DataSourceAsMediaEntity.MediaId);
            mediaRatioFilter.Add(MediaRatioTypeMediaFields.MediaType == mediaType);
            mediaRatioView.Filter = mediaRatioFilter;

            if (mediaRatioView.Count > 0 && !item.Selected)
            {
                foreach (MediaRatioTypeMediaEntity mediaRatio in mediaRatioView)
                {
                    mediaRatio.AddToTransaction(this.DataSourceAsMediaEntity);
                    if (mediaRatio.Delete())
                    {
                        mediaRatioView.RelatedCollection.Remove(mediaRatio);
                    }
                }
            }
        }

        private void DeleteMediaRatioTypeMedia(EntityView<MediaRatioTypeMediaEntity> mediaRatioTypeMediaView)
        {
            foreach (MediaRatioTypeMediaEntity mediaRatioTypeMediaEntity in mediaRatioTypeMediaView)
            {
                if (this.DataSourceAsMediaEntity.MediaCollection.Count > 0)
                {
                    // Delete the mrtm on the child agnostic media
                    this.UpdateChildAgnosticMedia(mediaRatioTypeMediaEntity, UpdateType.Delete);
                }

                mediaRatioTypeMediaEntity.AddToTransaction(this.DataSourceAsMediaEntity);
                if (mediaRatioTypeMediaEntity.Delete())
                {
                    mediaRatioTypeMediaView.RelatedCollection.Remove(mediaRatioTypeMediaEntity);
                }
            }
        }

        private void UpdateChildAgnosticMedia(MediaRatioTypeMediaEntity mrtm, UpdateType type, Func<byte[]> downloadDelegate = null)
        {
            foreach (MediaEntity media in this.DataSourceAsMediaEntity.MediaCollection)
            {
                EntityView<MediaRatioTypeMediaEntity> childMrtmView = media.MediaRatioTypeMediaCollection.DefaultView;

                MediaRatioTypeMediaEntity childMrtm = media.MediaRatioTypeMediaCollection.SingleOrDefault(m => m.MediaType == mrtm.MediaType);
                if (type == UpdateType.Add)
                {
                    if (childMrtm == null)
                    {
                        MediaRatioTypeMediaEntity newMrtm = new MediaRatioTypeMediaEntity();
                        newMrtm.MediaId = media.MediaId;
                        newMrtm.MediaType = mrtm.MediaType;

                        MediaHelper.SetDefaultCutout(media, newMrtm);

                        newMrtm.AddToTransaction(this.DataSourceAsMediaEntity);
                        if (newMrtm.Save())
                        {
                            MediaHelper.UploadToCdn(newMrtm, this.DataSourceAsMediaEntity);
                        }
                    }
                }
                else if (type == UpdateType.Delete)
                {
                    if (childMrtm != null)
                    {
                        childMrtm.AddToTransaction(this.DataSourceAsMediaEntity);
                        if (childMrtm.Delete())
                        {
                            childMrtmView.RelatedCollection.Remove(childMrtm);
                        }
                    }
                }
            }
        }

        public void Push()
        {
            List<MediaRatioTypeMediaEntity> galleryMediaRatios = this.DataSourceAsMediaEntity.MediaRatioTypeMediaCollection.Where(x => x.MediaType.HasValue && (x.MediaType.Value == (int)MediaType.Gallery || x.MediaType.Value == (int)MediaType.GalleryLarge || x.MediaType.Value == (int)MediaType.GallerySmall)).ToList();
            if (galleryMediaRatios.Count > 0)
            {
                if (!this.DataSourceAsMediaEntity.CompanyEntity.IsNew)
                {
                    if (ScheduledCommandTaskHelper.IsScheduledCommandTaskRunning(this.DataSourceAsMediaEntity.CompanyEntity.CompanyId))
                    {
                        this.AddInformator(InformatorType.Information, "Unable to push images at this moment. Scheduled command task is running for this company");
                        this.Validate();
                        return;
                    }

                    DateTime utcNow = DateTime.UtcNow;

                    this.DataSourceAsMediaEntity.CompanyEntity.CompanyDataLastModifiedUTC = utcNow;
                    this.DataSourceAsMediaEntity.CompanyEntity.CompanyMediaLastModifiedUTC = utcNow;
                    this.DataSourceAsMediaEntity.CompanyEntity.Save();

                    this.WriteTimestampCache();

                    foreach (MediaRatioTypeMediaEntity galleryMediaRatio in galleryMediaRatios)
                    {
                        MediaHelper.PushMediaEntityToCompany(this.DataSourceAsMediaEntity, galleryMediaRatio);
                    }

                    this.AddInformator(InformatorType.Information, "The gallery image is being pushed.");
                    this.Validate();
                }
            }
        }

        private void WriteTimestampCache()
        {
            if (RedisCacheHelper.Instance.RedisEnabled)
            {
                string cacheKey = "CompanyCache-" + this.DataSourceAsMediaEntity.CompanyEntity.CompanyId;
                RedisCacheHelper.Instance.Add(cacheKey, this.DataSourceAsMediaEntity.CompanyEntity);
            }
        }

        #endregion

        #region Event handlers

        /// <summary>
        /// Deletes the current data entity of the page.
        /// </summary>
        /// <returns>
        /// True if deletion was successfull, False if not
        /// </returns>
        /// <exception cref="EmptyException">Variable 'DataSource' is empty.</exception>
        public override bool Delete()
        {
            string mediaPath = this.DataSourceAsMediaEntity.GetMediaPath(FileNameType.DeletionWildcard);
            CloudUploader.Delete(ObymobiConstants.CloudContainerMediaFiles, mediaPath, true);

            return base.Delete();
        }

        private bool ValidateNumberOfActions()
        {
            bool success = true;

            int numActions = 0;
            if (this.ddlActionProductId.ValidId > 0)
                numActions++;

            if (this.ddlActionCategoryId.ValidId > 0)
                numActions++;

            if (this.ddlActionEntertainmentId.ValidId > 0)
                numActions++;

            if (this.ddlActionEntertainmentcategoryId.ValidId > 0)
                numActions++;

            if (this.tbActionUrl.Value.Length > 0)
                numActions++;

            if (this.ddlActionSiteId.ValidId > 0)
                numActions++;

            if (numActions > 1)
            {
                this.MultiValidatorDefault.AddError(this.Translate("MediaOnlyOneAction", "Multiple actions have been selected. Only one action is allowed per media item."));
            }

            if (!this.Validate() || !this.ValidateLanguageSettings())
                success = false;

            return success;
        }

        private bool NeedsConversionToJpg(string extension)
        {
            bool convertImageToJpg = false;

            switch (extension.ToLower())
            {
                case ".jpg":
                    // No conversion needed, file is already a jpg
                    break;
                case ".gif":
                    if (!MediaHelper.IsGifAllowed(this.RelatedEntityType))
                        convertImageToJpg = true;
                    break;
                case ".png":
                    if (!MediaHelper.IsPngAllowed(this.RelatedEntityType))
                        convertImageToJpg = true;
                    break;
                case ".jpeg":
                case ".tif":
                case ".tiff":
                case ".bmp":
                    convertImageToJpg = true;
                    break;
            }

            return convertImageToJpg;
        }

        private bool ConvertImageToJpg(ref string fileName, ref byte[] bytes)
        {
            bool success = true;

            // Change the extension of the filename
            fileName = System.IO.Path.GetFileNameWithoutExtension(fileName) + ".jpg";

            // Convert the bytes to a jpg image bytes array
            try
            {
                bytes = ImageUtil.ConvertToJpg(bytes);
            }
            catch
            {
                // Not a valid image file
                success = false;
            }

            return success;
        }

        private Bitmap ConvertBytesToImage(byte[] bytes)
        {
            // Create an image of the bytes
            MemoryStream memoryStream = new MemoryStream(bytes);
            Bitmap image = null;
            try
            {
                image = (System.Drawing.Bitmap)System.Drawing.Image.FromStream(memoryStream);
            }
            catch
            {
                // Not a valid image file
                image = null;
            }

            return image;
        }

        private void ResizeImageToMaximumSize(ref byte[] bytes, ref Bitmap image)
        {
            // Resize the image if it's too big
            // Had to enlarge this since the largest MediaRatio image is now 3200x1500
            if (image.Size.Width > 4000 || image.Size.Height > 2000)
            {
                bytes = ImageUtil.ResizeImageFile(bytes, 4000, 2000, true, 90);

                // Reload image into memory since size might be changed
                image = (System.Drawing.Bitmap)System.Drawing.Image.FromStream(new MemoryStream(bytes));
            }
        }

        private void SetSizeAndMd5(byte[] bytes, MediaEntity mediaEntity)
        {
            // Save some meta data of the file
            if (bytes.Length > 0)
            {
                // Save the size of the image
                mediaEntity.SizeKb = Convert.ToInt32(bytes.Length) / 1024;

                // Save the Md5 of the file
                mediaEntity.MediaFileMd5 = Dionysos.Security.Cryptography.Hasher.GetHash(bytes, Dionysos.Security.Cryptography.HashType.MD5);
            }
        }

        private void DeleteOldMediaFromCloud(MediaEntity mediaEntity)
        {
            if (mediaEntity.LastDistributedVersionTicks.HasValue)
            {
                string mediaPathOld = mediaEntity.GetMediaPath(FileNameType.Format);
                mediaPathOld = mediaPathOld.Replace(MediaEntity.FILENAME_FORMAT_TICKS, mediaEntity.LastDistributedVersionTicks.Value.ToString(CultureInfo.InvariantCulture));

                CloudUploader.Delete(ObymobiConstants.CloudContainerMediaFiles, mediaPathOld, false);
            }
        }

        public override bool SaveCommand()
        {
            if (RelatedEntityId == 0)
            {
                AddInformator(InformatorType.Warning, "No related entity specified. Please create media from Media tab of related entity.");
                return false;
            }

            bool success = false;
            bool wasNew = this.PageMode == DionysosWeb.PageMode.Add;

            MediaCollection mediaToUpload;
            MediaRatioTypeMediaCollection mediaRatioTypeMediaToUpload;

            this.SaveTransactionToUse = new Transaction(IsolationLevel.ReadUncommitted, "Media-Save");
            try
            {
                this.DataSourceAsMediaEntity.AddToTransaction(this.SaveTransactionToUse);

                success = this.Save(out mediaToUpload, out mediaRatioTypeMediaToUpload);

                this.SaveTransactionToUse.Commit();
            }
            catch (Exception ex)
            {
                this.SaveTransactionToUse.Rollback();
                throw ex;
            }
            finally
            {
                this.SaveTransactionToUse.Dispose();
            }

            if (success)
            {
                DataSourceAsMediaEntity.FileDownloadDelegate = () => DataSourceAsMediaEntity.Download();

                this.UploadMedia(mediaToUpload);
                this.UploadMediaRatioTypeMediaCollection(mediaRatioTypeMediaToUpload);
            }

            bool newMedia = mediaToUpload.Count > 0;
            if ((wasNew || newMedia) && this.ReloadOnFirstSave)
            {
                this.DataSourceAsMediaEntity.Refetch();
                this.RedirectToMediaPage();
            }

            return success;
        }

        public bool Save(out MediaCollection mediaToUpload, out MediaRatioTypeMediaCollection mediaRatioTypeMediaCollectionToUpload)
        {
            mediaToUpload = new MediaCollection();
            mediaRatioTypeMediaCollectionToUpload = new MediaRatioTypeMediaCollection();

            // Set some values that can't be data bound
            if (this.PageMode == DionysosWeb.PageMode.Add)
            {
                // The the Related Entity Id            
                string idFieldName = string.Format("{0}Id", LLBLGenUtil.GetEntityName(this.RelatedEntityType.ToString()));
                this.DataSource.SetNewFieldValue(idFieldName, this.RelatedEntityId);
            }

            if (!this.ValidateNumberOfActions())
                return false;

            // Put homepage image quality default to 100
            ListItemCollection dimensions = this.pnlMediaTypes.RetrieveListItemsByDeviceCategory(DeviceCategory.InRoomTablet);
            ListItem homepage = dimensions.FindByValue(MediaType.Homepage1280x800.ToIntString());
            ListItem homepagefullscreen = dimensions.FindByValue(MediaType.HomepageFullscreen1280x800.ToIntString());
            if ((homepage != null && homepage.Selected) || (homepagefullscreen != null && homepagefullscreen.Selected))
            {
                this.DataSourceAsMediaEntity.JpgQuality = 100;
            }

            // Check whether a file has been selected
            bool isImage = true;

            bool newFileUpload = this.fuDocument.HasFile;
            if (newFileUpload)
            {
                string fileName = this.fuDocument.FileName;
                string extension = System.IO.Path.GetExtension(fileName).ToLower();
                byte[] bytes = this.fuDocument.FileBytes;

                if (!MediaHelper.IsSupportedImage(fileName))
                {
                    isImage = false;
                    string invalidFileText = this.Translate("FileTypeNotSupporter", "The file type '{0}' is not supported.");
                    invalidFileText = invalidFileText.FormatSafe(Path.GetExtension(fileName));

                    this.AddInformator(Dionysos.Web.UI.InformatorType.Warning, invalidFileText);
                    this.Validate();

                    return false;
                }

                if (this.NeedsConversionToJpg(extension) && !this.ConvertImageToJpg(ref fileName, ref bytes))
                {
                    string warningNotAValidImageFile = this.Translate("ErrorNotAValidImageFile", "Het gekozen bestand is geen (verwerkbare) afbeelding.");
                    this.Validate();

                    return false;
                }

                // If a new file was uploaded, don't save the Ratio panels because they still have the ratio's of the old file.
                for (int i = (this.ControlList.Count - 1); i >= 0; i--)
                {
                    if (this.ControlList[i] is MediaRatioPanel)
                    {
                        this.ControlList.Remove(this.ControlList[i]);
                    }
                }

                // Get the datasource Media entity and set the file properties
                MediaEntity mediaEntity = this.DataSourceAsMediaEntity;
                mediaEntity.Name = Path.GetFileName(fileName);
                mediaEntity.FilePathRelativeToMediaPath = Path.GetFileName(fileName);
                mediaEntity.MediaType = -1;
                mediaEntity.SetMimeTypeAndExtension(fileName);
                mediaEntity.FileBytes = bytes;

                if (bytes.Length > 0 && isImage)
                {
                    Bitmap image = this.ConvertBytesToImage(bytes);
                    if (image == null)
                    {
                        // Not a valid image file
                        string warningNotAValidImageFile = this.Translate("ErrorNotAValidImageFile", "Het gekozen bestand is geen (verwerkbare) afbeelding.");
                        this.Validate();
                        return false;
                    }

                    this.ResizeImageToMaximumSize(ref bytes, ref image);

                    // Save image size in database
                    mediaEntity.SizeWidth = image.Size.Width;
                    mediaEntity.SizeHeight = image.Size.Height;
                    mediaEntity.FileBytes = bytes;
                }

                this.SetSizeAndMd5(bytes, mediaEntity);

                // Save the media entity
                if (mediaEntity.Save())
                {
                    this.DeleteOldMediaFromCloud(mediaEntity);

                    mediaEntity.Refetch();

                    mediaEntity.LastDistributedVersionTicks = mediaEntity.UpdatedUTC.GetValueOrDefault(mediaEntity.CreatedUTC.GetValueOrDefault(mediaEntity.CreatedUTC.GetValueOrDefault())).Ticks;
                    mediaToUpload.Add(mediaEntity);

                    // Clean old thumbnails
                    ThumbnailGeneratorUtil.CleanUpTempFilesForMedia(mediaEntity.MediaId);
                }
            }

            this.SaveCultureSettings();

            if (this.DataSourceAsMediaEntity.IsCultureAgnostic || newFileUpload)
            {
                // Get the entity view for the media ratio type media collection
                EntityView<MediaRatioTypeMediaEntity> mediaRatioView = this.DataSourceAsMediaEntity.MediaRatioTypeMediaCollection.DefaultView;
                EntityView<MediaRatioTypeMediaEntity> mediaRatioTypeMediaView = this.DataSourceAsMediaEntity.MediaRatioTypeMediaCollection.DefaultView;

                try
                {
                    // GK In the case of ProductAttachments we still have to select the appropriate MediaRatioTypeCheckboxes
                    // based on if it's either an Image or Pdf
                    if (this.RelatedEntityType == EntityType.AttachmentEntity && newFileUpload)
                    {
                        ListItemCollection miscItems = this.pnlMediaTypes.RetrieveListItemsByDeviceCategory(DeviceCategory.Misc);

                        string mediaType;
                        if (DionysosWeb.QueryStringHelper.TryGetValue("mediaType", out mediaType))
                        {
                            miscItems.FindByValue(mediaType).Selected = true;
                        }
                        else
                        {
                            if (isImage)
                                miscItems.FindByValue(MediaType.AttachmentImage.ToIntString()).Selected = true;
                            else
                                miscItems.FindByValue(MediaType.AttachmentPdf.ToIntString()).Selected = true;
                        }
                    }

                    // Walk through the checkbox items for the different media ratio types for archos
                    foreach (ListItem item in this.pnlMediaTypes.RetrieveListItemsByDeviceCategory(DeviceCategory.Archos))
                    {
                        this.SaveMediaRatioType(item, mediaRatioView, mediaRatioTypeMediaView, newFileUpload, mediaRatioTypeMediaCollectionToUpload);
                    }

                    // Walk through the checkbox items for the different media ratio types for samsung
                    foreach (ListItem item in this.pnlMediaTypes.RetrieveListItemsByDeviceCategory(DeviceCategory.InRoomTablet))
                    {
                        this.SaveMediaRatioType(item, mediaRatioView, mediaRatioTypeMediaView, newFileUpload, mediaRatioTypeMediaCollectionToUpload);
                    }

                    // Walk through the checkbox items for the different media ratio types for android tv box
                    foreach (ListItem item in this.pnlMediaTypes.RetrieveListItemsByDeviceCategory(DeviceCategory.TvBox))
                    {
                        this.SaveMediaRatioType(item, mediaRatioView, mediaRatioTypeMediaView, newFileUpload, mediaRatioTypeMediaCollectionToUpload);
                    }

                    // Walk through the checkbox items for the different media ratio types for otoucho
                    foreach (ListItem item in this.pnlMediaTypes.RetrieveListItemsByDeviceCategory(DeviceCategory.Otoucho))
                    {
                        this.SaveMediaRatioType(item, mediaRatioView, mediaRatioTypeMediaView, newFileUpload, mediaRatioTypeMediaCollectionToUpload);
                    }

                    // Walk through the checkbox items for the different media ratio types for mobile (android)
                    foreach (ListItem item in this.pnlMediaTypes.RetrieveListItemsByDeviceCategory(DeviceCategory.MobileAndroid))
                    {
                        this.SaveMediaRatioType(item, mediaRatioView, mediaRatioTypeMediaView, newFileUpload, mediaRatioTypeMediaCollectionToUpload);
                    }

                    // Walk through the checkbox items for the different media ratio types for mobile (iphone)
                    foreach (ListItem item in this.pnlMediaTypes.RetrieveListItemsByDeviceCategory(DeviceCategory.MobileIphone))
                    {
                        this.SaveMediaRatioType(item, mediaRatioView, mediaRatioTypeMediaView, newFileUpload, mediaRatioTypeMediaCollectionToUpload);
                    }

                    // Walk through the checkbox items for the different media ratio types for ipad
                    foreach (ListItem item in this.pnlMediaTypes.RetrieveListItemsByDeviceCategory(DeviceCategory.Ipad))
                    {
                        this.SaveMediaRatioType(item, mediaRatioView, mediaRatioTypeMediaView, newFileUpload, mediaRatioTypeMediaCollectionToUpload);
                    }

                    foreach (ListItem item in this.pnlMediaTypes.RetrieveListItemsByDeviceCategory(DeviceCategory.AppLess))
                    {
                        this.SaveMediaRatioType(item, mediaRatioView, mediaRatioTypeMediaView, newFileUpload, mediaRatioTypeMediaCollectionToUpload);
                    }

                    // Walk through the checkbox items for the different media ratio types for misc
                    foreach (ListItem item in this.pnlMediaTypes.RetrieveListItemsByDeviceCategory(DeviceCategory.Misc))
                    {
                        this.SaveMediaRatioType(item, mediaRatioView, mediaRatioTypeMediaView, newFileUpload, mediaRatioTypeMediaCollectionToUpload);
                    }
                }
                finally
                {
                    // Enable updating version ticks
                    Thread.SetData(Thread.GetNamedDataSlot(ObymobiConstants.GeneralValidatorDisableUpdateVersions), false);
                }
            }

            return base.Save();
        }

        private void UploadMedia(MediaCollection mediaCollection)
        {
            foreach (MediaEntity media in mediaCollection)
            {
                media.Upload(media.FileBytes);
            }
        }

        private void UploadMediaRatioTypeMediaCollection(MediaRatioTypeMediaCollection mediaRatioTypeMediaCollection)
        {
            foreach (MediaRatioTypeMediaEntity mediaRatioTypeMedia in mediaRatioTypeMediaCollection)
            {
                MediaHelper.UploadToCdn(mediaRatioTypeMedia, this.DataSourceAsMediaEntity);
            }
        }


        private void RedirectToMediaPage()
        {
            string url;

            int mediaType = 0;
            DionysosWeb.QueryStringHelper.GetInt("mediaType", out mediaType);
            if (mediaType > 0)
                url = ResolveUrl(string.Format("~/Generic/Media.aspx?id={0}&mediaType={1}", this.DataSourceAsMediaEntity.MediaId, mediaType));
            else
                url = ResolveUrl(string.Format("~/Generic/Media.aspx?id={0}", this.DataSourceAsMediaEntity.MediaId));

            if (DionysosWeb.QueryStringHelper.HasValue(NoCulturesQueryParameter))
            {
                url += $"&{NoCulturesQueryParameter}=true";
            }

            Response.Redirect(url);
        }

        public override bool SaveAndGo()
        {
            // Set flag to bypass the redirect in the save method
            this.ReloadOnFirstSave = false;

            return base.SaveAndGo();
        }

        public override bool SaveAndNew()
        {
            // Set flag to bypass the redirect in the save method
            this.ReloadOnFirstSave = false;

            if (this.IsValid && this.Save())
            {
                // Redirect to Page in Add mode
                string url = this.Request.Path + "?mode=add";

                // Rebuild URL with mode Add and no Id
                for (int i = 0; i < this.Request.QueryString.AllKeys.Length; i++)
                {
                    if (this.Request.QueryString.AllKeys[i] != null &&
                        this.Request.QueryString.AllKeys[i].ToLowerInvariant() != "mode" &&
                        this.Request.QueryString.AllKeys[i].ToLowerInvariant() != "id")
                    {
                        string key = this.Request.QueryString.AllKeys[i];
                        url += String.Format("&{0}={1}", key, this.Request.QueryString[key]);
                    }
                }

                if (!DionysosWeb.QueryStringHelper.HasValue("RelatedEntityType"))
                {
                    // Append the relatedEntityType & RelatedEntityId
                    string relatedEntityTypeStr = "&RelatedEntityType=";
                    url += (relatedEntityTypeStr + this.relatedEntityType);
                    //if (this.DataSourceAsMediaEntity != null && !this.DataSourceAsMediaEntity.RelatedEntityName.IsNullOrWhiteSpace())
                    //    url += (relatedEntityTypeStr + this.DataSourceAsMediaEntity.RelatedEntityName);
                }
                if (!DionysosWeb.QueryStringHelper.HasValue("RelatedEntityId"))
                {
                    string relatedEntityIdStr = "&RelatedEntityId=";
                    if (this.RelatedEntityId > 0)
                        url += (relatedEntityIdStr + this.RelatedEntityId.ToString());
                    else if (this.DataSourceAsMediaEntity != null && !this.DataSourceAsMediaEntity.RelatedEntityId.IsNullOrZero())
                        url += (relatedEntityIdStr + this.DataSourceAsMediaEntity.RelatedEntityId);
                }

                this.Response.Redirect(url, false);
                return true;
            }
            else
            {
                return false;
            }
        }

        protected void ddlActionPageId_OnCallback(object sender, CallbackEventArgsBase e)
        {
            if (this.ddlActionSiteId.ValidId > 0)
            {
                PageCollection pages = this.LoadPages(this.ddlActionSiteId.ValidId);
                this.ddlActionPageId.DataSource = pages;
                this.ddlActionPageId.DataBind();
            }
        }

        public new bool Validate()
        {
            if (!this.fuDocument.IsValid)
            {
                this.AddInformator(InformatorType.Warning, this.fuDocument.ErrorMessage);
                return false;
            }

            if (this.DataSourceAsMediaEntity.IsNew && !this.fuDocument.HasFile)
            {
                this.AddInformator(InformatorType.Warning, this.Translate("NoFileSelected", "No picture has been choosen to upload."));
                return false;
            }

            base.Validate();
            return true;
        }

        private bool ValidateLanguageSettings()
        {
            bool ret = true;
            if (!this.DataSourceAsMediaEntity.IsNew && !this.cbMediaCultureAgnostic.Checked)
            {
                // Check if image is used as an agnotic image for other images
                PredicateExpression filter = new PredicateExpression(MediaFields.AgnosticMediaId == this.DataSourceAsMediaEntity.MediaId);
                int mediaCount = new MediaCollection().GetDbCount(filter);
                if (mediaCount > 0)
                {
                    this.AddInformator(InformatorType.Warning, this.Translate("ImageUsedAsAgnotic", "This image is linked to one or more culture images and therefore can't be set as culture specific."));
                    ret = false;
                }
            }

            if (!this.cbMediaCultureAgnostic.Checked && this.lbMediaCultureCultures.SelectedItemsCount > 0)
            {
                string selectedAgnoticImage = GetSelectedAgnosticImage(this.Controls, "cbAgnosticImages");
                if (selectedAgnoticImage.IsNullOrWhiteSpace())
                {
                    this.AddInformator(InformatorType.Warning, this.Translate("NoAgnosticImageSelected", "An agnostic image has to be choosen to be linked to this culture specific image."));
                    ret = false;
                }
                else
                {
                    MediaEntity agnosticMedia = new MediaEntity(int.Parse(selectedAgnoticImage));

                    int exclude = this.DataSourceAsMediaEntity.IsNew ? -1 : this.DataSourceAsMediaEntity.MediaId;
                    List<string> mediaCultureCodes = this.GetMediaCultureCodes(agnosticMedia, exclude);

                    foreach (string cultureCode in this.lbMediaCultureCultures.SelectedValuesAsStringList)
                    {
                        if (mediaCultureCodes.Contains(cultureCode, StringComparison.InvariantCultureIgnoreCase))
                        {
                            // Language already exists for agnostic media, can't have two of the same cultures
                            string cultureName = Obymobi.Culture.Mappings[cultureCode].ToString();
                            this.AddInformator(InformatorType.Warning, this.Translate("LanguageAlreadySetOnAgnosticImage", "The selected agnostic image already has an image set for language: ") + cultureName);
                            ret = false;
                            break;
                        }
                    }
                }
            }

            this.Validate();

            return ret;
        }

        private List<string> GetMediaCultureCodes(MediaEntity mediaEntity, int mediaIdToExclude)
        {
            List<string> cultureCodes = new List<string>();
            MediaCollection medias = mediaEntity.MediaCollection.Where(x => x.MediaId != mediaIdToExclude).ToEntityCollection<MediaCollection>();
            foreach (MediaEntity childMedia in medias)
            {
                foreach (MediaCultureEntity mediaCulture in childMedia.MediaCultureCollection)
                {
                    if (!cultureCodes.Contains(mediaCulture.CultureCode))
                    {
                        cultureCodes.Add(mediaCulture.CultureCode);
                    }
                }
            }
            return cultureCodes;
        }

        private void SaveCultureSettings()
        {
            if (this.lbMediaCultureCultures.SelectedItemsCount > 0)
            {
                string selectedAgnoticImage = GetSelectedAgnosticImage(this.Controls, "cbAgnosticImages");
                this.DataSourceAsMediaEntity.AgnosticMediaId = int.Parse(selectedAgnoticImage);

                List<string> selectedCultures = this.lbMediaCultureCultures.SelectedValuesAsStringList;
                foreach (MediaCultureEntity mediaCultureEntity in this.DataSourceAsMediaEntity.MediaCultureCollection)
                {
                    if (!selectedCultures.Contains(mediaCultureEntity.CultureCode, StringComparison.InvariantCultureIgnoreCase))
                    {
                        // Removed deleted languages
                        mediaCultureEntity.AddToTransaction(this.DataSourceAsMediaEntity);
                        mediaCultureEntity.Delete();
                    }
                    else
                    {
                        // Language still exists, remove from list
                        selectedCultures.Remove(mediaCultureEntity.CultureCode);
                    }
                }

                // Create missing cultures
                foreach (string culture in selectedCultures)
                {
                    MediaCultureEntity mediaLanguageEntity = new MediaCultureEntity();
                    mediaLanguageEntity.ParentCompanyId = this.DataSourceAsMediaEntity.RelatedEntityCompanyId;
                    mediaLanguageEntity.CultureCode = culture;
                    mediaLanguageEntity.MediaId = this.DataSourceAsMediaEntity.MediaId;
                    mediaLanguageEntity.AddToTransaction(this.DataSourceAsMediaEntity);
                    mediaLanguageEntity.Save();
                }
            }
            else if (this.cbMediaCultureAgnostic.Checked)
            {
                this.DataSourceAsMediaEntity.AgnosticMediaId = null;
                this.DataSourceAsMediaEntity.MediaCultureCollection.AddToTransaction(this.DataSourceAsMediaEntity);
                this.DataSourceAsMediaEntity.MediaCultureCollection.DeleteMulti();
            }
        }

        private string GetSelectedAgnosticImage(System.Web.UI.ControlCollection controlCollection, string groupName)
        {
            string ret = "";
            foreach (Control ctl in controlCollection)
            {
                if (ctl.Controls.Count != 0)
                {
                    if (ret == "")
                        ret = GetSelectedAgnosticImage(ctl.Controls, groupName);
                }

                if (ctl.ToString() == "System.Web.UI.WebControls.RadioButton")
                {
                    RadioButton rb = (RadioButton)ctl;
                    if (rb.GroupName.Equals(groupName, StringComparison.InvariantCultureIgnoreCase) && rb.Checked)
                    {
                        ret = rb.ID.Replace("cbAgnosticImage", String.Empty);
                        break;
                    }
                }
            }
            return ret;
        }

        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "Media";

            // Load MediaRatioPanels
            this.LoadUserControls();
            this.DataSourceLoaded += Media_DataSourceLoaded;
            base.OnInit(e);
        }

        private void Media_DataSourceLoaded(object sender)
        {
            this.pnlMediaTypes.MediaEntity = this.DataSourceAsMediaEntity;

            if (!this.DataSourceAsMediaEntity.IsNew)
            {
                this.btPush.Visible = (this.DataSourceAsMediaEntity.MediaRatioTypeMediaCollection.Count(x => x.MediaType.HasValue && (x.MediaType.Value == (int)MediaType.Gallery || x.MediaType.Value == (int)MediaType.GalleryLarge || x.MediaType.Value == (int)MediaType.GallerySmall)) > 0);
            }

            this.SelectCultures();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.VerifyValidPageRequest();
            this.SetGui();
            this.HookupEvents();
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (!IsPostBack)
            {
                // GK In the future we need to review the Media.aspx page, for now I just try to make the experience better when managing sites
                if (this.RelatedEntityType == EntityType.PageElementEntity || this.RelatedEntityType == EntityType.PageTemplateElementEntity || this.RelatedEntityType == EntityType.AttachmentEntity ||
                    (this.relatedEntityType == EntityType.UIWidgetEntity && this.HasPdfMediaRatioTypeMedia))
                {
                    // Enable the related MediaTypes automatically.                    
                    List<MediaTypeGroup> mediaTypeGroups = null;
                    List<MediaType> mediaTypes = null;
                    if (this.RelatedEntityType == EntityType.PageElementEntity)
                    {
                        PageElementEntity pageElement = new PageElementEntity(this.RelatedEntityId);
                        ImageElement imageElement = PageTypeHelper.GetPageElement<ImageElement>(pageElement.PageEntity.PageTypeAsEnum, pageElement.SystemName);
                        mediaTypeGroups = imageElement.GetMediaTypeGroups();
                        mediaTypes = imageElement.GetMediaTypes();
                    }
                    else if (this.RelatedEntityType == EntityType.PageTemplateElementEntity)
                    {
                        PageTemplateElementEntity pageTemplateElement = new PageTemplateElementEntity(this.RelatedEntityId);
                        ImageElement imageElement = PageTypeHelper.GetPageElement<ImageElement>(pageTemplateElement.PageTemplateEntity.PageType.ToEnum<Logic.Cms.PageType>(), pageTemplateElement.SystemName);
                        mediaTypeGroups = imageElement.GetMediaTypeGroups();
                    }

                    // Hide the selection, we know better than the user! ;)                    
                    this.plhMediaRatioTypes.AddJavascript("HideElement('pnlMediaRatioTypes');", true);

                    if (this.RelatedEntityType == EntityType.PageElementEntity || this.RelatedEntityType == EntityType.PageTemplateElementEntity)
                    {
                        // We do however show the image dimenions for the user - just to they now
                        this.plhMediaRatioTypeInformation.AddHtml("<tr><td class=\"label\">Dimensions</td><td>");
                        if (mediaTypes != null)
                        {
                            this.plhMediaRatioTypeInformation.AddHtml("<table style=\"margin-top:6px;margin-left:7px;\"><tr><td colspan=\"2\"><strong>{0}</strong></td></tr><tr>", "Sites V2");
                            foreach (MediaType mediaType in mediaTypes)
                            {
                                MediaRatioType type = MediaRatioTypes.GetMediaRatioType(mediaType);
                                this.plhMediaRatioTypeInformation.AddHtml("<tr><td>{0}&nbsp;&nbsp;</td><td>{1}x{2}</td></tr>", type.Name, type.Width, type.Height);
                            }
                            this.plhMediaRatioTypeInformation.AddHtml("</table>");
                        }

                        foreach (MediaTypeGroup mediaTypeGroup in mediaTypeGroups)
                        {
                            this.plhMediaRatioTypeInformation.AddHtml("<table style=\"margin-top:6px;margin-left:7px;\"><tr><td colspan=\"2\"><strong>{0}</strong></td></tr><tr>", mediaTypeGroup.ToString());
                            List<MediaRatioType> types = MediaRatioTypes.GetMediaRatioTypes(mediaTypeGroup, this.RelatedEntityType.ToString());
                            foreach (MediaRatioType type in types)
                            {
                                this.plhMediaRatioTypeInformation.AddHtml("<tr><td>{0}&nbsp;&nbsp;</td><td>{1}x{2}</td></tr>", type.Name, type.Width, type.Height);
                            }
                            this.plhMediaRatioTypeInformation.AddHtml("</table>");
                        }
                        this.plhMediaRatioTypeInformation.AddHtml("</td></tr>");
                    }
                    else if (this.RelatedEntityType == EntityType.AttachmentEntity || this.HasPdfMediaRatioTypeMedia)
                    {
                        if (this.PageMode != Dionysos.Web.PageMode.Add)
                        {
                            this.plhZoomInformation.Visible = true;
                        }
                        else
                        {
                            string mediaType;
                            if (DionysosWeb.QueryStringHelper.TryGetValue("mediaType", out mediaType))
                            {
                                MediaType type = int.Parse(mediaType).ToEnum<MediaType>();
                                MediaRatioType mediaRatioType = MediaRatioTypes.GetMediaRatioType(type);
                                if (mediaRatioType != null)
                                {
                                    this.plhMediaRatioTypeInformation.AddHtml("<tr><td class=\"label\">Dimensions</td><td>");
                                    this.plhMediaRatioTypeInformation.AddHtml("<table style=\"margin-top:6px;margin-left:7px;\"><tr><td colspan=\"2\"><strong>Attachment</strong></td></tr><tr>");
                                    this.plhMediaRatioTypeInformation.AddHtml("<tr><td>{0}&nbsp;&nbsp;</td><td>{1}x{2}</td></tr>", mediaRatioType.Name, mediaRatioType.Width, mediaRatioType.Height);
                                    this.plhMediaRatioTypeInformation.AddHtml("</table>");
                                    this.plhMediaRatioTypeInformation.AddHtml("</td></tr>");
                                }
                            }
                        }
                    }
                }

            }

            base.OnPreRender(e);
        }

        private void hlDeleteDocument_Click(object sender, EventArgs e)
        {
            CloudUploader.Delete(ObymobiConstants.CloudContainerMediaFiles, this.DataSourceAsMediaEntity.GetMediaPath(FileNameType.DeletionWildcard), true);
            this.Refresh();
        }

        private void hlDownloadOriginalImage_Click(object sender, EventArgs e)
        {
            DownloadOriginalImage();
        }

        private void btRefreshAllCdn_Click(object sender, EventArgs e)
        {
            foreach (MediaRatioTypeMediaEntity mediaratioTypeMedia in this.DataSourceAsMediaEntity.MediaRatioTypeMediaCollection)
            {
                MediaHelper.QueueMediaRatioTypeMediaFileTask(mediaratioTypeMedia, MediaProcessingTaskEntity.ProcessingAction.Upload, true, null, null);
            }
            string text = this.Translate("RequeuedAllForCdn", "Alle items zijn is opnieuw klaar gezet om te verzenden naar het CDN."); // The item '{0}' has been requeued for publication to the CDN 
            this.AddInformatorInfo(text);
            this.Validate();
        }

        private void btRefreshCdn_Click(object sender, EventArgs e)
        {
            if (sender is LinkButton)
            {
                LinkButton lb = (LinkButton)sender;

                if (lb.ID.Contains("-original"))
                {
                    if (this.DataSourceAsMediaEntity.FileBytes == null || this.DataSourceAsMediaEntity.FileBytes.Length == 0)
                    {
                        this.AddInformator(InformatorType.Warning, "Unable to requeue original image. This image is not loaded into memory. If it does not exists on the CDN contact a dev.");
                    }
                    else
                    {
                        string mediaPathOld = string.Empty;
                        if (this.DataSourceAsMediaEntity.LastDistributedVersionTicks.HasValue)
                        {
                            mediaPathOld = this.DataSourceAsMediaEntity.GetMediaPath(FileNameType.Format);
                            mediaPathOld = mediaPathOld.Replace(MediaEntity.FILENAME_FORMAT_TICKS, this.DataSourceAsMediaEntity.LastDistributedVersionTicks.Value.ToString(CultureInfo.InvariantCulture));
                        }

                        this.DataSourceAsMediaEntity.LastDistributedVersionTicks = DateTime.UtcNow.Ticks;
                        this.DataSourceAsMediaEntity.Save();

                        string mediaPath = this.DataSourceAsMediaEntity.GetMediaPath(FileNameType.Format);
                        mediaPath = mediaPath.Replace(MediaEntity.FILENAME_FORMAT_TICKS, this.DataSourceAsMediaEntity.LastDistributedVersionTicks.GetValueOrDefault(0).ToString(CultureInfo.InvariantCulture));

                        // Upload original image to the cloud
                        HostingEnvironment.QueueBackgroundWorkItem((_) =>
                                              {
                                                  try
                                                  {
                                                      GeneralAuthorizer.SetThreadGodMode(true);

                                                      if (!mediaPathOld.IsNullOrWhiteSpace())
                                                      {
                                                          CloudUploader.Delete(ObymobiConstants.CloudContainerMediaFiles, mediaPathOld, false);
                                                      }

                                                      CloudUploader.UploadMedia(this.DataSourceAsMediaEntity, ObymobiConstants.CloudContainerMediaFiles, mediaPath, this.DataSourceAsMediaEntity.FileBytes);

                                                  }
                                                  catch (Exception exc)
                                                  {
                                                      ErrorLoggerWeb.LogError(exc, exc.Message);
                                                  }
                                              });

                        this.AddInformatorInfo("The original image is being uploaded to the CDN.");
                    }
                }
                else
                {
                    int mediaRatioTypeMediaId = Convert.ToInt32(StringUtil.GetAllAfterFirstOccurenceOf(lb.ID, "-"));
                    MediaRatioTypeMediaEntity mrtm = new MediaRatioTypeMediaEntity(mediaRatioTypeMediaId);
                    if (!mrtm.IsNew)
                    {
                        MediaHelper.QueueMediaRatioTypeMediaFileTask(mrtm, MediaProcessingTaskEntity.ProcessingAction.Upload, true, null, null);
                        string text = this.Translate("RequeuedForCdn", "Het item '{0}' is opnieuw klaar gezet om te verzenden naar het CDN."); // The item '{0}' has been requeued for publication to the CDN 
                        this.AddInformatorInfo(text, mrtm.MediaRatioType.Name);
                    }
                }
            }
        }

        private void btGetMediaProcessingQueueStatus_Click(object sender, EventArgs e)
        {
            string url = string.Format("~/Configuration/Sys/CdnManagement.aspx?Type=6&MediaId={0}", this.DataSourceAsMediaEntity.MediaId);
            this.Response.Redirect(url);
        }

        #endregion

        #region Properties

        private bool HasPdfMediaRatioTypeMedia
        {
            get { return this.DataSourceAsMediaEntity != null && this.DataSourceAsMediaEntity.MediaRatioTypeMediaCollection.Count(x => x.MediaType != null && x.MediaType == (int)MediaType.WidgetPdf1280x800) > 0; }
        }

        private bool IsAppLessMediaRatioTypeMedia
        {
            get
            {
                return this.DataSourceAsMediaEntity != null &&
                       this.DataSourceAsMediaEntity.MediaRatioTypeMediaCollection.Any(m => m.MediaTypeAsEnum.ToString().Contains("AppLess", StringComparison.InvariantCultureIgnoreCase));
            }
        }

        private MediaEntity DataSourceAsMediaEntity
        {
            get
            {
                MediaEntity mediaEntity = this.DataSource as MediaEntity;
                if (mediaEntity.FileDownloadDelegate == null)
                    mediaEntity.FileDownloadDelegate = () => mediaEntity.Download();
                return mediaEntity;
            }
        }

        private EntityType relatedEntityType;

        public EntityType RelatedEntityType
        {
            get
            {
                return this.relatedEntityType;
            }
            set
            {
                this.relatedEntityType = value;

                if (this.pnlMediaTypes != null)
                    this.pnlMediaTypes.RelatedEntityType = value;
            }
        }

        private int relatedEntityId;

        public int RelatedEntityId
        {
            get
            {
                return this.relatedEntityId;
            }
            set
            {
                this.relatedEntityId = value;

                if (this.pnlMediaTypes != null)
                    this.pnlMediaTypes.RelatedEntityId = value;
            }
        }

        #endregion        

        #region Enums

        private enum UpdateType
        {
            Add,
            Delete
        }

        #endregion
    }
}
