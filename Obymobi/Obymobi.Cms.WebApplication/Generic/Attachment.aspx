﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Generic.Attachment" CodeBehind="Attachment.aspx.cs" %>

<%@ Reference VirtualPath="~/Generic/SubPanels/ImagesPanel.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" runat="Server">
    <div>
        <X:PageControl ID="tabsMain" runat="server" Width="100%">
            <TabPages>
                <X:TabPage Text="Algemeen" Name="Generic">
                    <controls>
                    <D:PlaceHolder runat="server">
					    <table class="dataformV2">
						    <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblName">Name</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
							    </td>
                                <td class="label">
								    <D:LabelTextOnly runat="server" id="lblTypeLabel">Type</D:LabelTextOnly>
							    </td>
							    <td class="control">
								    <D:Label runat="server" id="lblTypeValue" LocalizeText="false"></D:Label>
							    </td>
						    </tr>	
                            <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblProductId" Visible="false">Product</D:LabelEntityFieldInfo>
                                    <D:LabelEntityFieldInfo runat="server" id="lblPageId" Visible="false">Site</D:LabelEntityFieldInfo>
                                    <D:LabelEntityFieldInfo runat="server" id="lblPageTemplateId" Visible="false">Page template</D:LabelEntityFieldInfo>
                                    <D:LabelEntityFieldInfo runat="server" id="lblGenericproductId" Visible="false">Generic Product</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
                                    <U:UltraBoxInt runat="server" ID="ddlProductId" Visible="false"></U:UltraBoxInt>                                
                                    <U:UltraBoxInt runat="server" ID="ddlPageId" EmptyItemText="" Visible="false"></U:UltraBoxInt>
                                    <U:UltraBoxInt runat="server" ID="ddlPageTemplateId" EmptyItemText="" Visible="false"></U:UltraBoxInt>
                                    <U:UltraBoxInt runat="server" ID="ddlGenericproductId" Visible="false"></U:UltraBoxInt>
							    </td>
	                            <td class="label">
		                            <D:LabelEntityFieldInfo runat="server" id="lblSortOrder">Sort order</D:LabelEntityFieldInfo>
	                            </td>
	                            <td class="control">
		                            <D:TextBoxInt ID="tbSortOrder" runat="server"></D:TextBoxInt>
	                            </td>
						    </tr>	
                            <D:PlaceHolder runat="server" ID="plhTypeWebsite">
								<tr>
									<td class="label">
										<D:LabelEntityFieldInfo runat="server" id="lblUrl">Url</D:LabelEntityFieldInfo>
									</td>
									<td class="control">
										<D:TextBoxString ID="tbUrl" runat="server"></D:TextBoxString>
									</td>

									<td class="label">
										<D:LabelEntityFieldInfo runat="server" id="lblAllowedDomains">Toegestane domeinen</D:LabelEntityFieldInfo>
									</td>
									<td class="control" rowspan="3">
										<D:TextBoxMultiLine ID="tblAllowedDomains" runat="server"></D:TextBoxMultiLine>
										<D:Label ID="lblDomainsComment" runat="server"><small>Het domein van de begin url wordt automatisch toegestaan. Plaats elk domein op een nieuwe regel.</small></D:Label>
									</td>							
								</tr>	

								<tr>
									<td class="label">
										<D:LabelEntityFieldInfo runat="server" id="lblOpenNewWindow">Open in new window</D:LabelEntityFieldInfo>
									</td>
									<td class="control">
										<D:CheckBox ID="tbOpenNewWindow" runat="server"></D:CheckBox>
									</td>
								</tr>

								<tr>
									<td class="label">
										<D:LabelEntityFieldInfo runat="server" id="lblAllowAllDomains">Alle domeinen toestaan</D:LabelEntityFieldInfo>
									</td>
									<td class="control" colspan="3">
										<D:CheckBox ID="tbAllowAllDomains" runat="server"></D:CheckBox>
									</td>
								</tr>
								<tr>
									<td class="label">
										&nbsp;
									</td>
									<td class="control" colspan="3">
										&nbsp;
									</td>
								</tr>
                            </D:PlaceHolder>
							<D:PlaceHolder runat="server" ID="plhTypeTelephone">
								<tr>
									<td class="label">
										<D:LabelEntityFieldInfo runat="server" id="lblTelephone">Telephone number</D:LabelEntityFieldInfo>
									</td>
									<td class="control">
										<D:TextBoxString ID="tbTelephone" runat="server"></D:TextBoxString>
									</td>
								</tr>
                            </D:PlaceHolder>
						    <D:PlaceHolder runat="server" ID="plhTypeYoutubeLink">
								<tr>
									<td class="label">
										<D:LabelEntityFieldInfo runat="server" id="lblYoutubeId">Youtube identifier</D:LabelEntityFieldInfo>
									</td>
									<td class="control">
										<D:TextBoxString ID="tbYoutubeIdentifier" runat="server"></D:TextBoxString>
									</td>
								</tr>
                            </D:PlaceHolder>
					    </table>			
                    </D:PlaceHolder>
				</controls>
                </X:TabPage>
            </TabPages>
        </X:PageControl>
    </div>
</asp:Content>

