﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Crave.Api.Logic.Requests.Publishing;
using Crave.Api.Logic.Timestamps;
using Crave.Api.Logic.UseCases;
using DevExpress.Web;
using Dionysos.Web.UI;
using Dionysos.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.ObymobiCms.UI;

namespace Obymobi.ObymobiCms.Generic
{
    public partial class UITheme : PageLLBLGenEntityCms
    {
        #region Methods

        protected override void OnInit(EventArgs e)
		{
            this.LoadUserControls();
            this.DataSourceLoaded += UITheme_DataSourceLoaded;
            base.OnInit(e);
        }

        private void UITheme_DataSourceLoaded(object sender)
        {
            this.SetGui();
        }

        private void SetGui()
        {
            this.ddlCompanyId.DataSource = CmsSessionHelper.CompanyCollectionForUser;
            this.ddlCompanyId.DataBind();
        }

        private void HookupEvents()
        {
            this.btnSetT2DefaultTextSizes.Click += (sender, e) => this.SetT2DefaultTextSizes();
            this.btnSetTminiDefaultTextSizes.Click += (sender, e) => this.SetTminiDefaultTextSizes();
        }

        private void LoadUserControls()
        {
            this.CreateControls();
            this.tabsMain.AddTabPage("Afbeeldingen", "Media", "~/Generic/UserControls/MediaCollection.ascx");
        }

        private void CreateControls()
        {
            this.CreateColorControls();
            this.CreateTextSizeControls();
        }

        private void CreateColorControls()
        {
            foreach (UIColorGroup uiColorGroup in Enum.GetValues(typeof(UIColorGroup)))
            {
                Dionysos.Web.UI.WebControls.Panel panel = new Dionysos.Web.UI.WebControls.Panel();
                panel.ID = string.Format("pnl{0}Color", Enum.GetName(typeof(UIColorGroup), uiColorGroup));
                panel.GroupingText = EnumUtil.GetStringValue(uiColorGroup);

                Table table = new Table();
                table.CssClass = "dataformV2";

                IEnumerable<UIColor> uiColors = UIColors.Colors.Where(x => x.Group == uiColorGroup);
                foreach (UIColor uiColor in uiColors)
                {
                    string colorName = Enum.GetName(typeof(UIColorType), uiColor.Type);

                    TableRow row = new TableRow();

                    // Color label
                    TableCell cellColorLabel = new TableCell();
                    cellColorLabel.CssClass = "label";

                    Dionysos.Web.UI.WebControls.Label labelColor = new Dionysos.Web.UI.WebControls.Label();
                    labelColor.LocalizeText = false;
                    labelColor.ID = string.Format("lbl{0}", colorName);
                    labelColor.Text = string.Format("{0} ({1})", uiColor.Type.GetStringValue(), (int)uiColor.Type);
                    cellColorLabel.Controls.Add(labelColor);

                    row.Cells.Add(cellColorLabel);

                    // Color edit
                    TableCell cellColorEdit = new TableCell();
                    cellColorEdit.CssClass = "control";

                    ASPxColorEdit colorEdit = new ASPxColorEdit();
                    colorEdit.ID = string.Format("ce{0}", colorName);
                    colorEdit.EnableCustomColors = true;
                    colorEdit.CssClass = "floatleft";
                    colorEdit.Color = System.Drawing.Color.FromArgb(uiColor.Color);
                    cellColorEdit.Controls.Add(colorEdit);

                    if(!string.IsNullOrWhiteSpace(uiColor.PreviewName))
                    {
                        System.Web.UI.WebControls.HyperLink preview = new System.Web.UI.WebControls.HyperLink();
                        preview.NavigateUrl = string.Format("../Images/UI-Scheme/{0}", uiColor.PreviewFile);
                        preview.Text = string.Format("Example ({0})", uiColor.PreviewName);
                        preview.Attributes.Add("rel", "lightbox");
                        preview.CssClass = "floatleft margin-top-5 margin-left-5";
                        cellColorEdit.Controls.Add(preview);
                    }
                    
                    row.Cells.Add(cellColorEdit);

                    // Opacity label
                    TableCell cellOpacityLabel = new TableCell();
                    cellOpacityLabel.CssClass = "label";

                    if (uiColor.OpacitySupported)
                    {
                        Dionysos.Web.UI.WebControls.Label labelOpacity = new Dionysos.Web.UI.WebControls.Label();
                        labelOpacity.ID = string.Format("lbl{0}Alpha", colorName);
                        labelOpacity.Text = this.Translate("opacity", "Opacity");
                        cellOpacityLabel.Controls.Add(labelOpacity);
                    }

                    row.Cells.Add(cellOpacityLabel);

                    // Opacity edit
                    TableCell cellOpacityEdit = new TableCell();
                    cellOpacityEdit.CssClass = "control";

                    if (uiColor.OpacitySupported)
                    {
                        ASPxTrackBar opacityEdit = new ASPxTrackBar();
                        opacityEdit.ID = string.Format("trb{0}Alpha", colorName);
                        opacityEdit.ClientIDMode = System.Web.UI.ClientIDMode.Static;
                        opacityEdit.MinValue = 0;
                        opacityEdit.MaxValue = 255;
                        opacityEdit.CssClass = "floatleft margin-top-minus-5";
                        opacityEdit.Height = 25;
                        opacityEdit.ClientSideEvents.PositionChanged = "function(s, e) { tbButtonBackgroundTopColorAlpha.value = trbButtonBackgroundTopColorAlpha.GetPosition(); }";
                        opacityEdit.Value = (int)((uiColor.Color & 0xFF000000) >> 24);
                        cellOpacityEdit.Controls.Add(opacityEdit);

                        TextBoxInt opacityEditText = new TextBoxInt();
                        opacityEditText.ID = string.Format("tb{0}Alpha", colorName);
                        opacityEditText.MinimalValue = 0;
                        opacityEditText.MaximalValue = 255;
                        opacityEditText.Width = 27;
                        opacityEditText.CssClass = "floatleft margin-top-5";
                        opacityEditText.Enabled = false;
                        opacityEditText.Value = (int)((uiColor.Color & 0xFF000000) >> 24);
                        cellOpacityEdit.Controls.Add(opacityEditText);
                    }

                    row.Cells.Add(cellOpacityEdit);

                    table.Rows.Add(row);
                }
                panel.Controls.Add(table);
                plhControls.Controls.Add(panel);
            }
        }

        private void CreateTextSizeControls()
        {
            foreach (UITextSizeGroup uiTextSizeGroup in Enum.GetValues(typeof(UITextSizeGroup)))
            {
                Dionysos.Web.UI.WebControls.Panel panel = new Dionysos.Web.UI.WebControls.Panel();
                panel.ID = string.Format("pnl{0}TextSize", Enum.GetName(typeof(UITextSizeGroup), uiTextSizeGroup));
                panel.GroupingText = EnumUtil.GetStringValue(uiTextSizeGroup);

                Table table = new Table();
                table.CssClass = "dataformV2";

                UITextSize[] uiTextSizes = UITextSizes.TextSizes.Where(x => x.Group == uiTextSizeGroup).ToArray();
                for (int i = 0; i < uiTextSizes.Length; i = i + 2)
                {
                    TableRow row = new TableRow();

                    UITextSize uiTextSize = uiTextSizes[i];
                    string textSizeName = Enum.GetName(typeof(UITextSizeType), uiTextSize.Type);
                    this.AddCellsToRow(uiTextSize, textSizeName, row);

                    if ((i+1) < uiTextSizes.Length)
                    {
                        uiTextSize = uiTextSizes[i+1];
                        textSizeName = Enum.GetName(typeof(UITextSizeType), uiTextSize.Type);
                        this.AddCellsToRow(uiTextSize, textSizeName, row);
                    }
                    else
                    {
                        this.AddCellsToRow(null, textSizeName, row);
                    }

                    table.Rows.Add(row);
                }

                panel.Controls.Add(table);
                this.plhTextSizes.Controls.Add(panel);
            }
        }

        private void AddCellsToRow(UITextSize uiTextSize, string textSizeName, TableRow row)
        {
            if (uiTextSize != null)
            {
                // TextSize label
                TableCell cellTextSizeLabel = new TableCell();
                cellTextSizeLabel.CssClass = "label";

                Dionysos.Web.UI.WebControls.Label labelTextSize = new Dionysos.Web.UI.WebControls.Label();
                labelTextSize.LocalizeText = false;
                labelTextSize.ID = string.Format("lbl{0}", textSizeName);
                labelTextSize.Text = string.Format("{0} ({1})", uiTextSize.Type.GetStringValue(), (int)uiTextSize.Type);
                cellTextSizeLabel.Controls.Add(labelTextSize);

                row.Cells.Add(cellTextSizeLabel);

                // TextSize edit
                TableCell cellTextSizeEdit = new TableCell();
                cellTextSizeEdit.CssClass = "control";

                TextBoxInt textSizeEditText = new TextBoxInt();
                textSizeEditText.ID = string.Format("tb{0}TextSize", textSizeName);
                textSizeEditText.Width = 27;
                textSizeEditText.CssClass = "floatleft margin-top-5";
                textSizeEditText.Value = uiTextSize.TextSize;
                textSizeEditText.Attributes.Add("placeholder", uiTextSize.TextSize.ToString());
                cellTextSizeEdit.Controls.Add(textSizeEditText);

                row.Cells.Add(cellTextSizeEdit);
            }
            else
            {
                // TextSize label
                TableCell cellTextSizeLabel = new TableCell();
                cellTextSizeLabel.CssClass = "label";
                row.Cells.Add(cellTextSizeLabel);

                // TextSize edit
                TableCell cellTextSizeEdit = new TableCell();
                cellTextSizeEdit.CssClass = "control";
                row.Cells.Add(cellTextSizeEdit);
            }
        }

        private void SetT2DefaultTextSizes()
        {
            if (this.Save())
            {
                this.SaveDefaultT2TextSizes();
            }
        }

        private void SetTminiDefaultTextSizes()
        {
            if (this.Save())
            {
                this.SaveDefaultTminiTextSizes();
            }
        }

        private void SetWarnings()
        {
            if (this.DataSourceAsUIThemeEntity.CompanyId.HasValue)
                this.AddInformator(InformatorType.Information, this.Translate("UITheme.LinkedToCompany", "This UI theme is linked to company: {0}"), this.DataSourceAsUIThemeEntity.CompanyEntity.Name);

            this.Validate();
        }

		#endregion

		#region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            this.HookupEvents();

            if (!this.IsPostBack)
            {
                this.SetWarnings();

                this.LoadColorFields();
                this.LoadTextSizeFields();
             }
       
            if (CmsSessionHelper.CurrentRole <= Role.Reseller)
            {
                this.lblCompanyId.Visible = false;
                this.ddlCompanyId.Visible = false;
            }
		}

        public override bool Save()
        {
            if (CmsSessionHelper.CurrentRole <= Role.Reseller)
            {
                this.DataSourceAsUIThemeEntity.CompanyId = CmsSessionHelper.CurrentCompanyId;
            }

            if (base.Save())
            {    
                this.SaveColorFields();
                this.SaveTextSizeFields();

                return true;
            }

            return false;
        }

        private void SaveColorFields()
        {
            foreach(UIColor uiColor in UIColors.Colors)
            {
                string colorName = Enum.GetName(typeof(UIColorType), uiColor.Type);

                ASPxColorEdit colorEdit = this.FindControlRecursive(this.Master, string.Format("ce{0}", colorName)) as ASPxColorEdit;
                ASPxTrackBar trackbar = this.FindControlRecursive(this.Master, string.Format("trb{0}Alpha", colorName)) as ASPxTrackBar;

                int alpha = trackbar != null && trackbar.Value != null ? (int)trackbar.Position : 255;
                if (colorEdit != null)
                {
                    int red = colorEdit.Color.R;
                    int green = colorEdit.Color.G;
                    int blue = colorEdit.Color.B;

                    UIThemeColorEntity uiThemeColor = this.DataSourceAsUIThemeEntity.UIThemeColorCollection.FirstOrDefault(x => x.Type == uiColor.Type);
                    if(uiThemeColor == null)
                    {
                        uiThemeColor = new UIThemeColorEntity();
                        uiThemeColor.UIThemeId = this.DataSourceAsUIThemeEntity.UIThemeId;
                        uiThemeColor.Type = uiColor.Type;
                        this.DataSourceAsUIThemeEntity.UIThemeColorCollection.Add(uiThemeColor);
                    }
                    uiThemeColor.Color = alpha << 24 | red << 16 | green << 8 | blue;
                }
            }
            this.DataSourceAsUIThemeEntity.UIThemeColorCollection.SaveMulti();
        }

        private void SaveTextSizeFields()
        {
            foreach (UITextSize uiTextSize in UITextSizes.TextSizes)
            {
                string textSizeName = Enum.GetName(typeof(UITextSizeType), uiTextSize.Type);

                TextBoxInt tbTextSize = this.FindControlRecursive(this.Master, string.Format("tb{0}TextSize", textSizeName)) as TextBoxInt;
                if (tbTextSize != null && tbTextSize.Value.HasValue)
                {
                    if (tbTextSize.Value == uiTextSize.TextSize)
                    {
                        // Text size is the default value
                        UIThemeTextSizeEntity uiThemeTextSizeEntity = this.DataSourceAsUIThemeEntity.UIThemeTextSizeCollection.FirstOrDefault(x => x.Type == uiTextSize.Type);
                        if (uiThemeTextSizeEntity != null)
                        {
                            if (uiThemeTextSizeEntity.Delete())
                                this.DataSourceAsUIThemeEntity.UIThemeTextSizeCollection.Remove(uiThemeTextSizeEntity);
                        }
                    }
                    else
                    {
                        // Text size value has been changed
                        UIThemeTextSizeEntity uiThemeTextSizeEntity = this.DataSourceAsUIThemeEntity.UIThemeTextSizeCollection.FirstOrDefault(x => x.Type == uiTextSize.Type);
                        if (uiThemeTextSizeEntity == null)
                        {
                            uiThemeTextSizeEntity = new UIThemeTextSizeEntity();
                            uiThemeTextSizeEntity.UIThemeId = this.DataSourceAsUIThemeEntity.UIThemeId;
                            uiThemeTextSizeEntity.Type = uiTextSize.Type;
                            this.DataSourceAsUIThemeEntity.UIThemeTextSizeCollection.Add(uiThemeTextSizeEntity);
                        }
                        uiThemeTextSizeEntity.TextSize = tbTextSize.Value.Value;
                    }
                }
            }
            this.DataSourceAsUIThemeEntity.UIThemeTextSizeCollection.SaveMulti();
        }

        private void SaveDefaultT2TextSizes()
        {
            foreach (UITextSize uiTextSize in UITextSizes.TextSizes)
            {
                string textSizeName = Enum.GetName(typeof(UITextSizeType), uiTextSize.Type);

                TextBoxInt tbTextSize = this.FindControlRecursive(this.Master, string.Format("tb{0}TextSize", textSizeName)) as TextBoxInt;
                if (tbTextSize != null && tbTextSize.Value.HasValue)
                {
                    if (tbTextSize.Value == uiTextSize.TextSize)
                    {
                        // Text size is the default value
                        UIThemeTextSizeEntity uiThemeTextSizeEntity = this.DataSourceAsUIThemeEntity.UIThemeTextSizeCollection.FirstOrDefault(x => x.Type == uiTextSize.Type);
                        if (uiThemeTextSizeEntity != null)
                        {
                            if (uiThemeTextSizeEntity.Delete())
                                this.DataSourceAsUIThemeEntity.UIThemeTextSizeCollection.Remove(uiThemeTextSizeEntity);
                        }
                    }
                    else
                    {
                        // Text size value has been changed
                        UIThemeTextSizeEntity uiThemeTextSizeEntity = this.DataSourceAsUIThemeEntity.UIThemeTextSizeCollection.FirstOrDefault(x => x.Type == uiTextSize.Type);
                        if (uiThemeTextSizeEntity == null)
                        {
                            uiThemeTextSizeEntity = new UIThemeTextSizeEntity();
                            uiThemeTextSizeEntity.UIThemeId = this.DataSourceAsUIThemeEntity.UIThemeId;
                            uiThemeTextSizeEntity.Type = uiTextSize.Type;
                            this.DataSourceAsUIThemeEntity.UIThemeTextSizeCollection.Add(uiThemeTextSizeEntity);
                        }
                        uiThemeTextSizeEntity.TextSize = uiTextSize.TextSize;
                    }
                }
            }
            this.DataSourceAsUIThemeEntity.UIThemeTextSizeCollection.SaveMulti();
        }

        private void SaveDefaultTminiTextSizes()
        {
            foreach (UITextSize uiTextSize in UITextSizes.TextSizes)
            {
                string textSizeName = Enum.GetName(typeof(UITextSizeType), uiTextSize.Type);

                TextBoxInt tbTextSize = this.FindControlRecursive(this.Master, string.Format("tb{0}TextSize", textSizeName)) as TextBoxInt;
                if (tbTextSize != null && tbTextSize.Value.HasValue)
                {
                    if (tbTextSize.Value == uiTextSize.TextSizeTmini)
                    {
                        // Text size is the default value
                        UIThemeTextSizeEntity uiThemeTextSizeEntity = this.DataSourceAsUIThemeEntity.UIThemeTextSizeCollection.FirstOrDefault(x => x.Type == uiTextSize.Type);
                        if (uiThemeTextSizeEntity != null)
                        {
                            if (uiThemeTextSizeEntity.Delete())
                                this.DataSourceAsUIThemeEntity.UIThemeTextSizeCollection.Remove(uiThemeTextSizeEntity);
                        }
                    }
                    else
                    {
                        // Text size value has been changed
                        UIThemeTextSizeEntity uiThemeTextSizeEntity = this.DataSourceAsUIThemeEntity.UIThemeTextSizeCollection.FirstOrDefault(x => x.Type == uiTextSize.Type);
                        if (uiThemeTextSizeEntity == null)
                        {
                            uiThemeTextSizeEntity = new UIThemeTextSizeEntity();
                            uiThemeTextSizeEntity.UIThemeId = this.DataSourceAsUIThemeEntity.UIThemeId;
                            uiThemeTextSizeEntity.Type = uiTextSize.Type;
                            this.DataSourceAsUIThemeEntity.UIThemeTextSizeCollection.Add(uiThemeTextSizeEntity);
                        }
                        uiThemeTextSizeEntity.TextSize = uiTextSize.TextSizeTmini;
                    }
                }
            }
            this.DataSourceAsUIThemeEntity.UIThemeTextSizeCollection.SaveMulti();
        }

        private void LoadColorFields()
        {
            foreach(UIThemeColorEntity uiThemeColor in this.DataSourceAsUIThemeEntity.UIThemeColorCollection)
            {
                string colorName = Enum.GetName(typeof(UIColorType), uiThemeColor.Type);

                ASPxColorEdit colorEdit = this.FindControlRecursive(this.Master, string.Format("ce{0}", colorName)) as ASPxColorEdit;
                TextBoxInt opacityEdit = this.FindControlRecursive(this.Master, string.Format("tb{0}Alpha", colorName)) as TextBoxInt;
                ASPxTrackBar trackBar = this.FindControlRecursive(this.Master, string.Format("trb{0}Alpha", colorName)) as ASPxTrackBar;

                if (colorEdit != null)
                    colorEdit.Color = System.Drawing.Color.FromArgb(uiThemeColor.Color);

                if (opacityEdit != null)
                    opacityEdit.Value = (int)((uiThemeColor.Color & 0xFF000000) >> 24);

                if (trackBar != null)
                    trackBar.Value = (int)((uiThemeColor.Color & 0xFF000000) >> 24);
            }
        }

        private void LoadTextSizeFields()
        {
            foreach (UIThemeTextSizeEntity uiThemeTextSize in this.DataSourceAsUIThemeEntity.UIThemeTextSizeCollection)
            {
                string textSizeName = Enum.GetName(typeof(UITextSizeType), uiThemeTextSize.Type);

                TextBoxInt tbTextSize = this.FindControlRecursive(this.Master, string.Format("tb{0}TextSize", textSizeName)) as TextBoxInt;
                if (tbTextSize != null)
                    tbTextSize.Value = uiThemeTextSize.TextSize;
            }
        }

        public Control FindControlRecursive(Control root, string id)
        {
            if (root.ID == id)
                return root;

            foreach (Control control in root.Controls)
            {
                Control foundControl = FindControlRecursive(control, id);
                if (foundControl != null)
                    return foundControl;
            }
            return null;
        }

		#endregion

		#region Properties

        public UIThemeEntity DataSourceAsUIThemeEntity
        {
            get
            {
                return this.DataSource as UIThemeEntity;
            }
        }

		#endregion
    }
}
