﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dionysos;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using System.Collections;
using Dionysos.Web.UI.WebControls;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Generic
{
    public partial class Entertainmenturl : Obymobi.ObymobiCms.UI.PageLLBLGenEntityCms
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            EntertainmentCollection collection = new EntertainmentCollection();
            PredicateExpression filter = new PredicateExpression();
            if (CmsSessionHelper.CurrentRole <= Role.Administrator)
            {
                filter.Add(EntertainmentFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            }
            collection.GetMulti(filter);

            this.ddlEntertainmentId.DataSource = collection;
            this.ddlEntertainmentId.DataBind();
        }
    }
}