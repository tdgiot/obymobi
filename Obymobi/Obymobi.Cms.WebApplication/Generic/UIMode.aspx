<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Generic.UIMode" Codebehind="UIMode.aspx.cs" %>
<%@ Reference Control="~/Generic/SubPanels/UIModeSettingsPanels/EmenuSettingsPanel.ascx" %>
<%@ Reference Control="~/Generic/SubPanels/UIModeSettingsPanels/ConsoleSettingsPanel.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true"/>
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<controls>
                    <D:PlaceHolder runat="server">
					    <table class="dataformV2">
						    <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblName">Name</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
							    </td>
                                <td class="label">
								    <D:Label runat="server" id="lblType">Type</D:Label>
							    </td>
							    <td class="control">
								    <X:ComboBoxInt runat="server" ID="ddlType" IsRequired="true"></X:ComboBoxInt>
							    </td>
					        </tr>
                            <D:PlaceHolder runat="server" ID="plhTypeSpecificSettings">
                            </D:PlaceHolder>
                        </table>
                    </D:PlaceHolder>
				</controls>
			</X:TabPage>			
            <X:TabPage Text="Gebruikt door" Name="tabUsedBy">
                <controls>
                    <D:PlaceHolder ID="plhUsedByDpgs" runat="server" Visible="false">
                        <D:Panel ID="pnlDeliverypointgroups" runat="server" GroupingText="Tafelgroepen">
                            <table>
                                <D:PlaceHolder runat="server" ID="plhDeliverypointGroups"></D:PlaceHolder>
                            </table>
                        </D:Panel>
                    </D:PlaceHolder>
                    <D:PlaceHolder ID="plhUsedByTerminals" runat="server" Visible="false">
                        <D:Panel ID="pnlTerminals" runat="server" GroupingText="Terminals">
                            <table>
                                <D:PlaceHolder runat="server" ID="plhTerminals"></D:PlaceHolder>
                            </table>
                        </D:Panel>
                    </D:PlaceHolder>
                </controls>
            </X:TabPage>
            <X:TabPage Text="Copy" Name="Copy">
                <controls>
                    <table class="dataformV2">                        
                        <tr>
		                    <td class="label"> 
                                <D:LabelTextOnly ID="lblNewUIModeName" runat="server">UI mode naam</D:LabelTextOnly>
                            </td>
                             <td class="control">   
                                <D:TextBoxString ID="tbNewUIModeName" runat="server" notdirty="true"></D:TextBoxString>
                            </td>
                             <td class="label">
                                 
                             </td>
                            <td class="control">
                                <D:Button ID="btnCopyUIMode" runat="server" Text="Kopiëren" ClientIDMode="Static"/>
                            </td>
                        </tr>        
                        <tr>
                            <td class="label">
                                
                            </td>
                            <td class="control">
                                <strong id="strProgress" style="display:none;"><D:LabelTextOnly runat="server" id="lblProgress">Progress</D:LabelTextOnly></strong>
                            </td>
                        </tr>                                        
                        <tr>
                            <td class="label">
                                
                            </td>
                            <td class="control" colspan="3">
                                <D:TextBoxMultiLine runat="server" ID="tbProgress" Rows="40" ClientIDMode="Static" ReadOnly="true" Style="display:none;"></D:TextBoxMultiLine>   
                            </td>
                        </tr>
                    </table>
                </controls>
            </X:TabPage>
		</TabPages>
	</X:PageControl>
</div>
<script type="text/javascript">
    var progressVisible = false;

    InitProgress();

    function InitProgress() {
        PageMethods.SessionExists(function (exists) {
            progressVisible = exists;
        });

        UpdateProgress();
    }

    function UpdateProgress() {
        PageMethods.SessionExists(function (exists) {
            if (exists) {
                progressVisible = true;
            } else {
                progressVisible = false;
            }

            if (progressVisible) {
                document.getElementById("btnCopyUIMode").style.display = 'none';
                document.getElementById("strProgress").style.display = 'block';
                document.getElementById("tbProgress").style.display = 'block';

                PageMethods.GetLog(function (log) {
                    document.getElementById("tbProgress").innerHTML = log;
                    document.getElementById("tbProgress").scrollTop = 99999;
                });
            } else {
                document.getElementById("btnCopyUIMode").style.display = 'block';
            }
        });

        setTimeout(function () { UpdateProgress(); }, 500);
    }
</script>
</asp:Content>

