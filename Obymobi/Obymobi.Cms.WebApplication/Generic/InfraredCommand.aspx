﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Generic.InfraredCommand" Codebehind="InfraredCommand.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
<X:PageControl Id="tabsMain" runat="server" Width="100%">   
<TabPages>
<X:TabPage Text="Generic" Name="Generic">
	<Controls>    
    <table class="dataformV2">
        <tr>
		    <td class="label">
			    <D:LabelEntityFieldInfo runat="server" id="lblType">Type</D:LabelEntityFieldInfo>
		    </td>
		    <td class="control">
			    <D:TextBoxString ID="cbTypeStringValue" runat="server" IsRequired="true" Enabled="False" UseDataBinding="true"></D:TextBoxString> 
		    </td>
            <td class="label">
                <D:LabelEntityFieldInfo runat="server" id="lblName">Name</D:LabelEntityFieldInfo>
            </td>            
            <td class="control">
                <D:TextBoxString ID="tbName" runat="server" UseDataBinding="true"></D:TextBoxString> 
            </td>            
	    </tr>		    
        <tr>
            <td class="label">
                <D:Label runat="server" id="lblHex">Hex code</D:Label>
            </td>
            <td class="control threeCol" colspan="3">
                <D:TextBoxMultiLine ID="tbHex" runat="server" Rows="10" UseDataBinding="true"></D:TextBoxMultiLine>
            </td>            
        </tr>
    </table>
    </Controls>
</X:TabPage>
</TabPages>    
</X:PageControl>
</div>
</asp:Content>