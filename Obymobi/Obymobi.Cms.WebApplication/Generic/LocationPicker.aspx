﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Generic_LocationPicker" Codebehind="LocationPicker.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Location picker</title>
    <style type="text/css">
           *, html, body
           {
               margin:0;
               padding:0;
           }
    </style>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDgSYuFGAwGS1dcPR6eHMjaYyTxSngFAx8&callback=initMap"></script>
    <script type="text/javascript" src="../js/locationpicker.jquery.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="map" style="width: 500px; height: 400px;"></div>
        <D:PlaceHolder ID="plhScript" runat="server"></D:PlaceHolder>
    </form>
</body>
</html>
