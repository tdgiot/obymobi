﻿using System;
using System.Net;
using System.Web.UI.WebControls;
using Crave.Api.Logic;
using Crave.Api.Logic.Requests;
using Crave.Api.Logic.UseCases;
using Dionysos;
using Dionysos.Web;
using Dionysos.Web.UI;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;

namespace Obymobi.ObymobiCms.Generic
{
    public partial class Publishing : Dionysos.Web.UI.PageLLBLGenEntity
    {
        #region Methods

        public void RePublish()
        {
            foreach (PublishingItemEntity publishingItemEntity in this.DataSourceAsPublishing.PublishingItemCollection)
            {
                RepublishApiOperationRequest request = new RepublishApiOperationRequest
                {
                    PublishingItemEntity = publishingItemEntity,
                    CompanyId = CmsSessionHelper.CurrentCompanyId,
                    UserEntity = CmsSessionHelper.CurrentUser,
                    BaseUrl = WebEnvironmentHelper.GetRestApiBaseUrl(),
                    Force = true
                };
                new RepublishApiOperationUseCase().Execute(request);
            }

            this.DataSourceAsPublishing.Refetch();
            if (this.DataSourceAsPublishing.Status == PublishingStatus.Success || this.DataSourceAsPublishing.Status == PublishingStatus.SuccessAfterRetry)
                this.AddInformator(InformatorType.Information, "Data was re-published successfully, please restart your devices to view the changes.");
            else
                this.AddInformator(InformatorType.Warning, "Something went wrong whilst trying to re-publish the data. Click <a href=\"{0}\" >here</a> for details.", ResolveUrl("~/Generic/Publishing.aspx?mode=view&id=" + this.DataSourceAsPublishing.PublishingId.ToString()));

            this.Validate();
        }

        protected override void OnInit(EventArgs e)
        {
            this.PageMode = PageMode.View;
            this.LoadUserControls();

            if (CmsSessionHelper.CurrentRole >= Role.Crave)
                this.tabsMain.AddTabPage("Operations", "ApiOperations", "~/Generic/SubPanels/PublishingItemCollection.ascx");

            base.OnInit(e);
            this.SetGui();
        }

        private void LoadUserControls()
        {
        }

        private void SetGui()
        {
            var masterPage = this.Master as MasterPages.MasterPageEntity;
            if (masterPage != null)
                masterPage.ToolBar.EditButton.Visible = false;

            string parameter1 = string.Format("{0}={1}", this.DataSourceAsPublishing.Parameter1Name, this.DataSourceAsPublishing.Parameter1Value);
            string parameter2 = string.Empty;
            if (!this.DataSourceAsPublishing.Parameter2Name.IsNullOrWhiteSpace() && !this.DataSourceAsPublishing.Parameter2Value.IsNullOrWhiteSpace())
                parameter2 = string.Format(" {0}={1}", this.DataSourceAsPublishing.Parameter2Name, this.DataSourceAsPublishing.Parameter2Value);
            string parameters = string.Format(" ({0}{1})", parameter1, parameter2);

            string publishRequest = this.DataSourceAsPublishing.PublishRequest.Replace("Crave.Api.Logic.Requests.Publishing.Publish", string.Empty);
            publishRequest += parameters;

            this.lblPublishRequestValue.Text = publishRequest;
            this.lblPublishedValue.Text = this.DataSourceAsPublishing.PublishedUTC.UtcToLocalTime(CmsSessionHelper.CompanyTimeZone).ToString();

            if (this.DataSourceAsPublishing.UserId.HasValue)
                this.lblUserName.Text = this.DataSourceAsPublishing.UserEntity.Username;
            else
                this.lblUserName.Text = this.DataSourceAsPublishing.Username;

            this.lblStatusValue.Text = this.DataSourceAsPublishing.StatusText;

            foreach (PublishingItemEntity publishingItemEntity in this.DataSourceAsPublishing.PublishingItemCollection)
            {
                if (!ApiMetaData.Operations.ContainsKey(publishingItemEntity.ApiOperation))
                    continue;

                ApiOperation apiOperation = ApiMetaData.Operations[publishingItemEntity.ApiOperation];

                this.plhApiOperations.AddHtml("<tr>");
                this.plhApiOperations.AddHtml("<td>{0}</td>", publishingItemEntity.ApiOperation);
                this.plhApiOperations.AddHtml("<td style=\"text-align: center;\">{0}</td>", publishingItemEntity.StatusText);

                string cdnPath = string.Format("{0}", apiOperation.Path);
                if (cdnPath.Contains("{" + publishingItemEntity.Parameter1Name + "}"))
                    cdnPath = cdnPath.Replace("{" + publishingItemEntity.Parameter1Name + "}", publishingItemEntity.Parameter1Value);
                if (cdnPath.Contains("{" + publishingItemEntity.Parameter2Name + "}"))
                    cdnPath = cdnPath.Replace("{" + publishingItemEntity.Parameter2Name + "}", publishingItemEntity.Parameter2Value);

                string fileName = string.Format("{0}_{1}-{2}", apiOperation.RequestName, publishingItemEntity.Parameter1Name, publishingItemEntity.Parameter1Value);
                if (!publishingItemEntity.Parameter2Name.IsNullOrWhiteSpace() && !publishingItemEntity.Parameter2Value.IsNullOrWhiteSpace())
                    fileName += string.Format("_{0}-{1}", publishingItemEntity.Parameter2Name, publishingItemEntity.Parameter2Value);

                if (!publishingItemEntity.ApiOperation.Contains("GetClientConfigurationTimestamps"))
                    fileName += "-" + publishingItemEntity.PublishedTicks.ToString();
                fileName += ".json.gz";

                cdnPath += "/" + fileName;

                string amazonUrl = WebEnvironmentHelper.GetCdnBaseUrlPublished(CloudStorageProviderIdentifier.Amazon, cdnPath, true);
                this.plhApiOperations.AddHtml(this.GetCdnTableCell(this.RemoteFileExists(amazonUrl), amazonUrl));

                this.plhApiOperations.AddHtml("<td class=\"refresh\">");
                LinkButton btnRepublish = new LinkButton { ID = "republish-" + publishingItemEntity.PublishingItemId };
                btnRepublish.Click += btnRepublish_Click;
                this.plhApiOperations.Controls.Add(btnRepublish);
                this.plhApiOperations.AddHtml("</td>");

                this.plhApiOperations.AddHtml("</tr>");
            }
        }

        private bool RemoteFileExists(string url)
        {
            try
            {
                //Creating the HttpWebRequest
                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
                //Setting the Request method HEAD, you can also use GET too.
                request.Method = "HEAD";
                //Getting the Web Response.
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                //Returns TRUE if the Status code == 200
                response.Close();
                return (response.StatusCode == HttpStatusCode.OK);
            }
            catch
            {
                //Any exception will returns false.
                return false;
            }
        }

        private string GetCdnTableCell(bool fileExists, string url)
        {
            if (fileExists)
                return "<td class=\"UpToDate\"><a href=\"{0}\" target=\"_blank\">&nbsp;</a></td>".FormatSafe(url);
            else
                return "<td class=\"NotAvailable\"><span></span></td>";
        }

        #endregion

        #region Properties

        public PublishingEntity DataSourceAsPublishing
        {
            get
            {
                return this.DataSource as PublishingEntity;
            }
        }

        #endregion

        #region Event handlers

        private void btnRepublish_Click(object sender, EventArgs e)
        {
            if (sender is LinkButton)
            {
                LinkButton lb = (LinkButton)sender;

                int publishingItemId = Convert.ToInt32(StringUtil.GetAllAfterFirstOccurenceOf(lb.ID, "-"));
                PublishingItemEntity publishingItemEntity = new PublishingItemEntity(publishingItemId);

                RepublishApiOperationRequest request = new RepublishApiOperationRequest
                {
                    PublishingItemEntity = publishingItemEntity,
                    CompanyId = CmsSessionHelper.CurrentCompanyId,
                    UserEntity = CmsSessionHelper.CurrentUser,
                    BaseUrl = WebEnvironmentHelper.GetRestApiBaseUrl(),
                    Force = true
                };
                new RepublishApiOperationUseCase().Execute(request);

                Response.Redirect(Request.RawUrl);
            }
        }

        #endregion
    }
}
