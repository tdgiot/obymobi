﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Generic.InfraredConfiguration" Codebehind="InfraredConfiguration.aspx.cs" %>
<%@ Reference Control="~/Generic/SubPanels/InfraredCommandsPanel.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
    <div>
        <X:PageControl Id="tabsMain" runat="server" Width="100%">   
            <TabPages>
                <X:TabPage Text="Generic" Name="Generic">
	                <Controls>
                        <table class="dataformV2">
                            <tr>
		                        <td class="label">
			                        <D:LabelEntityFieldInfo runat="server" id="lblName">Name</D:LabelEntityFieldInfo>
		                        </td>
		                        <td class="control">
			                        <D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
		                        </td>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblMillisecondsBetweenCommands">Milliseconds between commands</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:TextBoxInt ID="tbMillisecondsBetweenCommands" runat="server" IsRequired="true"></D:TextBoxInt>
                                </td>
	                        </tr>		    
                            <tr>
                                <td class="label">
                                    <D:Label runat="server" id="lblButtons">Buttons</D:Label>
                                </td>
                                <td class="control" colspan="3">
                                    <div style="width: 736px;">
                                        <D:PlaceHolder runat="server" ID="plhCommandsPanel"></D:PlaceHolder>
                                        <br />
                                        <D:Panel runat="server" ID="pnlCreateCommands" Visible="false">
                                            <D:Label runat="server" ID="lblNoCommands" Text="No commands have been created yet. Add commands manually or use the button below to create a set of commands."></D:Label>
                                            <br />
                                        </D:Panel>
                                        <D:Button runat="server" ID="btnAddCustomCommand" Text="Add custom command" Visible="false" />
                                        <D:Button runat="server" ID="btnCreateCommandsForRemote" Text="Create commands for remote" Visible="false" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </Controls>
                </X:TabPage>
                <X:TabPage Text="Import" Name="Import">
	                <Controls>
                        <table class="dataformV2">
                             <tr>
                                <td class="label">
                                    <D:Label runat="server" id="lbJsonFile">Json file</D:Label>
                                </td>
                                <td class="control">
                                    <D:FileUpload runat="server" ID="fuDocument" notdirty="true" />
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                </td>
                                <td class="control">
                                    <D:Button runat="server" ID="btnImport" Text="Import" />
                                </td>
                                <td class="label">
                                </td>
                                <td class="control">
                                </td>
                            </tr>
                        </table>
	                </Controls>
                </X:TabPage>
                <X:TabPage Text="Copy" Name="Copy">
	                <Controls>
                        <table class="dataformV2">
                            <tr>
                                <td class="label">
                                    <D:Label runat="server" ID="lblCopyName">Name</D:Label>
                                </td>
                                <td class="control">
                                    <D:TextBoxString runat="server" ID="tbCopyName" notdirty="true"></D:TextBoxString>
                                    
                                </td>
                                <td class="label">
                                </td>
                                <td class="control">
                                    <D:Button runat="server" ID="btnCopy" Text="Copy" />
                                </td>
                            </tr>
                        </table>
	                </Controls>
                </X:TabPage>
            </TabPages>    
        </X:PageControl>
    </div>
</asp:Content>