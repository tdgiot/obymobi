﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Web.UI.WebControls;
using Obymobi.Enums;
using Obymobi.ObymobiCms.UI;
using Obymobi.Logic.Cms;
using Dionysos.Web.UI.DevExControls;
using DevExpress.Web;
using Dionysos.Data.LLBLGen;
using Obymobi.Logic.HelperClasses;
using Dionysos.Web.Google.Geocoding;
using Dionysos;

namespace Obymobi.ObymobiCms.Generic
{
    public partial class VenueCategory : PageLLBLGenEntityCms
    {
		#region Methods

		protected override void OnInit(EventArgs e)
		{
            this.LoadUserControls();
            //this.DataSourceLoaded += new Dionysos.Delegates.Web.DataSourceLoadedHandler(VenueCategory_DataSourceLoaded);
			base.OnInit(e);			
		}

        private void LoadUserControls()
        {
            this.tabsMain.AddTabPage("Translations", "CustomTextCollection", "~/Generic/UserControls/CustomTextCollection.ascx", "Generic");
            
            // Re-enable this when the parent-child relation is added again
            //this.tabsMain.AddTabPage("Child categories", "VenueCategoryCollection", "~/Generic/SubPanels/VenueCategoryCollection.ascx");
        }

        public override bool Save()
        {
            if (base.Save())
            {
                CustomTextHelper.UpdateCustomTexts(this.DataSource);                
                return true;
            }
            else
                return false;
        }

		#endregion

		#region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
		}

		#endregion

		#region Properties

        public VenueCategoryEntity DataSourceAsVenueCategoryEntity
        {
            get
            {
                return this.DataSource as VenueCategoryEntity;
            }
        }

		#endregion
    }
}
