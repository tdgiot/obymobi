﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Generic.AccessCode" Codebehind="AccessCode.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
    <script type="text/javascript">
        function OnListBoxSelectionChanged(listBox, args) {
            if (args.index == 0)
                args.isSelected ? listBox.SelectAll() : listBox.UnselectAll();
        }
</script>
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblCode">Code</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbCode" runat="server" IsRequired="true" UseDataBinding="false"></D:TextBoxString>
                                <D:Label ID="lblCodeValue" runat="server" Visible="false" LocalizeText="false"></D:Label>
							</td>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblType">Type</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <X:ComboBoxEnum ID="cbType" runat="server" Type="Obymobi.Enums.AccessCodeType, Obymobi" IsRequired="true" UseDataBinding="false"></X:ComboBoxEnum> 
                                <D:Label ID="lblTypeValue" runat="server" Visible="false" LocalizeText="false"></D:Label>
                            </td>
						</tr>
                        <tr>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblAnalyticsEnabled">Analytics Enabled</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <D:CheckBox runat="server" ID="cbAnalyticsEnabled" UseDataBinding="True"/>
                            </td>
                            <td class="label"></td>
                            <td class="control"></td>
                        </tr>
                        <tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblCompanies">Companies</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
                                <dxe:ASPxCheckBox ID="cbCompaniesAllNone" runat="server" CssClass="listbox-checkall-checkbox" ClientIDMode="Static" EnableTheming="false" ClientSideEvents-CheckedChanged="function () { if(cbCompaniesAllNone.GetChecked()) { cblCompanies.SelectAll(); } else { cblCompanies.SelectIndex(-1); } }" Text="Check all/none"/>        
                                <X:ListBox ID="cblCompanies" runat="server" SelectionMode="CheckColumn" ClientIDMode="Static" ValueType="System.Int32" />
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblPointsOfInterest">Points of interest</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
                                <dxe:ASPxCheckBox ID="cbPointsOfInterestAllNone" runat="server" CssClass="listbox-checkall-checkbox" ClientIDMode="Static" EnableTheming="false" ClientSideEvents-CheckedChanged="function () { if(cbPointsOfInterestAllNone.GetChecked()) { cblPointsOfInterest.SelectAll(); } else { cblPointsOfInterest.SelectIndex(-1); } }" Text="Check all/none"/>        
                                <X:ListBoxLLBLGenEntityCollection ID="cblPointsOfInterest" runat="server" SelectionMode="CheckColumn" EntityName="PointOfInterest" ClientIDMode="Static" />
							</td>
                        </tr>					
					</table>
				</controls>
			</X:TabPage>
			<X:TabPage Text="Hero section" Name="Hero">
				<Controls>
					<table class="dataformV2">
                        <tr>
							<td class="label">
								<D:Label runat="server" id="lblShowOnCarrouselCompanies">Companies</D:Label>
							</td>
							<td class="control">
                                <dxe:ASPxCheckBox ID="cbShowOnCarrouselCompaniesAllNone" runat="server" CssClass="listbox-checkall-checkbox" ClientIDMode="Static" EnableTheming="false" ClientSideEvents-CheckedChanged="function () { if(cbShowOnCarrouselCompaniesAllNone.GetChecked()) { cblShowOnCarrouselCompanies.SelectAll(); } else { cblShowOnCarrouselCompanies.SelectIndex(-1); } }" Text="Check all/none" />        
                                <X:ListBox ID="cblShowOnCarrouselCompanies" runat="server" SelectionMode="CheckColumn" ClientIDMode="Static" />
							</td>
							<td class="label">
								<D:Label runat="server" id="lblShowOnCarrouselPointsOfInterest">Points of interest</D:Label>
							</td>
							<td class="control">
                                <dxe:ASPxCheckBox ID="cbShowOnCarrouselPointsOfInterestAllNone" runat="server" CssClass="listbox-checkall-checkbox" ClientIDMode="Static" EnableTheming="false" ClientSideEvents-CheckedChanged="function () { if(cbShowOnCarrouselPointsOfInterestAllNone.GetChecked()) { cblShowOnCarrouselPointsOfInterest.SelectAll(); } else { cblShowOnCarrouselPointsOfInterest.SelectIndex(-1); } }" Text="Check all/none" />        
                                <X:ListBoxLLBLGenEntityCollection ID="cblShowOnCarrouselPointsOfInterest" runat="server" SelectionMode="CheckColumn" EntityName="PointOfInterest" ClientIDMode="Static" PreventEntityCollectionInitialization="true" />
							</td>
                        </tr>					
					</table>
				</controls>
			</X:TabPage>
			<X:TabPage Text="Action" Name="Action">
				<Controls>
                    <D:Panel ID="pnlAddCompaniesOrPOIsFromCity" runat="server" GroupingText="Add companies or points of interest from city">
					    <table class="dataformV2">
                            <tr>
							    <td class="label">
								    <D:Label runat="server" id="lblAddCompaniesFromCity">Add companies from city</D:Label>
							    </td>
							    <td class="control">
                                    <X:ComboBox runat="server" id="cbAddCompaniesFromCity" notdirty="true"></X:ComboBox>
                                    <div style="padding-top: 3px">
                                        <D:Button ID="btnAddCompaniesFromCity" runat="server" Text="Add"></D:Button>
                                    </div>
							    </td>
							    <td class="label">
                                    <D:Label runat="server" id="lblAddPointsOfInterestFromCity">Add points of interest from city</D:Label>
							    </td>
							    <td class="control">
                                    <X:ComboBox runat="server" id="cbAddPointsOfInterestFromCity" notdirty="true"></X:ComboBox>
                                    <div style="padding-top: 3px">
                                        <D:Button ID="btnAddPointsOfInterestFromCity" runat="server" Text="Add"></D:Button>
                                    </div>
							    </td>
                            </tr>					
					    </table>
                    </D:Panel>
				</controls>
			</X:TabPage>
        </TabPages>
	</X:PageControl>
</div>
</asp:Content>