﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Dionysos.Web;
using Obymobi.Data.CollectionClasses;
using Obymobi.Enums;
using Obymobi.Data.EntityClasses;

namespace Obymobi.ObymobiCms.Generic
{
    public partial class UIThemes : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "UITheme";
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (CmsSessionHelper.CurrentRole <= Role.Reseller)
            {
                LLBLGenProDataSource datasource = this.DataSource as LLBLGenProDataSource;
                if (datasource != null)
                {
                    var filter = new PredicateExpression();
                    filter.Add(UIThemeFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
                    datasource.FilterToUse = filter;
                }
            }
        }

        #endregion
    }
}
