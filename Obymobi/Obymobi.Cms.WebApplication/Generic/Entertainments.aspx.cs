﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Dionysos.Web;
using Obymobi.Enums;

namespace Obymobi.ObymobiCms.Generic
{
	public partial class Entertainments : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            int companySpecific;
            if (QueryStringHelper.TryGetValue("companyspecific", out companySpecific))
            {
                if (companySpecific == 1)
                    this.cbCompanySpecific.Checked = true;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            LLBLGenProDataSource datasource = this.DataSource as LLBLGenProDataSource;
            if (datasource != null)
            {
                if (this.cbCompanySpecific.Checked || CmsSessionHelper.CurrentRole < Role.Administrator)
                {
                    PredicateExpression filter = new PredicateExpression();
                    filter.Add(EntertainmentFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
                    datasource.FilterToUse = filter;
                }
                else
                {
                    PredicateExpression filter = new PredicateExpression();
                    filter.Add(EntertainmentFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
                    filter.AddWithOr(EntertainmentFields.CompanyId == DBNull.Value);
                    datasource.FilterToUse = filter;
                }
            }

            if (CmsSessionHelper.CurrentRole < Role.Administrator)
            {
                this.lblOnlyCompanySpecific.Visible = false;
                this.cbCompanySpecific.Visible = false;
            }
        }

        public void Refresh()
        {
            QueryStringHelper qs = new QueryStringHelper();

            if (this.cbCompanySpecific.Checked)
                qs.AddItem("companyspecific", "1");
            else
                qs.AddItem("companyspecific", "0");

            WebShortcuts.Redirect(qs.MergeQuerystringWithRawUrl());
        }

        #endregion
    }
}
