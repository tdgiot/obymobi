﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Generic.EntertainmentConfiguration" Codebehind="EntertainmentConfiguration.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%" EnableViewState="True">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
                <controls>
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:Label runat="server" id="lblName">Name</D:Label>
							</td>
							<td class="control">
							    <D:TextBoxString runat="server" ID="tbName" />
							</td>
							<td class="label">								
							</td>
							<td class="control">                                
							</td>	
					    </tr>                            
					 </table>
                    <D:Panel runat="server" ID="pnlEntertainments" GroupingText="Entertainment">
                        <br/>
                        <D:CheckBoxListLLBLGenEntityCollection runat="server" ID="cblEntertainments" EntityName="Entertainment" DataTextField="Name" LinkCollectionPropertyOnParentDataSource="EntertainmentConfigurationEntertainmentCollection" LinkEntityName="EntertainmentConfigurationEntertainmentEntity" RepeatColumns="3" CssClass="checkboxlist-advertisements" />
                    </D:Panel>
				</Controls>
			</X:TabPage>            
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>

