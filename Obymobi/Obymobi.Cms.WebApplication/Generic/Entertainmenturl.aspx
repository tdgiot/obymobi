﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Generic.Entertainmenturl" Codebehind="Entertainmenturl.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:Label runat="server" id="lblUrl">URL</D:Label>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbUrl" runat="server" IsRequired="true"></D:TextBoxString>
							</td>
                        	<td class="label">
								<D:Label runat="server" id="Label1">Toegang tot volledig domein</D:Label>
							</td>
							<td class="control">
							    <D:CheckBox runat="server" ID="cbAccessFullDomain" />
							</td>
					    </tr>
						<tr>
                            <td class="label">
								<D:Label runat="server" id="lblType">Initial zoom</D:Label>
							</td>
							<td class="control">
								<D:TextBoxInt ID="tbInitialZoom" runat="server" IsRequired="false" MinimalValue="0" MaximalValue="100"></D:TextBoxInt>
							</td>	
							<td class="label">
							</td>
							<td class="control">
							    <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlEntertainmentId" EntityName="Entertainment" PreventEntityCollectionInitialization="True" IsRequired="false" Visible="false"/>
							</td>
					    </tr>        								    
					 </table>	
				</Controls>
			</X:TabPage>						
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>

