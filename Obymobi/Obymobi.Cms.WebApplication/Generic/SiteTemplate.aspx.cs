﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using Obymobi.ObymobiCms.UI;
using Obymobi.Logic.Cms;
using Dionysos;
using Dionysos.Data.LLBLGen;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Cms.Logic.Sites;
using Obymobi.Logic.HelperClasses;
using Obymobi.ObymobiCms.Generic.SubPanels.Cms;
using Obymobi.Web.Media;
using SiteTemplateHelper = Obymobi.Cms.Logic.Sites.SiteTemplateHelper;

namespace Obymobi.ObymobiCms.Generic
{
    public partial class SiteTemplate : PageLLBLGenEntityCms
    {
        #region Fields

        private const string INCLUDE_ATTACHMENTS = "1";
        private const string INCLUDE_MEDIA = "2";

        private PageManagerPanel pageManagerPanel;

        #endregion

        #region Methods

        protected override void OnInit(EventArgs e)
		{
            this.LoadUserControls();
            this.DataSourceLoaded += SiteTemplate_DataSourceLoaded;
			base.OnInit(e);			
		}

        private void LoadUserControls()
        {
            this.tabsMain.AddTabPage("Cultures", "SiteTemplateCultureCollection", "~/Cms/SubPanels/SiteTemplateCultureCollection.ascx", "Advanced");

            bool addMode = (this.PageMode == Dionysos.Web.PageMode.Add);
            if (!addMode)
            {
                this.tabsMain.AddTabPage("Translations", "CustomTextCollection", "~/Generic/UserControls/CustomTextCollection.ascx", "Generic");

                this.pageManagerPanel = (PageManagerPanel)this.LoadControl("~/Generic/SubPanels/Cms/PageManagerPanel.ascx");
                this.pageManagerPanel.ID = "pageManager";
                this.pageManagerPanel.Init(EntityType.SiteTemplateEntity);
                this.plhPageManager.Controls.Add(this.pageManagerPanel);
            }

            this.lblSiteStructure.Visible = !addMode;
            this.plhPageManager.Visible = !addMode;

            this.cbListIncludeInCopy.Items.Add(new ListItem("Copy attachments", SiteTemplate.INCLUDE_ATTACHMENTS));
            this.cbListIncludeInCopy.Items.Add(new ListItem("Copy media", SiteTemplate.INCLUDE_MEDIA));
        }

        private void SetGui()
        {
            IOrderedEnumerable<SiteType> siteTypes = SiteTemplateHelper.SiteTypesCompatibleWithTemplate(this.DataSourceTyped).OrderBy(x => x.ToString());
            foreach (SiteType siteType in siteTypes)
            {
                this.cbSiteType.Items.Add(siteType.ToString(), (int)siteType);
            }

            bool readyToCreateSites = (this.PageMode != Dionysos.Web.PageMode.Add);
            this.plhCreateSite.Visible = readyToCreateSites;
            this.tabsMain.GetTabPageByName("Advanced").Visible = readyToCreateSites;

            if (this.pageManagerPanel != null)
            {
                this.pageManagerPanel.RefreshDataSource(this.DataSource);
            }

            //this.SetLanguagesRelatedGui();
        }

        //private void SetLanguagesRelatedGui()
        //{
        //    this.plhLanguageSelectionCheckBoxes.Controls.Clear();

        //    var languageSort = new SortExpression(LanguageFields.Name | SortOperator.Ascending);
        //    var languages = new LanguageCollection();
        //    languages.GetMulti(null, 0, languageSort);

        //    // Get the languages from the page elements
        //    var relationsFromPageElements = new RelationCollection();
        //    relationsFromPageElements.Add(LanguageEntityBase.Relations.PageTemplateElementEntityUsingLanguageId, JoinHint.Left);
        //    relationsFromPageElements.Add(PageTemplateElementEntityBase.Relations.PageTemplateEntityUsingPageTemplateId, "PageTemplateViaPageTemplateElement", JoinHint.Left);

        //    var filterFromPageElements = new PredicateExpression();
        //    filterFromPageElements.Add(new FieldCompareValuePredicate(PageTemplateFields.SiteTemplateId, ComparisonOperator.Equal, this.DataSourceTyped.SiteTemplateId, "PageTemplateViaPageTemplateElement"));

        //    var languagesFromPageElements = new LanguageCollection();
        //    languagesFromPageElements.GetMulti(filterFromPageElements, 0, languageSort, relationsFromPageElements);

        //    // Get the languages from the page languages
        //    var relationsFromPageLanguages = new RelationCollection();
        //    relationsFromPageLanguages.Add(LanguageEntityBase.Relations.PageTemplateLanguageEntityUsingLanguageId, JoinHint.Left);
        //    relationsFromPageLanguages.Add(PageTemplateLanguageEntityBase.Relations.PageTemplateEntityUsingPageTemplateId, "PageTemplateViaPageTemplateLanguage", JoinHint.Left);

        //    var filterFromPageLanguages = new PredicateExpression();
        //    filterFromPageLanguages.Add(new FieldCompareValuePredicate(PageTemplateFields.SiteTemplateId, ComparisonOperator.Equal, this.DataSourceTyped.SiteTemplateId, "PageTemplateViaPageTemplateLanguage"));

        //    var languagesFromPageLanguages = new LanguageCollection();
        //    languagesFromPageLanguages.GetMulti(filterFromPageLanguages, 0, languageSort, relationsFromPageLanguages);

        //    // Combine the languages to one collection
        //    var languagesWithContent = new LanguageCollection();

        //    foreach (var languageEntity in languagesFromPageElements)
        //        languagesWithContent.Add(languageEntity);

        //    foreach (var languageEntity in languagesFromPageLanguages)
        //        if (!languagesWithContent.Any(x => x.LanguageId == languageEntity.LanguageId))
        //            languagesWithContent.Add(languageEntity);

        //    bool anyLanguages = false;
        //    foreach (var language in languages)
        //    {
        //        this.plhLanguageSelectionCheckBoxes.AddHtml("<tr><td>");
        //        Dionysos.Web.UI.WebControls.CheckBox cb = new Dionysos.Web.UI.WebControls.CheckBox();
        //        cb.ID = "cbLanguage-" + language.LanguageId;
        //        cb.LocalizeText = false;

        //        cb.Checked = this.DataSourceTyped.SiteTemplateLanguageCollection.Any(x => x.LanguageId == language.LanguageId);

        //        if (languagesWithContent.Any(x => x.LanguageId == language.LanguageId))
        //        {
        //            cb.Enabled = false;
        //        }
        //        this.plhLanguageSelectionCheckBoxes.Controls.Add(cb);

        //        Dionysos.Web.UI.WebControls.LabelAssociated lbl = new Dionysos.Web.UI.WebControls.LabelAssociated();
        //        lbl.LocalizeText = false;
        //        lbl.Text = language.Name;
        //        lbl.AssociatedControlID = cb.ID;
        //        this.plhLanguageSelectionCheckBoxes.Controls.Add(lbl);

        //        this.plhLanguageSelectionCheckBoxes.AddHtml("</td></tr>");

        //        if (cb.Checked)
        //        {
        //            anyLanguages = true;
        //            this.plhInroomTabletPreviewLinks.AddHtml("<a href=\"{0}\" target=\"_preview{1}\">{1}</a>", WebEnvironmentHelper.GetApiUrl("Sites/Renderer.ashx?siteTemplateId={0}&languageCode={1}".FormatSafe(this.EntityId, language.Code)), language.Code);
        //            this.plhSmartphonePreviewLinks.AddHtml("<a href=\"{0}\" target=\"_preview{1}\">{1}</a>", WebEnvironmentHelper.GetApiUrl("Sites/Renderer.ashx?siteTemplateId={0}&languageCode={1}&mobile=true".FormatSafe(this.EntityId, language.Code)), language.Code);
        //        }

        //        this.plhPreviewLinks.Visible = anyLanguages && (this.PageMode != Dionysos.Web.PageMode.Add);
        //    }
        //}

        private void HookUpEvents()
        {
            this.btCreateSite.Click += btCreateSite_Click;
        }

		#endregion

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            this.HookUpEvents();
		}

        private void SiteTemplate_DataSourceLoaded(object sender)
        {
            this.SetGui();
        }
        private void btCreateSite_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.tbCreateSiteName.Text.IsNullOrWhiteSpace())
                    this.MultiValidatorDefault.AddError(this.Translate("Error.NameRequiredToCreateSite", "Er dient een naam te worden ingevoerd voor het maken van een Site."));
                else
                {
                    bool includeAttachments = this.cbListIncludeInCopy.SelectedValues("-").Contains(SiteTemplate.INCLUDE_ATTACHMENTS);
                    bool includeMedia = this.cbListIncludeInCopy.SelectedValues("-").Contains(SiteTemplate.INCLUDE_MEDIA);

                    CopySiteTemplateRequest request = new CopySiteTemplateRequest
                    {
                        SiteTemplate = this.DataSourceTyped,
                        SiteName = this.tbCreateSiteName.Text,
                        CopyAttachments = includeAttachments,
                        CopyMedia = includeMedia,
                        CopyMediaOnCdn = MediaCloudHelper.CopyMediaOnCdn
                    };
                    SiteEntity site = SiteTemplateHelper.CopySiteTemplateToSite(request);
                    
                    this.AddInformatorInfo(this.Translate("SiteCreated", "De Site is gemaakt: ") + "<a href=\"{0}\">{1}</a>".FormatSafe(
                        this.ResolveUrl("~/Cms/Site.aspx?id={0}".FormatSafe(site.SiteId)), site.Name));
                }
            }
            catch (Exception ex)
            {
                this.MultiValidatorDefault.AddError(this.Translate("Error.ErrorDuringSiteCopy", "Er is een fout opgetreden tijdens het kopieëren van de Site: ") + ex.Message);
            }

            this.Validate();
        }

        public override bool Save()
        {
            if (base.Save())
            {
                CustomTextHelper.UpdateCustomTexts(this.DataSource);
                return true;
            }
            else
                return false;
        }

		#endregion

		#region Properties

        public SiteTemplateEntity DataSourceTyped
        {
            get
            {
                return this.DataSource as SiteTemplateEntity;
            }
        }

		#endregion
    }
}
