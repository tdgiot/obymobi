﻿using System;
using DevExpress.Web;
using Dionysos.Web.UI;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.ObymobiCms.MasterPages;

namespace Obymobi.ObymobiCms.Generic
{
    public partial class Tags : PageLLBLOverviewDataSourceCollection
    {
        private bool CanManageSystemTags => CmsSessionHelper.CurrentRole >= Role.Crave;
        private bool CanManageCompanyTags => CmsSessionHelper.CurrentRole >= Role.Supervisor;

        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "Tag";
            this.EntityPageUrl = "~/Generic/Tag.aspx";
            
            base.OnInit(e);
            this.SetGui();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.DataSource is LLBLGenProDataSource dataSource)
            {
                dataSource.FilterToUse = GetGridFilter();
            }
            
            this.MainGridView.HtmlDataCellPrepared += MainGridView_HtmlDataCellPrepared;
        }

        private void SetGui()
        {
            if (this.Master is MasterPageEntityCollection masterPage)
            {
                masterPage.ToolBar.AddButton.Visible = CanManageCompanyTags || CanManageSystemTags;
            }

            this.gridFilter.Visible = CanManageSystemTags;
            this.cbShowSystemTags.Checked = CanManageSystemTags;
        }

        private PredicateExpression GetGridFilter()
        {
            PredicateExpression filter = new PredicateExpression();

            if (CanManageSystemTags)
            {
                bool showCompanyTags = this.cbShowCompanyTags.Checked;
                bool showSystemTags = this.cbShowSystemTags.Checked;

                if ((showCompanyTags && !showSystemTags))
                {
                    filter.Add(TagFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
                }
                else if (!showCompanyTags && showSystemTags)
                {
                    filter.Add(TagFields.CompanyId == DBNull.Value);
                }
                else
                {
                    filter.Add(TagFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
                    filter.AddWithOr(TagFields.CompanyId == DBNull.Value);
                }
            }
            else
            {
                filter.Add(TagFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            }

            return filter;
        }

        private void MainGridView_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.DataColumn.FieldName.Equals("CompanyEntity.Name"))
            {
                e.Cell.Text = e.CellValue is string 
                    ? "<span class=\"company-tag-label\">Company</span>" 
                    : "<span class=\"system-tag-label\">System</span>";
            }
        }

        #endregion
    }
}
