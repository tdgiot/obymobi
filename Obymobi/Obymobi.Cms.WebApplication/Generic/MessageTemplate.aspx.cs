﻿using System;
using Obymobi.Data.EntityClasses;

namespace Obymobi.ObymobiCms.Generic
{
    public partial class MessageTemplate : Dionysos.Web.UI.PageLLBLGenEntity
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public override bool Save()
        {
            this.DataSourceAsMessageTemplateEntity.CompanyEntity = null;
            return base.Save();
        }

        /// <summary>
        /// Return the page's datasource as a MessageTemplateEntity
        /// </summary>
        public MessageTemplateEntity DataSourceAsMessageTemplateEntity
        {
            get
            {
                return this.DataSource as MessageTemplateEntity;
            }
        }
    }
}