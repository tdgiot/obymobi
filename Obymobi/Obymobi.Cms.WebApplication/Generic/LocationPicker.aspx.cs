﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Generic_LocationPicker : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        double latitude;
        double longitude;

        if (Request.QueryString["lat"] != null && double.TryParse(Request.QueryString["lat"], out latitude)
            && Request.QueryString["lon"] != null && double.TryParse(Request.QueryString["lon"], out longitude)
            && Request.QueryString["latInput"] != null && Request.QueryString["lonInput"] != null)
        {
            string latInput = Request.QueryString["latInput"];
            string lonInput = Request.QueryString["lonInput"];
            bool viewOnly = Request.QueryString["viewOnly"] != null;

            this.plhScript.AddHtml("<script>");
            this.plhScript.AddHtml("$('#map').locationpicker({");
            this.plhScript.AddHtml("location: {{ latitude: {0}, longitude: {1} }},", latitude, longitude);
            this.plhScript.AddHtml("radius: 0");
            if (!viewOnly)
            {
                this.plhScript.AddHtml(",");
                this.plhScript.AddHtml("inputBinding: {");
                this.plhScript.AddHtml("latitudeInput: $('#{0}', parent.document),", latInput);
                this.plhScript.AddHtml("longitudeInput: $('#{0}', parent.document)", lonInput);
                this.plhScript.AddHtml("},");
                this.plhScript.AddHtml("onchanged: function(currentLocation, radius, isMarkerDropped) {");
                this.plhScript.AddHtml("parent.pageIsDirty = true;");
                this.plhScript.AddHtml("$('.ToolbarSave span', parent.document.body).css('color', '#ff0000');");
                this.plhScript.AddHtml("$('.ToolbarSave span', parent.document.body).css('font-weight', 'bold');");
                this.plhScript.AddHtml("}");
            }
            this.plhScript.AddHtml("});");
            this.plhScript.AddHtml("</script>");
        }
    }
}