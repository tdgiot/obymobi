﻿using System;
using Dionysos.Web;
using Dionysos.Web.UI;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Extensions;
using Obymobi.ObymobiCms.MasterPages;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Generic
{
    public partial class Tag : PageLLBLGenEntity
    {
        private bool CanManageSystemTags => CmsSessionHelper.CurrentRole >= Role.Crave;
        private bool CanManageCompanyTags => CmsSessionHelper.CurrentRole >= Role.Supervisor;
        
        protected override void OnInit(EventArgs e)
        {
            EntityName = nameof(Tag);
            DataSourceLoaded += Tag_DataSourceLoaded;

            base.OnInit(e);
        }
        
		/// <summary>
		/// Gets the data source as a ProductEntity instance
		/// </summary>
		public TagEntity DataSourceAsTagEntity => this.DataSource as TagEntity;
        private TagType TagType => (TagType) cbTagType.Value.GetValueOrDefault();
        
		private void Tag_DataSourceLoaded(object sender)
		{
            SetGui();
		}

        private void SetGui()
        {
            if (PageMode == PageMode.Add)
            {
                cbTagType.Value = (int) TagType.Company;
                cbTagType.Enabled = CanManageSystemTags;
            }
            else
            {
                tbName.ReadOnly = !CanManageCompanyTags;

                cbTagType.Enabled = DataSourceAsTagEntity.GetTagType() == TagType.Company && CanManageSystemTags;
                cbTagType.Value = (int) DataSourceAsTagEntity.GetTagType();

                ((MasterPageEntity)this.Master).ToolBar.DeleteButton.Visible = CanManageCompanyTags && !IsTagUsed();
            }
        }

        public override bool Save()
        {
            int companyId = CmsSessionHelper.CurrentCompanyId;
            if (PageMode == PageMode.Add && TagType == TagType.Company)
            {
                DataSourceAsTagEntity.CompanyId = companyId;
            }

            if (CanManageSystemTags)
            {
                TagType tagType = (TagType)cbTagType.Value.GetValueOrDefault();
                if (this.DataSourceAsTagEntity.CompanyId.HasValue && tagType == TagType.System)
                {
                    // Upgrade tag to system
                    this.DataSourceAsTagEntity.CompanyId = null;
                }
            }

            return base.Save();
        }

        private bool IsTagUsed()
        {
            int tagId = this.DataSourceAsTagEntity.TagId;
            
            int count = (int)new CategoryTagCollection().GetScalar(CategoryTagFieldIndex.TagId, null, AggregateFunction.Count, CategoryTagFields.TagId == tagId);
            if (count > 0) return true;

            count = (int)new ProductTagCollection().GetScalar(ProductTagFieldIndex.TagId, null, AggregateFunction.Count, ProductTagFields.TagId == tagId);
            if (count > 0) return true;

            count = (int)new AlterationoptionTagCollection().GetScalar(AlterationoptionTagFieldIndex.TagId, null, AggregateFunction.Count, AlterationoptionTagFields.TagId == tagId);
            if (count > 0) return true;

            return false;
        }
    }
}
