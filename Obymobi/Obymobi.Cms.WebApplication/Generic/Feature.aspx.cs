﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;

namespace Obymobi.ObymobiCms.Generic
{
	public partial class Feature : Dionysos.Web.UI.PageLLBLGenEntity
    {
		#region Methods

		protected override void OnInit(EventArgs e)
		{
            this.LoadUserControls();
			base.OnInit(e);			
		}

        private void LoadUserControls()
        {
        }

		void SetGui()
		{

	
		}

		#endregion

		#region Event Handlers

		protected void Page_Load(object sender, EventArgs e)
		{
            if (!this.IsPostBack && this.PageMode == Dionysos.Web.PageMode.Add)
                this.cbVisible.Checked = true;
		}		


		#endregion
    }
}
