﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Generic.Feature" Codebehind="Feature.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblName">Naam</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblVisible">Tonen</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:CheckBox runat="server" id="cbVisible" Checked="true"></D:CheckBox>
							</td>     
						</tr>
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblCode">Code</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxInt ID="tbCode" runat="server" IsRequired="true"></D:TextBoxInt>
							</td>
							<td class="label">
							</td>
							<td class="control">
							</td>     
						</tr>                        						
					 </table>					
				</Controls>
			</X:TabPage>	
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>

