﻿using System;
using Crave.Api.Logic.Requests.Publishing;
using Crave.Api.Logic.Timestamps;
using Crave.Api.Logic.UseCases;
using Dionysos.Web.UI;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Generic
{
    public partial class EntertainmentConfiguration : Dionysos.Web.UI.PageLLBLGenEntity
	{
        protected override void OnInit(EventArgs e)
		{
            this.LoadUserControls();
			base.OnInit(e);			
		}        

        private void LoadUserControls()
        {
            PredicateExpression filter = new PredicateExpression(EntertainmentFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            filter.AddWithOr(EntertainmentFields.CompanyId == DBNull.Value);

            SortExpression sort = new SortExpression(new SortClause(EntertainmentFields.Name, SortOperator.Ascending));

            EntertainmentCollection entertainments = new EntertainmentCollection();
            entertainments.GetMulti(filter, 0, sort);

            this.cblEntertainments.DataSource = entertainments;
            this.cblEntertainments.DataBind();
        }        

        public override bool Save()
        {
            if (this.PageMode == Dionysos.Web.PageMode.Add)
            {
                this.DataSourceAsEntertainmentConfigurationEntity.CompanyId = CmsSessionHelper.CurrentCompanyId;
            }        
            return base.Save();
        }

        public EntertainmentConfigurationEntity DataSourceAsEntertainmentConfigurationEntity
        {
            get
            {
                return this.DataSource as EntertainmentConfigurationEntity;
            }
        }        
    }
}
