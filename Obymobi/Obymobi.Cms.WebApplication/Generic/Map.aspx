﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Generic.Map" Codebehind="Map.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
    <D:Button runat="server" ID="btPushToTestClients" Text="Preview" />
    <D:Button runat="server" ID="btPushToLiveClients" Text="Push" PreSubmitWarning="Pushing this map will refresh the map on all clients linked to this map. Are you sure you want to push this map?" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
                    <table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblName">Name</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
							</td>                            
                             <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblMapProvider">Map provider</D:LabelEntityFieldInfo>
							</td>
                            <td class="control">
                                <X:ComboBoxInt runat="server" ID="cbMapProvider" CssClass="input" IncrementalFilteringMode="Contains" IncrementalFilteringDelay="1000" DisplayEmptyItem="false" IsRequired="true"></X:ComboBoxInt>
							</td>       
						</tr>					
                        <tr>
                            <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblMyLocationEnabled">My location enabled</D:LabelEntityFieldInfo>
							</td>
                            <td class="control">
								<D:CheckBox runat="server" ID="cbMyLocationEnabled" />
							</td>                            
                            <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblMapType">Map type</D:LabelEntityFieldInfo>
							</td>
                            <td class="control">
                                <X:ComboBoxInt runat="server" ID="cbMapType" CssClass="input" IncrementalFilteringMode="Contains" IncrementalFilteringDelay="1000" DisplayEmptyItem="false" IsRequired="true"></X:ComboBoxInt>								
							</td>                           
                        </tr>	
                        <tr>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblIndoorEnabled">Indoor enabled</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <D:CheckBox runat="server" ID="cbIndoorEnabled" />
                            </td>
                            <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblZoomControlsEnabled">Zoom controls enabled</D:LabelEntityFieldInfo>
							</td>
                            <td class="control">
								<D:CheckBox runat="server" ID="cbZoomControlsEnabled" />
							</td> 
                        </tr>
                        <tr>                                         
                            <td class="label">
								<D:Label runat="server" id="lblCompany">Company</D:Label>
							</td>
							<td class="control">
								<X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlCompanyId" UseDataBinding="true" EntityName="Company" IsRequired="false" PreventEntityCollectionInitialization="true"></X:ComboBoxLLBLGenEntityCollection>            
							</td>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblZoomLevel">Zoom level</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <D:TextBoxInt runat="server" ID="tbZoomLevel" />
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
	                            <D:Label runat="server" ID="lblPois" LocalizeText="true">Points of interest</D:Label>
                            </td>
                            <td class="control">
	                            <X:ListBox runat="server" ID="lbPois" SelectionMode="CheckColumn" TextField="Name" ValueField="PointOfInterestId"></X:ListBox>
                            </td>            
                        </tr>
                    </table>
                </Controls>
            </X:TabPage>
            <X:TabPage Text="Copy" Name="Copy">
                <Controls>
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:LabelTextOnly ID="lblNewMapName" runat="server">Map name</D:LabelTextOnly>
                            </td>
                            <td class="control">
                                <D:TextBoxString ID="tbNewMapName" runat="server" notdirty="true"></D:TextBoxString>
                            </td>
                            <td class="label">

                            </td>
                            <td class="control">
                                <D:Button ID="btnCopyMap" runat="server" Text="Copy"/>
                            </td>
                        </tr>
                    </table>
                </Controls>
            </X:TabPage>
        </TabPages>
    </X:PageControl>
</div>		
</asp:Content>

