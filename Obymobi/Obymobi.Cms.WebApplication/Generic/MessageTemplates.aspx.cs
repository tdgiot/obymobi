﻿using System;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Generic
{
    public partial class MessageTemplates : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            LLBLGenProDataSource datasource = this.DataSource as LLBLGenProDataSource;
            if (datasource != null)
            {
                var filter = new PredicateExpression(MessageTemplateFields.CompanyId == DBNull.Value);
                datasource.FilterToUse = filter;
            }

            this.EntityPageUrl =  "~/Generic/MessageTemplate.aspx";
        }
    }
}