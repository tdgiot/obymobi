﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Generic.PointOfInterest" Codebehind="PointOfInterest.aspx.cs" %>
<%@ Reference VirtualPath="~/Generic/SubPanels/PointOfInterestPanels/PointOfInterestTabPanel.ascx" %>
<%@ Reference VirtualPath="~/Generic/UserControls/CustomTextCollection.ascx" %>
<%@ Reference VirtualPath="~/Generic/SubPanels/PointOfInterestPanels/PointOfInterestUITab.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
                    <D:Panel ID="pnlContactDetails" runat="server" GroupingText="Contact details">
					    <table class="dataformV2 poi">
						    <tr>
							    <td class="label"> 
								    <D:LabelEntityFieldInfo runat="server" id="lblName">Naam</D:LabelEntityFieldInfo><D:LinkButton ID="lbName" runat="server" Text="Name" LocalizeText="false" ForeColor="Black"></D:LinkButton>
							    </td>
							    <td class="control">
								    <D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
							    </td>
							    <td class="label">
								    &nbsp;
							    </td>
							    <td class="control">
								    &nbsp;
							    </td>
						    </tr>						
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblZipcode">Postcode</D:LabelEntityFieldInfo><D:LinkButton ID="lbZipcode" runat="server" Text="Zipcode" LocalizeText="false" ForeColor="Black"></D:LinkButton>
                                </td>
                                <td class="control">
                                    <D:TextBoxString ID="tbZipcode" runat="server"></D:TextBoxString>
                                </td>
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblAddressline1">Adresregel 1</D:LabelEntityFieldInfo><D:LinkButton ID="lbAddressline1" runat="server" Text="Address line 1" LocalizeText="false" ForeColor="Black"></D:LinkButton>
							    </td>
							    <td class="control">
                                    <D:TextBoxString ID="tbAddressline1" runat="server"></D:TextBoxString>
							    </td>	
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblCity">Plaats</D:LabelEntityFieldInfo><D:LinkButton ID="lbCity" runat="server" Text="City" LocalizeText="false" ForeColor="Black"></D:LinkButton>
                                </td>
                                <td class="control">
                                    <D:TextBoxString ID="tbCity" runat="server"></D:TextBoxString>
                                </td>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblAddressline2">Adresregel 2</D:LabelEntityFieldInfo><D:LinkButton ID="lbAddressline2" runat="server" Text="Address line 2" LocalizeText="false" ForeColor="Black"></D:LinkButton>
                                </td>
                                <td class="control">
                                    <D:TextBoxString ID="tbAddressline2" runat="server"></D:TextBoxString>
                                </td>                            
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblTelephone">Telefoon</D:LabelEntityFieldInfo><D:LinkButton ID="lbTelephone" runat="server" Text="Telephone" LocalizeText="false" ForeColor="Black"></D:LinkButton>
                                </td>
                                <td class="control">
                                    <D:TextBoxString ID="tbTelephone" runat="server"></D:TextBoxString>
                                </td>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblAddressline3">Adresregel 3</D:LabelEntityFieldInfo><D:LinkButton ID="lbAddressline3" runat="server" Text="Address line 3" LocalizeText="false" ForeColor="Black"></D:LinkButton>
                                </td>
                                <td class="control">
                                    <D:TextBoxString ID="tbAddressline3" runat="server"></D:TextBoxString>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblFax">Fax</D:LabelEntityFieldInfo><D:LinkButton ID="lbFax" runat="server" Text="Fax" LocalizeText="false" ForeColor="Black"></D:LinkButton>
                                </td>
                                <td class="control">
                                    <D:TextBoxString ID="tbFax" runat="server"></D:TextBoxString>
                                </td>
                                <td class="label">
                                    <D:LinkButton ID="lbLatitudeLongitude" runat="server" Text="Latitude / longitude" LocalizeText="false" ForeColor="Black"></D:LinkButton>
                                </td>
                                <td class="control">
                                    <D:TextBoxDouble ID="tbLatitude" runat="server" ReadOnly="True" Width="100px"></D:TextBoxDouble>&nbsp;<D:TextBoxDouble ID="tbLongitude" runat="server" ReadOnly="True" Width="100px"></D:TextBoxDouble>&nbsp;<D:PlaceHolder ID="plhMap" runat="server"></D:PlaceHolder>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblCountryId">Land</D:LabelEntityFieldInfo><D:LinkButton ID="lbCountryId" runat="server" Text="Country" LocalizeText="false" ForeColor="Black"></D:LinkButton>
                                </td>
                                <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlCountryId" CssClass="input" UseDataBinding="true" EntityName="Country" IsRequired="true" Width="70%"></X:ComboBoxLLBLGenEntityCollection>
                                </td>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblCurrencyId">Valuta</D:LabelEntityFieldInfo><D:LinkButton ID="lbCurrencyId" runat="server" Text="Currency" LocalizeText="false" ForeColor="Black"></D:LinkButton>
                                </td>
                                <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlCurrencyId" CssClass="input" UseDataBinding="true" EntityName="Currency" IsRequired="true"></X:ComboBoxLLBLGenEntityCollection>
                                </td>
                            </tr>
                            <tr>
                                 <td class="label">
                                     <D:LabelEntityFieldInfo runat="server" id="lblEmail">Email</D:LabelEntityFieldInfo><D:LinkButton ID="lbEmail" runat="server" Text="E-mail" LocalizeText="false" ForeColor="Black"></D:LinkButton>
                                </td>
                                <td class="control">
                                    <D:TextBoxString ID="tbEmail" runat="server"></D:TextBoxString>
                                </td>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblWebsite">Website</D:LabelEntityFieldInfo><D:LinkButton ID="lbWebsite" runat="server" Text="Website" LocalizeText="false" ForeColor="Black"></D:LinkButton>
                                </td>
                                <td class="control">
                                    <D:TextBoxString ID="tbWebsite" runat="server"></D:TextBoxString>
                                </td>
                            </tr>
                        </table>
                    </D:Panel>
                    <D:Panel ID="pnlSettings" runat="server" GroupingText="Settings">
                        <table class="dataformV2">
                             <tr>
                                <td class="label">
                                    <D:Label runat="server" id="lblVisible">Zichtbaar</D:Label>
                                </td>
                                <td class="control">
                                    <D:CheckBox runat="server" id="cbVisible" />
                                </td>
                                <td class="label">
								    <D:Label runat="server" id="lblTimeZoneId">Timezone</D:Label>
							    </td>
							    <td class="control">
							        <X:ComboBox runat="server" ID="ddlTimeZoneOlsonId" IsRequired="false" TextField="Name" ValueField="OlsonTimeZoneId"></X:ComboBox>
							    </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:Label runat="server" id="lblDisplayDistance">Display distance</D:Label>
                                </td>
                                <td class="control">
                                    <D:CheckBox runat="server" ID="cbDisplayDistance" />
                                </td>
                                <td class="label">
                                    <D:Label runat="server" id="lblFloor">Floor level</D:Label>
                                </td>
                                <td class="control">
                                    <D:TextBoxInt runat="server" ID="tbFloor" AllowZero="True"/>
                                </td>
                            </tr>
                        </table>
                    </D:Panel>
                    <D:Panel ID="pnlMobile" runat="server" GroupingText="Mobile">                    
                        <table class="dataformV2 poi">
                            <tr>
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblDescriptionSingleLine">Omschrijving kort</D:LabelEntityFieldInfo><D:LinkButton ID="lbDescriptionSingleLine" runat="server" Text="Description short" LocalizeText="false" ForeColor="Black"></D:LinkButton>
							    </td>
							    <td class="control" colspan="3">
                                    <D:TextBoxString ID="tbDescriptionSingleLine" runat="server"></D:TextBoxString>
							    </td>
                                <td class="label">
								    &nbsp;
							    </td>
							    <td class="control">
								    &nbsp;
							    </td>
					        </tr>                                                   	
						    <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblDescription">Omschrijving</D:LabelEntityFieldInfo><D:LinkButton ID="lbDescription" runat="server" Text="Description" LocalizeText="false" ForeColor="Black"></D:LinkButton>
							    </td>						
							    <td colspan="3" class="control">
								    <D:TextBox ID="tbDescription" runat="server" Rows="5" TextMode="MultiLine" CssClass="inputNoHeight"></D:TextBox>
							    </td>
						    </tr>
                            <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblActionButtonId">Actie knop</D:LabelEntityFieldInfo><D:LinkButton ID="lbActionButtonId" runat="server" Text="Action button" LocalizeText="false" ForeColor="Black"></D:LinkButton>
							    </td>
							    <td class="control">
							        <X:ComboBoxLLBLGenEntityCollection runat="server" ID="cbActionButtonId" CssClass="input" UseDataBinding="true" EntityName="ActionButton"></X:ComboBoxLLBLGenEntityCollection>
							    </td>
                                <td class="label">
							    </td>
							    <td class="control">
							    </td>
					        </tr>
                            <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblActionButtonUrlTablet">Actie knop url tablet</D:LabelEntityFieldInfo><D:LinkButton ID="lbActionButtonUrlTablet" runat="server" Text="Action button url tablet" LocalizeText="false" ForeColor="Black"></D:LinkButton>
							    </td>
							    <td class="control">
							        <D:TextBoxString ID="tbActionButtonUrlTablet" runat="server"></D:TextBoxString>
							    </td>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblActionButtonUrlMobile">Actie knop url mobile</D:LabelEntityFieldInfo><D:LinkButton ID="lbActionButtonUrlMobile" runat="server" Text="Action button url mobile" LocalizeText="false" ForeColor="Black"></D:LinkButton>
							    </td>
							    <td class="control">
                                    <D:TextBoxString ID="tbActionButtonUrlMobile" runat="server"></D:TextBoxString>
							    </td>
					        </tr>
                            <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblCostIndicationType">Prijs indicatie type</D:LabelEntityFieldInfo><D:LinkButton ID="lbCostIndicationType" runat="server" Text="Cost indication type" LocalizeText="false" ForeColor="Black"></D:LinkButton>
							    </td>
							    <td class="control">
                                    <X:ComboBoxEnum ID="cbCostIndicationType" runat="server" Type="Obymobi.Enums.CostIndicationType, Obymobi" ></X:ComboBoxEnum>
							    </td>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblCostIndicationValue">Prijs indicatie waarde</D:LabelEntityFieldInfo><D:LinkButton ID="lbCostIndicationValue" runat="server" Text="Cost indication value" LocalizeText="false" ForeColor="Black"></D:LinkButton>
							    </td>
							    <td class="control">
                                    <D:TextBoxDecimal ID="tbCostIndicationValue" runat="server"></D:TextBoxDecimal>
							    </td>
					        </tr>
					    </table>
                    </D:Panel>
				</controls>
			</X:TabPage>
            <X:TabPage Text="Categories" Name="tabCategories">
                <controls>
					<D:PlaceHolder ID="PlaceHolder1" runat="server">							
						<table class="dataformV2">
							<tr>
								<td class="label">
									<D:Label runat="server" ID="lblPointOfInterestVenueCategories">Categories</D:Label>
								</td>
								<td class="control">
									<D:CheckBoxListLLBLGenEntityCollection runat="server" ID="cblCategories" EntityName="VenueCategory" DataTextField="Name" LinkCollectionPropertyOnParentDataSource="PointOfInterestVenueCategoryCollection" LinkEntityName="PointOfInterestVenueCategoryEntity" RepeatColumns="3" CssClass="checkboxlist-advertisements" />
								</td>
								<td class="label">
								</td>
								<td class="control">
								</td>
							</tr>
						</table>
					</D:PlaceHolder>
				</controls>
            </X:TabPage>
            <X:TabPage Text="Amenities" Name="tabAmenities">
                <controls>
					<D:PlaceHolder ID="PlaceHolder2" runat="server">							
						<table class="dataformV2">
							<tr>
								<td class="label">
									<D:Label runat="server" ID="lblPointOfInterestAmenities">Amenities</D:Label>
								</td>
								<td class="control">
									<D:CheckBoxListLLBLGenEntityCollection runat="server" ID="cblAmenites" EntityName="Amenity" DataTextField="Name" LinkCollectionPropertyOnParentDataSource="PointOfInterestAmenityCollection" LinkEntityName="PointOfInterestAmenityEntity" RepeatColumns="3" CssClass="checkboxlist-advertisements" />
								</td>
								<td class="label">
								</td>
								<td class="control">
								</td>
							</tr>
						</table>
					</D:PlaceHolder>
				</controls>
            </X:TabPage>
            <X:TabPage Text="Tabs" Name="Tabs">
				<Controls>
                    <D:PlaceHolder ID="plhTabs" runat="server"></D:PlaceHolder>
                </Controls>
            </X:TabPage>
            <X:TabPage Text="Images to import" Name="ImagesToImport">
                <Controls>
                    <D:Label ID="lblNoImagesToImport" runat="server" Visible="false">There are no images available to import.</D:Label>
                    <D:PlaceHolder ID="plhImages" runat="server"></D:PlaceHolder>
                </Controls>
            </X:TabPage>
        </TabPages>
	</X:PageControl>
</div>
  


</asp:Content>