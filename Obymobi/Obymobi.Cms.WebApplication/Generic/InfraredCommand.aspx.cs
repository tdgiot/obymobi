﻿using System;
using Obymobi.Data.EntityClasses;
using Obymobi.ObymobiCms.MasterPages;
using Obymobi.ObymobiCms.UI;

namespace Obymobi.ObymobiCms.Generic
{
    public partial class InfraredCommand : PageLLBLGenEntityCms
    {
        private void LoadUserControls()
        {
            InfraredCommandEntity entity = new InfraredCommandEntity(this.EntityId);
            if (entity.IsNew)
                return;

            this.tabsMain.AddTabPage("Translations", "CustomTextCollection", "~/Generic/UserControls/CustomTextCollection.ascx", "Generic");
        }

        private void SetGui()
        {
            this.cbTypeStringValue.Text = this.DataSourceAsInfraredCommandEntity.TypeStringValue;
        }

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            this.DataSourceLoaded += this.InfraredCommand_DataSourceLoaded;
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ((MasterPageEntity)this.Master).ToolBar.SaveAndNewButton.Visible = false;
        }

        private void InfraredCommand_DataSourceLoaded(object sender)
        {
            this.SetGui();
        }

        public InfraredCommandEntity DataSourceAsInfraredCommandEntity
        {
            get { return (this.DataSource as InfraredCommandEntity); }
        }        
    }
}