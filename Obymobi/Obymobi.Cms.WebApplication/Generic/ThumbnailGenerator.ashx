<%@ WebHandler Language="C#" Class="ThumbnailGenerator" %>

using System;
using System.Drawing;
using System.Net;
using System.Web;
using Dionysos;
using Obymobi.Data.EntityClasses;
using Obymobi.Web.CloudStorage;

public class ThumbnailGenerator : IHttpHandler 
{    
    public void ProcessRequest (HttpContext context) 
    {
        int mediaId, width, height;
        bool fitInDimensions;
        
        Dionysos.Web.QueryStringHelper.TryGetValue("fitInDimensions", out fitInDimensions);
        Dionysos.Web.QueryStringHelper.TryGetValue("height", out height); // height is optional                        
        
        if(!Dionysos.Web.QueryStringHelper.TryGetValue("mediaId", out mediaId) ||
            !Dionysos.Web.QueryStringHelper.TryGetValue("width", out width))            
        {
            context.Response.ContentType = "text/plain";
            context.Response.Write("MediaId, Width or Height parameter missing or incorrect format.");
            return;
        }
        
        // Retrieve image from dB (via MediaEntity because we need to know if it's an image or pdf)
        MediaEntity media = new MediaEntity(mediaId);
        if (media.IsNew)
        {
            context.Response.ContentType = "text/plain";
            context.Response.Write("Media does not exists.");
            return;
        }
        
        string cacheKey = String.Format("{0}-{1}-{2}-{3}.jpg", mediaId, fitInDimensions, height, width);

        byte[] resizedImage;
        if(!ThumbnailGeneratorUtil.TryGetFileFromCache(cacheKey, out resizedImage))
        {
            byte[] sourceImage = media.Download();
            if (sourceImage == null || sourceImage.Length <= 0)
            {
                context.Response.ContentType = "text/plain";
                context.Response.Write("Unable to load source image.");
                return;
            }

            try
            {
                resizedImage = Dionysos.Drawing.ImageUtil.ResizeAndReturn(sourceImage, new Size(width, height), fitInDimensions, 30);

                ThumbnailGeneratorUtil.SaveFileToCache(cacheKey, resizedImage);
            }
            catch (Exception ex)
            {
                context.Response.Clear();
                context.Response.StatusCode = 500;
                context.Response.ContentType = "text/plain";
                context.Response.Write("Unable to connect resize image: " + ex.Message);
                return;
            }
        }

        context.Response.ContentType = media.MimeType.IsNullOrWhiteSpace() ? "image/jpeg" : media.MimeType;
        context.Response.BinaryWrite(resizedImage);

        ThumbnailGeneratorUtil.CleanUpOldTempFiles();
    }

    private byte[] DownloadImageFromCdn(string url)
    {
        using (MyWebClient webClient = new MyWebClient())
        {
            try
            {
                return webClient.DownloadData(url);
            }
            catch (Exception)
            {
                // Ignored
            }
        }

        return null;
    }

    private class MyWebClient : WebClient
    {
        protected override WebRequest GetWebRequest(Uri uri)
        {
            WebRequest w = base.GetWebRequest(uri);
            w.Timeout = 3 * 1000;
            return w;
        }
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}