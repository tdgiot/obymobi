using System;
using System.Linq;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Enums;
using Dionysos.Web;
using Obymobi.Logic.HelperClasses;
using Obymobi.Data;
using System.Data;

namespace Obymobi.ObymobiCms.Generic
{
    public partial class Attachment : Dionysos.Web.UI.PageLLBLGenEntity
    {
        #region Fields
        
        private readonly AttachmentType[] attachmentTypesWithThumbnail = { AttachmentType.Document, AttachmentType.Website };
        private readonly AttachmentType[] attachmentTypesWithMedia = { AttachmentType.Document };

        private readonly AttachmentType[] affiliateAttachmentTypes = { AttachmentType.AffiliateBookOnline, AttachmentType.AffiliateVideoAgent, AttachmentType.AffiliatePrices };

        private AttachmentType attachmentType = AttachmentType.NotSet;
        private EntityType entityType;
        private int? primaryId;

        private SubPanels.ImagesPanel imagesPanel = null;

        #endregion

        #region Methods

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            this.DataSourceLoaded += Attachment_DataSourceLoaded;
            base.OnInit(e);
            this.SetGui();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.SaveTransactionToUse = new Transaction(IsolationLevel.ReadUncommitted, "Attachment-Save");
        }

        private void LoadUserControls()
        {
            // We need the Product/Page id's earlier... So manually
            if (PageMode == PageMode.Add)
            {
                if (QueryStringHelper.TryGetValue("ProductId", out int productId))
                {
                    primaryId = productId;
                    entityType = EntityType.ProductEntity;
                }
                else if (QueryStringHelper.TryGetValue("PageId", out int pageId))
                {
                    primaryId = pageId;
                    entityType = EntityType.PageEntity;
                }
                else if (QueryStringHelper.TryGetValue("PageTemplateId", out int pageTemplateId))
                {
                    primaryId = pageTemplateId;
                    entityType = EntityType.PageTemplateEntity;
                }
                else if (QueryStringHelper.TryGetValue("GenericproductId", out int genericProductId))
                {
                    primaryId = genericProductId;
                    entityType = EntityType.GenericproductEntity;
                }
            }
            else if (QueryStringHelper.TryGetValue("id", out int attachmentId))
            {
                var attachment = new AttachmentEntity(attachmentId);
                if (attachment.ProductId.HasValue)
                {
                    this.primaryId = attachment.ProductId.Value;
                    this.entityType = EntityType.ProductEntity;
                }
                else if (attachment.PageId.HasValue)
                {
                    this.primaryId = attachment.PageId.Value;
                    this.entityType = EntityType.PageEntity;
                }
                else if (attachment.PageTemplateId.HasValue)
                {
                    this.primaryId = attachment.PageTemplateId.Value;
                    this.entityType = EntityType.PageTemplateEntity;
                }
                else if (attachment.GenericproductId.HasValue)
                {
                    this.primaryId = attachment.GenericproductId.Value;
                    this.entityType = EntityType.GenericproductEntity;
                }
            }

            if (primaryId == 0)
            { return; }

            this.imagesPanel = this.tabsMain.AddTabPage("Thumbnail", "editor-agnostic", "~/Generic/SubPanels/ImagesPanel.ascx") as SubPanels.ImagesPanel;
            this.imagesPanel.ExclusiveMediaType = MediaType.AttachmentThumbnail;

            this.tabsMain.AddTabPage("Translations", "CustomTextCollection", "~/Generic/UserControls/CustomTextCollection.ascx", "Generic");
            this.tabsMain.AddTabPage("Documenten", "Media", "~/Generic/UserControls/MediaCollection.ascx");
        }

        private void SetGui()
        {
            this.DetermineAttachmentType();

            if (this.entityType == EntityType.ProductEntity)
            {
                this.lblProductId.Visible = true;
                this.ddlProductId.Visible = true;
                this.ddlProductId.IsRequired = true;
                this.ddlProductId.Value = this.primaryId.Value;
            }
            else if (this.entityType == EntityType.PageEntity)
            {
                this.lblPageId.Visible = true;
                this.ddlPageId.Visible = true;
                this.ddlPageId.IsRequired = true;
                this.ddlPageId.Value = this.primaryId.Value;
            }
            else if (this.entityType == EntityType.PageTemplateEntity)
            {
                this.lblPageTemplateId.Visible = true;
                this.ddlPageTemplateId.Visible = true;
                this.ddlPageTemplateId.IsRequired = true;
                this.ddlPageTemplateId.Value = this.primaryId.Value;
            }
            else if (this.entityType == EntityType.GenericproductEntity)
            {
                this.lblGenericproductId.Visible = true;
                this.ddlGenericproductId.Visible = true;
                this.ddlGenericproductId.IsRequired = true;
                this.ddlGenericproductId.Value = this.primaryId.Value;
            }

            this.tabsMain.GetTabPageByName("Media").Visible = this.attachmentTypesWithMedia.Contains(attachmentType);
            this.tabsMain.GetTabPageByName("editor-agnostic").Visible = this.attachmentTypesWithThumbnail.Contains(attachmentType);

            // You can never edit Page/ProductId as it's pre-set
            this.ddlPageId.Enabled = false;
            this.ddlProductId.Enabled = false;
            this.ddlPageTemplateId.Enabled = false;

            this.SetAttachmentTypeSpecificGui();

            this.lblTypeValue.Text = this.attachmentType.ToString();
        }

        private void SetAttachmentTypeSpecificGui()
        {
            this.plhTypeWebsite.Visible = false;
            this.plhTypeTelephone.Visible = false;
            this.plhTypeYoutubeLink.Visible = false;

            switch (this.attachmentType)
            {
                case AttachmentType.Document:
                case AttachmentType.AffiliateVideoAgent:
                    break;
                case AttachmentType.Website:
                    this.EnableWebsiteConfigurationPanel(true);
                    break;
                case AttachmentType.AffiliateBookOnline:
                case AttachmentType.AffiliatePrices:
                    this.EnableWebsiteConfigurationPanel(false);
                    break;
                case AttachmentType.Youtube:
                    this.plhTypeYoutubeLink.Visible = true;
                    break;
                case AttachmentType.Telephone:
                    this.plhTypeTelephone.Visible = true;
                    break;
                default:
                    break;
            }
        }

        private void EnableWebsiteConfigurationPanel(bool showAllowDomainsOptions)
        {
            this.plhTypeWebsite.Visible = true;
            this.tbUrl.IsRequired = true;

            this.lblAllowAllDomains.Visible = showAllowDomainsOptions;
            this.tbAllowAllDomains.Visible = showAllowDomainsOptions;

            this.lblAllowedDomains.Visible = showAllowDomainsOptions;
            this.tblAllowedDomains.Visible = showAllowDomainsOptions;
            this.lblDomainsComment.Visible = showAllowDomainsOptions;
        }

        public override bool Save()
        {
            if (!primaryId.HasValue || primaryId == 0)
            {
                AddInformator(Dionysos.Web.UI.InformatorType.Warning, "Attachments must be linked to another entity. Please add attachments from the Attachments tab of another entity.");
                return false; 
            }

            this.DataSourceAsAttachment.AddToTransaction(this.SaveTransactionToUse);

            if (this.DataSourceAsAttachment.IsNew)
            {
                this.DataSourceAsAttachment.Type = this.attachmentType;
            }

            if (this.attachmentType == AttachmentType.Telephone)
            {
                this.DataSourceAsAttachment.Url = this.tbTelephone.Value;
            }
            else if (this.attachmentType == AttachmentType.Youtube)
            {
                this.DataSourceAsAttachment.Url = this.tbYoutubeIdentifier.Value;
            }

            if (base.Save())
            {
                if (this.DataSourceAsAttachment.PageId.HasValue ||
                    this.DataSourceAsAttachment.PageTemplateId.HasValue ||
                    this.DataSourceAsAttachment.GenericproductId.HasValue ||
                    !this.DataSourceAsAttachment.ParentCompanyId.HasValue)
                {
                    CustomTextHelper.UpdateCustomTexts(this.DataSourceAsAttachment);
                }
                else
                {
                    CustomTextHelper.UpdateCustomTexts(this.DataSourceAsAttachment, new CompanyEntity(this.DataSourceAsAttachment.ParentCompanyId.Value));
                }
                return true;
            }
            else
                return false;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the datasource as a PosalterationEntity instance
        /// </summary>
        public AttachmentEntity DataSourceAsAttachment
        {
            get
            {
                return this.DataSource as AttachmentEntity;
            }
        }

        #endregion

        #region Events

        private void Attachment_DataSourceLoaded(object sender)
        {
            this.DetermineAttachmentType();

            var resource = this.DataSourceAsAttachment.Url;
            this.tbTelephone.Value = resource;
            this.tbYoutubeIdentifier.Value = resource;

            if (this.attachmentTypesWithThumbnail.Contains(this.attachmentType))
            {
                this.LoadThumbnailMediaIntoPanel();
            }

            if (!this.DataBindProducts() && !this.DataBindPages() && !this.DataBindPageTemplates() && !this.DataBindGenericproducts())
            { return; }

            if (this.DataSourceAsAttachment.IsNew && this.affiliateAttachmentTypes.Contains(this.attachmentType))
            {
                this.DataSourceAsAttachment.Name = this.GetDefaultDisplayName(this.attachmentType);
            }
        }

        private string GetDefaultDisplayName(AttachmentType attachmentType)
        {
            switch (this.attachmentType)
            {
                case AttachmentType.AffiliateBookOnline:
                    return "Book online";
                case AttachmentType.AffiliatePrices:
                    return "Check prices & availability";
                case AttachmentType.AffiliateVideoAgent:
                    return "Book via video agent";
                default:
                    throw new ArgumentOutOfRangeException(nameof(attachmentType), $"No default text configured for attachment type: {attachmentType}");
            }
        }

        private void DetermineAttachmentType()
        {
            if (!DataSourceAsAttachment.IsNew)
            {
                attachmentType = DataSourceAsAttachment.Type;
            }
            else if (QueryStringHelper.TryGetValue("type", out string type) && EnumUtil.TryParse(type, out AttachmentType typeEnum))
            {
                attachmentType = typeEnum;
            }
            else
            {
                tabsMain.Visible = false;
            }
        }

        private void LoadThumbnailMediaIntoPanel()
        {
            // Only load the thumnail media for the imagesPanel (these media are filtered out on the documents tab)
            var filter = new PredicateExpression();
            filter.Add(MediaRatioTypeMediaFields.MediaType == (int)MediaType.AttachmentThumbnail);
            filter.AddWithAnd(MediaFields.AttachmentId == this.DataSourceAsAttachment.AttachmentId);

            var relations = new RelationCollection();
            relations.Add(MediaEntityBase.Relations.MediaRatioTypeMediaEntityUsingMediaId);

            var prefetch = new PrefetchPath(EntityType.MediaEntity);
            prefetch.Add(MediaEntityBase.PrefetchPathMediaRatioTypeMediaCollection);

            var thumbnailMedia = new MediaCollection();
            thumbnailMedia.GetMulti(filter, 0, null, relations, prefetch);

            this.imagesPanel.CustomMediaCollection = thumbnailMedia;
        }

        private bool DataBindPages()
        {
            bool toReturn = true;

            // This page will only be called from a Page - so just load that one and lock the thing down for speed.
            int pageId;
            if (!this.DataSourceAsAttachment.PageId.HasValue)
            {
                if (!QueryStringHelper.TryGetValue("PageId", out pageId))
                {
                    toReturn = false;
                }
            }
            else
                pageId = this.DataSourceAsAttachment.PageId.Value;

            PageEntity page = new PageEntity(pageId);
            PageCollection pages = new PageCollection();
            pages.Add(page);

            this.ddlPageId.BindEntityCollection(pages, PageFields.Name, PageFields.PageId);

            return toReturn;
        }

        private bool DataBindProducts()
        {
            bool toReturn = true;

            // This page will only be called from a Product - so just load that one and lock the thing down for speed.
            int productId;
            if (!this.DataSourceAsAttachment.ProductId.HasValue)
            {
                if (!QueryStringHelper.TryGetValue("ProductId", out productId))
                {
                    toReturn = false;
                }
            }
            else
                productId = this.DataSourceAsAttachment.ProductId.Value;

            ProductEntity product = new ProductEntity(productId);
            ProductCollection products = new ProductCollection();
            products.Add(product);

            this.ddlProductId.BindEntityCollection(products, ProductFields.Name, ProductFields.ProductId);

            return toReturn;
        }

        private bool DataBindPageTemplates()
        {
            bool toReturn = true;

            // This page will only be called from a PageTemplate - so just load that one and lock the thing down for speed.
            int pageTemplateId;
            if (!this.DataSourceAsAttachment.PageTemplateId.HasValue)
            {
                if (!QueryStringHelper.TryGetValue("PageTemplateId", out pageTemplateId))
                {
                    toReturn = false;
                }
            }
            else
                pageTemplateId = this.DataSourceAsAttachment.PageTemplateId.Value;

            PageTemplateEntity pageTemplate = new PageTemplateEntity(pageTemplateId);
            PageTemplateCollection pageTemplates = new PageTemplateCollection();
            pageTemplates.Add(pageTemplate);

            this.ddlPageTemplateId.BindEntityCollection(pageTemplates, PageTemplateFields.Name, PageTemplateFields.PageTemplateId);

            return toReturn;
        }

        private bool DataBindGenericproducts()
        {
            bool toReturn = true;

            // This page will only be called from a Product - so just load that one and lock the thing down for speed.
            int genericproductId;
            if (!this.DataSourceAsAttachment.GenericproductId.HasValue)
            {
                if (!QueryStringHelper.TryGetValue("GenericproductId", out genericproductId))
                {
                    toReturn = false;
                }
            }
            else
                genericproductId = this.DataSourceAsAttachment.GenericproductId.Value;

            GenericproductEntity product = new GenericproductEntity(genericproductId);
            GenericproductCollection products = new GenericproductCollection();
            products.Add(product);

            this.ddlGenericproductId.BindEntityCollection(products, GenericproductFields.Name, GenericproductFields.GenericproductId);

            return toReturn;
        }

        #endregion
    }
}
