﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Generic.SiteTemplate" Codebehind="SiteTemplate.aspx.cs" %>
<%@ Reference VirtualPath="~/Generic/SubPanels/Cms/PageManagerPanel.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">    
<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true"/>
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
				    <D:PlaceHolder runat="server">
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblName">Naam</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
							</td>
                            <td class="label">
                                <D:Label runat="server" id="lblSiteType">Site Type</D:Label>
                            </td>
                            <td class="control">
                                <X:ComboBoxInt runat="server" ID="cbSiteType" IsRequired="true"></X:ComboBoxInt>                                
                            </td>
						</tr>	
                        <tr>
                            <D:PlaceHolder runat="server" ID="plhPreviewLinks">
                            <td class="label">
                                <D:Label runat="server" id="lblPreview">HTML Previews</D:Label>
                            </td>
                            <td class="control" colspan="3">
                                <span class="nobold" style="margin-left:0;">
                                    <D:Label runat="server" id="lblInroomTablet">In-room Tablet</D:Label>:&nbsp; <D:PlaceHolder runat="server" ID="plhInroomTabletPreviewLinks"></D:PlaceHolder> | 
                                    <D:Label runat="server" id="lblSmartphone">Smartphone</D:Label>:&nbsp; <D:PlaceHolder runat="server" ID="plhSmartphonePreviewLinks"></D:PlaceHolder>
                                </span>
                            </td>
                            </D:PlaceHolder>   
                        </tr>
                        <tr>
                            <td class="label">
                                <D:Label runat="server" id="lblSiteStructure">Site Structuur</D:Label>
                            </td>
                            <td class="control" colspan="3">
                                <div style="width: 736px;">
                                    <D:PlaceHolder runat="server" ID="plhPageManager"></D:PlaceHolder>
                                </div>
                            </td>
                        </tr>					                   
					</table>      
                    </D:PlaceHolder>             
				</controls>
			</X:TabPage>    
            <X:TabPage Text="Geavanceerd" Name="Advanced">
				<Controls>
                    <D:PlaceHolder runat="server">
					    <table class="dataformV2">
                            <D:PlaceHolder runat="server" ID="plhCreateSite">
						    <tr>
						        <td class="label">
								    <D:LabelAssociated runat="server" id="lblCreateSiteName">Site Naam</D:LabelAssociated>
							    </td>
							    <td class="control">
							    
                                    <div>
								        <D:TextBoxString runat="server" ID="tbCreateSiteName" notdirty="true"></D:TextBoxString><br/><br/>                                    
                                        <D:CheckBoxList ID="cbListIncludeInCopy" runat="server"></D:CheckBoxList>
                                        <label id="lblCopyDescription" runat="server" style="float: left; margin-top: 6px;"><small>Note: By checking the options above, these entities will be copied from the page templates to the individual pages. When unchecked, the attachments and media from the page templates are shown on the devices.</small></label><br/><br/><br/>
                                        <D:Button runat="server" ID="btCreateSite" Text="Site maken"/>
                                    </div>
							    </td>
                                <td class="label">
                                </td>
                                <td class="control">
                                </td>
                            </tr>
                            </D:PlaceHolder>
                        </table>
                    </D:PlaceHolder>
                </Controls>
            </X:TabPage>
        </TabPages>
	</X:PageControl>
</div>
</asp:Content>