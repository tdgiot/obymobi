﻿using System;
using Crave.Api.Logic;
using Dionysos;
using Dionysos.Web;
using Obymobi.Data.EntityClasses;

namespace Obymobi.ObymobiCms.Generic
{
    public partial class PublishingItem : Dionysos.Web.UI.PageLLBLGenEntity
    {
        #region Methods

        protected override void OnInit(EventArgs e)
        {
            this.PageMode = PageMode.View;
            this.LoadUserControls();

            base.OnInit(e);
            this.SetGui();
        }

        private void LoadUserControls()
        {
        }

        private void SetGui()
        {
            var masterPage = this.Master as MasterPages.MasterPageEntity;
            if (masterPage != null)
                masterPage.ToolBar.EditButton.Visible = false;

            this.lblStatusValue.Text = this.DataSourceAsPublishingItem.StatusText;
        }

        #endregion

        #region Properties

        public PublishingItemEntity DataSourceAsPublishingItem
        {
            get
            {
                return this.DataSource as PublishingItemEntity;
            }
        }

        #endregion
    }
}
