﻿using System;
using System.Collections.Generic;
using System.Linq;
using Crave.Api.Logic.Requests.Publishing;
using Crave.Api.Logic.Timestamps;
using Crave.Api.Logic.UseCases;
using Dionysos.Web.Google.Geocoding;
using Dionysos.Web.UI;
using Obymobi.Cms.Logic.Sites;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Obymobi.ObymobiCms.Generic.SubPanels.PointOfInterestPanels;
using Obymobi.ObymobiCms.UI;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Generic
{
    public partial class PointOfInterest : PageLLBLGenEntityCms
    {
        #region Fields

        private Generic.UserControls.CustomTextCollection translationsPanel;

        #endregion

        #region Methods

        protected override void OnInit(EventArgs e)
		{            
            this.LoadUserControls();
            this.DataSourceLoaded += new Dionysos.Delegates.Web.DataSourceLoadedHandler(PointOfInterest_DataSourceLoaded);
			base.OnInit(e);			
		}

        private void PointOfInterest_DataSourceLoaded(object sender)
        {
            this.LoadTabs();

            if (!this.DataSourceAsPointOfInterestEntity.ExternalId.HasValue)
            {
                this.lblNoImagesToImport.Visible = true;
            }

            this.ddlTimeZoneOlsonId.SelectedItem = this.ddlTimeZoneOlsonId.Items.FindByValue(this.DataSourceAsPointOfInterestEntity.TimeZoneOlsonId);
        }

        private void LoadTabs()
        {
            if (!this.DataSourceAsPointOfInterestEntity.IsNew)
            {
                this.LoadTabControls();

                if (this.translationsPanel != null)
                {
                    this.translationsPanel.Initialize();

                    List<string> cultureCodes = this.DataSourceAsPointOfInterestEntity.CustomTextCollection.Where(x => !string.IsNullOrWhiteSpace(x.CultureCode)).Select(x => x.CultureCode).Distinct().ToList();
                    if (cultureCodes.Count > 0)
                    {
                        // Combine the tabs to a complete list
                        List<UITabEntity> combinedTabs = new List<UITabEntity>();
                        if (this.DataSourceAsPointOfInterestEntity.TabletUIModeEntity != null)
                            combinedTabs.AddRange(this.DataSourceAsPointOfInterestEntity.TabletUIModeEntity.UITabCollection.ToList());
                        if (this.DataSourceAsPointOfInterestEntity.MobileUIModeEntity != null)
                            combinedTabs.AddRange(this.DataSourceAsPointOfInterestEntity.MobileUIModeEntity.UITabCollection.ToList());
                        combinedTabs = combinedTabs.OrderBy(x => x.SortOrder).ToList();

                        // Generate list with unique tab names so we can loop through that list
                        List<string> uniqueTabNames = combinedTabs.Select(x => x.Caption).Distinct().ToList();
                        foreach (string tabName in uniqueTabNames)
                        {
                            foreach (string cultureCode in cultureCodes)
                            {
                                UITabEntity uiTabTabletDevice = combinedTabs.SingleOrDefault(t => t.Caption.Equals(tabName, StringComparison.InvariantCultureIgnoreCase) && t.UIModeEntity.Type == UIModeType.GuestOwnedTabletDevices);
                                UITabEntity uiTabMobileDevice = combinedTabs.SingleOrDefault(t => t.Caption.Equals(tabName, StringComparison.InvariantCultureIgnoreCase) && t.UIModeEntity.Type == UIModeType.GuestOwnedMobileDevices);

                                PointOfInterestUITab control = this.CreatePointOfInterestUITab(cultureCode, uiTabTabletDevice, uiTabMobileDevice);
                                this.translationsPanel.AddControl(control, cultureCode);
                            }
                        }
                    }
                }
            }            
        }

        private void LoadTabControls()
        { 
            // Combine the tabs to a complete list
            List<UITabEntity> combinedTabs = new List<UITabEntity>();
            if (this.DataSourceAsPointOfInterestEntity.TabletUIModeEntity != null)
                combinedTabs.AddRange(this.DataSourceAsPointOfInterestEntity.TabletUIModeEntity.UITabCollection.ToList());
            if (this.DataSourceAsPointOfInterestEntity.MobileUIModeEntity != null)
                combinedTabs.AddRange(this.DataSourceAsPointOfInterestEntity.MobileUIModeEntity.UITabCollection.ToList());
            combinedTabs = combinedTabs.OrderBy(x => x.SortOrder).ToList();

            // Generate list with unique tab names so we can loop through that list
            List<string> uniqueTabNames = this.GetUniqueCaptions(combinedTabs);

            // Fetch Sites once here for performance, instead of per control
            SiteCollection sites = SiteHelper.GetSitesAvailableToPointOfInterests(new IncludeFieldsList(SiteFields.Name));

            // Create a control per unique tab name
            int maxSortOrder = 0;            
            foreach (string tabName in uniqueTabNames)
            {
                // Creat the tab panel
                var tabPanel = this.LoadControl<PointOfInterestTabPanel>("~/Generic/Subpanels/PointOfInterestPanels/PointOfInterestTabPanel.ascx");
                tabPanel.TabletUIModeId = this.DataSourceAsPointOfInterestEntity.TabletUIModeEntity.UIModeId;
                tabPanel.MobileUIModeId = this.DataSourceAsPointOfInterestEntity.MobileUIModeEntity.UIModeId;
                tabPanel.Sites = sites;

                // Set the right UI tab entities for the panel
                foreach (var tab in combinedTabs)
                {
                    if (tabName.Equals(tab.Caption))
                    {
                        if (tab.UIModeEntity.Type == UIModeType.GuestOwnedTabletDevices) tabPanel.TabletUITab = tab;                                                    
                        else if (tab.UIModeEntity.Type == UIModeType.GuestOwnedMobileDevices) tabPanel.MobileUITab = tab;
                                                   
                        maxSortOrder = tab.SortOrder > maxSortOrder ? tab.SortOrder : maxSortOrder;                            
                    }
                }
                
                tabPanel.Initialize();
                this.plhTabs.Controls.Add(tabPanel);
            }

            // Render the new empty tabs at the end
            this.RenderEmptyTabPanels(uniqueTabNames.Count, maxSortOrder, sites);
        }

        private void RenderEmptyTabPanels(int renderedPanels, int maxSortOrder, SiteCollection sites)
        {
            maxSortOrder++;            
            int emptyTabsToAdd = 2;

            if (renderedPanels == 0)
                emptyTabsToAdd = 6;
            else if (renderedPanels < 4) // Get the difference between the full tabs and minimum of 4
                emptyTabsToAdd = 2 + Math.Abs(renderedPanels - 4);

            for (int i = 0; i < emptyTabsToAdd; i++)
            {
                var tabPanel = this.LoadControl<PointOfInterestTabPanel>("~/Generic/Subpanels/PointOfInterestPanels/PointOfInterestTabPanel.ascx");
                tabPanel.Sites = sites;
                // Set the right UI Mode ids
                if (this.DataSourceAsPointOfInterestEntity.TabletUIModeEntity != null)
                    tabPanel.TabletUIModeId = this.DataSourceAsPointOfInterestEntity.TabletUIModeEntity.UIModeId;
                if (this.DataSourceAsPointOfInterestEntity.MobileUIModeEntity != null)
                    tabPanel.MobileUIModeId = this.DataSourceAsPointOfInterestEntity.MobileUIModeEntity.UIModeId;

                // Set Sort Orders
                tabPanel.SortOrder = maxSortOrder;
                maxSortOrder++;

                this.plhTabs.Controls.Add(tabPanel);
            }
        }

        private List<string> GetUniqueCaptions(List<UITabEntity> tabs)
        {
            List<string> ret = new List<string>();
            foreach (var tab in tabs)
            {
                if (!ret.Contains(tab.Caption))
                    ret.Add(tab.Caption);
            }
            return ret;
        }

        private void LoadUserControls()
        {
            this.translationsPanel = this.tabsMain.AddTabPage("Translations", "CustomTextCollection", "~/Generic/UserControls/CustomTextCollection.ascx") as Generic.UserControls.CustomTextCollection;
            this.translationsPanel.FullWidth = true;            

            this.tabsMain.AddTabPage("Afbeeldingen", "Media", "~/Generic/UserControls/MediaCollection.ascx");
            this.tabsMain.AddTabPage("Bedrijfsuren", "Businesshours", "~/Generic/SubPanels/BusinesshourPanel.ascx");

            IEnumerable<Obymobi.TimeZone> timezones = Obymobi.TimeZone.Mappings.Values.OrderByDescending(x => x.UtcOffset);

            this.ddlTimeZoneOlsonId.DataSource = timezones;
            this.ddlTimeZoneOlsonId.DataBind();
        }

        private PointOfInterestUITab CreatePointOfInterestUITab(string cultureCode, UITabEntity uiTabTabletDevice, UITabEntity uiTabMobileDevice)
        {
            PointOfInterestUITab tabPanel = this.LoadControl<PointOfInterestUITab>("~/Generic/SubPanels/PointOfInterestPanels/PointOfInterestUITab.ascx");
            tabPanel.CultureCode = cultureCode;
            tabPanel.CraveUITabEntity = uiTabTabletDevice;
            tabPanel.MobileUITabEntity = uiTabMobileDevice;                            
            tabPanel.Initialize();
            return tabPanel;
        }

        #endregion

		#region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.DataSourceAsPointOfInterestEntity.IsNew && this.DataSourceAsPointOfInterestEntity.ExternalId.HasValue)
            {
                this.AddInformatorInfo(this.Translate("poi-is-imported", "This point-of-interest is imported from an external feed, therefore you cannot edit the fields by default. Click on the name of a field to overwrite the value."));
                this.Validate();
            }

            this.SetGui();
            this.HookupEvents();
		}   

        private void SetGui()
        {
            this.tbLatitude.ReadOnly = !this.DataSourceAsPointOfInterestEntity.LatitudeOverridden;
            this.tbLongitude.ReadOnly = !this.DataSourceAsPointOfInterestEntity.LongitudeOverridden;

            double? latitude = this.DataSourceAsPointOfInterestEntity.Latitude;
            double? longitude = this.DataSourceAsPointOfInterestEntity.Longitude;

            if (!latitude.HasValue || !longitude.HasValue)
                this.LookupMapCoordinates(out latitude, out longitude, this.DataSourceAsPointOfInterestEntity);
            
            if (this.DataSourceAsPointOfInterestEntity.LatitudeOverridden && this.DataSourceAsPointOfInterestEntity.LongitudeOverridden)
                this.plhMap.AddHtml("<a href=\"LocationPicker.aspx?lat={0}&lon={1}&latInput={2}&lonInput={3}\" rel=\"lightbox[500 400]\" title=\"Pick a location::Drag the marker to change the location\">View map</a>", latitude ?? 51.50703296721856, longitude ?? -0.127716064453125, this.tbLatitude.ClientID, this.tbLongitude.ClientID);
            else
                this.plhMap.AddHtml("<a href=\"LocationPicker.aspx?lat={0}&lon={1}&latInput={2}&lonInput={3}&viewOnly=true\" rel=\"lightbox[500 400]\">View map</a>", latitude ?? 51.50703296721856, longitude ?? -0.127716064453125, this.tbLatitude.ClientID, this.tbLongitude.ClientID);

            this.lblName.Visible = !this.DataSourceAsPointOfInterestEntity.ExternalId.HasValue;
            this.lblZipcode.Visible = !this.DataSourceAsPointOfInterestEntity.ExternalId.HasValue;
            this.lblCity.Visible = !this.DataSourceAsPointOfInterestEntity.ExternalId.HasValue;
            this.lblTelephone.Visible = !this.DataSourceAsPointOfInterestEntity.ExternalId.HasValue;
            this.lblAddressline1.Visible = !this.DataSourceAsPointOfInterestEntity.ExternalId.HasValue;
            this.lblAddressline2.Visible = !this.DataSourceAsPointOfInterestEntity.ExternalId.HasValue;
            this.lblAddressline3.Visible = !this.DataSourceAsPointOfInterestEntity.ExternalId.HasValue;
            this.lblCountryId.Visible = !this.DataSourceAsPointOfInterestEntity.ExternalId.HasValue;
            this.lblCurrencyId.Visible = !this.DataSourceAsPointOfInterestEntity.ExternalId.HasValue;
            this.lblFax.Visible = !this.DataSourceAsPointOfInterestEntity.ExternalId.HasValue;
            this.lblWebsite.Visible = !this.DataSourceAsPointOfInterestEntity.ExternalId.HasValue;
            this.lblEmail.Visible = !this.DataSourceAsPointOfInterestEntity.ExternalId.HasValue;
            this.lblDescriptionSingleLine.Visible = !this.DataSourceAsPointOfInterestEntity.ExternalId.HasValue;
            this.lblDescription.Visible = !this.DataSourceAsPointOfInterestEntity.ExternalId.HasValue;
            this.lblActionButtonId.Visible = !this.DataSourceAsPointOfInterestEntity.ExternalId.HasValue;
            this.lblActionButtonUrlTablet.Visible = !this.DataSourceAsPointOfInterestEntity.ExternalId.HasValue;
            this.lblActionButtonUrlMobile.Visible = !this.DataSourceAsPointOfInterestEntity.ExternalId.HasValue;
            this.lblCostIndicationType.Visible = !this.DataSourceAsPointOfInterestEntity.ExternalId.HasValue;
            this.lblCostIndicationValue.Visible = !this.DataSourceAsPointOfInterestEntity.ExternalId.HasValue;

            this.lbName.Visible = this.DataSourceAsPointOfInterestEntity.ExternalId.HasValue;
            this.lbZipcode.Visible = this.DataSourceAsPointOfInterestEntity.ExternalId.HasValue;
            this.lbCity.Visible = this.DataSourceAsPointOfInterestEntity.ExternalId.HasValue;
            this.lbTelephone.Visible = this.DataSourceAsPointOfInterestEntity.ExternalId.HasValue;
            this.lbAddressline1.Visible = this.DataSourceAsPointOfInterestEntity.ExternalId.HasValue;
            this.lbAddressline2.Visible = this.DataSourceAsPointOfInterestEntity.ExternalId.HasValue;
            this.lbAddressline3.Visible = this.DataSourceAsPointOfInterestEntity.ExternalId.HasValue;
            this.lbCountryId.Visible = this.DataSourceAsPointOfInterestEntity.ExternalId.HasValue;
            this.lbCurrencyId.Visible = this.DataSourceAsPointOfInterestEntity.ExternalId.HasValue;
            this.lbFax.Visible = this.DataSourceAsPointOfInterestEntity.ExternalId.HasValue;
            this.lbWebsite.Visible = this.DataSourceAsPointOfInterestEntity.ExternalId.HasValue;
            this.lbEmail.Visible = this.DataSourceAsPointOfInterestEntity.ExternalId.HasValue;
            this.lbDescriptionSingleLine.Visible = this.DataSourceAsPointOfInterestEntity.ExternalId.HasValue;
            this.lbDescription.Visible = this.DataSourceAsPointOfInterestEntity.ExternalId.HasValue;
            this.lbActionButtonId.Visible = this.DataSourceAsPointOfInterestEntity.ExternalId.HasValue;
            this.lbActionButtonUrlTablet.Visible = this.DataSourceAsPointOfInterestEntity.ExternalId.HasValue;
            this.lbActionButtonUrlMobile.Visible = this.DataSourceAsPointOfInterestEntity.ExternalId.HasValue;
            this.lbCostIndicationType.Visible = this.DataSourceAsPointOfInterestEntity.ExternalId.HasValue;
            this.lbCostIndicationValue.Visible = this.DataSourceAsPointOfInterestEntity.ExternalId.HasValue;
        }
     
        private void HookupEvents()
        {
            this.lbName.Click += lbName_Click;
            this.lbZipcode.Click += lbZipcode_Click;
            this.lbCity.Click += lbCity_Click;
            this.lbTelephone.Click += lbTelephone_Click;
            this.lbAddressline1.Click += lbAddressline1_Click;
            this.lbAddressline2.Click += lbAddressline2_Click;
            this.lbAddressline3.Click += lbAddressline3_Click;
            this.lbLatitudeLongitude.Click += lbLatitudeLongitude_Click;
            this.lbCountryId.Click += lbCountryId_Click;
            this.lbCurrencyId.Click += lbCurrencyId_Click;
            this.lbFax.Click += lbFax_Click;
            this.lbWebsite.Click += lbWebsite_Click;
            this.lbEmail.Click += lbEmail_Click;
            this.lbDescriptionSingleLine.Click += lbDescriptionSingleLine_Click;
            this.lbDescription.Click += lbDescription_Click;
            this.lbActionButtonId.Click += lbActionButtonId_Click;
            this.lbActionButtonUrlTablet.Click += lbActionButtonUrlTablet_Click;
            this.lbActionButtonUrlMobile.Click += lbActionButtonUrlMobile_Click;
            this.lbCostIndicationType.Click += lbCostIndicationType_Click;
            this.lbCostIndicationValue.Click += lbCostIndicationValue_Click;
        }

        private void LookupMapCoordinates(out double? lat, out double? lng, PointOfInterestEntity pointOfInterestEntity)
        {
            lat = null;
            lng = null;

            string addressLine1 = (pointOfInterestEntity == null ? this.tbAddressline1.Text : pointOfInterestEntity.Addressline1);
            string addressLine2 = (pointOfInterestEntity == null ? this.tbAddressline2.Text : pointOfInterestEntity.Addressline2);
            string addressLine3 = (pointOfInterestEntity == null ? this.tbAddressline3.Text : pointOfInterestEntity.Addressline3);
            string zipcode = (pointOfInterestEntity == null ? this.tbZipcode.Text : pointOfInterestEntity.Zipcode);
            string city = (pointOfInterestEntity == null ? this.tbCity.Text : pointOfInterestEntity.City);
            string country = (pointOfInterestEntity == null ? this.ddlCountryId.Text : pointOfInterestEntity.CountryEntity.Name);

            var address = "";
            if (addressLine1.Length > 0)
                address += addressLine1 + "+";
            if (addressLine2.Length > 0)
                address += addressLine2 + "+";
            if (addressLine3.Length > 0)
                address += addressLine3 + "+";

            if (zipcode.Length > 0)
                address += zipcode + "+";

            // Only add city/country and lookup when we have an address
            if (address.Length > 0)
            {
                if (city.Length > 0)
                    address += city + "+";
                if (country.Length > 0)
                    address += country;

                // Lookup address
                var response = GeoCoder.LookupAddress(address, false);
                if (response != null)
                {
                    if (response.Status.Equals("OK") && response.Results.Length >= 1)
                    {
                        var data = response.Results[0];
                        lat = (double?)data.Geometry.Location.Lat;
                        lng = (double?)data.Geometry.Location.Lng;
                    }
                }
            }
        }

        private void SaveMapCoordinates()
        {
            if (this.DataSourceAsPointOfInterestEntity.LatitudeOverridden && this.DataSourceAsPointOfInterestEntity.LongitudeOverridden)
                return;

            double? lat = null, lng = null;

            this.LookupMapCoordinates(out lat, out lng, null);

            PointOfInterestEntity entity = this.DataSourceAsPointOfInterestEntity;
            if (!entity.LatitudeOverridden)
                entity.Latitude = lat;
            if (!entity.LongitudeOverridden)
                entity.Longitude = lng;
            entity.Save();
        }
        
        public override bool Save()
        {
            this.DataSourceAsPointOfInterestEntity.TimeZoneOlsonId = this.ddlTimeZoneOlsonId.SelectedValueString;

            if (base.Save())
            {
                CustomTextHelper.UpdateCustomTexts(this.DataSource);
                this.SaveMapCoordinates();
            }            

            return true;
        }

        private void lbName_Click(object sender, EventArgs e)
        {
            this.DataSourceAsPointOfInterestEntity.NameOverridden = !this.DataSourceAsPointOfInterestEntity.NameOverridden;
            this.Save();
        }

        private void lbZipcode_Click(object sender, EventArgs e)
        {
            this.DataSourceAsPointOfInterestEntity.ZipcodeOverridden = !this.DataSourceAsPointOfInterestEntity.ZipcodeOverridden;
            this.Save();
        }

        private void lbCity_Click(object sender, EventArgs e)
        {
            this.DataSourceAsPointOfInterestEntity.CityOverridden = !this.DataSourceAsPointOfInterestEntity.CityOverridden;
            this.Save();
        }

        private void lbTelephone_Click(object sender, EventArgs e)
        {
            this.DataSourceAsPointOfInterestEntity.TelephoneOverridden = !this.DataSourceAsPointOfInterestEntity.TelephoneOverridden;
            this.Save();
        }

        private void lbAddressline1_Click(object sender, EventArgs e)
        {
            this.DataSourceAsPointOfInterestEntity.Addressline1Overridden = !this.DataSourceAsPointOfInterestEntity.Addressline1Overridden;
            this.Save();
        }

        private void lbAddressline2_Click(object sender, EventArgs e)
        {
            this.DataSourceAsPointOfInterestEntity.Addressline2Overridden = !this.DataSourceAsPointOfInterestEntity.Addressline2Overridden;
            this.Save();
        }

        private void lbAddressline3_Click(object sender, EventArgs e)
        {
            this.DataSourceAsPointOfInterestEntity.Addressline3Overridden = !this.DataSourceAsPointOfInterestEntity.Addressline3Overridden;
            this.Save();
        }

        private void lbLatitudeLongitude_Click(object sender, EventArgs e)
        {
            this.DataSourceAsPointOfInterestEntity.LatitudeOverridden = !this.DataSourceAsPointOfInterestEntity.LatitudeOverridden;
            this.DataSourceAsPointOfInterestEntity.LongitudeOverridden = !this.DataSourceAsPointOfInterestEntity.LongitudeOverridden;
            this.Save();
        }

        private void lbCountryId_Click(object sender, EventArgs e)
        {
            this.DataSourceAsPointOfInterestEntity.CountryIdOverridden = !this.DataSourceAsPointOfInterestEntity.CountryIdOverridden;
            this.Save();
        }

        private void lbCurrencyId_Click(object sender, EventArgs e)
        {
            this.DataSourceAsPointOfInterestEntity.CurrencyIdOverridden = !this.DataSourceAsPointOfInterestEntity.CurrencyIdOverridden;
            this.Save();
        }

        private void lbFax_Click(object sender, EventArgs e)
        {
            this.DataSourceAsPointOfInterestEntity.FaxOverridden = !this.DataSourceAsPointOfInterestEntity.FaxOverridden;
            this.Save();
        }

        private void lbWebsite_Click(object sender, EventArgs e)
        {
            this.DataSourceAsPointOfInterestEntity.WebsiteOverridden = !this.DataSourceAsPointOfInterestEntity.WebsiteOverridden;
            this.Save();
        }

        private void lbEmail_Click(object sender, EventArgs e)
        {
            this.DataSourceAsPointOfInterestEntity.EmailOverridden = !this.DataSourceAsPointOfInterestEntity.EmailOverridden;
            this.Save();
        }

        private void lbDescriptionSingleLine_Click(object sender, EventArgs e)
        {
            this.DataSourceAsPointOfInterestEntity.DescriptionSingleLineOverridden = !this.DataSourceAsPointOfInterestEntity.DescriptionSingleLineOverridden;
            this.Save();
        }

        private void lbDescription_Click(object sender, EventArgs e)
        {
            this.DataSourceAsPointOfInterestEntity.DescriptionOverridden = !this.DataSourceAsPointOfInterestEntity.DescriptionOverridden;
            this.Save();
        }

        private void lbActionButtonId_Click(object sender, EventArgs e)
        {
            this.DataSourceAsPointOfInterestEntity.ActionButtonIdOverridden = !this.DataSourceAsPointOfInterestEntity.ActionButtonIdOverridden;
            this.Save();
        }

        private void lbActionButtonUrlTablet_Click(object sender, EventArgs e)
        {
            this.DataSourceAsPointOfInterestEntity.ActionButtonUrlTabletOverridden = !this.DataSourceAsPointOfInterestEntity.ActionButtonUrlTabletOverridden;
            this.Save();
        }

        private void lbActionButtonUrlMobile_Click(object sender, EventArgs e)
        {
            this.DataSourceAsPointOfInterestEntity.ActionButtonUrlMobileOverridden = !this.DataSourceAsPointOfInterestEntity.ActionButtonUrlMobileOverridden;
            this.Save();
        }

        private void lbCostIndicationType_Click(object sender, EventArgs e)
        {
            this.DataSourceAsPointOfInterestEntity.CostIndicationTypeOverridden = !this.DataSourceAsPointOfInterestEntity.CostIndicationTypeOverridden;
            this.Save();
        }

        private void lbCostIndicationValue_Click(object sender, EventArgs e)
        {
            this.DataSourceAsPointOfInterestEntity.CostIndicationValueOverridden = !this.DataSourceAsPointOfInterestEntity.CostIndicationValueOverridden;
            this.Save();
        }

		#endregion

		#region Properties

        public PointOfInterestEntity DataSourceAsPointOfInterestEntity
        {
            get
            {
                return this.DataSource as PointOfInterestEntity;
            }
        }

		#endregion
    }
}
