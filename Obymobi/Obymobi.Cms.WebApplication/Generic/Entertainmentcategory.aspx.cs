﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Web.UI.WebControls;
using Obymobi.Enums;
using Obymobi.ObymobiCms.UI;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.ObymobiCms.Generic
{
    public partial class Entertainmentcategory : PageLLBLGenEntityCms
    {
		#region Methods

		protected override void OnInit(EventArgs e)
		{
            this.tabsMain.AddTabPage("Translations", "CustomTextCollection", "~/Generic/UserControls/CustomTextCollection.ascx");
            base.OnInit(e);
            this.tbUrl.Text = string.Format("crave://entertainmentcategory/{0}", this.DataSourceAsEntertainmentcategoryEntity.EntertainmentcategoryId);
		}

        public override bool Save()
        {
            if (base.Save())
            {
                CustomTextHelper.UpdateCustomTexts(this.DataSource);
                return true;
            }
            else
                return false;
        }

		#endregion

		#region Properties

        public EntertainmentcategoryEntity DataSourceAsEntertainmentcategoryEntity
        {
            get
            {
                return this.DataSource as EntertainmentcategoryEntity;
            }
        }

		#endregion
    }
}
