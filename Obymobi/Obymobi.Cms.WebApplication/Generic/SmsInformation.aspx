﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Generic.SmsInformation" Codebehind="SmsInformation.aspx.cs" %>
<%@ Reference VirtualPath="~/Generic/SubPanels/Sms/SmsKeywordPanel.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
<X:PageControl Id="tabsMain" runat="server" Width="100%">   
<TabPages>
<X:TabPage Text="Generic" Name="Generic">
	<Controls>    
    <D:Panel ID="pnlSmsInformation" runat="server" GroupingText="SMS Information">
        <table class="dataformV2">
            <tr>
		        <td class="label">
			        <D:LabelEntityFieldInfo runat="server" id="lblName">Name</D:LabelEntityFieldInfo>
		        </td>
		        <td class="control">
			        <D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
		        </td>
                <td class="label"></td>
                <td class="control"></td>
	        </tr>	
	        <tr>
		        <td class="label">
			        <D:LabelEntityFieldInfo runat="server" id="lblOriginator">SMS Afzender</D:LabelEntityFieldInfo>
		        </td>
		        <td class="control">
			        <D:TextBoxString ID="tbOriginator" runat="server" IsRequired="true"></D:TextBoxString><br />
                    <D:Label ID="lblOriginatorMaxCharacters" runat="server" LocalizeText="True" CssClass="small">Maximaal 9 tekens</D:Label>
		        </td>
                <td class="label"></td>
                <td class="control"></td>
	        </tr>						
            <tr>
                <td class="label">
                    <D:LabelEntityFieldInfo runat="server" id="lblGooglePlayPackage">Google Play Package</D:LabelEntityFieldInfo>
                </td>
                <td class="control">
                    <D:TextBoxString ID="tbGooglePlayPackage" runat="server" IsRequired="true"></D:TextBoxString>
                </td>
                <td class="label">
                    <D:LabelEntityFieldInfo runat="server" id="lblAppStoreAppIp">AppStore ID</D:LabelEntityFieldInfo>
                </td>
		        <td class="control">
		            <D:TextBoxString ID="tbAppStoreAppId" runat="server" IsRequired="true"></D:TextBoxString>
		        </td>	  
            </tr>
            <tr>
                <td class="label">
                    <D:LabelEntityFieldInfo runat="server" id="lblBodyText">Antwoord Tekst</D:LabelEntityFieldInfo>
                </td>
                <td class="control" colspan="3">
                    <D:TextBoxString ID="tbBodyText" runat="server" MaxLength="130" IsRequired="true" onkeyup="LimitCharacters();" Width="100%"></D:TextBoxString><br />
                    <D:Label ID="lblCount" runat="server" LocalizeText="False" CssClass="small">0 characters remaining</D:Label>
                </td>
            </tr>
            <tr>
                <td class="label">
                    <D:LabelEntityFieldInfo runat="server" id="lblIsDefault">Standaard</D:LabelEntityFieldInfo>
                </td>
                <td class="control">
                    <D:CheckBox runat="server" ID="cbIsDefault" OnClick="ConfirmMakeDefault();"/>
                </td>
            </tr>
        </table>
    </D:Panel>
    
    <D:Panel ID="pnlSmsKeywords" runat="server" GroupingText="Sleutelwoorden">
    
    </D:Panel>
    </Controls>
</X:TabPage>
</TabPages>    
</X:PageControl>
</div>
<style>
    .small {
        font-size: x-small;
    }
</style>
<script type="text/javascript">
    var cbIsDefault = document.getElementById('<%=cbIsDefault.ClientID %>');
    var tbText = document.getElementById('<%=tbBodyText.ClientID %>');
    tbText.maxLength = 130;
    
    var countLabel = document.getElementById('<%=lblCount.ClientID %>');

    function ConfirmMakeDefault() {
        var result = confirm("<%=AlertMessageDefault%>");
        if (result == false) {
            cbIsDefault.checked = false;
        }
    }

    function LimitCharacters() {
        var chars = tbText.value.length;
        var labelString = countLabel.innerHTML.split(":");

        countLabel.innerHTML = labelString[0] + ": " + (tbText.maxLength - chars);
    }
</script> 
</asp:Content>