﻿using System;

namespace Obymobi.ObymobiCms.Generic
{
    public partial class InfraredConfigurations : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "InfraredConfiguration";
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #endregion
    }
}
