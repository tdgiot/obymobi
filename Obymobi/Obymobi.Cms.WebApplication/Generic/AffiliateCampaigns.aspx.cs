﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Dionysos.Web;

namespace Obymobi.ObymobiCms.Generic
{
	public partial class AffiliateCampaigns : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "AffiliateCampaign";
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        #endregion
    }
}
