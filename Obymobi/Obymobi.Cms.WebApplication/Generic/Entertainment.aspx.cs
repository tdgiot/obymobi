﻿using System;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.ObymobiCms.UI;
using Obymobi.Logic.Cms;
using Dionysos.Data.LLBLGen;
using Obymobi.Logic.HelperClasses;
using Dionysos;
using Obymobi.Enums;

namespace Obymobi.ObymobiCms.Generic
{
    public partial class Entertainment : PageLLBLGenEntityCms
    {
		#region Methods

        private void SetGui()
        {
            if (this.DataSourceAsEntertainmentEntity.IsNew)
                this.DataSourceAsEntertainmentEntity.Visible = true;

            this.LoadSubPanels();
        }        

        private void LoadSubPanels()
        {
            if (this.DataSourceAsEntertainmentEntity.EntertainmentType == Obymobi.Enums.EntertainmentType.Android)
            {
                var androidPanel = this.LoadControl("~/Generic/SubPanels/EntertainmentPanels/AndroidPanel.ascx");
                this.plhSettingsPanel.Controls.Add(androidPanel);
            }
            else if (this.DataSourceAsEntertainmentEntity.EntertainmentType == Obymobi.Enums.EntertainmentType.Web || 
                    this.DataSourceAsEntertainmentEntity.EntertainmentType == Obymobi.Enums.EntertainmentType.Browser ||
                    this.DataSourceAsEntertainmentEntity.EntertainmentType == Obymobi.Enums.EntertainmentType.RestrictedBrowser ||
                    this.DataSourceAsEntertainmentEntity.EntertainmentType == Obymobi.Enums.EntertainmentType.Cms ||
                    this.DataSourceAsEntertainmentEntity.EntertainmentType == Obymobi.Enums.EntertainmentType.IpadBrowser)
            {
                var webPanel = this.LoadControl("~/Generic/SubPanels/EntertainmentPanels/WebPanel.ascx");
                this.plhSettingsPanel.Controls.Add(webPanel);
            }
            else if (this.DataSourceAsEntertainmentEntity.EntertainmentType == Obymobi.Enums.EntertainmentType.CustomBrowser)
            {
                var webPanel = this.LoadControl("~/Generic/SubPanels/EntertainmentPanels/WebPanel.ascx");
                var customBrowserPanel = this.LoadControl("~/Generic/SubPanels/EntertainmentPanels/CustomBrowserPanel.ascx");

                this.plhSettingsPanel.Controls.Add(webPanel);
                this.plhSettingsPanel.Controls.Add(customBrowserPanel);
            }
            else if (this.DataSourceAsEntertainmentEntity.EntertainmentType == Obymobi.Enums.EntertainmentType.Survey)
            {
                var surveyPanel = this.LoadControl("~/Generic/SubPanels/EntertainmentPanels/SurveyPanel.ascx");
                this.plhSettingsPanel.Controls.Add(surveyPanel);
            }
            else if (this.DataSourceAsEntertainmentEntity.EntertainmentType == Obymobi.Enums.EntertainmentType.Site)
            {
                var sitePanel = this.LoadControl("~/Generic/SubPanels/EntertainmentPanels/SitePanel.ascx");
                this.plhSettingsPanel.Controls.Add(sitePanel);
            }
        }

        private void LoadUserControls()
        {
            CompanyCollection companies = new CompanyCollection();

            if (CmsSessionHelper.CurrentCompanyId > 0)
                companies.GetMulti(CompanyFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            else
                companies.GetMulti(null);

            if (CmsSessionHelper.CurrentRole < Role.Administrator)
            {
                this.ddlCompanyId.DisplayEmptyItem = false;
                this.ddlCompanyId.SelectedIndex = 0;
                this.ddlCompanyId.Enabled = false;   
            }

            this.ddlCompanyId.DataSource = companies;
            this.ddlCompanyId.DataBind();

            // Add the tab pages
            this.tabsMain.AddTabPage("Afbeeldingen", "Media", "~/Generic/UserControls/MediaCollection.ascx");
            this.tabsMain.AddTabPage("Urls", "EntertainmenturlCollection", "~/Generic/EntertainmenturlCollection.ascx");
            this.tabsMain.AddTabPage("CDN", "CloudProcessingTaskCollection", "~/Generic/UserControls/CloudTaskCollection.ascx");
        }

        private void CopyEntertainment()
        {
            EntertainmentEntity source = this.DataSourceAsEntertainmentEntity;

            var destination = new EntertainmentEntity();
            LLBLGenEntityUtil.CopyFields(source, destination);
            destination.Name = source.Name + " (copy)";

            if (this.cblCopyToCompanyId.Value == null)
                destination.CompanyId = CmsSessionHelper.CurrentCompanyId;
            else
                destination.CompanyId = this.cblCopyToCompanyId.Value;

            if (destination.Save())
            {
                // AdvertisementTagEntertainment
                foreach (var item in source.AdvertisementTagEntertainmentCollection)
                {
                    var ate = new AdvertisementTagEntertainmentEntity();
                    ate.EntertainmentId = destination.EntertainmentId;
                    ate.AdvertisementTagId = item.AdvertisementTagId;
                    ate.Save();
                }

                // Media
                foreach (var item in source.MediaCollection)
                {
                    var media = new MediaEntity();
                    LLBLGenEntityUtil.CopyFields(item, media);
                    media.EntertainmentId = destination.EntertainmentId;

                    if (media.Extension.IsNullOrWhiteSpace() || media.MimeType.IsNullOrWhiteSpace())
                    {
                        media.SetMimeTypeAndExtension(media.FilePathRelativeToMediaPath);
                    }                        

                    if (media.Save())
                    {
                        // MediaRatioTypeMedia
                        foreach (var temp in item.MediaRatioTypeMediaCollection)
                        {
                            var mrtm = new MediaRatioTypeMediaEntity();
                            LLBLGenEntityUtil.CopyFields(temp, mrtm);
                            mrtm.MediaId = media.MediaId;
                            mrtm.Save();
                        }
                    }
                }

                int? defaultEntertainmenturlId = null;

                // Entertainmenturl
                foreach (var item in source.EntertainmenturlCollection)
                {
                    var url = new EntertainmenturlEntity();
                    LLBLGenEntityUtil.CopyFields(item, url);
                    url.EntertainmentId = destination.EntertainmentId;
                    url.Save();

                    if (source.DefaultEntertainmenturlId.HasValue && item.EntertainmenturlId == source.DefaultEntertainmenturlId.Value)
                        defaultEntertainmenturlId = url.EntertainmenturlId;
                }

                if (defaultEntertainmenturlId.HasValue)
                {
                    destination.DefaultEntertainmenturlId = defaultEntertainmenturlId;
                    destination.Save();
                }

                if (source.EntertainmentFileId.HasValue)
                {
                    var entertainmentFile = new EntertainmentFileEntity();
                    LLBLGenEntityUtil.CopyFields(source.EntertainmentFileEntity, entertainmentFile);

                    var filenameSplit = entertainmentFile.Filename.Split('-');
                    entertainmentFile.Filename = string.Format("{0}-{1}", destination.EntertainmentId, filenameSplit[1]);

                    entertainmentFile.Save();

                    // Push entertainment to cloud
                    CloudTaskHelper.UploadEntertainmentFile(destination.EntertainmentId, entertainmentFile);

                    destination.EntertainmentFileId = entertainmentFile.EntertainmentFileId;
                    destination.Save();
                }

                // Redirect to new entertainment item
                if (destination.CompanyId == CmsSessionHelper.CurrentCompanyId)
                    Response.Redirect(ResolveUrl(string.Format("~/Generic/Entertainment.aspx?id={0}", destination.EntertainmentId)));
            }
        }

		#endregion

		#region Event Handlers
        
        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            this.DataSourceLoaded += Entertainment_DataSourceLoaded;
            base.OnInit(e);
        }

        private void Entertainment_DataSourceLoaded(object sender)
        {
            this.SetGui();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                // Bind the company combobox
                CompanyCollection companies = new CompanyCollection();

                PredicateExpression filter = null;
                if (CmsSessionHelper.CurrentRole < Role.Administrator)
                {
                    filter = new PredicateExpression(CompanyFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
                }

                companies.GetMulti(filter, 0, new SortExpression(new SortClause(CompanyFields.Name, SortOperator.Ascending)));
                this.cblCopyToCompanyId.DataSource = companies;
                this.cblCopyToCompanyId.DataBind();
            }

            // Set the URL
            this.tbUrl.Text = string.Format("crave://entertainment/{0}", this.DataSourceAsEntertainmentEntity.EntertainmentId);

            if (CmsSessionHelper.CurrentRole < Role.Administrator)
            {
                this.pnlEntertainmentCopy.Visible = false;
            }

            if (this.PageMode == Dionysos.Web.PageMode.Add)
                this.pnlEntertainmentCopy.Visible = false;

            this.btnCopy.Click += btnCopy_Click;
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            this.CopyEntertainment();
        }

        public bool SaveData()
        {
            if (this.DataSourceAsEntertainmentEntity.Fields["CompanyId"].IsChanged && this.DataSourceAsEntertainmentEntity.CompanyId.HasValue)
            {
                PredicateExpression filter = new PredicateExpression();
                filter.Add(DeliverypointgroupEntertainmentFields.EntertainmentId == this.DataSourceAsEntertainmentEntity.EntertainmentId);
                filter.Add(DeliverypointgroupFields.CompanyId != this.DataSourceAsEntertainmentEntity.CompanyId.Value);

                RelationCollection relations = new RelationCollection();
                relations.Add(DeliverypointgroupEntertainmentEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId);
                
                DeliverypointgroupEntertainmentCollection deliverypointgroupEntertainments = new DeliverypointgroupEntertainmentCollection();
                deliverypointgroupEntertainments.GetMulti(filter, relations);

                if (deliverypointgroupEntertainments.Count > 0)
                {
                    deliverypointgroupEntertainments.DeleteMulti();
                }
            }

            return true;
        }

        public override bool Save()
        {
            SaveData();
            return base.Save();
        }

        public override bool SaveAndGo()
        {
            SaveData();
            return base.SaveAndGo();
        }

        public override bool SaveAndNew()
        {
            SaveData();
            return base.SaveAndNew();
        }

		#endregion

		#region Properties

        public EntertainmentEntity DataSourceAsEntertainmentEntity
        {
            get
            {
                return this.DataSource as EntertainmentEntity;
            }
        }

		#endregion
    }
}
