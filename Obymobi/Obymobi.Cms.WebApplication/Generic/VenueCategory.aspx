﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Generic.VenueCategory" Codebehind="VenueCategory.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblName">Naam</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
							</td>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblMarkerIcon">Marker icon</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <X:ComboBoxEnum ID="cbMarkerIcon" runat="server" Type="Obymobi.Enums.MarkerIcon, Obymobi" ></X:ComboBoxEnum>
                            </td>
						</tr>
                    </table>
				</controls>
			</X:TabPage>
        </TabPages>
	</X:PageControl>
</div>
</asp:Content>