﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Generic.PublishingItem" Codebehind="PublishingItem.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
                    <D:PlaceHolder runat="server">
					    <table class="dataformV2">
						    <tr>
                                <td class="label">
                                    <D:Label runat="server" id="lblStatus">Status</D:Label>
							    </td>
							    <td class="control">
                                    <D:Label runat="server" id="lblStatusValue" LocalizeText="false"></D:Label>
							    </td>
							    <td class="label">
							    </td>
							    <td class="control">
							    </td>
						    </tr>
						    <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblLog">Log</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control" colspan="3">
                                    <D:TextBoxMultiLine ID="tbLog" runat="server" ReadOnly="true" />
							    </td>
						    </tr>
					     </table>			
                    </D:PlaceHolder>
				</Controls>
			</X:TabPage>						
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>

