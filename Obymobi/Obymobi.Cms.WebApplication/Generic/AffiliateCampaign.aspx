﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Generic.AffiliateCampaign" CodeBehind="AffiliateCampaign.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" runat="Server">
    <div>
        <X:PageControl ID="tabsMain" runat="server" Width="100%">
            <TabPages>
                <X:TabPage Text="Algemeen" Name="Generic">
                    <controls>
                        <table class="dataformV2">
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" ID="lblName">Name</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
                                </td>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" ID="lblCode" LocalizeText="False">Code</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:TextBoxString ID="tbCode" runat="server" IsRequired="true"></D:TextBoxString>
                                </td>
                            </tr>
                        </table>

                        <D:Panel ID="pnlPartnerCodes" GroupingText="Affiliate Partner Codes" runat="server">
                            <table class="dataformV2">
                                <D:PlaceHolder ID="plhPartnerCodes" runat="server"></D:PlaceHolder> 
                            </table>
                        </D:Panel>
                    </controls>
                </X:TabPage>
            </TabPages>
        </X:PageControl>
    </div>
</asp:Content>
