﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Generic.Entertainment" Codebehind="Entertainment.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
                    <D:Panel ID="pnlEntertainmentDetails" runat="server" GroupingText="Entertainment gegevens">
					    <table class="dataformV2">
						    <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblName">Name</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
							    </td>
                                <td class="label">
								    <D:Label runat="server" id="lblEntertainmentType">Type</D:Label>
							    </td>
							    <td class="control">
                                    <X:ComboBoxEnum ID="cbEntertainmentType" runat="server" IsRequired="true" Type="Obymobi.Enums.EntertainmentType, Obymobi" ></X:ComboBoxEnum>
							    </td>	  
						    </tr>						
						    <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblTextColor">Tekstkleur</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <D:TextBoxString ID="tbTextColor" runat="server"></D:TextBoxString> 
							    </td>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblBackgroundColor">Achtergrondkleur</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <D:TextBoxString ID="tbBackgroundColor" runat="server"></D:TextBoxString>
							    </td>
						    </tr>
                            <tr>
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblEntertainmentcategoryId">Category</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlEntertainmentcategoryId" UseDataBinding="true" EntityName="Entertainmentcategory" ValueField="EntertainmentcategoryId" TextField="Name" IsRequired="false"></X:ComboBoxLLBLGenEntityCollection>  
							    </td> 
                                <td class="label">
								    <D:Label runat="server" id="lblCompany">Bedrijf</D:Label>
							    </td>
							    <td class="control">
								    <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlCompanyId" UseDataBinding="true" EntityName="Company" IsRequired="false" PreventEntityCollectionInitialization="true"></X:ComboBoxLLBLGenEntityCollection>            
							    </td>
						    </tr>			
                            <tr>
							    <td class="label">
								    <D:Label runat="server" id="lblDescription">Beschrijving</D:Label>
							    </td>
							    <td colspan="3" class="control">
								    <D:TextBox ID="tbDescription" runat="server" Rows="5" TextMode="MultiLine" CssClass="inputNoHeight"></D:TextBox>                
							    </td>
						    </tr>		
                            <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblVisible">Tonen</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <D:CheckBox runat="server" id="cbVisible" />
							    </td> 
                                <td class="label">
                                    <D:Label runat="server" ID="lblUrl">URL</D:Label>
							    </td>
							    <td class="control">
                                    <D:TextBoxString ID="tbUrl" runat="server" UseDataBinding="false" ReadOnly="true"></D:TextBoxString>
							    </td>
						    </tr>		
                             <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblAnnouncementAction">Announcement action</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <D:CheckBox runat="server" id="cbAnnouncementAction" />
							    </td> 
						    </tr>	
					     </table>
                     </D:Panel>		
                     <D:Panel ID="pnlEntertainmentSettings" runat="server" GroupingText="Entertainment instellingen">
                        <D:PlaceHolder ID="plhSettingsPanel" runat="server"></D:PlaceHolder>			
                     </D:Panel>
                    <D:Panel ID="pnlEntertainmentCopy" runat="server" GroupingText="Entertainment kopieren">
					    <table class="dataformV2">
						    <tr>
							    <td class="label">
								    <D:Label runat="server" ID="lblCopyToCompanyId">Bedrijf</D:Label>
							    </td>
							    <td class="control">																																					 
								    <X:ComboBoxLLBLGenEntityCollection runat="server" ID="cblCopyToCompanyId" UseDataBinding="true" EntityName="Company" IsRequired="false" PreventEntityCollectionInitialization="true"></X:ComboBoxLLBLGenEntityCollection>            
                                    <small><D:Label ID="lblCopyToCompanyIdDescription" runat="server" Text="Indien u geen bedrijf selecteert wordt het entertainment item gekopieerd naar het huidige ingelogde bedrijf"></D:Label></small>
							    </td> 
							    <td class="label">
							    </td>
							    <td class="control">
							    </td>    			                              
						    </tr>
						    <tr>
							    <td class="label">
							    </td>
							    <td class="control">																																					 
								    <D:Button ID="btnCopy" runat="server" Text="Copy entertainment" />
							    </td> 
							    <td class="label">
							    </td>
							    <td class="control">
							    </td>    			                              
						    </tr>
					    </table>                            
                    </D:Panel>
				</Controls>
			</X:TabPage>	
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>

