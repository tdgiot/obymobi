﻿using System;   
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Obymobi.Logic.Cms;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;

namespace Obymobi.ObymobiCms.Generic
{
	public partial class Features : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if (CmsSessionHelper.CurrentRole < Role.Administrator)
            {
                var masterPage = this.Master as MasterPages.MasterPageEntityCollection;
                if(masterPage != null)
                    masterPage.ToolBar.Visible = false;
            }
        }

	    protected void Page_Load(object sender, EventArgs e)
        {
        }

        #endregion
    }
}
