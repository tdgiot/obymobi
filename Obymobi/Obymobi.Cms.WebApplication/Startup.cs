﻿using AspNetDependencyInjection;
using Crave.AuditService.AuditEventModelBuilder;
using Crave.AuditService.AuditEventModelBuilder.Interfaces;
using Crave.AuditService.Client;
using Crave.AuditService.Client.Interfaces;
using Crave.AuditService.NetFrameworkAuditEventModelDecorator;
using Crave.AuditService.NetFrameworkAuditEventModelDecorator.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Obymobi.Cms.WebApplication;
using Obymobi.Cms.WebApplication.Validation.UserPasswords;
using Obymobi.Cms.WebApplication.Validation.UserPasswords.Factories;
using Obymobi.Cms.WebApplication.Validation.UserPasswords.Factories.Interfaces;
using Obymobi.Cms.WebApplication.Validation.UserPasswords.Interfaces;
using System.Net.Http;
using WebActivatorEx;

[assembly: PreApplicationStartMethod(typeof(Startup), nameof(Startup.PreStart))]
namespace Obymobi.Cms.WebApplication
{
    //https://github.com/Jehoel/AspNetDependencyInjection/blob/master/GETTING_STARTED.md
    internal static class Startup
    {
        private static ApplicationDependencyInjection _di;

        internal static void PreStart()
        {
            _di = new ApplicationDependencyInjectionBuilder()
                .ConfigureServices(ConfigureServices)
                .Build();
        }

        private static void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IBuilder>(new Builder("Obymobi", new RetrieveChangedValuesHelper()))
                    .AddSingleton<IAuditEventModelDecorator, AuditEventModelDecorator>()
                    .AddSingleton<IAuditServiceClient>(new AuditServiceClient(new HttpClient(), System.Configuration.ConfigurationManager.AppSettings["AuditServiceUrl"]))
                    .AddSingleton<IValidationMessageFactory, ValidationMessageFactory>()
                    .AddSingleton<IUserPasswordValidator, UserPasswordValidator>();
        }

        /// <summary>Invoked at the end of ASP.NET application start-up, after Global's Application_Start method runs. Dependency-injection re-configuration may be called here if you have services that depend on Global being initialized.</summary>
        internal static void PostStart()
        {
        }
    }
}