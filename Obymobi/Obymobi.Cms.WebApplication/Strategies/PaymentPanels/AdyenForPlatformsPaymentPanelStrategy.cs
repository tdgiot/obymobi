﻿using Obymobi.Cms.WebApplication.Strategies.PaymentProvider;

namespace Obymobi.Cms.WebApplication.Strategies.PaymentPanels
{
    public class AdyenForPlatformsPaymentPanelStrategy : PaymentPanelStrategy
    {
        public override PaymentIntegrationConfigurationPanelStrategy GetPaymentIntegrationConfigurationPanelStrategy() => new AdyenForPlatformsPaymentIntegrationConfigurationPanelStrategy();
        public override PaymentProviderPanelStrategy GetPaymentProviderPanelStrategy() => new AdyenPaymentProviderPanelStrategy();
    }
}