﻿using Obymobi.Cms.WebApplication.Strategies.PaymentProvider;

namespace Obymobi.Cms.WebApplication.Strategies.PaymentPanels
{
    public class AdyenHppPaymentPanelStrategy : PaymentPanelStrategy
    {
        public override PaymentIntegrationConfigurationPanelStrategy GetPaymentIntegrationConfigurationPanelStrategy() => new AdyenHppPaymentIntegrationConfigurationPanelStrategy();
        public override PaymentProviderPanelStrategy GetPaymentProviderPanelStrategy() => new AdyenHppPaymentProviderPanelStrategy();
    }
}