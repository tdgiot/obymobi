﻿using Obymobi.Cms.WebApplication.Strategies.PaymentProvider;

namespace Obymobi.Cms.WebApplication.Strategies.PaymentPanels
{
    public class DefaultPaymentPanelStrategy : PaymentPanelStrategy
    {
        public override PaymentIntegrationConfigurationPanelStrategy GetPaymentIntegrationConfigurationPanelStrategy() => new DefaultPaymentIntegrationConfigurationPanelStrategy();
        public override PaymentProviderPanelStrategy GetPaymentProviderPanelStrategy() => new DefaultPaymentProviderPanelStrategy();
    }
}