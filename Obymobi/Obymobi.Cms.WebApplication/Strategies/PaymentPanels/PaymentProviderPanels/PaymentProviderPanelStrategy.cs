﻿using Dionysos.Interfaces;
using Obymobi.Cms.WebApplication.Old_App_Code.Payments;
using Obymobi.Data.EntityClasses;
using Obymobi.Integrations.Payment;

namespace Obymobi.Cms.WebApplication.Strategies.PaymentProvider
{
    public abstract class PaymentProviderPanelStrategy
    {
        protected ISaveableControl Panel { get; set; }

        public virtual void AddPanel(ObymobiCms.Configuration.Sys.PaymentProvider paymentProvider) { }        
        public virtual bool Save(ObymobiCms.Configuration.Sys.PaymentProvider paymentProvider) => Panel == null || Panel.Save();

        protected bool IsAuthenticated(IPaymentProviderConfiguration paymentProviderConfiguration) => new PaymentFactory()
            .CreateClient(paymentProviderConfiguration)
            .IsAuthenticated();
    }
}