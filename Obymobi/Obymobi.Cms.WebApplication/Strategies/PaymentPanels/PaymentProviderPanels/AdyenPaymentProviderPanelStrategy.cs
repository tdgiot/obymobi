﻿using Dionysos;
using Dionysos.Security.Cryptography;
using Dionysos.Web.UI;
using Obymobi.Cms.WebApplication.Configuration.Sys.SubPanels;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Integrations.Payment;
using System;

namespace Obymobi.Cms.WebApplication.Strategies.PaymentProvider
{
    public class AdyenPaymentProviderPanelStrategy : PaymentProviderPanelStrategy
    {
        private const string DUMMY_MERCHANT_CODE = "DUMMY_MERCHANT_CODE";

        public override void AddPanel(ObymobiCms.Configuration.Sys.PaymentProvider paymentProvider)
        {
            AdyenPaymentProvider adyenPaymentProviderPanel = (AdyenPaymentProvider)paymentProvider.LoadControl("~/Configuration/Sys/SubPanels/AdyenPaymentProvider.ascx");
            adyenPaymentProviderPanel.PaymentProviderEntity = paymentProvider.DataSourceAsPaymentProviderEntity;

            Panel = adyenPaymentProviderPanel;

            paymentProvider.PaymentProviderInfoPlaceHolder.Controls.Add(adyenPaymentProviderPanel);
            paymentProvider.PaymentProviderInfoPlaceHolder.Visible = true;
        }

        public override bool Save(ObymobiCms.Configuration.Sys.PaymentProvider paymentProvider)
        {
            bool saved = base.Save(paymentProvider);

            if (paymentProvider.DataSourceAsPaymentProviderEntity.OriginDomain.IsNullOrWhiteSpace() || !Uri.TryCreate(paymentProvider.DataSourceAsPaymentProviderEntity.OriginDomain, UriKind.RelativeOrAbsolute, out Uri _))
            {
                paymentProvider.AddInformator(InformatorType.Warning, "A valid origin domain has to be configured");
            }

            if (paymentProvider.DataSourceAsPaymentProviderEntity.Environment == PaymentProviderEnvironment.Live &&
                paymentProvider.DataSourceAsPaymentProviderEntity.LiveEndpointUrlPrefix.IsNullOrWhiteSpace())
            {
                paymentProvider.AddInformator(InformatorType.Warning, "Live Endpoint Url Prefix have to be entered on the live environment.");
            }

            if (!paymentProvider.DataSourceAsPaymentProviderEntity.ApiKey.IsNullOrWhiteSpace() && !IsAuthenticated(CreateAdyenPaymentProviderConfiguration(paymentProvider.DataSourceAsPaymentProviderEntity)))
            {
                paymentProvider.AddInformator(InformatorType.Warning, "Failed to authenticate with the adyen payment provider.");
            }

            paymentProvider.Validate();

            if (!paymentProvider.IsValid)
            {
                return false;
            }

            return saved;
        }

        private IPaymentProviderConfiguration CreateAdyenPaymentProviderConfiguration(PaymentProviderEntity paymentProvider) => new PaymentProviderConfiguration
        {
            PaymentProviderType = paymentProvider.Type,
            ApiKey = !string.IsNullOrWhiteSpace(paymentProvider.ApiKey) ? Cryptographer.DecryptUsingCBC(paymentProvider.ApiKey) : string.Empty,
            IsProduction = paymentProvider.Environment == PaymentProviderEnvironment.Live,
            LiveEndpointUrlPrefix = paymentProvider.LiveEndpointUrlPrefix,
            AdyenMerchantCode = DUMMY_MERCHANT_CODE
        };
    }
}