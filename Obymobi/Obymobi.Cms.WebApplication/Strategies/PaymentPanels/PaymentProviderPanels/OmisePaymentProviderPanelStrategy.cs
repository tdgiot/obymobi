﻿using Dionysos;
using Dionysos.Security.Cryptography;
using Dionysos.Web.UI;
using Obymobi.Cms.WebApplication.Configuration.Sys.SubPanels;
using Obymobi.Data.EntityClasses;
using Obymobi.Integrations.Payment;

namespace Obymobi.Cms.WebApplication.Strategies.PaymentProvider
{
    public class OmisePaymentProviderPanelStrategy : PaymentProviderPanelStrategy
    {
        public override void AddPanel(ObymobiCms.Configuration.Sys.PaymentProvider paymentProvider) 
        {
            OmisePaymentProvider omisePaymentProviderPanel = (OmisePaymentProvider)paymentProvider.LoadControl("~/Configuration/Sys/SubPanels/OmisePaymentProvider.ascx");
            omisePaymentProviderPanel.PaymentProviderEntity = paymentProvider.DataSourceAsPaymentProviderEntity;

            Panel = omisePaymentProviderPanel;

            paymentProvider.PaymentProviderInfoPlaceHolder.Controls.Add(omisePaymentProviderPanel);
            paymentProvider.PaymentProviderInfoPlaceHolder.Visible = true;
        }

        public override bool Save(ObymobiCms.Configuration.Sys.PaymentProvider paymentProvider)
        {
            bool saved = base.Save(paymentProvider);

            if (!paymentProvider.DataSourceAsPaymentProviderEntity.OmiseSecretKey.IsNullOrWhiteSpace() && !IsAuthenticated(CreateOmisePaymentProviderConfiguration(paymentProvider.DataSourceAsPaymentProviderEntity)))
            {
                paymentProvider.AddInformator(InformatorType.Warning, "Failed to authenticate with the omise payment provider.");
            }

            paymentProvider.Validate();

            if (!paymentProvider.IsValid)
            {
                return false;
            }

            return saved;
        }

        private IPaymentProviderConfiguration CreateOmisePaymentProviderConfiguration(PaymentProviderEntity paymentProvider) => new PaymentProviderConfiguration
        {
            PaymentProviderType = paymentProvider.Type,
            ApiKey = !string.IsNullOrWhiteSpace(paymentProvider.OmiseSecretKey) ? Cryptographer.DecryptUsingCBC(paymentProvider.OmiseSecretKey) : string.Empty,
        };
    }
}