﻿using Obymobi.Cms.WebApplication.Configuration.Sys.SubPanels;

namespace Obymobi.Cms.WebApplication.Strategies.PaymentProvider
{
    public class AdyenHppPaymentProviderPanelStrategy : PaymentProviderPanelStrategy
    {
        public override void AddPanel(ObymobiCms.Configuration.Sys.PaymentProvider paymentProvider)
        {
            AdyenHppPaymentProvider adyenHppPaymentProviderPanel = (AdyenHppPaymentProvider)paymentProvider.LoadControl("~/Configuration/Sys/SubPanels/AdyenHppPaymentProvider.ascx");
            adyenHppPaymentProviderPanel.PaymentProviderEntity = paymentProvider.DataSourceAsPaymentProviderEntity;

            Panel = adyenHppPaymentProviderPanel;

            paymentProvider.PaymentProviderInfoPlaceHolder.Controls.Add(adyenHppPaymentProviderPanel);
            paymentProvider.PaymentProviderInfoPlaceHolder.Visible = true;
        }
    }
}