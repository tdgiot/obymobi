﻿using Obymobi.Cms.WebApplication.Strategies.PaymentProvider;

namespace Obymobi.Cms.WebApplication.Strategies.PaymentPanels
{
    public abstract class PaymentPanelStrategy
    {
        public abstract PaymentProviderPanelStrategy GetPaymentProviderPanelStrategy();
        public abstract PaymentIntegrationConfigurationPanelStrategy GetPaymentIntegrationConfigurationPanelStrategy();
    }
}