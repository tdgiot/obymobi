﻿using Obymobi.Cms.WebApplication.Strategies.PaymentProvider;

namespace Obymobi.Cms.WebApplication.Strategies.PaymentPanels
{
    public class OmisePaymentPanelStrategy : PaymentPanelStrategy
    {
        public override PaymentIntegrationConfigurationPanelStrategy GetPaymentIntegrationConfigurationPanelStrategy() => new DefaultPaymentIntegrationConfigurationPanelStrategy();
        public override PaymentProviderPanelStrategy GetPaymentProviderPanelStrategy() => new OmisePaymentProviderPanelStrategy();
    }
}