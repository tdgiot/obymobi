﻿using Obymobi.Cms.WebApplication.Strategies.PaymentProvider;

namespace Obymobi.Cms.WebApplication.Strategies.PaymentPanels
{
    public class AdyenPaymentPanelStrategy : PaymentPanelStrategy
    {
        public override PaymentIntegrationConfigurationPanelStrategy GetPaymentIntegrationConfigurationPanelStrategy() => new AdyenPaymentIntegrationConfigurationPanelStrategy();
        public override PaymentProviderPanelStrategy GetPaymentProviderPanelStrategy() => new AdyenPaymentProviderPanelStrategy();
    }
}