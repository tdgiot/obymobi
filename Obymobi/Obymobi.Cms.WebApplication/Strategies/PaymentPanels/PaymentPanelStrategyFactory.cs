﻿using Obymobi.Cms.WebApplication.Strategies.PaymentPanels;
using Obymobi.Enums;

namespace Obymobi.Cms.WebApplication.Strategies.PaymentProvider
{
    public static class PaymentPanelStrategyFactory
    {
        public static PaymentPanelStrategy Get(PaymentProviderType paymentProviderType)
        {
            switch (paymentProviderType)
            {
                case PaymentProviderType.Adyen:
                    return new AdyenPaymentPanelStrategy();

                case PaymentProviderType.Omise:
                    return new OmisePaymentPanelStrategy();

                case PaymentProviderType.AdyenHpp:
                    return new AdyenHppPaymentPanelStrategy();

                case PaymentProviderType.AdyenForPlatforms:
                    return new AdyenForPlatformsPaymentPanelStrategy();

                default:
                    return new DefaultPaymentPanelStrategy();
            }
        }
    }
}