﻿using Obymobi.Cms.WebApplication.AppLess.SubPanels.PaymentIntegrationConfigurations;
using Obymobi.ObymobiCms.AppLess;

namespace Obymobi.Cms.WebApplication.Strategies.PaymentProvider
{
    public class AdyenHppPaymentIntegrationConfigurationPanelStrategy : PaymentIntegrationConfigurationPanelStrategy
    {
        public override void AddPanel(PaymentIntegrationConfiguration paymentIntegrationConfiguration)
        {
            AdyenHppPaymentIntegrationConfiguration adyenHppPaymentIntegrationConfigurationPanel = (AdyenHppPaymentIntegrationConfiguration)paymentIntegrationConfiguration.LoadControl("~/AppLess/SubPanels/PaymentIntegrationConfigurations/AdyenHppPaymentIntegrationConfiguration.ascx");
            adyenHppPaymentIntegrationConfigurationPanel.PaymentIntegrationConfigurationEntity = paymentIntegrationConfiguration.DataSourceAsPaymentIntegrationConfigurationEntity;

            Panel = adyenHppPaymentIntegrationConfigurationPanel;

            paymentIntegrationConfiguration.PaymentIntegrationConfigurationPlaceHolder.Controls.Add(adyenHppPaymentIntegrationConfigurationPanel);
            paymentIntegrationConfiguration.PaymentIntegrationConfigurationPlaceHolder.Visible = true;
        }
    }
}