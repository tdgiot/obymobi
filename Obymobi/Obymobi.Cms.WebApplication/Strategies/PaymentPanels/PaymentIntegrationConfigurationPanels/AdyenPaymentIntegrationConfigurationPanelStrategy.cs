﻿using Obymobi.Cms.WebApplication.AppLess.SubPanels.PaymentIntegrationConfigurations;
using Obymobi.ObymobiCms.AppLess;

namespace Obymobi.Cms.WebApplication.Strategies.PaymentProvider
{
    public class AdyenPaymentIntegrationConfigurationPanelStrategy : PaymentIntegrationConfigurationPanelStrategy
    {
        public override void AddPanel(PaymentIntegrationConfiguration paymentIntegrationConfiguration)
        {
            AdyenPaymentIntegrationConfiguration adyenPaymentIntegrationConfigurationPanel = (AdyenPaymentIntegrationConfiguration)paymentIntegrationConfiguration.LoadControl("~/AppLess/SubPanels/PaymentIntegrationConfigurations/AdyenPaymentIntegrationConfiguration.ascx");
            adyenPaymentIntegrationConfigurationPanel.PaymentIntegrationConfigurationEntity = paymentIntegrationConfiguration.DataSourceAsPaymentIntegrationConfigurationEntity;

            Panel = adyenPaymentIntegrationConfigurationPanel;

            paymentIntegrationConfiguration.PaymentIntegrationConfigurationPlaceHolder.Controls.Add(adyenPaymentIntegrationConfigurationPanel);
            paymentIntegrationConfiguration.PaymentIntegrationConfigurationPlaceHolder.Visible = true;
        }        
    }
}