﻿using Dionysos.Interfaces;
using Obymobi.ObymobiCms.AppLess;

namespace Obymobi.Cms.WebApplication.Strategies.PaymentProvider
{
    public abstract class PaymentIntegrationConfigurationPanelStrategy
    {
        protected ISaveableControl Panel { get; set; }
        public virtual void AddPanel(PaymentIntegrationConfiguration paymentIntegrationConfiguration) { }        
        public virtual bool Save() => Panel == null || Panel.Save();
    }
}