﻿using Obymobi.Cms.WebApplication.AppLess.SubPanels.PaymentIntegrationConfigurations;
using Obymobi.ObymobiCms.AppLess;

namespace Obymobi.Cms.WebApplication.Strategies.PaymentProvider
{
    public class AdyenForPlatformsPaymentIntegrationConfigurationPanelStrategy : PaymentIntegrationConfigurationPanelStrategy
    {
        public override void AddPanel(PaymentIntegrationConfiguration paymentIntegrationConfiguration)
        {
            AdyenForPlatformsPaymentIntegrationConfiguration adyenForPlatformsPaymentIntegrationConfigurationPanel = (AdyenForPlatformsPaymentIntegrationConfiguration)paymentIntegrationConfiguration.LoadControl("~/AppLess/SubPanels/PaymentIntegrationConfigurations/AdyenForPlatformsPaymentIntegrationConfiguration.ascx");
            adyenForPlatformsPaymentIntegrationConfigurationPanel.PaymentIntegrationConfigurationEntity = paymentIntegrationConfiguration.DataSourceAsPaymentIntegrationConfigurationEntity;

            Panel = adyenForPlatformsPaymentIntegrationConfigurationPanel;

            paymentIntegrationConfiguration.PaymentIntegrationConfigurationPlaceHolder.Controls.Add(adyenForPlatformsPaymentIntegrationConfigurationPanel);
            paymentIntegrationConfiguration.PaymentIntegrationConfigurationPlaceHolder.Visible = true;
        }        
    }
}