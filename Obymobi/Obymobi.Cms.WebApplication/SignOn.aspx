﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.SignOn" Title="Inloggen" Codebehind="SignOn.aspx.cs" %>
<%@ Register src="~/UserControls/SignOnPanel.ascx" tagname="SignOn" tagprefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="favicon.ico" type="image/x-icon" link="" rel="shortcut icon" /><link href="favicon.ico" type="image/x-icon" link="" rel="icon" />
<title>Login</title>
<style type="text/css">
body, html, form
{
	padding:0;
	margin:0;
	width:100%;
	height:100%;
	background-color:#edf3f4;
}	
body, input, a
{
	color:#000;	
	font-family:Tahoma, Verdana, Arial; 
	font-size:12px;
	line-height:12px;
}
.loginpanel
{
	position:absolute;
	left:50%;
	top:50%;
	margin-left:-198px;
	margin-top:-147px;
	width:396px;
	height:294px;
	background-image:url(images/Interface/login-background.gif);
}
.title
{
	position:absolute;
	font-weight:bold;
	color:#fff;
	top:15px;
	left:19px;
}
.message, .warning
{
	position:absolute;
	width:366px;
	height:23px;
	top:36px;
	left:9px;
	border:1px solid #fff;
	padding:12px 0px 0px 10px;
}
.warning
{	
	color:#ef6c5e;
	font-weight:bold;
	background-image:url(images/Interface/login-error-background.gif);
}
.controls
{
	position:absolute;
	top:94px;
	left:9px;
	width:370px;
}
.controls .blockRight, .controls .blockRemember
{
	float:left;
	text-align:left;
	width:235px;
	padding-top:3px;	
	height:21px;
}
.controls label, .controls .blockLeft
{
	float:left;
	width:106px;
	padding-right:19px;
	padding-top:3px;
	text-align:right;
}
.controls .blockPadding
{
    float:left;
    width:auto;
    padding-left: 20px;
    padding-right: 20px;
    text-align:left;
}
.controls .input
{
	float:left;
	width:233px;
	padding-top:1px;
	padding-left:2px;
	padding-bottom:4px;
	border:1px solid #7eacb1;
	margin-bottom:3px;	
}
.controls .checkbox
{
	position:relative;
	left:-4px;
	top:1px;
}
.controls .checkbox label
{
	position:relative;
	width:120px;
	float:none;
	left:5px;
	top:-2px;
}
.controls .button
{
	position:relative;
	top:3px;
	left:1px;
}
.logo
{
	position:absolute;
	top:226px;
	height:24px;
	padding-top:12px;
	text-align:center;
	width:100%;
}
.poweredby
{
	position:absolute;
	top:269px;
	text-align:center;

	width:100%;
}
.poweredby a
{
	font-size:10px;
	color:#c4c5c6;
	text-decoration:none;	
}
.poweredby a:hover
{
	text-decoration:underline;
}
#version
{
	position:absolute;
	bottom:0px;
	left:50%;
	width:200px;
	height:20px;
	text-align:center;
}
a.flag-nl,a.flag-en
{
    display:inline-block;
    width:20px;
    height:15px;
    background-image:url(images/icons/flag-nl.gif);
    background-position:0px -15px;
}
a.flag-en
{
    background-image:url(images/icons/flag-en.gif);
}
a.flag-nl:hover, a.flag-en:hover, a.active
{
    background-position:0px 0px;
}

</style>
</head>

<body>
<form id="Form1" runat="server">	
	<div class="loginpanel">
		<div class="title"><D:LabelTextOnly runat="server" ID="lblApplicationName" Text="Obymobi Content Management System" LocalizeText="false"></D:LabelTextOnly></div>
		<D:PlaceHolder runat="server" ID="plhNormalMessage"><div class="message"><D:LabelTextOnly runat="server" ID="lblNormalMessage">Hieronder kunt u inloggen</D:LabelTextOnly></div></D:PlaceHolder>
		<D:PlaceHolder runat="server" ID="plhSignInError" Visible="False"><div class="message warning"><D:LabelTextOnly runat="server" ID="lblErrorMessage" LocalizeText="False"></D:LabelTextOnly></div></D:PlaceHolder>		
		<div class="controls">
			<uc1:SignOn ID="SignOn1" runat="server" />			
		</div>
		<div class="logo">         
			<D:LinkButton runat="server" ID="btNl" CssClass="flag-nl" />&nbsp;<D:LinkButton runat="server" ID="btEn" CssClass="flag-en" />
		</div>
		<div class="poweredby">
		    <asp:HyperLink runat="server" ID="hlPoweredBy" NavigateUrl="http://www.grenos.com" Target="_blank" Text="door Grenos"></asp:HyperLink>
		</div>
	</div>
</form>
</body>
</html>





