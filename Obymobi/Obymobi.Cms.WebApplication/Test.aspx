﻿<%@ Page Title="Test" Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Test" CodeBehind="Test.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitleHolder" runat="Server">
    <D:LabelTextOnly runat="server" ID="lblWelcome">Test playground</D:LabelTextOnly>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cplhToolbarHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cplhContentHolder" runat="Server">
    <X:PageControl ID="tabsMain" runat="server" Width="100%">
        <TabPages>
            <X:TabPage Name="UserControls" Text="User controls" runat="server">
                <controls>
                    <D:Panel ID="pnlTokenBox" GroupingText="Obymobi tag box control example" runat="server">
                        <table class="dataformV2">
                            <tr>
                                <td class="label">Tag box</td>
                                <td class="control">
                                    <CC:TagBox ID="TagBox" ClientInstanceName="TagBoxControl" TextField="Name" ValueField="Id" AllowCustomTokens="false" runat="server" />
                                </td>

                                <td class="control" colspan="2">
                                    <table class="dataformV2">
                                        <tr>
                                            <td class="label">
                                                <h4>Demo settings</h4>
                                            </td>
                                            <td class="control"></td>
                                        </tr>

                                        <tr>
                                            <td class="label">Allow custom tokens</td>
                                            <td class="control">
                                                <D:CheckBox ID="chkAllowCustomTokens" AutoPostBack="true" runat="server" />
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td class="label">ReadOnly Mode</td>
                                            <td class="control">
                                                <D:CheckBox ID="cbReadOnly" AutoPostBack="true" runat="server" />
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="label">Show dropdown on focus</td>
                                            <td class="control">
                                                <X:ComboBoxInt ID="ddlShowDropdownOnFocus" runat="server"></X:ComboBoxInt>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="label">Incremental filtering mode</td>
                                            <td class="control">
                                                <X:ComboBoxInt ID="ddlIncrementalFilteringMode" runat="server"></X:ComboBoxInt>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>                        
                    </D:Panel>
                </controls>
            </X:TabPage>
        </TabPages>
    </X:PageControl>
</asp:Content>
