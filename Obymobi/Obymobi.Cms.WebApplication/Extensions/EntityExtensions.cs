﻿using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Cms.WebApplication.Extensions
{
    public static class EntityExtensions
    {
        public static bool IsNullOrNew(this IEntity entity) => entity == null || entity.IsNew;
    }
}