using Obymobi.Cms.WebApplication.Old_App_Code;
using Obymobi.Constants;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using System;

namespace Obymobi.Cms.WebApplication.Extensions
{
    public static class DeviceExtensions
    {
        public static bool IsSupportToolsOnline(this DeviceEntity device) => device.LastSupportToolsRequestUTC.HasValue && device.LastSupportToolsRequestUTC.Value >= DateTime.UtcNow.AddMilliseconds(ObymobiIntervals.DEVICE_OFFLINE_THRESHOLD * -1);

        public static bool IsRemoteControlActive(this DeviceEntity device) => !device.IsNullOrNew() && device.IsSupportToolsOnline();

        public static bool ShowRemoteControl(this DeviceEntity device, Role role) => RoleRights.AllowedToRemoteControl(role) && device.IsRemoteControlActive();

        public static bool ShowLogCat(this DeviceEntity device, Role role) => RoleRights.AllowedToViewLogCat(role) && device.IsRemoteControlActive();
    }
}