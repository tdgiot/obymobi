﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dionysos;
using Obymobi.Data.EntityClasses;
using Dionysos.Web.UI.WebControls;
using Obymobi.Enums;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dionysos.Data.LLBLGen;
using Obymobi.Data;
using System.Data;
using Obymobi.Logic.HelperClasses;
using System.IO;
using Dionysos.Web.UI.DevExControls;
using DevExpress.Web;

namespace Obymobi.ObymobiCms.Company
{
    public partial class Announcement : Obymobi.ObymobiCms.UI.PageLLBLGenEntityCms
    {
        private const int ACTION_NONE = 0;
        private const int ACTION_CATEGORY = 1;
        private const int ACTION_ENTERTAINMENT = 2;
        private const int ACTION_PRODUCT = 3;

        protected override void OnInit(EventArgs e)
        {            
            this.LoadUserControls();
            this.DataSourceLoaded += new Dionysos.Delegates.Web.DataSourceLoadedHandler(Announcement_DataSourceLoaded);
            base.OnInit(e);
        }

        private void SetFilters()
        {
            this.SetProductFilter();
            this.SetCategoryFilter();
            this.SetEntertainmentFilter();
        }

        private void SetProductFilter()
        {
            // Create a filter for retrieving the active products
            PredicateExpression productsFilter = new PredicateExpression();
            productsFilter.Add(ProductFields.VisibilityType != VisibilityType.Never);           // Product should be visible
            productsFilter.Add(ProductCategoryFields.ProductCategoryId != DBNull.Value);        // Product should be in a category
            productsFilter.Add(ProductFields.CompanyId == CmsSessionHelper.CurrentCompanyId);   // Product should belong to the specified company
            productsFilter.Add(CategoryFields.VisibilityType != VisibilityType.Never);

            // The join to get linked to category products only
            RelationCollection joins = new RelationCollection();
            joins.Add(ProductEntity.Relations.ProductCategoryEntityUsingProductId, JoinHint.Inner);
            joins.Add(ProductCategoryEntity.Relations.CategoryEntityUsingCategoryId);

            // Sort the products
            SortExpression sort = new SortExpression(new SortClause(ProductFields.Name, SortOperator.Ascending));

            // Get the products
            ProductCollection products = new ProductCollection();
            products.GetMulti(productsFilter, 0, sort, joins);

            // Bind the data
            this.ddlOnYesProduct.DataSource = products;
            this.ddlOnYesProduct.DataBind();
        }

        private void SetCategoryFilter()
        {
            // Create the filter
            PredicateExpression filter = new PredicateExpression();
            filter.Add(CategoryFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            filter.Add(CategoryFields.Visible == true);
            filter.Add(CategoryFields.AnnouncementAction == true);

            // Sort the results
            SortExpression sort = new SortExpression(new SortClause(CategoryFields.Name, SortOperator.Ascending));

            // Categories
            CategoryCollection categoryCollection = new CategoryCollection();
            categoryCollection.GetMulti(filter, 0, sort);

            this.ddlOnYesCategory.DataSource = categoryCollection;
            this.ddlOnYesCategory.DataBind();
        }

        private void SetEntertainmentFilter()
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(EntertainmentFields.AnnouncementAction == true);

            RelationCollection relations = new RelationCollection();

            if (this.DataSourceAsAnnouncementEntity.DeliverypointgroupId > -1)
            {
                // Filter on deliverypointgroup
                filter.Add(DeliverypointgroupEntertainmentFields.DeliverypointgroupId == this.DataSourceAsAnnouncementEntity.DeliverypointgroupId);

                // Relations
                relations.Add(EntertainmentEntity.Relations.DeliverypointgroupEntertainmentEntityUsingEntertainmentId, JoinHint.Left);
            }
            else if (this.DataSourceAsAnnouncementEntity.CompanyId > -1)
            {
                // Filter
                filter.Add(DeliverypointgroupFields.CompanyId == this.DataSourceAsAnnouncementEntity.CompanyId);

                // Relations
                relations.Add(EntertainmentEntity.Relations.DeliverypointgroupEntertainmentEntityUsingEntertainmentId, JoinHint.Left);
                relations.Add(DeliverypointgroupEntertainmentEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId);
            }
            else
            {
                // Filter
                filter.Add(DeliverypointgroupFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

                // Relations
                relations.Add(EntertainmentEntity.Relations.DeliverypointgroupEntertainmentEntityUsingEntertainmentId, JoinHint.Left);
                relations.Add(DeliverypointgroupEntertainmentEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId);
            }

            EntertainmentCollection entertainmentCollection = new EntertainmentCollection();
            entertainmentCollection.GetMulti(filter, relations);

            // Bind the results
            this.ddlOnYesEntertainment.DataSource = entertainmentCollection;
            this.ddlOnYesEntertainment.DataBind();
        }

        private void SetGui()
        {
            DataTable table = new DataTable();
            table.Columns.Add(new DataColumn("ImageUrl"));
            table.Columns.Add(new DataColumn("MediaUrl"));
            table.Columns.Add(new DataColumn("MediaId"));
            table.Columns.Add(new DataColumn("Name"));

            RelationCollection relations = new RelationCollection();
            relations.Add(MediaRatioTypeMediaEntity.Relations.MediaEntityUsingMediaId);

            PredicateExpression mediaFilter = new PredicateExpression();
            mediaFilter.Add(MediaRatioTypeMediaFields.MediaType == MediaType.Gallery);
            mediaFilter.AddWithOr(MediaRatioTypeMediaFields.MediaType == MediaType.GalleryLarge);
            mediaFilter.AddWithOr(MediaRatioTypeMediaFields.MediaType == MediaType.GallerySmall);

            PredicateExpression filter = new PredicateExpression();
            filter.Add(MediaFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            filter.Add(mediaFilter);

            PrefetchPath prefetch = new PrefetchPath(EntityType.MediaRatioTypeMediaEntity);
            prefetch.Add(MediaRatioTypeMediaEntity.PrefetchPathMediaEntity);
            prefetch.Add(MediaRatioTypeMediaEntity.PrefetchPathMediaRatioTypeMediaFileEntity);

            MediaRatioTypeMediaCollection mediaRatioTypeMediaCollection = new MediaRatioTypeMediaCollection();
            mediaRatioTypeMediaCollection.GetMulti(filter, 0, null, relations, prefetch);

            List<int> uniqueMediaIds = new List<int>();
            foreach (MediaRatioTypeMediaEntity mediaRatioTypeMedia in mediaRatioTypeMediaCollection)
            {
                if (!uniqueMediaIds.Contains(mediaRatioTypeMedia.MediaId) && mediaRatioTypeMedia.MediaEntity.IsCultureAgnostic)
                {
                    uniqueMediaIds.Add(mediaRatioTypeMedia.MediaId);

                    DataRow row = table.NewRow();
                    row["MediaUrl"] = ResolveUrl(string.Format("~/Generic/Media.aspx?id={0}&mediaType={1}", mediaRatioTypeMedia.MediaEntity.MediaId, mediaRatioTypeMedia.MediaType));
                    string filePathRelative = "~/Generic/ThumbnailGenerator.ashx?mediaId={0}&width={1}&height={2}&fitInDimensions=false".FormatSafe(mediaRatioTypeMedia.MediaId, 0, 150);
                    row["ImageUrl"] = filePathRelative;
                    row["MediaId"] = mediaRatioTypeMedia.MediaEntity.MediaId;
                    row["Name"] = mediaRatioTypeMedia.MediaEntity.Name;
                    table.Rows.Add(row);
                }
            }

            this.dvGallery.DataSource = table;
            this.dvGallery.DataBind();           
        }

        private void LoadUserControls()
        {
            Generic.UserControls.CustomTextCollection translationsPanel = this.tabsMain.AddTabPage("Translations", "CustomTextCollection", "~/Generic/UserControls/CustomTextCollection.ascx") as Generic.UserControls.CustomTextCollection;
            translationsPanel.FullWidth = true;            
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            bool recurring = (this.DataSourceAsAnnouncementEntity.Recurring.HasValue ? this.DataSourceAsAnnouncementEntity.Recurring.Value : false);
            this.plhNonRecurring.Visible = !recurring;
            this.plhRecurring.Visible = recurring;

            if (!this.IsPostBack)
            {
                if (this.DataSourceAsAnnouncementEntity.MediaId.HasValue)
                {
                    MediaEntity mediaEntity = new MediaEntity(this.DataSourceAsAnnouncementEntity.MediaId.Value);
                    if (!mediaEntity.IsNew)
                    {
                        this.tbImageId.Value = mediaEntity.MediaId;
                        this.tbImageName.Value = mediaEntity.Name;
                    }
                }
            }
        }

        void Announcement_DataSourceLoaded(object sender)
        {
            this.SetFilters();
            this.SetGui();
        }

        public void setCustomValues()
        {
            if (this.teTimeToShow.Value.HasValue)
                this.DataSourceAsAnnouncementEntity.TimeToShow = DateTimeUtil.CombineDateAndTime(this.teTimeToShow.Value.Value, DateTime.Now);

            if (this.teRecurringBeginTime.Value.HasValue)
                this.DataSourceAsAnnouncementEntity.RecurringBeginTime = DateTimeUtil.CombineDateAndTime(this.teRecurringBeginTime.Value.Value, DateTime.Now);

            if (this.teRecurringEndTime.Value.HasValue)
                this.DataSourceAsAnnouncementEntity.RecurringEndTime = DateTimeUtil.CombineDateAndTime(this.teRecurringEndTime.Value.Value, DateTime.Now);
            
            this.DataSourceAsAnnouncementEntity.MediaId = null;
            int imageId;
            if (int.TryParse(this.tbImageId.Value.ToString(), out imageId))
            {
                if (imageId > 0)
                {
                    this.DataSourceAsAnnouncementEntity.MediaId = imageId;
                }
            }
        }

        public override bool Save()
        {
            bool success = false;

            this.CustomValidation();

            if (this.IsValid)
            {
                setCustomValues();
                if (base.Save())
                {
                    CustomTextHelper.UpdateCustomTexts(this.DataSource, this.DataSourceAsAnnouncementEntity.CompanyEntity);                    
                    success =  true;
                }
                else
                    success = false;
            }

            return success;
        }

        public override bool SaveAndGo()
        {
            bool success = false;

            this.CustomValidation();

            if (this.IsValid)
            {
                setCustomValues();
                success = base.SaveAndGo();
            }

            return success;
        }

        public override bool SaveAndNew()
        {
            bool success = false;

            this.CustomValidation();

            if (this.IsValid)
            {
                setCustomValues();
                success = base.SaveAndNew();
            }

            return success;
        }

        private void CustomValidation()
        {
            if (this.DataSourceAsAnnouncementEntity.Recurring.HasValue && this.DataSourceAsAnnouncementEntity.Recurring.Value)
            {
                if (this.deRecurringBeginDate.Value == null)
                    this.MultiValidatorDefault.AddError(this.Translate("RecurringStartDateNotSpecified", "De startdatum van het terugkeerpatroon is niet ingevuld."));
                if (this.teRecurringBeginTime.Value == null)
                    this.MultiValidatorDefault.AddError(this.Translate("RecurringStartTimeNotSpecified", "De starttijd van het terugkeerpatroon is niet ingevuld."));

                if (this.deRecurringEndDate.Value == null)
                    this.MultiValidatorDefault.AddError(this.Translate("RecurringEndDateNotSpecified", "De einddatum van het terugkeerpatroon is niet ingevuld."));
                if (this.teRecurringEndTime.Value == null)
                    this.MultiValidatorDefault.AddError(this.Translate("RecurringStartTimeNotSpecified", "De eindtijd van het terugkeerpatroon is niet ingevuld."));

                AnnouncementRecurringPeriod period = (AnnouncementRecurringPeriod)(this.ddlRecurringPeriod.SelectedIndex + 1);

                // Recurring
                switch (period)
                {
                    case AnnouncementRecurringPeriod.EveryXMinutes:
                        if (this.tbRecurringMinutes.Value == null || this.tbRecurringMinutes.Value <= 0)
                            this.MultiValidatorDefault.AddError(this.Translate("RecurringMinutesNotSpecified", "De terugkeer minuten zijn niet ingevuld."));
                        break;
                    case AnnouncementRecurringPeriod.Hourly:
                        break;
                    case AnnouncementRecurringPeriod.Daily:
                        break;
                    case AnnouncementRecurringPeriod.Weekly:
                        break;
                    case AnnouncementRecurringPeriod.Monthly:
                        break;
                    case AnnouncementRecurringPeriod.Yearly:
                        break;
                    default:
                        break;
                }
            }
            else
            {
                // Display once
                if (this.deDateToShow.Value == null)
                    this.deDateToShow.Value = DateTime.Now;
                    
                if (this.teTimeToShow.Value == null)
                    this.MultiValidatorDefault.AddError(this.Translate("TimeToShowNotSpecified", "De tijd voor het tonen van de aankondiging is niet ingevuld."));
            }

            int yesActions = 0;
            if (this.ddlOnYesProduct.ValidId > 0)
                yesActions++;

            if (this.ddlOnYesCategory.ValidId > 0)
                yesActions++;

            if (this.ddlOnYesEntertainment.ValidId > 0)
                yesActions++;

            if (yesActions > 1)
            {
                this.MultiValidatorDefault.AddError(this.Translate("AnnouncementOnlyOneYesAction", "Multiple yes actions have been selected. Only one yes action is allowed per announcement."));
            }

            if (this.tbText.Text.Length > 1000)
                this.MultiValidatorDefault.AddError(this.Translate("AnnouncementTooMuchCharactersInText", "The text contains too much characters, therefore it might be possible that not all text is visible on the device."));

            this.Validate();
        }

        /// <summary>
        /// Return the page's datasource as a AnnouncementEntity
        /// </summary>
        public AnnouncementEntity DataSourceAsAnnouncementEntity
        {
            get
            {
                return this.DataSource as AnnouncementEntity;
            }
        }
    }
}