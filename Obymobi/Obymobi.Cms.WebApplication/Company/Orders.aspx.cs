using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data;
using Dionysos.Diagnostics;
using Obymobi.Web.Google;
using System.Text;
using DevExpress.Web;
using System.Data;
using Google.Apis.Analytics.v3.Data;
using Dionysos.Web.UI;
using DevExpress.XtraCharts.Web;
using DevExpress.XtraCharts;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrintingLinks;
using System.IO;
using DevExpress.XtraCharts.Native;
using Dionysos;

namespace Obymobi.ObymobiCms.Company
{
    public partial class Orders : PageDefault
    {
        OrderCollection orders = new OrderCollection();

        #region Event handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.deDateFrom.Value.HasValue)
                this.deDateFrom.Value = DateTime.Now;

            if (!this.deDateTo.Value.HasValue)
                this.deDateTo.Value = DateTime.Now;

            if (!this.IsPostBack || this.ddlDeliverypointgroupId.Items.Count <= 0)
            {
                DeliverypointgroupCollection deliverypointgroups = new DeliverypointgroupCollection();
                deliverypointgroups.GetMulti(DeliverypointgroupFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

                this.ddlDeliverypointgroupId.DataSource = deliverypointgroups;
                this.ddlDeliverypointgroupId.DataBind();
            }

            if (this.ddlDeliverypointId.ValidId <= 0)
            {
                var filter = new PredicateExpression();
                if (this.ddlDeliverypointgroupId.ValidId > 0)
                    filter.Add(DeliverypointFields.DeliverypointgroupId == this.ddlDeliverypointgroupId.ValidId);
                else
                    filter.Add(DeliverypointFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

                var sort = new SortExpression(new SortClause(DeliverypointFields.DeliverypointgroupId, SortOperator.Ascending));
                sort.Add(new SortClause(DeliverypointFields.Number, SortOperator.Ascending));

                var deliverypoints = new DeliverypointCollection();
                deliverypoints.GetMulti(filter, 0, sort);

                this.ddlDeliverypointId.DataSource = deliverypoints;
                this.ddlDeliverypointId.DataBind();
            }            

            this.SetFilter();
            this.SetGui();
        }

        protected void Order_Clicked(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            int orderId = Int32.Parse(e.Parameters);

            string url = ResolveUrl(string.Format("~/Company/Order.aspx?id={0}", orderId));
            ASPxWebControl.RedirectOnCallback(url);
        }

        #endregion

        #region Methods

        private void SetFilter()
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(OrderFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

            if (this.ddlDeliverypointId.ValidId > 0)
                filter.Add(OrderFields.DeliverypointId == this.ddlDeliverypointId.ValidId);
            else if (this.ddlDeliverypointgroupId.ValidId > 0)
                filter.Add(DeliverypointFields.DeliverypointgroupId == this.ddlDeliverypointgroupId.ValidId);
            
            DateTime dateFrom = this.deDateFrom.Value.Value;
            DateTime dateTo = this.deDateTo.Value.Value;

            if (this.deDateFrom.Value != null)
                filter.Add(OrderFields.CreatedUTC >= TimeZoneInfo.ConvertTimeToUtc(dateFrom, TimeZoneInfo.Local));
            if (this.deDateTo.Value != null)
                filter.Add(OrderFields.CreatedUTC <= TimeZoneInfo.ConvertTimeToUtc(dateTo, TimeZoneInfo.Local).AddDays(1));

            PrefetchPath prefetch = new PrefetchPath(EntityType.OrderEntity);
            prefetch.Add(OrderEntity.PrefetchPathDeliverypointEntity).SubPath.Add(DeliverypointEntity.PrefetchPathDeliverypointgroupEntity);
            
            RelationCollection relations = new RelationCollection();
            relations.Add(OrderEntity.Relations.DeliverypointEntityUsingDeliverypointId);
            relations.Add(DeliverypointEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId);

            SortExpression sort = new SortExpression();
            sort.Add(new SortClause(OrderFields.CreatedUTC, SortOperator.Descending));

            this.orders = new OrderCollection();
            this.orders.GetMulti(filter, 0, sort, relations, prefetch);
        }

        private void SetGui()
        { 
            // Create the table for binding later on
            DataTable table = new DataTable();
            
            // Create columns
            DataColumn orderIdColumn = new DataColumn("OrderId");
            DataColumn orderitemsColumn = new DataColumn("Orderitems");
            DataColumn deliverypointColumn = new DataColumn("Deliverypoint");
            DataColumn deliverypointgroupColumn = new DataColumn("Deliverypointgroup");
            DataColumn statusColumn = new DataColumn("Status");
            DataColumn statusTextColumn = new DataColumn("StatusText");
            DataColumn createdColumn = new DataColumn("Created");

            // Translations
            orderIdColumn.Caption = this.Translate("clmnOrderId", "OrderId");
            orderitemsColumn.Caption = this.Translate("clmnOrderitems", "Orderitems");
            deliverypointColumn.Caption = this.Translate("clmDeliverypoint", "Tafel");
            deliverypointgroupColumn.Caption = this.Translate("clmDeliverypointgroup", "Tafelgroep");
            statusColumn.Caption = this.Translate("clmStatus", "Status");
            statusTextColumn.Caption = this.Translate("clmStatusText", "Status tekst");
            createdColumn.Caption = this.Translate("clmCreated", "Aangemaakt");

            // Add Columns
            table.Columns.Add(orderIdColumn);
            table.Columns.Add(orderitemsColumn);
            table.Columns.Add(deliverypointColumn);
            table.Columns.Add(deliverypointgroupColumn);
            table.Columns.Add(statusColumn);
            table.Columns.Add(statusTextColumn);
            table.Columns.Add(createdColumn);

            foreach (OrderEntity order in this.orders)
            {
                DataRow row = table.NewRow();

                row[0] = order.OrderId;

                string orderitemString = "";
                for (int i = 0; i < order.OrderitemCollection.Count; i++)
                {
                    OrderitemEntity orderitem = order.OrderitemCollection[i];

                    if (i != order.OrderitemCollection.Count - 1)
                        orderitemString += orderitem.ProductName + ", ";
                    else
                        orderitemString += orderitem.ProductName;
                }
                
                row[1] = orderitemString;
                row[2] = order.DeliverypointEntity.Name;
                row[3] = order.DeliverypointEntity.DeliverypointgroupEntity.Name;
                row[4] = order.Status;
                row[5] = order.StatusText;
                row[6] = order.CreatedUTC.Value.UtcToLocalTime();
                
                table.Rows.Add(row);
            }

            // Bind the data
            this.ordersGrid.DataSource = table;
            this.ordersGrid.DataBind();
        }

        #endregion


        

        
    }
}