﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.SurveyQuestion" Codebehind="SurveyQuestion.aspx.cs" %>
<%@ Reference Control="~/Company/Subpanels/SurveyAnswerPanel.ascx" %>
<%@ Reference Control="~/Company/Subpanels/SurveyQuestionTypePanels/MatrixPanel.ascx" %>
<%@ Reference Control="~/Company/Subpanels/SurveyQuestionTypePanels/DescriptiveTextPanel.ascx" %>
<%@ Reference Control="~/Company/Subpanels/SurveyQuestionTypePanels/MultipleChoicePanel.ascx" %>
<%@ Reference Control="~/Company/Subpanels/SurveyQuestionTypePanels/FormPanel.ascx" %>
<%@ Reference Control="~/Company/Subpanels/SurveyQuestionTypePanels/RankingPanel.ascx" %>
<%@ Reference Control="~/Company/Subpanels/SurveyQuestionTypePanels/RatingPanel.ascx" %>
<%@ Reference Control="~/Company/Subpanels/SurveyQuestionTypePanels/YesNoPanel.ascx" %>
<%@ Reference Control="~/Company/Subpanels/SurveyQuestionTypePanels/SingleTextboxPanel.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
                    <D:PlaceHolder ID="PlaceHolder1" runat="server">
					<table class="dataformV2">
						<tr>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblType">Type</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <X:ComboBoxInt ID="ddlType" runat="server" UseDataBinding="true" IsRequired="true" DisabledOnceSelectedAndSaved="true" DisabledOnceSelectedFromQueryString="true"></X:ComboBoxInt>
                            </td>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblSortOrder">Order</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <D:TextBoxInt ID="tbSortOrder" runat="server" ThousandsSeperators="false"></D:TextBoxInt>
                            </td>
					    </tr>
                        <tr>
                            <D:PlaceHolder ID="plhQuestion" runat="server">
                                <td class="label">
							        <D:LabelEntityFieldInfo runat="server" id="lblQuestion">Question</D:LabelEntityFieldInfo>
						        </td>
						        <td class="control">
							        <D:TextBoxString ID="tbQuestion" runat="server" IsRequired="true"></D:TextBoxString>
						        </td>
                            </D:PlaceHolder>
                            <D:PlaceHolder ID="plhRequired" runat="server">
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblRequired">Antwoord noodzakelijk</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:CheckBox runat="server" ID="cbRequired" ValueField="Required"/>
                                </td>
                            </D:PlaceHolder>  
                            <D:PlaceHolder ID="plhNotesEnabled" runat="server">
                                <td class="label">
                                    <D:Label runat="server" ID="lbNotesEnabled">Opmerkingen toegestaan</D:Label>
                                </td>
                                <td class="control">
                                    <D:CheckBox runat="server" ID="cbNotesEnabled" />
                                </td>
                            </D:PlaceHolder>
                        </tr>                        
                        <tr>
                            <td colspan="4">
                                <D:PlaceHolder runat="server" ID="plhTypeSpecificSettings"></D:PlaceHolder>
                            </td>
                        </tr> 
					 </table>	
                    </D:PlaceHolder>
				</Controls>
			</X:TabPage>	
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>

