﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.SurveyPage" Codebehind="SurveyPage.aspx.cs" %>
<%@ Reference VirtualPath="~/Generic/UserControls/MediaCollection.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblName">Naam</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblSurveyId">Survey</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlSurveyId" IncrementalFilteringMode="StartsWith" EntityName="Survey" TextField="Name" ValueField="SurveyId" />
                            </td>
					    </tr>
                        <tr>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblSortOrder">Order</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <D:TextBoxInt ID="tbSortOrder" runat="server" ThousandsSeperators="false"></D:TextBoxInt>
                            </td>
                            <td class="label">

                            </td>
                            <td class="control">

                            </td>
                        </tr>
					 </table>	
				</Controls>
			</X:TabPage>	
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>

