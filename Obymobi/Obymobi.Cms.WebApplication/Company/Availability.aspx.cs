﻿using System;
using DevExpress.Web;
using Obymobi.Cms.Logic.Sites;
using Obymobi.Data.EntityClasses;
using Obymobi.Cms.Logic.HelperClasses;

namespace Obymobi.ObymobiCms.Company
{
	public partial class Availability : Dionysos.Web.UI.PageLLBLGenEntity
    {
		#region Methods

		protected override void OnInit(EventArgs e)
		{
		    this.LoadUserControls();
			this.DataSourceLoaded += this.Availability_DataSourceLoaded;
			base.OnInit(e);			
		}

	    private void LoadUserControls()
	    {
	        AvailabilityEntity availabilityEntity = new AvailabilityEntity(this.EntityId);
            if (availabilityEntity.IsNew)
            {
                return;
            }

            this.tabsMain.AddTabPage("Translations", "CustomTextCollection", "~/Generic/UserControls/CustomTextCollection.ascx");	        
	    }

	    private void SetGui()
	    {
	        this.DataBindCategories();
	        this.DataBindProducts();
	        this.DataBindSites();
	        this.DataBindPages();
	        this.DataBindEntertainments();
	    }

	    private void DataBindCategories()
	    {
	        this.cbActionCategoryId.DataSource = CategoryHelper.GetCategories(CmsSessionHelper.CurrentCompanyId, true);
            this.cbActionCategoryId.DataBind();
	    }

	    private void DataBindProducts()
	    {
            int categoryId = 0;
            if (this.IsPostBack)
            {
                categoryId = this.cbActionCategoryId.ValidId;
            }
            else if (this.DataSourceAsAvailabilityEntity.ActionCategoryId.HasValue)
            {
                categoryId = this.DataSourceAsAvailabilityEntity.ActionCategoryId.Value;
            }

            this.cbActionProductCategoryId.DataSource = Obymobi.Logic.HelperClasses.ProductHelper.GetProductCategories(CmsSessionHelper.CurrentCompanyId, true, categoryId);
            this.cbActionProductCategoryId.DataBind();
	    }

	    private void DataBindSites()
	    {
            this.cbActionSiteId.DataSource = SiteHelper.GetSites(CmsSessionHelper.CurrentCompanyId, true);
            this.cbActionSiteId.DataBind();
	    }

	    private void DataBindPages()
	    {
            int siteId = 0;
            if (this.IsPostBack)
            {
                siteId = this.cbActionSiteId.ValidId;
            }
            else if (this.DataSourceAsAvailabilityEntity.ActionSiteId.HasValue)
            {
                siteId = this.DataSourceAsAvailabilityEntity.ActionSiteId.Value;
            }

            this.cbActionPageId.DataSource = PageHelper.GetPages(siteId, true);
            this.cbActionPageId.DataBind();
	    }

	    private void DataBindEntertainments()
	    {
            this.cbActionEntertainmentId.DataSource = EntertainmentHelper.GetEntertainments(CmsSessionHelper.CurrentCompanyId, true, true);
            this.cbActionEntertainmentId.DataBind();
	    }

	    public override void Validate()
	    {
            base.Validate();
            if (this.IsValid)
            {
                int selectedActions = 0;

                if (this.cbActionCategoryId.ValidId > 0 || this.cbActionProductCategoryId.ValidId > 0)
                    selectedActions++;
                if (this.cbActionEntertainmentId.ValidId > 0)
                    selectedActions++;
                if (this.cbActionSiteId.ValidId > 0 || this.cbActionPageId.ValidId > 0)
                    selectedActions++;
                if (!string.IsNullOrWhiteSpace(this.tbUrl.Value))
                    selectedActions++;

                if (selectedActions > 1)
                {
                    this.MultiValidatorDefault.AddError("Multiple actions have been selected, please select only one action.");
                    base.Validate();
                }
            }
	    }

	    #endregion

		#region Event Handlers

	    protected void Page_Load(object sender, EventArgs e)
		{
	        if (this.PageMode != Dionysos.Web.PageMode.Add)
	        {
                this.Validate();
	        }
		}

		private void Availability_DataSourceLoaded(object sender)
		{
			this.SetGui();
		}

        public override bool Save()
        {
            if (this.PageMode == Dionysos.Web.PageMode.Add)
            {
                this.DataSourceAsAvailabilityEntity.CompanyId = CmsSessionHelper.CurrentCompanyId;
            }

            if (base.Save())
            {
                Obymobi.Logic.HelperClasses.CustomTextHelper.UpdateCustomTexts(this.DataSourceAsAvailabilityEntity, this.DataSourceAsAvailabilityEntity.CompanyEntity);
                return true;
            }
            else
            {
                return false;
            }
        }

        protected void cbActionProductCategoryId_OnCallback(object sender, CallbackEventArgsBase e)
        {
            this.DataBindProducts();
        }

        protected void cbActionPageId_OnCallback(object sender, CallbackEventArgsBase e)
        {
            this.DataBindPages();
        }

		#endregion

		#region Properties

		public AvailabilityEntity DataSourceAsAvailabilityEntity
		{
			get
			{
				return this.DataSource as AvailabilityEntity;
			}
		}

		#endregion
    }
}
