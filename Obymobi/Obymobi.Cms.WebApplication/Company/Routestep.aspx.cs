﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Logic.Cms;
using Obymobi.Logic.Routing;

namespace Obymobi.ObymobiCms.Company
{
    public partial class Routestep : Dionysos.Web.UI.PageLLBLGenEntity
    {
        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            this.DataSourceLoad += new Dionysos.Delegates.Web.DataSourceLoadHandler(Routestep_DataSourceLoad);
            this.DataSourceLoaded += new Dionysos.Delegates.Web.DataSourceLoadedHandler(Routestep_DataSourceLoaded);
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        private void LoadUserControls()
        {
            this.tabsMain.AddTabPage("Routestephandlers", "RoutestephandlerCollection", "~/Company/SubPanels/RoutestephandlerCollection.ascx");
        }

        public override bool Save()
        {
            if (this.DataSourceAsRoutestepEntity.IsNew)
            {
                int routeId = int.Parse(Request.QueryString["RouteId"]);
                this.DataSourceAsRoutestepEntity.RouteId = routeId;
            }

            return base.Save();
        }

        void Routestep_DataSourceLoad(object sender, ref bool preventLoading)
        {
            RouteCollection routes = new RouteCollection();
            PredicateExpression filterRoutes = new PredicateExpression();
            filterRoutes.Add(RouteFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            SortExpression sort = new SortExpression();
            sort.Add(RouteFields.Name | SortOperator.Ascending);
            routes.GetMulti(filterRoutes, 0, sort);

            this.ddlRouteId.TextField = "Name";
            this.ddlRouteId.ValueField = "RouteId";
            this.ddlRouteId.DataSource = routes;
            this.ddlRouteId.DataBind();            
        }

        void Routestep_DataSourceLoaded(object sender)
        {
            this.lblStepTimeoutMinutesValue.Text = this.DataSourceAsRoutestepEntity.RouteEntity.StepTimeoutMinutes.ToString();

            // Don't allow a last step to be failed and continue (silent erroring of an order should be discouraged).
            if (this.DataSourceAsRoutestepEntity.RouteEntity.RoutestepCollection.Max(rs => rs.Number) == this.DataSourceAsRoutestepEntity.Number)
            {
                this.AddInformatorInfo(this.Translate("LastStepOfRoute", "Dit is de laatste stap van de route."));

                if (this.DataSourceAsRoutestepEntity.ContinueOnFailure)
                {
                    this.AddInformator(Dionysos.Web.UI.InformatorType.Warning, this.Translate("LastStepOfRouteContinueOnFailureIgnored", "'Doorgaan na falen' wordt voor deze stap niet toegepast omdat het de laatste is."));
                }
            }    
            
            // If a step has an automatic handler, no additional steps can be added
            if (this.DataSourceAsRoutestepEntity.RoutestephandlerCollection.Any(rs => RoutingHelper.GetAutomaticlyHandledRoutestephandlers().Contains(rs.HandlerTypeAsEnum)))
            { 
                this.AddInformatorInfo(this.Translate("AutomaticRouteStepCanOnlyHaveOneHandler", "Deze route stap heeft een automatische afhandelaar, er kunnen daarom niet meer afhandelaars worden ingesteld."));
            }
            else if (this.DataSourceAsRoutestepEntity.RoutestephandlerCollection.Any(rs => RoutingHelper.GetManuallyHandledRoutestephandlers().Contains(rs.HandlerTypeAsEnum)))
            { 
                this.AddInformatorInfo(this.Translate("ManualRouteStepCanOnlyHaveManualHandler", "Deze route stap heeft een handmatige afhandelaar, er kunnen daarom alleen andere handmatige afhandelaars worden toegevoegd."));
            }

            this.Validate("fake");
        }

        /// <summary>
        /// Return the page's datasource as a FormEntity
        /// </summary>
        public RoutestepEntity DataSourceAsRoutestepEntity
        {
            get
            {
                return this.DataSource as RoutestepEntity;
            }
        }
    }
}
