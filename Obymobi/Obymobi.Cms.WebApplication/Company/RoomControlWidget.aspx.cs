﻿using System;
using System.Drawing;
using System.Globalization;
using DevExpress.Web;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Enums;
using Dionysos.Web;
using Dionysos.Data.LLBLGen;
using Google.Apis.Util;
using Obymobi.Api.Logic.HelperClasses;
using Obymobi.Data;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.ObymobiCms.Company
{
	public partial class RoomControlWidget : Dionysos.Web.UI.PageLLBLGenEntity
	{
		#region Methods

        private void LoadUserControls()
        {
            RoomControlWidgetEntity roomControlWidgetEntity = new RoomControlWidgetEntity(this.EntityId);
            if (roomControlWidgetEntity.IsNew)
                return;

            this.tabsMain.AddTabPage("Translations", "CustomTextCollection", "~/Generic/UserControls/CustomTextCollection.ascx", "Generic");            
        }

	    private void LoadRoomControlComponents()
	    {
            int roomControlSectionId = -1;
            if (QueryStringHelper.HasValue("RoomControlSectionId"))
                Int32.TryParse(QueryStringHelper.GetValue("RoomControlSectionId"), out roomControlSectionId);

	        if (roomControlSectionId < 0 && this.DataSourceAsRoomControlWidgetEntity.RoomControlSectionId.HasValue)
	            roomControlSectionId = this.DataSourceAsRoomControlWidgetEntity.RoomControlSectionId.Value;

            if (roomControlSectionId > -1)
	        {
	            var filter = new PredicateExpression(RoomControlSectionFields.RoomControlSectionId == roomControlSectionId);

	            var relations = new RelationCollection(RoomControlConfigurationEntityBase.Relations.RoomControlAreaEntityUsingRoomControlConfigurationId);
	            relations.Add(RoomControlAreaEntityBase.Relations.RoomControlSectionEntityUsingRoomControlAreaId);

	            var roomControlConfigurations = new RoomControlConfigurationCollection();
	            roomControlConfigurations.GetMulti(filter, 0, null, relations);

	            if (roomControlConfigurations.Count == 1)
	            {
	                int configurationId = roomControlConfigurations[0].RoomControlConfigurationId;
                    RoomControlComponentType type = this.GetRoomControlComponentTypeByWidgetType(this.DataSourceAsRoomControlWidgetEntity.Type);

	                if (type != RoomControlComponentType.Unknown)
	                {
                        Obymobi.Data.CollectionClasses.RoomControlComponentCollection components = RoomControlComponentHelper.GetAllRoomControlComponentsInConfigurationWithComponentType(configurationId, type);
	                    
                        this.ddlRoomControlComponentId1.DataSource = components;
                        this.ddlRoomControlComponentId2.DataSource = components;
                        this.ddlRoomControlComponentId3.DataSource = components;
                        this.ddlRoomControlComponentId4.DataSource = components;
                        this.ddlRoomControlComponentId5.DataSource = components;
                        this.ddlRoomControlComponentId6.DataSource = components;
                        this.ddlRoomControlComponentId7.DataSource = components;
                        this.ddlRoomControlComponentId8.DataSource = components;
                        this.ddlRoomControlComponentId9.DataSource = components;
                        this.ddlRoomControlComponentId10.DataSource = components;

                        this.ddlRoomControlComponentId1.DataBind();
                        this.ddlRoomControlComponentId2.DataBind();
                        this.ddlRoomControlComponentId3.DataBind();
                        this.ddlRoomControlComponentId4.DataBind();
                        this.ddlRoomControlComponentId5.DataBind();
                        this.ddlRoomControlComponentId6.DataBind();
                        this.ddlRoomControlComponentId7.DataBind();
                        this.ddlRoomControlComponentId8.DataBind();
                        this.ddlRoomControlComponentId9.DataBind();
                        this.ddlRoomControlComponentId10.DataBind();
	                }	                
	            }	            
	        }	        
	    }

	    private RoomControlComponentType GetRoomControlComponentTypeByWidgetType(RoomControlWidgetType widgetType)
	    {
            RoomControlComponentType componentType = RoomControlComponentType.Unknown;
	        if (widgetType == RoomControlWidgetType.Light2x1 ||
                widgetType == RoomControlWidgetType.Light1x1)
	        {
                componentType = RoomControlComponentType.Light;
	        }	        
            else if (widgetType == RoomControlWidgetType.Scene1x1)
            {
                componentType = RoomControlComponentType.LightingScene;
            }	        
            else if (widgetType == RoomControlWidgetType.Blind1x2 || widgetType == RoomControlWidgetType.Blind1x1)
            {
                componentType = RoomControlComponentType.Blind;
            }	        
            else if (widgetType == RoomControlWidgetType.Thermostat4x2)
            {
                componentType = RoomControlComponentType.Thermostat;
            }	        
	        return componentType;
	    }

	    public override bool Save()
        {
            // Set the UITabId if specified in querystring
            if (this.DataSourceAsRoomControlWidgetEntity.IsNew)
            {
                int id;
                if (QueryStringHelper.TryGetValue("RoomControlSectionId", out id))
                    this.DataSourceAsRoomControlWidgetEntity.RoomControlSectionId = id;                
            }

	        if (this.DataSourceAsRoomControlWidgetEntity.Type == RoomControlWidgetType.Volume1x2 || 
                this.DataSourceAsRoomControlWidgetEntity.Type == RoomControlWidgetType.Volume1x1)
	        {
	            this.DataSourceAsRoomControlWidgetEntity.FieldValue1 = this.tbVolumeUpScene.Text;
	            this.DataSourceAsRoomControlWidgetEntity.FieldValue2 = this.tbVolumeDownScene.Text;
	            this.DataSourceAsRoomControlWidgetEntity.FieldValue3 = this.tbVolumeMuteScene.Text;
	            this.DataSourceAsRoomControlWidgetEntity.FieldValue4 = this.tbPowerScene.Text;
	        }
            else if (this.DataSourceAsRoomControlWidgetEntity.Type == RoomControlWidgetType.Channel1x2 || 
                this.DataSourceAsRoomControlWidgetEntity.Type == RoomControlWidgetType.Channel1x1)
            {
                this.DataSourceAsRoomControlWidgetEntity.FieldValue1 = this.tbChannelUpScene.Text;
                this.DataSourceAsRoomControlWidgetEntity.FieldValue2 = this.tbChannelDownScene.Text;
            }
            else if (this.DataSourceAsRoomControlWidgetEntity.Type == RoomControlWidgetType.Navigation1x2)
            {
                this.DataSourceAsRoomControlWidgetEntity.FieldValue1 = this.tbLeftButtonScene.Text;
                this.DataSourceAsRoomControlWidgetEntity.FieldValue2 = this.tbTopButtonScene.Text;
                this.DataSourceAsRoomControlWidgetEntity.FieldValue3 = this.tbRightButtonScene.Text;
                this.DataSourceAsRoomControlWidgetEntity.FieldValue4 = this.tbBottomButtonScene.Text;
                this.DataSourceAsRoomControlWidgetEntity.FieldValue5 = this.tbSelectButtonScene.Text;
            }

	        this.Validate();

            if (base.Save())
            {
                CustomTextHelper.UpdateCustomTexts(this.DataSource, new CompanyEntity(this.DataSourceAsRoomControlWidgetEntity.RoomControlSectionEntity.RoomControlAreaEntity.RoomControlConfigurationEntity.CompanyId));
                return true;
            }
            else
            {
                return false;
            }
        }

        public override bool SaveAndNew()
        {
            bool result = Save();
            if(result)
            {
                string url = this.Request.Path + "?mode=add";
                if (this.DataSourceAsRoomControlWidgetEntity.RoomControlSectionId > 0)
                    url += string.Format("&RoomControlSectionId={0}", this.DataSourceAsRoomControlWidgetEntity.RoomControlSectionId);

                url += string.Format("&Type={0}", (int)this.DataSourceAsRoomControlWidgetEntity.Type); // Default on first page

                this.Response.Redirect(url, false);
            }
            return result;
        }               

        private void BindUITabTypes()
        {
            foreach (RoomControlWidgetType type in Enum.GetValues(typeof(RoomControlWidgetType)))
            {
                this.cbType.Items.Add(new DevExpress.Web.ListEditItem(type.ToString(), (int)type));
            }       
        }

        private void SetGui()
        {
            if (this.DataSourceAsRoomControlWidgetEntity.IsNew)
                this.DataSourceAsRoomControlWidgetEntity.Visible = true;

            // Bind UITab types depending on the UIModeType parameter
            int type = -1;
            if (QueryStringHelper.HasValue("Type"))
                Int32.TryParse(QueryStringHelper.GetValue("Type"), out type);

            if (this.DataSourceAsRoomControlWidgetEntity.IsNew && type > 0)
                this.DataSourceAsRoomControlWidgetEntity.Type = (RoomControlWidgetType)type;

            this.BindUITabTypes();

            if (!this.IsPostBack)
            {
                this.cbType.Value = (int)this.DataSourceAsRoomControlWidgetEntity.Type;
            }

            if (this.ShowRoomControlComponent(this.DataSourceAsRoomControlWidgetEntity))
            {
                this.plhRoomControlComponent.Visible = true;

                this.ddlRoomControlComponentId1.IsRequired = true;

                this.LoadRoomControlComponents();
            }
            else if (this.ShowRoomControlComponents(this.DataSourceAsRoomControlWidgetEntity))
            {
                this.plhRoomControlComponent.Visible = true;
                this.plhRoomControlComponents.Visible = true;

                this.ddlRoomControlComponentId1.IsRequired = true;
                
                this.LoadRoomControlComponents();
            }
            else if (this.ShowVolume(this.DataSourceAsRoomControlWidgetEntity))
            {
                this.plhVolume.Visible = true;

                this.tbVolumeUpScene.IsRequired = true;
                this.tbVolumeUpScene.Text = this.DataSourceAsRoomControlWidgetEntity.FieldValue1;

                this.tbVolumeDownScene.IsRequired = true;
                this.tbVolumeDownScene.Text = this.DataSourceAsRoomControlWidgetEntity.FieldValue2;

                this.tbVolumeMuteScene.IsRequired = true;
                this.tbVolumeMuteScene.Text = this.DataSourceAsRoomControlWidgetEntity.FieldValue3;

                this.tbPowerScene.IsRequired = true;
                this.tbPowerScene.Text = this.DataSourceAsRoomControlWidgetEntity.FieldValue4;
            }
            else if (this.ShowChannel(this.DataSourceAsRoomControlWidgetEntity))
            {
                this.plhChannel.Visible = true;

                this.tbChannelUpScene.IsRequired = true;
                this.tbChannelUpScene.Text = this.DataSourceAsRoomControlWidgetEntity.FieldValue1;

                this.tbChannelDownScene.IsRequired = true;
                this.tbChannelDownScene.Text = this.DataSourceAsRoomControlWidgetEntity.FieldValue2;
            }
            else if (this.ShowRemoteControl(this.DataSourceAsRoomControlWidgetEntity))
            {
                this.plhRemoteControl.Visible = true;

                this.tbLeftButtonScene.IsRequired = true;
                this.tbLeftButtonScene.Text = this.DataSourceAsRoomControlWidgetEntity.FieldValue1;

                this.tbTopButtonScene.IsRequired = true;
                this.tbTopButtonScene.Text = this.DataSourceAsRoomControlWidgetEntity.FieldValue2;

                this.tbRightButtonScene.IsRequired = true;
                this.tbRightButtonScene.Text = this.DataSourceAsRoomControlWidgetEntity.FieldValue3;

                this.tbBottomButtonScene.IsRequired = true;
                this.tbBottomButtonScene.Text = this.DataSourceAsRoomControlWidgetEntity.FieldValue4;

                this.tbSelectButtonScene.IsRequired = true;
                this.tbSelectButtonScene.Text = this.DataSourceAsRoomControlWidgetEntity.FieldValue5;
            }

            bool saveEntity = true;
            if (QueryStringHelper.HasValue("mode"))
            {
                string mode = QueryStringHelper.GetString("mode");
                saveEntity = mode.Equals("delete", StringComparison.InvariantCultureIgnoreCase);
            }

            if (this.DataSourceAsRoomControlWidgetEntity.IsDirty && !this.DataSourceAsRoomControlWidgetEntity.IsNew && saveEntity)
                this.DataSourceAsRoomControlWidgetEntity.Save();            
        }

	    private bool ShowRoomControlComponent(RoomControlWidgetEntity widget)
	    {
	        return widget.Type == RoomControlWidgetType.Light2x1 ||
	               widget.Type == RoomControlWidgetType.Scene1x1 ||
	               widget.Type == RoomControlWidgetType.Thermostat4x2 ||
                   widget.Type == RoomControlWidgetType.Light1x1;
	    }

        private bool ShowRoomControlComponents(RoomControlWidgetEntity widget)
        {
            return widget.Type == RoomControlWidgetType.Blind1x2 || widget.Type == RoomControlWidgetType.Blind1x1;
        }

	    private bool ShowVolume(RoomControlWidgetEntity widget)
	    {
	        return widget.Type == RoomControlWidgetType.Volume1x2 || widget.Type == RoomControlWidgetType.Volume1x1;
	    }

	    private bool ShowChannel(RoomControlWidgetEntity widget)
	    {
	        return widget.Type == RoomControlWidgetType.Channel1x2 || widget.Type == RoomControlWidgetType.Channel1x1;
	    }

	    private bool ShowRemoteControl(RoomControlWidgetEntity widget)
	    {
	        return widget.Type == RoomControlWidgetType.Navigation1x2;
	    }

	    #endregion

		#region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            this.DataSourceLoaded += RoomControlWidget_DataSourceLoaded;
            base.OnInit(e);
        }

        private void RoomControlWidget_DataSourceLoaded(object sender)
        {
            this.SetGui();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

		#endregion

		#region Properties

		public RoomControlWidgetEntity DataSourceAsRoomControlWidgetEntity
		{
			get
			{
                return this.DataSource as RoomControlWidgetEntity;
			}
		}

		#endregion
	}
}
