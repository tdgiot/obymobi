﻿using System;
using System.Data;
using System.IO;
using Dionysos;
using Dionysos.Drawing;
using Dionysos.Web;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Obymobi.Web.CloudStorage;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Company
{
    public partial class Gallery : Dionysos.Web.UI.PageQueryStringDataBinding
    {
        #region Fields

        #endregion

        #region Methods

        protected override void OnInit(EventArgs e)
        {
            this.SetGui();
            base.OnInit(e);
        }

        protected override void SetDefaultValuesToControls()
        {

        }

        private void AddImage()
        {
            if (this.fuDocument.HasFile && CmsSessionHelper.CurrentCompanyId > 0)
            {
                string fileExtension = System.IO.Path.GetExtension(this.fuDocument.FileName).ToLower();;
                if (fileExtension != ".png" && fileExtension != ".jpg" && fileExtension != ".jpeg" && fileExtension != ".gif" && fileExtension != ".bmp")
                {
                    this.MultiValidatorDefault.AddError(this.Translate("galleryOnlyImageFiles", "Only image files are supported."));
                    this.Validate();
                    return;
                }

                string fileName = this.fuDocument.FileName;

                // Get the bytes from the selected image
                byte[] bytes = this.fuDocument.FileBytes;

                // Convert any other file type than .JPG (.png, .jpeg, .gif, .bmp) to .JPG format
                if (System.IO.Path.GetExtension(fileName).ToLower() != ".jpg")
                {
                    // Change the extension of the filename
                    fileName = System.IO.Path.GetFileNameWithoutExtension(fileName) + ".jpg";

                    // Convert the bytes to a jpg image bytes array
                    bytes = ImageUtil.ConvertToJpg(bytes);
                }

                string filePath = Path.GetFileName(fileName);

                // Get the datasource Media entity
                // and set the file properties
                MediaEntity mediaEntity = new MediaEntity();
                mediaEntity.CompanyId = CmsSessionHelper.CurrentCompanyId;
                mediaEntity.Name = filePath;
                mediaEntity.FilePathRelativeToMediaPath = filePath;
                mediaEntity.SetMimeTypeAndExtension(filePath);
                mediaEntity.MediaType = -1;

                // Get the bytes of the uploaded document
                if (bytes.Length > 0)
                {
                    // Create an image of the bytes
                    MemoryStream memoryStream = new MemoryStream(bytes);
                    System.Drawing.Image image = System.Drawing.Image.FromStream(memoryStream);

                    // Resize the image if it's too big
                    // Had to enlarge this since the largest MediaRatio image is now 3200x1500
                    if (image.Size.Width > 4000 || image.Size.Height > 2000)
                    {
                        bytes = ImageUtil.ResizeImageFile(bytes, 4000, 2000, true, 90);

                        // Reload image into memory since size might be changed
                        image = (System.Drawing.Bitmap)System.Drawing.Image.FromStream(new MemoryStream(bytes));
                    }

                    // Save image size in database
                    mediaEntity.SizeWidth = image.Size.Width;
                    mediaEntity.SizeHeight = image.Size.Height;
                    mediaEntity.SizeKb = Convert.ToInt32(bytes.Length) / 1024;
                    mediaEntity.MediaFileMd5 = Dionysos.Security.Cryptography.Hasher.GetHash(bytes, Dionysos.Security.Cryptography.HashType.MD5);
                }

                mediaEntity.FileBytes = bytes;

                // Save the media entity
                if (mediaEntity.Save())
                {
                    mediaEntity.Refetch();

                    mediaEntity.LastDistributedVersionTicks = mediaEntity.UpdatedUTC.GetValueOrDefault(mediaEntity.CreatedUTC.GetValueOrDefault(mediaEntity.CreatedUTC.GetValueOrDefault())).Ticks;

                    // Upload original image to the cloud                    
                    mediaEntity.Upload(bytes);

                    // Clean old thumbnails
                    ThumbnailGeneratorUtil.CleanUpTempFilesForMedia(mediaEntity.MediaId);                    
                }

                this.ResizeAndPublishMediaRatio(mediaEntity, MediaType.Gallery);
                this.ResizeAndPublishMediaRatio(mediaEntity, MediaType.GalleryLarge);
                this.ResizeAndPublishMediaRatio(mediaEntity, MediaType.GallerySmall);
            }

            WebShortcuts.Redirect("~/Company/Gallery.aspx");
        }

        private void ResizeAndPublishMediaRatio(MediaEntity mediaEntity, MediaType type)
        {
            MediaRatioType mediaRatioType = MediaRatioTypes.GetMediaRatioType(type);
            if (mediaRatioType != null)
            {
                MediaRatioTypeMediaEntity mrtm = new MediaRatioTypeMediaEntity();
                mrtm.MediaId = mediaEntity.MediaId;
                mrtm.MediaTypeAsEnum = mediaRatioType.MediaType;
                MediaHelper.SetDefaultCutout(mediaEntity, mrtm);
                mrtm.Save();

                MediaHelper.UploadToCdn(mrtm, mediaEntity);                
            }
        }

        void SetGui()
        {
            DataTable table = new DataTable();
            table.Columns.Add(new DataColumn("ImageUrl"));
            table.Columns.Add(new DataColumn("MediaUrl"));
            table.Columns.Add(new DataColumn("Name"));

            PredicateExpression mediaFilter = new PredicateExpression();
            mediaFilter.Add(MediaRatioTypeMediaFields.MediaType == (int)MediaType.Gallery);
            mediaFilter.AddWithOr(MediaRatioTypeMediaFields.MediaType == (int)MediaType.GalleryLarge);
            mediaFilter.AddWithOr(MediaRatioTypeMediaFields.MediaType == (int)MediaType.GallerySmall);

            PredicateExpression filter = new PredicateExpression();
            filter.Add(MediaFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            filter.Add(mediaFilter);

            RelationCollection relations = new RelationCollection();
            relations.Add(MediaEntityBase.Relations.MediaRatioTypeMediaEntityUsingMediaId);

            MediaCollection mediaCollection = new MediaCollection();
            mediaCollection.GetMulti(filter, relations);

            foreach (MediaEntity media in mediaCollection)
            {
                DataRow row = table.NewRow();
                row["MediaUrl"] = this.ResolveUrl(string.Format("~/Generic/Media.aspx?id={0}", media.MediaId));
                string filePathRelative = "~/Generic/ThumbnailGenerator.ashx?mediaId={0}&width={1}&height={2}&fitInDimensions=false".FormatSafe(media.MediaId, 0, 150);
                row["ImageUrl"] = filePathRelative;                
                row["Name"] = media.Name;
                table.Rows.Add(row);
            }

            this.dvGallery.DataSource = table;
            this.dvGallery.DataBind();
        }

        private void HookUpEvents()
        {
            this.btnAddImage.Click += this.btnAddImage_Click;
        }

        #endregion

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            this.HookUpEvents();
        }

        private void btnAddImage_Click(object sender, EventArgs e)
        {
            this.AddImage();
        }

        #endregion
    }
}