﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.UIFooterItem" Codebehind="UIFooterItem.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
                    <D:PlaceHolder runat="server">
                    <table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblName">Name</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblVisible">Visible</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:CheckBox runat="server" ID="cbVisible" />
							</td>
					    </tr>
                        <tr>
                            <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblPosition">Footerbar Position</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
                                <X:ComboBoxInt ID="cbPosition" runat="server" IsRequired="true" UseDataBinding="true" DisplayEmptyItem="False"></X:ComboBoxInt>
							</td>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblSortOrder">Sort Order</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <D:TextBoxInt ID="tbSortOrder" runat="server" IsRequired="true"></D:TextBoxInt>
                            </td>
                        </tr>
                    </table>
                    <D:Panel ID="pnlActions" runat="server" GroupingText="Item On-Click Action">
                        <table class="dataformV2">
                            <tr>
                                <td class="label">
							    	<D:LabelEntityFieldInfo runat="server" id="lblActionIntent">Action</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
                                    <X:ComboBoxInt ID="cbActionIntent" runat="server" UseDataBinding="true"></X:ComboBoxInt>
							    </td>
                                <td class="label">    
                                </td>
                                <td class="control">
                                </td>
                            </tr>
                        </table>
                    </D:Panel>
                    </D:PlaceHolder>
				</Controls>
			</X:TabPage>	
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>