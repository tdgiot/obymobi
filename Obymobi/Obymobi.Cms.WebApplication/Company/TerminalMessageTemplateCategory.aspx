﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.TerminalMessageTemplateCategory" Codebehind="TerminalMessageTemplateCategory.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server" />
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server" />
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<X:PageControl Id="tabsMain" runat="server" Width="100%">
	<TabPages>
		<X:TabPage Text="Algemeen" Name="Generic">
			<Controls>
				<table class="dataformV2">
					<tr>
						<td class="label">
							<D:Label runat="server" id="lblTerminalId">Terminal</D:Label>
						</td>
						<td class="control">
                            <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlTerminalId" IncrementalFilteringMode="StartsWith" EntityName="Terminal" TextField="Name" ValueField="TerminalId" PreventEntityCollectionInitialization="true" />
                        </td>
						<td class="label">
                            <D:Label runat="server" id="lblMessageTemplateId">Message template</D:Label>
						</td>
						<td class="control">
                            <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlMessageTemplateCategoryId" IncrementalFilteringMode="StartsWith" EntityName="MessageTemplateCategory" TextField="Name" ValueField="MessageTemplateCategoryId" PreventEntityCollectionInitialization="true" />
                        </td>        
					</tr> 
				 </table>			
			</Controls>
		</X:TabPage>
	</TabPages>
</X:PageControl>
</asp:Content>