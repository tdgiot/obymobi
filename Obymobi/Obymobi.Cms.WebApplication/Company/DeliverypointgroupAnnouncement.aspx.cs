using System;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic.Cms;

namespace Obymobi.ObymobiCms.Company
{
    public partial class DeliverypointgroupAnnouncement : Dionysos.Web.UI.PageLLBLGenEntity
    {
        #region EventHandlers

        protected override void OnInit(EventArgs e)
        {
            this.DataSourceLoaded += this.DeliverypointgroupAnnouncement_DataSourceLoaded;
            base.OnInit(e);
        }

        private void DeliverypointgroupAnnouncement_DataSourceLoaded(object sender)
        {
            this.Response.Redirect("~/Company/Announcement.aspx?id=" + ((DeliverypointgroupAnnouncementEntity)this.DataSource).AnnouncementId);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        #endregion
    }
}
