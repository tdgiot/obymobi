﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.ClientConfiguration" Codebehind="ClientConfiguration.aspx.cs" %>
<%@ Reference VirtualPath="~/Generic/UserControls/CustomTextCollection.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%" EnableViewState="True">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:Label runat="server" id="lblName">Name</D:Label> 
							</td>
							<td class="control">
							    <D:TextBoxString runat="server" ID="tbName" />
							</td>
                            <td class="label">
                                <D:Label runat="server" id="lblDeliverypointgroupId">Deliverypointgroup</D:Label>
                            </td>
                            <td class="control">
                                <X:ComboBoxInt runat="server" ID="cbDeliverypointgroupId" CssClass="input" TextField="Name" ValueField="DeliverypointgroupId" IncrementalFilteringMode="Contains" IncrementalFilteringDelay="1000"></X:ComboBoxInt>
                            </td>
					    </tr>    
                        <tr>
                            <td class="label">
                                <D:Label runat="server" id="lblUIModeId">UI mode</D:Label>
                            </td>
                            <td class="control">
                                <X:ComboBoxInt runat="server" ID="cbUIModeId" IsRequired="True" CssClass="input" TextField="Name" ValueField="UIModeId" IncrementalFilteringMode="Contains" IncrementalFilteringDelay="1000"></X:ComboBoxInt>
                            </td>
                            <td class="label">
                                <D:Label runat="server" id="lblMenuId">Menu</D:Label>
                            </td>
                            <td class="control">
                                <X:ComboBoxInt runat="server" ID="cbMenuId" CssClass="input" IsRequired="True" TextField="Name" ValueField="MenuId" IncrementalFilteringMode="Contains" IncrementalFilteringDelay="1000"></X:ComboBoxInt>
                            </td>
                        </tr>    
                        <tr>
                            <td class="label">
                                <D:Label runat="server" id="lblUIScheduleId">UI schedule</D:Label>
                            </td>
                            <td class="control">
                                <X:ComboBoxInt runat="server" ID="cbUIScheduleId" CssClass="input" TextField="Name" ValueField="UIScheduleId" IncrementalFilteringMode="Contains" IncrementalFilteringDelay="1000"></X:ComboBoxInt>
                            </td>
                            <td class="label">
                                <D:Label runat="server" id="lblUIThemeId">UI theme</D:Label>
                            </td>
                            <td class="control">
                                <X:ComboBoxInt runat="server" ID="cbUIThemeId" CssClass="input" TextField="Name" ValueField="UIThemeId" IncrementalFilteringMode="Contains" IncrementalFilteringDelay="1000"></X:ComboBoxInt>
                            </td>
                        </tr>    
                        <tr>
                            <td class="label">
                                <D:Label runat="server" id="lblPriceScheduleId">Price schedule</D:Label>
                            </td>
                            <td class="control">
                                <X:ComboBoxInt runat="server" ID="cbPriceScheduleId" CssClass="input" TextField="Name" ValueField="PriceScheduleId" IncrementalFilteringMode="Contains" IncrementalFilteringDelay="1000"></X:ComboBoxInt>
                            </td>
                            <td class="label">
                                <D:Label runat="server" id="lblRoomControlConfigurationId">Room control configuration</D:Label>
                            </td>
                            <td class="control">
                                <X:ComboBoxInt runat="server" ID="cbRoomControlConfigurationId" CssClass="input" TextField="Name" ValueField="RoomControlConfigurationId" IncrementalFilteringMode="Contains" IncrementalFilteringDelay="1000"></X:ComboBoxInt>
                            </td>
                        </tr>    
                        <tr>
                            <td class="label">
                                <D:Label runat="server" id="lblEntertainmentConfigurationId">Entertainment configuration</D:Label>
                            </td>
                            <td class="control">
                                <X:ComboBoxInt runat="server" ID="cbEntertainmentConfigurationId" CssClass="input" TextField="Name" ValueField="EntertainmentConfigurationId" IncrementalFilteringMode="Contains" IncrementalFilteringDelay="1000"></X:ComboBoxInt>
                            </td>
                            <td class="label">
                                <D:Label runat="server" id="lblAdvertisementConfigurationId">Advertisement configuration</D:Label>
                            </td>
                            <td class="control">
                                <X:ComboBoxInt runat="server" ID="cbAdvertisementConfigurationId" CssClass="input" TextField="Name" ValueField="AdvertisementConfigurationId" IncrementalFilteringMode="Contains" IncrementalFilteringDelay="1000"></X:ComboBoxInt>
                            </td>            
                        </tr>    
                        <tr>
                            <td class="label">
                                <D:Label runat="server" id="lblWifiConfigurationId">Wifi configuration</D:Label>
                            </td>
                            <td class="control">
                                <X:ComboBoxInt runat="server" ID="cbWifiConfigurationId" CssClass="input" TextField="Ssid" ValueField="WifiConfigurationId" IncrementalFilteringMode="Contains" IncrementalFilteringDelay="1000"></X:ComboBoxInt>
                            </td>         
                        </tr>
					 </table>
				</Controls>
			</X:TabPage>	
            <X:TabPage Text="Advanced" Name="advanced">
                <controls>
                    <D:Panel ID="pnlTimers" runat="server" GroupingText="Timers">
                        <table class="dataformV2">
                            <tr>
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblDailyOrderReset">Daily order reset</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
							        <X:TimeEdit runat="server" ID="teDailyOrderReset" UseDataBinding="false"></X:TimeEdit>
							    </td>        
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblHomepageSlideshowInterval">Homepage slideshow interval (seconds)</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
							        <D:TextBoxInt runat="server" ID="tbHomepageSlideshowInterval" />
							    </td>                                        
                            </tr>
                            <tr>
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblScreenTimeoutInterval">Idle timer (seconds)</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
							        <D:TextBoxInt runat="server" ID="tbScreenTimeoutInterval"></D:TextBoxInt>
							    </td>        
                            </tr>
                            <tr>
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblResetTimeout">Reset timer (minutes)</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
							        <D:TextBoxInt runat="server" ID="tbResetTimeout"></D:TextBoxInt>
							    </td>        
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblClearSessionOnTimeout">Clear session on reset</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
							        <D:CheckBox runat="server" ID="cbClearSessionOnTimeout"></D:CheckBox>
							    </td>        
                            </tr>                            
                        </table>
                    </D:Panel>
                    <D:Panel ID="pnlSecurity" runat="server" GroupingText="Security">
					    <table class="dataformV2">
                            <tr>
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblPincode">Pincode</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
							        <D:TextBoxString runat="server" ID="tbPincode" />
							    </td>        
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblPincodeGM">Pincode godmode</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
                                    <D:TextBoxString runat="server" ID="tbPincodeGM" />
							    </td>   				
                            </tr>		
                            <tr>
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblPincodeSU">Pincode superuser</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
							        <D:TextBoxString runat="server" ID="tbPincodeSU" />
							    </td>       
                            </tr>				
					     </table>			
                    </D:Panel>
                    <D:Panel ID="pnlPricing" runat="server" GroupingText="Pricing">
                        <table class="dataformV2">
                            <tr>
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblHidePrices">Hide prices</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
							        <D:CheckBox runat="server" ID="cbHidePrices" />
							    </td>        
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblShowCurrencySymbol">Show currency symbol</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
							        <D:CheckBox runat="server" ID="cbShowCurrencySymbol" />
							    </td>        
                            </tr>
                            <tr>
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblRoomserviceCharge">Room service charge</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
							        <D:TextBoxDecimal runat="server" ID="tbRoomserviceCharge" />
							    </td>        
                            </tr>
                        </table>
                    </D:Panel>
                    <D:Panel ID="pnlRestart" runat="server" GroupingText="Restart / reboot">
                        <table class="dataformV2">
                            <tr>
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblRestartApplicationDialogEnabled">Show restart dialog</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
							        <D:CheckBox runat="server" ID="cbRestartApplicationDialogEnabled" />
							    </td>        
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblRebootDeviceDialogEnabled">Show reboot dialog</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
							        <D:CheckBox runat="server" ID="cbRebootDeviceDialogEnabled" />
							    </td>        
                            </tr>
                            <tr>
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblRestartTimeoutSeconds">Restart/reboot timeout (seconds)</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
							        <D:TextBoxInt runat="server" ID="tbRestartTimeoutSeconds" />
							    </td>        
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblDeviceRebootMethod">Device reboot method</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
							        <X:ComboBoxInt runat="server" ID="ddlDeviceRebootMethod" UseDataBinding="True" DisplayEmptyItem="False"></X:ComboBoxInt>         
							    </td>        
                            </tr>
                        </table>
                    </D:Panel>
                    <D:Panel ID="pnlMisc" runat="server" GroupingText="Misc">
                        <table class="dataformV2">
                            <tr>
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblAllowFreeText">Allow free text</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
							        <D:CheckBox runat="server" ID="cbAllowFreeText" />
							    </td>        
                                <td class="label">
							        <D:Label runat="server" id="lblGooglePrinterId">Google printer</D:Label>
							    </td>
							    <td class="control">
                                    <X:ComboBoxString runat="server" ID="ddlGooglePrinterId" IncrementalFilteringMode="StartsWith" TextField="Name" ValueField="PrinterId"></X:ComboBoxString>
                                    <D:LinkButton ID="btSelectPrinter" runat="server" Text="Load printers" LocalizeText="false" ForeColor="Black" />
                                    <D:LinkButton ID="btRefreshPrinters" runat="server" Text="Refresh printers" LocalizeText="false" ForeColor="Black" />
							    </td>
                            </tr>
                            <tr>
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblBrowserAgeVerificationEnabled">Browser age verification</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
							        <D:CheckBox runat="server" ID="cbBrowserAgeVerificationEnabled" />
							    </td>        
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblBrowserAgeVerificationLayout">Browser age verification layout</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
							        <X:ComboBoxInt runat="server" ID="ddlBrowserAgeVerificationLayout" DisplayEmptyItem="false"></X:ComboBoxInt>
							    </td>                                    
                            </tr>                            
                        </table>
                    </D:Panel>
				</Controls>
			</X:TabPage>   
            <X:TabPage Text="Notifications" Name="tabNotifications">
                <controls>
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:Label runat="server" id="lblIsChargerRemovedDialogEnable">Charger removed dialog</D:Label>
                            </td>
                            <td class="control">
                                <D:CheckBox runat="server" ID="cbIsChargerRemovedDialogEnabled" />
                            </td>
                            <td class="label">
                                <D:Label runat="server" id="lblOrderCompletedNotificationEnable">Order complete verification dialog</D:Label>
                            </td>
                            <td class="control">
                                <D:CheckBox runat="server" ID="cbOrderCompletedNotificationEnabled" />
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                <D:Label runat="server" id="lblIsChargerRemoveReminderDialogEnabled">Charger removed reminder dialog</D:Label>
                            </td>
                            <td class="control">
                                <D:CheckBox runat="server" ID="cbIsChargerRemovedReminderDialogEnabled" />
                            </td>
                            <td class="label">
                                <D:Label runat="server" id="lblIsOrderitemAddedDialogEnabled">Orderitem added dialog</D:Label>
                            </td>
                            <td class="control">
                                <D:CheckBox runat="server" ID="cbIsOrderitemAddedDialogEnabled" />
                            </td>
                            
                        </tr>
                    </table>                            
                </controls>         
            </X:TabPage>
            <X:TabPage Text="Power management" Name="tabPowerManagement">
                <controls>
                    <D:Panel ID="pnlCharging" runat="server" GroupingText="Charging settings">
                        <table class="dataformV2">
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblOutOfChargeLevel">Battery out of charge %</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">								
                                    <D:TextBoxInt runat="server" ID="tbOutOfChargeLevel" allowZero="true"></D:TextBoxInt>
							    </td>    
                                <td class="label"
                                </td>
                                <td class="control"
                                </td>
                            </tr>                                                        
                        </table>
                    </D:Panel>
                    <D:Panel ID="pnlPowerSave" runat="server" GroupingText="Power save">
				        <table class="dataformV2">
                            <tr>
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblPowerSaveTimeout">Power save (minutes)</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
			                        <D:TextBoxInt runat="server" ID="tbPowerSaveTimeout" allowZero="true"></D:TextBoxInt>
							    </td>        
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblPowerSaveLevel">Power save brightness</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
			                        <D:TextBoxInt runat="server" ID="tbPowerSaveLevel" allowZero="true"></D:TextBoxInt>
							    </td>        				
						    </tr>
                             <tr>
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblScreensaverMode">Screensaver</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
							        <X:ComboBoxInt runat="server" ID="ddlScreensaverMode" DisplayEmptyItem="false"></X:ComboBoxInt>
							    </td>  
                                <td class="label">
								
							    </td>
							    <td class="control">
			                    
							    </td>
						    </tr>
					    </table>			
                    </D:Panel>
                    <D:Panel ID="pnlNightMode" runat="server" GroupingText="Night mode">
				        <table class="dataformV2">
                            <tr>
                                <td class="label">
								    <D:Label runat="server" id="lblSleepTime">Sleep time</D:Label>
							    </td>
							    <td class="control">
							        <X:TimeEdit runat="server" ID="teSleepTime" UseDataBinding="false"></X:TimeEdit>
							    </td>  
                                <td class="label">
								    <D:Label runat="server" id="lblWakeUpTime">Wake up time</D:Label>
							    </td>
							    <td class="control">
							        <X:TimeEdit runat="server" ID="teWakeUpTime" UseDataBinding="false"></X:TimeEdit>
							    </td>  
						    </tr>      
					    </table>			
                    </D:Panel>
                    <D:Panel ID="pnlBrightness" runat="server" GroupingText="Screen brightness">
                        <table class="dataformV2">
						    <tr>
                                <td class="label">
							    </td>
							    <td class="control">
							        <strong><D:Label runat="server" id="lblDeviceDefault" TranslationTag="lblDeviceDefault" LocalizeText="true">Default</D:Label></strong>
							    </td>  
                                <td class="label">
							    </td>
							    <td class="control">
							        <strong><D:Label runat="server" id="lblDeviceDocked" TranslationTag="lblDeviceDocked" LocalizeText="true">Docked</D:Label></strong>
							    </td>  
						    </tr>
                            <tr>
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblDimLevelBright">Bright brightness</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
							        <D:TextBoxInt runat="server" ID="tbDimLevelBright" allowZero="true"></D:TextBoxInt>
							    </td>
							
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblDockedDimLevelBright">Bright brightness</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
							        <D:TextBoxInt runat="server" ID="tbDockedDimLevelBright" allowZero="true"></D:TextBoxInt>
							    </td>
						    </tr>
						    <tr>
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblDimLevelMedium">Medium brightness</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
							        <D:TextBoxInt runat="server" ID="tbDimLevelMedium" allowZero="true"></D:TextBoxInt>
							    </td>  
							
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblDockedDimLevelMedium">Medium brightness</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
							        <D:TextBoxInt runat="server" ID="tbDockedDimLevelMedium" allowZero="true"></D:TextBoxInt>
							    </td> 
						    </tr>
						    <tr>
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblDimLevelDull">Dull brightness</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
							        <D:TextBoxInt runat="server" ID="tbDimLevelDull" allowZero="true"></D:TextBoxInt>
							    </td> 
							
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblDockedDimLevelDull">Dull brightness</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
							        <D:TextBoxInt runat="server" ID="tbDockedDimLevelDull" allowZero="true"></D:TextBoxInt>
							    </td> 
						    </tr>
                        </table>
                    </D:Panel>
                    <D:Panel ID="pnlScreenBehaviour" runat="server" GroupingText="Screen power behaviour">
                        <table class="dataformV2">
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblPowerButtonHardBehaviour">Hard power button</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <X:ComboBoxInt runat="server" ID="ddlPowerButtonHardBehaviour" DisplayEmptyItem="false"></X:ComboBoxInt>
                                </td>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblScreenOffMode">Screen off mode</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <X:ComboBoxInt runat="server" ID="ddlScreenOffMode" DisplayEmptyItem="false"></X:ComboBoxInt>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblPowerButtonSoftBehaviour">Soft power button</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <X:ComboBoxInt runat="server" ID="ddlPowerButtonSoftBehaviour" DisplayEmptyItem="false"></X:ComboBoxInt>
                                </td>
                                <td class="label">
                                    
                                </td>
                                <td class="control">
                                    
                                </td>
                            </tr>
                        </table>
                    </D:Panel>                             
				</Controls>
			</X:TabPage>            
            <X:TabPage Text="Routing" Name="Routing">
                <controls>
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:Label runat="server" ID="lblNormalRoute">Normal route</D:Label>
                            </td>
                            <td class="control">
                                <X:ComboBoxInt runat="server" ID="cbNormalRoute" CssClass="input" TextField="Name" ValueField="RouteId" IncrementalFilteringMode="Contains" IncrementalFilteringDelay="1000"></X:ComboBoxInt>
                            </td>
                            <td class="label">
                                <D:Label runat="server" ID="lblSystemMessageRoute">System message route</D:Label>
                            </td>
                            <td class="control">
                                <X:ComboBoxInt runat="server" ID="cbSystemMessageRoute" CssClass="input" TextField="Name" ValueField="RouteId" IncrementalFilteringMode="Contains" IncrementalFilteringDelay="1000"></X:ComboBoxInt>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                <D:Label runat="server" ID="lblOrderNotesRoute">Order notes route</D:Label>
                            </td>
                            <td class="control">
                                <X:ComboBoxInt runat="server" ID="cbOrderNotesRoute" CssClass="input" TextField="Name" ValueField="RouteId" IncrementalFilteringMode="Contains" IncrementalFilteringDelay="1000"></X:ComboBoxInt>    
                            </td>
                            <td class="label">
                                <D:Label runat="server" ID="lblEmailDocumentRoute">Email document route</D:Label>
                            </td>
                            <td class="control">
                                <X:ComboBoxInt runat="server" ID="cbEmailDocumentRoute" CssClass="input" TextField="Name" ValueField="RouteId" IncrementalFilteringMode="Contains" IncrementalFilteringDelay="1000"></X:ComboBoxInt>
                            </td>
                        </tr>
                    </table>
                </controls>
            </X:TabPage>
            <X:TabPage Text="PMS" Name="PMS">
                <controls>
					<table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblPmsIntegration">PMS integration</D:LabelEntityFieldInfo>
							</td>
							<td class="control">								
                                <D:CheckBox runat="server" ID="cbPmsIntegration" />
							</td>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblPmsAllowShowBill">Allow show bill</D:LabelEntityFieldInfo>
							</td>
							<td class="control">								
                                <D:CheckBox runat="server" ID="cbPmsAllowShowBill" />
							</td>
                        </tr>						
                        <tr>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblPmsAllowExpressCheckout">Allow express checkout</D:LabelEntityFieldInfo>
							</td>
							<td class="control">								
                                <D:CheckBox runat="server" ID="cbPmsAllowExpressCheckout" />
							</td>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblPmsAllowShowGuestName">Show guest name</D:LabelEntityFieldInfo>
							</td>
							<td class="control">								
                                <D:CheckBox runat="server" ID="cbPmsAllowShowGuestName" />
							</td>
                        </tr>
                        <tr>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblPmsLockClientWhenNotCheckedIn">Lock on checkout</D:LabelEntityFieldInfo>
							</td>
							<td class="control">								
                                <D:CheckBox runat="server" ID="cbPmsLockClientWhenNotCheckedIn" />
							</td>
                        </tr>
                        <tr>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblPmsWelcomeTimeoutMinutes">Welcome timeout (minutes)</D:LabelEntityFieldInfo>
							</td>
							<td class="control">								
                                <D:TextBoxInt runat="server" ID="tbPmsWelcomeTimeoutMinutes" />
							</td>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblPmsCheckoutCompleteTimeoutMinutes">Checkout complete timeout (minutes)</D:LabelEntityFieldInfo>
							</td>
							<td class="control">								
                                <D:TextBoxInt runat="server" ID="tbPmsCheckoutCompleteTimeoutMinutes" />
							</td>
                        </tr>
					 </table>			
				</Controls>
			</X:TabPage>      
            <X:TabPage Text="Linked" Name="Linked">
                <controls>
                    <D:PlaceHolder runat="server" ID="plhLinkedEntities"></D:PlaceHolder>
                </controls>
            </X:TabPage>
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>

