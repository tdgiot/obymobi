﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dionysos.Web.UI;
using Obymobi.Data.HelperClasses;
using System.Data;
using Obymobi.Data.EntityClasses;
using Dionysos;
using Obymobi.Data.CollectionClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Logic.HelperClasses;
using Obymobi.Data;

namespace Obymobi.ObymobiCms.Company
{
    public partial class AddClient : PageDefault
    {
        #region Methods

        protected override void OnInit(EventArgs e)
        {
            this.SetGui();
            base.OnInit(e);
        }

        private void HookupEvents()
        {
            this.btAddClient.Click += new EventHandler(btAddClient_Click);
        }

        private void SetGui()
        {
            int companyId = CmsSessionHelper.CurrentCompanyId;

            this.ddlCompanyId.Value = companyId;

            PredicateExpression deliverypointgroupFilter = new PredicateExpression(DeliverypointgroupFields.CompanyId == companyId);

            DeliverypointgroupCollection deliverypointgroups = new DeliverypointgroupCollection();
            deliverypointgroups.GetMulti(deliverypointgroupFilter);

            this.ddlDeliverypointGroupId.DataSource = deliverypointgroups;
            this.ddlDeliverypointGroupId.DataBind();

            if (deliverypointgroups.Count == 1)
                this.ddlDeliverypointGroupId.Value = deliverypointgroups[0].DeliverypointgroupId;

            this.btAddClient.PreSubmitWarning = this.Translate("PreSubmitWarning", "Weet u zeker dat u het Otoucho scherm wilt aanmaken?");
        }

        private void CreateClient()
        {
            // Create the transaction object, pass the isolation level and give it a name
            Transaction transactionManager = new Transaction(IsolationLevel.ReadUncommitted, "Client");

            bool isSuccess = false;
            try
            {
                ClientEntity client = this.CreateClient(transactionManager);
                // GK Not required, is done in the Injectable.
                //CustomerEntity customer = this.CreateCustomer(transactionManager, client);
                
                // Commit the transaction
                transactionManager.Commit();
                isSuccess = true;
            }
            catch(ObymobiException ex)
            {
                // abort, roll back the transaction
                transactionManager.Rollback();
    
                if(ex.ErrorEnumValue.Equals(ClientSaveResult.NonUniqueSerialNumber))
                {
                    this.MultiValidatorDefault.AddError(this.Translate("AddClientNonUniqueSerialNumber", "Er bestaat al een client met dit serienummer."));
                    this.Validate();
                }
                else if (ex.ErrorEnumValue.Equals(ClientSaveResult.MultipleClientsForDevice))
                {
                    this.MultiValidatorDefault.AddError(this.Translate("AddClientMultipleClientsForDevice", "Er bestaat al een client die gekoppeld is aan dit apparaat."));
                    this.Validate();
                }
            }
            catch (Exception)
            {
                // abort, roll back the transaction
                transactionManager.Rollback();
                throw;
            }
            finally
            {
                // clean up. Necessary action.
                transactionManager.Dispose();

                if (isSuccess)
                {
                    // Show information bar
                    this.AddInformatorInfo(this.Translate("ClientAdded", "Het nieuwe scherm is aangemaakt."));

                    // Clear fields
                    ClearFields();
                }
            }            
        }

        private ClientEntity CreateClient(Transaction transactionManager)
        {
            int? deliverypointGroupId = null;
            if (this.ddlDeliverypointGroupId.ValidId > -1)
                deliverypointGroupId = this.ddlDeliverypointGroupId.ValidId;

            // Create the client entity
            ClientEntity client = ClientHelper.CreateClientEntity(string.Empty, this.ddlCompanyId.ValidId, deliverypointGroupId, this.tbTeamviewerId.Value, this.tbConfiguration.Text, this.tbNotes.Text);

            // Create the device entity
            DeviceEntity device = null;
            if (!this.tbIdentifier.Text.IsNullOrWhiteSpace())
            {
                PrefetchPath prefetch = new PrefetchPath(EntityType.DeviceEntity);
                prefetch.Add(DeviceEntity.PrefetchPathClientCollection);

                device = DeviceHelper.GetDeviceEntityByIdentifier(this.tbIdentifier.Text, true, prefetch, DeviceType.Unknown);
                if (device.ClientCollection.Count == 0)
                    client.DeviceId = device.DeviceId;
                else
                    throw new ObymobiException(ClientSaveResult.MultipleClientsForDevice, "There is already a client with id '{0}' connected to device with identifier '{1}'!", device.ClientCollection[0].ClientId, device.Identifier); 
            }            

            // Add the client to the transaction
            transactionManager.Add(client);

            // And save the entity
            client.Save();

            return client;
        }

        private DeviceEntity CreateDevice(Transaction transactionManager, string identifier)
        {
            // Create the device entity
            DeviceEntity device = new DeviceEntity();
            device.Identifier = identifier;
            transactionManager.Add(device);
            device.Save();

            return device;
        }

        private CustomerEntity CreateCustomer(Transaction transactionManager, ClientEntity clientEntity)
        {
            // Create the customer entity
            CustomerEntity customer = new CustomerEntity();
            customer.Phonenumber = "0000000000";
            customer.Password = "w9NnDYGVvVk4JnQ6w2DIwRgFW3U=";
            customer.ClientEntity = clientEntity;
            customer.Firstname = clientEntity.CompanyEntity.Name;

            transactionManager.Add(customer);
            customer.Save();

            return customer;
        }

        private void CustomValidation()
        {
            if (this.ddlDeliverypointGroupId.ValidId == -1)
                this.MultiValidatorDefault.AddError("U dient een tafel of tafelgroep te selecteren");

            this.Validate();
        }

        private void ClearFields()
        {
            this.tbIdentifier.Value = "";
            this.tbTeamviewerId.Value = "";
            this.tbConfiguration.Text = "";
            this.tbNotes.Text = "";
        }

        #endregion

        #region Event handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            this.HookupEvents();
        }

        private void btAddClient_Click(object sender, EventArgs e)
        {
            this.CustomValidation();

            if (this.IsValid)
                this.CreateClient();
        }

        #endregion
    }
}