﻿using System;
using System.Collections.Generic;
using System.Linq;
using Crave.Api.Logic.Requests.Publishing;
using Crave.Api.Logic.Timestamps;
using Crave.Api.Logic.UseCases;
using Dionysos;
using Dionysos.Web.UI;
using Dionysos.Web.UI.DevExControls;
using Obymobi.Cms.Logic.Factories;
using Obymobi.Cms.Logic.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;

namespace Obymobi.ObymobiCms.Company
{
    public partial class ClientConfiguration : Dionysos.Web.UI.PageLLBLGenEntity
	{
        #region Properties

        public ClientConfigurationEntity DataSourceAsClientConfigurationEntity
        {
            get
            {
                return this.DataSource as ClientConfigurationEntity;
            }
        }

        #endregion

        #region Methods

        private void LoadUserControls()
        {
            // Translations tab
            Generic.UserControls.CustomTextCollection translationsPanel = this.tabsMain.AddTabPage("Translations", "CustomTextCollection", "~/Generic/UserControls/CustomTextCollection.ascx") as Generic.UserControls.CustomTextCollection;

            // Generic tab
            this.cbDeliverypointgroupId.DataBindEntityCollection<DeliverypointgroupEntity>(DeliverypointgroupFields.CompanyId == CmsSessionHelper.CurrentCompanyId, DeliverypointgroupFields.Name, DeliverypointgroupFields.Name);
            this.cbMenuId.DataBindEntityCollection<MenuEntity>(MenuFields.CompanyId == CmsSessionHelper.CurrentCompanyId, MenuFields.Name, MenuFields.Name);
            this.cbUIModeId.DataBindEntityCollection<UIModeEntity>(UIModeFields.CompanyId == CmsSessionHelper.CurrentCompanyId, UIModeFields.Name, UIModeFields.Name);
            this.cbUIThemeId.DataBindEntityCollection<UIThemeEntity>(null, UIThemeFields.Name, UIThemeFields.Name);
            this.cbUIScheduleId.DataBindEntityCollection<UIScheduleEntity>(PredicateFactory.CreateUIScheduleFilter(CmsSessionHelper.CurrentCompanyId, UIScheduleType.Legacy), UIScheduleFields.Name, UIScheduleFields.Name);
            this.cbRoomControlConfigurationId.DataBindEntityCollection<RoomControlConfigurationEntity>(RoomControlConfigurationFields.CompanyId == CmsSessionHelper.CurrentCompanyId, RoomControlConfigurationFields.Name, RoomControlConfigurationFields.Name);
            this.cbWifiConfigurationId.DataBindEntityCollection<WifiConfigurationEntity>(WifiConfigurationFields.CompanyId == CmsSessionHelper.CurrentCompanyId, WifiConfigurationFields.Ssid, WifiConfigurationFields.Ssid);
            this.cbEntertainmentConfigurationId.DataBindEntityCollection<EntertainmentConfigurationEntity>(EntertainmentConfigurationFields.CompanyId == CmsSessionHelper.CurrentCompanyId, EntertainmentConfigurationFields.Name, EntertainmentConfigurationFields.Name);
            this.cbAdvertisementConfigurationId.DataBindEntityCollection<AdvertisementConfigurationEntity>(AdvertisementConfigurationFields.CompanyId == CmsSessionHelper.CurrentCompanyId, AdvertisementConfigurationFields.Name, AdvertisementConfigurationFields.Name);
            this.cbPriceScheduleId.DataBindEntityCollection<PriceScheduleEntity>(PriceScheduleFields.CompanyId == CmsSessionHelper.CurrentCompanyId, PriceScheduleFields.Name, PriceScheduleFields.Name);

            // Advanced tab
            this.ddlDeviceRebootMethod.DataBindEnum<DeviceRebootMethod>();
            this.ddlBrowserAgeVerificationLayout.DataBindEnum<BrowserAgeVerificationLayoutType>();

            // Power management tab
            this.ddlScreensaverMode.DataBindEnum<ScreensaverMode>();
            this.ddlScreenOffMode.DataBindEnum<ScreenOffMode>();
            this.ddlPowerButtonHardBehaviour.DataBindEnum<PowerButtonMode>();
            this.ddlPowerButtonSoftBehaviour.DataBindEnum<PowerButtonMode>();

            // Routes tab
            RouteCollection routes = RouteHelper.GetRouteCollection(CmsSessionHelper.CurrentCompanyId);
            RouteCollection documentRoutes = RouteHelper.GetRouteCollection(CmsSessionHelper.CurrentCompanyId, true);

            this.cbNormalRoute.DataSource = routes;
            this.cbNormalRoute.DataBind();

            this.cbOrderNotesRoute.DataSource = routes;
            this.cbOrderNotesRoute.DataBind();

            this.cbSystemMessageRoute.DataSource = routes;
            this.cbSystemMessageRoute.DataBind();

            this.cbEmailDocumentRoute.DataSource = documentRoutes;
            this.cbEmailDocumentRoute.DataBind();
        }        

        private void SetGui()
		{
            if (!this.DataSourceAsClientConfigurationEntity.DailyOrderReset.IsNullOrWhiteSpace()) this.teDailyOrderReset.Value = this.GetTimeFromString(this.DataSourceAsClientConfigurationEntity.DailyOrderReset);
            if (!this.DataSourceAsClientConfigurationEntity.SleepTime.IsNullOrWhiteSpace()) this.teSleepTime.Value = this.GetTimeFromString(this.DataSourceAsClientConfigurationEntity.SleepTime);
            if (!this.DataSourceAsClientConfigurationEntity.WakeUpTime.IsNullOrWhiteSpace()) this.teWakeUpTime.Value = this.GetTimeFromString(this.DataSourceAsClientConfigurationEntity.WakeUpTime);

            int normalRouteId = this.GetClientConfigurationRoute(RouteType.Normal).RouteId;
            int orderNotesRouteId = this.GetClientConfigurationRoute(RouteType.OrderNotes).RouteId;
            int systemMessageRouteId = this.GetClientConfigurationRoute(RouteType.SystemMessage).RouteId;
            int emailMessageRouteId = this.GetClientConfigurationRoute(RouteType.EmailDocument).RouteId;

            if (normalRouteId > 0) this.cbNormalRoute.Value = normalRouteId;
            if (orderNotesRouteId > 0) this.cbOrderNotesRoute.Value = orderNotesRouteId;
            if (systemMessageRouteId > 0) this.cbSystemMessageRoute.Value = systemMessageRouteId;
            if (emailMessageRouteId > 0) this.cbEmailDocumentRoute.Value = emailMessageRouteId;

            RenderLinkedDeliverypointgroups();
            RenderLinkedDeliverypoints();

            MasterPages.MasterPageEntity masterPage = this.Master as MasterPages.MasterPageEntity;
            if (masterPage != null)
            {
                masterPage.ToolBar.SaveAndNewButton.Visible = false;
            }
        }

        private ClientConfigurationRouteEntity GetClientConfigurationRoute(RouteType type)
        {
            ClientConfigurationRouteEntity route = this.DataSourceAsClientConfigurationEntity.ClientConfigurationRouteCollection.FirstOrDefault(x => x.Type == (int)type);
            if (route == null)
            {
                route = new ClientConfigurationRouteEntity();
                route.ClientConfigurationId = this.EntityId;
                route.Type = (int)type;                
            }
            return route;
        }

        private void SaveClientConfigurationRoute(ComboBoxInt cb, RouteType type)
        {
            ClientConfigurationRouteEntity route = this.GetClientConfigurationRoute(type);
            if (cb.ValidId > 0)
            {
                route.RouteId = cb.ValidId;
                route.Save();
            }
            else if (!route.IsNew)
            {
                this.DataSourceAsClientConfigurationEntity.ClientConfigurationRouteCollection.Remove(route);
                route.Delete();
            }
        }

        public override bool Save()
        {
            if (this.PageMode == Dionysos.Web.PageMode.Add)
            {
                this.DataSourceAsClientConfigurationEntity.CompanyId = CmsSessionHelper.CurrentCompanyId;

                if (this.tbPincode.Value.IsNullOrWhiteSpace()) this.tbPincode.Value = "1260";
                if (this.tbPincodeSU.Value.IsNullOrWhiteSpace()) this.tbPincodeSU.Value = "1260";
                if (this.tbPincodeGM.Value.IsNullOrWhiteSpace()) this.tbPincodeGM.Value = "1260";
            }

            this.DataSourceAsClientConfigurationEntity.DailyOrderReset = this.teDailyOrderReset.Value.Value.ToString("HHmm");
            this.DataSourceAsClientConfigurationEntity.SleepTime = this.teSleepTime.Value.Value.ToString("HHmm");
            this.DataSourceAsClientConfigurationEntity.WakeUpTime = this.teWakeUpTime.Value.Value.ToString("HHmm");
            this.DataSourceAsClientConfigurationEntity.GooglePrinterId = this.ddlGooglePrinterId.Value.IsNullOrWhiteSpace() ? string.Empty : this.ddlGooglePrinterId.Text;

            this.DataSourceAsClientConfigurationEntity.Pincode = this.tbPincode.Value.TrimToLength(25, false);
            this.DataSourceAsClientConfigurationEntity.PincodeSU = this.tbPincodeSU.Value.TrimToLength(25, false);
            this.DataSourceAsClientConfigurationEntity.PincodeGM = this.tbPincodeGM.Value.TrimToLength(25, false);
            
            if (this.IsValid())
            {
                if (base.Save())
                {
                    this.DataSourceAsClientConfigurationEntity.Refetch();
                    this.SaveClientConfigurationRoute(this.cbNormalRoute, RouteType.Normal);
                    this.SaveClientConfigurationRoute(this.cbOrderNotesRoute, RouteType.OrderNotes);
                    this.SaveClientConfigurationRoute(this.cbSystemMessageRoute, RouteType.SystemMessage);
                    this.SaveClientConfigurationRoute(this.cbEmailDocumentRoute, RouteType.EmailDocument);
                }
            }
            return false;
        }        

        public new bool IsValid()
        {
            if (this.DataSourceAsClientConfigurationEntity.Pincode.Length < 4 || this.DataSourceAsClientConfigurationEntity.PincodeSU.Length < 4 || this.DataSourceAsClientConfigurationEntity.PincodeGM.Length < 4)
            {
                this.MultiValidatorDefault.AddError(string.Format("Pincodes have to be at least 4 characters long (Pincode: {0}, SU: {1}, GM: {2})",
                    this.DataSourceAsClientConfigurationEntity.Pincode.Length,
                    this.DataSourceAsClientConfigurationEntity.PincodeSU.Length,
                    this.DataSourceAsClientConfigurationEntity.PincodeGM.Length));               

                this.Validate();
                return false;
            }
            return true;
        }

        public DateTime GetTimeFromString(string time)
        {
            int hours = Convert.ToInt32(time.Substring(0, 2));
            int minutes = Convert.ToInt32(time.Substring(2, 2));
            return new DateTime(2000, 1, 1, hours, minutes, 0);
        }

        private void RenderLinkedDeliverypointgroups()
        {
            System.Text.StringBuilder builder = new System.Text.StringBuilder();
            builder.AppendFormatLine("<table class=\"dataformV2\">");
            builder.AppendFormatLine("<tr>");
            builder.AppendFormatLine("   <td class='control'>");
            builder.AppendFormatLine("       <b>{0}</b>", this.Translate("lblDPGHeader", "Deliverypointgroups"));
            builder.AppendFormatLine("   </td>");
            builder.AppendFormatLine("   <td class='label'>");
            builder.AppendFormatLine("   </td>");
            builder.AppendFormatLine("</tr>");

            if (this.DataSourceAsClientConfigurationEntity.DeliverypointgroupCollection.Count == 0)
            {
                builder.AppendFormatLine("<tr>");
                builder.AppendFormatLine("   <td class='control'>");
                builder.AppendFormatLine("       None");
                builder.AppendFormatLine("   </td>");
                builder.AppendFormatLine("   <td class='label'>");
                builder.AppendFormatLine("   </td>");
                builder.AppendFormatLine("</tr>");

            }

            foreach (DeliverypointgroupEntity dpgEntity in this.DataSourceAsClientConfigurationEntity.DeliverypointgroupCollection)
            {
                string link = string.Format("<a href=\"Deliverypointgroup.aspx?id={0}\">{1}</a>", dpgEntity.DeliverypointgroupId, dpgEntity.Name);

                builder.AppendFormatLine("<tr>");
                builder.AppendFormatLine("   <td class='control'>");
                builder.AppendFormatLine("       {0}", link);
                builder.AppendFormatLine("   </td>");
                builder.AppendFormatLine("   <td class='label'>");
                builder.AppendFormatLine("   </td>");
                builder.AppendFormatLine("</tr>");
            }

            builder.AppendFormatLine("</table>");
            this.plhLinkedEntities.AddHtml(builder.ToString());
        }

        private void RenderLinkedDeliverypoints()
        {
            System.Text.StringBuilder builder = new System.Text.StringBuilder();
            builder.AppendFormatLine("<table class=\"dataformV2\">");
            builder.AppendFormatLine("<tr>");
            builder.AppendFormatLine("   <td class='control'>");
            builder.AppendFormatLine("       <b>{0}</b>", this.Translate("lblDPHeader", "Deliverypoints"));
            builder.AppendFormatLine("   </td>");
            builder.AppendFormatLine("   <td class='label'>");
            builder.AppendFormatLine("   </td>");
            builder.AppendFormatLine("</tr>");

            if (this.DataSourceAsClientConfigurationEntity.DeliverypointCollection.Count == 0)
            {
                builder.AppendFormatLine("<tr>");
                builder.AppendFormatLine("   <td class='control'>");
                builder.AppendFormatLine("       None");
                builder.AppendFormatLine("   </td>");
                builder.AppendFormatLine("   <td class='label'>");
                builder.AppendFormatLine("   </td>");
                builder.AppendFormatLine("</tr>");

            }

            foreach (DeliverypointEntity entity in this.DataSourceAsClientConfigurationEntity.DeliverypointCollection)
            {
                string link = string.Format("<a href=\"Deliverypoint.aspx?id={0}\">{1}</a>", entity.DeliverypointId, entity.Name);

                builder.AppendFormatLine("<tr>");
                builder.AppendFormatLine("   <td class='control'>");
                builder.AppendFormatLine("       {0}", link);
                builder.AppendFormatLine("   </td>");
                builder.AppendFormatLine("   <td class='label'>");
                builder.AppendFormatLine("   </td>");
                builder.AppendFormatLine("</tr>");
            }

            builder.AppendFormatLine("</table>");
            this.plhLinkedEntities.AddHtml(builder.ToString());
        }

        #endregion

        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            this.DataSourceLoaded += this.ClientConfiguration_DataSourceLoaded;
            base.OnInit(e);
        }

        private void ClientConfiguration_DataSourceLoaded(object sender)
        {
            this.SetGui();
        }

        #endregion
    }
}
