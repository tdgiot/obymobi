﻿using DevExpress.Web;
using Dionysos;
using Dionysos.Web.Google.Geocoding;
using Dionysos.Web.UI;
using Dionysos.Web.UI.DevExControls;
using Obymobi.Constants;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using Obymobi.ObymobiCms.MasterPages;
using Obymobi.Web.Caching;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.UI;
using Label = Dionysos.Web.UI.WebControls.Label;

namespace Obymobi.ObymobiCms.Company
{
    public partial class Company : PageLLBLGenEntity
    {
        public ISortClause SortFields { get; set; }

        /// <summary>
        /// Return the page's datasource as a CompanyEntity
        /// </summary>
        public CompanyEntity DataSourceAsCompanyEntity
        {
            get
            {
                return DataSource as CompanyEntity;
            }
        }

        private IEnumerable<Culture> GetCultures() => Obymobi.Culture.Mappings.Values.OrderBy(x => x.NameAndCultureCode);

        public override bool Delete()
        {
            DataSourceAsCompanyEntity.OverruleDeletionPrevention = true;
            return base.Delete();
        }

        public override bool Save()
        {
            if (DataSource.IsNew)
            {
                // You have to create companies using the Wizard
                MultiValidatorDefault.AddError(Translate("AddCompanyUsingAddCompanyPage", "Bedrijven dienen toegevoegd te worden middels de 'Bedrijf toevoegen' pagina"));
            }
            else
            {
                if (tbSystemPassword.Value.Length > 0 && tbSystemPassword.Value.Length < 8)
                {
                    MultiValidatorDefault.AddError("System password has to be at least 8 characters long");
                }
            }

            Validate();

            DataSourceAsCompanyEntity.TimeZoneOlsonId = ddlTimeZoneOlsonId.SelectedValueString;
            DataSourceAsCompanyEntity.CurrencyCode = ddlCurrencyCode.SelectedValueString;
            DataSourceAsCompanyEntity.CountryCode = ddlCountryId.SelectedValueString;
            DataSourceAsCompanyEntity.CultureCode = ddlCultureCode.SelectedValueString;

            if (CmsSessionHelper.CurrentRole >= Role.Crave)
            {
                DataSourceAsCompanyEntity.ApiVersion = cbApiVersionV2.Checked ? 2 : 1;
                DataSourceAsCompanyEntity.MessagingVersion = cbMessagingVersionV3.Checked ? 3 : 1;
                DataSourceAsCompanyEntity.HotSwappableContentEnabled = cbHotSwappableContent.Checked;
            }

            bool isLatLongEmpty = tbLatitude.Text.IsNullOrWhiteSpace() || tbLongitude.Text.IsNullOrWhiteSpace();

            bool isContactDetailChanged = DataSourceAsCompanyEntity.Fields[CompanyFields.CountryId.FieldIndex].IsChanged ||
                    DataSourceAsCompanyEntity.Fields[CompanyFields.Zipcode.FieldIndex].IsChanged ||
                    DataSourceAsCompanyEntity.Fields[CompanyFields.City.FieldIndex].IsChanged ||
                    DataSourceAsCompanyEntity.Fields[CompanyFields.Addressline1.FieldIndex].IsChanged ||
                    DataSourceAsCompanyEntity.Fields[CompanyFields.Addressline2.FieldIndex].IsChanged ||
                    DataSourceAsCompanyEntity.Fields[CompanyFields.Addressline3.FieldIndex].IsChanged;

            if (base.Save())
            {
                Obymobi.Logic.HelperClasses.CustomTextHelper.UpdateCustomTexts(DataSource, DataSourceAsCompanyEntity);

                SaveSpecialProducts(ProductType.Paymentmethod);

                if (CmsSessionHelper.CurrentRole >= Role.Administrator)
                {
                    SaveReleases();
                }

                if (isContactDetailChanged || isLatLongEmpty)
                {
                    SaveMapCoordinates();
                }

                if (!DataSourceAsCompanyEntity.IsNew && DataSourceAsCompanyEntity.Fields[(int)CompanyFieldIndex.CometHandlerType].IsChanged)
                {
                    MultiValidatorDefault.AddInformation(Translate("CometHandlerTypeChanged", "Clients have been notified about the changed Comet Handler."));
                }

                return true;
            }

            return false;
        }


        public override bool Refresh()
        {
            if (IsPostBack && UsePostRedirectGet)
            {
                Response.Redirect(Request.RawUrl, false);
            }
            return true;
        }

        private bool HasPlacedOrders()
        {
            OrderCollection order = new OrderCollection();

            PredicateExpression filter = new PredicateExpression();
            filter.Add(OrderFields.CompanyId == DataSourceAsCompanyEntity.CompanyId);

            return (order.GetDbCount(filter) > 0);
        }

        private void SaveSpecialProducts(ProductType productType)
        {
            SortExpression sort = new SortExpression();
            sort.Add(ProductFields.SortOrder | SortOperator.Ascending);

            // Retrieve paymentmethods for company
            ProductCollection products = new ProductCollection();
            PredicateExpression filterProducts = new PredicateExpression();
            filterProducts.Add(ProductFields.Type == productType);
            filterProducts.Add(ProductFields.CompanyId == DataSourceAsCompanyEntity.CompanyId);
            products.GetMulti(filterProducts, 0, sort);

            EntityView<ProductEntity> productView = products.DefaultView;

            if (productView.Count > 15)
            {
                if (productType == ProductType.Paymentmethod)
                    throw new EntitySaveException("Er zijn meer dan 15 betaalmethoden, dat kan niet via het CMS worden beheerd");

                throw new EntitySaveException("Er zijn meer dan 15 bedieningsmogelijkheden, dat kan niet via het CMS worden beheerd");
            }

            string ddlIdPrefix = productType == ProductType.Service ? "ddlServiceProduct" : "ddlPaymentMethod";
            List<int> productsInUse = new List<int>();

            foreach (Control control in ControlList)
            {
                if (!control.ID.IsNullOrWhiteSpace() && control.ID.StartsWith(ddlIdPrefix))
                {
                    ComboBoxInt ddl = (ComboBoxInt)control;
                    int sortOrder = Convert.ToInt32(ddl.ID.GetAllAfterFirstOccurenceOf(ddlIdPrefix));

                    // GK This is kind of a hack unfortunatly. Thing is: the emptyitemtext is Translated at the later moment than we are here
                    // therefore this: ddl.Text != ddl.EmptyItemText will give an invalid result if we're not working in Dutch.
                    if (ddl.ValidId > 0 || (ddl.Text.Length > 0 && ddl.Text != ddl.EmptyItemText && ddl.Text != "None - Select ..."))
                    {
                        // Load OR Create Product
                        int productId = ddl.ValidId;
                        ProductEntity product;
                        if (productId <= 0)
                        {
                            // Create a new product
                            product = new ProductEntity();
                            product.CompanyId = DataSourceAsCompanyEntity.CompanyId;
                            product.Name = ddl.Text;
                            product.Type = (int)productType;
                            product.PriceIn = 0;
                            product.VattariffId = 3; // GK Hardcoded :P Curse at me when you find this to be the problem on a very late night :)
                            product.SortOrder = sortOrder;
                            product.Visible = true;
                        }
                        else
                            product = new ProductEntity(productId);

                        // Update / Add            
                        product.SortOrder = sortOrder;
                        product.Save();

                        productsInUse.Add(product.ProductId);
                    }
                }
            }

            // Hide all non-used products
            filterProducts.Add(ProductFields.ProductId != productsInUse);

            products.GetMulti(filterProducts);

            foreach (ProductEntity product in products)
            {
                product.Visible = false;
                product.AvailableOnObymobi = false;
                product.SortOrder = 9999;
                product.Save();
            }
        }

        private void PopulateSpecialProducts(ProductType productType)
        {
            SortExpression sort = new SortExpression();
            sort.Add(ProductFields.SortOrder | SortOperator.Ascending);

            PredicateExpression filterProducts = new PredicateExpression();
            filterProducts.Add(ProductFields.Type == productType);
            filterProducts.Add(ProductFields.CompanyId == DataSourceAsCompanyEntity.CompanyId);
            filterProducts.Add(ProductFields.SortOrder != 9999); // Visible products, not having sortorder 9999

            ProductCollection visibleProducts = new ProductCollection();
            visibleProducts.GetMulti(filterProducts, 0, sort);

            EntityView<ProductEntity> productView = visibleProducts.DefaultView;

            string ddlIdPrefix = productType == ProductType.Service ? "ddlServiceProduct" : "ddlPaymentMethod";

            // Populate all the 
            foreach (Control control in ControlList)
            {
                if (!control.ID.IsNullOrWhiteSpace() && control.ID.StartsWith(ddlIdPrefix))
                {
                    productView.Filter = null;
                    ComboBoxInt ddl = (ComboBoxInt)control;
                    ddl.DisplayEmptyItem = true;
                    ddl.EmptyItemText = "- Niet ingesteld -";
                    ddl.DataSource = visibleProducts;
                    ddl.DataBind();

                    //ddl.Items.Insert(0, new DevExpress.Web.ListEditItem("- Niet ingesteld -", -1));
                    ddl.SelectedIndex = -1;

                    int sortOrder = Convert.ToInt32(ddl.ID.GetAllAfterFirstOccurenceOf(ddlIdPrefix));

                    // Check if this paymentmethod has a selected product for this position					                    
                    productView.Filter = new PredicateExpression(ProductFields.SortOrder == sortOrder);

                    if (productView.Count == 1)
                    {
                        ddl.Value = productView[0].ProductId;
                    }
                }
            }
        }

        private void PopulateReleases()
        {
            plhReleases.Controls.Clear();

            ApplicationCollection applications = new ApplicationCollection();
            applications.GetMulti(null);

            foreach (ApplicationEntity application in applications)
            {
                bool autoUpdateEnabled = CmsSessionHelper.CurrentRole >= Role.Crave &&
                    (application.Code == ApplicationCode.Emenu ||
                    application.Code == ApplicationCode.Agent ||
                    application.Code == ApplicationCode.SupportTools ||
                    application.Code == ApplicationCode.Console);

                plhReleases.AddHtml("<tr>");
                plhReleases.AddHtml("<td class=\"label\">");

                // Create the label 
                Label lblApplicationName = new Dionysos.Web.UI.WebControls.Label();
                lblApplicationName.ID = "lblApplicationName-" + application.ApplicationId;
                lblApplicationName.UseDataBinding = false;
                lblApplicationName.Text = application.Name;
                lblApplicationName.LocalizeText = false;
                plhReleases.Controls.Add(lblApplicationName);

                plhReleases.AddHtml("</td>");
                plhReleases.AddHtml("<td class=\"control\">");

                PredicateExpression filter2 = new PredicateExpression();
                filter2.Add(ReleaseFields.ApplicationId == application.ApplicationId);
                filter2.Add(ReleaseFields.Intermediate == false);
                filter2.Add(ReleaseFields.Filename != DBNull.Value);
                filter2.Add(new FieldCompareRangePredicate(ReleaseFields.ReleaseGroup, GetReleaseGroups()));

                IncludeFieldsList includes = new IncludeFieldsList();
                includes.Add(ReleaseFields.Version);
                includes.Add(ReleaseFields.ReleaseId);
                includes.Add(ReleaseFields.ApplicationId);

                ReleaseCollection releases = new ReleaseCollection();
                releases.GetMulti(filter2, includes, null);

                ComboBoxInt cbReleaseId = new ComboBoxInt();
                cbReleaseId.ID = "ddlReleaseId-" + application.ApplicationId.ToString();

                plhReleases.Controls.Add(cbReleaseId);

                cbReleaseId.UseDataBinding = false;
                foreach (ReleaseEntity release in releases)
                {
                    cbReleaseId.Items.Add(release.Version, release.ReleaseId);
                }
                cbReleaseId.IncrementalFilteringMode = IncrementalFilteringMode.StartsWith;

                if (!application.ReleaseId.HasValue)
                {
                    cbReleaseId.Enabled = false;
                    plhReleases.AddHtml("<p style=\"color: Red;\">" + Translate("NoStableRelease", "Deze applicatie heeft nog geen stabiele versie") + "</p>");
                }

                CompanyReleaseCollection companyReleases = new CompanyReleaseCollection();

                RelationCollection relations = new RelationCollection();
                relations.Add(CompanyReleaseEntityBase.Relations.ReleaseEntityUsingReleaseId);
                relations.Add(ReleaseEntityBase.Relations.ApplicationEntityUsingApplicationId);

                PredicateExpression filter = new PredicateExpression();
                filter.Add(CompanyReleaseFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
                filter.Add(ApplicationFields.ApplicationId == application.ApplicationId);

                SortExpression sort = new SortExpression();
                sort.Add(new SortClause(ReleaseFields.Version, SortOperator.Descending));

                companyReleases.GetMulti(filter, 0, sort, relations);

                CompanyReleaseEntity companyRelease = companyReleases.Count > 0 ? companyReleases[0] : null;
                if (companyRelease != null)
                {
                    cbReleaseId.Value = companyRelease.ReleaseId;
                }
                else
                {
                    cbReleaseId.SelectedIndex = 0;
                }

                if (autoUpdateEnabled)
                {
                    Dionysos.Web.UI.WebControls.CheckBox cbAutoUpdate = new Dionysos.Web.UI.WebControls.CheckBox();
                    cbAutoUpdate.ID = string.Format("cbAutoUpdate-{0}", application.ApplicationId);
                    cbAutoUpdate.Text = "Auto update";
                    cbAutoUpdate.Checked = companyRelease != null && companyRelease.AutoUpdate;
                    plhReleases.Controls.Add(cbAutoUpdate);
                }

                plhReleases.AddHtml("</td>");
                plhReleases.AddHtml("<td class=\"label\">");
                plhReleases.AddHtml("</td>");
                plhReleases.AddHtml("<td class=\"control\">");
                plhReleases.AddHtml("</td>");

                plhReleases.AddHtml("</tr>");
            }
        }

        private void SaveReleases()
        {
            ApplicationCollection applications = new ApplicationCollection();
            applications.GetMulti(new PredicateExpression());

            foreach (ApplicationEntity application in applications)
            {
                CompanyReleaseCollection companyReleases = new CompanyReleaseCollection();
                RelationCollection relations = new RelationCollection();
                relations.Add(CompanyReleaseEntityBase.Relations.ReleaseEntityUsingReleaseId);
                relations.Add(ReleaseEntityBase.Relations.ApplicationEntityUsingApplicationId);

                PredicateExpression filter = new PredicateExpression();
                filter.Add(CompanyReleaseFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
                filter.Add(ApplicationFields.ApplicationId == application.ApplicationId);
                companyReleases.GetMulti(filter, relations);

                ComboBoxInt cbReleaseId = (ComboBoxInt)plhReleases.FindControl("ddlReleaseId-" + application.ApplicationId.ToString());
                if (cbReleaseId != null && cbReleaseId.Value.HasValue && cbReleaseId.SelectedIndex >= 0)
                {
                    bool enableAutoUpdate = false;

                    Dionysos.Web.UI.WebControls.CheckBox cbAutoUpdate = (Dionysos.Web.UI.WebControls.CheckBox)plhReleases.FindControl(string.Format("cbAutoUpdate-{0}", application.ApplicationId));
                    if (cbAutoUpdate != null)
                    {
                        enableAutoUpdate = cbAutoUpdate.Checked;
                    }

                    if (companyReleases.Count > 0)
                    {
                        companyReleases[0].ReleaseId = cbReleaseId.Value.GetValueOrDefault();
                        companyReleases[0].AutoUpdate = enableAutoUpdate;
                        companyReleases[0].Save();
                    }
                    else
                    {
                        CompanyReleaseEntity companyRelease = new CompanyReleaseEntity();
                        companyRelease.CompanyId = CmsSessionHelper.CurrentCompanyId;
                        companyRelease.ReleaseId = cbReleaseId.Value.GetValueOrDefault();
                        companyRelease.AutoUpdate = enableAutoUpdate;
                        companyRelease.Save();
                    }
                }
                else
                {
                    if (companyReleases.Count > 0)
                    {
                        companyReleases[0].Delete();
                    }
                }
            }
        }

        private void LookupMapCoordinates(out double? lat, out double? lng, CompanyEntity companyEntity)
        {
            lat = null;
            lng = null;

            string addressLine1 = (companyEntity == null ? tbAddressline1.Text : companyEntity.Addressline1);
            string addressLine2 = (companyEntity == null ? tbAddressline2.Text : companyEntity.Addressline2);
            string addressLine3 = (companyEntity == null ? tbAddressline3.Text : companyEntity.Addressline3);
            string zipcode = (companyEntity == null ? tbZipcode.Text : companyEntity.Zipcode);
            string city = (companyEntity == null ? tbCity.Text : companyEntity.City);
            string country = (companyEntity == null ? ddlCountryId.Text : companyEntity.CountryEntity.Name);

            string address = "";
            if (addressLine1.Length > 0)
                address += addressLine1 + "+";
            if (addressLine2.Length > 0)
                address += addressLine2 + "+";
            if (addressLine3.Length > 0)
                address += addressLine3 + "+";

            if (zipcode.Length > 0)
                address += zipcode + "+";

            // Only add city/country and lookup when we have an address
            if (address.Length > 0)
            {
                if (city.Length > 0)
                    address += city + "+";
                if (country.Length > 0)
                    address += country;

                // Lookup address
                GeoResponse response = GeoCoder.LookupAddress(address, false, "AIzaSyDbNDzR5Wcl6wWy4uPRMqRraXgkErbZUnI");
                if (response != null)
                {
                    if (response.Status.Equals("OK") && response.Results.Length >= 1)
                    {
                        GeoResponse.CResult data = response.Results[0];
                        lat = (double?)data.Geometry.Location.Lat;
                        lng = (double?)data.Geometry.Location.Lng;
                    }
                }
            }
        }

        private void SaveMapCoordinates()
        {
            if (DataSourceAsCompanyEntity.LatitudeOverridden && DataSourceAsCompanyEntity.LongitudeOverriden)
                return;

            double? lat, lng;

            LookupMapCoordinates(out lat, out lng, null);

            CompanyEntity entity = DataSourceAsCompanyEntity;
            if (!entity.LatitudeOverridden)
                entity.Latitude = lat;
            if (!entity.LongitudeOverriden)
                entity.Longitude = lng;
            entity.Save();
        }

        private void HookUpEvents()
        {
            lbLatitudeLongitude.Click += lbLatitudeLongitude_Click;

            btResetCacheTimeStamps.Click += btResetCacheTimeStamps_Click;

            btnRefreshCacheTiemstamps_Company.Click += btResetCacheTimeStamps_Company_Click;
            btnRefreshCacheTimeStamps_Advertisement.Click += btResetCacheTimeStamps_Advertisement_Click;
            btnRefreshCacheTimeStamps_Announcement.Click += btResetCacheTimeStamps_Annoucement_Click;
            btnRefreshCacheTimeStamps_Deliverypoint.Click += btResetCacheTimeStamps_Deliverypoint_Click;
            btnRefreshCacheTimeStamps_Entertainment.Click += btResetCacheTimeStamps_Entertainment_Click;
            btnRefreshCacheTimeStamps_Pos.Click += btResetCacheTimeStamps_Pos_Click;
            btnRefreshCacheTimeStamps_Survey.Click += btResetCacheTimeStamps_Survey_Click;
        }

        private void SetGui()
        {
            if (CmsSessionHelper.CurrentRole < Role.Reseller)
            {
                tabsMain.TabPages.FindByName("Advanced").ClientVisible = false;

                plhForAdministrator.Visible = false;

                lblCompanyOwnerId.Visible = false;
                ddlCompanyOwnerId.Visible = false;
            }
            else if (CmsSessionHelper.CurrentRole == Role.Reseller)
            {
                lblCode.Visible = false;
                tbObyCode.Visible = false;
                lblCompanyOwnerId.Visible = false;
                ddlCompanyOwnerId.Visible = false;
                tbMaxDownloadConnections.Enabled = false;
                ddlCometHandlerType.Enabled = false;
            }
            else if (CmsSessionHelper.CurrentRole >= Role.Crave)
            {
                pnlServiceVersions.Visible = true;

                cbApiVersionV2.Checked = DataSourceAsCompanyEntity.ApiVersion == 2;

                cbMessagingVersionV3.Enabled = true;
                cbMessagingVersionV3.Checked = DataSourceAsCompanyEntity.MessagingVersion == 3;

                pnlContent.Visible = true;

                cbHotSwappableContent.Enabled = true;
                cbHotSwappableContent.Checked = DataSourceAsCompanyEntity.HotSwappableContentEnabled;
            }

            if (DataSourceAsCompanyEntity.GoogleAnalyticsId.Length > 0)
            {
                tbGoogleAnalyticsId.Enabled = false;

                if (CmsSessionHelper.CurrentRole >= Role.Administrator)
                    btEditGoogleAnalyticsId.Visible = true;
            }

            ddlCultureCode.Enabled = CmsSessionHelper.CurrentRole == Role.GodMode;

            tbLatitude.ReadOnly = !DataSourceAsCompanyEntity.LatitudeOverridden;
            tbLongitude.ReadOnly = !DataSourceAsCompanyEntity.LongitudeOverriden;

            double? latitude = DataSourceAsCompanyEntity.Latitude;
            double? longitude = DataSourceAsCompanyEntity.Longitude;

            if (!latitude.HasValue || !longitude.HasValue)
                LookupMapCoordinates(out latitude, out longitude, DataSourceAsCompanyEntity);

            if (DataSourceAsCompanyEntity.LatitudeOverridden && DataSourceAsCompanyEntity.LongitudeOverriden)
                plhMap.AddHtml("<a href=\"../Generic/LocationPicker.aspx?lat={0}&lon={1}&latInput={2}&lonInput={3}\" rel=\"lightbox[500 400]\" title=\"Pick a location::Drag the marker to change the location\">View map</a>", latitude ?? 51.50703296721856, longitude ?? -0.127716064453125, tbLatitude.ClientID, tbLongitude.ClientID);
            else
                plhMap.AddHtml("<a href=\"../Generic/LocationPicker.aspx?lat={0}&lon={1}&latInput={2}&lonInput={3}&viewOnly=true\" rel=\"lightbox[500 400]\">View map</a>", latitude ?? 51.50703296721856, longitude ?? -0.127716064453125, tbLatitude.ClientID, tbLongitude.ClientID);

            ((MasterPages.MasterPageEntity)Master).ToolBar.SaveAndNewButton.Visible = false;

            ddlCultureCode.SelectedItem = ddlCultureCode.Items.FindByValue(DataSourceAsCompanyEntity.CultureCode);
            ddlCurrencyCode.SelectedItem = ddlCurrencyCode.Items.FindByValue(DataSourceAsCompanyEntity.CurrencyCode);
            ddlTimeZoneOlsonId.SelectedItem = ddlTimeZoneOlsonId.Items.FindByValue(DataSourceAsCompanyEntity.TimeZoneOlsonId);
            ddlCountryId.SelectedItem = ddlCountryId.Items.FindByValue(DataSourceAsCompanyEntity.CountryCode);

            if (DataSourceAsCompanyEntity.ApiVersion == 2 && DataSourceAsCompanyEntity.TerminalCollection.All(t => t.HandlingMethod.ToEnum<TerminalType>() != TerminalType.OnSiteServer))
            {
                pnlActions.Visible = false;
            }

            SetTimestamps();

            pnlContent.Visible = CmsSessionHelper.CurrentUser.HasFeature(FeatureToggle.HotSwappableMenuContent);

            ConfigureArchiveButton();
        }

        private void ConfigureArchiveButton()
        {
            if (DataSourceAsCompanyEntity.Archived)
            {
                btnArchive.Visible = CmsSessionHelper.CurrentUser.Role == Role.GodMode;
                btnArchive.Text = "Unarchive";
                btnArchive.PreSubmitWarning = "Are you sure you want to revive this company?";
            }
            else
            {
                btnArchive.Visible = CmsSessionHelper.CurrentUser.Role >= Role.Crave;
                btnArchive.Text = "Remove";
                btnArchive.PreSubmitWarning = "Are you sure you want to archive this company?";
            }
        }

        private void SetTimestamps()
        {
            // Timestamps
            lblTimestampAdvertisement.Text = DataSourceAsCompanyEntity.AdvertisementDataLastModifiedUTC.ToString(CultureInfo.InvariantCulture);
            lblTimestampAnnouncement.Text = DataSourceAsCompanyEntity.AnnouncementDataLastModifiedUTC.ToString(CultureInfo.InvariantCulture);
            lblTimestampCompany.Text = DataSourceAsCompanyEntity.CompanyDataLastModifiedUTC.ToString(CultureInfo.InvariantCulture);
            lblTimestampDeliverypoint.Text = DataSourceAsCompanyEntity.DeliverypointDataLastModifiedUTC.ToString(CultureInfo.InvariantCulture);
            lblTimestampEntertainment.Text = DataSourceAsCompanyEntity.EntertainmentDataLastModifiedUTC.ToString(CultureInfo.InvariantCulture);
            lblTimestampPos.Text = DataSourceAsCompanyEntity.PosIntegrationInfoLastModifiedUTC.ToString(CultureInfo.InvariantCulture);
            lblTimestampSurvey.Text = DataSourceAsCompanyEntity.SurveyDataLastModifiedUTC.ToString(CultureInfo.InvariantCulture);
        }

        private void SetFilters()
        {
            ddlRouteId.DataSource = Obymobi.Cms.Logic.HelperClasses.RouteHelper.GetRouteCollection(CmsSessionHelper.CurrentCompanyId);
            ddlRouteId.DataBind();
        }

        private void LoadUserControls()
        {
            if (CmsSessionHelper.CurrentRole >= Role.Reseller)
            {
                Generic.UserControls.CustomTextCollection translationsPanel = tabsMain.AddTabPage("Translations", "CustomTextCollection", "~/Generic/UserControls/CustomTextCollection.ascx") as Generic.UserControls.CustomTextCollection;
                translationsPanel.FullWidth = true;

                tabsMain.AddTabPage("Reports", "Reports", "~/Company/SubPanels/ReportProcessingTaskPanel.ascx");
                tabsMain.AddTabPage("Businesshours", "Businesshours", "~/Company/SubPanels/BusinesshourPanel.ascx");
                tabsMain.AddTabPage("Currencies", "CompanyCurrencyCollection", "~/Company/SubPanels/CompanyCurrencyCollection.ascx", "CompanyCultureCollection");
            }

            tabsMain.AddTabPage("Images", "Media", "~/Generic/UserControls/MediaCollection.ascx");

            if (CmsSessionHelper.CurrentRole >= Role.Administrator)
            {
                tabsMain.AddTabPage("Setup Codes", "SetupCodes", "~/Company/Subpanels/SetupCodePanel.ascx");
                tabsMain.AddTabPage("Cultures", "CompanyCultureCollection", "~/Company/SubPanels/CompanyCultureCollection.ascx", "Advanced");
                tabsMain.AddTabPage("Brands", "CompanyBrandCollection", "~/Company/SubPanels/CompanyBrandCollection.ascx", "CompanyCurrencyCollection");
            }
            else if (CmsSessionHelper.CurrentRole <= Role.Reseller)
            {
                tabsMain.TabPages.FindByName("Releases").Visible = false;

                if (CmsSessionHelper.CurrentRole < Role.Reseller)
                {
                    tabsMain.TabPages.FindByName("tabCategories").Visible = false;
                    tabsMain.TabPages.FindByName("tabAmenities").Visible = false;
                }
            }

            ddlDeviceRebootMethod.DataBindEnumStringValuesAsDataSource(typeof(DeviceRebootMethod));
            ddlCometHandlerType.DataBindEnumStringValuesAsDataSource(typeof(ClientCommunicationMethod), (int)ClientCommunicationMethod.Webservice, (int)ClientCommunicationMethod.All - 1);
            ddlClockMode.DataBindEnumStringValuesAsDataSource(typeof(ClockMode));
            ddlWeatherClockWidgetMode.DataBindEnumStringValuesAsDataSource(typeof(WeatherClockWidgetMode));
            ddlAlterationDialogMode.DataBindEnumStringValuesAsDataSource(typeof(AlterationDialogMode));
            ddlPriceFormatType.DataBindEnumStringValuesAsDataSource(typeof(PriceFormatType));

            IEnumerable<Obymobi.Culture> cultures = Obymobi.Culture.Mappings.Values.OrderBy(x => x.NameAndCultureCode);

            ddlCultureCode.DataSource = cultures;
            ddlCultureCode.DataBind();

            IEnumerable<Obymobi.Currency> currencies = Obymobi.Currency.Mappings.Values.OrderBy(x => x.Name);

            ddlCurrencyCode.DataSource = currencies;
            ddlCurrencyCode.DataBind();

            IEnumerable<Obymobi.TimeZone> timezones = Obymobi.TimeZone.Mappings.Values.OrderByDescending(x => x.UtcOffset);

            ddlTimeZoneOlsonId.DataSource = timezones;
            ddlTimeZoneOlsonId.DataBind();

            IEnumerable<Obymobi.Country> countries = Obymobi.Country.Mappings.Values.OrderBy(x => x.Name);

            ddlCountryId.DataSource = countries;
            ddlCountryId.DataBind();
        }

        private void SetWarnings()
        {
            if (CmsSessionHelper.CurrentRole != Role.Customer)
            {
                CompanyEntity company = DataSourceAsCompanyEntity;

                // Route
                if (company.RouteId == null || company.RouteId == 0)
                    AddInformator(Dionysos.Web.UI.InformatorType.Warning, Translate("RouteNotSpecified", "Er is geen route gekozen."));

                // Support pool
                if (company.SupportpoolId == null || company.SupportpoolId == 0)
                    AddInformator(Dionysos.Web.UI.InformatorType.Warning, Translate("SupportpoolNotSpecified", "Er is geen support pool gekozen."));

                // Mobile but not complete
                if (company.MobileOrderingDisabled)
                    AddInformator(Dionysos.Web.UI.InformatorType.Information, Translate("Information.MobileOrderingDisabled", "Op dit moment is bestellen via de App uitgeschakeld."));

                // Check UI Modes for Company
                if (company.AvailableOnObymobi && !company.DeliverypointgroupCollection.Any(dpg => dpg.AvailableOnObymobi && dpg.TabletUIModeId.HasValue))
                    AddInformator(Dionysos.Web.UI.InformatorType.Warning, Translate("Warning.NoDeliverypointGroupAvailableForMobile", "Dit bedrijf kan niet worden gebruikt op de App voor Tablets omdat er geen Tafelgroepen zijn geconfigureerd voor de App met Tablet UI mode."));
                if (company.AvailableOnObymobi && !company.DeliverypointgroupCollection.Any(dpg => dpg.AvailableOnObymobi && dpg.MobileUIModeId.HasValue))
                    AddInformator(Dionysos.Web.UI.InformatorType.Warning, Translate("Warning.NoDeliverypointGroupAvailableForTablet", "Dit bedrijf kan niet worden gebruikt op de App voor Mobile omdat er geen Tafelgroepen zijn geconfigureerd voor de App met Mobile UI mode."));

                if (company.SystemPassword.Length == 0)
                {
                    AddInformator(InformatorType.Warning, Translate("Warning.NoSystemPassword", "This company does not have a system password set, possible security issue"));
                }
            }

            Validate();
        }

        protected override void OnInit(EventArgs e)
        {
            LoadUserControls();
            SetFilters();
            DataSourceLoaded += (this.Company_DataSourceLoaded);
            base.OnInit(e);

            ddlCurrencyCode.Enabled = !HasPlacedOrders();
            DataSourceAsCompanyEntity.PreviousCultureCode = DataSourceAsCompanyEntity.CultureCode;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SetWarnings();
            }

            if (this.Master is MasterPageEntity masterPage)
            {
                masterPage.ToolBar.SaveButton.Visible = true;
                masterPage.ToolBar.SaveAndGoButton.Visible = false;
                masterPage.ToolBar.SaveAndNewButton.Visible = false;
                masterPage.ToolBar.CancelButton.Visible = true;

                if (masterPage.ToolBar.DeleteButton != null)
                {
                    masterPage.ToolBar.DeleteButton.Visible = false;
                }
            }

            HookUpEvents();
        }

        public void ToggleArchived(object sender, EventArgs e)
        {
            DataSourceAsCompanyEntity.Archived = !DataSourceAsCompanyEntity.Archived;
            DataSourceAsCompanyEntity.UseMonitoring = !DataSourceAsCompanyEntity.Archived;

            if (DataSourceAsCompanyEntity.Save())
            {
                Refresh();
            }
        }

        private void Company_DataSourceLoaded(object sender)
        {
            PopulateSpecialProducts(ProductType.Paymentmethod);
            PopulateReleases();

            SetGui();
        }

        private void btResetCacheTimeStamps_Click(object sender, EventArgs e)
        {
            if (Obymobi.Logic.HelperClasses.ScheduledCommandTaskHelper.IsScheduledCommandTaskRunning(DataSourceAsCompanyEntity.CompanyId))
            {
                AddInformator(InformatorType.Information, "Unable to refresh timestamps at this moment. Scheduled command task is running for this company");
                Validate();
                return;
            }

            DateTime utcNow = DateTime.UtcNow;

            DataSourceAsCompanyEntity.CompanyDataLastModifiedUTC = utcNow;
            DataSourceAsCompanyEntity.CompanyMediaLastModifiedUTC = utcNow;
            DataSourceAsCompanyEntity.PosIntegrationInfoLastModifiedUTC = utcNow;
            DataSourceAsCompanyEntity.DeliverypointDataLastModifiedUTC = utcNow;
            DataSourceAsCompanyEntity.SurveyDataLastModifiedUTC = utcNow;
            DataSourceAsCompanyEntity.SurveyMediaLastModifiedUTC = utcNow;
            DataSourceAsCompanyEntity.AnnouncementDataLastModifiedUTC = utcNow;
            DataSourceAsCompanyEntity.AnnouncementMediaLastModifiedUTC = utcNow;
            DataSourceAsCompanyEntity.EntertainmentDataLastModifiedUTC = utcNow;
            DataSourceAsCompanyEntity.EntertainmentMediaLastModifiedUTC = utcNow;
            DataSourceAsCompanyEntity.AdvertisementDataLastModifiedUTC = utcNow;
            DataSourceAsCompanyEntity.AdvertisementMediaLastModifiedUTC = utcNow;
            DataSourceAsCompanyEntity.Save();

            WriteTimestampCache();
            SetTimestamps();

            File.AppendAllText(Server.MapPath("~/App_Data/CompanyTimestamp.txt"), string.Format("{0} - {1} - {2} - {3}\n", DateTime.Now, DataSourceAsCompanyEntity.Name, "All", CmsSessionHelper.CurrentUser.Username));
        }

        private void btResetCacheTimeStamps_Company_Click(object sender, EventArgs e)
        {
            if (Obymobi.Logic.HelperClasses.ScheduledCommandTaskHelper.IsScheduledCommandTaskRunning(DataSourceAsCompanyEntity.CompanyId))
                return;

            DateTime utcNow = DateTime.UtcNow;

            DataSourceAsCompanyEntity.CompanyDataLastModifiedUTC = utcNow;
            DataSourceAsCompanyEntity.CompanyMediaLastModifiedUTC = utcNow;
            DataSourceAsCompanyEntity.Save();

            WriteTimestampCache();
            SetTimestamps();

            File.AppendAllText(Server.MapPath("~/App_Data/CompanyTimestamp.txt"), string.Format("{0} - {1} - {2} - {3}\n", DateTime.Now, DataSourceAsCompanyEntity.Name, "Company", CmsSessionHelper.CurrentUser.Username));
        }

        private void btResetCacheTimeStamps_Pos_Click(object sender, EventArgs e)
        {
            if (Obymobi.Logic.HelperClasses.ScheduledCommandTaskHelper.IsScheduledCommandTaskRunning(DataSourceAsCompanyEntity.CompanyId))
                return;

            DateTime utcNow = DateTime.UtcNow;

            DataSourceAsCompanyEntity.PosIntegrationInfoLastModifiedUTC = utcNow;
            DataSourceAsCompanyEntity.Save();

            WriteTimestampCache();
            SetTimestamps();

            File.AppendAllText(Server.MapPath("~/App_Data/CompanyTimestamp.txt"), string.Format("{0} - {1} - {2} - {3}\n", DateTime.Now, DataSourceAsCompanyEntity.Name, "POS", CmsSessionHelper.CurrentUser.Username));
        }

        private void btResetCacheTimeStamps_Deliverypoint_Click(object sender, EventArgs e)
        {
            if (Obymobi.Logic.HelperClasses.ScheduledCommandTaskHelper.IsScheduledCommandTaskRunning(DataSourceAsCompanyEntity.CompanyId))
                return;

            DateTime utcNow = DateTime.UtcNow;

            DataSourceAsCompanyEntity.DeliverypointDataLastModifiedUTC = utcNow;
            DataSourceAsCompanyEntity.Save();

            WriteTimestampCache();
            SetTimestamps();

            File.AppendAllText(Server.MapPath("~/App_Data/CompanyTimestamp.txt"), string.Format("{0} - {1} - {2} - {3}\n", DateTime.Now, DataSourceAsCompanyEntity.Name, "Deliverypoint", CmsSessionHelper.CurrentUser.Username));
        }

        private void btResetCacheTimeStamps_Survey_Click(object sender, EventArgs e)
        {
            if (Obymobi.Logic.HelperClasses.ScheduledCommandTaskHelper.IsScheduledCommandTaskRunning(DataSourceAsCompanyEntity.CompanyId))
                return;

            DateTime utcNow = DateTime.UtcNow;

            DataSourceAsCompanyEntity.SurveyDataLastModifiedUTC = utcNow;
            DataSourceAsCompanyEntity.SurveyMediaLastModifiedUTC = utcNow;
            DataSourceAsCompanyEntity.Save();

            WriteTimestampCache();
            SetTimestamps();

            File.AppendAllText(Server.MapPath("~/App_Data/CompanyTimestamp.txt"), string.Format("{0} - {1} - {2} - {3}\n", DateTime.Now, DataSourceAsCompanyEntity.Name, "Survey", CmsSessionHelper.CurrentUser.Username));
        }

        private void btResetCacheTimeStamps_Annoucement_Click(object sender, EventArgs e)
        {
            if (Obymobi.Logic.HelperClasses.ScheduledCommandTaskHelper.IsScheduledCommandTaskRunning(DataSourceAsCompanyEntity.CompanyId))
                return;

            DateTime utcNow = DateTime.UtcNow;

            DataSourceAsCompanyEntity.AnnouncementDataLastModifiedUTC = utcNow;
            DataSourceAsCompanyEntity.AnnouncementMediaLastModifiedUTC = utcNow;
            DataSourceAsCompanyEntity.Save();

            WriteTimestampCache();
            SetTimestamps();

            File.AppendAllText(Server.MapPath("~/App_Data/CompanyTimestamp.txt"), string.Format("{0} - {1} - {2} - {3}\n", DateTime.Now, DataSourceAsCompanyEntity.Name, "Announcement", CmsSessionHelper.CurrentUser.Username));
        }

        private void btResetCacheTimeStamps_Advertisement_Click(object sender, EventArgs e)
        {
            if (Obymobi.Logic.HelperClasses.ScheduledCommandTaskHelper.IsScheduledCommandTaskRunning(DataSourceAsCompanyEntity.CompanyId))
                return;

            DateTime utcNow = DateTime.UtcNow;

            DataSourceAsCompanyEntity.AdvertisementDataLastModifiedUTC = utcNow;
            DataSourceAsCompanyEntity.AdvertisementMediaLastModifiedUTC = utcNow;
            DataSourceAsCompanyEntity.Save();

            WriteTimestampCache();
            SetTimestamps();

            File.AppendAllText(Server.MapPath("~/App_Data/CompanyTimestamp.txt"), string.Format("{0} - {1} - {2} - {3}\n", DateTimeOffset.Now, DataSourceAsCompanyEntity.Name, "Advertisement", CmsSessionHelper.CurrentUser.Username));
        }

        private void btResetCacheTimeStamps_Entertainment_Click(object sender, EventArgs e)
        {
            if (Obymobi.Logic.HelperClasses.ScheduledCommandTaskHelper.IsScheduledCommandTaskRunning(DataSourceAsCompanyEntity.CompanyId))
                return;

            DateTime utcNow = DateTime.UtcNow;

            DataSourceAsCompanyEntity.EntertainmentDataLastModifiedUTC = utcNow;
            DataSourceAsCompanyEntity.EntertainmentMediaLastModifiedUTC = utcNow;
            DataSourceAsCompanyEntity.Save();

            WriteTimestampCache();
            SetTimestamps();

            File.AppendAllText(Server.MapPath("~/App_Data/CompanyTimestamp.txt"), string.Format("{0} - {1} - {2} - {3}\n", DateTimeOffset.Now, DataSourceAsCompanyEntity.Name, "Entertainment", CmsSessionHelper.CurrentUser.Username));
        }

        private void WriteTimestampCache()
        {
            if (RedisCacheHelper.Instance.RedisEnabled)
            {
                string cacheKey = "CompanyCache-" + DataSourceAsCompanyEntity.CompanyId;
                RedisCacheHelper.Instance.Add(cacheKey, DataSourceAsCompanyEntity);
            }
        }

        private void lbLatitudeLongitude_Click(object sender, EventArgs e)
        {
            DataSourceAsCompanyEntity.LatitudeOverridden = !DataSourceAsCompanyEntity.LatitudeOverridden;
            DataSourceAsCompanyEntity.LongitudeOverriden = !DataSourceAsCompanyEntity.LongitudeOverriden;

            if (!DataSourceAsCompanyEntity.LatitudeOverridden &&
                !DataSourceAsCompanyEntity.LongitudeOverriden)
            {
                double? latitude, longitude;

                LookupMapCoordinates(out latitude, out longitude, DataSourceAsCompanyEntity);

                DataSourceAsCompanyEntity.Latitude = latitude;
                DataSourceAsCompanyEntity.Longitude = longitude;
            }

            Save();
        }

        private IEnumerable<ReleaseGroup> GetReleaseGroups() =>
            Enum.GetValues(typeof(ReleaseGroup)).Cast<ReleaseGroup>().Where(x => x >= CmsSessionHelper.CurrentCompanyReleaseGroup).ToList();
    }
}
