﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.Installation" Title="Installation overview" Codebehind="Installation.aspx.cs" %>
<%@ Reference VirtualPath="~/Company/SubPanels/LandingPagePanels/TerminalPanel.ascx" %>
<%@ Reference VirtualPath="~/Company/SubPanels/LandingPagePanels/ClientPanel.ascx" %>
<%@ Reference VirtualPath="~/Company/SubPanels/LandingPagePanels/DeliverypointgroupPanel.ascx" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cplhTitle">Installation overview</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
	<D:Button runat="server" ID="btnRefresh" Text="Verversen" CausesValidation="true" style="margin-left: 3px;" />
</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="cplhPageContent">
<div>
	<X:PageControl ID="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
                    <D:Panel ID="pnlCompany" runat="server" GroupingText="Company">
                        <table class="dataformV2">
                            <tr>
                                <td class="label">
                                    <D:Label ID="lblCompanyNameCaption" runat="server">Bedrijfsnaam</D:Label>
                                </td>
                                <td class="label">
                                    <D:Label ID="lblCompanyName" runat="server" LocalizeText="false" CssClass="lblInstallationValue"></D:Label>
                                </td>
                                <td class="label">
                                    <D:Label ID="lblUsernameCaption" runat="server">Gebruikersnaam</D:Label>
                                </td>
                                <td class="label">
                                    <D:Label ID="lblUsername" runat="server" LocalizeText="false" CssClass="lblInstallationValue"></D:Label>
                                </td>                                
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:Label ID="lblCompanyIdCaption" runat="server">Company Id</D:Label>
                                </td>
                                <td class="label">
                                    <D:Label ID="lblCompanyId" runat="server" LocalizeText="false" CssClass="lblInstallationValue"></D:Label>
                                </td>
                                <td class="label">
                                    <D:Label ID="lblPasswordCaption" runat="server">Wachtwoord</D:Label>
                                </td>
                                <td class="label">
                                    <D:Label ID="lblPasswordDecrypted" runat="server" LocalizeText="false" CssClass="lblInstallationValue"></D:Label>
                                </td>                                
                            </tr>
                        </table>
                    </D:Panel>
                    <D:Panel ID="pnlNotes" runat="server" GroupingText="Notes" Visible="False">
	                    <br/>
	                    <D:TextBoxMultiLine ID="tbNotes" runat="server" Rows="15" UseDataBinding="false" ClientIDMode="Static"></D:TextBoxMultiLine>
                    </D:Panel>
                    <D:Panel ID="pnlTerminals" runat="server" GroupingText="Terminals">
                        <br/>
                        <D:PlaceHolder ID="plhTerminals" runat="server"></D:PlaceHolder>
                    </D:Panel>
                    <D:Panel ID="pnlClients" runat="server" GroupingText="Clients">
                        <br/>
                        <D:PlaceHolder ID="plhClients" runat="server"></D:PlaceHolder>
                    </D:Panel>                    
                    <D:Panel ID="pnlDeliverypointgroups" runat="server" GroupingText="Deliverypointgroups">
                        <br/>
                        <D:PlaceHolder ID="plhDeliverypointgroups" runat="server"></D:PlaceHolder>
                    </D:Panel>                    
                    <D:Panel ID="pnlAction" runat="server" GroupingText="Actions">
                        <table class="dataformV2">
                            <tr>
                                <td class="label">
                                    <D:Label ID="lblClientV1Label" runat="server">API V1</D:Label>
                                </td>
                                <td class="label">
                                    <D:Label ID="lblClientsV1" runat="server" LocalizeText="false" CssClass="lblInstallationValue"></D:Label>
                                </td>
                                <td class="label">
                                    <D:Label ID="lblClientV2Label" runat="server">API V2</D:Label>
                                </td>
                                <td class="label">
                                    <D:Label ID="lblClientsV2" runat="server" LocalizeText="false" CssClass="lblInstallationValue"></D:Label>
                                </td>                                
                            </tr>
                        </table>
                        <br/>
                        <D:HyperLink runat="server" ID="lnkClientStatus" Text="Company Client Statuses" Target="_blank"/>
                    </D:Panel> 
                </Controls>
            </X:TabPage>            
        </TabPages>
    </X:PageControl>
</div>
</asp:Content>