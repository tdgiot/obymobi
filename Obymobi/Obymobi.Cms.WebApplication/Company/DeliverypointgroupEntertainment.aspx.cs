using System;
using Obymobi.Enums;
using Obymobi.Logic.Cms;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;

namespace Obymobi.ObymobiCms.Company
{
    public partial class DeliverypointgroupEntertainment : Dionysos.Web.UI.PageLLBLGenEntity
    {
        #region EventHandlers

        protected override void OnInit(EventArgs e)
        {
            this.DataSourceLoaded += DeliverypointgroupEntertainment_DataSourceLoaded;
            base.OnInit(e);
        }

        private void DeliverypointgroupEntertainment_DataSourceLoaded(object sender)
        {
            Response.Redirect("~/Generic/Entertainment.aspx?id=" + ((DeliverypointgroupEntertainmentEntity)this.DataSource).EntertainmentId);
            //this.SetGui();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        #endregion

        #region Methods

        private void SetGui()
        {
            // Init the entertainments
            EntertainmentCollection entertainmentCollection = new EntertainmentCollection();
            IncludeFieldsList entertainmentFields = new IncludeFieldsList(EntertainmentFields.Name);
            SortExpression entertainmentSort = new SortExpression(EntertainmentFields.Name | SortOperator.Ascending);
            PredicateExpression entertainmentFilter = new PredicateExpression(EntertainmentFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            entertainmentFilter.AddWithOr(EntertainmentFields.CompanyId == DBNull.Value);
            entertainmentCollection.GetMulti(entertainmentFilter, 0, entertainmentSort, null, null, entertainmentFields, 0, 0);
            this.ddlEntertainmentId.BindEntityCollection(entertainmentCollection, EntertainmentFields.Name, EntertainmentFields.EntertainmentId);

            // Init the deliverypointgroups
            DeliverypointgroupCollection deliverypointgroupCollection = new DeliverypointgroupCollection();
            IncludeFieldsList deliverypointgroupFields = new IncludeFieldsList();
            deliverypointgroupFields.Add(DeliverypointgroupFields.Name);
            SortExpression deliverypointgroupSort = new SortExpression(DeliverypointgroupFields.Name | SortOperator.Ascending);
            PredicateExpression deliverypointgroupFilter = new PredicateExpression(DeliverypointgroupFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            deliverypointgroupCollection.GetMulti(deliverypointgroupFilter, 0, deliverypointgroupSort, null, null, deliverypointgroupFields, 0, 0);
            this.ddlDeliverypointgroupId.BindEntityCollection(deliverypointgroupCollection, DeliverypointgroupFields.Name, DeliverypointgroupFields.DeliverypointgroupId);
        }

        #endregion
    }
}
