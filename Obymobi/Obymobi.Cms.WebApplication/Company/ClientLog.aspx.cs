﻿using System;
using System.Globalization;
using System.IO;
using System.Net;
using System.Web;
using Dionysos;
using Obymobi.Data.EntityClasses;
using Obymobi.ObymobiCms.MasterPages;

namespace Obymobi.ObymobiCms.Company
{
    public partial class ClientLog : Dionysos.Web.UI.PageLLBLGenEntity
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Hide toolbar buttons, only show cancel (aka. go back)
            var master = this.Master as MasterPageEntity;
            if (master != null)
            {
                master.ToolBar.Visible = false;
            }
            this.btBack.Attributes.Add("onClick", "javascript:history.back(); return false;");

            this.lblCreatedValue.Text = (this.DataSourceAsClientLogEntity.CreatedUTC.HasValue ? this.DataSourceAsClientLogEntity.CreatedUTC.Value.UtcToLocalTime().ToString(CultureInfo.InvariantCulture) : "Onbekend");
            this.lblTypeValue.Text = this.DataSourceAsClientLogEntity.TypeName;
            this.lblFromStatusValue.Text = this.DataSourceAsClientLogEntity.FromStatusName;
            this.lblToStatusValue.Text = this.DataSourceAsClientLogEntity.ToStatusName;
            this.lblFromOperationModeValue.Text = this.DataSourceAsClientLogEntity.FromOperationModeName;
            this.lblToOperationModeValue.Text = this.DataSourceAsClientLogEntity.ToOperationModeName;
            this.tbMessage.Text = this.DataSourceAsClientLogEntity.Message;

            if (DataSourceAsClientLogEntity.ClientLogFileId.HasValue)
            {
                this.phlNormalLog.Visible = false;
                this.phlShippedLog.Visible = true;

                this.lblLogMessage.Value = DataSourceAsClientLogEntity.Message;

                string url = DataSourceAsClientLogEntity.ClientLogFileEntity.Message;
                this.hlLogUrl.Text = url;
                this.hlLogUrl.NavigateUrl = url;

                this.btnDownloadLog.Click += btnDownloadLog_Click;                

                //try
                //{
                //    using (var client = new WebClient())
                //    {
                //        string url = DataSourceAsClientLogEntity.ClientLogFileEntity.Message;

                //        this.tbLog.Text = client.DownloadString(url);

                //        this.btnDownloadLog.NavigateUrl = url;
                //    }
                //}
                //catch
                //{
                //    this.tbLog.Visible = false;
                //}
            }
        }

        void btnDownloadLog_Click(object sender, EventArgs e)
        {
            Uri uriResult;
            if (Uri.TryCreate(DataSourceAsClientLogEntity.ClientLogFileEntity.Message, UriKind.Absolute, out uriResult) &&
                (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps))
            {
                DownloadExternalFile(uriResult.AbsoluteUri, uriResult.Segments[uriResult.Segments.Length - 1]);
            }
            else
            {
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();
                Response.AddHeader("content-disposition", "attachment;filename=" + DataSourceAsClientLogEntity.Message + ".txt");
                Response.ContentType = "text/plain";
                Response.Write(DataSourceAsClientLogEntity.ClientLogFileEntity.Message);
                Response.End();
            }
        }

        void DownloadExternalFile(string url, string fileName)
        {
            Stream stream = null;

            const int bytesToRead = 10000;
            byte[] buffer = new Byte[bytesToRead];

            try
            {
                //Create a WebRequest to get the file
                HttpWebRequest fileReq = (HttpWebRequest)WebRequest.Create(url);
                HttpWebResponse fileResp = (HttpWebResponse)fileReq.GetResponse();

                if (fileReq.ContentLength > 0)
                    fileResp.ContentLength = fileReq.ContentLength;

                stream = fileResp.GetResponseStream();
                if (stream == null)
                    return;

                var resp = HttpContext.Current.Response;

                resp.ContentType = "application/octet-stream";
                resp.AddHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
                resp.AddHeader("Content-Length", fileResp.ContentLength.ToString());

                int length;
                do
                {
                    if (resp.IsClientConnected)
                    {
                        length = stream.Read(buffer, 0, bytesToRead);
                     
                        resp.OutputStream.Write(buffer, 0, length);
                        resp.Flush();

                        buffer = new Byte[bytesToRead];
                    }
                    else
                    {
                        length = -1;
                    }
                } while (length > 0);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Close();
                }
            }
        }

        #region Properties

        /// <summary>
        /// Return the page's datasource as a SurveyQuestionEntity
        /// </summary>
        public ClientLogEntity DataSourceAsClientLogEntity
        {
            get
            {
                return this.DataSource as ClientLogEntity;
            }
        }

        #endregion
    }
}