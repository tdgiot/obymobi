﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.RelationClasses;
using System.Text;
using System.Data;
using Obymobi.Data.DaoClasses;
using Dionysos;
using Dionysos.Web.UI.WebControls;
using Dionysos.Web.UI.DevExControls;
using Obymobi.Data;
using Dionysos.Data.LLBLGen;
using Obymobi.Enums;
using Dionysos.Web;
using Dionysos.Interfaces;

namespace Obymobi.ObymobiCms.Company
{
	public partial class SurveyPage : Dionysos.Web.UI.PageLLBLGenEntity
	{
        Generic.UserControls.MediaCollection mediaCollection;

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            this.DataSourceLoaded += new Dionysos.Delegates.Web.DataSourceLoadedHandler(SurveyPage_DataSourceLoaded);			
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack && !this.DataSourceAsSurveyPageEntity.IsNew)
                this.SetWarnings();
        }

        private void SetWarnings()
        {
             // Check for survey questions

            if (this.DataSourceAsSurveyPageEntity.SurveyQuestionCollection.Count == 0)
            {
                this.AddInformator(Dionysos.Web.UI.InformatorType.Warning, this.Translate("UnableToSelectMedia", "U kunt pas plaatjes toevoegen indien er vragen op de pagina zijn toegevoegd."));
                this.Validate();
            }
        }

        private void LoadUserControls()
        {
            this.tabsMain.AddTabPage("Vragen", "SurveyQuestionCollection", "~/Company/SubPanels/SurveyQuestionCollection.ascx");
            //this.tabsMain.AddTabPage("Afbeeldingen", "Media", "~/Generic/UserControls/MediaCollection.ascx");

            TabPage mediaPage = new TabPage();
            mediaPage.Text = "Afbeeldingen";
            mediaPage.Name = "Media";

            this.mediaCollection = this.LoadControl<Generic.UserControls.MediaCollection>("~/Generic/UserControls/MediaCollection.ascx");
            this.mediaCollection.Parent = this.Page as IGuiEntitySimple;
            mediaPage.Controls.Add(this.mediaCollection);

            this.tabsMain.TabPages.Add(mediaPage);
        }

        private void SetFilters()
        { 
        }

        private void SetGui()
        {
            // Set the question if a parameter is provided
            if (this.DataSourceAsSurveyPageEntity.IsNew)
            {
                int surveyId;
                if (QueryStringHelper.TryGetValue("SurveyId", out surveyId))
                {
                    if (surveyId > 0)
                        this.DataSourceAsSurveyPageEntity.SurveyId = surveyId;
                }
            }

            if (this.DataSourceAsSurveyPageEntity.SurveyId > 0)
                this.ddlSurveyId.Enabled = false;

            // Only show media tab if there are questions on this page
            if (this.DataSourceAsSurveyPageEntity.SurveyQuestionCollection.Count == 0)
            {
                this.tabsMain.GetTabPageByName("Media").Visible = false;
            }

            // Set the media types on the media page based on the survey questions on the page
            this.mediaCollection.MediaTypes = this.GetMediaTypes();
        }

        private void SurveyPage_DataSourceLoaded(object sender)
        {
            this.SetFilters();
            this.SetGui();
        }

        private List<MediaType> GetMediaTypes()
        { 
            List<MediaType> mediaTypes = new List<MediaType>();

            var descriptiveQuestion = this.DataSourceAsSurveyPageEntity.SurveyQuestionCollection.SingleOrDefault(sq => sq.TypeAsEnum == SurveyQuestionType.DescriptiveText);

            if (descriptiveQuestion != null)
            {
                mediaTypes.Add(MediaType.SurveyPageBottom1280x800);
                mediaTypes.Add(MediaType.SurveyPageLogo1280x800);
            }
            else
            {
                mediaTypes.Add(MediaType.SurveyPageRight1280x800);
                mediaTypes.Add(MediaType.SurveyPageRightHalf1280x800);
            }
            
            return mediaTypes;
        }

        #region Properties

        /// <summary>
        /// Return the page's datasource as a SurveyPageEntity
        /// </summary>
        public SurveyPageEntity DataSourceAsSurveyPageEntity
        {
            get
            {
                return this.DataSource as SurveyPageEntity;
            }
        }

        #endregion
	}
}
