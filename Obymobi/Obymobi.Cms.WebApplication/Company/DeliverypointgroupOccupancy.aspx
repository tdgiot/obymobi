﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.DeliverypointgroupOccupancy" Codebehind="DeliverypointgroupOccupancy.aspx.cs" %>
<%@ Register Namespace="DevExpress.Web" TagPrefix="dx" Assembly="DevExpress.Web.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="cplhTitleHolder">Deliverypointgroup Occupancy</asp:Content>
<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="cplhToolbarHolder"></asp:Content>
<asp:Content ID="Content6" runat="server" ContentPlaceHolderID="cplhContentHolder">
<div>
    <script type="text/javascript">
        function OnInit(s, e) {
            var calendar = s.GetCalendar();
            calendar.owner = s;
            //calendar.SetVisible(false);
            calendar.GetMainElement().style.opacity = '0';
        }

        function OnDropDown(s, e) {
            var calendar = s.GetCalendar();
            var fastNav = calendar.fastNavigation;
            fastNav.activeView = calendar.GetView(0, 0);
            fastNav.Prepare();
            fastNav.GetPopup().popupVerticalAlign = "Below";
            fastNav.GetPopup().ShowAtElement(s.GetMainElement());

            fastNav.OnOkClick = function() {
                var parentDateEdit = this.calendar.owner;
                var currentDate = new Date(fastNav.activeYear, fastNav.activeMonth, 1);
                parentDateEdit.SetDate(currentDate);
                parentDateEdit.HideDropDown();
            };

            fastNav.OnCancelClick = function() {
                var parentDateEdit = this.calendar.owner;
                parentDateEdit.HideDropDown();
            };
        }
    </script>    
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Occupancy" Name="Generic">
				<Controls>
				    <table class="dataformV2">
						<tr>
							<td class="label">
								<D:Label ID="lblMonth" runat="server">Maand</D:Label>
							</td>
							<td class="control">
								<dx:ASPxDateEdit ID="de" runat="server" ShowShadow="false" DisplayFormatString="MMM yyyy">
                                    <ClientSideEvents DropDown="OnDropDown" Init="OnInit" />
                                </dx:ASPxDateEdit>
							</td>
							<td class="label">
								
							</td>
							<td class="control">
                                
							</td>
					    </tr>
                        <tr>
							<td class="label">
								
							</td>
							<td class="control">
								<D:Button ID="btnApplyFilter" LocalizeText="True" Text="Toepassen" runat="server" />
							</td>
							<td class="label">
								
							</td>
							<td class="control">
                                
							</td>
					    </tr>
					    <tr>						
							<td colspan="4">
                                <dx:ASPxGridView ID="grid" runat="server" OnCellEditorInitialize="grid_CellEditorInitialize" OnInit="grid_Init">
                                </dx:ASPxGridView>
							</td>        				
						</tr>
					</table>
                </Controls>
            </X:TabPage>            								
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>