﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Utils;
using DevExpress.Web;
using DevExpress.Web.Internal;
using DevExpress.Web.ASPxTreeList;
using Obymobi.Data.EntityClasses;

namespace Obymobi.ObymobiCms.Company
{
    public partial class StationList : Dionysos.Web.UI.PageLLBLGenEntity
    {
        #region Fields

        private StationDataSource stationDataSource;

        #endregion

        #region Methods

        private void CreateTreeList()
        {
            this.tlStations.SettingsEditing.Mode = TreeListEditMode.EditFormAndDisplayNode;
            this.tlStations.SettingsPopupEditForm.Width = 600;
            this.tlStations.SettingsBehavior.ColumnResizeMode = ColumnResizeMode.Disabled;
            this.tlStations.SettingsBehavior.AllowSort = false;
            this.tlStations.SettingsBehavior.AllowDragDrop = false;
            this.tlStations.ClientInstanceName = this.TreelistId;
            this.tlStations.AutoGenerateColumns = false;
            this.tlStations.SettingsEditing.AllowNodeDragDrop = true;
            this.tlStations.SettingsEditing.AllowRecursiveDelete = true;
            this.tlStations.Settings.GridLines = GridLines.Both;
            this.tlStations.KeyFieldName = "StationId";
            this.tlStations.Width = Unit.Pixel(736);
            this.tlStations.ClientSideEvents.EndDragNode = this.EndDragNodeMethod;
            this.tlStations.ClientSideEvents.NodeDblClick = this.NodeDblClickMethod;
            this.tlStations.CustomErrorText += this.tlStations_CustomErrorText;            

            TreeListTextColumn nameColumn = new TreeListTextColumn();
            nameColumn.Caption = "Name";
            nameColumn.Width = Unit.Pixel(167);
            nameColumn.VisibleIndex = 0;
            nameColumn.FieldName = "Name";
            nameColumn.CellStyle.Wrap = DefaultBoolean.True;
            nameColumn.EditFormSettings.VisibleIndex = 0;
            this.tlStations.Columns.Add(nameColumn);

            TreeListTextColumn sceneColumn = new TreeListTextColumn();
            sceneColumn.Caption = "Scene";
            sceneColumn.Width = Unit.Pixel(167);
            sceneColumn.VisibleIndex = 1;
            sceneColumn.FieldName = "Scene";
            sceneColumn.CellStyle.Wrap = DefaultBoolean.True;
            sceneColumn.EditFormSettings.VisibleIndex = 2;
            this.tlStations.Columns.Add(sceneColumn);

            TreeListTextColumn channelColumn = new TreeListTextColumn();
            channelColumn.Caption = "Channel";
            channelColumn.Width = Unit.Pixel(167);
            channelColumn.VisibleIndex = 2;
            channelColumn.FieldName = "Channel";
            channelColumn.CellStyle.Wrap = DefaultBoolean.True;
            channelColumn.EditFormSettings.VisibleIndex = 1;
            this.tlStations.Columns.Add(channelColumn);

            TreeListCommandColumn editCommandColumn = new TreeListCommandColumn();
            editCommandColumn.VisibleIndex = 4;
            editCommandColumn.Width = Unit.Pixel(60);
            editCommandColumn.ShowNewButtonInHeader = true;
            editCommandColumn.EditButton.Visible = true;
            editCommandColumn.NewButton.Visible = false;
            editCommandColumn.DeleteButton.Visible = true;
            editCommandColumn.ButtonType = ButtonType.Image;
            editCommandColumn.EditButton.Image.Url = this.ResolveUrl("~/images/icons/pencil.png");
            editCommandColumn.DeleteButton.Image.Url = this.ResolveUrl("~/images/icons/delete.png");
            editCommandColumn.CancelButton.Image.Url = this.ResolveUrl("~/images/icons/cross_grey.png");
            editCommandColumn.NewButton.Image.Url = this.ResolveUrl("~/images/icons/add2.png");
            editCommandColumn.UpdateButton.Image.Url = this.ResolveUrl("~/images/icons/disk.png");
            this.tlStations.Columns.Add(editCommandColumn);

            TreeListHyperLinkColumn editPageColumn = new TreeListHyperLinkColumn();
            editPageColumn.Name = "EditCommand";
            editPageColumn.Caption = " ";
            editPageColumn.VisibleIndex = 5;
            editPageColumn.Width = Unit.Pixel(15);
            editPageColumn.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            editPageColumn.PropertiesHyperLink.ImageUrl = "~/images/icons/page_edit.png";
            editPageColumn.EditCellStyle.CssClass = "hidden";
            editPageColumn.FieldName = "StationId";
            editPageColumn.EditFormSettings.VisibleIndex = 2;
            editPageColumn.EditFormCaptionStyle.CssClass = "hidden";
            this.tlStations.Columns.Add(editPageColumn);

            this.tlStations.HtmlDataCellPrepared += this.tlStations_HtmlDataCellPrepared;
            this.tlStations.CustomCallback += this.tlStations_CustomCallback;

            this.InitDataSource();            
        }

        private void InitDataSource()
        {
            if (this.DataSource != null)
            {
                this.stationDataSource = new StationDataSource(this.DataSourceAsStationListEntity.StationListId);
                this.tlStations.DataSource = this.stationDataSource;
            }
        }

        private void LoadUserControls()
        {
            this.btDeleteStationsTop.Click += BtDeleteStations_Click;
            this.btDeleteStationsBottom.Click += BtDeleteStations_Click;
        }                

        private void SetGui()
        {
            if (!this.DataSourceAsStationListEntity.IsNew)
            {
                this.CreateTreeList();
                this.plhStations.Visible = true;
            }
        }

        private void DeleteSelectedStations()
        {
            List<TreeListNode> selectedNodes = this.GetSelectedNodes();
            if (selectedNodes.Count <= 0)
            {
                this.MultiValidatorDefault.AddError("No item has been selected to be deleted.");
                this.Validate();
            }
            else
            {
                this.DeleteItems(selectedNodes);

                this.InitDataSource();
                this.tlStations.DataBind();

                this.AddInformatorInfo("The selected item(s) has been deleted.");
                this.Validate();
            }
        }

        private void DeleteItems(List<TreeListNode> nodes)
        {
            foreach (TreeListNode node in nodes)
            {
                if (int.TryParse(node.Key, out int stationId))
                {
                    this.stationDataSource.Manager.Delete(stationId);
                }
            }
        }

        private List<TreeListNode> GetSelectedNodes()
        {
            List<TreeListNode> selectedNodes = new List<TreeListNode>();

            foreach (TreeListNode node in this.tlStations.Nodes)
            {
                if (node.Selected)
                {
                    selectedNodes.Add(node);
                }
            }

            return selectedNodes;
        }

        #endregion

		#region Properties
		
		public StationListEntity DataSourceAsStationListEntity
		{
			get
			{
				return this.DataSource as StationListEntity;
			}
		}

        private string TreelistId
        {
            get { return string.Format("{0}_treelist", this.ID); }
        }

        #endregion

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            this.DataSourceLoaded += StationList_DataSourceLoaded;
            base.OnInit(e);
            
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.tlStations.DataBind();
        }

        private void StationList_DataSourceLoaded(object sender)
		{
            this.SetGui();
		}

        public override bool Save()
        {
            if (this.PageMode == Dionysos.Web.PageMode.Add)
                this.DataSourceAsStationListEntity.CompanyId = CmsSessionHelper.CurrentCompanyId;

            return base.Save();
        }

        private void tlStations_CustomErrorText(object sender, TreeListCustomErrorTextEventArgs e)
        {
            if (e.Exception.InnerException != null)
            {
                ObymobiException exception = e.Exception.InnerException as ObymobiException;
                if (exception != null)
                {
                    e.ErrorText = exception.AdditionalMessage;
                }
                else
                {
                    e.ErrorText = e.Exception.InnerException.Message;
                }
            }
        }

        private void tlStations_HtmlDataCellPrepared(object sender, TreeListHtmlDataCellEventArgs e)
        {
            if (e.Column.Name == "EditCommand" && e.NodeKey != null)
            {
                string navigateUrl = string.Format("~/Company/Station.aspx?id={0}", e.NodeKey);

                foreach (Control item in e.Cell.Controls)
                {
                    HyperLinkDisplayControl link = item as HyperLinkDisplayControl;
                    if (link != null)
                    {
                        link.NavigateUrl = navigateUrl;
                        break;
                    }
                }
            }
        }

        private void tlStations_CustomCallback(object sender, TreeListCustomCallbackEventArgs e)
        {
            // Node dragged callback
            string[] nodes = e.Argument.Split(new char[] { ':' });
            if (nodes.Length <= 1)
            {
                return;
            }

            if (!int.TryParse(nodes[0], out int draggedStationId) || !int.TryParse(nodes[1], out int draggedUponStationId))
            {
                return;
            }
            
            this.stationDataSource.Manager.DraggedToSort(draggedStationId, draggedUponStationId);
            this.tlStations.DataBind();
        }

        private void BtDeleteStations_Click(object sender, EventArgs e)
        {
            this.DeleteSelectedStations();
        }

        private string EndDragNodeMethod
        {
            get
            {
                return @" function(s, e) {                                                                
                                e.cancel = true;
		                        var key = s.GetNodeKeyByRow(e.targetElement);
		                        " + this.TreelistId + @".PerformCustomCallback(e.nodeKey + ':' + key);	                            
                            }";
            }
        }

        private string NodeDblClickMethod
        {
            get { return @" function(s, e) {
	                            " + this.TreelistId + @".StartEdit(e.nodeKey);
		                        e.cancel = true;	                                                                    
                            }"; }
        }

        #endregion
    }
}
