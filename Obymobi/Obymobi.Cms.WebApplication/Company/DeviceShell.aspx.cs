﻿using System;
using System.Web.Services;
using Dionysos;
using Dionysos.Web;
using Dionysos.Web.UI;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Comet;
using Obymobi.Logic.Comet.Netmessages;
using Obymobi.Logic.HelperClasses;
using Obymobi.Security;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System.Data;
using Obymobi.Logic.Model;

namespace Obymobi.ObymobiCms.Company
{
	public partial class DeviceShell : PageDefault
	{
        #region Web methods

        [WebMethod(EnableSession = true)]
        public static object[] GetAuthentication(string pokeInIdentifier)
        {
            long timestamp = DateTime.UtcNow.ToUnixTime();
            string identifier = CometConstants.CometIdentifierCms + "_" + CmsSessionHelper.CurrentUser.UserId;
            string hash = Hasher.GetHashFromParameters(CometConstants.CometInternalSalt, timestamp, identifier, pokeInIdentifier);

            return new object[] { timestamp, identifier, hash };
        }

        [WebMethod(EnableSession = true)]
        public static string NetmessageSetClientType()
        {
            NetmessageSetClientType netmessage = new NetmessageSetClientType();
            netmessage.ClientType = NetmessageClientType.Cms;

            return netmessage.ToJson();
        }

        [WebMethod(EnableSession = true)]
        public static string NetmessageConnectToAgent(string deviceMac)
        {
            NetmessageConnectToAgent netmessage = new NetmessageConnectToAgent();
            netmessage.ReceiverIdentifier = deviceMac;
            netmessage.AgentMacAddress = deviceMac;
            return netmessage.ToJson();
        }

        [WebMethod(EnableSession = true)]
        public static string NetmessagePong()
        {
            NetmessagePong netmessage = new NetmessagePong();
            return netmessage.ToJson();
        }

        [WebMethod(EnableSession = true)]
        public static string SendCommand(string macAddress, string command)
        {
            NetmessageAgentCommandRequest netmessage = new NetmessageAgentCommandRequest();
            netmessage.Guid = GuidUtil.CreateGuid();
            netmessage.ReceiverIdentifier = macAddress;
            netmessage.AgentMacAddress = macAddress;
            netmessage.Command = command;

            DeviceShell.SaveNetmessage(netmessage);

            return netmessage.ToJson();
        }

        [WebMethod(EnableSession = true)]
	    public static void SwitchTabCommand(string identifier, string uiTabId, bool client)
        {
            NetmessageSwitchTab netmessage = new NetmessageSwitchTab();
            netmessage.UITabId = uiTabId;

            if (client)
            {
                netmessage.ReceiverClientId = int.Parse(identifier);
            }
            else
            {
                netmessage.ReceiverTerminalId = int.Parse(identifier);
            }

            CometHelper.Instance.SendMessage(netmessage);
        }

	    private static void SaveNetmessage(NetmessageAgentCommandRequest netmessage)
	    {
            using (Transaction transaction = new Transaction(IsolationLevel.ReadUncommitted, "CMS-DeviceShell-SendCommand-" + netmessage.MessageType))
            {
                try
                {
                    // Save netmessage in database so it can be picked up by SupportTools even if the connection is down
                    NetmessageEntity netmessageEntity = NetmessageHelper.CreateNetmessageEntityFromModel(netmessage);
                    netmessageEntity.AddToTransaction(transaction);
                    netmessageEntity.Save();

                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                }
            }
	    }

	    #endregion

        protected void Page_Init(object sender, EventArgs e)
		{
            this.RenderCommands();
		}	    

	    protected void Page_Load(object sender, EventArgs e)
	    {
	        
	    }

	    private void RenderCommands()
	    {
            DeviceOperatingSystem os = this.GetDeviceOperatingSystem();
            if (os == DeviceOperatingSystem.Android)
            {
                // Render android commands
                this.tabsMain.AddTabPage("Commands", "AndroidCommandPanel", "~/Company/SubPanels/DeviceShellPanels/AndroidCommandPanel.ascx", false);
            }
            else if (os == DeviceOperatingSystem.Windows)
            {
                // Render windows commands
                this.tabsMain.AddTabPage("Commands", "WindowsCommandPanel", "~/Company/SubPanels/DeviceShellPanels/WindowsCommandPanel.ascx", false);
            }
	    }

	    private DeviceOperatingSystem GetDeviceOperatingSystem()
        {
            DeviceOperatingSystem os = DeviceOperatingSystem.Android;

            int terminalId;
            if (QueryStringHelper.TryGetValue("TerminalId", out terminalId))
            {   
                IncludeFieldsList includes = new IncludeFieldsList(TerminalFields.HandlingMethod);
                TerminalCollection terminals = new TerminalCollection();
                terminals.GetMulti(new PredicateExpression(TerminalFields.TerminalId == terminalId), includes, 0);

                if (terminals.Count > 0 && terminals[0].HandlingMethod == (int)TerminalType.OnSiteServer)
                {
                    os = DeviceOperatingSystem.Windows;                    
                }
            }

            return os;
        }

        private enum DeviceOperatingSystem
        {
            Android,
            Windows
        }
	}
}