﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.UITab" Codebehind="UITab.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
                    <D:PlaceHolder runat="server">
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblCaption">Caption</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbCaption" runat="server" IsRequired="true"></D:TextBoxString>
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblType">Type</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
                                <X:ComboBoxInt ID="cbType" runat="server" IsRequired="true" UseDataBinding="true"></X:ComboBoxInt>
							</td>
					    </tr>	
                        <tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblWidth">Width</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxInt ID="tbWidth" runat="server"></D:TextBoxInt>
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblSortOrder">Sort order</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxInt ID="tbSortOrder" runat="server"></D:TextBoxInt>
							</td>
					    </tr>	
                        <tr>
                            <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblVisible">Visible</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:CheckBox runat="server" ID="cbVisible" />
							</td>
                        </tr>			                        		    
                        <tr>
                            <D:PlaceHolder ID="plhZoom" runat="server" Visible="false">
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblZoom">Zoom</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:TextBoxInt ID="tbZoom" runat="server"></D:TextBoxInt>
                                </td>	
                            </D:PlaceHolder>
                            <D:PlaceHolder ID="plhRestrictedAccess" runat="server" Visible="false">
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" ID="lblRestrictedAccess">Restricted access</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:CheckBox ID="cbRestrictedAccess" runat="server" />
                                </td>
                            </D:PlaceHolder>
                            <D:PlaceHolder ID="plhCategory" runat="server" Visible="false">
							        <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" id="lblCategoryId">Category</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlCategoryId" IncrementalFilteringMode="StartsWith" EntityName="Category" TextField="Name" ValueField="CategoryId" PreventEntityCollectionInitialization="true" />
                                    </td>	
                            </D:PlaceHolder>
                            <D:PlaceHolder ID="plhEntertainment" runat="server" Visible="false">

                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" id="lblEntertainmentId">Entertainment</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlEntertainmentId" IncrementalFilteringMode="StartsWith" EntityName="Entertainment" TextField="Name" ValueField="EntertainmentId" PreventEntityCollectionInitialization="true" />
                                    </td>			
                            </D:PlaceHolder>
                            <D:PlaceHolder ID="plhSite" runat="server" Visible="false">

							        <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" id="lblSiteId">Site</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlSiteId" EntityName="Site" TextField="Name" ValueField="SiteId" PreventEntityCollectionInitialization="true" />
                                    </td>	
                            </D:PlaceHolder>
                            <D:PlaceHolder ID="plhMap" runat="server" Visible="false">
							        <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" id="lblMapId">Map</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlMapId" IncrementalFilteringMode="StartsWith" EntityName="Map" TextField="Name" ValueField="MapId" PreventEntityCollectionInitialization="true" />
                                    </td>	
                            </D:PlaceHolder>
					    </tr>
                        <D:PlaceHolder ID="plhUrl" runat="server" Visible="false">
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblURL">URL</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control" colspan="3">
                                    <D:TextBoxString ID="tbURL" runat="server"></D:TextBoxString>
                                </td>
                            </tr>	    
                        </D:PlaceHolder>
                         <D:PlaceHolder ID="plhAllCategoryVisible" runat="server" Visible="false">
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblAllCategoryVisible">All category visible</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:CheckBox ID="cbAllCategoryVisible" runat="server" />
                                </td>
                                <td class="label">
                                </td>
                                <td class="control">
                                </td>
                            </tr>	    
                        </D:PlaceHolder>                        
                        <D:PlaceHolder ID="plhMessage" runat="server" Visible="false">
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblShowPmsMessagegroups">Show PMS messagegroups</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:CheckBox ID="cbShowPmsMessagegroups" runat="server" />
                                </td>
                            </tr>
                        </D:PlaceHolder>
					 </table>	
                    </D:PlaceHolder>
				</Controls>
			</X:TabPage>	
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>

