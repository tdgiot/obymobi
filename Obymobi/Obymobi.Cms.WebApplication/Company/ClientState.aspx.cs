﻿using System;
using Dionysos;
using Obymobi.Data.EntityClasses;
using Obymobi.ObymobiCms.MasterPages;

namespace Obymobi.ObymobiCms.Company
{
    public partial class ClientState : Dionysos.Web.UI.PageLLBLGenEntity
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Hide toolbar buttons, only show cancel (aka. go back)
            var master = this.Master as MasterPageEntity;
            if (master != null)
            {
                master.ToolBar.Visible = false;
            }

            this.btBack.Attributes.Add("onClick", "javascript:history.back(); return false;");

            this.lblCreatedValue.Text = (this.DataSourceAsClientStateEntity.CreatedUTC.HasValue ? this.DataSourceAsClientStateEntity.CreatedUTC.Value.UtcToLocalTime().ToString() : "Unknown");
            this.lblOnlineValue.Text = this.DataSourceAsClientStateEntity.Online.ToString();
            this.lblOperationModeValue.Text = this.DataSourceAsClientStateEntity.OperationModeAsEnum.ToString();
            this.lblLastBatteryLevelValue.Text = (this.DataSourceAsClientStateEntity.LastBatteryLevel.HasValue ? this.DataSourceAsClientStateEntity.LastBatteryLevel.Value.ToString() : "Unknown");
            this.lblLastIsChargingValue.Text = this.DataSourceAsClientStateEntity.LastIsCharging.ToString();
            this.tbMessage.Text = this.DataSourceAsClientStateEntity.Message;
        }

        #region Properties

        public ClientStateEntity DataSourceAsClientStateEntity
        {
            get
            {
                return this.DataSource as ClientStateEntity;
            }
        }

        #endregion
    }
}