﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.RelationClasses;
using System.Text;
using System.Data;
using Obymobi.Data.DaoClasses;
using Dionysos;
using Dionysos.Web.UI.WebControls;
using Dionysos.Web.UI.DevExControls;
using Obymobi.Data;
using Dionysos.Data.LLBLGen;
using Obymobi.Enums;
using Dionysos.Web;
using Obymobi.Logic.HelperClasses;
using System.IO;

namespace Obymobi.ObymobiCms.Company
{
	public partial class SurveyQuestion : Dionysos.Web.UI.PageLLBLGenEntity
	{
        #region Event Handlers
        
        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            this.DataSourceLoaded += new Dionysos.Delegates.Web.DataSourceLoadedHandler(SurveyQuestion_DataSourceLoaded);			
            base.OnInit(e);
        }

        private void SurveyQuestion_DataSourceLoaded(object sender)
        {
            this.SetGui();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!QueryStringHelper.HasValue("SurveyPageId") && !QueryStringHelper.HasValue("id"))
            {
                throw new Exception("Page must be called with a valid SurveyPageId or SurveyQuestionId (=id)");
            }
        }

        #endregion

        #region Methods

        private void SetGui()
        {
            // Question type specific
            SurveyQuestionType surveyQuestionType = this.DataSource.IsNew ? this.ddlType.ValidId.ToEnum<SurveyQuestionType>() : this.DataSourceAsSurveyQuestionEntity.TypeAsEnum;

            switch (surveyQuestionType)
            {
                case SurveyQuestionType.DescriptiveText:

                    this.plhNotesEnabled.Visible = false;
                    this.plhRequired.Visible = false;
                    
                    var descriptiveTextPanel = this.LoadControl<Subpanels.SurveyQuestionTypePanels.DescriptiveTextPanel>("~/Company/SubPanels/SurveyQuestionTypePanels/DescriptiveTextPanel.ascx");
                    descriptiveTextPanel.SurveyQuestionEntity = this.DataSourceAsSurveyQuestionEntity;
                    this.plhTypeSpecificSettings.Controls.Add(descriptiveTextPanel);

                    break;
                case SurveyQuestionType.SingleTextbox:

                    this.plhNotesEnabled.Visible = false;

                    var singleTextPanel = this.LoadControl<Subpanels.SurveyQuestionTypePanels.SingleTextboxPanel>("~/Company/SubPanels/SurveyQuestionTypePanels/SingleTextboxPanel.ascx");
                    singleTextPanel.SurveyQuestionEntity = this.DataSourceAsSurveyQuestionEntity;
                    this.plhTypeSpecificSettings.Controls.Add(singleTextPanel);

                    break;
                case SurveyQuestionType.MultipleChoice:

                    this.plhRequired.Visible = false;

                    this.tabsMain.AddTabPage("Antwoorden", "SurveyAnswerPanel", "~/Company/SubPanels/SurveyAnswerPanel.ascx");

                    var multipleChoicePanel = this.LoadControl<Subpanels.SurveyQuestionTypePanels.MultipleChoicePanel>("~/Company/SubPanels/SurveyQuestionTypePanels/MultipleChoicePanel.ascx");
                    multipleChoicePanel.SurveyQuestionEntity = this.DataSourceAsSurveyQuestionEntity;
                    this.plhTypeSpecificSettings.Controls.Add(multipleChoicePanel);

                    break;
                case SurveyQuestionType.Form:

                    this.plhRequired.Visible = false;

                    var formPanel = this.LoadControl<Subpanels.SurveyQuestionTypePanels.FormPanel>("~/Company/SubPanels/SurveyQuestionTypePanels/FormPanel.ascx");
                    formPanel.SurveyQuestionEntity = this.DataSourceAsSurveyQuestionEntity;
                    this.plhTypeSpecificSettings.Controls.Add(formPanel);

                    break;
                case SurveyQuestionType.Matrix:

                    this.plhRequired.Visible = false;
                    this.plhQuestion.Visible = false;

                    var matrixPanel = this.LoadControl<Subpanels.SurveyQuestionTypePanels.MatrixPanel>("~/Company/SubPanels/SurveyQuestionTypePanels/MatrixPanel.ascx");
                    matrixPanel.SurveyQuestionEntity = this.DataSourceAsSurveyQuestionEntity;
                    this.plhTypeSpecificSettings.Controls.Add(matrixPanel);
                    
                    break;
                case SurveyQuestionType.Ranking:

                    this.plhQuestion.Visible = false;

                    var rankingPanel = this.LoadControl<Subpanels.SurveyQuestionTypePanels.RankingPanel>("~/Company/SubPanels/SurveyQuestionTypePanels/RankingPanel.ascx");
                    rankingPanel.SurveyQuestionEntity = this.DataSourceAsSurveyQuestionEntity;
                    this.plhTypeSpecificSettings.Controls.Add(rankingPanel);

                    break;
                case SurveyQuestionType.Rating:

                    this.plhQuestion.Visible = false;

                    var ratingPanel = this.LoadControl<Subpanels.SurveyQuestionTypePanels.RatingPanel>("~/Company/SubPanels/SurveyQuestionTypePanels/RatingPanel.ascx");
                    ratingPanel.SurveyQuestionEntity = this.DataSourceAsSurveyQuestionEntity;
                    this.plhTypeSpecificSettings.Controls.Add(ratingPanel);

                    break;
                case SurveyQuestionType.YesNo:

                    this.plhNotesEnabled.Visible = false;

                    var yesNoPanel = this.LoadControl<Subpanels.SurveyQuestionTypePanels.YesNoPanel>("~/Company/SubPanels/SurveyQuestionTypePanels/YesNoPanel.ascx");
                    yesNoPanel.SurveyQuestionEntity = this.DataSourceAsSurveyQuestionEntity;
                    this.plhTypeSpecificSettings.Controls.Add(yesNoPanel);

                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        private void LoadUserControls()
        {
            // Fill the available handler types				
            this.ddlType.DataBindEnumStringValuesAsDataSource(typeof(SurveyQuestionType));
        }

        public override bool Save()
        {
            if (this.DataSourceAsSurveyQuestionEntity.IsNew)
            {
                int surveyPageId = int.Parse(Request.QueryString["SurveyPageId"]);
                this.DataSourceAsSurveyQuestionEntity.SurveyPageId = surveyPageId;
            }

            return base.Save();
        }

        #endregion
                
        #region Properties


        /// <summary>
        /// Return the page's datasource as a SurveyQuestionEntity
        /// </summary>
        public SurveyQuestionEntity DataSourceAsSurveyQuestionEntity
        {
            get
            {
                return this.DataSource as SurveyQuestionEntity;
            }
        }

        #endregion
	}
}
