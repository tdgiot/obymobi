﻿using System;
using Dionysos;
using Dionysos.Web.UI;
using Obymobi.Cms.Logic.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Company
{
    public partial class UISchedule : PageLLBLGenEntity
    {
        #region Fields

        private Company_SubPanels_ScheduledItemsPanel scheduledItemsPanel;

        #endregion

        #region Methods

        private void HookUpEvents()
        {
            this.btnCopyUISchedule.Click += this.btnCopyUISchedule_Click;
        }

        private void CopyUISchedule()
        {
            try
            {
                if (this.tbCopyName.Text.IsNullOrWhiteSpace() || !this.ddlCopyDeliverypointgroupId.Value.HasValue)
                {
                    this.MultiValidatorDefault.AddError(this.Translate("Error.NameAndDeliverypointgroupRequired", "Please enter a name and select a deliverypoint group."));
                }
                else
                {
                    UIScheduleEntity uiSchedule = UIScheduleHelper.CopyUISchedule(this.DataSourceAsUIScheduleEntity.UIScheduleId, this.tbCopyName.Text, this.ddlCopyDeliverypointgroupId.Value.Value);
                    this.AddInformatorInfo(this.Translate("UIScheduleCopied", "The UI schedule copy has been made: ") + "<a href=\"{0}\">{1}</a>".FormatSafe(
                        this.ResolveUrl("~/Company/UISchedule.aspx?id={0}".FormatSafe(uiSchedule.UIScheduleId)), uiSchedule.Name));
                }
            }
            catch (Exception ex)
            {
                this.MultiValidatorDefault.AddError(this.Translate("Error.ErrorDuringUIScheduleCopy", "Something went wrong trying to copy the UI schedule: ") + ex.Message);
            }
            this.Validate();
        }        

        private void SetGui()
        {
            if (!this.DataSourceAsUIScheduleEntity.IsNew)
            {
                this.scheduledItemsPanel = (Company_SubPanels_ScheduledItemsPanel)this.LoadControl("~/Company/SubPanels/ScheduledItemsPanel.ascx");
                this.scheduledItemsPanel.UIScheduleId = this.DataSourceAsUIScheduleEntity.UIScheduleId;
                this.scheduledItemsPanel.DeliverypointgroupId = this.DataSourceAsUIScheduleEntity.DeliverypointgroupId.GetValueOrDefault(0);
                this.plhSchedule.Controls.Add(this.scheduledItemsPanel);
            }
            
            PredicateExpression filter = new PredicateExpression(DeliverypointgroupFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

            SortExpression sort = new SortExpression(DeliverypointgroupFields.Name | SortOperator.Ascending);

            DeliverypointgroupCollection deliverypointgroups = new DeliverypointgroupCollection();

            deliverypointgroups.GetMulti(filter, 0, sort);

            this.ddlDeliverypointgroupId.DataSource = deliverypointgroups;
            this.ddlDeliverypointgroupId.DataBind();
            this.ddlDeliverypointgroupId.Enabled = this.DataSourceAsUIScheduleEntity.IsNew;

            this.ddlCopyDeliverypointgroupId.DataSource = deliverypointgroups;
            this.ddlCopyDeliverypointgroupId.DataBind();

            if (CmsSessionHelper.CurrentRole <= Role.Reseller)
            {
                MasterPages.MasterPageEntity masterPage = this.Master as MasterPages.MasterPageEntity;
				if (masterPage != null && masterPage.ToolBar.DeleteButton != null)
				{
					masterPage.ToolBar.DeleteButton.Visible = false;
				}
			}
		}

        public override bool Save()
        {
            if (this.PageMode == Dionysos.Web.PageMode.Add)
            {
                this.DataSourceAsUIScheduleEntity.CompanyId = CmsSessionHelper.CurrentCompanyId;
            }
            return base.Save();
        }

        #endregion

		#region Properties

		
		public UIScheduleEntity DataSourceAsUIScheduleEntity
		{
			get
			{
                return this.DataSource as UIScheduleEntity;
			}
		}

		#endregion

		#region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            this.DataSourceLoaded += this.RoomControlConfiguration_DataSourceLoaded;
            base.OnInit(e);
        }

        private void Page_Load(object sender, EventArgs e)
        {
            this.HookUpEvents();
        }

        private void RoomControlConfiguration_DataSourceLoaded(object sender)
		{
            this.SetGui();
		}

        private void btnCopyUISchedule_Click(object sender, EventArgs e)
        {
            this.CopyUISchedule();
        }

		#endregion
	}
}
