﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using DevExpress.Web;
using Dionysos;
using Dionysos.Web.UI;
using Obymobi.Constants;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.ScheduledCommands;
using Obymobi.Logic.ScheduledCommands.Client;
using Obymobi.Logic.ScheduledCommands.Terminal;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Company
{
    public partial class ScheduledCommandTask : UI.PageLLBLGenEntityCms
    {
        #region Fields

        private readonly Dictionary<DeviceModel, VersionNumber> versionNumbersOS = new Dictionary<DeviceModel, VersionNumber>();

        private Dictionary<DeviceModel, ReleaseEntity> companyReleases = new Dictionary<DeviceModel, ReleaseEntity>();

        #endregion

        #region Methods

        protected void cbAll_Init(object sender, EventArgs e)
        {
            var chk = sender as ASPxCheckBox;
            if (chk != null)
            {
                var gridViewHeaderTemplateContainer = chk.NamingContainer as GridViewHeaderTemplateContainer;
                if (gridViewHeaderTemplateContainer != null)
                {
                    ASPxGridView grid = gridViewHeaderTemplateContainer.Grid;
                    chk.Checked = (grid.Selection.Count == grid.VisibleRowCount);
                }
            }
        }

        private void LoadUserControls()
        {
            this.tabsMain.AddTabPage("Commands", "Commands", "~/Company/SubPanels/ScheduledCommandCollection.ascx");
            this.tabsMain.AddTabPage("Reschedule", "Reschedule", "~/Company/SubPanels/RescheduleCommandsPanel.ascx");

            this.lbCommands.DataBindEnum<StandardCommand>();
            this.lbCommands.DataBind();
        }

        private void LoadClients()
        {
            // Get the clients from the database
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ClientFields.CompanyId == this.DataSourceAsScheduledCommandTask.CompanyId);

            var relation = new RelationCollection(ClientEntity.Relations.DeviceEntityUsingDeviceId);

            SortExpression sort = new SortExpression();
            sort.Add(new SortClause(DeviceFields.LastRequestUTC, SortOperator.Descending));

            PrefetchPath prefetch = new PrefetchPath(EntityType.ClientEntity);
            prefetch.Add(ClientEntityBase.PrefetchPathDeviceEntity);
            prefetch.Add(ClientEntityBase.PrefetchPathLastDeliverypointEntity);

            ClientCollection clientCollection = new ClientCollection();
            clientCollection.GetMulti(filter, 0, sort, relation, prefetch);

            // Create the datatable with the client info
            DataTable dataTable = new DataTable();

            // Create the columns
            DataColumn clientIdColumn = new DataColumn("ClientId");
            DataColumn deliverypointNumber = new DataColumn("DeliverypointNumber");
            DataColumn deliverypointName = new DataColumn("DeliverypointName");
            DataColumn lastRequest = new DataColumn("LastRequest");
            DataColumn localIp = new DataColumn("LocalIp");
            DataColumn externalIp = new DataColumn("ExternalIp");
            DataColumn version = new DataColumn("Version");
            DataColumn versionSupportTools = new DataColumn("VersionST");
            DataColumn versionAgent = new DataColumn("VersionAgent");
            DataColumn versionOS = new DataColumn("VersionOS");
            DataColumn versionMessaging = new DataColumn("VersionMessaging");
            DataColumn downloaded = new DataColumn("Downloaded");
            DataColumn isSupportTools = new DataColumn("IsSupportTools");
            DataColumn isOnline = new DataColumn("IsOnline");

            // Translate the columns
            clientIdColumn.Caption = this.Translate("clmnClientId", "ClientId");
            deliverypointNumber.Caption = this.Translate("clmnDeliverypointNumber", "DP Num");
            deliverypointName.Caption = this.Translate("clmnDeliverypointName", "DP Name");
            lastRequest.Caption = this.Translate("clmnLastRequest", "Last request");
            localIp.Caption = this.Translate("clmnLocalIp", "Local IP");
            externalIp.Caption = this.Translate("clmnExternalIp", "External IP");
            version.Caption = this.Translate("clmnVersion", "Emenu");
            versionSupportTools.Caption = this.Translate("clmnVersionST", "SupportTools");
            versionAgent.Caption = this.Translate("clmnVersionAgent", "Agent");
            versionOS.Caption = this.Translate("clmnVersionOS", "OS");
            versionMessaging.Caption = Translate("clmnVersionMessaging", "Messaging");
            downloaded.Caption = this.Translate("clmnDownloaded", "Downloaded");
            isSupportTools.Caption = this.Translate("clmnConnectedAs", "Connected as");

            // Add the columns
            dataTable.Columns.Add(clientIdColumn);
            dataTable.Columns.Add(deliverypointNumber);
            dataTable.Columns.Add(deliverypointName);
            dataTable.Columns.Add(lastRequest);
            dataTable.Columns.Add(localIp);
            dataTable.Columns.Add(externalIp);
            dataTable.Columns.Add(version);
            dataTable.Columns.Add(versionSupportTools);
            dataTable.Columns.Add(versionAgent);
            dataTable.Columns.Add(versionOS);
            dataTable.Columns.Add(versionMessaging);
            dataTable.Columns.Add(downloaded);
            dataTable.Columns.Add(isOnline);

            this.companyReleases.Clear();

            foreach (ClientEntity clientEntity in clientCollection)
            {
                DataRow row = dataTable.NewRow();
                row["ClientId"] = clientEntity.ClientId;

                if (clientEntity.DeliverypointId.HasValue)
                {
                    row["DeliverypointNumber"] = clientEntity.DeliverypointEntity.Number;
                    row["DeliverypointName"] = clientEntity.DeliverypointEntity.Name;
                }

                row["LastRequest"] = (clientEntity.DeviceEntity.LastRequestUTC.HasValue ? clientEntity.DeviceEntity.LastRequestUTC.Value.UtcToLocalTime().ToString("dd/MM/yyyy HH:mm:ss") : "Unknown");
                row["LocalIp"] = clientEntity.DeviceEntity.PrivateIpAddresses;
                row["ExternalIp"] = clientEntity.DeviceEntity.PublicIpAddress;
                row["Version"] = clientEntity.DeviceEntity.ApplicationVersion;
                row["VersionST"] = clientEntity.DeviceEntity.SupportToolsVersion;
                row["VersionAgent"] = clientEntity.DeviceEntity.AgentVersion;
                row["VersionOS"] = clientEntity.DeviceEntity.OsVersion;
                row["VersionMessaging"] = clientEntity.DeviceEntity.MessagingServiceVersion;
                row["IsOnline"] = clientEntity.DeviceEntity.LastRequestUTC.HasValue && clientEntity.DeviceEntity.LastRequestUTC.Value >= DateTime.UtcNow.AddMilliseconds(ObymobiIntervals.DEVICE_OFFLINE_THRESHOLD * -1);

                VersionNumber companyVersionNumberOs = null;
                if (clientEntity.DeviceEntity != null)
                {
                    DeviceModel deviceModel = clientEntity.DeviceEntity.DeviceModel.GetValueOrDefault(DeviceModel.Unknown);
                    if (!versionNumbersOS.TryGetValue(deviceModel, out companyVersionNumberOs))
                    {
                        ReleaseEntity releaseEntity = null;
                        if (!this.companyReleases.TryGetValue(deviceModel, out releaseEntity))
                        {
                            releaseEntity = ReleaseHelper.GetCompanyRelease(deviceModel, clientEntity.CompanyId);
                            this.companyReleases.Add(deviceModel, releaseEntity);
                        }

                        if (releaseEntity != null)
                        {
                            companyVersionNumberOs = releaseEntity.VersionAsVersionNumber;
                            versionNumbersOS.Add(deviceModel, companyVersionNumberOs);
                        }
                    }
                }

                row["VersionOS"] += "|" + (companyVersionNumberOs == null ? "-1" : companyVersionNumberOs.NumberAsString);


                List<string> downloadedList = new List<string>();
                if (clientEntity.DeviceEntity.UpdateEmenuDownloaded)
                    downloadedList.Add("APP");
                if (clientEntity.DeviceEntity.UpdateAgentDownloaded)
                    downloadedList.Add("AG");
                if (clientEntity.DeviceEntity.UpdateSupportToolsDownloaded)
                    downloadedList.Add("ST");
                if (clientEntity.DeviceEntity.UpdateOSDownloaded)
                    downloadedList.Add("OS");

                if (downloadedList.Count == 0)
                    row["Downloaded"] = "None";
                else
                {
                    string downloadedString = string.Empty;
                    foreach (string app in downloadedList)
                    {
                        if (downloadedString != string.Empty)
                            downloadedString += ", ";
                        downloadedString += app;
                    }
                    row["Downloaded"] = downloadedString;
                }

                dataTable.Rows.Add(row);
            }

            this.clientGrid.DataSource = dataTable;
            this.clientGrid.DataBind();
        }

        private void LoadTerminals()
        {
            // Get the terminals from the database
            PredicateExpression filter = new PredicateExpression();
            filter.Add(TerminalFields.CompanyId == this.DataSourceAsScheduledCommandTask.CompanyId);

            RelationCollection relation = new RelationCollection();
            relation.Add(TerminalEntity.Relations.DeviceEntityUsingDeviceId);

            SortExpression sort = new SortExpression();
            sort.Add(new SortClause(DeviceFields.LastRequestUTC, SortOperator.Descending));

            PrefetchPath prefetch = new PrefetchPath(EntityType.TerminalEntity);
            prefetch.Add(TerminalEntityBase.PrefetchPathDeviceEntity);

            TerminalCollection terminalCollection = new TerminalCollection();
            terminalCollection.GetMulti(filter, 0, sort, relation, prefetch);

            // Create the datatable with the terminal info
            DataTable dataTable = new DataTable();

            // Create the columns
            DataColumn terminalId = new DataColumn("TerminalId");
            DataColumn name = new DataColumn("Name");
            DataColumn lastRequest = new DataColumn("LastRequest");
            DataColumn localIp = new DataColumn("LocalIp");
            DataColumn externalIp = new DataColumn("ExternalIp");
            DataColumn version = new DataColumn("VersionTerminal");
            DataColumn versionSupportTools = new DataColumn("VersionST");
            DataColumn versionAgent = new DataColumn("VersionAgent");
            DataColumn versionOS = new DataColumn("VersionOS");
            DataColumn versionMessaging = new DataColumn("VersionMessaging");
            DataColumn downloaded = new DataColumn("Downloaded");

            // Translate the columns
            terminalId.Caption = Translate("clmnTerminalId", "TerminalId");
            name.Caption = this.Translate("clmnName", "Name");
            lastRequest.Caption = this.Translate("clmnLastRequest", "Laatst online");
            localIp.Caption = this.Translate("clmnLocalIp", "Lokaal ip");
            externalIp.Caption = this.Translate("clmnExternalIp", "Extern ip");
            version.Caption = this.Translate("clmnVersion", "Console");
            versionSupportTools.Caption = this.Translate("clmnVersionST", "SupportTools");
            versionAgent.Caption = this.Translate("clmnVersionAgent", "Agent");
            versionOS.Caption = this.Translate("clmnVersionOS", "OS");
            versionMessaging.Caption = Translate("clmnVersionMessaging", "Messaging");
            downloaded.Caption = this.Translate("clmnDownloaded", "Downloaded");

            // Add the columns
            dataTable.Columns.Add(terminalId);
            dataTable.Columns.Add(name);
            dataTable.Columns.Add(lastRequest);
            dataTable.Columns.Add(localIp);
            dataTable.Columns.Add(externalIp);
            dataTable.Columns.Add(version);
            dataTable.Columns.Add(versionSupportTools);
            dataTable.Columns.Add(versionAgent);
            dataTable.Columns.Add(versionOS);
            dataTable.Columns.Add(versionMessaging);
            dataTable.Columns.Add(downloaded);

            this.companyReleases.Clear();

            foreach (TerminalEntity terminalEntity in terminalCollection)
            {
                DataRow row = dataTable.NewRow();
                row["TerminalId"] = terminalEntity.TerminalId;
                row["Name"] = terminalEntity.Name;
                row["LastRequest"] = (terminalEntity.DeviceEntity.LastRequestUTC.HasValue ? terminalEntity.DeviceEntity.LastRequestUTC.Value.UtcToLocalTime().ToString("dd/MM/yyyy HH:mm:ss") : "Unknown");
                row["LocalIp"] = terminalEntity.DeviceEntity.PrivateIpAddresses;
                row["ExternalIp"] = terminalEntity.DeviceEntity.PublicIpAddress;
                row["VersionTerminal"] = terminalEntity.DeviceEntity.ApplicationVersion;
                row["VersionST"] = terminalEntity.DeviceEntity.SupportToolsVersion;
                row["VersionAgent"] = terminalEntity.DeviceEntity.AgentVersion;
                row["VersionOS"] = terminalEntity.DeviceEntity.OsVersion;
                row["VersionMessaging"] = terminalEntity.DeviceEntity.MessagingServiceVersion;

                VersionNumber companyVersionNumberOs = null;
                if (terminalEntity.DeviceEntity != null)
                {
                    DeviceModel deviceModel = terminalEntity.DeviceEntity.DeviceModel.GetValueOrDefault(DeviceModel.Unknown);
                    if (!versionNumbersOS.TryGetValue(deviceModel, out companyVersionNumberOs))
                    {
                        ReleaseEntity releaseEntity = null;
                        if (!this.companyReleases.TryGetValue(deviceModel, out releaseEntity))
                        {
                            releaseEntity = ReleaseHelper.GetCompanyRelease(deviceModel, terminalEntity.CompanyId);
                            this.companyReleases.Add(deviceModel, releaseEntity);
                        }

                        if (releaseEntity != null)
                        {
                            companyVersionNumberOs = releaseEntity.VersionAsVersionNumber;
                            versionNumbersOS.Add(deviceModel, companyVersionNumberOs);
                        }
                    }
                }
                row["VersionOS"] += "|" + (companyVersionNumberOs == null ? "-1" : companyVersionNumberOs.NumberAsString);

                var downloadedList = new List<string>();
                if (terminalEntity.DeviceEntity.UpdateConsoleDownloaded)
                    downloadedList.Add("APP");
                if (terminalEntity.DeviceEntity.UpdateAgentDownloaded)
                    downloadedList.Add("AG");
                if (terminalEntity.DeviceEntity.UpdateSupportToolsDownloaded)
                    downloadedList.Add("ST");
                if (terminalEntity.DeviceEntity.UpdateOSDownloaded)
                    downloadedList.Add("OS");

                if (downloadedList.Count == 0)
                    row["Downloaded"] = "None";
                else
                {
                    string downloadedString = string.Empty;
                    foreach (string app in downloadedList)
                    {
                        if (downloadedString != string.Empty)
                            downloadedString += ", ";
                        downloadedString += app;
                    }
                    row["Downloaded"] = downloadedString;
                }

                dataTable.Rows.Add(row);
            }

            this.terminalGrid.DataSource = dataTable;
            this.terminalGrid.DataBind();
        }

        private void SetGui()
        {
            this.lblActive.Visible = !this.DataSourceAsScheduledCommandTask.IsNew;
            this.cbActive.Visible = !this.DataSourceAsScheduledCommandTask.IsNew;

            CompanyEntity companyEntity = null;
            if (this.DataSourceAsScheduledCommandTask.IsNew)
                companyEntity = new CompanyEntity(CmsSessionHelper.CurrentCompanyId);
            else
                companyEntity = this.DataSourceAsScheduledCommandTask.CompanyEntity;

            this.lblCompanyIdValue.Text = companyEntity.Name;

            if (!companyEntity.TimeZoneOlsonId.IsNullOrWhiteSpace())
            {
                this.lblTimezoneValue.Text = Obymobi.TimeZone.Mappings[companyEntity.TimeZoneOlsonId].ToString();
            }
            else
            {
                this.lblTimezoneValue.Text = TimeZoneInfo.Utc.DisplayName;
            }

            this.lblStatus.Visible = !this.DataSourceAsScheduledCommandTask.IsNew;
            this.lblStatusValue.Visible = !this.DataSourceAsScheduledCommandTask.IsNew;
            this.lblStatusValue.Text = this.DataSourceAsScheduledCommandTask.StatusText;

            this.tbStart.IsRequired = this.DataSourceAsScheduledCommandTask.IsNew;
            this.tbStart.Visible = this.DataSourceAsScheduledCommandTask.IsNew;
            this.lblStartValue.Visible = !this.DataSourceAsScheduledCommandTask.IsNew;
            this.lblStartValue.Text = this.DataSourceAsScheduledCommandTask.StartText;
            this.btnStartValue.Visible = !this.DataSourceAsScheduledCommandTask.IsNew;

            this.tbExpires.IsRequired = this.DataSourceAsScheduledCommandTask.IsNew;
            this.tbExpires.Visible = this.DataSourceAsScheduledCommandTask.IsNew;
            this.lblExpiresValue.Visible = !this.DataSourceAsScheduledCommandTask.IsNew;
            this.lblExpiresValue.Text = this.DataSourceAsScheduledCommandTask.ExpiresText;

            this.lblCompleted.Visible = !this.DataSourceAsScheduledCommandTask.IsNew && this.DataSourceAsScheduledCommandTask.CompletedUTC.HasValue;
            this.lblCompletedValue.Visible = !this.DataSourceAsScheduledCommandTask.IsNew && this.DataSourceAsScheduledCommandTask.CompletedUTC.HasValue;
            this.lblCompletedValue.Text = this.DataSourceAsScheduledCommandTask.CompletedText;

            this.lblProgress.Visible = !this.DataSourceAsScheduledCommandTask.IsNew;
            this.lblProgressValue.Visible = !this.DataSourceAsScheduledCommandTask.IsNew;
            this.lblProgressValue.Text = this.DataSourceAsScheduledCommandTask.ProgressText;

            this.pnlAddCommands.Visible = !this.DataSourceAsScheduledCommandTask.IsNew;
            this.pnlStandardTasks.Visible = this.DataSourceAsScheduledCommandTask.IsNew;

            TabPage rescheduleTab = this.tabsMain.TabPages.FindByName("Reschedule");
            if (this.DataSourceAsScheduledCommandTask.Status != ScheduledCommandTaskStatus.CompletedWithErrors &&
                this.DataSourceAsScheduledCommandTask.Status != ScheduledCommandTaskStatus.Expired)
            {
                rescheduleTab.ClientVisible = false;
            }
            else if (!ScheduledCommandTaskHelper.GetHasUnsuccessfullCommands(this.DataSourceAsScheduledCommandTask.ScheduledCommandTaskId))
            {
                rescheduleTab.ClientVisible = false;
            }
            else
            {
                rescheduleTab.ClientVisible = true;
            }

            if (!this.DataSourceAsScheduledCommandTask.IsNew)
            {
                // Show validation message if the scheduled command task is not active yet
                if (!this.DataSourceAsScheduledCommandTask.Active && this.DataSourceAsScheduledCommandTask.ExpiresUTC > DateTime.UtcNow)
                {
                    this.AddInformator(InformatorType.Information, this.Translate("scheduledcommandtask-not-active", "This scheduled command task is not active yet."));
                }

                // Show validation message if the scheduled command task has no commands yet
                if (this.DataSourceAsScheduledCommandTask.ScheduledCommandCollection.Count == 0)
                {
                    this.AddInformator(InformatorType.Information, this.Translate("scheduledcommandtask-has-no-commands", "This scheduled command task does not have any commands yet."));
                }

                // Show validation message if the scheduled command task has expired.
                if (this.DataSourceAsScheduledCommandTask.ExpiresUTC < DateTime.UtcNow)
                {
                    this.AddInformator(InformatorType.Information, "This scheduled command task has expired.");

                    // Do not allow reactivation after the SCT has expired.
                    this.cbActive.Value = false;
                    this.cbActive.Enabled = false;
                }

                this.Validate();
            }
        }

        private void HookupEvents()
        {
            if (!this.DataSourceAsScheduledCommandTask.IsNew)
            {
                this.btnStartValue.Click += btnStartValue_Click;

                this.btnAddClients.Click += btnAddClients_Click;
                this.btnAddAllClients.Click += btnAddAllClients_Click;
                this.btnAddWebserviceClients.Click += btnAddWebserviceClients_Click;
                this.btnAddOutdatedClients.Click += BtnAddOutdatedClients_Click;
                this.btnAddOutdatedSupportTools.Click += BtnAddOutdatedSupportTools_Click;
                this.btnAddOutdatedAgents.Click += BtnAddOutdatedAgents_Click;

                this.btnAddTerminals.Click += btnAddTerminals_Click;

                this.clientGrid.HtmlDataCellPrepared += grid_HtmlDataCellPrepared;
                this.terminalGrid.HtmlDataCellPrepared += grid_HtmlDataCellPrepared;
            }

            this.btnCreateScheduledCommandTask.Click += btnCreateScheduledCommands_Click;
        }

        public override bool Save()
        {
            if (this.DataSourceAsScheduledCommandTask.IsNew)
            {
                this.DataSourceAsScheduledCommandTask.CompanyId = CmsSessionHelper.CurrentCompanyId;

                var companyEntity = new CompanyEntity(CmsSessionHelper.CurrentCompanyId);

                // Start
                if (this.tbStart.Value <= DateTime.UtcNow.UtcToLocalTime(companyEntity.TimeZoneInfo))
                {
                    this.MultiValidatorDefault.AddError("", "Starts on", "value has to be in the future.");
                }

                // Expires
                if (this.tbExpires.Value <= this.tbStart.Value)
                {
                    this.MultiValidatorDefault.AddError("", "Expires on", "value has to be further in time than value of field 'Starts on'.");
                }

                this.Validate();

                if (this.IsValid)
                {
                    this.DataSourceAsScheduledCommandTask.StartUTC = this.tbStart.Value.LocalTimeToUtc(companyEntity.TimeZoneInfo).Value;
                    this.DataSourceAsScheduledCommandTask.ExpiresUTC = this.tbExpires.Value.LocalTimeToUtc(companyEntity.TimeZoneInfo).Value;
                }
            }

            return base.Save();
        }

        #endregion

        #region Events handlers

        private void ScheduledCommandTask_DataSourceLoaded(object sender)
        {
            if (!this.DataSourceAsScheduledCommandTask.IsNew)
            {
                if (!this.IsPostBack)
                {
                    this.cbClientCommand.Items.Add(ClientCommand.RestartApplication.ToString(), (int)ClientCommand.RestartApplication);
                    this.cbClientCommand.Items.Add(ClientCommand.RestartDevice.ToString(), (int)ClientCommand.RestartDevice);
                    this.cbClientCommand.Items.Add(ClientCommand.SendLogToWebservice.ToString(), (int)ClientCommand.SendLogToWebservice);
                    this.cbClientCommand.Items.Add(ClientCommand.DailyOrderReset.ToString(), (int)ClientCommand.DailyOrderReset);
                    this.cbClientCommand.Items.Add(ClientCommand.DownloadEmenuUpdate.ToString(), (int)ClientCommand.DownloadEmenuUpdate);
                    this.cbClientCommand.Items.Add(ClientCommand.DownloadAgentUpdate.ToString(), (int)ClientCommand.DownloadAgentUpdate);
                    this.cbClientCommand.Items.Add(ClientCommand.DownloadSupportToolsUpdate.ToString(), (int)ClientCommand.DownloadSupportToolsUpdate);
                    this.cbClientCommand.Items.Add(ClientCommand.InstallEmenuUpdate.ToString(), (int)ClientCommand.InstallEmenuUpdate);
                    this.cbClientCommand.Items.Add(ClientCommand.InstallAgentUpdate.ToString(), (int)ClientCommand.InstallAgentUpdate);
                    this.cbClientCommand.Items.Add(ClientCommand.InstallSupportToolsUpdate.ToString(), (int)ClientCommand.InstallSupportToolsUpdate);
                    this.cbClientCommand.Items.Add(ClientCommand.ClearBrowserCache.ToString(), (int)ClientCommand.ClearBrowserCache);
                    this.cbClientCommand.Items.Add(ClientCommand.DownloadOSUpdate.ToString(), (int)ClientCommand.DownloadOSUpdate);
                    this.cbClientCommand.Items.Add(ClientCommand.InstallOSUpdate.ToString(), (int)ClientCommand.InstallOSUpdate);
                    this.cbClientCommand.Items.Add(ClientCommand.DownloadInstallMessagingServiceUpdate.ToString(), (int)ClientCommand.DownloadInstallMessagingServiceUpdate);

                    this.cbTerminalCommand.Items.Add(TerminalCommand.RestartApplication.ToString(), (int)TerminalCommand.RestartApplication);
                    this.cbTerminalCommand.Items.Add(TerminalCommand.RestartDevice.ToString(), (int)TerminalCommand.RestartDevice);
                    this.cbTerminalCommand.Items.Add(TerminalCommand.SendLogToWebservice.ToString(), (int)TerminalCommand.SendLogToWebservice);
                    this.cbTerminalCommand.Items.Add(TerminalCommand.DownloadConsoleUpdate.ToString(), (int)TerminalCommand.DownloadConsoleUpdate);
                    this.cbTerminalCommand.Items.Add(TerminalCommand.DownloadAgentUpdate.ToString(), (int)TerminalCommand.DownloadAgentUpdate);
                    this.cbTerminalCommand.Items.Add(TerminalCommand.DownloadSupportToolsUpdate.ToString(), (int)TerminalCommand.DownloadSupportToolsUpdate);
                    this.cbTerminalCommand.Items.Add(TerminalCommand.InstallConsoleUpdate.ToString(), (int)TerminalCommand.InstallConsoleUpdate);
                    this.cbTerminalCommand.Items.Add(TerminalCommand.InstallAgentUpdate.ToString(), (int)TerminalCommand.InstallAgentUpdate);
                    this.cbTerminalCommand.Items.Add(TerminalCommand.InstallSupportToolsUpdate.ToString(), (int)TerminalCommand.InstallSupportToolsUpdate);
                    this.cbTerminalCommand.Items.Add(TerminalCommand.DownloadOSUpdate.ToString(), (int)TerminalCommand.DownloadOSUpdate);
                    this.cbTerminalCommand.Items.Add(TerminalCommand.InstallOSUpdate.ToString(), (int)TerminalCommand.InstallOSUpdate);
                    this.cbTerminalCommand.Items.Add(TerminalCommand.DownloadInstallMessagingServiceUpdate.ToString(), (int)TerminalCommand.DownloadInstallMessagingServiceUpdate);
                }

                this.LoadClients();
                this.LoadTerminals();
            }
        }

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            this.DataSourceLoaded += ScheduledCommandTask_DataSourceLoaded;
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.SetGui();
            this.HookupEvents();
        }

        private void grid_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            string str = e.GetValue("LastRequest").ToString();
            DateTime lastRequest;
            if (DateTime.TryParse(str, null, DateTimeStyles.AssumeLocal, out lastRequest))
            {
                var deviceThreshold = DateTime.UtcNow.AddMilliseconds(ObymobiIntervals.DEVICE_OFFLINE_THRESHOLD * -1);
                if (lastRequest < deviceThreshold)
                {
                    e.Cell.ForeColor = Color.Red;
                }
            }

            if (e.DataColumn.FieldName.Equals("Version", StringComparison.InvariantCultureIgnoreCase)) // Emenu version
            {
                VersionNumber.VersionState state = VersionNumber.CompareVersions(e.CellValue.ToString(), this.DataSourceAsScheduledCommandTask.CompanyEntity.ClientApplicationVersion);
                switch (state)
                {
                    case VersionNumber.VersionState.Older:
                        e.Cell.ForeColor = Color.Orange;
                        break;
                    case VersionNumber.VersionState.Newer:
                        e.Cell.ForeColor = Color.Blue;
                        break;
                }
            }
            if (e.DataColumn.FieldName.Equals("VersionTerminal", StringComparison.InvariantCultureIgnoreCase)) // Emenu version
            {
                VersionNumber.VersionState state = VersionNumber.CompareVersions(e.CellValue.ToString(), this.DataSourceAsScheduledCommandTask.CompanyEntity.TerminalApplicationVersion);
                switch (state)
                {
                    case VersionNumber.VersionState.Older:
                        e.Cell.ForeColor = Color.Orange;
                        break;
                    case VersionNumber.VersionState.Newer:
                        e.Cell.ForeColor = Color.Blue;
                        break;
                }
            }
            else if (e.DataColumn.FieldName.Equals("VersionST", StringComparison.InvariantCultureIgnoreCase)) // SupportTools version
            {
                VersionNumber.VersionState state = VersionNumber.CompareVersions(e.CellValue.ToString(), this.DataSourceAsScheduledCommandTask.CompanyEntity.SupportToolsVersion);
                switch (state)
                {
                    case VersionNumber.VersionState.Older:
                        e.Cell.ForeColor = Color.Orange;
                        break;
                    case VersionNumber.VersionState.Newer:
                        e.Cell.ForeColor = Color.Blue;
                        break;
                }
            }
            else if (e.DataColumn.FieldName.Equals("VersionAgent", StringComparison.InvariantCultureIgnoreCase)) // Agent version
            {
                VersionNumber.VersionState state = VersionNumber.CompareVersions(e.CellValue.ToString(), this.DataSourceAsScheduledCommandTask.CompanyEntity.AgentVersion);
                switch (state)
                {
                    case VersionNumber.VersionState.Older:
                        e.Cell.ForeColor = Color.Orange;
                        break;
                    case VersionNumber.VersionState.Newer:
                        e.Cell.ForeColor = Color.Blue;
                        break;
                }
            }
            else if (e.DataColumn.FieldName.Equals("VersionOS", StringComparison.InvariantCultureIgnoreCase)) // OS version
            {
                string cellValue = e.CellValue.ToString();
                string[] versionNumbers = cellValue.Split('|');
                e.Cell.Text = versionNumbers[0];

                if (versionNumbers[1].Equals("-1"))
                {
                    e.Cell.ForeColor = Color.Gray;
                }
                else
                {
                    VersionNumber.VersionState state = VersionNumber.CompareVersions(versionNumbers[0], versionNumbers[1]);
                    switch (state)
                    {
                        case VersionNumber.VersionState.Older:
                            e.Cell.ForeColor = Color.Orange;
                            break;
                        case VersionNumber.VersionState.Newer:
                            e.Cell.ForeColor = Color.Blue;
                            break;
                    }
                }
            }

            if (e.DataColumn.FieldName.Equals("VersionMessaging", StringComparison.InvariantCultureIgnoreCase))
            {
                string companyMessagingVersion = DataSourceAsScheduledCommandTask.CompanyEntity.CompanyReleaseCollection
                    .FirstOrDefault(x => x.ReleaseEntity.ApplicationEntity.Code == ApplicationCode.MessagingService)?.ReleaseEntity?.Version;

                VersionNumber.VersionState state = VersionNumber.CompareVersions(e.CellValue.ToString(), companyMessagingVersion);
                switch (state)
                {
                    case VersionNumber.VersionState.Older:
                        e.Cell.ForeColor = Color.Orange;
                        break;
                    case VersionNumber.VersionState.Newer:
                        e.Cell.ForeColor = Color.Blue;
                        break;
                }
            }
        }

        private void btnAddClients_Click(object sender, EventArgs e)
        {
            this.AddClients(false);
        }

        private void btnAddAllClients_Click(object sender, EventArgs e)
        {
            this.AddClients(true);
        }

        private void btnAddWebserviceClients_Click(object sender, EventArgs e)
        {
            this.AddWebserviceClients();
        }

        private void BtnAddOutdatedSupportTools_Click(object sender, EventArgs e)
        {
            this.AddOutdatedSupportToolsClients();
        }

        private void BtnAddOutdatedAgents_Click(object sender, EventArgs e)
        {
            this.AddOutdatedAgentsClients();
        }

        private void BtnAddOutdatedClients_Click(object sender, EventArgs e)
        {
            this.AddOutdatedClients();
        }

        private void AddClients(bool allClients)
        {
            if (this.cbClientCommand.ValidId < 0)
            {
                // Validation message: no client command selected
                this.AddInformator(InformatorType.Warning, this.Translate("no-clientcommand-selected", "You haven't selected a command to add for the clients."));
                return;
            }

            List<object> selectedClientIds = null;
            if (!allClients)
            {
                selectedClientIds = this.clientGrid.GetSelectedFieldValues("ClientId");
                if (selectedClientIds.Count == 0)
                {
                    // Validation message: no clients selected
                    this.AddInformator(InformatorType.Warning, this.Translate("no-clients-selected", "You haven't selected any of the clients to add the command for."));
                    return;
                }
            }
            else
            {
                if (this.clientGrid.VisibleRowCount == 0)
                {
                    // Validation message: no clients available
                    this.AddInformator(InformatorType.Warning, this.Translate("no-clients-available", "There are no clients to add the command for."));
                    return;
                }
            }

            ClientCommand clientCommand = (ClientCommand)this.cbClientCommand.Value.GetValueOrDefault(0);

            ClientCollection clientCollection = new ClientCollection();
            if (allClients)
            {
                clientCollection.GetMulti(ClientFields.CompanyId == this.DataSourceAsScheduledCommandTask.CompanyId);
            }
            else
            {
                clientCollection.GetMulti(ClientFields.ClientId == selectedClientIds);
            }

            this.CreateCommandsForClients(clientCollection, clientCommand, true);
        }

        private void AddWebserviceClients()
        {
            if (this.cbClientCommand.ValidId < 0)
            {
                // Validation message: no client command selected
                this.AddInformator(InformatorType.Warning, this.Translate("no-clientcommand-selected", "You haven't selected a command to add for the clients."));
                return;
            }

            ClientCommand clientCommand = (ClientCommand)this.cbClientCommand.Value.GetValueOrDefault(0);

            PredicateExpression filter = new PredicateExpression();
            filter.Add(ClientFields.CompanyId == this.DataSourceAsScheduledCommandTask.CompanyId);
            filter.Add(DeviceFields.CommunicationMethod == ClientCommunicationMethod.Webservice);

            RelationCollection relations = new RelationCollection();
            relations.Add(ClientEntity.Relations.DeviceEntityUsingDeviceId);

            ClientCollection clientCollection = new ClientCollection();
            clientCollection.GetMulti(filter, relations);

            this.CreateCommandsForClients(clientCollection, clientCommand, true);
        }

        private void AddOutdatedSupportToolsClients()
        {
            if (this.cbClientCommand.ValidId < 0)
            {
                // Validation message: no client command selected
                this.AddInformator(InformatorType.Warning, this.Translate("no-clientcommand-selected", "You haven't selected a command to add for the clients."));
                return;
            }

            ClientCommand clientCommand = (ClientCommand)this.cbClientCommand.Value.GetValueOrDefault(0);

            CompanyEntity companyEntity = new CompanyEntity(this.DataSourceAsScheduledCommandTask.CompanyId);

            PredicateExpression filter = new PredicateExpression();
            filter.Add(ClientFields.CompanyId == this.DataSourceAsScheduledCommandTask.CompanyId);

            PredicateExpression subFilter = new PredicateExpression();
            subFilter.Add(DeviceFields.SupportToolsVersion < companyEntity.SupportToolsVersion);
            subFilter.AddWithOr(DeviceFields.SupportToolsVersion == DBNull.Value);
            subFilter.AddWithOr(DeviceFields.SupportToolsVersion == string.Empty);
            filter.Add(subFilter);

            RelationCollection relations = new RelationCollection();
            relations.Add(ClientEntity.Relations.DeviceEntityUsingDeviceId);

            ClientCollection clientCollection = new ClientCollection();
            clientCollection.GetMulti(filter, relations);

            this.CreateCommandsForClients(clientCollection, clientCommand, true);
        }

        private void AddOutdatedAgentsClients()
        {
            if (this.cbClientCommand.ValidId < 0)
            {
                // Validation message: no client command selected
                this.AddInformator(InformatorType.Warning, this.Translate("no-clientcommand-selected", "You haven't selected a command to add for the clients."));
                return;
            }

            ClientCommand clientCommand = (ClientCommand)this.cbClientCommand.Value.GetValueOrDefault(0);

            CompanyEntity companyEntity = new CompanyEntity(this.DataSourceAsScheduledCommandTask.CompanyId);

            PredicateExpression filter = new PredicateExpression();
            filter.Add(ClientFields.CompanyId == this.DataSourceAsScheduledCommandTask.CompanyId);

            PredicateExpression subFilter = new PredicateExpression();
            subFilter.Add(DeviceFields.AgentVersion < companyEntity.AgentVersion);
            subFilter.AddWithOr(DeviceFields.AgentVersion == DBNull.Value);
            subFilter.AddWithOr(DeviceFields.AgentVersion == string.Empty);
            filter.Add(subFilter);

            RelationCollection relations = new RelationCollection();
            relations.Add(ClientEntity.Relations.DeviceEntityUsingDeviceId);

            ClientCollection clientCollection = new ClientCollection();
            clientCollection.GetMulti(filter, relations);

            this.CreateCommandsForClients(clientCollection, clientCommand, true);
        }

        private void AddOutdatedClients()
        {
            if (this.cbClientCommand.ValidId < 0)
            {
                // Validation message: no client command selected
                this.AddInformator(InformatorType.Warning, this.Translate("no-clientcommand-selected", "You haven't selected a command to add for the clients."));
                return;
            }

            ClientCommand clientCommand = (ClientCommand)this.cbClientCommand.Value.GetValueOrDefault(0);

            CompanyEntity companyEntity = new CompanyEntity(this.DataSourceAsScheduledCommandTask.CompanyId);

            PredicateExpression filter = new PredicateExpression();
            filter.Add(ClientFields.CompanyId == this.DataSourceAsScheduledCommandTask.CompanyId);

            PredicateExpression subFilter = new PredicateExpression();
            subFilter.Add(DeviceFields.ApplicationVersion < companyEntity.ClientApplicationVersion);
            subFilter.AddWithOr(DeviceFields.ApplicationVersion == DBNull.Value);
            subFilter.AddWithOr(DeviceFields.ApplicationVersion == string.Empty);
            filter.Add(subFilter);

            RelationCollection relations = new RelationCollection();
            relations.Add(ClientEntity.Relations.DeviceEntityUsingDeviceId);

            ClientCollection clientCollection = new ClientCollection();
            clientCollection.GetMulti(filter, relations);

            this.CreateCommandsForClients(clientCollection, clientCommand, true);
        }

        private void CreateCommandsForClients(ClientCollection clients, ClientCommand command, bool showWarnings)
        {
            ClientCommandSchedulerBase scheduler = CommandSchedulers.GetClientCommandScheduler(command);

            DateTime utcNowMinusOfflineThreshold = DateTime.UtcNow.AddMilliseconds(ObymobiIntervals.DEVICE_OFFLINE_THRESHOLD * -1);
            IEnumerable<ClientEntity> orderedClientCollection = clients.OrderBy(c => !(c.DeviceEntity.LastRequestUTC.HasValue && c.DeviceEntity.LastRequestUTC >= utcNowMinusOfflineThreshold));

            ScheduledCommandCollection scheduledCommandCollection = new ScheduledCommandCollection();
            scheduledCommandCollection.GetMulti(ScheduledCommandFields.ScheduledCommandTaskId == this.DataSourceAsScheduledCommandTask.ScheduledCommandTaskId);
            EntityView<ScheduledCommandEntity> scheduledCommandView = scheduledCommandCollection.DefaultView;

            int added = 0;
            int notAdded = 0;
            int notAddedVersion = 0;

            ScheduledCommandCollection commandsToAdd = new ScheduledCommandCollection();
            foreach (ClientEntity clientEntity in orderedClientCollection)
            {
                // Create the filter for the scheduled command view
                PredicateExpression filter = new PredicateExpression();
                filter.Add(ScheduledCommandFields.ClientCommand == command);
                filter.Add(ScheduledCommandFields.ClientId == clientEntity.ClientId);
                scheduledCommandView.Filter = filter;

                if (scheduledCommandView.Count > 0) // Command already existed for the terminal and command
                {
                    notAdded++;
                    continue;
                }

                if (!scheduler.CanCommandBeAdded(clientEntity)) // Command cannot be added
                {
                    notAddedVersion++;
                    continue;
                }

                ScheduledCommandEntity scheduledCommandEntity = new ScheduledCommandEntity();
                scheduledCommandEntity.ParentCompanyId = clientEntity.CompanyId;
                scheduledCommandEntity.Validator = null;
                scheduledCommandEntity.ScheduledCommandTaskId = this.DataSourceAsScheduledCommandTask.ScheduledCommandTaskId;
                scheduledCommandEntity.ClientCommand = command;
                scheduledCommandEntity.ClientId = clientEntity.ClientId;

                if (this.DataSourceAsScheduledCommandTask.Active)
                {
                    scheduledCommandEntity.Status = ScheduledCommandStatus.Pending;
                }

                scheduledCommandView.RelatedCollection.Add(scheduledCommandEntity);
                commandsToAdd.Add(scheduledCommandEntity);
                added++;
            }
            commandsToAdd.SaveMulti();

            if (showWarnings)
            {
                if (added > 0) this.AddInformator(InformatorType.Information, this.Translate("x-clientcommands-added", "{0} client commands have been added."), added);
                if (notAdded > 0) this.AddInformator(InformatorType.Warning, this.Translate("x-clientcommands-not-added", "{0} client commands could not be added because they were already added."), notAdded);
                if (notAddedVersion > 0) this.AddInformator(InformatorType.Warning, this.Translate("x-clientcommands-not-added-version", "{0} client commands could not be added because the selected clients are already up-to-date."), notAddedVersion);
            }
        }

        private void btnAddTerminals_Click(object sender, EventArgs e) // TODO Refactor
        {
            List<object> selectedTerminalIds = this.terminalGrid.GetSelectedFieldValues("TerminalId");
            if (selectedTerminalIds.Count == 0)
            {
                // Validation message: no terminals selected
                this.AddInformator(InformatorType.Warning, this.Translate("no-terminals-selected", "You haven't selected any of the terminals to add the command for."));
            }
            else if (this.cbTerminalCommand.ValidId < 0)
            {
                // Validation message: no terminal command selected
                this.AddInformator(InformatorType.Warning, this.Translate("no-terminalcommand-selected", "You haven't selected a command to add for the selected terminals."));
            }
            else
            {
                TerminalCommand terminalCommand = (TerminalCommand)this.cbTerminalCommand.Value.GetValueOrDefault(0);

                TerminalCollection terminalCollection = new TerminalCollection();
                terminalCollection.GetMulti(TerminalFields.TerminalId == selectedTerminalIds);

                this.CreateCommandsForTerminals(terminalCollection, terminalCommand, true);
            }
        }

        private void CreateCommandsForTerminals(TerminalCollection terminals, TerminalCommand command, bool showWarnings)
        {
            TerminalCommandSchedulerBase scheduler = CommandSchedulers.GetTerminalCommandScheduler(command);

            ScheduledCommandCollection scheduledCommandCollection = new ScheduledCommandCollection();
            scheduledCommandCollection.GetMulti(ScheduledCommandFields.ScheduledCommandTaskId == this.DataSourceAsScheduledCommandTask.ScheduledCommandTaskId);
            EntityView<ScheduledCommandEntity> scheduledCommandView = scheduledCommandCollection.DefaultView;

            int added = 0;
            int notAdded = 0;
            int notAddedVersion = 0;

            ScheduledCommandCollection commandsToAdd = new ScheduledCommandCollection();
            foreach (TerminalEntity terminal in terminals)
            {
                // Create the filter for the scheduled command view
                PredicateExpression filter = new PredicateExpression();
                filter.Add(ScheduledCommandFields.TerminalCommand == command);
                filter.Add(ScheduledCommandFields.TerminalId == terminal.TerminalId);
                scheduledCommandView.Filter = filter;

                if (scheduledCommandView.Count > 0) // Command already existed for the terminal and command
                {
                    notAdded++;
                    continue;
                }

                if (!scheduler.CanCommandBeAdded(terminal)) // Command cannot be added
                {
                    notAddedVersion++;
                    continue;
                }

                ScheduledCommandEntity scheduledCommandEntity = new ScheduledCommandEntity();
                scheduledCommandEntity.ScheduledCommandTaskId = this.DataSourceAsScheduledCommandTask.ScheduledCommandTaskId;
                scheduledCommandEntity.TerminalCommand = command;
                scheduledCommandEntity.TerminalId = terminal.TerminalId;

                scheduledCommandView.RelatedCollection.Add(scheduledCommandEntity);
                commandsToAdd.Add(scheduledCommandEntity);
                added++;
            }
            commandsToAdd.SaveMulti();

            if (showWarnings)
            {
                if (added > 0) this.AddInformator(InformatorType.Information, this.Translate("x-terminalcommands-added", "{0} terminal commands have been added."), added);
                if (notAdded > 0) this.AddInformator(InformatorType.Warning, this.Translate("x-terminalcommands-not-added", "{0} terminal commands could not be added because they were already added."), notAdded);
                if (notAddedVersion > 0) this.AddInformator(InformatorType.Warning, this.Translate("x-terminalcommands-not-added-version", "{0} terminal commands could not be added because the selected terminals are already up-to-date."), notAddedVersion);
            }
        }

        private void btnCreateScheduledCommands_Click(object sender, EventArgs e)
        {
            if (this.Save())
            {
                List<ClientCommand> clientCommands = this.GetSelectedClientCommands();
                if (clientCommands.Count > 0)
                {
                    ClientCollection clientCollection = new ClientCollection();
                    clientCollection.GetMulti(ClientFields.CompanyId == this.DataSourceAsScheduledCommandTask.CompanyId);

                    foreach (ClientCommand clientCommand in clientCommands)
                    {
                        this.CreateCommandsForClients(clientCollection, clientCommand, false);
                    }
                }

                List<TerminalCommand> terminalCommands = this.GetSelectedTerminalCommands();
                if (terminalCommands.Count > 0)
                {
                    TerminalCollection terminalCollection = new TerminalCollection();
                    terminalCollection.GetMulti(TerminalFields.CompanyId == this.DataSourceAsScheduledCommandTask.CompanyId);

                    foreach (TerminalCommand terminalCommand in terminalCommands)
                    {
                        this.CreateCommandsForTerminals(terminalCollection, terminalCommand, false);
                    }
                }
            }
        }

        private List<ClientCommand> GetSelectedClientCommands()
        {
            List<ClientCommand> clientCommands = new List<ClientCommand>();
            foreach (int value in this.lbCommands.SelectedItemsValuesAsIntList)
            {
                ClientCommand clientCommand = this.StandardCommandToClientCommand(value);
                if (clientCommand != ClientCommand.DoNothing)
                {
                    clientCommands.Add(clientCommand);
                }
            }
            return clientCommands;
        }

        private List<TerminalCommand> GetSelectedTerminalCommands()
        {
            List<TerminalCommand> terminalCommands = new List<TerminalCommand>();
            foreach (int value in this.lbCommands.SelectedItemsValuesAsIntList)
            {
                TerminalCommand terminalCommand = this.StandardCommandToTerminalCommand(value);
                if (terminalCommand != TerminalCommand.DoNothing)
                {
                    terminalCommands.Add(terminalCommand);
                }
            }
            return terminalCommands;
        }

        private ClientCommand StandardCommandToClientCommand(int value)
        {
            ClientCommand clientCommand = ClientCommand.DoNothing;

            switch (value.ToEnum<StandardCommand>())
            {
                case StandardCommand.RestartEmenu:
                    clientCommand = ClientCommand.RestartApplication;
                    break;
                case StandardCommand.DownloadEmenu:
                    clientCommand = ClientCommand.DownloadEmenuUpdate;
                    break;
                case StandardCommand.InstallEmenu:
                    clientCommand = ClientCommand.InstallEmenuUpdate;
                    break;
                case StandardCommand.DownloadAgent:
                    clientCommand = ClientCommand.DownloadAgentUpdate;
                    break;
                case StandardCommand.InstallAgent:
                    clientCommand = ClientCommand.InstallAgentUpdate;
                    break;
                case StandardCommand.DownloadSupportTools:
                    clientCommand = ClientCommand.DownloadSupportToolsUpdate;
                    break;
                case StandardCommand.InstallSupportTools:
                    clientCommand = ClientCommand.InstallSupportToolsUpdate;
                    break;
                case StandardCommand.DownloadOS:
                    clientCommand = ClientCommand.DownloadOSUpdate;
                    break;
                case StandardCommand.InstallOS:
                    clientCommand = ClientCommand.InstallOSUpdate;
                    break;
                case StandardCommand.RebootDevice:
                    clientCommand = ClientCommand.RestartDevice;
                    break;
                case StandardCommand.UpdateMessagingService:
                    clientCommand = ClientCommand.DownloadInstallMessagingServiceUpdate;
                    break;
            }
            return clientCommand;
        }

        private TerminalCommand StandardCommandToTerminalCommand(int value)
        {
            TerminalCommand terminalCommand = TerminalCommand.DoNothing;

            switch (value.ToEnum<StandardCommand>())
            {
                case StandardCommand.RestartConsole:
                    terminalCommand = TerminalCommand.RestartApplication;
                    break;
                case StandardCommand.DownloadConsole:
                    terminalCommand = TerminalCommand.DownloadConsoleUpdate;
                    break;
                case StandardCommand.InstallConsole:
                    terminalCommand = TerminalCommand.InstallConsoleUpdate;
                    break;
                case StandardCommand.DownloadAgent:
                    terminalCommand = TerminalCommand.DownloadAgentUpdate;
                    break;
                case StandardCommand.InstallAgent:
                    terminalCommand = TerminalCommand.InstallAgentUpdate;
                    break;
                case StandardCommand.DownloadSupportTools:
                    terminalCommand = TerminalCommand.DownloadSupportToolsUpdate;
                    break;
                case StandardCommand.InstallSupportTools:
                    terminalCommand = TerminalCommand.InstallSupportToolsUpdate;
                    break;
                case StandardCommand.DownloadOS:
                    terminalCommand = TerminalCommand.DownloadOSUpdate;
                    break;
                case StandardCommand.InstallOS:
                    terminalCommand = TerminalCommand.InstallOSUpdate;
                    break;
                case StandardCommand.RebootDevice:
                    terminalCommand = TerminalCommand.RestartDevice;
                    break;
                case StandardCommand.UpdateMessagingService:
                    terminalCommand = TerminalCommand.DownloadInstallMessagingServiceUpdate;
                    break;
            }

            return terminalCommand;
        }

        private void btnStartValue_Click(object sender, EventArgs e)
        {
            // Do nothing, postback is enough
        }

        #endregion

        #region Properties

        /// <summary>
        /// Return the page's datasource as a ScheduledCommandTask
        /// </summary>
        public ScheduledCommandTaskEntity DataSourceAsScheduledCommandTask
        {
            get
            {
                return this.DataSource as ScheduledCommandTaskEntity;
            }
        }

        #endregion

        #region Enums

        private enum StandardCommand : int
        {
            [StringValue("Restart Emenu")]
            RestartEmenu = 1,
            [StringValue("Download Emenu")]
            DownloadEmenu = 2,
            [StringValue("Install Emenu")]
            InstallEmenu = 3,
            [StringValue("Restart Console")]
            RestartConsole = 4,
            [StringValue("Download Console")]
            DownloadConsole = 5,
            [StringValue("Install Console")]
            InstallConsole = 6,
            [StringValue("Download Agent")]
            DownloadAgent = 7,
            [StringValue("Install Agent")]
            InstallAgent = 8,
            [StringValue("Download SupportTools")]
            DownloadSupportTools = 9,
            [StringValue("Install SupportTools")]
            InstallSupportTools = 10,
            [StringValue("Download Crave OS")]
            DownloadOS = 11,
            [StringValue("Install Crave OS")]
            InstallOS = 12,
            [StringValue("Reboot device")]
            RebootDevice = 13,
            [StringValue("Update MessagingSerivce")]
            UpdateMessagingService = 14,
        }

        #endregion
    }
}
