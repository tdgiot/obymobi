﻿using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.Injectables.Validators;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using System.Data;
using System.Linq;
using Obymobi.Data.DaoClasses;
using Obymobi.Enums;
using Dionysos.Web;
using Dionysos.Data.LLBLGen;
using Obymobi.Data;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.ObymobiCms.Company
{
    public partial class RoomControlSection : Dionysos.Web.UI.PageLLBLGenEntity
    {
        #region Methods

        private void LoadUserControls()
        {
            this.tabsMain.AddTabPage("Afbeeldingen", "Media", "~/Generic/UserControls/MediaCollection.ascx");

            // Add languages
            RoomControlSectionEntity roomControlSectionEntity = new RoomControlSectionEntity(this.EntityId);
            if (roomControlSectionEntity.IsNew)
                return;

            this.tabsMain.AddTabPage("Translations", "CustomTextCollection", "~/Generic/UserControls/CustomTextCollection.ascx", "Generic");

            if (roomControlSectionEntity.Type == RoomControlSectionType.Custom ||
                roomControlSectionEntity.Type == RoomControlSectionType.Timers)
            {
                this.tabsMain.AddTabPage("Section items", "RoomControlSectionItemCollection",
                    "~/Company/SubPanels/RoomControlSectionItemCollection.ascx");
            }
            else if (roomControlSectionEntity.Type == RoomControlSectionType.Dashboard)
            {
                this.tabsMain.AddTabPage("Widgets", "RoomControlWidgetCollection",
                    "~/Company/SubPanels/RoomControlWidgetCollection.ascx");
            }
            else if (roomControlSectionEntity.Type != RoomControlSectionType.Service)
            {
                this.tabsMain.AddTabPage("Components", "RoomControlComponentCollection",
                    "~/Company/SubPanels/RoomControlComponentCollection.ascx");
            }
        }

        public override bool Save()
        {
            // Set the UITabId if specified in querystring
            if (this.DataSourceAsRoomControlSectionEntity.IsNew)
            {
                int uiTabId;
                if (QueryStringHelper.TryGetValue("RoomControlAreaId", out uiTabId))
                    this.DataSourceAsRoomControlSectionEntity.RoomControlAreaId = uiTabId;
            }

            if (this.DataSourceAsRoomControlSectionEntity.Type != RoomControlSectionType.Custom &&
                this.DataSourceAsRoomControlSectionEntity.Type != RoomControlSectionType.Service)
            {
                PredicateExpression filter = new PredicateExpression();
                filter.Add(RoomControlSectionFields.Type == this.DataSourceAsRoomControlSectionEntity.Type);
                filter.Add(RoomControlSectionFields.RoomControlAreaId ==
                           this.DataSourceAsRoomControlSectionEntity.RoomControlAreaId);
                if (this.PageMode == Dionysos.Web.PageMode.Edit)
                {
                    filter.Add(RoomControlSectionFields.RoomControlSectionId !=
                               this.DataSourceAsRoomControlSectionEntity.RoomControlSectionId);
                }

                Obymobi.Data.CollectionClasses.RoomControlSectionCollection sections =
                    new Obymobi.Data.CollectionClasses.RoomControlSectionCollection();
                sections.GetMulti(filter);

                if (sections.Count > 0)
                {
                    this.MultiValidatorDefault.AddError(
                        string.Format("This area already has a {0} room control section configured",
                            this.DataSourceAsRoomControlSectionEntity.Type.ToString()));
                }
            }

            if (this.DataSourceAsRoomControlSectionEntity.Type == RoomControlSectionType.Service)
            {
                this.DataSourceAsRoomControlSectionEntity.FieldValue1 = this.tbSystemName.Text;
            }            

            this.Validate();

            if (base.Save())
            {
                CustomTextHelper.UpdateCustomTexts(this.DataSource, new CompanyEntity(this.DataSourceAsRoomControlSectionEntity.RoomControlAreaEntity.RoomControlConfigurationEntity.CompanyId));
                return true;
            }
            else
            {
                return false;
            }
        }

        public override bool SaveAndNew()
        {
            bool result = Save();
            if (result)
            {
                string url = this.Request.Path + "?mode=add";
                if (this.DataSourceAsRoomControlSectionEntity.RoomControlAreaId > 0)
                    url += string.Format("&RoomControlAreaId={0}",
                        this.DataSourceAsRoomControlSectionEntity.RoomControlAreaId);

                url += string.Format("&Type={0}", (int) this.DataSourceAsRoomControlSectionEntity.Type);
                    // Default on first page

                this.Response.Redirect(url, false);
            }
            return result;
        }        

        private void BindUITabTypes()
        {
            foreach (RoomControlSectionType type in Enum.GetValues(typeof (RoomControlSectionType)))
            {
                this.cbType.Items.Add(new DevExpress.Web.ListEditItem(type.ToString(), (int) type));
            }
        }

        private void SetGui()
        {
            if (this.DataSourceAsRoomControlSectionEntity.IsNew)
                this.DataSourceAsRoomControlSectionEntity.Visible = true;

            int type = -1;
            if (QueryStringHelper.HasValue("Type"))
                Int32.TryParse(QueryStringHelper.GetValue("Type"), out type);

            if (this.DataSourceAsRoomControlSectionEntity.IsNew && type > 0)
                this.DataSourceAsRoomControlSectionEntity.Type = (RoomControlSectionType) type;

            this.BindUITabTypes();

            if (!this.IsPostBack)
            {
                this.cbType.Value = (int) this.DataSourceAsRoomControlSectionEntity.Type;
            }

            if (this.DataSourceAsRoomControlSectionEntity.Type == RoomControlSectionType.Service)
            {
                this.plhService.Visible = true;

                this.tbSystemName.IsRequired = true;
                this.tbSystemName.Text = this.DataSourceAsRoomControlSectionEntity.FieldValue1;
            }            

            if (this.DataSourceAsRoomControlSectionEntity.IsDirty && !this.DataSourceAsRoomControlSectionEntity.IsNew)
                this.DataSourceAsRoomControlSectionEntity.Save();
        }       

        #endregion

		#region Event Handlers


        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            this.DataSourceLoaded += new Dionysos.Delegates.Web.DataSourceLoadedHandler(RoomControlSection_DataSourceLoaded);
            base.OnInit(e);
        }

        void RoomControlSection_DataSourceLoaded(object sender)
        {
            this.SetGui();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

		#endregion

		#region Properties

		public RoomControlSectionEntity DataSourceAsRoomControlSectionEntity
		{
			get
			{
                return this.DataSource as RoomControlSectionEntity;
			}
		}

		#endregion
	}
}
