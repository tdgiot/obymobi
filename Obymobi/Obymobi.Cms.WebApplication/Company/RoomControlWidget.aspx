﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.RoomControlWidget" Codebehind="RoomControlWidget.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
					<table class="dataformV2">
						<tr>
						    <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblCaption">Caption</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbCaption" runat="server"></D:TextBoxString>
							</td>							
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblType">Type</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
                                <X:ComboBoxInt ID="cbType" runat="server" IsRequired="true" UseDataBinding="true" Enabled="false"></X:ComboBoxInt>
							</td>				
					    </tr>
                        <tr>                            
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblVisible">Visible</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <D:CheckBox runat="server" ID="cbVisible" />
                            </td>             
                            <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblSortOrder">Sort order</D:LabelEntityFieldInfo>
							</td>
                            <td class="control">
								<D:TextBoxInt ID="tbSortOrder" runat="server"></D:TextBoxInt>
							</td>
                        </tr>    
                     </table>            

                     <D:PlaceHolder ID="plhRoomControlComponent" runat="server" Visible="false"> 
                        <table class="dataformV2">
                            <tr>
                                <td class="label">
                                    <D:Label runat="server" id="lblRoomControlComponentId1">Component 1</D:Label>
                                </td>
                                <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlRoomControlComponentId1" IncrementalFilteringMode="StartsWith" EntityName="RoomControlComponent" TextField="NameAndArea" ValueField="RoomControlComponentId" PreventEntityCollectionInitialization="true" />
                                </td>
                                <td class="label">
                                </td>
                                <td class="control">
                                </td>
                            </tr>
                        </table>                               
                     </D:PlaceHolder>

                     <D:PlaceHolder ID="plhRoomControlComponents" runat="server" Visible="false"> 
                        <D:Panel ID="lnlRoomControlComponents" runat="server" GroupingText="Components">
                            <table class="dataformV2"> 
                                <tr>
                                    <td class="label">
                                        <D:Label runat="server" id="lblRoomControlComponentId2">Component 2</D:Label>
                                    </td>
                                    <td class="control">
                                        <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlRoomControlComponentId2" IncrementalFilteringMode="StartsWith" EntityName="RoomControlComponent" TextField="NameAndArea" ValueField="RoomControlComponentId" PreventEntityCollectionInitialization="true" />
                                    </td>
                                    <td class="label">
                                        <D:Label runat="server" id="lblRoomControlComponentId3">Component 3</D:Label>
                                    </td>
                                    <td class="control">
                                        <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlRoomControlComponentId3" IncrementalFilteringMode="StartsWith" EntityName="RoomControlComponent" TextField="NameAndArea" ValueField="RoomControlComponentId" PreventEntityCollectionInitialization="true" />
                                    </td>
                                </tr>                               
                                <tr>
                                    <td class="label">
                                        <D:Label runat="server" id="lblRoomControlComponentId4">Component 4</D:Label>
                                    </td>
                                    <td class="control">
                                        <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlRoomControlComponentId4" IncrementalFilteringMode="StartsWith" EntityName="RoomControlComponent" TextField="NameAndArea" ValueField="RoomControlComponentId" PreventEntityCollectionInitialization="true" />
                                    </td>
                                    <td class="label">
                                        <D:Label runat="server" id="lblRoomControlComponentId5">Component 5</D:Label>
                                    </td>
                                    <td class="control">
                                        <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlRoomControlComponentId5" IncrementalFilteringMode="StartsWith" EntityName="RoomControlComponent" TextField="NameAndArea" ValueField="RoomControlComponentId" PreventEntityCollectionInitialization="true" />
                                    </td>
                                </tr>   
                                <tr>
                                    <td class="label">
                                        <D:Label runat="server" id="lblRoomControlComponentId6">Component 6</D:Label>
                                    </td>
                                    <td class="control">
                                        <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlRoomControlComponentId6" IncrementalFilteringMode="StartsWith" EntityName="RoomControlComponent" TextField="NameAndArea" ValueField="RoomControlComponentId" PreventEntityCollectionInitialization="true" />
                                    </td>
                                    <td class="label">
                                        <D:Label runat="server" id="lblRoomControlComponentId7">Component 7</D:Label>
                                    </td>
                                    <td class="control">
                                        <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlRoomControlComponentId7" IncrementalFilteringMode="StartsWith" EntityName="RoomControlComponent" TextField="NameAndArea" ValueField="RoomControlComponentId" PreventEntityCollectionInitialization="true" />
                                    </td>
                                </tr>   
                                <tr>
                                    <td class="label">
                                        <D:Label runat="server" id="lblRoomControlComponentId8">Component 8</D:Label>
                                    </td>
                                    <td class="control">
                                        <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlRoomControlComponentId8" IncrementalFilteringMode="StartsWith" EntityName="RoomControlComponent" TextField="NameAndArea" ValueField="RoomControlComponentId" PreventEntityCollectionInitialization="true" />
                                    </td>
                                    <td class="label">
                                        <D:Label runat="server" id="lblRoomControlComponentId9">Component 9</D:Label>
                                    </td>
                                    <td class="control">
                                        <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlRoomControlComponentId9" IncrementalFilteringMode="StartsWith" EntityName="RoomControlComponent" TextField="NameAndArea" ValueField="RoomControlComponentId" PreventEntityCollectionInitialization="true" />
                                    </td>
                                </tr>   
                                <tr>
                                    <td class="label">
                                        <D:Label runat="server" id="lblRoomControlComponentId10">Component 10</D:Label>
                                    </td>
                                    <td class="control">
                                        <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlRoomControlComponentId10" IncrementalFilteringMode="StartsWith" EntityName="RoomControlComponent" TextField="NameAndArea" ValueField="RoomControlComponentId" PreventEntityCollectionInitialization="true" />
                                    </td>                            
                                </tr> 
                            </table>  
                        </D:Panel>
                    </D:PlaceHolder>

                    <D:PlaceHolder ID="plhVolume" runat="server" Visible="false">
                        <D:Panel ID="pnlVolume" runat="server" GroupingText="Volume">
                            <table class="dataformV2">
                                <tr>
                                    <td class="label">
                                        <D:Label runat="server" id="lblVolumeUpScene">Volume up scene</D:Label>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString ID="tbVolumeUpScene" runat="server"></D:TextBoxString>
                                    </td>
                                    <td class="label">
                                        <D:Label runat="server" id="lblVolumneDownScene">Volume down scene</D:Label>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString ID="tbVolumeDownScene" runat="server"></D:TextBoxString>
                                    </td>
                                </tr>    
                                <tr>
                                    <td class="label">
                                        <D:Label runat="server" id="lblVolumeMute">Mute scene</D:Label>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString ID="tbVolumeMuteScene" runat="server"></D:TextBoxString>
                                    </td>                 
                                    <td class="label">
                                        <D:Label runat="server" id="lblPowerScene">Power scene</D:Label>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString ID="tbPowerScene" runat="server"></D:TextBoxString>
                                    </td>
                                </tr> 
                            </table>
                        </D:Panel>
                    </D:PlaceHolder>

                    <D:PlaceHolder ID="plhChannel" runat="server" Visible="false">
                        <D:Panel ID="pnlChannel" runat="server" GroupingText="Channel">
                            <table class="dataformV2">
                                <tr>
                                    <td class="label">
                                        <D:Label runat="server" id="lblChannelUpScene">Channel up scene</D:Label>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString ID="tbChannelUpScene" runat="server"></D:TextBoxString>
                                    </td>
                                    <td class="label">
                                        <D:Label runat="server" id="lblChannelDownScene">Channel down scene</D:Label>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString ID="tbChannelDownScene" runat="server"></D:TextBoxString>
                                    </td>
                                </tr>       
                            </table> 
                        </D:Panel>                    
                    </D:PlaceHolder>

                    <D:PlaceHolder ID="plhRemoteControl" runat="server" Visible="false">
                        <D:Panel ID="pnlRemoteControl" runat="server" GroupingText="Remote control">
                            <table class="dataformV2">
                                <tr>
                                    <td class="label">
                                        <D:Label runat="server" id="lblLeftButtonScene">Left button scene</D:Label>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString ID="tbLeftButtonScene" runat="server"></D:TextBoxString>
                                    </td>
                                    <td class="label">
                                        <D:Label runat="server" id="lblTopButtonScene">Top button scene</D:Label>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString ID="tbTopButtonScene" runat="server"></D:TextBoxString>
                                    </td>                            
                                </tr>                            
                                <tr>
                                    <td class="label">
                                        <D:Label runat="server" id="lblRightButtonScene">Right button scene</D:Label>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString ID="tbRightButtonScene" runat="server"></D:TextBoxString>
                                    </td>
                                    <td class="label">
                                        <D:Label runat="server" id="lblBottomButtonScene">Bottom button scene</D:Label>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString ID="tbBottomButtonScene" runat="server"></D:TextBoxString>
                                    </td>                            
                                </tr>                            
                                <tr>
                                    <td class="label">
                                        <D:Label runat="server" id="lblSelectButtonScene">Select button scene</D:Label>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString ID="tbSelectButtonScene" runat="server"></D:TextBoxString>
                                    </td>                                
                                </tr>
                            </table>
                        </D:Panel>
                     </D:PlaceHolder>

                    <D:Panel ID="pnlInfraRed" runat="server" GroupingText="Infra-red">
                        <table class="dataformV2">
                            <tr>
                                <td class="label">
                                    <D:Label runat="server" id="lblInfraredConfigurationId">Infra-red configuration</D:Label>
                                </td>
                                <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlInfraredConfigurationId" IncrementalFilteringMode="StartsWith" EntityName="InfraredConfiguration" TextField="Name" ValueField="InfraredConfigurationId" />
                                </td>
                                <td class="label">
                                </td>
                                <td class="control">
                                </td>
                            </tr>
                        </table>
                    </D:Panel>
				</Controls>
			</X:TabPage>	
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>


