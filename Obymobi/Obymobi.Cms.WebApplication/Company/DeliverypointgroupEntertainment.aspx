<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.DeliverypointgroupEntertainment" Title="Deliverypointgroup entertainment" Codebehind="DeliverypointgroupEntertainment.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
<D:Label runat="server" ID="lblTitleProducts">Deliverypointgroup entertainment</D:Label> -
<D:Label runat="server" ID="lblTitleEdit">Bewerken</D:Label> -
<D:Label runat="server" ID="lblTitleBrandName" LocalizeText="false">Bewerken</D:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<X:PageControl Id="tabsMain" runat="server" Width="100%">
	<TabPages>
		<X:TabPage Text="Algemeen" Name="Generic">
			<Controls>
				<table class="dataformV2">
					<tr>
						<td class="label">
							<D:LabelEntityFieldInfo runat="server" id="lblEntertainmentId">Entertainment</D:LabelEntityFieldInfo>
						</td>
						<td class="control">
							<U:UltraBoxInt runat="server" ID="ddlEntertainmentId"></U:UltraBoxInt>
						</td>
						<td class="label">
                            <D:LabelEntityFieldInfo runat="server" id="lblDeliverypointgroupId">Deliverypoint group</D:LabelEntityFieldInfo>
						</td>
						<td class="control">
                            <U:UltraBoxInt runat="server" ID="ddlDeliverypointgroupId"></U:UltraBoxInt>
						</td>        
					</tr> 
				 </table>			
			</Controls>
		</X:TabPage>
	</TabPages>
</X:PageControl>
  
</asp:Content>

