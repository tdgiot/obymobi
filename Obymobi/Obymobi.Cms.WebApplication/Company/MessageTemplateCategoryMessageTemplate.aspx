﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.MessageTemplateCategoryMessageTemplate" Codebehind="MessageTemplateCategoryMessageTemplate.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server" />
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server" />
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<X:PageControl Id="tabsMain" runat="server" Width="100%">
	<TabPages>
		<X:TabPage Text="Algemeen" Name="Generic">
			<Controls>
				<table class="dataformV2">
					<tr>
						<td class="label">
							<D:Label runat="server" id="lblMessageTemplateCategoryId">Message template category</D:Label>
						</td>
						<td class="control">
							<X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlMessageTemplateCategoryId" UseDataBinding="true" EntityName="MessageTemplateCategory" TextField="Name" IsRequired="true" />
						</td>
						<td class="label">
                            <D:Label runat="server" id="lblMessageTemplateId">Message template</D:Label>
						</td>
						<td class="control">
                            <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlMessageTemplateId" UseDataBinding="true" EntityName="MessageTemplate" TextField="Name" IsRequired="true" />
						</td>        
					</tr> 
				 </table>			
			</Controls>
		</X:TabPage>
	</TabPages>
</X:PageControl>
</asp:Content>