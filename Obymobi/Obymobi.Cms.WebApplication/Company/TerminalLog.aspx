﻿<%@ Page Title="Log" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.TerminalLog" ValidateRequest="false" Codebehind="TerminalLog.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server"></asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="cplhAdditionalToolBarButtons">
    <span class="toolbar">
        <D:Button runat="server" ID="btBack" Text="Cancel" />	
    </span>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Log" Name="Log">
				<Controls>
				    <D:PlaceHolder runat="server" ID="phlNormalLog">
                    <table class="dataformV2">
	                    <tr>
		                    <td class="label">
			                    <D:LabelEntityFieldInfo runat="server" ID="lblCreated">Created</D:LabelEntityFieldInfo>
		                    </td>
		                    <td class="control">
			                    <D:Label ID="lblCreatedValue" runat="server" LocalizeText="false" />
		                    </td>
		                    <td class="label">
			                    <D:LabelEntityFieldInfo runat="server" ID="lblType">Type</D:LabelEntityFieldInfo>
		                    </td>
		                    <td class="control">
			                    <D:Label ID="lblTypeValue" runat="server" LocalizeText="false" />
		                    </td>
	                    </tr>
	                    <tr>
		                    <td class="label">
			                    <D:LabelEntityFieldInfo runat="server" ID="lblStatus">Status</D:LabelEntityFieldInfo>
		                    </td>
		                    <td class="control">
			                    <D:Label ID="lblStatusValue" runat="server"  LocalizeText="false" />
		                    </td>
		                    <td class="label">
			                    <D:LabelEntityFieldInfo runat="server" ID="lblMessage">Message</D:LabelEntityFieldInfo>
		                    </td>
		                    <td class="control">
			                    <D:Label ID="lblMessageValue" runat="server" LocalizeText="false"></D:Label>
		                    </td>
	                    </tr>
                        <tr>
		                    <td class="label">
			                    <D:LabelEntityFieldInfo runat="server" ID="lblLog">Log</D:LabelEntityFieldInfo>
		                    </td>
		                    <td class="control" colspan="3">
			                    <D:TextBoxMultiLine ID="tbLog" runat="server" LocalizeText="false" Rows="30" ReadOnly="true" BackColor="White" />
		                    </td>
	                    </tr>
                    </table>
                    </D:PlaceHolder>
                    <D:PlaceHolder runat="server" ID="phlShippedLog" Visible="false">
                        <table class="dataformV2">
	                    <tr>
		                    <td class="label">
		                        <D:Label runat="server" ID="lblLogCaption" LocalizeText="False">Log</D:Label>
		                    </td>
		                    <td class="control">
			                    <D:Label runat="server" ID="lblLogMessage" LocalizeText="False"></D:Label>
		                    </td>
		                    <td class="label">
		                    </td>
		                    <td class="control">
		                    </td>
	                    </tr>
                        <tr>
                            <td class="label">
		                    </td>
		                    <td class="control">
		                        <D:Button runat="server" ID="btnDownloadLog" Text="Download Log File" LocalizeText="True" TranslationTagText="btnDownloadLogText"/>
		                    </td>
                            <td class="label">
		                    </td>
		                    <td class="control">
		                    </td>
                        </tr>
                        <tr>
                            <td class="label"></td>
                            <td class="control" colspan="3">
                                <D:TextBoxMultiLine ID="tbMessage" runat="server" LocalizeText="false" Rows="30" ReadOnly="true" BackColor="White" />
                            </td>
                        </tr>
                        </table>
                    </D:PlaceHolder>
				</Controls>
			</X:TabPage>					
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>