﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.RoomControlSection" Codebehind="RoomControlSection.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
				    <D:PlaceHolder runat="server" ID="plhGeneric">
				            <table class="dataformV2">
						        <tr>
							        <td class="label">
								        <D:LabelEntityFieldInfo runat="server" id="lblName">Name</D:LabelEntityFieldInfo>
							        </td>
							        <td class="control">
								        <D:TextBoxString ID="tbName" runat="server"></D:TextBoxString>
							        </td>
							        <td class="label">
								        <D:LabelEntityFieldInfo runat="server" id="lblType">Type</D:LabelEntityFieldInfo>
							        </td>
							        <td class="control">
                                        <X:ComboBoxInt ID="cbType" runat="server" IsRequired="true" UseDataBinding="true"></X:ComboBoxInt>
							        </td>
					            </tr>
                                <D:PlaceHolder ID="plhService" runat="server" Visible="false">
                                <tr>
                                    <td class="label">
                                        <D:Label runat="server" id="lblSystemName">System name</D:Label>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString ID="tbSystemName" runat="server"></D:TextBoxString>
                                    </td>                            
                                </tr>
                                </D:PlaceHolder>
                                <tr>
							        <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" id="lblVisible">Visible</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <D:CheckBox runat="server" ID="cbVisible" />
                                    </td>
                                    <td class="label">
								        <D:LabelEntityFieldInfo runat="server" id="lblSortOrder">Sort order</D:LabelEntityFieldInfo>
							        </td>
							        <td class="control">
								        <D:TextBoxInt ID="tbSortOrder" runat="server"></D:TextBoxInt>
							        </td>
					            </tr>     
                            </table>	   
					</D:PlaceHolder>                    
				</Controls>
			</X:TabPage>	
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>

