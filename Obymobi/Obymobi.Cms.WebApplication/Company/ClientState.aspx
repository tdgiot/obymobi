﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.ClientState" Codebehind="ClientState.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server"></asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="cplhAdditionalToolBarButtons">
    <span class="toolbar">
        <D:Button runat="server" ID="btBack" Text="Cancel" />	
    </span>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Log" Name="Log">
				<Controls>
                    <table class="dataformV2">
	                    <tr>
		                    <td class="label">
			                    <D:LabelEntityFieldInfo runat="server" ID="lblCreated">Created</D:LabelEntityFieldInfo>
		                    </td>
		                    <td class="control">
			                    <D:Label ID="lblCreatedValue" runat="server" LocalizeText="false" />
		                    </td>
		                    <td class="label">
		                    </td>
		                    <td class="control">
		                    </td>
	                    </tr>
	                    <tr>
		                    <td class="label">
			                    <D:LabelEntityFieldInfo runat="server" ID="lblOnline">Online</D:LabelEntityFieldInfo>
		                    </td>
		                    <td class="control">
			                    <D:Label ID="lblOnlineValue" runat="server"  LocalizeText="false" />
		                    </td>
		                    <td class="label">
			                    <D:LabelEntityFieldInfo runat="server" ID="lblOperationMode">Operation mode</D:LabelEntityFieldInfo>
		                    </td>
		                    <td class="control">
			                    <D:Label ID="lblOperationModeValue" runat="server" LocalizeText="false"></D:Label>
		                    </td>
	                    </tr>
	                    <tr>
		                    <td class="label">
			                    <D:LabelEntityFieldInfo runat="server" ID="lblLastBatteryLevel">Last battery level</D:LabelEntityFieldInfo>
		                    </td>
		                    <td class="control">
			                    <D:Label ID="lblLastBatteryLevelValue" runat="server"  LocalizeText="false" />
		                    </td>
		                    <td class="label">
			                    <D:LabelEntityFieldInfo runat="server" ID="lblLastIsCharging">Last is charging</D:LabelEntityFieldInfo>
		                    </td>
		                    <td class="control">
			                    <D:Label ID="lblLastIsChargingValue" runat="server" LocalizeText="false"></D:Label>
		                    </td>
	                    </tr>
                        <tr>
		                    <td class="label">
			                    <D:LabelEntityFieldInfo runat="server" ID="lblMessage">Message</D:LabelEntityFieldInfo>
		                    </td>
		                    <td class="control" colspan="3">
			                    <D:TextBoxMultiLine ID="tbMessage" runat="server" LocalizeText="false" Rows="30" ReadOnly="true" BackColor="White" />
		                    </td>
	                    </tr>
                    </table>

				</Controls>
			</X:TabPage>					
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>