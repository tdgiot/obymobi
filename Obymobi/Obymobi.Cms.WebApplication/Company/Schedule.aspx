﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.Schedule" Codebehind="Schedule.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblName">Naam</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
							</td>
							<td class="label">
                                &nbsp;
							</td>
							<td class="control">
								&nbsp;
							</td>     
						</tr>
                        <tr>
							<td class="label">
                                &nbsp;
							</td>
							<td class="control" colspan="3">
                                <table class="orderhour">
                                    <tr>
                                        <th>
                                            <D:Label runat="server" ID="lblDay">Dag</D:Label>
                                        </th>
                                        <th>
                                            <D:Label runat="server" ID="lblFromTo">Van / Tot</D:Label>
                                        </th>
                                        <th style="width:20px;">&nbsp;</th>
                                        <th>
                                            <D:Label runat="server" ID="lblFromTo2">Van / Tot</D:Label>
                                        </th>
                                        <th style="width:20px;">&nbsp;</th>
                                        <th>
                                            <D:Label runat="server" ID="lblFromTo3">Van / Tot</D:Label>
                                        </th>
                                        <th style="width:20px;">&nbsp;</th>
                                        <th>
                                            <D:Label runat="server" ID="lblAvailable">Beschikbaarheid</D:Label>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td>
                                            <D:Label runat="server" ID="lblAllDays">Alle dagen</D:Label>
                                        </td>
                                        <td>                
                                            <D:TimePicker2 runat="server" ID="teDayAllBegin1"></D:TimePicker2>
                                            <D:TimePicker2 runat="server" ID="teDayAllEnd1"></D:TimePicker2>
                                        </td>
                                        <td class="empty_space"></td>
                                        <td>
                                            <D:TimePicker2 runat="server" ID="teDayAllBegin2"></D:TimePicker2>
                                            <D:TimePicker2 runat="server" ID="teDayAllEnd2"></D:TimePicker2>
                                        </td>
                                        <td class="empty_space"></td>
                                        <td>
                                            <D:TimePicker2 runat="server" ID="teDayAllBegin3"></D:TimePicker2>
                                            <D:TimePicker2 runat="server" ID="teDayAllEnd3"></D:TimePicker2>
                                        </td>        
                                        <td class="empty_space"></td>
                                        <td class="available_column">
                                            <D:Label runat="server" ID="lblDayAllAvailable" LocalizeText="false" CssClass="available"></D:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <%= Dionysos.Global.TranslationProvider.GetTranslation(Dionysos.Globalization.GenericTranslatables.Monday) %>
                                        </td>
                                        <td>
                                            <D:TimePicker2 runat="server" ID="teDay1Begin1"></D:TimePicker2>
                                            <D:TimePicker2 runat="server" ID="teDay1End1"></D:TimePicker2>
                                        </td>
                                        <td class="empty_space"></td>
                                        <td>
                                            <D:TimePicker2 runat="server" ID="teDay1Begin2"></D:TimePicker2>
                                            <D:TimePicker2 runat="server" ID="teDay1End2"></D:TimePicker2>
                                        </td>
                                        <td class="empty_space"></td>
                                        <td>
                                            <D:TimePicker2 runat="server" ID="teDay1Begin3"></D:TimePicker2>
                                            <D:TimePicker2 runat="server" ID="teDay1End3"></D:TimePicker2>
                                        </td>        
                                        <td class="empty_space"></td>
                                        <td class="available_column">
                                            <D:Label runat="server" ID="lblDay1Available" LocalizeText="false" CssClass="available"></D:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <%= Dionysos.Global.TranslationProvider.GetTranslation(Dionysos.Globalization.GenericTranslatables.Tuesday) %>
                                        </td>
                                        <td>
                                            <D:TimePicker2 runat="server" ID="teDay2Begin1"></D:TimePicker2>
                                            <D:TimePicker2 runat="server" ID="teDay2End1"></D:TimePicker2>
                                        </td>
                                        <td class="empty_space"></td>
                                        <td>
                                            <D:TimePicker2 runat="server" ID="teDay2Begin2"></D:TimePicker2>
                                            <D:TimePicker2 runat="server" ID="teDay2End2"></D:TimePicker2>
                                        </td>
                                        <td class="empty_space"></td>
                                        <td>
                                            <D:TimePicker2 runat="server" ID="teDay2Begin3"></D:TimePicker2>
                                            <D:TimePicker2 runat="server" ID="teDay2End3"></D:TimePicker2>
                                        </td>        
                                        <td class="empty_space"></td>
                                        <td class="available_column">
                                            <D:Label runat="server" ID="lblDay2Available" LocalizeText="false" CssClass="available"></D:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <%= Dionysos.Global.TranslationProvider.GetTranslation(Dionysos.Globalization.GenericTranslatables.Wednesday) %>
                                        </td>
                                        <td>
                                            <D:TimePicker2 runat="server" ID="teDay3Begin1"></D:TimePicker2>
                                            <D:TimePicker2 runat="server" ID="teDay3End1"></D:TimePicker2>
                                        </td>
                                        <td class="empty_space"></td>
                                        <td>
                                            <D:TimePicker2 runat="server" ID="teDay3Begin2"></D:TimePicker2>
                                            <D:TimePicker2 runat="server" ID="teDay3End2"></D:TimePicker2>
                                        </td>
                                        <td class="empty_space"></td>
                                        <td>
                                            <D:TimePicker2 runat="server" ID="teDay3Begin3"></D:TimePicker2>
                                            <D:TimePicker2 runat="server" ID="teDay3End3"></D:TimePicker2>
                                        </td>        
                                        <td class="empty_space"></td>
                                        <td class="available_column">
                                            <D:Label runat="server" ID="lblDay3Available" LocalizeText="false" CssClass="available"></D:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <%= Dionysos.Global.TranslationProvider.GetTranslation(Dionysos.Globalization.GenericTranslatables.Thursday) %>
                                        </td>
                                        <td>
                                            <D:TimePicker2 runat="server" ID="teDay4Begin1"></D:TimePicker2>
                                            <D:TimePicker2 runat="server" ID="teDay4End1"></D:TimePicker2>
                                        </td>
                                        <td class="empty_space"></td>
                                        <td>
                                            <D:TimePicker2 runat="server" ID="teDay4Begin2"></D:TimePicker2>
                                            <D:TimePicker2 runat="server" ID="teDay4End2"></D:TimePicker2>
                                        </td>
                                        <td class="empty_space"></td>
                                        <td>
                                            <D:TimePicker2 runat="server" ID="teDay4Begin3"></D:TimePicker2>
                                            <D:TimePicker2 runat="server" ID="teDay4End3"></D:TimePicker2>
                                        </td>        
                                        <td class="empty_space"></td>
                                        <td class="available_column">
                                            <D:Label runat="server" ID="lblDay4Available" LocalizeText="false" CssClass="available"></D:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <%= Dionysos.Global.TranslationProvider.GetTranslation(Dionysos.Globalization.GenericTranslatables.Friday) %>
                                        </td>
                                        <td>
                                            <D:TimePicker2 runat="server" ID="teDay5Begin1"></D:TimePicker2>
                                            <D:TimePicker2 runat="server" ID="teDay5End1"></D:TimePicker2>
                                        </td>
                                        <td class="empty_space"></td>
                                        <td>
                                            <D:TimePicker2 runat="server" ID="teDay5Begin2"></D:TimePicker2>
                                            <D:TimePicker2 runat="server" ID="teDay5End2"></D:TimePicker2>
                                        </td>
                                        <td class="empty_space"></td>
                                        <td>
                                            <D:TimePicker2 runat="server" ID="teDay5Begin3"></D:TimePicker2>
                                            <D:TimePicker2 runat="server" ID="teDay5End3"></D:TimePicker2>
                                        </td>        
                                        <td class="empty_space"></td>
                                        <td class="available_column">
                                            <D:Label runat="server" ID="lblDay5Available" LocalizeText="false" CssClass="available"></D:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <%= Dionysos.Global.TranslationProvider.GetTranslation(Dionysos.Globalization.GenericTranslatables.Saturday) %>
                                        </td>
                                        <td>
                                            <D:TimePicker2 runat="server" ID="teDay6Begin1"></D:TimePicker2>
                                            <D:TimePicker2 runat="server" ID="teDay6End1"></D:TimePicker2>
                                        </td>
                                        <td class="empty_space"></td>
                                        <td>
                                            <D:TimePicker2 runat="server" ID="teDay6Begin2"></D:TimePicker2>
                                            <D:TimePicker2 runat="server" ID="teDay6End2"></D:TimePicker2>
                                        </td>
                                        <td class="empty_space"></td>
                                        <td>
                                            <D:TimePicker2 runat="server" ID="teDay6Begin3"></D:TimePicker2>
                                            <D:TimePicker2 runat="server" ID="teDay6End3"></D:TimePicker2>
                                        </td>        
                                        <td class="empty_space"></td>
                                        <td class="available_column">
                                            <D:Label runat="server" ID="lblDay6Available" LocalizeText="false" CssClass="available"></D:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <%= Dionysos.Global.TranslationProvider.GetTranslation(Dionysos.Globalization.GenericTranslatables.Sunday) %>
                                        </td>
                                        <td>
                                            <D:TimePicker2 runat="server" ID="teDay0Begin1"></D:TimePicker2>
                                            <D:TimePicker2 runat="server" ID="teDay0End1"></D:TimePicker2>
                                        </td>
                                        <td class="empty_space"></td>
                                        <td>
                                            <D:TimePicker2 runat="server" ID="teDay0Begin2"></D:TimePicker2>
                                            <D:TimePicker2 runat="server" ID="teDay0End2"></D:TimePicker2>
                                        </td>
                                        <td class="empty_space"></td>
                                        <td>
                                            <D:TimePicker2 runat="server" ID="teDay0Begin3"></D:TimePicker2>
                                            <D:TimePicker2 runat="server" ID="teDay0End3"></D:TimePicker2>
                                        </td>        
                                        <td class="empty_space"></td>
                                        <td class="available_column">
                                            <D:Label runat="server" ID="lblDay0Available" LocalizeText="false" CssClass="available"></D:Label>
                                        </td>
                                    </tr>
                                </table>    								
							</td>
                        </tr>
					 </table>
				</Controls>
			</X:TabPage>	
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>

