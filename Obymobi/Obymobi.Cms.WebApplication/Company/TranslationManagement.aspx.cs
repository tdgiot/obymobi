﻿using System;
using System.Collections.Generic;
using System.Web;
using Dionysos.Web.UI;
using Obymobi.Data.EntityClasses;
using System.IO;
using System.Linq;
using Dionysos;
using Obymobi.Logic.Globalization;
using DevExpress.Web;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data;

namespace Obymobi.ObymobiCms.Company
{
    public partial class TranslationManagement : PageDefault
    {
        #region Fields

        private TranslationManager translationManager = null;
        private CompanyEntity companyEntity;
        private int timeOut;

        #endregion

        #region Methods

        public TranslationManagement()
        {
            this.Init += this.TranslationManagement_Init;
        }

        void TranslationManagement_Init(object sender, EventArgs e)
        {
            timeOut = Server.ScriptTimeout;

            // Give it 10 minues = 600 seconds
            Server.ScriptTimeout = 600;
        }

        private void SetGui()
        {
            PrefetchPath prefetch = new PrefetchPath(EntityType.CompanyEntity);
            prefetch.Add(CompanyEntityBase.PrefetchPathCompanyCultureCollection);

            this.companyEntity = new CompanyEntity(CmsSessionHelper.CurrentCompanyId, prefetch);

            this.btnExportAppLessTranslations.Visible = companyEntity.ApplicationConfigurationCollection.Any();

            this.lblExportCompanyValue.Text = companyEntity.Name;
            this.lblImportCompanyValue.Text = companyEntity.Name;

            this.translationManager = new TranslationManager(CmsSessionHelper.CurrentCompanyId);

            this.DataBindMenu();
            this.DataBindSourceCultures();
            this.DataBindDestinationCultures();
        }

        private void HookupEvents()
        {
            // Export buttons
            this.btnExportCompanyTranslations.Click += this.btnExportCompanyTranslations_Click;
            this.btnExportMenuTranslations.Click += this.btnExportMenuTranslations_Click;
            this.btnExportGenericTranslations.Click += this.btnExportGenericTranslations_Click;
            this.btnExportAppLessTranslations.Click += this.btnExportAppLessTranslations_Click;

            // Import buttons
            this.btnImport.Click += this.btnImport_Click;
        }

        private void ExportCompanyTranslations()
        {
            string sourceCultureCode = this.cbSourceCulture.SelectedValueString;
            List<string> destinationCultureCodes = this.lbDestinationCultures.SelectedValuesAsStringList;

            if (destinationCultureCodes.Count == 0)
            {
                this.MultiValidatorDefault.AddError("No destination cultures have been selected, please select one of more cultures.");
                this.Validate();
                return;
            }

            translationManager.LoadCompanyTranslations();

            string path = HttpContext.Current.Server.MapPath($"~/Temp/{CmsSessionHelper.CurrentCompanyId}_{DateTime.Now:yyyyMMddhhmmss}/");
            string filePath = translationManager.Export(path, sourceCultureCode, destinationCultureCodes);

            this.DownloadFile(filePath);
        }

        private void ExportMenuTranslations()
        {
            string sourceCultureCode = this.cbSourceCulture.SelectedValueString;
            List<string> destinationCultureCodes = this.lbDestinationCultures.SelectedValuesAsStringList;

            if (destinationCultureCodes.Count == 0)
            {
                this.MultiValidatorDefault.AddError("No destination cultures have been selected, please select one or more cultures.");
            }

            int? menuId = this.GetSelectedMenuId();
            if (!menuId.HasValue)
            {
                this.MultiValidatorDefault.AddError("No menu has been selected, please select a menu to export.");
            }
            
            this.Validate();
            if (!this.IsValid)
            {
                return;
            }

            translationManager.LoadMenuTranslations(menuId.Value);

            string path = HttpContext.Current.Server.MapPath($"~/Temp/{CmsSessionHelper.CurrentCompanyId}_{DateTime.Now:yyyyMMddhhmmss}/");
            string filePath = translationManager.Export(path, sourceCultureCode, destinationCultureCodes);

            this.DownloadFile(filePath);
        }

        private void ExportGenericTranslations()
        {
            // Generic stuff is saved under culture code 'en_GB' as default english
            string sourceCultureCode = Obymobi.Culture.English_United_Kingdom.Code;
            List<string> destinationCultureCodes = this.lbDestinationCultures.SelectedValuesAsStringList;

            if (destinationCultureCodes.Count == 0)
            {
                this.MultiValidatorDefault.AddError("No destination cultures have been selected, please select one of more cultures.");
                this.Validate();
                return;
            }

            this.translationManager.LoadGenericTranslations();

            string path = HttpContext.Current.Server.MapPath($"~/Temp/");
            string filePath = this.translationManager.Export(path, sourceCultureCode, destinationCultureCodes);

            this.DownloadFile(filePath);
        }

        private void ExportAppLessTranslations()
        {
            string sourceCultureCode = this.cbSourceCulture.SelectedValueString;
            List<string> destinationCultureCodes = this.lbDestinationCultures.SelectedValuesAsStringList;

            if (destinationCultureCodes.Count == 0)
            {
                this.MultiValidatorDefault.AddError("No destination cultures have been selected, please select one of more cultures.");
                this.Validate();
                return;
            }

            translationManager.LoadAppLessTranslations();

            string path = HttpContext.Current.Server.MapPath($"~/Temp/{CmsSessionHelper.CurrentCompanyId}_{DateTime.Now:yyyyMMddhhmmss}/");
            string filePath = translationManager.Export(path, sourceCultureCode, destinationCultureCodes);

            this.DownloadFile(filePath);
        }

        private int? GetSelectedMenuId()
        {
            int? menuId = null;
            if (this.ddlMenuId.SelectedIndex > 0)
            {
                menuId = (int) this.ddlMenuId.SelectedItem.Value;
            }

            return menuId;
        }

        private void DownloadFile(string filePath)
        {
            this.Response.Clear();
            this.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            this.Response.AddHeader("Content-Disposition", $"attachment; filename={Path.GetFileName(filePath)}");
            this.Response.BinaryWrite(File.ReadAllBytes(filePath));
            this.Response.Flush();
            this.Response.Close();
            this.Response.End();
        }

        private void Import(string path)
        {
            string errorMessage = this.translationManager.ValidateExcel(path);
            if (!errorMessage.IsNullOrWhiteSpace())
            {
                errorMessage = "The following culture codes in the import file are invalid or have not been activated at the company:" + " " + errorMessage;

                this.MultiValidatorDefault.AddError(errorMessage);
                this.Validate();
            }
            else
            {
                string result = translationManager.ImportFromExcel(path);
                this.lblResultValue.Text = result.Replace(Environment.NewLine, "<br/>");

                this.AddInformatorInfo(this.Translate("translation-file-succesfully-imported", "The translation file was successfully imported."));
                this.Validate();
            }
        }

        #endregion

        #region Event handlers

        protected void Page_Init(object sender, EventArgs e)
        {
            this.SetGui();
        }        

        protected void Page_Load(object sender, EventArgs e)
        {
            this.HookupEvents();
        }

        public void fuImportFile_FileUploadComplete(object sender, DevExpress.Web.FileUploadCompleteEventArgs e)
        {
            string localTempPath = HttpContext.Current.Server.MapPath("~/Temp/");
            if (!Directory.Exists(localTempPath))
            {
                Directory.CreateDirectory(localTempPath);
            }

            string path = Path.Combine(localTempPath, e.UploadedFile.FileName);

            e.UploadedFile.SaveAs(path, true);
        }

        

        private void btnExportCompanyTranslations_Click(object sender, EventArgs e)
        {
            this.ExportCompanyTranslations();
        }

        private void btnExportMenuTranslations_Click(object sender, EventArgs e)
        {
            this.ExportMenuTranslations();
        }

        private void btnExportGenericTranslations_Click(object sender, EventArgs e)
        {
            this.ExportGenericTranslations();
        }

        private void btnExportAppLessTranslations_Click(object sender, EventArgs e)
        {
            this.ExportAppLessTranslations();
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            if (this.fuImportFile.UploadedFiles.Length == 0 || string.IsNullOrWhiteSpace(this.fuImportFile.UploadedFiles[0].FileName))
            {
                this.MultiValidatorDefault.AddError(this.Translate("no-files-have-been-uploaded", "No files have been selected to upload, please select a file and click the Upload button."));
                this.Validate();
            }
            else if (!this.fuImportFile.UploadedFiles[0].FileName.EndsWith(".xlsx"))
            {
                this.MultiValidatorDefault.AddError(this.Translate("uploaded-file-no-valid-excel-file", "The uploaded file is not a valid Excel file with extension .xlsx"));
                this.Validate();
            }
            else
            {
                string localTempPath = HttpContext.Current.Server.MapPath("~/Temp/");
                if (!Directory.Exists(localTempPath))
                    Directory.CreateDirectory(localTempPath);

                string file = this.fuImportFile.UploadedFiles[0].FileName;
                string path = Path.Combine(localTempPath, file);

                this.Import(path);
            }
        }

        private void DataBindMenu()
        {
            this.ddlMenuId.DataBindEntityCollection<MenuEntity>(MenuFields.CompanyId == CmsSessionHelper.CurrentCompanyId, MenuFields.Name, MenuFields.Name);
        }

        private void DataBindSourceCultures()
        {
            this.cbSourceCulture.Items.Clear();

            foreach (CompanyCultureEntity companyCulture in companyEntity.CompanyCultureCollection)
            {
                Culture culture = Obymobi.Culture.Mappings[companyCulture.CultureCode];
                
                this.cbSourceCulture.Items.Add(new ListEditItem(culture.ToString(), culture.Code));
            }

            this.cbSourceCulture.Value = companyEntity.CultureCode;
        }

        private void DataBindDestinationCultures()
        {
            this.lbDestinationCultures.Items.Clear();

            string defaultCultureCode = Obymobi.Culture.Mappings[companyEntity.CultureCode].Code;
            string defaultIndicator = $" ({this.Translate("default-language", "Default")})";

            foreach (CompanyCultureEntity companyCulture in companyEntity.CompanyCultureCollection)
            {
                Culture culture = Obymobi.Culture.Mappings[companyCulture.CultureCode];
                if (defaultCultureCode == culture.Code)
                {
                    this.lbDestinationCultures.Items.Add(new ListEditItem($"{culture} {defaultIndicator}", culture.Code));
                }
                else
                {
                    this.lbDestinationCultures.Items.Add(new ListEditItem(culture.ToString(), culture.Code));
                }
            }
        }

        protected new void Page_Unload(object sender, EventArgs e)
        {
            Server.ScriptTimeout = timeOut;

            base.Page_Unload(sender, e);
        }

        #endregion
    }
}