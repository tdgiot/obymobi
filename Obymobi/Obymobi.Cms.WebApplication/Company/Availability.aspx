﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.Availability" Codebehind="Availability.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<controls>
				    <D:Panel ID="pnlAvailability" runat="server" GroupingText="Availability">
					    <table class="dataformV2">
						    <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblName">Name</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
							        <D:TextBoxString ID="tbName" runat="server" IsRequired="true"/>
							    </td>
							    <td class="label">
								    <D:Label runat="server" id="lblStatus">Status</D:Label>
							    </td>
							    <td class="control">
							        <D:TextBoxString ID="tbStatus" runat="server" IsRequired="true"/>
							    </td>
					        </tr>
                        </table>
                    </D:Panel>
                    <D:Panel ID="pnlActions" runat="server" GroupingText="Actions">
				        <table class="dataformV2">
				            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblActionCategoryId">Category</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection ID="cbActionCategoryId" runat="server" IncrementalFilteringMode="StartsWith" EntityName="Category" ValueField="CategoryId" TextField="FullCategoryMenuName" PreventEntityCollectionInitialization="true" UseDataBinding="true">
                                        <ClientSideEvents SelectedIndexChanged="function(s, e) {cbActionProductCategoryId.PerformCallback('1');}" />
                                    </X:ComboBoxLLBLGenEntityCollection>
                                </td>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblActionProductCategoryId">Product</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection ID="cbActionProductCategoryId" ClientInstanceName="cbActionProductCategoryId" OnCallback="cbActionProductCategoryId_OnCallback" runat="server" IncrementalFilteringMode="StartsWith" EntityName="ProductCategory" ValueField="ProductCategoryId" TextField="ProductCategoryMenuName" PreventEntityCollectionInitialization="true" UseDataBinding="true"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblActionEntertainmentId">Entertainment</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection ID="cbActionEntertainmentId" runat="server" IncrementalFilteringMode="StartsWith" EntityName="Entertainment" ValueField="EntertainmentId" TextField="Name" PreventEntityCollectionInitialization="true" UseDataBinding="true"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblActionSiteId">Site</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection ID="cbActionSiteId" runat="server" IncrementalFilteringMode="StartsWith" EntityName="Site" ValueField="SiteId" TextField="Name" PreventEntityCollectionInitialization="true" UseDataBinding="true">
                                        <ClientSideEvents SelectedIndexChanged="function(s, e) {cbActionPageId.PerformCallback('1');}" />
                                    </X:ComboBoxLLBLGenEntityCollection>
                                </td>
								<td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblActionPageId">Page</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection ID="cbActionPageId" ClientInstanceName="cbActionPageId" OnCallback="cbActionPageId_OnCallback" runat="server" IncrementalFilteringMode="StartsWith" EntityName="Page" ValueField="PageId" TextField="Name" PreventEntityCollectionInitialization="true" UseDataBinding="true"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblUrl">URL</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:TextBoxString ID="tbUrl" runat="server" IsRequired="false"></D:TextBoxString>
                                </td>
                            </tr>
                        </table>
                    </D:Panel>
                </controls>
            </X:TabPage>            								
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>

