﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.Management" Title="Management" CodeBehind="Management.aspx.cs" %>

<%@ Import Namespace="Obymobi" %>
<%@ Import Namespace="Obymobi.Logic.Comet" %>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="cplhTitleHolder">Management</asp:Content>
<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="cplhToolbarHolder"></asp:Content>
<asp:Content ID="Content6" runat="server" ContentPlaceHolderID="cplhContentHolder">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true" />
    <div>
        <X:PageControl ID="tabsMain" runat="server" Width="100%">
            <TabPages>
                <X:TabPage Text="Clients" Name="Clients">
                    <controls>
                        <D:Button ID="btnRefreshClients" runat="server" Text="Refresh clients" Style="margin: 4px 0 8px 0;" OnClientClick="PageButtons.RefreshClients(); return false;" /><D:Label runat="server" ID="lblClientRefresh"></D:Label>
                        <D:Button ID="btnRestartClients" runat="server" Text="Restart all clients" Style="margin: 4px 0 8px 8px;" PreSubmitWarning="Are you sure you want to restart all clients?" />

                        <table cellspacing="3">
                            <tr>
                                <td style="font-weight: bold; padding-right: 5px;">Legend:</td>
                                <td style="color: black; padding-right: 5px;">&#9632; OK</td>
                                <td style="color: blue; padding-right: 5px;">&#9632; Newer version</td>
                                <td style="color: orange; padding-right: 5px;">&#9632; Outdated</td>
                                <td style="color: red;">&#9632; Offline</td>
                                <td style="color: gray;">&#9632; Unknown</td>
                            </tr>
                        </table>

                        <X:GridView ID="clientGrid" ClientInstanceName="clientGrid" runat="server" Width="100%" KeyFieldName="ClientId">
                            <settingspager pagesize="250"></settingspager>
                            <settingsbehavior allowgroup="false" allowdragdrop="false" autofilterrowinputdelay="350" />
                            <columns>
                                <dxwgv:GridViewDataHyperLinkColumn FieldName="ClientId" VisibleIndex="1" SortIndex="0" SortOrder="Ascending">
                                    <settings autofiltercondition="Contains" />
                                    <propertieshyperlinkedit navigateurlformatstring="Client.aspx?id={0}" target="_blank" textfield="ClientId" />
                                </dxwgv:GridViewDataHyperLinkColumn>
                                <dxwgv:GridViewDataColumn FieldName="DeliverypointNumber" VisibleIndex="2">
                                    <settings autofiltercondition="Contains" />
                                </dxwgv:GridViewDataColumn>
                                <dxwgv:GridViewDataColumn FieldName="LastRequest" VisibleIndex="3">
                                    <settings autofiltercondition="Contains" />
                                </dxwgv:GridViewDataColumn>
                                <dxwgv:GridViewDataColumn FieldName="Loaded" VisibleIndex="4" Width="70">
                                    <settings autofiltercondition="Contains" />
                                </dxwgv:GridViewDataColumn>
                                <dxwgv:GridViewDataColumn FieldName="LocalIp" VisibleIndex="5">
                                    <settings autofiltercondition="Contains" />
                                </dxwgv:GridViewDataColumn>
                                <dxwgv:GridViewDataColumn FieldName="ExternalIp" VisibleIndex="6">
                                    <settings autofiltercondition="Contains" />
                                </dxwgv:GridViewDataColumn>
                                <dxwgv:GridViewDataColumn FieldName="Version" VisibleIndex="7">
                                    <settings autofiltercondition="Contains" />
                                </dxwgv:GridViewDataColumn>
                                <dxwgv:GridViewDataColumn FieldName="VersionST" VisibleIndex="8">
                                    <settings autofiltercondition="Contains" />
                                </dxwgv:GridViewDataColumn>
                                <dxwgv:GridViewDataColumn FieldName="VersionAgent" VisibleIndex="9">
                                    <settings autofiltercondition="Contains" />
                                </dxwgv:GridViewDataColumn>
                                <dxwgv:GridViewDataColumn FieldName="VersionOS" VisibleIndex="10">
                                    <settings autofiltercondition="Contains" />
                                </dxwgv:GridViewDataColumn>
                                <dxwgv:GridViewDataColumn FieldName="VersionMessaging" VisibleIndex="11">
                                    <settings autofiltercondition="Contains" />
                                </dxwgv:GridViewDataColumn>
                                <dxwgv:GridViewDataColumn FieldName="IsSupportTools" VisibleIndex="12">
                                    <settings autofiltercondition="Contains" />
                                </dxwgv:GridViewDataColumn>
                                <dxwgv:GridViewDataColumn FieldName="RoomControl" VisibleIndex="13">
                                    <settings autofiltercondition="Contains" />
                                </dxwgv:GridViewDataColumn>
                                <dxwgv:GridViewDataHyperLinkColumn FieldName="AgentLink" VisibleIndex="14">
                                    <propertieshyperlinkedit navigateurlformatstring="DeviceShell.aspx?Device={0}" textfield="Agent" />
                                </dxwgv:GridViewDataHyperLinkColumn>
                            </columns>
                            <settings showfilterrow="True" />
                        </X:GridView>
                    </controls>
                </X:TabPage>
                <X:TabPage Text="Servers" Name="Terminals">
                    <controls>
                        <D:Button ID="btnRefreshTerminals" runat="server" Text="Refresh terminals" Style="margin: 4px 0 8px 0;" OnClientClick="PageButtons.RefreshClients(); return false;" /><D:Label runat="server" ID="lblTerminalRefresh"></D:Label>

                        <table cellspacing="3">
                            <tr>
                                <td style="font-weight: bold; padding-right: 5px;">Legend:</td>
                                <td style="color: black; padding-right: 5px;">&#9632; OK</td>
                                <td style="color: blue; padding-right: 5px;">&#9632; Newer version</td>
                                <td style="color: orange; padding-right: 5px;">&#9632; Outdated</td>
                                <td style="color: red;">&#9632; Offline</td>
                            </tr>
                        </table>

                        <X:GridView ID="terminalGrid" ClientInstanceName="terminalGrid" runat="server" Width="100%" KeyFieldName="TerminalId">
                            <settingsbehavior autofilterrowinputdelay="350" />
                            <settingspager pagesize="250"></settingspager>
                            <settingsbehavior allowgroup="false" allowdragdrop="false" />
                            <columns>
                                <dxwgv:GridViewDataHyperLinkColumn FieldName="TerminalId" VisibleIndex="1" SortIndex="0" SortOrder="Ascending">
                                    <settings autofiltercondition="Contains" />
                                    <propertieshyperlinkedit navigateurlformatstring="Terminal.aspx?id={0}" target="_blank" textfield="TerminalId" />
                                </dxwgv:GridViewDataHyperLinkColumn>
                                <dxwgv:GridViewDataColumn FieldName="Name" VisibleIndex="2">
                                    <settings autofiltercondition="Contains" />
                                </dxwgv:GridViewDataColumn>
                                <dxwgv:GridViewDataColumn FieldName="LastRequest" VisibleIndex="3">
                                    <settings autofiltercondition="Contains" />
                                </dxwgv:GridViewDataColumn>
                                <dxwgv:GridViewDataColumn FieldName="Loaded" VisibleIndex="4" Width="70">
                                    <settings autofiltercondition="Contains" />
                                </dxwgv:GridViewDataColumn>
                                <dxwgv:GridViewDataColumn FieldName="LocalIp" VisibleIndex="5">
                                    <settings autofiltercondition="Contains" />
                                </dxwgv:GridViewDataColumn>
                                <dxwgv:GridViewDataColumn FieldName="ExternalIp" VisibleIndex="6">
                                    <settings autofiltercondition="Contains" />
                                </dxwgv:GridViewDataColumn>
                                <dxwgv:GridViewDataColumn FieldName="VersionTerminal" VisibleIndex="7">
                                    <settings autofiltercondition="Contains" />
                                </dxwgv:GridViewDataColumn>
                                <dxwgv:GridViewDataColumn FieldName="VersionST" VisibleIndex="8">
                                    <settings autofiltercondition="Contains" />
                                </dxwgv:GridViewDataColumn>
                                <dxwgv:GridViewDataColumn FieldName="VersionAgent" VisibleIndex="9">
                                    <settings autofiltercondition="Contains" />
                                </dxwgv:GridViewDataColumn>
                                <dxwgv:GridViewDataColumn FieldName="VersionOS" VisibleIndex="10">
                                    <settings autofiltercondition="Contains" />
                                </dxwgv:GridViewDataColumn>
                                <dxwgv:GridViewDataColumn FieldName="VersionMessaging" VisibleIndex="11">
                                    <settings autofiltercondition="Contains" />
                                </dxwgv:GridViewDataColumn>
                                <dxwgv:GridViewDataColumn FieldName="IsSupportTools" VisibleIndex="12">
                                    <settings autofiltercondition="Contains" />
                                </dxwgv:GridViewDataColumn>
                                <dxwgv:GridViewDataHyperLinkColumn FieldName="AgentLink" VisibleIndex="13">
                                    <propertieshyperlinkedit navigateurlformatstring="DeviceShell.aspx?Device={0}" textfield="Agent" />
                                </dxwgv:GridViewDataHyperLinkColumn>
                            </columns>
                            <settings showfilterrow="True" />
                        </X:GridView>
                    </controls>
                </X:TabPage>
                <X:TabPage Text="Sandbox Mode" Name="SandboxMode">
                    <controls>
                        <D:Button ID="btnRefreshSandboxClients" runat="server" Text="Refresh sandbox clients" Style="margin: 4px 0 8px 0;" OnClientClick="PageButtons.RefreshClients(); return false;" />

                        <X:PageControl Id="tabSandbox" runat="server" Width="100%">
                            <tabpages>
                                <X:TabPage Text="Sandbox Control" Name="SandboxControl">
                                    <controls>
                                        <table class="dataformV2">
                                            <tr>
                                                <td class="label">
                                                    <D:Label ID="lblAssignCompany" runat="server">Bedrijf</D:Label>
                                                </td>
                                                <td class="control">
                                                    <X:ComboBoxLLBLGenEntityCollection ID="cbCompanyConfig" EntityName="Company" AutoPostBack="True" runat="server" PreventEntityCollectionInitialization="true"></X:ComboBoxLLBLGenEntityCollection>
                                                </td>
                                                <td class="label" style="width: 40%">
                                                    <X:Button runat="server" ID="btnPushCompanyConfigToClient" Text="Stuur bedrijfs configuratie naar geselecteerde apparaten"></X:Button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="label">
                                                    <D:Label ID="Label1" runat="server">Deliverypointgroup</D:Label>
                                                </td>
                                                <td class="control">
                                                    <X:ComboBoxLLBLGenEntityCollection ID="cbCompanyDeliverypointgroup" EntityName="Deliverypointgroup" UseDataBinding="false" runat="server" PreventEntityCollectionInitialization="true"></X:ComboBoxLLBLGenEntityCollection>
                                                </td>
                                                <td class="label" style="width: 40%"></td>
                                            </tr>
                                        </table>
                                    </controls>
                                </X:TabPage>
                            </tabpages>
                        </X:PageControl>
                        <br />
                        <X:GridView ID="sandboxGrid" runat="server" Width="100%" KeyFieldName="DeviceId">
                            <settingsbehavior autofilterrowinputdelay="350" />
                            <settingspager pagesize="100"></settingspager>
                            <settingsbehavior allowgroup="false" allowdragdrop="false" />
                            <columns>
                                <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0">
                                    <headertemplate>
                                        <input type="checkbox" onclick="sandboxGrid.SelectAllRowsOnPage(this.checked);" title="Selecteer/deselecteer alle rijen op de pagina" />
                                    </headertemplate>
                                    <headerstyle horizontalalign="Center" />
                                </dxwgv:GridViewCommandColumn>
                                <dxwgv:GridViewDataHyperLinkColumn FieldName="DeviceId" VisibleIndex="1">
                                    <settings autofiltercondition="Contains" />
                                    <propertieshyperlinkedit navigateurlformatstring="~/Configuration/Device.aspx?id={0}" target="_blank" textfield="DeviceId" />
                                </dxwgv:GridViewDataHyperLinkColumn>
                                <dxwgv:GridViewDataColumn FieldName="MacAddress" VisibleIndex="2">
                                    <settings autofiltercondition="Contains" />
                                </dxwgv:GridViewDataColumn>
                                <dxwgv:GridViewDataColumn FieldName="LocalIp" VisibleIndex="3">
                                    <settings autofiltercondition="Contains" />
                                </dxwgv:GridViewDataColumn>
                                <dxwgv:GridViewDataColumn FieldName="ExternalIp" VisibleIndex="4">
                                    <settings autofiltercondition="Contains" />
                                </dxwgv:GridViewDataColumn>
                                <dxwgv:GridViewDataColumn FieldName="LastCompany" VisibleIndex="5">
                                    <settings autofiltercondition="Contains" />
                                </dxwgv:GridViewDataColumn>
                            </columns>
                            <settings showfilterrow="True" />
                        </X:GridView>

                    </controls>
                </X:TabPage>
            </TabPages>
        </X:PageControl>
    </div>
    <script src="../Scripts/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.signalR-2.1.1.min.js" type="text/javascript"></script>
    <script src="<%= WebEnvironmentHelper.GetMessagingUrl() %>/signalr/js" type="text/javascript"></script>
    <script type="text/javascript">

        var lblClientRefresh = document.getElementById('<%=lblClientRefresh.ClientID %>');
        var lblTerminalRefresh = document.getElementById('<%=lblTerminalRefresh.ClientID %>');

        (function (SignalRConnector, $, undefined) {
            SignalRConnector.Connect = function () {
                $.signalR.hub.start()
                    .done(function () {
                        console.log('Now connected, connection ID=' + $.connection.hub.id + ', transport = ' + $.connection.hub.transport.name);
                        PageMethods.GetAuthentication($.connection.hub.id, SignalRConnector.Authenticate);
                    })
                    .fail(function (err) {
                        console.log('Could not connect: ' + err);
                        lblClientRefresh.innerHTML = "  Failed to connect";
                    });
            };

            SignalRConnector.Authenticate = function (collection) {
                $.signalR.signalRInterface.server.authenticate(collection[0], collection[1], collection[2]);
            };

            SignalRConnector.SendMessage = function (message) {
                $.signalR.signalRInterface.server.receiveMessage(message);
            };

        }(window.SignalRConnector = window.SignalRConnector || {}, $));

        $(function () {
            $.signalR.signalRInterface.client.ReceiveMessage = function (message) {
                console.log("Received: " + message);
                var jsonObject = $.parseJSON(message);

                if (jsonObject.messageType == 17) {
                    if (jsonObject.isAuthenticated) {
                        if (jsonObject.isAuthenticated) {
                            console.log("Authenticated, sending ClientType");
                            PageMethods.NetmessageSetClientType(SignalRConnector.SendMessage, OnError);

                            if (!isPostBack) {
                                PageButtons.RefreshClients();
                            }
                        } else {
                            console.log("Failed to authenticate: " + jsonObject.authenticateErrorMessage);
                        }
                    }
                } else if (jsonObject.messageType == 9998) {
                    PageMethods.NetmessagePong(SignalRConnector.SendMessage, OnError);
                } else if (jsonObject.messageType == 27) {
                    console.log('Postbackkkkk');
                    __doPostBack("RefreshClients", message);
                } else if (jsonObject.messageType == 1000) {
                    console.log("Disconnected");
                    $.signalR.hub.reconnect();
                } else {
                    console.log(jsonObject.messageType);
                }
            };
            $.signalR.signalRInterface.client.MessageVerified = function (guid) { };

            $.signalR.hub.reconnect = function () {
                $.signalR.hub.stop();
                SignalRConnector.Connect();
            };

            // Configuration
            $.signalR.hub.url = "<%= WebEnvironmentHelper.GetMessagingUrl() %>/signalr";
            $.signalR.hub.qs = { "deviceIdentifier": "<%=CometConstants.CometIdentifierCms  + "_" + CmsSessionHelper.CurrentUser.UserId %>" };
            $.signalR.hub.error(function (error) {
                console.log('SignalR error: ' + error);
                $.signalR.hub.reconnect();
            });
            $.connection.json = window.JSON;
            // Connect
            SignalRConnector.Connect();
        });

        function OnError(error) {
            alert(error._message);
            console.log(error);
        }

        (function (PageButtons) {

            PageButtons.RefreshClients = function () {
                console.log("Refreshing");
                PageMethods.NetmessageGetConnectedClients(SignalRConnector.SendMessage, OnError);
                lblClientRefresh.innerHTML = "  Refreshing...";
                lblTerminalRefresh.innerHTML = "  Refreshing...";
            };

        }(window.PageButtons = window.PageButtons || {}));

    </script>
</asp:Content>
