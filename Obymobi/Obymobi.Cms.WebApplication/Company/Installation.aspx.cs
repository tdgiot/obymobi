﻿using System;
using System.Linq;
using Dionysos.Web;
using Dionysos.Web.UI;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.ObymobiCms.Company.SubPanels.LandingPagePanels;
using Obymobi.ObymobiCms.MasterPages;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Company
{
    public partial class Installation : PageLLBLGenEntity
    {
        private bool CanEditNotes => CmsSessionHelper.CurrentRole >= Role.Crave;

        #region Methods                

        private void LoadUserControls()
        {
            // Terminals
            TerminalPanel terminalPanel = (TerminalPanel)this.LoadControl("~/Company/SubPanels/LandingPagePanels/TerminalPanel.ascx");
            terminalPanel.Parent = this;
            this.plhTerminals.Controls.Add(terminalPanel);

            // Clients
            ClientPanel clientPanel = (ClientPanel)this.LoadControl("~/Company/SubPanels/LandingPagePanels/ClientPanel.ascx");
            clientPanel.Parent = this;
            this.plhClients.Controls.Add(clientPanel);

            // Deliverypointgroups
            DeliverypointgroupPanel deliverypointgroupPanel = (DeliverypointgroupPanel)this.LoadControl("~/Company/SubPanels/LandingPagePanels/DeliverypointgroupPanel.ascx");
            deliverypointgroupPanel.Parent = this;
            this.plhDeliverypointgroups.Controls.Add(deliverypointgroupPanel);
        }

        private void SetGui()
        {
            // Company            
            this.lblCompanyName.Text = string.Format("<a href=\"Company.aspx?id={0}\">{1}</a>", this.DataSourceAsCompanyEntity.CompanyId, this.DataSourceAsCompanyEntity.Name);
            this.lblCompanyId.Text = this.DataSourceAsCompanyEntity.CompanyId.ToString();            

            this.lblUsername.Text = this.DataSourceAsCompanyEntity.CompanyOwnerEntity.Username;
            this.lblPasswordDecrypted.Text = this.DataSourceAsCompanyEntity.CompanyOwnerEntity.DecryptedPassword;

            this.lnkClientStatus.NavigateUrl = "~/Vdara.ashx?companyId=" + this.DataSourceAsCompanyEntity.CompanyId;

            // API v1 / v2
            RelationCollection relation = new RelationCollection();
            relation.Add(ClientEntityBase.Relations.DeviceEntityUsingDeviceId);

            PredicateExpression totalFilter = new PredicateExpression();
            totalFilter.Add(ClientFields.CompanyId == this.DataSourceAsCompanyEntity.CompanyId);
            totalFilter.Add(DeviceFields.LastRequestUTC >= DateTime.UtcNow.AddMinutes(-3));

            IncludeFieldsList includeFieldsList = new IncludeFieldsList();
            includeFieldsList.Add(ClientFields.LastApiVersion);

            ClientCollection clients = new ClientCollection();
            clients.GetMulti(totalFilter, 0, null, relation, null, includeFieldsList, 0, 0);

            int clientCount = clients.Count;
            int clientsApiV1 = clients.Count(x => x.LastApiVersion <= 1);
            int clientsApiV1Percentage = clients.Count > 0 ? (int)(((double)clientsApiV1 / clientCount) * 100.0) : 0;
            int clientsApiV2 = clients.Count(x => x.LastApiVersion == 2);
            int clientsApiV2Percentage = clients.Count > 0 ? (int)((clientsApiV2 / (double)clientCount) * 100.0) : 0;

            this.lblClientsV1.Text = string.Format("{0}/{1} ({2}%)", clientsApiV1, clientCount, clientsApiV1Percentage);
            this.lblClientsV2.Text = string.Format("{0}/{1} ({2}%)", clientsApiV2, clientCount, clientsApiV2Percentage);

            this.pnlNotes.Visible = CanEditNotes;
            this.tbNotes.Text = DataSourceAsCompanyEntity.Notes;
        }

        private void SetWarnings()
        {
            if (this.DataSourceAsCompanyEntity.RouteId == null || this.DataSourceAsCompanyEntity.RouteId == 0)
                this.AddInformator(InformatorType.Warning, this.Translate("RouteNotSpecified", "Er is geen route gekozen."));
            if (this.DataSourceAsCompanyEntity.SupportpoolId == null || this.DataSourceAsCompanyEntity.SupportpoolId == 0)
                this.AddInformator(InformatorType.Warning, this.Translate("SupportpoolNotSpecified", "Er is geen support pool gekozen."));
            if (this.DataSourceAsCompanyEntity.UIModeCollection.Count == 0)
                this.AddInformator(InformatorType.Warning, this.Translate("NoUIModes", "Dit bedrijf heeft geen UI modes"));
            if (this.DataSourceAsCompanyEntity.DeliverypointgroupCollection.Count == 0)
                this.AddInformator(InformatorType.Warning, this.Translate("NoDeliverypointgroupsConfigured", "Er zijn nog geen deliverypointgroups aangemaakt voor dit bedrijf."));
            else
            {
                foreach (var deliverypointgroup in this.DataSourceAsCompanyEntity.DeliverypointgroupCollection)
                {
                    if (deliverypointgroup.RouteId.IsNullOrZero())
                        this.AddInformator(InformatorType.Warning, this.Translate("DeliverypointgroupWithoutRoute", "Er is geen order route geconfigureerd voor deliverypointgroup {0}"), deliverypointgroup.Name);                    

                    if (deliverypointgroup.UIModeId.IsNullOrZero())
                        this.AddInformator(InformatorType.Warning, this.Translate("DeliverypointgroupNoUIMode", "Er is geen UIMode ingesteld voor deliverypointgroup {0}"), deliverypointgroup.Name);
                }
            }
            if (this.DataSourceAsCompanyEntity.TerminalCollection.Count == 0)
                this.AddInformator(InformatorType.Warning, this.Translate("NoTerminals", "Dit bedrijf heeft geen terminals."));
            if (this.DataSourceAsCompanyEntity.AvailableOnObymobi && !this.DataSourceAsCompanyEntity.DeliverypointgroupCollection.Any(dpg => dpg.AvailableOnObymobi && dpg.TabletUIModeId.HasValue))
                this.AddInformator(Dionysos.Web.UI.InformatorType.Warning, this.Translate("Warning.NoDeliverypointGroupAvailableForMobile", "Dit bedrijf kan niet worden gebruikt op de App voor Tablets omdat er geen Tafelgroepen zijn geconfigureerd voor de App met Tablet UI mode."));
            if (this.DataSourceAsCompanyEntity.AvailableOnObymobi && !this.DataSourceAsCompanyEntity.DeliverypointgroupCollection.Any(dpg => dpg.AvailableOnObymobi && dpg.MobileUIModeId.HasValue))
                this.AddInformator(Dionysos.Web.UI.InformatorType.Warning, this.Translate("Warning.NoDeliverypointGroupAvailableForTablet", "Dit bedrijf kan niet worden gebruikt op de App voor Mobile omdat er geen Tafelgroepen zijn geconfigureerd voor de App met Mobile UI mode."));

            this.Validate();
        }

        private new void Refresh()
        {
            WebShortcuts.Redirect(Request.RawUrl);
        }

        #endregion

        #region Event handlers

        protected override void OnPreInit(EventArgs e)
        {
            this.EntityId = CmsSessionHelper.CurrentCompanyId;
            this.EntityName = "CompanyEntity";

            base.OnPreInit(e);
        }

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            this.DataSourceLoaded += Company_DataSourceLoaded;
            base.OnInit(e);
        }

        public override bool InitializeEntity()
        {
            if (this.EntityId > 0)
            {                
                var prefetch = new PrefetchPath(EntityType.CompanyEntity);
                IPrefetchPathElement prefetchCompanyCompanyOwner = prefetch.Add(CompanyEntityBase.PrefetchPathCompanyOwnerEntity);
                IPrefetchPathElement prefetchCompanyTerminal = prefetch.Add(CompanyEntityBase.PrefetchPathTerminalCollection);
                
                IPrefetchPathElement prefetchCompanyClient = prefetch.Add(CompanyEntityBase.PrefetchPathClientCollection);
                IPrefetchPathElement prefetchCompanyClientDevice = prefetchCompanyClient.SubPath.Add(ClientEntityBase.PrefetchPathDeviceEntity);
                IPrefetchPathElement prefetchCompanyClientLastDeliverypoint = prefetchCompanyClient.SubPath.Add(ClientEntityBase.PrefetchPathLastDeliverypointEntity);
                IPrefetchPathElement prefetchCompanyClientLastDeliverypointDeliverypointgroup = prefetchCompanyClientLastDeliverypoint.SubPath.Add(DeliverypointEntityBase.PrefetchPathDeliverypointgroupEntity);
                
                IPrefetchPathElement prefetchCompanyDeliverypointgroup = prefetch.Add(CompanyEntityBase.PrefetchPathDeliverypointgroupCollection);
                IPrefetchPathElement prefetchCompanyDeliverypointgroupMenu = prefetchCompanyDeliverypointgroup.SubPath.Add(DeliverypointgroupEntityBase.PrefetchPathMenuEntity);
                IPrefetchPathElement prefetchCompanyDeliverypointgroupRoute = prefetchCompanyDeliverypointgroup.SubPath.Add(DeliverypointgroupEntityBase.PrefetchPathRouteEntity);

                this.DataSource = new CompanyEntity(this.EntityId, prefetch);
            }

            return base.InitializeEntity();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Master is MasterPageEntity masterPage)
            {
                masterPage.ToolBar.SaveButton.Visible = CanEditNotes;
                masterPage.ToolBar.SaveAndGoButton.Visible = false;
                masterPage.ToolBar.SaveAndNewButton.Visible = false;
                masterPage.ToolBar.CancelButton.Visible = false;
                masterPage.ToolBar.DeleteButton.Visible = false;
            }

            this.HookupEvents();

            if (!this.IsPostBack)
                this.SetWarnings();            
        }

        private void Company_DataSourceLoaded(object sender)
        {
            this.SetGui();
        }

        private void HookupEvents()
        {
            this.btnRefresh.Click += btnRefresh_Click;
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            this.Refresh();
        }

        public override bool Save()
        {
            CompanyEntity company = new CompanyEntity(DataSourceAsCompanyEntity.CompanyId);
            company.Notes = tbNotes.Text;
            company.IsNew = false;

            return company.Save();
        }

        #endregion

        #region Properties

        public CompanyEntity DataSourceAsCompanyEntity
        {
            get { return this.DataSource as CompanyEntity; }
        }
        
        #endregion
    }
}
