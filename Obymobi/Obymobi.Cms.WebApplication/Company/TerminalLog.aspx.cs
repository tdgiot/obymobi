﻿using System;
using System.Net;
using Dionysos;
using Obymobi.Data.EntityClasses;
using Obymobi.ObymobiCms.MasterPages;
using System.IO;
using System.Web;

namespace Obymobi.ObymobiCms.Company
{
    public partial class TerminalLog : Dionysos.Web.UI.PageLLBLGenEntity
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Hide toolbar buttons, only show cancel (aka. go back)
            if (this.Master is MasterPageEntity master)
            {
                master.ToolBar.Visible = false;
            }

            this.btBack.Attributes.Add("onClick", "javascript:history.back(); return false;");

            this.lblCreatedValue.Text = (this.DataSourceAsTerminalLogEntity.CreatedUTC.HasValue ? this.DataSourceAsTerminalLogEntity.CreatedUTC.Value.UtcToLocalTime().ToString() : "Onbekend");
            this.lblTypeValue.Text = this.DataSourceAsTerminalLogEntity.TypeName;
            this.lblStatusValue.Text = this.DataSourceAsTerminalLogEntity.Status;
            this.lblMessageValue.Text = this.DataSourceAsTerminalLogEntity.Message;

            if (DataSourceAsTerminalLogEntity.TerminalLogFileId > 0)
            {
                this.phlNormalLog.Visible = false;
                this.phlShippedLog.Visible = true;

                this.lblLogMessage.Value = DataSourceAsTerminalLogEntity.Message;

                this.btnDownloadLog.Click += btnDownloadLog_Click;

                try
                {
                    using (var client = new WebClient())
                    {
                        string url = DataSourceAsTerminalLogEntity.TerminalLogFileEntity.Message;

                        this.tbMessage.Text = client.DownloadString(url);
                    }
                }
                catch
                {
                    this.tbMessage.Visible = false;
                }
            }
        }

        void btnDownloadLog_Click(object sender, EventArgs e)
        {
            Uri uriResult;
            if (Uri.TryCreate(this.DataSourceAsTerminalLogEntity.TerminalLogFileEntity.Message, UriKind.Absolute, out uriResult) &&
                (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps))
            {
                DownloadExternalFile(uriResult.AbsoluteUri, uriResult.Segments[uriResult.Segments.Length - 1]);
            }
            else
            {
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();
                Response.AddHeader("content-disposition", "attachment;filename=" + this.DataSourceAsTerminalLogEntity.Message + ".txt");
                Response.ContentType = "text/plain";
                Response.Write(this.DataSourceAsTerminalLogEntity.TerminalLogFileEntity.Message);
                Response.End();
            }
        }

        void DownloadExternalFile(string url, string fileName)
        {
            Stream stream = null;

            const int bytesToRead = 10000;
            byte[] buffer = new Byte[bytesToRead];

            try
            {
                //Create a WebRequest to get the file
                HttpWebRequest fileReq = (HttpWebRequest)WebRequest.Create(url);
                HttpWebResponse fileResp = (HttpWebResponse)fileReq.GetResponse();

                if (fileReq.ContentLength > 0)
                    fileResp.ContentLength = fileReq.ContentLength;

                stream = fileResp.GetResponseStream();
                if (stream == null)
                    return;

                var resp = HttpContext.Current.Response;

                resp.ContentType = "application/octet-stream";
                resp.AddHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
                resp.AddHeader("Content-Length", fileResp.ContentLength.ToString());

                int length;
                do
                {
                    if (resp.IsClientConnected)
                    {
                        length = stream.Read(buffer, 0, bytesToRead);

                        resp.OutputStream.Write(buffer, 0, length);
                        resp.Flush();

                        buffer = new Byte[bytesToRead];
                    }
                    else
                    {
                        length = -1;
                    }
                } while (length > 0);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Close();
                }
            }
        }

        #region Properties


        /// <summary>
        /// Return the page's datasource as a SurveyQuestionEntity
        /// </summary>
        public TerminalLogEntity DataSourceAsTerminalLogEntity
        {
            get
            {
                return this.DataSource as TerminalLogEntity;
            }
        }

        #endregion
    }
}