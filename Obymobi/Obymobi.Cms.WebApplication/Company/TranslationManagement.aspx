﻿<%@ Page Title="Translation management" Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.TranslationManagement" ValidateRequest="false" Codebehind="TranslationManagement.aspx.cs" %>
<%@ Register Assembly="DevExpress.Web.v20.1" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitleHolder" Runat="Server">
    Translation management
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhToolbarHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhContentHolder" Runat="Server">
<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true"/>
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Export" Name="Export">
				<controls>
					<table class="dataformV2">
						<tr>
							<td class="label">
                                <D:Label runat="server" id="lblExportCompany">Company</D:Label>
							</td>
							<td class="control">
                                <D:Label runat="server" id="lblExportCompanyValue" LocalizeText="false"></D:Label>
							</td>
							<td class="label">
                                <D:Label runat="server" id="lblExportMenu">Menu</D:Label>
							</td>
							<td class="control">
								<X:ComboBoxLLBLGenEntityCollection ID="ddlMenuId" runat="server" IncrementalFilteringMode="Contains" EntityName="Menu" ValueField="MenuId" TextField="Name" PreventEntityCollectionInitialization="true" UseDataBinding="true" />
							</td>
                        </tr>
						<tr>
							<td class="label">
                                <D:Label runat="server" id="lblSourceCulture">Source culture</D:Label>
							</td>
							<td class="control">
                                <X:ComboBox ID="cbSourceCulture" runat="server"></X:ComboBox>
								<div class="btn-group">
									<D:Button runat="server" ID="btnExportCompanyTranslations" Text="Export company translations" />
									<D:Button runat="server" ID="btnExportMenuTranslations" Text="Export menu translations" />
									<D:Button runat="server" ID="btnExportGenericTranslations" Text="Export generic translations" />
									<D:Button runat="server" ID="btnExportAppLessTranslations" Text="Export AppLess translations" Visible="false"/>
								</div>
							</td>
							<td class="label">
                                <D:Label runat="server" id="lblDestinationCultures">Destination culture(s)</D:Label>
							</td>
							<td class="control" rowspan="2">
                                <X:ListBox ID="lbDestinationCultures" runat="server" SelectionMode="CheckColumn" ClientIDMode="Static" />
							</td>
                        </tr>
                    </table>
				    <D:PlaceHolder ID="plhResult" Visible="true" runat="server"/>
                </controls>
            </X:TabPage>
            <X:TabPage Text="Import" Name="Import">
				<controls>
					<table class="dataformV2">
						<tr>
							<td class="label">
                                <D:Label runat="server" id="lblImportCompany">Company</D:Label>
							</td>
							<td class="control">
                                <D:Label runat="server" id="lblImportCompanyValue" LocalizeText="false"></D:Label>
							</td>
							<td class="label">
                                &nbsp;
							</td>
							<td class="control">
                                &nbsp;
							</td>
                        </tr>
						<tr>
							<td class="label">
								<D:Label runat="server" id="lblImportFile">Importeer bestand</D:Label>
							</td>
							<td class="control">
                                <dx:ASPxUploadControl runat="server" ID="fuImportFile" ClientInstanceName="fuImportFile" ShowProgressPanel="True" Size="45" OnFileUploadComplete="fuImportFile_FileUploadComplete" FileUploadMode="OnPageLoad" />
							</td>
							<td class="label">
								&nbsp;
							</td>
							<td class="control">
                                &nbsp;
							</td>
                        </tr>
                        <tr>
							<td class="label">
							</td>
                            <td class="control">
                                <D:Button runat="server" ID="btnImport" Text="Upload en importeer bestand" />
                            </td>
                            <td class="label">
							</td>
                            <td class="control">
                            </td>
                        </tr>

                            <tr>
                                <td class="label">
                                    <D:Label runat="server" id="lblResult">Result</D:Label>
							    </td>
                                <td class="control" colspan="3">
                                    <D:LabelTextOnly  runat="server" ID="lblResultValue" LocalizeText="False"/>
                                </td>
                            </tr>
                    </table>
                </controls>
            </X:TabPage>
        </TabPages>
    </X:PageControl>
</div>
</asp:Content>
