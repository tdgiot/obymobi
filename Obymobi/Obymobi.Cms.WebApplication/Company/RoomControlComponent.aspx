﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.RoomControlComponent" Codebehind="RoomControlComponent.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
				    <D:PlaceHolder runat="server">
					<table class="dataformV2">
						<tr>
						    <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblName">Name</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbName" runat="server"></D:TextBoxString>
							</td>			
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblType">Type</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
                                <X:ComboBoxInt ID="cbType" runat="server" IsRequired="true" UseDataBinding="true" Enabled="false"></X:ComboBoxInt>
							</td>				
					    </tr>
                        <tr>
                            <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblNameSystem">System name</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbNameSystem" runat="server" IsRequired="true"></D:TextBoxString>
							</td>				
                            <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblSortOrder">Sort order</D:LabelEntityFieldInfo>
							</td>
                            <td class="control">
								<D:TextBoxInt ID="tbSortOrder" runat="server"></D:TextBoxInt>
							</td>
                        </tr>    
                        <tr>
                            <td class="label">
                            </td>
                            <td class="control">
                            </td>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblVisible">Visible</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <D:CheckBox runat="server" ID="cbVisible" />
                            </td>                            
                        </tr>          
                        <D:PlaceHolder runat="server" ID="plhBlind" Visible="false">
                            <tr>
                                <td class="label">
                                    <D:Label runat="server" id="lblOpenBlindScene">Open blind scene</D:Label>
                                </td>
                                <td class="control">
                                    <D:TextBoxString ID="tbOpenBlindScene" runat="server"></D:TextBoxString>
                                </td>
                                <td class="label">
                                    <D:Label runat="server" id="lblCloseBlindScene">Close blind scene</D:Label>
                                </td>
                                <td class="control">
                                    <D:TextBoxString ID="tbCloseBlindScene" runat="server"></D:TextBoxString>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:Label runat="server" id="lblStopBlindScene">Stop blind scene</D:Label>
                                </td>
                                <td class="control">
                                    <D:TextBoxString ID="tbStopBlindScene" runat="server"></D:TextBoxString>
                                </td>
                                <td class="label">
                                    <D:Label runat="server" id="lvlIsCurtain">Use as curtain</D:Label>
                                </td>
                                <td class="control">
                                    <D:CheckBox ID="cbIsCurtain" runat="server"></D:CheckBox>
                                </td>
                            </tr>
                        </D:PlaceHolder>     
                        <D:PlaceHolder runat="server" ID="plhLightingScene" Visible="false">
                            <tr>
                                <td class="label">
					               <D:Label runat="server" id="lblEnableDeactiveButton">Enable deactivate button</D:Label>
					            </td>
					            <td class="control">
					               <D:CheckBox runat="server" ID="cbEnableDeactivateButton"/>
					            </td>
                            </tr>
                        </D:PlaceHolder>
					    <D:PlaceHolder runat="server" ID="plhLight" Visible="false">
					        <tr>
					            
					            <td class="label">
					                <D:Label runat="server" id="lblToggleLight">Is Toggle Light</D:Label>
					            </td>
					            <td class="control">
                                    <D:CheckBox runat="server" ID="cbToggleLight"/>
					            </td>
					            <td class="label">
					                <D:Label runat="server" id="lblDeviceAddress">Device address</D:Label>
					            </td>
					            <td class="control">
					                <D:TextBoxString ID="tbDeviceAddress" runat="server"></D:TextBoxString>
					            </td>
					        </tr>
					        <tr>
					            
					            <td class="label">
					                <D:Label runat="server" id="lblLightOnMainController">Light connected on main controller</D:Label>
					            </td>
					            <td class="control">
					                <D:CheckBox runat="server" ID="cbLightOnMainController"/>
					            </td>					            
					        </tr>
					    </D:PlaceHolder>  
                    </table>	
                    <D:Panel ID="pnlConfiguration" runat="server" GroupingText="Configuration" Visible="false">
                        <table class="dataformV2">
                            <tr>
                                <td class="label">
                                    <D:Label runat="server" id="lblModes">HVAC Modes</D:Label>
                                </td>
                                <td class="control">
                                    <D:CheckBoxList runat="server" id="cblModes">
                                        <asp:ListItem value="Off">Off</asp:ListItem>
                                        <asp:ListItem value="Cool">Cool</asp:ListItem>
                                        <asp:ListItem value="Heat">Heat</asp:ListItem>
                                        <asp:ListItem value="Auto">Auto</asp:ListItem>
                                    </D:CheckBoxList>
                                </td>
                                <td class="label">
                                    <D:Label runat="server" id="lblFanModes">Fan Modes</D:Label>
                                </td>
                                <td class="control">
                                    <D:CheckBoxList runat="server" id="cblFanModes">
                                        <asp:ListItem value="Auto">Fan speed is set automatically</asp:ListItem>
                                    </D:CheckBoxList>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:Label runat="server" id="lblShowCurrentTemp">Show current temperature</D:Label>
                                </td>
                                <td class="control">
                                    <D:CheckBox runat="server" ID="cbShowCurrentTemp" />
                                </td>
                                <td class="label">
                                    <D:Label runat="server" id="lblHvacOnMainController">HVAC connected on main controller</D:Label>
                                </td>
                                <td class="control">
                                    <D:CheckBox runat="server" ID="cbHvacOnMainController"/>
                                </td>
                            </tr>
                        </table>
                    </D:Panel>
                    <D:Panel ID="pnlInfraRed" runat="server" GroupingText="Infra-red">
                        <table class="dataformV2">
                            <tr>
                                <td class="label">
                                    <D:Label runat="server" id="lblInfraredConfigurationId">Infra-red configuration</D:Label>
                                </td>
                                <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlInfraredConfigurationId" IncrementalFilteringMode="StartsWith" EntityName="InfraredConfiguration" TextField="Name" ValueField="InfraredConfigurationId" />
                                </td>
                                <td class="label">
                                </td>
                                <td class="control">
                                </td>
                            </tr>
                        </table>
                    </D:Panel>
                    </D:PlaceHolder>
				</Controls>
			</X:TabPage>	
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>

