﻿using System;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Company
{
    public partial class Survey : Dionysos.Web.UI.PageLLBLGenEntity
	{
        #region Methods

        protected override void OnInit(EventArgs e)
		{
			this.LoadUserControls();
            this.DataSourceLoaded += new Dionysos.Delegates.Web.DataSourceLoadedHandler(Survey_DataSourceLoaded);
			base.OnInit(e);

			// Read only if we have any answers
			if (this.PageMode == Dionysos.Web.PageMode.Edit && this.HasResults())
			{
				this.AddInformatorInfo(this.Translate("InformatorReadOnlyAlreadyHasResults", "Er zijn al antwoorden binnen, deze Enquête kan niet meer worden veranderd."));
			}
		}

        private void LoadUserControls()
        {
            // Add images tab
            this.tabsMain.AddTabPage("Afbeeldingen", "Media", "~/Generic/UserControls/MediaCollection.ascx");
        }

		bool HasResults()
		{
			RelationCollection joins = new RelationCollection();
			joins.Add(SurveyResultEntity.Relations.SurveyAnswerEntityUsingSurveyAnswerId);
			joins.Add(SurveyAnswerEntity.Relations.SurveyQuestionEntityUsingSurveyQuestionId);
            joins.Add(SurveyQuestionEntity.Relations.SurveyPageEntityUsingSurveyPageId);

			PredicateExpression filter = new PredicateExpression();
			filter.Add(SurveyPageFields.SurveyId == this.EntityId);

			SurveyResultCollection srs = new SurveyResultCollection();
			return srs.GetDbCount(filter, joins) > 0;
		}

        public override bool Save()
        {
            if (this.PageMode == Dionysos.Web.PageMode.Add)
                this.DataSourceAsSurveyEntity.CompanyId = CmsSessionHelper.CurrentCompanyId;

            return base.Save();            
        }

		#endregion

		#region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            this.HookUpEvents();
        }

        private void HookUpEvents()
        {
            this.btShowResult.Click += new EventHandler(btShowResult_Click);
        }

        void btShowResult_Click(object sender, EventArgs e)
        {
            string url = ResolveUrl(string.Format("~/Analytics/SurveyResult.aspx?id={0}", this.DataSourceAsSurveyEntity.SurveyId));
            Dionysos.Web.WebShortcuts.ResponseRedirect(url, true);
        }

        void Survey_DataSourceLoaded(object sender)
        {
            // Render the pages
            this.tabsMain.AddTabPage("Pagina's", "SurveyPagePanel", "~/Company/SubPanels/SurveyPagePanel.ascx");
        }

		#endregion

		#region Properties


		/// <summary>
		/// Return the page's datasource as a SurveyEntity
		/// </summary>
		public SurveyEntity DataSourceAsSurveyEntity
		{
			get
			{
				return this.DataSource as SurveyEntity;
			}
		}

		#endregion
	}
}
