﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.EntityClasses;

namespace Obymobi.ObymobiCms.Company
{
    public partial class CompanyOwners : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "CompanyOwner";
            this.EntityPageUrl = "~/Company/CompanyOwner.aspx";
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            LLBLGenProDataSource datasource = this.DataSource as LLBLGenProDataSource;
            if (datasource != null)
            {
                RelationCollection relations = new RelationCollection(CompanyEntity.Relations.CompanyOwnerEntityUsingCompanyOwnerId);
                PredicateExpression filter = new PredicateExpression(CompanyFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

                datasource.RelationsToUse = relations;
                datasource.FilterToUse = filter;
            }

            ((MasterPages.MasterPageEntityCollection)this.Master).ToolBar.AddButton.Visible = false;
        }

        #endregion
    }
}