﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.CompanyBrand" Codebehind="CompanyBrand.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Standaard" Name="Generic">
				<Controls>
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblCompany">Company</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
                                <D:HyperLink runat="server" ID="hlCompany"></D:HyperLink>
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblDecryptedPassword">Brand</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
                                <D:HyperLink runat="server" ID="hlBrand" LocalizeText="false" Target="_blank"></D:HyperLink>
							</td>
					    </tr>
						<tr>
							<td class="label">
							    <D:LabelEntityFieldInfo runat="server" id="lblPriority">Priority</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
							    <D:TextBoxInt ID="tbPriority" runat="server" ></D:TextBoxInt>
							</td>
							<td class="label">
								
							</td>
							<td class="control">
                                
							</td>
					    </tr>
					 </table>			
				</Controls>
			</X:TabPage>
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>

