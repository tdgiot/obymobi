﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.StationList" Codebehind="StationList.aspx.cs" %>
<%@ Register TagPrefix="dxwtl" Namespace="DevExpress.Web.ASPxTreeList" Assembly="DevExpress.Web.ASPxTreeList.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Reference VirtualPath="~/Company/SubPanels/StationCollection.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">    
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<controls>
                    <D:PlaceHolder runat="server" ID="plhStationList">
                        <table class="dataformV2">
						    <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblName">Name</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
							    </td>	  
                                <td class="label">
                                
                                </td>
                                <td class="control">
                                
                                </td>
						    </tr>    
                            <D:PlaceHolder ID="plhStations" runat="server" Visible="false">
                            <tr>
                                <td class="label">
                                    <D:Label runat="server" id="lblStations">Stations</D:Label>
                                </td>
                                <td class="control" colspan="3">
                                    <div style="width: 736px;">    
                                        <D:Button runat="server" ID="btDeleteStationsTop" Text="Delete selected station(s)" PreSubmitWarning="Are you sure you want to delete these items?" Style="float:right; margin-bottom:10px" />
                                        <dxwtl:ASPxTreeList ID="tlStations" ClientInstanceName="tlStations" runat="server" EnableViewState="false" AutoGenerateColumns="False">
                                            <SettingsSelection Enabled="true" Recursive="false" />                                                
                                        </dxwtl:ASPxTreeList>    
                                        <D:Button runat="server" ID="btDeleteStationsBottom" Text="Delete selected station(s)" PreSubmitWarning="Are you sure you want to delete these items?" Style="float:right; margin-top:10px" />
                                    </div>
                                </td>
                            </tr>
                            </D:PlaceHolder>
                        </table>
                    </D:PlaceHolder>					
                </controls>                    
            </X:TabPage>
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>