﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;

namespace Obymobi.ObymobiCms.Company
{
    public partial class CompanyOwner : Dionysos.Web.UI.PageLLBLGenEntity
    {
        #region Methods

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            base.OnInit(e);
        }

        private void LoadUserControls()
        {
            if (CmsSessionHelper.CurrentUser.Role == Role.GodMode)
            {
                this.tbDecryptedPassword.ReadOnly = false;
                this.tbUsername.ReadOnly = false;
            }
        }

        public override bool Save()
        {
            if (CmsSessionHelper.CurrentUser.Role == Role.GodMode)
            {
                if (this.tbUsername.Value.Length > 0)
                {
                    this.DataSourceAsCompanyOwnerEntity.Username = this.tbUsername.Value;
                }

                if (this.tbDecryptedPassword.Value.Length > 0)
                {
                    string password = Dionysos.Security.Cryptography.Cryptographer.EncryptStringUsingRijndael(this.tbDecryptedPassword.Value);
                    this.DataSourceAsCompanyOwnerEntity.Password = password;
                }

                return base.Save();
            }

            return true;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the data source as company owner entity.
        /// </summary>
        public CompanyOwnerEntity DataSourceAsCompanyOwnerEntity
        {
            get
            {
                return this.DataSource as CompanyOwnerEntity;
            }
        }

        #endregion

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.PageMode == Dionysos.Web.PageMode.Add)
                throw new ApplicationException(this.Translate("company_owners_should_be_added_via_add_company_page", "Company owners should be added via the 'Add company' page."));
            else
            {
                //this.lblUsernameValue.Text = this.DataSourceAsCompanyOwnerEntity.Username;
                //this.lblPasswordValue.Text = this.DataSourceAsCompanyOwnerEntity.Password;
                this.tbHashedPassword.Value = this.DataSourceAsCompanyOwnerEntity.Password;
            }
        }

        #endregion
    }
}