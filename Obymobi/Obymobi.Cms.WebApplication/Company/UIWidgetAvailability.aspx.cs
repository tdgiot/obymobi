﻿using System;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.ObymobiCms.Company
{
	public partial class UIWidgetAvailability : Dionysos.Web.UI.PageLLBLGenEntity
    {
		#region Methods

		protected override void OnInit(EventArgs e)
		{
            this.DataSourceLoaded += this.UIWidgetAvailability_DataSourceLoaded;
			base.OnInit(e);			
		}

	    private void SetGui()
	    {
	        this.tbName.Text = this.DataSourceAsUIWidgetAvailabilityEntity.AvailabilityEntity.Name;
            this.tbStatusText.Text = this.DataSourceAsUIWidgetAvailabilityEntity.AvailabilityEntity.Status;
	    }        

		#endregion

		#region Event Handlers

	    protected void Page_Load(object sender, EventArgs e)
		{
	        if (this.PageMode != Dionysos.Web.PageMode.Add)
	        {
                this.Validate();
	        }
		}

		private void UIWidgetAvailability_DataSourceLoaded(object sender)
		{
			this.SetGui();
		}

        public override bool Save()
        {
            this.DataSourceAsUIWidgetAvailabilityEntity.AvailabilityEntity.StatusText = this.tbStatusText.Text;
            this.DataSourceAsUIWidgetAvailabilityEntity.AvailabilityEntity.Name = this.tbName.Text;
            this.DataSourceAsUIWidgetAvailabilityEntity.AvailabilityEntity.Save();

            if (base.Save())
            {
                CustomTextHelper.UpdateCustomTexts(this.DataSourceAsUIWidgetAvailabilityEntity.AvailabilityEntity, this.DataSourceAsUIWidgetAvailabilityEntity.AvailabilityEntity.CompanyEntity);
                return true;
            }

            return false;
        }

		#endregion

		#region Properties

		public UIWidgetAvailabilityEntity DataSourceAsUIWidgetAvailabilityEntity
		{
			get
			{
                return this.DataSource as UIWidgetAvailabilityEntity;
			}
		}

		#endregion
    }
}
