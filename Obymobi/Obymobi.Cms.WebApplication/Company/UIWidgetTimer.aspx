﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.UIWidgetTimer" Codebehind="UIWidgetTimer.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
                    <D:PlaceHolder runat="server">
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblCountToTime">Time</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
							    <X:TimeEdit runat="server" ID="teCountToTime"></X:TimeEdit>
							</td>
							<td class="label">
							    <D:LabelEntityFieldInfo runat="server" id="lblCountToDate">Date</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
							    <X:DateEdit runat="server" ID="deCountToDate"></X:DateEdit>
							</td>
					    </tr>	
					 </table>	
                    </D:PlaceHolder>
				</Controls>
			</X:TabPage>	
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>

