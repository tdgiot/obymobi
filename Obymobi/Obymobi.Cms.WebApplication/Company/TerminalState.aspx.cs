﻿using System;
using Dionysos;
using Obymobi.Data.EntityClasses;
using Obymobi.ObymobiCms.MasterPages;

namespace Obymobi.ObymobiCms.Company
{
    public partial class TerminalState : Dionysos.Web.UI.PageLLBLGenEntity
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Hide toolbar buttons, only show cancel (aka. go back)
            var master = this.Master as MasterPageEntity;
            if (master != null)
            {
                master.ToolBar.Visible = false;
            }

            this.btBack.Attributes.Add("onClick", "javascript:history.back(); return false;");

            this.lblCreatedValue.Text = (this.DataSourceAsTerminalStateEntity.CreatedUTC.HasValue ? this.DataSourceAsTerminalStateEntity.CreatedUTC.Value.UtcToLocalTime().ToString() : "Unknown");
            this.lblOnlineValue.Text = this.DataSourceAsTerminalStateEntity.Online.ToString();
            this.lblOperationModeValue.Text = this.DataSourceAsTerminalStateEntity.OperationModeAsEnum.ToString();
            this.lblLastBatteryLevelValue.Text = (this.DataSourceAsTerminalStateEntity.LastBatteryLevel.HasValue ? this.DataSourceAsTerminalStateEntity.LastBatteryLevel.Value.ToString() : "Unknown");
            this.lblLastIsChargingValue.Text = this.DataSourceAsTerminalStateEntity.LastIsCharging.ToString();
            this.tbMessage.Text = this.DataSourceAsTerminalStateEntity.Message;
        }

        #region Properties

        public TerminalStateEntity DataSourceAsTerminalStateEntity
        {
            get
            {
                return this.DataSource as TerminalStateEntity;
            }
        }

        #endregion
    }
}