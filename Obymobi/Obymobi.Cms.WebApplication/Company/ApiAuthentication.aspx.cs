﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dionysos;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using System.Collections;
using Dionysos.Web.UI.WebControls;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;

namespace Obymobi.ObymobiCms.Company
{
    public partial class ApiAuthentication : Obymobi.ObymobiCms.UI.PageLLBLGenEntityCms
    {
        protected override void OnInit(EventArgs e)
        {
            this.DataSourceLoaded += ApiAuthentication_DataSourceLoaded;
            base.OnInit(e);   
        }

        private void ApiAuthentication_DataSourceLoaded(object sender)
        {
            this.LoadUserControls();
        }

        private void LoadUserControls()
        {
            if (this.DataSourceAsApiAuthenticationEntity.Type == ApiAuthenticationType.Google)
            {
                var googlePanel = this.LoadControl("~/Company/SubPanels/ApiAuthenticationPanels/GoogleSettingsPanel.ascx");
                this.plhSettingsPanel.Controls.Add(googlePanel);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public ApiAuthenticationEntity DataSourceAsApiAuthenticationEntity
        {
            get
            {
                return this.DataSource as ApiAuthenticationEntity;
            }
        }
    }
}