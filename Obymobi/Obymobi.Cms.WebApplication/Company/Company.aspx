<%@ Page Title="" Async="true" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.Company" CodeBehind="Company.aspx.cs" %>

<%@ Reference VirtualPath="~/Company/SubPanels/CompanyCulturePanel.ascx" %>
<%@ Reference VirtualPath="~/Generic/UserControls/CustomTextCollection.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" runat="Server">
    <D:Button runat="server" ID="btnArchive" Text="Remove" OnClick="ToggleArchived" LocalizeText="False" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" runat="Server">
    <div>
        <X:PageControl ID="tabsMain" runat="server" Width="100%">
            <TabPages>
                <X:TabPage Text="Generic" Name="Generic">
                    <controls>
                        <D:Panel ID="pnlContactDetails" runat="server" GroupingText="Contact gegevens">
                            <table class="dataformV2">
                                <tr>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblCountryId">Land</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <X:ComboBox runat="server" ID="ddlCountryId" IsRequired="true" TextField="Name" ValueField="CodeAlpha3"></X:ComboBox>
                                    </td>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblAddressline1">Adres 1</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString ID="tbAddressline1" runat="server"></D:TextBoxString>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblZipcode">Postcode</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString ID="tbZipcode" runat="server" IsRequired="false"></D:TextBoxString>
                                    </td>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblAddressline2">Adres 2</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString ID="tbAddressline2" runat="server"></D:TextBoxString>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblCity">Plaats</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString ID="tbCity" runat="server"></D:TextBoxString>
                                    </td>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblAddressline3">Adres 3</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString ID="tbAddressline3" runat="server"></D:TextBoxString>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        <D:LinkButton ID="lbLatitudeLongitude" runat="server" Text="Latitude / longitude" LocalizeText="false" ForeColor="Black" />
                                    </td>
                                    <td class="control">
                                        <D:TextBoxDouble ID="tbLatitude" runat="server" ReadOnly="True" Width="100px"></D:TextBoxDouble>
                                        &nbsp;<D:TextBoxDouble ID="tbLongitude" runat="server" ReadOnly="True" Width="100px"></D:TextBoxDouble>&nbsp;<D:PlaceHolder ID="plhMap" runat="server"></D:PlaceHolder>
                                    </td>
                                    <td class="label">&nbsp;
                                    </td>
                                    <td class="control">&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblTelephone">Telefoon</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString ID="tbTelephone" runat="server"></D:TextBoxString>
                                    </td>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblFax">Fax</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString ID="tbFax" runat="server"></D:TextBoxString>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblWebsite">Website</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString ID="tbWebsite" runat="server"></D:TextBoxString>
                                    </td>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblEmail">E-mail</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <D:TextBox ID="tbEmail" runat="server"></D:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </D:Panel>
                        <D:Panel ID="pnlSettings" runat="server" GroupingText="Instellingen">
                            <table class="dataformV2">
                                <tr>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblName">Naam</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
                                    </td>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblSystemType">Systeem</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <X:ComboBoxEnum ID="cbSystemType" runat="server" DisplayEmptyItem="false" Type="Obymobi.Enums.SystemType, Obymobi"></X:ComboBoxEnum>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        <D:Label ID="lblCulture" runat="server" Text="Default culture" />
                                    </td>
                                    <td class="control">
                                        <X:ComboBox runat="server" ID="ddlCultureCode" IsRequired="true" TextField="NameAndCultureCode" ValueField="Code"></X:ComboBox>
                                    </td>
                                    <td class="label">
                                        <D:Label ID="lblCurrency" runat="server" Text="Default currency" />
                                    </td>
                                    <td class="control">
                                        <X:ComboBox runat="server" ID="ddlCurrencyCode" IsRequired="true" TextField="Name" ValueField="Code"></X:ComboBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        <D:Label runat="server" ID="lblTimeZoneId">Timezone</D:Label>
                                    </td>
                                    <td class="control">
                                        <X:ComboBox runat="server" ID="ddlTimeZoneOlsonId" IsRequired="true" TextField="Name" ValueField="OlsonTimeZoneId"></X:ComboBox>
                                    </td>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblCompanyOwnerId">Company owner</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlCompanyOwnerId" CssClass="input" UseDataBinding="true" EntityName="CompanyOwner" IsRequired="true"></X:ComboBoxLLBLGenEntityCollection>
                                    </td>
                                </tr>
                                <D:PlaceHolder ID="plhForAdministrator" runat="server">
                                    <tr>
                                        <td class="label">
                                            <D:LabelEntityFieldInfo runat="server" ID="lblSupportpoolId">Ondersteuning</D:LabelEntityFieldInfo>
                                        </td>
                                        <td class="control">
                                            <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlSupportpoolId" CssClass="input" UseDataBinding="true" EntityName="Supportpool"></X:ComboBoxLLBLGenEntityCollection>
                                        </td>
                                        <td class="label">
                                            <D:LabelEntityFieldInfo runat="server" ID="lblCode">Oby-code</D:LabelEntityFieldInfo>
                                        </td>
                                        <td class="control">
                                            <D:TextBoxString ID="tbObyCode" runat="server"></D:TextBoxString>
                                        </td>
                                    </tr>
                                </D:PlaceHolder>
                                <tr>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblRouteId">Route</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlRouteId" IncrementalFilteringMode="StartsWith" PreventEntityCollectionInitialization="true" EntityName="Route" TextField="Name" ValueField="RouteId" />
                                    </td>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblWeatherClockWidgetMode">Weather Clock Widget Version</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <X:ComboBoxInt runat="server" ID="ddlWeatherClockWidgetMode" UseDataBinding="True" DisplayEmptyItem="False"></X:ComboBoxInt>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblClockMode">Clock Mode</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <X:ComboBoxInt runat="server" ID="ddlClockMode" UseDataBinding="True" DisplayEmptyItem="False"></X:ComboBoxInt>
                                    </td>
                                    <td class="label"></td>
                                    <td class="control"></td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblAlterationDialogMode">Alteration Dialog Mode</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <X:ComboBoxInt runat="server" ID="ddlAlterationDialogMode" UseDataBinding="True" DisplayEmptyItem="False"></X:ComboBoxInt>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblAvailableOnObymobi">Obymobi</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <D:CheckBox runat="server" ID="cbAvailableOnObymobi" />
                                    </td>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblAvailableOnOtoucho">Otoucho</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <D:CheckBox runat="server" ID="cbAvailableOnOtoucho" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblMobileOrderingDisabled">Mobiele Ordering Uitgeschakeld</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <D:CheckBox runat="server" ID="cbMobileOrderingDisabled" />
                                    </td>
                                    <td class="label"></td>
                                    <td class="control"></td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblDescriptionSingleLine">Description short</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control" colspan="3">
                                        <D:TextBoxString ID="tbDescriptionSingleLine" runat="server"></D:TextBoxString>
                                    </td>
                                    <td class="label">&nbsp;
                                    </td>
                                    <td class="control">&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblDescription">Beschrijving</D:LabelEntityFieldInfo>
                                    </td>
                                    <td colspan="3" class="control">
                                        <D:TextBox ID="tbDescription" runat="server" Rows="5" TextMode="MultiLine" CssClass="inputNoHeight"></D:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </D:Panel>
                        <D:Panel ID="pnlMobile" runat="server" GroupingText="Mobile">
                            <table class="dataformV2">
                                <tr>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblActionButtonId">Action button</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <X:ComboBoxLLBLGenEntityCollection runat="server" ID="cbActionButtonId" CssClass="input" UseDataBinding="true" EntityName="ActionButton"></X:ComboBoxLLBLGenEntityCollection>
                                    </td>
                                    <td class="label"></td>
                                    <td class="control"></td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblActionButtonUrlTablet">Action button url tablet</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString ID="tbActionButtonUrlTablet" runat="server"></D:TextBoxString>
                                    </td>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblActionButtonUrlMobile">Action button url mobile</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString ID="tbActionButtonUrlMobile" runat="server"></D:TextBoxString>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblCostIndicationType">Cost indication type</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <X:ComboBoxEnum ID="cbCostIndicationType" runat="server" Type="Obymobi.Enums.CostIndicationType, Obymobi"></X:ComboBoxEnum>
                                    </td>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblCostIndicationValue">Cost indication value</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxDecimal ID="tbCostIndicationValue" runat="server"></D:TextBoxDecimal>
                                    </td>
                                </tr>
                            </table>
                        </D:Panel>
                    </controls>
                </X:TabPage>
                <X:TabPage Text="Advanced" Name="Advanced">
                    <controls>
                        <D:Panel ID="pnlCrave" runat="server" GroupingText="Crave">
                            <table class="dataformV2">
                                <tr>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblAllowFreeText">Allow free text</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <D:CheckBox runat="server" ID="cbAllowFreeText" />
                                    </td>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblUseMonitoring">SMS support monitoring</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <D:CheckBox runat="server" ID="cbUseMonitoring" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        <D:Label runat="server" ID="lblIncludeDeliveryTimeInOrderNote">Include delivery time in notes</D:Label>
                                    </td>
                                    <td class="control">
                                        <D:CheckBox runat="server" ID="cbIncludeDeliveryTimeInOrderNotes" />
                                    </td>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblAutomaticClientOperationModes">Automatic client operation modes</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <D:CheckBox runat="server" ID="cbAutomaticClientOperationModes" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblMaxDownloadConnections">Max downloads</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxInt runat="server" ID="tbMaxDownloadConnections" AllowNegative="false" AllowZero="false"></D:TextBoxInt>
                                    </td>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblDeviceRebootMethod">Device reboot method</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <X:ComboBoxInt runat="server" ID="ddlDeviceRebootMethod" UseDataBinding="True" DisplayEmptyItem="False"></X:ComboBoxInt>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label"></td>
                                    <td class="control"></td>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblCorrespondenceEmail">Correspondentie email</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control" rowspan="2">
                                        <D:TextBoxMultiLine ID="tbCorrespondenceEmail" runat="server"></D:TextBoxMultiLine>
                                        <br />
                                        <D:Label ID="lblEmailComment" runat="server"><small>Meerdere e-mailadressen: Plaats elk e-mailadres op een nieuwe regel.</small></D:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblCometHandlerType">Comet Handler</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <X:ComboBoxInt ID="ddlCometHandlerType" runat="server" UseDataBinding="true" DisplayEmptyItem="False"></X:ComboBoxInt>
                                    </td>
                                    <td class="label"></td>
                                    <td class="control"></td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblPercentageDownForNotification">Max percentage clients down</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxInt runat="server" ID="tbPercentageDownForNotification" AllowNegative="false" AllowZero="false"></D:TextBoxInt>
                                    </td>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblPercentageDownJumpForNotification">Max percentage clients down (jump)</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxInt runat="server" ID="tbPercentageDownJumpForNotification" AllowNegative="false" AllowZero="false"></D:TextBoxInt>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblPercentageDownIntervalHours">Percentage down interval hours</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxInt runat="server" ID="tbPercentageDownIntervalHours" AllowNegative="false" AllowZero="false"></D:TextBoxInt>
                                    </td>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblDailyReportSendTime">Daily report send time</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <D:TimePicker2 ID="teDailyReportSendTime" runat="server"></D:TimePicker2>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblIncludeMenuItemsNotExternallyLinked">Include menu items not externally linked</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <D:CheckBox runat="server" ID="cbIncludeMenuItemsNotExternallyLinked" />
                                    </td>
                                    <td class="label"></td>
                                    <td class="control"></td>
                                </tr>
                            </table>
                        </D:Panel>
                        <D:Panel ID="pnlAnalytics" runat="server" GroupingText="Analytics (Emenu only)">
                            <table class="dataformV2">
                                <tr>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblGoogleAnalyticsId">Google analytics ID</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString ID="tbGoogleAnalyticsId" CssClass="tbGoogleAnalyticsId" runat="server"></D:TextBoxString>
                                        <D:Button runat="server" ID="btEditGoogleAnalyticsId" CssClass="btEditGoogleAnalyticsId" Visible="false" Text="Edit" OnClientClick="return editGoogleAnalyticsId();" />
                                    </td>
                                    <td class="label"></td>
                                    <td class="control"></td>
                                </tr>
                            </table>
                        </D:Panel>
                        <D:Panel ID="pnlActions" runat="server" GroupingText="Acties">
                            <table class="dataformV2">
                                <tr>
                                    <td class="label">
                                        <D:Label runat="server" ID="lblRefreshCacheTimeStamps_Company">Refresh Company Timestamps</D:Label>
                                    </td>
                                    <td class="control">
                                        <D:Button runat="server" ID="btnRefreshCacheTiemstamps_Company" Text="Refresh" />
                                        &nbsp;<D:Label runat="server" ID="lblTimestampCompany" LocalizeText="False" />
                                    </td>
                                    <td class="label">
                                        <D:Label runat="server" ID="lblResetCacheTimeStampsAll">Refresh All</D:Label>
                                    </td>
                                    <td class="control">
                                        <D:Button runat="server" ID="btResetCacheTimeStamps" Text="Verversen" />
                                    </td>
                                </tr>

                                <tr>

                                    <td class="label">
                                        <D:Label runat="server" ID="lblRefreshCacheTimeStamps_Pos">Refresh POS Timestamps</D:Label>
                                    </td>
                                    <td class="control">
                                        <D:Button runat="server" ID="btnRefreshCacheTimeStamps_Pos" Text="Refresh" />
                                        &nbsp;<D:Label runat="server" ID="lblTimestampPos" LocalizeText="False" />
                                    </td>
                                    <td class="label"></td>
                                    <td class="control"></td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        <D:Label runat="server" ID="lblRefreshCacheTimeStamps_Deliverypoint">Refresh Deliverypoint Timestamps</D:Label>
                                    </td>
                                    <td class="control">
                                        <D:Button runat="server" ID="btnRefreshCacheTimeStamps_Deliverypoint" Text="Refresh" />
                                        &nbsp;<D:Label runat="server" ID="lblTimestampDeliverypoint" LocalizeText="False" />
                                    </td>
                                    <td class="label"></td>
                                    <td class="control"></td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        <D:Label runat="server" ID="lblRefreshCacheTimeStamps_Survey">Refresh Survey Timestamps</D:Label>
                                    </td>
                                    <td class="control">
                                        <D:Button runat="server" ID="btnRefreshCacheTimeStamps_Survey" Text="Refresh" />
                                        &nbsp;<D:Label runat="server" ID="lblTimestampSurvey" LocalizeText="False" />
                                    </td>
                                    <td class="label"></td>
                                    <td class="control"></td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        <D:Label runat="server" ID="lblRefreshCacheTimeStamps_Announcement">Refresh Announcement Timestamps</D:Label>
                                    </td>
                                    <td class="control">
                                        <D:Button runat="server" ID="btnRefreshCacheTimeStamps_Announcement" Text="Refresh" />
                                        &nbsp;<D:Label runat="server" ID="lblTimestampAnnouncement" LocalizeText="False" />
                                    </td>
                                    <td class="label"></td>
                                    <td class="control"></td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        <D:Label runat="server" ID="lblRefreshCacheTimeStamps_Entertainment">Refresh Entertainment Timestamps</D:Label>
                                    </td>
                                    <td class="control">
                                        <D:Button runat="server" ID="btnRefreshCacheTimeStamps_Entertainment" Text="Refresh" />
                                        &nbsp;<D:Label runat="server" ID="lblTimestampEntertainment" LocalizeText="False" />
                                    </td>
                                    <td class="label"></td>
                                    <td class="control"></td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        <D:Label runat="server" ID="lblRefreshCacheTimeStamps_Advertisement">Refresh Advertisement Timestamps</D:Label>
                                    </td>
                                    <td class="control">
                                        <D:Button runat="server" ID="btnRefreshCacheTimeStamps_Advertisement" Text="Refresh" />
                                        &nbsp;<D:Label runat="server" ID="lblTimestampAdvertisement" LocalizeText="False" />
                                    </td>
                                    <td class="label"></td>
                                    <td class="control"></td>
                                </tr>
                            </table>
                        </D:Panel>
                        <D:Panel ID="pnlCredentials" runat="server" GroupingText="Credentials">
                            <table class="dataformV2">
                                <tr>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblPincode">Pincode</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString runat="server" ID="tbPincode" MaxLength="25" />
                                    </td>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblPincodeGM">Pincode godmode</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString runat="server" ID="tbPincodeGM" MaxLength="25" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblPincodeSU">Pincode superuser</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString runat="server" ID="tbPincodeSU" MaxLength="25" />
                                    </td>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblSystemPassword">System password</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString runat="server" ID="tbSystemPassword" MaxLength="20" />
                                    </td>
                                </tr>
                            </table>
                        </D:Panel>
                        <D:Panel ID="pnlFormatting" runat="server" GroupingText="Formatting">
                            <table class="dataformV2">
                                <tr>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblShowCurrencySymbol">Show currency symbol</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <D:CheckBox runat="server" ID="cbShowCurrencySymbol" />
                                    </td>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblPriceFormatType">Empty price format</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <X:ComboBoxInt ID="ddlPriceFormatType" runat="server" UseDataBinding="true" DisplayEmptyItem="False"></X:ComboBoxInt>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblPricesIncludeTaxes">Prices including taxes</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <D:CheckBox runat="server" ID="cbPricesIncludeTaxes" />
                                    </td>
                                    <td class="label"></td>
                                    <td class="control"></td>
                                </tr>
                            </table>
                        </D:Panel>
                        <D:Panel ID="pnlServiceVersions" runat="server" GroupingText="Service versions" Visible="False">
                            <table class="dataformV2">
                                <tr>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblApiVersion">Use API v2</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <D:CheckBox runat="server" ID="cbApiVersionV2" />
                                    </td>
                                    <td class="label"></td>
                                    <td class="control"></td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblMessagingVersion">Use AWS Messaging</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <D:CheckBox runat="server" ID="cbMessagingVersionV3" />
                                    </td>
                                    <td class="label"></td>
                                    <td class="control"></td>
                                </tr>
                            </table>
                        </D:Panel>
                        <D:Panel ID="pnlContent" runat="server" GroupingText="Content" Visible="False">
                            <table class="dataformV2">
                                <tr>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblHotswappableContent">Hot-swappable menu content</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <D:CheckBox runat="server" ID="cbHotSwappableContent" />
                                    </td>
                                    <td class="label"></td>
                                    <td class="control"></td>
                                </tr>
                            </table>
                        </D:Panel>
                    </controls>
                </X:TabPage>
                <X:TabPage Text="Categories" Name="tabCategories">
                    <controls>
                        <D:PlaceHolder ID="PlaceHolder1" runat="server">
                            <table class="dataformV2">
                                <tr>
                                    <td class="label">
                                        <D:Label runat="server" ID="lblCompanyVenueCategories">Categories</D:Label>
                                    </td>
                                    <td class="control threeCol" colspan="3">
                                        <D:CheckBoxListLLBLGenEntityCollection runat="server" ID="cblCategories" EntityName="VenueCategory" DataTextField="Name" LinkCollectionPropertyOnParentDataSource="CompanyVenueCategoryCollection" LinkEntityName="CompanyVenueCategoryEntity" RepeatColumns="3" CssClass="checkboxlist-advertisements" />
                                    </td>
                                </tr>
                            </table>
                        </D:PlaceHolder>
                    </controls>
                </X:TabPage>
                <X:TabPage Text="Amenities" Name="tabAmenities">
                    <controls>
                        <D:PlaceHolder ID="PlaceHolder2" runat="server">
                            <table class="dataformV2">
                                <tr>
                                    <td class="label">
                                        <D:Label runat="server" ID="lblCompanyAmenities">Amenities</D:Label>
                                    </td>
                                    <td class="control threeCol" colspan="3">
                                        <D:CheckBoxListLLBLGenEntityCollection runat="server" ID="cblAmenites" EntityName="Amenity" DataTextField="Name" LinkCollectionPropertyOnParentDataSource="CompanyAmenityCollection" LinkEntityName="CompanyAmenityEntity" RepeatColumns="3" CssClass="checkboxlist-advertisements" />
                                    </td>
                                </tr>
                            </table>
                        </D:PlaceHolder>
                    </controls>
                </X:TabPage>
                <X:TabPage Text="Releases" Name="Releases">
                    <controls>
                        <D:PlaceHolder ID="PlaceHolder5" runat="server">
                            <table class="dataformV2">
                                <D:PlaceHolder runat="server" ID="plhReleases"></D:PlaceHolder>
                            </table>
                        </D:PlaceHolder>
                    </controls>
                </X:TabPage>
            </TabPages>
        </X:PageControl>
    </div>
    <script type="text/javascript">
        function editGoogleAnalyticsId() {
            if (confirm("Are you sure you want to edit the Google Analytics ID?")) {
                document.querySelector('.tbGoogleAnalyticsId').disabled = false;
                document.querySelector('.btEditGoogleAnalyticsId').style.visibility = 'hidden';
            }
        }
    </script>
</asp:Content>
