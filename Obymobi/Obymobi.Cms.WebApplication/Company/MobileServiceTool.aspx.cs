﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Dionysos;
using Dionysos.Web.UI;
using Obymobi.Cms.WebApplication.MobileService;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Security;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Company
{
    public partial class MobileServiceTool : PageDefault
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.SetGui();
        }

        #region Methods

        public void Show()
        {
            this.PerformWebserviceCall(false);
        }

        public void Download()
        {
            this.PerformWebserviceCall(true);
        }

        void SetGui()
        {
            CompanyCollection companies = new CompanyCollection();
            // GK Removed the check for Obymobi == TRUE - It's an advanced user tool - if you use a wrongly configured Company you'll just gte an error
            // also that check prevent you from testing for Companies not on live yet, but available using access codes.
            companies.GetMulti(null, 0, new SortExpression(CompanyFields.Name | SortOperator.Ascending));

            foreach (var company in companies)
            {
                this.cbCompany.Items.Add(company.Name, company.CompanyId);
            }

            if (companies.Any(x => x.CompanyId == CmsSessionHelper.CurrentCompanyId))
                this.cbCompany.Value = CmsSessionHelper.CurrentCompanyId;

            // Points of Interst
            PointOfInterestCollection pointsOfInterest = new PointOfInterestCollection();
            pointsOfInterest.GetMulti(null, 0, new SortExpression(PointOfInterestFields.Name | SortOperator.Ascending));
            foreach (var poi in pointsOfInterest)
            {
                this.cbPointOfInterest.Items.Add(poi.Name, poi.PointOfInterestId);
            }

            this.cbPointOfInterest.SelectedIndex = 0;

            // Menu
            MenuCollection menus = new MenuCollection();
            menus.GetMulti(MenuFields.CompanyId == this.cbCompany.ValidId, 0, new SortExpression(MenuFields.Name | SortOperator.Ascending));

            foreach (var menu in menus)
            {
                this.cbMenu.Items.Add(menu.Name, menu.MenuId);
            }

            this.cbMenu.SelectedIndex = 0;


            this.cbMethod.EmptyItemText = "TRUE";

            SiteCollection sites = new SiteCollection();
            sites.GetMulti(null, 0, new SortExpression(SiteFields.Name | SortOperator.Ascending));
            foreach (var site in sites)
                this.cbSiteId.Items.Add(site.Name, site.SiteId);
            this.cbSiteId.SelectedIndex = 0;

            IEnumerable<Obymobi.Culture> cultures = Obymobi.Culture.Mappings.Values.OrderBy(x => x.ToString());
            foreach (Obymobi.Culture culture in cultures)
            {
                this.ddlCultureCode.Items.Add(culture.ToString(), culture.Code);
            }
            this.ddlCultureCode.Value = Obymobi.Culture.English_United_Kingdom.Code;

            // Webservice methods
            this.cbMethod.Items.Add("IsWebserviceAlive");
            this.cbMethod.Items.Add("IsAlive");
            this.cbMethod.Items.Add("IsDatabaseAlive");
            this.cbMethod.Items.Add("GetCurrentTime");
            this.cbMethod.Items.Add("GetCompanyListTicks");
            this.cbMethod.Items.Add("GetCompanyList");
            this.cbMethod.Items.Add("GetCompanyTicks");
            this.cbMethod.Items.Add("GetCompany");
            this.cbMethod.Items.Add("GetMenuTicks");
            this.cbMethod.Items.Add("GetMenu");
            this.cbMethod.Items.Add("GetSite");
            this.cbMethod.Items.Add("GetSiteTicks");
            this.cbMethod.Items.Add("GetPointOfInterest");
            this.cbMethod.Items.Add("GetPointOfInterestTicks");
            this.cbMethod.Items.Add("GetCommonData");
            this.cbMethod.Items.Add("GetCommonDataTicks");
            this.cbMethod.Items.Add("GetAccessCodes");
            this.cbMethod.Items.Add("GetAccessCodesTicks");

            this.cbDeviceType.DataBindEnum<DeviceType>();
            this.cbDeviceType.Value = (int)DeviceType.PhoneNormal;
        }

        private void PerformWebserviceCall(bool download)
        {
            // Check if we have a customer to test with (a customer with a device)
            DeviceCollection devices = new DeviceCollection();
            PredicateExpression filter = new PredicateExpression();
            RelationCollection relations = new RelationCollection();
            relations.Add(DeviceEntity.Relations.CustomerEntityUsingCustomerId);
            filter.Add(DeviceFields.CustomerId != DBNull.Value);
            filter.Add(CustomerFields.CommunicationSalt != "");
            filter.Add(CustomerFields.CommunicationSalt != DBNull.Value);

            if (!this.tbEmail.Text.IsNullOrWhiteSpace())
                filter.Add(DeviceFields.Identifier % this.tbEmail.Text);

            devices.GetMulti(filter, 1, null, relations);

            if (devices.Count == 0 || devices[0].CustomerEntity.IsNew)
            {
                this.AddInformator(InformatorType.Warning, this.Translate("CustomerNotFound", "Er is geen 'Customer' gevonden om de webservice call mee te doen."));
                return;
            }
            else if (devices[0].CustomerEntity.CommunicationSalt.IsNullOrWhiteSpace())
            {
                this.AddInformator(InformatorType.Warning, this.Translate("CustomerCommunicationSaltEmpty", "Er is geen 'CommunicationSalt' voor de gekozen 'Customer'"));
                return;
            }

            if (this.cbMethod.Value == null || this.cbMethod.Value.ToString().IsNullOrWhiteSpace())
            {
                this.AddInformator(InformatorType.Warning, this.Translate("SelectMethod", "Er moet een methode gekozen worden om aan te roepen"));
                return;
            }

            if (this.cbDeviceType.ValidId <= 0)
            {
                this.AddInformator(InformatorType.Warning, this.Translate("DeviceTypeMissing", "Er moet een DeviceType gekozen zijn."));
                return;
            }

            if (this.cbCompany.ValidId <= 0 && !TestUtil.IsPcGabriel)
            {
                this.AddInformator(InformatorType.Warning, this.Translate("CompanyMissing", "Er moet een Company gekozen zijn."));
                return;
            }

            // Do the magic
            CustomerEntity customer = devices[0].CustomerEntity;
            string method = this.cbMethod.Value.ToString();
            using (var service = new MobileService())
            {
                service.Url = WebEnvironmentHelper.GetApiUrl("25.0/MobileService.asmx");
                //service.Url = WebEnvironmentHelper.GetApiMobileServiceUrl();
                //service.Url = "http://localhost/trunk/api/15.0/MobileService.asmx";

                long timestamp = DateTimeUtil.ToUnixTime(DateTime.Now);
                string salt = customer.CommunicationSalt;
                string mac = devices[0].Identifier; // == customer.Email
                string hash, xml, resultCode, resultMessage, resultType;
                xml = string.Empty;
                resultCode = string.Empty;
                resultMessage = string.Empty;
                resultType = string.Empty;
                bool tablet = this.cbTablet.Checked;
                int deviceType = this.cbDeviceType.ValidId;
                int companyId = this.cbCompany.ValidId;
                int menuId = this.cbMenu.ValidId;
                int siteId = this.cbSiteId.ValidId;
                string cultureCode = this.ddlCultureCode.Value != null ? this.ddlCultureCode.Value.ToString() : string.Empty;
                int pointOfInterestId = this.cbPointOfInterest.ValidId;
                string languageCode = string.Empty;
                string existingAccessCodeIds = this.tbAccessCodeIds.Text;
                string accessCodeToAdd = this.tbAccessCodeToAdd.Text;

                List<Object> parameters = new List<object>();

                Stopwatch webcallStopwatch = new Stopwatch();
                string error = string.Empty;
                string modelCount = string.Empty;
                try
                {
                    switch (method)
                    {
                        case "IsWebserviceAlive":
                            parameters = new List<object> { };
                            webcallStopwatch.Start();
                            var getIsWebserviceAlive = service.IsWebserviceAlive();
                            webcallStopwatch.Stop();
                            xml = getIsWebserviceAlive.ToString();
                            resultCode = "N/A";
                            resultMessage = "N/A";
                            resultType = "N/A";
                            modelCount = "N/A";
                            break;
                        case "IsAlive":
                            parameters = new List<object> { };
                            webcallStopwatch.Start();
                            var getIsAlive = service.IsAlive();
                            webcallStopwatch.Stop();
                            xml = getIsAlive.ToString();
                            resultCode = "N/A";
                            resultMessage = "N/A";
                            resultType = "N/A";
                            modelCount = "N/A";
                            break;
                        case "IsDatabaseAlive":
                            parameters = new List<object> { };
                            webcallStopwatch.Start();
                            var getIsDatabaseAlive = service.IsDatabaseAlive();
                            webcallStopwatch.Stop();
                            xml = getIsDatabaseAlive.ToString();
                            resultCode = "N/A";
                            resultMessage = "N/A";
                            resultType = "N/A";
                            modelCount = "N/A";
                            break;
                        case "GetCurrentTime":
                            parameters = new List<object> { };
                            webcallStopwatch.Start();
                            var getGetCurrentTime = service.GetCurrentTime();
                            webcallStopwatch.Stop();
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(getGetCurrentTime.ModelCollection);
                            resultCode = "N/A";
                            resultMessage = "N/A";
                            resultType = "N/A";
                            modelCount = getGetCurrentTime.ModelCollection.Length.ToString();
                            break;
                        case "GetCompanyListTicks":
                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac, existingAccessCodeIds);
                            parameters = new List<object> { timestamp, mac, existingAccessCodeIds, hash };
                            webcallStopwatch.Start();
                            var getCompanyListTicksResponse = service.GetCompanyListTicks(timestamp, mac, existingAccessCodeIds, hash);
                            webcallStopwatch.Stop();
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(getCompanyListTicksResponse.ModelCollection);
                            resultCode = getCompanyListTicksResponse.ResultCode.ToString();
                            resultMessage = getCompanyListTicksResponse.ResultMessage;
                            resultType = getCompanyListTicksResponse.ResultType;
                            modelCount = getCompanyListTicksResponse.ModelCollection.Length.ToString();
                            break;
                        case "GetCompanyList":
                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac, deviceType, tablet, existingAccessCodeIds);
                            parameters = new List<object> { timestamp, mac, deviceType, tablet, existingAccessCodeIds, hash };
                            webcallStopwatch.Start();
                            var getCompanyListResponse = service.GetCompanyList(timestamp, mac, deviceType, tablet, existingAccessCodeIds, hash);
                            webcallStopwatch.Stop();
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(getCompanyListResponse, false);
                            resultCode = getCompanyListResponse.ResultCode.ToString();
                            resultMessage = getCompanyListResponse.ResultMessage;
                            resultType = getCompanyListResponse.ResultType;
                            if (getCompanyListResponse.ModelCollection.Length > 0)
                                modelCount = getCompanyListResponse.ModelCollection[0].Companies.Length.ToString();
                            break;
                        case "GetCompanyTicks":
                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac, companyId);
                            parameters = new List<object> { timestamp, mac, companyId, hash };
                            webcallStopwatch.Start();
                            var getCompanyTicksResponse = service.GetCompanyTicks(timestamp, mac, companyId, hash);
                            webcallStopwatch.Stop();
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(getCompanyTicksResponse.ModelCollection);
                            resultCode = getCompanyTicksResponse.ResultCode.ToString();
                            resultMessage = getCompanyTicksResponse.ResultMessage;
                            resultType = getCompanyTicksResponse.ResultType;
                            modelCount = getCompanyTicksResponse.ModelCollection.Length.ToString();
                            break;
                        case "GetCompany":
                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac, companyId, deviceType, tablet);
                            parameters = new List<object> { timestamp, mac, companyId, deviceType, tablet, hash };
                            webcallStopwatch.Start();
                            var getCompanyResponse = service.GetCompany(timestamp, mac, companyId, deviceType, tablet, hash);
                            webcallStopwatch.Stop();
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(getCompanyResponse.ModelCollection);
                            resultCode = getCompanyResponse.ResultCode.ToString();
                            resultMessage = getCompanyResponse.ResultMessage;
                            resultType = getCompanyResponse.ResultType;
                            modelCount = getCompanyResponse.ModelCollection.Length.ToString();
                            break;
                        case "GetMenuTicks":
                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac, companyId);
                            parameters = new List<object> { timestamp, mac, companyId, hash };
                            webcallStopwatch.Start();
                            var getMenuTicksResponse = service.GetMenuTicks(timestamp, mac, companyId, hash);
                            webcallStopwatch.Stop();
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(getMenuTicksResponse.ModelCollection);
                            resultCode = getMenuTicksResponse.ResultCode.ToString();
                            resultMessage = getMenuTicksResponse.ResultMessage;
                            resultType = getMenuTicksResponse.ResultType;
                            modelCount = getMenuTicksResponse.ModelCollection.Length.ToString();
                            break;
                        case "GetMenu":
                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac, menuId, deviceType);
                            parameters = new List<object> { timestamp, mac, menuId, deviceType, hash };
                            webcallStopwatch.Start();
                            var getMenuResponse = service.GetMenu(timestamp, mac, menuId, deviceType, hash);
                            webcallStopwatch.Stop();
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(getMenuResponse.ModelCollection);
                            resultCode = getMenuResponse.ResultCode.ToString();
                            resultMessage = getMenuResponse.ResultMessage;
                            resultType = getMenuResponse.ResultType;
                            modelCount = getMenuResponse.ModelCollection.Length.ToString();
                            break;
                        case "GetSite":
                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac, siteId, languageCode, deviceType, tablet);
                            webcallStopwatch.Start();
                            var getSiteResponse = service.GetSite(timestamp, mac, siteId, languageCode, deviceType, tablet, hash);
                            webcallStopwatch.Stop();
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(getSiteResponse.ModelCollection);
                            resultCode = getSiteResponse.ResultCode.ToString();
                            resultMessage = getSiteResponse.InnerResultMessage;
                            resultType = getSiteResponse.ResultType;
                            modelCount = getSiteResponse.ModelCollection.Length.ToString();
                            break;
                        case "GetSiteTicks":
                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac, siteId);
                            parameters = new List<object> { timestamp, mac, companyId, hash };
                            webcallStopwatch.Start();
                            var getSiteTicksResponse = service.GetSiteTicks(timestamp, mac, siteId, hash);
                            webcallStopwatch.Stop();
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(getSiteTicksResponse.ModelCollection);
                            resultCode = getSiteTicksResponse.ResultCode.ToString();
                            resultMessage = getSiteTicksResponse.ResultMessage;
                            resultType = getSiteTicksResponse.ResultType;
                            modelCount = getSiteTicksResponse.ModelCollection.Length.ToString();
                            break;
                        case "GetPointOfInterest":
                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac, pointOfInterestId, deviceType, tablet);
                            webcallStopwatch.Start();
                            var getPointOfInterestResponse = service.GetPointOfInterest(timestamp, mac, pointOfInterestId, deviceType, tablet, hash);
                            webcallStopwatch.Stop();
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(getPointOfInterestResponse.ModelCollection);
                            resultCode = getPointOfInterestResponse.ResultCode.ToString();
                            resultMessage = getPointOfInterestResponse.InnerResultMessage;
                            resultType = getPointOfInterestResponse.ResultType;
                            modelCount = getPointOfInterestResponse.ModelCollection.Length.ToString();
                            break;
                        case "GetPointOfInterestTicks":
                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac, pointOfInterestId);
                            parameters = new List<object> { timestamp, mac, companyId, hash };
                            webcallStopwatch.Start();
                            var getPointOfInterestTicksResponse = service.GetPointOfInterestTicks(timestamp, mac, pointOfInterestId, hash);
                            webcallStopwatch.Stop();
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(getPointOfInterestTicksResponse.ModelCollection);
                            resultCode = getPointOfInterestTicksResponse.ResultCode.ToString();
                            resultMessage = getPointOfInterestTicksResponse.ResultMessage;
                            resultType = getPointOfInterestTicksResponse.ResultType;
                            modelCount = getPointOfInterestTicksResponse.ModelCollection.Length.ToString();
                            break;
                        case "GetCommonData":
                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac);
                            parameters = new List<object> { timestamp, mac, companyId, hash };
                            webcallStopwatch.Start();
                            var getCommonDataResponse = service.GetCommonData(timestamp, mac, hash);
                            webcallStopwatch.Stop();
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(getCommonDataResponse.ModelCollection);
                            resultCode = getCommonDataResponse.ResultCode.ToString();
                            resultMessage = getCommonDataResponse.ResultMessage;
                            resultType = getCommonDataResponse.ResultType;
                            modelCount = getCommonDataResponse.ModelCollection.Length.ToString();
                            break;
                        case "GetCommonDataTicks":
                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac);
                            parameters = new List<object> { timestamp, mac, companyId, hash };
                            webcallStopwatch.Start();
                            var getCommonDataTicksResponse = service.GetCommonDataTicks(timestamp, mac, hash);
                            webcallStopwatch.Stop();
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(getCommonDataTicksResponse.ModelCollection);
                            resultCode = getCommonDataTicksResponse.ResultCode.ToString();
                            resultMessage = getCommonDataTicksResponse.ResultMessage;
                            resultType = getCommonDataTicksResponse.ResultType;
                            modelCount = getCommonDataTicksResponse.ModelCollection.Length.ToString();
                            break;
                        case "GetAccessCodes":
                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac, existingAccessCodeIds, accessCodeToAdd);
                            parameters = new List<object> { timestamp, mac, existingAccessCodeIds, accessCodeToAdd, hash };
                            webcallStopwatch.Start();
                            var getAccessCodesResponse = service.GetAccessCodes(timestamp, mac, existingAccessCodeIds, accessCodeToAdd, hash);
                            webcallStopwatch.Stop();
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(getAccessCodesResponse.ModelCollection);
                            resultCode = getAccessCodesResponse.ResultCode.ToString();
                            resultMessage = getAccessCodesResponse.ResultMessage;
                            resultType = getAccessCodesResponse.ResultType;
                            modelCount = getAccessCodesResponse.ModelCollection.Length.ToString();
                            break;
                        case "GetAccessCodesTicks":
                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac, existingAccessCodeIds);
                            parameters = new List<object> { timestamp, mac, existingAccessCodeIds, hash };
                            webcallStopwatch.Start();
                            var getAccessCodesTicksResponse = service.GetAccessCodesTicks(timestamp, mac, existingAccessCodeIds, hash);
                            webcallStopwatch.Stop();
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(getAccessCodesTicksResponse.ModelCollection);
                            resultCode = getAccessCodesTicksResponse.ResultCode.ToString();
                            resultMessage = getAccessCodesTicksResponse.ResultMessage;
                            resultType = getAccessCodesTicksResponse.ResultType;
                            modelCount = getAccessCodesTicksResponse.ModelCollection.Length.ToString();
                            break;

                        default:
                            throw new NotImplementedException();
                    }

                    if (download)
                    {
                        this.Response.Clear();
                        Response.AddHeader("content-disposition", "attachment;filename={0}-{1}.xml".FormatSafe(this.cbMethod.Text, DateTime.Now.Ticks));
                        this.Response.Write(xml);
                        this.Response.ContentType = "text/xml";
                        this.Response.End();
                    }
                    else
                    {
                        string parameterText = String.Join(", ", parameters);
                        this.plhReponse.AddHtml("\r\n{0}({1});", method, parameterText);
                        this.plhReponse.AddHtml("\r\n", resultCode);
                        this.plhReponse.AddHtml("Webservice url: " + service.Url);
                        this.plhReponse.AddHtml("Result code: {0}\r\n", resultCode);
                        this.plhReponse.AddHtml("Result message: {0}\r\n", resultMessage);
                        this.plhReponse.AddHtml("Result type: {0}\r\n", resultType);
                        this.plhReponse.AddHtml("Response time: {0}ms\r\n", webcallStopwatch.Elapsed);
                        this.plhReponse.AddHtml("Model count: {0}\r\n", modelCount);
                        this.plhReponse.AddHtml("Webservice URL: {0}\r\n", service.Url);
                        this.plhReponse.AddHtml(xml.HtmlEncode());
                    }
                }
                catch (Exception ex)
                {
                    throw ex;

                    //this.plhReponse.AddHtml("Call failed");
                    //string parameterText = string.Join(", ", parameters);
                    //this.plhReponse.AddHtml("\r\n{0}({1});", method, parameterText);
                    //this.plhReponse.AddHtml("\r\nError: {0}", ex.Message);
                    //this.plhReponse.AddHtml("\r\nStack: {0}", ex.StackTrace);
                }
            }
        }

        #endregion

        #region Event handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            //this.HookupEvents();
        }


        #endregion
    }
}
