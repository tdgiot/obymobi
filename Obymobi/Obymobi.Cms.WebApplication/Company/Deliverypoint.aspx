﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.Deliverypoint" Codebehind="Deliverypoint.aspx.cs" %>
<%@ Reference VirtualPath="~/Generic/SubPanels/PublishingCollection.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<controls>
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblName">Name</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblDeliverypointgroupId">Tafelgroep</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlDeliverypointgroupId" IncrementalFilteringMode="StartsWith" EntityName="Deliverypointgroup" TextField="Name" ValueField="DeliverypointgroupId" IsRequired="true" PreventEntityCollectionInitialization="true" />
							</td>
					    </tr>
					    <tr>						
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblNumber">Nummer</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxInt ID="tbNumber" runat="server" IsRequired="true" MaxLength="9" AllowNegative="False" ThousandsSeperators="False"></D:TextBoxInt>
							</td>        
                            <td class="label">
                                <D:Label ID="lblClientConfiguration" runat="server">Client configuration</D:Label>
                            </td>
                            <td class="control">
                                <X:ComboBoxInt runat="server" ID="cbClientConfigurationId" CssClass="input" TextField="Name" ValueField="ClientConfigurationId" IncrementalFilteringMode="Contains" IncrementalFilteringDelay="1000"></X:ComboBoxInt>                                
                            </td>
						</tr>                        
                        <tr>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblGooglePrinter">Printer</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
                                <X:ComboBoxString runat="server" ID="ddlGooglePrinterId" IncrementalFilteringMode="StartsWith" TextField="Name" ValueField="PrinterId"></X:ComboBoxString>
                                <D:LinkButton ID="btSelectPrinter" runat="server" Text="Load Printers" LocalizeText="false" ForeColor="Black" />
                                <D:LinkButton ID="btRefreshPrinters" runat="server" Text="Refresh Printers" LocalizeText="false" ForeColor="Black" />
							</td>        							
							<td class="label">
                                <D:Label runat="server" ID="lblDeviceId">Print vanaf</D:Label>
							</td>
							<td class="control">
							    <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlDeviceId" IncrementalFilteringMode="StartsWith" EntityName="Device" TextField="Identifier" ValueField="DeviceId" PreventEntityCollectionInitialization="true" />
							</td>
						</tr>
                    </table>
                    <D:Panel ID="pnlRoomControl" runat="server" GroupingText="Room Control Settings">
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblRoomControlType">Room control type</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
                                <X:ComboBoxEnum ID="ddlRoomControllerType" runat="server" DisplayEmptyItem="false" Type="Obymobi.Enums.RoomControlType, Obymobi"></X:ComboBoxEnum> 
							</td>
                            <td class="label"></td>
							<td class="control"></td>
                        </tr>
                        <tr>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblRoomControllerIp">Room control IP</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
                                <D:TextBoxString ID="tbRoomControllerIp" runat="server"></D:TextBoxString>
							</td>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblRoomControlConfigurationId">Room control configuration</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlRoomControlConfigurationId" IncrementalFilteringMode="StartsWith" EntityName="RoomControlConfiguration" TextField="Name" ValueField="RoomControlConfigurationId" PreventEntityCollectionInitialization="true" />
                            </td>             
                        </tr>
                        <tr>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblRoomControllerSlaveId">Modbus Slave Id</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <D:TextBoxInt ID="tbRoomControllerSlaveId" runat="server"></D:TextBoxInt>
                            </td>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblRoomControlAreaId">Default room control area</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlRoomControlAreaId" IncrementalFilteringMode="StartsWith" EntityName="RoomControlArea" TextField="Name" ValueField="RoomControlAreaId" PreventEntityCollectionInitialization="true" />
                            </td>
                        </tr>
					</table>
                    </D:Panel>
				</Controls>
			</X:TabPage>
            <X:TabPage Text="Geavanceerd" Name="tabAdvanced">
                <controls>
				<D:PlaceHolder ID="PlaceHolder1" runat="server">
					<table class="dataformV2">
				        <tr>
                            <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblPosdeliverypointId" />
							</td>
							<td class="control">
                                <U:UltraBoxInt runat="server" ID="ddlPosdeliverypointId" />
							</td>
							<td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblEnableAnalytics">Enable Analytics</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <D:CheckBox runat="server" ID="cbEnableAnalytics"/>
                            </td>
						</tr>                        
                    </table>
                </D:PlaceHolder>
                </controls>
            </X:TabPage>            								
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>

