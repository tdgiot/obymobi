﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.ClientLog" ValidateRequest="false" Codebehind="ClientLog.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server"></asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="cplhAdditionalToolBarButtons">
    <span class="toolbar">
        <D:Button runat="server" ID="btBack" Text="Go back" />	
    </span>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Log" Name="Log">
				<Controls>
				    <D:PlaceHolder runat="server" ID="phlNormalLog">
                    <table class="dataformV2">
	                    <tr>
		                    <td class="label">
			                    <D:LabelEntityFieldInfo runat="server" ID="lblCreated">Created</D:LabelEntityFieldInfo>
		                    </td>
		                    <td class="control">
			                    <D:Label ID="lblCreatedValue" runat="server" LocalizeText="false" />
		                    </td>
		                    <td class="label">
			                    <D:LabelEntityFieldInfo runat="server" ID="lblType">Type</D:LabelEntityFieldInfo>
		                    </td>
		                    <td class="control">
			                    <D:Label ID="lblTypeValue" runat="server" LocalizeText="false" />
		                    </td>
	                    </tr>
	                    <tr>
		                    <td class="label">
			                    <D:LabelEntityFieldInfo runat="server" ID="lblFromStatus">From Status</D:LabelEntityFieldInfo>
		                    </td>
		                    <td class="control">
			                    <D:Label ID="lblFromStatusValue" runat="server" LocalizeText="false" />
		                    </td>
		                    <td class="label">
			                    <D:LabelEntityFieldInfo runat="server" ID="lblToStatus">To Status</D:LabelEntityFieldInfo>
		                    </td>
		                    <td class="control">
			                    <D:Label ID="lblToStatusValue" runat="server" LocalizeText="false"></D:Label>
		                    </td>
	                    </tr>
	                    <tr>
		                    <td class="label">
			                    <D:LabelEntityFieldInfo runat="server" ID="lblFromOperationMode">From Operation Mode</D:LabelEntityFieldInfo>
		                    </td>
		                    <td class="control">
			                    <D:Label ID="lblFromOperationModeValue" runat="server" LocalizeText="false" />
		                    </td>
		                    <td class="label">
			                    <D:LabelEntityFieldInfo runat="server" ID="lblToOperationMode">To Operation Mode</D:LabelEntityFieldInfo>
		                    </td>
		                    <td class="control">
			                    <D:Label ID="lblToOperationModeValue" runat="server" LocalizeText="false"></D:Label>
		                    </td>
	                    </tr>
                        <tr>
		                    <td class="label">
			                    <D:LabelEntityFieldInfo runat="server" ID="lblMessage">Log</D:LabelEntityFieldInfo>
		                    </td>
		                    <td class="control" colspan="3">
			                    <D:TextBoxMultiLine ID="tbMessage" runat="server" LocalizeText="false" Rows="30" ReadOnly="true" BackColor="White" />
		                    </td>
	                    </tr>
                    </table>
                    </D:PlaceHolder>
                    <D:PlaceHolder runat="server" ID="phlShippedLog" Visible="false">
                        <table class="dataformV2">
	                    <tr>
		                    <td class="label">
		                        <D:Label runat="server" ID="lblLogCaption" LocalizeText="False">Log</D:Label>
		                    </td>
		                    <td class="control">
			                    <D:Label runat="server" ID="lblLogMessage" LocalizeText="False"></D:Label>
		                    </td>
		                    <td class="label">

		                    </td>
		                    <td class="control">

		                    </td>
	                    </tr>
                        <tr>
                            <td class="label">
                                <D:Label runat="server" ID="lblLogUrlCaption" LocalizeText="False">Url</D:Label>
		                    </td>
		                    <td class="control" colspan="3">
                                <D:HyperLink  runat="server" Target="_blank" ID="hlLogUrl" LocalizeText="True" TranslationTagText="btnDownloadLogText2"/>                                
		                    </td>
                            <td class="label">
		                    </td>
		                    <td class="control">
		                    </td>
                        </tr>                        
                        <tr>
                            <td class="label">                                
                            </td>
                            <td class="control">
                                <D:Button runat="server" ID="btnDownloadLog" Text="Download Log File" LocalizeText="True" TranslationTagText="btnDownloadLogText"/>                                
                            </td>
                            <td class="label">
                            </td>
                            <td class="control">
                            </td>
                        </tr>
                        </table>
                    </D:PlaceHolder>
				</Controls>
			</X:TabPage>					
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>