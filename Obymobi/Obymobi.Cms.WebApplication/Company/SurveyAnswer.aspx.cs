﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.RelationClasses;
using System.Text;
using System.Data;
using Obymobi.Data.DaoClasses;
using Dionysos;
using Dionysos.Web.UI.WebControls;
using Obymobi.Data;
using Dionysos.Data.LLBLGen;
using Dionysos.Web;
using Obymobi.Logic.HelperClasses;


namespace Obymobi.ObymobiCms.Company
{
	public partial class SurveyAnswer : Dionysos.Web.UI.PageLLBLGenEntity
	{
        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            this.DataSourceLoaded += new Dionysos.Delegates.Web.DataSourceLoadedHandler(SurveyAnswer_DataSourceLoaded);			
            base.OnInit(e);
        }

        private void SurveyAnswer_DataSourceLoaded(object sender)
        {
            // Set the question if a parameter is provided
            if (this.DataSourceAsSurveyAnswerEntity.IsNew)
            {
                int surveyQuestionId;
                if (QueryStringHelper.TryGetValue("SurveyQuestionId", out surveyQuestionId))
                {
                    if (surveyQuestionId > 0)
                        this.DataSourceAsSurveyAnswerEntity.SurveyQuestionId = surveyQuestionId;
                }
            }

            if (this.DataSourceAsSurveyAnswerEntity.SurveyQuestionId > 0)
                this.ddlSurveyQuestionId.Enabled = false;
        }

        private void LoadUserControls()
        {
         
        }

        #region Properties


        /// <summary>
        /// Return the page's datasource as a SurveyQuestionEntity
        /// </summary>
        public SurveyAnswerEntity DataSourceAsSurveyAnswerEntity
        {
            get
            {
                return this.DataSource as SurveyAnswerEntity;
            }
        }

        #endregion
	}
}
