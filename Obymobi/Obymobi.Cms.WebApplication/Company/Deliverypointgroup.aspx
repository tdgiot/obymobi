﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.Deliverypointgroup" Codebehind="Deliverypointgroup.aspx.cs" %>
<%@ Reference VirtualPath="~/Company/SubPanels/DeliverypointgroupCulturePanel.ascx" %>
<%@ Reference VirtualPath="~/Company/SubPanels/DeliverypointgroupEntertainmentCollection.ascx" %>
<%@ Reference VirtualPath="~/Company/SubPanels/DeliverypointgroupAdvertisementCollection.ascx" %>
<%@ Reference VirtualPath="~/Company/SubPanels/DeliverypointgroupAnnouncementCollection.ascx" %>
<%@ Reference VirtualPath="~/Company/SubPanels/DeliverypointCollection.ascx" %>
<%@ Reference VirtualPath="~/Company/SubPanels/DeliverypointgroupOccupancyPanel.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" runat="Server">
	<div>        
		<X:PageControl ID="tabsMain" runat="server" Width="100%">
			<TabPages>
				<X:TabPage Text="Generic" Name="Generic">
					<Controls>
					<table class="dataformV2">
						<tr> 
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblName">Name</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
							</td>
							<td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblMenuId">Menu</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlMenuId" IncrementalFilteringMode="StartsWith" EntityName="Menu" TextField="Name" ValueField="MenuId" IsRequired="true" PreventEntityCollectionInitialization="true" />
							</td>
					    </tr>
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblActive">Actief</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:CheckBox runat="server" ID="cbActive" />
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblAvailableOnObymobi">Crave mobile</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:CheckBox runat="server" ID="cbAvailableOnObymobi" />
							</td>
					    </tr>		
                        <tr>							
							<td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblRouteId">Route</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlRouteId" IncrementalFilteringMode="StartsWith" EntityName="Route" TextField="Name" ValueField="RouteId" PreventEntityCollectionInitialization="true" />
                            </td>				
                            <td class="label">
                                <D:Label runat="server" id="lblUIModeId" LocalizeText="true">UI mode venue owned devices</D:Label>
							</td>
							<td class="control">
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlUIModeId" IncrementalFilteringMode="StartsWith" EntityName="UIMode" TextField="Name" ValueField="UIModeId" PreventEntityCollectionInitialization="true" IsRequired="true" />
							</td>					                                			
					    </tr>			    
                        <tr>
                            <td class="label">
                                <D:Label runat="server" id="lblSystemMessageRouteId" LocalizeText="true">Systeem berichten route</D:Label>
                            </td>
                            <td class="control">
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlSystemMessageRouteId" IncrementalFilteringMode="StartsWith" EntityName="Route" TextField="Name" ValueField="RouteId" PreventEntityCollectionInitialization="true" />
                            </td>	
                            <td class="label">
                                <D:Label runat="server" id="lblMobileUIModeId" LocalizeText="true">UI mode guest owned mobile devices</D:Label>
							</td>
							<td class="control">
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlMobileUIModeId" IncrementalFilteringMode="StartsWith" EntityName="UIMode" TextField="Name" ValueField="UIModeId" PreventEntityCollectionInitialization="true" />
							</td>						
					    </tr>
                        <tr>
                            <td class="label">
                                <D:Label ID="lblEmailDocumentRouteId" runat="server">Email document route</D:Label>
                            </td>
                            <td class="control">
                                <X:ComboBoxLLBLGenEntityCollection ID="ddlEmailDocumentRouteId" runat="server" IncrementalFilteringMode="StartsWith" EntityName="Route" TextField="Name" ValueField="RouteId" PreventEntityCollectionInitialization="true"/>
                            </td>
                            <td class="label">
                                <D:Label runat="server" id="lblTabletUIModeId" LocalizeText="true">UI mode guest owned tablets</D:Label>
							</td>
							<td class="control">
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlTabletUIModeId" IncrementalFilteringMode="StartsWith" EntityName="UIMode" TextField="Name" ValueField="UIModeId" PreventEntityCollectionInitialization="true" />
							</td>
                        </tr>
                        <tr>
                            <td class="label">
                                <D:Label ID="lblOrderNotesRouteId" runat="server">Order with notes route</D:Label>
                            </td>
                            <td class="control">
                                <X:ComboBoxLLBLGenEntityCollection ID="ddlOrderNotesRouteId" runat="server" IncrementalFilteringMode="StartsWith" EntityName="Route" TextField="Name" ValueField="RouteId" PreventEntityCollectionInitialization="true"/>
                            </td>
                            <td class="label">
                                <D:Label ID="lblUIScheduleId" runat="server">UI schedule</D:Label>
							</td>
							<td class="control">
                                <X:ComboBoxLLBLGenEntityCollection ID="ddlUIScheduleId" runat="server" IncrementalFilteringMode="StartsWith" EntityName="UISchedule" TextField="Name" ValueField="UIScheduleId" PreventEntityCollectionInitialization="true"/>
							</td>
                        </tr>
                        <tr>
                            <td class="label">
                                <D:Label ID="lblClientConfiguration" runat="server">Client configuration</D:Label>
                            </td>
                            <td class="control">
                                <X:ComboBoxInt runat="server" ID="cbClientConfigurationId" CssClass="input" TextField="Name" ValueField="ClientConfigurationId" IncrementalFilteringMode="Contains" IncrementalFilteringDelay="1000"></X:ComboBoxInt>                                
                            </td>
                            <td class="label">
                                <D:Label ID="lblPriceScheduleId" runat="server">Price schedule</D:Label>
							</td>
							<td class="control">
                                <X:ComboBoxLLBLGenEntityCollection ID="ddlPriceScheduleId" runat="server" IncrementalFilteringMode="StartsWith" EntityName="PriceSchedule" TextField="Name" ValueField="PriceScheduleId" PreventEntityCollectionInitialization="true"/>
							</td>
                        </tr>
					 </table>			
					<D:Panel ID="allThingslondon" runat="server" GroupingText="All things London">
                        <table class="dataformV2">
                            <tr>
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="LabelEntityFieldInfo1">Campaign</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
                                    <X:ComboBoxInt runat="server" ID="cbAffiliateCampaignId" IncrementalFilteringMode="StartsWith" EntityName="Campaign" TextField="Name" ValueField="AffiliateCampaignId" />
							    </td>
                                <td class="label">
								    &nbsp;
							    </td>
							    <td class="control">
                                    &nbsp;
							    </td>                                
						    </tr>    
                        </table>
                    </D:Panel>
				</Controls>
		    </X:TabPage>
            <X:TabPage Text="Advanced" Name="tabAdvanced">
                <Controls>
                    <D:PlaceHolder runat="server" ID="plhPmsIntegration">
                        <D:Panel ID="pnlPmsSettings" runat="server" GroupingText="PMS instellingen">
                            <table class="dataformV2">
						        <tr>
							        <td class="label">
								        <D:LabelEntityFieldInfo runat="server" id="lblPmsIntegration">PMS integratie</D:LabelEntityFieldInfo>
							        </td>
							        <td class="control">
								        <D:CheckBox runat="server" ID="cbPmsIntegration" />
							        </td>
							        <td class="label">
								        <D:LabelEntityFieldInfo runat="server" id="lblPmsLockClientWhenNotCheckedIn">Blokkeren indien niet ingechecked</D:LabelEntityFieldInfo>
							        </td>
							        <td class="control">
								        <D:CheckBox runat="server" ID="cbPmsLockClientWhenNotCheckedIn" />
							        </td>	
					            </tr>
                                <tr>
							        <td class="label">
								        <D:LabelEntityFieldInfo runat="server" id="lblPmsAllowShowBill">Rekening tonen</D:LabelEntityFieldInfo>
							        </td>
							        <td class="control">
								        <D:CheckBox runat="server" ID="cbPmsAllowShowBill" />
							        </td>
							        <td class="label">
								        <D:LabelEntityFieldInfo runat="server" id="lblPmsAllowExpressCheckout">Express checkout</D:LabelEntityFieldInfo>
							        </td>
							        <td class="control">
								        <D:CheckBox runat="server" ID="cbPmsAllowExpressCheckout" />
							        </td>
						        </tr>
                                <tr>
                                    <td class="label">
								        <D:LabelEntityFieldInfo runat="server" id="lblPmsAllowShowGuestName">Show guest name</D:LabelEntityFieldInfo>
							        </td>
							        <td class="control">
								        <D:CheckBox runat="server" ID="cbPmsAllowShowGuestName" />
							        </td>
                                </tr>
                                <tr>
							        <td class="label">
								        <D:LabelEntityFieldInfo runat="server" id="lblPmsWelcomeTimeoutMinutes">PMS welkom dialoog minuten</D:LabelEntityFieldInfo>
							        </td>
							        <td class="control">
								        <D:TextBoxInt runat="server" ID="tbPmsWelcomeTimeoutMinutes" />
							        </td>
							       <td class="label">
								        <D:LabelEntityFieldInfo runat="server" id="lblPmsCheckoutCompleteTimeoutMinutes">PMS checkout gelukt dialoog minuten</D:LabelEntityFieldInfo>
							        </td>
							        <td class="control">
								        <D:TextBoxInt runat="server" ID="tbPmsCheckoutCompleteTimeoutMinutes" />
							        </td>
						        </tr>
                            </table>
                        </D:Panel>
                    </D:PlaceHolder>
                    <D:Panel ID="pnlHotSOS" runat="server" GroupingText="HotSOS">
                        <table class="dataformV2">
                            <tr>
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblHotSOSBatteryLowProductId">Battery low product</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection runat="server" ID="cbHotSOSBatteryLowProductId" IncrementalFilteringMode="StartsWith" EntityName="Product" TextField="Name" ValueField="ProductId" PreventEntityCollectionInitialization="true" />
							    </td>
                                <td class="label">
								    &nbsp;
							    </td>
							    <td class="control">
                                    &nbsp;
							    </td>                                
						    </tr>    
                        </table>
                    </D:Panel>
                    <D:Panel ID="pnlWifiSettings" runat="server" GroupingText="Wifi instellingen">
                        <table class="dataformV2">
                            <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblWifiSecurityMethod">Wifi ssid</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <X:ComboBoxInt runat="server" ID="ddlWifiSecurityMethod" DisplayEmptyItem="false"></X:ComboBoxInt>
							    </td>
					        </tr>
                            <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblWifiSsid">Wifi ssid</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <D:TextBoxString ID="tbWifiSsid" runat="server" IsRequired="false"></D:TextBoxString>
							    </td>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblWifiPassword">Wifi wachtwoord</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <D:TextBoxString ID="tbWifiPassword" runat="server" IsRequired="false"></D:TextBoxString>
							    </td>	
					        </tr>
                            <D:PlaceHolder runat="server" ID="plhWifiAdvanced">
                            <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblWifiEapMode">Eap mode</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <X:ComboBoxInt runat="server" ID="ddlWifiEapMode" DisplayEmptyItem="false"></X:ComboBoxInt>
							    </td>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblWifiPhase2Authentication">Phase 2 authentication</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <X:ComboBoxInt ID="ddlWifiPhase2Authentication" DisplayEmptyItem="false" runat="server" IsRequired="false"></X:ComboBoxInt>
							    </td>	
					        </tr>
                            <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblWifiIdentity">Identiteit</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <D:TextBoxString ID="tbWifiIdentity" runat="server" IsRequired="false"></D:TextBoxString>
							    </td>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblWifiAnonymousIdentify">Anonieme identiteit</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <D:TextBoxString ID="tbWifiAnonymousIdentify" runat="server" IsRequired="false"></D:TextBoxString>
							    </td>	
					        </tr>
                            </D:PlaceHolder>
                        </table>
                    </D:Panel>                 
                    <D:Panel ID="pnlCrave" runat="server" GroupingText="Crave">
                        <table class="dataformV2">
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblDefaultProductFullView">Standaard volledige product weergave</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:CheckBox runat="server" ID="cbDefaultProductFullView" />
                                </td>
                                <td class="label">
								    
							    </td>
                                <td class="control">
							        
							    </td>   			       						
                            </tr>
                            <tr>
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblHomepageSlideshowInterval">Home-pagina slideshow interval</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
							        <D:TextBoxInt runat="server" ID="tbHomepageSlideshowInterval" ThousandsSeperators="false" AllowNegative="false"></D:TextBoxInt>
							    </td>
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblRoomserviceCharge">Room service charge</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
							        <D:TextBoxDecimal runat="server" ID="tbRoomserviceCharge"></D:TextBoxDecimal>
							    </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblEstimatedDeliveryTime">Estimated delivery time (minutes)</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:TextBoxInt runat="server" ID="tbEstimatedDeliveryTime" ThousandsSeperators="False" AllowNegative="False" AllowZero="False"/>
                                </td>
                                <td class="label">                                    
                                </td>
                                <td class="control">                                    
                                </td>
                            </tr>
                        </table>
                    </D:Panel>
                    <D:Panel ID="pnlMisc" runat="server" GroupingText="Divers">
                        <table class="dataformV2">
                            <tr>
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblPosdeliverypointgroupId" />
							    </td>
							    <td class="control">
                                    <X:ComboBoxInt runat="server" ID="ddlPosdeliverypointgroupId" />
							    </td>
                                <td class="label">
		                            <D:LabelEntityFieldInfo runat="server" ID="lblDeliverypointCaption">Tafel naam</D:LabelEntityFieldInfo>
	                            </td>
	                            <td class="control">
		                            <D:TextBoxString runat="server" ID="tbDeliverypointCaption" UseDataBinding="false"></D:TextBoxString>
	                            </td> 
						    </tr>    
                            <tr>
						        <td class="label">
							        <D:LabelEntityFieldInfo runat="server" id="lblScreenTimeoutInterval">Scherm timeout (s)</D:LabelEntityFieldInfo>
						        </td>
						        <td class="control">
							        <D:TextBoxInt runat="server" ID="tbScreenTimeoutInterval"></D:TextBoxInt>
						        </td>       	    
                                <td class="label">
							        <D:Label runat="server" id="lblGooglePrinterId">Google printer</D:Label>
							    </td>
							    <td class="control">
                                    <X:ComboBoxString runat="server" ID="ddlGooglePrinterId" IncrementalFilteringMode="StartsWith" TextField="Name" ValueField="PrinterId"></X:ComboBoxString>
                                    <D:LinkButton ID="btSelectPrinter" runat="server" Text="Load Printers" LocalizeText="false" ForeColor="Black" />
                                    <D:LinkButton ID="btRefreshPrinters" runat="server" Text="Refresh Printers" LocalizeText="false" ForeColor="Black" />
							    </td>
					        </tr>
                            <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblResetTimeout">Reset na x minuten</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
							        <D:TextBoxInt runat="server" ID="tbResetTimeout"></D:TextBoxInt>
							    </td> 
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblClearSessionOnTimeout">Order wissen na reset</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
							        <D:CheckBox runat="server" ID="cbClearSessionOnTimeout" />
							    </td>        				       						
						    </tr>
                           <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblPincode">Pincode</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
							        <D:TextBoxString runat="server" ID="tbPincode" />
							    </td>        
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblPincodeGM">Pincode godmode</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
                                    <D:TextBoxString runat="server" ID="tbPincodeGM" />
							    </td>   				
						    </tr>
                            <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblPincodeSU">Pincode superuser</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
							        <D:TextBoxString runat="server" ID="tbPincodeSU" />
							    </td>       
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblLocale">Taal</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
							        <X:ComboBoxString runat="server" ID="ddlLocale">
                                    </X:ComboBoxString>
							    </td>  
						    </tr>
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblUseHardKeyboard">Bluetooth Keyboard</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <X:ComboBoxInt runat="server" ID="ddlUseHardKeyboard" DisplayEmptyItem="false"></X:ComboBoxInt>
                                </td>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblUIThemeId">Theme</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection runat="server" ID="cbUIThemeId" IncrementalFilteringMode="StartsWith" EntityName="UITheme" TextField="Name" ValueField="UIThemeId" PreventEntityCollectionInitialization="true" />
                                </td>
                            </tr>      
                            <tr>
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblBrowserAgeVerificationEnabled">Browser Age Verification</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <D:CheckBox runat="server" ID="cbBrowserAgeVerificationEnabled" />
							    </td>      
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblBrowserAgeVerificationLayout">Browser Age Verification Layout</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <X:ComboBoxInt runat="server" ID="ddlBrowserAgeVerificationLayout" DisplayEmptyItem="false"></X:ComboBoxInt>
                                </td>
                            </tr>    
                            <tr>
                                <td class="label">
                                    <D:Label runat="server" id="lblRestartApplicationDialogEnabled">Restart application dialog enabled</D:Label>
                                </td>
                                <td class="control">
                                    <D:CheckBox runat="server" ID="cbRestartApplicationDialogEnabled" />
                                </td>
                                <td class="label">
                                    <D:Label runat="server" id="lblRestartTimeoutSeconds">Restart timeout (seconds)</D:Label>
                                </td>
                                <td class="control">
                                    <D:TextBoxInt runat="server" ID="tbRestartTimeoutSeconds" ThousandsSeperators="False" AllowNegative="False" AllowZero="False"/>
                                </td>
                            </tr>                                              
                            <tr>
                                <td class="label">
                                    <D:Label runat="server" id="lblRebootDeviceDialogEnabled">Reboot device dialog enabled</D:Label>
                                </td>
                                <td class="control">
                                    <D:CheckBox runat="server" ID="cbRebootDeviceDialogEnabled" />
                                </td>                               
                                <td class="label">
                                    <D:Label runat="server" id="lblCraveAnalytics">Crave analytics</D:Label>
                                </td>
                                <td class="control">
                                    <D:CheckBox runat="server" ID="cbCraveAnalytics" />
                                </td>
                            </tr>
                            <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblGeoFencingEnabled">GeoFencing Enabled</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
							        <D:CheckBox runat="server" ID="chkGeoFencingEnabled" />
							    </td>
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblGeoFencingRadius">GeoFencing Radius</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <D:TextBoxInt runat="server" ID="tbGeoFencingRadius" ThousandsSeperators="False" Width="80%" /> Meter
							    </td>
					        </tr>
                        </table>
                    </D:Panel>
                    <D:Panel ID="pnlServiceVersions" runat="server" GroupingText="API Version" Visible="False">
                        <table class="dataformV2">
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblApiVersion">Use API v2</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:CheckBox runat="server" ID="cbApiVersionV2" />
                                </td>
                                <td class="label"></td>
                                <td class="control"></td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblMessagingVersion">Use AWS Messaging</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:CheckBox runat="server" ID="cbMessagingVersionV23" />
                                </td>
                                <td class="label"></td>
                                <td class="control"></td>
                            </tr>
                        </table>
                    </D:Panel>
				</controls>
			</X:TabPage>
            <X:TabPage Text="Copy" Name="Copy">
                <controls>
                    <table class="dataformV2">
                        <tr>
                           <td class="label">
                               <D:LabelTextOnly ID="lblNewDeliverypointgroupName" runat="server">Deliverypointgroup naam</D:LabelTextOnly>
                           </td> 
                            <td class="control">
                               <D:TextBoxString ID="tbNewDeliverypointgroupNameName" runat="server" notdirty="true"></D:TextBoxString>
                           </td>                             
                            <td class="label">
                               
                           </td> 
                            <td class="control">
                               <D:Button ID="btnCopyDeliverypointgroup" runat="server" Text="Kopiëren"/>
                           </td> 
                        </tr>
                    </table>
                </controls>
            </X:TabPage>
            <X:TabPage Text="Mobile" Name="Mobile">
				<controls>
					<table class="dataformV2">
                       <tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblOrderProcessingNotificationEnabled">Laat "in behandeling" notificaties zien</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
							    <D:CheckBox runat="server" ID="cbOrderProcessingNotificationEnabled" />
							</td>       
                            <td class="label">
							</td>
							<td class="control">
							</td>     
						</tr>
                        <tr>
                            <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblOrderProcessingNotificationTitle">In behandeling titel</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
							    <D:TextBoxString runat="server" ID="tbOrderProcessingNotificationTitle" UseDataBinding="false"></D:TextBoxString>
							</td>  	
                             <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblOrderProcessingNotification">In behandeling tekst</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
							    <D:TextBoxString runat="server" ID="tbOrderProcessingNotification" UseDataBinding="false"></D:TextBoxString>
							</td>  		
                        </tr>
                        <tr>
                            <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblServiceProcessingNotificationTitle">Service in behandeling titel</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
							    <D:TextBoxString runat="server" ID="tbServiceProcessingNotificationTitle" UseDataBinding="false"></D:TextBoxString>
							</td>  	
                            <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblServiceProcessingNotification">Service in behandeling tekst</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
							    <D:TextBoxString runat="server" ID="tbServiceProcessingNotification" UseDataBinding="false"></D:TextBoxString>
							</td>  				
						</tr>
                        <tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblOrderProcessedNotificationEnabled">Laat "voltooid" notificatie zien</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
							    <D:CheckBox runat="server" ID="cbOrderProcessedNotificationEnabled" />
							</td>     
                            <td class="label">
							</td>
							<td class="control">
							</td> 
                        </tr>  
                        <tr>
                            <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblOrderProcessedNotificationTitle">Voltooid titel</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
							    <D:TextBoxString runat="server" ID="tbOrderProcessedNotificationTitle" UseDataBinding="false"></D:TextBoxString>
							</td>  	
                            <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblOrderProcessedNotification">Voltooid tekst</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
							    <D:TextBoxString runat="server" ID="tbOrderProcessedNotification" UseDataBinding="false"></D:TextBoxString>
							</td>  				
						</tr>
                        <tr>
                            <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblServiceProcessedNotificationTitle">Service voltooid titel</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
							    <D:TextBoxString runat="server" ID="tbServiceProcessedNotificationTitle" UseDataBinding="false"></D:TextBoxString>
							</td>  	
                            <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblServiceProcessedNotification">Service voltooid tekst</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
							    <D:TextBoxString runat="server" ID="tbServiceProcessedNotification" UseDataBinding="false"></D:TextBoxString>
							</td>  				
						</tr>
                        <tr>
                            <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblFreeformMessageTitle">Vrije tekst bericht titel</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
							    <D:TextBoxString runat="server" ID="tbFreeformMessageTitle" UseDataBinding="false"></D:TextBoxString>
							</td>  	
                            <td class="label">
							</td>
							<td class="control">
							</td>  				
						</tr>
                         <tr>
                            <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblPreorderingEnabled">Voorbestelling toestaan</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
                                <D:CheckBox runat="server" ID="cbPreorderingEnabled" />
							</td>  	
                            <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblPreorderingActiveMinutes">Voorbestelling aantal minuten</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
							    <D:TextBoxInt runat="server" ID="tbPreorderingActiveMinutes"></D:TextBoxInt>
							</td>  				
						</tr>
                        <tr>
                            <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblAnalyticsOrderingVisible">Bestellingsstatistieken zichtbaar</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
                                <D:CheckBox runat="server" ID="cbAnalyticsOrderingVisible" />
							</td>  	
                            <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblAnalyticsBestsellersVisible">Best verkochte producten zichtbaar</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
							    <D:CheckBox runat="server" ID="cbAnalyticsBestsellersVisible" />
							</td>  				
						</tr>      
                        <tr>
                            <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblHideCompanyDetails">Hide company details</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
                                <D:CheckBox runat="server" ID="cbHideCompanyDetails" />
							</td>  	
                            <td class="label">
							</td>
							<td class="control">
							</td>  				
						</tr>                       
					 </table>	
				</controls>
			</X:TabPage>
            <X:TabPage Text="Power management" Name="PowerManagement2">
				<controls>
					<table class="dataformV2">
                        <tr>
                            <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblDailyOrderReset">Dagelijkse bestellingen reset</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
                                <X:TimeEdit runat="server" ID="teDailyOrderReset" UseDataBinding="false" IsRequired="True" />
							</td>  
                            <td class="label">
								
							</td>
							<td class="control">
			                    
							</td>
						</tr>
                        <tr>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblOutOfChargeLevel">Batterij leeg op %</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <D:TextBoxInt runat="server" ID="tbOutOfChargeLevel" allowZero="true"></D:TextBoxInt>
                            </td>
                            <td class="label">
                                
                            </td>
                            <td class="control">
                                
                            </td>
                        </tr>    
                    </table>
                    <D:Panel ID="pnlPowerSave" runat="server" GroupingText="Power Save">      
                        <table class="dataformV2">
                            <tr>
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblPowerSaveTimeout">Stroom besparing (minuten)</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
			                        <D:TextBoxInt runat="server" ID="tbPowerSaveTimeout" allowZero="true"></D:TextBoxInt>
							    </td>        
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblPowerSaveLevel">Stroom besparing helderheid</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
			                        <D:TextBoxInt runat="server" ID="tbPowerSaveLevel" allowZero="true"></D:TextBoxInt>
							    </td>        				
						    </tr>
                             <tr>
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblScreensaverMode">Screensaver</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
							        <X:ComboBoxInt runat="server" ID="ddlScreensaverMode" DisplayEmptyItem="false"></X:ComboBoxInt>
							    </td>  
                                <td class="label">
								
							    </td>
							    <td class="control">
			                    
							    </td>
						    </tr>
                        </table>
                    </D:Panel>   
                    <D:Panel ID="pnlNightMode" runat="server" GroupingText="Night Mode">      
                        <table class="dataformV2">
                            <tr>
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblSleepTime">Slaap modus tijd</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
                                    <X:TimeEdit runat="server" ID="teSleepTime" UseDataBinding="false" IsRequired="True" />
							    </td>  
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblWakeUpTime">Actieve modus tijd</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
                                    <X:TimeEdit runat="server" ID="teWakeUpTime" UseDataBinding="false" IsRequired="True" />
							    </td>  
						    </tr>      
                        </table>
                    </D:Panel>
                    <D:Panel ID="pnlBrightness" runat="server" GroupingText="Screen Brightness">      
                        <table class="dataformV2">
						    <tr>
                                <td class="label">
							    </td>
							    <td class="control">
							        <strong><D:Label runat="server" id="lblDeviceDefault" TranslationTag="lblDeviceDefault" LocalizeText="true">Default</D:Label></strong>
							    </td>  
                                <td class="label">
							    </td>
							    <td class="control">
							        <strong><D:Label runat="server" id="lblDeviceDocked" TranslationTag="lblDeviceDocked" LocalizeText="true">Docked</D:Label></strong>
							    </td>  
						    </tr>
                            <tr>
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblDimLevelBright">Helder helderheid %</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
							        <D:TextBoxInt runat="server" ID="tbDimLevelBright" allowZero="true"></D:TextBoxInt>
							    </td>
							
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblDockedDimLevelBright">Helder helderheid %</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
							        <D:TextBoxInt runat="server" ID="tbDockedDimLevelBright" allowZero="true"></D:TextBoxInt>
							    </td>
						    </tr>
						    <tr>
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblDimLevelMedium">Medium helderheid %</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
							        <D:TextBoxInt runat="server" ID="tbDimLevelMedium" allowZero="true"></D:TextBoxInt>
							    </td>  
							
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblDockedDimLevelMedium">Medium helderheid %</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
							        <D:TextBoxInt runat="server" ID="tbDockedDimLevelMedium" allowZero="true"></D:TextBoxInt>
							    </td> 
						    </tr>
						    <tr>
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblDimLevelDull">Doffe helderheid %</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
							        <D:TextBoxInt runat="server" ID="tbDimLevelDull" allowZero="true"></D:TextBoxInt>
							    </td> 
							
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblDockedDimLevelDull">Doffe helderheid %</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
							        <D:TextBoxInt runat="server" ID="tbDockedDimLevelDull" allowZero="true"></D:TextBoxInt>
							    </td> 
						    </tr>
                        </table>
                    </D:Panel>
                    <D:Panel ID="pnlScreenBehaviour" runat="server" GroupingText="Screen Power Behaviour">
                        <table class="dataformV2">
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblPowerButtonHardBehaviour">Hard power button</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <X:ComboBoxInt runat="server" ID="ddlPowerButtonHardBehaviour" DisplayEmptyItem="false"></X:ComboBoxInt>
                                </td>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblScreenOffMode">Screen off mode</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <X:ComboBoxInt runat="server" ID="ddlScreenOffMode" DisplayEmptyItem="false"></X:ComboBoxInt>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblPowerButtonSoftBehaviour">Soft power button</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <X:ComboBoxInt runat="server" ID="ddlPowerButtonSoftBehaviour" DisplayEmptyItem="false"></X:ComboBoxInt>
                                </td>
                                <td class="label">
                                    
                                </td>
                                <td class="control">
                                    
                                </td>
                            </tr>
                        </table>
                    </D:Panel>                           
				</controls>
			</X:TabPage>
            <X:TabPage Text="Service requests" Name="ServiceRequests">
				<controls>
					<D:PlaceHolder ID="PlaceHolder3" runat="server">													                
                    <table class="dataformV2">															
						<tr>
							<td class="label">
								<D:Label runat="server" id="lblServiceProduct1">Optie 1</D:Label>
							</td>
							<td class="control">
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlServiceProduct1" IncrementalFilteringMode="StartsWith" EntityName="Product" TextField="Name" ValueField="ProductId" PreventEntityCollectionInitialization="true" />							
							</td>
							<td class="label">
								&nbsp;
							</td>
							<td class="control">
								&nbsp;
							</td>        
						</tr>
	                    <tr>
							<td class="label">
								<D:Label runat="server" id="lblServiceProduct2">Optie 2</D:Label>
							</td>
							<td class="control">
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlServiceProduct2" IncrementalFilteringMode="StartsWith" EntityName="Product" TextField="Name" ValueField="ProductId" PreventEntityCollectionInitialization="true"/>
							</td>
							<td class="label">
								&nbsp;
							</td>
							<td class="control">
								&nbsp;
							</td>        
						</tr>
	                    <tr>
							<td class="label">
								<D:Label runat="server" id="lblServiceProduct3">Optie 3</D:Label>
							</td>
							<td class="control">
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlServiceProduct3" IncrementalFilteringMode="StartsWith" EntityName="Product" TextField="Name" ValueField="ProductId" PreventEntityCollectionInitialization="true"/>
							</td>
							<td class="label">
								&nbsp;
							</td>
							<td class="control">
								&nbsp;
							</td>        
						</tr>
	                    <tr>
							<td class="label">
								<D:Label runat="server" id="lblServiceProduct4">Optie 4</D:Label>
							</td>
							<td class="control">
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlServiceProduct4" IncrementalFilteringMode="StartsWith" EntityName="Product" TextField="Name" ValueField="ProductId" PreventEntityCollectionInitialization="true"/>
							</td>
							<td class="label">
								&nbsp;
							</td>
							<td class="control">
								&nbsp;
							</td>        
						</tr>
	                    <tr>
							<td class="label">
								<D:Label runat="server" id="lblServiceProduct5">Optie 5</D:Label>
							</td>
							<td class="control">
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlServiceProduct5" IncrementalFilteringMode="StartsWith" EntityName="Product" TextField="Name" ValueField="ProductId" PreventEntityCollectionInitialization="true"/>
							</td>
							<td class="label">
								&nbsp;
							</td>
							<td class="control">
								&nbsp;
							</td>        
						</tr>
	                    <tr>
							<td class="label">
								<D:Label runat="server" id="lblServiceProduct6">Optie 6</D:Label>
							</td>
							<td class="control">
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlServiceProduct6" IncrementalFilteringMode="StartsWith" EntityName="Product" TextField="Name" ValueField="ProductId" PreventEntityCollectionInitialization="true"/>
							</td>
							<td class="label">
								&nbsp;
							</td>
							<td class="control">
								&nbsp;
							</td>        
						</tr>
	                    <tr>
							<td class="label">
								<D:Label runat="server" id="lblServiceProduct7">Optie 7</D:Label>
							</td>
							<td class="control">
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlServiceProduct7" IncrementalFilteringMode="StartsWith" EntityName="Product" TextField="Name" ValueField="ProductId" PreventEntityCollectionInitialization="true"/>
							</td>
							<td class="label">
								&nbsp;
							</td>
							<td class="control">
								&nbsp;
							</td>        
						</tr>
	                    <tr>
							<td class="label">
								<D:Label runat="server" id="lblServiceProduct8">Optie 8</D:Label>
							</td>
							<td class="control">
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlServiceProduct8" IncrementalFilteringMode="StartsWith" EntityName="Product" TextField="Name" ValueField="ProductId" PreventEntityCollectionInitialization="true"/>
							</td>
							<td class="label">
								&nbsp;
							</td>
							<td class="control">
								&nbsp;
							</td>        
						</tr>
	                    <tr>
							<td class="label">
								<D:Label runat="server" id="lblServiceProduct9">Optie 9</D:Label>
							</td>
							<td class="control">
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlServiceProduct9" IncrementalFilteringMode="StartsWith" EntityName="Product" TextField="Name" ValueField="ProductId" PreventEntityCollectionInitialization="true"/>
							</td>
							<td class="label">
								&nbsp;
							</td>
							<td class="control">
								&nbsp;
							</td>        
						</tr>
	                    <tr>
							<td class="label">
								<D:Label runat="server" id="lblServiceProduct10">Optie 10</D:Label>
							</td>
							<td class="control">
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlServiceProduct10" IncrementalFilteringMode="StartsWith" EntityName="Product" TextField="Name" ValueField="ProductId" PreventEntityCollectionInitialization="true"/>
							</td>
							<td class="label">
								&nbsp;
							</td>
							<td class="control">
								&nbsp;
							</td>        
						</tr>
	                    <tr>
							<td class="label">
								<D:Label runat="server" id="lblServiceProduct11">Optie 11</D:Label>
							</td>
							<td class="control">
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlServiceProduct11" IncrementalFilteringMode="StartsWith" EntityName="Product" TextField="Name" ValueField="ProductId" PreventEntityCollectionInitialization="true"/>
							</td>
							<td class="label">
								&nbsp;
							</td>
							<td class="control">
								&nbsp;
							</td>        
						</tr>
	                    <tr>
							<td class="label">
								<D:Label runat="server" id="lblServiceProduct12">Optie 12</D:Label>
							</td>
							<td class="control">
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlServiceProduct12" IncrementalFilteringMode="StartsWith" EntityName="Product" TextField="Name" ValueField="ProductId" PreventEntityCollectionInitialization="true"/>
							</td>
							<td class="label">
								&nbsp;
							</td>
							<td class="control">
								&nbsp;
							</td>        
						</tr>
	                    <tr>
							<td class="label">
								<D:Label runat="server" id="lblServiceProduct13">Optie 13</D:Label>
							</td>
							<td class="control">
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlServiceProduct13" IncrementalFilteringMode="StartsWith" EntityName="Product" TextField="Name" ValueField="ProductId" PreventEntityCollectionInitialization="true"/>
							</td>
							<td class="label">
								&nbsp;
							</td>
							<td class="control">
								&nbsp;
							</td>        
						</tr>
	                    <tr>
							<td class="label">
								<D:Label runat="server" id="lblServiceProduct14">Optie 14</D:Label>
							</td>
							<td class="control">
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlServiceProduct14" IncrementalFilteringMode="StartsWith" EntityName="Product" TextField="Name" ValueField="ProductId" PreventEntityCollectionInitialization="true"/>
							</td>
							<td class="label">
								&nbsp;
							</td>
							<td class="control">
								&nbsp;
							</td>        
						</tr>
	                    <tr>
							<td class="label">
								<D:Label runat="server" id="lblServiceProduct15">Optie 15</D:Label>
							</td>
							<td class="control">
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlServiceProduct15" IncrementalFilteringMode="StartsWith" EntityName="Product" TextField="Name" ValueField="ProductId" PreventEntityCollectionInitialization="true"/>
							</td>
							<td class="label">
								&nbsp;
							</td>
							<td class="control">
								&nbsp;
							</td>        
						</tr>
                        </table>
					</D:PlaceHolder>
				</controls>
			</X:TabPage>
            <X:TabPage Text="Notifications" Name="Notifications">
				<controls>
                    <table class="dataformV2">
                        <tr>
	                        <td class="label">
		                        <D:LabelEntityFieldInfo runat="server" ID="lblOutOfChargeTitle">Batterij leeg titel</D:LabelEntityFieldInfo>
	                        </td>
	                        <td class="control">
		                        <D:TextBoxString runat="server" ID="tbOutOfChargeTitle" UseDataBinding="false" />
	                        </td>
                            <td class="label">
		                        <D:LabelEntityFieldInfo runat="server" ID="lblOutOfChargeMessage">Batterij leeg bericht</D:LabelEntityFieldInfo>
	                        </td>
	                        <td class="control">
		                        <D:TextBoxString runat="server" ID="tbOutOfChargeMessage" UseDataBinding="false" />
	                        </td>
	                    </tr>
                        <tr>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" ID="lblProductExtraCommentTitle">Product comment title</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <D:TextBoxString runat="server" ID="tbProductExtraCommentTitle" UseDataBinding="false" />
                            </td>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" ID="lblProductExtraCommentText">Product comment message</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <D:TextBoxString runat="server" ID="tbProductExtraCommentText" UseDataBinding="false" />
                            </td>
                        </tr>
                        <tr>
	                        <td class="label">
		                        <D:LabelEntityFieldInfo runat="server" ID="lblOrderProcessedTitle">Bestelling behandeld titel</D:LabelEntityFieldInfo>
	                        </td>
	                        <td class="control">
		                        <D:TextBoxString runat="server" ID="tbOrderProcessedTitle" UseDataBinding="false" />
	                        </td>
                            <td class="label">
		                        <D:LabelEntityFieldInfo runat="server" ID="lblOrderProcessedMessage">Bestelling behandeld bericht</D:LabelEntityFieldInfo>
	                        </td>
	                        <td class="control">
		                        <D:TextBoxString runat="server" ID="tbOrderProcessedMessage" UseDataBinding="false" />
	                        </td>
	                    </tr>
                        <tr>
	                        <td class="label">
		                        <D:LabelEntityFieldInfo runat="server" ID="lblOrderFailedTitle">Bestelling gefaald titel</D:LabelEntityFieldInfo>
	                        </td>
	                        <td class="control">
		                        <D:TextBoxString runat="server" ID="tbOrderFailedTitle" UseDataBinding="false" />
	                        </td>
                            <td class="label">
		                        <D:LabelEntityFieldInfo runat="server" ID="lblOrderFailedMessage">Bestelling gefaald bericht</D:LabelEntityFieldInfo>
	                        </td>
	                        <td class="control">
		                        <D:TextBoxString runat="server" ID="tbOrderFailedMessage" UseDataBinding="false" />
	                        </td>
	                    </tr>
                        <tr>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" ID="lvlOrderSavingMessage">Bestelling wordt opgeslagen bericht</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <D:TextBoxString runat="server" ID="tbOrderSavingMessage" UseDataBinding="false" />
                            </td>
	                        <td class="label">
		                        <D:LabelEntityFieldInfo runat="server" ID="lblOrderSavingTitle">Bestelling wordt opgeslagen titel</D:LabelEntityFieldInfo>
	                        </td>
	                        <td class="control">
		                        <D:TextBoxString runat="server" ID="tbOrderSavingTitle" UseDataBinding="false" />
	                        </td>
	                    </tr>
                        <tr>						
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblOrderCompletedEnabled">Bestelling voltooid verificatie toestaan</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
							    <D:CheckBox runat="server" ID="cbOrderCompletedEnabled" />
							</td>        
							<td class="label">
							</td>
							<td class="control">
							</td>        				
						</tr>
                        <tr>
	                        <td class="label">
		                        <D:LabelEntityFieldInfo runat="server" ID="lblOrderCompletedTitle">Bestelling voltooid verificatie titel</D:LabelEntityFieldInfo>
	                        </td>
	                        <td class="control">
		                        <D:TextBoxString runat="server" ID="tbOrderCompletedTitle" UseDataBinding="false" />
	                        </td>
                            <td class="label">
		                        <D:LabelEntityFieldInfo runat="server" ID="lblOrderCompletedMessage">Bestelling voltooid verificatie bericht</D:LabelEntityFieldInfo>
	                        </td>
	                        <td class="control">
		                        <D:TextBoxString runat="server" ID="tbOrderCompletedMessage" UseDataBinding="false" />
	                        </td>
	                    </tr>
                        <tr>
	                        <td class="label">
		                        <D:LabelEntityFieldInfo runat="server" ID="lblServiceSavingTitle">Service wordt opgeslagen titel</D:LabelEntityFieldInfo>
	                        </td>
	                        <td class="control">
		                        <D:TextBoxString runat="server" ID="tbServiceSavingTitle" UseDataBinding="false" />
	                        </td>
                            <td class="label">
		                        <D:LabelEntityFieldInfo runat="server" ID="lblServiceSavingMessage">Service wordt opgeslagen bericht</D:LabelEntityFieldInfo>
	                        </td>
	                        <td class="control">
		                        <D:TextBoxString runat="server" ID="tbServiceSavingMessage" UseDataBinding="false" />
	                        </td>
	                    </tr>
                        <tr>
	                        <td class="label">
		                        <D:LabelEntityFieldInfo runat="server" ID="lblServiceProcessedTitle">Service behandeld titel</D:LabelEntityFieldInfo>
	                        </td>
	                        <td class="control">
		                        <D:TextBoxString runat="server" ID="tbServiceProcessedTitle" UseDataBinding="false" />
	                        </td>
                            <td class="label">
		                        <D:LabelEntityFieldInfo runat="server" ID="lblServiceProcessedMessage">Service behandeld bericht</D:LabelEntityFieldInfo>
	                        </td>
	                        <td class="control">
		                        <D:TextBoxString runat="server" ID="tbServiceProcessedMessage" UseDataBinding="false" />
	                        </td>
	                    </tr>
                        <tr>
	                        <td class="label">
		                        <D:LabelEntityFieldInfo runat="server" ID="lblServiceFailedTitle">Service failed title</D:LabelEntityFieldInfo>
	                        </td>
	                        <td class="control">
		                        <D:TextBoxString runat="server" ID="tbServiceFailedTitle" UseDataBinding="false" />
	                        </td>
                            <td class="label">
		                        <D:LabelEntityFieldInfo runat="server" ID="lblServiceFailedMessage">Service failed message</D:LabelEntityFieldInfo>
	                        </td>
	                        <td class="control">
		                        <D:TextBoxString runat="server" ID="tbServiceFailedMessage" UseDataBinding="false" />
	                        </td>
	                    </tr>
                        <tr>
	                        <td class="label">
		                        <D:LabelEntityFieldInfo runat="server" ID="lblRatingSavingTitle">Waardering wordt opgeslagen titel</D:LabelEntityFieldInfo>
	                        </td>
	                        <td class="control">
		                        <D:TextBoxString runat="server" ID="tbRatingSavingTitle" UseDataBinding="false" />
	                        </td>
                            <td class="label">
		                        <D:LabelEntityFieldInfo runat="server" ID="lblRatingSavingMessage">Waardering wordt opgeslagen bericht</D:LabelEntityFieldInfo>
	                        </td>
	                        <td class="control">
		                        <D:TextBoxString runat="server" ID="tbRatingSavingMessage" UseDataBinding="false" />
	                        </td>
	                    </tr>
                        <tr>
	                        <td class="label">
		                        <D:LabelEntityFieldInfo runat="server" ID="lblRatingProcessedTitle">Waardering behandeld titel</D:LabelEntityFieldInfo>
	                        </td>
	                        <td class="control">
		                        <D:TextBoxString runat="server" ID="tbRatingProcessedTitle" UseDataBinding="false" />
	                        </td>
                            <td class="label">
		                        <D:LabelEntityFieldInfo runat="server" ID="lblRatingProcessedMessage">Waardering behandeld bericht</D:LabelEntityFieldInfo>
	                        </td>
	                        <td class="control">
		                        <D:TextBoxString runat="server" ID="tbRatingProcessedMessage" UseDataBinding="false" />
	                        </td>
	                    </tr>
                        <tr>
	                        <td class="label">
		                        <D:LabelEntityFieldInfo runat="server" ID="lblOrderingNotAvailableMessage">Bestellen niet mogelijk bericht</D:LabelEntityFieldInfo>
	                        </td>
	                        <td class="control">
		                        <D:TextBoxString runat="server" ID="tbOrderingNotAvailableMessage" UseDataBinding="false" />
	                        </td>
                            <td class="label">
	                        </td>
	                        <td class="control">
	                        </td>
	                    </tr>
                        <tr>
	                        <td class="label">
		                        <D:LabelEntityFieldInfo runat="server" ID="lblPmsDeviceLockedTitle">PMS apparaat vergrendeld titel</D:LabelEntityFieldInfo>
	                        </td>
	                        <td class="control">
		                        <D:TextBoxString runat="server" ID="tbPmsDeviceLockedTitle" UseDataBinding="false" />
	                        </td>
                            <td class="label">
		                        <D:LabelEntityFieldInfo runat="server" ID="lblPmsDeviceLockedText">PMS apparaat vergrendeld tekst</D:LabelEntityFieldInfo>
	                        </td>
	                        <td class="control">
		                        <D:TextBoxString runat="server" ID="tbPmsDeviceLockedText" UseDataBinding="false" />
	                        </td>
	                    </tr>
                        <tr>
	                        <td class="label">
		                        <D:LabelEntityFieldInfo runat="server" ID="lblPmsCheckinFailedTitle">PMS checkin mislukt titel</D:LabelEntityFieldInfo>
	                        </td>
	                        <td class="control">
		                        <D:TextBoxString runat="server" ID="tbPmsCheckinFailedTitle" UseDataBinding="false" />
	                        </td>
                            <td class="label">
		                        <D:LabelEntityFieldInfo runat="server" ID="lblPmsCheckinFailedText">PMS checkin mislukt tekst</D:LabelEntityFieldInfo>
	                        </td>
	                        <td class="control">
		                        <D:TextBoxString runat="server" ID="tbPmsCheckinFailedText" UseDataBinding="false" />
	                        </td>
	                    </tr>
                        <tr>
	                        <td class="label">
		                        <D:LabelEntityFieldInfo runat="server" ID="lblPmsRestartTitle">PMS restart titel</D:LabelEntityFieldInfo>
	                        </td>
	                        <td class="control">
		                        <D:TextBoxString runat="server" ID="tbPmsRestartTitle" UseDataBinding="false" />
	                        </td>
                            <td class="label">
		                        <D:LabelEntityFieldInfo runat="server" ID="lblPmsRestartText">PMS restart text</D:LabelEntityFieldInfo>
	                        </td>
	                        <td class="control">
		                        <D:TextBoxString runat="server" ID="tbPmsRestartText" UseDataBinding="false" />
	                        </td>
	                    </tr>
                        <tr>
	                        <td class="label">
		                        <D:LabelEntityFieldInfo runat="server" ID="lblPmsWelcomeTitle">PMS welkom titel</D:LabelEntityFieldInfo>
	                        </td>
	                        <td class="control">
		                        <D:TextBoxString runat="server" ID="tbPmsWelcomeTitle" UseDataBinding="false" />
	                        </td>
                            <td class="label">
		                        <D:LabelEntityFieldInfo runat="server" ID="lblPmsWelcomeText">PMS welkom tekst</D:LabelEntityFieldInfo>
	                        </td>
	                        <td class="control">
		                        <D:TextBoxString runat="server" ID="tbPmsWelcomeText" UseDataBinding="false" />
	                        </td>
	                    </tr>
                        <tr>
	                        <td class="label">
		                        <D:LabelEntityFieldInfo runat="server" ID="lblPmsCheckoutCompleteTitle">PMS checkout gelukt titel</D:LabelEntityFieldInfo>
	                        </td>
	                        <td class="control">
		                        <D:TextBoxString runat="server" ID="tbPmsCheckoutCompleteTitle" UseDataBinding="false" />
	                        </td>
                            <td class="label">
		                        <D:LabelEntityFieldInfo runat="server" ID="lblPmsCheckoutCompleteText">PMS checkout gelukt tekst</D:LabelEntityFieldInfo>
	                        </td>
	                        <td class="control">
		                        <D:TextBoxString runat="server" ID="tbPmsCheckoutCompleteText" UseDataBinding="false" />
	                        </td>
	                    </tr>
                         <tr>
	                        <td class="label">
		                        <D:LabelEntityFieldInfo runat="server" ID="lblPmsCheckoutApproveText">PMS checkout goedkeuring tekst</D:LabelEntityFieldInfo>
	                        </td>
	                        <td class="control">
		                        <D:TextBoxString runat="server" ID="tbPmsCheckoutApproveText" UseDataBinding="false" />
	                        </td>
                            <td class="label">
	                        </td>
	                        <td class="control">
	                        </td>
	                    </tr>
                        <tr>
                            <td class="label">
                                <D:Label runat="server" id="lblIsChargerRemovedDialogEnabled">Charger removed enabled</D:Label>
                            </td>
                            <td class="control">
                                <D:CheckBox runat="server" ID="cbIsChargerRemovedDialogEnabled" />
                            </td>
                            <td class="label">

                            </td>
                            <td class="control">

                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                <D:Label runat="server" id="lblChargerRemovedDialogTitle">Charger removed title</D:Label>
                            </td>
                            <td class="control">
                                <D:TextBoxString runat="server" ID="tbChargerRemovedDialogTitle" UseDataBinding="false"></D:TextBoxString>
                            </td>
                            <td class="label">
                                <D:Label runat="server" id="lblChargerRemovedDialogText">Charger removed text</D:Label>
                            </td>
                            <td class="control">
                                <D:TextBoxString runat="server" ID="tbChargerRemovedDialogText" UseDataBinding="false"></D:TextBoxString>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                <D:Label runat="server" id="lblTurnOffPrivacyTitle">Turn off privacy title</D:Label>
                            </td>
                            <td class="control">
                                <D:TextBoxString runat="server" ID="tbTurnOffPrivacyTitle" UseDataBinding="false"></D:TextBoxString>
                            </td>
                            <td class="label">
                                <D:Label runat="server" id="lblTurnOffPrivacyText">Turn off privacy text</D:Label>
                            </td>
                            <td class="control">
                                <D:TextBoxString runat="server" ID="tbTurnOffPrivacyText" UseDataBinding="false"></D:TextBoxString>
                            </td>
                        </tr>
                         <tr>
                            <td class="label">
                                <D:Label runat="server" id="lblItemCurrentlyUnavailableText">Item currently unavailable</D:Label>
                            </td>
                            <td class="control">
                                <D:TextBoxString runat="server" ID="tbItemCurrentlyUnavailableText" UseDataBinding="false"></D:TextBoxString>
                            </td>
                            <td class="label">
                            </td>
                            <td class="control">
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                <D:Label runat="server" id="lblAlarmSetWhileNotChargingTitle">Alarm set while not charging title</D:Label>
                            </td>
                            <td class="control">
                                <D:TextBoxString runat="server" ID="tbAlarmSetWhileNotChargingTitle" UseDataBinding="false"></D:TextBoxString>
                            </td>
                            <td class="label">
                                <D:Label runat="server" id="lblAlarmSetWhileNotChargingText">Alarm set while not charging text</D:Label>
                            </td>
                            <td class="control">
                                <D:TextBoxString runat="server" ID="tbAlarmSetWhileNotChargingText" UseDataBinding="false"></D:TextBoxString>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                <D:Label runat="server" id="lblIsOrderitemAddedEnabled">Orderitem added dialog enabled</D:Label>
                            </td>
                            <td class="control">
                                <D:CheckBox runat="server" ID="cbIsOrderitemAddedDialogEnabled" />
                            </td>
                            <td class="label">
                                <D:Label runat="server" id="lblIsClearBasketDialogEnabled">Clear basket dialog enabled</D:Label>
                            </td>
                            <td class="control">
                                <D:CheckBox runat="server" ID="cbIsClearBasketDialogEnabled" />
                            </td>
                        </tr>                        
                    </table>
                </controls>
			</X:TabPage>
			<X:TabPage Text="Crave" Name="Crave">
				<controls>
					<table class="dataformV2">
					    <tr>						
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblReorderNotificationEnabled">Opnieuw bestellen notificatie</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
							    <D:CheckBox runat="server" ID="cbReorderNotificationEnabled" />
							</td>        
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblReorderNotificationInterval">Notificatie interval (s)</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
							    <D:TextBoxInt runat="server" ID="tbReorderNotificationInterval" MinimalValue="0" MaximalValue="9999"></D:TextBoxInt>
							</td>        				
						</tr>
                        <tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblReorderNotificationAnnouncementId">Te tonen aankondiging</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
                                <X:ComboBoxInt runat="server" ID="cbReorderNotificationAnnouncementId" IncrementalFilteringMode="StartsWith" TextField="Title" ValueField="AnnouncementId" IsRequired="false"></X:ComboBoxInt>
							</td>  
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblDeviceActivationRequired">Handmatige activatie</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
							    <D:CheckBox runat="server" ID="cbDeviceActivationRequired" />
							</td>
						</tr>
					 </table>	
				</controls>
			</X:TabPage>
			</TabPages>
		</X:PageControl>        
	</div>
</asp:Content>
