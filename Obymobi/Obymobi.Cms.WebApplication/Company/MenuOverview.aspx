﻿<%@ Page Title="Menu overview" Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.MenuOverview" Codebehind="MenuOverview.aspx.cs" %>
<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v20.1" Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dxwtl" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitleHolder" Runat="Server">
    <D:Label runat="server" id="lblTitle">Menu overzicht</D:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhToolbarHolder" Runat="Server">
    <span class="toolbar">
        <X:ToolBarButton runat="server" ID="btFilter" CommandName="Filter" Text="Apply filter"
            Image-Url="~/Images/Icons/table_refresh.png" />
        <X:ToolBarButton runat="server" ID="btCollapse" CommandName="CollapseCategories" Text="Collapse categories"
            Image-Url="~/Images/Icons/arrow_up.png" />
    </span>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhContentHolder" Runat="Server">
    <script type="text/javascript">
        function OnNodeHover(position, status, missing) {
            var divProductInfo = document.getElementById("divProductInfo");

            divProductInfo.innerHTML = "";

            // Set the status of this product
            divProductInfo.innerHTML += "Status: " + status + "<br />";

            // Check if missing is set
            if (missing.indexOf(":") != -1) {
                var values = missing.split(":");
                divProductInfo.innerHTML += "Missing: <br />";
                for (var i = 0; i < values.length; i++){
                    divProductInfo.innerHTML += values[i] + "<br />";
                }
            }

            var curLeftBox = 0;
            var curTopBox = 0;

            do {
                curLeftBox += position.offsetLeft;
                curTopBox += position.offsetTop;
            } while (position = position.offsetParent);

            divProductInfo.style.top = curTopBox + 20 + "px";
            divProductInfo.style.left = curLeftBox + 6 + "px";

            divProductInfo.style.display = "block";
            divProductInfo.style.visibility = "visible";
        }

        function OnCancelNodeHover() {
            var divProductInfo = document.getElementById("divProductInfo");
            divProductInfo.style.display = "none";
            divProductInfo.style.visibility = "hidden";
        }
    </script>
    <div id="divProductInfo">
    </div>
    <X:PageControl ID="pcOverview" runat="server" Width="100%">
        <TabPages>
            <X:TabPage Text="Menu overzicht" Name="tpOverview">
                <Controls>
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:Label runat="server" ID="lblCompanyName">Bedrijf</D:Label>
                            </td>
                            <td class="control">
                                <D:CheckBox ID="cbMenu" runat="server" Text="Menu" CssClass="menuOverviewOption" Checked="true" />
                                <D:CheckBox ID="cbServiceRequests" runat="server" Text="Service artikelen" CssClass="menuOverviewOption"/>
                                <D:CheckBox ID="cbPaymentmethods" runat="server" Text="Betaalwijzen" CssClass="menuOverviewOption"/>
                                <D:CheckBox ID="cbAdvertisements" runat="server" Text="Advertenties" CssClass="menuOverviewOption"/>
                                <D:CheckBox ID="cbSurveys" runat="server" Text="Enquête" CssClass="menuOverviewOption"/>
                                <D:CheckBox ID="cbProductsFrontpage" runat="server" Text="Producten op home-pagina" CssClass="menuOverviewOption"/>
                                <D:CheckBox ID="cbEntertainment" runat="server" Text="Amusement" CssClass="menuOverviewOption"/>
                                <!--<D:CheckBox ID="cbPictures" runat="server" Text="Media" CssClass="menuOverviewOption"/>-->
                                <D:CheckBox ID="cbProductType" runat="server" Text="Product type" CssClass="menuOverviewOption"/>
                            </td>
                            <td class="label">
                                <D:Label runat="server" ID="lblSelectionType">Selectie</D:Label>
                            </td>
                            <td class="control">
                                <D:RadioButtonList ID="rblSelectionType" runat="server">
                                    <asp:ListItem Text="No selection" Selected="true"></asp:ListItem>
                                    <asp:ListItem Text="Complete"></asp:ListItem>
                                    <asp:ListItem Text="Incomplete"></asp:ListItem>
                                </D:RadioButtonList>
                            </td>
                        </tr>
                    </table>

                    <X:TreeList ID="tlMenuOverview" runat="server" AutoGenerateColumns="False" Width="100%" OnHtmlRowPrepared="tlMenuOverview_HtmlRowPrepared"
                        onhtmldatacellprepared="tlMenuOverview_HtmlDataCellPrepared">
                        <settingsbehavior autoexpandallnodes="True" />
                        <Settings GridLines="Horizontal" />
                        <Settings ShowColumnHeaders="true" />
                    </X:TreeList> 
                </Controls>
            </X:TabPage>
        </TabPages>
    </X:PageControl>
</asp:Content>
