﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dionysos.Web.UI;
using Obymobi.Data.HelperClasses;
using System.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using Dionysos;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using Obymobi.Logic.Routing;
using Dionysos.Web.Security;

namespace Obymobi.ObymobiCms.Company
{
    public partial class AddCompany : PageDefault
    {
        #region Methods

        private void HookupEvents()
        {
            this.btAddCompany.Click += this.btAddCompany_Click;
        }

        private void CreateCompany()
        {
            List<string> cultureCodes = this.lbCompanyCultures.SelectedValuesAsStringList;

            if (this.ddlCultureCode.Value != null)
            {
                if (!cultureCodes.Contains(this.ddlCultureCode.Value.ToString()))
                {
                    cultureCodes.Add(this.ddlCultureCode.Value.ToString());
                }
            }

            CompanyGeneratorSettings settings = new CompanyGeneratorSettings
            {
                Username = this.tbUsername.Value,
                Password = this.tbPasswordDecrypted.Value,
                CompanyName = this.tbName.Value,
                Zipcode = this.tbZipcode.Value,
                CountryCode = this.ddlCountry.SelectedValueString,
                CultureCode = this.ddlCultureCode.SelectedValueString,
                CurrencyCode = this.ddlCurrencyCode.SelectedValueString,
                TimeZoneOlsonId = this.ddlTimeZoneOlsonId.SelectedValueString,
                SupportpoolId = this.ddlSupportpoolId.Value,
                CompanyCultures = cultureCodes,
                CreateTerminal = this.cbCreateTerminal.Checked,
                TerminalName = this.tbTerminalName.Value,
                TerminalType = this.ddlTerminalType.ValidId,
                CreateDeliverypointgroup = this.cbCreateDeliverypointgroup.Checked,
                DeliverypointgroupName = this.tbDeliverypointgroupName.Value,
                Pincode = this.tbPincode.Value,
                PincodeGodmode = this.tbPincodeGM.Value,
                PincodeSuperuser = this.tbPincodeSU.Value,
                UIThemeId = this.cbUITheme.ValidId,
                CreateRoute = this.cbCreateRoute.Checked,
                RoutestephandlerType = this.ddlRoutestephandlerType.ValidId,
                CreateDeliverypoints = this.cbCreateDeliverypoints.Checked,
                RangeStart = this.tbRangeStart.Value.GetValueOrDefault(0),
                RangeEnd = this.tbRangeEnd.Value.GetValueOrDefault(0),
                ApiVersion = int.Parse(this.ddlApiVersion.SelectedValue),
                MessagingVersion = int.Parse(this.ddlMessagingVersion.SelectedValue)
            };

            if (!this.IsValid || !this.ValidateSettings(settings))
            {
                this.Validate();
                return;
            }

            string message;
            int companyId;

            CompanyGenerator generator = new CompanyGenerator(settings);
            if (!generator.Generate(out message, out companyId))
            {
                this.MultiValidatorDefault.AddError("Error occurred while generating company: " + message);
                this.Validate();
            }
            else
            {
                this.ClearCompanyListFromCache();
                CmsSessionHelper.CurrentCompanyId = companyId;
                this.Response.Redirect("~/Company/Company.aspx?id=" + companyId);
            }
        }        

        #endregion

        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            this.ddlRoutestephandlerType.DataBindEnumStringValuesAsDataSource(typeof(RoutestephandlerType));
            this.ddlRoutestephandlerType.SelectedIndex = ddlRoutestephandlerType.Items.IndexOfText(RoutestephandlerType.Console.GetStringValue());
            this.ddlRoutestephandlerType.Enabled = false;

            this.ddlTerminalType.DataBindEnumStringValuesAsDataSource(typeof(TerminalType));
            this.ddlTerminalType.SelectedIndex = 0;

            this.ddlCountry.DataSource = Obymobi.Country.Mappings.Values.OrderBy(x => x.Name);
            this.ddlCountry.DataBind();

            this.ddlCurrencyCode.DataSource = Obymobi.Currency.Mappings.Values.OrderBy(x => x.Name);
            this.ddlCurrencyCode.DataBind();

            this.ddlTimeZoneOlsonId.DataSource = Obymobi.TimeZone.Mappings.Values.OrderByDescending(x => x.UtcOffset);
            this.ddlTimeZoneOlsonId.DataBind();

            this.cbUITheme.DataBindEntityCollection<UIThemeEntity>(UIThemeFields.CompanyId == DBNull.Value, UIThemeFields.Name, UIThemeFields.Name);

            IEnumerable<Obymobi.Culture> cultures = Obymobi.Culture.Mappings.Values.OrderBy(x => x.ToString());
            foreach (Obymobi.Culture culture in cultures)
            {
                this.ddlCultureCode.Items.Add(culture.ToString(), culture.Code);
            }

            this.lbCompanyCultures.DataSource = cultures;
            this.lbCompanyCultures.DataBind();

            this.btAddCompany.PreSubmitWarning = this.Translate("PreSubmitWarning", "Weet u zeker dat u het bedrijf wilt aanmaken?");

            this.ddlApiVersion.Items.Add(new ListItem("Legacy API (v1)", "1"));
            this.ddlApiVersion.Items.Add(new ListItem("REST API (v2)", "2"));

            this.ddlMessagingVersion.Items.Add(new ListItem("Legacy Messaging (v1)", "1"));

            if (CmsSessionHelper.CurrentRole == Role.GodMode)
            {
                this.ddlMessagingVersion.Items.Add(new ListItem("AWS IoT (v3)", "3"));
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {               
            this.HookupEvents();
        }

        private void btAddCompany_Click(object sender, EventArgs e)
        {
            this.CreateCompany();
        }

        private bool ValidateSettings(CompanyGeneratorSettings settings)
        {
            bool success = true;

            if (settings.Username.StartsWith("test") || settings.Username.StartsWith("dev") || settings.Username.StartsWith("man"))
            {
                this.MultiValidatorDefault.AddError(this.Translate("CompanyOwnerUsernameInvalid", "The username can not start with 'man', 'dev' or 'test'"));
                success = false;
            }
            if (settings.Password.Length < 8)
            {
                this.MultiValidatorDefault.AddError(this.Translate("CompanyOwnerPasswordTooShortt", "You need to supply a password of at least 8 characters."));
                success = false;
            }

            if (settings.CreateTerminal)
            {
                if (settings.TerminalName.IsNullOrWhiteSpace())
                {
                    this.MultiValidatorDefault.AddError("Please enter a name for the terminal.");
                    success = false;
                }

                if (settings.TerminalType <= 0)
                {
                    this.MultiValidatorDefault.AddError("Please select a terminal type.");
                    success = false;
                }

                if (settings.CreateRoute)
                {
                    if (settings.RoutestephandlerType <= 0)
                    {
                        this.MultiValidatorDefault.AddError("Please select a routestep handler type.");
                        success = false;
                    }
                    else if (!RoutingHelper.IsRoutestepHandlerTypeCompatibleWithTerminalType(settings.RoutestephandlerType.ToEnum<RoutestephandlerType>(), settings.TerminalType.ToEnum<TerminalType>()))
                    {
                        this.MultiValidatorDefault.AddError(this.Translate("ConfigurationTerminalTypeAndHandlerTypeNotValid", "De gekozen combinatie van Terminal Type en Afhandelingswijze is niet mogelijk."));
                        success = false;
                    }
                }
            }
            else
            {
                if (settings.CreateRoute)
                {
                    if (settings.RoutestephandlerType <= 0)
                    {
                        this.MultiValidatorDefault.AddError("Please select a routestep handler type.");
                        success = false;
                    }
                }
            }

            if (settings.CreateDeliverypointgroup)
            {
                if (settings.DeliverypointgroupName.IsNullOrWhiteSpace())
                {
                    this.MultiValidatorDefault.AddError("Please enter a name for the deliverypoint group.");
                    success = false;
                }

                if (settings.UIThemeId.GetValueOrDefault(0) <= 0)
                {
                    this.MultiValidatorDefault.AddError("Please select an UI theme.");
                    success = false;
                }

                if (settings.Pincode.IsNullOrWhiteSpace())
                {
                    this.MultiValidatorDefault.AddError("Please enter a pincode.");
                    success = false;
                }
                else if (settings.Pincode.Length < 4)
                {
                    this.MultiValidatorDefault.AddError("Please enter a pincode with at least 4 characters.");
                    success = false;
                }

                if (settings.PincodeGodmode.IsNullOrWhiteSpace())
                {
                    this.MultiValidatorDefault.AddError("Please enter a pincode for godmode access.");
                    success = false;
                }
                else if (settings.PincodeGodmode.Length < 4)
                {
                    this.MultiValidatorDefault.AddError("Please enter a pincode for godmode access with at least 4 characters.");
                    success = false;
                }

                if (settings.PincodeSuperuser.IsNullOrWhiteSpace())
                {
                    this.MultiValidatorDefault.AddError("Please enter a pincode for superuser access.");
                    success = false;
                }
                else if (settings.PincodeSuperuser.Length < 4)
                {
                    this.MultiValidatorDefault.AddError("Please enter a pincode for superuser access with at least 4 characters.");
                    success = false;
                }

                if (settings.CreateDeliverypoints && settings.RangeStart > settings.RangeEnd)
                {
                    this.MultiValidatorDefault.AddError("The start range of the deliverypoints must be smaller than end range.");
                    success = false;
                }
            }                        

            return success;
        }

        private void ClearCompanyListFromCache()
        {
            UserEntity user = UserManager.CurrentUser as UserEntity;
            if (user != null)
            {
                string cacheKey = "Obymobi.Cms.CompanyCollection";

                if (user.AccountId.HasValue)
                    cacheKey += string.Format("-{0}", user.AccountId.Value);

                Dionysos.Web.CacheHelper.Remove(false, cacheKey);
            }
        }

        #endregion
    }
}