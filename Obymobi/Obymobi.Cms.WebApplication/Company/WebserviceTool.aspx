﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.WebserviceTool" Title="Webservice Tool" Codebehind="WebserviceTool.aspx.cs" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cplhTitleHolder">Webservice Tool</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="cplhToolbarHolder">        
    <X:ToolBarButton runat="server" ID="btCall" CommandName="Show" Text="Call Webservice (View)" Image-Url="~/images/icons/table.png" style="margin-left: 3px;float:left;"></X:ToolBarButton>
    <X:ToolBarButton runat="server" ID="btDownload" CommandName="Download" Text="Call Webservice (Download)" Image-Url="~/images/icons/table_save.png" style="margin-left: 3px;float:left"></X:ToolBarButton>    
</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="cplhContentHolder">
<div>
	<X:PageControl ID="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:Label ID="lblClient" runat="server">Client</D:Label>
                            </td>
                            <td class="label">
                                <X:ComboBoxInt runat="server" ID="cbClient"></X:ComboBoxInt>
                            </td>
                            <td class="label">
                                <D:Label ID="lblTerminal" runat="server">Terminal</D:Label>
                            </td>
                            <td class="label">
                                <X:ComboBoxInt runat="server" ID="cbTerminal"></X:ComboBoxInt>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                <D:Label ID="lblUsername" runat="server">Gebruikersnaam</D:Label>
                            </td>
                            <td class="label">
                                <D:TextBox runat="server" ID="tbUsername"></D:TextBox>
                            </td>
                            <td class="label">
                                <D:Label ID="lblPassword" runat="server">Wachtwoord</D:Label>
                            </td>
                            <td class="label">
                                <D:TextBoxPassword runat="server" ID="tbPassword"></D:TextBoxPassword>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                <D:Label ID="lblDeliverypointId" runat="server">Tafel</D:Label>
                            </td>
                            <td class="label">
                                <X:ComboBoxInt runat="server" ID="cbDeliverypointId"></X:ComboBoxInt>
                            </td>
                            <td class="label">
                                <D:Label ID="lblSiteId" runat="server">Site</D:Label>
                            </td>
                            <td class="label">
                                <X:ComboBoxInt runat="server" ID="cbSiteId"></X:ComboBoxInt>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                <D:Label ID="lblApplication" runat="server">Applicatie</D:Label>
                            </td>
                            <td class="label">
                                <X:ComboBox runat="server" ID="cbApplication"></X:ComboBox>
                            </td>
                            <td class="label">
                                <D:Label ID="lblOrderGuid" runat="server">Order Guid</D:Label>
                            </td>
                            <td class="label">
                                <D:TextBoxString runat="server" ID="tbOrderGuid"></D:TextBoxString>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                <D:Label ID="lblNetmessageId" runat="server">NetmessageId</D:Label>
                            </td>
                            <td class="label">
                                <D:TextBoxInt runat="server" ID="tbNetmessageId" ThousandsSeperators="false"></D:TextBoxInt>
                            </td>
                            <td class="label">
                                <D:Label ID="lblNetmessageGuid" runat="server">Netmessage Guid</D:Label>
                            </td>
                            <td class="label">
                                <D:TextBox runat="server" ID="tbNetmessageGuid"></D:TextBox>
                            </td>
                        </tr>                        
                        <tr>
                            <td class="label">
                                <D:Label ID="lblWebserviceMethod" runat="server">Webservice method</D:Label>
                            </td>
                            <td class="label">
                                <X:ComboBox runat="server" ID="cbMethod"></X:ComboBox>
                            </td>                                
                            <td class="label">
                                <D:Label ID="lblCultureCode" runat="server">Culture</D:Label>
                            </td>
                            <td class="label">
                                <X:ComboBox runat="server" ID="ddlCultureCode"></X:ComboBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="label"><D:Label ID="lblWebserviceUrl" runat="server">Webservice URL</D:Label></td>
                            <td class="label"><D:TextBoxString runat="server" ID="tbWebserviceUrl"></D:TextBoxString></td>
                            <td class="label"></td>
                            <td class="label"></td>
                        </tr>
                    </table> 
                </Controls>
            </X:TabPage>
        </TabPages>
    </X:PageControl>
    <pre><D:PlaceHolder runat="server" ID="plhReponse"></D:PlaceHolder></pre>
</div>
</asp:Content>