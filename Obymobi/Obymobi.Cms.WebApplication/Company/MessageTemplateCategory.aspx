﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.MessageTemplateCategory" Codebehind="MessageTemplateCategory.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" runat="Server">
<div>
	<X:PageControl ID="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Generic" Name="Generic">
				<Controls>
                <table class="dataformV2">
					<tr>
						<td class="label">
							<D:LabelEntityFieldInfo runat="server" id="lblName">Name</D:LabelEntityFieldInfo>
						</td>
						<td class="control">
							<D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
						</td>
						<td class="label"></td>
						<td class="control"></td>
				    </tr>
                </table>
                <D:Panel ID="pnlMessageTemplates" runat="server" GroupingText="Message Templates" Visible="False">
                    <D:PlaceHolder ID="plhMessageTemplates" runat="server" />
                </D:Panel>
                </Controls>
            </X:TabPage>
        </TabPages>
    </X:PageControl>
</div>
</asp:Content>