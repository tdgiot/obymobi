﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.Routestephandler" ValidateRequest="false" CodeBehind="Routestephandler.aspx.cs" %>

<%@ Reference Control="~/Company/SubPanels/RoutestephandlerSettingsPanels/EmailOrderSettingsPanel.ascx" %>
<%@ Reference Control="~/Company/SubPanels/RoutestephandlerSettingsPanels/PosSettingsPanel.ascx" %>
<%@ Reference Control="~/Company/SubPanels/RoutestephandlerSettingsPanels/PrintSettingsPanel.ascx" %>
<%@ Reference Control="~/Company/SubPanels/RoutestephandlerSettingsPanels/SmsSettingsPanel.ascx" %>
<%@ Reference Control="~/Company/SubPanels/RoutestephandlerSettingsPanels/HotSOSSettingsPanel.ascx" %>
<%@ Reference Control="~/Company/SubPanels/RoutestephandlerSettingsPanels/QuoreSettingsPanel.ascx" %>
<%@ Reference Control="~/Company/SubPanels/RoutestephandlerSettingsPanels/HyattSettingsPanel.ascx" %>
<%@ Reference Control="~/Company/SubPanels/RoutestephandlerSettingsPanels/ExternalSystemSettingsPanel.ascx" %>
<%@ Reference Control="~/Company/SubPanels/RoutestephandlerSettingsPanels/EmailDocumentSettingsPanel.ascx" %>

<%@ Reference VirtualPath="~/Generic/UserControls/CustomTextCollection.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" runat="Server">
    <div>
        <X:PageControl ID="tabsMain" runat="server" Width="100%">
            <TabPages>
                <X:TabPage Text="Algemeen" Name="Generic">
                    <controls>
                        <D:PlaceHolder runat="server">
                            <table class="dataformV2">
                                <tr>
                                   <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" id="lblHandlerType">Handler Type</D:LabelEntityFieldInfo>
                                   </td>
                                   <td class="control">
                                        <X:ComboBoxInt ID="ddlHandlerType" runat="server" UseDataBinding="true" IsRequired="true" DisabledOnceSelectedAndSaved="true" DisabledOnceSelectedFromQueryString="true"></X:ComboBoxInt>
                                   </td>

                                   <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" id="lblRoutestepId">Route step</D:LabelEntityFieldInfo>
                                   </td>
                                   <td class="control">
                                       <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlRoutestepId"  UseDataBinding="true" IncrementalFilteringMode="StartsWith" EntityName="Routestep" TextField="Number" ValueField="RoutestepId" IsRequired="true" DisabledOnceSelectedAndSaved="true" DisabledOnceSelectedFromQueryString="true" PreventEntityCollectionInitialization="true" />
                                   </td>
                                </tr>

                                <D:PlaceHolder ID="plhTerminal" runat="server" Visible="false">
                                    <tr>
                                       <td class="label">
                                            <D:LabelEntityFieldInfo runat="server" id="lblTerminalId">Terminal</D:LabelEntityFieldInfo>
                                       </td>
                                       <td class="control">
                                           <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlTerminalId" IncrementalFilteringMode="StartsWith" EntityName="Terminal" TextField="Name" ValueField="TerminalId" PreventEntityCollectionInitialization="true" />
                                       </td>

                                        <td class="label">
                                            <D:LabelEntityFieldInfo runat="server" id="lblPrintReportType" Visible="false">Print Report Type</D:LabelEntityFieldInfo>
                                        </td>
                                        <td class="control">
                                            <X:ComboBoxInt ID="ddlPrintReportType" runat="server" UseDataBinding="true" Visible="false"></X:ComboBoxInt>
                                        </td>
                                   </tr>
                                </D:PlaceHolder>

                                <D:PlaceHolder ID="plhSupportpool" runat="server" Visible="false">
                                   <tr>
                                        <td class="label">
                                           <D:LabelEntityFieldInfo runat="server" id="lblSupportpoolId">Support pool</D:LabelEntityFieldInfo>
                                        </td>
                                        <td class="control">
                                           <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlSupportpoolId" EntityName="Supportpool"></X:ComboBoxLLBLGenEntityCollection>
                                        </td>

                                       <td class="label"></td>
                                       <td class="control"></td>
                                   </tr>
                                </D:PlaceHolder>

                                <D:PlaceHolder runat="server" ID="plhHandlerTypeSpecificSettings"></D:PlaceHolder>
                            </table>
                        </D:PlaceHolder>
                    </controls>
                </X:TabPage>
            </TabPages>
        </X:PageControl>
    </div>
</asp:Content>
