﻿using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.ObymobiCms.Company.SubPanels;
using Obymobi.Enums;
using Dionysos.Web;
using Dionysos.Data.LLBLGen;
using Obymobi.Data;
using Obymobi.Logic.HelperClasses;
using DevExpress.Web;
using Dionysos;
using Obymobi.Cms.Logic.HelperClasses;
using MediaCollection = Obymobi.ObymobiCms.Generic.UserControls.MediaCollection;

namespace Obymobi.ObymobiCms.Company
{
	public partial class UIWidget : Dionysos.Web.UI.PageLLBLGenEntity
    {
        #region Properties

	    private UIWidgetCultureCollection uiWidgetCultureCollection = null;
	    private MediaCollection mediaCollection = null;

        #endregion

        #region Methods

        private void LoadUserControls()
        {
            UIWidgetEntity uiWidgetEntity = new UIWidgetEntity(this.EntityId);
            if (uiWidgetEntity.IsNew)
            {
                return;
            }

            this.uiWidgetCultureCollection = this.tabsMain.AddTabPage("Translations", "UIWidgetCultureCollection", "~/Company/SubPanels/UIWidgetCultureCollection.ascx", "Generic") as UIWidgetCultureCollection;

            this.mediaCollection = this.tabsMain.AddTabPage("Afbeeldingen", "Media", "~/Generic/UserControls/MediaCollection.ascx") as MediaCollection;
            
            if (uiWidgetEntity.Type == UIWidgetType.CountDown2x1 || uiWidgetEntity.Type == UIWidgetType.CountDown2x2)
            {
                this.tabsMain.AddTabPage("Timers", "UIWidgetTimer", "~/Company/SubPanels/UIWidgetTimerCollection.ascx");
            }
            else if (uiWidgetEntity.Type == UIWidgetType.Availability2x1 || uiWidgetEntity.Type == UIWidgetType.Availability2x2)
            {
                this.tabsMain.AddTabPage("Availability", "UIWidgetAvailability", "~/Company/SubPanels/UIWidgetAvailabilityCollection.ascx");
            }
        }

        public override bool Save()
        {
            if (this.DataSourceAsUIWidgetEntity.IsNew)
            {
                int uiTabId;
                if (QueryStringHelper.TryGetValue("UITabId", out uiTabId))
                    this.DataSourceAsUIWidgetEntity.UITabId = uiTabId;
            }

            if (this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.SceneWidget1x1 ||
                this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.SceneWidget2x1)
            {
                this.DataSourceAsUIWidgetEntity.FieldValue1 = this.tbScene.Text;
                this.DataSourceAsUIWidgetEntity.FieldValue2 = this.tbSuccessMessage.Text;
            }
            else if (this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.WeatherClockV2)
            {
                this.DataSourceAsUIWidgetEntity.FieldValue1 = this.ddlTemperatureUnit.SelectedItem.Value.ToString();
                this.DataSourceAsUIWidgetEntity.FieldValue2 = this.ddlClockMode.SelectedItem.Value.ToString();
            }
            else if (this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.Weather2x3)
            {
                this.DataSourceAsUIWidgetEntity.FieldValue1 = this.ddlTemperatureUnit.SelectedItem.Value.ToString();
            }
            else if (this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.Clock2x2)
            {
                this.DataSourceAsUIWidgetEntity.FieldValue2 = this.ddlClockMode.SelectedItem.Value.ToString();
            }
            else if (this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.Availability2x1 || this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.Availability2x2)
            {
                this.DataSourceAsUIWidgetEntity.FieldValue1 = this.tbDialogTitle.Text;

                if (!this.tbTopText.Text.IsNullOrWhiteSpace() || !this.tbBottomText.Text.IsNullOrWhiteSpace())
                {
                    this.DataSourceAsUIWidgetEntity.FieldValue2 = string.Concat(this.tbTopText.Text, UIWidgetHelper.AVAILABILITY_PLACEHOLDER, this.tbBottomText.Text);
                }
                else if (this.tbTopText.Text.IsNullOrWhiteSpace() && this.tbBottomText.Text.IsNullOrWhiteSpace())
                {
                    this.DataSourceAsUIWidgetEntity.FieldValue2 = string.Empty;
                }                
            }
            else if (this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.ConnectBtSpeaker1x1 || 
                this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.ConnectBtSpeaker2x1 ||
                this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.ConnectBtSpeaker2x2 ||
                this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.ConnectBtDeviceInstructions1x1)
            {
                this.DataSourceAsUIWidgetEntity.FieldValue1 = this.ddlBluetoothDevice.SelectedItem.Value.ToString();
            }
            else if (this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.AdWidget2x1 ||
                this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.AdWidget2x2 ||
                this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.AdWidget2x5 ||
                this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.AdWidget2x10)
            {
                this.DataSourceAsUIWidgetEntity.FieldValue1 = this.tbAdUnitId.Text;
            }

            if (base.Save())
            {
                if (!this.DataSourceAsUIWidgetEntity.UITabEntity.UIModeEntity.CompanyId.HasValue)
                {
                    CustomTextHelper.UpdateCustomTexts(this.DataSourceAsUIWidgetEntity);
                }
                else
                {
                    CustomTextHelper.UpdateCustomTexts(this.DataSourceAsUIWidgetEntity, this.DataSourceAsUIWidgetEntity.UITabEntity.UIModeEntity.CompanyEntity);
                }

                return true;
            }
            return false;
        }

        public override bool SaveAndNew()
        {
            bool result = this.Save();
            if (result)
            {
                string url = this.Request.Path + "?mode=add";
                if (this.DataSourceAsUIWidgetEntity.UITabId > 0)
                    url += string.Format("&UITabId={0}", this.DataSourceAsUIWidgetEntity.UITabId);

                url += string.Format("&Type={0}", (int)this.DataSourceAsUIWidgetEntity.Type); // Default on first page

                this.Response.Redirect(url, false);
            }
            return result;
        }               

        private void SetGui()
        {
            if (this.uiWidgetCultureCollection != null)
            {
                this.uiWidgetCultureCollection.Initialize();
            }

            int type = -1;
            if (QueryStringHelper.HasValue("Type"))
                Int32.TryParse(QueryStringHelper.GetValue("Type"), out type);

            int uiTabId = -1;
            if (QueryStringHelper.HasValue("UITabId"))
                Int32.TryParse(QueryStringHelper.GetValue("UITabId"), out uiTabId);

            if (this.DataSourceAsUIWidgetEntity.IsNew && type > 0)
                this.DataSourceAsUIWidgetEntity.Type = (UIWidgetType)type;

            this.cbType.DataBindEnum<UIWidgetType>();
            if (!this.IsPostBack)
            {
                this.cbType.Value = (int)this.DataSourceAsUIWidgetEntity.Type;
            }
 
            if (this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.WeatherClockV1 ||
                this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.WeatherClockV2 ||
                this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.LanguagePicker ||
                this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.ConnectBtSpeaker1x1 ||
                this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.ConnectBtSpeaker2x1 ||
                this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.ConnectBtSpeaker2x2 ||
                this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.ConnectBtKeyboard1x1 ||                
                this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.ConnectBtKeyboard2x1 ||
                this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.AdvertisementSmall ||
                this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.AdvertisementMedium ||
                this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.PmsVoicemail1x1 ||
                this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.PmsVoicemail2x1 ||
                this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.Placeholder || 
                this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.ServiceRequest1x1 ||
                this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.ServiceRequest2x1 ||
                this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.Advertisement1x1 ||
                this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.Clock2x2 ||
                this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.Weather2x3 ||
                this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.MenuFill || 
                this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.Currency2x1 || 
                this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.Currency2x2 || 
                this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.Currency1x1)
            {
                this.tbCaption.Enabled = false;
                this.tbCaption.IsRequired = false;
            }

            if (this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.AlarmClock1x1 ||
                this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.AlarmClock2x1 ||
                this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.PmsBill1x1 ||
                this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.PmsBill2x1)
            {
                this.tbCaption.Enabled = true;
                this.tbCaption.IsRequired = false;
            }

            if (this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.AdvertisementSmall ||
                this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.AdvertisementMedium ||
                this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.Advertisement1x1)
            {
                this.plhAdvertisement.Visible = true;
                this.dataBindAdvertisements();
            }
            else if (this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.ServiceRequest1x1 ||
                     this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.ServiceRequest2x1)
            {
                this.plhProductCategory.Visible = true;

                this.DataBindCategories(true);
                this.DataBindServiceRequests();
            }
            else if (this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.LinkWidget1x1 ||
                     this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.LinkWidget2x1 ||
                     this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.LinkWidget2x2 || 
                     this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.CountDown2x1 || 
                     this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.CountDown2x2)
            {
                this.plhProductCategory.Visible = true;
                this.DataBindCategories(false);
                this.DataBindProducts();

                this.plhEntertainment.Visible = true;
                this.DataBindEntertainments();

                this.plhEntertainmentcategory.Visible = true;

                this.plhSite.Visible = true;
                this.DataBindSites();
                this.DataBindPages();

                this.plhUITabType.Visible = true;
                this.DataBindTabs();

                this.plhUrl.Visible = true;
                this.plhPdf.Visible = true;

                this.btConvertPdf.Enabled = (this.GetEntityActions() == 0);

                this.cbRoomControlType.DataBindEnumStringValuesAsDataSource(typeof(RoomControlSectionType), (int)RoomControlSectionType.Lighting, (int)RoomControlSectionType.Custom - 1);
                this.plhRoomControlType.Visible = true;

                if (this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.CountDown2x1 || this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.CountDown2x2)
                {
                    this.lblTime.Visible = true;
                }
            }
            else if (this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.SceneWidget1x1 ||
                     this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.SceneWidget2x1)
            {
                this.plhScene.Visible = true;
                
                this.tbScene.IsRequired = true;
                this.tbScene.Text = this.DataSourceAsUIWidgetEntity.FieldValue1;
                this.tbSuccessMessage.Text = this.DataSourceAsUIWidgetEntity.FieldValue2;
            }
            else if (this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.WeatherClockV2)
            {
                this.ShowTemperatureFields();
                this.ShowClockFields();
            }
            else if (this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.Weather2x3)
            {
                this.ShowTemperatureFields();
            }
            else if (this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.Clock2x2)
            {
                this.ShowClockFields();
            }
            else if (this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.Availability2x1 || this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.Availability2x2)
            {
                string[] texts = this.DataSourceAsUIWidgetEntity.FieldValue2.Split(new string[] { UIWidgetHelper.AVAILABILITY_PLACEHOLDER }, StringSplitOptions.None);
                
                this.tbDialogTitle.Text = this.DataSourceAsUIWidgetEntity.FieldValue1;

                if (texts.Length > 0)
                {
                    this.tbTopText.Text = texts[0];
                }
                if (texts.Length > 1)
                {
                    this.tbBottomText.Text = texts[1];
                }

                this.ddlMessageLayoutType.DataBindEnum<MessageLayoutType>();

                this.plhAvailability.Visible = true;
            }
            else if (this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.ConnectBtSpeaker1x1 || 
                this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.ConnectBtSpeaker2x1 ||
                this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.ConnectBtSpeaker2x2 ||
                this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.ConnectBtDeviceInstructions1x1)
            {
                this.ShowBluetoothDeviceFields();
            }
            else if (this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.AdWidget2x1 ||
                this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.AdWidget2x2 ||
                this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.AdWidget2x5 ||
                this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.AdWidget2x10)
            {
                this.ShowAdUnitFields();
            }

            if (!this.IsPostBack && this.PageMode == Dionysos.Web.PageMode.Add)
            {
                object scalar = new Obymobi.Data.CollectionClasses.UIWidgetCollection().GetScalar(UIWidgetFieldIndex.SortOrder, null, AggregateFunction.Max, UIWidgetFields.UITabId == uiTabId);
                int sortValue;
                if (!int.TryParse(scalar.ToString(), out sortValue))
                    sortValue = 0;
                this.tbSortOrder.Value = ++sortValue;

                // Visible by default
                this.DataSourceAsUIWidgetEntity.IsVisible = true;
            }            

            if (this.DataSourceAsUIWidgetEntity.IsDirty && !this.DataSourceAsUIWidgetEntity.IsNew)
                this.DataSourceAsUIWidgetEntity.Save();
        }

	    private void ShowTemperatureFields()
	    {
            this.plhTemperature.Visible = true;

            this.ddlTemperatureUnit.IsRequired = true;
            this.ddlTemperatureUnit.DataBindEnumStringValuesAsDataSource(typeof(TemperatureUnit));

            int selectedIndex;
            if (!int.TryParse(this.DataSourceAsUIWidgetEntity.FieldValue1, out selectedIndex))
            {
                selectedIndex = 0;
            }
            this.ddlTemperatureUnit.SelectedIndex = selectedIndex;
	    }

	    private void ShowClockFields()
	    {
	        this.plhClock.Visible = true;

	        this.ddlClockMode.IsRequired = true;
            this.ddlClockMode.DataBindEnumStringValuesAsDataSource(typeof(ClockMode));

            int selectedIndex;
            if (!int.TryParse(this.DataSourceAsUIWidgetEntity.FieldValue2, out selectedIndex))
            {
                selectedIndex = 0;
            }
            this.ddlClockMode.SelectedIndex = selectedIndex;
	    }

        private void ShowBluetoothDeviceFields()
        {
            this.plhBluetoothDevice.Visible = true;

            this.ddlBluetoothDevice.IsRequired = true;
            this.ddlBluetoothDevice.DataBindEnumStringValuesAsDataSource(typeof(BluetoothDeviceType));

            int selectedIndex;
            if (!int.TryParse(this.DataSourceAsUIWidgetEntity.FieldValue1, out selectedIndex))
            {
                selectedIndex = 0;
            }
            this.ddlBluetoothDevice.SelectedIndex = selectedIndex;
        }

        private void ShowAdUnitFields()
        {
            this.plhAdUnit.Visible = true;
            this.tbAdUnitId.IsRequired = true;
            this.tbAdUnitId.Text = this.DataSourceAsUIWidgetEntity.FieldValue1;
        }

        private void dataBindAdvertisements()
        {
            IncludeFieldsList includeFields = new IncludeFieldsList();
            includeFields.Add(AdvertisementFields.AdvertisementId);
            includeFields.Add(AdvertisementFields.Name);

            PredicateExpression filter = new PredicateExpression();
            filter.AddWithOr(DeliverypointgroupFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

            RelationCollection relations = new RelationCollection();
            relations.Add(AdvertisementEntityBase.Relations.DeliverypointgroupAdvertisementEntityUsingAdvertisementId);
            relations.Add(DeliverypointgroupAdvertisementEntityBase.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId);

            SortExpression sort = new SortExpression(new SortClause(AdvertisementFields.Name, SortOperator.Ascending));

            AdvertisementCollection advertisements = new AdvertisementCollection();
            advertisements.GetMulti(filter, 0, sort, relations, null, includeFields, 0, 0);

            this.cbAdvertisementId.DataSource = advertisements;
            this.cbAdvertisementId.DataBind();
        }

	    private void DataBindServiceRequests()
	    {
	        PredicateExpression serviceRequestFilter = new PredicateExpression();
            serviceRequestFilter.AddWithOr(ProductFields.SubType == (int)ProductSubType.ServiceItems);
	        serviceRequestFilter.AddWithOr(CategoryFields.Type == (int)CategoryType.ServiceItems);
            
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ProductFields.VisibilityType != VisibilityType.Never);
	        filter.Add(ProductFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
	        filter.Add(serviceRequestFilter);

	        RelationCollection relations = new RelationCollection();
	        relations.Add(ProductCategoryEntityBase.Relations.ProductEntityUsingProductId);
	        relations.Add(ProductCategoryEntityBase.Relations.CategoryEntityUsingCategoryId);

	        PrefetchPath prefetch = new PrefetchPath(EntityType.ProductCategoryEntity);
	        prefetch.Add(ProductCategoryEntityBase.PrefetchPathProductEntity);
            prefetch.Add(ProductCategoryEntityBase.PrefetchPathCategoryEntity);

	        SortExpression sort = new SortExpression(new SortClause(ProductFields.Name, SortOperator.Ascending));

            ProductCategoryCollection productCategories = new ProductCategoryCollection();
	        productCategories.GetMulti(filter, 0, sort, relations, prefetch);

	        this.cbProductCategoryId.DataSource = productCategories;
            this.cbProductCategoryId.DataBind();
	    }

        private void DataBindCategories(bool serviceItemsOnly)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(CategoryFields.Visible == true);
            filter.Add(CategoryFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            if (serviceItemsOnly)
            {
                filter.Add(CategoryFields.Type == (int)CategoryType.ServiceItems);
            }

            SortExpression sort = new SortExpression(new SortClause(CategoryFields.Name, SortOperator.Ascending));

            CategoryCollection categories = new CategoryCollection();
            categories.GetMulti(filter, 0, sort, null, null);

            this.cbCategoryId.DataSource = categories;
            this.cbCategoryId.DataBind();
        }

        private void DataBindProducts()
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ProductFields.VisibilityType != VisibilityType.Never);
            filter.Add(ProductFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

            int categoryId = 0;
            if (this.IsPostBack)
            {
                categoryId = this.cbCategoryId.ValidId;
            }
            else if (this.DataSourceAsUIWidgetEntity.CategoryId.HasValue)
            {
                categoryId = this.DataSourceAsUIWidgetEntity.CategoryId.Value;
            }

            if (categoryId > 0)
            {
                filter.Add(ProductCategoryFields.CategoryId == categoryId);
            }

            RelationCollection relations = new RelationCollection();
            relations.Add(ProductCategoryEntityBase.Relations.ProductEntityUsingProductId);
            relations.Add(ProductCategoryEntityBase.Relations.CategoryEntityUsingCategoryId);

            PrefetchPath prefetch = new PrefetchPath(EntityType.ProductCategoryEntity);
            prefetch.Add(ProductCategoryEntityBase.PrefetchPathProductEntity);
            prefetch.Add(ProductCategoryEntityBase.PrefetchPathCategoryEntity);

            SortExpression sort = new SortExpression(new SortClause(ProductFields.Name, SortOperator.Ascending));

            ProductCategoryCollection productCategories = new ProductCategoryCollection();
            productCategories.GetMulti(filter, 0, sort, relations, prefetch);

            this.cbProductCategoryId.DataSource = productCategories;
            this.cbProductCategoryId.DataBind();
        }

        private void DataBindEntertainments()
        {
            PredicateExpression companyFilter = new PredicateExpression();
            companyFilter.Add(EntertainmentFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            companyFilter.AddWithOr(EntertainmentFields.CompanyId == DBNull.Value);

            PredicateExpression filter = new PredicateExpression();
            filter.Add(EntertainmentFields.Visible == true);
            filter.Add(companyFilter);

            SortExpression sort = new SortExpression(new SortClause(EntertainmentFields.Name, SortOperator.Ascending));

            EntertainmentCollection entertainments = new EntertainmentCollection();
            entertainments.GetMulti(filter, 0, sort, null, null);

            this.cbEntertainmentId.DataSource = entertainments;
            this.cbEntertainmentId.DataBind();
        }

        private void DataBindSites()
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(SiteFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            filter.AddWithOr(SiteFields.CompanyId == DBNull.Value);

            SortExpression sort = new SortExpression(new SortClause(SiteFields.Name, SortOperator.Ascending));

            SiteCollection sites = new SiteCollection();
            sites.GetMulti(filter, 0, sort, null, null);

            this.cbSiteId.DataSource = sites;
            this.cbSiteId.DataBind();
        }

        private void DataBindPages()
        {
            int siteId = 0;
            if (this.IsPostBack)
            {
                siteId = this.cbSiteId.ValidId;
            }
            else if(this.DataSourceAsUIWidgetEntity.SiteId.HasValue)
            {
                siteId = this.DataSourceAsUIWidgetEntity.SiteId.Value;
            }

            if (siteId > 0)
            {
                PredicateExpression filter = new PredicateExpression();
                filter.Add(PageFields.Visible == true);
                filter.Add(PageFields.SiteId == siteId);

                SortExpression sort = new SortExpression(new SortClause(PageFields.Name, SortOperator.Ascending));

                PageCollection pages = new PageCollection();
                pages.GetMulti(filter, 0, sort, null, null);

                this.cbPageId.DataSource = pages;
                this.cbPageId.DataBind();
            }
            else
            {
                this.cbPageId.DataSource = new PageCollection();
                this.cbPageId.DataBind();
            }
        }

        private void DataBindTabs()
        {
            Dictionary<int, string> tabTypes = new Dictionary<int, string>()
            {
                { (int)UITabType.Home, EnumUtil.GetStringValue(UITabType.Home, null)},
                { (int)UITabType.More, EnumUtil.GetStringValue(UITabType.More, null)},
                { (int)UITabType.Basket, EnumUtil.GetStringValue(UITabType.Basket, null)},
                { (int)UITabType.RoomControl, EnumUtil.GetStringValue(UITabType.RoomControl, null)}
            };

            this.cbUITabType.DataSource = tabTypes;
            this.cbUITabType.DataBind();
        }

        public override void Validate()
        {
            base.Validate();
            if (this.IsValid)
            {
                if (this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.ServiceRequest1x1 ||
                    this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.ServiceRequest2x1)
                {
                    if (this.cbCategoryId.ValidId > 0 && this.cbProductCategoryId.ValidId < 0)
                    {
                        this.MultiValidatorDefault.AddError("No product has been selected, please select a product.");
                        base.Validate();
                    }
                }
                else if (this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.LinkWidget1x1 ||
                         this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.LinkWidget2x1 ||
                         this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.LinkWidget2x2 || 
                         this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.CountDown2x1 || 
                         this.DataSourceAsUIWidgetEntity.Type == UIWidgetType.CountDown2x2)
                {
                    int selectedActions = this.GetSelectedActions();
                    if (selectedActions > 1)
                    {
                        this.MultiValidatorDefault.AddError("Multiple actions have been selected, please select only one action.");
                        base.Validate();
                    }
                }
            }
        }

        private int GetSelectedActions()
	    {
            int selectedActions = 0;

            if (this.cbCategoryId.ValidId > 0 || this.cbProductCategoryId.ValidId > 0)
                selectedActions++;
            if (this.cbEntertainmentId.ValidId > 0)
                selectedActions++;
            if (this.cbEntertainmentcategoryId.ValidId > 0)
                selectedActions++;
            if (this.cbSiteId.ValidId > 0 || this.cbPageId.ValidId > 0)
                selectedActions++;
            if (this.cbUITabType.ValidId > 0)
                selectedActions++;
            if (!string.IsNullOrWhiteSpace(this.tbUrl.Value))
                selectedActions++;
            if (this.cbRoomControlType.ValidId > 0)
                selectedActions++;            
            if (this.HasPdfAction())
                selectedActions++;

	        return selectedActions;
	    }

	    private void HookUpEvents()
        {
            this.btConvertPdf.Click += this.btUploadPdf_Click;
            this.btDeletePdf.Click += this.btDeletePdf_Click;
        }

	    private void UploadPdf()
	    {
            int selectedActions = this.GetSelectedActions();
            if (selectedActions > 1)
            {
                this.MultiValidatorDefault.AddError("Multiple actions have been selected, please select only one action.");
                base.Validate();
            }
            else if (this.mediaCollection != null)
	        {
                this.mediaCollection.UploadPdf();
	        }
	    }

	    private void DeletePdf()
	    {
	        Data.CollectionClasses.MediaCollection medias = MediaHelper.GetMediaWithMediaType(this.DataSourceAsUIWidgetEntity, MediaType.WidgetPdf1280x800);
	        medias.DeleteMulti();

	        this.Refresh();
	    }

        private bool HasPdfAction()
        {
            return MediaHelper.GetMediaWithMediaType(this.DataSourceAsUIWidgetEntity, MediaType.WidgetPdf1280x800).Count > 0;            
        }

	    private int GetEntityActions()
	    {
            int actions = 0;

            if (this.DataSourceAsUIWidgetEntity.CategoryId.HasValue || this.DataSourceAsUIWidgetEntity.ProductCategoryId.HasValue)
                actions++;
            if (this.DataSourceAsUIWidgetEntity.EntertainmentId.HasValue)
                actions++;
            if (this.DataSourceAsUIWidgetEntity.EntertainmentcategoryId.HasValue)
                actions++;
            if (this.DataSourceAsUIWidgetEntity.SiteId.HasValue || this.DataSourceAsUIWidgetEntity.PageId.HasValue)
                actions++;            
            if (!this.DataSourceAsUIWidgetEntity.Url.IsNullOrWhiteSpace())
                actions++;
            if (this.DataSourceAsUIWidgetEntity.RoomControlType.HasValue)
                actions++;
            if (this.HasPdfAction())
                actions++;

            return actions;
	    }

	    #endregion

		#region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            this.DataSourceLoaded += this.UIWidget_DataSourceLoaded;
            base.OnInit(e);
        }

        private void UIWidget_DataSourceLoaded(object sender)
        {
            this.SetGui();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!QueryStringHelper.HasValue("UITabId") && !QueryStringHelper.HasValue("id"))
            {
                throw new Exception("Page must be called with a valid UITabId or with a UIWidgetId (=id)");
            }

            this.HookUpEvents();
        }	    

	    protected void cbProductCategoryId_OnCallback(object sender, CallbackEventArgsBase e)
        {
            this.DataBindProducts();
        }

        protected void cbPageId_OnCallback(object sender, CallbackEventArgsBase e)
        {
            this.DataBindPages();
        }

        private void btUploadPdf_Click(object sender, EventArgs e)
        {
            this.UploadPdf();
        }

        private void btDeletePdf_Click(object sender, EventArgs e)
        {
            this.DeletePdf();
        }

		#endregion

		#region Properties

		/// <summary>
		/// Return the page's datasource as a UIWidgetEntity
		/// </summary>
		public UIWidgetEntity DataSourceAsUIWidgetEntity
		{
			get
			{
				return this.DataSource as UIWidgetEntity;
			}
		}

		#endregion
	}
}
