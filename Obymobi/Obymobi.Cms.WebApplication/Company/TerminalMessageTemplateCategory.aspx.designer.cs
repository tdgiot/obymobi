﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Obymobi.ObymobiCms.Company
{


    public partial class TerminalMessageTemplateCategory
    {

        /// <summary>
        /// tabsMain control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Dionysos.Web.UI.DevExControls.PageControl tabsMain;

        /// <summary>
        /// lblTerminalId control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Dionysos.Web.UI.WebControls.Label lblTerminalId;

        /// <summary>
        /// ddlTerminalId control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Dionysos.Web.UI.DevExControls.ComboBoxLLBLGenEntityCollection ddlTerminalId;

        /// <summary>
        /// lblMessageTemplateId control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Dionysos.Web.UI.WebControls.Label lblMessageTemplateId;

        /// <summary>
        /// ddlMessageTemplateCategoryId control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Dionysos.Web.UI.DevExControls.ComboBoxLLBLGenEntityCollection ddlMessageTemplateCategoryId;
    }
}
