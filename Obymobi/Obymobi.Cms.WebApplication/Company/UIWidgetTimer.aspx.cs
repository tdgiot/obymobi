﻿using System;
using Dionysos;
using Obymobi.Data.EntityClasses;

namespace Obymobi.ObymobiCms.Company
{
	public partial class UIWidgetTimer : Dionysos.Web.UI.PageLLBLGenEntity
	{
		#region Methods

	    private void SetCustomValues()
        {
            if (!this.teCountToTime.Value.HasValue)
                return;

            DateTime countToTime = DateTimeUtil.CombineDateAndTime(this.teCountToTime.Value.Value, DateTime.Now);
            DateTime? countToDate = this.deCountToDate.Value;

            DateTime countToTimeUtc = countToTime.LocalTimeToUtc(CmsSessionHelper.CompanyTimeZone);
            DateTime? countToDateUtc = countToDate.LocalTimeToUtc(CmsSessionHelper.CompanyTimeZone);

            this.DataSourceAsUIWidgetTimerEntity.CountToTime = countToTime;
            this.DataSourceAsUIWidgetTimerEntity.CountToDate = countToDate;

            this.DataSourceAsUIWidgetTimerEntity.CountToTimeUTC = countToTimeUtc;
            this.DataSourceAsUIWidgetTimerEntity.CountToDateUTC = countToDateUtc;
        }

	    #endregion

		#region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            this.DataSourceLoaded += this.UIWidgetTimer_DataSourceLoaded;
            base.OnInit(e);
        }

        private void UIWidgetTimer_DataSourceLoaded(object sender)
        {
            
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

	    public override bool Save()
	    {
	        bool success = false;

	        if (this.IsValid)
	        {
	            this.SetCustomValues();
	            success = base.Save();
	        }

	        return success;
	    }

	    public override bool SaveAndGo()
	    {

            bool success = false;

            if (this.IsValid)
            {
                this.SetCustomValues();
                success = base.SaveAndGo();
            }

            return success;
        }

	    public override bool SaveAndNew()
	    {
            bool success = false;

            if (this.IsValid)
            {
                this.SetCustomValues();
                success = base.SaveAndNew();
            }

            return success;
	    }

	    #endregion

		#region Properties

		public UIWidgetTimerEntity DataSourceAsUIWidgetTimerEntity
		{
			get
			{
				return this.DataSource as UIWidgetTimerEntity;
			}
		}

        #endregion
    }
}
