﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.Netmessage" CodeBehind="Netmessage.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" runat="Server"></asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="cplhAdditionalToolBarButtons">
    <span class="toolbar">
        <D:Button runat="server" ID="btBack" Text="Go back" />
    </span>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" runat="Server">
    <div>
        <X:PageControl ID="tabsMain" runat="server" Width="100%">
            <TabPages>
                <X:TabPage Text="Log" Name="Log">
                    <controls>
                        <table class="dataformV2">
                            <tr>
                                <td class="label">
                                    <D:Label runat="server" ID="lblMessageTypeLabel">Message Type</D:Label>
                                </td>
                                <td class="control">
                                    <D:Label ID="lblMessageType" runat="server" LocalizeText="false" />
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:Label runat="server" ID="lblSenderLabel">Sender</D:Label>
                                </td>
                                <td class="control">
                                    <span>
                                        <D:HyperLink ID="hlSender" runat="server" LocalizeText="false" /></span>
                                </td>
                                <td class="label">
                                    <D:Label runat="server" ID="lblReceiverLabel">Receiver</D:Label>
                                </td>
                                <td class="control">
                                    <span>
                                        <D:HyperLink ID="hlReceiver" runat="server" LocalizeText="false" /></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:Label runat="server" ID="lblCreatedLabel">Created</D:Label>
                                </td>
                                <td class="control">
                                    <D:Label ID="lblCreated" runat="server" LocalizeText="false" />
                                </td>
                                <td class="label">
                                    <D:Label runat="server" ID="lblStatusLabel">Status</D:Label>
                                </td>
                                <td class="control">
                                    <D:Label ID="lblStatus" runat="server" LocalizeText="false" />
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:Label runat="server" ID="lblDescriptionLabel">Description</D:Label>
                                </td>
                                <td class="control" colspan="3">
                                    <D:Label ID="lblDescription" runat="server" LocalizeText="false" />
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:Label runat="server" ID="lblErrorMessageLabel">Error Message</D:Label>
                                </td>
                                <td class="control" colspan="3">
                                    <D:Label ID="lblErrorMessage" runat="server" LocalizeText="false" />
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:Label runat="server" ID="lblFieldValueLabel1">Field Value 1</D:Label>
                                </td>
                                <td class="control">
                                    <D:Label ID="lblFieldValue1" runat="server" LocalizeText="false" />
                                </td>
                                <td class="label">
                                    <D:Label runat="server" ID="lblFieldValueLabel2">Field Value 2</D:Label>
                                </td>
                                <td class="control">
                                    <D:Label ID="lblFieldValue2" runat="server" LocalizeText="false" />
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:Label runat="server" ID="lblFieldValueLabel3">Field Value 3</D:Label>
                                </td>
                                <td class="control">
                                    <D:Label ID="lblFieldValue3" runat="server" LocalizeText="false" />
                                </td>
                                <td class="label">
                                    <D:Label runat="server" ID="lblFieldValueLabel4">Field Value 4</D:Label>
                                </td>
                                <td class="control">
                                    <D:Label ID="lblFieldValue4" runat="server" LocalizeText="false" />
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:Label runat="server" ID="lblFieldValueLabel5">Field Value 5</D:Label>
                                </td>
                                <td class="control">
                                    <D:Label ID="lblFieldValue5" runat="server" LocalizeText="false" />
                                </td>
                                <td class="label">
                                    <D:Label runat="server" ID="lblFieldValueLabel6">Field Value 6</D:Label>
                                </td>
                                <td class="control">
                                    <D:Label ID="lblFieldValue6" runat="server" LocalizeText="false" />
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:Label runat="server" ID="lblFieldValueLabel7">Field Value 7</D:Label>
                                </td>
                                <td class="control">
                                    <D:Label ID="lblFieldValue7" runat="server" LocalizeText="false" />
                                </td>
                                <td class="label">
                                    <D:Label runat="server" ID="lblFieldValueLabel8">Field Value 8</D:Label>
                                </td>
                                <td class="control">
                                    <D:Label ID="lblFieldValue8" runat="server" LocalizeText="false" />
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:Label runat="server" ID="lblFieldValueLabel9">Field Value 9</D:Label>
                                </td>
                                <td class="control">
                                    <D:Label ID="lblFieldValue9" runat="server" LocalizeText="false" />
                                </td>
                                <td class="label">
                                    <D:Label runat="server" ID="lblFieldValueLabel10">Field Value 10</D:Label>
                                </td>
                                <td class="control">
                                    <D:Label ID="lblFieldValue10" runat="server" LocalizeText="false" />
                                </td>
                            </tr>
                        </table>

                    </controls>
                </X:TabPage>
            </TabPages>
        </X:PageControl>
    </div>
</asp:Content>
