﻿using System;
using System.Linq;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Obymobi.ObymobiCms.Company.Subpanels;

namespace Obymobi.ObymobiCms.Company
{
    public partial class RoomControlArea : Dionysos.Web.UI.PageLLBLGenEntity
    {
        #region Methods

        private void LoadUserControls()
        {
            this.tabsMain.AddTabPage("Sections", "RoomControlSectionCollection", "~/Company/SubPanels/RoomControlSectionCollection.ascx");

            this.BindRoomControlAreaTypes();

            RoomControlAreaEntity roomControlAreaEntity = new RoomControlAreaEntity(this.EntityId);
            if (roomControlAreaEntity.IsNew)
                return;

            this.tabsMain.AddTabPage("Translations", "CustomTextCollection", "~/Generic/UserControls/CustomTextCollection.ascx", "Generic");            
        }

        private void SetGui()
        {
            if (this.DataSourceAsRoomControlAreaEntity.IsNew)
                this.DataSourceAsRoomControlAreaEntity.Visible = true;            

            if (this.DataSourceAsRoomControlAreaEntity.IsDirty && !this.DataSourceAsRoomControlAreaEntity.IsNew)
                this.DataSourceAsRoomControlAreaEntity.Save();            
        }

        private void BindRoomControlAreaTypes()
        {
            foreach (RoomControlAreaType type in Enum.GetValues(typeof(RoomControlAreaType)))
            {
                this.cbType.Items.Add(new DevExpress.Web.ListEditItem(type.ToString(), (int)type));
            }
        }

        #endregion

		#region Properties

		
		public RoomControlAreaEntity DataSourceAsRoomControlAreaEntity
		{
			get
			{
				return this.DataSource as RoomControlAreaEntity;
			}
		}

		#endregion

		#region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            this.DataSourceLoaded += RoomControlArea_DataSourceLoaded;
            base.OnInit(e);
        }

		private void RoomControlArea_DataSourceLoaded(object sender)
		{
            this.SetGui();
		}

        public override bool Save()
        {
            if (base.Save())
            {
                CustomTextHelper.UpdateCustomTexts(this.DataSource, new CompanyEntity(this.DataSourceAsRoomControlAreaEntity.RoomControlConfigurationEntity.CompanyId));
                return true;
            }
            else
                return false;
        }

		#endregion
	}
}
