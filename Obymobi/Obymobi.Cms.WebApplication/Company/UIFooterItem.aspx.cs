﻿using System;
using Dionysos.Data.LLBLGen;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Web;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data;

namespace Obymobi.ObymobiCms.Company
{
	public partial class UIFooterItem : Dionysos.Web.UI.PageLLBLGenEntity
	{
		#region Methods

        private void LoadUserControls()
        {
            var uiFooterItemEntity = new UIFooterItemEntity(this.EntityId);
            if (uiFooterItemEntity.IsNew)
                return;

            if (uiFooterItemEntity.TypeAsEnum == FooterbarItemType.Icon)
            {
                this.tabsMain.AddTabPage("Afbeeldingen", "Media", "~/Generic/UserControls/MediaCollection.ascx");
            }
            else
            {
                this.tabsMain.AddTabPage("Translations", "CustomTextCollection", "~/Generic/UserControls/CustomTextCollection.ascx", "Generic");
            }
        }

        public override bool Save()
        {
            if (this.DataSourceAsUIFooterItemEntity.IsNew)
            {
                int uiModeId;
                if (QueryStringHelper.TryGetValue("UIModeId", out uiModeId))
                    this.DataSourceAsUIFooterItemEntity.UIModeId = uiModeId;
            }

            if (base.Save())
            {
                if (!this.DataSourceAsUIFooterItemEntity.UIModeEntity.CompanyId.HasValue)
                {
                    CustomTextHelper.UpdateCustomTexts(this.DataSourceAsUIFooterItemEntity);
                }
                else
                {
                    CustomTextHelper.UpdateCustomTexts(this.DataSourceAsUIFooterItemEntity, this.DataSourceAsUIFooterItemEntity.UIModeEntity.CompanyEntity);
                }

                return true;
            }
                
            return false;
        }

        public override bool SaveAndNew()
        {
            bool result = Save();
            if(result)
            {
                string url = this.Request.Path + "?mode=add";
                if (this.DataSourceAsUIFooterItemEntity.UIModeId > 0)
                    url += string.Format("&UIModeId={0}", this.DataSourceAsUIFooterItemEntity.UIModeId);

                url += string.Format("&Type={0}", (int)this.DataSourceAsUIFooterItemEntity.Type); // Default on first page

                this.Response.Redirect(url, false);
            }
            return result;
        }

        private void SetGui()
        {
            // Bind UITab types depending on the UIModeType parameter
            int type = -1;
            if (QueryStringHelper.HasValue("Type"))
                Int32.TryParse(QueryStringHelper.GetValue("Type"), out type);

            if (this.DataSourceAsUIFooterItemEntity.IsNew && type > 0)
                this.DataSourceAsUIFooterItemEntity.Type = type;

            this.cbPosition.DataBindEnumStringValuesAsDataSource(typeof(FooterbarItemPosition));
            this.cbActionIntent.DataBindEnumStringValuesAsDataSource(typeof(FooterbarItemAction));
        }

	    #endregion

		#region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            this.DataSourceLoaded += (UIFooterItem_DataSourceLoaded);
            base.OnInit(e);
        }

        void UIFooterItem_DataSourceLoaded(object sender)
        {
            // Set default to visible
            if (this.DataSourceAsUIFooterItemEntity.IsNew)
                this.DataSourceAsUIFooterItemEntity.Visible = true;

            this.SetGui();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!QueryStringHelper.HasValue("id") && (!QueryStringHelper.HasValue("UIModeId") || !QueryStringHelper.HasValue("Type")))
            {
                throw new Exception("Page must be called with a valid UIModeId and Type");
            }
        }

		#endregion

		#region Properties

		/// <summary>
        /// Return the page's datasource as a UIFooterItemEntity
		/// </summary>
		public UIFooterItemEntity DataSourceAsUIFooterItemEntity
		{
			get
			{
                return this.DataSource as UIFooterItemEntity;
			}
		}

		#endregion
	}
}
