﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.UISchedule" Codebehind="UISchedule.aspx.cs" %>
<%@ Register TagPrefix="uc" TagName="ScheduledItemsPanel" Src="~/Company/SubPanels/ScheduledItemsPanel.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">    
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
                    <D:PlaceHolder ID="PlaceHolder1" runat="server">
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblName">Name</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
							</td>	  
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblDeliverypointgroup">Deliverypointgroup</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlDeliverypointgroupId" IncrementalFilteringMode="StartsWith" EntityName="Deliverypointgroup" TextField="Name" ValueField="DeliverypointgroupId" IsRequired="true" PreventEntityCollectionInitialization="true" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <D:PlaceHolder runat="server" ID="plhSchedule" />
                            </td>
                        </tr>							                  
                    </table>
                    </D:PlaceHolder>   
                </Controls>                    
            </X:TabPage>
            <X:TabPage Text="Copy" Name="Copy">
				<Controls>
				    <table class="dataformV2">
				        <tr>
							<td class="label">
								<D:Label runat="server" id="lblCopyName">Name</D:Label>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbCopyName" runat="server"></D:TextBoxString>
							</td>	  
                            <td class="label">
                                <D:Label runat="server" id="lblCopyDeliverypointgroup">Deliverypointgroup</D:Label>
                            </td>
                            <td class="control">
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlCopyDeliverypointgroupId" IncrementalFilteringMode="StartsWith" EntityName="Deliverypointgroup" TextField="Name" ValueField="DeliverypointgroupId" PreventEntityCollectionInitialization="true" />
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                
                            </td>
                            <td class="control">
                                
                            </td>
                            <td class="label">
                                
                            </td>
                            <td class="control">
                                <D:Button runat="server" ID="btnCopyUISchedule" Text="Copy"/>
                            </td>
                        </tr>
                    </table>
                </Controls>
            </X:TabPage>
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>