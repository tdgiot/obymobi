﻿using System;
using System.Collections.Generic;
using System.Linq.Dynamic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.LinqSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Web.UI.DevExControls;
using Dionysos;
using Obymobi.Data;
using Dionysos.Data.LLBLGen;
using System.IO;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Dionysos.Web;
using Obymobi.Logic.Cms;
using System.Text;
using System.Data;
using Obymobi.Data.DaoClasses;
using Dionysos.Web.UI;
using DevExpress.Web;

namespace Obymobi.ObymobiCms.Company
{
    public partial class Orderitem : PageDefault
    {
        OrderitemEntity orderitem = null;

        protected override void OnInit(EventArgs e)
        {            
            this.LoadUserControls();
            base.OnInit(e);
        }

        #region Methods

        private void LoadUserControls()
        {            
            
        }

		void SetGui()
		{
            if (this.orderitem != null)
            {
                // General order info
                this.lblOrderitemId.Text = this.orderitem.OrderitemId.ToString();

                string url = ResolveUrl(string.Format("~/Catalog/Product.aspx?id={0}", this.orderitem.ProductId));
                this.lblProduct.Text = string.Format("<a href=\"{0}\">{1}</a>", url, this.orderitem.ProductName);

                this.lblQuantity.Text = this.orderitem.Quantity.ToString();
                this.lblPrice.Text = String.Format("{0:0.00}", this.orderitem.PriceIn);
                this.tbNotes.Text = this.orderitem.Notes;

                // Alterations
                this.RenderAlterations();
            }
		}

        private void RenderAlterations()
        {
            // Create the table
            DataTable table = new DataTable();

            // Create columns
            DataColumn alterationIdColumn = new DataColumn("AlterationId");
            DataColumn nameColumn = new DataColumn("Name");
            DataColumn optionColumn = new DataColumn("Option");
            DataColumn priceColumn = new DataColumn("Price");

            // Translations
            alterationIdColumn.Caption = this.Translate("clmnAlterationId", "AlterationId");
            nameColumn.Caption = this.Translate("clmnName", "Naam");
            optionColumn.Caption = this.Translate("clmnOption", "Optie");
            priceColumn.Caption = this.Translate("clmnPrice", "Prijs");

            // Add columns
            table.Columns.Add(alterationIdColumn);
            table.Columns.Add(nameColumn);
            table.Columns.Add(optionColumn);
            table.Columns.Add(priceColumn);

            foreach (OrderitemAlterationitemEntity alterationitem in this.orderitem.OrderitemAlterationitemCollection)
            {
                DataRow row = table.NewRow();

                row[0] = alterationitem.AlterationitemEntity.AlterationId;
                row[1] = alterationitem.AlterationName;
                row[2] = alterationitem.AlterationoptionName;
                row[3] = alterationitem.AlterationoptionPriceIn;

                table.Rows.Add(row);
            }

            this.alterationsGrid.DataSource = table;
            this.alterationsGrid.DataBind();
        }

        private void SetFilters()
        {
            int orderitemId = Int32.Parse(Request.QueryString["id"]);

            if (orderitemId > 0)
            {
                PredicateExpression filter = new PredicateExpression();
                filter.Add(OrderitemFields.OrderitemId == orderitemId);

                OrderitemCollection orderitems = new OrderitemCollection();
                orderitems.GetMulti(filter);

                if (orderitems.Count > 0)
                    this.orderitem = orderitems[0];
            }
        }

        #endregion

        #region Event handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            {
                this.SetFilters();
                this.SetGui();
            }            
        }

        #endregion
    }
}
