﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.MobileServiceTool" Title="MobileServiceTool Tool" Codebehind="MobileServiceTool.aspx.cs" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cplhTitleHolder">Webservice Tool</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="cplhToolbarHolder">        
    <X:ToolBarButton runat="server" ID="btCall" CommandName="Show" Text="Call Webservice (View)" Image-Url="~/images/icons/table.png" style="margin-left: 3px;float:left;"></X:ToolBarButton>
    <X:ToolBarButton runat="server" ID="btDownload" CommandName="Download" Text="Call Webservice (Download)" Image-Url="~/images/icons/table_save.png" style="margin-left: 3px;float:left"></X:ToolBarButton>    
</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="cplhContentHolder">
<div>
	<X:PageControl ID="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:Label ID="lblCompany" runat="server">Bedrijf</D:Label>
                            </td>
                            <td class="control">
                                <X:ComboBoxInt runat="server" ID="cbCompany"></X:ComboBoxInt>
                            </td>
                            <td class="label">                                
                                <D:Label ID="lblMenu" runat="server">Menu</D:Label>
                            </td>
                            <td class="control">                                
                                <X:ComboBoxInt runat="server" ID="cbMenu"></X:ComboBoxInt> 
                            </td>
                        </tr>   
                        <tr>
                            <td class="label">
                                <D:Label ID="lblPointOfInterest" runat="server">Point of Interest</D:Label>
                            </td>
                            <td class="control">
                                <X:ComboBoxInt runat="server" ID="cbPointOfInterest"></X:ComboBoxInt>
                            </td>                         
                            <td class="label">
                                <D:Label ID="lblUsername" runat="server">Customer Email</D:Label>
                            </td>
                            <td class="control">
                                <D:TextBox runat="server" ID="tbEmail"></D:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                <D:Label ID="lblDeviceType" runat="server">Device Type</D:Label>
                            </td>
                            <td class="control">
                                <X:ComboBoxInt runat="server" ID="cbDeviceType"></X:ComboBoxInt>
                            </td>
                            <td class="label">
                                <D:Label ID="lblTablet" runat="server">Tablet</D:Label>
                            </td>
                            <td class="control">
                                <D:CheckBox runat="server" ID="cbTablet" />
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                <D:Label ID="lblSiteId" runat="server">Site</D:Label>
                            </td>
                            <td class="control">
                                <X:ComboBoxInt runat="server" ID="cbSiteId"></X:ComboBoxInt>
                            </td>
                            <td class="label">
                                <D:Label ID="lblCultureCode" runat="server">Culture</D:Label>
                            </td>
                            <td class="label">
                                <X:ComboBox runat="server" ID="ddlCultureCode"></X:ComboBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                <D:Label ID="lblWebserviceMethod" runat="server">Webservice method</D:Label>
                            </td>
                            <td class="control">
                                <X:ComboBox runat="server" ID="cbMethod"></X:ComboBox>
                            </td>        
                            <td class="label">
                            </td>
                            <td class="control">
                            </td>                                                            
                        </tr>
                        <tr>
                            <td class="label">
                                <D:Label ID="lblAccessCodeToAdd" runat="server">Access code to add</D:Label>
                            </td>
                            <td class="control">
                                <D:TextBox runat="server" ID="tbAccessCodeToAdd"></D:TextBox>
                            </td>        
                            <td class="label">
                                <D:Label ID="lblAccessCodeIds" runat="server">Access code IDs</D:Label>
                            </td>
                            <td class="control">
                                <D:TextBox runat="server" ID="tbAccessCodeIds"></D:TextBox>
                            </td>                                                            
                        </tr>

                    </table> 
                </Controls>
            </X:TabPage>
        </TabPages>
    </X:PageControl>
    <pre><D:PlaceHolder runat="server" ID="plhReponse"></D:PlaceHolder></pre>
</div>
</asp:Content>