﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.CloudStorageAccount" Codebehind="CloudStorageAccount.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblAccountName">Account name</D:LabelEntityFieldInfo>
							</td>
							<td class="control" >
								<D:TextBoxString ID="tbAccountName" runat="server" IsRequired="true"></D:TextBoxString>
							</td>
							<td class="label">
								<D:Label runat="server" id="lblType">Type</D:Label>
							</td>
                            <td class="control">
								<X:ComboBoxEnum ID="cbType" runat="server" Type="Obymobi.Enums.CloudStorageAccountType, Obymobi"></X:ComboBoxEnum>
							</td>
                        </tr>
                        <tr>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblAccountPassword">Account password</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <D:TextBoxString ID="tbAccountPassword" runat="server" IsRequired="true"></D:TextBoxString>
                            </td>
                        </tr>
					 </table>	
				</Controls>
			</X:TabPage>						
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>

