﻿using System;
using Obymobi.Data.EntityClasses;
using System.Data;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Data;
using Obymobi.Logic.HelperClasses;
using System.IO;
using System.Linq;
using Dionysos;
using System.Collections.Generic;

namespace Obymobi.ObymobiCms.Company
{
    public partial class Messagegroup : Dionysos.Web.UI.PageLLBLGenEntity
    {
        private void LoadUserControls()
        {
            DataTable deliverypoints = this.LoadDeliverypoints();
            this.dpGrid.DataSource = deliverypoints;
            this.dpGrid.DataBind();

            PredicateExpression filter = new PredicateExpression(DeliverypointgroupFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            filter.Add(DeliverypointgroupFields.Active == true);

            DeliverypointgroupCollection deliverypointgroups = new DeliverypointgroupCollection();
            deliverypointgroups.GetMulti(filter);
            this.ddlDeliverypointgroupId.DataSource = deliverypointgroups;
            this.ddlDeliverypointgroupId.DataBind();

            this.cbImportDeliverypointgroup.DataSource = deliverypointgroups;
            this.cbImportDeliverypointgroup.DataBind();

            this.cbType.DataBindEnum<MessagegroupType>();
        }

        private DataTable LoadDeliverypoints()
        {
            var dataTable = new DataTable();

            var deliverypointIdColumn = new DataColumn("DeliverypointId");
            var deliverypointNumberColumn = new DataColumn("DeliverypointNumber");
            var deliverypointNameColumn = new DataColumn("DeliverypointName");
            var deliverypointgroupNameColumn = new DataColumn("DeliverypointgroupName");
            var deliverypointgroupIdColumn = new DataColumn("DeliverypointgroupId");

            deliverypointIdColumn.Caption = this.Translate("clmnDeliverypointId", "Number");
            deliverypointNumberColumn.Caption = this.Translate("clmnDeliverypointNumber", "Number");
            deliverypointNameColumn.Caption = this.Translate("clmnDeliverypointName", "Name");
            deliverypointgroupNameColumn.Caption = this.Translate("clmnDeliverypointgroupName", "Deliverypointgroup");
            deliverypointgroupIdColumn.Caption = this.Translate("clmnDeliverypointgroupId", "Deliverypointgroup");

            dataTable.Columns.Add(deliverypointIdColumn);
            dataTable.Columns.Add(deliverypointNumberColumn);
            dataTable.Columns.Add(deliverypointNameColumn);
            dataTable.Columns.Add(deliverypointgroupNameColumn);
            dataTable.Columns.Add(deliverypointgroupIdColumn);

            foreach (DeliverypointEntity deliverypointEntity in this.DataSourceAsMessagegroup.DeliverypointCollectionViaMessagegroupDeliverypoint)
            {
                DataRow row = dataTable.NewRow();
                row["DeliverypointId"] = deliverypointEntity.DeliverypointId;
                row["DeliverypointNumber"] = deliverypointEntity.Number;
                row["DeliverypointName"] = deliverypointEntity.Name;
                row["DeliverypointgroupName"] = deliverypointEntity.DeliverypointgroupEntity.Name;
                row["DeliverypointgroupId"] = deliverypointEntity.DeliverypointgroupId;

                dataTable.Rows.Add(row);
            }

            return dataTable;
        }

        private void SetGui()
        {
            if (this.DataSourceAsMessagegroup.IsNew || this.DataSourceAsMessagegroup.Type != MessagegroupType.Custom)
            {
                this.pnlDeliverypoints.Visible = false;
            }

            if (this.DataSourceAsMessagegroup.UpdatedUTC.HasValue)
            {
                this.lblLastUpdated.Text = TimeZoneInfo.ConvertTime(this.DataSourceAsMessagegroup.UpdatedUTC.Value, this.DataSourceAsMessagegroup.CompanyEntity.TimeZoneInfo).ToString();
            }
            else
            {
                this.lblLastUpdated.Text = TimeZoneInfo.ConvertTime(this.DataSourceAsMessagegroup.CreatedUTC.GetValueOrDefault(), this.DataSourceAsMessagegroup.CompanyEntity.TimeZoneInfo).ToString();
            }

            this.plhGroupName.Visible = this.DataSourceAsMessagegroup.Type == MessagegroupType.Group;            
        }

        private void AddDeliverypoints()
        {
            this.AddRemoveDeliverypoints(true);
        }

        private void RemoveDeliverypoints()
        {
            this.AddRemoveDeliverypoints(false);
        }

        private void AddRemoveDeliverypoints(bool add)
        {
            this.Save();

            int from;
            int to;
            if (!int.TryParse(this.tbFrom.Text, out from))
            {
                string errorMessage = ((Dionysos.Web.UI.PageDefault)this.Page).Translate("NoFromValueSet", "Enter a starting number");
                ((Dionysos.Web.UI.PageDefault)this.Page).AddInformator(Dionysos.Web.UI.InformatorType.Warning, errorMessage);
            }
            else if (!int.TryParse(this.tbTo.Text, out to))
            {
                string errorMessage = ((Dionysos.Web.UI.PageDefault)this.Page).Translate("NoToValueSet", "Enter an ending number");
                ((Dionysos.Web.UI.PageDefault)this.Page).AddInformator(Dionysos.Web.UI.InformatorType.Warning, errorMessage);
            }
            else
            {
                int tmpTo = to;
                if (from > to)
                {
                    to = from;
                    from = tmpTo;
                    this.tbTo.Text = to.ToString();
                    this.tbFrom.Text = from.ToString();
                }

                DeliverypointCollection companyDeliverypoints = this.DataSourceAsMessagegroup.CompanyEntity.DeliverypointCollection;
                MessagegroupDeliverypointCollection messagegroupDeliverypoints = this.DataSourceAsMessagegroup.MessagegroupDeliverypointCollection;
                DeliverypointCollection addedDeliverypoints = this.DataSourceAsMessagegroup.DeliverypointCollectionViaMessagegroupDeliverypoint;

                IEnumerable<DeliverypointEntity> deliverypointsInRange;
                if (this.ddlDeliverypointgroupId.ValidId > 0)
                {
                    deliverypointsInRange = companyDeliverypoints.Where(dp => int.Parse(dp.Number) >= from && int.Parse(dp.Number) <= to && dp.DeliverypointgroupId == this.ddlDeliverypointgroupId.ValidId);
                }
                else
                {
                    deliverypointsInRange = companyDeliverypoints.Where(dp => int.Parse(dp.Number) >= from && int.Parse(dp.Number) <= to);
                }
                foreach(DeliverypointEntity deliverypoint in deliverypointsInRange)
                {
                    MessagegroupDeliverypointEntity messagegroupDeliverypoint = messagegroupDeliverypoints.FirstOrDefault(x => x.DeliverypointId == deliverypoint.DeliverypointId);
                    if (add && messagegroupDeliverypoint == null)
                    {
                        messagegroupDeliverypoint = new MessagegroupDeliverypointEntity();
                        messagegroupDeliverypoint.MessagegroupId = this.DataSourceAsMessagegroup.MessagegroupId;
                        messagegroupDeliverypoint.DeliverypointId = deliverypoint.DeliverypointId;
                        messagegroupDeliverypoint.Save();

                        messagegroupDeliverypoints.Add(messagegroupDeliverypoint);
                        addedDeliverypoints.Add(deliverypoint);
                    }
                    else if(!add && messagegroupDeliverypoint != null)
                    {
                        messagegroupDeliverypoints.Remove(messagegroupDeliverypoint);
                        messagegroupDeliverypoint.Delete();

                        DeliverypointEntity addedDeliverypoint = addedDeliverypoints.FirstOrDefault(x => x.DeliverypointId == deliverypoint.DeliverypointId);
                        if (addedDeliverypoint != null)
                        {
                            addedDeliverypoints.Remove(addedDeliverypoint);
                        }
                    }
                }

                this.LoadUserControls();

                string message;
                if (add)
                {
                    message = ((Dionysos.Web.UI.PageDefault)this.Page).Translate("DeliverypointsAdded", "The deliverypoints have been added");
                }
                else
                {
                    message = ((Dionysos.Web.UI.PageDefault)this.Page).Translate("DeliverypointsRemoved", "The deliverypoints have been removed");
                }

                ((Dionysos.Web.UI.PageDefault)this.Page).AddInformator(Dionysos.Web.UI.InformatorType.Information, message);
            }

            this.Validate();
        }

        public override bool InitializeEntity()
        {
            if (this.EntityId > 0)
            {
                PrefetchPath prefetch = new PrefetchPath(EntityType.MessagegroupEntity);
                prefetch.Add(MessagegroupEntity.PrefetchPathMessagegroupDeliverypointCollection);
                prefetch.Add(MessagegroupEntity.PrefetchPathDeliverypointCollectionViaMessagegroupDeliverypoint);

                this.DataSource = new MessagegroupEntity(this.EntityId, prefetch);
            }

            return base.InitializeEntity();
        }

        protected override void OnInit(EventArgs e)
        {
            base.DataSourceLoaded += Messagegroup_DataSourceLoaded;

            this.btnAddRange.Click += btnAddRange_Click;
            this.btnRemoveRange.Click += btnRemoveRange_Click;

            this.btnDeleteSelected.Click += btnDeleteSelected_Click;

            base.OnInit(e);
        }

        void btnDeleteSelected_Click(object sender, EventArgs e)
        {
            List<object> selectedIds = this.dpGrid.GetSelectedFieldValues("DeliverypointId");
            if(selectedIds.Count > 0)
            {
                MessagegroupDeliverypointCollection messagegroupDeliverypoints = this.DataSourceAsMessagegroup.MessagegroupDeliverypointCollection;
                DeliverypointCollection addedDeliverypoints = this.DataSourceAsMessagegroup.DeliverypointCollectionViaMessagegroupDeliverypoint;
                foreach (object selectedId in selectedIds)
                {
                    int deliverypointId = int.Parse(selectedId.ToString());
                    MessagegroupDeliverypointEntity messagegroupEntity = messagegroupDeliverypoints.FirstOrDefault(x => x.DeliverypointId == deliverypointId);
                    if (messagegroupEntity != null)
                    {
                        messagegroupDeliverypoints.Remove(messagegroupEntity);
                        messagegroupEntity.Delete();

                        DeliverypointEntity addedDeliverypoint = addedDeliverypoints.FirstOrDefault(x => x.DeliverypointId == deliverypointId);
                        if (addedDeliverypoint != null)
                        {
                            addedDeliverypoints.Remove(addedDeliverypoint);
                        }
                    }
                }

                this.Save();
                this.LoadUserControls();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.dpGrid.CustomButtonCallback += new DevExpress.Web.ASPxGridViewCustomButtonCallbackEventHandler(dpGrid_CustomButtonCallback);
            this.btnImport.Click += BtnImport_Click;
            this.SetGui();
        }

        private void BtnImport_Click(object sender, EventArgs e)
        {
            if (this.cbImportDeliverypointgroup.ValidId <= 0)
            {
                return;
            }

            DeliverypointCollection companyDeliverypoints = this.DataSourceAsMessagegroup.CompanyEntity.DeliverypointCollection;
            MessagegroupDeliverypointCollection messagegroupDeliverypoints = this.DataSourceAsMessagegroup.MessagegroupDeliverypointCollection;
            int deliverypointgroupId = this.cbImportDeliverypointgroup.ValidId;

            string[] lines = this.tbNumbers.Text.SplitLines();
            foreach (string line in lines)
            {
                int number;
                if (int.TryParse(line, out number))
                {
                    MessagegroupDeliverypointEntity entity = messagegroupDeliverypoints.FirstOrDefault(x => x.DeliverypointEntity.DeliverypointgroupId == deliverypointgroupId && x.DeliverypointEntity.Number.Equals(line, StringComparison.InvariantCultureIgnoreCase));
                    if (entity == null)
                    {
                        DeliverypointEntity deliverypoint = companyDeliverypoints.FirstOrDefault(x => x.DeliverypointgroupId == deliverypointgroupId && x.Number.Equals(line, StringComparison.InvariantCultureIgnoreCase));
                        if (deliverypoint != null)
                        {
                            MessagegroupDeliverypointEntity messagegroupDeliverypoint = new MessagegroupDeliverypointEntity();
                            messagegroupDeliverypoint.MessagegroupId = this.DataSourceAsMessagegroup.MessagegroupId;
                            messagegroupDeliverypoint.DeliverypointId = deliverypoint.DeliverypointId;
                            messagegroupDeliverypoint.Save();

                            messagegroupDeliverypoints.Add(messagegroupDeliverypoint);
                        }
                    }
                }
            }

            this.Save();
        }

        void dpGrid_CustomButtonCallback(object sender, DevExpress.Web.ASPxGridViewCustomButtonCallbackEventArgs e)
        {
            if (e.ButtonID == "deleteDp")
            {
                this.Save();

                DataRow row = this.dpGrid.GetDataRow(e.VisibleIndex);
                int deliverypointId = int.Parse(row["DeliverypointId"].ToString());

                PredicateExpression filter = new PredicateExpression();
                filter.Add(MessagegroupDeliverypointFields.DeliverypointId == deliverypointId);
                filter.Add(MessagegroupDeliverypointFields.MessagegroupId == this.DataSourceAsMessagegroup.MessagegroupId);

                MessagegroupDeliverypointCollection messagegroupDeliverypoints = new MessagegroupDeliverypointCollection();
                messagegroupDeliverypoints.GetMulti(filter);

                if (messagegroupDeliverypoints.Count > 0)
                {
                    messagegroupDeliverypoints[0].Delete();
                }

                DevExpress.Web.ASPxWebControl.RedirectOnCallback(Request.RawUrl);
            }
        }

        void btnAddRange_Click(object sender, EventArgs e)
        {
            this.AddDeliverypoints();
        }

        void btnRemoveRange_Click(object sender, EventArgs e)
        {
            this.RemoveDeliverypoints();
        }

        void Messagegroup_DataSourceLoaded(object sender)
        {
            this.LoadUserControls();
        }

        private void SetCustomValues()
        {
            if (this.PageMode == Dionysos.Web.PageMode.Add)
            {
                this.DataSourceAsMessagegroup.CompanyId = CmsSessionHelper.CurrentCompanyId;
            }
        }

        public override bool Save()
        {
            this.DataSourceAsMessagegroup.UpdatedUTC = DateTime.UtcNow;

            this.SetCustomValues();
            return base.Save();
        }

        public override bool SaveAndNew()
        {
            this.SetCustomValues();
            return base.SaveAndNew();
        }

        public override bool SaveAndGo()
        {
            this.SetCustomValues();
            return base.SaveAndGo();
        }

        /// <summary>
        /// Return the page's datasource as a MessagegroupEntity
        /// </summary>
        public MessagegroupEntity DataSourceAsMessagegroup
        {
            get
            {
                return this.DataSource as MessagegroupEntity;
            }
        }
    }
}