﻿using System;
using System.Collections.Generic;
using System.Linq.Dynamic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.LinqSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Web.UI.DevExControls;
using Dionysos;
using Obymobi.Data;
using Dionysos.Data.LLBLGen;
using System.IO;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Dionysos.Web;
using Obymobi.Logic.Cms;
using System.Text;
using System.Data;
using Obymobi.Data.DaoClasses;
using Dionysos.Web.UI;
using DevExpress.Web;

namespace Obymobi.ObymobiCms.Company
{
    public partial class Order : PageDefault
    {
        OrderEntity order = null;

        protected override void OnInit(EventArgs e)
        {            
            this.LoadUserControls();
            base.OnInit(e);
        }

        #region Methods

        private void LoadUserControls()
        {            
            
        }

		void SetGui()
		{
            if (this.order != null)
            {
                // General order info
                this.lblOrderId.Text = this.order.OrderId.ToString();
                this.lblPrice.Text = String.Format("{0:0.00}", this.order.PriceIn);
                this.lblDeliverypoint.Text = this.order.DeliverypointEntity?.Name;
                this.lblDeliverypointgroup.Text = this.order.DeliverypointEntity?.DeliverypointgroupEntity.Name;
                this.lblTimePlaced.Text = this.order.CreatedUTC.Value.UtcToLocalTime().ToString();
                this.lblTimeProcessed.Text = this.order.ProcessedUTC.HasValue ? this.order.ProcessedUTC.Value.UtcToLocalTime().ToString() : "";
                this.lblStatus.Text = this.order.StatusAsEnum.ToString();
                this.lblStatusText.Text = this.order.StatusText;
                this.tbNotes.Text = this.order.Notes;

                // Order items
                this.RenderOrderitems();

                // Order steps
                this.RenderOrderRoutesteps();
            }
		}

        private void RenderOrderitems()
        {
            // Create the table
            DataTable table = new DataTable();

            // Create columns
            DataColumn orderitemIdColumn = new DataColumn("OrderitemId");
            DataColumn productColumn = new DataColumn("Product");
            DataColumn quantityColumn = new DataColumn("Quantity");
            DataColumn priceColumn = new DataColumn("Price");

            // Translation
            orderitemIdColumn.Caption = this.Translate("clmnOrderitemId", "OrderitemId");
            productColumn.Caption = this.Translate("clmnProduct", "Product");
            quantityColumn.Caption = this.Translate("clmnQuantity", "Aantal");
            priceColumn.Caption = this.Translate("clmnPrice", "Prijs");

            // Add columns
            table.Columns.Add(orderitemIdColumn);
            table.Columns.Add(productColumn);
            table.Columns.Add(quantityColumn);
            table.Columns.Add(priceColumn);

            foreach (OrderitemEntity orderitem in this.order.OrderitemCollection)
            {
                DataRow row = table.NewRow();

                row[0] = orderitem.OrderitemId;
                row[1] = orderitem.ProductName;
                row[2] = orderitem.Quantity;
                row[3] = String.Format("{0:0.00}", orderitem.PriceIn);

                table.Rows.Add(row);
            }

            this.orderitemsGrid.DataSource = table;
            this.orderitemsGrid.DataBind();
        }

        private void RenderOrderRoutesteps()
        {
            // Create the table
            DataTable table = new DataTable();

            // Create columns
            DataColumn orderRoutestephandlerIdColumn = new DataColumn("OrderRoutestephandlerId");
            DataColumn terminalColumn = new DataColumn("Terminal");
            DataColumn numberColumn = new DataColumn("Number");
            DataColumn handlerTypeColumn = new DataColumn("HandlerType");
            DataColumn statusColumn = new DataColumn("Status");
            DataColumn errorCodeColumn = new DataColumn("ErrorCode");
            DataColumn errorTextColumn = new DataColumn("ErrorText");

            // Translations
            orderRoutestephandlerIdColumn.Caption = this.Translate("clmnOrderRoutestephandlerId", "Order Routestephandler Id");
            terminalColumn.Caption = this.Translate("clmnTerminal", "Server");
            numberColumn.Caption = this.Translate("clmnNumber", "Nummer");
            handlerTypeColumn.Caption = this.Translate("clmnHandlerType", "Afhandelingsmethode");
            statusColumn.Caption = this.Translate("clmnStatus", "Status");
            errorCodeColumn.Caption = this.Translate("clmnErrorCode", "Error code");
            errorTextColumn.Caption = this.Translate("clmnErrorText", "Error tekst");

            // Add columns
            table.Columns.Add(orderRoutestephandlerIdColumn);
            table.Columns.Add(terminalColumn);
            table.Columns.Add(numberColumn);
            table.Columns.Add(handlerTypeColumn);
            table.Columns.Add(statusColumn);
            table.Columns.Add(errorCodeColumn);
            table.Columns.Add(errorTextColumn);

            if (this.order.OrderRoutestephandlerCollection.Count > 0)
            {
                foreach (OrderRoutestephandlerEntity step in this.order.OrderRoutestephandlerCollection)
                {
                    DataRow row = table.NewRow();

                    row[0] = step.OrderRoutestephandlerId;

                    if (step.TerminalEntity != null)
                        row[1] = step.TerminalEntity.Name;

                    row[2] = step.Number;
                    row[3] = step.HandlerTypeAsEnum.ToString();
                    row[4] = step.StatusAsEnum.ToString();
                    row[5] = step.ErrorCode;
                    row[6] = step.ErrorText;

                    table.Rows.Add(row);
                }
            }
            else
            { 
                foreach (OrderRoutestephandlerHistoryEntity step in this.order.OrderRoutestephandlerHistoryCollection)
                {
                    DataRow row = table.NewRow();

                    row[0] = step.OrderRoutestephandlerHistoryId;

                    if (step.TerminalEntity != null)
                        row[1] = step.TerminalEntity.Name;

                    row[2] = step.Number;
                    row[3] = step.HandlerTypeAsEnum.ToString();
                    row[4] = step.StatusAsEnum.ToString();
                    row[5] = step.ErrorCode;
                    row[6] = step.ErrorText;

                    table.Rows.Add(row);
                }
            }

            this.stepsGrid.DataSource = table;
            this.stepsGrid.DataBind();
        }

        private void SetFilters()
        {
            int orderId = Int32.Parse(Request.QueryString["id"]);

            if (orderId > 0)
            {
                PredicateExpression filter = new PredicateExpression();
                filter.Add(OrderFields.OrderId == orderId);
                
                PrefetchPath prefetch = new PrefetchPath(EntityType.OrderEntity);
                prefetch.Add(OrderEntity.PrefetchPathDeliverypointEntity).SubPath.Add(DeliverypointEntity.PrefetchPathDeliverypointgroupEntity);

                OrderCollection orders = new OrderCollection();
                orders.GetMulti(filter, 0, null, null, prefetch);

                if (orders.Count > 0)
                    this.order = orders[0];
            }
        }

        #endregion

        #region Properties

        

        #endregion

        #region Event handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            {
                this.SetFilters();
                this.SetGui();
            }            
        }

        protected void Orderitem_Clicked(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            int orderitemId = Int32.Parse(e.Parameters);

            string url = ResolveUrl(string.Format("~/Company/Orderitem.aspx?id={0}", orderitemId));
            DevExpress.Web.ASPxWebControl.RedirectOnCallback(url);
        }

        #endregion
    }
}
