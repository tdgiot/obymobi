using System;
using System.Globalization;
using System.Linq;
using System.Data;
using System.Web.Services;
using Dionysos.Web;
using Dionysos.Web.UI;
using Newtonsoft.Json;
using Obymobi.Data.CollectionClasses;
using Obymobi.Logic.Comet;
using Obymobi.Logic.Comet.Netmessages;
using Obymobi.Security;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using System.Collections.Generic;
using Dionysos;
using Obymobi.Logic.HelperClasses;
using Obymobi.Constants;
using System.Drawing;
using DevExpress.Utils.Filtering;
using Dionysos.Data.LLBLGen;
using Obymobi.Data;

namespace Obymobi.ObymobiCms.Company
{
    public partial class Management : PageQueryStringDataBinding
    {
        #region Constants

        private static string CLIENT_KEY = "CmsManagement_ClientKey" + CmsSessionHelper.CurrentUser.UserId;
        private static string TERMINAL_KEY = "CmsManagement_TerminalKey" + CmsSessionHelper.CurrentUser.UserId;
        private static string SANDBOX_KEY = "CmsManagement_SandboxModeKey" + CmsSessionHelper.CurrentUser.UserId;

        public enum UpdateApplications
        {
            [StringValue("Emenu")]
            Emenu,
            [StringValue("SupportTools")]
            SupportTools,
            [StringValue("Agent")]
            Agent,
            [StringValue("CraveOS")]
            CraveOS
        }

        #endregion

        private CompanyEntity currentCompany;

        readonly Dictionary<DeviceModel, VersionNumber> versionNumbersOS = new Dictionary<DeviceModel, VersionNumber>();

        #region Methods

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            base.OnInit(e);
        }

        private void LoadUserControls()
        {
            if (CmsSessionHelper.CurrentRole < Role.Administrator)
            {
                this.tabsMain.TabPages.FindByName("SandboxMode").Visible = false;
            }
            else
            {
                CompanyCollection companyCollection = new CompanyCollection();
                companyCollection.GetMulti(CompanyFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
                this.cbCompanyConfig.DataSource = companyCollection;
                this.cbCompanyConfig.DataBind();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // NOTE: the following uses an overload of RegisterClientScriptBlock() 
            // that will surround our string with the needed script tags 
            ClientScript.RegisterClientScriptBlock(GetType(), "IsPostBack", "var isPostBack = " + IsPostBack.ToString().ToLower() + ";", true);

            if (!IsPostBack)
            {
                this.cbCompanyConfig.SelectByItemValue(CmsSessionHelper.CurrentCompanyId);

                // Set deliverypointgroups for selected company
                FillSandboxDropdownMenus();
            }
            else
            {
                var target = Request["__EVENTTARGET"];
                if (target.Equals("RefreshClients"))
                {
                    var message = JsonConvert.DeserializeObject<NetmessageGetConnectedClients>(Request.Form.Get("__EVENTARGUMENT"));
                    ProcessCometClients(message);
                }
                else if (target.Contains("cbCompanyConfig"))
                {
                    // Set deliverypointgroups for selected company
                    FillSandboxDropdownMenus();
                }
            }

            this.currentCompany = new CompanyEntity(CmsSessionHelper.CurrentCompanyId);

            this.HookupEvents();
            this.SetGui();
        }

        private void FillSandboxDropdownMenus()
        {
            if (this.cbCompanyConfig.SelectedItem != null && this.cbCompanyConfig.SelectedItem.Value != null)
            {
                // Set deliverypointgroups for selected company
                int selectedCompanyId;
                if (int.TryParse(this.cbCompanyConfig.SelectedItem.Value.ToString(), out selectedCompanyId))
                {
                    var deliverypointgroups = DeliverypointgroupHelper.GetDeliverypointgroupCollectionForCompany(CmsSessionHelper.CurrentCompanyId, null);
                    this.cbCompanyDeliverypointgroup.DataSource = deliverypointgroups;
                    this.cbCompanyDeliverypointgroup.DataBind();
                }
            }
        }

        private void HookupEvents()
        {
            this.clientGrid.HtmlDataCellPrepared += grid_HtmlDataCellPrepared;
            this.terminalGrid.HtmlDataCellPrepared += grid_HtmlDataCellPrepared;

            this.btnPushCompanyConfigToClient.Click += btnPushCompanyConfigToClient_Click;

            this.btnRestartClients.Click += btnRestartClients_Click;
        }

        private void SetGui()
        {
            this.clientGrid.Settings.ShowFilterRow = false;
            this.terminalGrid.Settings.ShowFilterRow = false;
            this.sandboxGrid.Settings.ShowFilterRow = false;

            try
            {
                this.clientGrid.DataSource = SessionHelper.GetValue<DataTable>(CLIENT_KEY) ?? LoadClients();
                this.clientGrid.DataBind();
            }
            catch (Exception)
            {
                this.clientGrid.DataSource = null;
                SessionHelper.RemoveValue(CLIENT_KEY);
                this.clientGrid.DataBind();
            }

            try
            {
                this.terminalGrid.DataSource = SessionHelper.GetValue<DataTable>(TERMINAL_KEY) ?? LoadTerminals();
                this.terminalGrid.DataBind();
            }
            catch (Exception)
            {
                this.terminalGrid.DataSource = null;
                SessionHelper.RemoveValue(TERMINAL_KEY);
                this.terminalGrid.DataBind();
            }

            try
            {
                this.sandboxGrid.DataSource = SessionHelper.GetValue<DataTable>(SANDBOX_KEY);
                this.sandboxGrid.DataBind();
            }
            catch (Exception)
            {
                this.sandboxGrid.DataSource = null;
                SessionHelper.RemoveValue(SANDBOX_KEY);
                this.sandboxGrid.DataBind();
            }
        }

        private void ProcessCometClients(NetmessageGetConnectedClients netmessage)
        {
            // Load the data  
            DataTable clients = this.LoadClients();
            DataTable terminals = this.LoadTerminals();
            DataTable sandboxClients = this.LoadSandboxClients(netmessage);

            // Put the data in session cache
            SessionHelper.SetValue(CLIENT_KEY, clients);
            SessionHelper.SetValue(TERMINAL_KEY, terminals);
            SessionHelper.SetValue(SANDBOX_KEY, sandboxClients);
        }

        #region Clients

        /// <summary>
        /// Loads clients when using the Legacy and the new AWS Messaging services
        /// </summary>
        /// <returns></returns>
        private DataTable LoadClients()
        {
            DataTable clientsDataTable = CreateClientsDataTable();

            return LoadMessagingClients(clientsDataTable);
        }

        private DataTable CreateClientsDataTable()
        {
            // Create the datatable with the client info
            var dataTable = new DataTable();

            // Create the columns
            var clientIdColumn = new DataColumn("ClientId");
            var deliverypointNumber = new DataColumn("DeliverypointNumber");
            var lastRequest = new DataColumn("LastRequest");
            var loaded = new DataColumn("Loaded");
            var localIp = new DataColumn("LocalIp");
            var externalIp = new DataColumn("ExternalIp");
            var version = new DataColumn("Version");
            var versionSupportTools = new DataColumn("VersionST");
            var versionAgent = new DataColumn("VersionAgent");
            var versionOS = new DataColumn("VersionOS");
            var versionMessaging = new DataColumn("VersionMessaging");
            var isSupportTools = new DataColumn("IsSupportTools");
            var agent = new DataColumn("Agent");
            var roomControl = new DataColumn("RoomControl");
            var agentLink = new DataColumn("AgentLink");

            // Translate the columns
            clientIdColumn.Caption = this.Translate("clmnClientId", "ClientId");
            deliverypointNumber.Caption = this.Translate("clmnDeliverypointNumber", "DP Num");
            lastRequest.Caption = this.Translate("clmnLastRequest", "Last request");
            loaded.Caption = this.Translate("clmLoaded", "Loaded");
            localIp.Caption = this.Translate("clmnLocalIp", "Local IP");
            externalIp.Caption = this.Translate("clmnExternalIp", "External IP");
            version.Caption = this.Translate("clmnVersion", "Emenu");
            versionSupportTools.Caption = this.Translate("clmnVersionSupportTools", "SupportTools");
            versionAgent.Caption = this.Translate("clmnVersionAgent2", "Agent");
            versionOS.Caption = this.Translate("clmnVersionOS", "OS");
            versionMessaging.Caption = this.Translate("clmnVersionMessaging", "Messaging");
            isSupportTools.Caption = this.Translate("clmnConnectedAs", "Connected as");
            agent.Caption = this.Translate("clmnAgent", "Agent");
            agentLink.Caption = this.Translate("clmnAgentLink", "Agent link");

            // Add the columns
            dataTable.Columns.Add(clientIdColumn);
            dataTable.Columns.Add(deliverypointNumber);
            dataTable.Columns.Add(lastRequest);
            dataTable.Columns.Add(loaded);
            dataTable.Columns.Add(localIp);
            dataTable.Columns.Add(externalIp);
            dataTable.Columns.Add(version);
            dataTable.Columns.Add(versionSupportTools);
            dataTable.Columns.Add(versionAgent);
            dataTable.Columns.Add(versionOS);
            dataTable.Columns.Add(versionMessaging);
            dataTable.Columns.Add(isSupportTools);
            dataTable.Columns.Add(agent);
            dataTable.Columns.Add(roomControl);
            dataTable.Columns.Add(agentLink);

            return dataTable;
        }

        private ClientCollection PopulateClientCollection()
        {
            ClientCollection clientCollection = new ClientCollection();

            PredicateExpression filter = new PredicateExpression();
            //filter.Add(DeviceFields.Identifier == identifiers); // FO: Can't use so many parameters in a query (2100 is limit)
            filter.Add(ClientFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            filter.Add(DeviceFields.LastRequestUTC >= DateTime.UtcNow.AddMinutes(-3));

            RelationCollection relations = new RelationCollection();
            relations.Add(ClientEntityBase.Relations.DeviceEntityUsingDeviceId);

            SortExpression sort = new SortExpression();
            sort.Add(new SortClause(DeviceFields.LastRequestUTC, SortOperator.Descending));

            PrefetchPath prefetch = new PrefetchPath(EntityType.ClientEntity);
            prefetch.Add(ClientEntityBase.PrefetchPathLastDeliverypointEntity);
            prefetch.Add(ClientEntityBase.PrefetchPathDeviceEntity);

            clientCollection.GetMulti(filter, 0, sort, relations, prefetch);

            return clientCollection;
        }

        private DataTable LoadMessagingClients(DataTable clientsDataTable)
        {
            ClientCollection clientCollection = PopulateClientCollection();

            foreach (ClientEntity clientEntity in clientCollection)
            {
                DeviceEntity deviceEntity = clientEntity.DeviceEntity;

                DataRow row = clientsDataTable.NewRow();
                row["ClientId"] = clientEntity.ClientId;

                if (clientEntity.DeliverypointId.HasValue)
                    row["DeliverypointNumber"] = clientEntity.DeliverypointEntity.Number;

                row["LastRequest"] = (deviceEntity.LastRequestUTC.HasValue
                    ? deviceEntity.LastRequestUTC.Value.UtcToLocalTime().ToString(CultureInfo.InvariantCulture)
                    : "Unknown");

                if (deviceEntity.SupportToolsRunning)
                {
                    row["Agent"] = "Open Management";
                    row["AgentLink"] = string.Format("{0}&ClientId={1}", deviceEntity.Identifier.Replace(":", ""),
                        clientEntity.ClientId);
                }

                if (deviceEntity.SupportToolsRunning && !string.IsNullOrWhiteSpace(deviceEntity.ApplicationVersion))
                {
                    row["IsSupportTools"] = "EMenu & Support Tools";
                }

                else if (deviceEntity.SupportToolsRunning)
                {
                    row["IsSupportTools"] = "EMenu & Support Tools";
                }
                else if (!string.IsNullOrWhiteSpace(deviceEntity.ApplicationVersion))
                {
                    row["IsSupportTools"] = "EMenu";
                }
                else
                {
                    row["IsSupportTools"] = "Unknown";
                }

                row["LocalIp"] = deviceEntity.PrivateIpAddresses;
                row["ExternalIp"] = deviceEntity.PublicIpAddress;
                row["Version"] = deviceEntity.ApplicationVersion;
                row["VersionST"] = deviceEntity.SupportToolsVersion;
                row["VersionAgent"] = deviceEntity.AgentVersion;
                row["VersionOS"] = deviceEntity.OsVersion;
                row["VersionMessaging"] = deviceEntity.MessagingServiceVersion;

                VersionNumber companyVersionNumberOs = null;
                var deviceModel = deviceEntity.DeviceModel.GetValueOrDefault(DeviceModel.Unknown);
                if (!versionNumbersOS.TryGetValue(deviceModel, out companyVersionNumberOs))
                {
                    var releaseEntity = ReleaseHelper.GetCompanyRelease(
                        deviceEntity.DeviceModel.GetValueOrDefault(DeviceModel.Unknown), clientEntity.CompanyId);
                    if (releaseEntity != null)
                    {
                        companyVersionNumberOs = releaseEntity.VersionAsVersionNumber;
                        versionNumbersOS.Add(deviceModel, companyVersionNumberOs);
                    }
                }

                row["VersionOS"] +=
                    "|" + (companyVersionNumberOs == null ? "-1" : companyVersionNumberOs.NumberAsString);
                row["RoomControl"] = clientEntity.RoomControlConnected ? "Connected" : "Disconnected";
                row["Loaded"] = clientEntity.LoadedSuccessfully;

                clientsDataTable.Rows.Add(row);
            }

            return clientsDataTable;
        }

        #endregion

        #region Terminals

        /// <summary>
        /// Loads terminals when using the Legacy and the new AWS Messaging services
        /// </summary>
        /// <returns></returns>
        private DataTable LoadTerminals()
        {
            DataTable terminalsDataTable = CreateTerminalsDataTable();

            return LoadMessagingTerminals(terminalsDataTable);
        }

        private DataTable CreateTerminalsDataTable()
        {
            // Create the datatable with the terminal info
            var dataTable = new DataTable();

            // Create the columns
            var terminalId = new DataColumn("TerminalId");
            var name = new DataColumn("Name");
            var lastRequest = new DataColumn("LastRequest");
            var loaded = new DataColumn("Loaded");
            var localIp = new DataColumn("LocalIp");
            var externalIp = new DataColumn("ExternalIp");
            var version = new DataColumn("VersionTerminal");
            var versionSupportTools = new DataColumn("VersionST");
            var versionAgent = new DataColumn("VersionAgent");
            var versionOS = new DataColumn("VersionOS");
            var versionMessaging = new DataColumn("VersionMessaging");
            var isSupportTools = new DataColumn("IsSupportTools");
            var agent = new DataColumn("Agent");
            var agentLink = new DataColumn("AgentLink");

            // Translate the columns
            terminalId.Caption = Translate("clmnTerminalId", "TerminalId");
            name.Caption = this.Translate("clmnName", "Naam");
            lastRequest.Caption = this.Translate("clmnLastRequest", "Laatst online");
            loaded.Caption = this.Translate("clmLoaded", "Loaded");
            localIp.Caption = this.Translate("clmnLocalIp", "Lokaal ip");
            externalIp.Caption = this.Translate("clmnExternalIp", "Extern ip");
            version.Caption = this.Translate("clmnVersion", "Console");
            versionSupportTools.Caption = this.Translate("clmnVersionSupportTools", "SupportTools");
            versionAgent.Caption = this.Translate("clmnVersionAgent2", "Agent");
            versionOS.Caption = this.Translate("clmnVersionOS", "OS");
            versionMessaging.Caption = this.Translate("clmnVersionMessaging", "Messaging");
            isSupportTools.Caption = this.Translate("clmnConnectedAs", "Connected as");
            agent.Caption = this.Translate("clmnAgent", "Agent");
            agentLink.Caption = this.Translate("clmnAgentLink", "Agent link");

            // Add the columns
            dataTable.Columns.Add(terminalId);
            dataTable.Columns.Add(name);
            dataTable.Columns.Add(lastRequest);
            dataTable.Columns.Add(loaded);
            dataTable.Columns.Add(localIp);
            dataTable.Columns.Add(externalIp);
            dataTable.Columns.Add(version);
            dataTable.Columns.Add(versionSupportTools);
            dataTable.Columns.Add(versionAgent);
            dataTable.Columns.Add(versionOS);
            dataTable.Columns.Add(versionMessaging);
            dataTable.Columns.Add(isSupportTools);
            dataTable.Columns.Add(agent);
            dataTable.Columns.Add(agentLink);

            return dataTable;
        }

        private TerminalCollection PopulateTerminalCollection()
        {
            TerminalCollection terminalCollection = new TerminalCollection();

            PredicateExpression filter = new PredicateExpression();
            filter.Add(TerminalFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            filter.Add(DeviceFields.LastRequestUTC >= DateTime.UtcNow.AddMinutes(-3));

            RelationCollection relations = new RelationCollection();
            relations.Add(TerminalEntityBase.Relations.DeviceEntityUsingDeviceId);

            SortExpression sort = new SortExpression();
            sort.Add(new SortClause(DeviceFields.LastRequestUTC, SortOperator.Descending));

            PrefetchPath prefetch = new PrefetchPath(EntityType.TerminalEntity);
            prefetch.Add(TerminalEntityBase.PrefetchPathDeviceEntity);

            terminalCollection.GetMulti(filter, 0, sort, relations, prefetch);

            return terminalCollection;
        }

        private DataTable LoadMessagingTerminals(DataTable terminalsDataTable)
        {
            TerminalCollection terminalCollection = PopulateTerminalCollection();

            foreach (TerminalEntity terminalEntity in terminalCollection)
            {
                DeviceEntity deviceEntity = terminalEntity.DeviceEntity;

                DataRow row = terminalsDataTable.NewRow();
                row["TerminalId"] = terminalEntity.TerminalId;
                row["Name"] = terminalEntity.Name;
                row["LastRequest"] = (deviceEntity.LastRequestUTC.HasValue ? deviceEntity.LastRequestUTC.Value.UtcToLocalTime().ToString(CultureInfo.InvariantCulture) : "Unknown");
                row["LocalIp"] = deviceEntity.PrivateIpAddresses;
                row["ExternalIp"] = deviceEntity.PublicIpAddress;
                row["VersionTerminal"] = deviceEntity.ApplicationVersion;
                row["VersionST"] = deviceEntity.SupportToolsVersion;
                row["VersionAgent"] = deviceEntity.AgentVersion;
                row["VersionOS"] = deviceEntity.OsVersion;
                row["VersionMessaging"] = deviceEntity.MessagingServiceVersion;

                if (deviceEntity.SupportToolsRunning)
                {
                    row["Agent"] = "Open Management";
                    row["AgentLink"] = string.Format("{0}&TerminalId={1}", deviceEntity.Identifier.Replace(":", ""), terminalEntity.TerminalId);
                }

                if (deviceEntity.SupportToolsRunning && !string.IsNullOrWhiteSpace(deviceEntity.ApplicationVersion))
                {
                    row["IsSupportTools"] = "EMenu & Support Tools";
                }

                else if (deviceEntity.SupportToolsRunning)
                {
                    row["IsSupportTools"] = "EMenu & Support Tools";
                }
                else if (!string.IsNullOrWhiteSpace(deviceEntity.ApplicationVersion))
                {
                    row["IsSupportTools"] = "EMenu";
                }
                else
                {
                    row["IsSupportTools"] = "Unknown";
                }

                VersionNumber companyVersionNumberOs = null;
                var deviceModel = deviceEntity.DeviceModel.GetValueOrDefault(DeviceModel.Unknown);
                if (!versionNumbersOS.TryGetValue(deviceModel, out companyVersionNumberOs))
                {
                    var releaseEntity = ReleaseHelper.GetCompanyRelease(deviceEntity.DeviceModel.GetValueOrDefault(DeviceModel.Unknown), terminalEntity.CompanyId);
                    if (releaseEntity != null)
                    {
                        companyVersionNumberOs = releaseEntity.VersionAsVersionNumber;
                        versionNumbersOS.Add(deviceModel, companyVersionNumberOs);
                    }
                }
                row["VersionOS"] += "|" + (companyVersionNumberOs == null ? "-1" : companyVersionNumberOs.NumberAsString);
                row["Loaded"] = terminalEntity.LoadedSuccessfully;

                terminalsDataTable.Rows.Add(row);
            }

            return terminalsDataTable;
        }

        #endregion

        private DataTable LoadSandboxClients(NetmessageGetConnectedClients netmessage)
        {
            // Create datatable with device info
            var dataTable = new DataTable();

            // Datatable columns
            var deviceId = new DataColumn("DeviceId");
            var macAddress = new DataColumn("MacAddress");
            var localIp = new DataColumn("LocalIp");
            var externalIp = new DataColumn("ExternalIp");
            var lastCompany = new DataColumn("LastCompany");

            deviceId.Caption = this.Translate("clmnDeviceId", "Apparaat Id");
            macAddress.Caption = this.Translate("clmnMacAddress", "Mac adres");
            localIp.Caption = this.Translate("clmnLocalIp", "Lokaal ip");
            externalIp.Caption = this.Translate("clmnExternalIp", "Extern ip");
            lastCompany.Caption = this.Translate("clmnLastCompany", "Laaste bedrijf");

            dataTable.Columns.Add(deviceId);
            dataTable.Columns.Add(macAddress);
            dataTable.Columns.Add(localIp);
            dataTable.Columns.Add(externalIp);
            dataTable.Columns.Add(lastCompany);

            var sandboxClientInfo = new Dictionary<string, string[]>();
            if (netmessage.SandboxIdentifiers.Length > 0)
            {
                // identifier,companyid,local_ip,external_ip|
                var sandboxClients = netmessage.SandboxIdentifiers.Split('|');
                foreach (string sandboxClient in sandboxClients)
                {
                    var clientInfo = sandboxClient.Split(',');
                    sandboxClientInfo.Add(clientInfo[0], clientInfo);
                }
            }

            var deviceCollection = new DeviceCollection();
            if (sandboxClientInfo.Keys.Any(x => !x.IsNullOrWhiteSpace()))
            {
                var filter = new PredicateExpression();
                filter.Add(DeviceFields.Identifier == sandboxClientInfo.Keys.ToList());
                deviceCollection.GetMulti(filter);
            }

            foreach (DeviceEntity deviceEntity in deviceCollection)
            {
                string[] clientInfo;
                if (!sandboxClientInfo.TryGetValue(deviceEntity.Identifier, out clientInfo))
                    continue;

                var row = SandboxCreateRow(ref dataTable, deviceEntity, clientInfo);
                if (row != null)
                {
                    sandboxClientInfo.Remove(deviceEntity.Identifier);
                    dataTable.Rows.Add(row);
                }
            }

            foreach (var clientInfo in sandboxClientInfo.Values)
            {
                var row = SandboxCreateRow(ref dataTable, null, clientInfo);
                if (row != null)
                    dataTable.Rows.Add(row);
            }

            return dataTable;
        }

        private readonly Dictionary<int, string> companyCache = new Dictionary<int, string>();
        private DataRow SandboxCreateRow(ref DataTable dataTable, DeviceEntity deviceEntity, string[] clientInfo)
        {
            string identifier = "";
            if (clientInfo.Length > 0)
                identifier = clientInfo[0];

            int companyId = 0;
            if (clientInfo.Length > 1)
                int.TryParse(clientInfo[1], out companyId);

            string localIP = "";
            if (clientInfo.Length > 2)
                localIP = clientInfo[2];

            string externalIP = "";
            if (clientInfo.Length > 3)
                externalIP = clientInfo[3];

            var row = dataTable.NewRow();
            if (deviceEntity == null)
            {
                row["DeviceId"] = "Unknown";
                row["MacAddress"] = identifier;
            }
            else
            {
                row["DeviceId"] = deviceEntity.DeviceId;
                row["MacAddress"] = deviceEntity.Identifier;
            }

            row["LocalIp"] = localIP;
            row["ExternalIp"] = externalIP;

            if (companyId > 0)
            {
                string companyName;
                if (!companyCache.TryGetValue(companyId, out companyName))
                {
                    if (CmsSessionHelper.CurrentCompanyId == companyId)
                    {
                        var entity = new CompanyEntity(companyId);
                        if (entity.IsNew)
                            companyName = "Unknown (" + companyId + ")";
                        else
                            companyName = entity.Name;
                    }
                    else
                        companyName = "Unknown (" + companyId + ")";

                    companyCache.Add(companyId, companyName);
                }

                row["LastCompany"] = companyName;
            }
            else
            {
                row["LastCompany"] = "Unknown";
            }

            return row;
        }

        protected override void SetDefaultValuesToControls()
        {
        }

        private void RestartClients()
        {
            int companyId = CmsSessionHelper.CurrentCompanyId;

            // First check whether a restart clients command was already issued or is currently in progress
            PredicateExpression pendingFilter = new PredicateExpression();
            pendingFilter.Add(ScheduledCommandTaskFields.CompanyId == companyId);
            pendingFilter.Add(ScheduledCommandTaskFields.Status == ScheduledCommandTaskStatus.Pending);
            pendingFilter.Add(ScheduledCommandTaskFields.StartUTC <= DateTime.UtcNow.AddMinutes(1)); // Starts within a minute

            PredicateExpression inprogressFilter = new PredicateExpression();
            inprogressFilter.Add(ScheduledCommandTaskFields.CompanyId == companyId);
            inprogressFilter.Add(ScheduledCommandTaskFields.Status == ScheduledCommandTaskStatus.Started);

            if (EntityCollection.GetDbCount<ScheduledCommandTaskCollection>(pendingFilter) > 0)
            {
                this.AddInformator(InformatorType.Warning, this.Translate("ScheduledTaskAlreadyIssuedToRestartClients", "Another task was already issued to restart the clients."));
            }
            else if (EntityCollection.GetDbCount<ScheduledCommandTaskCollection>(inprogressFilter) > 0)
            {
                this.AddInformator(InformatorType.Warning, this.Translate("ScheduledTaskAlreadyInProgressToRestartClients", "Another task is already in progress to restart the clients."));
            }
            else
            {
                var filter = new PredicateExpression(ClientFields.CompanyId == companyId);
                var sort = new SortExpression(new SortClause(DeviceFields.LastRequestUTC, SortOperator.Descending));
                var relation = new RelationCollection(ClientEntity.Relations.DeviceEntityUsingDeviceId);

                ClientCollection clientCollection = EntityCollection.GetMulti<ClientCollection>(filter, sort, relation, null, null);

                ScheduledCommandTaskEntity scheduledCommandTaskEntity = new ScheduledCommandTaskEntity();
                scheduledCommandTaskEntity.CompanyId = companyId;

                scheduledCommandTaskEntity.StartUTC = DateTime.SpecifyKind(DateTime.UtcNow.AddMinutes(1), DateTimeKind.Unspecified);
                scheduledCommandTaskEntity.ExpiresUTC = DateTime.SpecifyKind(DateTime.UtcNow.AddMinutes(60), DateTimeKind.Unspecified);

                scheduledCommandTaskEntity.Active = true;
                scheduledCommandTaskEntity.CommandCount = clientCollection.Count;
                scheduledCommandTaskEntity.FinishedCount = 0;
                scheduledCommandTaskEntity.Save();

                foreach (ClientEntity clientEntity in clientCollection)
                {
                    ScheduledCommandEntity scheduledCommandEntity = new ScheduledCommandEntity();
                    scheduledCommandEntity.ScheduledCommandTaskId = scheduledCommandTaskEntity.ScheduledCommandTaskId;
                    scheduledCommandEntity.ClientId = clientEntity.ClientId;
                    scheduledCommandEntity.ClientCommand = ClientCommand.RestartApplication;
                    scheduledCommandEntity.Status = ScheduledCommandStatus.Pending;
                    scheduledCommandEntity.Save();
                }

                this.AddInformator(InformatorType.Information, this.Translate("ScheduledTaskQueuedToRestartClients", "A task is added to restart the clients, which will start within a minute."));
            }
        }

        #endregion

        #region Web methods

        [WebMethod(EnableSession = true)]
        public static object[] GetAuthentication(string pokeInIdentifier)
        {
            long timestamp = DateTime.UtcNow.ToUnixTime();
            string identifier = CometConstants.CometIdentifierCms + "_" + CmsSessionHelper.CurrentUser.UserId;
            string hash = Hasher.GetHashFromParameters(CometConstants.CometInternalSalt, timestamp, identifier, pokeInIdentifier);

            return new object[] { timestamp, identifier, hash };
        }

        [WebMethod(EnableSession = true)]
        public static string NetmessageSetClientType()
        {
            var netmessage = new NetmessageSetClientType();
            netmessage.ClientType = NetmessageClientType.Cms;

            return netmessage.ToJson();
        }

        [WebMethod(EnableSession = true)]
        public static string NetmessagePong()
        {
            var netmessage = new NetmessagePong();
            return netmessage.ToJson();
        }

        [WebMethod(EnableSession = true)]
        public static string NetmessageGetConnectedClients()
        {
            var netmessage = new NetmessageGetConnectedClients();
            netmessage.CompanyId = CmsSessionHelper.CurrentCompanyId;
            return netmessage.ToJson();
        }

        #endregion

        #region Event handlers

        private void grid_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.DataColumn.FieldName.Equals("Version")) // Emenu version
            {
                Obymobi.VersionNumber.VersionState state = VersionNumber.CompareVersions(e.CellValue.ToString(), this.currentCompany.ClientApplicationVersion);
                switch (state)
                {
                    case VersionNumber.VersionState.Older:
                        e.Cell.ForeColor = Color.Orange;
                        break;
                    case VersionNumber.VersionState.Newer:
                        e.Cell.ForeColor = Color.Blue;
                        break;
                }
            }
            else if (e.DataColumn.FieldName.Equals("VersionTerminal")) // Console version
            {
                Obymobi.VersionNumber.VersionState state = VersionNumber.CompareVersions(e.CellValue.ToString(), this.currentCompany.TerminalApplicationVersion);
                switch (state)
                {
                    case VersionNumber.VersionState.Older:
                        e.Cell.ForeColor = Color.Orange;
                        break;
                    case VersionNumber.VersionState.Newer:
                        e.Cell.ForeColor = Color.Blue;
                        break;
                }
            }
            else if (e.DataColumn.FieldName.Equals("VersionST")) // SupportTools version
            {
                Obymobi.VersionNumber.VersionState state = VersionNumber.CompareVersions(e.CellValue.ToString(), this.currentCompany.SupportToolsVersion);
                switch (state)
                {
                    case VersionNumber.VersionState.Older:
                        e.Cell.ForeColor = Color.Orange;
                        break;
                    case VersionNumber.VersionState.Newer:
                        e.Cell.ForeColor = Color.Blue;
                        break;
                }
            }
            else if (e.DataColumn.FieldName.Equals("VersionAgent")) // Agent version
            {
                Obymobi.VersionNumber.VersionState state = VersionNumber.CompareVersions(e.CellValue.ToString(), this.currentCompany.AgentVersion);
                switch (state)
                {
                    case VersionNumber.VersionState.Older:
                        e.Cell.ForeColor = Color.Orange;
                        break;
                    case VersionNumber.VersionState.Newer:
                        e.Cell.ForeColor = Color.Blue;
                        break;
                }
            }
            else if (e.DataColumn.FieldName.Equals("VersionOS")) // OS version
            {
                string cellValue = e.CellValue.ToString();
                string[] versionNumbers = cellValue.Split('|');
                e.Cell.Text = versionNumbers[0];

                if (versionNumbers[1].Equals("-1"))
                {
                    e.Cell.ForeColor = Color.Gray;
                }
                else
                {
                    VersionNumber.VersionState state = VersionNumber.CompareVersions(versionNumbers[0], versionNumbers[1]);
                    switch (state)
                    {
                        case VersionNumber.VersionState.Older:
                            e.Cell.ForeColor = Color.Orange;
                            break;
                        case VersionNumber.VersionState.Newer:
                            e.Cell.ForeColor = Color.Blue;
                            break;
                    }
                }
            }
            else if (e.DataColumn.FieldName.Equals("LastRequest"))
            {
                string lastRequestStr = e.CellValue.ToString();
                DateTime lastRequest;
                if (!lastRequestStr.Equals("Unknown", StringComparison.InvariantCultureIgnoreCase) && DateTime.TryParse(lastRequestStr, out lastRequest) && lastRequest < DateTime.Now.AddMilliseconds(ObymobiIntervals.DEVICE_OFFLINE_THRESHOLD * -1))
                {
                    e.Cell.ForeColor = Color.Red;
                }
            }
            else if (e.DataColumn.FieldName.Equals("RoomControl"))
            {
                string statusStr = e.CellValue.ToString();
                e.Cell.ForeColor = statusStr == "Connected" ? Color.Green : Color.Red;
            }
            else if (e.DataColumn.FieldName.Equals("Loaded"))
            {
                string cellValue = e.CellValue.ToString();
                bool loadedSuccessfully = cellValue.Contains("true", StringComparison.InvariantCultureIgnoreCase);
                e.Cell.ForeColor = loadedSuccessfully ? Color.Green : Color.Red;
            }
            if (e.DataColumn.FieldName.Equals("VersionMessaging", StringComparison.InvariantCultureIgnoreCase))
            {
                string companyMessagingVersion = currentCompany.CompanyReleaseCollection
                    .FirstOrDefault(x => x.ReleaseEntity.ApplicationEntity.Code == ApplicationCode.MessagingService)?.ReleaseEntity?.Version;

                VersionNumber.VersionState state = VersionNumber.CompareVersions(e.CellValue.ToString(), companyMessagingVersion);
                switch (state)
                {
                    case VersionNumber.VersionState.Older:
                        e.Cell.ForeColor = Color.Orange;
                        break;
                    case VersionNumber.VersionState.Newer:
                        e.Cell.ForeColor = Color.Blue;
                        break;
                }
            }
        }

        void btnPushCompanyConfigToClient_Click(object sender, EventArgs e)
        {
            if (this.cbCompanyConfig.SelectedItem.Value == null)
            {
                this.AddInformator(InformatorType.Warning, this.Translate("NoCompanySelected", "Geen bedrijf geselecteerd."));
            }
            else if (this.cbCompanyDeliverypointgroup.SelectedItem.Value == null)
            {
                this.AddInformator(InformatorType.Warning, this.Translate("NoDeliverypointgroupSelected", "Geen deliverypointgroup geselecteerd."));
            }
            else
            {
                CompanyEntity companyEntity = CompanyHelper.GetCompanyEntityByCompanyId((int)this.cbCompanyConfig.SelectedItem.Value);

                int deliverypointgroupId;
                int.TryParse(this.cbCompanyDeliverypointgroup.SelectedItem.Value.ToString(), out deliverypointgroupId);

                var selectedDevices = this.sandboxGrid.GetSelectedFieldValues("MacAddress");
                if (selectedDevices.Count == 0)
                {
                    this.AddInformator(InformatorType.Warning, this.Translate("NoDeviceSelected", "Geen apparaaat geselecteerd."));
                }
                else
                {
                    foreach (var selectedDevice in selectedDevices)
                    {
                        var configMessage = new NetmessageSandboxConfig();
                        configMessage.CompanyOwnerUsername = companyEntity.CompanyOwnerEntity.Username;
                        configMessage.CompanyOwnerPassword = companyEntity.CompanyOwnerEntity.DecryptedPassword;
                        configMessage.CompanyId = companyEntity.CompanyId;
                        configMessage.ReceiverIdentfier = (string)selectedDevice;
                        configMessage.DeliverypointgroupId = deliverypointgroupId;
                        configMessage.Salt = companyEntity.Salt;

                        CometHelper.Instance.SendMessage(configMessage);
                    }

                    this.AddInformator(InformatorType.Information, this.Translate("ConfigPushedToDevice", "Configuratie verstuurd naar geselecteerde devices."));
                }
            }
        }

        private void btnRestartClients_Click(object sender, EventArgs e)
        {
            this.RestartClients();
        }

        #endregion
    }
}
