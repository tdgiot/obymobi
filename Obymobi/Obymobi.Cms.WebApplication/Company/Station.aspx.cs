﻿using System;
using System.Linq;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.ObymobiCms.Company
{
    public partial class Station : Dionysos.Web.UI.PageLLBLGenEntity
    {
        #region Methods
        
        private void LoadUserControls()
        {
            this.tabsMain.AddTabPage("Afbeeldingen", "Media", "~/Generic/UserControls/MediaCollection.ascx");

            StationEntity stationEntity = new StationEntity(this.EntityId);
            if (stationEntity.IsNew)
                return;

            Generic.UserControls.CustomTextCollection translationsPanel = this.tabsMain.AddTabPage("Translations", "CustomTextCollection", "~/Generic/UserControls/CustomTextCollection.ascx", "Generic") as Generic.UserControls.CustomTextCollection;
            translationsPanel.FullWidth = true;            
        }

        private void SetGui()
        {

        }

        #endregion

		#region Properties

		
		public StationEntity DataSourceAsStationEntity
		{
			get
			{
				return this.DataSource as StationEntity;
			}
		}

		#endregion

		#region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            this.DataSourceLoaded += Station_DataSourceLoaded;
            base.OnInit(e);
        }

		private void Station_DataSourceLoaded(object sender)
		{
            this.SetGui();
		}

        public override bool Save()
        {
            if (base.Save())
            {
                CustomTextHelper.UpdateCustomTexts(this.DataSource, this.DataSourceAsStationEntity.StationListEntity.CompanyEntity);
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion
	}
}
