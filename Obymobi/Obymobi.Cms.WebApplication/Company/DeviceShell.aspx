﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.DeviceShell" Title="Device Shell" Codebehind="DeviceShell.aspx.cs" %>
<%@ Reference VirtualPath="~/Company/SubPanels/DeviceShellPanels/AndroidCommandPanel.ascx" %>
<%@ Import Namespace="Obymobi" %>
<%@ Import Namespace="Obymobi.Enums" %>
<%@ Import Namespace="Obymobi.Logic.Comet" %>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="cplhTitleHolder">Device Shell</asp:Content>
<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="cplhToolbarHolder"></asp:Content>
<asp:Content ID="Content6" runat="server" ContentPlaceHolderID="cplhContentHolder">
<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true" EnablePartialRendering="true"/>
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%" ClientInstanceName="PageControl">
		<TabPages>
			<X:TabPage Text="Shell" Name="Shell">
				<Controls>
					<table class="dataformV2">
						<tr>
                            <td class="control" style="padding-right:12px">
                            	<D:Label runat="server" id="lblCommand" TranslationTag="lblCommand" LocalizeText="true">Command:</D:Label>
								<input type="text" id="tbInput" name="tbInput" style="width:100%" onkeydown="return onKeyDown(event);" />
                            </td>
                        </tr>   
						<tr>
                            <td class="control" style="padding-right:22px">
                            	<div class="editable" id="taTerminal" runat="server" />
							</td>
						</tr>
					</table>
				</Controls>
			</X:TabPage>			
		</TabPages>
	</X:PageControl>
</div>    
<style>
div.editable {
    width: 100%;
    height: 500px;
    border: 1px solid #ccc;
	background-color: #fff;
    padding: 5px;
    font-family: consolas;
    font-size: 10pt;
    overflow: auto;
}​
 .cbTabs #content input.dxeEditArea_Glass[type="text"], 
 #content td.control .dxeButtonEditSys input.dxeEditArea_Glass[type="password"], 
 #content td.control .dxeTextBoxSys input.dxeEditArea_Glass[type="text"], 
 #content div.dxtcLite_Glass .dxeButtonEditSys input.dxeEditArea_Glass[type="text"] {
     height: 10px;
 }
</style>
<script type="text/javascript">
    var tb = document.getElementById("tbInput");
    var ta = document.getElementById('<%=taTerminal.ClientID %>');

    var originalMac = "";

    var firstLoad = true;
    var commandHistory = [];
    var currentHistoryIndex = 0;

    function fireCommandFromButton(command) {
        addTextToTerminal('> ' + command);
        PageMethods.SendCommand(originalMac, command, SignalRConnector.SendMessage);
        PageControl.SetActiveTab(PageControl.GetTabByName("Shell"));
        return false;
    }

    function takeScreenshotFromTab(uiTabId, uiTabText)
    {
        if (uiTabId == null) {
            fireCommandFromButton('getscreenshot');
        } else if (getQueryVariable("ClientId", false) != undefined) {
            addTextToTerminal('> switching to client tab: ' + uiTabText);
            PageMethods.SwitchTabCommand(getQueryVariable("ClientId", false), uiTabId, true, SignalRConnector.SendMessage);
            PageControl.SetActiveTab(PageControl.GetTabByName("Shell"));
            setTimeout(function () {
                fireCommandFromButton('getscreenshot');
            }, 2000);
        }
        else if (getQueryVariable("TerminalId", false) != undefined) {
            addTextToTerminal('> switching to terminal tab: ' + uiTabText);
            PageMethods.SwitchTabCommand(getQueryVariable("TerminalId", false), uiTabId, false, SignalRConnector.SendMessage);
            PageControl.SetActiveTab(PageControl.GetTabByName("Shell"));
            setTimeout(function () {
                fireCommandFromButton('getscreenshot');
            }, 2000);
        }
    }

    function showDialogButton() {
        var title = document.getElementById("tbShowDialogTitle").value;
        var message = document.getElementById("tbShowDialogMessage").value;

        var command = "showdialog -t " + title + " -m " + message;
        fireCommandFromButton(command);
    }    

    function setEmenuConfigButton() {
        var key = document.getElementById("tbSetEmenuConfigKey").value;
        var data = document.getElementById("tbSetEmenuConfigValue").value;
        var encrypted = (document.getElementById("cbEmenuConfigEncrypted").checked == 1) ? "-p " : "";

        var command = "setemenuconfig " + encrypted + key + " " + data;
        fireCommandFromButton(command);
    }
	
    function startAppSelectionChanged(obj) {
        var value = obj.value;
        if (value.length == 0) {
            document.getElementById("tbStartAppPackage").value = "";
            document.getElementById("tbStartAppActivity").value = "";
        } else {
            var n = value.split(" ");
            document.getElementById("tbStartAppPackage").value = n[0];
            document.getElementById("tbStartAppActivity").value = n[1];
        }
    }
	
    function startAppCommand() {
        var packageName = document.getElementById("tbStartAppPackage").value;
        var activityName = document.getElementById("tbStartAppActivity").value;
        var service = (document.getElementById("cbStartAppService").checked == 1) ? "-s " : "";

        var command = "startapp " + service + packageName + " " + activityName;
        fireCommandFromButton(command);
    }

    function startServiceCommand(service) {
        if (service.length > 0) {
            fireCommandFromButton("startapp " + service);
        }
    }

    function closeAppCommand() {
        var packageName = document.getElementById("tbCloseAppPackage").value;
        
        var command = "closeapp " + packageName;
        fireCommandFromButton(command);
    }

    function closeServiceCommand(service) {
        if (service.length > 0) {
            fireCommandFromButton("closeapp " + service);
        }
    }

    function restartAppCommand() {
        var packageName = document.getElementById("tbCloseAppPackage").value;
        
        var command = "closeapp -r " + packageName;
        fireCommandFromButton(command);
    }

    function restartServiceCommand(service) {
        if (service.length > 0) {

            var command = "closeapp -r " + service;
            fireCommandFromButton(command);
        }
    }

    function updateServiceCommand(service) {
        if (service.length > 0) {

            var command = "update " + service;
            fireCommandFromButton(command);
        }
    }

    function uploadFile() {
        fuImportFile.Upload();
    }
	
    function uploadConfigFile() {
        fuImportConfigFile.Upload();
    }

    function uploadComplete(s, e, targetDir) {
        if (e.isValid) {
            var command = "putfile " + e.callbackData + " " + targetDir;
            fireCommandFromButton(command);
        } else {
            addTextToTerminal("putfile Error: " + e.errorText);
        }
    }

    function configUploaded(s, e, targetDir) {
        if (e.isValid) {
            var command = "setconfig " + e.callbackData + " " + targetDir;
            fireCommandFromButton(command);
        } else {
            addTextToTerminal("setconfig Error: " + e.errorText);
        }
    }

    function onKeyDown(e) {
        if (e.keyCode == 38) { // Up
            if (currentHistoryIndex > 0) {
                currentHistoryIndex--;
                tb.value = commandHistory[currentHistoryIndex];
            } else {
                currentHistoryIndex = -1;
                tb.value = "";
            }

            return false;
        } else if (e.keyCode == 40) { // Down
            if (currentHistoryIndex < (commandHistory.length - 1)) {
                currentHistoryIndex++;
                tb.value = commandHistory[currentHistoryIndex];
            } else {
                currentHistoryIndex = commandHistory.length;
                tb.value = "";
            }

            return false;
        } else if (e.keyCode == 13) {
            if (tb.value == "clear" || tb.value == "cls") {
                ta.innerHTML = "";
                ta.scrollTop = ta.scrollHeight;
            } else {
                fireCommandFromButton(tb.value);
            }

            // Add this command to history stack
            commandHistory.push(tb.value);
            currentHistoryIndex = commandHistory.length;
			
            tb.value = "";
            return false;
        }
		
        return true;
    }
	
    function getQueryVariable(variable, logIfEmpy) {
        var query = window.location.search.substring(1);
        var vars = query.split('&');
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split('=');
            if (decodeURIComponent(pair[0]) == variable) {
                return decodeURIComponent(pair[1]);
            }
        }

        if (logIfEmpy) {
            addTextToTerminal('Query variable ' + variable + ' not found');
        }        
    }

    function addTextToTerminal(message) {
        message = replaceURLWithHTMLLinks(message);
        ta.innerHTML += message + '<br/>';
        
        ta.scrollTop = ta.scrollHeight;
    }
	
    function replaceURLWithHTMLLinks(text) {
        var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
        text = text.replace(/\n/g, "<br />");
        text = text.replace(/\<DIR>/g, "DIR");
        return text.replace(exp, "<a href='$1' target='_blank'>$1</a>");        
    }
	
    function getConsoleHeader() {
        var consoleHeader = "*****************************************************<br/>";
        consoleHeader += "** Device Terminal for MAC " + originalMac + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**<br/>";
        consoleHeader += "** Type 'help' to get a list of available commands **<br/>";
        consoleHeader += "*****************************************************";

        return consoleHeader;
    }
</script>
    <script src="../Scripts/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.signalR-2.1.1.min.js" type="text/javascript"></script>
    <script src="<%= WebEnvironmentHelper.GetMessagingUrl() %>/signalr/js" type="text/javascript"></script>
    <script type="text/javascript">
        
        (function (SignalRConnector, $, undefined) {
            SignalRConnector.Connect = function () {
                $.signalR.hub.start()
                     .done(function () {
                         console.log('Now connected, connection ID=' + $.connection.hub.id);
                         PageMethods.GetAuthentication($.connection.hub.id, SignalRConnector.Authenticate);
                     })
                     .fail(function (err) {
                         console.log('Could not connect: ' + err);
                         lblClientRefresh.innerHTML = "  Failed to connect";
                     });
            };

            SignalRConnector.Authenticate = function (collection) {
                $.signalR.signalRInterface.server.authenticate(collection[0], collection[1], collection[2]);
            };

            SignalRConnector.SendMessage = function (message) {
                $.signalR.signalRInterface.server.receiveMessage(message);
            };

        }(window.SignalRConnector = window.SignalRConnector || {}, $));

        $(function () {
            $.signalR.signalRInterface.client.ReceiveMessage = function (message) {
                console.log("Received: " + message);
                var jsonObject = $.parseJSON(message);

                if (jsonObject.messageType == 17) {
                    if (jsonObject.isAuthenticated) {
                        if (jsonObject.isAuthenticated) {
                            console.log("Authenticated, sending ClientType");

                            PageMethods.NetmessageSetClientType(SignalRConnector.SendMessage);
                            addTextToTerminal("Authenticated! Ready to send commands!");

                            PageMethods.NetmessageConnectToAgent(originalMac, SignalRConnector.SendMessage);

                            fireCommandFromButton("hello");
                        } else {
                            addTextToTerminal("Failed to authenticate: " + jsonObject.authenticateErrorMessage);
                        }
                    }
                } else if (jsonObject.messageType == 9998) {
                    PageMethods.NetmessagePong(SignalRConnector.SendMessage);
                } else if (jsonObject.messageType == 22) {
                    addTextToTerminal(jsonObject.reponseMessage);
                } else if (jsonObject.messageType == 1000) {
                    addTextToTerminal("Trying to reconnect");
                    $.signalR.hub.reconnect();
                } else {
                    console.log(jsonObject.messageType);
                }
            };
            $.signalR.signalRInterface.client.MessageVerified = function (guid) { };

            $.signalR.hub.reconnect = function () {
                $.signalR.hub.stop();
                SignalRConnector.Connect();
            };

            // Configuration
            $.signalR.hub.url = "<%= WebEnvironmentHelper.GetMessagingUrl() %>/signalr";
            $.signalR.hub.qs = { "deviceIdentifier": "<%=CometConstants.CometIdentifierCms  + "_" + CmsSessionHelper.CurrentUser.UserId %>" };
            $.signalR.hub.error(function (error) {
                console.log('SignalR error: ' + error);
                $.signalR.hub.reconnect();
            });
            $.connection.json = window.JSON;
            
            var mac = getQueryVariable("Device", true);

            if (mac.length == 0) {
                addTextToTerminal("ERROR: No MAC address found.");
            } else {
                // Put : back in mac address
                var macSplit = mac.split("");

                for (var i = 0; i < macSplit.length; i++) {
                    originalMac += macSplit[i];

                    if (i % 2 != 0 && i != (macSplit.length - 1))
                        originalMac += ":";
                }

                if (firstLoad) {                    
                    addTextToTerminal(getConsoleHeader());                    
                    firstLoad = false;
                }

                window.deviceMac = originalMac;
                addTextToTerminal("Connecting to Messaging service...");
                window.pokeInReady = true;

                console.log("Starting comet connection.");

                // Connect
                SignalRConnector.Connect();
            }
        });
    </script>
</asp:Content>