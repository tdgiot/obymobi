﻿using System;
using System.Web.UI;
using Obymobi.Data.EntityClasses;
using Dionysos.Web.UI;
using Dionysos.Web.UI.DevExControls;
using Dionysos.Web.UI.WebControls;

namespace Obymobi.ObymobiCms.Company
{
    public partial class MessageTemplateCategory : PageLLBLGenEntity
    {
        private void LoadUserControls()
        {
            this.tabsMain.AddTabPage("Message Templates", "MessageTemplates", "~/Company/Subpanels/MessageTemplatePanel.ascx");
        }

        private void SetGui()
        {
            this.tabsMain.TabPages.FindByName("MessageTemplates").ClientVisible = !DataSourceAsMessageTemplateCategory.IsNew;
        }

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            base.DataSourceLoaded += MessageTemplateCategory_DataSourceLoaded;

            base.OnInit(e);
        }

        void MessageTemplateCategory_DataSourceLoaded(object sender)
        {
            this.SetGui();
        }

        public override bool Save()
        {
            if (DataSourceAsMessageTemplateCategory.IsNew)
            {
                this.DataSourceAsMessageTemplateCategory.CompanyId = CmsSessionHelper.CurrentCompanyId;
            }

            return base.Save();
        }

        /// <summary>
        /// Return the page's datasource as a MessagegroupEntity
        /// </summary>
        public MessageTemplateCategoryEntity DataSourceAsMessageTemplateCategory
        {
            get
            {
                return this.DataSource as MessageTemplateCategoryEntity;
            }
        }
    }
}