﻿using System;
using Obymobi.Data.EntityClasses;
using Obymobi.ObymobiCms.MasterPages;
using Dionysos;
using Dionysos.Data;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.ObymobiCms.Company
{
    public partial class Netmessage : Dionysos.Web.UI.PageLLBLGenEntity
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Hide toolbar buttons, only show cancel (aka. go back)
            var master = this.Master as MasterPageEntity;
            if (master != null)
            {
                master.ToolBar.Visible = false;
            }
            this.btBack.Attributes.Add("onClick", "javascript:history.back(); return false;");

            // Sender
            if (this.DataSourceAsNetmessage.SenderCustomerId.HasValue)
            {
                var customer = new CustomerEntity(this.DataSourceAsNetmessage.SenderCustomerId.Value);
                this.hlSender.Text = string.Format("{0}: {1}", EntityInformationUtil.GetFriendlyName(customer), customer.Lastname);
            }
            else if (this.DataSourceAsNetmessage.SenderClientId.HasValue)
            {
                var client = new ClientEntity(this.DataSourceAsNetmessage.SenderClientId.Value);
                this.hlSender.NavigateUrl = "~/Company/Client.aspx?id=" + client.ClientId;
                this.hlSender.Text = string.Format("{0}: {1} - {2}", EntityInformationUtil.GetFriendlyName(client), client.MacAddress, client.DeliverypointEntity.Number);
            }
            else if (this.DataSourceAsNetmessage.SenderCompanyId.HasValue)
            {
                var company = new CompanyEntity(this.DataSourceAsNetmessage.SenderCompanyId.Value);
                this.hlSender.NavigateUrl = "~/Company/Company.aspx?id=" + company.CompanyId;
                this.hlSender.Text = string.Format("{0}: {1}", EntityInformationUtil.GetFriendlyName(company), company.Name);
            }
            else if (this.DataSourceAsNetmessage.SenderDeliverypointId.HasValue)
            {
                var dp = new DeliverypointEntity(this.DataSourceAsNetmessage.SenderDeliverypointId.Value);
                this.hlSender.NavigateUrl = "~/Company/Deliverypoint.aspx?id=" + dp.DeliverypointId;
                this.hlSender.Text = string.Format("{0}: {1}", EntityInformationUtil.GetFriendlyName(dp), dp.Number);
            }
            else if (this.DataSourceAsNetmessage.SenderTerminalId.HasValue)
            {
                var terminal = new TerminalEntity(this.DataSourceAsNetmessage.SenderTerminalId.Value);
                this.hlSender.NavigateUrl = "~/Company/Terminal.aspx?id=" + terminal.TerminalId;
                this.hlSender.Text = string.Format("{0}: {1}", EntityInformationUtil.GetFriendlyName(terminal), terminal.Name);
            }
            else
                this.hlSender.Text = "Not specified";

            // Receiver
            if (this.DataSourceAsNetmessage.ReceiverCustomerId.HasValue)
            {
                var customer = new CustomerEntity(this.DataSourceAsNetmessage.ReceiverCustomerId.Value);
                this.hlReceiver.Text = string.Format("{0}: {1}", EntityInformationUtil.GetFriendlyName(customer), customer.Lastname);
            }
            else if (this.DataSourceAsNetmessage.ReceiverClientId.HasValue)
            {
                var client = new ClientEntity(this.DataSourceAsNetmessage.ReceiverClientId.Value);
                this.hlReceiver.NavigateUrl = "~/Company/Client.aspx?id=" + client.ClientId;
                this.hlReceiver.Text = string.Format("{0}: {1} - {2}", EntityInformationUtil.GetFriendlyName(client), client.MacAddress, client.DeliverypointEntity.Number);
            }
            else if (this.DataSourceAsNetmessage.ReceiverCompanyId.HasValue)
            {
                var company = new CompanyEntity(this.DataSourceAsNetmessage.ReceiverCompanyId.Value);
                this.hlReceiver.NavigateUrl = "~/Company/Company.aspx?id=" + company.CompanyId;
                this.hlReceiver.Text = string.Format("{0}: {1}", EntityInformationUtil.GetFriendlyName(company), company.Name);
            }
            else if (this.DataSourceAsNetmessage.ReceiverDeliverypointId.HasValue)
            {
                var dp = new DeliverypointEntity(this.DataSourceAsNetmessage.ReceiverDeliverypointId.Value);
                this.hlReceiver.NavigateUrl = "~/Company/Deliverypoint.aspx?id=" + dp.DeliverypointId;
                this.hlReceiver.Text = string.Format("{0}: {1}", EntityInformationUtil.GetFriendlyName(dp), dp.Number);
            }
            else if (this.DataSourceAsNetmessage.ReceiverTerminalId.HasValue)
            {
                var terminal = new TerminalEntity(this.DataSourceAsNetmessage.ReceiverTerminalId.Value);
                this.hlReceiver.NavigateUrl = "~/Company/Terminal.aspx?id=" + terminal.TerminalId;
                this.hlReceiver.Text = string.Format("{0}: {1}", EntityInformationUtil.GetFriendlyName(terminal), terminal.Name);
            }
            else
                this.hlReceiver.Text = "Not Specified";

            this.lblMessageType.Text = string.Format("{0} ({1})", this.DataSourceAsNetmessage.MessageTypeAsEnum.ToString(), this.DataSourceAsNetmessage.MessageType.ToString());
            this.lblStatus.Text = this.DataSourceAsNetmessage.StatusAsEnum.ToString();

            if (this.DataSourceAsNetmessage.CreatedUTC.HasValue)
                this.lblCreated.Text = this.DataSourceAsNetmessage.CreatedUTC.Value.UtcToLocalTime().ToString();

            this.lblDescription.Text = NetmessageHelper.CreateTypedNetmessageModelFromEntity(this.DataSourceAsNetmessage).ToString();
            this.lblFieldValue1.Text = this.DataSourceAsNetmessage.FieldValue1;
            this.lblFieldValue2.Text = this.DataSourceAsNetmessage.FieldValue2;
            this.lblFieldValue3.Text = this.DataSourceAsNetmessage.FieldValue3;
            this.lblFieldValue4.Text = this.DataSourceAsNetmessage.FieldValue4;
            this.lblFieldValue5.Text = this.DataSourceAsNetmessage.FieldValue5;
            this.lblFieldValue6.Text = this.DataSourceAsNetmessage.FieldValue6;
            this.lblFieldValue7.Text = this.DataSourceAsNetmessage.FieldValue7;
            this.lblFieldValue8.Text = this.DataSourceAsNetmessage.FieldValue8;
            this.lblFieldValue9.Text = this.DataSourceAsNetmessage.FieldValue9;
            //this.lblFieldValue10.Text = this.DataSourceAsNetmessage.FieldValue10;
            this.lblErrorMessage.Text = DataSourceAsNetmessage.ErrorMessage;
        }

        #region Properties


        /// <summary>
        /// Return the page's datasource as a SurveyQuestionEntity
        /// </summary>
        public NetmessageEntity DataSourceAsNetmessage
        {
            get
            {
                return this.DataSource as NetmessageEntity;
            }
        }

        #endregion
    }
}