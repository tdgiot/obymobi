﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using Crave.Api.Logic.Requests.Publishing;
using Crave.Api.Logic.Timestamps;
using Crave.Api.Logic.UseCases;
using Dionysos.Data.LLBLGen;
using Dionysos.Web.UI;
using Obymobi.Cms.Logic.HelperClasses;
using Obymobi.Cms.Logic.Requests;
using Obymobi.Cms.Logic.UseCases;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Web.Media;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Company
{
    public partial class UIMode : Dionysos.Web.UI.PageLLBLGenEntity
    {
        #region

        private Transaction transaction = null;

        #endregion

        #region Methods

        protected override void OnInit(EventArgs e)
		{
            this.LoadUserControls();
            this.DataSourceLoaded += new Dionysos.Delegates.Web.DataSourceLoadedHandler(UIMode_DataSourceLoaded);
			base.OnInit(e);
		}

        void UIMode_DataSourceLoaded(object sender)
        {
            this.SetGui();
        }

        private void LoadUserControls()
        { 
            this.tabsMain.AddTabPage("Tabbladen", "UITabCollection", "~/Company/SubPanels/UITabCollection.ascx");
            this.tabsMain.AddTabPage("Footerbar Items", "UIFooterItemCollection", "~/Company/SubPanels/UIFooterItemCollection.ascx");
        }

        private void SetGui()
        {
            this.ddlType.DataBindEnumStringValuesAsDataSource(typeof(UIModeType));
            this.ddlNewUIModeType.DataBindEnumStringValuesAsDataSource(typeof(UIModeType));

            // UI Mode type
            UIModeType type = this.DataSourceAsUIModeEntity.Type;
            switch (type)
            {
                case UIModeType.VenueOwnedUserDevices:
                    var emenuSettingsPanel = this.LoadControl<Subpanels.UIModeSettingsPanels.EmenuSettingsPanel>("~/Company/SubPanels/UIModeSettingsPanels/EmenuSettingsPanel.ascx");
                    emenuSettingsPanel.UIModeEntity = this.DataSourceAsUIModeEntity;
                    this.plhTypeSpecificSettings.Controls.Add(emenuSettingsPanel);
                    this.RenderDeliverypointGroups();
                    break;
                case UIModeType.VenueOwnedStaffDevices:
                    var consoleSettingsPanel = this.LoadControl<Subpanels.UIModeSettingsPanels.ConsoleSettingsPanel>("~/Company/SubPanels/UIModeSettingsPanels/ConsoleSettingsPanel.ascx");
                    consoleSettingsPanel.UIModeEntity = this.DataSourceAsUIModeEntity;
                    this.plhTypeSpecificSettings.Controls.Add(consoleSettingsPanel);
                    this.RenderTerminals();
                    break;
                default:
                    if (this.DataSourceAsUIModeEntity.PointOfInterestId.HasValue)
                        this.RenderPointsOfInterest();
                    break;
            }

            // For now we only support custom footerbar items in Emenu
            this.tabsMain.TabPages.FindByName("UIFooterItemCollection").Visible = (type == UIModeType.VenueOwnedUserDevices);

            if (this.DataSourceAsUIModeEntity.UITabCollection.Count > 0)
                this.ddlType.Enabled = false;
        }

        private void SetWarnings()
        {
            if (!this.DataSourceAsUIModeEntity.DefaultUITabId.HasValue)
            {
                this.AddInformator(InformatorType.Warning, this.Translate("NoDefaultTabSelected", "Er is geen standaard UI Tab geselecteerd."));
            }

            this.Validate();
        }

        public override bool Save()
        {
            if (this.PageMode == Dionysos.Web.PageMode.Add)
                this.DataSourceAsUIModeEntity.CompanyId = CmsSessionHelper.CurrentCompanyId;

            return base.Save();
        }

        public override bool Delete()
        {
            foreach (UITabEntity tabEntity in this.DataSourceAsUIModeEntity.UITabCollection)
            {
                tabEntity.Delete();
            }
            return base.Delete();
        }

        private void RenderDeliverypointGroups()
        {
            this.plhUsedByDpgs.Visible = true;

            PredicateExpression filter = new PredicateExpression();
            filter.Add(DeliverypointgroupFields.UIModeId == this.DataSourceAsUIModeEntity.UIModeId);

            SortExpression sort = new SortExpression();
            sort.Add(new SortClause(DeliverypointgroupFields.Name, SortOperator.Ascending));
            
            var dpgs = new DeliverypointgroupCollection();
            dpgs.GetMulti(filter, 0, sort);

            this.plhDeliverypointGroups.AddHtml("<tr style='height: 10px;'><td></td></tr>");

            foreach (var dpg in dpgs)
                this.plhDeliverypointGroups.AddHtml("<tr><td><a href=\"{0}\" target=\"_blank\">{1}</a></td></tr>", this.ResolveUrl("~/Company/Deliverypointgroup.aspx?Id=" + dpg.DeliverypointgroupId), dpg.Name);
        }

        private void RenderTerminals()
        {
            this.plhUsedByTerminals.Visible = true;

            PredicateExpression filter = new PredicateExpression();
            filter.Add(TerminalFields.UIModeId == this.DataSourceAsUIModeEntity.UIModeId);

            SortExpression sort = new SortExpression();
            sort.Add(new SortClause(TerminalFields.Name, SortOperator.Ascending));

            var terminals = new TerminalCollection();
            terminals.GetMulti(filter, 0, sort);

            this.plhTerminals.AddHtml("<tr style='height: 10px;'><td></td></tr>");

            foreach (var terminal in terminals)
                this.plhTerminals.AddHtml("<tr><td><a href=\"{0}\" target=\"_blank\">{1}</a></td></tr>", this.ResolveUrl("~/Company/Terminal.aspx?Id=" + terminal.TerminalId), terminal.Name);
        }

        private void RenderPointsOfInterest()
        {
            this.plhUsedByPointsOfInterest.Visible = true;

            PointOfInterestEntity poiEntity = this.DataSourceAsUIModeEntity.PointOfInterestEntity;

            this.plhPointsOfInterest.AddHtml("<tr style='height: 10px;'><td></td></tr>");
            this.plhPointsOfInterest.AddHtml("<tr><td><a href=\"{0}\" target=\"_blank\">{1}</a></td></tr>", this.ResolveUrl("~/Generic/PointOfInterest.aspx?Id=" + poiEntity.PointOfInterestId), poiEntity.Name);
        }

        private void HookUpEvents()
        {
            this.btnCopyUIMode.Click += btnCopyUIMode_Click;
        }

        private void CopyUIMode(UIModeEntity oldUIModeEntity, string newUImodeName, UIModeType type)
        {
            AppendLog("Copying UI mode... '{0}'", newUImodeName);

            // Copy UI mode
            ArrayList uiModeFieldsNotToBeCopied = new ArrayList();
            uiModeFieldsNotToBeCopied.Add(UIModeFields.Name);
            uiModeFieldsNotToBeCopied.Add(UIModeFields.Type);

            UIModeEntity newUIModeEntity = new UIModeEntity();
            newUIModeEntity.AddToTransaction(this.transaction);
            LLBLGenEntityUtil.CopyFields(oldUIModeEntity, newUIModeEntity, uiModeFieldsNotToBeCopied);
            newUIModeEntity.Name = newUImodeName;
            newUIModeEntity.Type = type;
            newUIModeEntity.Save();

            // Default UI tab
            int newDefaultUITabId = -1;

            // Copy UI tabs
            List<UITabType> supportedUITabTypes = UITabHelper.GetSupportedUITabTypes(type);
            foreach (UITabEntity oldUITabEntity in oldUIModeEntity.UITabCollection)
            {
                if (!supportedUITabTypes.Contains(oldUITabEntity.TypeAsEnum))
                    continue;

                AppendLog("Copying UI tab... '{0}'", oldUITabEntity.CaptionOrType);

                ArrayList uiTabFieldsNotToBeCopied = new ArrayList();
                uiTabFieldsNotToBeCopied.Add(UITabFields.UIModeId);

                UITabEntity newUITabEntity = new UITabEntity();
                newUITabEntity.AddToTransaction(this.transaction);
                LLBLGenEntityUtil.CopyFields(oldUITabEntity, newUITabEntity, uiTabFieldsNotToBeCopied);
                newUITabEntity.UIModeId = newUIModeEntity.UIModeId;
                newUITabEntity.Save();

                foreach (UIWidgetEntity oldUIWidgetEntity in oldUITabEntity.UIWidgetCollection)
                {
                    AppendLog("Copying UI widget... '{0}'", oldUIWidgetEntity.NameOrTypeName);

                    ArrayList uiWidgetFieldsNotToBeCopied = new ArrayList { UIWidgetFields.UITabId };

                    UIWidgetEntity newUIWidgetEntity = new UIWidgetEntity();
                    newUIWidgetEntity.AddToTransaction(this.transaction);
                    LLBLGenEntityUtil.CopyFields(oldUIWidgetEntity, newUIWidgetEntity, uiWidgetFieldsNotToBeCopied);
                    newUIWidgetEntity.UITabId = newUITabEntity.UITabId;
                    newUIWidgetEntity.Save();

                    foreach (MediaEntity oldMedia in oldUIWidgetEntity.MediaCollection)
                    {
                        CopyMediaRequest request = new CopyMediaRequest
                        {
                            MediaToCopy = oldMedia,
                            ReferenceField = MediaFields.UIWidgetId,
                            ReferenceValue = newUIWidgetEntity.UIWidgetId,
                            CopyMediaOnCdn = MediaCloudHelper.CopyMediaOnCdn,
                            Transaction = this.transaction
                        };
                        new CopyMediaUseCase().Execute(request);
                    }
                }

                if (oldUIModeEntity.DefaultUITabId.HasValue && oldUIModeEntity.DefaultUITabId == oldUITabEntity.UITabId)
                    newDefaultUITabId = newUITabEntity.UITabId;
            }

            // Set default UI tab 
            if (newDefaultUITabId > 0)
            {
                AppendLog("Found a default UI tab and setting it now... '{0}'", newDefaultUITabId);
                newUIModeEntity.DefaultUITabId = newDefaultUITabId;
                newUIModeEntity.Save();
            }            
        }

		#endregion

		#region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            this.HookUpEvents();

            if (!this.IsPostBack && !this.DataSourceAsUIModeEntity.IsNew)
                this.SetWarnings();
        }

        private void btnCopyUIMode_Click(object sender, EventArgs e)
        {
            if (this.tbNewUIModeName.Value.Length <= 0)
            {
                this.MultiValidatorDefault.AddError("Please enter a name for the new UI mode");
                this.Validate();
            }
            else if (this.ddlNewUIModeType.ValidId < 0)
            {
                this.MultiValidatorDefault.AddError("Please select an type for the new UI mode");
                this.Validate();
            }
            else
            {
                StartSession();

                new Thread((param) =>
                {
                    HttpContext.Current = (HttpContext)param;
                    int userId = CmsSessionHelper.CurrentUser.UserId;

                    if (Thread.GetNamedDataSlot("Role") == null)
                        Thread.AllocateNamedDataSlot("Role");

                    Thread.SetData(Thread.GetNamedDataSlot("Role"), CmsSessionHelper.CurrentRole);

                    this.transaction = new Transaction(System.Data.IsolationLevel.ReadUncommitted, "CopyUIMode");
                    try
                    {
                        AppendLog("Copying of UI mode '{0}' started...", this.DataSourceAsUIModeEntity.Name);

                        this.CopyUIMode(this.DataSourceAsUIModeEntity, this.tbNewUIModeName.Value, (UIModeType)this.ddlNewUIModeType.ValidId);

                        transaction.Commit();
                        AppendLog("New UI mode '{0}' successfully created!", this.tbNewUIModeName.Value);
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        AppendLog("Something went wrong while copying UI mode '{0}'.", ex.Message);
                    }
                    finally
                    {
                        transaction.Dispose();
                        EndSessionOnGetLog();
                    }
                }).Start(HttpContext.Current);

                string message = "Copying of the UI mode started in the background and can take a couple of minutes to complete!";
                ((Dionysos.Web.UI.PageDefault)this.Page).AddInformator(Dionysos.Web.UI.InformatorType.Information, message);
            }
        }

		#endregion

		#region Properties


		/// <summary>
		/// Return the page's datasource as a UIModeEntity
		/// </summary>
		public UIModeEntity DataSourceAsUIModeEntity
		{
			get
			{
				return this.DataSource as UIModeEntity;
			}
		}

		#endregion

        #region Logging

        private static string existsKey = "CopyUIModeExists" + CmsSessionHelper.CurrentUser.UserId;
        private static string logKey = "CopyUIModeLog" + CmsSessionHelper.CurrentUser.UserId;
        private static bool endSessionOnGetLog = false;

        [WebMethod(EnableSession = true), ScriptMethod]
        public static string GetLog()
        {
            string log = string.Empty;
            if (LogExists())
            {
                log = (string)HttpContext.Current.Application.Get(logKey);

                if (endSessionOnGetLog)
                {
                    endSessionOnGetLog = false;
                    EndSession();
                }
            }                
            return log;
        }

        [WebMethod(EnableSession = true), ScriptMethod]
        public static bool SessionExists()
        {
            return (HttpContext.Current.Application.Get(existsKey) != null);
        }

        [WebMethod(EnableSession = true), ScriptMethod]
        public static bool LogExists()
        {
            return (HttpContext.Current.Application.Get(logKey) != null);
        }

        public static void AppendLog(string log, params object[] args)
        {
            if (SessionExists())
                HttpContext.Current.Application[logKey] += string.Format(log + "\n", args);
        }

        public static void StartSession()
        {
            HttpContext.Current.Application[existsKey] = true;
            HttpContext.Current.Application[logKey] = string.Empty;            
        }

        public static void EndSessionOnGetLog()
        {
            endSessionOnGetLog = true;
        }

        public static void EndSession()
        {
            if (SessionExists())
                HttpContext.Current.Application.Remove(existsKey);
        }

        #endregion
    }
}
