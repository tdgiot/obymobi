﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data;

namespace Obymobi.ObymobiCms.Company
{
    public partial class Routes : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            LLBLGenProDataSource datasource = this.DataSource as LLBLGenProDataSource;
            if (datasource != null)
            {
                PredicateExpression filter = new PredicateExpression(RouteFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
                datasource.FilterToUse = filter;

                PrefetchPath prefetch = new PrefetchPath(EntityType.RouteEntity);
                prefetch.Add(RouteEntity.PrefetchPathRoutestepCollection);

                datasource.PrefetchPathToUse = prefetch;
            }

            this.MainGridView.DataBound += new EventHandler(MainGridView_DataBound);
        }

        void MainGridView_DataBound(object sender, EventArgs e)
        {
            if (this.MainGridView.Columns["Routesteps"] != null)
            {
                this.MainGridView.Columns["Routesteps"].Width = 30;
                this.MainGridView.Columns["Routesteps"].CellStyle.HorizontalAlign = HorizontalAlign.Center;
            }
        }
    }
}
