﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.RoomControlSectionItem" Codebehind="RoomControlSectionItem.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
				    <D:PlaceHolder runat="server" ID="plhGeneric">
					    <table class="dataformV2">
						    <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblName">Name</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <D:TextBoxString ID="tbName" runat="server"></D:TextBoxString>
							    </td>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblType">Type</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
                                    <X:ComboBoxInt ID="cbType" runat="server" IsRequired="true" UseDataBinding="true" Enabled="false"></X:ComboBoxInt>
							    </td>
					        </tr>
                            <tr>
                                <D:PlaceHolder ID="plhStationListing" runat="server" Visible="False">
							        <td class="label">
							            <D:LabelEntityFieldInfo runat="server" id="lblStationList">Station list</D:LabelEntityFieldInfo>
							        </td>
							        <td class="control">
							            <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlStationListId" IncrementalFilteringMode="StartsWith" EntityName="StationList" TextField="Name" ValueField="StationListId" PreventEntityCollectionInitialization="true" />
							        </td>
                                </D:PlaceHolder>
                                <D:PlaceHolder ID="plhService" runat="server" Visible="False">
                                    <td class="label">
                                        <D:Label runat="server" id="lblService">Service</D:Label>
                                    </td>
                                    <td class="control">
                                        <X:ComboBoxInt ID="cbService" runat="server" UseDataBinding="true"></X:ComboBoxInt>
                                    </td>
                                </D:PlaceHolder>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblSortOrder">Sort order</D:LabelEntityFieldInfo>
							    </td>
                                <td class="control">
								    <D:TextBoxInt ID="tbSortOrder" runat="server"></D:TextBoxInt>
							    </td>
                            </tr>
                            <tr>
                                <D:PlaceHolder ID="plhScene" runat="server" Visible="False">
                                    <td class="label">
                                        <D:Label runat="server" id="lblScene">Scene</D:Label>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString ID="tbScene" runat="server"></D:TextBoxString>
                                    </td>
                                    <td class="label">
                                        <D:Label runat="server" id="lblSceneSuccessMessage">Success message</D:Label>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString ID="tbSceneSuccessMessage" runat="server"></D:TextBoxString>
                                    </td>
                                </D:PlaceHolder>
                                <D:PlaceHolder ID="plhSleepTimer" runat="server" Visible="False">
                                    <td class="label">
                                        <D:Label runat="server" id="lblTurnOffAllScene">'Turn off all Audio & Video' scene</D:Label>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString ID="tbTurnOffAllScene" runat="server"></D:TextBoxString>
                                    </td>
                                </D:PlaceHolder>
					        </tr>
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblVisible">Visible</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:CheckBox runat="server" ID="cbVisible" />
                                </td>
                            </tr>
                        </table>	
                    </D:PlaceHolder>
                    <D:PlaceHolder ID="plhRemoteControl" runat="server" Visible="false">
                        <D:Panel ID="pnlPower" runat="server" GroupingText="Power">
                            <table class="dataformV2">
                                <tr>
                                    <td class="label">
                                        <D:Label runat="server" id="lblPowerButtonScene">Power button scene</D:Label>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString ID="tbPowerButtonScene" runat="server"></D:TextBoxString>
                                    </td>                                    
                                    <td class="label">
                                    </td>
                                    <td class="control">
                                    </td>
                                </tr>
                            </table>
                        </D:Panel>
                        <D:Panel ID="pnlVolume" runat="server" GroupingText="Volume">
                            <table class="dataformV2">
                                <tr>
                                    <td class="label">
                                        <D:Label runat="server" id="lblVolumeUpScene">Volume up scene</D:Label>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString ID="tbVolumeUpScene" runat="server"></D:TextBoxString>
                                    </td>
                                    <td class="label">
                                        <D:Label runat="server" id="lblVolumeDownScene">Volume down scene</D:Label>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString ID="tbVolumeDownScene" runat="server"></D:TextBoxString>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        <D:Label runat="server" id="lblVolumeMuteScene">Mute scene</D:Label>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString ID="tbVolumeMuteScene" runat="server"></D:TextBoxString>
                                    </td>
                                </tr>
                            </table>
                        </D:Panel>
                        <D:Panel ID="pnlChannel" runat="server" GroupingText="Channel">
                            <table class="dataformV2">
                                <tr>
                                    <td class="label">
                                        <D:Label runat="server" id="lblChannelUpScene">Channel up scene</D:Label>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString ID="tbChannelUpScene" runat="server"></D:TextBoxString>
                                    </td>
                                    <td class="label">
                                        <D:Label runat="server" id="lblChannelDown">Channel down scene</D:Label>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString ID="tbChannelDownScene" runat="server"></D:TextBoxString>
                                    </td>
                                </tr>                                
                            </table>
                        </D:Panel>             
                        <D:Panel ID="pnlNavigation" runat="server" GroupingText="Navigation">
                            <table class="dataformV2">
                                <tr>
                                    <td class="label">
                                        <D:Label runat="server" id="lblLeftButtonScene">Left button scene</D:Label>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString ID="tbLeftButtonScene" runat="server"></D:TextBoxString>
                                    </td>
                                    <td class="label">
                                        <D:Label runat="server" id="lblTopButtonScene">Top button scene</D:Label>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString ID="tbTopButtonScene" runat="server"></D:TextBoxString>
                                    </td>
                                </tr>                                
                                <tr>
                                    <td class="label">
                                        <D:Label runat="server" id="lblRightButtonScene">Right button scene</D:Label>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString ID="tbRightButtonScene" runat="server"></D:TextBoxString>
                                    </td>
                                    <td class="label">
                                        <D:Label runat="server" id="lblBottomButtonScene">Bottom button scene</D:Label>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString ID="tbBottomButtonScene" runat="server"></D:TextBoxString>
                                    </td>
                                </tr>                                
                                <tr>
                                    <td class="label">
                                        <D:Label runat="server" id="lblSelectButtonScene">Select button scene</D:Label>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString ID="tbSelectButtonScene" runat="server"></D:TextBoxString>
                                    </td>                                    
                                </tr>                                
                            </table>
                        </D:Panel>       
                        <D:Panel ID="pnlNumpad" runat="server" GroupingText="Numpad">
                            <table class="dataformV2">
                                <tr>
                                    <td class="label">
                                        <D:Label runat="server" id="lblOneButtonScene">1 button scene</D:Label>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString ID="tbOneButtonScene" runat="server"></D:TextBoxString>
                                    </td>
                                    <td class="label">
                                        <D:Label runat="server" id="lblTwoButtonScene">2 button scene</D:Label>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString ID="tbTwoButtonScene" runat="server"></D:TextBoxString>
                                    </td>
                                </tr>                                
                                <tr>
                                    <td class="label">
                                        <D:Label runat="server" id="lblThreeButtonScene">3 button scene</D:Label>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString ID="tbThreeButtonScene" runat="server"></D:TextBoxString>
                                    </td>
                                    <td class="label">
                                        <D:Label runat="server" id="lblFourButtonScene">4 button scene</D:Label>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString ID="tbFourButtonScene" runat="server"></D:TextBoxString>
                                    </td>
                                </tr>                                
                                <tr>
                                    <td class="label">
                                        <D:Label runat="server" id="lblFiveButtonScene">5 button scene</D:Label>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString ID="tbFiveButtonScene" runat="server"></D:TextBoxString>
                                    </td>                                    
                                    <td class="label">
                                        <D:Label runat="server" id="lblSixButtonScene">6 button scene</D:Label>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString ID="tbSixButtonScene" runat="server"></D:TextBoxString>
                                    </td>                                    
                                </tr>                                
                                <tr>
                                    <td class="label">
                                        <D:Label runat="server" id="lblSevenButtonScene">7 button scene</D:Label>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString ID="tbSevenButtonScene" runat="server"></D:TextBoxString>
                                    </td>                                    
                                    <td class="label">
                                        <D:Label runat="server" id="lblEightButtonScene">8 button scene</D:Label>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString ID="tbEightButtonScene" runat="server"></D:TextBoxString>
                                    </td>                                    
                                </tr>
                                <tr>
                                    <td class="label">
                                        <D:Label runat="server" id="lblNineButtonScene">9 button scene</D:Label>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString ID="tbNineButtonScene" runat="server"></D:TextBoxString>
                                    </td>                                    
                                    <td class="label">
                                        <D:Label runat="server" id="lblZeroButtonScene">0 button scene</D:Label>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString ID="tbZeroButtonScene" runat="server"></D:TextBoxString>
                                    </td>                                    
                                </tr>
                            </table>
                        </D:Panel>  
                    </D:PlaceHolder>
                    <D:Panel ID="pnlInfraRed" runat="server" GroupingText="Infra-red">
                        <table class="dataformV2">
                            <tr>
                                <td class="label">
                                    <D:Label runat="server" id="lblInfraredConfigurationId">Infra-red configuration</D:Label>
                                </td>
                                <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlInfraredConfigurationId" IncrementalFilteringMode="StartsWith" EntityName="InfraredConfiguration" TextField="Name" ValueField="InfraredConfigurationId" />
                                </td>
                                <td class="label">
                                </td>
                                <td class="control">
                                </td>
                            </tr>
                        </table>
                    </D:Panel>
				</Controls>
			</X:TabPage>	
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>

