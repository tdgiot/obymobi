﻿using System;
using System.Globalization;
using System.Web.Script.Serialization;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;
using Obymobi.ObymobiCms.UI;

namespace Obymobi.ObymobiCms.Company
{
    public partial class SetupCode : PageLLBLGenEntityCms
    {
        #region Properties

        public SetupCodeEntity DataSourceAsSetupCodeEntity
        {
            get
            {
                return this.DataSource as SetupCodeEntity;
            }
        }

        #endregion

        protected override void OnInit(EventArgs e)
        {
            this.btnDownloadSetupConfig.Click += btnDownloadSetupConfig_Click;
            this.DataSourceLoaded += SetupCode_DataSourceLoaded;
            base.OnInit(e);
        }

        void SetupCode_DataSourceLoaded(object sender)
        {
            this.lblCodeValue.Text = this.DataSourceAsSetupCodeEntity.Code;
            this.lblDeliverypointgroupValue.Text = this.DataSourceAsSetupCodeEntity.DeliverypointgroupEntity.Name;

            var convertedTime = CmsSessionHelper.ConvertDateTimeToUserTimeZone(this.DataSourceAsSetupCodeEntity.ExpireDateUTC);
            this.lblExpireDateValue.Text = convertedTime.ToString("yyyy-MM-dd HH:mm");
        }

        void btnDownloadSetupConfig_Click(object sender, EventArgs e)
        {
            EmenuConfiguration emenuConfig = SetupCodeHelper.GetConfiguration(DataSourceAsSetupCodeEntity.Code);
            if (emenuConfig.ErrorCode != 0)
            {
                this.MultiValidatorDefault.AddError(Translate("Error", "Something went wrong while generating setup configuration."));
                this.MultiValidatorDefault.AddInformation("Message: " + emenuConfig.Error);
                this.Validate();
            }
            else
            {
                JavaScriptSerializer scriptSerializer = new JavaScriptSerializer();
                String emenuConfigJson = scriptSerializer.Serialize(emenuConfig);

                Response.ContentType = "application/json";
                Response.AddHeader("content-disposition", "attachment; filename=CraveConfiguration.json");

                // write string data to Response.OutputStream here
                Response.Write(emenuConfigJson);
                Response.Write("\n");

                Response.End();
            }
        }
    }
}