﻿using System;
using Obymobi.Enums;
using Obymobi.Data.EntityClasses;

namespace Obymobi.ObymobiCms.Company
{
    public partial class WifiConfiguration : Obymobi.ObymobiCms.UI.PageLLBLGenEntityCms
    {
        protected override void OnInit(EventArgs e)
        {
            LoadUserControls();
            this.DataSourceLoaded += WifiConfiguration_DataSourceLoaded;
            base.OnInit(e);
        }

        private void WifiConfiguration_DataSourceLoaded(object sender)
        {
            SetGui();
        }

        private void LoadUserControls()
        {
            // Set Enum values to Comboboxes
            this.ddlSecurity.DataBindEnum<WifiSecurityMethod>();
            this.ddlEapMethod.DataBindEnum<WifiEapMode>();
            this.ddlEapPhase2.DataBindEnum<WifiPhase2Authentication>();
        }

        private void SetGui()
        {
            this.plhWifiAdvanced.Visible = (this.DataSourceAsWifiConfigurationEntity.SecurityAsEnum == WifiSecurityMethod.EAP);
        }

        #region Properties

        /// <summary>
        ///     Return the page's datasource as a WifiConfigurationEntity
        /// </summary>
        public WifiConfigurationEntity DataSourceAsWifiConfigurationEntity
        {
            get { return this.DataSource as WifiConfigurationEntity; }
        }

        #endregion
    }
}