﻿using System;
using Dionysos;
using Dionysos.Web.UI;
using Obymobi.Cms.Logic.HelperClasses;
using Obymobi.Data.EntityClasses;

namespace Obymobi.ObymobiCms.Company
{
    public partial class Route : Dionysos.Web.UI.PageLLBLGenEntity
    {
        protected override void OnInit(EventArgs e)
        {            
            this.LoadUserControls();
            this.DataSourceLoaded += new Dionysos.Delegates.Web.DataSourceLoadedHandler(Route_DataSourceLoaded);
            base.OnInit(e);
            //this.Informators.Clear(); // GK bit of dirty... But otherwise if you have 2 different messages between postbacks it will show both.
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
            if(this.PageMode != Dionysos.Web.PageMode.Add)
                this.Validate();
        }

        private void LoadUserControls()
        {
            this.tabsMain.AddTabPage("Routesteps", "RoutestepCollection", "~/Company/SubPanels/RoutestepCollection.ascx");
        }

        public override bool Save()
        {
            if (this.PageMode == Dionysos.Web.PageMode.Add)
                this.DataSourceAsRouteEntity.CompanyId = CmsSessionHelper.CurrentCompanyId;

            return base.Save();
        }

        void SetGui()
        {            
            // Inform the user of non-usable time outs            
            if (this.DataSourceAsRouteEntity.BeingHandledSupportNotificationTimeoutMinutes >= this.DataSourceAsRouteEntity.StepTimeoutMinutes &&
                this.DataSourceAsRouteEntity.RetrievalSupportNotificationTimeoutMinutes >= this.DataSourceAsRouteEntity.StepTimeoutMinutes)
            {
                // Both Notification timeouts are larger than step time-out, so not usable
                this.AddInformatorInfo("Support notifications will not be sent because both notification timeouts are larger than or equal to the step-timeout");
            }
            else if (this.DataSourceAsRouteEntity.BeingHandledSupportNotificationTimeoutMinutes >= this.DataSourceAsRouteEntity.StepTimeoutMinutes)
            {
                this.AddInformatorInfo("On-the-case support notifications will not be sent because the notification timeouts are larger than or equal as the step-timeout");
            }
            else if (this.DataSourceAsRouteEntity.RetrievalSupportNotificationTimeoutMinutes >= this.DataSourceAsRouteEntity.StepTimeoutMinutes)
            {
                this.AddInformatorInfo("Retrieval support notifications will not be sent because the notification timeouts are larger than or equal as the step-timeout");
            }            

            var routes = RouteHelper.GetRouteCollection(CmsSessionHelper.CurrentCompanyId);
            this.ddlEscalationRouteId.DataSource = routes;
            this.ddlEscalationRouteId.DataBind();

            string stepTextTranslated = this.Translate("Step", "Stap");
            string yesText = this.Translate("Yes", "Ja");
            string noText = this.Translate("No", "Nee");
            foreach (var step in this.DataSourceAsRouteEntity.RoutestepCollection)
            { 
                // Link url
                string url = this.ResolveUrl("~/Company/Routestep.aspx?id=" + step.RoutestepId);

                // Render step heading
                this.plhSummary.AddHtml("<tr><td colspan=\"3\"><strong><a href=\"{0}\">{1} {2}</a></strong></td></tr>",
                    url, stepTextTranslated, step.Number);

                // Render step settings
                this.plhSummary.AddHtml("<tr><td colspan=\"3\">");

                this.plhSummary.AddHtml("{0}: {1}<br />", this.Translate("ContinueOnFailure", "Doorgaan na falen"), step.ContinueOnFailure ? yesText : noText);
                this.plhSummary.AddHtml("{0}: {1}<br />", this.Translate("CompleteRouteOnComplete", "Route afronden na success"), step.CompleteRouteOnComplete ? yesText : noText);
                int timeout = step.TimeoutMinutes.HasValue && step.TimeoutMinutes > 0 ? step.TimeoutMinutes.Value : this.DataSourceAsRouteEntity.StepTimeoutMinutes;
                this.plhSummary.AddHtml("Timeout: {1}<br />", this.Translate("CompleteRouteOnComplete", "Route afronden na success"), timeout);

                this.plhSummary.AddHtml("</td></tr>");
                
                // Render handlers
                foreach (var handler in step.RoutestephandlerCollection)
                {
                    string handlerText = "Cloud";
                    if (handler.TerminalId.HasValue)
                    {
                        handlerText = "<a href=\"Terminal.aspx?id={0}\">{1}</a>".FormatSafe(handler.TerminalId, handler.TerminalEntity.Name);
                    }

                    this.plhSummary.AddHtml("<tr><td>{0}&nbsp;&nbsp;</td><td>{1}&nbsp;&nbsp;</td><td><a href=\"Routestephandler.aspx?id={2}\">Details...</a></tr>", 
                    handler.RoutestephandlerTypeAsText, handlerText, handler.RoutestephandlerId);
                }

                this.plhSummary.AddHtml("<tr><td colspan=\"2\">&nbsp;</td></tr>");
            }
        }

        void Route_DataSourceLoaded(object sender)
        {
            this.SetGui();

            CheckIfRouteIsValid();
        }

        private void CheckIfRouteIsValid()
        {
            bool isInValid = false;
            bool hasTerminalStep = false;
            bool hasAutomatedStep = false;
            foreach (var step in this.DataSourceAsRouteEntity.RoutestepCollection)
            {
                bool stepHasTerminal = false;
                bool stepHasCloud = false;
                foreach (var handler in step.RoutestephandlerCollection)
                {
                    if (handler.TerminalId.HasValue)
                    {
                        stepHasTerminal = true;
                    }
                    else
                    {
                        if (hasTerminalStep || hasAutomatedStep)
                        {
                            isInValid = true;
                            break;
                        }
                        stepHasCloud = true;
                    }
                }

                if (stepHasTerminal) hasTerminalStep = true;
                if (stepHasCloud) hasAutomatedStep = true;

                if (isInValid) break;
            }

            if (isInValid)
            {
                this.AddInformatorInfo("A route with multiple Cloud steps or Cloud steps after a terminal step is currently not (officially) supported");
            }
        }

        /// <summary>
        /// Return the page's datasource as a FormEntity
        /// </summary>
        public RouteEntity DataSourceAsRouteEntity
        {
            get
            {
                return this.DataSource as RouteEntity;
            }
        }
    }
}
