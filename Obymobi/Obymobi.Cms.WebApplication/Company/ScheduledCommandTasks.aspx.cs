﻿using System;
using System.Drawing;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;

namespace Obymobi.ObymobiCms.Company
{
    public partial class ScheduledCommandTasks : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.MainGridView != null)
                this.MainGridView.HtmlRowPrepared += MainGridView_HtmlRowPrepared;

            LLBLGenProDataSource datasource = this.DataSource as LLBLGenProDataSource;
            if (datasource != null)
            {
                if (CmsSessionHelper.CurrentRole < Role.Administrator)
                {
                    PredicateExpression filter = new PredicateExpression();
                    filter.Add(ScheduledCommandTaskFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
                    datasource.FilterToUse = filter;
                }
            }
        }

        private void MainGridView_HtmlRowPrepared(object sender, DevExpress.Web.ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType != DevExpress.Web.GridViewRowType.Data) return;

            var status = (ScheduledCommandTaskStatus)e.GetValue("Status");
            switch (status)
            {
                case ScheduledCommandTaskStatus.Inactive:
                    e.Row.ForeColor = Color.Gray;
                    break;
                case ScheduledCommandTaskStatus.Pending:
                    // Normal
                    break;
                case ScheduledCommandTaskStatus.Started:
                    e.Row.ForeColor = Color.Blue;
                    break;
                case ScheduledCommandTaskStatus.Completed:
                    e.Row.ForeColor = Color.Green;
                    break;
                case ScheduledCommandTaskStatus.Expired:
                case ScheduledCommandTaskStatus.CompletedWithErrors:
                    e.Row.ForeColor = Color.Red;
                    break;
            }
        }
    }
}
