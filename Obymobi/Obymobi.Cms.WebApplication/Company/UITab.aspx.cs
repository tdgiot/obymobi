﻿using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.Injectables.Validators;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using System.Data;
using System.Linq;
using Obymobi.Data.DaoClasses;
using Obymobi.Enums;
using Dionysos.Web;
using Dionysos.Data.LLBLGen;
using Obymobi.Cms.Logic.HelperClasses;
using Obymobi.Data;
using Obymobi.Logic.HelperClasses;
using EntertainmentHelper = Obymobi.Cms.Logic.HelperClasses.EntertainmentHelper;

namespace Obymobi.ObymobiCms.Company
{
	public partial class UITab : Dionysos.Web.UI.PageLLBLGenEntity
	{
		#region Methods

        private void LoadUserControls()
        {
            UITabEntity uiTabEntity = new UITabEntity(this.EntityId);
            if (uiTabEntity.IsNew)
                return;

            this.tabsMain.AddTabPage("Translations", "CustomTextCollection", "~/Generic/UserControls/CustomTextCollection.ascx");

            UITabType[] tabsWithWidgetSupport = new UITabType[] { UITabType.Home, UITabType.Category, UITabType.Basket, UITabType.More, UITabType.Site, UITabType.ExploreMap };
            if (tabsWithWidgetSupport.Contains(uiTabEntity.TypeAsEnum) && uiTabEntity.UIModeEntity.Type == UIModeType.VenueOwnedUserDevices)
            {
                this.tabsMain.AddTabPage("Widgets", "UIWidgetCollection", "~/Company/SubPanels/UIWidgetCollection.ascx");
            }
        }

        public override bool Save()
        {
            if (this.DataSourceAsUITabEntity.IsNew && QueryStringHelper.TryGetValue("UIModeId", out int uiModeId))
            {
                this.DataSourceAsUITabEntity.UIModeId = uiModeId;
            }

            if (this.DataSourceAsUITabEntity.Type == (int)UITabType.Home ||
               this.DataSourceAsUITabEntity.Type == (int)UITabType.More ||
               this.DataSourceAsUITabEntity.Type == (int)UITabType.Basket)
            {
                PredicateExpression filter = new PredicateExpression
                {
                    UITabFields.Type == this.DataSourceAsUITabEntity.Type,
                    UITabFields.UIModeId == this.DataSourceAsUITabEntity.UIModeId
                };

                if (this.PageMode == PageMode.Edit)
                {
                    filter.Add(UITabFields.UITabId != this.DataSourceAsUITabEntity.UITabId);
                }

                UITabCollection uitabs = new UITabCollection();
                uitabs.GetMulti(filter);

                if (uitabs.Count > 0)
                {
                    this.MultiValidatorDefault.AddError(string.Format("This UI mode already has a {0} tab configured", this.DataSourceAsUITabEntity.Type.ToString()));
                }
            }

            int linkCount = 0;
            if (this.ddlCategoryId.ValidId > 0)
            {
                linkCount++;
            }

            if (this.ddlEntertainmentId.ValidId > 0)
            {
                linkCount++;
            }

            if (this.tbURL.Value.Length > 0)
            {
                linkCount++;
            }

            if (this.ddlMapId.ValidId > 0)
            {
                linkCount++;
            }

            if (linkCount > 1)
            {
                this.MultiValidatorDefault.AddError(this.Translate("UITabOnlyOneLink", "Multiple links from the UITab has been selected. Only one link is allowed."));
            }

            if (IsDuplicateTabCaption())
            {
                MultiValidatorDefault.AddError(string.Format("This UI mode already has a tab with the name {0}", DataSourceAsUITabEntity.Caption.ToString()));
            }

            this.Validate();

            try
            {
                if (base.Save())
                {
                    if (!this.DataSourceAsUITabEntity.UIModeEntity.CompanyId.HasValue)
                    {
                        CustomTextHelper.UpdateCustomTexts(this.DataSourceAsUITabEntity);
                    }
                    else
                    {
                        CustomTextHelper.UpdateCustomTexts(this.DataSourceAsUITabEntity, this.DataSourceAsUITabEntity.UIModeEntity.CompanyEntity);
                    }
                    
                    return true;
                }
            }
            catch (ObymobiException ex)
            {
                if (ex.ErrorEnumValue.Equals(UITabValidator.UITabValidatorResult.AutomaticSignOnUserRequiredForCmsLogin))
                {
                    this.MultiValidatorDefault.AddError(this.Translate("AutomaticSignOnUserRequiredForCmsLogin", "There is no user configured for automatic cms signon."));
                }
                else
                    throw;

                this.Validate();
            }
                
            return false;
        }

        private bool IsDuplicateTabCaption()
        {
            PredicateExpression filter = new PredicateExpression
            {
                UITabFields.Caption == DataSourceAsUITabEntity.Caption,
                UITabFields.UIModeId == DataSourceAsUITabEntity.UIModeId,
                UITabFields.UITabId != DataSourceAsUITabEntity.UITabId
            };

            return new UITabCollection().GetDbCount(filter) > 0;
        }

        public override bool SaveAndNew()
        {
            bool result = Save();
            if(result)
            {
                string url = this.Request.Path + "?mode=add";
                if (this.DataSourceAsUITabEntity.UIModeId > 0)                
                    url += string.Format("&UIModeId={0}", this.DataSourceAsUITabEntity.UIModeId);
                if (this.DataSourceAsUITabEntity.UIModeEntity != null)
                    url += string.Format("&UIModeType={0}", (int)this.DataSourceAsUITabEntity.UIModeEntity.Type);

                url += string.Format("&Type={0}", (int)this.DataSourceAsUITabEntity.Type); // Default on first page

                this.Response.Redirect(url, false);
            }
            return result;
        }
        
        private void BindUITabTypes(int uiModeType)
        {
            List<UITabType> types = UITabHelper.GetSupportedUITabTypes(uiModeType.ToEnum<UIModeType>());

            foreach (UITabType type in types)
            {
                this.cbType.Items.Add(new DevExpress.Web.ListEditItem(type.ToString(), (int)type));
            }
        }

        private void SetGui()
        {
            // Bind UITab types depending on the UIModeType parameter
            int uiModeType = 0;
            if (QueryStringHelper.HasValue("UIModeType"))
                uiModeType = QueryStringHelper.GetInt("UIModeType");
            else if (this.DataSourceAsUITabEntity != null && this.DataSourceAsUITabEntity.UIModeEntity != null)
                uiModeType = (int)this.DataSourceAsUITabEntity.UIModeEntity.Type;

            int type = -1;
            if (QueryStringHelper.HasValue("Type"))
                Int32.TryParse(QueryStringHelper.GetValue("Type"), out type);

            int uiModeId = -1;
            if (QueryStringHelper.HasValue("UIModeId"))
                uiModeId = QueryStringHelper.GetInt("UIModeId");
            else if (this.DataSourceAsUITabEntity != null && this.DataSourceAsUITabEntity.UIModeId > 0)
                uiModeId = (int)this.DataSourceAsUITabEntity.UIModeId;

            if (this.DataSourceAsUITabEntity.IsNew && type > 0)
                this.DataSourceAsUITabEntity.Type = type;

            this.BindUITabTypes(uiModeType);
            if (!this.IsPostBack)
            {
                this.cbType.Value = this.DataSourceAsUITabEntity.Type;
            }
 
            if (this.DataSourceAsUITabEntity.IsStandardConsoleTab)
            {
                this.DataSourceAsUITabEntity.Caption = string.Empty;
                this.tbCaption.Enabled = false;
                this.tbCaption.IsRequired = false;
            }
            else if (this.DataSourceAsUITabEntity.TypeAsEnum == UITabType.Category)
            {
                this.tbCaption.IsRequired = false;
            }
            
            if (this.ShowCategoryOptions(this.DataSourceAsUITabEntity))
            {
                this.plhCategory.Visible = true;
                // Set categories
                ResultsetFields fields = new ResultsetFields(2);
                fields.DefineField(CategoryFields.CategoryId, 0, "CategoryId");
                fields.DefineField(CategoryFields.Name, 1, "Name");

                fields[1].ExpressionToApply = new DbFunctionCall("{0} + ': ' + {1}", new object[] 
                                          { 
                                              MenuFields.Name, CategoryFields.Name 
                                          });

                RelationCollection relations = new RelationCollection();
                relations.Add(CategoryEntityBase.Relations.MenuEntityUsingMenuId, JoinHint.Inner);

                PredicateExpression filter = new PredicateExpression();
                filter.Add(CategoryFields.ParentCategoryId == DBNull.Value);
                filter.Add(CategoryFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

                if (uiModeId > -1)
                {
                    var usedCategoryFilter = new PredicateExpression(UITabFields.UIModeId == uiModeId);
                    usedCategoryFilter.Add(UITabFields.Type == (int)UITabType.Category);
                    usedCategoryFilter.Add(UITabFields.CategoryId != DBNull.Value);

                    if (!this.DataSourceAsUITabEntity.IsNew && this.DataSourceAsUITabEntity.CategoryId.HasValue)
                        usedCategoryFilter.Add(UITabFields.CategoryId != this.DataSourceAsUITabEntity.CategoryId.Value);

                    var categoryTabs = new Obymobi.Data.CollectionClasses.UITabCollection();
                    categoryTabs.GetMulti(usedCategoryFilter);

                    var usedCategoryIds = categoryTabs.Select(ct => ct.CategoryId).ToList();
                    filter.Add(CategoryFields.CategoryId != usedCategoryIds);
                } 

                SortExpression sort = new SortExpression();
                sort.Add(new SortClause(fields[1], SortOperator.Ascending));

                DataTable categoryTable = new DataTable();
                TypedListDAO dao = new TypedListDAO();
                dao.GetMultiAsDataTable(fields, categoryTable, 0, sort, filter, relations, false, null, null, 0, 0);

                this.ddlCategoryId.DataSource = categoryTable;
                this.ddlCategoryId.DataBind();
            }
            else
                this.DataSourceAsUITabEntity.CategoryId = null;

            if (this.ShowEntertainmentOptions(this.DataSourceAsUITabEntity))
            {
                this.plhEntertainment.Visible = true;
                // Set entertainments
                var filter = new PredicateExpression();

                PredicateExpression companyFilter = new PredicateExpression();
                companyFilter.Add(EntertainmentFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
                companyFilter.AddWithOr(EntertainmentFields.CompanyId == DBNull.Value);
                filter.Add(companyFilter);

                PredicateExpression typeFilter = new PredicateExpression();
                typeFilter.Add(EntertainmentFields.EntertainmentType == EntertainmentType.Web);
                typeFilter.AddWithOr(EntertainmentFields.EntertainmentType == EntertainmentType.Browser);
                typeFilter.AddWithOr(EntertainmentFields.EntertainmentType == EntertainmentType.RestrictedBrowser);
                typeFilter.AddWithOr(EntertainmentFields.EntertainmentType == EntertainmentType.Android);
                typeFilter.AddWithOr(EntertainmentFields.EntertainmentType == EntertainmentType.Survey);
                typeFilter.AddWithOr(EntertainmentFields.EntertainmentType == EntertainmentType.CustomBrowser);
                typeFilter.AddWithOr(EntertainmentFields.EntertainmentType == EntertainmentType.IpadBrowser);
                filter.Add(typeFilter);

                EntertainmentCollection entertainment = new EntertainmentCollection();

                if (this.DataSourceAsUITabEntity.Type == (int)UITabType.Cms)
                    entertainment = EntertainmentHelper.GetEntertainment(CmsSessionHelper.CurrentCompanyId, EntertainmentType.Cms);
                else
                    entertainment.GetMulti(filter);

                this.ddlEntertainmentId.DataSource = entertainment;
                this.ddlEntertainmentId.DataBind();
            }
            else            
                this.DataSourceAsUITabEntity.EntertainmentId = null;

            if (this.ShowUrlOptions(this.DataSourceAsUITabEntity))
                this.plhUrl.Visible = true;
            else
                this.DataSourceAsUITabEntity.URL = string.Empty;
            
            this.plhZoom.Visible = this.ShowZoomOptions(this.DataSourceAsUITabEntity);
            this.plhRestrictedAccess.Visible = this.ShowRestrictedAccess(this.DataSourceAsUITabEntity);

            if (this.ShowSiteOptions(this.DataSourceAsUITabEntity))
            {
                SiteCollection sites = new SiteCollection();
                PredicateExpression filter = new PredicateExpression();
                if (this.PageMode == PageMode.Add)
                    filter.Add(SiteFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
                else
                    filter.Add(SiteFields.CompanyId == this.DataSourceAsUITabEntity.UIModeEntity.CompanyId);

                if (CmsSessionHelper.CurrentRole >= Role.Administrator)
                    filter.AddWithOr(SiteFields.CompanyId == DBNull.Value);

                SortExpression sort = new SortExpression();
                sort.Add(SiteFields.Name | SortOperator.Ascending);
                sites.GetMulti(filter, 0, sort);

                this.ddlSiteId.DataSource = sites;
                this.ddlSiteId.DataBind();
            }
            else
                this.DataSourceAsUITabEntity.SiteId = null;

            if (this.ShowMapOptions(this.DataSourceAsUITabEntity))
            {
                this.ddlMapId.DataSource = MapHelper.GetMaps(CmsSessionHelper.CurrentCompanyId);
                this.ddlMapId.DataBind();
                this.plhMap.Visible = true;
            }
            else
            {
                this.plhMap.Visible = false;
            }

            this.plhSite.Visible = this.ShowSiteOptions(this.DataSourceAsUITabEntity);
            this.plhAllCategoryVisible.Visible = this.ShowMoreOptions(this.DataSourceAsUITabEntity);
            this.plhMessage.Visible = this.ShowMessageOptions(this.DataSourceAsUITabEntity);            

            if (this.DataSourceAsUITabEntity.IsDirty && !this.DataSourceAsUITabEntity.IsNew)
                this.DataSourceAsUITabEntity.Save();            
        }

        private bool ShowSiteOptions(UITabEntity uiTab)
        {
            if (uiTab.TypeAsEnum == UITabType.Site)
                    return true;

            return false;
        }

        private bool ShowZoomOptions(UITabEntity uiTab)
        {
            if (uiTab.TypeAsEnum == UITabType.Web)
                return true;

            return false;
        }

        private bool ShowRestrictedAccess(UITabEntity uiTab)
        {
            if (uiTab.TypeAsEnum == UITabType.Web)
                return true;

            return false; 
        }

        private bool ShowUrlOptions(UITabEntity uiTab)
        {
            if (uiTab.TypeAsEnum == UITabType.Web)
                return true;

            return false;
        }

        private bool ShowEntertainmentOptions(UITabEntity uiTab)
        {
            if (uiTab.TypeAsEnum == UITabType.Entertainment || uiTab.TypeAsEnum == UITabType.Cms)
                return true;

            return false;
        }

        private bool ShowCategoryOptions(UITabEntity uiTab)
        {
            if (uiTab.TypeAsEnum == UITabType.Category)
                return true;

            return false;
        }

        private bool ShowMoreOptions(UITabEntity uiTab)
        {
            if (uiTab.TypeAsEnum == UITabType.More)
                return true;

            return false;
        }

	    private bool ShowRoomControlOptions(UITabEntity uiTab)
	    {
	        return uiTab.TypeAsEnum == UITabType.RoomControl;
	    }

        private bool ShowMessageOptions(UITabEntity uiTab)
        {
            return uiTab.TypeAsEnum == UITabType.Message;
        }

        private bool ShowMapOptions(UITabEntity uiTab)
        {
            return uiTab.TypeAsEnum == UITabType.ExploreMap;
        }

	    #endregion

		#region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            this.DataSourceLoaded += new Dionysos.Delegates.Web.DataSourceLoadedHandler(UITab_DataSourceLoaded);
            base.OnInit(e);
        }

        void UITab_DataSourceLoaded(object sender)
        {
            // Set default to visible
            if (this.DataSourceAsUITabEntity.IsNew)
                this.DataSourceAsUITabEntity.Visible = true;

            this.SetGui();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if ((!QueryStringHelper.HasValue("UIModeId") && !QueryStringHelper.HasValue("UIModeType")) && !QueryStringHelper.HasValue("id"))
            {
                throw new Exception("Page must be called with a valid UIModeId and UIModeType or with a UITabId (=id)");
            }
        }

		#endregion

		#region Properties

		/// <summary>
		/// Return the page's datasource as a UITabEntity
		/// </summary>
		public UITabEntity DataSourceAsUITabEntity
		{
			get
			{
				return this.DataSource as UITabEntity;
			}
		}

		#endregion
	}
}
