﻿using System;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.ObymobiCms.Company
{
    public partial class RoomControlConfigurations : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        #region Event handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            LLBLGenProDataSource datasource = this.DataSource as LLBLGenProDataSource;
            if (datasource != null)
            {
                PredicateExpression filter = new PredicateExpression(RoomControlConfigurationFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
                datasource.FilterToUse = filter;                
            }
        }

        public void CreateDemoConfig()
        {
            RoomControlDemoManager.CreateRoomControlConfigurationDemo(CmsSessionHelper.CurrentCompanyId);
            this.Response.Redirect(this.Request.Url.ToString());
        }

        #endregion
    }
}
