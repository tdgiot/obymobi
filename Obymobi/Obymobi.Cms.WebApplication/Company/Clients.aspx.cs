﻿using Dionysos;
using Google.Apis.Manual.Util;
using Obymobi.Data;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Drawing;
using System.Web.UI.WebControls;

namespace Obymobi.ObymobiCms.Company
{
    public partial class Clients : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        private readonly IncludeFieldsList deviceIncludeFieldsList = new IncludeFieldsList
        {
            DeviceFields.PrivateIpAddresses,
            DeviceFields.PublicIpAddress,
            DeviceFields.ApplicationVersion,
            DeviceFields.AgentVersion,
            DeviceFields.SupportToolsVersion,
            DeviceFields.OsVersion,
            DeviceFields.LastRequestUTC,
            DeviceFields.DeviceModel,
            DeviceFields.Type,
            DeviceFields.Identifier
        };

        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            MasterPages.MasterPageEntityCollection masterPage = this.Master as MasterPages.MasterPageEntityCollection;
            if (masterPage != null)
            {
                masterPage.ToolBar.AddButton.Visible = false;
            }

            this.PrefetchPath = new PrefetchPath(EntityType.ClientEntity);
            this.PrefetchPath.Add(ClientEntity.PrefetchPathDeviceEntity, deviceIncludeFieldsList);
            this.PrefetchPath.Add(ClientEntity.PrefetchPathDeliverypointgroupEntity, new IncludeFieldsList(DeliverypointgroupFields.Name));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            LLBLGenProDataSource datasource = this.DataSource as LLBLGenProDataSource;
            if (datasource != null)
            {
                PredicateExpression filter = new PredicateExpression(ClientFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
                datasource.FilterToUse = filter;
            }

            this.MainGridView.HtmlRowPrepared += MainGridView_HtmlRowPrepared;
            this.MainGridView.HtmlDataCellPrepared += MainGridView_HtmlDataCellPrepared;
            this.MainGridView.Styles.Cell.HorizontalAlign = HorizontalAlign.Left;
            this.MainGridView.DataBound += MainGridView_DataBound;
        }

        private void MainGridView_DataBound(object sender, EventArgs e)
        {
            if (this.MainGridView.Columns["Notes"] != null) this.MainGridView.Columns["Notes"].Visible = false;
        }

        private void MainGridView_HtmlRowPrepared(object sender, DevExpress.Web.ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType != DevExpress.Web.GridViewRowType.Data) return;

            int clientId = Convert.ToInt32(e.KeyValue);
            if (clientId > 0)
            {
                PrefetchPath prefetch = new PrefetchPath(EntityType.ClientEntity);
                prefetch.Add(ClientEntityBase.PrefetchPathDeviceEntity, deviceIncludeFieldsList);

                ClientEntity client = new ClientEntity();
                client.FetchUsingPK(clientId, prefetch, null, new IncludeFieldsList(ClientFields.LastStatus));

                if (!client.IsNew)
                {
                    DeviceEntity device = client.DeviceEntity;
                    if (device != null)
                    {
                        this.SetCellValue(e.Row, "LastPrivateIpAddresses", device.PrivateIpAddresses);
                        this.SetCellValue(e.Row, "LastPublicIpAddress", device.PublicIpAddress);
                        this.SetCellValue(e.Row, "LastApplicationVersion", device.ApplicationVersion);
                        this.SetCellValue(e.Row, "LastAgentVersion", device.AgentVersion);
                        this.SetCellValue(e.Row, "LastSupportToolsVersion", device.SupportToolsVersion);
                        this.SetCellValue(e.Row, "LastOsVersion", device.OsVersion);
                        this.SetCellValue(e.Row, "LastRequestUTC", device.LastRequestUTC.HasValue ? device.LastRequestUTC.Value.UtcToLocalTime().ToString("g") : string.Empty);
                    }

                    if (client.IsOnline)
                    {
                        // Online & ok status
                        e.Row.ForeColor = Color.Green;
                    }
                    else if (client.IsOnline)
                    {
                        // Online
                        e.Row.ForeColor = Color.Orange;
                    }
                    else
                    {
                        // Offline
                        e.Row.ForeColor = Color.Red;
                    }
                }
            }
        }

        private void SetCellValue(TableRow row, string fieldName, string value)
        {
            if (this.MainGridView.VisibleColumns[fieldName] != null)
            {
                int index = MainGridView.VisibleColumns.IndexOf(MainGridView.VisibleColumns[fieldName]);

                if (index < row.Cells.Count)
                {
                    row.Cells[index].Text = value;
                }
            }
        }

        private void MainGridView_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.DataColumn.FieldName.Equals("ClientId") && !e.DataColumn.EditFormCaption.Equals("&nbsp;"))
            {
                e.Cell.Text = Convert.ToInt32(e.CellValue).ToString();
            }
            else if (e.DataColumn.FieldName.Equals("DeviceEntity.Identifier"))
            {
                if (!((string)e.CellValue).IsNotNullOrEmpty())
                {
                    e.Cell.Text = "Not Linked";
                }
            }
            else if (e.DataColumn.FieldName.Equals("Loaded"))
            {
                e.Cell.HorizontalAlign = HorizontalAlign.Center;
            }
        }

        #endregion
    }
}
