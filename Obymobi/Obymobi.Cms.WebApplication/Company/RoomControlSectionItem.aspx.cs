﻿using System;
using System.Drawing;
using System.Globalization;
using System.Linq;
using DevExpress.Web;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Enums;
using Dionysos.Web;
using Dionysos.Data.LLBLGen;
using Google.Apis.Util;
using Obymobi.Data;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.ObymobiCms.Company
{
	public partial class RoomControlSectionItem : Dionysos.Web.UI.PageLLBLGenEntity
	{
		#region Methods

        private void LoadUserControls()
        {
            this.tabsMain.AddTabPage("Afbeeldingen", "Media", "~/Generic/UserControls/MediaCollection.ascx");

            // Add languages
            this.cbService.DataBindEnum<DoorStationState>();

            RoomControlSectionItemEntity roomControlSectionItemEntity = new RoomControlSectionItemEntity(this.EntityId);
            if (roomControlSectionItemEntity.IsNew)
                return;

            this.tabsMain.AddTabPage("Translations", "CustomTextCollection", "~/Generic/UserControls/CustomTextCollection.ascx", "Generic");            
        }

        public override bool Save()
        {
            // Set the UITabId if specified in querystring
            if (this.DataSourceAsRoomControlSectionItemEntity.IsNew)
            {
                int roomControlSectionId;
                if (QueryStringHelper.TryGetValue("RoomControlSectionId", out roomControlSectionId))
                    this.DataSourceAsRoomControlSectionItemEntity.RoomControlSectionId = roomControlSectionId;
            }

            if (this.DataSourceAsRoomControlSectionItemEntity.Type == RoomControlSectionItemType.Scene)
            {
                this.DataSourceAsRoomControlSectionItemEntity.FieldValue1 = this.tbScene.Text;
                this.DataSourceAsRoomControlSectionItemEntity.FieldValue2 = this.tbSceneSuccessMessage.Text;
            }
            else if (this.DataSourceAsRoomControlSectionItemEntity.Type == RoomControlSectionItemType.SleepTimer)
            {
                this.DataSourceAsRoomControlSectionItemEntity.FieldValue1 = this.tbTurnOffAllScene.Text;
            }
            else if (this.DataSourceAsRoomControlSectionItemEntity.Type == RoomControlSectionItemType.RemoteControl)
            {
                this.SaveRemoteControlValues();
            }            

            this.Validate();

            if (base.Save())
            {
                CustomTextHelper.UpdateCustomTexts(this.DataSource, new CompanyEntity(this.DataSourceAsRoomControlSectionItemEntity.RoomControlSectionEntity.RoomControlAreaEntity.RoomControlConfigurationEntity.CompanyId));
                return true;
            }
            else
            {
                return false;
            }
        }

        public override bool SaveAndNew()
        {
            bool result = Save();
            if(result)
            {
                string url = this.Request.Path + "?mode=add";
                if (this.DataSourceAsRoomControlSectionItemEntity.RoomControlSectionId > 0)                
                    url += string.Format("&RoomControlSectionId={0}", this.DataSourceAsRoomControlSectionItemEntity.RoomControlSectionId);

                url += string.Format("&Type={0}", (int)this.DataSourceAsRoomControlSectionItemEntity.Type); // Default on first page

                this.Response.Redirect(url, false);
            }
            return result;
        }             

        private void BindUITabTypes()
        {
            foreach (RoomControlSectionItemType type in Enum.GetValues(typeof(RoomControlSectionItemType)))
            {
                this.cbType.Items.Add(new DevExpress.Web.ListEditItem(type.ToString(), (int)type));
            }       
        }

        private void SetGui()
        {
            if (this.DataSourceAsRoomControlSectionItemEntity.IsNew)
                this.DataSourceAsRoomControlSectionItemEntity.Visible = true;

            // Bind UITab types depending on the UIModeType parameter
            int type = -1;
            if (QueryStringHelper.HasValue("Type"))
                Int32.TryParse(QueryStringHelper.GetValue("Type"), out type);

            int roomControlSectionId = -1;
            if (QueryStringHelper.HasValue("RoomControlSectionId"))
                roomControlSectionId = QueryStringHelper.GetInt("RoomControlSectionId");
            else if (this.DataSourceAsRoomControlSectionItemEntity!= null && this.DataSourceAsRoomControlSectionItemEntity.RoomControlSectionId > 0)
                roomControlSectionId = (int)this.DataSourceAsRoomControlSectionItemEntity.RoomControlSectionId;

            if (this.DataSourceAsRoomControlSectionItemEntity.IsNew && type > 0)
                this.DataSourceAsRoomControlSectionItemEntity.Type = (RoomControlSectionItemType)type;

            this.BindUITabTypes();

            if (!this.IsPostBack)
            {
                this.cbType.Value = (int)this.DataSourceAsRoomControlSectionItemEntity.Type;
            }

            if (this.DataSourceAsRoomControlSectionItemEntity.IsDirty && !this.DataSourceAsRoomControlSectionItemEntity.IsNew)
                this.DataSourceAsRoomControlSectionItemEntity.Save();

            if (this.DataSourceAsRoomControlSectionItemEntity.Type == RoomControlSectionItemType.StationListing)
            {
                this.plhStationListing.Visible = true;
                this.ddlStationListId.IsRequired = true;

                var filter = new PredicateExpression();
                filter.Add(StationListFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

                var sort = new SortExpression();
                sort.Add(new SortClause(StationListFields.Name, SortOperator.Ascending));

                StationListCollection stationLists = new StationListCollection();
                stationLists.GetMulti(filter, 0, sort);

                this.ddlStationListId.DataSource = stationLists;
                this.ddlStationListId.DataBind();
            }
            else if (this.DataSourceAsRoomControlSectionItemEntity.Type == RoomControlSectionItemType.Scene)
            {
                this.plhScene.Visible = true;                

                this.tbScene.Text = this.DataSourceAsRoomControlSectionItemEntity.FieldValue1;
                this.tbSceneSuccessMessage.Text = this.DataSourceAsRoomControlSectionItemEntity.FieldValue2;
                this.tbScene.IsRequired = true;
            }
            else if (this.DataSourceAsRoomControlSectionItemEntity.Type == RoomControlSectionItemType.SleepTimer)
            {
                this.plhSleepTimer.Visible = true;

                this.tbTurnOffAllScene.Text = this.DataSourceAsRoomControlSectionItemEntity.FieldValue1;
                this.tbTurnOffAllScene.IsRequired = true;
            }
            else if (this.DataSourceAsRoomControlSectionItemEntity.Type == RoomControlSectionItemType.RemoteControl)
            {
                if (!this.DataSourceAsRoomControlSectionItemEntity.IsNew)
                {
                    this.plhRemoteControl.Visible = true;
                    this.SetRemoteControlValues();
                }                
            }
        }

        private void SetRemoteControlValues()
        {
            var powerButtonWidget = this.DataSourceAsRoomControlSectionItemEntity.RoomControlWidgetCollection.SingleOrDefault(w => w.Type == RoomControlWidgetType.PowerButton1x1);
            if (powerButtonWidget != null)
            {
                this.tbPowerButtonScene.Text = powerButtonWidget.FieldValue1;
            }

            var volumeWidget = this.DataSourceAsRoomControlSectionItemEntity.RoomControlWidgetCollection.SingleOrDefault(w => w.Type == RoomControlWidgetType.Volume1x4);
            if (volumeWidget != null)
            {
                this.tbVolumeUpScene.Text = volumeWidget.FieldValue1;
                this.tbVolumeDownScene.Text = volumeWidget.FieldValue2;
                this.tbVolumeMuteScene.Text = volumeWidget.FieldValue3;
            }

            var channelWidget = this.DataSourceAsRoomControlSectionItemEntity.RoomControlWidgetCollection.SingleOrDefault(w => w.Type == RoomControlWidgetType.Channel1x4);
            if (channelWidget != null)
            {
                this.tbChannelUpScene.Text = channelWidget.FieldValue1;
                this.tbChannelDownScene.Text = channelWidget.FieldValue2;
            }

            var navigationWidget = this.DataSourceAsRoomControlSectionItemEntity.RoomControlWidgetCollection.SingleOrDefault(w => w.Type == RoomControlWidgetType.Navigation2x3);
            if (navigationWidget != null)
            {
                this.tbLeftButtonScene.Text = navigationWidget.FieldValue1;
                this.tbTopButtonScene.Text = navigationWidget.FieldValue2;
                this.tbRightButtonScene.Text = navigationWidget.FieldValue3;
                this.tbBottomButtonScene.Text = navigationWidget.FieldValue4;
                this.tbSelectButtonScene.Text = navigationWidget.FieldValue5;
            }

            var numpadWidget = this.DataSourceAsRoomControlSectionItemEntity.RoomControlWidgetCollection.SingleOrDefault(w => w.Type == RoomControlWidgetType.Numpad2x3);
            if (numpadWidget != null)
            {
                this.tbOneButtonScene.Text = numpadWidget.FieldValue1;
                this.tbTwoButtonScene.Text = numpadWidget.FieldValue2;
                this.tbThreeButtonScene.Text = numpadWidget.FieldValue3;
                this.tbFourButtonScene.Text = numpadWidget.FieldValue4;
                this.tbFiveButtonScene.Text = numpadWidget.FieldValue5;
                this.tbSixButtonScene.Text = numpadWidget.FieldValue6;
                this.tbSevenButtonScene.Text = numpadWidget.FieldValue7;
                this.tbEightButtonScene.Text = numpadWidget.FieldValue8;
                this.tbNineButtonScene.Text = numpadWidget.FieldValue9;
                this.tbZeroButtonScene.Text = numpadWidget.FieldValue10;
            }
        }

        private void SaveRemoteControlValues()
        {
            var powerButtonWidget = this.DataSourceAsRoomControlSectionItemEntity.RoomControlWidgetCollection.SingleOrDefault(w => w.Type == RoomControlWidgetType.PowerButton1x1);
            if (powerButtonWidget != null)
            {
                powerButtonWidget.FieldValue1 = this.tbPowerButtonScene.Text;
                powerButtonWidget.Save();
            }

            var volumeWidget = this.DataSourceAsRoomControlSectionItemEntity.RoomControlWidgetCollection.SingleOrDefault(w => w.Type == RoomControlWidgetType.Volume1x4);
            if (volumeWidget != null)
            {
                volumeWidget.FieldValue1 = this.tbVolumeUpScene.Text;
                volumeWidget.FieldValue2 = this.tbVolumeDownScene.Text;
                volumeWidget.FieldValue3 = this.tbVolumeMuteScene.Text;
                volumeWidget.Save();
            }

            var channelWidget = this.DataSourceAsRoomControlSectionItemEntity.RoomControlWidgetCollection.SingleOrDefault(w => w.Type == RoomControlWidgetType.Channel1x4);
            if (channelWidget != null)
            {
                channelWidget.FieldValue1 = this.tbChannelUpScene.Text;
                channelWidget.FieldValue2 = this.tbChannelDownScene.Text;
                channelWidget.Save();
            }

            var navigationWidget = this.DataSourceAsRoomControlSectionItemEntity.RoomControlWidgetCollection.SingleOrDefault(w => w.Type == RoomControlWidgetType.Navigation2x3);
            if (navigationWidget != null)
            {
                navigationWidget.FieldValue1 = this.tbLeftButtonScene.Text;
                navigationWidget.FieldValue2 = this.tbTopButtonScene.Text;
                navigationWidget.FieldValue3 = this.tbRightButtonScene.Text;
                navigationWidget.FieldValue4 = this.tbBottomButtonScene.Text;
                navigationWidget.FieldValue5 = this.tbSelectButtonScene.Text;
                navigationWidget.Save();
            }

            var numpadWidget = this.DataSourceAsRoomControlSectionItemEntity.RoomControlWidgetCollection.SingleOrDefault(w => w.Type == RoomControlWidgetType.Numpad2x3);
            if (numpadWidget != null)
            {
                numpadWidget.FieldValue1 = this.tbOneButtonScene.Text;
                numpadWidget.FieldValue2 = this.tbTwoButtonScene.Text;
                numpadWidget.FieldValue3 = this.tbThreeButtonScene.Text;
                numpadWidget.FieldValue4 = this.tbFourButtonScene.Text;
                numpadWidget.FieldValue5 = this.tbFiveButtonScene.Text;
                numpadWidget.FieldValue6 = this.tbSixButtonScene.Text;
                numpadWidget.FieldValue7 = this.tbSevenButtonScene.Text;
                numpadWidget.FieldValue8 = this.tbEightButtonScene.Text;
                numpadWidget.FieldValue9 = this.tbNineButtonScene.Text;
                numpadWidget.FieldValue10 = this.tbZeroButtonScene.Text;
                numpadWidget.Save();
            }
        }

		#endregion

		#region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            this.DataSourceLoaded += RoomControlSection_DataSourceLoaded;
            base.OnInit(e);
        }

        private void RoomControlSection_DataSourceLoaded(object sender)
        {
            this.SetGui();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

		#endregion

		#region Properties

		public RoomControlSectionItemEntity DataSourceAsRoomControlSectionItemEntity
		{
			get
			{
                return this.DataSource as RoomControlSectionItemEntity;
			}
		}

		#endregion
	}
}
