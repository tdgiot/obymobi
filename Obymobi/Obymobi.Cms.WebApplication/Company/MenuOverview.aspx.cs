﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dionysos.Web;
using Dionysos.Web.UI;
using System.Text;
using Dionysos;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data;
using System.Collections;
using DevExpress.Web.ASPxTreeList;
using System.Drawing;
using Obymobi.Enums;

namespace Obymobi.ObymobiCms.Company
{
    public partial class MenuOverview : Dionysos.Web.UI.PageQueryStringDataBinding
    {
        #region Fields
        
        int nodeIndex = 1;
        bool isOtoucho = false;

        CompanyEntity company;

        String validIcon = "~/Images/Icons/valid.png";
        String subErrorIcon = "~/Images/Icons/suberror.png";
        String errorIcon = "~/Images/Icons/error.png";
        String informationIcon = "~/Images/Icons/information.png";
        String missingIcon = "~/Images/Icons/false.png";

        int selectionType = 0;

        #endregion

        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if (CmsSessionHelper.CurrentCompanyId != 0)
            {
                PrefetchPath prefetch = new PrefetchPath(EntityType.CompanyEntity);
                prefetch.Add(CompanyEntity.PrefetchPathCategoryCollection);

                company = new CompanyEntity(CmsSessionHelper.CurrentCompanyId, prefetch);

                // Check what system is used for this company
                if (company.SystemType == Enums.SystemType.Otoucho)
                    isOtoucho = true;

                this.CreateColumns();
                this.CreateNodes();
            }
        }

        protected void tlMenuOverview_HtmlDataCellPrepared(object sender, TreeListHtmlDataCellEventArgs e)
        {
            if (e.Column.FieldName == "Settings")
            {
                if (e.NodeKey.Contains("-"))
                {
                    string[] values = e.NodeKey.ToString().Split('-');
                    var status = values[1];
                    var missing = values[2];
                    e.Cell.Attributes.Add("onMouseOver", string.Format("OnNodeHover(this, '{0}', '{1}');", status, missing));
                    e.Cell.Attributes.Add("onMouseOut", "OnCancelNodeHover();");
                }
            }
        }

        protected override void SetDefaultValuesToControls()
        {
            
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            btFilter.TranslatableProperties.Clear();
            btCollapse.TranslatableProperties.Clear();
        }

        public void Filter()
        {
            if (CmsSessionHelper.CurrentCompanyId != 0)
            {
                company = new CompanyEntity(CmsSessionHelper.CurrentCompanyId);

                // Clear nodes
                tlMenuOverview.ClearNodes();

                // Clear columns
                tlMenuOverview.Columns.Clear();

                // Set the selection type for this filter
                this.selectionType = rblSelectionType.SelectedIndex;

                // Create the columns
                this.CreateColumns();

                // Create the nodes
                this.CreateNodes();
            }
        }

        protected void tlMenuOverview_HtmlRowPrepared(object sender, TreeListHtmlRowEventArgs e)
        {
            if (e.GetValue("Status") != null)
            {
                String status = e.GetValue("Status").ToString();
                if (status == errorIcon)
                    e.Row.ForeColor = Color.Red;
                else if (status == subErrorIcon)
                    e.Row.ForeColor = Color.Orange;
                else if (status == validIcon)
                    e.Row.ForeColor = Color.Green;
                else if (status == informationIcon)
                    e.Row.ForeColor = Color.Blue;
            }
        }

        #endregion

        #region Methods

        public void CollapseCategories()
        {
            if (CmsSessionHelper.CurrentCompanyId != 0)
            {
                bool isExpanded;

                // Collapse every node that qualifies as a category
                if (!SessionHelper.TryGetValue("expanded", out isExpanded) || !isExpanded)
                {
                    btCollapse.Text = this.Translate("lblExpandCategories", "Categoriën uitklappen");
                    // "Expand categories"
                }
                else
                {
                    btCollapse.Text = this.Translate("lblCollapseCategories", "Categoriën inklappen");
                    //"Collapse categories"
                }

                SessionHelper.SetValue("expanded", !isExpanded); // Flip boolean
            }
        }

        private void CreateColumns()
        {
            int visibleIndex = 0;

            // Hyperlink column
            TreeListHyperLinkColumn categoryColumn = new TreeListHyperLinkColumn();
            categoryColumn.CellStyle.HorizontalAlign = HorizontalAlign.Left;
            categoryColumn.VisibleIndex = visibleIndex;
            categoryColumn.FieldName = "Settings";
            categoryColumn.Caption = this.Translate("clmnSettings", "Instellingen");
            categoryColumn.PropertiesHyperLink.TextField = "Name";
            categoryColumn.PropertiesHyperLink.Target = "_blank";
            categoryColumn.PropertiesHyperLink.NavigateUrlFormatString = "{0}";
            tlMenuOverview.Columns.Add(categoryColumn);

            visibleIndex++;

            // Description
            TreeListImageColumn descriptionColumn = new TreeListImageColumn();
            descriptionColumn.VisibleIndex = visibleIndex;
            descriptionColumn.Width = 60;
            descriptionColumn.FieldName = "Description";
            descriptionColumn.Caption = this.Translate("clmnDescription", "Beschrijving");
            descriptionColumn.PropertiesImage.AlternateText = "description";
            descriptionColumn.PropertiesImage.ImageUrlFormatString = "{0}";
            tlMenuOverview.Columns.Add(descriptionColumn);

            visibleIndex++;

            if (this.isOtoucho)
            {
                // Text-color column
                TreeListImageColumn txtColorColumn = new TreeListImageColumn();
                txtColorColumn.VisibleIndex = visibleIndex;
                txtColorColumn.Width = 60;
                txtColorColumn.FieldName = "Text-color";
                txtColorColumn.Caption = this.Translate("clmnTextColor", "Tekst kleur");
                txtColorColumn.PropertiesImage.AlternateText = "text-color";
                txtColorColumn.PropertiesImage.ImageUrlFormatString = "{0}";
                tlMenuOverview.Columns.Add(txtColorColumn);

                visibleIndex++;

                // Background-color column
                TreeListImageColumn bgColorColumn = new TreeListImageColumn();
                bgColorColumn.VisibleIndex = visibleIndex;
                bgColorColumn.Width = 60;
                bgColorColumn.FieldName = "Background-color";
                bgColorColumn.Caption = this.Translate("clmnBackgroundColor", "Achtergrond kleur");
                bgColorColumn.PropertiesImage.AlternateText = "background-color";
                bgColorColumn.PropertiesImage.ImageUrlFormatString = "{0}";
                tlMenuOverview.Columns.Add(bgColorColumn);

                visibleIndex++;
            }

            // Button-image column
            TreeListImageColumn buttonImageColumn = new TreeListImageColumn();
            buttonImageColumn.VisibleIndex = visibleIndex;
            buttonImageColumn.Width = 60;
            buttonImageColumn.FieldName = "Button-image";
            buttonImageColumn.PropertiesImage.AlternateText = "buttonimage";
            buttonImageColumn.PropertiesImage.ImageUrlFormatString = "{0}";
            tlMenuOverview.Columns.Add(buttonImageColumn);

            visibleIndex++;

            // Branding-image column
            TreeListImageColumn brandingImageColumn = new TreeListImageColumn();
            brandingImageColumn.VisibleIndex = visibleIndex;
            brandingImageColumn.Width = 60;
            brandingImageColumn.FieldName = "Branding-image";
            brandingImageColumn.PropertiesImage.AlternateText = "brandingimage";
            brandingImageColumn.PropertiesImage.ImageUrlFormatString = "{0}";
            tlMenuOverview.Columns.Add(brandingImageColumn);

            visibleIndex++;

            // Contains products
            TreeListImageColumn containsProductsColumn = new TreeListImageColumn();
            containsProductsColumn.VisibleIndex = visibleIndex;
            containsProductsColumn.Width = 60;
            containsProductsColumn.FieldName = "Contains-products";
            containsProductsColumn.Caption = this.Translate("clmnContainsProducts", "Producten");
            containsProductsColumn.PropertiesImage.AlternateText = "containsproducts";
            containsProductsColumn.PropertiesImage.ImageUrlFormatString = "{0}";
            tlMenuOverview.Columns.Add(containsProductsColumn);

            visibleIndex++;

            // Category button picture
            TreeListImageColumn categoryButtonColumn = new TreeListImageColumn();
            categoryButtonColumn.VisibleIndex = visibleIndex;
            categoryButtonColumn.Width = 60;
            categoryButtonColumn.FieldName = "Category-button";
            categoryButtonColumn.PropertiesImage.AlternateText = "categorybutton";
            categoryButtonColumn.PropertiesImage.ImageUrlFormatString = "{0}";
            tlMenuOverview.Columns.Add(categoryButtonColumn);

            visibleIndex++;

            // Schedule column
            TreeListImageColumn scheduleColumn = new TreeListImageColumn();
            scheduleColumn.VisibleIndex = visibleIndex;
            scheduleColumn.Width = 60;
            scheduleColumn.FieldName = "Schedule";
            scheduleColumn.Caption = this.Translate("clmnSchedule", "Schema");
            scheduleColumn.PropertiesImage.AlternateText = "schedule";
            scheduleColumn.PropertiesImage.ImageUrlFormatString = "{0}";
            tlMenuOverview.Columns.Add(scheduleColumn);

            visibleIndex++;

            // Alteration column
            TreeListImageColumn alterationColumn = new TreeListImageColumn();
            alterationColumn.VisibleIndex = visibleIndex;
            alterationColumn.Width = 60;
            alterationColumn.FieldName = "Alteration";
            alterationColumn.Caption = this.Translate("clmnAlteration", "Productsamenstelling");
            alterationColumn.PropertiesImage.AlternateText = "alteration";
            alterationColumn.PropertiesImage.ImageUrlFormatString = "{0}";
            tlMenuOverview.Columns.Add(alterationColumn);

            visibleIndex++;

            // Suggestion column
            TreeListImageColumn suggestionColumn = new TreeListImageColumn();
            suggestionColumn.VisibleIndex = visibleIndex;
            suggestionColumn.Width = 60;
            suggestionColumn.FieldName = "Suggestion";
            suggestionColumn.Caption = this.Translate("clmnSuggestion", "Suggestie");
            suggestionColumn.PropertiesImage.AlternateText = "suggestion";
            suggestionColumn.PropertiesImage.ImageUrlFormatString = "{0}";
            tlMenuOverview.Columns.Add(suggestionColumn);

            visibleIndex++;

            // Status column
            TreeListImageColumn statusColumn = new TreeListImageColumn();
            statusColumn.VisibleIndex = visibleIndex;
            statusColumn.Width = 60;
            statusColumn.FieldName = "Status";
            statusColumn.PropertiesImage.AlternateText = "image";
            statusColumn.PropertiesImage.ImageUrlFormatString = "{0}";
            tlMenuOverview.Columns.Add(statusColumn);
        }

        private void CreateNodes()
        {
            // Create company nodes
            TreeListNode parentNode = this.CreateCompanyNodes();

            // Service request nodes
            if (cbServiceRequests.Checked)
                this.CreateServiceRequestNodes(parentNode);

            // Payment method nodes
            if (cbPaymentmethods.Checked)
                this.CreatePaymentmethodNodes(parentNode);

            // Advertisements
            if (cbAdvertisements.Checked)
                this.CreateAdvertisements(parentNode);

            // Survey
            if (cbSurveys.Checked)
                this.CreateSurveyNodes(parentNode);

            // Product types
            if (cbProductType.Checked)
                this.CreateProductTypeNodes(parentNode);

            bool rootCategoriesValid = true;

            if (cbMenu.Checked)
                rootCategoriesValid = this.CreateMenuNodes(parentNode);

            // Set company node
            if (!company.IsComplete)
                this.SetNode(parentNode, company.Name, "Company.aspx?id=" + company.CompanyId, NodeStatus.Error);
            else
            {
                if (rootCategoriesValid)
                    this.SetNode(parentNode, company.Name, "Company.aspx?id=" + company.CompanyId, NodeStatus.Valid);
                else
                    this.SetNode(parentNode, company.Name, "Company.aspx?id=" + company.CompanyId, NodeStatus.SubError);
            }
        }

        private TreeListNode CreateCompanyNodes()
        {
            TreeListNode node = this.CreateNode(null);

            // Check what kind of system is used for this company
            if (!this.isOtoucho)
                this.SetNode(this.CreateNode(node), "The system used for this company is: Crave", "Company.aspx?id=" + company.CompanyId, NodeStatus.Information);
            else
                this.SetNode(this.CreateNode(node), "The system used for this company is: Otoucho", "Company.aspx?id=" + company.CompanyId, NodeStatus.Information);

            // Eye-catcher
            if (!company.HasImage)
                this.SetNode(this.CreateNode(node), "This company does not have a eye-catcher yet", "Company.aspx?id=" + company.CompanyId, NodeStatus.Error);

            // 4 frontpage products
            if (this.isOtoucho && cbProductsFrontpage.Checked)
            {
                if (!company.HasFrontpageProducts)
                    this.SetNode(this.CreateNode(node), "This company does not have 4 products on the frontpage", "../Catalog/Products.aspx", NodeStatus.Error);
                else
                {
                    TreeListNode fpNode = this.CreateNode(node);
                    this.SetNode(fpNode, "The following products are on the frontpage of this company:", "../Catalog/Products.aspx", NodeStatus.Valid);

                    foreach (ProductEntity productEntity in company.FrontpageProducts)
                    {
                        this.SetNode(this.CreateNode(fpNode), productEntity.Name, string.Format("../Catalog/Product.aspx?id={0}", productEntity.ProductId), NodeStatus.Valid);
                    }
                }
            }

            // Entertainment
            if (cbEntertainment.Checked)
            {
                if (!company.HasEntertainment)
                {
                    this.SetNode(this.CreateNode(node), "This company does not have any entertainment on the frontpage", "Company.aspx?id=" + company.CompanyId, NodeStatus.Error);
                }
                else
                {
                    TreeListNode entertainmentNode = this.CreateNode(node);
                    this.SetNode(entertainmentNode, "The following entertainments are selected for this company:", "Company.aspx?id=" + company.CompanyId, NodeStatus.Valid);

                    foreach (EntertainmentEntity entertainmentEntity in company.Entertainment)
                    {
                        this.SetNode(this.CreateNode(entertainmentNode), entertainmentEntity.Name, "Company.aspx?id=" + company.CompanyId, NodeStatus.Valid);
                    }
                }
            }

            return node;
        }

        private bool CreateMenuNodes(TreeListNode parentNode)
        {
            bool rootCategoriesValid = true;

            TreeListNode parentMenuNode = this.CreateNode(parentNode);

            // Menu nodes
            foreach (MenuEntity menu in company.MenuCollection)
            {
                TreeListNode menuNode = this.CreateNode(parentMenuNode);    

                CategoryCollection rootCategories = menu.CategoryCollection;

                rootCategories.Sort((int)CategoryFieldIndex.SortOrder, System.ComponentModel.ListSortDirection.Ascending);

                foreach (CategoryEntity categoryEntity in rootCategories)
                {
                    if (categoryEntity.ParentCategoryId == null)
                    {
                        if (categoryEntity.IsComplete)
                            this.SetNode(menuNode, menu.Name, string.Format("../Catalog/Menu.aspx?id={0}", menu.MenuId), NodeStatus.Valid);
                        else
                        {
                            this.SetNode(menuNode, menu.Name, string.Format("../Catalog/Menu.aspx?id={0}", menu.MenuId), NodeStatus.SubError);
                            rootCategoriesValid = false;
                        }
                        this.CreateCategoryNode(categoryEntity, menuNode);
                    }
                }
            }

            if (rootCategoriesValid)
                this.SetNode(parentMenuNode, "Menus", "../Catalog/Menus.aspx", NodeStatus.Valid);
            else
                this.SetNode(parentMenuNode, "Menus", "../Catalog/Menus.aspx", NodeStatus.SubError);

            return rootCategoriesValid;
        }

        private void CreateServiceRequestNodes(TreeListNode parentNode)
        {
            if (company.HasServiceRequests)
            {
                TreeListNode node = this.CreateNode(parentNode);
                this.SetNode(node, "Service requests", "../Catalog/ServiceItems.aspx", NodeStatus.Valid);

                foreach (ProductEntity productEntity in company.ServiceRequests)
                {
                    this.SetNode(this.CreateNode(node), productEntity.Name, string.Format("../Catalog/Product.aspx?id={0}", productEntity.ProductId), NodeStatus.Valid);
                }
            }
            else
            {
                this.SetNode(this.CreateNode(parentNode), "This company does not have any service requests yet", "../Catalog/ServiceItems.aspx", NodeStatus.Error);
            }
        }

        private void CreatePaymentmethodNodes(TreeListNode parentNode)
        {
            if (company.HasPaymentmethods)
            {
                TreeListNode node = this.CreateNode(parentNode);
                this.SetNode(node, "Payment methods", "../Catalog/PaymentMethods.aspx", NodeStatus.Valid);

                foreach (ProductEntity productEntity in company.Paymentmethods)
                {
                    this.SetNode(this.CreateNode(node), productEntity.Name, string.Format("../Catalog/Product.aspx?id={0}", productEntity.ProductId), NodeStatus.Valid);
                }
            }
            else
            {
                this.SetNode(this.CreateNode(parentNode), "This company does not have any payment methods yet", "../Catalog/PaymentMethods.aspx", NodeStatus.Error);
            }
        }

        private void CreateAdvertisements(TreeListNode parentNode)
        {
            if (company.HasAdvertisements)
            {
                TreeListNode node = this.CreateNode(parentNode);
                this.SetNode(node, "Advertisements", "../Advertising/Advertisements.aspx", NodeStatus.Valid);

                foreach (AdvertisementEntity advertisementEntity in company.AdvertisementCollection)
                {
                    this.SetNode(this.CreateNode(node), advertisementEntity.Name, string.Format("../Advertising/Advertisement.aspx?id={0}", advertisementEntity.AdvertisementId), NodeStatus.Valid);
                }
            }
            else
            {
                this.SetNode(this.CreateNode(parentNode), "This company does not have any advertisement yet", "../Advertising/Advertisements.aspx", NodeStatus.Error);
            }
        }

        private void CreateSurveyNodes(TreeListNode parentNode)
        {
            if (company.HasSurveys)
            {
                TreeListNode node = this.CreateNode(parentNode);
                this.SetNode(node, "Surveys", "../Company/Surveys.aspx", NodeStatus.Valid);

                foreach (SurveyEntity surveyEntity in company.SurveyCollection)
                {
                    TreeListNode surveyNode = this.CreateNode(node);
                    this.SetNode(surveyNode, surveyEntity.Name, string.Format("../Company/Survey.aspx?id={0}", surveyEntity.SurveyId), NodeStatus.Valid);

                    foreach (SurveyPageEntity surveyPageEntity in surveyEntity.SurveyPageCollection)
                    {
                        TreeListNode surveyPageNode = this.CreateNode(surveyNode);
                        this.SetNode(surveyPageNode, surveyPageEntity.Name, string.Format("../Company/SurveyPage.aspx?id={0}", surveyPageEntity.SurveyPageId), NodeStatus.Valid);

                        foreach (SurveyQuestionEntity surveyQuestionEntity in surveyPageEntity.SurveyQuestionCollection)
                        {
                            TreeListNode surveyQuestionNode = this.CreateNode(surveyPageNode);
                            this.SetNode(surveyQuestionNode, surveyQuestionEntity.Question, string.Format("../Company/SurveyQuestion.aspx?id={0}", surveyQuestionEntity.SurveyQuestionId), NodeStatus.Valid);

                            foreach (SurveyAnswerEntity surveyAnswerEntity in surveyQuestionEntity.SurveyAnswerCollection)
                            {
                                TreeListNode surveyAnswerNode = this.CreateNode(surveyQuestionNode);
                                this.SetNode(surveyAnswerNode, surveyAnswerEntity.Answer, string.Format("../Company/SurveyAnswer.aspx?id={0}", surveyAnswerEntity.SurveyAnswerId), NodeStatus.Valid);
                            }
                        }
                    }
                }
            }
            else
            {
                this.SetNode(this.CreateNode(parentNode), "This company does not have any survey yet", "../Company/Surveys.aspx", NodeStatus.Error);
            }
        }

        private void CreateProductTypeNodes(TreeListNode parentNode)
        {
            // Sort the products by name
            ProductCollection sortedProducts = company.ProductCollection;
            sortedProducts.Sort((int)ProductFieldIndex.Name, System.ComponentModel.ListSortDirection.Ascending);
            
            // Get all the products of a company
            EntityView<ProductEntity> allProducts = company.ProductCollection.DefaultView;
            
            if (allProducts.Count > 0)
            {
                TreeListNode node = this.CreateNode(parentNode);
                this.SetNode(node, "Product types", "../Catalog/Products.aspx", NodeStatus.Valid);

                string[] names = Enum.GetNames(typeof(ProductType));
                Array values = Enum.GetValues(typeof(ProductType));

                // Iteratie over the product types
                for (int i = 0; i < names.Length; i++)
                {
                    string name = names[i];
                    int value = Convert.ToInt32(values.GetValue(i));
                    string urlName = "Company.aspx?id=" + company.CompanyId;

                    if (value == (int)ProductType.Product)
                        urlName = "../Catalog/Products.aspx";
                    else if (value == (int)ProductType.Service)
                        urlName = "../Catalog/ServiceItems.aspx";
                    else if (value == (int)ProductType.Paymentmethod)
                        urlName = "../Catalog/PaymentMethods.aspx";
                    else if (value == (int)ProductType.PosServiceMessage)
                        urlName = "../Catalog/POSMessages.aspx";

                    TreeListNode productTypeNode = this.CreateNode(node);
                    this.SetNode(productTypeNode, name, urlName, NodeStatus.Valid);

                    // Get all the products for this product type
                    allProducts.Filter = new PredicateExpression(ProductFields.Type == value);

                    // iterate over the products for this product type
                    foreach (ProductEntity product in allProducts)
                    {
                        TreeListNode productNode = this.CreateNode(productTypeNode);
                        this.SetNode(productNode, product.Name, string.Format("../Catalog/Product.aspx?id={0}", product.ProductId), NodeStatus.Valid);
                    }
                }
            }
            else
            {
                this.SetNode(this.CreateNode(parentNode), "This company does not have any products yet", "../Catalog/Product.aspx", NodeStatus.Error);
            }
        }

        private void CreateCategoryNode(CategoryEntity category, TreeListNode parentNode)
        {
            // Create category node
            TreeListNode node = this.CreateNode(parentNode);
            bool isExpanded;
            if (!SessionHelper.TryGetValue("expanded", out isExpanded))
            {
                SessionHelper.SetValue("expanded", true);
                isExpanded = true;
            }
            node.Expanded = isExpanded;

            if (category.IsComplete)
            {
                this.SetCategoryNode(node, category.Name, string.Format("../Catalog/Category.aspx?id={0}", category.CategoryId), NodeStatus.Valid, category);
            }
            else
            {
                if (category.ChildCategoryCollection.Count == 0 && !category.HasProducts || !category.HasCategoryButton && category.ParentCategoryId != null)
                {
                    // Category doesn't have any child categories and products or the category doesn't have a category button and is not a root category 
                    this.SetCategoryNode(node, category.Name, string.Format("../Catalog/Category.aspx?id={0}", category.CategoryId), NodeStatus.Error, category);
                }
                else
                {
                    this.SetCategoryNode(node, category.Name, string.Format("../Catalog/Category.aspx?id={0}", category.CategoryId), NodeStatus.SubError, category);
                }
            }

            if (category.ChildCategoryCollection.Count > 0)
            {
                // Category has child categories

                CategoryCollection orderedCategories = category.ChildCategoryCollection;

                orderedCategories.Sort((int)CategoryFieldIndex.SortOrder, System.ComponentModel.ListSortDirection.Ascending);

                foreach (CategoryEntity categoryEntity in orderedCategories)
                {
                    if (selectionType == (int)Selection.None)
                        this.CreateCategoryNode(categoryEntity, node);
                    else if (selectionType == (int)Selection.Complete)
                    {
                        if (categoryEntity.HasOneCompleteCategoryOrProduct)
                            this.CreateCategoryNode(categoryEntity, node);
                    }
                    else if (selectionType == (int)Selection.Incomplete)
                    {
                        if (!categoryEntity.IsComplete)
                            this.CreateCategoryNode(categoryEntity, node);
                    }
                }
            }
            else if (category.ProductCollectionViaProductCategory.Count > 0)
            {
                // Category has products

                ProductCollection orderedProducts = category.ProductCollectionViaProductCategory;

                orderedProducts.Sort((int)ProductFieldIndex.SortOrder, System.ComponentModel.ListSortDirection.Ascending);

                foreach (ProductEntity productEntity in orderedProducts)
                {
                    if (productEntity.Visible)
                    {
                        if (selectionType == (int)Selection.None)
                            this.CreateProductNode(productEntity, node);
                        else if (selectionType == (int)Selection.Complete)
                        {
                            if (productEntity.IsComplete)
                                this.CreateProductNode(productEntity, node);
                        }
                        else if (selectionType == (int)Selection.Incomplete)
                        {
                            if (!productEntity.IsComplete)
                                this.CreateProductNode(productEntity, node);
                        }
                    }
                }
            }
        }

        private void CreateProductNode(ProductEntity product, TreeListNode parentNode)
        {
            // Create product node
            PosproductEntity posproductEntity = product.PosproductEntity;
            //object key = nodeIndex + "-" + posproductEntity.Name + " (" + posproductEntity.ExternalId + ")-" + product.PriceIn;
            object key;
            if (product.IsComplete)
                key = nodeIndex + "-" + "Complete" + "-";
            else
            {
                var missing = "";

                if (this.isOtoucho)
                {
                    if (!product.HasDescription)
                        missing += "Description:";
                    if (!product.HasProductBranding)
                        missing += "Branding image:";
                    if (!product.HasProductButton)
                        missing += "Button image:";
                    if (!product.HasTextColor)
                        missing += "Text color:";
                    if (!product.HasBackgroundColor)
                        missing += "Background color:";
                }
                else
                {
                    if (!product.HasDescription)
                        missing += "Description:";
                    if (!product.HasProductBrandingCrave)
                        missing += "Branding image:";
                    if (!product.HasProductButtonCrave)
                        missing += "Button image:";
                }

                key = nodeIndex + "-" + "Incomplete" + "-" + missing;
            }
            
            TreeListNode node = tlMenuOverview.AppendNode(key, parentNode);

            if (!product.IsComplete)
                this.SetProductNode(node, product.Name, string.Format("../Catalog/Product.aspx?id={0}", product.ProductId), NodeStatus.Error, product);
            else
                this.SetProductNode(node, product.Name, string.Format("../Catalog/Product.aspx?id={0}", product.ProductId), NodeStatus.Valid, product);

            nodeIndex++;
        }

        private TreeListNode CreateNode(TreeListNode parentNode)
        {
            TreeListNode node = tlMenuOverview.AppendNode(nodeIndex, parentNode);
            node.Expanded = true;
            nodeIndex++;
            return node;
        }

        private void SetNode(TreeListNode node, string nodeText, string nodeLink, NodeStatus nodeStatus)
        {
            node["Name"] = nodeText;
            node["Settings"] = nodeLink;

            switch (nodeStatus)
            {
                case NodeStatus.Valid:
                    // No error
                    node["Status"] = validIcon;
                    break;
                case NodeStatus.SubError:
                    // Error in sub category and/or sub products
                    node["Status"] = subErrorIcon;
                    break;
                case NodeStatus.Error:
                    // Error in category and/or product
                    node["Status"] = errorIcon;
                    break;
                case NodeStatus.Information:
                    // Information node
                    node["Status"] = informationIcon;
                    break;
                default:
                    break;
            }
        }

        private void SetCategoryNode(TreeListNode node, string nodeText, string nodeLink, NodeStatus nodeStatus, CategoryEntity category)
        {
            this.SetNode(node, nodeText, nodeLink, nodeStatus);

            node["Suggestion"] = missingIcon;
            node["Schedule"] = missingIcon;
            node["Alteration"] = missingIcon;
            node["Category-button"] = missingIcon;
            node["Contains-products"] = missingIcon;

            // Check for child categories or child products
            if (category.ChildCategoryCollection.Count > 0 || category.HasProducts)
                node["Contains-products"] = validIcon;

            // Category button
            if (category.HasCategoryButton)
                node["Category-button"] = validIcon;
            else
            {
                if (category.ParentCategoryId == null)
                {
                    node["Category-button"] = validIcon;
                }
            }

            // Alteration
            if (category.HasAlteration)
                node["Alteration"] = validIcon;

            // Suggestion
            if (category.HasSuggestions)
                node["Suggestion"] = validIcon;

            // Schedule
            if (category.HasSchedule)
                node["Schedule"] = validIcon;
        }

        private void SetProductNode(TreeListNode node, string nodeText, string nodeLink, NodeStatus nodeStatus, ProductEntity product)
        {
            this.SetNode(node, nodeText, nodeLink, nodeStatus);

            node["Suggestion"] = missingIcon;
            node["Alteration"] = missingIcon;
            node["Schedule"] = missingIcon;
            node["Description"] = missingIcon;
            node["Button-image"] = missingIcon;
            node["Branding-image"] = missingIcon;
            node["Text-color"] = missingIcon;
            node["Background-color"] = missingIcon;

            // Description
            if (product.HasDescription)
                node["Description"] = validIcon;

            if (this.isOtoucho)
            {
                // Text-color
                if (product.HasTextColor)
                    node["Text-color"] = validIcon;

                // Background-color
                if (product.HasBackgroundColor)
                    node["Background-color"] = validIcon;

                // Button image
                if (product.HasProductButton)
                    node["Button-image"] = validIcon;

                // Branding image
                if (product.HasProductBranding)
                    node["Branding-image"] = validIcon;
            }
            else
            {
                // Button image
                if (product.HasProductButtonCrave)
                    node["Button-image"] = validIcon;

                // Branding image
                if (product.HasProductBrandingCrave)
                    node["Branding-image"] = validIcon;
            }

            // Schedule
            if (product.HasSchedule)
                node["Schedule"] = validIcon;

            // Alteration
            if (product.HasAlterations)
                node["Alteration"] = validIcon;

            // Suggestion
            if (product.HasSuggestions)
                node["Suggestion"] = validIcon;
        }

        #endregion
    }

    #region Properties

    enum NodeStatus : int
    {
        Valid = 0,
        SubError = 1,
        Error = 2,
        Information = 3
    }

    enum Selection : int
    {
        None = 0,
        Complete = 1,
        Incomplete = 2
    }

    #endregion 
    
}