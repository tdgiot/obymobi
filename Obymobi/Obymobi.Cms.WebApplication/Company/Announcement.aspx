﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.Announcement" Codebehind="Announcement.aspx.cs" %>
<%@ Reference VirtualPath="~/Generic/UserControls/CustomTextCollection.ascx" %>

<%@ Register Assembly="DevExpress.Web.v20.1" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" runat="Server">
     <script type="text/javascript">
         function SelectImage(id, value) {
             document.getElementById('<%= tbImageId.ClientID %>').value = id;
             document.getElementById('<%= tbImageName.ClientID %>').value = value;
             popup.Hide();
         }
         function ClearImage(id, value) {
             document.getElementById('<%= tbImageId.ClientID %>').value = 0;
             document.getElementById('<%= tbImageName.ClientID %>').value = "";
             popup.Hide();
         }
    </script>
    <div>
        <dx:ASPxPopupControl ClientInstanceName="popup" PopupHorizontalAlign="WindowCenter"
            PopupVerticalAlign="WindowCenter" ID="pcImage" runat="server" ShowHeader="True"
            Modal="True" CssPostfix="1" EnableAnimation="false" AutoUpdatePosition="true" ShowShadow="false"
            HeaderText="Click on an image to select it">
            <ContentCollection>
                <dx:PopupControlContentControl runat="server">
                    <div style="overflow:auto;height:450px;width:530px">
                   <dx:ASPxDataView ID="dvGallery" runat="server" Layout="Flow" AllowPaging="false" CssPostfix="1" ShowVerticalScrollBar="true">
                        <ItemStyle Height="170px" Width="150px" />
                        <ItemTemplate>
                            <div align="center">
                                <table>
                                    <tr> 
                                        <td class="thumbnailImageCell" onclick="SelectImage(<%# Eval("MediaId") %>,'<%# Eval("Name") %>')">
                                            <D:Image ID="imgPhoto" ImageUrl='<%# Eval("ImageUrl") %>' AlternateText='<%# Eval("Name") %>' Height='150px' Width='150px' runat="server"/>
                                        </td>
                                    </tr>
                                </table>
                                <asp:Label Text='<%# Eval("Name") %>' runat="server" ID="lbName" Width='150px' />
                            </div>
                        </ItemTemplate>
                    </dx:ASPxDataView>
                    </div>
                </dx:PopupControlContentControl>
            </ContentCollection>
            <ContentStyle VerticalAlign="Middle" HorizontalAlign="Center" />
            <ModalBackgroundStyle BackColor="Black" />
        </dx:ASPxPopupControl>
        <X:PageControl ID="tabsMain" runat="server" Width="100%">
            <TabPages>
                <X:TabPage Text="Algemeen" Name="Generic">
                    <controls>
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblTitle">Titel</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbTitle" runat="server" IsRequired="true"></D:TextBoxString>
							</td>
                        </tr>                        
                        <tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblText">Tekst</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxMultiLine runat="server" ID="tbText" MaxLength="1000" Rows="4" IsRequired="true"></D:TextBoxMultiLine>
							</td>
					    </tr>
                        <tr>
							<td class="label">
								<D:Label runat="server" id="lblImage">Afbeelding</D:Label>
							</td>
							<td class="control" >
								<D:TextBox ID="tbImageName" runat="server" ReadOnly="true"></D:TextBox>
                                
							</td>
							<td class="control">
                                <D:Button ID="btSelectImage" runat="server" Text="Afbeelding selecteren..." UseSubmitBehavior="false" OnClientClick="popup.Show(); return false;" />
                                <D:Button ID="btClearImage" runat="server" Text="Clear" UseSubmitBehavior="false" OnClientClick="ClearImage(); return false;" />
							</td>
							<td class="control" >
								<D:TextBox ID="tbImageId" runat="server" style="visibility:hidden;"></D:TextBox>
							</td>
                        </tr>
                        <tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblDuration">Tijdsduur minuten</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxInt ID="tbDuration" runat="server" AllowNegative="false"></D:TextBoxInt>
							</td>
							<td class="label">
							</td>
							<td class="control">
							</td>
                        </tr>
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblRecurring">Herhalen</D:LabelEntityFieldInfo>
							</td>
							<td class="control" >
								<D:CheckBox ID="cbRecurring" runat="server"></D:CheckBox>
                                <br />
                                <D:Label ID="lblRecurComment" runat="server"><small>Herhalingsopties zullen zichtbaar worden zodra 'Herhaling' is geselecteerd en de aankondiging opgeslagen is.</small></D:Label>
							</td>
							<td class="label">
							</td>
							<td class="control" >
							</td>
                        </tr>
                    </table>
                    <D:PlaceHolder ID="plhNonRecurring" runat="server">
                        <table class="dataformV2">
                            <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblTimeToShow">Tonen om</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <X:TimeEdit runat="server" ID="teTimeToShow"></X:TimeEdit>
							    </td>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblDateToShow">Datum</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <X:DateEdit runat="server" ID="deDateToShow"></X:DateEdit>
							    </td>
                            </tr>
                        </table>
                    </D:PlaceHolder>
                    <D:PlaceHolder ID="plhRecurring" runat="server">
                        <table class="dataformV2">
                            <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblRecurringPeriod">Terugkeer periode</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
                                    <X:ComboBoxEnum ID="ddlRecurringPeriod" runat="server" Type="Obymobi.Enums.AnnouncementRecurringPeriod, Obymobi"></X:ComboBoxEnum> 
							    </td>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblRecurringMinutes">Terugkeer minuten</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <D:TextBoxInt ID="tbRecurringMinutes" runat="server" AllowNegative="false"></D:TextBoxInt>
							    </td>
                            </tr>
                            <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblRecurringBeginDate">Begin terugkeerperiode</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <X:DateEdit CssClass="teRecurring" runat="server" ID="deRecurringBeginDate"></X:DateEdit><X:TimeEdit CssClass="teRecurring" runat="server" ID="teRecurringBeginTime"></X:TimeEdit>
							    </td>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblRecurringEndDate">Einde terugkeerperiode</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <X:DateEdit CssClass="teRecurring" runat="server" ID="deRecurringEndDate"></X:DateEdit><X:TimeEdit CssClass="teRecurring" runat="server" ID="teRecurringEndTime"></X:TimeEdit>
							    </td>
                            </tr>
                        </table>
                    </D:PlaceHolder>
                </controls>
                </X:TabPage>
                <X:TabPage Text="Yes actie" Name="Action">
                    <Controls>
                        <table class="dataformV2">
					        <tr>
						        <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblOnYesProduct">Product</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlOnYesProduct" IncrementalFilteringMode="StartsWith" EntityName="Product" TextField="Name" ValueField="ProductId" PreventEntityCollectionInitialization="true" />
                                </td>       
						        <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblOnYesCategory">Category</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlOnYesCategory" IncrementalFilteringMode="StartsWith" EntityName="Category" TextField="Name" ValueField="CategoryId" PreventEntityCollectionInitialization="true" />
                                </td>       
					        </tr>
                            <tr>
						        <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblOnYesEntertainment">Entertainment</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlOnYesEntertainment" IncrementalFilteringMode="StartsWith" EntityName="Entertainment" TextField="Name" ValueField="EntertainmentId" PreventEntityCollectionInitialization="true" />
                                </td>
                            </tr>
                        </table>
                    </Controls>
                </X:TabPage>
            </TabPages>
        </X:PageControl>
    </div>
</asp:Content>
