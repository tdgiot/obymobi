﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Dionysos.Web.UI;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Cms.Logic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Web.UI.DevExControls;
using Obymobi.Logic.HelperClasses;
using Dionysos.Web.UI.WebControls;
using Obymobi.Logic;

namespace Obymobi.ObymobiCms.Company
{
	public partial class Schedule : Dionysos.Web.UI.PageLLBLGenEntity
    {
        #region Properties

        public ScheduleEntity DataSourceAsScheduleEntity
        {
            get
            {
                return this.DataSource as ScheduleEntity;
            }
        }

        #endregion

        #region Methods

        protected override void OnInit(EventArgs e)
		{
            Dionysos.Web.UI.MasterPage.LinkedCssFiles.Add("~/css/catalog.css", 1000);
            this.LoadUserControls();
			base.OnInit(e);
            this.DataSourceLoaded += Schedule_DataSourceLoaded;
		}

        private ScheduleitemCollection GetScheduleitems()
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ScheduleitemFields.ScheduleId == this.EntityId);

            SortExpression sort = new SortExpression();
            sort.Add(ScheduleitemFields.DayOfWeek | SortOperator.Ascending);

            ScheduleitemCollection scheduleitems = new ScheduleitemCollection();
            scheduleitems.GetMulti(filter, 0, sort);

            return scheduleitems;
        }
				

		public override bool Save()
        {
            // Set the company id
            this.DataSourceAsScheduleEntity.CompanyId = CmsSessionHelper.CurrentCompanyId;

            var scheduleitemsView = this.GetScheduleitems().DefaultView;

            // 7 + 1 (All) days.
            for (int iDay = 0; iDay < 8; iDay++)
            {
                for (int iInterval = 1; iInterval < 4; iInterval++)
                {
                    string controlNameBegin = string.Empty;
                    string controlNameEnd = string.Empty;
                    PredicateExpression filter = new PredicateExpression();
                    if (iDay <= 6)
                    {
                        controlNameBegin = string.Format("teDay{0}Begin{1}", iDay, iInterval);
                        controlNameEnd = string.Format("teDay{0}End{1}", iDay, iInterval);
                        filter.Add(ScheduleitemFields.DayOfWeek == (DayOfWeek)iDay);
                    }
                    else
                    {
                        controlNameBegin = string.Format("teDayAllBegin{0}", iInterval);
                        controlNameEnd = string.Format("teDayAllEnd{0}", iInterval);
                        filter.Add(ScheduleitemFields.DayOfWeek == DBNull.Value);
                    }

                    filter.Add(ScheduleitemFields.SortOrder == iInterval);

                    scheduleitemsView.Filter = filter;

                    // Find Scheduleitem in View
                    ScheduleitemEntity scheduleitem;
                    if (scheduleitemsView.Count > 1)
                        throw new NotImplementedException();
                    else if (scheduleitemsView.Count == 1)
                        scheduleitem = scheduleitemsView[0];
                    else
                        scheduleitem = null;

                    // Get the controls
                    TimePicker2 teBegin = this.FindControlRecursively(controlNameBegin) as TimePicker2;
                    TimePicker2 teEnd = this.FindControlRecursively(controlNameEnd) as TimePicker2;

                    // Sync the years
                    if (teBegin.Value.HasValue && teEnd.Value.HasValue)
                    {
                        if (teBegin.Value.Value.Year > teEnd.Value.Value.Year)
                        {
                            teEnd.Value = teEnd.Value.Value.AddYears(teBegin.Value.Value.Year - teEnd.Value.Value.Year);
                        }
                        else if (teEnd.Value.Value.Year > teBegin.Value.Value.Year)
                        {
                            teBegin.Value = teBegin.Value.Value.AddYears(teEnd.Value.Value.Year - teBegin.Value.Value.Year);
                        }
                    }

                    if (teBegin.Value.HasValue && teEnd.Value.HasValue && (teBegin.Value != teEnd.Value) && (teBegin.Value < teEnd.Value))
                    {
                        // Got a value
                        if (scheduleitem == null)
                        {
                            scheduleitem = new ScheduleitemEntity();
                            scheduleitem.DayOfWeek = iDay <= 6 ? (DayOfWeek?)iDay : null;
                            scheduleitem.SortOrder = iInterval;
                        }

                        scheduleitem.TimeStartFromDateTime(teBegin.Value.Value);
                        scheduleitem.TimeEndFromDateTime(teEnd.Value.Value);

                        if (this.DataSourceAsScheduleEntity.IsNew)
                            this.DataSourceAsScheduleEntity.Save();

                        scheduleitem.ScheduleId = this.DataSourceAsScheduleEntity.ScheduleId;
                        scheduleitem.Save();
                    }
                    else
                    {
                        // No value
                        if (scheduleitem != null)
                        {
                            scheduleitemsView.RelatedCollection.Remove(scheduleitem);
                            scheduleitem.Delete();
                        }
                    }

                }
            }

            return base.Save();
        }

        private void LoadValues()
        {
            var scheduleitems = this.GetScheduleitems().DefaultView;
            var scheduleitemsView = this.GetScheduleitems().DefaultView;

            Dictionary<int, string> weekAvailability = new Dictionary<int, string>();
            for (int i = 0; i < scheduleitems.Count; i++)
            {
                var scheduleitem = scheduleitems[i];

                if (scheduleitem.SortOrder > 3 || scheduleitem.SortOrder < 1)
                    throw new NotImplementedException(string.Format("The CMS currently only supports SortOrder > 1 && < 4  per day, DayOfWeek '{0}' has different.", scheduleitem.DayOfWeek));

                // Get the controlname
                string controlNameBegin = string.Empty;
                string controlNameEnd = string.Empty;

                if (scheduleitem.DayOfWeek.HasValue)
                {
                    controlNameBegin = string.Format("teDay{0}Begin{1}", (int)scheduleitem.DayOfWeek, scheduleitem.SortOrder);
                    controlNameEnd = string.Format("teDay{0}End{1}", (int)scheduleitem.DayOfWeek, scheduleitem.SortOrder);
                }
                else
                {
                    controlNameBegin = string.Format("teDayAllBegin{0}", scheduleitem.SortOrder);
                    controlNameEnd = string.Format("teDayAllEnd{0}", scheduleitem.SortOrder);
                }

                // Find the controls
                TimePicker2 teBegin = this.FindControlRecursively(controlNameBegin) as TimePicker2;
                TimePicker2 teEnd = this.FindControlRecursively(controlNameEnd) as TimePicker2;

                teBegin.Value = scheduleitem.TimeStartAsDateTime();
                teEnd.Value = scheduleitem.TimeEndAsDateTime();

                // Get the day of the week, 999 for all days
                int day = 0;
                if (scheduleitem.DayOfWeek.HasValue)
                    day = (int)scheduleitem.DayOfWeek.Value;
                else
                    day = 999;

                string newTime = string.Format("from {0} to {1}", scheduleitem.TimeStartAsDateTime().ToString("t"), scheduleitem.TimeEndAsDateTime().ToString("t"));

                string dayTimes;
                if (weekAvailability.TryGetValue(day, out dayTimes))
                    weekAvailability[day] = dayTimes += ", " + newTime;
                else
                    weekAvailability.Add(day, newTime);
            }

            // Check if there's a schedule for "allDays"
            string defaultAvailability;
            bool allDaysSet = false;
            if (weekAvailability.TryGetValue(999, out defaultAvailability))
            {
                allDaysSet = true;
            }
            else
            {
                defaultAvailability = "Not available";
            }

            // Loop through weekdays, 0 = sunday, 6 = saterday, all days = 999
            for (int i = 0; i <= 6; i++)
            {
                string controlAvailable = string.Format("lblDay{0}Available", i);
                Label lblAvailable = this.FindControlRecursively(controlAvailable) as Label;

                if (lblAvailable != null)
                {
                    // Check if there's a schedule for this day
                    string dayAvailability;
                    if (weekAvailability.TryGetValue(i, out dayAvailability))
                    {
                        // We got a schedule
                        lblAvailable.Text = dayAvailability;
                    }
                    else
                    {
                        // Use the default availability
                        lblAvailable.Text = defaultAvailability;
                    }
                }
            }

            // Last but not least, set the availability for "allDays"
            string controlAllAvailable = string.Format("lblDayAllAvailable");
            Label lblAllAvailable = this.FindControlRecursively(controlAllAvailable) as Label;
            if (lblAllAvailable != null)
            {
                if (allDaysSet)
                    lblAllAvailable.Text = "";
                else
                    lblAllAvailable.Text = defaultAvailability;
            }

        }

        private void LoadUserControls()
        {
        }

		private void SetGui()
		{
		}

		#endregion

		#region Event Handlers

		protected void Page_Load(object sender, EventArgs e)
		{
            if (!IsPostBack)
                this.LoadValues();
		}

        private void Schedule_DataSourceLoaded(object sender)
        {
            
        }

		#endregion
    }
}
