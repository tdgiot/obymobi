﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.Orderitem" Title="Orderitem" Codebehind="Orderitem.aspx.cs" %>
<%@ Register Assembly="DevExpress.XtraCharts.v20.1.Web" Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>
<%@ Register Assembly="DevExpress.XtraCharts.v20.1" Namespace="DevExpress.XtraCharts" TagPrefix="dxcharts" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitleHolder" Runat="Server">
    <D:Label runat="server" id="lblTitle">Bestellingsitem</D:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhToolbarHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhContentHolder" Runat="Server">
    <div>
        <X:PageControl ID="tabsMain" runat="server" Width="100%">
            <TabPages>
                <X:TabPage Text="Algemeen" Name="Generic">
                    <controls>
					    <table class="dataformV2">
                            <tr>
                                <td class="label">
                                    <D:Label ID="lblOrderitemIdCaption" runat="server">Orderitem Id</D:Label>
                                </td>
                                <td class="control">
                                    <D:Label ID="lblOrderitemId" runat="server" LocalizeText="false" CssClass="lblOrderValue"></D:Label>
                                </td>
                                <td class="label">
                                    <D:Label ID="lblProductCaption" runat="server">Product</D:Label>
                                </td>
                                <td class="control">
                                    <D:Label ID="lblProduct" runat="server" LocalizeText="false" CssClass="lblOrderValue"></D:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:Label ID="lblQuantityCaption" runat="server">Aantal</D:Label>
                                </td>
                                <td class="control">
                                    <D:Label ID="lblQuantity" runat="server" LocalizeText="false" CssClass="lblOrderValue"></D:Label>
                                </td>
                                <td class="label">
                                    <D:Label ID="lblPriceCaption" runat="server">Prijs</D:Label>
                                </td>
                                <td class="control">
                                    <D:Label ID="lblPrice" runat="server" LocalizeText="false" CssClass="lblOrderValue"></D:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
		                            <D:Label ID="lblNotesCaption" runat="server">Opmerking</D:Label>
	                            </td>
		                        <td colspan="3" class="control">
			                        <D:TextBox ID="tbNotes" runat="server" Rows="5" TextMode="MultiLine" CssClass="inputNoHeight" LocalizeText="false" Enabled="false"></D:TextBox>                
		                        </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:Label ID="lblAlterationsCaption" runat="server">Productsamenstellingen</D:Label>
                                </td>
                                <td colspan="3" class="control">
                                    <D:PlaceHolder ID="PlaceHolder1" runat="server">
                                        <table class="dataformV2">
                                            <X:GridView ID="alterationsGrid" ClientInstanceName="alterationsGrid" runat="server" Width="100%">
                                                <SettingsBehavior AutoFilterRowInputDelay="350" />
                                                <SettingsPager PageSize="20"></SettingsPager>
                                                <SettingsBehavior AllowGroup="false" AllowDragDrop="false" />
                                                <Columns>
                                                    <dxwgv:GridViewDataColumn FieldName="AlterationId" VisibleIndex="0" Width="5%">
                                                    </dxwgv:GridViewDataColumn>
                                                    <dxwgv:GridViewDataColumn FieldName="Name" VisibleIndex="1" Width="5%">
                                                    </dxwgv:GridViewDataColumn>
                                                    <dxwgv:GridViewDataColumn FieldName="Option" VisibleIndex="2" Width="5%">
                                                    </dxwgv:GridViewDataColumn>
                                                    <dxwgv:GridViewDataColumn FieldName="Price" VisibleIndex="3" Width="5%">
                                                    </dxwgv:GridViewDataColumn>
                                                </Columns>
                                            </X:GridView>  
                                        </table>
                                    </D:PlaceHolder>
                                </td>
                            </tr>
                        </table>
				    </controls>
                </X:TabPage>
            </TabPages>
        </X:PageControl>
    </div>
</asp:Content>
