<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.Client" Codebehind="Client.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%" EnableViewState="True">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblDeviceId">Device</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<X:ComboBoxLLBLGenEntityCollection runat="server" ID="dllDeviceId" EntityName="Device" Enabled="false" PreventEntityCollectionInitialization="true"></X:ComboBoxLLBLGenEntityCollection>
                                <D:PlaceHolder ID="plhDeviceSettings" runat="server">
                                    <D:LinkButton runat="server" ID="btViewDevice" LocalizeText="true">View Device</D:LinkButton> - <D:LinkButton runat="server" ID="btnDeviceShell" LocalizeText="true">Device Shell</D:LinkButton>
									<D:PlaceHolder ID="plhRemoteControl" runat="server">
                                        - <D:LinkButton runat="server" ID="btRemoteControl">Remote Control</D:LinkButton>
                                    </D:PlaceHolder>
									<D:PlaceHolder ID="plhLogCat" runat="server">
										- <D:LinkButton runat="server" ID="btnLogCat">LogCat</D:LinkButton>
                                    </D:PlaceHolder>
                                </D:PlaceHolder>
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblCompanyId">Company</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlCompanyId" CssClass="input" UseDataBinding="true" IncrementalFilteringMode="StartsWith" EntityName="Company" TextField="Name" ValueField="CompanyId" IsRequired="true" PreventEntityCollectionInitialization="true" />
							</td>	
					    </tr>
                        <tr>
							<td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblLastOperationMode">Last Operation Mode</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
                                <D:Label runat="server" id="lblLastOperationModeValue" LocalizeText="false"></D:Label>
							</td>
							<td class="label">                                
                                <D:LabelEntityFieldInfo runat="server" id="lblIsTestClient">Preview mode</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
                                <D:CheckBox runat="server" ID="cbIsTestClient" />
							</td>
                        </tr>
                        <tr>
							<td class="label">
                                <D:Label runat="server" id="lblLastDeliverypointNumber">Last deliverypoint number</D:Label>
							</td>
							<td class="control">
                                <D:Label runat="server" id="lblLastDeliverypointNumberValue" LocalizeText="false"></D:Label>
							</td>
							<td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblLastCommunicationMethod">Last communication method</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
                                <D:Label runat="server" id="lblLastCommunicationMethodValue" LocalizeText="false"></D:Label>
							</td>
                        </tr>
						<tr>
							<td class="label">
								<D:Label runat="server" id="lblLastRequest">Last request</D:Label>
							</td>
							<td class="control">
								<D:Label runat="server" id="lblLastRequestValue" LocalizeText="false"></D:Label>
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblLastStatusText">Last status</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:Label runat="server" id="lblLastStatusValue" LocalizeText="false"></D:Label>
							</td>	
					    </tr>
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblLastApplicationVersion">Application Version</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:Label runat="server" id="lblLastApplicationVersionValue" LocalizeText="false"></D:Label>
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblLastOsVersion">OS Version</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:Label runat="server" id="lblLastOsVersionValue" LocalizeText="false"></D:Label>
							</td>
                        </tr>
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblLastSupportToolsVersion">SupportTools Version</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:Label runat="server" id="lblLastSupportToolsVersionValue" LocalizeText="false"></D:Label>
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblLastSupportToolsRequest">Last SupportTools Request</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:Label runat="server" id="lblLastSupportToolsRequestValue" LocalizeText="false"></D:Label>
							</td>
                        </tr>
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblLastAgentVersion">Android Agent Version</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:Label runat="server" id="lblLastAgentVersionValue" LocalizeText="false"></D:Label>
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblLastCloudEnvironment">Cloud Environment</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:Label runat="server" id="lblLastCloudEnvironmentValue" LocalizeText="false"></D:Label>
							</td>
                        </tr>
                    <tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblLastPrivateIpAddresses">IP Adress (private)</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:Label runat="server" id="lblLastPrivateIpAddressesValue" LocalizeText="false"></D:Label>
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblLastPublicIpAddress">IP Adress (public)</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:Label runat="server" id="lblLastPublicIpAddressValue" LocalizeText="false"></D:Label>
							</td>		
					    </tr>
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblLastBatteryLevel">Battery level</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:Label runat="server" id="lblLastBatteryLevelValue" LocalizeText="false"></D:Label>
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblLastIsCharging">Charging?</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:Label runat="server" id="lblIsChargingYes" LocalizeText="true">Yes</D:Label>
                                <D:Label runat="server" id="lblIsChargingNo" LocalizeText="true">No</D:Label>
							</td>
                        </tr>
                        <tr>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblLastWifiSsid">Last wifi ssid</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <D:Label runat="server" id="lblLastWifiSsidValue" LocalizeText="false"></D:Label>
                            </td>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblLastWifiStrength">Last wifi strength</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <D:Label runat="server" id="lblLastWifiStrengthValue" LocalizeText="false"></D:Label>
                            </td>
						</tr>
                        <tr>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblBluetoothKeyboardConnected">Bluetooth Keyboard</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <D:Label runat="server" id="lblBluetoothKeyboardConnectedValue" LocalizeText="false"></D:Label>
                            </td>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblLastApiVersion">API Version</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <D:Label runat="server" id="lblLastApiVersionValue" LocalizeText="false"></D:Label>
                            </td>
						</tr>
                        <tr>
                            <td class="label">
                                <D:Label runat="server" ID="lblDevelopmentModeLabel">Development mode</D:Label>
                            </td>
                            <td class="control">
                                <D:Label runat="server" ID="lblDevelopmentModeYes" LocalizeText="true" Visible="False">Yes</D:Label>
                                <D:Label runat="server" ID="lblDevelopmentModeNo" LocalizeText="true">No</D:Label>
                            </td>
                            <td class="label">
                                <D:Label runat="server" ID="lblDeviceModelLabel">Device model</D:Label>
                            </td>
                            <td class="control">
                                <D:Label runat="server" ID="lblDeviceModelValue" LocalizeText="false"></D:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                <D:Label runat="server" ID="lblLoadedSuccessfullyLabel">Loaded successfully</D:Label>
                            </td>
                            <td class="control">
                                <D:Label runat="server" id="lblLoadedSuccessfullyLabelYes" LocalizeText="false" Visible="false">Yes</D:Label>
                                <D:Label runat="server" id="lblLoadedSuccessfullyLabelNo" LocalizeText="false">No</D:Label>
                            </td>
                            <td class="label">
                                <D:Label runat="server" ID="lblDeviceTypeLabel">Device type</D:Label>
                            </td>
                            <td class="control">
                                <D:Label runat="server" ID="lblDeviceTypeValue" LocalizeText="false"></D:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                <D:Label runat="server" ID="lblRoomControlConnected">Room Control</D:Label>
                            </td>
                            <td class="control">
                                <D:Label runat="server" ID="lblRoomControlConnectedValue" LocalizeText="false"></D:Label>
                            </td>                                                       
                        </tr>
                        <tr>
                            <td class="label">
                                <D:Label runat="server" ID="lblDoNotDisturbActive">Do Not Disturb Active</D:Label>
                            </td>
                            <td class="control">
                                <D:Label runat="server" ID="lblDoNotDisturbActiveValue" LocalizeText="false"></D:Label>
                            </td>
                            <td class="label">
                                <D:Label runat="server" ID="lblServiceRoomActive">Service Room Active</D:Label>
                            </td>
                            <td class="control">
                                <D:Label runat="server" ID="lblServiceRoomActiveValue" LocalizeText="false"></D:Label>
                            </td>                                                       
                        </tr>
                        <tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblNotes">Notes</D:LabelEntityFieldInfo>
							</td>						
							<td colspan="3" class="control">
								<D:TextBox ID="tbNotes" runat="server" Rows="5" TextMode="MultiLine" CssClass="inputNoHeight"></D:TextBox>                
							</td>
						</tr>
					 </table>
				
                <D:Panel ID="pnlMessagingService" runat="server" GroupingText="Messaging Service Stats" Visible="False">
                    <table class="dataformV2">
                        <tr>
                            <td class="label">Messaging Service Version</td>
                            <td class="control"><D:Label runat="server" id="lblLastMessagingServiceVersionValue" LocalizeText="false"></D:Label></td>
                            <td class="label">IoT Connecting (last 24h)</td>
                            <td class="control"><D:Label runat="server" ID="lblMessagingConnectingCount24h" LocalizeText="false"></D:Label></td> 
                        </tr>
                        <tr>
                            <td class="label">Last Messaging Service Request</td>
                            <td class="control"><D:Label runat="server" id="lblLastMessagingServiceRequestValue" LocalizeText="false"></D:Label></td>
                            <td class="label">IoT Connected (last 24h)</td>
                            <td class="control"><D:Label runat="server" ID="lblMessagingConnectedCount24h" LocalizeText="false"></D:Label></td> 
                        </tr>
                        <tr>
                            <td class="label">Last connection type</td>
                            <td class="control"><D:Label runat="server" ID="lblLastMessagingConnectionType" LocalizeText="false"></D:Label></td>
                            <td class="label">IoT Reconnecting (last 24h)</td>
                            <td class="control"><D:Label runat="server" ID="lblMessagingReconnectingCount24h" LocalizeText="false"></D:Label></td> 
				        </tr>
                        <tr>
                            <td class="label">Last IoT connection status</td>
                            <td class="control"><D:Label runat="server" ID="lblLastMessagingConnectionStatus" LocalizeText="false"></D:Label></td>
                            <td class="label">IoT Connection Lost (last 24h)</td>
                            <td class="control"><D:Label runat="server" ID="lblMessagingConnectionLostCount24h" LocalizeText="false"></D:Label></td> 
                        </tr>
                        <tr>
                            <td class="label">Last successful IoT connection</td>
                            <td class="control"><D:Label runat="server" ID="lblLastMessagingSuccessfulConnection" LocalizeText="false"></D:Label></td>
                            <td class="label"></td>
                            <td class="control"></td>
                        </tr>
                    </table>
				</D:Panel>
				</Controls>
			</X:TabPage>	
            <X:TabPage Text="Technisch" Name="tabAdvanced">
                <controls>
					<table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:Label runat="server" id="lblDeliverypointgroupAndDeliverypoint">Deliverypointgroup + deliverypoint</D:Label>
                            </td>
                            <td class="control">
                                <X:ComboBoxString runat="server" ID="cbDeliverypointgroupAndDeliverypoint" CssClass="input" IncrementalFilteringMode="Contains" IncrementalFilteringDelay="1000"></X:ComboBoxString>
                            </td>
                            <td class ="control">
                                <D:Button runat="server" ID="btnChangeDeliverypointDeliverypointGroup" Text="Change Deliverypoint"/>
                            </td>
                        </tr>
						<tr>
							<td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblRoomControlAreaId">Default room control area</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlRoomControlAreaId" IncrementalFilteringMode="StartsWith" EntityName="RoomControlArea" TextField="Name" ValueField="RoomControlAreaId" PreventEntityCollectionInitialization="true" />
                            </td>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblOperationMode">Operation Mode</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <X:ComboBoxInt runat="server" ID="ddlOperationMode"></X:ComboBoxInt>
                            </td>
                            <td class="label">
								
							</td>
							<td class="control">
								
							</td>
					    </tr>				               	                        	    					    
					 </table>			
				</Controls>
			</X:TabPage>
            <X:TabPage Text="Geïnstalleerde applicaties" Name="InstalledApplications">
				<controls>
					<D:PlaceHolder ID="PlaceHolder4" runat="server">						
						<table class="dataformV2">
							<tr>
								<td class="label">
									<D:Label runat="server" ID="lblApplications">Applications</D:Label>
								</td>
								<td class="control">																																					 
									<D:CheckBoxListLLBLGenEntityCollection runat="server" ID="cblClientEntertainment" EntityName="Entertainment" LinkCollectionPropertyOnParentDataSource="ClientEntertainmentCollection" LinkEntityName="ClientEntertainmentEntity" Enabled="false" RepeatColumns="2" PreventEntityCollectionInitialization="true" />
								</td> 
								<td class="label">
								</td>
								<td class="control">
								</td>    			                              
							</tr>
						</table>						
					</D:PlaceHolder>
				</controls>
			</X:TabPage>
            <X:TabPage Text="Activiteit" Name="tabActivity">
				<controls>
					<D:PlaceHolder ID="PlaceHolder1" runat="server">						
						<table class="dataformV2">
							<tr>
								<td class="label">
									<D:Label runat="server" ID="lblTypesToShow">Visible</D:Label>
								</td>
								<td class="control">						
                                    <D:CheckBoxList runat="server" ID="cblTypesToShow">                                        
                                        <asp:ListItem notdirty="true" Value="ClientLog" Selected="true">Logs</asp:ListItem>
                                        <asp:ListItem  notdirty="true" Value="Netmessages" Selected="true">Netmessages</asp:ListItem>
                                        <asp:ListItem  notdirty="true" Value="ClientState" Selected="true">State changes</asp:ListItem>
                                    </D:CheckBoxList>
								</td> 
								<td class="label" rowspan="2">
                                    <D:Label runat="server" ID="lblCommand">Commando</D:Label>                                    
								</td>
								<td class="control" rowspan="2">
                                    <X:ComboBoxInt runat="server" ID="cbCommand" notdirty="true"></X:ComboBoxInt><br />
								    <D:CheckBox runat="server" ID="cbCommandToSupportTools" Text="SupportTools" /><br /><br/>
                                    <X:Button runat="server" ID="btExecuteCommand" Text="Uitvoeren"></X:Button>
								</td>    			                              
							</tr>        
							<tr>
								<td class="label">
									<D:Label runat="server" ID="lblAmountToShow">Limit</D:Label>
								</td>
								<td class="control">						
                                    <D:TextBoxInt runat="server" ID="tbAmountToShow" notdirty="true" Width="65px" Text="100"></D:TextBoxInt>
								</td>                               
							</tr>     
							<tr>
								<td class="label">
									&nbsp;
								</td>
								<td class="control">						
                                    <X:Button runat="server" ID="btUpdateActivityLog" Text="Refresh"></X:Button>
								</td>                               
							</tr>                                                                             
						</table>	
                        <X:GridView ID="gvClientActivityLog" ClientInstanceName="categoriesGrid" runat="server" Width="100%">        
                            <SettingsBehavior AutoFilterRowInputDelay="350" />                            
                            <SettingsPager PageSize="17"></SettingsPager>
                            <SettingsBehavior AllowGroup="false" AllowDragDrop="false" />
                            <Columns>
                                <dxwgv:GridViewDataDateColumn FieldName="DateTime" Caption="Time" VisibleIndex="0" Width="110px">
                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yy HH:mm:ss"> </PropertiesDateEdit>
                                </dxwgv:GridViewDataDateColumn>
                                <dxwgv:GridViewDataColumn FieldName="TypeText" Caption="Type" VisibleIndex="1">
			                        <CellStyle HorizontalAlign="left" />
                                </dxwgv:GridViewDataColumn>
                                <dxwgv:GridViewDataColumn FieldName="DescriptionTextShort" Caption="Description" VisibleIndex="2">
                                    <CellStyle HorizontalAlign="left" />
                                </dxwgv:GridViewDataColumn>
							    <dxwgv:GridViewDataHyperLinkColumn FieldName="LinkToEntityPageCms" Caption="Details" VisibleIndex="10" >
                                    <PropertiesHyperLinkEdit NavigateUrlFormatString="{0}" Text="Details"  />
                                </dxwgv:GridViewDataHyperLinkColumn>
                            </Columns>
                        </X:GridView>   
					</D:PlaceHolder>
				</controls>
			</X:TabPage>            					
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>

