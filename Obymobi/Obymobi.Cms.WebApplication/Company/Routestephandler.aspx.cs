﻿using System;
using System.Collections.Generic;
using System.Linq;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.Injectables.Validators;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Enums;
using Dionysos.Web;
using Obymobi.Logic.Model;

namespace Obymobi.ObymobiCms.Company
{
    public partial class Routestephandler : Dionysos.Web.UI.PageLLBLGenEntity
    {
        #region Methods

        private void SetWarnings()
        {
            var handler = this.DataSourceAsRoutestephandlerEntity;
            if (handler != null)
            {
                if (handler.HandlerTypeAsEnum == RoutestephandlerType.SMS)
                {
                    if (handler.FieldValue2.Contains("@")) // Originator field
                        this.AddInformator(Dionysos.Web.UI.InformatorType.Warning, this.Translate("lblIllegalCharacterInOriginatorField", "An illegal character is used in the originator field: '@'"));
                }

                this.Validate();
            }
        }

        private void SetGui()
        {
            // Prepare stuff to get Terminal, but the filter will be finalized in the HandlerType specific switch
            TerminalCollection terminals = new TerminalCollection();
            PredicateExpression terminalFilter = new PredicateExpression(TerminalFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            SortExpression terminalSort = new SortExpression(TerminalFields.Name | SortOperator.Ascending);

            // Don't allow terminals that we already used in this step to be used again            
            RoutestepEntity routeStep = null;
            int routeStepId = -1;
            if (!this.DataSourceAsRoutestephandlerEntity.IsNew)
            {
                routeStep = this.DataSourceAsRoutestephandlerEntity.RoutestepEntity;
            }
            else if (QueryStringHelper.TryGetValue("RoutestepId", out routeStepId))
            {   
                routeStep = new RoutestepEntity(routeStepId);
            }

            if (routeStep != null)
            {
                // One liner 20121031 (c) GK                
                List<int> terminalIdsAlreadyUsedInThisRouteStep = routeStep.RoutestephandlerCollection.Where(rs => rs.TerminalId.HasValue).Select(rs => rs.TerminalId.Value).ToList();

                // Remove the terminal used by this step of this list, because it's used to limit
                // the terminals in the dropdownlist, but it needs the terminal used by this step. (is this comment helpful, or confusing? ;))
                if (!this.DataSourceAsRoutestephandlerEntity.IsNew &&
                    this.DataSourceAsRoutestephandlerEntity.TerminalId.HasValue &&
                    terminalIdsAlreadyUsedInThisRouteStep.Contains(this.DataSourceAsRoutestephandlerEntity.TerminalId.Value))
                {
                    terminalIdsAlreadyUsedInThisRouteStep.Remove(this.DataSourceAsRoutestephandlerEntity.TerminalId.Value);
                }

                terminalFilter.Add(TerminalFields.TerminalId != terminalIdsAlreadyUsedInThisRouteStep);
            }

            // Get the routesteps for the dropdownlist
            RoutestepCollection routesteps = new RoutestepCollection();
            PredicateExpression filterRoutesteps = new PredicateExpression();
            filterRoutesteps.Add(RouteFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            RelationCollection joins = new RelationCollection();
            joins.Add(RoutestepEntity.Relations.RouteEntityUsingRouteId);
            routesteps.GetMulti(filterRoutesteps, joins);

            this.ddlRoutestepId.TextField = "RouteNameAndNumber";
            this.ddlRoutestepId.ValueField = "RoutestepId";
            this.ddlRoutestepId.DataSource = routesteps;
            this.ddlRoutestepId.DataBind();

            // Fill the available handler types				
            this.ddlHandlerType.DataBindEnumStringValuesAsDataSource(typeof(RoutestephandlerType));

            // Fill the available print report types				
            this.ddlPrintReportType.DataBindEnumStringValuesAsDataSource(typeof(PrintReportType));

            RoutestephandlerType handlerType = this.DataSource.IsNew ? this.ddlHandlerType.ValidId.ToEnum<RoutestephandlerType>() : this.DataSourceAsRoutestephandlerEntity.HandlerTypeAsEnum;

            switch (handlerType)
            {
                case RoutestephandlerType.Console:
                    this.plhTerminal.Visible = true;
                    this.plhSupportpool.Visible = true;
                    terminalFilter.Add(TerminalFields.HandlingMethod == (int)TerminalType.Console);
                    break;
                case RoutestephandlerType.Printer:
                    this.plhTerminal.Visible = true;
                    var consoleSettingsPanel = this.LoadControl<Subpanels.RoutestephandlerSettingsPanels.PrintSettingsPanel>("~/Company/SubPanels/RoutestephandlerSettingsPanels/PrintSettingsPanel.ascx");
                    this.plhHandlerTypeSpecificSettings.Controls.Add(consoleSettingsPanel);
                    break;
                case RoutestephandlerType.POS:
                    this.plhTerminal.Visible = true;
                    terminalFilter.Add(TerminalFields.HandlingMethod == (int)TerminalType.OnSiteServer);
                    break;
                case RoutestephandlerType.EmailOrder:
                    // GK Disable, bit to soon (my mistake), first need to make Support Pools per Company... 
                    //this.ddlSupportpoolId.Visible = true;
                    //this.lblSupportpoolId.Visible = true;
                    var emailSettingsPanel = this.LoadControl<Subpanels.RoutestephandlerSettingsPanels.EmailOrderSettingsPanel>("~/Company/SubPanels/RoutestephandlerSettingsPanels/EmailOrderSettingsPanel.ascx");
                    emailSettingsPanel.RoutestephandlerEntity = this.DataSourceAsRoutestephandlerEntity;
                    this.plhHandlerTypeSpecificSettings.Controls.Add(emailSettingsPanel);
                    break;
                case RoutestephandlerType.SMS:
                    // GK Disable, bit to soon (my mistake), first need to make Support Pools per Company... 
                    //this.ddlSupportpoolId.Visible = true;
                    //this.lblSupportpoolId.Visible = true;
                    var smsSettingsPanel = this.LoadControl<Subpanels.RoutestephandlerSettingsPanels.SmsSettingsPanel>("~/Company/SubPanels/RoutestephandlerSettingsPanels/SmsSettingsPanel.ascx");
                    smsSettingsPanel.RoutestephandlerEntity = this.DataSourceAsRoutestephandlerEntity;
                    this.plhHandlerTypeSpecificSettings.Controls.Add(smsSettingsPanel);
                    break;
                case RoutestephandlerType.HotSOS:
                    var hotSOSSettingsPanel = this.LoadControl<Subpanels.RoutestephandlerSettingsPanels.HotSOSSettingsPanel>("~/Company/SubPanels/RoutestephandlerSettingsPanels/HotSOSSettingsPanel.ascx");
                    this.plhHandlerTypeSpecificSettings.Controls.Add(hotSOSSettingsPanel);
                    break;
                case RoutestephandlerType.Quore:
                    var quoreSettingsPanel = this.LoadControl<Subpanels.RoutestephandlerSettingsPanels.QuoreSettingsPanel>("~/Company/SubPanels/RoutestephandlerSettingsPanels/QuoreSettingsPanel.ascx");
                    this.plhHandlerTypeSpecificSettings.Controls.Add(quoreSettingsPanel);
                    break;
                case RoutestephandlerType.Hyatt_HotSOS:
                    var hyattSettingsPanel = this.LoadControl<Subpanels.RoutestephandlerSettingsPanels.HyattSettingsPanel>("~/Company/SubPanels/RoutestephandlerSettingsPanels/HyattSettingsPanel.ascx");
                    this.plhHandlerTypeSpecificSettings.Controls.Add(hyattSettingsPanel);
                    break;
                case RoutestephandlerType.Alice:
                    var aliceSettingsPanel = this.LoadControl<Subpanels.RoutestephandlerSettingsPanels.AliceSettingsPanel>("~/Company/SubPanels/RoutestephandlerSettingsPanels/AliceSettingsPanel.ascx");
                    this.plhHandlerTypeSpecificSettings.Controls.Add(aliceSettingsPanel);
                    break;
                case RoutestephandlerType.ExternalSystem:
                    var externalSystemSettingsPanel = this.LoadControl<Subpanels.RoutestephandlerSettingsPanels.ExternalSystemSettingsPanel>("~/Company/SubPanels/RoutestephandlerSettingsPanels/ExternalSystemSettingsPanel.ascx");
                    this.plhHandlerTypeSpecificSettings.Controls.Add(externalSystemSettingsPanel);
                    break;
                case RoutestephandlerType.EmailDocument:
                    this.plhTerminal.Visible = true;
                    this.ddlTerminalId.IsRequired = true;
                    terminalFilter.Add(TerminalFields.HandlingMethod == (int)TerminalType.Console);
                    var emailDocumentSettingsPanel = this.LoadControl<Subpanels.RoutestephandlerSettingsPanels.EmailDocumentSettingsPanel>("~/Company/SubPanels/RoutestephandlerSettingsPanels/EmailDocumentSettingsPanel.ascx");
                    emailDocumentSettingsPanel.RoutestephandlerEntity = this.DataSourceAsRoutestephandlerEntity;
                    this.plhHandlerTypeSpecificSettings.Controls.Add(emailDocumentSettingsPanel);
                    break;
                default:
                    throw new NotImplementedException($"Routestephandler type '{handlerType}' is not implemented");
            }

            // Get and databind terminals
            terminals.GetMulti(terminalFilter, 0, terminalSort);
            this.ddlTerminalId.DataSource = terminals;
            this.ddlTerminalId.DataBind();

            // Remove Media tab for everything but email
            if (this.DataSourceAsRoutestephandlerEntity.HandlerTypeAsEnum != RoutestephandlerType.EmailOrder)
            {
                this.tabsMain.TabPages.FindByName("Media").Visible = false;
            }
        }

        #endregion

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            this.DataSourceLoaded += new Dionysos.Delegates.Web.DataSourceLoadedHandler(Routestephandler_DataSourceLoaded);
            base.OnInit(e);
        }

        private void LoadUserControls()
        {
            Generic.UserControls.CustomTextCollection translationsPanel = this.tabsMain.AddTabPage("Translations", "CustomTextCollection", "~/Generic/UserControls/CustomTextCollection.ascx", "Generic") as Generic.UserControls.CustomTextCollection;
            translationsPanel.FullWidth = true;

            this.tabsMain.AddTabPage("Afbeeldingen", "Media", "~/Generic/UserControls/MediaCollection.ascx");
        }

        private void Routestephandler_DataSourceLoaded(object sender)
        {
            this.SetGui();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!QueryStringHelper.HasValue("RoutestepId") && !QueryStringHelper.HasValue("id"))
            {
                throw new Exception("Page must be called with a valid RoutestepId or RoutestephandlerId (=id)");
            }

            if (!this.IsPostBack && !this.DataSourceAsRoutestephandlerEntity.IsNew)
                this.SetWarnings();
        }

        public override bool Save()
        {
            if (this.DataSourceAsRoutestephandlerEntity.IsNew)
            {
                int routestepId = int.Parse(Request.QueryString["RoutestepId"]);
                this.DataSourceAsRoutestephandlerEntity.RoutestepId = routestepId;
            }

            try
            {
                bool hasSupportpool = (DataSourceAsRoutestephandlerEntity.SupportpoolId.HasValue && DataSourceAsRoutestephandlerEntity.SupportpoolId.Value > 0);

                this.Validate();

                if (this.IsValid)
                    base.Save();
            }
            catch (ObymobiException ex)
            {
                if (ex.ErrorEnumValue.Equals(RoutestephandlerValidator.RoutestephandlerValidatorResult.HandlerTypeAndTerminalIncompatible))
                    this.MultiValidatorDefault.AddError(this.Translate("HandlerTypeAndTerminalIncompatible", "Het gekozen handler type kan niet worden gebruikt met deze terminal."));
                else if (ex.ErrorEnumValue.Equals(RoutestephandlerValidator.RoutestephandlerValidatorResult.OnlyOneAutomaticHandlerAllowedPerRoutestep))
                    this.MultiValidatorDefault.AddError(this.Translate("OnlyOneAutomaticHandlerAllowedPerRoutestep", "Voor deze routestep is al een (automatische) handler geconfigureerd, er kan slechts 1 automatische handler per stap worden gebruikt."));

                this.Validate();
            }

            return this.IsValid;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Return the page's datasource as a RoutestephandlerEntity
        /// </summary>
        public RoutestephandlerEntity DataSourceAsRoutestephandlerEntity
        {
            get
            {
                return this.DataSource as RoutestephandlerEntity;
            }
        }

        #endregion
    }
}
