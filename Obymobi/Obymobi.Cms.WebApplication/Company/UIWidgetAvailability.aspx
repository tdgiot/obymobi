﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.UIWidgetAvailability" Codebehind="UIWidgetAvailability.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<controls>
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblName">Name</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
							    <D:TextBoxString ID="tbName" runat="server" IsRequired="true" UseDataBinding="false"/>
							</td>
							<td class="label">
								<D:Label runat="server" id="lblStatusText">Status</D:Label>
							</td>
							<td class="control">
							    <D:TextBox ID="tbStatusText" runat="server" IsRequired="true" UseDataBinding="false"/>
							</td>
					    </tr>
                        <tr>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblSortOrder">Sort order</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <D:TextBoxInt ID="tbSortOrder" runat="server" IsRequired="true" ThousandsSeperators="false" />
                            </td>
                        </tr>
                    </table>
                </controls>
            </X:TabPage>            								
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>

