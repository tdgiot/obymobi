﻿using System;
using Crave.Api.Logic.Requests.Publishing;
using Crave.Api.Logic.Timestamps;
using Crave.Api.Logic.UseCases;
using Dionysos;
using Dionysos.Web.UI;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;

namespace Obymobi.ObymobiCms.Company
{
    public partial class TerminalConfiguration : Dionysos.Web.UI.PageLLBLGenEntity
	{
        protected override void OnInit(EventArgs e)
		{
            this.LoadUserControls();
            this.DataSourceLoaded += ClientConfiguration_DataSourceLoaded;
			base.OnInit(e);			
		}

        private void ClientConfiguration_DataSourceLoaded(object sender)
        {
            this.SetGui();
        }

        private void SetGui()
        {
            this.RenderLinkedTerminals();

            MasterPages.MasterPageEntity masterPage = this.Master as MasterPages.MasterPageEntity;
            if (masterPage != null)
            {
                masterPage.ToolBar.SaveAndNewButton.Visible = false;
            }
        }

        private void LoadUserControls()
        {
            // Generic tab
            this.cbTerminalId.DataBindEntityCollection<TerminalEntity>(TerminalFields.CompanyId == CmsSessionHelper.CurrentCompanyId, TerminalFields.Name, TerminalFields.Name);
            this.cbUIModeId.DataBindEntityCollection<UIModeEntity>(UIModeFields.CompanyId == CmsSessionHelper.CurrentCompanyId, UIModeFields.Name, UIModeFields.Name);
            this.ddlUseHardKeyboard.DataBindEnum<HardwareKeyboardType>();
        }        

        public override bool Save()
        {
            if (this.PageMode == Dionysos.Web.PageMode.Add)
            {
                this.DataSourceAsTerminalConfigurationEntity.CompanyId = CmsSessionHelper.CurrentCompanyId;
            }

            this.DataSourceAsTerminalConfigurationEntity.Pincode = this.tbPincode.Value.TrimToLength(25, false);
            this.DataSourceAsTerminalConfigurationEntity.PincodeSU = this.tbPincodeSU.Value.TrimToLength(25, false);
            this.DataSourceAsTerminalConfigurationEntity.PincodeGM = this.tbPincodeGM.Value.TrimToLength(25, false);

            if (this.IsValid())
            {
                return base.Save();
            }
            return false;
        }

        public new bool IsValid()
        {
            if (this.DataSourceAsTerminalConfigurationEntity.Pincode.Length < 6 || this.DataSourceAsTerminalConfigurationEntity.PincodeSU.Length < 6 || this.DataSourceAsTerminalConfigurationEntity.PincodeGM.Length < 6)
            {
                this.MultiValidatorDefault.AddError(string.Format("Pincodes have to be at least 6 characters long (Pincode: {0}, SU: {1}, GM: {2})",
                    this.DataSourceAsTerminalConfigurationEntity.Pincode.Length,
                    this.DataSourceAsTerminalConfigurationEntity.PincodeSU.Length,
                    this.DataSourceAsTerminalConfigurationEntity.PincodeGM.Length));

                this.Validate();
                return false;
            }
            return true;
        }

        private void RenderLinkedTerminals()
        {
            System.Text.StringBuilder builder = new System.Text.StringBuilder();
            builder.AppendFormatLine("<table class=\"dataformV2\">");
            builder.AppendFormatLine("<tr>");
            builder.AppendFormatLine("   <td class='control'>");
            builder.AppendFormatLine("       <b>{0}</b>", this.Translate("lblTerminalsHeader", "Terminals"));
            builder.AppendFormatLine("   </td>");
            builder.AppendFormatLine("   <td class='label'>");
            builder.AppendFormatLine("   </td>");
            builder.AppendFormatLine("</tr>");

            if (this.DataSourceAsTerminalConfigurationEntity.TerminalCollection.Count == 0)
            {
                builder.AppendFormatLine("<tr>");
                builder.AppendFormatLine("   <td class='control'>");
                builder.AppendFormatLine("       None");
                builder.AppendFormatLine("   </td>");
                builder.AppendFormatLine("   <td class='label'>");
                builder.AppendFormatLine("   </td>");
                builder.AppendFormatLine("</tr>");

            }

            foreach (TerminalEntity entity in this.DataSourceAsTerminalConfigurationEntity.TerminalCollection)
            {
                string link = string.Format("<a href=\"Terminal.aspx?id={0}\">{1}</a>", entity.TerminalId, entity.Name);

                builder.AppendFormatLine("<tr>");
                builder.AppendFormatLine("   <td class='control'>");
                builder.AppendFormatLine("       {0}", link);
                builder.AppendFormatLine("   </td>");
                builder.AppendFormatLine("   <td class='label'>");
                builder.AppendFormatLine("   </td>");
                builder.AppendFormatLine("</tr>");
            }

            builder.AppendFormatLine("</table>");
            this.plhLinkedEntities.AddHtml(builder.ToString());
        }

        public TerminalConfigurationEntity DataSourceAsTerminalConfigurationEntity
        {
            get
            {
                return this.DataSource as TerminalConfigurationEntity;
            }
        }        
    }
}
