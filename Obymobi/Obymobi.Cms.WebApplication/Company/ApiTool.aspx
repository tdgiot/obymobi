﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.ApiTool" Title="API Tool" Codebehind="ApiTool.aspx.cs" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cplhTitleHolder">REST API Tool</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="cplhToolbarHolder">        
    <X:ToolBarButton runat="server" ID="btCall" CommandName="Show" Text="Call API (View)" Image-Url="~/images/icons/table.png" style="margin-left: 3px;float:left;"></X:ToolBarButton>
    <X:ToolBarButton runat="server" ID="btDownload" CommandName="Download" Text="Call API (Download)" Image-Url="~/images/icons/table_save.png" style="margin-left: 3px;float:left"></X:ToolBarButton>    
</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="cplhContentHolder">
<script type="text/javascript">

    function OnApiOperationChanged(cbApiOperation) {
        HideAllTableRows();
        var apiOperation = cbApiOperation.GetValue().toString();
        ShowTableRowsForApiOperation(apiOperation);
    }

    function ShowTableRowsForApiOperation(apiOperation) {
        // The variable that is evaluated using eval is generated in the RegisterJavaScript method in the code behind
        var parameterNamesForApiOperation = eval(apiOperation);
        if (parameterNamesForApiOperation != null)
            ShowTableRows(parameterNamesForApiOperation);
    }

    function ShowTableRows(parameterNames) {
        for (i = 0; i < parameterNames.length; i++) {
            ShowTableRow(parameterNames[i]);
        }
    }

    function ShowTableRow(parameterName) {
        var id = "ctl00_cplhContentHolder_tabsMain_tr" + parameterName;
        var element = document.getElementById(id);
        if (element != null)
            element.style.display = "table-row";
    }

    function HideTableRow(parameterName) {
        var id = "ctl00_cplhContentHolder_tabsMain_tr" + parameterName;
        var element = document.getElementById(id);
        if (element != null)
            element.style.display = "none";
    }

</script>
<div>
	<X:PageControl ID="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
                    <asp:Table ID="tblMain" runat="server" CssClass="dataformV2">
                        <asp:TableRow>
                            <asp:TableCell CssClass="label">
<D:Label ID="lblApiOperation" runat="server">API operation</D:Label>
                            </asp:TableCell>
                            <asp:TableCell CssClass="control">
<X:ComboBox runat="server" ID="cbApiOperation" AutoPostBack="false" ClientInstanceName="cbApiOperation" >
    <ClientSideEvents SelectedIndexChanged="function(s, e) { OnApiOperationChanged(s); }"></ClientSideEvents>
</X:ComboBox>
                            </asp:TableCell>
                            <asp:TableCell CssClass="label">
<D:Label ID="lblApiBaseUrl" runat="server">API base URL</D:Label>
                            </asp:TableCell>
                            <asp:TableCell CssClass="control">
<D:TextBoxString runat="server" ID="tbApiBaseUrl" Text=""></D:TextBoxString>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="trClientId" CssClass="hidden">
                            <asp:TableCell CssClass="label"><D:Label ID="lblClientId" runat="server">Client</D:Label></asp:TableCell>
                            <asp:TableCell CssClass="control"><X:ComboBoxInt runat="server" ID="cbClientId"></X:ComboBoxInt></asp:TableCell>
                            <asp:TableCell CssClass="label"></asp:TableCell>
                            <asp:TableCell CssClass="control"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="trTerminalId" CssClass="hidden">
                            <asp:TableCell CssClass="label"><D:Label ID="lblTerminalId" runat="server">Terminal</D:Label></asp:TableCell>
                            <asp:TableCell CssClass="control"><X:ComboBoxInt runat="server" ID="cbTerminalId"></X:ComboBoxInt></asp:TableCell>
                            <asp:TableCell CssClass="label"></asp:TableCell>
                            <asp:TableCell CssClass="control"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="trAdvertisementConfigurationId" CssClass="hidden">
                            <asp:TableCell CssClass="label"><D:Label ID="lblAdvertisementConfigurationId" runat="server">Advertisement configuration</D:Label></asp:TableCell>
                            <asp:TableCell CssClass="control"><X:ComboBoxInt runat="server" ID="cbAdvertisementConfigurationId"></X:ComboBoxInt></asp:TableCell>
                            <asp:TableCell CssClass="label"></asp:TableCell>
                            <asp:TableCell CssClass="control"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="trClientConfigurationId" CssClass="hidden">
                            <asp:TableCell CssClass="label"><D:Label ID="lblClientConfigurationId" runat="server">Client configuration</D:Label></asp:TableCell>
                            <asp:TableCell CssClass="control"><X:ComboBoxInt runat="server" ID="cbClientConfigurationId"></X:ComboBoxInt></asp:TableCell>
                            <asp:TableCell CssClass="label"></asp:TableCell>
                            <asp:TableCell CssClass="control"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="trDeliverypointId" CssClass="hidden">
                            <asp:TableCell CssClass="label"><D:Label ID="lblDeliverypointId" runat="server">Deliverypoint</D:Label></asp:TableCell>
                            <asp:TableCell CssClass="control"><X:ComboBoxInt runat="server" ID="cbDeliverypointId"></X:ComboBoxInt></asp:TableCell>
                            <asp:TableCell CssClass="label"></asp:TableCell>
                            <asp:TableCell CssClass="control"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="trDeliverypointgroupId" CssClass="hidden">
                            <asp:TableCell CssClass="label"><D:Label ID="lblDeliverypointgroupId" runat="server">Deliverypointgroup</D:Label></asp:TableCell>
                            <asp:TableCell CssClass="control"><X:ComboBoxInt runat="server" ID="cbDeliverypointgroupId"></X:ComboBoxInt></asp:TableCell>
                            <asp:TableCell CssClass="label"></asp:TableCell>
                            <asp:TableCell CssClass="control"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="trEntertainmentConfigurationId" CssClass="hidden">
                            <asp:TableCell CssClass="label"><D:Label ID="lblEntertainmentConfigurationId" runat="server">Entertainment configuration</D:Label></asp:TableCell>
                            <asp:TableCell CssClass="control"><X:ComboBoxInt runat="server" ID="cbEntertainmentConfigurationId"></X:ComboBoxInt></asp:TableCell>
                            <asp:TableCell CssClass="label"></asp:TableCell>
                            <asp:TableCell CssClass="control"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="trInfraredConfigurationId" CssClass="hidden">
                            <asp:TableCell CssClass="label"><D:Label ID="lblInfraredConfigurationId" runat="server">Infrared configuration</D:Label></asp:TableCell>
                            <asp:TableCell CssClass="control"><X:ComboBoxInt runat="server" ID="cbInfraredConfigurationId"></X:ComboBoxInt></asp:TableCell>
                            <asp:TableCell CssClass="label"></asp:TableCell>
                            <asp:TableCell CssClass="control"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="trMenuId" CssClass="hidden">
                            <asp:TableCell CssClass="label"><D:Label ID="lblMenuId" runat="server">Menu</D:Label></asp:TableCell>
                            <asp:TableCell CssClass="control"><X:ComboBoxInt runat="server" ID="cbMenuId"></X:ComboBoxInt></asp:TableCell>
                            <asp:TableCell CssClass="label"></asp:TableCell>
                            <asp:TableCell CssClass="control"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="trPointOfInterestId" CssClass="hidden">
                            <asp:TableCell CssClass="label"><D:Label ID="lblPointOfInterestId" runat="server">Point of interest</D:Label></asp:TableCell>
                            <asp:TableCell CssClass="control"><X:ComboBoxInt runat="server" ID="cbPointOfInterestId"></X:ComboBoxInt></asp:TableCell>
                            <asp:TableCell CssClass="label"></asp:TableCell>
                            <asp:TableCell CssClass="control"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="trPriceScheduleId" CssClass="hidden">
                            <asp:TableCell CssClass="label"><D:Label ID="lblPriceScheduleId" runat="server">Price schedule</D:Label></asp:TableCell>
                            <asp:TableCell CssClass="control"><X:ComboBoxInt runat="server" ID="cbPriceScheduleId"></X:ComboBoxInt></asp:TableCell>
                            <asp:TableCell CssClass="label"></asp:TableCell>
                            <asp:TableCell CssClass="control"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="trRoomControlConfigurationId" CssClass="hidden">
                            <asp:TableCell CssClass="label"><D:Label ID="lblRoomControlConfigurationId" runat="server">Room control configuration</D:Label></asp:TableCell>
                            <asp:TableCell CssClass="control"><X:ComboBoxInt runat="server" ID="cbRoomControlConfigurationId"></X:ComboBoxInt></asp:TableCell>
                            <asp:TableCell CssClass="label"></asp:TableCell>
                            <asp:TableCell CssClass="control"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="trSiteId" CssClass="hidden">
                            <asp:TableCell CssClass="label"><D:Label ID="lblSiteId" runat="server">Site</D:Label></asp:TableCell>
                            <asp:TableCell CssClass="control"><X:ComboBoxInt runat="server" ID="cbSiteId"></X:ComboBoxInt></asp:TableCell>
                            <asp:TableCell CssClass="label"><D:Label ID="lblCultureCode" runat="server">Culture code</D:Label></asp:TableCell>
                            <asp:TableCell CssClass="control"><D:TextBoxString runat="server" ID="tbCultureCode"></D:TextBoxString></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="trTerminalConfigurationId" CssClass="hidden">
                            <asp:TableCell CssClass="label"><D:Label ID="lblTerminalConfigurationId" runat="server">Terminal configuration</D:Label></asp:TableCell>
                            <asp:TableCell CssClass="control"><X:ComboBoxInt runat="server" ID="cbTerminalConfigurationId"></X:ComboBoxInt></asp:TableCell>
                            <asp:TableCell CssClass="label"></asp:TableCell>
                            <asp:TableCell CssClass="control"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="trUIModeId" CssClass="hidden">
                            <asp:TableCell CssClass="label"><D:Label ID="lblUIModeId" runat="server">UI mode</D:Label></asp:TableCell>
                            <asp:TableCell CssClass="control"><X:ComboBoxInt runat="server" ID="cbUIModeId"></X:ComboBoxInt></asp:TableCell>
                            <asp:TableCell CssClass="label"></asp:TableCell>
                            <asp:TableCell CssClass="control"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="trUIScheduleId" CssClass="hidden">
                            <asp:TableCell CssClass="label"><D:Label ID="lblUIScheduleId" runat="server">UI schedule</D:Label></asp:TableCell>
                            <asp:TableCell CssClass="control"><X:ComboBoxInt runat="server" ID="cbUIScheduleId"></X:ComboBoxInt></asp:TableCell>
                            <asp:TableCell CssClass="label"></asp:TableCell>
                            <asp:TableCell CssClass="control"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="trUIThemeId" CssClass="hidden">
                            <asp:TableCell CssClass="label"><D:Label ID="lblUIThemeId" runat="server">UI theme</D:Label></asp:TableCell>
                            <asp:TableCell CssClass="control"><X:ComboBoxInt runat="server" ID="cbUIThemeId"></X:ComboBoxInt></asp:TableCell>
                            <asp:TableCell CssClass="label"></asp:TableCell>
                            <asp:TableCell CssClass="control"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="trApplicationCode" CssClass="hidden">
                            <asp:TableCell CssClass="label"><D:Label ID="lblApplicationCode" runat="server">Application</D:Label></asp:TableCell>
                            <asp:TableCell CssClass="control"><X:ComboBoxString runat="server" ID="cbApplicationCode"></X:ComboBoxString></asp:TableCell>
                            <asp:TableCell CssClass="label"></asp:TableCell>
                            <asp:TableCell CssClass="control"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="trUsername" CssClass="hidden">
                            <asp:TableCell CssClass="label"><D:Label ID="lblUsername" runat="server">Username</D:Label></asp:TableCell>
                            <asp:TableCell CssClass="control"><D:TextBoxString runat="server" ID="tbUsername"></D:TextBoxString></asp:TableCell>
                            <asp:TableCell CssClass="label"></asp:TableCell>
                            <asp:TableCell CssClass="control"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="trPassword" CssClass="hidden">
                            <asp:TableCell CssClass="label"><D:Label ID="lblPassword" runat="server">Password</D:Label></asp:TableCell>
                            <asp:TableCell CssClass="control"><D:TextBoxPassword runat="server" ID="tbPassword"></D:TextBoxPassword></asp:TableCell>
                            <asp:TableCell CssClass="label"></asp:TableCell>
                            <asp:TableCell CssClass="control"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="trOrderIds" CssClass="hidden">
                            <asp:TableCell CssClass="label"><D:Label ID="lblOrderIds" runat="server">Orders</D:Label></asp:TableCell>
                            <asp:TableCell CssClass="control"><D:TextBoxString runat="server" ID="tbOrderIds"></D:TextBoxString></asp:TableCell>
                            <asp:TableCell CssClass="label"></asp:TableCell>
                            <asp:TableCell CssClass="control"></asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </Controls>
            </X:TabPage>
        </TabPages>
    </X:PageControl>
    <pre><D:PlaceHolder runat="server" ID="plhReponse"></D:PlaceHolder></pre>
</div>
</asp:Content>