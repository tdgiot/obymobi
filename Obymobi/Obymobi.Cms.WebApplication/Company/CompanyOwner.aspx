﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.CompanyOwner" Codebehind="CompanyOwner.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Standaard" Name="Generic">
				<Controls>
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblUsername">Name</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
                                <D:TextBoxString ID="tbUsername" runat="server" ReadOnly="true"></D:TextBoxString>
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblDecryptedPassword">Password</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
                                <D:TextBoxString ID="tbDecryptedPassword" runat="server" ReadOnly="true"></D:TextBoxString>
							</td>
					    </tr>
						<tr>
							<td class="label">
							</td>
							<td class="control">
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblHashedPassword">Password hash</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
                                <D:TextBoxString ID="tbHashedPassword" runat="server" ReadOnly="true"></D:TextBoxString>
							</td>
					    </tr>
					 </table>			
				</Controls>
			</X:TabPage>
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>

