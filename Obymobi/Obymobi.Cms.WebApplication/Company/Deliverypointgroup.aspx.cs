﻿using System;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dionysos;
using Dionysos.Web;
using Dionysos.Web.UI;
using Dionysos.Web.UI.DevExControls;
using Obymobi.Cms.Logic.HelperClasses;
using Obymobi.Cms.Logic.Requests;
using Obymobi.Cms.Logic.UseCases;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using Obymobi.ObymobiCms.Company.Subpanels;
using Obymobi.Web.Caching;
using Obymobi.Web.Google;
using Obymobi.Web.Media;
using Resources;
using SD.LLBLGen.Pro.ORMSupportClasses;
using CacheHelper = Dionysos.Web.CacheHelper;
using CheckBoxList = System.Web.UI.WebControls.CheckBoxList;
using DeliverypointCollection = Obymobi.ObymobiCms.Company.Subpanels.DeliverypointCollection;

namespace Obymobi.ObymobiCms.Company
{
    public partial class Deliverypointgroup : PageLLBLGenEntity
    {
        private DeliverypointCollection deliverypointsPanel;
        private DeliverypointgroupAddress deliverypointgroupAddress;

        public DeliverypointgroupEntity DataSourceAsDeliverypointgroupEntity
        {
            get { return this.DataSource as DeliverypointgroupEntity; }
        }

        private void LoadUserControls()
        {
            if (CmsSessionHelper.CurrentRole < Role.Reseller)
            {
                this.tabsMain.TabPages.FindByName("tabAdvanced").ClientVisible = false;
                this.tabsMain.TabPages.FindByName("Copy").ClientVisible = false;
                this.tabsMain.TabPages.FindByName("Mobile").ClientVisible = false;
                this.tabsMain.TabPages.FindByName("PowerManagement").ClientVisible = false;
                this.tabsMain.TabPages.FindByName("ServiceRequests").ClientVisible = false;
                this.tabsMain.TabPages.FindByName("Crave").ClientVisible = false;

                this.tabsMain.AddTabPage("Media", "Media", "~/Generic/UserControls/MediaCollection.ascx");
                this.tabsMain.AddTabPage("Announcements", "Announcements", "~/Company/SubPanels/DeliverypointgroupAnnouncementCollection.ascx");
                this.tabsMain.AddTabPage("Advertisements", "DeliverypointgroupAdvertisementPanel", "~/Company/Subpanels/DeliverypointgroupAdvertisementCollection.ascx");
            }
            else 
            {
                this.tabsMain.AddTabPage("Translations", "CustomTextCollection", "~/Generic/UserControls/CustomTextCollection.ascx");
                this.tabsMain.AddTabPage("Media", "Media", "~/Generic/UserControls/MediaCollection.ascx");
                this.tabsMain.AddTabPage("Announcements", "Announcements", "~/Company/SubPanels/DeliverypointgroupAnnouncementCollection.ascx");
                this.deliverypointsPanel = this.tabsMain.AddTabPage("Deliverypoints", "Deliverypoints", "~/Company/SubPanels/DeliverypointCollection.ascx") as DeliverypointCollection;
                this.tabsMain.AddTabPage("Entertainment", "DeliverypointgroupEntertainmentPanel", "~/Company/Subpanels/DeliverypointgroupEntertainmentCollection.ascx");
                this.tabsMain.AddTabPage("Advertisements", "DeliverypointgroupAdvertisementPanel", "~/Company/Subpanels/DeliverypointgroupAdvertisementCollection.ascx");
            }

            if (CmsSessionHelper.CurrentUser.HasFeature(FeatureToggle.AllThingsLondon))
            {
                deliverypointgroupAddress = (DeliverypointgroupAddress)tabsMain.AddTabPage("Address", "DeliverypointgroupAddress", "~/Company/Subpanels/DeliverypointgroupAddress.ascx");
            }

            this.ddlWifiSecurityMethod.DataBindEnum<WifiSecurityMethod>();
            this.ddlWifiEapMode.DataBindEnum<WifiEapMode>();
            this.ddlWifiPhase2Authentication.DataBindEnum<WifiPhase2Authentication>();

            this.ddlPowerButtonHardBehaviour.DataBindEnum<PowerButtonMode>();
            this.ddlPowerButtonSoftBehaviour.DataBindEnum<PowerButtonMode>();
            this.ddlScreenOffMode.DataBindEnum<ScreenOffMode>();

            this.ddlUseHardKeyboard.DataBindEnum<HardwareKeyboardType>();

            this.ddlBrowserAgeVerificationLayout.DataBindEnum<BrowserAgeVerificationLayoutType>();

            this.ddlScreensaverMode.DataBindEnum<ScreensaverMode>();

            PredicateExpression filter = new PredicateExpression();
            filter.Add(ProductFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            filter.Add(ExternalSystemFields.Type == ExternalSystemType.HotSOS);

            RelationCollection relations = new RelationCollection();
            relations.Add(ProductEntity.Relations.ExternalProductEntityUsingExternalProductId);
            relations.Add(ExternalProductEntity.Relations.ExternalSystemEntityUsingExternalSystemId);

            ProductCollection hotSOSProducts = new ProductCollection();
            hotSOSProducts.GetMulti(filter, relations);

            this.cbHotSOSBatteryLowProductId.DataSource = hotSOSProducts;
            this.cbHotSOSBatteryLowProductId.DataBind();

            // Email document routes
            this.ddlEmailDocumentRouteId.DataSource = RouteHelper.GetRouteCollection(CmsSessionHelper.CurrentCompanyId, true);
            this.ddlEmailDocumentRouteId.DataBind();

            this.btRefreshPrinters.Click += this.btRefreshPrinters_Click;
            this.btSelectPrinter.Click += this.btSelectPrinter_Click;

            this.cbClientConfigurationId.DataBindEntityCollection<ClientConfigurationEntity>(ClientConfigurationFields.CompanyId == CmsSessionHelper.CurrentCompanyId, ClientConfigurationFields.Name, ClientConfigurationFields.Name);
            this.cbAffiliateCampaignId.DataBindEntityCollection<AffiliateCampaignEntity>(null, AffiliateCampaignFields.Name, AffiliateCampaignFields.Name);
        }

        public void BindEnumStringValuesToCheckboxList(CheckBoxList cbl, Type enumeration)
        {
            string[] names = Enum.GetNames(enumeration);
            Array values = Enum.GetValues(enumeration);

            for (int i = 0; i < names.Length; i++)
            {
                ListItem item = new ListItem(((Enum)values.GetValue(i)).GetStringValue(), Convert.ToInt32(values.GetValue(i)).ToString());
                cbl.Items.Add(item);
            }
        }

        private void SetFilters()
        {
            this.SetServiceitemFilter();

            PredicateExpression entertainmentFilter = new PredicateExpression();
            entertainmentFilter.Add(EntertainmentFields.CompanyId == DBNull.Value);

            if (CmsSessionHelper.CurrentCompanyId > 0)
            {
                entertainmentFilter.AddWithOr(EntertainmentFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            }

            PrefetchPath path = new PrefetchPath(EntityType.EntertainmentEntity);
            path.Add(EntertainmentEntityBase.PrefetchPathEntertainmentcategoryEntity);

            PredicateExpression pathFilter = new PredicateExpression(DeliverypointgroupFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            RelationCollection pathRelations = new RelationCollection(DeliverypointgroupEntertainmentEntityBase.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId);
            path.Add(EntertainmentEntityBase.PrefetchPathDeliverypointgroupEntertainmentCollection, 0, pathFilter, pathRelations);

            SortExpression sort = new SortExpression();
            sort.Add(new SortClause(EntertainmentcategoryFields.Name, SortOperator.Ascending));
            sort.Add(new SortClause(DeliverypointgroupEntertainmentFields.SortOrder, SortOperator.Ascending));
            sort.Add(new SortClause(EntertainmentFields.Name, SortOperator.Ascending));

            RelationCollection relations = new RelationCollection();
            relations.Add(EntertainmentEntityBase.Relations.EntertainmentcategoryEntityUsingEntertainmentcategoryId, JoinHint.Left);
            relations.Add(EntertainmentEntityBase.Relations.DeliverypointgroupEntertainmentEntityUsingEntertainmentId, JoinHint.Left);

            EntertainmentCollection entertainment = new EntertainmentCollection();
            entertainment.GetMulti(entertainmentFilter, 0, sort, relations, path);
        }

        private void SetServiceitemFilter()
        {
            PredicateExpression serviceFilter = new PredicateExpression();
            serviceFilter.Add(ProductFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            serviceFilter.Add(ProductFields.Type == ProductType.Service);

            SortExpression sort = new SortExpression();
            sort.Add(new SortClause(ProductFields.Name, SortOperator.Ascending));

            ProductCollection serviceitems = new ProductCollection();
            serviceitems.GetMulti(serviceFilter, 0, sort);

            this.BindServiceItemsToCombobox(this.ddlServiceProduct1, serviceitems);
            this.BindServiceItemsToCombobox(this.ddlServiceProduct2, serviceitems);
            this.BindServiceItemsToCombobox(this.ddlServiceProduct3, serviceitems);
            this.BindServiceItemsToCombobox(this.ddlServiceProduct4, serviceitems);
            this.BindServiceItemsToCombobox(this.ddlServiceProduct5, serviceitems);
            this.BindServiceItemsToCombobox(this.ddlServiceProduct6, serviceitems);
            this.BindServiceItemsToCombobox(this.ddlServiceProduct6, serviceitems);
            this.BindServiceItemsToCombobox(this.ddlServiceProduct7, serviceitems);
            this.BindServiceItemsToCombobox(this.ddlServiceProduct8, serviceitems);
            this.BindServiceItemsToCombobox(this.ddlServiceProduct9, serviceitems);
            this.BindServiceItemsToCombobox(this.ddlServiceProduct10, serviceitems);
            this.BindServiceItemsToCombobox(this.ddlServiceProduct11, serviceitems);
            this.BindServiceItemsToCombobox(this.ddlServiceProduct12, serviceitems);
            this.BindServiceItemsToCombobox(this.ddlServiceProduct13, serviceitems);
            this.BindServiceItemsToCombobox(this.ddlServiceProduct14, serviceitems);
            this.BindServiceItemsToCombobox(this.ddlServiceProduct15, serviceitems);
        }

        private void BindServiceItemsToCombobox(ComboBoxLLBLGenEntityCollection cb, ProductCollection serviceItems)
        {
            foreach (ProductEntity product in serviceItems)
            {
                cb.Items.Add(product.Name, product.ProductId);
            }
        }

        private void SetDefaults()
        {
            this.tbResetTimeout.Value = 7;
            this.ddlLocale.SelectedIndex = 0;
        }

        private void SetGui()
        {
            SetPosdeliverypointgroupFilter();

            PredicateExpression announcementsFilter = new PredicateExpression();
            announcementsFilter.Add(AnnouncementFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            announcementsFilter.Add(AnnouncementFields.DeliverypointgroupId == DBNull.Value);

            AnnouncementCollection announcementCollection = new AnnouncementCollection();
            announcementCollection.GetMulti(announcementsFilter);

            this.cbReorderNotificationAnnouncementId.DataSource = announcementCollection;
            this.cbReorderNotificationAnnouncementId.DataBind();

            this.ddlLocale.Items.Add("Dutch", "nl_NL");
            this.ddlLocale.Items.Add("English", "en_UK");
            this.ddlLocale.Items.Add("German", "de_DE");
            
            RouteCollection routes = RouteHelper.GetRouteCollection(CmsSessionHelper.CurrentCompanyId);
            this.ddlRouteId.DataSource = routes;
            this.ddlRouteId.DataBind();

            this.ddlOrderNotesRouteId.DataSource = routes;
            this.ddlOrderNotesRouteId.DataBind();

            this.ddlSystemMessageRouteId.DataSource = routes;
            this.ddlSystemMessageRouteId.DataBind();

            MenuCollection menus = new MenuCollection();
            PredicateExpression menuFilter = new PredicateExpression(MenuFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            SortExpression menuSort = new SortExpression(MenuFields.Name | SortOperator.Ascending);
            menus.GetMulti(menuFilter, 0, menuSort);

            this.ddlMenuId.DataSource = menus;
            this.ddlMenuId.DataBind();

            PredicateExpression venueOwnedUserDevicesFilter = new PredicateExpression();
            venueOwnedUserDevicesFilter.Add(UIModeFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            venueOwnedUserDevicesFilter.Add(UIModeFields.Type == (int)UIModeType.VenueOwnedUserDevices);
            UIModeCollection venueOwnedUserDevicesUiModes = new UIModeCollection();
            venueOwnedUserDevicesUiModes.GetMulti(venueOwnedUserDevicesFilter);
            this.ddlUIModeId.DataSource = venueOwnedUserDevicesUiModes;
            this.ddlUIModeId.DataBind();

            PredicateExpression guestOwnedMobileDevicesFilter = new PredicateExpression();
            guestOwnedMobileDevicesFilter.Add(UIModeFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            guestOwnedMobileDevicesFilter.Add(UIModeFields.Type == (int)UIModeType.GuestOwnedMobileDevices);
            UIModeCollection guestOwnedMobileDevicesUiModes = new UIModeCollection();
            guestOwnedMobileDevicesUiModes.GetMulti(guestOwnedMobileDevicesFilter);
            this.ddlMobileUIModeId.DataSource = guestOwnedMobileDevicesUiModes;
            this.ddlMobileUIModeId.DataBind();

            PredicateExpression guestOwnedTabletDevicesFilter = new PredicateExpression();
            guestOwnedTabletDevicesFilter.Add(UIModeFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            guestOwnedTabletDevicesFilter.Add(UIModeFields.Type == (int)UIModeType.GuestOwnedTabletDevices);
            UIModeCollection guestOwnedTabletDevicesUiModes = new UIModeCollection();
            guestOwnedTabletDevicesUiModes.GetMulti(guestOwnedTabletDevicesFilter);
            this.ddlTabletUIModeId.DataSource = guestOwnedTabletDevicesUiModes;
            this.ddlTabletUIModeId.DataBind();

            if (!this.DataSourceAsDeliverypointgroupEntity.DailyOrderReset.IsNullOrWhiteSpace())
            {
                this.teDailyOrderReset.Value = this.GetTimeFromString(this.DataSourceAsDeliverypointgroupEntity.DailyOrderReset);
            }

            if (!this.DataSourceAsDeliverypointgroupEntity.SleepTime.IsNullOrWhiteSpace())
            {
                this.teSleepTime.Value = this.GetTimeFromString(this.DataSourceAsDeliverypointgroupEntity.SleepTime);
            }

            if (!this.DataSourceAsDeliverypointgroupEntity.WakeUpTime.IsNullOrWhiteSpace())
            {
                this.teWakeUpTime.Value = this.GetTimeFromString(this.DataSourceAsDeliverypointgroupEntity.WakeUpTime);
            }

            this.plhWifiAdvanced.Visible = (this.DataSourceAsDeliverypointgroupEntity.WifiSecurityMethodAsEnum == WifiSecurityMethod.EAP);
            this.PopulateServiceitems();
            this.PopulateThemes();
            this.PopulateCustomTexts();

            this.ddlUIScheduleId.DataSource = UIScheduleHelper.GetUISchedules(CmsSessionHelper.CurrentCompanyId);
            this.ddlUIScheduleId.DataBind();

            this.ddlPriceScheduleId.DataSource = PriceScheduleHelper.GetPriceSchedules(CmsSessionHelper.CurrentCompanyId);
            this.ddlPriceScheduleId.DataBind();

            if (CmsSessionHelper.CurrentRole >= Role.Crave)
            {
                this.pnlServiceVersions.Visible = true;
                this.cbApiVersionV2.Checked = this.DataSourceAsDeliverypointgroupEntity.ApiVersion == 2;

                this.cbMessagingVersionV23.Enabled = true;
                this.cbMessagingVersionV23.Checked = this.DataSourceAsDeliverypointgroupEntity.MessagingVersion == 3;
            }

            this.cbClientConfigurationId.Enabled = !this.DataSourceAsDeliverypointgroupEntity.ClientConfigurationId.HasValue;

            deliverypointgroupAddress?.RenderAddress(DataSourceAsDeliverypointgroupEntity.AddressEntity);

            if(!CmsSessionHelper.CurrentUser.HasFeature(FeatureToggle.AllThingsLondon))
            {
                this.allThingslondon.Visible = false;
            }
        }

        private void SetPosdeliverypointgroupFilter()
        {
            PosdeliverypointgroupCollection posdeliverypointgroups = new PosdeliverypointgroupCollection();
            PredicateExpression posdeliverypointgroupFilter = new PredicateExpression(PosdeliverypointgroupFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            SortExpression posdeliverypointgroupSort = new SortExpression(PosdeliverypointgroupFields.Name | SortOperator.Ascending);
            posdeliverypointgroups.GetMulti(posdeliverypointgroupFilter, 0, posdeliverypointgroupSort);

            foreach (PosdeliverypointgroupEntity posdeliverypointgroup in posdeliverypointgroups)
            {
                if (!string.IsNullOrWhiteSpace(posdeliverypointgroup.RevenueCenter))
                {
                    this.ddlPosdeliverypointgroupId.Items.Add($"{posdeliverypointgroup.Name} ({posdeliverypointgroup.ExternalId}) [{posdeliverypointgroup.RevenueCenter}]", posdeliverypointgroup.PosdeliverypointgroupId);
                }
                else
                {
                    this.ddlPosdeliverypointgroupId.Items.Add($"{posdeliverypointgroup.Name} ({posdeliverypointgroup.ExternalId})", posdeliverypointgroup.PosdeliverypointgroupId);
                }
            }
        }

        private void PopulateCustomTexts()
        {
            CustomTextRenderer renderer = new CustomTextRenderer(this.DataSourceAsDeliverypointgroupEntity, CmsSessionHelper.DefaultCultureCodeForCompany);
            
            // Advanced tab
            renderer.RenderCustomText(this.tbDeliverypointCaption, CustomTextType.DeliverypointgroupDeliverypointCaption);

            // Mobile tab
            renderer.RenderCustomText(this.tbOrderProcessingNotificationTitle, CustomTextType.DeliverypointgroupOrderProcessingNotificationTitle);
            renderer.RenderCustomText(this.tbOrderProcessingNotification, CustomTextType.DeliverypointgroupOrderProcessingNotificationText);
            renderer.RenderCustomText(this.tbServiceProcessingNotificationTitle, CustomTextType.DeliverypointgroupServiceProcessingNotificationTitle);
            renderer.RenderCustomText(this.tbServiceProcessingNotification, CustomTextType.DeliverypointgroupServiceProcessingNotificationText);
            renderer.RenderCustomText(this.tbOrderProcessedNotificationTitle, CustomTextType.DeliverypointgroupOrderProcessedNotificationTitle);
            renderer.RenderCustomText(this.tbOrderProcessedNotification, CustomTextType.DeliverypointgroupOrderProcessedNotificationText);
            renderer.RenderCustomText(this.tbServiceProcessedNotificationTitle, CustomTextType.DeliverypointgroupServiceProcessedNotificationTitle);
            renderer.RenderCustomText(this.tbServiceProcessedNotification, CustomTextType.DeliverypointgroupServiceProcessedNotificationText);
            renderer.RenderCustomText(this.tbFreeformMessageTitle, CustomTextType.DeliverypointgroupFreeformMessageTitle);

            // Notifications tab
            renderer.RenderCustomText(this.tbOutOfChargeTitle, CustomTextType.DeliverypointgroupOutOfChargeTitle);
            renderer.RenderCustomText(this.tbOutOfChargeMessage, CustomTextType.DeliverypointgroupOutOfChargeText);
            renderer.RenderCustomText(this.tbProductExtraCommentTitle, CustomTextType.DeliverypointgroupProductExtraCommentTitle);
            renderer.RenderCustomText(this.tbProductExtraCommentText, CustomTextType.DeliverypointgroupProductExtraCommentText);
            renderer.RenderCustomText(this.tbOrderProcessedTitle, CustomTextType.DeliverypointgroupOrderProcessedTitle);
            renderer.RenderCustomText(this.tbOrderProcessedMessage, CustomTextType.DeliverypointgroupOrderProcessedText);
            renderer.RenderCustomText(this.tbOrderFailedTitle, CustomTextType.DeliverypointgroupOrderFailedTitle);
            renderer.RenderCustomText(this.tbOrderFailedMessage, CustomTextType.DeliverypointgroupOrderFailedText);
            renderer.RenderCustomText(this.tbOrderSavingTitle, CustomTextType.DeliverypointgroupOrderSavingTitle);
            renderer.RenderCustomText(this.tbOrderSavingMessage, CustomTextType.DeliverypointgroupOrderSavingText);
            renderer.RenderCustomText(this.tbOrderCompletedTitle, CustomTextType.DeliverypointgroupOrderCompletedTitle);
            renderer.RenderCustomText(this.tbOrderCompletedMessage, CustomTextType.DeliverypointgroupOrderCompletedText);
            renderer.RenderCustomText(this.tbServiceSavingTitle, CustomTextType.DeliverypointgroupServiceSavingTitle);
            renderer.RenderCustomText(this.tbServiceSavingMessage, CustomTextType.DeliverypointgroupServiceSavingText);
            renderer.RenderCustomText(this.tbServiceProcessedTitle, CustomTextType.DeliverypointgroupServiceProcessedTitle);
            renderer.RenderCustomText(this.tbServiceProcessedMessage, CustomTextType.DeliverypointgroupServiceProcessedText);
            renderer.RenderCustomText(this.tbServiceFailedTitle, CustomTextType.DeliverypointgroupServiceFailedTitle);
            renderer.RenderCustomText(this.tbServiceFailedMessage, CustomTextType.DeliverypointgroupServiceFailedText);
            renderer.RenderCustomText(this.tbRatingSavingTitle, CustomTextType.DeliverypointgroupRatingSavingTitle);
            renderer.RenderCustomText(this.tbRatingSavingMessage, CustomTextType.DeliverypointgroupRatingSavingText);
            renderer.RenderCustomText(this.tbRatingProcessedTitle, CustomTextType.DeliverypointgroupRatingProcessedTitle);
            renderer.RenderCustomText(this.tbRatingProcessedMessage, CustomTextType.DeliverypointgroupRatingProcessedText);
            renderer.RenderCustomText(this.tbOrderingNotAvailableMessage, CustomTextType.DeliverypointgroupOrderingNotAvailableText);
            renderer.RenderCustomText(this.tbPmsDeviceLockedTitle, CustomTextType.DeliverypointgroupPmsDeviceLockedTitle);
            renderer.RenderCustomText(this.tbPmsDeviceLockedText, CustomTextType.DeliverypointgroupPmsDeviceLockedText);
            renderer.RenderCustomText(this.tbPmsCheckinFailedTitle, CustomTextType.DeliverypointgroupPmsCheckinFailedTitle);
            renderer.RenderCustomText(this.tbPmsCheckinFailedText, CustomTextType.DeliverypointgroupPmsCheckinFailedText);
            renderer.RenderCustomText(this.tbPmsRestartTitle, CustomTextType.DeliverypointgroupPmsRestartTitle);
            renderer.RenderCustomText(this.tbPmsRestartText, CustomTextType.DeliverypointgroupPmsRestartText);
            renderer.RenderCustomText(this.tbPmsWelcomeTitle, CustomTextType.DeliverypointgroupPmsWelcomeTitle);
            renderer.RenderCustomText(this.tbPmsWelcomeText, CustomTextType.DeliverypointgroupPmsWelcomeText);
            renderer.RenderCustomText(this.tbPmsCheckoutCompleteTitle, CustomTextType.DeliverypointgroupPmsCheckoutCompleteTitle);
            renderer.RenderCustomText(this.tbPmsCheckoutCompleteText, CustomTextType.DeliverypointgroupPmsCheckoutCompleteText);
            renderer.RenderCustomText(this.tbPmsCheckoutApproveText, CustomTextType.DeliverypointgroupPmsCheckoutApproveText);
            renderer.RenderCustomText(this.tbChargerRemovedDialogTitle, CustomTextType.DeliverypointgroupChargerRemovedTitle);
            renderer.RenderCustomText(this.tbChargerRemovedDialogText, CustomTextType.DeliverypointgroupChargerRemovedText);
            renderer.RenderCustomText(this.tbTurnOffPrivacyTitle, CustomTextType.DeliverypointgroupTurnOffPrivacyTitle);
            renderer.RenderCustomText(this.tbTurnOffPrivacyText, CustomTextType.DeliverypointgroupTurnOffPrivacyText);
            renderer.RenderCustomText(this.tbItemCurrentlyUnavailableText, CustomTextType.DeliverypointgroupItemCurrentlyUnavailableText);
            renderer.RenderCustomText(this.tbAlarmSetWhileNotChargingTitle, CustomTextType.DeliverypointgroupAlarmSetWhileNotChargingTitle);
            renderer.RenderCustomText(this.tbAlarmSetWhileNotChargingText, CustomTextType.DeliverypointgroupAlarmSetWhileNotChargingText);
        }        

        private void SaveCustomTexts()
        {
            CustomTextRenderer renderer = new CustomTextRenderer(this.DataSourceAsDeliverypointgroupEntity, CmsSessionHelper.DefaultCultureCodeForCompany);

            // Advanced tab
            renderer.SaveCustomText(this.tbDeliverypointCaption, CustomTextType.DeliverypointgroupDeliverypointCaption);

            // Mobile tab
            renderer.SaveCustomText(this.tbOrderProcessingNotificationTitle, CustomTextType.DeliverypointgroupOrderProcessingNotificationTitle);
            renderer.SaveCustomText(this.tbOrderProcessingNotification, CustomTextType.DeliverypointgroupOrderProcessingNotificationText);
            renderer.SaveCustomText(this.tbServiceProcessingNotificationTitle, CustomTextType.DeliverypointgroupServiceProcessingNotificationTitle);
            renderer.SaveCustomText(this.tbServiceProcessingNotification, CustomTextType.DeliverypointgroupServiceProcessingNotificationText);
            renderer.SaveCustomText(this.tbOrderProcessedNotificationTitle, CustomTextType.DeliverypointgroupOrderProcessedNotificationTitle);
            renderer.SaveCustomText(this.tbOrderProcessedNotification, CustomTextType.DeliverypointgroupOrderProcessedNotificationText);
            renderer.SaveCustomText(this.tbServiceProcessedNotificationTitle, CustomTextType.DeliverypointgroupServiceProcessedNotificationTitle);
            renderer.SaveCustomText(this.tbServiceProcessedNotification, CustomTextType.DeliverypointgroupServiceProcessedNotificationText);
            renderer.SaveCustomText(this.tbFreeformMessageTitle, CustomTextType.DeliverypointgroupFreeformMessageTitle);

            // Notifications tab
            renderer.SaveCustomText(this.tbOutOfChargeTitle, CustomTextType.DeliverypointgroupOutOfChargeTitle);
            renderer.SaveCustomText(this.tbOutOfChargeMessage, CustomTextType.DeliverypointgroupOutOfChargeText);
            renderer.SaveCustomText(this.tbProductExtraCommentTitle, CustomTextType.DeliverypointgroupProductExtraCommentTitle);
            renderer.SaveCustomText(this.tbProductExtraCommentText, CustomTextType.DeliverypointgroupProductExtraCommentText);
            renderer.SaveCustomText(this.tbOrderProcessedTitle, CustomTextType.DeliverypointgroupOrderProcessedTitle);
            renderer.SaveCustomText(this.tbOrderProcessedMessage, CustomTextType.DeliverypointgroupOrderProcessedText);
            renderer.SaveCustomText(this.tbOrderFailedTitle, CustomTextType.DeliverypointgroupOrderFailedTitle);
            renderer.SaveCustomText(this.tbOrderFailedMessage, CustomTextType.DeliverypointgroupOrderFailedText);
            renderer.SaveCustomText(this.tbOrderSavingTitle, CustomTextType.DeliverypointgroupOrderSavingTitle);
            renderer.SaveCustomText(this.tbOrderSavingMessage, CustomTextType.DeliverypointgroupOrderSavingText);
            renderer.SaveCustomText(this.tbOrderCompletedTitle, CustomTextType.DeliverypointgroupOrderCompletedTitle);
            renderer.SaveCustomText(this.tbOrderCompletedMessage, CustomTextType.DeliverypointgroupOrderCompletedText);
            renderer.SaveCustomText(this.tbServiceSavingTitle, CustomTextType.DeliverypointgroupServiceSavingTitle);
            renderer.SaveCustomText(this.tbServiceSavingMessage, CustomTextType.DeliverypointgroupServiceSavingText);
            renderer.SaveCustomText(this.tbServiceProcessedTitle, CustomTextType.DeliverypointgroupServiceProcessedTitle);
            renderer.SaveCustomText(this.tbServiceProcessedMessage, CustomTextType.DeliverypointgroupServiceProcessedText);
            renderer.SaveCustomText(this.tbServiceFailedTitle, CustomTextType.DeliverypointgroupServiceFailedTitle);
            renderer.SaveCustomText(this.tbServiceFailedMessage, CustomTextType.DeliverypointgroupServiceFailedText);
            renderer.SaveCustomText(this.tbRatingSavingTitle, CustomTextType.DeliverypointgroupRatingSavingTitle);
            renderer.SaveCustomText(this.tbRatingSavingMessage, CustomTextType.DeliverypointgroupRatingSavingText);
            renderer.SaveCustomText(this.tbRatingProcessedTitle, CustomTextType.DeliverypointgroupRatingProcessedTitle);
            renderer.SaveCustomText(this.tbRatingProcessedMessage, CustomTextType.DeliverypointgroupRatingProcessedText);
            renderer.SaveCustomText(this.tbOrderingNotAvailableMessage, CustomTextType.DeliverypointgroupOrderingNotAvailableText);
            renderer.SaveCustomText(this.tbPmsDeviceLockedTitle, CustomTextType.DeliverypointgroupPmsDeviceLockedTitle);
            renderer.SaveCustomText(this.tbPmsDeviceLockedText, CustomTextType.DeliverypointgroupPmsDeviceLockedText);
            renderer.SaveCustomText(this.tbPmsCheckinFailedTitle, CustomTextType.DeliverypointgroupPmsCheckinFailedTitle);
            renderer.SaveCustomText(this.tbPmsCheckinFailedText, CustomTextType.DeliverypointgroupPmsCheckinFailedText);
            renderer.SaveCustomText(this.tbPmsRestartTitle, CustomTextType.DeliverypointgroupPmsRestartTitle);
            renderer.SaveCustomText(this.tbPmsRestartText, CustomTextType.DeliverypointgroupPmsRestartText);
            renderer.SaveCustomText(this.tbPmsWelcomeTitle, CustomTextType.DeliverypointgroupPmsWelcomeTitle);
            renderer.SaveCustomText(this.tbPmsWelcomeText, CustomTextType.DeliverypointgroupPmsWelcomeText);
            renderer.SaveCustomText(this.tbPmsCheckoutCompleteTitle, CustomTextType.DeliverypointgroupPmsCheckoutCompleteTitle);
            renderer.SaveCustomText(this.tbPmsCheckoutCompleteText, CustomTextType.DeliverypointgroupPmsCheckoutCompleteText);
            renderer.SaveCustomText(this.tbPmsCheckoutApproveText, CustomTextType.DeliverypointgroupPmsCheckoutApproveText);
            renderer.SaveCustomText(this.tbChargerRemovedDialogTitle, CustomTextType.DeliverypointgroupChargerRemovedTitle);
            renderer.SaveCustomText(this.tbChargerRemovedDialogText, CustomTextType.DeliverypointgroupChargerRemovedText);
            renderer.SaveCustomText(this.tbTurnOffPrivacyTitle, CustomTextType.DeliverypointgroupTurnOffPrivacyTitle);
            renderer.SaveCustomText(this.tbTurnOffPrivacyText, CustomTextType.DeliverypointgroupTurnOffPrivacyText);
            renderer.SaveCustomText(this.tbItemCurrentlyUnavailableText, CustomTextType.DeliverypointgroupItemCurrentlyUnavailableText);
            renderer.SaveCustomText(this.tbAlarmSetWhileNotChargingTitle, CustomTextType.DeliverypointgroupAlarmSetWhileNotChargingTitle);
            renderer.SaveCustomText(this.tbAlarmSetWhileNotChargingText, CustomTextType.DeliverypointgroupAlarmSetWhileNotChargingText);
        }
        
        private void PopulateThemes()
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(UIThemeFields.CompanyId == DBNull.Value);
            filter.AddWithOr(UIThemeFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

            SortExpression sort = new SortExpression();
            sort.Add(UIThemeFields.Name | SortOperator.Ascending);

            UIThemeCollection collection = new UIThemeCollection();
            collection.GetMulti(filter, 0, sort);

            this.cbUIThemeId.DataSource = collection;
            this.cbUIThemeId.DataBind();
        }

        private void PopulateServiceitems()
        {
            SortExpression sort = new SortExpression();
            sort.Add(ProductFields.SortOrder | SortOperator.Ascending);

            // Retrieve all service items for the company
            PredicateExpression filterProducts = new PredicateExpression();
            filterProducts.Add(ProductFields.Type == ProductType.Service);
            filterProducts.Add(ProductFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            filterProducts.Add(ProductFields.SortOrder != 9999);

            ProductCollection visibleProducts = new ProductCollection();
            visibleProducts.GetMulti(filterProducts, 0, sort);

            EntityView<ProductEntity> productView = visibleProducts.DefaultView;
            EntityView<DeliverypointgroupProductEntity> serviceitemView = this.DataSourceAsDeliverypointgroupEntity.DeliverypointgroupProductCollection.DefaultView;
            serviceitemView.Filter = new PredicateExpression(DeliverypointgroupProductFields.Type == ProductType.Service);

            string ddlIdPrefix = "ddlServiceProduct";

            // Populate all the serviceitems
            foreach (Control control in this.ControlList)
            {
                if (!control.ID.IsNullOrWhiteSpace() && control.ID.StartsWith(ddlIdPrefix))
                {
                    productView.Filter = null;
                    serviceitemView.Filter = new PredicateExpression(DeliverypointgroupProductFields.Type == ProductType.Service);
                    ComboBoxInt ddl = (ComboBoxInt)control;
                    ddl.DisplayEmptyItem = true;
                    ddl.EmptyItemText = "None - Select ...";
                    ddl.DataSource = visibleProducts;
                    ddl.DataBind();

                    ddl.SelectedIndex = -1;

                    int sortOrder = Convert.ToInt32(ddl.ID.GetAllAfterFirstOccurenceOf(ddlIdPrefix));

                    // Check if the deliverypoint has this serviceitem
                    serviceitemView.Filter = new PredicateExpression(DeliverypointgroupProductFields.SortOrder == sortOrder);

                    if (serviceitemView.Count == 1)
                    {
                        ddl.Value = serviceitemView[0].ProductId;
                    }
                }
            }
        }

        public DateTime GetTimeFromString(string time)
        {
            int hours = Convert.ToInt32(time.Substring(0, 2));
            int minutes = Convert.ToInt32(time.Substring(2, 2));
            return new DateTime(2000, 1, 1, hours, minutes, 0);
        }

        public override void Validate()
        {
            if (this.DataSourceAsDeliverypointgroupEntity.AvailableOnObymobi) // Mobile ordering
            {
                if (!this.DataSourceAsDeliverypointgroupEntity.MobileUIModeId.HasValue)
                    this.MultiValidatorDefault.AddError("A mobile UI mode is required when the Crave mobile setting is enabled.");
                if (!this.DataSourceAsDeliverypointgroupEntity.TabletUIModeId.HasValue)
                    this.MultiValidatorDefault.AddError("A tablet UI mode is required when the Crave mobile setting is enabled.");
            }

            CompanyEntity companyEntity = new CompanyEntity(CmsSessionHelper.CurrentCompanyId);
            if (companyEntity.AvailableOnOtoucho && !this.DataSourceAsDeliverypointgroupEntity.UIModeId.HasValue)
                this.MultiValidatorDefault.AddError("A tablet UI mode is required when the hotel uses Crave in-room tablets.");

            if (this.DataSourceAsDeliverypointgroupEntity.Pincode.Length < 4)
                this.MultiValidatorDefault.AddError("Pincode has to be at least 4 characters long.");
            if (this.DataSourceAsDeliverypointgroupEntity.PincodeSU.Length < 4)
                this.MultiValidatorDefault.AddError("Pincode superuser has to be at least 4 characters long.");
            if (this.DataSourceAsDeliverypointgroupEntity.PincodeGM.Length < 4)
                this.MultiValidatorDefault.AddError("Pincode godmode has to be at least 4 characters long.");

            base.Validate();
        }

        public override bool Save()
        {
            if (this.PageMode == PageMode.Add)
            {
                this.DataSourceAsDeliverypointgroupEntity.CompanyId = CmsSessionHelper.CurrentCompanyId;
            }

            this.DataSourceAsDeliverypointgroupEntity.DailyOrderReset = this.teDailyOrderReset.Value.GetValueOrDefault().ToString("HHmm");
            this.DataSourceAsDeliverypointgroupEntity.SleepTime = this.teSleepTime.Value.GetValueOrDefault().ToString("HHmm");
            this.DataSourceAsDeliverypointgroupEntity.WakeUpTime = this.teWakeUpTime.Value.GetValueOrDefault().ToString("HHmm");

            if (!string.IsNullOrWhiteSpace(this.ddlGooglePrinterId.Value))
            {
                this.DataSourceAsDeliverypointgroupEntity.GooglePrinterName = this.ddlGooglePrinterId.Text;
            }
            else
            {
                this.DataSourceAsDeliverypointgroupEntity.GooglePrinterName = string.Empty;
            }

            if (CmsSessionHelper.CurrentRole >= Role.Reseller)
            {
                this.DataSourceAsDeliverypointgroupEntity.Pincode = this.tbPincode.Value.TrimToLength(25, false);
                this.DataSourceAsDeliverypointgroupEntity.PincodeSU = this.tbPincodeSU.Value.TrimToLength(25, false);
                this.DataSourceAsDeliverypointgroupEntity.PincodeGM = this.tbPincodeGM.Value.TrimToLength(25, false);
            }

            if (CmsSessionHelper.CurrentRole >= Role.Crave)
            {
                this.DataSourceAsDeliverypointgroupEntity.ApiVersion = this.cbApiVersionV2.Checked ? 2 : 1;
                this.DataSourceAsDeliverypointgroupEntity.MessagingVersion = this.cbMessagingVersionV23.Checked ? 3 : 1;
            }

            deliverypointgroupAddress?.SetAddress(DataSourceAsDeliverypointgroupEntity.AddressEntity);

            bool deliveryTimeChanged = DataSourceAsDeliverypointgroupEntity.Fields[(int)DeliverypointgroupFieldIndex.EstimatedDeliveryTime].IsChanged;
            if (base.Save(true))
            {
                if (deliveryTimeChanged)
                {
                    this.UpdateDeliverytimeCache();
                }

                this.SaveServiceitems();
                this.SaveCustomTexts();                                        
                return true;
            }

            return false;
        }

        private void SaveServiceitems()
        {
            EntityView<DeliverypointgroupProductEntity> serviceitemView = this.DataSourceAsDeliverypointgroupEntity.DeliverypointgroupProductCollection.DefaultView;
            serviceitemView.Filter = new PredicateExpression(DeliverypointgroupProductFields.Type == ProductType.Service);

            string ddlIdPrefix = "ddlServiceProduct";

            foreach (Control control in this.ControlList)
            {
                if (!control.ID.IsNullOrWhiteSpace() && control.ID.StartsWith(ddlIdPrefix))
                {
                    ComboBoxInt ddl = (ComboBoxInt)control;

                    int sortOrder = Convert.ToInt32(ddl.ID.GetAllAfterFirstOccurenceOf(ddlIdPrefix));
                    PredicateExpression filter = new PredicateExpression(DeliverypointgroupProductFields.SortOrder == sortOrder);
                    filter.Add(DeliverypointgroupProductFields.Type == ProductType.Service);

                    serviceitemView.Filter = filter;

                    if (ddl.ValidId > 0)
                    {
                        // Update / Add
                        DeliverypointgroupProductEntity dpgsi;
                        if (serviceitemView.Count == 0)
                        {
                            dpgsi = new DeliverypointgroupProductEntity();
                            dpgsi.DeliverypointgroupId = this.DataSourceAsDeliverypointgroupEntity.DeliverypointgroupId;
                            dpgsi.SortOrder = sortOrder;
                        }
                        else
                        {
                            dpgsi = serviceitemView[0];
                        }

                        dpgsi.ProductId = ddl.ValidId;
                        dpgsi.Save();
                    }
                    else
                    {
                        // Delete if any
                        if (serviceitemView.Count == 1)
                        {
                            serviceitemView[0].AddToTransaction(this.DataSourceAsDeliverypointgroupEntity);
                            serviceitemView[0].Delete();
                            serviceitemView.RelatedCollection.Remove(serviceitemView[0]);
                        }
                    }
                }
            }
        }

        private void UpdateDeliverytimeCache()
        {
            if (RedisCacheHelper.Instance.RedisEnabled)
            {
                string cacheKey = "GetEstimatedDeliveryTime-" + this.DataSourceAsDeliverypointgroupEntity.DeliverypointgroupId;
                RedisCacheHelper.Instance.Add(cacheKey, this.DataSourceAsDeliverypointgroupEntity.EstimatedDeliveryTime);

                cacheKey = "GetEstimatedDeliveryTimes-" + this.DataSourceAsDeliverypointgroupEntity.CompanyId;
                DeliveryTime[] deliveryTimes = Obymobi.Logic.HelperClasses.DeliveryTimeHelper.GetDeliveryTimesForCompany(this.DataSourceAsDeliverypointgroupEntity.CompanyId).ToArray();
                RedisCacheHelper.Instance.Add(cacheKey, deliveryTimes);
            }

            Obymobi.Logic.HelperClasses.CloudTaskHelper.UploadDeliveryTime(this.DataSourceAsDeliverypointgroupEntity.DeliverypointgroupId, this.DataSourceAsDeliverypointgroupEntity.EstimatedDeliveryTime);
        }

        private void SetWarnings()
        {
            DeliverypointgroupEntity deliverypoint = this.DataSourceAsDeliverypointgroupEntity;

            // Screen timeout
            if (deliverypoint.ScreenTimeoutInterval == 0)
            {
                this.AddInformator(InformatorType.Warning, this.Translate("ScreenTimeoutIntervalNotSpecified", "Er is geen screen time out ingegeven."));
            }

            // Reset timeout
            if (deliverypoint.ResetTimeout == 0)
            {
                this.AddInformator(InformatorType.Warning, this.Translate("ResetTimeoutNotSpecified", "Er is geen reset time out ingegeven."));
            }

            // Pincodes
            string pincode = deliverypoint.Pincode;
            string pincodeSU = deliverypoint.PincodeSU;
            string pincodeGM = deliverypoint.PincodeGM;

            if (pincode.IsNullOrWhiteSpace())
            {
                this.AddInformator(InformatorType.Warning, this.Translate("PincodeNotSpecified", "Er is geen standaard pincode ingegeven."));
            }
            if (pincodeSU.IsNullOrWhiteSpace())
            {
                this.AddInformator(InformatorType.Warning, this.Translate("PincodeSUNotSpecified", "Er is geen super user pincode ingegeven."));
            }
            if (pincodeGM.IsNullOrWhiteSpace())
            {
                this.AddInformator(InformatorType.Warning, this.Translate("PincodeGMNotSpecified", "Er is geen god mode pincode ingegeven."));
            }

            if (pincode.Length < 6 || pincodeSU.Length < 6 || pincodeGM.Length < 6)
            {
                this.AddInformator(InformatorType.Warning, this.Translate("PincodeLengthWarning", "Recommended minimum pincode length is 6 characters."));
            }

            if (!pincode.IsNullOrWhiteSpace() && pincode.Equals(pincodeSU, StringComparison.InvariantCultureIgnoreCase))
            {
                this.AddInformator(InformatorType.Warning, this.Translate("WarningSamePincodeNormalSU", "De super user pincode is hetzelfde als de standaard pincode."));
            }
            if (!pincode.IsNullOrWhiteSpace() && !pincodeGM.IsNullOrWhiteSpace() && pincode.Equals(pincodeGM, StringComparison.InvariantCultureIgnoreCase))
            {
                this.AddInformator(InformatorType.Warning, this.Translate("WarningSamePincodeNormalGM", "De god mode pincode is hetzelfde als de standaard pincode."));
            }
            if (!pincodeGM.IsNullOrWhiteSpace() && !pincodeSU.IsNullOrWhiteSpace() && pincodeGM.Equals(pincodeSU, StringComparison.InvariantCultureIgnoreCase))
            {
                this.AddInformator(InformatorType.Warning, this.Translate("WarningSamePincodeSUGM", "De god mode pincode is hetzelfde als de super user pincode."));
            }

            // Locale
            if (deliverypoint.Locale.IsNullOrWhiteSpace())
            {
                this.AddInformator(InformatorType.Warning, this.Translate("LocaleNotSpecified", "Er is geen locale geselecteerd."));
            }

            // Route
            if (deliverypoint.RouteId == null || deliverypoint.RouteId == 0)
            {
                this.AddInformator(InformatorType.Warning, this.Translate("RouteNotSpecified", "Er is geen route gekozen."));
            }

            // Menu
            if (deliverypoint.MenuId == null || deliverypoint.MenuId == 0)
            {
                this.AddInformator(InformatorType.Warning, this.Translate("MenuNotSpecified", "Er is geen menu gekozen."));
            }            

            // Roomservice charge text
            if (deliverypoint.RoomserviceCharge.HasValue && deliverypoint.RoomserviceCharge.Value > 0 && deliverypoint.CustomTextCollection.Any(x => x.Type == CustomTextType.DeliverypointgroupRoomserviceChargeText))
            {
                this.AddInformator(InformatorType.Warning, this.Translate("RoomserviceChargeSpecifiedTwice", "Er is een bedrag én een tekst ingegeven voor de bezorgkosten, het bedrag zal worden getoond."));
            }

            if (deliverypoint.PowerSaveLevel > 0 && deliverypoint.PowerSaveLevel < 20)
            {
                this.AddInformator(InformatorType.Warning, "Power save brightness higher than 0 and lower than 20 is not recommended");
            }

            if (((deliverypoint.DimLevelBright > 0 && deliverypoint.DimLevelBright < 20) || (deliverypoint.DockedDimLevelBright > 0 && deliverypoint.DockedDimLevelBright < 20)) ||
                ((deliverypoint.DimLevelMedium > 0 && deliverypoint.DimLevelMedium < 20) || (deliverypoint.DockedDimLevelMedium > 0 && deliverypoint.DockedDimLevelMedium < 20)) ||
                ((deliverypoint.DimLevelDull > 0 && deliverypoint.DimLevelDull < 20) || (deliverypoint.DockedDimLevelDull > 0 && deliverypoint.DockedDimLevelDull < 20)))
            {
                this.AddInformator(InformatorType.Warning, "Screen brightness higher than 0 and lower than 20 is not recommended");
            }

            this.Validate();
        }

        private void HookUpEvents()
        {
            this.btnCopyDeliverypointgroup.Click += this.btnCopyDeliverypointgroup_Click;
        }

        private void CopyDeliverypointgroup()
        {
            try
            {
                if (this.DataSourceAsDeliverypointgroupEntity.IsNew)
                {
                    this.MultiValidatorDefault.AddError("The deliverypointgroup has to be saved first before creating a copy.");
                }
                else if (this.tbNewDeliverypointgroupNameName.Text.IsNullOrWhiteSpace())
                {
                    this.MultiValidatorDefault.AddError("The deliverypointgroup needs to have a name.");
                }
                else
                {
                    CopyDeliverypointgroupRequest copyDeliverypointgroupRequest = new CopyDeliverypointgroupRequest
                    {
                        DeliverypointgroupIdToCopy = this.DataSourceAsDeliverypointgroupEntity.DeliverypointgroupId,
                        NewDeliverypointgroupName = this.tbNewDeliverypointgroupNameName.Text,
                        CopyMediaOnCdn = MediaCloudHelper.CopyMediaOnCdn
                    };
                    DeliverypointgroupEntity newDeliverypointgroupEntity = new CopyDeliverypointgroupUseCase().Execute(copyDeliverypointgroupRequest);
                    if (newDeliverypointgroupEntity != null)
                    {


                        this.AddInformatorInfo(this.Translate("DeliverypointgroupCreated", "De deliverypointgroup is aangemaakt: ") + "<a href=\"{0}\">{1}</a>".FormatSafe(this.ResolveUrl("~/Company/Deliverypointgroup.aspx?id={0}".FormatSafe(newDeliverypointgroupEntity.DeliverypointgroupId)), newDeliverypointgroupEntity.Name));
                    }
                    else
                    {
                        throw new Exception(this.Translate("Error.UnableToCopyDpg", "Er is een fout opgetreden tijdens het kopieëren van de deliverypointgroup."));
                    }
                }
            }
            catch (Exception ex)
            {
                this.MultiValidatorDefault.AddError(this.Translate("Error.ErrorDuringDpgCopy", "Er is een fout opgetreden tijdens het kopieëren van de deliverypointgroup: ") + ex.Message);
            }

            this.Validate();
        }

        public override bool InitializeEntity()
        {
            if (this.EntityId > 0)
            {
                PrefetchPath prefetch = new PrefetchPath(EntityType.DeliverypointgroupEntity);
                prefetch.Add(DeliverypointgroupEntityBase.PrefetchPathCustomTextCollection);
                this.DataSource = new DeliverypointgroupEntity(this.EntityId, prefetch);
            }

            return base.InitializeEntity();
        }

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            this.DataSourceLoaded += this.Deliverypoint_DataSourceLoaded;
            base.OnInit(e);
        }

        private void btSelectPrinter_Click(object sender, EventArgs e)
        {
            this.ViewState["select_printer"] = true;
        }

        private void btRefreshPrinters_Click(object sender, EventArgs e)
        {
            this.ViewState["refresh_printers"] = true;
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            try
            {
                bool selectPrinter = (bool?)this.ViewState["select_printer"] ?? false;
                bool refreshPrinters = (bool?)this.ViewState["refresh_printers"] ?? false;

                string cacheKey = string.Format("Printers.{0}", CmsSessionHelper.CurrentCompanyId);

                Printer[] printers;
                if (refreshPrinters || (!CacheHelper.TryGetValue(cacheKey, false, out printers) && selectPrinter))
                {
                    printers = GoogleAPI.GetPrinters(CmsSessionHelper.CurrentCompanyId);
                    if (printers != null)
                    {
                        CacheHelper.AddSlidingExpire(false, cacheKey, printers, 30);
                    }
                    else
                    {
                        this.MultiValidatorDefault.AddWarning(strings.Company_Deliverypointgroup_No_printer_can_be_selected);
                        this.Validate();
                    }
                }

                if (printers != null)
                {
                    DataTable data = new DataTable();
                    data.Columns.Add("Name");
                    data.Columns.Add("PrinterId");

                    foreach (Printer printer in printers)
                    {
                        DataRow row = data.NewRow();
                        row["Name"] = printer.displayName;
                        row["PrinterId"] = printer.id;
                        data.Rows.Add(row);
                    }

                    this.ddlGooglePrinterId.UseDataBinding = true;
                    this.ddlGooglePrinterId.DataSource = data;
                    this.ddlGooglePrinterId.DataBind();

                    this.ddlGooglePrinterId.Value = this.DataSourceAsDeliverypointgroupEntity.GooglePrinterId;

                    this.ddlGooglePrinterId.Enabled = true;

                    this.btSelectPrinter.Visible = false;
                    this.btRefreshPrinters.Visible = true;
                }
                else if (!selectPrinter)
                {
                    if (!string.IsNullOrWhiteSpace(this.DataSourceAsDeliverypointgroupEntity.GooglePrinterName))
                    {
                        this.ddlGooglePrinterId.NullText = this.DataSourceAsDeliverypointgroupEntity.GooglePrinterName;
                    }
                    else
                    {
                        this.ddlGooglePrinterId.NullText = this.Translate("google_printer_none", "None");
                    }

                    this.ddlGooglePrinterId.UseDataBinding = false;
                    this.ddlGooglePrinterId.Enabled = false;

                    this.btSelectPrinter.Visible = true;
                    this.btRefreshPrinters.Visible = false;
                }
            }
            catch (Exception ex)
            {
                // When something goes wrong here, too bad! Unless you're a developer, then you're gonna have a bad time.
                // Unless you are Mr G or Mr D.
                if (TestUtil.IsPcDeveloper && !TestUtil.IsPcGabriel && !TestUtil.IsPcBattleStationDanny)
                {
                    throw ex;
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.HookUpEvents();

            if (this.PageMode == PageMode.Add && !this.IsPostBack)
            {
                this.SetDefaults();
            }

            if (!this.IsPostBack && !this.DataSourceAsDeliverypointgroupEntity.IsNew)
            {
                this.SetWarnings();
            }
        }

        private void Deliverypoint_DataSourceLoaded(object sender)
        {
            this.SetFilters();
            this.SetGui();
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (this.deliverypointsPanel != null)
            {
                this.deliverypointsPanel.ParentLoadCompleted();
            }
        }

        private void btnCopyDeliverypointgroup_Click(object sender, EventArgs e)
        {
            this.CopyDeliverypointgroup();
        }
    }
}