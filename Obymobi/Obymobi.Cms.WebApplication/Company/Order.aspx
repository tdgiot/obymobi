﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.Order" Title="Order" Codebehind="Order.aspx.cs" %>
<%@ Register Assembly="DevExpress.XtraCharts.v20.1.Web" Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>
<%@ Register Assembly="DevExpress.XtraCharts.v20.1" Namespace="DevExpress.XtraCharts" TagPrefix="dxcharts" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitleHolder" Runat="Server">
    <D:Label runat="server" id="lblTitle">Bestelling</D:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhToolbarHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhContentHolder" Runat="Server">
    <div>
        <X:PageControl ID="tabsMain" runat="server" Width="100%">
            <TabPages>
                <X:TabPage Text="Algemeen" Name="Generic">
                    <controls>
					    <table class="dataformV2">
                            <tr>
                                <td class="label">
                                    <D:Label ID="lblOrderIdCaption" runat="server">Order Id</D:Label>
                                </td>
                                <td class="control">
                                    <D:Label ID="lblOrderId" runat="server" LocalizeText="false" CssClass="lblOrderValue"></D:Label>
                                </td>
                                <td class="label">
                                    <D:Label ID="lblStatusCaption" runat="server">Status</D:Label>
                                </td>
                                <td class="control">
                                    <D:Label ID="lblStatus" runat="server" LocalizeText="false" CssClass="lblOrderValue"></D:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:Label ID="lblPriceCaption" runat="server">Totaal</D:Label>
                                </td>
                                <td class="control">
                                    <D:Label ID="lblPrice" runat="server" LocalizeText="false" CssClass="lblOrderValue"></D:Label>
                                </td>
                                <td class="label">
                                    <D:Label ID="lblStatusTextCaption" runat="server">Status tekst</D:Label>
                                </td>
                                <td class="control">
                                    <D:Label ID="lblStatusText" runat="server" LocalizeText="false" CssClass="lblOrderValue"></D:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:Label ID="lblTimePlacedCaption" runat="server">Tijd order geplaatst</D:Label>
                                </td>
                                <td class="control">
                                    <D:Label ID="lblTimePlaced" runat="server" LocalizeText="false" CssClass="lblOrderValue"></D:Label>
                                </td>
                                <td class="label">
                                    <D:Label ID="lblDeliverypointCaption" runat="server">Tafelnummer</D:Label>
                                </td>
                                <td class="control">
                                    <D:Label ID="lblDeliverypoint" runat="server" LocalizeText="false" CssClass="lblOrderValue"></D:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:Label ID="lblTimeProcessedCaption" runat="server">Tijd order verwerkt</D:Label>
                                </td>
                                <td class="control">
                                    <D:Label ID="lblTimeProcessed" runat="server" LocalizeText="false" CssClass="lblOrderValue"></D:Label>
                                </td>
                                <td class="label">
                                    <D:Label ID="lblDeliverypointgroupCaption" runat="server">Tafelgroep</D:Label>
                                </td>
                                <td class="control">
                                    <D:Label ID="lblDeliverypointgroup" runat="server" LocalizeText="false" CssClass="lblOrderValue"></D:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
		                            <D:Label ID="lblNotes" runat="server">Opmerking</D:Label>
	                            </td>
		                        <td colspan="3" class="control">
			                        <D:TextBox ID="tbNotes" runat="server" Rows="5" TextMode="MultiLine" CssClass="inputNoHeight" LocalizeText="false" Enabled="false"></D:TextBox>                
		                        </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:Label ID="lblOrderitemsCaption" runat="server">Orderitems</D:Label>
                                </td>
                                <td colspan="3" class="control">
                                    <X:GridView ID="orderitemsGrid" ClientInstanceName="orderitemsGrid" OnCustomCallback="Orderitem_Clicked" runat="server" Width="100%" KeyFieldName="OrderitemId">
                                        <SettingsBehavior AutoFilterRowInputDelay="350" />                            
                                        <SettingsPager PageSize="20"></SettingsPager>
                                        <SettingsBehavior AllowGroup="false" AllowDragDrop="false" />
                                        <ClientSideEvents RowDblClick="
                                            function(s, e){
                                                e.cancel = true;
                                                var key = s.GetRowKey(e.visibleIndex);
                                                orderitemsGrid.PerformCallback(key);
                                            } " />
                                        <Columns>
                                            <dxwgv:GridViewDataColumn FieldName="OrderitemId" VisibleIndex="0" Width="5%">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="Product" VisibleIndex="1">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="Quantity" VisibleIndex="2">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="Price" VisibleIndex="3">
                                            </dxwgv:GridViewDataColumn>
                                        </Columns>
                                    </X:GridView>  
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:Label ID="lblRoutestephandlersCaption" runat="server">Routestephandlers</D:Label>
                                </td>
                                <td colspan="3" class="control">
                                    <D:PlaceHolder ID="PlaceHolder1" runat="server">
                                        <table class="dataformV2">
                                            <X:GridView ID="stepsGrid" ClientInstanceName="stepsGrid" runat="server" Width="100%">
                                                <SettingsBehavior AutoFilterRowInputDelay="350" />                            
                                                <SettingsPager PageSize="20"></SettingsPager>
                                                <SettingsBehavior AllowGroup="false" AllowDragDrop="false" />
                                                <Columns>
                                                    <dxwgv:GridViewDataColumn FieldName="OrderRoutestephandlerId" VisibleIndex="0" Width="5%">
                                                    </dxwgv:GridViewDataColumn>
                                                    <dxwgv:GridViewDataColumn FieldName="Terminal" VisibleIndex="1" Width="5%">
                                                    </dxwgv:GridViewDataColumn>
                                                    <dxwgv:GridViewDataColumn FieldName="Number" VisibleIndex="2" Width="5%">
                                                    </dxwgv:GridViewDataColumn>
                                                    <dxwgv:GridViewDataColumn FieldName="HandlerType" VisibleIndex="3" Width="5%">
                                                    </dxwgv:GridViewDataColumn>
                                                    <dxwgv:GridViewDataColumn FieldName="Status" VisibleIndex="4" Width="5%">
                                                    </dxwgv:GridViewDataColumn>
                                                    <dxwgv:GridViewDataColumn FieldName="ErrorCode" VisibleIndex="5" Width="5%">
                                                    </dxwgv:GridViewDataColumn>
                                                    <dxwgv:GridViewDataColumn FieldName="ErrorText" VisibleIndex="6" Width="10%">
                                                    </dxwgv:GridViewDataColumn>
                                                </Columns>
                                            </X:GridView>  
                                        </table>
                                    </D:PlaceHolder>
                                </td>
                            </tr>
                        </table>
				    </controls>
                </X:TabPage>
            </TabPages>
        </X:PageControl>
    </div>
</asp:Content>
