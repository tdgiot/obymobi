﻿<%@ Page Title="Print table numbers" Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.PrintDeliverypointNumbers" Codebehind="PrintDeliverypointNumbers.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitleHolder" Runat="Server"><D:Label runat="server" id="lblTitle">Tafelnummers afdrukken</D:Label></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhToolbarHolder" Runat="Server">
    <D:Button runat="server" ID="btnGenerateDeliverypointNumbers" Text="Tafelnummers genereren" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhContentHolder" Runat="Server">
  <div>
    <X:PageControl ID="tabsMain" runat="server" Width="100%">
      <TabPages>
        <X:TabPage Text="Algemeen" Name="Generic">
          <controls>
            <D:PlaceHolder runat="server">
	            <table class="dataformV2">
		            <tr>
			            <td class="label">
				            <D:Label runat="server" ID="lblDateFrom">Datum van</D:Label>
			            </td>
			            <td class="control">
				            <X:DateEdit runat="server" ID="deTableNumbersFromDate"></X:DateEdit>
			            </td>							
			            <td class="label">
				            <D:Label runat="server" ID="lblDateTo">Datum tot</D:Label>
			            </td>
			            <td class="control">
				            <X:DateEdit runat="server" ID="deTableNumbersToDate"></X:DateEdit>
			            </td>							
		            </tr>
                </table>
            </D:PlaceHolder>
          </controls>
        </X:TabPage>
      </TabPages>
    </X:PageControl>
  </div>

</asp:Content>

