using System;
using Obymobi.Enums;
using Obymobi.Logic.Cms;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;

namespace Obymobi.ObymobiCms.Company
{
    public partial class DeliverypointgroupAdvertisement : Dionysos.Web.UI.PageLLBLGenEntity
    {
        #region EventHandlers

        protected override void OnInit(EventArgs e)
        {
            this.DataSourceLoaded += DeliverypointgroupAdvertisement_DataSourceLoaded;
            base.OnInit(e);
        }

        private void DeliverypointgroupAdvertisement_DataSourceLoaded(object sender)
        {
            Response.Redirect("~/Advertising/Advertisement.aspx?id=" + ((DeliverypointgroupAdvertisementEntity)this.DataSource).AdvertisementId);
            //this.SetGui();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        #endregion

        #region Methods

        private void SetGui()
        {
            // Init the advertisements
            AdvertisementCollection advertisementCollection = new AdvertisementCollection();
            IncludeFieldsList advertisementFields = new IncludeFieldsList(AdvertisementFields.Name);
            SortExpression advertisementSort = new SortExpression(AdvertisementFields.Name | SortOperator.Ascending);
            PredicateExpression advertisementFilter = new PredicateExpression(AdvertisementFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            advertisementFilter.AddWithOr(AdvertisementFields.CompanyId == DBNull.Value);
            advertisementCollection.GetMulti(advertisementFilter, 0, advertisementSort, null, null, advertisementFields, 0, 0);
            this.ddlAdvertisementId.BindEntityCollection(advertisementCollection, AdvertisementFields.Name, AdvertisementFields.AdvertisementId);

            // Init the deliverypointgroups
            DeliverypointgroupCollection deliverypointgroupCollection = new DeliverypointgroupCollection();
            IncludeFieldsList deliverypointgroupFields = new IncludeFieldsList();
            deliverypointgroupFields.Add(DeliverypointgroupFields.Name);
            SortExpression deliverypointgroupSort = new SortExpression(DeliverypointgroupFields.Name | SortOperator.Ascending);
            PredicateExpression deliverypointgroupFilter = new PredicateExpression(DeliverypointgroupFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            deliverypointgroupCollection.GetMulti(deliverypointgroupFilter, 0, deliverypointgroupSort, null, null, deliverypointgroupFields, 0, 0);
            this.ddlDeliverypointgroupId.BindEntityCollection(deliverypointgroupCollection, DeliverypointgroupFields.Name, DeliverypointgroupFields.DeliverypointgroupId);
        }

        #endregion
    }
}
