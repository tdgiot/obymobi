﻿using System;
using System.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Web.Google;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Company
{
    public partial class Deliverypoint : Dionysos.Web.UI.PageLLBLGenEntity
    {
        #region Methods

        protected override void OnInit(EventArgs e)
		{
		    this.LoadUserControls();
			this.DataSourceLoaded += Deliverypoint_DataSourceLoaded;
			base.OnInit(e);			
		}

	    private void LoadUserControls()
	    {
	        if (CmsSessionHelper.CurrentCompanyId > 0)
	        {
	            this.LoadRoomControlConfigurations();
	        }

            this.cbClientConfigurationId.DataBindEntityCollection<ClientConfigurationEntity>(ClientConfigurationFields.CompanyId == CmsSessionHelper.CurrentCompanyId, ClientConfigurationFields.Name, ClientConfigurationFields.Name);

            if (CmsSessionHelper.CurrentRole >= Role.Reseller)
            {
                this.tabsMain.AddTabPage("External deliverypoints", "DeliverypointExternalDeliverypointCollection", "~/Company/SubPanels/DeliverypointExternalDeliverypointCollection.ascx");
            }
        }

	    private void LoadRoomControlConfigurations()
	    {
            var sort = new SortExpression(RoomControlConfigurationFields.Name | SortOperator.Ascending);
            var filter = new PredicateExpression(RoomControlConfigurationFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

            var roomControlConfigurations = new RoomControlConfigurationCollection();
            roomControlConfigurations.GetMulti(filter, 0, sort);

            this.ddlRoomControlConfigurationId.DataSource = roomControlConfigurations;
            this.ddlRoomControlConfigurationId.DataBind();
	    }

	    private void SetGui()
		{
            // Deliverypointgroups
			DeliverypointgroupCollection dpgs = new DeliverypointgroupCollection();
			PredicateExpression dgpFilter = new PredicateExpression();
			dgpFilter.Add(DeliverypointgroupFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
			SortExpression sort = new SortExpression(DeliverypointgroupFields.Name | SortOperator.Ascending);
			dpgs.GetMulti(dgpFilter, 0, sort);            

			this.ddlDeliverypointgroupId.DataSource = dpgs;
            this.ddlDeliverypointgroupId.DataBind();

            SetPosdeliverypointFilter();

            // Devices to print from
		    var deviceFilter = new PredicateExpression(ClientFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
		    var deviceRelations = new RelationCollection();
		    deviceRelations.Add(DeviceEntityBase.Relations.ClientEntityUsingDeviceId);
		    var deviceSort = new SortExpression(new SortClause(DeviceFields.Identifier, SortOperator.Ascending));
		    var devices = new DeviceCollection();
            devices.GetMulti(deviceFilter, 0, deviceSort, deviceRelations);

		    this.ddlDeviceId.DataSource = devices;
            this.ddlDeviceId.DataBind();

            if (CmsSessionHelper.CurrentRole < Role.Administrator)
            {
                this.tabsMain.TabPages.FindByName("tabAdvanced").Visible = false;
            }

            if (this.DataSourceAsDeliverypointEntity.HasClient)
            {
                this.AddInformatorInfo(this.Translate("CantChangeDeliverypointgroupBecauseItHasClients", "De Afleverpuntgroep kan niet worden gewijzigd omdat er al Clients zijn ingesteld voor dit Afleverpunt"));                                
                this.ddlDeliverypointgroupId.Enabled = false;
            }

            if (DataSourceAsDeliverypointEntity.RoomControlConfigurationId.HasValue)
            {
	            DataBindRoomControlAreas(DataSourceAsDeliverypointEntity.RoomControlConfigurationId.Value);
            }
		}

	    private void DataBindRoomControlAreas(int roomControlConfigurationId)
	    {
		    SortExpression sort = new SortExpression(RoomControlAreaFields.Name | SortOperator.Ascending);
		    PredicateExpression filter = new PredicateExpression(RoomControlAreaFields.RoomControlConfigurationId == roomControlConfigurationId);
		    filter.Add(RoomControlAreaFields.Visible == true);

		    RoomControlAreaCollection roomControlAreas = new RoomControlAreaCollection();
		    roomControlAreas.GetMulti(filter, 0, sort);

		    ddlRoomControlAreaId.DataSource = roomControlAreas;
		    ddlRoomControlAreaId.DataBind();
        }

        private void SetPosdeliverypointFilter()
        {
            PosdeliverypointCollection posdeliverypoints = new PosdeliverypointCollection();
            PredicateExpression posdeliverypointFilter = new PredicateExpression(PosdeliverypointFields.CompanyId == this.DataSourceAsDeliverypointEntity.CompanyId);
            SortExpression posdeliverypointSort = new SortExpression(PosdeliverypointFields.Name | SortOperator.Ascending);
            posdeliverypoints.GetMulti(posdeliverypointFilter, 0, posdeliverypointSort);

            foreach (PosdeliverypointEntity posdeliverypoint in posdeliverypoints)
            {
                if (!string.IsNullOrWhiteSpace(posdeliverypoint.RevenueCenter))
                {
                    this.ddlPosdeliverypointId.AddItem($"{posdeliverypoint.Name} ({posdeliverypoint.ExternalId}) [{posdeliverypoint.RevenueCenter}]", posdeliverypoint.PosdeliverypointId);
                }
                else
                {
                    this.ddlPosdeliverypointId.AddItem($"{posdeliverypoint.Name} ({posdeliverypoint.ExternalId})", posdeliverypoint.PosdeliverypointId);
                }
            }
        }

        private void HookUpEvents()
        {
            this.btRefreshPrinters.Click += btRefreshPrinters_Click;
            this.btSelectPrinter.Click += btSelectPrinter_Click;
        }

		#endregion

		#region Event Handlers

        protected void Page_PreRender(object sender, EventArgs e)
        {
            try
            {
                bool selectPrinter = ViewState["select_printer"] != null ? (bool)ViewState["select_printer"] : false;
                bool refreshPrinters = ViewState["refresh_printers"] != null ? (bool)ViewState["refresh_printers"] : false;

                string cacheKey = string.Format("Printers.{0}", CmsSessionHelper.CurrentCompanyId);

                Printer[] printers = null;
                if (refreshPrinters || (!Dionysos.Web.CacheHelper.TryGetValue(cacheKey, false, out printers) && selectPrinter))
                {
                    printers = GoogleAPI.GetPrinters(CmsSessionHelper.CurrentCompanyId);
                    if (printers != null)
                    {
                        Dionysos.Web.CacheHelper.AddSlidingExpire(false, cacheKey, printers, 30);
                    }
                    else
                    {
                        this.MultiValidatorDefault.AddWarning("No printer available");
                        this.Validate();
                    }
                }

                if (printers != null)
                {
                    DataTable data = new DataTable();
                    data.Columns.Add("Name");
                    data.Columns.Add("PrinterId");

                    foreach (Printer printer in printers)
                    {
                        DataRow row = data.NewRow();
                        row["Name"] = printer.displayName;
                        row["PrinterId"] = printer.id;
                        data.Rows.Add(row);
                    }

                    this.ddlGooglePrinterId.UseDataBinding = true;
                    this.ddlGooglePrinterId.DataSource = data;
                    this.ddlGooglePrinterId.DataBind();

                    this.ddlGooglePrinterId.Value = this.DataSourceAsDeliverypointEntity.GooglePrinterId;

                    this.ddlGooglePrinterId.Enabled = true;

                    this.btSelectPrinter.Visible = false;
                    this.btRefreshPrinters.Visible = true;
                }
                else if (!selectPrinter)
                {
                    if (!string.IsNullOrWhiteSpace(this.DataSourceAsDeliverypointEntity.GooglePrinterName))
                    {
                        this.ddlGooglePrinterId.NullText = this.DataSourceAsDeliverypointEntity.GooglePrinterName;
                    }
                    else
                    {
                        this.ddlGooglePrinterId.NullText = this.Translate("google_printer_none", "None");
                    }

                    this.ddlGooglePrinterId.UseDataBinding = false;
                    this.ddlGooglePrinterId.Enabled = false;

                    this.btSelectPrinter.Visible = true;
                    this.btRefreshPrinters.Visible = false;
                }
            }
            catch (Exception ex)
            {
                // When something goes wrong here, too bad! Unless you're a developer, then you're gonna have a bad time.
                // Unless you are Mr G or Mr D.
                if (TestUtil.IsPcDeveloper && !TestUtil.IsPcGabriel && !TestUtil.IsPcBattleStationDanny)
                {
                    throw ex;
                }
            }
        }	    

	    protected void Page_Load(object sender, EventArgs e)
		{
            this.HookUpEvents();

            if(this.PageMode != Dionysos.Web.PageMode.Add)
                this.Validate();
		}

		private void Deliverypoint_DataSourceLoaded(object sender)
		{
            if (this.PageMode == Dionysos.Web.PageMode.Add)
            {
                this.DataSourceAsDeliverypointEntity.EnableAnalytics = true;
            }

			this.SetGui();
		}

        private void btSelectPrinter_Click(object sender, EventArgs e)
        {
            ViewState["select_printer"] = true;
        }

        private void btRefreshPrinters_Click(object sender, EventArgs e)
        {
            ViewState["refresh_printers"] = true;
        }

        public override bool Save()
        {
            if (this.PageMode == Dionysos.Web.PageMode.Add)
            {
                this.DataSourceAsDeliverypointEntity.CompanyId = CmsSessionHelper.CurrentCompanyId;
            }

            bool canSave = true;
            if (this.ddlRoomControllerType.SelectedItem != null)
            {
                RoomControlType roomControlType = (RoomControlType)this.ddlRoomControllerType.SelectedItem.Value;

                if (roomControlType == RoomControlType.Modbus)
                {
                    int slaveId;
                    if (tbRoomControllerSlaveId.Text.Length > 0 && !int.TryParse(this.tbRoomControllerSlaveId.Text, out slaveId))
                    {
                        canSave = false;
                        this.MultiValidatorDefault.AddError(Translate("invalid_modbus_slaveid", "Modbus slave id has be an integer."));
                    }
                }
            }

            if (!canSave)
            {
                Validate();
                return false;
            }

            if (!string.IsNullOrWhiteSpace(this.ddlGooglePrinterId.Value))
            {
                this.DataSourceAsDeliverypointEntity.GooglePrinterName = this.ddlGooglePrinterId.Text;
            }
            else
            {
                this.DataSourceAsDeliverypointEntity.GooglePrinterName = string.Empty;
            }

            bool success = false;

            try
            {
                success = base.Save();
            }
            catch (ObymobiException oex)
            {
                switch ((DeliverypointSaveResult)oex.ErrorEnumValue)
                {
                    case DeliverypointSaveResult.MultipleDeliverypointsWithSameNumber:
                        this.MultiValidatorDefault.AddError("Another deliverypoint already exists for the specified number.");
                        break;
                }
                this.Validate();
            }

            return success;
        }

		#endregion

		#region Properties


		/// <summary>
		/// Return the page's datasource as a DeliverypointEntity
		/// </summary>
		public DeliverypointEntity DataSourceAsDeliverypointEntity
		{
			get
			{
				return this.DataSource as DeliverypointEntity;
			}
		}

		#endregion
    }
}
