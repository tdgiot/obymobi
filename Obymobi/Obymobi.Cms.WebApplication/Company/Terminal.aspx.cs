using Dionysos;
using Dionysos.Web.UI;
using Obymobi.Cms.Logic.HelperClasses;
using Obymobi.Cms.WebApplication.Extensions;
using Obymobi.Cms.WebApplication.Old_App_Code;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.Injectables.Validators;
using Obymobi.Enums;
using Obymobi.Extensions;
using Obymobi.Logic.Comet;
using Obymobi.Logic.Comet.Netmessages;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Globalization;
using LogicHelpers = Obymobi.Logic.HelperClasses;

namespace Obymobi.ObymobiCms.Company
{
    public partial class Terminal : PageLLBLGenEntity
    {
        #region Fields

        private const int SWITCH_TO_WEBSERVICE_COMMAND = 99997;
        private const int SWITCH_TO_SIGNALR_COMMAND = 99998;
        private const int CLOSE_SUPPORT_TOOLS = 99999;

        private bool forwardToTerminalChanged;

        #endregion

        #region Methods

        protected override void OnInit(EventArgs e)
        {
            this.DataSourceLoaded += Terminal_DataSourceLoaded;

            this.tabsMain.AddTabPage("Message Template Categories", "MessageTemplateCategories", "~/Company/Subpanels/MessageTemplateCategoryPanel.ascx");

            // GK Entity is required PreInit
            if (this.PageMode == Dionysos.Web.PageMode.Edit)
            {
                this.DataSource = new TerminalEntity(this.EntityId);

                // Load POS Settings Control
                POSConnectorType posConnectorType = this.DataSourceAsTerminalEntity.POSConnectorTypeEnum;
                switch (posConnectorType)
                {
                    case POSConnectorType.Unknown:
                        // Do nothing
                        break;
                    case POSConnectorType.Agilysys:
                        this.tabsMain.AddTabPage("Kassakoppeling", "POSSettings", "~/Company/SubPanels/PosSettingsPanels/AgilysysSettingsPanel.ascx");
                        break;
                    case POSConnectorType.SimphonyGen1:
                        this.tabsMain.AddTabPage("Kassakoppeling", "POSSettings", "~/Company/SubPanels/PosSettingsPanels/SimphonyGen1SettingsPanel.ascx");
                        break;
                }

                // Load PMS Settings Control
                PMSConnectorType pmsConnectorType = this.DataSourceAsTerminalEntity.PMSConnectorTypeEnum;
                switch (pmsConnectorType)
                {
                    case PMSConnectorType.Unknown:
                        // Do Nodshing
                        break;
                    case PMSConnectorType.Comtrol:
                        this.tabsMain.AddTabPage("PMS-integration", "PMSSettings", "~/Company/SubPanels/PmsSettingsPanels/ComtrolSettingsPanel.ascx");
                        break;
                    case PMSConnectorType.Tigertms:
                        this.tabsMain.AddTabPage("PMS-integration", "PMSSettings", "~/Company/SubPanels/PmsSettingsPanels/TigertmsSettingsPanel.ascx");
                        break;
                    case PMSConnectorType.Innsist:
                        this.tabsMain.AddTabPage("PMS-integration", "PMSSettings", "~/Company/SubPanels/PmsSettingsPanels/InnsistPmsSettingsPanel.ascx");
                        break;
                }
            }

            //this.tabsMain.AddTabPage("Log", "TerminalLogCollection", "~/Company/SubPanels/TerminalLogCollection.ascx");
            base.OnInit(e);
            SetGui();
        }

        private void LoadUserControls()
        {
            this.cbTerminalConfigurationId.DataBindEntityCollection<TerminalConfigurationEntity>(TerminalConfigurationFields.CompanyId == CmsSessionHelper.CurrentCompanyId, TerminalConfigurationFields.Name, TerminalConfigurationFields.Name);



            this.ddlUseHardKeyboard.DataBindEnum<HardwareKeyboardType>();
        }

        private void SetGui()
        {
            //this.cbReceiptTypes.Items.Add("Klant-bon", "EndUser");
            this.cbReceiptTypes.Items.Add("Bar-bon", "InternalUse");
            //this.cbReceiptTypes.Items.Add("Bar- & Klantbon", "InternalUse,EndUser");

            foreach (POSConnectorType type in Enum.GetValues(typeof(POSConnectorType)))
            {
                this.cbPOSConnectorType.Items.Add(type.GetStringValue(), (int)type);
            }

            foreach (PMSConnectorType type in Enum.GetValues(typeof(PMSConnectorType)))
            {
                this.cbPMSConnectorType.Items.Add(type.GetStringValue(), (int)type);
            }

            // Noficiation Methods
            foreach (TerminalNotificationMethod method in Enum.GetValues(typeof(TerminalNotificationMethod)))
            {
                this.cbNotificationForNewOrder.Items.Add(method.ToString().FormatPascalCase(), (int)method);
                this.cbNotificationForOverdueOrder.Items.Add(method.ToString().FormatPascalCase(), (int)method);
            }

            ProductCollection products = LogicHelpers.ProductHelper.GetProductCollectionForCompany(CmsSessionHelper.CurrentCompanyId, null, ProductType.PosServiceMessage);
            foreach (ProductEntity product in products)
            {
                this.cbBatteryLowProductId.Items.Add(product.Name, product.ProductId);
                this.cbUnlockDeliverypointProductId.Items.Add(product.Name, product.ProductId);
                this.cbClientDisconnectedProductId.Items.Add(product.Name, product.ProductId);
                this.cbOrderFailedProductId.Items.Add(product.Name, product.ProductId);
            }
            this.cbBatteryLowProductId.DataBind();
            this.cbUnlockDeliverypointProductId.DataBind();
            this.cbClientDisconnectedProductId.DataBind();

            DeliverypointCollection deliverypoints = DeliverypointHelper.GetDeliverypoints(CmsSessionHelper.CurrentCompanyId, null, false, 0);

            this.cbSystemMessagesDeliverypointId.DataSource = deliverypoints;
            this.cbSystemMessagesDeliverypointId.DataBind();

            this.cbAltSystemMessagesDeliverypointId.DataSource = deliverypoints;
            this.cbAltSystemMessagesDeliverypointId.DataBind();

            if (this.PageMode == Dionysos.Web.PageMode.Add)
            {
                this.lblForwardToTerminalId.Visible = false;
                this.cbForwardToTerminalId.Visible = false;
            }
            else
            {
                PredicateExpression filterTerminals = new PredicateExpression
                {
                    TerminalFields.CompanyId == this.DataSourceAsTerminalEntity.CompanyId,
                    TerminalFields.TerminalId != this.DataSourceAsTerminalEntity.TerminalId,
                    TerminalFields.HandlingMethod == this.DataSourceAsTerminalEntity.HandlingMethod
                };
                TerminalCollection terminals = LogicHelpers.TerminalHelper.GetTerminalCollection(filterTerminals);
                this.cbForwardToTerminalId.DataSource = terminals;
                this.cbForwardToTerminalId.DataBind();
            }

            // Deliverypointgroups
            this.cbDeliverypointgroupId.DataSource = LogicHelpers.DeliverypointgroupHelper.GetDeliverypointgroupCollectionForCompany(CmsSessionHelper.CurrentCompanyId, null);
            this.cbDeliverypointgroupId.DataBind();

            if (CmsSessionHelper.CurrentRole < Role.Reseller)
            {
                this.tabsMain.TabPages.FindByName("Settings").Visible = false;
                //this.tabsMain.TabPages.FindByName("POSSettings").Visible = false;

                if (this.tabsMain.TabPages.IndexOfName("Icrtouchprintermapping") > 0)
                {
                    this.tabsMain.TabPages.FindByName("Icrtouchprintermapping").Visible = false;
                }
            }

            // PMS / POS are mutually exclusive
            this.plhPosSettings.Visible = this.DataSourceAsTerminalEntity.POSConnectorTypeEnum != POSConnectorType.Unknown;
            this.cbPOSConnectorType.Enabled = this.DataSourceAsTerminalEntity.PMSConnectorTypeEnum == PMSConnectorType.Unknown;
            this.cbPMSConnectorType.Enabled = this.DataSourceAsTerminalEntity.POSConnectorTypeEnum == POSConnectorType.Unknown;

            // Handling Methods
            this.ddlHandlingMethod.DataBindEnumStringValuesAsDataSource(typeof(TerminalType));

            this.cbForwardToTerminalId.SelectedIndexChanged += cbForwardToTerminalId_SelectedIndexChanged;

            DeviceEntity device = this.DataSourceAsTerminalEntity.DeviceEntity;

            // Commands
            this.cbCommand.Items.Add(Translate("SendLogToWebservice", "Log versturen naar webservice"), (int)TerminalCommand.SendLogToWebservice);
            this.cbCommand.Items.Add(Translate("RestartApplication", "Applicatie herstarten"), (int)TerminalCommand.RestartApplication);
            this.cbCommand.Items.Add(Translate("RestartDevice", "Apparaat herstarten"), (int)TerminalCommand.RestartDevice);
            this.cbCommand.Items.Add(Translate("RestartInRecovery", "Apparaat herstarten (recovery)"), (int)TerminalCommand.RestartInRecovery);
            this.cbCommand.Items.Add(Translate("PosDataSynchronisation", "POS data synchroniseren"), (int)TerminalCommand.PosDataSynchronisation);
            this.cbCommand.Items.Add(Translate("NonPosDataSynchronisation", "Data synchroniseren"), (int)TerminalCommand.NonPosDataSynchronisation);
            this.cbCommand.Items.Add(Translate("MenuConsistencyCheck", "Menu consistentie check"), (int)TerminalCommand.MenuConsistencyCheck);
            this.cbCommand.Items.Add(Translate("DownloadConsoleUpdate", "Download console update"), (int)TerminalCommand.DownloadConsoleUpdate);
            this.cbCommand.Items.Add(Translate("DownloadAgentUpdate", "Download Agent update"), (int)TerminalCommand.DownloadAgentUpdate);
            this.cbCommand.Items.Add(Translate("DownloadSupporttoolsUpdate", "Download SupportTools update"), (int)TerminalCommand.DownloadSupportToolsUpdate);
            this.cbCommand.Items.Add(Translate("UpdateMessagingService", "Update MessagingService"), (int)TerminalCommand.DownloadInstallMessagingServiceUpdate);
            this.cbCommand.Items.Add(Translate("ClearBrowserCache", "Clear browser cache"), (int)TerminalCommand.ClearBrowserCache);
            if (device != null && device.UpdateConsoleDownloaded)
            {
                this.cbCommand.Items.Add(Translate("InstallConsoleUpdate", "Install Console update"), (int)TerminalCommand.InstallConsoleUpdate);
            }

            if (device != null && device.UpdateAgentDownloaded)
            {
                this.cbCommand.Items.Add(Translate("InstallAgentUpdate", "Install Agent update"), (int)TerminalCommand.InstallAgentUpdate);
            }

            if (device != null && device.UpdateSupportToolsDownloaded)
            {
                this.cbCommand.Items.Add(Translate("InstallSupportToolsUpdate", "Install SupportTools update"), (int)TerminalCommand.InstallSupportToolsUpdate);
            }

            this.cbCommand.Items.Add("Switch to webservice mode", Terminal.SWITCH_TO_WEBSERVICE_COMMAND);
            this.cbCommand.Items.Add("Switch to signalr mode", Terminal.SWITCH_TO_SIGNALR_COMMAND);
            this.cbCommand.Items.Add("Close SupportTools", Terminal.CLOSE_SUPPORT_TOOLS);

            // Device mac
            if (this.DataSourceAsTerminalEntity.DeviceId.HasValue)
            {
                this.lblDeviceMac.Visible = true;
                this.lblDeviceNotLinked.Visible = false;
                this.lblDeviceMac.Text = this.DataSourceAsTerminalEntity.DeviceEntity.Identifier;
                this.btUnlinkDevice.Visible = true;
                this.btDeviceShell.Visible = true;
            }
            else
            {
                this.lblDeviceMac.Visible = false;
                this.lblDeviceNotLinked.Visible = true;
                this.btUnlinkDevice.Visible = false;
                this.btDeviceShell.Visible = false;
            }

            // UI modes
            PredicateExpression uiModeFilter = new PredicateExpression
            {
                UIModeFields.CompanyId == CmsSessionHelper.CurrentCompanyId,
                UIModeFields.Type == (int)UIModeType.VenueOwnedStaffDevices
            };

            UIModeCollection uiModes = new UIModeCollection();
            uiModes.GetMulti(uiModeFilter);

            this.ddlUIModeId.DataSource = uiModes;
            this.ddlUIModeId.DataBind();

            // Monitoring
            if (this.DataSourceAsTerminalEntity.UseMonitoring && !this.DataSourceAsTerminalEntity.CompanyEntity.UseMonitoring)
            {
                AddInformator(InformatorType.Warning, Translate("CompanyMonitoringNotEnabled", "Support monitoring is uitgeschakeld voor dit bedrijf."));
            }

            // MTV/RL: HotFix / Temporary code to disable/hide the POS Synchronize for Kimpton (KNZ).
            HidePOSSynchronizeKMZ();

            this.cbTerminalConfigurationId.Enabled = !this.DataSourceAsTerminalEntity.TerminalConfigurationId.HasValue;
            this.plhRemoteControl.Visible = device.ShowRemoteControl(CmsSessionHelper.CurrentRole);
            this.plhLogCat.Visible = device.ShowLogCat(CmsSessionHelper.CurrentRole);
        }

        /// <summary>
        /// Temporary code to disable the POS synchronize on start for Kimpton (KMZ).
        /// </summary>
        private void HidePOSSynchronizeKMZ()
        {
            if (IsKimptonHotelAndNotGodMode())
            {
                this.lblSynchronizeWithPOSOnStart.Visible = false;
                this.cbSynchronizeWithPOSOnStart.Visible = false;

                this.cbSynchronizeWithPOSOnStart.Value = false;
            }
        }

        /// <summary>
        /// Temoorary code to decide if the hotel is from Kipton and the user is not in GodMode.
        /// </summary>
        /// <returns>Returns the condition if the hotel is from Kimpton and the user is not in GodMode</returns>
        private bool IsKimptonHotelAndNotGodMode() => (CmsSessionHelper.CurrentCompanyId == 509 || CmsSessionHelper.CurrentCompanyId == 514) && CmsSessionHelper.CurrentRole != Role.GodMode;

        private void cbForwardToTerminalId_SelectedIndexChanged(object sender, EventArgs e) => this.forwardToTerminalChanged = true;

        private void SetDefaults()
        {
            this.cbReceiptTypes.SelectedIndex = 0;
            this.tbMaxDaysLogHistory.Value = 30;
            this.tbMaxStatusReports.Value = 3;
            this.tbRequestInterval.Value = 25;
            this.cbReceiptTypes.SelectedIndex = 0;
            this.cbUseMonitoring.Checked = true;
        }

        private void SetWarnings()
        {
            TerminalEntity terminal = this.DataSourceAsTerminalEntity;
            if (terminal != null)
            {
                if (terminal.POSConnectorTypeEnum != POSConnectorType.Unknown)
                {
                    if (!terminal.HasSystemMessagesDeliverypoint)
                    {
                        AddInformator(InformatorType.Warning, Translate("SystemMessagesDeliverypointNotSpecified", "Er is geen tafel voor systeemmeldingen ingegeven."));
                    }

                    if (!terminal.HasAltSystemMessagesDeliverypoint)
                    {
                        AddInformator(InformatorType.Warning, Translate("AltSystemMessagesDeliverypointNotSpecified", "Er is geen alternatieve tafel voor systeemmeldingen ingegeven."));
                    }

                    if (!terminal.HasUnlockDeliverypointProduct)
                    {
                        AddInformator(InformatorType.Warning, Translate("UnlockDeliverypointProductNotSpecified", "Er is geen product geselecteerd om te melden dat een tafel nog geopend is."));
                    }

                    if (!terminal.HasBatteryLowProduct)
                    {
                        AddInformator(InformatorType.Warning, Translate("BatteryLowProductNotSpecified", "Er is geen product geselecteerd om te melden dat een batterij op dreigt te raken."));
                    }

                    if (!terminal.HasOrderFailedProduct)
                    {
                        AddInformator(InformatorType.Warning, Translate("OrderFailedProductNotSpecified", "Er is geen product geselecteerd om te melden dat een bestelling mislukt is."));
                    }

                    if (!terminal.HasClientDisconnectedProduct)
                    {
                        AddInformator(InformatorType.Warning, Translate("ClientDisconnectedNotSpecified", "Er is geen product geselecteerd om te melden dat een client niet meer verbonden is."));
                    }
                }

                if (this.PageMode == Dionysos.Web.PageMode.Add &&
                    terminal.HandlingMethod == (int)TerminalType.Console &&
                    (terminal.UIModeId <= 0 || terminal.UIModeId == null))
                {
                    AddInformator(InformatorType.Warning, Translate("UIModeNotSpecified", "Er is geen UI Mode geselecteerd voor de terminal."));
                }

                if (this.DataSourceAsTerminalEntity.DeviceId.HasValue && this.DataSourceAsTerminalEntity.DeviceEntity.IsDevelopment)
                {
                    AddInformator(InformatorType.Information, Translate("Information.IsDevelopmentDevice", "This device is set in development mode."));
                }

                Validate();
            }
        }

        public override bool Save()
        {
            if (IsKimptonHotelAndNotGodMode())
            {
                this.cbSynchronizeWithPOSOnStart.Value = false;
            }

            if (this.PageMode == Dionysos.Web.PageMode.Add)
            {
                this.DataSourceAsTerminalEntity.CompanyId = CmsSessionHelper.CurrentCompanyId;
            }

            if (this.forwardToTerminalChanged)
            {
                int forwardToTerminalId = 0;
                if (this.cbForwardToTerminalId.ValidId > 0)
                {
                    forwardToTerminalId = this.cbForwardToTerminalId.ValidId;
                }

                CometHelper.SetForwardTerminal(this.DataSourceAsTerminalEntity.TerminalId, forwardToTerminalId);
            }

            // Set Pos Connector Type, because ValueId == 0 (zero) results in null
            this.DataSourceAsTerminalEntity.POSConnectorType = this.cbPOSConnectorType.ValidId == -1 ? 0 : this.cbPOSConnectorType.ValidId;

            bool result = false;
            try
            {
                result = base.Save();
            }
            catch (ObymobiException ex)
            {
                if (ex.ErrorEnumValue.Equals(TerminalValidator.TerminalValidatorResult.MultipleTerminalsWithPmsConnectorNotSupported))
                {
                    AddInformator(InformatorType.Warning, Translate("MultipleTerminalsWithPmsConnectorNotSupported", "Another terminal has already been configured with PMS connector. Multiple terminals with PMS connectors is not supported."));
                }
                else
                {
                    throw;
                }
            }

            return result;
        }

        private void HookUpEvents()
        {
            this.btUnlinkDevice.Click += btUnlinkDevice_Click;
            this.btDeviceShell.Click += btDeviceShell_Click;
            this.btExecuteCommand.Click += btExecuteCommand_Click;
            this.btUpdateActivityLog.Click += btUpdateActivityLog_Click;
            this.btRemoteControl.Click += (sender, args) =>
                                          {
                                              string url = new RemoteControlUrl(this.DataSourceAsTerminalEntity.DeviceEntity.Identifier);
                                              this.Response.Write($"<script>window.open('{url}','_self');</script>");
                                          };
            this.btnLogCat.Click += (sender, args) =>
                                    {
                                        string url = new LogCatUrl(this.DataSourceAsTerminalEntity.DeviceEntity.Identifier);
                                        this.Response.Write($"<script>window.open('{url}','_self');</script>");
                                    };
        }

        private void btDeviceShell_Click(object sender, EventArgs e) => this.Response.Redirect(string.Format("DeviceShell.aspx?Device={0}&TerminalId={1}", this.DataSourceAsTerminalEntity.DeviceEntity.Identifier.Replace(":", ""), this.DataSourceAsTerminalEntity.TerminalId));

        private void btUpdateActivityLog_Click(object sender, EventArgs e)
        {
            // Do nothing, the postback is enough.
        }

        private string WifiStrengthToText(int? strength)
        {
            if (!strength.HasValue)
            {
                return "Unknown";
            }

            if (strength > -55)
            {
                return "Excellent";
            }
            else if (strength > -66)
            {
                return "Good";
            }
            else if (strength > -77)
            {
                return "Average";
            }
            else if (strength > -88)
            {
                return "Poor";
            }
            else
            {
                return "Bad";
            }
        }

        private void RenderDeviceInfo(DeviceEntity device)
        {
            this.lblLastRequestValue.Text = device.LastRequestUTC.HasValue ? device.LastRequestUTC.Value.UtcToLocalTime().ToString(CultureInfo.InvariantCulture) : "Unknown";
            this.lblLastSupportToolsRequestValue.Text = device.LastSupportToolsRequestUTC.HasValue ? device.LastSupportToolsRequestUTC.Value.UtcToLocalTime().ToString(CultureInfo.InvariantCulture) : "Unknown";
            this.lblLastApplicationVersionValue.Text = !device.ApplicationVersion.IsNullOrWhiteSpace() ? new VersionNumber(device.ApplicationVersion).NumberAsString : "Unknown";
            this.lblLastSupportToolsVersionValue.Text = !device.SupportToolsVersion.IsNullOrWhiteSpace() ? new VersionNumber(device.SupportToolsVersion).NumberAsString : "Unknown";
            this.lblLastAgentVersionValue.Text = !device.AgentVersion.IsNullOrWhiteSpace() ? new VersionNumber(device.AgentVersion).NumberAsString : "Unknown";
            this.lblLastMessagingServiceVersionValue.Text = !device.MessagingServiceVersion.IsNullOrWhiteSpace() ? new VersionNumber(device.MessagingServiceVersion).NumberAsString : "Unknown";
            this.lblLastMessagingServiceRequestValue.Text = device.LastMessagingServiceRequest.HasValue ? device.LastMessagingServiceRequest.Value.UtcToLocalTime().ToString(CultureInfo.InvariantCulture) : "Unknown";
            this.lblLastOsVersionValue.Text = !device.OsVersion.IsNullOrWhiteSpace() ? new VersionNumber(device.OsVersion).NumberAsString : "Unknown";
            this.lblLastPrivateIpAddressesValue.Text = device.PrivateIpAddresses;
            this.lblLastPublicIpAddressValue.Text = device.PublicIpAddress;
            this.lblLastBatteryLevelValue.Text = device.BatteryLevel.HasValue ? device.BatteryLevel.Value + "%" : "Unknown";
            this.lblLastWifiStrengthValue.Text = string.Format("{0} ({1})", WifiStrengthToText(device.WifiStrength), device.WifiStrength.GetValueOrDefault(-1));
            this.lblLastCommunicationMethodValue.Text = device.CommunicationMethod.ToEnum<ClientCommunicationMethod>().ToString();
            this.lblLastCloudEnvironmentValue.Text = device.CloudEnvironment;
            this.lblDeviceTypeValue.Text = device.DeviceModel.HasValue ? device.DeviceModel.GetStringValue() : device.TypeText;

            this.lblIsChargingYes.Visible = device.IsCharging;
            this.lblIsChargingNo.Visible = !device.IsCharging;
        }

        #endregion

        #region Event handlers

        private void Terminal_DataSourceLoaded(object sender) => LoadUserControls();

        protected void Page_Load(object sender, EventArgs e)
        {
            HookUpEvents();

            if (this.DataSourceAsTerminalEntity.IsNew)
            {
                this.DataSourceAsTerminalEntity.OfflineNotificationEnabled = true;
            }

            if (this.PageMode == Dionysos.Web.PageMode.Edit)
            {
                TerminalEntity terminalEntity = this.DataSourceAsTerminalEntity;
                DeviceEntity deviceEntity = this.DataSourceAsTerminalEntity.DeviceEntity;

                if (this.DataSourceAsTerminalEntity.DeviceId.HasValue)
                {
                    RenderDeviceInfo(deviceEntity);
                }

                if (this.DataSourceAsTerminalEntity.LastStatus.HasValue)
                {
                    this.lblLastStatusTextValue.Text = this.DataSourceAsTerminalEntity.LastStatusText;
                }

                this.lblLoadedSuccessfullyLabelYes.Visible = this.DataSourceAsTerminalEntity.LoadedSuccessfully;
                this.lblLoadedSuccessfullyLabelNo.Visible = !this.DataSourceAsTerminalEntity.LoadedSuccessfully;
                this.lblPrinterConnectedValue.Text = terminalEntity.PrinterConnected ? "Connected" : "Disconnected";

                foreach (TerminalOperationState state in Enum.GetValues(typeof(TerminalOperationState)))
                {
                    if ((int)state == this.DataSourceAsTerminalEntity.OperationState)
                    {
                        this.lblOperationStateValue.Text = state.GetStringValue();
                        break;
                    }
                }

                // Activity Gridview (in page load, since we require the contorls to be databound)
                this.gvTerminalActivityLog.DataSource = LogicHelpers.TerminalHelper.GetTerminalActivityLogs(this.DataSourceAsTerminalEntity, null, this.cblTypesToShow.Items[1].Selected, this.cblTypesToShow.Items[0].Selected, this.cblTypesToShow.Items[2].Selected, this.tbAmountToShow.Value ?? 100);
                this.gvTerminalActivityLog.DataBind();

                if (this.DataSourceAsTerminalEntity.HandlingMethod == (int)TerminalType.Console)
                {
                    this.ddlUIModeId.IsRequired = true;
                }
                else
                {
                    this.ddlUIModeId.IsRequired = false;
                }
            }

            if (this.PageMode == Dionysos.Web.PageMode.Add && !this.IsPostBack)
            {
                SetDefaults();
            }

            if (!this.IsPostBack)
            {
                SetWarnings();
            }
        }

        private void btExecuteCommand_Click(object sender, EventArgs e)
        {
            // Issue command
            if (this.cbCommand.ValidId > 0)
            {
                if (this.cbCommand.ValidId == Terminal.SWITCH_TO_WEBSERVICE_COMMAND)
                {
                    NetmessageSwitchCometHandler switchToWebservice = new NetmessageSwitchCometHandler
                    {
                        ReceiverTerminalId = this.EntityId,
                        Handler = ClientCommunicationMethod.Webservice
                    };

                    CometHelper.Instance.SendMessage(switchToWebservice);
                }
                else if (this.cbCommand.ValidId == Terminal.SWITCH_TO_SIGNALR_COMMAND)
                {
                    NetmessageSwitchCometHandler switchToSignalR = new NetmessageSwitchCometHandler
                    {
                        ReceiverTerminalId = this.EntityId,
                        Handler = ClientCommunicationMethod.SignalR
                    };

                    CometHelper.Instance.SendMessage(switchToSignalR);
                }
                else if (this.cbCommand.ValidId == Terminal.CLOSE_SUPPORT_TOOLS)
                {
                    NetmessageDeviceCommandExecute netmessage = new NetmessageDeviceCommandExecute
                    {
                        ReceiverTerminalId = this.EntityId,
                        Command = "am force-stop net.craveinteractive.supporttools"
                    };

                    CometHelper.Instance.SendMessage(netmessage);
                }
                else
                {
                    TerminalCommand terminalCommand = this.cbCommand.ValidId.ToEnum<TerminalCommand>();

                    if (terminalCommand == TerminalCommand.InstallConsoleUpdate &&
                        this.DataSourceAsTerminalEntity.DeviceEntity.DeviceModel.IsT3() &&
                        VersionNumber.CompareVersions(this.DataSourceAsTerminalEntity.DeviceEntity.ApplicationVersion, "2022010602") == VersionNumber.VersionState.Older)
                    {
                        CometHelper.DeviceCommandExecuteTerminal(this.EntityId, "pm install -r sdcard/Download/CraveEmenu.apk");
                    }
                    else
                    {
                        CometHelper.TerminalCommand(this.EntityId, this.cbCommand.ValidId.ToEnum<TerminalCommand>(),
                            this.cbCommandToSupportTools.Checked);
                    }
                }

                AddInformatorInfo(Translate("CommandIsRequested", "Het commando '{0}' is verzocht."), this.cbCommand.Text);
            }
            else
            {
                AddInformator(Dionysos.Web.UI.InformatorType.Warning, Translate("ErrorYouHaveToSelectACommandToRequest", "U dient een commando the kiezen om te verzoeken"));
            }
        }

        private void btUnlinkDevice_Click(object sender, EventArgs e)
        {
            this.DataSourceAsTerminalEntity.DeviceId = null;
            this.btUnlinkDevice.Visible = false;
            this.lblDeviceNotLinked.Visible = true;
            this.lblDeviceMac.Visible = false;
            this.DataSourceAsTerminalEntity.Save();
        }

        #endregion

        #region Properties

        public TerminalEntity DataSourceAsTerminalEntity => this.DataSource as TerminalEntity;

        #endregion
    }
}
