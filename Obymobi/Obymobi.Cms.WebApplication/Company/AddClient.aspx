﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.AddClient" Codebehind="AddClient.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitleHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhToolbarHolder" Runat="Server">
    <D:Button runat="server" ID="btAddClient" Text="Otoucho scherm aanmaken" />	
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhContentHolder" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Algemeen">
				<Controls>
					<table class="dataformV2">
						<tr>
							<td class="label">
                                <D:Label runat="server" id="lblIdentifier">Identifier</D:Label>
							</td>
							<td class="control">
                                <D:TextBoxString ID="tbIdentifier" runat="server"></D:TextBoxString>
							</td>
							<td class="label">
								<D:Label runat="server" id="lblCompanyId">Bedrijf</D:Label>
							</td>
							<td class="control">
								<X:ComboBoxLLBLGenEntityCollection ID="ddlCompanyId" runat="server" EntityName="Company" Enabled="false" BackColor="White"></X:ComboBoxLLBLGenEntityCollection>
							</td>	
					    </tr>
						<tr>
							<td class="label">
								<D:Label runat="server" id="lblDeliverypointGroupId">Tafelgroep</D:Label>
							</td>
							<td class="control">
								<X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlDeliverypointGroupId" EntityName="Deliverypointgroup"></X:ComboBoxLLBLGenEntityCollection>            
							</td>	
							<td class="label">
								<D:Label runat="server" id="lblTeamviewerId">TeamViewerId</D:Label>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbTeamviewerId" runat="server"></D:TextBoxString>
							</td>
					    </tr>
						<tr>
							<td class="label">
								<D:Label runat="server" id="lblConfiguration">Configuratie</D:Label>
							</td>						
							<td colspan="3" class="control">
								<D:TextBox ID="tbConfiguration" runat="server" Rows="5" TextMode="MultiLine" CssClass="inputNoHeight"></D:TextBox>                
							</td>
						</tr>
						<tr>
							<td class="label">
								<D:Label runat="server" id="lblNotes">Notities</D:Label>
							</td>						
							<td colspan="3" class="control">
								<D:TextBox ID="tbNotes" runat="server" Rows="5" TextMode="MultiLine" CssClass="inputNoHeight"></D:TextBox>                
							</td>
						</tr>			
                    </table>
                </Controls>
            </X:TabPage>
        </TabPages>
    </X:PageControl>
</div>
</asp:Content>

