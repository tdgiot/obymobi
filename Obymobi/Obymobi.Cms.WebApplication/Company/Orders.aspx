﻿<%@ Page Title="Orders" Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.Orders" Codebehind="Orders.aspx.cs" %>
<%@ Register Assembly="DevExpress.XtraCharts.v20.1.Web" Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>
<%@ Register Assembly="DevExpress.XtraCharts.v20.1" Namespace="DevExpress.XtraCharts" TagPrefix="dxcharts" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitleHolder" Runat="Server">
    <D:Label runat="server" id="lblTitle">Bestellingen</D:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhToolbarHolder" Runat="Server">
    <D:Button runat="server" ID="btnRefresh" Text="Verversen" CausesValidation="true"/>	
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhContentHolder" Runat="Server">
	<table class="dataformV2">
		<tr>
			<td class="label">
				<D:Label runat="server" id="lblDeliverypointgroupId">Tafelgroep</D:Label>
			</td>
			<td class="control">
                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlDeliverypointgroupId" IncrementalFilteringMode="StartsWith" EntityName="Deliverypointgroup" TextField="Name" ValueField="DeliverypointgroupId" PreventEntityCollectionInitialization="true" />
			</td>							
			<td class="label">
                <D:Label runat="server" id="lblDeliverypoint">Tafel</D:Label>
			</td>
			<td class="control">
                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlDeliverypointId" IncrementalFilteringMode="StartsWith" EntityName="Deliverypoint" TextField="DeliverypointgroupNameDeliverypointNumber" ValueField="DeliverypointId" PreventEntityCollectionInitialization="true" />
			</td>											
		</tr>
		<tr>
			<td class="label">
				<D:Label runat="server" id="lblDateFrom">Datum van</D:Label>
			</td>
			<td class="control">
                <X:DateEdit runat="server" ID="deDateFrom"></X:DateEdit>
			</td>							
			<td class="label">
				<D:Label runat="server" id="lblDateTo">Datum tot</D:Label>
			</td>
			<td class="control">
                <X:DateEdit runat="server" ID="deDateTo"></X:DateEdit>
			</td>							
		</tr>
    </table>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Overzicht" Name="Overview">
				<controls>
                    <D:PlaceHolder ID="PlaceHolder1" runat="server">
                        <table class="dataformV2">
					    	<D:PlaceHolder runat="server" ID="plhOverview"></D:PlaceHolder>
                            <X:GridView ID="ordersGrid" ClientInstanceName="ordersGrid" OnCustomCallback="Order_Clicked" runat="server" Width="100%" KeyFieldName="OrderId">
                                <SettingsBehavior AutoFilterRowInputDelay="350" />                            
                                <SettingsPager PageSize="20"></SettingsPager>
                                <SettingsBehavior AllowGroup="false" AllowDragDrop="false" />
                                <ClientSideEvents RowDblClick="function(s, e){
                                        e.cancel = true;
                                        var key = s.GetRowKey(e.visibleIndex);
                                        ordersGrid.PerformCallback(key);
                                    }" />
                                <Columns>
                                    <dxwgv:GridViewDataColumn FieldName="OrderId" VisibleIndex="0" Width="5%">
                                    </dxwgv:GridViewDataColumn>
                                    <dxwgv:GridViewDataColumn FieldName="Orderitems" VisibleIndex="1">
                                    </dxwgv:GridViewDataColumn>
                                    <dxwgv:GridViewDataColumn FieldName="Deliverypoint" VisibleIndex="2" Width="5%">
                                    </dxwgv:GridViewDataColumn>
                                    <dxwgv:GridViewDataColumn FieldName="Deliverypointgroup" VisibleIndex="3" Width="10%">
                                    </dxwgv:GridViewDataColumn>
                                    <dxwgv:GridViewDataColumn FieldName="Status" VisibleIndex="4" Width="5%">
                                    </dxwgv:GridViewDataColumn>
                                    <dxwgv:GridViewDataColumn FieldName="StatusText" VisibleIndex="5" Width="10%">
                                    </dxwgv:GridViewDataColumn>
                                    <dxwgv:GridViewDataColumn FieldName="Created" VisibleIndex="6" Width="10%">
                                    </dxwgv:GridViewDataColumn>
                                </Columns>
                            </X:GridView>  
					    </table>			
                     </D:PlaceHolder>
				</controls>
			</X:TabPage>
        </TabPages>
    </X:PageControl>
</asp:Content>

