﻿using System;
using System.Drawing;
using System.Globalization;
using DevExpress.Web;
using DevExpress.XtraPrinting.Native;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Enums;
using Dionysos.Web;
using Dionysos.Data.LLBLGen;
using Google.Apis.Manual.Util;
using Google.Apis.Util;
using Obymobi.Data;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.ObymobiCms.Company
{
	public partial class RoomControlComponent : Dionysos.Web.UI.PageLLBLGenEntity
	{
        #region Methods

        private void LoadUserControls()
        {
            RoomControlComponentEntity roomControlComponentEntity = new RoomControlComponentEntity(this.EntityId);
            if (roomControlComponentEntity.IsNew)
                return;

            this.tabsMain.AddTabPage("Translations", "CustomTextCollection", "~/Generic/UserControls/CustomTextCollection.ascx", "Generic");
        }

        public override bool Save()
        {
            // Set the UITabId if specified in querystring
            if (this.DataSourceAsRoomControlComponentEntity.IsNew)
            {
                int id;
                if (QueryStringHelper.TryGetValue("RoomControlSectionId", out id))
                    this.DataSourceAsRoomControlComponentEntity.RoomControlSectionId = id;                
            }

            if (this.ShowBlind(this.DataSourceAsRoomControlComponentEntity))
            {
                this.DataSourceAsRoomControlComponentEntity.FieldValue1 = this.tbOpenBlindScene.Text;
                this.DataSourceAsRoomControlComponentEntity.FieldValue2 = this.tbCloseBlindScene.Text;
                this.DataSourceAsRoomControlComponentEntity.FieldValue3 = this.tbStopBlindScene.Text;
                this.DataSourceAsRoomControlComponentEntity.FieldValue4 = this.cbIsCurtain.Checked.ToString();
            }
            else if (this.DataSourceAsRoomControlComponentEntity.Type == RoomControlComponentType.Light)
            {
                this.DataSourceAsRoomControlComponentEntity.FieldValue1 = this.tbDeviceAddress.Text;
                this.DataSourceAsRoomControlComponentEntity.FieldValue2 = this.cbToggleLight.Checked.ToString();
                this.DataSourceAsRoomControlComponentEntity.ControllerId = this.cbLightOnMainController.Checked ? "0" : null;                
            }
            else if (this.DataSourceAsRoomControlComponentEntity.Type == RoomControlComponentType.LightingScene)
            {
                this.DataSourceAsRoomControlComponentEntity.FieldValue5 = this.cbEnableDeactivateButton.Checked ? "1" : null;
            }
            else if (this.DataSourceAsRoomControlComponentEntity.Type == RoomControlComponentType.Thermostat)
            {
                this.DataSourceAsRoomControlComponentEntity.FieldValue1 = this.cblModes.SelectedValues(",");
                this.DataSourceAsRoomControlComponentEntity.FieldValue2 = this.cblFanModes.SelectedValues(",");
                this.DataSourceAsRoomControlComponentEntity.FieldValue3 = this.cbShowCurrentTemp.Checked.ToString();
                this.DataSourceAsRoomControlComponentEntity.ControllerId = this.cbHvacOnMainController.Checked ? "0" : null;
            }

            this.Validate();

            if (base.Save())
            {
                CustomTextHelper.UpdateCustomTexts(this.DataSource, new CompanyEntity(this.DataSourceAsRoomControlComponentEntity.RoomControlSectionEntity.RoomControlAreaEntity.RoomControlConfigurationEntity.CompanyId));
                return true;
            }
            else
            {
                return false;
            }
        }

        public override bool SaveAndNew()
        {
            bool result = Save();
            if(result)
            {
                string url = this.Request.Path + "?mode=add";
                if (this.DataSourceAsRoomControlComponentEntity.RoomControlSectionId > 0)
                    url += string.Format("&RoomControlSectionId={0}", this.DataSourceAsRoomControlComponentEntity.RoomControlSectionId);

                url += string.Format("&Type={0}", (int)this.DataSourceAsRoomControlComponentEntity.Type); // Default on first page

                this.Response.Redirect(url, false);
            }
            return result;
        }

        private void BindRoomControlComponentTypes()
        {
            foreach (RoomControlComponentType type in Enum.GetValues(typeof(RoomControlComponentType)))
            {
                this.cbType.Items.Add(new DevExpress.Web.ListEditItem(type.ToString(), (int)type));
            }       
        }

        private void SetGui()
        {
            if (this.DataSourceAsRoomControlComponentEntity.IsNew)
                this.DataSourceAsRoomControlComponentEntity.Visible = true;

            // Bind UITab types depending on the UIModeType parameter
            int type = -1;
            if (QueryStringHelper.HasValue("Type"))
                Int32.TryParse(QueryStringHelper.GetValue("Type"), out type);

            if (this.DataSourceAsRoomControlComponentEntity.IsNew && type > 0)
                this.DataSourceAsRoomControlComponentEntity.Type = (RoomControlComponentType)type;

            this.BindRoomControlComponentTypes();

            if (!this.IsPostBack)
            {
                this.cbType.Value = (int)this.DataSourceAsRoomControlComponentEntity.Type;
            }

            if (this.ShowBlind(this.DataSourceAsRoomControlComponentEntity))
            {
                this.plhBlind.Visible = true;

                this.lblNameSystem.Visible = false;
                this.tbNameSystem.IsRequired = false;
                this.tbNameSystem.Visible = false;

                this.tbOpenBlindScene.IsRequired = true;
                this.tbOpenBlindScene.Text = this.DataSourceAsRoomControlComponentEntity.FieldValue1;

                this.tbCloseBlindScene.IsRequired = true;
                this.tbCloseBlindScene.Text = this.DataSourceAsRoomControlComponentEntity.FieldValue2;

                this.tbStopBlindScene.IsRequired = true;
                this.tbStopBlindScene.Text = this.DataSourceAsRoomControlComponentEntity.FieldValue3;

                if (this.DataSourceAsRoomControlComponentEntity.FieldValue4.IsNotNullOrEmpty())
                {
                    this.cbIsCurtain.Checked = bool.Parse(this.DataSourceAsRoomControlComponentEntity.FieldValue4);
                }
            }
            else if (this.DataSourceAsRoomControlComponentEntity.Type == RoomControlComponentType.LightingScene)
            {
                this.plhLightingScene.Visible = true;

                this.cbEnableDeactivateButton.Checked = this.DataSourceAsRoomControlComponentEntity.FieldValue5.Equals("1", StringComparison.InvariantCultureIgnoreCase);
            }
            else if (this.DataSourceAsRoomControlComponentEntity.Type == RoomControlComponentType.Light)
            {
                this.plhLight.Visible = true;

                this.tbDeviceAddress.Text = this.DataSourceAsRoomControlComponentEntity.FieldValue1;
                this.cbToggleLight.Checked = this.DataSourceAsRoomControlComponentEntity.FieldValue2.Equals("True", StringComparison.InvariantCultureIgnoreCase);
                this.cbLightOnMainController.Checked = this.DataSourceAsRoomControlComponentEntity.ControllerId.IsNotNullOrEmpty();             
            }
            else if (this.DataSourceAsRoomControlComponentEntity.Type == RoomControlComponentType.Thermostat)
            {
                this.pnlConfiguration.Visible = true;

                this.cbHvacOnMainController.Checked = this.DataSourceAsRoomControlComponentEntity.ControllerId.IsNotNullOrEmpty();

                if (string.IsNullOrWhiteSpace(this.DataSourceAsRoomControlComponentEntity.FieldValue1))
                {
                    this.cblModes.SelectAll();
                }
                else
                {
                    this.cblModes.SelectMultipleItems(this.DataSourceAsRoomControlComponentEntity.FieldValue1, ',');    
                }

                if (!string.IsNullOrWhiteSpace(this.DataSourceAsRoomControlComponentEntity.FieldValue2))
                {
                    this.cblFanModes.SelectMultipleItems(this.DataSourceAsRoomControlComponentEntity.FieldValue2, ',');
                }

                if (string.IsNullOrWhiteSpace(this.DataSourceAsRoomControlComponentEntity.FieldValue3))
                {
                    this.cbShowCurrentTemp.Checked = true;
                }
                else
                {
                    this.cbShowCurrentTemp.Checked = bool.Parse(this.DataSourceAsRoomControlComponentEntity.FieldValue3);
                }
            }

            if (this.DataSourceAsRoomControlComponentEntity.IsDirty && !this.DataSourceAsRoomControlComponentEntity.IsNew)
                this.DataSourceAsRoomControlComponentEntity.Save();            
        }

	    private bool ShowBlind(RoomControlComponentEntity component)
	    {
	        return component.Type == RoomControlComponentType.Blind;
	    }

	    #endregion

		#region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            this.DataSourceLoaded += RoomControlComponent_DataSourceLoaded;
            base.OnInit(e);
        }

        private void RoomControlComponent_DataSourceLoaded(object sender)
        {
            this.SetGui();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }        

		#endregion

		#region Properties

		public RoomControlComponentEntity DataSourceAsRoomControlComponentEntity
		{
			get
			{
                return this.DataSource as RoomControlComponentEntity;
			}
		}

		#endregion
	}
}
