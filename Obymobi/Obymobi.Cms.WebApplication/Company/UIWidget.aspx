﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.UIWidget" Codebehind="UIWidget.aspx.cs" %>
<%@ Reference Control="~/Generic/UserControls/MediaCollection.ascx" %>
<%@ Reference Control="~/Company/SubPanels/UIWidgetCultureCollection.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
                    <D:PlaceHolder runat="server">
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblType">Type</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
                                <X:ComboBoxInt ID="cbType" runat="server" IsRequired="true" UseDataBinding="true"></X:ComboBoxInt>
							</td>
                            <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblSortOrder">Sort order</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxInt ID="tbSortOrder" runat="server"></D:TextBoxInt>
							</td>
					    </tr>	
                        <tr>
                            <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblName">Name</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbName" runat="server" IsRequired="false"></D:TextBoxString>
							</td>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblIsVisible">Visible</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
                                <D:CheckBox runat="server" ID="cbIsVisible" UseDataBinding="true"/>
							</td>
                        </tr>
                        <tr>
                            <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblCaption">Caption</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxMultiLine ID="tbCaption" runat="server" IsRequired="false" Rows="6"></D:TextBoxMultiLine>
                                <small runat="server" id="lblTime" Visible="false">Note: Use placeholder [time] in the caption to display the time within the text.</small>
							</td>
							<td class="label">
							</td>
							<td class="control">
							</td>
					    </tr>
                         <D:PlaceHolder ID="plhAdvertisement" runat="server" Visible="false">
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblAdvertisementId">Advertisement</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection ID="cbAdvertisementId" runat="server" IncrementalFilteringMode="StartsWith" EntityName="Advertisement" ValueField="AdvertisementId" PreventEntityCollectionInitialization="true" UseDataBinding="true"/>
                                </td>
                            </tr>	    
                        </D:PlaceHolder>
                        <D:PlaceHolder ID="plhProductCategory" runat="server" Visible="false">
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblCategoryId">Category</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection ID="cbCategoryId" runat="server" IncrementalFilteringMode="StartsWith" EntityName="Category" ValueField="CategoryId" TextField="Name" PreventEntityCollectionInitialization="true" UseDataBinding="true">
                                        <ClientSideEvents SelectedIndexChanged="function(s, e) {cbProductCategoryId.PerformCallback('1');}" />
                                    </X:ComboBoxLLBLGenEntityCollection>
                                </td>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblProductCategoryId">Product</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection ID="cbProductCategoryId" ClientInstanceName="cbProductCategoryId" OnCallback="cbProductCategoryId_OnCallback" runat="server" IncrementalFilteringMode="StartsWith" EntityName="ProductCategory" ValueField="ProductCategoryId" TextField="ProductCategoryMenuName" PreventEntityCollectionInitialization="true" UseDataBinding="true"/>
                                </td>
                            </tr>
                        </D:PlaceHolder>
						<D:PlaceHolder ID="plhEntertainment" runat="server" Visible="false">
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblEntertainmentId">Entertainment</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection ID="cbEntertainmentId" runat="server" IncrementalFilteringMode="StartsWith" EntityName="Entertainment" ValueField="EntertainmentId" TextField="Name" PreventEntityCollectionInitialization="true" UseDataBinding="true"/>
                                </td>
                            </tr>
                        </D:PlaceHolder>
						<D:PlaceHolder ID="plhEntertainmentcategory" runat="server" Visible="false">
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblEntertainmentcategoryId">Entertainment Category</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection ID="cbEntertainmentcategoryId" runat="server" IncrementalFilteringMode="StartsWith" EntityName="Entertainmentcategory" ValueField="EntertainmentcategoryId" TextField="Name" PreventEntityCollectionInitialization="false" UseDataBinding="true"/>
                                </td>
                            </tr>
                        </D:PlaceHolder>
						<D:PlaceHolder ID="plhSite" runat="server" Visible="false">
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblSiteId">Site</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection ID="cbSiteId" runat="server" IncrementalFilteringMode="StartsWith" EntityName="Site" ValueField="SiteId" TextField="Name" PreventEntityCollectionInitialization="true" UseDataBinding="true">
                                        <ClientSideEvents SelectedIndexChanged="function(s, e) {cbPageId.PerformCallback('1');}" />
                                    </X:ComboBoxLLBLGenEntityCollection>
                                </td>
								<td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblPageId">Page</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection ID="cbPageId" ClientInstanceName="cbPageId" OnCallback="cbPageId_OnCallback" runat="server" IncrementalFilteringMode="StartsWith" EntityName="Page" ValueField="PageId" TextField="Name" PreventEntityCollectionInitialization="true" UseDataBinding="true"/>
                                </td>
                            </tr>
                        </D:PlaceHolder>
						<D:PlaceHolder ID="plhUITabType" runat="server" Visible="false">
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblUITabType">Tab</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
									<X:ComboBoxInt ID="cbUITabType" runat="server" UseDataBinding="true" ValueField="Key" TextField="Value"></X:ComboBoxInt>
                                </td>
                            </tr>
                        </D:PlaceHolder>
						<D:PlaceHolder ID="plhUrl" runat="server" Visible="false">
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblUrl">URL</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:TextBoxString ID="tbUrl" runat="server" IsRequired="false"></D:TextBoxString>
                                </td>                                
                            </tr>
                        </D:PlaceHolder>                        
                        <D:PlaceHolder ID="plhRoomControlType" runat="server" Visible="false">
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblRoomControlType">Room Control Type</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
									<X:ComboBoxInt ID="cbRoomControlType" runat="server" UseDataBinding="true"></X:ComboBoxInt>
                                </td>
                            </tr>
                        </D:PlaceHolder>
                        <D:PlaceHolder ID="plhPdf" runat="server" Visible="false">
					        <tr>
                                <td class="label">
                                    <D:Label runat="server" id="lblPdf">PDF</D:Label>
                                </td>
                                <td class="control">
                                    <D:Button runat="server" ID="btConvertPdf" Text="Upload PDF" OnClientClick="togglePdfUpload(); return false;" Style="float:left;"/>
                                    <D:Button runat="server" ID="btDeletePdf" Text="Delete PDF" Style="float:left;margin-left: 8px;" />                                    
                                </td>
                            </tr>
                        </D:PlaceHolder>
                        <D:PlaceHolder ID="plhScene" runat="server" Visible="false">
                            <tr>
                                <td class="label">
                                    <D:Label runat="server" id="lblScene">Scene</D:Label>
                                </td>
                                <td class="control">
                                    <D:TextBoxString ID="tbScene" runat="server"></D:TextBoxString>
                                </td>
                                <td class="label">
                                    <D:Label runat="server" id="lblSuccessMessage">Success message</D:Label>
                                </td>
                                <td class="control">
                                    <D:TextBoxString ID="tbSuccessMessage" runat="server"></D:TextBoxString>
                                </td>
                            </tr>
                        </D:PlaceHolder>
                        <D:PlaceHolder ID="plhTemperature" runat="server" Visible="false">
                            <tr>
                                <td class="label">
                                    <D:Label runat="server" id="lblTemperatureUnit">Temperature Unit</D:Label>
                                </td>
                                <td class="control">
                                    <X:ComboBoxInt runat="server" ID="ddlTemperatureUnit" UseDataBinding="True" DisplayEmptyItem="False"></X:ComboBoxInt>
                                </td>
                                <td class="label"></td>
                                <td class="control"></td>
                            </tr>
                        </D:PlaceHolder>
                        <D:PlaceHolder ID="plhClock" runat="server" Visible="false">
                            <tr>
                                <td class="label">
                                    <D:Label runat="server" id="lblClockMode">Clock Mode</D:Label>
                                </td>
                                <td class="control">
                                    <X:ComboBoxInt runat="server" ID="ddlClockMode" UseDataBinding="True" DisplayEmptyItem="False"></X:ComboBoxInt>
                                </td>
                                <td class="label"></td>
                                <td class="control"></td>
                            </tr>
                        </D:PlaceHolder>
                        <D:PlaceHolder ID="plhAvailability" runat="server" Visible="false">
                            <tr>
                                <td class="label">
                                    <D:Label runat="server" id="lblDialogTitle">Dialog title</D:Label>
                                </td>
                                <td class="control">
                                    <D:TextBoxString ID="tbDialogTitle" runat="server"></D:TextBoxString>
                                </td>
                                <td class="label">                                    
                                    <D:LabelTextOnly runat="server" ID="lblMessageLayoutType">Message layout type</D:LabelTextOnly>
                                </td>
                                <td class="control">                                    
                                    <X:ComboBoxInt ID="ddlMessageLayoutType" runat="server" UseDataBinding="true" DisplayEmptyItem="false"></X:ComboBoxInt>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:Label runat="server" id="lblDialogTopText">Dialog top text</D:Label>
                                </td>
                                <td class="control" colspan="3">
                                    <D:TextBoxMultiLine ID="tbTopText" runat="server" Rows="6"></D:TextBoxMultiLine>
                                </td>
                                <td class="label">
                                </td>
                                <td class="control">
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:Label runat="server" id="lblDialogBottomText">Dialog bottom text</D:Label>
                                </td>
                                <td class="control" colspan="3">
                                    <D:TextBoxMultiLine ID="tbBottomText" runat="server" Rows="6"></D:TextBoxMultiLine>
                                </td>
                                <td class="label">
                                </td>
                                <td class="control">
                                </td>
                            </tr>
                        </D:PlaceHolder>   
                        <D:PlaceHolder ID="plhBluetoothDevice" runat="server" Visible="false">
                            <tr>
                                <td class="label">
                                    <D:Label runat="server" id="lblBluetoothDevice">Bluetooth Device</D:Label>
                                </td>
                                <td class="control">
                                    <X:ComboBoxInt runat="server" ID="ddlBluetoothDevice" UseDataBinding="false" DisplayEmptyItem="False"></X:ComboBoxInt>
                                </td>
                                <td class="label"></td>
                                <td class="control"></td>
                            </tr>
                        </D:PlaceHolder>
                        <D:PlaceHolder ID="plhAdUnit" runat="server" Visible="false">
                            <tr>
                                <td class="label">
                                    <D:Label runat="server" id="lblAdUnit">Ad unit</D:Label>
                                </td>
                                <td class="control">
                                    <D:TextBoxString ID="tbAdUnitId" runat="server" FriendlyName="Ad unit"></D:TextBoxString>
                                </td>
                                <td class="label"></td>
                                <td class="control"></td>
                            </tr>
                        </D:PlaceHolder>                                             
					 </table>	
                    </D:PlaceHolder>
				</Controls>
			</X:TabPage>	
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>