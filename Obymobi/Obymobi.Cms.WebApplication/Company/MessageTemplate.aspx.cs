﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using DevExpress.Web;
using Dionysos;
using Dionysos.Web;
using Dionysos.Web.UI;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Company
{
    public partial class MessageTemplate : PageLLBLGenEntity
    {
        #region Fields

        public const int MAX_LENGTH_MESSAGE = 750;

        #endregion

        #region Properties

        /// <summary>
        ///     Return the page's datasource as a MessageTemplateEntity
        /// </summary>
        public MessageTemplateEntity DataSourceAsMessageTemplateEntity
        {
            get { return this.DataSource as MessageTemplateEntity; }
        }

        #endregion

        protected override void OnInit(EventArgs e)
        {
            this.DataSourceLoaded += MessageTemplate_DataSourceLoaded;
            base.OnInit(e);
        }

        private void MessageTemplate_DataSourceLoaded(object sender)
        {
            SetGui();
        }

        private void SetGui()
        {
            DataTable table = new DataTable();
            table.Columns.Add(new DataColumn("ImageUrl"));
            table.Columns.Add(new DataColumn("MediaUrl"));
            table.Columns.Add(new DataColumn("MediaId"));
            table.Columns.Add(new DataColumn("Name"));

            MediaRatioTypeMediaCollection mediaRatioTypeMediaCollection = new MediaRatioTypeMediaCollection();

            RelationCollection relations = new RelationCollection();
            relations.Add(MediaRatioTypeMediaEntity.Relations.MediaEntityUsingMediaId);

            PredicateExpression mediaFilter = new PredicateExpression();
            mediaFilter.Add(MediaRatioTypeMediaFields.MediaType == MediaType.Gallery);
            mediaFilter.AddWithOr(MediaRatioTypeMediaFields.MediaType == MediaType.GalleryLarge);
            mediaFilter.AddWithOr(MediaRatioTypeMediaFields.MediaType == MediaType.GallerySmall);

            PredicateExpression filter = new PredicateExpression();
            filter.Add(MediaFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            filter.Add(mediaFilter);

            PrefetchPath prefetch = new PrefetchPath(EntityType.MediaRatioTypeMediaEntity);
            prefetch.Add(MediaRatioTypeMediaEntity.PrefetchPathMediaEntity);
            prefetch.Add(MediaRatioTypeMediaEntity.PrefetchPathMediaRatioTypeMediaFileEntity);

            this.tbMessage.MaxLength = MAX_LENGTH_MESSAGE;

            mediaRatioTypeMediaCollection.GetMulti(filter, 0, null, relations, prefetch);

            List<int> uniqueMediaIds = new List<int>();
            foreach (MediaRatioTypeMediaEntity mediaRatioTypeMedia in mediaRatioTypeMediaCollection)
            {
                if (!uniqueMediaIds.Contains(mediaRatioTypeMedia.MediaId))
                {
                    uniqueMediaIds.Add(mediaRatioTypeMedia.MediaId);

                    DataRow row = table.NewRow();
                    string filePathRelative = "~/Generic/ThumbnailGenerator.ashx?mediaId={0}&width={1}&height={2}&fitInDimensions=false".FormatSafe(mediaRatioTypeMedia.MediaId, 0, 150);
                    row["ImageUrl"] = filePathRelative;
                    row["MediaId"] = mediaRatioTypeMedia.MediaEntity.MediaId;
                    row["Name"] = mediaRatioTypeMedia.MediaEntity.Name;
                    table.Rows.Add(row);
                }
            }

            this.dvGallery.DataSource = table;
            this.dvGallery.DataBind();

            this.DataBindCategories();
            this.DataBindProducts();
            this.DataBindEntertainments();

            this.ddlMessageLayoutType.DataBindEnum<MessageLayoutType>();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (this.PageMode == PageMode.Add)
                {
                    this.DataSourceAsMessageTemplateEntity.MessageLayoutType = MessageLayoutType.Medium;
                }

                if (this.DataSourceAsMessageTemplateEntity.MediaId.HasValue)
                {
                    MediaEntity mediaEntity = new MediaEntity(this.DataSourceAsMessageTemplateEntity.MediaId.Value);
                    if (!mediaEntity.IsNew)
                    {
                        this.tbImageId.Value = mediaEntity.MediaId;
                        this.tbImageName.Value = mediaEntity.Name;
                    }
                }
            }
        }

        public override void Validate()
        {
            base.Validate();
            if (this.IsValid)
            {
                bool hasError = false;

                int selectedActions = 0;

                if (this.cbCategoryId.ValidId > 0)
                    selectedActions++;
                
                if (this.cbProductId.ValidId > 0)
                    selectedActions++;
                
                if (this.cbEntertainmentId.ValidId > 0)
                    selectedActions++;

                if (selectedActions > 1)
                {
                    this.MultiValidatorDefault.AddError("Multiple actions have been selected, please select only one action.");
                    hasError = true;
                }

                if (this.tbMessage.Text.IsNullOrWhiteSpace() && this.ddlMessageLayoutType.Value != (int)MessageLayoutType.AutoSize)
                {
                    this.MultiValidatorDefault.AddError("When no message has been specified, Message layout type needs to be 'Auto size'.");
                    hasError = true;
                }

                if (this.tbMessage.Value.Length > MAX_LENGTH_MESSAGE)
                {
                    this.MultiValidatorDefault.AddError("The message cannot be longer then " + MAX_LENGTH_MESSAGE + " characters.");
                    hasError = true;
                }

                if (hasError)
                    base.Validate();
            }
        }

        public override bool Save()
        {
            if (this.PageMode == PageMode.Add)
            {
                this.DataSourceAsMessageTemplateEntity.CompanyId = CmsSessionHelper.CurrentCompanyId;
            }

            this.DataSourceAsMessageTemplateEntity.MediaId = null;
            int imageId;

            if (int.TryParse(this.tbImageId.Value.ToString(), out imageId))
            {
                if (imageId > 0)
                {
                    this.DataSourceAsMessageTemplateEntity.MediaId = imageId;
                }
            }

            return base.Save();
        }

        private void DataBindCategories()
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(CategoryFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            filter.Add(CategoryFields.AnnouncementAction == true);
            filter.Add(CategoryFields.Visible == true);

            CategoryCollection categoryCollection = new CategoryCollection();
            categoryCollection.GetMulti(filter);

            this.cbCategoryId.DataSource = categoryCollection;
            this.cbCategoryId.DataBind();
        }

        private void DataBindProducts()
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ProductFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            filter.Add(ProductFields.AnnouncementAction == true);
            filter.Add(ProductFields.VisibilityType != VisibilityType.Never);

            ProductCollection productCollection = new ProductCollection(); 
            productCollection.GetMulti(filter);

            this.cbProductId.DataSource = productCollection;
            this.cbProductId.DataBind();
        }

        private void DataBindEntertainments()
        {
            RelationCollection relations = new RelationCollection();
            relations.Add(EntertainmentEntity.Relations.DeliverypointgroupEntertainmentEntityUsingEntertainmentId);
            relations.Add(DeliverypointgroupEntertainmentEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId);

            PredicateExpression filter = new PredicateExpression();
            filter.Add(DeliverypointgroupFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            filter.Add(EntertainmentFields.AnnouncementAction == true);

            EntertainmentCollection entertainmentCollection = new EntertainmentCollection();
            entertainmentCollection.GetMulti(filter, relations);

            this.cbEntertainmentId.DataSource = entertainmentCollection;
            this.cbEntertainmentId.DataBind();
        }
    }
}