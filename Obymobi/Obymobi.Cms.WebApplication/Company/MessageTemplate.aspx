﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.MessageTemplate" Codebehind="MessageTemplate.aspx.cs" %>
<%@ Register Assembly="DevExpress.Web.v20.1" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" runat="Server">
     <script type="text/javascript">
         function SelectImage(id, value) {
             document.getElementById('<%= tbImageId.ClientID %>').value = id;
             document.getElementById('<%= tbImageName.ClientID %>').value = value;
             popup.Hide();
         }
         function ClearImage(id, value) {
             document.getElementById('<%= tbImageId.ClientID %>').value = 0;
             document.getElementById('<%= tbImageName.ClientID %>').value = "";
             popup.Hide();
         }
    </script>
    <div>
    <dx:ASPxPopupControl ClientInstanceName="popup" PopupHorizontalAlign="WindowCenter"
            PopupVerticalAlign="WindowCenter" ID="pcImage" runat="server" ShowHeader="True"
            Modal="True" CssPostfix="1" EnableAnimation="false" AutoUpdatePosition="true"
            HeaderText="Click on an image to select it">
            <ContentCollection>
                <dx:PopupControlContentControl runat="server">
                    <div style="overflow:auto;height:450px;width:530px">
                   <dx:ASPxDataView ID="dvGallery" runat="server" Layout="Flow" AllowPaging="false" CssPostfix="1" ShowVerticalScrollBar="true">
                        <ItemStyle Height="170px" Width="150px" />
                        <ItemTemplate>
                            <div align="center">
                                <table>
                                    <tr> 
                                        <td class="thumbnailImageCell" onclick="SelectImage(<%# Eval("MediaId") %>,'<%# Eval("Name") %>')">
                                            <D:Image ID="imgPhoto" ImageUrl='<%# Eval("ImageUrl") %>' AlternateText='<%# Eval("Name") %>' Height='150px' Width='150px' runat="server"/>
                                        </td>
                                    </tr>
                                </table>
                                <asp:Label Text='<%# Eval("Name") %>' runat="server" ID="lbName" Width='150px' />
                            </div>
                        </ItemTemplate>
                    </dx:ASPxDataView>
                    </div>
                </dx:PopupControlContentControl>
            </ContentCollection>
            <ContentStyle VerticalAlign="Middle" HorizontalAlign="Center" />
            <ModalBackgroundStyle BackColor="Black" />
        </dx:ASPxPopupControl>
        <X:PageControl ID="tabsMain" runat="server" Width="100%">
            <TabPages>
                <X:TabPage Text="Algemeen" Name="Generic">
                    <controls>
                    <D:Panel ID="pnlMessageDetails" runat="server" GroupingText="Message details">
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblName">Naam</D:LabelEntityFieldInfo>
							</td>
							<td class="control" >
								<D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
							</td>
                        </tr>
                        <tr>
                        	<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblTitle">Titel</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbTitle" runat="server" IsRequired="true"></D:TextBoxString>
							</td>
                        </tr>
                        <tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblMessage">Tekst</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxMultiLine runat="server" ID="tbMessage" Rows="4"></D:TextBoxMultiLine>
							</td>
					    </tr>
                        <tr>
                            <td class="label">
                                <D:LabelTextOnly runat="server" ID="lblMessageLayoutType">Message layout type</D:LabelTextOnly>
                            </td>
                            <td class="control">
                                <X:ComboBoxInt ID="ddlMessageLayoutType" runat="server" UseDataBinding="true" DisplayEmptyItem="false" IsRequired="true"></X:ComboBoxInt>
                            </td>                                
                        </tr>
                        <tr>
							<td class="label">
								<D:Label runat="server" id="lblImage">Afbeelding</D:Label>
							</td>
							<td class="control" >
								<D:TextBox ID="tbImageName" runat="server" ReadOnly="true"></D:TextBox>                                
							</td>
							<td class="control">
                                <D:Button ID="btSelectImage" runat="server" Text="Afbeelding selecteren..." UseSubmitBehavior="false" OnClientClick="popup.Show(); return false;" />
                                <D:Button ID="btClearImage" runat="server" Text="Schoonmaken" UseSubmitBehavior="false" OnClientClick="ClearImage(); return false;" />
							</td>
							<td class="control" >
								<D:TextBox ID="tbImageId" runat="server" style="visibility:hidden;"></D:TextBox>
							</td>
                        </tr>                        
                        <tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblDuration">Duration seconds</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxInt ID="tbDuration" runat="server" AllowNegative="false"></D:TextBoxInt>
							</td>
							<td class="label">
							</td>
							<td class="control">
							</td>
                        </tr>	
                    </table>
                    </D:Panel>
                     <D:Panel ID="pnlAction" runat="server" GroupingText="Action">
                        <table class="dataformV2">
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblCategoryId">Category</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection ID="cbCategoryId" runat="server" IncrementalFilteringMode="StartsWith" EntityName="Category" ValueField="CategoryId" TextField="Name" PreventEntityCollectionInitialization="true" UseDataBinding="true"/>
                                </td>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblProductId">Product</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection ID="cbProductId" ClientInstanceName="cbProductId" runat="server" IncrementalFilteringMode="StartsWith" EntityName="Product" ValueField="ProductId" TextField="Name" PreventEntityCollectionInitialization="true" UseDataBinding="true"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblEntertainmentId">Entertainment</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection ID="cbEntertainmentId" runat="server" IncrementalFilteringMode="StartsWith" EntityName="Entertainment" ValueField="EntertainmentId" TextField="Name" PreventEntityCollectionInitialization="true" UseDataBinding="true"/>
                                </td>
                                <td class="label" >
                                    <D:LabelEntityFieldInfo runat="server" id="lblNotifyOnYes">Notify on 'Yes'</D:LabelEntityFieldInfo>
                                 </td>
                                 <td class="control">
                                     <D:CheckBox runat="server" ID="cbNotifyOnYes" />
                                     <small id="lblOnlyProductActions" runat="server"> (only for product actions)</small>
                                 </td>
                            </tr>
				        </table> 
                    </D:Panel>
                    </controls>
                </X:TabPage>
            </TabPages>
        </X:PageControl>
    </div>
</asp:Content>
