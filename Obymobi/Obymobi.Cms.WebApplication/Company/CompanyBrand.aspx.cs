﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;

namespace Obymobi.ObymobiCms.Company
{
    public partial class CompanyBrand : Dionysos.Web.UI.PageLLBLGenEntity
    {
        #region Methods

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            base.OnInit(e);
        }

        private void LoadUserControls()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the data source as company owner entity.
        /// </summary>
        public CompanyBrandEntity DataSourceAsCompanyOwnerEntity
        {
            get
            {
                return this.DataSource as CompanyBrandEntity;
            }
        }

        #endregion

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.PageMode == Dionysos.Web.PageMode.Add)
                throw new ApplicationException(this.Translate("company_brand_should_be_added_via_add_brand_page", "Company brands should be added via the 'Brands' page."));

            if (CmsSessionHelper.GetUserRoleForBrand(this.DataSourceAsCompanyOwnerEntity.BrandId).GetValueOrDefault(BrandRole.Viewer) != BrandRole.Owner)
            {
                throw new ApplicationException("You don't have the permissions to edit this company to brand link");
            }

            this.hlCompany.Text = this.DataSourceAsCompanyOwnerEntity.CompanyName + " &#187;&#187;";
            this.hlCompany.NavigateUrl = this.ResolveUrl(string.Format("~/Company/Company.aspx?id={0}", this.DataSourceAsCompanyOwnerEntity.CompanyId));

            this.hlBrand.Text = this.DataSourceAsCompanyOwnerEntity.BrandName + " &#187;&#187;";
            this.hlBrand.NavigateUrl = this.ResolveUrl(string.Format("~/Generic/Brand.aspx?id={0}", this.DataSourceAsCompanyOwnerEntity.BrandId));
        }

        #endregion
    }
}