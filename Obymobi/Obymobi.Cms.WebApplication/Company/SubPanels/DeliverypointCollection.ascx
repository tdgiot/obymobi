﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.Subpanels.DeliverypointCollection" CodeBehind="DeliverypointCollection.ascx.cs" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<D:Panel ID="pnlDeliverypoints" LocalizeText="false" runat="server" style="padding-bottom: 11px;">
    <table class="dataformV2">
        <tr>
            <td class="label">
                <D:LabelTextOnly runat="server" ID="lblCreateDeliverypoints">Create deliverypoint range</D:LabelTextOnly>
            </td>
            <td class="control">
                <D:TextBoxInt ID="tbFrom" runat="server" ThousandsSeperators="false" Style="width: 30px;" notdirty="true"></D:TextBoxInt>
                -
                <D:TextBoxInt ID="tbTo" runat="server" ThousandsSeperators="false" Style="width: 30px;" notdirty="true"></D:TextBoxInt><br />
                <D:Button ID="btnCreateDeliverypoints" runat="server" Text="Create" style="margin-top: 6px;"/>
            </td>
           <td class="label">
                <D:LabelTextOnly runat="server" ID="lblBatchDeliverypoints" LocalizeText="false">Batch deliverypoints (.csv)</D:LabelTextOnly>
            </td>
            <td class="control">
                <D:FileUpload runat="server" ID="fuDeliverypoints" notdirty="true" style="float: left;" /> 
                <a id="csv-info-popup" href="#" style="float: left; position: relative; top: 0; right: 55px;">
                    <img src="<%= ResolveUrl("~/Images/Icons/information.png") %>" style="float: right;" alt="csv information" />
                </a><br /><br />
                <D:Button ID="btnBatchCreateDeliverypoints" runat="server" Text="Create" notdirty="true" style="float: left; margin-right: 4px" />
                <D:Button ID="btnBatchDeleteDeliverypoints" runat="server" Text="Delete" notdirty="true" style="float: left; margin-right: 4px" />
                <D:CheckBox ID="cbInvertDelete" runat="server" Text="Invert delete" notdirty="true" style="float: left;margin: 0;" />
                <a id="invert-delete-info-popup" href="#" style="float: left;">
                    <div id="invertDeleteIcon" runat="server"> 
                    <img src="<%= ResolveUrl("~/Images/Icons/information.png") %>" style="float: right;" alt="invert delete" />
                    </div>
                </a><br /><br />
            </td>
        </tr>        
    </table>
</D:Panel>

<X:GridViewColumnSelector runat="server" ID="gvcsDeliverypoint">
    <columns />
</X:GridViewColumnSelector>

<dx:ASPxPopupControl ID="pcDeliverypointsCsv" ClientInstanceName="pcDeliverypointsCsv" PopupElementID="csv-info-popup" Width="350" PopupAction="MouseOver" CloseAction="MouseOut" CloseOnEscape="false" Modal="false" ShowOnPageLoad="false" HeaderText="CSV file format" runat="server">
    <ContentCollection>
        <dx:PopupControlContentControl runat="server">
            <p>
                Example.csv<br /><br />
                Number,Name<br />
                1,Room 1<br />
                2,Room 2<br />
                ...<br />
                <br />
                <b>Note: Name column is optional</b>
            </p>
        </dx:PopupControlContentControl>
    </ContentCollection>
</dx:ASPxPopupControl>

<dx:ASPxPopupControl ID="pcInvertDelete" ClientInstanceName="pcInvertDelete" PopupElementID="invert-delete-info-popup" Width="350" PopupAction="MouseOver" CloseAction="MouseOut" CloseOnEscape="false" Modal="false" ShowOnPageLoad="false" HeaderText="Invert delete" runat="server">
    <ContentCollection>
        <dx:PopupControlContentControl runat="server">
            <p>
                Normally all deliverypoints supplied in the csv file will be deleted.<br /><br />
                With 'Invert delete' checked <b>all</b> deliverypoints of the deliverypoint group will be deleted, <b>except</b> the ones supplied in the csv file.
            </p>
        </dx:PopupControlContentControl>
    </ContentCollection>
</dx:ASPxPopupControl>