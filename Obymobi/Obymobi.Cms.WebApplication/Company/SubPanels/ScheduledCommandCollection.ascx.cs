﻿using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Web;
using Dionysos;
using Dionysos.Web.UI.DevExControls;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Obymobi.ObymobiCms.Company.SubPanels
{
    public partial class ScheduledCommandCollection : Dionysos.Web.UI.DevExControls.SubPanelLLBLOverviewDataSourceCollection
    {
        #region Fields

        private TimeZoneInfo timeZoneInfo;

        #endregion

        #region Methods

        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "ScheduledCommand";
            this.EntityPageUrl = "~/Company/ScheduledCommand.aspx";
            base.OnInit(e);

            this.DataSourceLoaded += ScheduledCommandCollection_DataSourceLoaded;
        }

        private void ScheduledCommandCollection_DataSourceLoaded(object sender)
        {
            CompanyEntity company = new CompanyEntity(CmsSessionHelper.CurrentCompanyId);
            timeZoneInfo = company.TimeZoneInfo;

            if (!this.IsPostBack)
                this.SetGridView();

            this.btnDelete.Click += btnDelete_Click;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.DataSource.SorterToUse = new SortExpression(ScheduledCommandFields.Sort | SortOperator.Descending);

            this.MainGridView.GenerateColumnsFromEntityInformation = false;
            this.MainGridView.EmptyEntityOfTypeToDisplay = new ScheduledCommandEntity();
            this.MainGridView.ShowColumnSelectorButton = false;
            this.MainGridView.PreventSaveColumnOrder = true;
            this.MainGridView.SettingsPager.PageSize = 50;
            this.MainGridView.ShowEditColumn = false;
            this.MainGridView.ShowEditHyperlinkColumn = false;
            this.MainGridView.EnableRowDoubleClick = false;
            this.MainGridView.HtmlDataCellPrepared += MainGridView_HtmlDataCellPrepared;

            this.MainGridView.LoadComplete += MainGridView_LoadComplete;
            this.btnRefresh.Click += btnRefresh_Click;
        }

        private void AddTextColumn(string fieldName, string caption, ref int visibleIndex)
        {
            GridViewDataTextColumn textCol = new GridViewDataTextColumn();
            textCol.FieldName = fieldName;
            textCol.Caption = caption;
            textCol.Name = fieldName;
            textCol.VisibleIndex = visibleIndex;
            visibleIndex++;
            textCol.Visible = true;
            this.MainGridView.Columns.Add(textCol);
        }

        private void SetGridView()
        {
            int visibleIndex = 0;

            // Add the selection column
            GridViewCommandColumn selectCol = new GridViewCommandColumn();
            selectCol.ShowSelectCheckbox = true;
            selectCol.Caption = "&nbsp";
            selectCol.Name = "Select";
            selectCol.VisibleIndex = visibleIndex;
            visibleIndex++;
            selectCol.Visible = true;
            selectCol.Width = Unit.Pixel(30);
            this.MainGridView.Columns.Add(selectCol);

            // Add the identifier column
            this.AddTextColumn(ScheduledCommandFields.Sort.Name, "Priority", ref visibleIndex);
            this.AddTextColumn("CommandText", "Command", ref visibleIndex);
            this.AddTextColumn("SubjectText", "Subject", ref visibleIndex);
            this.AddTextColumn("StatusText", "Status", ref visibleIndex);
            this.AddTextColumn(ScheduledCommandFields.StartedUTC.Name, "Starts on (UTC)", ref visibleIndex);
            this.AddTextColumn(ScheduledCommandFields.ExpiresUTC.Name, "Expires on (UTC)", ref visibleIndex);
            this.AddTextColumn(ScheduledCommandFields.CompletedUTC.Name, "Completed on (UTC)", ref visibleIndex);

            GridViewDataColumn colDelete = new GridViewDataColumn();
            colDelete.Name = GridViewEntityCollection.HyperLinkDeleteColumnName;
            colDelete.FieldName = this.MainGridView.EmptyEntityOfTypeToDisplay.PrimaryKeyFields[0].Name;
            colDelete.UnboundType = UnboundColumnType.Bound;
            colDelete.Caption = "&nbsp;";
            colDelete.Settings.AllowSort = DefaultBoolean.False;
            colDelete.Settings.AllowDragDrop = DefaultBoolean.False;
            colDelete.VisibleIndex = visibleIndex;
            visibleIndex++;
            colDelete.Width = Unit.Pixel(16);
            this.MainGridView.Columns.Add(colDelete);
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            // Do nothing, postback is enough
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            List<object> selection = this.MainGridView.GetSelectedFieldValues(this.MainGridView.KeyFieldName);
            foreach (var id in selection)
            {
                ScheduledCommandEntity scheduledCommandEntity = new ScheduledCommandEntity((int)id);
                scheduledCommandEntity.Delete();
            }
        }

        private void MainGridView_LoadComplete(object sender, EventArgs e)
        {
            // Make the sort column smaller
            if (this.MainGridView.Columns["Sort"] != null)
                this.MainGridView.Columns["Sort"].Width = 30;
        }

        private void MainGridView_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridViewTableDataCellEventArgs e)
        {
            ScheduledCommandStatus status = (ScheduledCommandStatus)e.GetValue("Status");
            switch (status)
            {
                case ScheduledCommandStatus.Inactive:
                    e.Cell.ForeColor = Color.Gray;
                    break;
                case ScheduledCommandStatus.Pending:
                    // Normal
                    break;
                case ScheduledCommandStatus.Started:
                    e.Cell.ForeColor = Color.Blue;
                    break;
                case ScheduledCommandStatus.Succeeded:
                    e.Cell.ForeColor = Color.Green;
                    break;
                case ScheduledCommandStatus.Expired:
                    e.Cell.ForeColor = Color.Red;
                    break;
                case ScheduledCommandStatus.Failed:
                    e.Cell.ForeColor = Color.Red;
                    break;
            }

            if (e.DataColumn.FieldName == "SubjectText")
            {
                string text = e.CellValue.ToString();
                string url = string.Empty;
                string id = string.Empty;
                if (text.StartsWith("Client "))
                {
                    id = text.Substring(7);
                    url = string.Format("Client.aspx?id={0}", id);
                    e.Cell.Text = string.Format("Client <a href=\"{0}\" target=\"_blank\" >{1}</a>", url, id);
                }
                else if (text.StartsWith("Terminal "))
                {
                    id = text.Substring(9);
                    url = string.Format("Terminal.aspx?id={0}", id);
                    e.Cell.Text = string.Format("Terminal <a href=\"{0}\" target=\"_blank\" >{1}</a>", url, id);
                }
            }
            else if (e.DataColumn.FieldName == ScheduledCommandFields.ExpiresUTC.Name)
            {
                ASPxGridView gridView = (ASPxGridView)sender;
                string statusText = (string)gridView.GetRowValues(e.VisibleIndex, "StatusText");
                if (statusText == "Succeeded")
                {
                    e.Cell.Text = "Completed";
                }
                else if (statusText == "Expired" || statusText == "Failed")
                {
                    e.Cell.Text = "Completed with error";
                }
                else
                {
                    DateTime? dateTimeValue = (DateTime?)e.CellValue;
                    if (dateTimeValue.HasValue)
                    {
                        DateTime convertedDateTime = dateTimeValue.Value.UtcToLocalTime(this.timeZoneInfo);
                        DateTime companyNow = DateTime.UtcNow.UtcToLocalTime(this.timeZoneInfo);
                        
                        e.Cell.Text = string.Format("{0} ({1})", convertedDateTime.ToString(), DateTimeUtil.GetSpanTextBetweenDateTimeAndUtcNow(convertedDateTime, companyNow));
                    }
                    else
                    {
                        e.Cell.Text = string.Empty;
                    }
                }
            }
            else if (e.DataColumn.FieldName == ScheduledCommandFields.StartedUTC.Name || e.DataColumn.FieldName == ScheduledCommandFields.CompletedUTC.Name)
            {
                DateTime? dateTimeValue = (DateTime?)e.CellValue;
                if (dateTimeValue.HasValue)
                {
                    DateTime convertedDateTime = dateTimeValue.Value.UtcToLocalTime(this.timeZoneInfo);
                    DateTime companyNow = DateTime.UtcNow.UtcToLocalTime(this.timeZoneInfo);

                    e.Cell.Text = string.Format("{0} ({1})", convertedDateTime.ToString(), DateTimeUtil.GetSpanTextBetweenDateTimeAndUtcNow(convertedDateTime, companyNow));
                }
                else
                {
                    e.Cell.Text = string.Empty;
                }
            }
        }

        #endregion
    }
}