﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Obymobi.Data.EntityClasses;
using Dionysos.Web;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Dionysos;
using Obymobi.Enums;
using ServiceStack;
using ServiceStack.Text;
using Dionysos.Data.LLBLGen;
using Dionysos.Web.UI;

namespace Obymobi.ObymobiCms.Company.Subpanels
{
    public partial class DeliverypointCollection : Dionysos.Web.UI.DevExControls.SubPanelLLBLOverviewDataSourceCollection
    {
        private const int MAX_BATCH_SIZE = 1000;

        private DeliverypointgroupEntity DataSourceAsDeliverypointgroupEntity => PageAsPageLLBLGenEntity.DataSource as DeliverypointgroupEntity;

        private void HookUpEvents()
        {
            MainGridView.CustomColumnSort += MainGridView_CustomColumnSort;
            btnCreateDeliverypoints.Click += (sender, args) => CreateDeliverypoints();
            btnBatchCreateDeliverypoints.Click += (sender, args) => TryExecuteActionForImportedDeliverypoints(BatchDeliverypointsAction.Create);
            btnBatchDeleteDeliverypoints.Click += (sender, args) => TryExecuteActionForImportedDeliverypoints(BatchDeliverypointsAction.Delete);
        }        

        private void TryExecuteActionForImportedDeliverypoints(BatchDeliverypointsAction batchDeliverypointsAction)
        {
            if (!fuDeliverypoints.HasFile)
            {
                AddInformator(InformatorType.Warning, "No file selected");
                return;
            }

            if (!Path.GetExtension(fuDeliverypoints.FileName).EndsWith(".csv"))
            {
                AddInformator(InformatorType.Warning, "Only csv files are supported");
                return;
            }

            IEnumerable<Deliverypoint> deliverypoints =
                CsvSerializer.DeserializeFromStream<List<Deliverypoint>>(fuDeliverypoints.FileContent)
                .Where(deliverypoint => !deliverypoint.Number.IsNullOrWhiteSpace());

            ExecuteActionForImportedDeliverypoints(batchDeliverypointsAction, deliverypoints, cbInvertDelete.Checked);
        }

        private void ExecuteActionForImportedDeliverypoints(BatchDeliverypointsAction batchDeliverypointsAction, IEnumerable<Deliverypoint> deliverypoints, bool invertDelete)
        {
            switch (batchDeliverypointsAction)
            {
                case BatchDeliverypointsAction.Create:
                    CreateDeliverypoints(deliverypoints);
                    break;
                case BatchDeliverypointsAction.Delete:
                    DeleteDeliverypoints(deliverypoints, invertDelete);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(batchDeliverypointsAction), batchDeliverypointsAction, "Batch deliverypoints action not supported.");
            }
        }

        private void CreateDeliverypoints(IEnumerable<Deliverypoint> deliverypoints)
        {
            int deliverypointsCreated = deliverypoints
                    .Where(deliverypoint => !DataSourceAsDeliverypointgroupEntity.DeliverypointCollection
                        .Select(deliverypointEntity => deliverypointEntity.Number)
                        .Contains(deliverypoint.Number))
                    .Select(deliverypoint => new DeliverypointEntity
                    {
                        EnableAnalytics = true,
                        CompanyId = DataSourceAsDeliverypointgroupEntity.CompanyId,
                        DeliverypointgroupId = DataSourceAsDeliverypointgroupEntity.DeliverypointgroupId,
                        Number = deliverypoint.Number,
                        Name = !deliverypoint.Name.IsNullOrWhiteSpace()
                            ? deliverypoint.Name
                            : deliverypoint.Number
                    })
                    .ToEntityCollection<Data.CollectionClasses.DeliverypointCollection>()
                    .SaveMulti();

            AddInformator(InformatorType.Information, $"{deliverypointsCreated} deliverypoint(s) created");
        }

        private void DeleteDeliverypoints(IEnumerable<Deliverypoint> deliverypoints, bool invertDelete)
        {
            IEnumerable<string> deliverypointNumbers = deliverypoints.Select(deliverypoint => deliverypoint.Number);

            int deliverypointsDeleted = DataSourceAsDeliverypointgroupEntity.DeliverypointCollection
                    .Where(deliverypointEntity => invertDelete
                        ? !deliverypointNumbers.Contains(deliverypointEntity.Number)
                        : deliverypointNumbers.Contains(deliverypointEntity.Number))
                    .ToEntityCollection<Data.CollectionClasses.DeliverypointCollection>()
                    .DeleteMulti();
            
            AddInformator(InformatorType.Information, $"{deliverypointsDeleted} deliverypoint(s) deleted");
        }

        private void CreateDeliverypoints()
        {
            int deliverypointgroupId;
            int from;
            int to;
            if (!QueryStringHelper.GetInt("id", out deliverypointgroupId))
            {
                // We dont know the parent 
                AddInformator(InformatorType.Warning, Translate("DeliverypointCollection.ExistingDeliverypointgroupOnly", "Deliverypoints can only be added to an existing deliverypointgroup."));
            }
            else if (!int.TryParse(this.tbFrom.Text, out from))
            {
                AddInformator(InformatorType.Warning, Translate("DeliverypointCollection.FromMissing", "Fill in a start number"));
            }
            else if (!int.TryParse(this.tbTo.Text, out to))
            {
                AddInformator(InformatorType.Warning, Translate("DeliverypointCollection.ToMissing", "Fill in an end number"));
            }
            else if (from <= 0 || to <= 0)
            {
                AddInformator(InformatorType.Warning, "The start and end of the deliverypoint range must be greater than 0");
            }
            else
            {
                int tmpTo = to;
                if (from > to)
                {
                    to = from;
                    from = tmpTo;
                    this.tbTo.Text = to.ToString();
                    this.tbFrom.Text = from.ToString();
                }

                int count = (to - from) + 1; // Include the "To" in the range
                List<int> range = Enumerable.Range(from, count).ToList();

                if (CmsSessionHelper.CurrentRole != Role.GodMode && range.Count() > MAX_BATCH_SIZE)
                {
                    AddInformator(InformatorType.Warning, $"Only {MAX_BATCH_SIZE} deliverypoints can be created in a single batch to prevent system flooding");
                    return;
                }

                DeliverypointgroupEntity deliverypointgroup = new DeliverypointgroupEntity(deliverypointgroupId);

                PredicateExpression filter = new PredicateExpression(DeliverypointFields.DeliverypointgroupId == deliverypointgroupId);
                IncludeFieldsList includes = new IncludeFieldsList(DeliverypointFields.Number);

                Data.CollectionClasses.DeliverypointCollection deliverypoints = new Data.CollectionClasses.DeliverypointCollection();
                deliverypoints.GetMulti(filter, includes, null);

                IEnumerable<string> existingNumbers = deliverypoints.Select(dp => dp.Number).ToList<string>();

                IEnumerable<int> numbersToCreate = range.Where(x => !existingNumbers.Contains(x.ToString())).ToList();
                IEnumerable<int> numbersNotToCreate = range.Where(x => existingNumbers.Contains(x.ToString())).ToList();

                foreach (int deliverypointNumber in numbersToCreate)
                {
                    DeliverypointEntity dp = new DeliverypointEntity();
                    dp.CompanyId = deliverypointgroup.CompanyId;
                    dp.DeliverypointgroupId = deliverypointgroupId;
                    dp.Number = deliverypointNumber.ToString();
                    dp.Name = deliverypointNumber.ToString();
                    dp.EnableAnalytics = true;
                    dp.Save();
                }

                if (numbersNotToCreate.Any())
                {
                    string numbers = StringUtil.CombineWithCommaSpace(numbersNotToCreate.Join(", "));
                    AddInformator(InformatorType.Warning, string.Format("The deliverypoint(s) '{0}' couldn't be created because they already exists for this deliverypoint group.", numbers));
                }
                else
                {
                    AddInformator(InformatorType.Information, Translate("DeliverypointCollection.DeliverypointsCreated", "The deliverypoints have been created!"));
                }
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            EntityName = "Deliverypoint";
            EntityPageUrl = "~/Company/Deliverypoint.aspx";

            invertDeleteIcon.Visible = CmsSessionHelper.CurrentRole == Role.GodMode;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            HookUpEvents();

            cbInvertDelete.Visible = CmsSessionHelper.CurrentRole == Role.GodMode;
        }

        public void ParentLoadCompleted()
        {
            DevExpress.Web.GridViewDataColumn column = this.MainGridView.Columns["Number"] as DevExpress.Web.GridViewDataColumn;
            if (column != null)
            {
                column.Settings.SortMode = DevExpress.XtraGrid.ColumnSortMode.Custom;
            }
        }

        private void MainGridView_CustomColumnSort(object sender, DevExpress.Web.CustomColumnSortEventArgs e)
        {
            if (e.Column.FieldName == "Number")
            {
                long numVal1 = 0, numVal2 = 0;
                try
                {
                    numVal1 = Convert.ToInt64(e.Value1);
                    numVal2 = Convert.ToInt64(e.Value2);

                    if (numVal1 < numVal2)
                        e.Result = -1;
                    else if (numVal1 > numVal2)
                        e.Result = 1;
                    else
                        e.Result = 0;
                }
                catch
                {
                    e.Result = 0;
                }

                e.Handled = true;
            }
        }

        private class Deliverypoint
        {
            public string Name { get; set; }
            public string Number { get; set; }
        }

        private enum BatchDeliverypointsAction
        {
            Create,
            Delete
        }
    }
}
