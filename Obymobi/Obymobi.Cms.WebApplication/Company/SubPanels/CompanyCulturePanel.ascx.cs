﻿using System;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using DevExpress.Web;
using Obymobi.Enums;

namespace Obymobi.ObymobiCms.Company.SubPanels
{
    public partial class CompanyCulturePanel : Dionysos.Web.UI.WebControls.SubPanelLLBLGenEntity
    {
        #region Properties

        public CompanyEntity ParentDataSourceAsCompanyEntity
        {
            get
            {
                return this.PageAsPageLLBLGenEntity.DataSource as CompanyEntity;
            }
        }

        public string CultureCode { get; set; }
        public bool IsDefaultCulture;

        #endregion

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "CompanyCulture";
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.SetGui();
        }

        #endregion

        #region Methods

        protected void SetGui()
        {
            this.tbCultureCode.Text = this.CultureCode;
            this.tbDescription.Enabled = !this.IsDefaultCulture;
            this.tbDescriptionSingleLine.Enabled = !this.IsDefaultCulture;
        }

        private void PopulateCustomTexts()
        {
            CustomTextRenderer renderer = new CustomTextRenderer(this.ParentDataSourceAsCompanyEntity, this.CultureCode);
            
            renderer.RenderCustomText(this.tbDescriptionSingleLine, CustomTextType.CompanyDescriptionShort);
            renderer.RenderCustomText(this.tbDescription, CustomTextType.CompanyDescription);
            renderer.RenderCustomText(this.tbVenuePageDescription, CustomTextType.CompanyVenuePageDescription);
        }

        protected override void OnParentDataSourceLoaded()
        {
            base.OnParentDataSourceLoaded();
            this.PopulateCustomTexts();
        }

        public override bool Save()
        {
            this.SaveCustomTexts();
            return false;
        }

        private void SaveCustomTexts()
        {
            CustomTextRenderer renderer = new CustomTextRenderer(this.ParentDataSourceAsCompanyEntity, this.CultureCode);

            renderer.SaveCustomText(this.tbDescriptionSingleLine, CustomTextType.CompanyDescriptionShort);
            renderer.SaveCustomText(this.tbDescription, CustomTextType.CompanyDescription);
            renderer.SaveCustomText(this.tbVenuePageDescription, CustomTextType.CompanyVenuePageDescription);
        }        

        #endregion
    }
}