﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.SubPanels.RoutestephandlerCollection" Codebehind="RoutestephandlerCollection.ascx.cs" %>
<D:PlaceHolder ID="plhAddRoutestephandler" runat="server">
<div style="overflow:hidden;margin-bottom:10px;">
   <div style="float:left;margin-right:10px;">
   <X:ComboBoxInt ID="ddlHandlerType" runat="server" UseDataBinding="true" Width="150px" notdirty="true"></X:ComboBoxInt>
   </div>
   <div style="float:left;">
   <D:Button runat="server" ID="btAdd" CommandName="Add" Text="Nieuwe toevoegen" ToolTip="Toevoegen" />
   </div>   
</div>
</D:PlaceHolder>
<X:GridViewColumnSelector runat="server" ID="gvcsRoutestephandler">
   <Columns />
</X:GridViewColumnSelector>