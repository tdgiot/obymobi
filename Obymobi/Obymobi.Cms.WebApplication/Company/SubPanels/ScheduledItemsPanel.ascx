﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Company_SubPanels_ScheduledItemsPanel" Codebehind="ScheduledItemsPanel.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.ASPxScheduler.v20.1" Namespace="DevExpress.Web.ASPxScheduler" TagPrefix="dx" %>

<table class="dataformV2">
    <tr>
		<td class="label">
			<D:LabelEntityFieldInfo runat="server" id="lblFilter">Filter</D:LabelEntityFieldInfo>
		</td>
		<td class="control">
			<dxe:ASPxComboBox ID="cbFilter" runat="server" SelectedIndex="0" ValueType="System.String" notdirty="true">
                <Items>
                    <dxe:ListEditItem Selected="True" Text="Show All" Value="all" />
                    <dxe:ListEditItem Text="Widgets only" Value="widgets" />
                    <dxe:ListEditItem Text="Slides only" Value="slides" />
                    <dxe:ListEditItem Text="Messages only" Value="messages" />
                </Items>
                <ClientSideEvents SelectedIndexChanged="function(s, e) {
	                hfFilter.value = s.GetValue();
	                scheduler.Refresh();
                }" />
            </dxe:ASPxComboBox>
		</td>	
        <td class="label">
		</td>
		<td class="control">
            <D:Button runat="server" ID="btAddItem" Text="Add Item" OnClientClick="scheduler.RaiseCallback('MNUVIEW|NewAppointment'); return false;"  />
            <asp:HiddenField ID="hfFilter" ClientIDMode="Static" runat="server" />
		</td>	
	</tr>		
</table>
<small>Note: Hold 'Ctrl' while dragging an item to copy the item.</small>
<dx:ASPxScheduler ID="scheduler" runat="server" Width="100%" ActiveViewType="Timeline" ClientIDMode="static" AppointmentDataSourceID="scheduleDataSource">
    <OptionsToolTips AppointmentToolTipUrl="~/Company/SubPanels/ScheduleAppointmentTooltip.ascx" AppointmentToolTipCornerType="None" />
    <OptionsBehavior ShowViewSelector="true" ShowFloatingActionButton="False"/>
    <Storage>
    <Appointments>
        <Mappings 
            AppointmentId="UIScheduleItemOccurrenceId" 
            Start="StartTime" 
            End="EndTime" 
            Subject="Subject" 
            AllDay="AllDay" 
            Description="Description" 
            Label="Label" 
            Location="Location" 
            RecurrenceInfo="RecurrenceInfo" 
            ReminderInfo="ReminderInfo" 
            Status="Status"                 
            Type="Type" />
        <CustomFieldMappings>
            <dx:ASPxAppointmentCustomFieldMapping Member="UIWidgetId" Name="UIWidgetId" ValueType="Integer" />
            <dx:ASPxAppointmentCustomFieldMapping Member="MediaId" Name="MediaId" ValueType="Integer" />
            <dx:ASPxAppointmentCustomFieldMapping Member="ScheduledMessageId" Name="ScheduledMessageId" ValueType="Integer" />
            <dx:ASPxAppointmentCustomFieldMapping Member="MessagegroupId" Name="MessagegroupId" ValueType="Integer" />
            <dx:ASPxAppointmentCustomFieldMapping Member="BackgroundColor" Name="BackgroundColor" ValueType="Integer" />
            <dx:ASPxAppointmentCustomFieldMapping Member="TextColor" Name="TextColor" ValueType="Integer" />                
        </CustomFieldMappings>
    </Appointments>
    </Storage>
    <OptionsForms AppointmentFormTemplateUrl="~/Company/SubPanels/MarketingScheduleAppointment.ascx" />    
</dx:ASPxScheduler>
<asp:ObjectDataSource ID="scheduleDataSource" runat="server" 
    DataObjectTypeName="MarketingAppointment"
    DeleteMethod="DeleteMethodHandler"
    InsertMethod="InsertMethodHandler" 
    SelectMethod="SelectMethodHandler" 
    UpdateMethod="UpdateMethodHandler" 
    TypeName="MarketingScheduleDataSource" 
    onobjectcreated="scheduleDataSource_ObjectCreated"> 
</asp:ObjectDataSource> 
<script type="text/javascript">
    // <![CDATA[
    scheduler.menuManager.UpdateAppointmentMenuItems = function (appointmentViewInfo, menu)
    {
        var apt = this.scheduler.GetAppointment(appointmentViewInfo.appointmentId);
        if (apt == null)
            return;
        var aptType = apt.appointmentType;
        var isRecurring = aptType == ASPxAppointmentType.Pattern || aptType == ASPxAppointmentType.Occurrence;
        var isChanged = aptType == ASPxAppointmentType.ChangedOccurrence;
        
        menu.GetItemByName("EditPattern").SetVisible(isRecurring);
        menu.GetItemByName("RestoreItem").SetVisible(isChanged);
        return this.constructor.prototype.UpdateAppointmentMenuItems.call(this, appointmentViewInfo, menu);
    }

    function DefaultAppointmentMenuHandler(control, s, args)
    {
        if (args.item.name == "AddItem")
        {
            control.RaiseCallback('MNUVIEW|NewAppointment');
        }
        else if (args.item.name == "EditItem")
        {
            control.RaiseCallback('MNUAPT|OpenAppointment');            
        }
        else if (args.item.name == "EditPattern")
        {
            control.RaiseCallback('MNUAPT|EditSeries');
        }
        else if (args.item.name == "RestoreItem")
        {
            control.RaiseCallback('MNUAPT|RestoreOccurrence');
        }
        else if (args.item.name == "DeleteAppointment")
        {
            control.RaiseCallback("MNUAPT|DeleteAppointment");
        }
    }
    // ]]> 
</script>