﻿using Dionysos.Web.UI;
using Obymobi.Data.EntityClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.Web;
using Obymobi;
using Dionysos.Web.UI.WebControls;

public partial class Company_SubPanels_CompanyCultureCollection : Dionysos.Web.UI.WebControls.SubPanelLLBLGenEntity
{
    #region Fields

    private readonly List<TextBoxDecimal> cultureTextBoxes = new List<TextBoxDecimal>();

    #endregion

    #region Methods

    protected override void OnInit(EventArgs e)
    {
        this.EntityName = "CompanyCulture";
        this.LoadUserControls();
        base.OnInit(e);        
    }

    private void LoadUserControls()
    {

    }

    protected override void OnParentDataSourceLoaded()
    {
        base.OnParentDataSourceLoaded();

        this.FillAllCultures();
    }    

    private void FillAllCultures()
    {
        List<string> cultureCodes = this.DataSourceAsCompanyEntity.CompanyCultureCollection.Select(x => x.CultureCode).ToList();
        
        this.lbCultures.DataSource = Obymobi.Culture.Mappings.Values.OrderByDescending(x => cultureCodes.Contains(x.Code)).ThenBy(x => x.NameAndCultureCode);
        this.lbCultures.DataBind();

        foreach (ListEditItem item in this.lbCultures.Items)
        {
            if (cultureCodes.Contains(item.Value))
            {
                item.Selected = true;
            }
        }
    }    

    public override bool Save()
    {        
        this.SaveCultureCodes(this.lbCultures.Items);

        return true;
    }

    private void SaveCultureCodes(ListEditItemCollection items)
    {
        string defaultCultureCode = this.DataSourceAsCompanyEntity.CultureCode;

        foreach (ListEditItem item in items)
        {
            string cultureCode = item.Value.ToString();

            CompanyCultureEntity culture = this.DataSourceAsCompanyEntity.CompanyCultureCollection.FirstOrDefault(x => x.CultureCode == cultureCode);
            if (item.Selected && culture == null)
            {
                culture = new CompanyCultureEntity();
                culture.CompanyId = CmsSessionHelper.CurrentCompanyId;
                culture.CultureCode = cultureCode;
                culture.Save();
            }
            else if (!item.Selected && culture != null && !culture.CultureCode.Equals(defaultCultureCode, StringComparison.InvariantCultureIgnoreCase))
            {
                culture.Delete();
                this.DataSourceAsCompanyEntity.CompanyCultureCollection.Remove(culture);
            }
        }
    }    

    #endregion
    
    #region Properties

    public new PageLLBLGenEntity Page
    {
        get
        {
            return base.Page as PageLLBLGenEntity;
        }
    }

    public CompanyEntity DataSourceAsCompanyEntity
    {
        get
        {
            return this.Page.DataSource as CompanyEntity;
        }
    }    

    #endregion
}