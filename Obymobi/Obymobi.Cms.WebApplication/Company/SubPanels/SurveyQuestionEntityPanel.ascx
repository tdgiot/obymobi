﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.Subpanels.SurveyQuestionEntityPanel" Codebehind="SurveyQuestionEntityPanel.ascx.cs" %>
<table class="dataformV2">
	<tr>
		<td class="label">
			<D:Label runat="server" ID="lblQuestionName">Vraag</D:Label>
		</td>
		<td class="control" colspan="3">
			<D:TextBoxString ID="tbQuestionName" runat="server"></D:TextBoxString>
		</td>
	</tr>
	<tr>
		<td class="label">
			<D:Label runat="server" ID="lblAnswer1">Antwoord 1</D:Label>
		</td>
		<td class="control">
			<D:TextBoxString ID="tbAnswer1" runat="server"></D:TextBoxString>
		</td>
	</tr>
	<tr>
		<td class="label">
			<D:Label runat="server" ID="lblAnswer2">Antwoord 2</D:Label>
		</td>
		<td class="control">
			<D:TextBoxString ID="tbAnswer2" runat="server"></D:TextBoxString>
		</td>
	</tr>
	<tr>
		<td class="label">
			<D:Label runat="server" ID="lblAnswer3">Antwoord 3</D:Label>
		</td>
		<td class="control">
			<D:TextBoxString ID="tbAnswer3" runat="server"></D:TextBoxString>
		</td>
	</tr>
	<tr>
		<td class="label">
			<D:Label runat="server" ID="lblAnswer4">Antwoord 4</D:Label>
		</td>
		<td class="control">
			<D:TextBoxString ID="tbAnswer4" runat="server"></D:TextBoxString>
		</td>
	</tr>
	<tr>
		<td class="label">
			<D:Label runat="server" ID="lblAnswer5">Antwoord 5</D:Label>
		</td>
		<td class="control">
			<D:TextBoxString ID="tbAnswer5" runat="server"></D:TextBoxString>
		</td>	
	</tr>
	<tr>
		<td class="label">
			&nbsp;
		</td>
		<td class="control">
			&nbsp;
		</td>	
		<td class="label">
			&nbsp;
		</td>
		<td class="control">
			&nbsp;
		</td>	
	</tr>
</table>

