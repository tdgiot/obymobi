﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.SubPanels.StationCollection" Codebehind="StationCollection.ascx.cs" %>
<div style="padding-bottom: 11px;">	
    <D:Panel ID="pnlName" runat="server" GroupingText="Naam" CssClass="alteration_panel">
        <table class="dataformV2">
            <tr>
                <td class="label">
                    <D:TextBoxString ID="tbStationName" runat="server" LocalizeDefaultValue="false" DefaultValue="Name" notdirty="true" TabIndex="1"></D:TextBoxString>
                </td>
            </tr>
        </table>
    </D:Panel>
    <D:Panel ID="pnlChannel" runat="server" GroupingText="Channel" CssClass="alteration_panel">
        <table class="dataformV2">
	        <tr>
		        <td class="label">
                    <D:TextBoxString ID="tbChannel" runat="server" LocalizeDefaultValue="false" DefaultValue="Channel" notdirty="true" TabIndex="2"></D:TextBoxString>
		        </td>            
	        </tr>            
        </table>
    </D:Panel>
    <D:Panel ID="pnlSortOrderLabel" runat="server" GroupingText="Sort order" CssClass="alteration_panel">
        <table class="dataformV2">
            <tr>
                <td class="label">
                    <D:TextBoxInt ID="tbSortOrder" runat="server" LocalizeDefaultValue="false" ThousandsSeperators="false" notdirty="true" TabIndex="2">1</D:TextBoxInt>
                </td>            
            </tr>            
        </table>
    </D:Panel>
    <D:Panel ID="pnlActions" runat="server" GroupingText="" CssClass="alteration_panel">
        <table class="dataformV2" style="margin-top:13px;">
	        <tr>           
                <td class="label" style="text-align:left;">
                    <D:Button runat="server" ID="btnAdd" Text="Toevoegen" ToolTip="Toevoegen" Style="margin-top: 3px;" TabIndex="3" />
                </td>
	        </tr>
        </table>    
    </D:Panel>    
</div>
<X:GridViewColumnSelector runat="server" ID="gvcsStation">
	<Columns />
</X:GridViewColumnSelector>

