﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.SubPanels.SurveyQuestionCollection" Codebehind="SurveyQuestionCollection.ascx.cs" %>
<div style="overflow:hidden;margin-bottom:10px;">
   <div style="float:left;margin-right:10px;">
   <X:ComboBoxInt ID="ddlQuestionType" runat="server" UseDataBinding="true" Width="150px" notdirty="true"></X:ComboBoxInt>
   </div>
   <div style="float:left;">
   <D:Button runat="server" ID="btAdd" CommandName="Add" Text="Nieuwe toevoegen" ToolTip="Toevoegen" />
   </div>
   
</div>
<X:GridViewColumnSelector runat="server" ID="gvcsQuestions">
   <Columns />
</X:GridViewColumnSelector>