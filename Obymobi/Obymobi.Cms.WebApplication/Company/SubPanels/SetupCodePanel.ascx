﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.SubPanels.SetupCodePanel" Codebehind="SetupCodePanel.ascx.cs" %>
<table class="dataformV2">
	<tr>
		<td class="label">
			<D:Label runat="server" ID="lblDeliverypointgroup">Deliverypointgroup</D:Label>
		</td>
		<td class="control">
			<X:ComboBoxString runat="server" ID="cbDeliverypointgroup" TextField="Name" DisplayEmptyItem="False" />
		</td>				
        <td class="label"></td>
        <td class="control"></td>
	</tr>
	<tr>
		<td class="label">
			<D:Label runat="server" ID="Label1">Expire At</D:Label>
		</td>
		<td class="control">
			<X:DateTimeEdit runat="server" ID="deExpireAt" AllowNull="false"></X:DateTimeEdit>
		</td>
	</tr>
    <tr>
		<td class="label">
		</td>
		<td class="control">
			<D:Button runat="server" ID="btnGenerateSetupCode" Text="Generate Setup Code" LocalizeText="True"/>
		</td>							
	</tr>
</table>

<X:GridViewColumnSelector runat="server" ID="gvcsSetupCodeCollection">
	<Columns />
</X:GridViewColumnSelector>