﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Data.CollectionClasses;
using Dionysos.Interfaces;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Dionysos.Web.UI.WebControls;
using Dionysos.Diagnostics;
using Obymobi.Data.EntityClasses;
using Dionysos.Web.UI;
using DevExpress.Web.ASPxTreeList;
using System.Data;
using Dionysos.Web;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Dionysos;
using Obymobi.Cms.Logic.HelperClasses;

namespace Obymobi.ObymobiCms.Company.SubPanels
{
    public partial class UIWidgetCulturePanel : System.Web.UI.UserControl, Dionysos.Interfaces.ISaveableControl
    {
        #region Properties

        public new PageLLBLGenEntity Page
        {
            get
            {
                return base.Page as PageLLBLGenEntity;
            }
        }

        public UIWidgetEntity ParentDataSourceAsUIWidgetEntity
        {
            get
            {
                return this.Page.DataSource as UIWidgetEntity;
            }
        }

        public string CultureCode { get; set; }
        
        public bool IsDefaultCultureCode;

        #endregion

        #region Event Handlers
        
        protected override void OnInit(EventArgs e)
        {
            //this.EntityName = "CustomText";
            base.OnInit(e);
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            this.LoadUserControls();
        }

        #endregion

        #region Methods

        private void LoadUserControls()
        {
            this.tbCultureName.Text = Obymobi.Culture.Mappings[this.CultureCode].NameAndCultureCode;
            this.tbCaption.Enabled = !this.IsDefaultCultureCode;
            this.tbDialogTitle.Enabled = !this.IsDefaultCultureCode;
            this.tbTopText.Enabled = !this.IsDefaultCultureCode;
            this.tbBottomText.Enabled = !this.IsDefaultCultureCode;            
        }

        protected void SetGui()
        {
            UIWidgetType type = this.ParentDataSourceAsUIWidgetEntity.Type;
            switch (type)
            {
                case UIWidgetType.Availability2x1:
                case UIWidgetType.Availability2x2:
                    this.RenderAvailabilityPanel();
                    break;
                case UIWidgetType.ConnectBtSpeaker1x1:
                case UIWidgetType.ConnectBtSpeaker2x1:
                case UIWidgetType.ConnectBtSpeaker2x2:
                    this.RenderBluetoothSpeakerPanel();
                    break;
            }                           
            this.PopulateCustomTexts();
        }

        private void RenderAvailabilityPanel()
        {
            Dictionary<CustomTextType, string> customTexts = this.ParentDataSourceAsUIWidgetEntity.CustomTextCollection.ToCultureCodeSpecificDictionary(this.CultureCode);
            this.tbDialogTitle.Text = customTexts.GetValueOrDefault(CustomTextType.UIWidgetFieldValue1);
            string fieldValue2 = customTexts.GetValueOrDefault(CustomTextType.UIWidgetFieldValue2);
            if (!fieldValue2.IsNullOrWhiteSpace())
            {
                string[] texts = fieldValue2.Split(new string[] { UIWidgetHelper.AVAILABILITY_PLACEHOLDER }, StringSplitOptions.None);
                if (texts.Length > 0)
                {
                    this.tbTopText.Text = texts[0];
                }
                if (texts.Length > 1)
                {
                    this.tbBottomText.Text = texts[1];
                }
            }
            this.plhAvailability.Visible = true;
        }

        private void RenderBluetoothSpeakerPanel()
        {
            Dictionary<CustomTextType, string> customTexts = this.ParentDataSourceAsUIWidgetEntity.CustomTextCollection.ToCultureCodeSpecificDictionary(this.CultureCode);
            this.tbConnectedText.Text = customTexts.GetValueOrDefault(CustomTextType.UIWidgetFieldValue2);
            this.tbConnectingText.Text = customTexts.GetValueOrDefault(CustomTextType.UIWidgetFieldValue3);
            this.tbDisconnectedText.Text = customTexts.GetValueOrDefault(CustomTextType.UIWidgetFieldValue4);
            this.tbDisconnectingText.Text = customTexts.GetValueOrDefault(CustomTextType.UIWidgetFieldValue5);
            this.plhBluetoothSpeaker.Visible = true;
        }

        private void PopulateCustomTexts()
        {
            CustomTextRenderer renderer = new CustomTextRenderer(this.ParentDataSourceAsUIWidgetEntity, this.CultureCode);
            renderer.RenderCustomText(this.tbCaption, CustomTextType.UIWidgetCaption);
        }

        protected override void CreateChildControls()
        {
            this.SetGui();
            base.CreateChildControls();
        }

        public bool Save()
        {
            CustomTextRenderer renderer = new CustomTextRenderer(this.ParentDataSourceAsUIWidgetEntity, this.CultureCode);
            renderer.SaveCustomText(this.tbCaption, CustomTextType.UIWidgetCaption);

            UIWidgetType type = this.ParentDataSourceAsUIWidgetEntity.Type;
            switch (type)
            {
                case UIWidgetType.Availability2x1:
                case UIWidgetType.Availability2x2:
                    this.SaveAvailabilityPanel(renderer);
                    break;
                case UIWidgetType.ConnectBtSpeaker1x1:
                case UIWidgetType.ConnectBtSpeaker2x1:
                case UIWidgetType.ConnectBtSpeaker2x2:
                    this.SaveBluetoothSpeakerPanel(renderer);
                    break;
            }            

            return true;
        }

        private void SaveAvailabilityPanel(CustomTextRenderer renderer)
        {
            renderer.SaveCustomText(this.tbDialogTitle, CustomTextType.UIWidgetFieldValue1);
            if (!this.tbTopText.Text.IsNullOrWhiteSpace() || !this.tbBottomText.Text.IsNullOrWhiteSpace())
            {
                this.tbTopText.Text = string.Concat(this.tbTopText.Text, UIWidgetHelper.AVAILABILITY_PLACEHOLDER, this.tbBottomText.Text);
            }
            else if (this.tbTopText.Text.IsNullOrWhiteSpace() && this.tbBottomText.Text.IsNullOrWhiteSpace())
            {
                this.tbTopText.Text = string.Empty;
            }
            renderer.SaveCustomText(this.tbTopText, CustomTextType.UIWidgetFieldValue2);
        }

        private void SaveBluetoothSpeakerPanel(CustomTextRenderer renderer)
        {
            renderer.SaveCustomText(this.tbConnectedText, CustomTextType.UIWidgetFieldValue2);
            renderer.SaveCustomText(this.tbConnectingText, CustomTextType.UIWidgetFieldValue3);
            renderer.SaveCustomText(this.tbDisconnectedText, CustomTextType.UIWidgetFieldValue4);
            renderer.SaveCustomText(this.tbDisconnectingText, CustomTextType.UIWidgetFieldValue5);
        }

        public void Initialize()
        {
            this.SetGui();            
        }

        #endregion
    }
}