﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using Dionysos.Web.UI;
using Dionysos.Web.UI.DevExControls;
using Dionysos.Web;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Dionysos;

namespace Obymobi.ObymobiCms.Company.SubPanels
{
    public partial class DeliverypointExternalDeliverypointCollection : Dionysos.Web.UI.DevExControls.SubPanelLLBLOverviewDataSourceCollection
    {
        #region Events

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            
            this.EntityName = "DeliverypointExternalDeliverypoint";
            this.EntityPageUrl = "~/External/DeliverypointExternalDeliverypoint.aspx";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        #endregion
    }
}