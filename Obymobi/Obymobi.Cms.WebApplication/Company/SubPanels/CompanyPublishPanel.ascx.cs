﻿using System;
using System.Drawing;
using System.Web.UI.WebControls;
using Crave.Api.Logic.Requests.Publishing;
using Crave.Api.Logic.Timestamps;
using Crave.Api.Logic.DataSources;
using DevExpress.Web.ASPxTreeList;
using Dionysos;
using Dionysos.Web;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;

namespace Obymobi.ObymobiCms.Company.SubPanels
{
    public partial class CompanyPublishPanel : System.Web.UI.UserControl
    {
        #region Fields

        private ASPxTreeList treeList = null;
        private bool advancedMode = false;
        private TimeZoneInfo timeZoneInfo = null;

        #endregion

        #region Methods

        protected override void CreateChildControls()
        {
            base.CreateChildControls();
            this.advancedMode = QueryStringHelper.HasValue<bool>("advanced");
            this.timeZoneInfo = CmsSessionHelper.CurrentUser.TimeZoneInfo;
            this.RenderTreeList();
        }

        private void RenderTreeList()
        {
            this.treeList = new ASPxTreeList();
            this.treeList.AutoGenerateColumns = false;
            this.treeList.Settings.GridLines = GridLines.Horizontal;
            this.treeList.Width = Unit.Percentage(100);
            this.treeList.SettingsEditing.Mode = TreeListEditMode.EditFormAndDisplayNode;
            this.treeList.SettingsSelection.Enabled = true;
            this.treeList.ID = "tlPublish-" + CmsSessionHelper.CurrentCompanyId.ToString();
            this.treeList.EnableCallbacks = true;
            this.treeList.ClientInstanceName = "tlPublish-" + CmsSessionHelper.CurrentCompanyId.ToString();
            this.treeList.HtmlRowPrepared += treeList_HtmlRowPrepared;

            // Name Column
            TreeListHyperLinkColumn tlcName = new TreeListHyperLinkColumn();
            tlcName.Caption = "Content";
            tlcName.FieldName = "Url";
            tlcName.CellStyle.HorizontalAlign = HorizontalAlign.Left;
            tlcName.PropertiesHyperLink.TextField = "Name";
            tlcName.PropertiesHyperLink.NavigateUrlFormatString = "{0}";
            this.treeList.Columns.Add(tlcName);

            if (this.advancedMode)
            {
                // Last modified Column
                TreeListTextColumn tlcLastModified = new TreeListTextColumn();
                tlcLastModified.Caption = "Last modified";
                tlcLastModified.Name = "LastModified";
                tlcLastModified.FieldName = "LastModified";
                tlcLastModified.CellStyle.HorizontalAlign = HorizontalAlign.Left;
                this.treeList.Columns.Add(tlcLastModified);
            }

            // Last published Column
            TreeListTextColumn tlcLastPublished = new TreeListTextColumn();
            tlcLastPublished.Caption = "Last published";
            tlcLastPublished.Name = "LastPublished";
            tlcLastPublished.FieldName = "LastPublished";
            tlcLastPublished.CellStyle.HorizontalAlign = HorizontalAlign.Left;
            this.treeList.Columns.Add(tlcLastPublished);

            int companyId = CmsSessionHelper.CurrentCompanyId;

            TreeListNode companyNode = this.CreateCompanyNode(companyId);
            this.CreateAvailabilitiesNode(companyId, companyNode);
            this.CreateCloudStorageAccountsNode(companyId, companyNode);
            this.CreateCompanyAmenitiesNode(companyId, companyNode);
            this.CreateCompanyVenueCategoriesNode(companyId, companyNode);

            TreeListNode menusNode = this.CreateMenusNode(companyId);
            this.CreateMenuNodes(companyId, menusNode);

            TreeListNode sitesNode = this.CreateSitesNode(companyId);
            this.CreateSiteNodes(companyId, sitesNode);

            TreeListNode deliverypointgroupsNode = this.CreateDeliverypointgroupsNode(companyId);
            this.CreateDeliverypointgroupNodes(companyId, deliverypointgroupsNode);

            TreeListNode clientConfigurationsNode = this.CreateClientConfigurationsNode(companyId);
            this.CreateClientConfigurationNodes(companyId, clientConfigurationsNode);

            TreeListNode uiModesNode = this.CreateUIModesNode(companyId);
            this.CreateUIModeNodes(companyId, uiModesNode);

            TreeListNode priceSchedulesNode = this.CreatePriceSchedulesNode(companyId);
            this.CreatePriceScheduleNodes(companyId, priceSchedulesNode);

            TreeListNode uiSchedulesNode = this.CreateUISchedulesNode(companyId);
            this.CreateUIScheduleNodes(companyId, uiSchedulesNode);

            TreeListNode uiThemesNode = this.CreateUIThemesNode(companyId);
            this.CreateUIThemeNodes(companyId, uiThemesNode);

            TreeListNode advertisementConfigurationsNode = this.CreateAdvertisementConfigurationsNode(companyId);
            this.CreateAdvertisementConfigurationNodes(companyId, advertisementConfigurationsNode);

            TreeListNode entertainmentConfigurationsNode = this.CreateEntertainmentConfigurationsNode(companyId);
            this.CreateEntertainmentConfigurationNodes(companyId, entertainmentConfigurationsNode);

            TreeListNode roomControlConfigurationsNode = this.CreateRoomControlConfigurationsNode(companyId);
            this.CreateRoomControlConfigurationNodes(companyId, roomControlConfigurationsNode);

            TreeListNode terminalsNode = this.CreateTerminalsNode(companyId);
            this.CreateTerminalNodes(companyId, terminalsNode);

            TreeListNode terminalConfigurationsNode = this.CreateTerminalConfigurationsNode(companyId);
            this.CreateTerminalConfigurationNodes(companyId, terminalConfigurationsNode);

            this.plhMenus.Controls.Add(this.treeList);
        }

        #region Node creation methods

        private TreeListNode CreateCompanyNode(int companyId)
        {
            PublishCompany publishCompany = new PublishCompany { CompanyId = companyId };

            CreateNodeRequest createCompanyNode = new CompanyPublishPanel.CreateNodeRequest
            {
                Key = string.Format("Company-{0}", companyId),
                ParentNode = null,
                Text = "Company",
                Url = string.Format("~/Company/Company.aspx?id={0}", companyId),
                LastModifiedUtc = publishCompany.Timestamp.GetLastModifiedUtc(),
                LastPublishedUtc = publishCompany.Timestamp.GetLastPublishedUtc(),
                Expanded = true,
                HasUnpublishedChanges = publishCompany.Timestamp.HasUnpublishedChanges()
            };

            return this.CreateNode(createCompanyNode);
        }

        private void CreateAvailabilitiesNode(int companyId, TreeListNode parentNode)
        {
            PublishAvailabilities publishAvailabilities = new PublishAvailabilities { CompanyId = companyId };

            CreateNodeRequest createAvailabilitiesNode = new CompanyPublishPanel.CreateNodeRequest
            {
                Key = string.Format("Availabilities-{0}", companyId),
                ParentNode = parentNode,
                Text = "Availabilities",
                Url = "~/Company/Availabilities.aspx",
                LastModifiedUtc = publishAvailabilities.Timestamp.GetLastModifiedUtc(),
                LastPublishedUtc = publishAvailabilities.Timestamp.GetLastPublishedUtc(),
                Expanded = false,
                HasUnpublishedChanges = publishAvailabilities.Timestamp.HasUnpublishedChanges()
            };
            TreeListNode availabilitiesNode = this.CreateNode(createAvailabilitiesNode);

            this.CreateNodesForTimestamp(publishAvailabilities.Timestamp, companyId, availabilitiesNode);
        }

        private void CreateCloudStorageAccountsNode(int companyId, TreeListNode parentNode)
        {
            PublishCloudStorageAccounts publishCloudStorageAccounts = new PublishCloudStorageAccounts { CompanyId = companyId };

            CreateNodeRequest createCloudStorageAccountsNode = new CompanyPublishPanel.CreateNodeRequest
            {
                Key = string.Format("CloudStorageAccounts-{0}", companyId),
                ParentNode = parentNode,
                Text = "Cloud storage accounts",
                Url = "~/Company/CloudStorageAccounts.aspx",
                LastModifiedUtc = publishCloudStorageAccounts.Timestamp.GetLastModifiedUtc(),
                LastPublishedUtc = publishCloudStorageAccounts.Timestamp.GetLastPublishedUtc(),
                Expanded = false,
                HasUnpublishedChanges = publishCloudStorageAccounts.Timestamp.HasUnpublishedChanges()
            };
            TreeListNode cloudStorageAccountsNode = this.CreateNode(createCloudStorageAccountsNode);

            this.CreateNodesForTimestamp(publishCloudStorageAccounts.Timestamp, companyId, cloudStorageAccountsNode);
        }

        private void CreateCompanyAmenitiesNode(int companyId, TreeListNode parentNode)
        {
            PublishCompanyAmenities publishCompanyAmenities = new PublishCompanyAmenities { CompanyId = companyId };

            CreateNodeRequest createCompanyAmenitiesNode = new CompanyPublishPanel.CreateNodeRequest
            {
                Key = string.Format("CompanyAmenities-{0}", companyId),
                ParentNode = parentNode,
                Text = "Amenities",
                Url = "~/Company/CompanyAmenities.aspx",
                LastModifiedUtc = publishCompanyAmenities.Timestamp.GetLastModifiedUtc(),
                LastPublishedUtc = publishCompanyAmenities.Timestamp.GetLastPublishedUtc(),
                Expanded = false,
                HasUnpublishedChanges = publishCompanyAmenities.Timestamp.HasUnpublishedChanges()
            };
            TreeListNode companyAmenitiesNode = this.CreateNode(createCompanyAmenitiesNode);

            this.CreateNodesForTimestamp(publishCompanyAmenities.Timestamp, companyId, companyAmenitiesNode);
        }

        private void CreateCompanyVenueCategoriesNode(int companyId, TreeListNode parentNode)
        {
            PublishCompanyVenueCategories publishCompanyVenueCategories = new PublishCompanyVenueCategories { CompanyId = companyId };

            CreateNodeRequest createCompanyVenueCategoriesNode = new CompanyPublishPanel.CreateNodeRequest
            {
                Key = string.Format("CompanyVenueCategories-{0}", companyId),
                ParentNode = parentNode,
                Text = "Venue categories",
                Url = "~/Company/CompanyVenueCategories.aspx",
                LastModifiedUtc = publishCompanyVenueCategories.Timestamp.GetLastModifiedUtc(),
                LastPublishedUtc = publishCompanyVenueCategories.Timestamp.GetLastPublishedUtc(),
                Expanded = false,
                HasUnpublishedChanges = publishCompanyVenueCategories.Timestamp.HasUnpublishedChanges()
            };
            TreeListNode companyVenueCategoriesNode = this.CreateNode(createCompanyVenueCategoriesNode);

            this.CreateNodesForTimestamp(publishCompanyVenueCategories.Timestamp, companyId, companyVenueCategoriesNode);
        }

        private TreeListNode CreateMenusNode(int companyId)
        {
            CreateNodeRequest createMenusNode = new CompanyPublishPanel.CreateNodeRequest
            {
                Key = string.Format("Menus-{0}", companyId),
                ParentNode = null,
                Text = "Menus",
                Url = "~/Catalog/Menus.aspx",
                Expanded = false
            };
            return this.CreateNode(createMenusNode);
        }

        private void CreateMenuNodes(int companyId, TreeListNode parentNode)
        {
            MenuCollection menuCollection = new Crave.Api.Logic.DataSources.MenuDataSource().GetEntities(companyId);
            foreach (MenuEntity menuEntity in menuCollection)
            {
                PublishMenu publishMenu = new PublishMenu { MenuId = menuEntity.MenuId };

                CreateNodeRequest createMenuNode = new CompanyPublishPanel.CreateNodeRequest
                {
                    Key = string.Format("Menu-{0}", menuEntity.MenuId),
                    ParentNode = parentNode,
                    Text = menuEntity.Name,
                    Url = string.Format("~/Catalog/Menu.aspx?id={0}", menuEntity.MenuId),
                    LastModifiedUtc = publishMenu.Timestamp.GetLastModifiedUtc(),
                    LastPublishedUtc = publishMenu.Timestamp.GetLastPublishedUtc(),
                    Expanded = false,
                    HasUnpublishedChanges = publishMenu.Timestamp.HasUnpublishedChanges()
                };
                TreeListNode menuNode = this.CreateNode(createMenuNode);

                this.CreateNodesForTimestamp(publishMenu.Timestamp, menuEntity.MenuId, menuNode);
            }
        }

        private TreeListNode CreateSitesNode(int companyId)
        {
            CreateNodeRequest createSitesNode = new CompanyPublishPanel.CreateNodeRequest
            {
                Key = string.Format("Sites-{0}", companyId),
                ParentNode = null,
                Text = "Sites",
                Url = "~/Cms/Sites.aspx",
                Expanded = false
            };
            return this.CreateNode(createSitesNode);
        }

        private void CreateSiteNodes(int companyId, TreeListNode parentNode)
        {
            SiteCollection siteCollection = new Crave.Api.Logic.DataSources.SiteDataSource().GetEntities(companyId);
            foreach (SiteEntity siteEntity in siteCollection)
            {
                PublishSite publishSite = new PublishSite { SiteId = siteEntity.SiteId };

                CreateNodeRequest createSiteNode = new CompanyPublishPanel.CreateNodeRequest
                {
                    Key = string.Format("Site-{0}", siteEntity.SiteId),
                    ParentNode = parentNode,
                    Text = siteEntity.Name,
                    Url = string.Format("~/Cms/Site.aspx?id={0}", siteEntity.SiteId),
                    LastModifiedUtc = publishSite.Timestamp.GetLastModifiedUtc(),
                    LastPublishedUtc = publishSite.Timestamp.GetLastPublishedUtc(),
                    Expanded = false,
                    HasUnpublishedChanges = publishSite.Timestamp.HasUnpublishedChanges()
                };
                TreeListNode siteNode = this.CreateNode(createSiteNode);

                this.CreateNodesForTimestamp(publishSite.Timestamp, siteEntity.SiteId, siteNode);
            }
        }

        private TreeListNode CreateDeliverypointgroupsNode(int companyId)
        {
            CreateNodeRequest createDeliverypointgroupsNode = new CompanyPublishPanel.CreateNodeRequest
            {
                Key = string.Format("Deliverypointgroups-{0}", companyId),
                ParentNode = null,
                Text = "Deliverypoint groups",
                Url = "~/Company/Deliverypointgroups.aspx",
                Expanded = false
            };
            return this.CreateNode(createDeliverypointgroupsNode);
        }

        private void CreateDeliverypointgroupNodes(int companyId, TreeListNode parentNode)
        {
            DeliverypointgroupCollection deliverypointgroupCollection = new Crave.Api.Logic.DataSources.DeliverypointgroupDataSource().GetEntities(companyId);
            foreach (DeliverypointgroupEntity deliverypointgroupEntity in deliverypointgroupCollection)
            {
                PublishDeliverypoints publishDeliverypoints = new PublishDeliverypoints { DeliverypointgroupId = deliverypointgroupEntity.DeliverypointgroupId };

                CreateNodeRequest createDeliverypointgroupNode = new CompanyPublishPanel.CreateNodeRequest
                {
                    Key = string.Format("Deliverypointgroup-{0}", deliverypointgroupEntity.DeliverypointgroupId),
                    ParentNode = parentNode,
                    Text = deliverypointgroupEntity.Name,
                    Url = string.Format("~/Catalog/Deliverypointgroup.aspx?id={0}", deliverypointgroupEntity.DeliverypointgroupId),
                    LastModifiedUtc = publishDeliverypoints.Timestamp.GetLastModifiedUtc(),
                    LastPublishedUtc = publishDeliverypoints.Timestamp.GetLastPublishedUtc(),
                    Expanded = false,
                    HasUnpublishedChanges = publishDeliverypoints.Timestamp.HasUnpublishedChanges()
                };
                TreeListNode deliverypointgroupNode = this.CreateNode(createDeliverypointgroupNode);

                this.CreateNodesForTimestamp(publishDeliverypoints.Timestamp, deliverypointgroupEntity.DeliverypointgroupId, deliverypointgroupNode);
            }
        }

        private TreeListNode CreateClientConfigurationsNode(int companyId)
        {
            CreateNodeRequest createClientConfigurationsNode = new CompanyPublishPanel.CreateNodeRequest
            {
                Key = string.Format("ClientConfigurations-{0}", companyId),
                ParentNode = null,
                Text = "Client configurations",
                Url = "~/Company/ClientConfigurations.aspx",
                Expanded = false
            };
            return this.CreateNode(createClientConfigurationsNode);
        }

        private void CreateClientConfigurationNodes(int companyId, TreeListNode parentNode)
        {
            ClientConfigurationCollection clientConfigurationCollection = new Crave.Api.Logic.DataSources.ClientConfigurationDataSource().GetEntities(companyId);
            foreach (ClientConfigurationEntity clientConfigurationEntity in clientConfigurationCollection)
            {
                PublishClientConfiguration publishClientConfiguration = new PublishClientConfiguration { ClientConfigurationId = clientConfigurationEntity.ClientConfigurationId };

                CreateNodeRequest createClientConfigurationNode = new CompanyPublishPanel.CreateNodeRequest
                {
                    Key = string.Format("ClientConfiguration-{0}", clientConfigurationEntity.ClientConfigurationId),
                    ParentNode = parentNode,
                    Text = clientConfigurationEntity.Name,
                    Url = string.Format("~/Company/ClientConfiguration.aspx?id={0}", clientConfigurationEntity.ClientConfigurationId),
                    LastModifiedUtc = publishClientConfiguration.Timestamp.GetLastModifiedUtc(),
                    LastPublishedUtc = publishClientConfiguration.Timestamp.GetLastPublishedUtc(),
                    Expanded = false,
                    HasUnpublishedChanges = publishClientConfiguration.Timestamp.HasUnpublishedChanges()
                };
                TreeListNode clientConfigurationNode = this.CreateNode(createClientConfigurationNode);

                this.CreateNodesForTimestamp(publishClientConfiguration.Timestamp, clientConfigurationEntity.ClientConfigurationId, clientConfigurationNode);
            }
        }

        private TreeListNode CreateUIModesNode(int companyId)
        {
            CreateNodeRequest createUIModesNode = new CompanyPublishPanel.CreateNodeRequest
            {
                Key = string.Format("UIModes-{0}", companyId),
                ParentNode = null,
                Text = "UI modes",
                Url = "~/Company/UIModes.aspx",
                Expanded = false
            };
            return this.CreateNode(createUIModesNode);
        }

        private void CreateUIModeNodes(int companyId, TreeListNode parentNode)
        {
            UIModeCollection uiModeCollection = new Crave.Api.Logic.DataSources.UIModeDataSource().GetEntities(companyId);
            foreach (UIModeEntity uiModeEntity in uiModeCollection)
            {
                PublishUIMode publishUIMode = new PublishUIMode { UIModeId = uiModeEntity.UIModeId };

                CreateNodeRequest createUIModeNode = new CompanyPublishPanel.CreateNodeRequest
                {
                    Key = string.Format("UIMode-{0}", uiModeEntity.UIModeId),
                    ParentNode = parentNode,
                    Text = uiModeEntity.Name,
                    Url = string.Format("~/Company/UIMode.aspx?id={0}", uiModeEntity.UIModeId),
                    LastModifiedUtc = publishUIMode.Timestamp.GetLastModifiedUtc(),
                    LastPublishedUtc = publishUIMode.Timestamp.GetLastPublishedUtc(),
                    Expanded = false,
                    HasUnpublishedChanges = publishUIMode.Timestamp.HasUnpublishedChanges()
                };
                TreeListNode uiModeNode = this.CreateNode(createUIModeNode);

                this.CreateNodesForTimestamp(publishUIMode.Timestamp, uiModeEntity.UIModeId, uiModeNode);
            }
        }

        private TreeListNode CreateUISchedulesNode(int companyId)
        {
            CreateNodeRequest createUISchedulesNode = new CompanyPublishPanel.CreateNodeRequest
            {
                Key = string.Format("UISchedules-{0}", companyId),
                ParentNode = null,
                Text = "UI schedules",
                Url = "~/Company/UISchedules.aspx",
                Expanded = false
            };
            return this.CreateNode(createUISchedulesNode);
        }

        private void CreateUIScheduleNodes(int companyId, TreeListNode parentNode)
        {
            UIScheduleCollection uiScheduleCollection = new Crave.Api.Logic.DataSources.UIScheduleDataSource().GetEntities(companyId);
            foreach (UIScheduleEntity uiScheduleEntity in uiScheduleCollection)
            {
                PublishUISchedule publishUISchedule = new PublishUISchedule { UIScheduleId = uiScheduleEntity.UIScheduleId };

                CreateNodeRequest createUIScheduleNode = new CompanyPublishPanel.CreateNodeRequest
                {
                    Key = string.Format("UISchedule-{0}", uiScheduleEntity.UIScheduleId),
                    ParentNode = parentNode,
                    Text = uiScheduleEntity.Name,
                    Url = string.Format("~/Company/UISchedule.aspx?id={0}", uiScheduleEntity.UIScheduleId),
                    LastModifiedUtc = publishUISchedule.Timestamp.GetLastModifiedUtc(),
                    LastPublishedUtc = publishUISchedule.Timestamp.GetLastPublishedUtc(),
                    Expanded = false,
                    HasUnpublishedChanges = publishUISchedule.Timestamp.HasUnpublishedChanges()
                };
                TreeListNode uiScheduleNode = this.CreateNode(createUIScheduleNode);

                this.CreateNodesForTimestamp(publishUISchedule.Timestamp, uiScheduleEntity.UIScheduleId, uiScheduleNode);
            }
        }

        private TreeListNode CreateUIThemesNode(int companyId)
        {
            CreateNodeRequest createUIThemesNode = new CompanyPublishPanel.CreateNodeRequest
            {
                Key = string.Format("UIThemes-{0}", companyId),
                ParentNode = null,
                Text = "UI themes",
                Url = "~/Generic/UIThemes.aspx",
                Expanded = false
            };
            return this.CreateNode(createUIThemesNode);
        }

        private void CreateUIThemeNodes(int companyId, TreeListNode parentNode)
        {
            UIThemeCollection uiThemeCollection = new Crave.Api.Logic.DataSources.UIThemeDataSource().GetEntities(companyId);
            foreach (UIThemeEntity uiThemeEntity in uiThemeCollection)
            {
                PublishUITheme publishUITheme = new PublishUITheme { UIThemeId = uiThemeEntity.UIThemeId };

                CreateNodeRequest createUIThemeNode = new CompanyPublishPanel.CreateNodeRequest
                {
                    Key = string.Format("UITheme-{0}", uiThemeEntity.UIThemeId),
                    ParentNode = parentNode,
                    Text = uiThemeEntity.Name,
                    Url = string.Format("~/Generic/UITheme.aspx?id={0}", uiThemeEntity.UIThemeId),
                    LastModifiedUtc = publishUITheme.Timestamp.GetLastModifiedUtc(),
                    LastPublishedUtc = publishUITheme.Timestamp.GetLastPublishedUtc(),
                    Expanded = false,
                    HasUnpublishedChanges = publishUITheme.Timestamp.HasUnpublishedChanges()
                };
                TreeListNode uiThemeNode = this.CreateNode(createUIThemeNode);

                this.CreateNodesForTimestamp(publishUITheme.Timestamp, uiThemeEntity.UIThemeId, uiThemeNode);
            }
        }

        private TreeListNode CreatePriceSchedulesNode(int companyId)
        {
            CreateNodeRequest createPriceSchedulesNode = new CompanyPublishPanel.CreateNodeRequest
            {
                Key = string.Format("PriceSchedules-{0}", companyId),
                ParentNode = null,
                Text = "Price schedules",
                Url = "~/Catalog/PriceSchedules.aspx",
                Expanded = false
            };
            return this.CreateNode(createPriceSchedulesNode);
        }

        private void CreatePriceScheduleNodes(int companyId, TreeListNode parentNode)
        {
            PriceScheduleCollection priceScheduleCollection = new Crave.Api.Logic.DataSources.PriceScheduleDataSource().GetEntities(companyId);
            foreach (PriceScheduleEntity priceScheduleEntity in priceScheduleCollection)
            {
                PublishPriceSchedule publishPriceSchedule = new PublishPriceSchedule { PriceScheduleId = priceScheduleEntity.PriceScheduleId };

                CreateNodeRequest createPriceScheduleNode = new CompanyPublishPanel.CreateNodeRequest
                {
                    Key = string.Format("PriceSchedule-{0}", priceScheduleEntity.PriceScheduleId),
                    ParentNode = parentNode,
                    Text = priceScheduleEntity.Name,
                    Url = string.Format("~/Catalog/PriceSchedule.aspx?id={0}", priceScheduleEntity.PriceScheduleId),
                    LastModifiedUtc = publishPriceSchedule.Timestamp.GetLastModifiedUtc(),
                    LastPublishedUtc = publishPriceSchedule.Timestamp.GetLastPublishedUtc(),
                    Expanded = false,
                    HasUnpublishedChanges = publishPriceSchedule.Timestamp.HasUnpublishedChanges()
                };
                TreeListNode priceScheduleNode = this.CreateNode(createPriceScheduleNode);

                this.CreateNodesForTimestamp(publishPriceSchedule.Timestamp, priceScheduleEntity.PriceScheduleId, priceScheduleNode);
            }
        }

        private TreeListNode CreateAdvertisementConfigurationsNode(int companyId)
        {
            CreateNodeRequest createAdvertisementConfigurationsNode = new CompanyPublishPanel.CreateNodeRequest
            {
                Key = string.Format("AdvertisementConfigurations-{0}", companyId),
                ParentNode = null,
                Text = "Advertisement configurations",
                Url = "~/Advertising/AdvertisementConfigurations.aspx",
                Expanded = false
            };
            return this.CreateNode(createAdvertisementConfigurationsNode);
        }

        private void CreateAdvertisementConfigurationNodes(int companyId, TreeListNode parentNode)
        {
            AdvertisementConfigurationCollection advertisementConfigurationCollection = new Crave.Api.Logic.DataSources.AdvertisementConfigurationDataSource().GetEntities(companyId);
            foreach (AdvertisementConfigurationEntity advertisementConfigurationEntity in advertisementConfigurationCollection)
            {
                PublishAdvertisements publishAdvertisements = new PublishAdvertisements { AdvertisementConfigurationId = advertisementConfigurationEntity.AdvertisementConfigurationId };

                CreateNodeRequest createAdvertisementConfigurationNode = new CompanyPublishPanel.CreateNodeRequest
                {
                    Key = string.Format("AdvertisementConfiguration-{0}", advertisementConfigurationEntity.AdvertisementConfigurationId),
                    ParentNode = parentNode,
                    Text = advertisementConfigurationEntity.Name,
                    Url = string.Format("~/Catalog/AdvertisementConfiguration.aspx?id={0}", advertisementConfigurationEntity.AdvertisementConfigurationId),
                    LastModifiedUtc = publishAdvertisements.Timestamp.GetLastModifiedUtc(),
                    LastPublishedUtc = publishAdvertisements.Timestamp.GetLastPublishedUtc(),
                    Expanded = false,
                    HasUnpublishedChanges = publishAdvertisements.Timestamp.HasUnpublishedChanges()
                };
                TreeListNode advertisementConfigurationNode = this.CreateNode(createAdvertisementConfigurationNode);

                this.CreateNodesForTimestamp(publishAdvertisements.Timestamp, advertisementConfigurationEntity.AdvertisementConfigurationId, advertisementConfigurationNode);
            }
        }

        private TreeListNode CreateEntertainmentConfigurationsNode(int companyId)
        {
            CreateNodeRequest createEntertainmentConfigurationsNode = new CompanyPublishPanel.CreateNodeRequest
            {
                Key = string.Format("EntertainmentConfigurations-{0}", companyId),
                ParentNode = null,
                Text = "Entertainment configurations",
                Url = "~/Generic/EntertainmentConfigurations.aspx",
                Expanded = false
            };
            return this.CreateNode(createEntertainmentConfigurationsNode);
        }

        private void CreateEntertainmentConfigurationNodes(int companyId, TreeListNode parentNode)
        {
            EntertainmentConfigurationCollection entertainmentConfigurationCollection = new Crave.Api.Logic.DataSources.EntertainmentConfigurationDataSource().GetEntities(companyId);
            foreach (EntertainmentConfigurationEntity entertainmentConfigurationEntity in entertainmentConfigurationCollection)
            {
                PublishEntertainment publishEntertainment = new PublishEntertainment { EntertainmentConfigurationId = entertainmentConfigurationEntity.EntertainmentConfigurationId };

                CreateNodeRequest createEntertainmentConfigurationNode = new CompanyPublishPanel.CreateNodeRequest
                {
                    Key = string.Format("EntertainmentConfiguration-{0}", entertainmentConfigurationEntity.EntertainmentConfigurationId),
                    ParentNode = parentNode,
                    Text = entertainmentConfigurationEntity.Name,
                    Url = string.Format("~/Catalog/EntertainmentConfiguration.aspx?id={0}", entertainmentConfigurationEntity.EntertainmentConfigurationId),
                    LastModifiedUtc = publishEntertainment.Timestamp.GetLastModifiedUtc(),
                    LastPublishedUtc = publishEntertainment.Timestamp.GetLastPublishedUtc(),
                    Expanded = false,
                    HasUnpublishedChanges = publishEntertainment.Timestamp.HasUnpublishedChanges()
                };
                TreeListNode entertainmentConfigurationNode = this.CreateNode(createEntertainmentConfigurationNode);

                this.CreateNodesForTimestamp(publishEntertainment.Timestamp, entertainmentConfigurationEntity.EntertainmentConfigurationId, entertainmentConfigurationNode);
            }
        }

        private TreeListNode CreateRoomControlConfigurationsNode(int companyId)
        {
            CreateNodeRequest createRoomControlConfigurationsNode = new CompanyPublishPanel.CreateNodeRequest
            {
                Key = string.Format("RoomControlConfigurations-{0}", companyId),
                ParentNode = null,
                Text = "Room control configurations",
                Url = "~/Company/RoomControlConfigurations.aspx",
                Expanded = false
            };
            return this.CreateNode(createRoomControlConfigurationsNode);
        }

        private void CreateRoomControlConfigurationNodes(int companyId, TreeListNode parentNode)
        {
            RoomControlConfigurationCollection roomControlConfigurationCollection = new Crave.Api.Logic.DataSources.RoomControlConfigurationDataSource().GetEntities(companyId);
            foreach (RoomControlConfigurationEntity roomControlConfigurationEntity in roomControlConfigurationCollection)
            {
                PublishRoomControlConfiguration publishRoomControlConfiguration = new PublishRoomControlConfiguration { RoomControlConfigurationId = roomControlConfigurationEntity.RoomControlConfigurationId };

                CreateNodeRequest createRoomControlConfigurationNode = new CompanyPublishPanel.CreateNodeRequest
                {
                    Key = string.Format("RoomControlConfiguration-{0}", roomControlConfigurationEntity.RoomControlConfigurationId),
                    ParentNode = parentNode,
                    Text = roomControlConfigurationEntity.Name,
                    Url = string.Format("~/Catalog/RoomControlConfiguration.aspx?id={0}", roomControlConfigurationEntity.RoomControlConfigurationId),
                    LastModifiedUtc = publishRoomControlConfiguration.Timestamp.GetLastModifiedUtc(),
                    LastPublishedUtc = publishRoomControlConfiguration.Timestamp.GetLastPublishedUtc(),
                    Expanded = false,
                    HasUnpublishedChanges = publishRoomControlConfiguration.Timestamp.HasUnpublishedChanges()
                };
                TreeListNode roomControlConfigurationNode = this.CreateNode(createRoomControlConfigurationNode);

                this.CreateNodesForTimestamp(publishRoomControlConfiguration.Timestamp, roomControlConfigurationEntity.RoomControlConfigurationId, roomControlConfigurationNode);
            }
        }

        private TreeListNode CreateTerminalsNode(int companyId)
        {
            CreateNodeRequest createTerminalsNode = new CompanyPublishPanel.CreateNodeRequest
            {
                Key = string.Format("Terminals-{0}", companyId),
                ParentNode = null,
                Text = "Terminals",
                Url = "~/Company/Terminals.aspx",
                Expanded = false
            };
            return this.CreateNode(createTerminalsNode);
        }

        private void CreateTerminalNodes(int companyId, TreeListNode parentNode)
        {
            Data.CollectionClasses.TerminalCollection terminalCollection = new Crave.Api.Logic.DataSources.TerminalDataSource().GetEntities(companyId);
            foreach (TerminalEntity terminalEntity in terminalCollection)
            {
                PublishTerminal publishTerminal = new PublishTerminal { TerminalId = terminalEntity.TerminalId };

                CreateNodeRequest createTerminalNode = new CompanyPublishPanel.CreateNodeRequest
                {
                    Key = string.Format("Terminal-{0}", terminalEntity.TerminalId),
                    ParentNode = parentNode,
                    Text = terminalEntity.Name,
                    Url = string.Format("~/Company/Terminal.aspx?id={0}", terminalEntity.TerminalId),
                    LastModifiedUtc = publishTerminal.Timestamp.GetLastModifiedUtc(),
                    LastPublishedUtc = publishTerminal.Timestamp.GetLastPublishedUtc(),
                    Expanded = false,
                    HasUnpublishedChanges = publishTerminal.Timestamp.HasUnpublishedChanges()
                };
                TreeListNode terminalNode = this.CreateNode(createTerminalNode);

                this.CreateNodesForTimestamp(publishTerminal.Timestamp, terminalEntity.TerminalId, terminalNode);
            }
        }

        private void CreateNodesForTimestamp(TimestampBase timestamp, object id, TreeListNode parentNode)
        {
            if (this.advancedMode)
            {
                foreach (TimestampFieldIndexPair timestampFieldIndexes in timestamp.Fields)
                {
                    TimestampFieldIndex modifiedField = timestampFieldIndexes.ModifiedField;
                    TimestampFieldIndex publishedField = timestampFieldIndexes.PublishedField;

                    DateTime? lastModifiedUtc = timestamp.GetTimestampUtc(modifiedField);
                    DateTime? lastPublishedUtc = timestamp.GetTimestampUtc(publishedField);

                    string timestampName = this.GetTimestampName(modifiedField);

                    CreateNodeRequest createTimestampNode = new CompanyPublishPanel.CreateNodeRequest
                    {
                        Key = string.Format("{0}-{1}", modifiedField, id),
                        ParentNode = parentNode,
                        Text = timestampName,
                        Url = string.Empty,
                        LastModifiedUtc = lastModifiedUtc,
                        LastPublishedUtc = lastPublishedUtc,
                        Expanded = false,
                        HasUnpublishedChanges = timestamp.IsFieldDirty(modifiedField, publishedField)
                    };
                    TreeListNode timestampNode = this.CreateNode(createTimestampNode);
                    timestampNode.AllowSelect = false;
                }
            }
        }

        private TreeListNode CreateTerminalConfigurationsNode(int companyId)
        {
            CreateNodeRequest createTerminalConfigurationsNode = new CompanyPublishPanel.CreateNodeRequest
            {
                Key = string.Format("TerminalConfigurations-{0}", companyId),
                ParentNode = null,
                Text = "Terminal configurations",
                Url = "~/Company/TerminalConfigurations.aspx",
                Expanded = false
            };
            return this.CreateNode(createTerminalConfigurationsNode);
        }

        private void CreateTerminalConfigurationNodes(int companyId, TreeListNode parentNode)
        {
            TerminalConfigurationCollection terminalConfigurationCollection = new Crave.Api.Logic.DataSources.TerminalConfigurationDataSource().GetEntities(companyId);
            foreach (TerminalConfigurationEntity terminalConfigurationEntity in terminalConfigurationCollection)
            {
                PublishTerminalConfiguration publishTerminalConfiguration = new PublishTerminalConfiguration { TerminalConfigurationId = terminalConfigurationEntity.TerminalConfigurationId };

                CreateNodeRequest createTerminalConfigurationNode = new CompanyPublishPanel.CreateNodeRequest
                {
                    Key = string.Format("TerminalConfiguration-{0}", terminalConfigurationEntity.TerminalConfigurationId),
                    ParentNode = parentNode,
                    Text = terminalConfigurationEntity.Name,
                    Url = string.Format("~/Company/TerminalConfiguration.aspx?id={0}", terminalConfigurationEntity.TerminalConfigurationId),
                    LastModifiedUtc = publishTerminalConfiguration.Timestamp.GetLastModifiedUtc(),
                    LastPublishedUtc = publishTerminalConfiguration.Timestamp.GetLastPublishedUtc(),
                    Expanded = false,
                    HasUnpublishedChanges = publishTerminalConfiguration.Timestamp.HasUnpublishedChanges()
                };
                TreeListNode terminalConfigurationNode = this.CreateNode(createTerminalConfigurationNode);

                this.CreateNodesForTimestamp(publishTerminalConfiguration.Timestamp, terminalConfigurationEntity.TerminalConfigurationId, terminalConfigurationNode);
            }
        }

        private TreeListNode CreateNode(CreateNodeRequest createNode)
        {
            TreeListNode node = this.treeList.AppendNode(createNode.Key, createNode.ParentNode);
            node["Name"] = createNode.Text;
            node["Url"] = createNode.Url;

            if (createNode.LastModifiedUtc.HasValue)
                node["LastModified"] = string.Format("{0} ({1} ago)", createNode.LastModifiedUtc.UtcToLocalTime(this.timeZoneInfo), DateTimeUtil.GetElapsedTimeString(createNode.LastModifiedUtc.Value, DateTime.UtcNow));

            if (createNode.LastPublishedUtc.HasValue)
                node["LastPublished"] = string.Format("{0} ({1} ago)", createNode.LastPublishedUtc.UtcToLocalTime(this.timeZoneInfo), DateTimeUtil.GetElapsedTimeString(createNode.LastPublishedUtc.Value, DateTime.UtcNow));

            node.Expanded = createNode.Expanded;
            node["HasUnpublishedChanges"] = createNode.HasUnpublishedChanges;

            if (createNode.HasUnpublishedChanges)
                node.Selected = true;

            return node;
        }

        #endregion

        private string GetTimestampName(TimestampFieldIndex field)
        {
            return field.ToString().Replace("Modified", string.Empty).Replace("UTC", string.Empty).FormatPascalCase();
        }

        #endregion

        #region Event handlers

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        private void treeList_HtmlRowPrepared(object sender, TreeListHtmlRowEventArgs e)
        {
            try
            {
                bool hasUnpublishedChanges = (bool)e.GetValue("HasUnpublishedChanges");
                if (hasUnpublishedChanges)
                    e.Row.ForeColor = Color.Red;
            }
            catch { }
        }

        #endregion

        #region Classes

        private class CreateNodeRequest
        {
            public object Key { get; set; }
            public string Text { get; set; }
            public string Url { get; set; }
            public DateTime? LastModifiedUtc { get; set; }
            public DateTime? LastPublishedUtc { get; set; }
            public TreeListNode ParentNode { get; set; }
            public bool Expanded { get; set; }
            public bool HasUnpublishedChanges { get; set; }
        }

        #endregion
    }
}