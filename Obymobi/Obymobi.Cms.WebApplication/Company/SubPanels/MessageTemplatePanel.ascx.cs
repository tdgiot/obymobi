﻿using System;
using System.Linq;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System.Collections.Generic;

namespace Obymobi.ObymobiCms.Company.SubPanels
{
    public partial class MessageTemplatePanel : Dionysos.Web.UI.DevExControls.SubPanelLLBLOverviewDataSourceCollection
    {
        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "MessageTemplateCategoryMessageTemplate";
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.btnAdd.Click += btnAdd_Click;

            this.MainGridView.ShowEditColumn = false;
            this.MainGridView.ShowEditHyperlinkColumn = false;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            this.AddMessageTemplate();
        }

        public override bool InitializeDataSource()
        {
            bool toReturn = base.InitializeDataSource();

            this.DataSource.PerformSelect +=DataSource_PerformSelect;

            return toReturn;
        }

        private MessageTemplateCategoryMessageTemplateCollection currentCollection = new MessageTemplateCategoryMessageTemplateCollection();
        void DataSource_PerformSelect(object sender, PerformSelectEventArgs e)
        {
            currentCollection = (MessageTemplateCategoryMessageTemplateCollection)this.DataSource.EntityCollection;

            SetGui();
        }

        private void SetGui()
        {
            List<int> currentLinkedTemplates = currentCollection.Select(x => x.MessageTemplateId).ToList();

            PredicateExpression filter = new PredicateExpression();
            filter.Add(MessageTemplateFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            filter.Add(MessageTemplateFields.MessageTemplateId != currentLinkedTemplates);

            MessageTemplateCollection messageTemplates = new MessageTemplateCollection();
            messageTemplates.GetMulti(filter);

            this.ddlMessageTemplateId.DataSource = messageTemplates;
            this.ddlMessageTemplateId.DataBind();
        }

        private void AddMessageTemplate()
        {
            if (this.ddlMessageTemplateId.ValidId > 0)
            {
                MessageTemplateCategoryMessageTemplateEntity mtcmte = new MessageTemplateCategoryMessageTemplateEntity();
                mtcmte.MessageTemplateCategoryId = this.ParentDataSourceTyped.MessageTemplateCategoryId;
                mtcmte.MessageTemplateId = this.ddlMessageTemplateId.ValidId;

                mtcmte.Save();
            }
        }

        private MessageTemplateCategoryEntity ParentDataSourceTyped
        {
            get { return (MessageTemplateCategoryEntity)this.ParentDataSource; }
        }
    }
}