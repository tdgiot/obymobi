﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.Subpanels.PmsSettingsPanel.InnsistSettingsPanel" Codebehind="InnsistPmsSettingsPanel.ascx.cs" %>
<table class="dataformV2">
    <tr>
        <td class="label">
            <D:Label runat="server" ID="lblPosValue1">Outgoing Webservice URL</D:Label>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbPosValue1"></D:TextBoxString>
        </td>
        <td class="label">
            <D:Label runat="server" ID="lblPosValue2">Incomming Webservice URL</D:Label>
            
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbPosValue2"></D:TextBoxString>
        </td>
    </tr>
    <tr>
        <td class="label">
            <D:Label runat="server" ID="lblPosValue3">Username</D:Label>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbPosValue3"></D:TextBoxString>
        </td>
        <td class="label">
            <D:Label runat="server" ID="lblPosValue4">Password</D:Label>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbPosValue4"></D:TextBoxString>
        </td>
    </tr>
    <tr>
        <td class="label">
            <D:Label runat="server" ID="lblPosValue5">From System Id</D:Label>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbPosValue5"></D:TextBoxString>
        </td>
        <td class="label">
            <D:Label runat="server" ID="lblPosValue6">To System Id</D:Label>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbPosValue6"></D:TextBoxString>
        </td>
    </tr>
    <tr>
        <td class="label">
            <D:Label runat="server" ID="lblPosValue7">WakeUp Code</D:Label>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbPosValue7"></D:TextBoxString>
        </td>
        <td class="label">
            
        </td>
        <td class="control">
            
        </td>
    </tr>
</table>
