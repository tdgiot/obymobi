﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.Subpanels.PmsSettingsPanel.TigertmsSettingsPanel" Codebehind="TigertmsSettingsPanel.ascx.cs" %>
<table class="dataformV2">
    <tr>
        <td class="label">
            <D:Label runat="server" ID="lblPosValue1">Webservice To Be Called On</D:Label>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbPosValue1"></D:TextBoxString>
        </td>
        <td class="label">
            <D:Label runat="server" ID="lblPosValue2">Webservice User Key</D:Label>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbPosValue2"></D:TextBoxString>
        </td>
    </tr>
    <tr>
        <td class="label">
            <D:Label runat="server" ID="lblPosValue3">Webservice To Call To</D:Label>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbPosValue3"></D:TextBoxString>
        </td>
        <td class="label">
            <D:Label runat="server" ID="lblPosValue4">Test Mode</D:Label>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbPosValue4"></D:TextBoxString>
        </td>
    </tr>
</table>
