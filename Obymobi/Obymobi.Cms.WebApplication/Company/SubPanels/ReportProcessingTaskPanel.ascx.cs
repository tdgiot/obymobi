﻿using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;

namespace Obymobi.ObymobiCms.Company.SubPanels
{
    public partial class ReportProcessingTaskPanel : Dionysos.Web.UI.DevExControls.SubPanelLLBLOverviewDataSourceCollection
    {
        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "ReportProcessingTask";
            base.OnInit(e);
            this.PreRender += ReportProcessingTaskPanel_PreRender;            
        }

        void ReportProcessingTaskPanel_PreRender(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                // Some black magic to get a nice default sort.
                if (this.MainGridView.Columns["CreatedManual"] != null)
                {
                    this.MainGridView.SortBy(this.MainGridView.Columns["CreatedManual"], DevExpress.Data.ColumnSortOrder.Descending);
                }
                if (this.MainGridView.Columns["CreatedUTC"] != null)
                {
                    this.MainGridView.SortBy(this.MainGridView.Columns["CreatedUTC"], DevExpress.Data.ColumnSortOrder.Descending);
                    this.MainGridView.Columns["CreatedUTC"].Visible = false;
                }
            }

            this.hlGeneratePageviewsReports.Visible = (CmsSessionHelper.CurrentRole != Role.Reseller);
        }

        public override bool InitializeDataSource()
        {
            base.InitializeDataSource();
            if (CmsSessionHelper.CurrentRole == Role.Reseller)
            {
                this.DataSource.FilterToUse = new PredicateExpression().Add(ReportProcessingTaskFields.AnalyticsReportType == AnalyticsReportType.Transactions);
                this.MainGridView.ShowDeleteHyperlinkColumn = false;
            }
            return true;
        }
    }
}