﻿using System;
using System.Collections.Generic;
using System.Linq;
using Obymobi.Cms.Logic.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;

namespace Obymobi.ObymobiCms.Company.Subpanels
{
    public partial class UIWidgetAvailabilityCollection : Dionysos.Web.UI.DevExControls.SubPanelLLBLOverviewDataSourceCollection
    {
        #region Methods

        private void SetGui()
        {
            List<int> usedIds = this.UIWidgetOnParentDataSource.UIWidgetAvailabilityCollection.Select(x => x.AvailabilityId).ToList();
            AvailabilityCollection availabilities = UIWidgetAvailabilityHelper.GetAvailabilitiesByCompanyId(CmsSessionHelper.CurrentCompanyId);

            this.ddlAvailabilityId.DataSource = availabilities.Where(x => !usedIds.Contains(x.AvailabilityId));
            this.ddlAvailabilityId.DataBind();
        }

        #endregion

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "UIWidgetAvailability";
            base.OnInit(e);   
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.btAddAvailability.Click += this.BtAdd_Click;
        }

        protected override void OnParentDataSourceLoaded()
        {
            base.OnParentDataSourceLoaded();
            this.SetGui();
        }

        private void BtAdd_Click(object sender, EventArgs e)
        {
            if (this.ddlAvailabilityId.ValidId <= 0)
            {
                this.PageAsPageLLBLGenEntity.AddInformator(Dionysos.Web.UI.InformatorType.Warning, this.PageAsPageLLBLGenEntity.Translate("SelectAnAvailability", "Please select an availability first"));
            }
            else
            {
                UIWidgetAvailabilityEntity uiWidgetAvailabilityEntity = new UIWidgetAvailabilityEntity();
                uiWidgetAvailabilityEntity.UIWidgetId = this.UIWidgetOnParentDataSource.UIWidgetId;
                uiWidgetAvailabilityEntity.AvailabilityId = this.ddlAvailabilityId.ValidId;
                uiWidgetAvailabilityEntity.SortOrder = this.UIWidgetOnParentDataSource.UIWidgetAvailabilityCollection.Any() ?  this.UIWidgetOnParentDataSource.UIWidgetAvailabilityCollection.Max(x => x.SortOrder) + 100 : 1000;
                uiWidgetAvailabilityEntity.Save();

                this.Response.Redirect(this.Request.RawUrl);
            }
        }        

        #endregion

        #region Properties                

        public UIWidgetEntity UIWidgetOnParentDataSource
        {
            get
            {
                return this.Parent.DataSource as UIWidgetEntity;
            }
        }

        #endregion
    }
}