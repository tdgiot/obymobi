﻿using System;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using System.Linq;

namespace Obymobi.ObymobiCms.Company.SubPanels
{
    public partial class DeliverypointgroupAdvertisementCollection : Dionysos.Web.UI.DevExControls.SubPanelLLBLOverviewDataSourceCollection
    {
        #region Methods

        private void SetGui()
        {
            var includeFieldsList = new IncludeFieldsList();
            includeFieldsList.Add(AdvertisementFields.Name);
            includeFieldsList.Add(AdvertisementFields.Visible);

            var companyAndGenericFilter = new PredicateExpression();
            companyAndGenericFilter.AddWithOr(AdvertisementFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            companyAndGenericFilter.AddWithOr(AdvertisementFields.CompanyId == DBNull.Value);

            var excludeUsedAdvertisementsFilter = new PredicateExpression(AdvertisementFields.AdvertisementId != this.DataSourceAsDeliverypointgroupEntity.DeliverypointgroupAdvertisementCollection.Select(dpga => dpga.AdvertisementId).ToList());

            var filter = new PredicateExpression();
            filter.Add(companyAndGenericFilter);
            filter.AddWithAnd(excludeUsedAdvertisementsFilter);            

            var sort = new SortExpression(new SortClause(AdvertisementFields.Name, SortOperator.Ascending));

            var advertisements = new AdvertisementCollection();
            advertisements.GetMulti(filter, 0, sort, null, null, includeFieldsList, 0, 0);

            this.ddlAdvertisementId.DataSource = advertisements;
            this.ddlAdvertisementId.DataBind();
        }

        private void AddDeliverypointgroupAdvertisement()
        {
            if (this.ddlAdvertisementId.ValidId > 0)
            {
                DeliverypointgroupAdvertisementEntity deliverypointgroupAdvertisement = new DeliverypointgroupAdvertisementEntity();
                deliverypointgroupAdvertisement.DeliverypointgroupId = this.ParentPrimaryKeyFieldValue;
                deliverypointgroupAdvertisement.AdvertisementId = this.ddlAdvertisementId.ValidId;
                
                if (deliverypointgroupAdvertisement.Save())
                    this.DataSourceAsDeliverypointgroupEntity.DeliverypointgroupAdvertisementCollection.Add(deliverypointgroupAdvertisement);

                this.SetGui();
            }
        }

        #endregion

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            this.DataSourceLoaded += DeliverypointgroupAdvertisementCollection_DataSourceLoaded;          
            base.OnInit(e);
            this.EntityName = "DeliverypointgroupAdvertisement";            
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.btnAdd.Click += btnAdd_Click;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            this.AddDeliverypointgroupAdvertisement();
        }

        private void DeliverypointgroupAdvertisementCollection_DataSourceLoaded(object sender)
        {
            this.SetGui();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Return the page's datasource as a DeliverypointgroupEntity
        /// </summary>
        public DeliverypointgroupEntity DataSourceAsDeliverypointgroupEntity
        {
            get
            {
                return this.ParentDataSource as DeliverypointgroupEntity;
            }
        }

        #endregion
    }
}