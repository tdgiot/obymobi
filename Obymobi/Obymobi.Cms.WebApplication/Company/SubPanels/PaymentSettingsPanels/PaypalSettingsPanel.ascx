﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.Subpanels.PosSettingsPanels.PaypalSettingsPanel" Codebehind="PaypalSettingsPanel.ascx.cs" %>
<table class="dataformV2">
    <tr>
        <td class="label">
            <D:Label runat="server" ID="lblFieldValue1">API Username</D:Label>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbFieldValue1"></D:TextBoxString>
        </td>
        <td class="label">
            <D:Label runat="server" ID="lblFieldValue2">API Password</D:Label>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbFieldValue2"></D:TextBoxString>
        </td>
    </tr>
    <tr>
        <td class="label">
            <D:Label runat="server" ID="lblFieldValue3">API Signature</D:Label>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbFieldValue3"></D:TextBoxString>
        </td>
        <td class="label">
            &nbsp;
        </td>
        <td class="control">
            &nbsp;
        </td>
    </tr>
</table>
