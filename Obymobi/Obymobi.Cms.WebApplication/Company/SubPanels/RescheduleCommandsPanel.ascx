﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.SubPanels.RescheduleCommandsPanel" Codebehind="RescheduleCommandsPanel.ascx.cs" %>
<table class="dataformV2">
    <tr>
        <td class="label">
            <D:Label runat="server" id="lblUnsuccessful">Unsuccessful</D:Label>
        </td>
        <td class="control">
            <D:Label ID="lblUnsuccessfulValue" runat="server" LocalizeText="false" />
        </td>
    </tr>
	<tr>
	    <td class="label">
            <D:Label runat="server" id="lblStart">Starts on</D:Label>
        </td>
        <td class="control">
            <X:DateTimeEdit ID="tbStart" runat="server"></X:DateTimeEdit>
            <D:Label ID="lblStartValue" runat="server" LocalizeText="false" />
        </td>
	    <td class="label">		    
	    </td>
	    <td class="control">		    
	    </td>
	</tr>
    <tr>	   		
        <td class="label">
            <D:Label runat="server" id="lblExpires">Expires on</D:Label>
        </td>
        <td class="control">
            <X:DateTimeEdit ID="tbExpires" runat="server"></X:DateTimeEdit>
            <D:Label ID="lblExpiresValue" runat="server" LocalizeText="false" />
        </td>
        <td class="label">
	    </td>
		<td class="control">
            <D:Button ID="btnRescheduleFailedCommands" runat="server" Text="Reschedule failed commands" />
        </td>
    </tr>	    
</table>