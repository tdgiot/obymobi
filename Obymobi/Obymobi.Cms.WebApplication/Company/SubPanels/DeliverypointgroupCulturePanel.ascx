﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.SubPanels.DeliverypointgroupCulturePanel" Codebehind="DeliverypointgroupCulturePanel.ascx.cs" %>
<table class="dataformV2">
	<tr>
        <td class="label">
		    <D:LabelEntityFieldInfo runat="server" ID="lblName">Culture</D:LabelEntityFieldInfo>
	    </td>
	    <td class="control">
		    <D:TextBoxString runat="server" ID="tbCultureName" LocalizeText="false" UseDataBinding="false" ReadOnly="true" />
	    </td>
         <td class="label">
             
	    </td>
	    <td class="control">
            
	    </td>
	</tr>
    <tr>
	    <td class="label">
		    <D:LabelEntityFieldInfo runat="server" ID="lblOutOfChargeTitle">Out of charge title</D:LabelEntityFieldInfo>
	    </td>
	    <td class="control">
		    <D:TextBoxString runat="server" ID="tbOutOfChargeTitle" UseDataBinding="false" />
	    </td>
        <td class="label">
		    <D:LabelEntityFieldInfo runat="server" ID="lblOutOfChargeMessage">Out of charge text</D:LabelEntityFieldInfo>
	    </td>
	    <td class="control">
		    <D:TextBoxString runat="server" ID="tbOutOfChargeMessage" UseDataBinding="false" />
	    </td>
	</tr>

<tr>
    <td class="label">
        <D:LabelEntityFieldInfo runat="server" ID="lblProductExtraCommentTitle">Product comment title</D:LabelEntityFieldInfo>
    </td>
    <td class="control">
        <D:TextBoxString runat="server" ID="tbProductExtraCommentTitle" UseDataBinding="false" />
    </td>
    <td class="label">
        <D:LabelEntityFieldInfo runat="server" ID="lblProductExtraCommentText">Product comment message</D:LabelEntityFieldInfo>
    </td>
    <td class="control">
        <D:TextBoxString runat="server" ID="tbProductExtraCommentText" UseDataBinding="false" />
    </td>
</tr>
    <tr>
	    <td class="label">
		    <D:LabelEntityFieldInfo runat="server" ID="lblOrderProcessedTitle">Order processed title</D:LabelEntityFieldInfo>
	    </td>
	    <td class="control">
		    <D:TextBoxString runat="server" ID="tbOrderProcessedTitle" UseDataBinding="false" />
	    </td>
        <td class="label">
		    <D:LabelEntityFieldInfo runat="server" ID="lblOrderProcessedMessage">Order processed text</D:LabelEntityFieldInfo>
	    </td>
	    <td class="control">
		    <D:TextBoxString runat="server" ID="tbOrderProcessedMessage" UseDataBinding="false" />
	    </td>
	</tr>
    <tr>
	    <td class="label">
		    <D:LabelEntityFieldInfo runat="server" ID="lblOrderFailedTitle">Order failed title</D:LabelEntityFieldInfo>
	    </td>
	    <td class="control">
		    <D:TextBoxString runat="server" ID="tbOrderFailedTitle" UseDataBinding="false" />
	    </td>
        <td class="label">
		    <D:LabelEntityFieldInfo runat="server" ID="lblOrderFailedMessage">Order failed text</D:LabelEntityFieldInfo>
	    </td>
	    <td class="control">
		    <D:TextBoxString runat="server" ID="tbOrderFailedMessage" UseDataBinding="false" />
	    </td>
	</tr>
    <tr>
	    <td class="label">
		    <D:LabelEntityFieldInfo runat="server" ID="lblOrderSavingTitle">Order saving title</D:LabelEntityFieldInfo>
	    </td>
	    <td class="control">
		    <D:TextBoxString runat="server" ID="tbOrderSavingTitle" UseDataBinding="false" />
	    </td>
        <td class="label">
		    <D:LabelEntityFieldInfo runat="server" ID="lvlOrderSavingMessage">Order saving text</D:LabelEntityFieldInfo>
	    </td>
	    <td class="control">
		    <D:TextBoxString runat="server" ID="tbOrderSavingMessage" UseDataBinding="false" />
	    </td>
	</tr>
    <tr>
	    <td class="label">
		    <D:LabelEntityFieldInfo runat="server" ID="lblOrderCompletedTitle">Order completed title</D:LabelEntityFieldInfo>
	    </td>
	    <td class="control">
		    <D:TextBoxString runat="server" ID="tbOrderCompletedTitle" UseDataBinding="false" />
	    </td>
        <td class="label">
		    <D:LabelEntityFieldInfo runat="server" ID="lblOrderCompletedMessage">Order completed text</D:LabelEntityFieldInfo>
	    </td>
	    <td class="control">
		    <D:TextBoxString runat="server" ID="tbOrderCompletedMessage" UseDataBinding="false" />
	    </td>
	</tr>
    <tr>
	    <td class="label">
		    <D:LabelEntityFieldInfo runat="server" ID="lblServiceSavingTitle">Service saving title</D:LabelEntityFieldInfo>
	    </td>
	    <td class="control">
		    <D:TextBoxString runat="server" ID="tbServiceSavingTitle" UseDataBinding="false" />
	    </td>
        <td class="label">
		    <D:LabelEntityFieldInfo runat="server" ID="lblServiceSavingMessage">Service saving text</D:LabelEntityFieldInfo>
	    </td>
	    <td class="control">
		    <D:TextBoxString runat="server" ID="tbServiceSavingMessage" UseDataBinding="false" />
	    </td>
	</tr>
    <tr>
	    <td class="label">
		    <D:LabelEntityFieldInfo runat="server" ID="lblServiceProcessedTitle">Service processed title</D:LabelEntityFieldInfo>
	    </td>
	    <td class="control">
		    <D:TextBoxString runat="server" ID="tbServiceProcessedTitle" UseDataBinding="false" />
	    </td>
        <td class="label">
		    <D:LabelEntityFieldInfo runat="server" ID="lblServiceProcessedMessage">Service processed text</D:LabelEntityFieldInfo>
	    </td>
	    <td class="control">
		    <D:TextBoxString runat="server" ID="tbServiceProcessedMessage" UseDataBinding="false" />
	    </td>
	</tr>
    <tr>
	    <td class="label">
		    <D:LabelEntityFieldInfo runat="server" ID="lblServiceFailedTitle">Service failed title</D:LabelEntityFieldInfo>
	    </td>
	    <td class="control">
		    <D:TextBoxString runat="server" ID="tbServiceFailedTitle" UseDataBinding="false" />
	    </td>
        <td class="label">
		    <D:LabelEntityFieldInfo runat="server" ID="lblServiceFailedMessage">Service failed text</D:LabelEntityFieldInfo>
	    </td>
	    <td class="control">
		    <D:TextBoxString runat="server" ID="tbServiceFailedMessage" UseDataBinding="false" />
	    </td>
	</tr>
    <tr>
	    <td class="label">
		    <D:LabelEntityFieldInfo runat="server" ID="lblRatingSavingTitle">Rating saving title</D:LabelEntityFieldInfo>
	    </td>
	    <td class="control">
		    <D:TextBoxString runat="server" ID="tbRatingSavingTitle" UseDataBinding="false" />
	    </td>
        <td class="label">
		    <D:LabelEntityFieldInfo runat="server" ID="lblRatingSavingMessage">Rating saving text</D:LabelEntityFieldInfo>
	    </td>
	    <td class="control">
		    <D:TextBoxString runat="server" ID="tbRatingSavingMessage" UseDataBinding="false" />
	    </td>
	</tr>
    <tr>
	    <td class="label">
		    <D:LabelEntityFieldInfo runat="server" ID="lblRatingProcessedTitle">Rating processed title</D:LabelEntityFieldInfo>
	    </td>
	    <td class="control">
		    <D:TextBoxString runat="server" ID="tbRatingProcessedTitle" UseDataBinding="false" />
	    </td>
        <td class="label">
		    <D:LabelEntityFieldInfo runat="server" ID="lblRatingProcessedMessage">Rating processed text</D:LabelEntityFieldInfo>
	    </td>
	    <td class="control">
		    <D:TextBoxString runat="server" ID="tbRatingProcessedMessage" UseDataBinding="false" />
	    </td>
	</tr>
     <tr>
	    <td class="label">
		    <D:Label runat="server" ID="lblOrderingNotAvailableMessage">Ordering not available text</D:Label>
	    </td>
	    <td class="control">
		    <D:TextBoxString runat="server" ID="tbOrderingNotAvailableMessage" UseDataBinding="false" />
	    </td>
        <td class="label">
	    </td>
	    <td class="control">
	    </td>
	</tr>
    <tr>
        <td class="label">
			<D:LabelEntityFieldInfo runat="server" id="lblHotelUrl1">Tab 1 URL</D:LabelEntityFieldInfo>
		</td>
		<td class="control">
		    <D:TextBoxString runat="server" ID="tbHotelUrl1"  UseDataBinding="false" />
		</td>  
        <td class="label">
			<D:LabelEntityFieldInfo runat="server" id="lblHotelUrl1Caption">Tab 1 Caption</D:LabelEntityFieldInfo>
		</td>
		<td class="control">
		    <D:TextBoxString runat="server" ID="tbHotelUrl1Caption" UseDataBinding="false" />
		</td>        				
	</tr>
    <tr>
        <td class="label">
			<D:LabelEntityFieldInfo runat="server" id="lblHotelUrl1Zoom">Tab 1 Zoom</D:LabelEntityFieldInfo>
		</td>
		<td class="control">
			<D:TextBoxInt runat="server" ID="tbHotelUrl1Zoom"></D:TextBoxInt>
		</td>  
        <td class="label">
		</td>
		<td class="control">
		</td>        				
	</tr>
    <tr>
        <td class="label">
			<D:LabelEntityFieldInfo runat="server" id="lblHotelUrl2">Tab 2 URL</D:LabelEntityFieldInfo>
		</td>
		<td class="control">
		    <D:TextBoxString runat="server" ID="tbHotelUrl2"  UseDataBinding="false" />
		</td>  
        <td class="label">
			<D:LabelEntityFieldInfo runat="server" id="lblHotelUrl2Caption">Tab 2 Caption</D:LabelEntityFieldInfo>
		</td>
		<td class="control">
		    <D:TextBoxString runat="server" ID="tbHotelUrl2Caption" UseDataBinding="false" />
		</td>        				
	</tr>
    <tr>
        <td class="label">
			<D:LabelEntityFieldInfo runat="server" id="lblHotelUrl2Zoom">Tab 2 Zoom</D:LabelEntityFieldInfo>
		</td>
		<td class="control">
			<D:TextBoxInt runat="server" ID="tbHotelUrl2Zoom"></D:TextBoxInt>
		</td>  
        <td class="label">
		</td>
		<td class="control">
		</td>        				
	</tr>
    <tr>
        <td class="label">
			<D:LabelEntityFieldInfo runat="server" id="lblHotelUrl3">Tab 3 URL</D:LabelEntityFieldInfo>
		</td>
		<td class="control">
		    <D:TextBoxString runat="server" ID="tbHotelUrl3"  UseDataBinding="false" />
		</td>  
        <td class="label">
			<D:LabelEntityFieldInfo runat="server" id="lblHotelUrl3Caption">Tab 3 Caption</D:LabelEntityFieldInfo>
		</td>
		<td class="control">
		    <D:TextBoxString runat="server" ID="tbHotelUrl3Caption" UseDataBinding="false" />
		</td>        				
	</tr>
    <tr>
        <td class="label">
			<D:LabelEntityFieldInfo runat="server" id="lblHotelUrl3Zoom">Tab 3 Zoom</D:LabelEntityFieldInfo>
		</td>
		<td class="control">
			<D:TextBoxInt runat="server" ID="tbHotelUrl3Zoom"></D:TextBoxInt>
		</td>  
        <td class="label">
		</td>
		<td class="control">
		</td>        				
	</tr>
    <tr>
        <td class="label">
		    <D:LabelEntityFieldInfo runat="server" ID="lblDeliverypointCaption">Deliverypoint caption</D:LabelEntityFieldInfo>
	    </td>
	    <td class="control">
		    <D:TextBoxString runat="server" ID="tbDeliverypointCaption"  UseDataBinding="false" />
	    </td> 
    </tr>
    <tr>
	    <td class="label">
		    <D:LabelEntityFieldInfo runat="server" ID="lblPmsDeviceLockedTitle">PMS device locked title</D:LabelEntityFieldInfo>
	    </td>
	    <td class="control">
		    <D:TextBoxString runat="server" ID="tbPmsDeviceLockedTitle" UseDataBinding="false" />
	    </td>
        <td class="label">
		    <D:LabelEntityFieldInfo runat="server" ID="lblPmsDeviceLockedText">PMS device locked text</D:LabelEntityFieldInfo>
	    </td>
	    <td class="control">
		    <D:TextBoxString runat="server" ID="tbPmsDeviceLockedText" UseDataBinding="false" />
	    </td>
    </tr>
    <tr>
	    <td class="label">
		    <D:LabelEntityFieldInfo runat="server" ID="lblPmsCheckinFailedTitle">PMS checkin failed title</D:LabelEntityFieldInfo>
	    </td>
	    <td class="control">
		    <D:TextBoxString runat="server" ID="tbPmsCheckinFailedTitle" UseDataBinding="false" />
	    </td>
        <td class="label">
		    <D:LabelEntityFieldInfo runat="server" ID="lblPmsCheckinFailedText">PMS checkin failed text</D:LabelEntityFieldInfo>
	    </td>
	    <td class="control">
		    <D:TextBoxString runat="server" ID="tbPmsCheckinFailedText" UseDataBinding="false" />
	    </td>
    </tr>
    <tr>
	    <td class="label">
		    <D:LabelEntityFieldInfo runat="server" ID="lblPmsRestartTitle">PMS restart title</D:LabelEntityFieldInfo>
	    </td>
	    <td class="control">
		    <D:TextBoxString runat="server" ID="tbPmsRestartTitle" UseDataBinding="false" />
	    </td>
        <td class="label">
		    <D:LabelEntityFieldInfo runat="server" ID="lblPmsRestartText">PMS restart text</D:LabelEntityFieldInfo>
	    </td>
	    <td class="control">
		    <D:TextBoxString runat="server" ID="tbPmsRestartText" UseDataBinding="false" />
	    </td>
    </tr>
    <tr>
	    <td class="label">
		    <D:LabelEntityFieldInfo runat="server" ID="lblPmsWelcomeTitle">PMS welcome title</D:LabelEntityFieldInfo>
	    </td>
	    <td class="control">
		    <D:TextBoxString runat="server" ID="tbPmsWelcomeTitle" UseDataBinding="false" />
	    </td>
        <td class="label">
		    <D:LabelEntityFieldInfo runat="server" ID="lblPmsWelcomeText">PMS welcome text</D:LabelEntityFieldInfo>
	    </td>
	    <td class="control">
		    <D:TextBoxString runat="server" ID="tbPmsWelcomeText" UseDataBinding="false" />
	    </td>
    </tr>
    <tr>
	    <td class="label">
		    <D:LabelEntityFieldInfo runat="server" ID="lblPmsCheckoutCompleteTitle">PMS checkout complete title</D:LabelEntityFieldInfo>
	    </td>
	    <td class="control">
		    <D:TextBoxString runat="server" ID="tbPmsCheckoutCompleteTitle" UseDataBinding="false" />
	    </td>
        <td class="label">
		    <D:LabelEntityFieldInfo runat="server" ID="lblPmsCheckoutCompleteText">PMS checkout complete text</D:LabelEntityFieldInfo>
	    </td>
	    <td class="control">
		    <D:TextBoxString runat="server" ID="tbPmsCheckoutCompleteText" UseDataBinding="false" />
	    </td>
	</tr>
    <tr>
	    <td class="label">
		    <D:LabelEntityFieldInfo runat="server" ID="lblPmsCheckoutApproveText">PMS checkout approve text</D:LabelEntityFieldInfo>
	    </td>
	    <td class="control">
		    <D:TextBoxString runat="server" ID="tbPmsCheckoutApproveText" UseDataBinding="false" />
	    </td>
	    <td class="label">
		    <D:LabelEntityFieldInfo runat="server" ID="lblRoomserviceChargeText">Room service charge text</D:LabelEntityFieldInfo>
	    </td>
	    <td class="control">
		    <D:TextBoxString runat="server" ID="tbRoomserviceChargeText" UseDataBinding="false" />
	    </td>
    </tr>
    <tr>
        <td class="label">
            <D:LabelEntityFieldInfo runat="server" ID="lblPrintingConfirmationTitle">Printing confirmation title</D:LabelEntityFieldInfo>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbPrintingConfirmationTitle" UseDataBinding="false" />
        </td>
        <td class="label">
            <D:LabelEntityFieldInfo runat="server" ID="lblPrintingConfirmationText">Printing confirmation text</D:LabelEntityFieldInfo>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbPrintingConfirmationText" UseDataBinding="false" />
        </td>
    </tr>
    <tr>
        <td class="label">
            <D:LabelEntityFieldInfo runat="server" ID="lblPrintingSucceededTitle">Printing succeeded title</D:LabelEntityFieldInfo>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbPrintingSucceededTitle" UseDataBinding="false" />
        </td>
        <td class="label">
            <D:LabelEntityFieldInfo runat="server" ID="lblPrintingSucceededText">Printing succeeded text</D:LabelEntityFieldInfo>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbPrintingSucceededText" UseDataBinding="false" />
        </td>
    </tr>
    <tr>
        <td class="label">
            <D:LabelEntityFieldInfo runat="server" ID="lblSuggestionsCaption">Suggestions caption</D:LabelEntityFieldInfo>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbSuggestionsCaption" UseDataBinding="false" />
        </td>
        <td class="label">
            <D:LabelEntityFieldInfo runat="server" ID="lblPagesCaption">Pages caption</D:LabelEntityFieldInfo>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbPagesCaption" UseDataBinding="false" />
        </td>
    </tr>
    <tr>
        <td class="label">
            <D:LabelEntityFieldInfo runat="server" id="lblChargerRemovedDialogTitle">Charger removed title</D:LabelEntityFieldInfo>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbChargerRemovedDialogTitle" UseDataBinding="false" />
        </td>
        <td class="label">
            <D:LabelEntityFieldInfo runat="server" id="lblChargerRemovedDialogText">Charger removed text</D:LabelEntityFieldInfo>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbChargerRemovedDialogText" UseDataBinding="false"/>
        </td>
    </tr>
    <tr>
        <td class="label">
            <D:LabelEntityFieldInfo runat="server" id="lblChargerRemovedReminderDialogTitle">Charger removed reminder title</D:LabelEntityFieldInfo>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbChargerRemovedReminderDialogTitle" UseDataBinding="false" />
        </td>
        <td class="label">
            <D:LabelEntityFieldInfo runat="server" id="lblChargerRemovedReminderDialogText">Charger removed reminder text</D:LabelEntityFieldInfo>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbChargerRemovedReminderDialogText" UseDataBinding="false"/>
        </td>
    </tr>
    <tr>
        <td class="label">
            <D:LabelEntityFieldInfo runat="server" id="lblTurnOffPrivacyTitle">Turn off privacy title</D:LabelEntityFieldInfo>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbTurnOffPrivacyTitle" UseDataBinding="false" />
        </td>
        <td class="label">
            <D:LabelEntityFieldInfo runat="server" id="lblTurnOffPrivacyText">Turn off privacy text</D:LabelEntityFieldInfo>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbTurnOffPrivacyText" UseDataBinding="false"/>
        </td>
    </tr>
    <tr>
        <td class="label">
            <D:LabelEntityFieldInfo runat="server" id="lblItemCurrentlyUnavailableText">Item currently unavailable</D:LabelEntityFieldInfo>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbItemCurrentlyUnavailableText" UseDataBinding="false"/>
        </td>
        <td class="label">
        </td>
        <td class="control">
        </td>
    </tr>
     <tr>
        <td class="label">
            <D:Label runat="server" id="lblAlarmSetWhileNotChargingTitle">Alarm set while not charging title</D:Label>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbAlarmSetWhileNotChargingTitle" UseDataBinding="false"/>
        </td>
        <td class="label">
            <D:Label runat="server" id="lblAlarmSetWhileNotChargingText">Alarm set while not charging text</D:Label>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbAlarmSetWhileNotChargingText" UseDataBinding="false"/>
        </td>
    </tr>
     <tr>
        <td class="label">
            <D:Label runat="server" id="lblTitle">Title</D:Label>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbTitle" UseDataBinding="false"/>
        </td>
        <td class="label">
            <D:Label runat="server" id="lblDescription">Description</D:Label>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbDescription" UseDataBinding="false"/>
        </td>
    </tr>
    <tr>
        <td class="label">
            <D:Label runat="server" id="lblDeliveryLocationMismatchTitle">Delivery location mismatch title</D:Label>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbDeliveryLocationMismatchTitle" UseDataBinding="false"/>
        </td>
        <td class="label">
            <D:Label runat="server" id="lblDeliveryLocationMismatchText">Delivery location mismatch text</D:Label>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbDeliveryLocationMismatchText" UseDataBinding="false"/>
        </td>
    </tr>
    <tr>
	    <td class="label">
		    <D:LabelEntityFieldInfo runat="server" ID="lblBrowserAgeVerificationTitle">Browser Age Verification Title</D:LabelEntityFieldInfo>
	    </td>
		<td colspan="3" class="control">
		    <D:TextBox ID="tbBrowserAgeVerificationTitle" runat="server" UseDataBinding="false" />
		</td>
	</tr>
    <tr>
	    <td class="label">
		    <D:LabelEntityFieldInfo runat="server" ID="lblBrowserAgeVerificationText">Browser Age Verification Text</D:LabelEntityFieldInfo>
	    </td>
		<td colspan="3" class="control">
		    <D:TextBox ID="tbBrowserAgeVerificationText" runat="server" Rows="5" TextMode="MultiLine" UseDataBinding="false" />
		</td>
	</tr>
        <tr>
	    <td class="label">
		    <D:LabelEntityFieldInfo runat="server" ID="lblBrowserAgeVerificationButton">Browser Age Verification Button</D:LabelEntityFieldInfo>
	    </td>
		<td colspan="3" class="control">
		    <D:TextBox ID="tbBrowserAgeVerificationButton" runat="server" UseDataBinding="false" />
		</td>
	</tr>
</table>