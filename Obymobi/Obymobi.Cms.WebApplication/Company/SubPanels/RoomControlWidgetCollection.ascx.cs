﻿using Dionysos;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using Obymobi.Data.EntityClasses;
using Dionysos.Web.UI;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.ObymobiCms.Company.Subpanels
{
    public partial class RoomControlWidgetCollection : Dionysos.Web.UI.DevExControls.SubPanelLLBLOverviewDataSourceCollection
    {
        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "RoomControlWidget";
            base.OnInit(e);
            this.MainGridView.LoadComplete += MainGridView_LoadComplete;
            this.PageAsPageLLBLGenEntity.DataSourceLoaded += PageAsPageLLBLGenEntity_DataSourceLoaded;
        }

        private void PageAsPageLLBLGenEntity_DataSourceLoaded(object sender)
        {
            foreach (RoomControlWidgetType type in Enum.GetValues(typeof(RoomControlWidgetType)))
            {
                if (!this.IsRemoteControlWidgetType(type))
                    this.ddlRoomControlWidgetType.Items.Add(new DevExpress.Web.ListEditItem(EnumUtil.GetStringValue(type, null), (int)type));
            }
        }

        private bool IsRemoteControlWidgetType(RoomControlWidgetType type)
        {
            return type == RoomControlWidgetType.Volume1x4 ||
                type == RoomControlWidgetType.Channel1x4 ||
                type == RoomControlWidgetType.Navigation2x3 ||
                type == RoomControlWidgetType.Numpad2x3 || 
                type == RoomControlWidgetType.PowerButton1x1;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.btAdd.Click += new EventHandler(btAdd_Click);

            if (this.MainGridView != null)
            {
                this.MainGridView.HtmlRowPrepared += MainGridView_HtmlRowPrepared;
            }
        }

        private void MainGridView_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType != GridViewRowType.Data) return;
            bool isVisible = (bool)e.GetValue("Visible");
            if (!isVisible)
                e.Row.ForeColor = Color.Gray;
        }

        void btAdd_Click(object sender, EventArgs e)
        {
            if (this.ddlRoomControlWidgetType.ValidId <= 0)
            {
                this.PageAsPageLLBLGenEntity.AddInformator(Dionysos.Web.UI.InformatorType.Warning, this.PageAsPageLLBLGenEntity.Translate("ChooseARoomControlWidgetTypeToAddARoomControlWidget", "Choose a room control widget type to add an item"));
            }
            else 
            {
                this.Response.Redirect(string.Format("~/Company/RoomControlWidget.aspx?mode=add&RoomControlSectionId={0}&Type={1}", this.PageAsPageLLBLGenEntity.EntityId, this.ddlRoomControlWidgetType.ValidId));
            }            
        }

        private void MainGridView_LoadComplete(object sender, EventArgs e)
        {
            DevExpress.Web.GridViewDataColumn column = this.MainGridView.Columns["SortOrder"] as DevExpress.Web.GridViewDataColumn;
            if (column != null)
            {
                this.MainGridView.SortBy(column, DevExpress.Data.ColumnSortOrder.Ascending);
            }
        }        
    }
}