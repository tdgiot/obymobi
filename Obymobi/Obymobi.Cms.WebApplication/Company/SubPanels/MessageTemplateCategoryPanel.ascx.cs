﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Obymobi.ObymobiCms.Company.SubPanels
{
    public partial class MessageTemplateCategoryPanel : Dionysos.Web.UI.DevExControls.SubPanelLLBLOverviewDataSourceCollection
    {
        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "TerminalMessageTemplateCategory";
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.btnAdd.Click += btnAdd_Click;

            this.MainGridView.ShowEditColumn = false;
            this.MainGridView.ShowEditHyperlinkColumn = false;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            this.AddMessageTemplateCategory();
        }

        public override bool InitializeDataSource()
        {
            bool toReturn = base.InitializeDataSource();

            this.DataSource.PerformSelect += DataSource_PerformSelect;

            return toReturn;
        }

        private TerminalMessageTemplateCategoryCollection currentCollection = new TerminalMessageTemplateCategoryCollection();
        void DataSource_PerformSelect(object sender, PerformSelectEventArgs e)
        {
            currentCollection = (TerminalMessageTemplateCategoryCollection)this.DataSource.EntityCollection;

            SetGui();
        }

        private void SetGui()
        {
            List<int> currentLinkedTemplates = currentCollection.Select(x => x.MessageTemplateCategoryId).ToList();

            PredicateExpression filter = new PredicateExpression();
            filter.Add(MessageTemplateCategoryFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            filter.Add(MessageTemplateCategoryFields.MessageTemplateCategoryId != currentLinkedTemplates);

            MessageTemplateCategoryCollection messageTemplateCategories = new MessageTemplateCategoryCollection();
            messageTemplateCategories.GetMulti(filter);

            this.ddlMessageTemplateCategoryId.DataSource = messageTemplateCategories;
            this.ddlMessageTemplateCategoryId.DataBind();
        }

        private void AddMessageTemplateCategory()
        {
            if (this.ddlMessageTemplateCategoryId.ValidId > 0)
            {
                TerminalMessageTemplateCategoryEntity tmtce = new TerminalMessageTemplateCategoryEntity();
                tmtce.TerminalId = this.ParentDataSourceTyped.TerminalId;
                tmtce.MessageTemplateCategoryId = this.ddlMessageTemplateCategoryId.ValidId;

                tmtce.Save();
            }
        }

        private TerminalEntity ParentDataSourceTyped
        {
            get { return (TerminalEntity)this.ParentDataSource; }
        }
    }
}