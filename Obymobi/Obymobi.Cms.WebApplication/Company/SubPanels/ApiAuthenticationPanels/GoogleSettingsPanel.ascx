﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.Subpanels.ApiAuthenticationPanels.GoogleSettingsPanel" Codebehind="GoogleSettingsPanel.ascx.cs" %>
<table class="dataformV2">
    <tr>
        <td class="label">
            <D:Label runat="server" ID="lblFieldValue1">Refresh Token</D:Label>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbFieldValue1" ReadOnly="true"></D:TextBoxString>
        </td>
        <td class="label">
        </td>
        <td class="control">
        </td>
    </tr>
    <tr>
	    <td class="label">
	    </td>
	    <td class="control">
		    <D:Button ID="btnConnect" runat="server" Text="Connect" />
	    </td>
	    <td class="label">
	    </td>
	    <td class="control">
	    </td>
    </tr>
</table>
