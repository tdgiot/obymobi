﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.Subpanels.ApiAuthenticationPanels.HotSOSSettingsPanel" Codebehind="HotSOSSettingsPanel.ascx.cs" %>
<table class="dataformV2">
    <tr>
        <td class="label">
            <D:Label runat="server" ID="lblFieldValue1">Username</D:Label>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbFieldValue1"></D:TextBoxString>
        </td>
        <td class="label">
            <D:Label runat="server" ID="lblFieldValue2">Password</D:Label>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbFieldValue2"></D:TextBoxString>
        </td>
    </tr>
    <tr>
	    <td class="label">
            <D:Label runat="server" ID="lblFieldValue3">Webservice Url</D:Label>
	    </td>
	    <td class="control">
		    <D:TextBoxString runat="server" ID="tbFieldValue3"></D:TextBoxString>
	    </td>
	    <td class="label">
	    </td>
	    <td class="control">
	    </td>
    </tr>
</table>
