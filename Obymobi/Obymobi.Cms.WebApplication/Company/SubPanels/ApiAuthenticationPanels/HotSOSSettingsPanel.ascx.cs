﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using Dionysos.Web.UI;
using Obymobi.ObymobiCms.UI;
using Obymobi.Enums;
using Dionysos;
using System.Diagnostics;
using Obymobi.Logic.Cms;
using Dionysos.Interfaces.Data;
using Dionysos.Web;

namespace Obymobi.ObymobiCms.Company.Subpanels.ApiAuthenticationPanels
{
    public partial class HotSOSSettingsPanel : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public ApiAuthenticationEntity DataSourceAsApiAuthenticationEntity
        {
            get
            {
                return (this.Page as PageLLBLGenEntityCms).DataSource as ApiAuthenticationEntity;
            }
        }

    }
}