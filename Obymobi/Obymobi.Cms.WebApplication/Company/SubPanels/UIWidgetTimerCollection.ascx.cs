﻿using System;
using Dionysos;
using Obymobi.Data.EntityClasses;
using Dionysos.Web.UI.DevExControls;

namespace Obymobi.ObymobiCms.Company.Subpanels
{
    public partial class UIWidgetTimerCollection : SubPanelLLBLOverviewDataSourceCollection
    {
        #region Methods

        #endregion

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "UIWidgetTimer";
            base.OnInit(e);
            this.MainGridView.LoadComplete += this.MainGridView_LoadComplete;            
        }

        private void MainGridView_LoadComplete(object sender, EventArgs e)
        {
            DevExpress.Web.GridViewDataColumn column = this.MainGridView.Columns["CountToTime"] as DevExpress.Web.GridViewDataColumn;
            if (column != null)
            {
                this.MainGridView.SortBy(column, DevExpress.Data.ColumnSortOrder.Ascending);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.btAdd.Click += this.BtAdd_Click;            
        }

        private void BtAdd_Click(object sender, EventArgs e)
        {
            if (!this.teCountToTime.Value.HasValue)
            {
                this.PageAsPageLLBLGenEntity.AddInformator(Dionysos.Web.UI.InformatorType.Warning, this.PageAsPageLLBLGenEntity.Translate("ChooseADateTimeToAddAUIWidgetTimer", "Please select a datetime first"));
            }
            else
            {
                DateTime countToTime = DateTimeUtil.CombineDateAndTime(this.teCountToTime.Value.Value, DateTime.Now);
                DateTime? countToDate = this.deCountToDate.Value;

                DateTime countToTimeUtc = countToTime.LocalTimeToUtc(CmsSessionHelper.CompanyTimeZone);
                DateTime? countToDateUtc = countToDate.LocalTimeToUtc(CmsSessionHelper.CompanyTimeZone);

                UIWidgetTimerEntity uiWidgetTimerEntity = new UIWidgetTimerEntity();
                uiWidgetTimerEntity.UIWidgetId = this.PageAsPageLLBLGenEntity.EntityId;
                uiWidgetTimerEntity.CountToTime = countToTime;
                uiWidgetTimerEntity.CountToDate = countToDate;
                uiWidgetTimerEntity.CountToTimeUTC = countToTimeUtc;
                uiWidgetTimerEntity.CountToDateUTC = countToDateUtc;
                uiWidgetTimerEntity.Save();

                this.Response.Redirect(this.Request.RawUrl);
            }
        }

        #endregion

        #region Properties

        public UIWidgetEntity UIWidgetOnParentDataSource
        {
            get
            {
                return this.Parent.DataSource as UIWidgetEntity;
            }
        }

        #endregion
    }
}