﻿using DevExpress.Web.ASPxScheduler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Company_SubPanels_ScheduleAppointmentTooltip : ASPxSchedulerToolTipBase
{
    public override bool ToolTipShowStem { get { return false; } }
    public override string ClassName { get { return "ASPxClientAppointmentToolTip"; } }
    protected override void OnLoad(EventArgs e)
    {

        base.OnLoad(e);
    }
    protected override Control[] GetChildControls()
    {
        Control[] controls = new Control[] { lblSubject, lblLocation, lblInterval, lblShowMenu, btnDelete, btnEdit };
        return controls;
    }
}