﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Obymobi.ObymobiCms.Company.SubPanels
{


    public partial class RoutestepCollection
    {

        /// <summary>
        /// plhAddRoutestep control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Dionysos.Web.UI.WebControls.PlaceHolder plhAddRoutestep;

        /// <summary>
        /// btAddStep control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Dionysos.Web.UI.WebControls.Button btAddStep;

        /// <summary>
        /// gvcsRoutestep control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Dionysos.Web.UI.DevExControls.GridViewColumnSelector gvcsRoutestep;
    }
}
