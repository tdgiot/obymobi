﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.Subpanels.UITabCollection" Codebehind="UITabCollection.ascx.cs" %>
<div style="overflow:hidden;margin-bottom:10px;">
	<div style="float:left;margin-right:10px">
        <X:ComboBoxInt ID="ddlUITabType" runat="server" UseDataBinding="true" Width="150px" notdirty="true"></X:ComboBoxInt>
	</div>
    <div style="float:left;">
        <D:Button runat="server" ID="btAdd" CommandName="Add" Text="Tabblad toevoegen" ToolTip="Toevoegen" />
    </div>
</div>
<X:GridViewColumnSelector runat="server" ID="gvcsUITabs">
	<Columns />
</X:GridViewColumnSelector>

