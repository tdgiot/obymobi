﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.SubPanels.CompanyCulturePanel" Codebehind="CompanyCulturePanel.ascx.cs" %>
<table class="dataformV2">
	<tr>
	    <td class="label">
	    </td>
	    <td class="control">
	    </td>
	    <td class="label">
		    <D:LabelEntityFieldInfo runat="server" ID="lblName">Culture</D:LabelEntityFieldInfo>
	    </td>
	    <td class="control">
		    <D:TextBoxString runat="server" ID="tbCultureCode" LocalizeText="false" UseDataBinding="false" ReadOnly="true" />
	    </td>
	</tr>
    <tr>
	    <td class="label">
		    <D:LabelEntityFieldInfo runat="server" ID="lblDescriptionSingleLine">Description short</D:LabelEntityFieldInfo>
	    </td>
		<td colspan="3" class="control">
		    <D:TextBoxString ID="tbDescriptionSingleLine" runat="server" UseDataBinding="false" />                
		</td>
    </tr>
	<tr>
	    <td class="label">
		    <D:LabelEntityFieldInfo runat="server" ID="lblDescription">Description</D:LabelEntityFieldInfo>
	    </td>
		<td colspan="3" class="control">
		    <D:TextBox ID="tbDescription" runat="server" Rows="5" TextMode="MultiLine" UseDataBinding="false" />
		</td>
	</tr>
	<tr>
	    <td class="label">
		    <D:LabelEntityFieldInfo runat="server" ID="lblVenuePageDescription">Venue Page Description</D:LabelEntityFieldInfo>
	    </td>
		<td colspan="3" class="control">
		    <D:TextBox ID="tbVenuePageDescription" runat="server" Rows="5" TextMode="MultiLine" UseDataBinding="false" />
		</td>
	</tr>
</table>