﻿using System;
using System.Web.UI;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Company.SubPanels
{
    public partial class DeliverypointgroupCulturePanel : Dionysos.Web.UI.WebControls.SubPanelLLBLGenEntity
    {
        #region Properties

        public DeliverypointgroupEntity ParentDataSourceAsDeliverypointgroupEntity
        {
            get
            {
                return this.PageAsPageLLBLGenEntity.DataSource as DeliverypointgroupEntity;
            }
        }

        public string CultureCode {get;set;}
        public bool IsDefaultCulture;

        #endregion

        #region Methods

        protected void SetGui()
        {
            this.tbCultureName.Text = this.CultureCode;
            if (this.IsDefaultCulture)
            {
                foreach (Control control in this.ControlList)
                { 
                    Dionysos.Web.UI.WebControls.TextBox textbox = control as Dionysos.Web.UI.WebControls.TextBox;
                    if (textbox != null && 
                        (textbox != this.tbRoomserviceChargeText && 
                        textbox != this.tbSuggestionsCaption && 
                        textbox != this.tbPrintingConfirmationTitle && 
                        textbox != this.tbPrintingConfirmationText && 
                        textbox != this.tbPagesCaption &&
                        textbox != this.tbPrintingSucceededTitle && 
                        textbox != this.tbPrintingSucceededText &&
                        textbox != this.tbTitle &&
                        textbox != this.tbDescription && 
                        textbox != this.tbDeliveryLocationMismatchTitle && 
                        textbox != this.tbDeliveryLocationMismatchText))
                    {
                        textbox.Enabled = false;                    
                    }
                }
            }            
        }

        private void PopulateCustomTexts()
        {
            CustomTextRenderer renderer = new CustomTextRenderer(this.ParentDataSourceAsDeliverypointgroupEntity, this.CultureCode);

            renderer.RenderCustomText(this.tbOutOfChargeTitle, CustomTextType.DeliverypointgroupOutOfChargeTitle);
            renderer.RenderCustomText(this.tbOutOfChargeMessage, CustomTextType.DeliverypointgroupOutOfChargeText);
            renderer.RenderCustomText(this.tbProductExtraCommentTitle, CustomTextType.DeliverypointgroupProductExtraCommentTitle);
            renderer.RenderCustomText(this.tbProductExtraCommentText, CustomTextType.DeliverypointgroupProductExtraCommentText);
            renderer.RenderCustomText(this.tbOrderProcessedTitle, CustomTextType.DeliverypointgroupOrderProcessedTitle);
            renderer.RenderCustomText(this.tbOrderProcessedMessage, CustomTextType.DeliverypointgroupOrderProcessedText);
            renderer.RenderCustomText(this.tbOrderFailedTitle, CustomTextType.DeliverypointgroupOrderFailedTitle);
            renderer.RenderCustomText(this.tbOrderFailedMessage, CustomTextType.DeliverypointgroupOrderFailedText);
            renderer.RenderCustomText(this.tbOrderSavingTitle, CustomTextType.DeliverypointgroupOrderSavingTitle);
            renderer.RenderCustomText(this.tbOrderSavingMessage, CustomTextType.DeliverypointgroupOrderSavingText);
            renderer.RenderCustomText(this.tbOrderCompletedTitle, CustomTextType.DeliverypointgroupOrderCompletedTitle);
            renderer.RenderCustomText(this.tbOrderCompletedMessage, CustomTextType.DeliverypointgroupOrderCompletedText);
            renderer.RenderCustomText(this.tbServiceSavingTitle, CustomTextType.DeliverypointgroupServiceSavingTitle);
            renderer.RenderCustomText(this.tbServiceSavingMessage, CustomTextType.DeliverypointgroupServiceSavingText);
            renderer.RenderCustomText(this.tbServiceProcessedTitle, CustomTextType.DeliverypointgroupServiceProcessedTitle);
            renderer.RenderCustomText(this.tbServiceProcessedMessage, CustomTextType.DeliverypointgroupServiceProcessedText);
            renderer.RenderCustomText(this.tbServiceFailedTitle, CustomTextType.DeliverypointgroupServiceFailedTitle);
            renderer.RenderCustomText(this.tbServiceFailedMessage, CustomTextType.DeliverypointgroupServiceFailedText);
            renderer.RenderCustomText(this.tbRatingSavingTitle, CustomTextType.DeliverypointgroupRatingSavingTitle);
            renderer.RenderCustomText(this.tbRatingSavingMessage, CustomTextType.DeliverypointgroupRatingSavingText);
            renderer.RenderCustomText(this.tbRatingProcessedTitle, CustomTextType.DeliverypointgroupRatingProcessedTitle);
            renderer.RenderCustomText(this.tbRatingProcessedMessage, CustomTextType.DeliverypointgroupRatingProcessedText);
            renderer.RenderCustomText(this.tbOrderingNotAvailableMessage, CustomTextType.DeliverypointgroupOrderingNotAvailableText);
            renderer.RenderCustomText(this.tbHotelUrl1, CustomTextType.DeliverypointgroupHotelUrl1);
            renderer.RenderCustomText(this.tbHotelUrl1Caption, CustomTextType.DeliverypointgroupHotelUrl1Caption);
            renderer.RenderCustomText(this.tbHotelUrl2, CustomTextType.DeliverypointgroupHotelUrl2);
            renderer.RenderCustomText(this.tbHotelUrl2Caption, CustomTextType.DeliverypointgroupHotelUrl2Caption);
            renderer.RenderCustomText(this.tbHotelUrl3, CustomTextType.DeliverypointgroupHotelUrl3);
            renderer.RenderCustomText(this.tbHotelUrl3Caption, CustomTextType.DeliverypointgroupHotelUrl3Caption);
            renderer.RenderCustomText(this.tbDeliverypointCaption, CustomTextType.DeliverypointgroupDeliverypointCaption);
            renderer.RenderCustomText(this.tbPmsDeviceLockedTitle, CustomTextType.DeliverypointgroupPmsDeviceLockedTitle);
            renderer.RenderCustomText(this.tbPmsDeviceLockedText, CustomTextType.DeliverypointgroupPmsDeviceLockedText);
            renderer.RenderCustomText(this.tbPmsCheckinFailedTitle, CustomTextType.DeliverypointgroupPmsCheckinFailedTitle);
            renderer.RenderCustomText(this.tbPmsCheckinFailedText, CustomTextType.DeliverypointgroupPmsCheckinFailedText);
            renderer.RenderCustomText(this.tbPmsRestartTitle, CustomTextType.DeliverypointgroupPmsRestartTitle);
            renderer.RenderCustomText(this.tbPmsRestartText, CustomTextType.DeliverypointgroupPmsRestartText);
            renderer.RenderCustomText(this.tbPmsWelcomeTitle, CustomTextType.DeliverypointgroupPmsWelcomeTitle);
            renderer.RenderCustomText(this.tbPmsWelcomeText, CustomTextType.DeliverypointgroupPmsWelcomeText);
            renderer.RenderCustomText(this.tbPmsCheckoutCompleteTitle, CustomTextType.DeliverypointgroupPmsCheckoutCompleteTitle);
            renderer.RenderCustomText(this.tbPmsCheckoutCompleteText, CustomTextType.DeliverypointgroupPmsCheckoutCompleteText);
            renderer.RenderCustomText(this.tbPmsCheckoutApproveText, CustomTextType.DeliverypointgroupPmsCheckoutApproveText);
            renderer.RenderCustomText(this.tbRoomserviceChargeText, CustomTextType.DeliverypointgroupRoomserviceChargeText);
            renderer.RenderCustomText(this.tbPrintingConfirmationTitle, CustomTextType.DeliverypointgroupPrintingConfirmationTitle);
            renderer.RenderCustomText(this.tbPrintingConfirmationText, CustomTextType.DeliverypointgroupPrintingConfirmationText);
            renderer.RenderCustomText(this.tbPrintingSucceededTitle, CustomTextType.DeliverypointgroupPrintingSucceededTitle);
            renderer.RenderCustomText(this.tbPrintingSucceededText, CustomTextType.DeliverypointgroupPrintingSucceededText);
            renderer.RenderCustomText(this.tbSuggestionsCaption, CustomTextType.DeliverypointgroupSuggestionsCaption);
            renderer.RenderCustomText(this.tbPagesCaption, CustomTextType.DeliverypointgroupPagesCaption);
            renderer.RenderCustomText(this.tbChargerRemovedDialogTitle, CustomTextType.DeliverypointgroupChargerRemovedTitle);
            renderer.RenderCustomText(this.tbChargerRemovedDialogText, CustomTextType.DeliverypointgroupChargerRemovedText);
            renderer.RenderCustomText(this.tbChargerRemovedReminderDialogTitle, CustomTextType.DeliverypointgroupChargerRemovedReminderTitle);
            renderer.RenderCustomText(this.tbChargerRemovedReminderDialogText, CustomTextType.DeliverypointgroupChargerRemovedReminderText);
            renderer.RenderCustomText(this.tbTurnOffPrivacyTitle, CustomTextType.DeliverypointgroupTurnOffPrivacyTitle);
            renderer.RenderCustomText(this.tbTurnOffPrivacyText, CustomTextType.DeliverypointgroupTurnOffPrivacyText);
            renderer.RenderCustomText(this.tbItemCurrentlyUnavailableText, CustomTextType.DeliverypointgroupItemCurrentlyUnavailableText);
            renderer.RenderCustomText(this.tbAlarmSetWhileNotChargingTitle, CustomTextType.DeliverypointgroupAlarmSetWhileNotChargingTitle);
            renderer.RenderCustomText(this.tbAlarmSetWhileNotChargingText, CustomTextType.DeliverypointgroupAlarmSetWhileNotChargingText);
            renderer.RenderCustomText(this.tbTitle, CustomTextType.DeliverypointgroupTitle);
            renderer.RenderCustomText(this.tbDescription, CustomTextType.DeliverypointgroupDescription);
            renderer.RenderCustomText(this.tbDeliveryLocationMismatchTitle, CustomTextType.DeliverypointgroupDeliveryLocationMismatchTitle);
            renderer.RenderCustomText(this.tbDeliveryLocationMismatchText, CustomTextType.DeliverypointgroupDeliveryLocationMismatchText);
            renderer.RenderCustomText(this.tbBrowserAgeVerificationTitle, CustomTextType.DeliverypointgroupBrowserAgeVerificationTitle);
            renderer.RenderCustomText(this.tbBrowserAgeVerificationText, CustomTextType.DeliverypointgroupBrowserAgeVerificationText);
            renderer.RenderCustomText(this.tbBrowserAgeVerificationButton, CustomTextType.DeliverypointgroupBrowserAgeVerificationButton);
        }

        protected override void OnParentDataSourceLoaded()
        {
            base.OnParentDataSourceLoaded();
            this.PopulateCustomTexts();
        }

        public override bool Save()
        {
            this.SaveCustomTexts();
            return false;
        }

        private void SaveCustomTexts()
        {
            CustomTextRenderer renderer = new CustomTextRenderer(this.ParentDataSourceAsDeliverypointgroupEntity, this.CultureCode);

            renderer.SaveCustomText(this.tbOutOfChargeTitle, CustomTextType.DeliverypointgroupOutOfChargeTitle);
            renderer.SaveCustomText(this.tbOutOfChargeMessage, CustomTextType.DeliverypointgroupOutOfChargeText);
            renderer.SaveCustomText(this.tbProductExtraCommentTitle, CustomTextType.DeliverypointgroupProductExtraCommentTitle);
            renderer.SaveCustomText(this.tbProductExtraCommentText, CustomTextType.DeliverypointgroupProductExtraCommentText);
            renderer.SaveCustomText(this.tbOrderProcessedTitle, CustomTextType.DeliverypointgroupOrderProcessedTitle);
            renderer.SaveCustomText(this.tbOrderProcessedMessage, CustomTextType.DeliverypointgroupOrderProcessedText);
            renderer.SaveCustomText(this.tbOrderFailedTitle, CustomTextType.DeliverypointgroupOrderFailedTitle);
            renderer.SaveCustomText(this.tbOrderFailedMessage, CustomTextType.DeliverypointgroupOrderFailedText);
            renderer.SaveCustomText(this.tbOrderSavingTitle, CustomTextType.DeliverypointgroupOrderSavingTitle);
            renderer.SaveCustomText(this.tbOrderSavingMessage, CustomTextType.DeliverypointgroupOrderSavingText);
            renderer.SaveCustomText(this.tbOrderCompletedTitle, CustomTextType.DeliverypointgroupOrderCompletedTitle);
            renderer.SaveCustomText(this.tbOrderCompletedMessage, CustomTextType.DeliverypointgroupOrderCompletedText);
            renderer.SaveCustomText(this.tbServiceSavingTitle, CustomTextType.DeliverypointgroupServiceSavingTitle);
            renderer.SaveCustomText(this.tbServiceSavingMessage, CustomTextType.DeliverypointgroupServiceSavingText);
            renderer.SaveCustomText(this.tbServiceProcessedTitle, CustomTextType.DeliverypointgroupServiceProcessedTitle);
            renderer.SaveCustomText(this.tbServiceProcessedMessage, CustomTextType.DeliverypointgroupServiceProcessedText);
            renderer.SaveCustomText(this.tbServiceFailedTitle, CustomTextType.DeliverypointgroupServiceFailedTitle);
            renderer.SaveCustomText(this.tbServiceFailedMessage, CustomTextType.DeliverypointgroupServiceFailedText);
            renderer.SaveCustomText(this.tbRatingSavingTitle, CustomTextType.DeliverypointgroupRatingSavingTitle);
            renderer.SaveCustomText(this.tbRatingSavingMessage, CustomTextType.DeliverypointgroupRatingSavingText);
            renderer.SaveCustomText(this.tbRatingProcessedTitle, CustomTextType.DeliverypointgroupRatingProcessedTitle);
            renderer.SaveCustomText(this.tbRatingProcessedMessage, CustomTextType.DeliverypointgroupRatingProcessedText);
            renderer.SaveCustomText(this.tbOrderingNotAvailableMessage, CustomTextType.DeliverypointgroupOrderingNotAvailableText);
            renderer.SaveCustomText(this.tbHotelUrl1, CustomTextType.DeliverypointgroupHotelUrl1);
            renderer.SaveCustomText(this.tbHotelUrl1Caption, CustomTextType.DeliverypointgroupHotelUrl1Caption);
            renderer.SaveCustomText(this.tbHotelUrl2, CustomTextType.DeliverypointgroupHotelUrl2);
            renderer.SaveCustomText(this.tbHotelUrl2Caption, CustomTextType.DeliverypointgroupHotelUrl2Caption);
            renderer.SaveCustomText(this.tbHotelUrl3, CustomTextType.DeliverypointgroupHotelUrl3);
            renderer.SaveCustomText(this.tbHotelUrl3Caption, CustomTextType.DeliverypointgroupHotelUrl3Caption);
            renderer.SaveCustomText(this.tbDeliverypointCaption, CustomTextType.DeliverypointgroupDeliverypointCaption);
            renderer.SaveCustomText(this.tbPmsDeviceLockedTitle, CustomTextType.DeliverypointgroupPmsDeviceLockedTitle);
            renderer.SaveCustomText(this.tbPmsDeviceLockedText, CustomTextType.DeliverypointgroupPmsDeviceLockedText);
            renderer.SaveCustomText(this.tbPmsCheckinFailedTitle, CustomTextType.DeliverypointgroupPmsCheckinFailedTitle);
            renderer.SaveCustomText(this.tbPmsCheckinFailedText, CustomTextType.DeliverypointgroupPmsCheckinFailedText);
            renderer.SaveCustomText(this.tbPmsRestartTitle, CustomTextType.DeliverypointgroupPmsRestartTitle);
            renderer.SaveCustomText(this.tbPmsRestartText, CustomTextType.DeliverypointgroupPmsRestartText);
            renderer.SaveCustomText(this.tbPmsWelcomeTitle, CustomTextType.DeliverypointgroupPmsWelcomeTitle);
            renderer.SaveCustomText(this.tbPmsWelcomeText, CustomTextType.DeliverypointgroupPmsWelcomeText);
            renderer.SaveCustomText(this.tbPmsCheckoutCompleteTitle, CustomTextType.DeliverypointgroupPmsCheckoutCompleteTitle);
            renderer.SaveCustomText(this.tbPmsCheckoutCompleteText, CustomTextType.DeliverypointgroupPmsCheckoutCompleteText);
            renderer.SaveCustomText(this.tbPmsCheckoutApproveText, CustomTextType.DeliverypointgroupPmsCheckoutApproveText);
            renderer.SaveCustomText(this.tbRoomserviceChargeText, CustomTextType.DeliverypointgroupRoomserviceChargeText);
            renderer.SaveCustomText(this.tbPrintingConfirmationTitle, CustomTextType.DeliverypointgroupPrintingConfirmationTitle);
            renderer.SaveCustomText(this.tbPrintingConfirmationText, CustomTextType.DeliverypointgroupPrintingConfirmationText);
            renderer.SaveCustomText(this.tbPrintingSucceededTitle, CustomTextType.DeliverypointgroupPrintingSucceededTitle);
            renderer.SaveCustomText(this.tbPrintingSucceededText, CustomTextType.DeliverypointgroupPrintingSucceededText);
            renderer.SaveCustomText(this.tbSuggestionsCaption, CustomTextType.DeliverypointgroupSuggestionsCaption);
            renderer.SaveCustomText(this.tbPagesCaption, CustomTextType.DeliverypointgroupPagesCaption);
            renderer.SaveCustomText(this.tbChargerRemovedDialogTitle, CustomTextType.DeliverypointgroupChargerRemovedTitle);
            renderer.SaveCustomText(this.tbChargerRemovedDialogText, CustomTextType.DeliverypointgroupChargerRemovedText);
            renderer.SaveCustomText(this.tbChargerRemovedReminderDialogTitle, CustomTextType.DeliverypointgroupChargerRemovedReminderTitle);
            renderer.SaveCustomText(this.tbChargerRemovedReminderDialogText, CustomTextType.DeliverypointgroupChargerRemovedReminderText);
            renderer.SaveCustomText(this.tbTurnOffPrivacyTitle, CustomTextType.DeliverypointgroupTurnOffPrivacyTitle);
            renderer.SaveCustomText(this.tbTurnOffPrivacyText, CustomTextType.DeliverypointgroupTurnOffPrivacyText);
            renderer.SaveCustomText(this.tbItemCurrentlyUnavailableText, CustomTextType.DeliverypointgroupItemCurrentlyUnavailableText);
            renderer.SaveCustomText(this.tbAlarmSetWhileNotChargingTitle, CustomTextType.DeliverypointgroupAlarmSetWhileNotChargingTitle);
            renderer.SaveCustomText(this.tbAlarmSetWhileNotChargingText, CustomTextType.DeliverypointgroupAlarmSetWhileNotChargingText);
            renderer.SaveCustomText(this.tbTitle, CustomTextType.DeliverypointgroupTitle);
            renderer.SaveCustomText(this.tbDescription, CustomTextType.DeliverypointgroupDescription);
            renderer.SaveCustomText(this.tbDeliveryLocationMismatchTitle, CustomTextType.DeliverypointgroupDeliveryLocationMismatchTitle);
            renderer.SaveCustomText(this.tbDeliveryLocationMismatchText, CustomTextType.DeliverypointgroupDeliveryLocationMismatchText);
            renderer.SaveCustomText(this.tbBrowserAgeVerificationTitle, CustomTextType.DeliverypointgroupBrowserAgeVerificationTitle);
            renderer.SaveCustomText(this.tbBrowserAgeVerificationText, CustomTextType.DeliverypointgroupBrowserAgeVerificationText);
            renderer.SaveCustomText(this.tbBrowserAgeVerificationButton, CustomTextType.DeliverypointgroupBrowserAgeVerificationButton);
        }

        #endregion

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "CustomText";
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.SetGui();
        }

        #endregion
    }
}