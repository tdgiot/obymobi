﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.SubPanels.UIWidgetCulturePanel" Codebehind="UIWidgetCulturePanel.ascx.cs" %>
<table class="dataformV2">
    <tr>
		<td class="label">
			<D:LabelEntityFieldInfo runat="server" id="lblCaption">Caption</D:LabelEntityFieldInfo>
		</td>
		<td class="control">
			<D:TextBoxString ID="tbCaption" runat="server" UseDataBinding="false"></D:TextBoxString>
		</td>
        <td class="label">
		    <D:LabelEntityFieldInfo runat="server" ID="lblCulture">Culture</D:LabelEntityFieldInfo>
	    </td>
	    <td class="control">
		    <D:TextBoxString runat="server" ID="tbCultureName" LocalizeText="false" UseDataBinding="false" ReadOnly="true" />
	    </td>
	</tr>
    <D:PlaceHolder ID="plhAvailability" runat="server" Visible="false">
        <tr>
            <tr>
                <td class="label">
                    <D:Label runat="server" id="lblDialogTitle">Dialog title</D:Label>
                </td>
                <td class="control">
                    <D:TextBoxString ID="tbDialogTitle" runat="server" UseDataBinding="false"/>
                </td>
                <td class="label">                                    
                </td>
                <td class="control">                                    
                </td>
            </tr>
            <tr>
                <td class="label">
                    <D:Label runat="server" id="lblDialogTopText">Dialog top text</D:Label>
                </td>
                <td class="control" colspan="3">
                    <D:TextBoxMultiLine ID="tbTopText" runat="server" Rows="6" UseDataBinding="false"/>
                </td>
                <td class="label">
                </td>
                <td class="control">
                </td>
            </tr>
            <tr>
                <td class="label">
                    <D:Label runat="server" id="lblDialogBottomText">Dialog bottom text</D:Label>
                </td>
                <td class="control" colspan="3">
                    <D:TextBoxMultiLine ID="tbBottomText" runat="server" Rows="6" UseDataBinding="false"/>
                </td>
                <td class="label">
                </td>
                <td class="control">
                </td>
            </tr>
        </tr>
    </D:PlaceHolder>
    <D:PlaceHolder ID="plhBluetoothSpeaker" runat="server" Visible="false">
        <tr>
            <td class="label">
                <D:Label runat="server" id="lblConnectedText">Connected text</D:Label>
            </td>
            <td class="control">
                <D:TextBoxString ID="tbConnectedText" runat="server" UseDataBinding="false"/>
            </td>
            <td class="label">
                <D:Label runat="server" id="lblConnectingText">Connecting text</D:Label>
            </td>
            <td class="control">
                <D:TextBoxString ID="tbConnectingText" runat="server" UseDataBinding="false"/>
            </td>
        </tr>
        <tr>
            <td class="label">
                <D:Label runat="server" id="lblDisconnectedText">Disconnected text</D:Label>
            </td>
            <td class="control">
                <D:TextBoxString ID="tbDisconnectedText" runat="server" UseDataBinding="false"/>
            </td>
            <td class="label">
                <D:Label runat="server" id="lblDisconnectingText">Disconnecting text</D:Label>
            </td>
            <td class="control">
                <D:TextBoxString ID="tbDisconnectingText" runat="server" UseDataBinding="false"/>
            </td>
        </tr>
    </D:PlaceHolder>
</table>