﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.SubPanels.DeliverypointgroupAdvertisementCollection" Codebehind="DeliverypointgroupAdvertisementCollection.ascx.cs" %>
<div style="overflow:hidden;margin-bottom:10px;">
   <div style="float:left;margin-right:10px;">
   <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlAdvertisementId" UseDataBinding="true" EntityName="Advertisement" TextField="Name" PreventEntityCollectionInitialization="true" Width="250px" notdirty="true"></X:ComboBoxLLBLGenEntityCollection>
   </div>
   <div style="float:left;">
   <D:Button runat="server" ID="btnAdd" Text="Add advertisement" ToolTip="Add" />
   </div>
</div>

<X:GridViewColumnSelector runat="server" ID="gvcsDeliverypointgroupAdvertisement" >
	<Columns />
</X:GridViewColumnSelector>