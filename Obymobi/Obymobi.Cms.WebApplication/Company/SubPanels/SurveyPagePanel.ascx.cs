﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Data.CollectionClasses;
using Dionysos.Interfaces;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Dionysos.Web.UI.WebControls;
using Dionysos.Diagnostics;
using Obymobi.Data.EntityClasses;
using Dionysos.Web.UI;
using DevExpress.Web.ASPxTreeList;
using System.Data;
using Dionysos.Web;

namespace Obymobi.ObymobiCms.Company
{
    public partial class SurveyPagePanel : System.Web.UI.UserControl, Dionysos.Interfaces.ISaveableControl
    {
        #region Methods

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

        // Drag-and-drop to reorder nodes
        protected void Page_CustomCallback(object sender, TreeListCustomCallbackEventArgs e)
        {
            if (e.Argument.StartsWith("reorder:"))
            {
                string[] arg = e.Argument.Split(':');

                // Swap the SortOrder of the pages nodes
                SwapNodes(tlPages.FindNodeByKeyValue(arg[1]), tlPages.FindNodeByKeyValue(arg[2]));

                // Rerender the treelist
                this.RenderSurveyPages();
            }
        }

        private void SwapNodes(TreeListNode node1, TreeListNode node2)
        {
            if (node1 == null || node2 == null) return;

            SurveyPageEntity surveyPage1 = node1.DataItem as SurveyPageEntity;
            SurveyPageEntity surveyPage2 = node2.DataItem as SurveyPageEntity;

            if (surveyPage1 != null && surveyPage2 != null)
            {
                // Get the sort order of both pages
                int tmpSortOrder1 = surveyPage1.SortOrder;
                int tmpSortOrder2 = surveyPage2.SortOrder;

                // Switch the sort orders
                surveyPage1.SortOrder = tmpSortOrder2;
                surveyPage2.SortOrder = tmpSortOrder1;

                // Save both pages
                surveyPage1.Save();
                surveyPage2.Save();
            }
        }

        private void RenderSurveyPages()
        {
            PredicateExpression filter = new PredicateExpression(SurveyPageFields.SurveyId == this.DataSourceAsSurveyEntity.SurveyId);

            SortExpression sort = new SortExpression(new SortClause(SurveyPageFields.SortOrder, SortOperator.Ascending));

            SurveyPageCollection pages = new SurveyPageCollection();
            pages.GetMulti(filter, 0, sort);

            this.tlPages.DataSource = pages;
            this.tlPages.DataBind();
        }

        void HookUpEvents()
        {
            this.btAddPage.Click += new EventHandler(btAddPage_Click);
            this.btDeletePage.Click += new EventHandler(btDeletePage_Click);
        }

        #endregion

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            this.HookUpEvents();
        }

        void btAddPage_Click(object sender, EventArgs e)
        {
            string url = string.Format("~/Company/SurveyPage.aspx?entity=SurveyPageEntity&mode=add&SurveyId={0}", this.DataSourceAsSurveyEntity.SurveyId);
            WebShortcuts.ResponseRedirect(url, true);           
        }

        void btDeletePage_Click(object sender, EventArgs e)
        {
            // Create entityview
            EntityView<SurveyPageEntity> sp = this.DataSourceAsSurveyEntity.SurveyPageCollection.DefaultView;

            foreach (TreeListNode node in this.tlPages.Nodes)
            {
                if (node.Selected)
                { 
                    int surveyPageId = Int32.Parse(node.Key);

                    // Get the Survey page entity
                    sp.Filter = new PredicateExpression(SurveyPageFields.SurveyPageId == surveyPageId);

                    if (sp.Count > 0)
                    {
                        sp[0].Delete();
                        sp.RelatedCollection.Remove(sp[0]);
                    }
                }
            }

            this.RenderSurveyPages();
        }

        public bool Save()
        {
            // Save
            

            return true;
        }

        protected override void CreateChildControls()
        {
            this.RenderSurveyPages();

            base.CreateChildControls();
        }

        #endregion

        #region Properties

        public new PageLLBLGenEntity Page
        {
            get
            {
                return base.Page as PageLLBLGenEntity;
            }
        }

        public SurveyEntity DataSourceAsSurveyEntity
        {
            get
            {
                return this.Page.DataSource as SurveyEntity;
            }
        }

        #endregion

    }
}