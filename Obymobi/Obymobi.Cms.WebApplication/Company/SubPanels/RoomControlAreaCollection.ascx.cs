﻿using System;
using DevExpress.Web;
using Dionysos;
using Obymobi.Data.EntityClasses;
using Dionysos.Web.UI.DevExControls;
using Dionysos.Web;
using Obymobi.Data;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System.Drawing;

namespace Obymobi.ObymobiCms.Company.SubPanels
{
    public partial class RoomControlAreaCollection : SubPanelLLBLOverviewDataSourceCollection
    {
        #region Methods

        private void LoadUserControls()
        {

        }

        private void HookUpEvents()
        {
            this.btnAdd.Click += btnAdd_Click;
        }

        private void AddArea()
        {
            int roomControlConfigurationId;
            if (!QueryStringHelper.GetInt("id", out roomControlConfigurationId))
            {
                // Show message
                string errorMessage = ((Dionysos.Web.UI.PageDefault)this.Page).Translate("AreaCollection.ExistingConfigurationOnly", "Er kunnen alleen areas toegevoegd worden voor een bestaande configuratie");
                ((Dionysos.Web.UI.PageDefault)this.Page).AddInformator(Dionysos.Web.UI.InformatorType.Warning, errorMessage);
            }
            else if (this.tbAreaName.Text.IsNullOrWhiteSpace() || this.tbAreaName.Text.Equals("Name", StringComparison.InvariantCultureIgnoreCase))
            {
                // Show message
                string errorMessage = ((Dionysos.Web.UI.PageDefault)this.Page).Translate("AreaCollection.NoAreaName", "Vul een naam voor de area in");
                ((Dionysos.Web.UI.PageDefault)this.Page).AddInformator(Dionysos.Web.UI.InformatorType.Warning, errorMessage);
            }
            else
            {
                // Create the relation between the stationlist and the station
                RoomControlAreaEntity area = new RoomControlAreaEntity();
                area.RoomControlConfigurationId = roomControlConfigurationId;
                area.NameSystem = this.tbAreaName.Text;
                area.SortOrder = int.Parse(this.tbSortOrder.Text);

                if (area.Save())
                {
                    string message = ((Dionysos.Web.UI.PageDefault)this.Page).Translate("AreaCollection.AreaLinked", "De area is succesvol toegevoegd aan de configuratie!");
                    ((Dionysos.Web.UI.PageDefault)this.Page).AddInformator(Dionysos.Web.UI.InformatorType.Information, message);
                }
            }
        }

        private void SetGui()
        {
            int sortOrder = (this.RoomControlConfigurationEntity != null && this.RoomControlConfigurationEntity.RoomControlAreaCollection.Count > 0) ? this.RoomControlConfigurationEntity.RoomControlAreaCollection.LastSortOrder + 1 : 1;
            this.tbSortOrder.Text = sortOrder.ToString();
        }

        private void MainGridView_LoadComplete(object sender, EventArgs e)
        {
            DevExpress.Web.GridViewDataColumn column = this.MainGridView.Columns["SortOrder"] as DevExpress.Web.GridViewDataColumn;
            if (column != null)
            {
                this.MainGridView.SortBy(column, DevExpress.Data.ColumnSortOrder.Ascending);
            }
        }

        #endregion

        #region Events

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            this.EntityName = "RoomControlArea";
            base.OnInit(e);
            this.MainGridView.LoadComplete += MainGridView_LoadComplete;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.HookUpEvents();
            if (!this.IsPostBack)
            {
                this.SetGui();
            }

            if (this.MainGridView != null)
            {
                this.MainGridView.HtmlRowPrepared += MainGridView_HtmlRowPrepared;
            }
        }

        private void MainGridView_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType != GridViewRowType.Data) return;
            bool isVisible = (bool)e.GetValue("Visible");
            if (!isVisible)
                e.Row.ForeColor = Color.Gray;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            this.AddArea();

            // GK Refresh page because of the DevEx bug that all tabs get cleared
            QueryStringHelper qs = new QueryStringHelper();
            qs.AddItem(PageControl.queryStringElementToActiveTab, "RoomControlAreaCollection");
            this.Response.Redirect(qs.MergeQuerystringWithRawUrl());
        }

        #endregion

        #region Properties

        public RoomControlConfigurationEntity RoomControlConfigurationEntity { get; set; }

        #endregion
    }
}