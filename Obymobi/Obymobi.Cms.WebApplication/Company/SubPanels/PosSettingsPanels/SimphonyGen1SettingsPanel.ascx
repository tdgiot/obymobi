﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.Subpanels.PosSettingsPanel.SimphonyGen1SettingsPanel" Codebehind="SimphonyGen1SettingsPanel.ascx.cs" %>
<table class="dataformV2">
    <tr>
        <td class="label">
            <D:Label runat="server" ID="lblPosValue1">Webservice URL</D:Label>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbPosValue1"></D:TextBoxString>
        </td>
        <td></td>
        <td></td>        
    </tr>
    <tr>
        <td class="label">
            <D:Label runat="server" ID="lblPosValue6">Vendor/Company Code</D:Label>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbPosValue6"></D:TextBoxString>
        </td>
        <td class="label">
            <D:Label runat="server" ID="lblPosValue2">Employee Object Number</D:Label>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbPosValue2"></D:TextBoxString>
        </td>
    </tr>
    <tr>
        <td class="label">
            <D:Label runat="server" ID="lblPosValue7">Tender Media Number</D:Label>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbPosValue7"></D:TextBoxString>
        </td>
        <td class="label">
            <D:Label runat="server" ID="lblPosValue8">Order Type Number</D:Label>

        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbPosValue8"></D:TextBoxString>
        </td>
    </tr>
    <tr>
        <td class="label">
            <D:Label runat="server" ID="lblPosValue3">Revenue Centers</D:Label>
        </td>
        <td class="control">
            <D:TextBoxMultiLine runat="server" ID="tbPosValue3" MaxLength="800"></D:TextBoxMultiLine><br />
            <small>Separate each Revenue Center with a comma</small>
        </td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td class="label">
            <D:Label runat="server" ID="lblPosValue4">Use Old Post Transaction Method</D:Label>
        </td>
        <td class="control">
            <D:Checkbox runat="server" ID="cbPosValue4"></D:Checkbox>
        </td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td class="label">
            <D:Label runat="server" ID="lblPosValue5">Sync In-stock Products Only</D:Label>
        </td>
        <td class="control">
            <D:Checkbox runat="server" ID="cbPosValue5"></D:Checkbox>
        </td>
        <td></td>
        <td></td>
    </tr>
</table>
