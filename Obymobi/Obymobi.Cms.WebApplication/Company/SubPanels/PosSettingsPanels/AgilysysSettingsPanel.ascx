﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.Subpanels.PosSettingsPanel.AgilysysSettingsPanel" Codebehind="AgilysysSettingsPanel.ascx.cs" %>
<table class="dataformV2">
    <tr>
        <td class="label">
            <D:Label runat="server" ID="lblPosValue1">Webservice URL</D:Label>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbPosValue1"></D:TextBoxString>
        </td>
        <td class="label">
            <D:Label runat="server" ID="lblPosValue2">Client ID</D:Label>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbPosValue2"></D:TextBoxString>
        </td>
    </tr>
    <tr>
        <td class="label">
            <D:Label runat="server" ID="lblPosValue3">Authentication Code</D:Label>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbPosValue3"></D:TextBoxString>
        </td>
        <td class="label">
            <D:Label runat="server" ID="lblPosValue4">Employee ID</D:Label>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbPosValue4"></D:TextBoxString>
        </td>
    </tr>
    <tr>
        <td class="label">
            <D:Label runat="server" ID="lblPosValue5">Check Type ID</D:Label>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbPosValue5"></D:TextBoxString>
        </td>
    </tr>
 </table>
