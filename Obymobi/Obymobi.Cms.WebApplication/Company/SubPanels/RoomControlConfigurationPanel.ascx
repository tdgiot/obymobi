﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.SubPanels.RoomControlConfigurationPanel" Codebehind="RoomControlConfigurationPanel.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v20.1" Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dxwtl" %>
<script type="text/javascript">    

    function OnInit(s, e) {        
        ASPxClientUtils.AttachEventToElement(s.GetMainElement(), "keypress",
        function (evt) {
            switch (evt.keyCode) {
                    //ENTER
                case 13:
                    if (s.IsEditing()) {
                        s.UpdateEdit();
                        evt.stopPropagation();
                    }
                    break;

                    //ESC               
                case 27:
                    if (s.IsEditing()) {
                        s.CancelEdit();
                        evt.stopPropagation();
                    }
                    break;
            }
        });
    }
</script>
<div style="width: 736px;">
    <span class="nobold" style="width: 100%; height: 24px; margin-left: 0;">
        <D:HyperLink runat="server" ID="hlExpandAll">Expand all</D:HyperLink> | <D:HyperLink runat="server" ID="hlCollapseAll">Collapse all</D:HyperLink> <D:Button runat="server" ID="btDeleteSelectedTop" style="float:right;" Text="Delete selected item(s)" />
    </span>                                    
    <dxwtl:ASPxTreeList ID="tlConfiguration" ClientInstanceName="tlConfiguration" runat="server" EnableViewState="false">
        <SettingsSelection Enabled="true" Recursive="false" />        
        <ClientSideEvents Init="OnInit" />                
    </dxwtl:ASPxTreeList>               
    <div id="divDeletePages" style="margin-top: 4px;">
        <D:Button runat="server" ID="btDeleteSelected" style="float:right;" Text="Delete selected items(s)" />
    </div> 
</div>