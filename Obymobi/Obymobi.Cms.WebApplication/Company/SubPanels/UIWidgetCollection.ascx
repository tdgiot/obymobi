﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.Subpanels.UIWidgetCollection" Codebehind="UIWidgetCollection.ascx.cs" %>
<div style="overflow:hidden;margin-bottom:10px;">
	<div style="float:left;margin-right:10px">
        <X:ComboBoxInt ID="ddlUIWidgetType" runat="server" UseDataBinding="true" Width="150px" notdirty="true"></X:ComboBoxInt>
	</div>
    <div style="float:left;">
        <D:Button runat="server" ID="btAdd" CommandName="Add" Text="Add Widget" ToolTip="Add" />
    </div>
</div>
<X:GridViewColumnSelector runat="server" ID="gvcsUIWidgets">
	<Columns />
</X:GridViewColumnSelector>

