﻿using System;
using DevExpress.Web;
using DevExpress.Web.ASPxScheduler;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;

public partial class Company_SubPanels_PriceScheduleAppointment : SchedulerFormControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public override void DataBind()
    {
        base.DataBind();

        PriceScheduleAppointmentForm container = (PriceScheduleAppointmentForm)Parent;
        ASPxScheduler control = container.Control;

        this.DataBindPriceLevels(container);

        if (container.IsNewAppointment)
        {
            this.ceBackgroundColor.Color = System.Drawing.Color.White;
            this.ceTextColor.Color = System.Drawing.Color.Black;
        }
        else
        {
            this.ceBackgroundColor.Color = System.Drawing.Color.FromArgb(container.BackgroundColor);
            this.ceTextColor.Color = System.Drawing.Color.FromArgb(container.TextColor);
        }

        this.btnOk.ClientSideEvents.Click = container.SaveHandler;
        this.btnCancel.ClientSideEvents.Click = container.CancelHandler;
    }

    private void DataBindPriceLevels(PriceScheduleAppointmentForm container)
    {
        PredicateExpression filter = new PredicateExpression(PriceLevelFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

        PriceLevelCollection priceLevels = new PriceLevelCollection();
        priceLevels.GetMulti(filter);

        this.ddlPriceLevelId.DataSource = priceLevels;
        this.ddlPriceLevelId.DataBind();
        this.ddlPriceLevelId.Value = container.PriceLevelId;
    }

    protected override ASPxEditBase[] GetChildEditors()
    {
        return new ASPxEditBase[] { this.ddlPriceLevelId, this.ceBackgroundColor, this.ceTextColor };
    }

    protected override ASPxButton[] GetChildButtons()
    {
        ASPxButton[] buttons = new ASPxButton[] { this.btnOk, this.btnCancel };
        return buttons;
    }
}