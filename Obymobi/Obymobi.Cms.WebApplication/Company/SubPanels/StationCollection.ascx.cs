﻿using System;
using Dionysos;
using Obymobi.Data.EntityClasses;
using Dionysos.Web.UI.DevExControls;
using Dionysos.Web;

namespace Obymobi.ObymobiCms.Company.SubPanels
{
    public partial class StationCollection : SubPanelLLBLOverviewDataSourceCollection
    {
        #region Methods

        private void LoadUserControls()
        {

        }

        private void HookUpEvents()
        {
            this.btnAdd.Click += btnAdd_Click;
        }

        private void AddStation()
        {
            int stationListId;
            if (!QueryStringHelper.GetInt("id", out stationListId))
            {
                // Show message
                string errorMessage = ((Dionysos.Web.UI.PageDefault)this.Page).Translate("StationCollection.ExistingStationOnly", "Er kunnen alleen stations toegevoegd worden voor een bestaande station list");
                ((Dionysos.Web.UI.PageDefault)this.Page).AddInformator(Dionysos.Web.UI.InformatorType.Warning, errorMessage);
            }
            else if (this.tbStationName.Text.IsNullOrWhiteSpace() || this.tbStationName.Text.Equals("Name", StringComparison.InvariantCultureIgnoreCase))
            {
                // Show message
                string errorMessage = ((Dionysos.Web.UI.PageDefault)this.Page).Translate("StationCollection.NoStationName", "Vul een naam voor de station in");
                ((Dionysos.Web.UI.PageDefault)this.Page).AddInformator(Dionysos.Web.UI.InformatorType.Warning, errorMessage);
            }
            else if (this.tbChannel.Text.IsNullOrWhiteSpace() || this.tbChannel.Text.Equals("Channel", StringComparison.InvariantCultureIgnoreCase))
            {
                // Show message
                string errorMessage = "Please enter a channel";
                ((Dionysos.Web.UI.PageDefault)this.Page).AddInformator(Dionysos.Web.UI.InformatorType.Warning, errorMessage);
            }
            else
            {
                // Create the relation between the stationlist and the station
                StationEntity station = new StationEntity();
                station.StationListId = stationListId;
                station.Name = this.tbStationName.Text;
                station.Channel = this.tbChannel.Text;
                station.SortOrder = int.Parse(this.tbSortOrder.Text);

                if (station.Save())
                {
                    string message = ((Dionysos.Web.UI.PageDefault)this.Page).Translate("StationCollection.StationLinked", "De station is succesvol toegevoegd aan de station list!");
                    ((Dionysos.Web.UI.PageDefault)this.Page).AddInformator(Dionysos.Web.UI.InformatorType.Information, message);
                }
            }
        }

        private void SetGui()
        {
            int sortOrder = (this.StationListEntity != null && this.StationListEntity.StationCollection.Count > 0) ? this.StationListEntity.StationCollection.LastSortOrder + 1 : 1;
            this.tbSortOrder.Text = sortOrder.ToString();
        }

        private void MainGridView_LoadComplete(object sender, EventArgs e)
        {
            DevExpress.Web.GridViewDataColumn column = this.MainGridView.Columns["SortOrder"] as DevExpress.Web.GridViewDataColumn;
            if (column != null)
            {
                this.MainGridView.SortBy(column, DevExpress.Data.ColumnSortOrder.Ascending);
            }
        }

        #endregion

        #region Events

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            this.EntityName = "Station";
            base.OnInit(e);
            this.MainGridView.LoadComplete += new EventHandler(MainGridView_LoadComplete);
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.HookUpEvents();
            if (!this.IsPostBack)
            {
                this.SetGui();
            }                       
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            this.AddStation();

            // GK Refresh page because of the DevEx bug that all tabs get cleared
            QueryStringHelper qs = new QueryStringHelper();
            qs.AddItem(PageControl.queryStringElementToActiveTab, "Stations");
            this.Response.Redirect(qs.MergeQuerystringWithRawUrl());
        }

        #endregion

        #region Properties

        public StationListEntity StationListEntity { get; set; }

        #endregion
    }
}