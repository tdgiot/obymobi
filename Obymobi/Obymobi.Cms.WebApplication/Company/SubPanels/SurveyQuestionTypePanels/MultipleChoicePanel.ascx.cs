﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dionysos.Web;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.ObymobiCms.UI;
using System.Text;
using Dionysos.Web.UI.DevExControls;
using Obymobi.Enums;
using Dionysos.Web.UI;
using Dionysos;
using DevExpress.Web;

namespace Obymobi.ObymobiCms.Company.Subpanels.SurveyQuestionTypePanels
{
    public partial class MultipleChoicePanel : System.Web.UI.UserControl, Dionysos.Interfaces.ISaveableControl
    {
        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        #endregion

        #region Methods

        private void LoadUserControls()
        {
            // Display
            this.cbDisplay.DataBindEnum<Orientation>();
        }

        private void SetGui()
        {
            if (string.IsNullOrEmpty(this.SurveyQuestionEntity.FieldValue1))
            {
                // Default min option = 0
                this.SurveyQuestionEntity.FieldValue1 = "0";
            }

            if (string.IsNullOrEmpty(this.SurveyQuestionEntity.FieldValue2))
            {
                // Default max option = 0
                this.SurveyQuestionEntity.FieldValue2 = "0";
            }

            if (string.IsNullOrEmpty(this.SurveyQuestionEntity.FieldValue3))
            {
                // Default display = horizontal
                this.SurveyQuestionEntity.FieldValue3 = "0";
            }

            // Set the display
            this.cbDisplay.Value = Int32.Parse(this.SurveyQuestionEntity.FieldValue3);
        }

        public bool Save()
        {
            // Check if min & max selected values are integers

            int min = -1;
            int max = -1;

            if (!Int32.TryParse(this.tbFieldValue1.Text, out min))
                this.SurveyQuestionEntity.FieldValue1 = "0";

            if (!Int32.TryParse(this.tbFieldValue2.Text, out max))
                this.SurveyQuestionEntity.FieldValue2 = "0";

            // Set the display type
            this.SurveyQuestionEntity.FieldValue3 = this.cbDisplay.SelectedItem.Value.ToString();

            return this.SurveyQuestionEntity.Save();
        }

        #endregion
        
        #region Properties

        private SurveyQuestionEntity surveyQuestionEntity = null;
        public SurveyQuestionEntity SurveyQuestionEntity
        {
            get
            {
                return this.surveyQuestionEntity;
            }
            set
            {
                this.surveyQuestionEntity = value;
                this.LoadUserControls();
                this.SetGui();
            }
        }

        #endregion
    }
}