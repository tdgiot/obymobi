﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.Subpanels.SurveyQuestionTypePanels.YesNoPanel" Codebehind="YesNoPanel.ascx.cs" %>
<D:Panel ID="pnlYesNoPanel" runat="server" GroupingText="Yes/No">
    <tr>
        <td class="label">
            <D:Label runat="server" id="lblTargetQuestion">Target question</D:Label>
        </td>
        <td class="control">
            <X:ComboBoxLLBLGenEntityCollection ID="ddlTargetSurveyQuestionId" runat="server" EntityName="SurveyQuestion" TextField="FriendlyName" ValueField="SurveyQuestionId" UseDataBinding="true" PreventEntityCollectionInitialization="true" />
        </td>
    </tr>
</D:Panel>