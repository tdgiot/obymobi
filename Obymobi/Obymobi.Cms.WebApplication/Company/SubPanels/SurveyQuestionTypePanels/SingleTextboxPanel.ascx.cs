﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dionysos.Web;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.ObymobiCms.UI;
using System.Text;
using Dionysos.Web.UI.DevExControls;
using Obymobi.Enums;
using Dionysos.Web.UI;
using Dionysos;

namespace Obymobi.ObymobiCms.Company.Subpanels.SurveyQuestionTypePanels
{
    public partial class SingleTextboxPanel : System.Web.UI.UserControl, Dionysos.Interfaces.ISaveableControl
    {
        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        #endregion

        #region Methods

        private void SetGui()
        {
            if (string.IsNullOrEmpty(this.SurveyQuestionEntity.FieldValue2))
                this.SurveyQuestionEntity.FieldValue2 = "1";
        }
        
        public bool Save()
        {
            // Save

            int lines = -1;
            if (!Int32.TryParse(this.tbFieldValue2.Text, out lines))
                this.SurveyQuestionEntity.FieldValue2 = "1";

            if (lines <= 0)
                this.SurveyQuestionEntity.FieldValue2 = "1";

            return this.SurveyQuestionEntity.Save();
        }

        #endregion
        
        #region Properties

        private SurveyQuestionEntity surveyQuestionEntity = null;
        public SurveyQuestionEntity SurveyQuestionEntity
        {
            get
            {
                return this.surveyQuestionEntity;
            }
            set
            {
                this.surveyQuestionEntity = value;
                this.SetGui();
            }
        }

        #endregion
    }
}