﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dionysos.Web;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.ObymobiCms.UI;
using System.Text;
using Dionysos.Web.UI.DevExControls;
using Obymobi.Enums;
using Dionysos.Web.UI;
using Dionysos;
using DevExpress.Web;

namespace Obymobi.ObymobiCms.Company.Subpanels.SurveyQuestionTypePanels
{
    public partial class MatrixPanel : System.Web.UI.UserControl, Dionysos.Interfaces.ISaveableControl
    {
        #region Event Handlers

        private void LoadUserControls()
        {
            // Rows
            for (int i = 1; i < 6; i++)
                this.cbRows.Items.Add(new ListEditItem(i.ToString()));

            // Columns
            for (int i = 2; i < 6; i++)
                this.cbColumns.Items.Add(new ListEditItem(i.ToString()));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        #endregion

        #region Methods

        private void SetGui()
        {
            if (!this.IsPostBack)
                this.LoadUserControls();

            // Default min option = 0
            if (string.IsNullOrEmpty(this.SurveyQuestionEntity.FieldValue1))
                this.SurveyQuestionEntity.FieldValue1 = "0";

            // Default max option = 0
            if (string.IsNullOrEmpty(this.SurveyQuestionEntity.FieldValue2))
                this.SurveyQuestionEntity.FieldValue2 = "0";

            // Default 1 question
            if (this.SurveyQuestionEntity.SurveyQuestionCollection.Count == 0)
                this.SurveyQuestionEntity.FieldValue4 = "1";

            // Default 2 answers
            if (this.SurveyQuestionEntity.SurveyAnswerCollection.Count == 0)
                this.SurveyQuestionEntity.FieldValue5 = "2";
            
            this.RenderQuestions();
            this.RenderAnswers();
        }

        private void RenderQuestions()
        {
            // Set the correct number of rows
            this.cbRows.SelectedItem = this.cbRows.Items.FindByText(this.SurveyQuestionEntity.FieldValue4);
            
            // Clear the placeholders
            this.plhQuestions.Controls.Clear();

            StringBuilder builder = new StringBuilder();

            // Questions
            if (this.SurveyQuestionEntity.SurveyQuestionCollection.Count > 0)
            {
                int i = 2;
                foreach (SurveyQuestionEntity question in this.SurveyQuestionEntity.SurveyQuestionCollection)
                {
                    builder.AppendFormat("<tr>");
                    builder.AppendFormat("<td class='label'>");
                    builder.AppendFormat("<D:Label runat='server' id='lblRow{0}'>Row {0}</D:Label>", i);
                    builder.AppendFormat("</td>");
                    builder.AppendFormat("<td class='control' colspan='3'>");

                    this.plhQuestions.AddHtml(builder.ToString());
                    
                    Dionysos.Web.UI.WebControls.TextBoxString tb = new Dionysos.Web.UI.WebControls.TextBoxString();
                    tb.ID = question.SurveyQuestionId.ToString();
                    tb.Text = question.Question;

                    this.plhQuestions.Controls.Add(tb);

                    builder = new StringBuilder();
                    builder.AppendFormat("</td>");
                    builder.AppendFormat("</tr>");

                    this.plhQuestions.AddHtml(builder.ToString());

                    i++;
                }
            }
        }

        public void RenderAnswers()
        {
            this.cbColumns.SelectedItem = this.cbColumns.Items.FindByText(this.SurveyQuestionEntity.FieldValue5);
            
            this.plhAnswers.Controls.Clear();

            StringBuilder builder = new StringBuilder();

            // Answers
            if (this.SurveyQuestionEntity.SurveyAnswerCollection.Count > 0)
            {
                double tbAnswerWidth = (100 / this.SurveyQuestionEntity.SurveyAnswerCollection.Count) - 0.4;

                builder.AppendFormat("<tr>");
                builder.AppendFormat("<td class='label'>");
                builder.AppendFormat("<D:Label runat='server' id='lblColumns'>Columns</D:Label>");
                builder.AppendFormat("</td>");
                builder.AppendFormat("<td class='control' colspan='3'>");

                this.plhAnswers.AddHtml(builder.ToString());

                foreach (SurveyAnswerEntity answer in this.SurveyQuestionEntity.SurveyAnswerCollection)
                {
                    Dionysos.Web.UI.WebControls.TextBoxString tb = new Dionysos.Web.UI.WebControls.TextBoxString();
                    tb.ID = answer.SurveyAnswerId.ToString();
                    tb.Text = answer.Answer;
                    tb.Width = Unit.Percentage(tbAnswerWidth);

                    this.plhAnswers.Controls.Add(tb);
                }

                builder = new StringBuilder();
                builder.AppendFormat("</td>");
                builder.AppendFormat("</tr>");
                this.plhAnswers.AddHtml(builder.ToString());
            }
        }

        public bool Save()
        {
            // Save

            // Check if min & max selected values are integers
                        
            int min = -1;
            if (!Int32.TryParse(this.tbFieldValue1.Text, out min))
                this.SurveyQuestionEntity.FieldValue1 = "0";

            int max = -1;
            if (!Int32.TryParse(this.tbFieldValue2.Text, out max))
                this.SurveyQuestionEntity.FieldValue2 = "0";

            int rows = Int32.Parse(this.cbRows.SelectedValueString);
            int columns = Int32.Parse(this.cbColumns.SelectedValueString);

            // Set the rows and columns
            this.SurveyQuestionEntity.FieldValue4 = rows.ToString();
            this.SurveyQuestionEntity.FieldValue5 = columns.ToString();

            // Check if we need more or less questions
            this.CheckSubQuestions(rows, columns);   
            
            // Check if we need more or less answers
            this.CheckAnswers(columns);

            // Check if any data has been modified
            this.CheckForModifiedData();

            return this.SurveyQuestionEntity.Save();
        }

        private void CheckSubQuestions(int rows, int columns)
        {
            if (this.SurveyQuestionEntity.SurveyQuestionCollection.Count < rows - 1)
            {
                // We want a new question

                // Check if the main question needs some answers first
                if (this.SurveyQuestionEntity.SurveyAnswerCollection.Count == 0)
                {
                    // Create the answers for this question

                    int answerSortOrder = 1000;
                    SurveyAnswerCollection answerCollection = new SurveyAnswerCollection();
                    for (int j = 0; j < columns; j++)
                    {
                        answerCollection.Add(this.CreateEmptyAnswer(answerSortOrder, this.SurveyQuestionEntity.SurveyQuestionId));

                        answerSortOrder = answerSortOrder + 100;
                    }
                    this.SurveyQuestionEntity.SurveyAnswerCollection.AddRange(answerCollection);
                }

                int sortOrder = this.GetNewQuestionSortOrder();
                for (int i = this.SurveyQuestionEntity.SurveyQuestionCollection.Count; i < rows - 1; i++)
                {
                    // Create a brand new question
                    SurveyQuestionEntity question = this.CreateEmptyQuestion(sortOrder, this.SurveyQuestionEntity.SurveyAnswerCollection.Count);
                    
                    // Set the answers
                    foreach (SurveyAnswerEntity subQuestionAnswer in question.SurveyAnswerCollection)
                    {
                        var answer = this.SurveyQuestionEntity.SurveyAnswerCollection.FirstOrDefault(sa => sa.SortOrder == subQuestionAnswer.SortOrder);

                        if (answer != null)
                            subQuestionAnswer.Answer = answer.Answer;
                    }

                    // Save them
                    question.SurveyAnswerCollection.SaveMulti();

                    // Add the sub question to this main question
                    this.SurveyQuestionEntity.SurveyQuestionCollection.Add(question);

                    // Update the sort order
                    sortOrder = sortOrder + 100;
                }
            }
            else if (this.SurveyQuestionEntity.SurveyQuestionCollection.Count > rows - 1)
            {
                // Remove the last question(s)

                for (int i = this.SurveyQuestionEntity.SurveyQuestionCollection.Count; i > rows - 1; i--)
                {
                    SurveyQuestionEntity childQuestion = this.SurveyQuestionEntity.SurveyQuestionCollection[i - 1];

                    childQuestion.Delete();
                    this.SurveyQuestionEntity.SurveyQuestionCollection.Remove(childQuestion);
                }
            }
        }

        private void CheckAnswers(int columns)
        {
            if (this.SurveyQuestionEntity.SurveyAnswerCollection.Count < columns)
            { 
                // We need more columns

                for (int i = this.SurveyQuestionEntity.SurveyAnswerCollection.Count; i < columns; i++)
                {
                    // Create empty answer for the extra amount of columns

                    int newSortOrder = this.GetNewAnswerSortOrder();

                    SurveyAnswerEntity answer = this.CreateEmptyAnswer(newSortOrder, this.SurveyQuestionEntity.SurveyQuestionId);
                    this.SurveyQuestionEntity.SurveyAnswerCollection.Add(answer);

                    foreach (SurveyQuestionEntity question in this.SurveyQuestionEntity.SurveyQuestionCollection)
                    {
                        SurveyAnswerEntity newAnswer = this.CreateEmptyAnswer(newSortOrder, question.SurveyQuestionId);
                        question.SurveyAnswerCollection.Add(newAnswer);
                    }
                }
            }
            else if (this.SurveyQuestionEntity.SurveyAnswerCollection.Count > columns)
            {
                // Delete the answers untill we have the same amount of answers as columns

                for (int i = this.SurveyQuestionEntity.SurveyAnswerCollection.Count; i > columns; i--)
                { 
                    SurveyAnswerEntity answer = this.SurveyQuestionEntity.SurveyAnswerCollection[i - 1];

                    // Delete this answer at all the child questions
                    foreach (SurveyQuestionEntity childQuestion in this.SurveyQuestionEntity.SurveyQuestionCollection)
                    {
                        var childAnswer = childQuestion.SurveyAnswerCollection.FirstOrDefault(sa => sa.SortOrder == answer.SortOrder);

                        if (childAnswer != null)
                        { 
                            // Found it

                            SurveyAnswerEntity childAnswerEntity = (SurveyAnswerEntity)childAnswer;
                            childAnswerEntity.Delete();
                            childQuestion.SurveyAnswerCollection.Remove(childAnswerEntity);
                        }
                    }

                    // Delete the answer from this question
                    answer.Delete();
                    this.SurveyQuestionEntity.SurveyAnswerCollection.Remove(answer);
                }
            }
        }

        private void CheckForModifiedData()
        {
            // Questions
            foreach (Control control in this.plhQuestions.Controls)
            {
                if (control is Dionysos.Web.UI.WebControls.TextBoxString)
                {
                    var tb = control as Dionysos.Web.UI.WebControls.TextBoxString;
                    int surveyQuestionId = Convert.ToInt32(tb.ID);

                    var surveyQuestion = this.SurveyQuestionEntity.SurveyQuestionCollection.FirstOrDefault(sq => sq.SurveyQuestionId == surveyQuestionId);

                    if (surveyQuestion != null)
                    {
                        if (surveyQuestion.Question != tb.Text)
                        {
                            surveyQuestion.Question = tb.Text;
                            surveyQuestion.Save();
                        }
                    }
                }
            }

            // Answers
            foreach (Control control in this.plhAnswers.Controls)
            {
                if (control is Dionysos.Web.UI.WebControls.TextBoxString)
                {
                    var tb = control as Dionysos.Web.UI.WebControls.TextBoxString;
                    int surveyAnswerId = Convert.ToInt32(tb.ID);

                    var surveyAnswer = this.SurveyQuestionEntity.SurveyAnswerCollection.FirstOrDefault(sa => sa.SurveyAnswerId == surveyAnswerId);

                    if (surveyAnswer != null)
                    {
                        if (surveyAnswer.Answer != tb.Text)
                        {
                            // Text has been changed

                            surveyAnswer.Answer = tb.Text;
                            surveyAnswer.Save();

                            int sortOrder = surveyAnswer.SortOrder;

                            // Change the answers of the sub questions
                            foreach (SurveyQuestionEntity question in this.SurveyQuestionEntity.SurveyQuestionCollection)
                            {
                                var childQuestionAnswer = question.SurveyAnswerCollection.FirstOrDefault(sa => sa.SortOrder == sortOrder);

                                if (childQuestionAnswer != null)
                                {
                                    childQuestionAnswer.Answer = tb.Text;
                                    childQuestionAnswer.Save();
                                }
                            }
                        }
                    }
                }
            }
        }

        private int GetNewQuestionSortOrder()
        {
            int sortOrder = 1000;

            foreach (SurveyQuestionEntity question in this.SurveyQuestionEntity.SurveyQuestionCollection)
            {
                if (question.SortOrder >= sortOrder)
                    sortOrder = question.SortOrder + 100;
            }

            return sortOrder;
        }

        private int GetNewAnswerSortOrder()
        {
            int sortOrder = 1000;

            foreach (SurveyAnswerEntity answer in this.SurveyQuestionEntity.SurveyAnswerCollection)
            {
                if (answer.SortOrder >= sortOrder)
                    sortOrder = answer.SortOrder + 100;
            }

            return sortOrder;
        }

        private SurveyQuestionEntity CreateEmptyQuestion(int sortOrder, int answers)
        {
            SurveyQuestionEntity question = new SurveyQuestionEntity();
            question.SortOrder = sortOrder;
            question.Question = "";
            question.Type = (int)SurveyQuestionType.Matrix;
            question.ParentQuestionId = this.SurveyQuestionEntity.SurveyQuestionId;
            question.Save();

            int answerSortOrder = 1000;
            SurveyAnswerCollection answerCollection = new SurveyAnswerCollection();
            for (int j = 0; j < answers; j++)
            {
                answerCollection.Add(this.CreateEmptyAnswer(answerSortOrder, question.SurveyQuestionId));

                answerSortOrder = answerSortOrder + 100;
            }
            question.SurveyAnswerCollection.AddRange(answerCollection);

            return question;
        }

        private SurveyAnswerEntity CreateEmptyAnswer(int sortOrder, int surveyQuestionId)
        {
            SurveyAnswerEntity answer = new SurveyAnswerEntity();
            answer.SortOrder = sortOrder;
            answer.SurveyQuestionId = surveyQuestionId;
            answer.Answer = "";
            answer.Save();

            return answer;
        }

        #endregion
        
        #region Properties

        private SurveyQuestionEntity surveyQuestionEntity = null;
        public SurveyQuestionEntity SurveyQuestionEntity
        {
            get
            {
                return this.surveyQuestionEntity;
            }
            set
            {
                this.surveyQuestionEntity = value;

                this.SetGui();
            }
        }

        #endregion
    }
}