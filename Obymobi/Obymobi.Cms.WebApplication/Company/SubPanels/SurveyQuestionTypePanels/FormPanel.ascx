﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.Subpanels.SurveyQuestionTypePanels.FormPanel" Codebehind="FormPanel.ascx.cs" %>
    <D:Panel ID="pnlForm" runat="server" GroupingText="Form">
        <table class="dataformV2">
            <tr>
                <td class="label">
                    <D:Label runat="server" id="lblFieldValue2">Sub titel</D:Label>
                </td>
                <td class="control">
                    <D:TextBoxMultiLine runat="server" ID="tbFieldValue2"></D:TextBoxMultiLine>
                </td>
                <td class="label">
                    <D:Label runat="server" id="lblFields">Velden</D:Label>
                </td>
                <td class="control">
                    <X:ComboBoxInt ID="cbFormFields" runat="server"></X:ComboBoxInt>
                </td>
            </tr>    
        </table>
    </D:Panel>
<D:PlaceHolder runat="server" ID="plhFieldsPanel" Visible="false">
    <D:Panel ID="pnlFields" runat="server" GroupingText="Velden">
        <table class="dataformV2">
            <tr>
                <td colspan="4">
                    <D:PlaceHolder ID="plhFields" runat="server">
                    </D:PlaceHolder>
                </td>
            </tr>
        </table>
    </D:Panel>
</D:PlaceHolder>