﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dionysos.Web;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.ObymobiCms.UI;
using System.Text;
using Dionysos.Web.UI.DevExControls;
using Obymobi.Enums;
using Dionysos.Web.UI;
using Dionysos;
using DevExpress.Web;

namespace Obymobi.ObymobiCms.Company.Subpanels.SurveyQuestionTypePanels
{
    public partial class RatingPanel : System.Web.UI.UserControl, Dionysos.Interfaces.ISaveableControl
    {
        #region Event Handlers

        private void LoadUserControls()
        {
            // Rows
            for (int i = 1; i < 4; i++)
                this.cbRows.Items.Add(new ListEditItem(i.ToString()));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        #endregion

        #region Methods

        private void SetGui()
        {
            if (!this.IsPostBack)
                this.LoadUserControls();

            // Default 1 question
            if (this.SurveyQuestionEntity.SurveyQuestionCollection.Count == 0)
                this.SurveyQuestionEntity.FieldValue1 = "1";

            this.RenderQuestions();
        }

        private void RenderQuestions()
        {
            // Set the correct number of rows
            this.cbRows.SelectedItem = this.cbRows.Items.FindByText(this.SurveyQuestionEntity.FieldValue1);

            // Clear the placeholders
            this.plhQuestions.Controls.Clear();

            StringBuilder builder = new StringBuilder();

            // Questions
            if (this.SurveyQuestionEntity.SurveyQuestionCollection.Count > 0)
            {
                int i = 2;
                foreach (SurveyQuestionEntity question in this.SurveyQuestionEntity.SurveyQuestionCollection)
                {
                    builder.AppendFormat("<tr>");
                    builder.AppendFormat("<td class='label'>");
                    builder.AppendFormat("<D:Label runat='server' id='lblRow{0}'>Row {0}</D:Label>", i);
                    builder.AppendFormat("</td>");
                    builder.AppendFormat("<td class='control' colspan='3'>");

                    this.plhQuestions.AddHtml(builder.ToString());

                    Dionysos.Web.UI.WebControls.TextBoxString tb = new Dionysos.Web.UI.WebControls.TextBoxString();
                    tb.ID = question.SurveyQuestionId.ToString();
                    tb.Text = question.Question;

                    this.plhQuestions.Controls.Add(tb);

                    builder = new StringBuilder();
                    builder.AppendFormat("</td>");
                    builder.AppendFormat("</tr>");

                    this.plhQuestions.AddHtml(builder.ToString());

                    i++;
                }
            }
        }
                
        public bool Save()
        {
            // Save
            int rows = Int32.Parse(this.cbRows.SelectedValueString);

            // Set the rows and columns
            this.SurveyQuestionEntity.FieldValue1 = rows.ToString();

            // Check if we need more or less questions
            this.CheckSubQuestions(rows);

            // Check if any data has been modified
            this.CheckForModifiedData();

            return this.SurveyQuestionEntity.Save();
        }

        private void CheckSubQuestions(int rows)
        {
            if (this.SurveyQuestionEntity.SurveyQuestionCollection.Count < rows - 1)
            {
                // We want a new question

                // Check if the main question needs some answers first
                if (this.SurveyQuestionEntity.SurveyAnswerCollection.Count == 0)
                {
                    // Create the answers for this question
                    this.SurveyQuestionEntity.SurveyAnswerCollection.AddRange(this.CreateAnswers(this.SurveyQuestionEntity.SurveyQuestionId));
                }

                int sortOrder = this.GetNewQuestionSortOrder();
                for (int i = this.SurveyQuestionEntity.SurveyQuestionCollection.Count; i < rows - 1; i++)
                {
                    // Create a brand new question
                    SurveyQuestionEntity question = this.CreateQuestion(sortOrder);

                    // Add the answers
                    question.SurveyAnswerCollection.AddRange(this.CreateAnswers(question.SurveyQuestionId));

                    // Add the sub question to this main question
                    this.SurveyQuestionEntity.SurveyQuestionCollection.Add(question);

                    // Update the sort order
                    sortOrder = sortOrder + 100;
                }
            }
            else if (this.SurveyQuestionEntity.SurveyQuestionCollection.Count > rows - 1)
            {
                // Remove the last question(s)

                for (int i = this.SurveyQuestionEntity.SurveyQuestionCollection.Count; i > rows - 1; i--)
                {
                    SurveyQuestionEntity childQuestion = this.SurveyQuestionEntity.SurveyQuestionCollection[i - 1];

                    childQuestion.Delete();
                    this.SurveyQuestionEntity.SurveyQuestionCollection.Remove(childQuestion);
                }
            }
        }

        private void CheckForModifiedData()
        {
            // Questions
            foreach (Control control in this.plhQuestions.Controls)
            {
                if (control is Dionysos.Web.UI.WebControls.TextBoxString)
                {
                    var tb = control as Dionysos.Web.UI.WebControls.TextBoxString;
                    int surveyQuestionId = Convert.ToInt32(tb.ID);

                    var surveyQuestion = this.SurveyQuestionEntity.SurveyQuestionCollection.FirstOrDefault(sq => sq.SurveyQuestionId == surveyQuestionId);

                    if (surveyQuestion != null)
                    {
                        if (surveyQuestion.Question != tb.Text)
                        {
                            surveyQuestion.Question = tb.Text;
                            surveyQuestion.Save();
                        }
                    }
                }
            }
        }

        private int GetNewQuestionSortOrder()
        {
            int sortOrder = 1000;

            foreach (SurveyQuestionEntity question in this.SurveyQuestionEntity.SurveyQuestionCollection)
            {
                if (question.SortOrder >= sortOrder)
                    sortOrder = question.SortOrder + 100;
            }

            return sortOrder;
        }

        private int GetNewAnswerSortOrder()
        {
            int sortOrder = 1000;

            foreach (SurveyAnswerEntity answer in this.SurveyQuestionEntity.SurveyAnswerCollection)
            {
                if (answer.SortOrder >= sortOrder)
                    sortOrder = answer.SortOrder + 100;
            }

            return sortOrder;
        }

        private SurveyQuestionEntity CreateQuestion(int sortOrder)
        {
            SurveyQuestionEntity question = new SurveyQuestionEntity();
            question.SortOrder = sortOrder;
            question.Question = "";
            question.Type = (int)SurveyQuestionType.Matrix;
            question.ParentQuestionId = this.SurveyQuestionEntity.SurveyQuestionId;
            question.Save();

            return question;
        }

        private SurveyAnswerCollection CreateAnswers(int surveyQuestionId)
        {
            SurveyAnswerCollection answers = new SurveyAnswerCollection();

            int sortOrder = 1000;
            for (int i = 1; i < 6; i++)
            {
                SurveyAnswerEntity answer = new SurveyAnswerEntity();
                answer.SortOrder = sortOrder;
                answer.SurveyQuestionId = surveyQuestionId;
                answer.Answer = i.ToString();
                answer.Save();

                answers.Add(answer);
                sortOrder = sortOrder + 100;
            }

            return answers;
        }

        #endregion
        
        #region Properties

        private SurveyQuestionEntity surveyQuestionEntity = null;
        public SurveyQuestionEntity SurveyQuestionEntity
        {
            get
            {
                return this.surveyQuestionEntity;
            }
            set
            {
                this.surveyQuestionEntity = value;
                this.SetGui();
            }
        }

        #endregion
    }
}