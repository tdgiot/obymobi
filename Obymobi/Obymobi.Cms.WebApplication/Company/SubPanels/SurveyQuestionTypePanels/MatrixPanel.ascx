﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.Subpanels.SurveyQuestionTypePanels.MatrixPanel" Codebehind="MatrixPanel.ascx.cs" %>
<D:Panel ID="pnlMatrix" runat="server" GroupingText="Matrix">
    <tr>
        <td class="label">
            <D:Label runat="server" id="lblMainQuestionTitle">Main question title</D:Label>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbFieldValue3"></D:TextBoxString>
        </td>
    </tr>
    <tr>
        <td class="label">
            <D:Label runat="server" id="lblMinSelected">Minimaal # geselecteerd</D:Label>
        </td>
        <td class="control">
            <D:TextBox runat="server" ID="tbFieldValue1" IsRequired="true"></D:TextBox>
        </td>
        <td class="label">
            <D:Label runat="server" id="lblMaxSelected">Maximaal # geselecteerd</D:Label>
        </td>
        <td class="control">
            <D:TextBox runat="server" ID="tbFieldValue2" IsRequired="true"></D:TextBox>
        </td>
    </tr>    
    <tr>
        <td class="label">
            <D:Label runat="server" id="lblRows">Rijen</D:Label>
        </td>
        <td class="control">
            <X:ComboBoxInt ID="cbRows" runat="server"></X:ComboBoxInt>
            <D:Label ID="lblMatrixComment" runat="server"><small>Note: De rijen en kolommen moeten opgegeven zijn om meer opties te verkrijgen.</small></D:Label>
        </td>
	    <td class="label">
            <D:Label runat="server" id="lblCols">Kolommen</D:Label>
        </td>
        <td class="control">
            <X:ComboBoxInt ID="cbColumns" runat="server"></X:ComboBoxInt>
        </td>
    </tr>
    <tr>
        <td colspan="4">
            <D:PlaceHolder ID="plhAnswers" runat="server" Visible="true">
            </D:PlaceHolder>
            <tr>
                <td class="label">
                    <D:Label runat='server' id='lblQuestion'>Row 1</D:Label>
                </td>
                <td class="control" colspan="3">
                    <D:TextBoxString runat="server" ID="tbQuestion" IsRequired="true"></D:TextBoxString>
                </td>
            </tr>
            <D:PlaceHolder ID="plhQuestions" runat="server" Visible="true">
            </D:PlaceHolder>
        </td>
    </tr>
</D:Panel>