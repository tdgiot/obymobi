﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dionysos.Web;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.ObymobiCms.UI;
using System.Text;
using Dionysos.Web.UI.DevExControls;
using Obymobi.Enums;
using Dionysos.Web.UI;
using Dionysos;
using DevExpress.Web;

namespace Obymobi.ObymobiCms.Company.Subpanels.SurveyQuestionTypePanels
{
    public partial class FormPanel : System.Web.UI.UserControl, Dionysos.Interfaces.ISaveableControl
    {
        private int Fields = 8;

        #region Event Handlers

        private void LoadUserControls()
        {
            // Fields
            for (int i = 1; i <= this.Fields; i++)
                this.cbFormFields.Items.Add(new ListEditItem(i.ToString()));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        #endregion

        #region Methods

        private void SetGui()
        {
            if (this.SurveyQuestionEntity.SurveyQuestionCollection.Count == 0)
            {
                // New form question means 0 fields at the beginning
                this.SurveyQuestionEntity.FieldValue1 = "0";
            }

            if (!this.IsPostBack)
                this.LoadUserControls();

            this.RenderFields();
        }

        private void RenderFields()
        {
            // Set the correct number of fields
            this.cbFormFields.Value = Int32.Parse(this.SurveyQuestionEntity.FieldValue1);

            StringBuilder builder = new StringBuilder();

            int i = 1;
            foreach (SurveyQuestionEntity question in this.SurveyQuestionEntity.SurveyQuestionCollection)
            {
                this.plhFields.AddHtml("<tr>");
                this.plhFields.AddHtml("<td class='label'>");
                this.plhFields.AddHtml("<label id='lblField{0}'>Field {0}</label>", i);
                this.plhFields.AddHtml("</td>");
                this.plhFields.AddHtml("<td class='control'>");

                Dionysos.Web.UI.WebControls.TextBoxString tb = new Dionysos.Web.UI.WebControls.TextBoxString();
                tb.ID = "question-" + question.SurveyQuestionId.ToString();
                tb.Text = question.Question;

                this.plhFields.Controls.Add(tb);

                this.plhFields.AddHtml("</td>");
                this.plhFields.AddHtml("<td class='label'>");
                this.plhFields.AddHtml("<label runat='server' id='lblType{0}'>Type</label>", i);
                this.plhFields.AddHtml("</td>");
                this.plhFields.AddHtml("<td class='control'>");

                ComboBoxInt cmb = new ComboBoxInt();
                cmb.ID = "field-type-" + question.SurveyQuestionId.ToString();
                this.plhFields.Controls.Add(cmb);

                cmb.DataBindEnum<FormFieldType>();

                int fieldType;
                if (Int32.TryParse(question.FieldValue1, out fieldType))
                    cmb.Value = fieldType;

                this.plhFields.AddHtml("</td>");
                this.plhFields.AddHtml("</tr>");

                if (fieldType != (int)FormFieldType.Date && fieldType != (int)FormFieldType.Checkbox)
                {
                    this.plhFields.AddHtml("<tr>");
                    this.plhFields.AddHtml("<td class='label'>");
                    this.plhFields.AddHtml("</td>");
                    this.plhFields.AddHtml("<td class='control'>");
                    this.plhFields.AddHtml("</td>");
                    this.plhFields.AddHtml("<td class='label'>");
                    this.plhFields.AddHtml("<label runat='server' id='lblRequired{0}'>Required</label>", i);
                    this.plhFields.AddHtml("</td>");
                    this.plhFields.AddHtml("<td class='control'>");

                    Dionysos.Web.UI.WebControls.CheckBox cb = new Dionysos.Web.UI.WebControls.CheckBox();
                    cb.ID = "required-" + question.SurveyQuestionId.ToString();
                    cb.Checked = question.Required;

                    this.plhFields.Controls.Add(cb);

                    this.plhFields.AddHtml("</td>");
                    this.plhFields.AddHtml("</tr>");
                }

                i++;
            }

            if (this.SurveyQuestionEntity.SurveyQuestionCollection.Count > 0)
                this.plhFieldsPanel.Visible = true;
            else
                this.plhFieldsPanel.Visible = false;
        }
        
        public bool Save()
        {
            // Save

            // Get the fields amount
            int fields = Int32.Parse(this.cbFormFields.SelectedValueString);

            // Check fields for changed data
            this.CheckFields(fields);
            
            // Set the amount of fields
            this.SurveyQuestionEntity.FieldValue1 = fields.ToString();

            // Save the question
            return this.SurveyQuestionEntity.Save();
        }

        private void CheckFields(int fields)
        { 
            if (fields == this.SurveyQuestionEntity.SurveyQuestionCollection.Count)
            {
                // No fields added or removed
                // Check for data modified

                foreach (Control control in this.plhFields.Controls)
                {
                    if (control is Dionysos.Web.UI.WebControls.TextBoxString)
                    {
                        // Question textboxes

                        var tb = control as Dionysos.Web.UI.WebControls.TextBoxString;
                        int surveyQuestionId = Convert.ToInt32(StringUtil.GetAllAfterFirstOccurenceOf(tb.ID, "question-"));

                        var surveyQuestion = this.SurveyQuestionEntity.SurveyQuestionCollection.FirstOrDefault(sq => sq.SurveyQuestionId == surveyQuestionId);

                        if (surveyQuestion != null && 
                            surveyQuestion.Question != tb.Text)
                        {
                            surveyQuestion.Question = tb.Text;
                            surveyQuestion.Save();
                        }
                    }
                    else if (control is Dionysos.Web.UI.WebControls.CheckBox)
                    {
                        // Required checkboxes

                        var cb = control as Dionysos.Web.UI.WebControls.CheckBox;
                        int surveyQuestionId = Convert.ToInt32(StringUtil.GetAllAfterFirstOccurenceOf(cb.ID, "required-"));

                        var surveyQuestion = this.SurveyQuestionEntity.SurveyQuestionCollection.FirstOrDefault(sq => sq.SurveyQuestionId == surveyQuestionId);

                        if (surveyQuestion != null && 
                            surveyQuestion.Required != cb.Checked)
                        {
                            surveyQuestion.Required = cb.Checked;
                            surveyQuestion.Save();
                        }
                    }
                    else if (control is ComboBoxInt)
                    { 
                        // Form field type

                        var cmb = control as ComboBoxInt;
                        int surveyQuestionId = Convert.ToInt32(StringUtil.GetAllAfterFirstOccurenceOf(cmb.ID, "field-type-"));

                        var surveyQuestion = this.SurveyQuestionEntity.SurveyQuestionCollection.FirstOrDefault(sq => sq.SurveyQuestionId == surveyQuestionId);

                        if (surveyQuestion != null && 
                            surveyQuestion.FieldValue1 != cmb.SelectedValueString)
                        {
                            surveyQuestion.FieldValue1 = cmb.SelectedValueString;
                            surveyQuestion.Save();   
                        }

                    }
                }
            }
            else if (fields > this.SurveyQuestionEntity.SurveyQuestionCollection.Count)
            {
                // We need more fields, dude

                int sortOrder = this.GetNewQuestionSortOrder();
                for (int i = this.SurveyQuestionEntity.SurveyQuestionCollection.Count; i < fields; i++)
                {
                    SurveyQuestionEntity question = new SurveyQuestionEntity();
                    question.ParentQuestionId = this.SurveyQuestionEntity.SurveyQuestionId;
                    question.SortOrder = sortOrder;
                    question.Question = "";
                    question.Type = (int)SurveyQuestionType.SingleTextbox;
                    question.Save();

                    this.SurveyQuestionEntity.SurveyQuestionCollection.Add(question);

                    sortOrder = sortOrder + 100;
                }
            }
            else
            { 
                // Delete some fields

                for (int i = this.SurveyQuestionEntity.SurveyQuestionCollection.Count; i > fields; i--)
                {
                    SurveyQuestionEntity childQuestion = this.SurveyQuestionEntity.SurveyQuestionCollection[i - 1];

                    childQuestion.Delete();
                    this.SurveyQuestionEntity.SurveyQuestionCollection.Remove(childQuestion);
                }
            }
        }

        private int GetNewQuestionSortOrder()
        {
            int sortOrder = 1000;

            foreach (SurveyQuestionEntity question in this.SurveyQuestionEntity.SurveyQuestionCollection)
            {
                if (question.SortOrder >= sortOrder)
                    sortOrder = sortOrder + 100;
            }

            return sortOrder;
        }

        #endregion
        
        #region Properties

        private SurveyQuestionEntity surveyQuestionEntity = null;
        public SurveyQuestionEntity SurveyQuestionEntity
        {
            get
            {
                return this.surveyQuestionEntity;
            }
            set
            {
                this.surveyQuestionEntity = value;
                this.SetGui();
            }
        }

        #endregion
    }
}