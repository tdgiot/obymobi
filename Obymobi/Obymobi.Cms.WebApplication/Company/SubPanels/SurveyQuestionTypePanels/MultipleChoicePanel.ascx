﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.Subpanels.SurveyQuestionTypePanels.MultipleChoicePanel" Codebehind="MultipleChoicePanel.ascx.cs" %>
<D:Panel ID="pnlMultipleChoice" runat="server" GroupingText="Multiple choice">
    <tr>
        <td class="label">
            <D:Label runat="server" id="lblMinSelected">Minimaal # geselecteerd</D:Label>
        </td>
        <td class="control">
            <D:TextBox runat="server" ID="tbFieldValue1" IsRequired="true"></D:TextBox>
        </td>
        <td class="label">
            <D:Label runat="server" id="lblMaxSelected">Maximaal # geselecteerd</D:Label>
        </td>
        <td class="control">
            <D:TextBox runat="server" ID="tbFieldValue2" IsRequired="true"></D:TextBox>
        </td>
    </tr>    
    <tr>
        <td class="label">
            <D:Label runat="server" id="lblDisplay">Display</D:Label>
        </td>
        <td class="control">
            <X:ComboBoxInt ID="cbDisplay" runat="server" DisplayEmptyItem="false"></X:ComboBoxInt>
        </td>
    </tr>
</D:Panel>