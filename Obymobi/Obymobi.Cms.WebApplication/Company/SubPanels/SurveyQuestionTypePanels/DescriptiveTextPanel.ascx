﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.Subpanels.SurveyQuestionTypePanels.DescriptiveTextPanel" Codebehind="DescriptiveTextPanel.ascx.cs" %>
<D:Panel ID="pnlDescriptiveTextPanel" runat="server" GroupingText="Begeleidende tekst">
    <tr>
        <td class="label">
            <D:Label runat="server" id="lblDescriptiveText">Begeleidende tekst</D:Label>
        </td>
        <td class="control" colspan="3">
            <D:TextBoxMultiLine runat="server" ID="tbFieldValue1" Rows="7"></D:TextBoxMultiLine>
        </td>
    </tr>    
    <tr>
        <td class="label">
            <D:Label runat="server" id="lblShowStartButton">"Start Survey" knop</D:Label>
        </td>
        <td class="control">
            <D:CheckBox ID="cbShowStartButton" runat="server" />
        </td>
    </tr>
</D:Panel>