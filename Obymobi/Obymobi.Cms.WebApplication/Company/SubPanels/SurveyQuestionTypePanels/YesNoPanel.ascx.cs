﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dionysos.Web;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.ObymobiCms.UI;
using System.Text;
using Dionysos.Web.UI.DevExControls;
using Obymobi.Enums;
using Dionysos.Web.UI;
using Dionysos;

namespace Obymobi.ObymobiCms.Company.Subpanels.SurveyQuestionTypePanels
{
    public partial class YesNoPanel : System.Web.UI.UserControl, Dionysos.Interfaces.ISaveableControl
    {
        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        #endregion

        #region Methods

        private void LoadUserControls()
        {
            // Only show target questions that are in the same survey as the question of which this answer belongs

            if (this.SurveyQuestionEntity.SurveyPageEntity != null)
            {
                PredicateExpression filter = new PredicateExpression();
                filter.Add(SurveyPageFields.SurveyId == this.SurveyQuestionEntity.SurveyPageEntity.SurveyId);
                filter.Add(SurveyQuestionFields.SurveyQuestionId != this.SurveyQuestionEntity.SurveyQuestionId);

                RelationCollection relations = new RelationCollection();
                relations.Add(SurveyPageEntity.Relations.SurveyQuestionEntityUsingSurveyPageId);

                PrefetchPath prefetch = new PrefetchPath(EntityType.SurveyPageEntity);
                prefetch.Add(SurveyPageEntity.PrefetchPathSurveyQuestionCollection);

                SortExpression sort = new SortExpression();
                sort.Add(new SortClause(SurveyQuestionFields.Question, SortOperator.Ascending));

                SurveyQuestionCollection questions = new SurveyQuestionCollection();
                questions.GetMulti(filter, 0, sort, relations, prefetch);

                this.ddlTargetSurveyQuestionId.DataSource = questions;
                this.ddlTargetSurveyQuestionId.DataBind();   
            }
        }

        private void SetGui()
        {
            if (!this.IsPostBack)
                this.LoadUserControls();

            if (this.SurveyQuestionEntity.SurveyAnswerCollection.Count > 1)
            {
                var noAnswer = this.SurveyQuestionEntity.SurveyAnswerCollection.FirstOrDefault(sa => sa.SortOrder == 1100);

                if (noAnswer != null && noAnswer.TargetSurveyQuestionId > 0)
                    this.ddlTargetSurveyQuestionId.SelectedItem = this.ddlTargetSurveyQuestionId.Items.FindByValue(noAnswer.TargetSurveyQuestionId);
            }
        }
        
        public bool Save()
        {
            // Save

            if (this.SurveyQuestionEntity.SurveyAnswerCollection.Count == 0)
            { 
                // Create the yes/no answers
                this.CreateAnswers();
            }

            // Set the target question
            this.SetTargetQuestionForNoAnswer();

            return this.SurveyQuestionEntity.Save();
        }

        private void SetTargetQuestionForNoAnswer()
        {
            if (this.ddlTargetSurveyQuestionId.ValidId > -1)
            {
                var noAnswer = this.SurveyQuestionEntity.SurveyAnswerCollection.FirstOrDefault(sa => sa.SortOrder == 1100);

                if (noAnswer != null)
                {
                    noAnswer.TargetSurveyQuestionId = this.ddlTargetSurveyQuestionId.Value;
                    noAnswer.Save();
                }
            }
        }

        private void CreateAnswers()
        {
            // Create a yes and a no answer

            SurveyAnswerEntity yesAnswer = new SurveyAnswerEntity();
            yesAnswer.SortOrder = 1000;
            yesAnswer.SurveyQuestionId = this.SurveyQuestionEntity.SurveyQuestionId;
            yesAnswer.Answer = "Yes";
            yesAnswer.Save();

            SurveyAnswerEntity noAnswer = new SurveyAnswerEntity();
            noAnswer.SortOrder = 1100;
            noAnswer.SurveyQuestionId = this.SurveyQuestionEntity.SurveyQuestionId;
            noAnswer.Answer = "No";
            noAnswer.Save();

            SurveyQuestionEntity.SurveyAnswerCollection.Add(yesAnswer);
            SurveyQuestionEntity.SurveyAnswerCollection.Add(noAnswer);
        }

        #endregion
        
        #region Properties

        private SurveyQuestionEntity surveyQuestionEntity = null;
        public SurveyQuestionEntity SurveyQuestionEntity
        {
            get
            {
                return this.surveyQuestionEntity;
            }
            set
            {
                this.surveyQuestionEntity = value;
                this.SetGui();
            }
        }

        #endregion
    }
}