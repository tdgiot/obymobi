﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.Subpanels.SurveyQuestionTypePanels.SingleTextboxPanel" Codebehind="SingleTextboxPanel.ascx.cs" %>
<D:Panel ID="pnlSingleTextboxPanel" runat="server" GroupingText="Single textbox">
    <tr>
        <td class="label">
            <D:Label runat="server" id="lblLines">Aantal lijnen</D:Label>
        </td>
        <td class="control">
            <D:TextBox runat="server" ID="tbFieldValue2"></D:TextBox>
        </td>
    </tr>    
</D:Panel>