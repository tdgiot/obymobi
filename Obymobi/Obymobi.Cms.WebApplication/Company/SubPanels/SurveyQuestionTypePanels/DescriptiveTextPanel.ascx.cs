﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dionysos.Web;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.ObymobiCms.UI;
using System.Text;
using Dionysos.Web.UI.DevExControls;
using Obymobi.Enums;
using Dionysos.Web.UI;
using Dionysos;

namespace Obymobi.ObymobiCms.Company.Subpanels.SurveyQuestionTypePanels
{
    public partial class DescriptiveTextPanel : System.Web.UI.UserControl, Dionysos.Interfaces.ISaveableControl
    {
        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        #endregion

        #region Methods

        private void SetGui()
        {
            if (this.SurveyQuestionEntity.FieldValue2 == null)
            {
                // Default is no start button

                this.SurveyQuestionEntity.FieldValue2 = "0";
            }

            int value = 0;
            Int32.TryParse(this.SurveyQuestionEntity.FieldValue2, out value);

            bool showButton = false;
            if (value > 0)
                showButton = true;

            this.cbShowStartButton.Checked = showButton;
        }
        
        public bool Save()
        {
            // Save

            if (this.cbShowStartButton.Checked)
                this.SurveyQuestionEntity.FieldValue2 = "1";
            else
                this.SurveyQuestionEntity.FieldValue2 = "0";

            return this.SurveyQuestionEntity.Save();
        }

        #endregion
        
        #region Properties

        private SurveyQuestionEntity surveyQuestionEntity = null;
        public SurveyQuestionEntity SurveyQuestionEntity
        {
            get
            {
                return this.surveyQuestionEntity;
            }
            set
            {
                this.surveyQuestionEntity = value;
                this.SetGui();
            }
        }

        #endregion
    }
}