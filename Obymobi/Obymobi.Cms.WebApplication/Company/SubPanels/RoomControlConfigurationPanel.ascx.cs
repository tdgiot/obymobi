﻿using Dionysos;
using DevExpress.Web.ASPxTreeList;
using Dionysos.Web.UI.WebControls;
using Obymobi.Data;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Cms;
using Obymobi.Logic.HelperClasses;
using System.Drawing;
using System.Diagnostics;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using System.Linq;
using DevExpress.Utils;
using DevExpress.Web;
using System.Data;
using System.Web.UI;
using Dionysos.Web.UI.DevExControls;
using Obymobi.Enums;
using DevExpress.Web;
using DevExpress.Web.Internal;

namespace Obymobi.ObymobiCms.Company.SubPanels
{
    public partial class RoomControlConfigurationPanel : SubPanelCustomDataSource
    {
        #region Properties

        private const string TRANSLATION_KEY_PREFIX = "Obymobi.ObymobiCms.Company.SubPanels.RoomControlConfigurationPanel.";

        private RoomControlConfigurationDataSource roomControlConfigurationDataSource;

        #endregion

        #region Methods

        private void RetrieveSelectedNodes(ref List<TreeListNode> selectedNodes, TreeListNode parent, bool includeChildren = true)
        {
            TreeListNodeCollection nodes;
            
            if (parent == null)
                nodes = this.tlConfiguration.Nodes;
            else
                nodes = parent.ChildNodes;                

            foreach (TreeListNode node in nodes)
            {
                if (includeChildren)
                {
                    if (node.Selected || node.ParentNode.Selected)
                    {
                        if (node.ParentNode.Selected)
                            node.Selected = true;

                        selectedNodes.Add(node);
                    }
                }
                else
                {
                    if (node.Selected && !node.ParentNode.Selected)
                        selectedNodes.Add(node);
                }

                if (node.HasChildren)                
                    this.RetrieveSelectedNodes(ref selectedNodes, node, includeChildren);
            }            
        }

        private void CreateTreeList()
        {
            // Create Tree View
            this.tlConfiguration.SettingsEditing.Mode = TreeListEditMode.Inline;
            this.tlConfiguration.SettingsBehavior.ColumnResizeMode = ColumnResizeMode.Disabled;
            this.tlConfiguration.SettingsBehavior.AllowSort = false;
            this.tlConfiguration.SettingsBehavior.AllowDragDrop = false;
            this.tlConfiguration.ClientInstanceName = TreelistId;
            this.tlConfiguration.AutoGenerateColumns = false;
            this.tlConfiguration.SettingsBehavior.AutoExpandAllNodes = true;
            this.tlConfiguration.SettingsEditing.AllowNodeDragDrop = true;
            this.tlConfiguration.SettingsEditing.AllowRecursiveDelete = true;
            this.tlConfiguration.Settings.GridLines = GridLines.Both;
            this.tlConfiguration.ClientSideEvents.NodeDblClick = this.nodeDblClickMethod;
            this.tlConfiguration.CustomErrorText += tlSiteStructure_CustomErrorText;

            this.tlConfiguration.KeyFieldName = "ItemId";
            this.tlConfiguration.ParentFieldName = "ParentItemId";

            // Name Column
            var nameColumn = new TreeListTextColumn();
            nameColumn.Caption = "Name Or Caption";            
            nameColumn.Width = Unit.Pixel(167);
            nameColumn.VisibleIndex = 0;
            nameColumn.FieldName = "Name";
            nameColumn.CellStyle.Wrap = DefaultBoolean.True;
            this.tlConfiguration.Columns.Add(nameColumn);

            // Name System Column
            var nameSystemColumn = new TreeListTextColumn();
            nameSystemColumn.Caption = "System Name";
            nameSystemColumn.Width = Unit.Pixel(167);
            nameSystemColumn.VisibleIndex = 1;
            nameSystemColumn.FieldName = "NameSystem";
            nameSystemColumn.CellStyle.Wrap = DefaultBoolean.True;
            nameSystemColumn.PropertiesTextEdit.ReadOnlyStyle.Border.BorderWidth = 0;
            nameSystemColumn.PropertiesTextEdit.ReadOnlyStyle.ForeColor = Color.Black;
            nameSystemColumn.ReadOnly = true;
            this.tlConfiguration.Columns.Add(nameSystemColumn);

            // Item type Column
            var itemTypeColumn = new TreeListTextColumn();
            itemTypeColumn.Caption = "Item Type";
            itemTypeColumn.Width = Unit.Pixel(100);
            itemTypeColumn.VisibleIndex = 2;
            itemTypeColumn.FieldName = "ItemType";
            itemTypeColumn.CellStyle.Wrap = DefaultBoolean.True;
            itemTypeColumn.PropertiesTextEdit.ReadOnlyStyle.Border.BorderWidth = 0;
            itemTypeColumn.PropertiesTextEdit.ReadOnlyStyle.ForeColor = Color.Black;
            itemTypeColumn.ReadOnly = true;
            this.tlConfiguration.Columns.Add(itemTypeColumn);

            // Type Column
            var typeColumn = new TreeListComboBoxColumn();
            typeColumn.Caption = "Type";
            typeColumn.FieldName = "Type";
            typeColumn.Width = Unit.Pixel(100);
            typeColumn.VisibleIndex = 3;
            typeColumn.CellStyle.Wrap = DefaultBoolean.True;

            this.tlConfiguration.Columns.Add(typeColumn);

            // In-line Edit Column
            var editCommandColumn = new TreeListCommandColumn();
            editCommandColumn.VisibleIndex = 4;
            editCommandColumn.Width = Unit.Pixel(60);
            editCommandColumn.ShowNewButtonInHeader = true;
            editCommandColumn.EditButton.Visible = true;
            editCommandColumn.NewButton.Visible = true;
            editCommandColumn.DeleteButton.Visible = true;
            editCommandColumn.ButtonType = ButtonType.Image;
            editCommandColumn.EditButton.Image.Url = this.ResolveUrl("~/images/icons/pencil.png");
            editCommandColumn.DeleteButton.Image.Url = this.ResolveUrl("~/images/icons/delete.png");
            editCommandColumn.CancelButton.Image.Url = this.ResolveUrl("~/images/icons/cross_grey.png");
            editCommandColumn.NewButton.Image.Url = this.ResolveUrl("~/images/icons/add2.png");
            editCommandColumn.UpdateButton.Image.Url = this.ResolveUrl("~/images/icons/disk.png");

            this.tlConfiguration.Columns.Add(editCommandColumn);

            // Edit Page Colum
            var editPageColumn = new TreeListHyperLinkColumn();
            editPageColumn.Name = "EditPage";
            editPageColumn.VisibleIndex = 5;
            editPageColumn.Caption = "&nbsp;";
            editPageColumn.Width = Unit.Pixel(15);
            editPageColumn.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            editPageColumn.PropertiesHyperLink.ImageUrl = "~/images/icons/page_edit.png";
            editPageColumn.EditCellStyle.CssClass = "hidden";
            editPageColumn.FieldName = "ItemId";

            this.tlConfiguration.Columns.Add(editPageColumn);

            // Events
            this.tlConfiguration.SettingsEditing.Mode = TreeListEditMode.Inline;
            this.tlConfiguration.NodeValidating += tlSiteStructure_NodeValidating;
            this.tlConfiguration.CellEditorInitialize += tlConfiguration_CellEditorInitialize;
            this.tlConfiguration.HtmlDataCellPrepared += tlConfiguration_HtmlDataCellPrepared;
            this.tlConfiguration.CommandColumnButtonInitialize += tlConfiguration_CommandColumnButtonInitialize;
            this.tlConfiguration.HtmlRowPrepared += tlConfiguration_HtmlRowPrepared;

            this.InitDataSource();

            // Width based on a 1280 width screen (fair requirement for CMS use?)
            // http://www.w3schools.com/browsers/browsers_display.asp | http://gs.statcounter.com/#desktop-resolution-ww-monthly-201212-201312            
            this.tlConfiguration.Width = Unit.Pixel(736);
        }

        void tlConfiguration_HtmlRowPrepared(object sender, TreeListHtmlRowEventArgs e)
        {
            if (e.NodeKey != null)
            {
                TreeListNode currentNode = this.tlConfiguration.FindNodeByKeyValue(e.NodeKey);
                if (currentNode != null && !isValidNode(currentNode))
                {
                    e.Row.Style.Add("Color", "Red");
                }
            }
        }

        bool isValidNode(TreeListNode node)
        {
            bool valid = true;
            if (node.DataItem is RoomControlAreaEntity)
            {
                RoomControlAreaEntity roomControlAreaEntity = node.DataItem as RoomControlAreaEntity;
                if (string.IsNullOrWhiteSpace(roomControlAreaEntity.NameSystem))
                {
                    valid = false;
                }
            }
            else if (node.DataItem is RoomControlSectionEntity)
            {
                RoomControlSectionEntity roomControlSectionEntity = node.DataItem as RoomControlSectionEntity;
                if (roomControlSectionEntity.Type == RoomControlSectionType.Service)
                {
                    if (string.IsNullOrWhiteSpace(roomControlSectionEntity.NameSystem))
                    {
                        valid = false;
                    }
                }
            }
            else if (node.DataItem is RoomControlComponentEntity)
            {
                RoomControlComponentEntity roomControlComponentEntity = node.DataItem as RoomControlComponentEntity;

                if (roomControlComponentEntity.Type == RoomControlComponentType.Blind)
                {
                    if (string.IsNullOrWhiteSpace(roomControlComponentEntity.FieldValue1))
                    {
                        valid = false;
                    }
                    else if (string.IsNullOrWhiteSpace(roomControlComponentEntity.FieldValue2))
                    {
                        valid = false;
                    }
                    else if (string.IsNullOrWhiteSpace(roomControlComponentEntity.FieldValue3))
                    {
                        valid = false;
                    }
                }
                else
                {
                    if (string.IsNullOrWhiteSpace(roomControlComponentEntity.NameSystem))
                    {
                        valid = false;
                    }
                }
            }
            else if (node.DataItem is RoomControlSectionItemEntity)
            {
                RoomControlSectionItemEntity roomControlSectionItemEntity = node.DataItem as RoomControlSectionItemEntity;

                if (roomControlSectionItemEntity.Type == RoomControlSectionItemType.Scene || roomControlSectionItemEntity.Type == RoomControlSectionItemType.SleepTimer)
                {
                    if (string.IsNullOrWhiteSpace(roomControlSectionItemEntity.FieldValue1))
                    {
                        valid = false;
                    }
                }
                if (roomControlSectionItemEntity.Type == RoomControlSectionItemType.StationListing)
                {
                    if (!roomControlSectionItemEntity.StationListId.HasValue)
                    {
                        valid = false;
                    }
                }
                else if (roomControlSectionItemEntity.Type == RoomControlSectionItemType.RemoteControl)
                {
                    foreach (RoomControlWidgetEntity widgetEntity in roomControlSectionItemEntity.RoomControlWidgetCollection)
                    {
                        int numRequiredFieldValues = 0;
                        if (widgetEntity.Type == RoomControlWidgetType.PowerButton1x1) numRequiredFieldValues = 1;
                        else if (widgetEntity.Type == RoomControlWidgetType.Volume1x4) numRequiredFieldValues = 3;
                        else if (widgetEntity.Type == RoomControlWidgetType.Channel1x4) numRequiredFieldValues = 2;
                        else if (widgetEntity.Type == RoomControlWidgetType.Navigation2x3) numRequiredFieldValues = 5;
                        else if (widgetEntity.Type == RoomControlWidgetType.Numpad2x3) numRequiredFieldValues = 10;

                        if (numRequiredFieldValues > 0)
                        {
                            for (int i = 1; i <= numRequiredFieldValues; i++)
                            {
                                if (string.IsNullOrWhiteSpace((string)widgetEntity.Fields[string.Format("FieldValue{0}", i)].CurrentValue))
                                {
                                    valid = false;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            else if (node.DataItem is RoomControlWidgetEntity)
            {
                RoomControlWidgetEntity roomControlWidgetEntity = node.DataItem as RoomControlWidgetEntity;

                if (roomControlWidgetEntity.Type == RoomControlWidgetType.Light2x1 ||
                    roomControlWidgetEntity.Type == RoomControlWidgetType.Blind1x2 ||
                    roomControlWidgetEntity.Type == RoomControlWidgetType.Scene1x1 ||
                    roomControlWidgetEntity.Type == RoomControlWidgetType.Thermostat4x2 ||
                    roomControlWidgetEntity.Type == RoomControlWidgetType.Light1x1)
                {
                    if (!roomControlWidgetEntity.RoomControlComponentId1.HasValue)
                    {
                        valid = false;
                    }
                }
                else if (roomControlWidgetEntity.Type == RoomControlWidgetType.Volume1x2)
                {
                    if (string.IsNullOrWhiteSpace(roomControlWidgetEntity.FieldValue1))
                    {
                        valid = false;
                    }
                    else if (string.IsNullOrWhiteSpace(roomControlWidgetEntity.FieldValue2))
                    {
                        valid = false;
                    }
                    else if (string.IsNullOrWhiteSpace(roomControlWidgetEntity.FieldValue3))
                    {
                        valid = false;
                    }
                }
                else if (roomControlWidgetEntity.Type == RoomControlWidgetType.Volume1x4)
                {
                    if (string.IsNullOrWhiteSpace(roomControlWidgetEntity.FieldValue1))
                    {
                        valid = false;
                    }
                    else if (string.IsNullOrWhiteSpace(roomControlWidgetEntity.FieldValue2))
                    {
                        valid = false;
                    }
                }
                else if (roomControlWidgetEntity.Type == RoomControlWidgetType.Channel1x2 ||
                         roomControlWidgetEntity.Type == RoomControlWidgetType.Channel1x4)
                {
                    if (string.IsNullOrWhiteSpace(roomControlWidgetEntity.FieldValue1))
                    {
                        valid = false;
                    }
                    else if (string.IsNullOrWhiteSpace(roomControlWidgetEntity.FieldValue2))
                    {
                        valid = false;
                    }
                }
                else if (roomControlWidgetEntity.Type == RoomControlWidgetType.Navigation1x2 ||
                         roomControlWidgetEntity.Type == RoomControlWidgetType.Navigation2x3)
                {
                    if (string.IsNullOrWhiteSpace(roomControlWidgetEntity.FieldValue1))
                    {
                        valid = false;
                    }
                    else if (string.IsNullOrWhiteSpace(roomControlWidgetEntity.FieldValue2))
                    {
                        valid = false;
                    }
                    else if (string.IsNullOrWhiteSpace(roomControlWidgetEntity.FieldValue3))
                    {
                        valid = false;
                    }
                    else if (string.IsNullOrWhiteSpace(roomControlWidgetEntity.FieldValue4))
                    {
                        valid = false;
                    }
                    else if (string.IsNullOrWhiteSpace(roomControlWidgetEntity.FieldValue5))
                    {
                        valid = false;
                    }
                }
                else if (roomControlWidgetEntity.Type == RoomControlWidgetType.PowerButton1x1)
                {
                    if (string.IsNullOrWhiteSpace(roomControlWidgetEntity.FieldValue1))
                    {
                        valid = false;
                    }
                }
                else if (roomControlWidgetEntity.Type == RoomControlWidgetType.Numpad2x3)
                {
                    for (int i = 1; i <= 10; i++)
                    {
                        if (string.IsNullOrWhiteSpace((string)roomControlWidgetEntity.Fields[string.Format("FieldValue{0}", i)].CurrentValue))
                        {
                            valid = false;
                            break;
                        }
                    }
                }
            }
            return valid;
        }

        void tlConfiguration_CommandColumnButtonInitialize(object sender, TreeListCommandColumnButtonEventArgs e)
        {
            if (e.NodeKey != null && e.ButtonType == TreeListCommandColumnButtonType.New)
            {
                string[] keyParts = e.NodeKey.Split(new string[] { "-" }, StringSplitOptions.RemoveEmptyEntries);
                if (keyParts.Length > 0)
                {
                    string itemType = keyParts[0];
                    if (itemType == RoomControlItemType.Component || itemType == RoomControlItemType.Widget)
                    {
                        e.Visible = DefaultBoolean.False;
                    }
                    else if (itemType == RoomControlItemType.Section)
                    {
                        TreeListNode currentNode = this.tlConfiguration.FindNodeByKeyValue(e.NodeKey);
                        if (currentNode != null && currentNode.DataItem != null)
                        {
                            int sectionType = (int)DataBinder.Eval(currentNode.DataItem, "Type");
                            if (sectionType == (int)RoomControlSectionType.ScreenSleep || sectionType == (int)RoomControlSectionType.Service)
                            {
                                e.Visible = DefaultBoolean.False;
                            }
                        }
                    }
                    else if (itemType == RoomControlItemType.SectionItem)
                    {
                        e.Visible = DefaultBoolean.False;
                        /*TreeListNode currentNode = this.tlConfiguration.FindNodeByKeyValue(e.NodeKey);
                        if (currentNode != null && currentNode.DataItem != null)
                        {
                            int sectionItemType = (int)DataBinder.Eval(currentNode.DataItem, "Type");
                            if (sectionItemType != (int)RoomControlSectionItemType.RemoteControl)
                            {
                                e.Visible = DefaultBoolean.False;
                            }
                        }*/
                    }
                }
            }
        }

        void tlConfiguration_HtmlDataCellPrepared(object sender, TreeListHtmlDataCellEventArgs e)
        {
            if (e.Column.Name == "EditPage" && e.NodeKey != null)
            {
                TreeListHyperLinkColumn editPageColumn = e.Column as TreeListHyperLinkColumn;

                string navigateUrl = string.Empty;
                string[] keyParts = e.NodeKey.Split(new string[] { "-" }, StringSplitOptions.RemoveEmptyEntries);
                if (keyParts.Length > 1)
                {
                    string itemType = keyParts[0];
                    string itemId = keyParts[1];

                    if (itemType == RoomControlItemType.Area)
                    {
                        navigateUrl = string.Format("~/Company/RoomControlArea.aspx?id={0}", itemId);
                    }
                    else if (itemType == RoomControlItemType.Section)
                    {
                        navigateUrl = string.Format("~/Company/RoomControlSection.aspx?id={0}", itemId);
                    }
                    else if (itemType == RoomControlItemType.Component)
                    {
                        navigateUrl = string.Format("~/Company/RoomControlComponent.aspx?id={0}", itemId);
                    }
                    else if (itemType == RoomControlItemType.SectionItem)
                    {
                        navigateUrl = string.Format("~/Company/RoomControlSectionItem.aspx?id={0}", itemId);
                    }
                    else if (itemType == RoomControlItemType.Widget)
                    {
                        navigateUrl = string.Format("~/Company/RoomControlWidget.aspx?id={0}", itemId);
                    }
                }
                foreach (Control item in e.Cell.Controls)
                {
                    HyperLinkDisplayControl link = item as HyperLinkDisplayControl;
                    if (link != null)
                    {
                        link.NavigateUrl = navigateUrl;
                        break;
                    }
                }
            }
            else if (e.Column.FieldName == "Type")
            {
                if (e.CellValue is Enum)
                {
                    e.Cell.Text = EnumUtil.GetStringValue((Enum)e.CellValue);
                }
            }
        }

        void tlConfiguration_CellEditorInitialize(object sender, TreeListColumnEditorEventArgs e)
        {
            if (e.Column.FieldName == "Type" || e.Column.FieldName == "ItemType" || e.Column.FieldName == "NameSystem")
            {
                string parentItemId = string.Empty;

                TreeListNode currentNode = null;
                TreeListNode parentNode = null;
                if (e.NodeKey != null)
                {
                    currentNode = this.tlConfiguration.FindNodeByKeyValue(e.NodeKey);
                    if (currentNode != null)
                    {
                        parentNode = currentNode.ParentNode;
                        if (parentNode != null)
                        {
                            parentItemId = parentNode.Key;
                        }
                    }
                }
                else
                {
                    parentItemId = this.tlConfiguration.NewNodeParentKey;
                    parentNode = this.tlConfiguration.FindNodeByKeyValue(parentItemId);
                }

                if (e.Column.FieldName == "Type")
                {
                    ASPxComboBox cbType = e.Editor as ASPxComboBox;

                    if (parentItemId.Length == 0)
                    {
                        this.DataBindEnumToComboBox(cbType, typeof(RoomControlAreaType));
                    }
                    else
                    {
                        string[] keyParts = parentItemId.Split(new string[] { "-" }, StringSplitOptions.RemoveEmptyEntries);
                        string parentItemType = keyParts[0];
                        if (parentItemType == RoomControlItemType.Area)
                        {
                            if (currentNode != null && currentNode.ChildNodes.Count > 0)
                            {
                                this.DataBindEnumValuesToCombBox(cbType, e.Value);
                                cbType.ReadOnly = true;
                            }
                            else
                            {
                                this.DataBindEnumToComboBox(cbType, typeof(RoomControlSectionType));
                            }
                        }
                        else if (parentItemType == RoomControlItemType.Section)
                        {
                            if (parentNode != null && parentNode.DataItem != null)
                            {
                                int sectionType = (int)DataBinder.Eval(parentNode.DataItem, "Type");
                                if (sectionType == (int)RoomControlSectionType.Custom)
                                {
                                    this.DataBindEnumValuesToCombBox(cbType, RoomControlSectionItemType.StationListing, RoomControlSectionItemType.RemoteControl, RoomControlSectionItemType.Scene);
                                }
                                else if (sectionType == (int)RoomControlSectionType.Dashboard)
                                {
                                    this.DataBindEnumToComboBox(cbType, typeof(RoomControlWidgetType));
                                }
                                else if (sectionType == (int)RoomControlSectionType.Timers)
                                {
                                    this.DataBindEnumValuesToCombBox(cbType, RoomControlSectionItemType.WakeUpTimer, RoomControlSectionItemType.SleepTimer);
                                }
                                if (sectionType == (int)RoomControlSectionType.Lighting)
                                {
                                    this.DataBindEnumValuesToCombBox(cbType, RoomControlComponentType.Light, RoomControlComponentType.LightingScene);
                                }
                                else if (sectionType == (int)RoomControlSectionType.Blinds)
                                {
                                    this.DataBindEnumValuesToCombBox(cbType, RoomControlComponentType.Blind);
                                }
                                else if (sectionType == (int)RoomControlSectionType.Comfort)
                                {
                                    this.DataBindEnumValuesToCombBox(cbType, RoomControlComponentType.Thermostat);
                                }
                            }
                        }
                    }
                    if (currentNode != null && currentNode.DataItem != null)
                    {
                        string selectedType = ((int)DataBinder.Eval(currentNode.DataItem, "Type")).ToString();
                        cbType.SelectedItem = cbType.Items.FindByValue(selectedType);
                    }
                }
                else if (e.Column.FieldName == "ItemType")
                {
                    ASPxTextBox txtItemType = e.Editor as ASPxTextBox;
                    txtItemType.ClientEnabled = false;

                    if (parentItemId.Length == 0)
                    {
                        e.Editor.Value = RoomControlItemType.Area;
                    }
                    else if (parentItemId.Length > 0)
                    {
                        string[] keyParts = parentItemId.Split(new string[] { "-" }, StringSplitOptions.RemoveEmptyEntries);
                        string parentItemType = keyParts[0];
                        if (parentItemType == RoomControlItemType.Area)
                        {
                            e.Editor.Value = RoomControlItemType.Section;
                        }
                        else if (parentItemType == RoomControlItemType.Section)
                        {
                            if (parentNode != null && parentNode.DataItem != null)
                            {
                                int sectionType = (int)DataBinder.Eval(parentNode.DataItem, "Type");

                                if (sectionType == (int)RoomControlSectionType.Custom || sectionType == (int)RoomControlSectionType.Timers)
                                {
                                    txtItemType.Value = RoomControlItemType.SectionItem;
                                }
                                else if (sectionType == (int)RoomControlSectionType.Dashboard)
                                {
                                    txtItemType.Value = RoomControlItemType.Widget;
                                }
                                else if (sectionType != (int)RoomControlSectionType.Service)
                                {
                                    txtItemType.Value = RoomControlItemType.Component;
                                }
                            }
                        }
                        else if (parentItemType == RoomControlItemType.SectionItem)
                        {
                            if (parentNode != null && parentNode.DataItem != null)
                            {
                                int sectionItemType = (int)DataBinder.Eval(parentNode.DataItem, "Type");
                                if (sectionItemType == (int)RoomControlSectionItemType.RemoteControl)
                                {
                                    txtItemType.Value = RoomControlItemType.Widget;
                                }
                            }
                        }
                    }
                }
                if (e.Column.FieldName == "NameSystem")
                {
                    ASPxTextBox txtNameSystem = e.Editor as ASPxTextBox;
                    txtNameSystem.ClientEnabled = false;
                    if (parentItemId.Length == 0)
                    {
                        txtNameSystem.ClientEnabled = true;
                        txtNameSystem.ReadOnly = false;
                    }
                    else if (currentNode != null && currentNode.DataItem != null)
                    {
                        string itemType = DataBinder.Eval(currentNode.DataItem, "ItemType").ToString();
                        if (itemType == RoomControlItemType.Area || itemType == RoomControlItemType.Component)
                        {
                            txtNameSystem.ClientEnabled = true;
                            txtNameSystem.ReadOnly = false;
                        }
                        else if (itemType == RoomControlItemType.Section)
                        {
                            int sectionType = (int)DataBinder.Eval(currentNode.DataItem, "Type");
                            if (sectionType == (int)RoomControlSectionType.Service)
                            {
                                txtNameSystem.ClientEnabled = true;
                                txtNameSystem.ReadOnly = false;
                            }
                        }
                    }
                    else if (parentNode != null && parentNode.DataItem != null)
                    {
                        string parentItemType = DataBinder.Eval(parentNode.DataItem, "ItemType").ToString();
                        if (parentItemType == RoomControlItemType.Area)
                        {

                        }
                        else if (parentItemType == RoomControlItemType.Section)
                        {
                            int sectionType = (int)DataBinder.Eval(parentNode.DataItem, "Type");
                            if (sectionType == (int)RoomControlSectionType.Lighting || sectionType == (int)RoomControlSectionType.Blinds || sectionType == (int)RoomControlSectionType.Comfort)
                            {
                                txtNameSystem.ClientEnabled = true;
                                txtNameSystem.ReadOnly = false;
                            }
                        }
                    }
                }
            }
        }

        private void DataBindEnumValuesToCombBox(ASPxComboBox combobox, params object[] values)
        {
            combobox.Items.Clear();
            foreach (object value in values)
            {
                string enumText = EnumUtil.GetStringValue((Enum)value);
                int enumValue = Convert.ToInt32(value);

                combobox.Items.Add(enumText, enumValue);
            }
            combobox.DataBind();
        }

        private void DataBindEnumToComboBox(ASPxComboBox combobox, Type enumType)
        {
            combobox.Items.Clear();

            string[] names = Enum.GetNames(enumType);
            Array values = Enum.GetValues(enumType);

            for (int i = 0; i < names.Length; i++)
            {
                string text = EnumUtil.GetStringValue((Enum)values.GetValue(i));
                int value = Convert.ToInt32(values.GetValue(i));
                combobox.Items.Add(text, value);
            }
            combobox.DataBind();
        }

        private void InitDataSource()
        {
            if (this.DataSource != null)
            {
                this.roomControlConfigurationDataSource = new RoomControlConfigurationDataSource(this.DataSourceAsRoomControlConfigurationEntity.RoomControlConfigurationId);
                this.tlConfiguration.DataSource = this.roomControlConfigurationDataSource;
            }
        }

        public void Refresh(bool expandAll = true)
        {
            this.InitDataSource();
            this.tlConfiguration.DataBind();
            if (expandAll)
                this.tlConfiguration.ExpandAll();
        }        

        #endregion

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            this.CreateTreeList();

            // Init the toolbar
            this.hlExpandAll.NavigateUrl = string.Format("javascript:{0}.ExpandAll()", this.TreelistId);
            this.hlCollapseAll.NavigateUrl = string.Format("javascript:{0}.CollapseAll()", this.TreelistId);                                   
        }

        public void RefreshDataSource(IEntity datasource)
        {
            this.DataSource = datasource;
            if (this.DataSource != null && !((IEntity)this.DataSource).IsNew)
            {
                this.Refresh();
            }            
        }

        private void tlSiteStructure_CustomErrorText(object sender, TreeListCustomErrorTextEventArgs e)
        {
            if (e.Exception.InnerException != null)
            {
                var exception = e.Exception.InnerException as ObymobiException;
                if (exception != null)
                {
                    e.ErrorText = exception.AdditionalMessage;
                }
                else
                {
                    e.ErrorText = e.Exception.InnerException.Message;
                }
            }
        }

        private void tlSiteStructure_NodeValidating(object sender, TreeListNodeValidationEventArgs e)
        {
            if (e.NewValues["Type"] == null)
            {
                e.Errors["Type"] = this.PageAsPageDefault.Translate(TRANSLATION_KEY_PREFIX + "Error.ItemTypeNotSelected", "No 'Type' has been selected for the item.", true);
            }
            
            if (e.HasErrors)
                e.NodeError = this.PageAsPageDefault.Translate(TRANSLATION_KEY_PREFIX + "PleaseCorrectErrors", "Please correct the errors.", true);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.tlConfiguration.DataBind();

            var message = this.PageAsPageDefault.Translate(TRANSLATION_KEY_PREFIX + "Warning.PreSubmit", "Are you sure you want to delete these items? Child items will be automatically removed if the parent item is selected.", true);
                
            this.btDeleteSelected.PreSubmitWarning = message;
            this.btDeleteSelectedTop.PreSubmitWarning = message;

            this.btDeleteSelected.Click += btDeleteSelected_Click;
            this.btDeleteSelectedTop.Click += btDeleteSelected_Click;
        }

        void btDeleteSelected_Click(object sender, EventArgs e)
        {
            this.DeleteSelected();
        }

        private void DeleteSelected()
        {
            List<TreeListNode> selectedNodes = new List<TreeListNode>();
            this.RetrieveSelectedNodes(ref selectedNodes, null);

            if (selectedNodes.Count <= 0)
            {
                this.PageAsPageDefault.MultiValidatorDefault.AddError(this.PageAsPageDefault.Translate(TRANSLATION_KEY_PREFIX + "Error.NoItemSelected", "No item has been selected to be deleted.", true));
                this.PageAsPageDefault.Validate();
            }
            else
            {
                this.DeleteItems(selectedNodes);

                this.tlConfiguration.DataBind();

                if (selectedNodes.Count == 1)
                    this.PageAsPageDefault.AddInformatorInfo(this.PageAsPageDefault.Translate(TRANSLATION_KEY_PREFIX + "Success.ItemsDeleted", "The selected item has been deleted.", true));
                else
                    this.PageAsPageDefault.AddInformatorInfo(this.PageAsPageDefault.Translate(TRANSLATION_KEY_PREFIX + "Success.ItemsDeleted", "The selected items have been deleted.", true));

                this.PageAsPageDefault.Validate();
            }
        }

        private void DeleteItems(List<TreeListNode> selectedNodes)
        {
            foreach (TreeListNode node in selectedNodes)
            {
                this.roomControlConfigurationDataSource.DeleteParameters.Clear();
                this.roomControlConfigurationDataSource.DeleteParameters.Add("ItemId", node.Key);
                this.roomControlConfigurationDataSource.Delete();
            }
           
            this.DataSource = new RoomControlConfigurationEntity(this.DataSourceAsRoomControlConfigurationEntity.RoomControlConfigurationId);
            this.InitDataSource();
        }

        private string TreelistId { get { return string.Format("{0}_treelist", this.ID); } }

        private RoomControlConfigurationEntity DataSourceAsRoomControlConfigurationEntity
        {
            get
            {
                return (RoomControlConfigurationEntity)this.DataSource;
            }
        }

        #endregion      

        #region Javascript Methods

        private string nodeDblClickMethod
        {
            get
            {
                return @" function(s, e) {
	                            " + this.TreelistId + @".StartEdit(e.nodeKey);
		                        e.cancel = true;	                                                                    
                            }";
            }
        }

        #endregion
    }
}