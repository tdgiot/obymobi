﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using Dionysos.Web;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.ObymobiCms.Company.SubPanels.DeviceShellPanels
{
    public partial class WindowsCommandPanel : Dionysos.Web.UI.WebControls.SubPanelCustomDataSource
    {
        #region Methods

        private void LoadUserControls()
        {

        }

        protected void SetGui()
        {
            
        }

        #endregion

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.SetGui();
        }

        public void fuImportFile_FileUploadComplete(object sender, DevExpress.Web.FileUploadCompleteEventArgs e)
        {
            string deviceMac = QueryStringHelper.GetString("Device");
            if (deviceMac.Length > 0)
            {
                if (!Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/Temp/Devicemedia/")))
                {
                    Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/Temp/Devicemedia/"));
                }

                string fileName = DateTime.Now.Ticks + "_" + e.UploadedFile.FileName;
                if (fileName.EndsWith(".config"))
                {
                    fileName = fileName.Replace(".config", ".xml");
                }

                if (!fileName.EndsWith(".xml"))
                {
                    e.IsValid = false;
                    e.ErrorText = "File extension is not valid. Only XML configuration files can be uploaded.";
                }
                else
                {
                    try
                    {

                        // instance a filestream pointing to the storage folder, use the original file name to name the resulting file
                        var fs = new FileStream(System.Web.Hosting.HostingEnvironment.MapPath("~/Temp/Devicemedia/") + fileName, FileMode.CreateNew);
                        fs.Write(e.UploadedFile.FileBytes, 0, e.UploadedFile.FileBytes.Length);
                        fs.Close();
                        fs.Dispose();

                        var identifier = Regex.Replace(deviceMac, @"(.{2})", "$1:").Substring(0, 17);

                        // Dump in database
                        DeviceEntity deviceEntity = DeviceHelper.GetDeviceEntityByIdentifier(identifier, false);
                        if (!deviceEntity.IsNew)
                        {
                            var devicemediaEntity = new DevicemediaEntity();
                            devicemediaEntity.DeviceId = deviceEntity.DeviceId;
                            devicemediaEntity.Filename = fileName;
                            devicemediaEntity.WebserviceUrl = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host + HttpContext.Current.Request.ApplicationPath;
                            devicemediaEntity.Save();

                            e.IsValid = true;
                            e.CallbackData = devicemediaEntity.WebserviceUrl + "/Temp/Devicemedia/" + fileName.Replace(" ", "%20");
                        }
                        else
                        {
                            e.IsValid = false;
                            e.ErrorText = "Device not found with identifier: " + deviceEntity;
                        }
                    }
                    catch (Exception ex)
                    {
                        e.IsValid = false;
                        e.ErrorText = "Error: " + ex.Message;
                    }       
                }                           
            }
            else
            {
                e.IsValid = false;
                e.ErrorText = "No valid device identifier: " + deviceMac;
            }
        }

        #endregion
    }
}