using Dionysos.Web;
using Obymobi.Constants;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.WebControls;

namespace Obymobi.ObymobiCms.Company.SubPanels.DeviceShellPanels
{
    public partial class AndroidCommandPanel : Dionysos.Web.UI.WebControls.SubPanelCustomDataSource
    {
        #region Methods

        private void LoadUserControls()
        {

        }

        protected void SetGui()
        {
            if (CmsSessionHelper.CurrentRole == Role.Supervisor)
            {
                this.plhAllCommands.Visible = false;
                this.plhSupervisorCommands.Visible = true;
            }
            else
            {
                this.plhAllCommands.Visible = true;
                this.plhSupervisorCommands.Visible = false;
            }

            PredicateExpression filter = new PredicateExpression();
            filter.Add(ApplicationFields.Code == ApplicationCode.Agent);
            filter.AddWithOr(ApplicationFields.Code == ApplicationCode.Emenu);
            filter.AddWithOr(ApplicationFields.Code == ApplicationCode.SupportTools);
            filter.AddWithOr(ApplicationFields.Code == ApplicationCode.Console);
            filter.AddWithOr(ApplicationFields.Code == ApplicationCode.MessagingService);

            SortExpression sort = new SortExpression();
            sort.Add(new SortClause(ReleaseFields.ApplicationId, SortOperator.Ascending));
            sort.Add(new SortClause(ReleaseFields.Version, SortOperator.Descending));

            RelationCollection relations = new RelationCollection(ReleaseEntityBase.Relations.ApplicationEntityUsingApplicationId);
            ExcludeFieldsList excludedFields = new ExcludeFieldsList(ReleaseFields.File);

            PrefetchPath prefetch = new PrefetchPath(EntityType.ReleaseEntity);
            prefetch.Add(ReleaseEntityBase.PrefetchPathApplicationEntity);

            ReleaseCollection releases = new ReleaseCollection();
            releases.GetMulti(filter, 0, sort, relations, prefetch, excludedFields, 0, 0);

            string webserviceUrl = WebEnvironmentHelper.GetAmazonCdnBaseUrl(WebEnvironmentHelper.CloudEnvironment);
            Dictionary<string, int> releaseCount = new Dictionary<string, int>();

            foreach (ReleaseEntity release in releases)
            {
                releaseCount[release.ApplicationName] = releaseCount.ContainsKey(release.ApplicationName)
                    ? releaseCount[release.ApplicationName] + 1
                    : 1;

                if (releaseCount[release.ApplicationName] > 5)
                {
                    continue;
                }

                string name = $"{release.ApplicationEntity.Name} ({release.Version})";
                this.cmbReleases.Items.Add(new ListItem(name, webserviceUrl + release.Filename));
            }
        }

        #endregion

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.SetGui();
        }

        public void fuImportFile_FileUploadComplete(object sender, DevExpress.Web.FileUploadCompleteEventArgs e)
        {
            string deviceMac = QueryStringHelper.GetString("Device");
            if (deviceMac.Length > 0)
            {
                if (!Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/Temp/Devicemedia/")))
                {
                    Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/Temp/Devicemedia/"));
                }

                if (e.UploadedFile.FileName.EndsWith(".xml"))
                {
                    string fileName = DateTime.Now.Ticks + "_" + e.UploadedFile.FileName;

                    try
                    {
                        // instance a filestream pointing to the storage folder, use the original file name to name the resulting file
                        var fs = new FileStream(System.Web.Hosting.HostingEnvironment.MapPath("~/Temp/Devicemedia/") + fileName, FileMode.CreateNew);
                        fs.Write(e.UploadedFile.FileBytes, 0, e.UploadedFile.FileBytes.Length);
                        fs.Close();
                        fs.Dispose();

                        var identifier = Regex.Replace(deviceMac, @"(.{2})", "$1:").Substring(0, 17);

                        // Dump in database
                        DeviceEntity deviceEntity = DeviceHelper.GetDeviceEntityByIdentifier(identifier, false);
                        if (!deviceEntity.IsNew)
                        {

                            var devicemediaEntity = new DevicemediaEntity();
                            devicemediaEntity.DeviceId = deviceEntity.DeviceId;
                            devicemediaEntity.Filename = fileName;
                            devicemediaEntity.WebserviceUrl = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host + HttpContext.Current.Request.ApplicationPath;
                            devicemediaEntity.Save();

                            e.IsValid = true;
                            e.CallbackData = devicemediaEntity.WebserviceUrl + "/Temp/Devicemedia/" + fileName.Replace(" ", "%20");
                        }
                        else
                        {
                            e.IsValid = false;
                            e.ErrorText = "Device not found with identifier: " + deviceEntity;
                        }
                    }
                    catch (Exception ex)
                    {
                        e.IsValid = false;
                        e.ErrorText = "Error: " + ex.Message;
                    }
                }
                else
                {
                    e.IsValid = false;
                    e.ErrorText = "File extension is not valid. Only XML configuration files can be uploaded.";
                }
            }
            else
            {
                e.IsValid = false;
                e.ErrorText = "No valid device identifier: " + deviceMac;
            }
        }

        #endregion
    }
}