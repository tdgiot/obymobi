<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.SubPanels.DeviceShellPanels.AndroidCommandPanel" Codebehind="AndroidCommandPanel.ascx.cs" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<D:PlaceHolder ID="plhSupervisorCommands" runat="server" Visible="false">
	<td class="control">
        <D:Button ID="btnGetScreenshotSupervisor" OnClientClick="fireCommandFromButton('getscreenshot'); return false;" Text="Get Screenshot" UseSubmitBehavior="false" runat="server"></D:Button>
    </td>
</D:PlaceHolder>
<D:PlaceHolder ID="plhAllCommands" runat="server">
	<table class="dataformV2">
		<tr>
            <td class="control">
                <D:Button ID="btnHelp" OnClientClick="fireCommandFromButton('help'); return false;" Text="Help" UseSubmitBehavior="false" runat="server"></D:Button>
            </td>
		</tr>
		<tr>
            <td class="control">
                <D:Button ID="btnPlaySound" OnClientClick="fireCommandFromButton('playsound'); return false;" Text="Play Sound" UseSubmitBehavior="false" runat="server"></D:Button>
            </td>
		</tr>
		<tr>
            <td class="control">
                <D:Button ID="btnRebootDevice" OnClientClick="fireCommandFromButton('reboot'); return false;" Text="Reboot Device" UseSubmitBehavior="false" runat="server"></D:Button>
            </td>
		</tr>
		<tr>
            <td class="control">
                <D:Button ID="btnRebootDeviceRecovery" OnClientClick="fireCommandFromButton('shell reboot recovery'); return false;" Text="Reboot Device in Recovery" UseSubmitBehavior="false" runat="server"></D:Button>
            </td>
		</tr>
		<tr>
            <td class="control">
                <D:Button ID="btnGetWifiStatus" OnClientClick="fireCommandFromButton('getwifistatus'); return false;" Text="Get WiFi Status" UseSubmitBehavior="false" runat="server"></D:Button>
            </td>
		</tr>
		<tr>
            <td class="control">
                <D:Button ID="btnGetScreenshot" OnClientClick="fireCommandFromButton('getscreenshot'); return false;" Text="Get Screenshot" UseSubmitBehavior="false" runat="server"></D:Button>
            </td>
		</tr>                        
        <tr>
			<td class="control">
				<D:Button ID="btnRCTab" OnClientClick="fireCommandFromButton('shell input tap 200 0 && input tap 200 0'); return false;" Text="Switch to C4 Tab (Aria)" UseSubmitBehavior="false" runat="server"></D:Button>
			</td>
		</tr>
        <tr>
            <td class="control">
                <D:Button ID="btnGetEmenuLatestLog" OnClientClick="fireCommandFromButton('getlog'); return false;" Text="Get Last Emenu Log" UseSubmitBehavior="false" runat="server"></D:Button>
            </td>
		</tr>
        <tr>
            <td class="control">
                <D:Button ID="btnGetEmenuLog" OnClientClick="fireCommandFromButton('getlog -all'); return false;" Text="Get All Emenu Logs" UseSubmitBehavior="false" runat="server"></D:Button>
            </td>
		</tr>
        <tr>
            <td class="control">
                <D:Button ID="btnGetAgentLatestLog" OnClientClick="fireCommandFromButton('getlog -app CraveAgent'); return false;" Text="Get Last Agent Log" UseSubmitBehavior="false" runat="server"></D:Button>
            </td>
		</tr>
        <tr>
            <td class="control">
                <D:Button ID="btnGetSupportToolsLatestLog" OnClientClick="fireCommandFromButton('getlog -app CraveSupportTools'); return false;" Text="Get Last SupportTools Log" UseSubmitBehavior="false" runat="server"></D:Button>
            </td>
		</tr>
        <tr>
            <td class="control">
                <D:Button ID="btnGetMessagingServiceLatestLog" OnClientClick="fireCommandFromButton('getfile /sdcard/CraveMessagingService/Log/log-latest.txt'); return false;" Text="Get Last Messaging Service Log" UseSubmitBehavior="false" runat="server"></D:Button>
            </td>
        </tr>
        <tr>
            <td class="control">
                <D:Button ID="btnGetMessagingServiceStatus" OnClientClick="fireCommandFromButton('messaging-status'); return false;" Text="Get Messaging Service Status" UseSubmitBehavior="false" runat="server"></D:Button>
            </td>
        </tr>
        <tr>
            <td class="control">
                <D:Button ID="btnDownloadConfig" OnClientClick="fireCommandFromButton('getfile sdcard/Crave/AppSettings.xml'); return false;" Text="Download Configuration" UseSubmitBehavior="false" runat="server"></D:Button>
            </td>
		</tr>
        <tr>
            <td class="control">
                <D:Button ID="Button3" OnClientClick="fireCommandFromButton('adb -wifi on'); return false;" Text="Enable ADB (WiFi)" UseSubmitBehavior="false" runat="server"></D:Button>
            </td>
		</tr>
        <tr>
            <td class="control">
                <D:Button ID="Button4" OnClientClick="fireCommandFromButton('adb off'); return false;" Text="Disable ADB" UseSubmitBehavior="false" runat="server"></D:Button>
            </td>
		</tr>
        <tr>
            <td class="control">
                <D:Button ID="btnGetLogFilePath" OnClientClick="fireCommandFromButton('getlogfilepath -all'); return false;" Text="Get log file paths" UseSubmitBehavior="false" runat="server"></D:Button>
            </td>
        </tr>
	</table>                    				
	<table class="dataformV2">
		<tr>
			<td class="control" colspan="4">
				<br/><strong><D:Label runat="server" id="lblExecuteCommand" TranslationTag="lblExecuteCommand" LocalizeText="true">Execute command on device shell</D:Label></strong>
			</td>
		</tr>
		<tr>
			<td class="label"></td>
			<td class="control">
				<select onchange="javascript:document.getElementById('tbShellCommand').value =  this.value; return false;">
					<option value="">Select an action...</option>
                    <option value="ls sdcard/">Show directory contents of sdcard</option>
                    <option value="ls sdcard/CraveAgent/Downloads/">Show contents of Downloads directory</option>
				</select>
            </td>
			<td class="label"></td>
			<td class="control"></td>
		</tr>
		<tr>
			<td class="label"><D:Label runat="server" id="lblExecuteCommandLabel" TranslationTag="lblExecuteCommandLabel" LocalizeText="true">Command</D:Label></td>
			<td class="control">
				<input type="text" id="tbShellCommand" name="tbShellCommand" style="width:100%" /><br />
            </td>
			<td class="label"></td>
			<td class="control"></td>
		</tr>
		<tr>
			<td class="control" colspan="4">
				<D:Button ID="btnShellCommand" OnClientClick="fireCommandFromButton('shell ' + document.getElementById('tbShellCommand').value); return false;" Text="Execute Command" UseSubmitBehavior="false" runat="server"></D:Button>
			</td>
		</tr>
	</table>					
	<table class="dataformV2">
		<tr>
			<td class="control" colspan="4">
				<br/><strong><D:Label runat="server" id="lblShowDialog" TranslationTag="lblShowDialog" LocalizeText="true">Show dialog on device</D:Label></strong>
			</td>
		</tr>
		<tr>
			<td class="label"><D:Label runat="server" id="lblShowDialogTitle" TranslationTag="lblShowDialogTitle" LocalizeText="true">Title</D:Label></td>
			<td class="control">
				<input type="text" id="tbShowDialogTitle" name="tbShowDialogTitle" style="width:100%" />
			</td>
			<td class="label"></td>
			<td class="control"></td>
		</tr>
		<tr>
			<td class="label"><D:Label runat="server" id="lblShowDialogMessage" TranslationTag="lblShowDialogMessage" LocalizeText="true">Message</D:Label></td>
			<td class="control">
				<input type="text" id="tbShowDialogMessage" name="tbShowDialogMessage" style="width:100%" />
			</td>
			<td class="label"></td>
			<td class="control"></td>
		</tr>
		<tr>
			<td class="control" colspan="4">
				<D:Button ID="btnShowDialog" OnClientClick="showDialogButton(); return false;" Text="Show Dialog" UseSubmitBehavior="false" runat="server"></D:Button>
			</td>
		</tr>
	</table>
                    
	<table class="dataformV2">
		<tr>
			<td class="control" colspan="4">
				<br/><strong><D:Label runat="server" id="lblUploadConfig" TranslationTag="lblUploadConfig" LocalizeText="true">Upload configuration to device</D:Label></strong>
			</td>
		</tr>
		<tr>
			<td class="label"><D:Label runat="server" id="lblUploadConfigFile" TranslationTag="lblUploadConfigFile" LocalizeText="true">File</D:Label></td>
			<td class="control">					
				<dx:ASPxUploadControl runat="server" ID="fuImportConfigFile" ClientInstanceName="fuImportConfigFile" ShowProgressPanel="True" Size="45" OnFileUploadComplete="fuImportFile_FileUploadComplete" FileUploadMode="OnPageLoad">
                    <ClientSideEvents FileUploadComplete="function(s, e) { uploadComplete(s, e, 'sdcard/Crave/AppSettings.xml') }" />
                </dx:ASPxUploadControl>
			</td>
			<td class="label"></td>
			<td class="control"></td>
		</tr>
		<tr>
			<td class="control" colspan="4">
				<D:Button ID="Button1" OnClientClick="uploadConfigFile(); return false;" Text="Upload Config" UseSubmitBehavior="false" runat="server"></D:Button>
			</td>
		</tr>
	</table>
					
	<table class="dataformV2">
		<tr>
			<td class="control" colspan="4">
				<br/><strong><D:Label runat="server" id="lblGetEmenuConfig" TranslationTag="lblGetEmenuConfig" LocalizeText="true">Get Emenu configuration value</D:Label></strong>
			</td>
		</tr>
		<tr>
			<td class="label"></td>
			<td class="control">
				<select onchange="javascript:document.getElementById('tbGetEmenuConfig').value =  this.value; return false;">
					<option value="">Select a configuration key...</option>
                    <option value="CompanyOwnerUsername">Company owner username</option>
                    <option value="CompanyOwnerPassword">Company owner password</option>
                    <option value="CompanyId">Company Id</option>
                    <option value="DeliverypointgroupId">Deliverypointgroup Id</option>
                    <option value="ClientId">Client Id</option>
                    <option value="TerminalId">Terminal Id</option>
                    <option value="Password">Password</option>
                    <option value="Pincode">Pincode</option>
                    <option value="PincodeSU">Pincode Super-User</option>
                    <option value="PincodeGM">Pincode God-MOode</option>
                    <option value="DeliverypointId">Deliverypoint Id</option>
                    <option value="DeliverypointNumber">Deliverypoint Number</option>
                    <option value="DisconnectedMode">Disconnected Mode</option>
                    <option value="EnvironmentMode">Environment Mode</option>
                    <option value="CacheImages">Cache Images</option>
                    <option value="SkipDownloadMedia">Skip Download Media</option>
                    <option value="TabletModel">Tablet Model</option>	
                    <option value="AnalyticsUA">Analytics UA</option>
                    <option value="UseTcpSockets">Use OnsiteServer</option>
                    <option value="ServerIp">Server Ip</option>
                    <option value="ServerPort">Server Port</option>
                    <option value="EnableEncryption">Enable Encryption</option>
                    <option value="UIMode">UI Mode</option>
                    <option value="WeatherLocation">Weather Location</option>
                    <option value="FailedStartups">Failed Startups</option>
                    <option value="ApplicationMode">Application Mode</option>
                    <option value="MasterConsole">Master Console</option>
                    <option value="LogToFile">Log To File</option>
                    <option value="ScreenTimeoutInterval">Screen Timeout Interval</option>
                    <option value="PingOfLifeInterval">Ping-Of-Life Interval</option>
                    <option value="LastHeartbeat">Last Heartbeat</option>
                    <option value="ManuallyClosed">Manually Closed</option>
                    <option value="LastRestart">Last Restart</option>
                    <option value="RestartInterval">Restart Interval</option>
                    <option value="MacAddress">MAC address</option>
                    <option value="CompanyDataLastModifiedTicks">Company Data Last Modified Ticks</option>
                    <option value="MediaLastModifiedTicks">Media Last Modified Ticks</option>
                    <option value="MenuDataLastModifiedTicks">Menu Data Last Modified Ticks</option>
                    <option value="CompanyIdLastModifiedTicks">CompanyId Last Modified Ticks</option>
                    <option value="PmsShownWelcomeDialog">Pms Shown Welcome Dialog</option>
                    <option value="PmsGuestInformation">Pms Guest Information</option>
                    <option value="PmsGuestInformationReceived">Pms Guest Information Received</option>	
                    <option value="CraveOsLastVersion">CraveOs Last Version</option>
                    <option value="">----------------------------------------------------------</option>
                    <option value="WifiFullyManaged">Wifi Fully Managed</option>
                    <option value="WifiNetwork">Wifi Network</option>
                    <option value="WifiUsername">Wifi Username</option>
                    <option value="WifiPassword">Wifi Password</option>
                    <option value="WifiSecurity">Wifi Security</option>
                    <option value="WifiHiddenSSID">Wifi Hidden SSID</option>
                    <option value="WifiAnonIdentity">Wifi Anon Identity</option>
                    <option value="WifiEAPMethod">Wifi EAP Method</option>
                    <option value="WifiPhase2Auth">Wifi Phase2 Auth</option>
				</select>
            </td>
			<td class="label"></td>
			<td class="control"></td>
		</tr>
		<tr>
			<td class="label"><D:Label runat="server" id="lblGetEmenuConfigLabel" TranslationTag="lblGetEmenuConfigLabel" LocalizeText="true">Config Key</D:Label></td>
			<td class="control">
				<input type="text" id="tbGetEmenuConfig" name="tbGetEmenuConfig" style="width:100%" />
            </td>
			<td class="label"></td>
			<td class="control"></td>
		</tr>
		<tr>
			<td class="control" colspan="4">
				<D:Button ID="btnGetEmenuConfig" OnClientClick="fireCommandFromButton('getemenuconfig ' + document.getElementById('tbGetEmenuConfig').value); return false;" Text="Get Emenu Config" UseSubmitBehavior="false" runat="server"></D:Button>
			</td>
		</tr>
	</table>
					
	<table class="dataformV2">
		<tr>
			<td class="control" colspan="4">
				<br/><strong><D:Label runat="server" id="lblSetEmenuConfig" TranslationTag="lblSetEmenuConfig" LocalizeText="true">Set Emenu configuration value</D:Label></strong>
			</td>
		</tr>
		<tr>
			<td class="label"></td>
			<td class="control">
				<select onchange="javascript:document.getElementById('tbSetEmenuConfigKey').value =  this.value; return false;">
					<option value="">Select a configuration key...</option>
                    <option value="CompanyOwnerUsername">Company owner username</option>
                    <option value="CompanyOwnerPassword">Company owner password</option>
                    <option value="CompanyId">Company Id</option>
                    <option value="DeliverypointgroupId">Deliverypointgroup Id</option>
                    <option value="ClientId">Client Id</option>
                    <option value="TerminalId">Terminal Id</option>
                    <option value="Password">Password</option>
                    <option value="Pincode">Pincode</option>
                    <option value="PincodeSU">Pincode Super-User</option>
                    <option value="PincodeGM">Pincode God-MOode</option>
                    <option value="DeliverypointId">Deliverypoint Id</option>
                    <option value="DeliverypointNumber">Deliverypoint Number</option>
                    <option value="DisconnectedMode">Disconnected Mode</option>
                    <option value="EnvironmentMode">Environment Mode</option>
                    <option value="CacheImages">Cache Images</option>
                    <option value="SkipDownloadMedia">Skip Download Media</option>
                    <option value="TabletModel">Tablet Model</option>	
                    <option value="AnalyticsUA">Analytics UA</option>
                    <option value="UseTcpSockets">Use OnsiteServer</option>
                    <option value="ServerIp">Server Ip</option>
                    <option value="ServerPort">Server Port</option>
                    <option value="EnableEncryption">Enable Encryption</option>
                    <option value="UIMode">UI Mode</option>
                    <option value="WeatherLocation">Weather Location</option>
                    <option value="FailedStartups">Failed Startups</option>
                    <option value="ApplicationMode">Application Mode</option>
                    <option value="MasterConsole">Master Console</option>
                    <option value="LogToFile">Log To File</option>
                    <option value="ScreenTimeoutInterval">Screen Timeout Interval</option>
                    <option value="PingOfLifeInterval">Ping-Of-Life Interval</option>
                    <option value="LastHeartbeat">Last Heartbeat</option>
                    <option value="ManuallyClosed">Manually Closed</option>
                    <option value="LastRestart">Last Restart</option>
                    <option value="RestartInterval">Restart Interval</option>
                    <option value="CompanyDataLastModifiedTicks">Company Data Last Modified Ticks</option>
                    <option value="MediaLastModifiedTicks">Media Last Modified Ticks</option>
                    <option value="MenuDataLastModifiedTicks">Menu Data Last Modified Ticks</option>
                    <option value="CompanyIdLastModifiedTicks">CompanyId Last Modified Ticks</option>
                    <option value="PmsShownWelcomeDialog">Pms Shown Welcome Dialog</option>
                    <option value="PmsGuestInformation">Pms Guest Information</option>
                    <option value="PmsGuestInformationReceived">Pms Guest Information Received</option>	
                    <option value="CraveOsLastVersion">CraveOs Last Version</option>
                    <option value="">----------------------------------------------------------</option>
                    <option value="WifiFullyManaged">Wifi Fully Managed</option>
                    <option value="WifiNetwork">Wifi Network</option>
                    <option value="WifiUsername">Wifi Username</option>
                    <option value="WifiPassword">Wifi Password</option>
                    <option value="WifiSecurity">Wifi Security</option>
                    <option value="WifiHiddenSSID">Wifi Hidden SSID</option>
                    <option value="WifiAnonIdentity">Wifi Anon Identity</option>
                    <option value="WifiEAPMethod">Wifi EAP Method</option>
                    <option value="WifiPhase2Auth">Wifi Phase2 Auth</option>
				</select>
            </td>
			<td class="label"></td>
			<td class="control"></td>
		</tr>
		<tr>
			<td class="label"><D:Label runat="server" id="lblSetEmenuConfigKey" TranslationTag="lblSetEmenuConfigKey" LocalizeText="true">Key</D:Label></td>
			<td class="control">
				<input type="text" id="tbSetEmenuConfigKey" name="tbSetEmenuConfigKey" style="width:100%" />
			</td>
			<td class="label"></td>
			<td class="control"></td>
		</tr>
		<tr>
			<td class="label"><D:Label runat="server" id="lblSetEmenuConfigValue" TranslationTag="lblSetEmenuConfigValue" LocalizeText="true">Value</D:Label></td>
			<td class="control">
				<input type="text" id="tbSetEmenuConfigValue" name="tbSetEmenuConfigValue" style="width:100%" />
			</td>
			<td class="label"><D:Label runat="server" id="lblSetEmenuConfigEncrypted" TranslationTag="lblSetEmenuConfigEncrypted" LocalizeText="true">Encrypt</D:Label></td>
			<td class="control"><input type="checkbox" id="cbEmenuConfigEncrypted" value="true" /></td>
		</tr>
		<tr>
			<td class="control" colspan="4">
				<D:Button ID="btnSetEmenuConfig" OnClientClick="setEmenuConfigButton(); return false;" Text="Set Config Value" UseSubmitBehavior="false" runat="server"></D:Button>
			</td>
		</tr>
	</table>
					
	<table class="dataformV2">
		<tr>
			<td class="control" colspan="4">
				<br/><strong><D:Label runat="server" id="lblPutFile" TranslationTag="lblPutFile" LocalizeText="true">Upload file to device</D:Label></strong>
			</td>
		</tr>
		<tr>
			<td class="label"><D:Label runat="server" id="lblPutFileName" TranslationTag="lblPutFileName" LocalizeText="true">File</D:Label></td>
			<td class="control">					
				<dx:ASPxUploadControl runat="server" ID="fuImportFile" ClientInstanceName="fuImportFile" ShowProgressPanel="True" Size="45" OnFileUploadComplete="fuImportFile_FileUploadComplete" FileUploadMode="OnPageLoad">
                    <ClientSideEvents FileUploadComplete="function(s, e) { uploadComplete(s, e, document.getElementById('tbPutFileTargetDirectory').value) }" />
                </dx:ASPxUploadControl>
			</td>
			<td class="label"></td>
			<td class="control"></td>
		</tr>
		<tr>
			<td class="label"><D:Label runat="server" id="lblPutFileTargetDirectory" TranslationTag="lblPutFileTargetDirectory" LocalizeText="true">Target Directory</D:Label></td>
			<td class="control">
				<input type="text" id="tbPutFileTargetDirectory" name="tbPutFileTargetDirectory" style="width:100%" value="sdcard/" />
			</td>
			<td class="label"></td>
			<td class="control"></td>
		</tr>
		<tr>
			<td class="control" colspan="4">
				<D:Button ID="btnUploadFile" OnClientClick="uploadFile(); return false;" Text="Upload File" UseSubmitBehavior="false" runat="server"></D:Button>
			</td>
		</tr>
	</table>
					
	<table class="dataformV2">
		<tr>
			<td class="control" colspan="4">
				<br/><strong><D:Label runat="server" id="lblGetFile" TranslationTag="lblGetFile" LocalizeText="true">Download file from device</D:Label></strong>
			</td>
		</tr>
		<tr>
			<td class="label"><D:Label runat="server" id="lblGetFilePath" TranslationTag="lblGetFilePath" LocalizeText="true">File Path</D:Label></td>
			<td class="control">
				<input type="text" id="tbGetFilePath" name="tbGetFilePath" style="width:100%" value="sdcard/" />
			</td>
			<td class="label"></td>
			<td class="control"></td>
		</tr>
		<tr>
			<td class="control" colspan="4">
				<D:Button ID="btnGetFile" OnClientClick="fireCommandFromButton('getfile ' + document.getElementById('tbGetFilePath').value); return false;" Text="Download File" UseSubmitBehavior="false" runat="server"></D:Button>
			</td>
		</tr>
	</table>

	<table class="dataformV2">
		<tr>
			<td class="control" colspan="4">
				<br/><strong><D:Label runat="server" id="lblInstallApp" TranslationTag="lblInstallApp" LocalizeText="true">Install APK on device</D:Label></strong>
			</td>
		</tr>
		<tr>
			<td class="label"><D:Label runat="server" id="lblInstallAppRelease" TranslationTag="lblInstallAppRelease" LocalizeText="true">Release</D:Label></td>
			<td class="control">
							    
				<select onchange="javascript:document.getElementById('tbInstallAppPath').value =  this.value; return false;" ID="cmbReleases" runat="server">
					<option value="sdcard/">Manual Path</option>
                </select>
			</td>
			<td class="label"></td>
			<td class="control"></td>
		</tr>
		<tr>
			<td class="label"><D:Label runat="server" id="lblInstallAppPath" TranslationTag="lblInstallAppPath" LocalizeText="true">File Path</D:Label></td>
			<td class="control">
				<input type="text" id="tbInstallAppPath" name="tbInstallAppPath" style="width:100%" value="sdcard/" />
			</td>
			<td class="label"></td>
			<td class="control"></td>
		</tr>
		<tr>
			<td class="control" colspan="4">
				<D:Button ID="btnInstallApp" OnClientClick="fireCommandFromButton('installapp ' + document.getElementById('tbInstallAppPath').value); return false;" Text="Install APK" UseSubmitBehavior="false" runat="server"></D:Button>
			</td>
		</tr>
	</table>
                    
	<table class="dataformV2">
		<tr>
			<td class="control" colspan="4">
				<br/><strong><D:Label runat="server" id="lblStartApp" TranslationTag="lblStartApp" LocalizeText="true">Start Application</D:Label></strong>
			</td>
		</tr>
		<tr>
			<td class="label"></td>
			<td class="control">
				<select onchange="startAppSelectionChanged(this); return false;">
					<option value="">Select an application...</option>
                    <option value="net.craveinteractive.android.client net.craveinteractive.android.client.StartupActivity">Crave Emenu</option>
                    <option value="net.craveinteractive.craveagent .service.AgentService">Crave Agent</option>
                    <option value="net.craveinteractive.supporttools .service.SupportToolsService">Crave SupportTools</option>
				</select>
            </td>
			<td class="label"></td>
			<td class="control"></td>
		</tr>
		<tr>
			<td class="label"><D:Label runat="server" id="lblStartAppPackage" TranslationTag="lblStartAppPackage" LocalizeText="true">Package Name</D:Label></td>
			<td class="control">
				<input type="text" id="tbStartAppPackage" name="tbStartAppPackage" style="width:100%" value="" />
			</td>
			<td class="label"><D:Label runat="server" id="lblStartAppService" TranslationTag="lblStartAppService" LocalizeText="true">Is Service</D:Label></td>
			<td class="control">
				<input type="checkbox" id="cbStartAppService" value="true" />
			</td>
		</tr>
		<tr>
			<td class="label"><D:Label runat="server" id="lblStartAppActivity" TranslationTag="lblStartAppActivity" LocalizeText="true">Activity Name</D:Label></td>
			<td class="control">
				<input type="text" id="tbStartAppActivity" name="tbStartAppActivity" style="width:100%" value="" />
			</td>
			<td class="label"></td>
			<td class="control">
			</td>
		</tr>
		<tr>
			<td class="control" colspan="4">
				<D:Button ID="btnStartApp" OnClientClick="startAppCommand(); return false;" Text="Start Application" UseSubmitBehavior="false" runat="server"></D:Button>
			</td>
		</tr>
	</table>
                    
	<table class="dataformV2">
		<tr>
			<td class="control" colspan="4">
				<br/><strong><D:Label runat="server" id="lblCloseApp" TranslationTag="lblCloseApp" LocalizeText="true">Close/Restart Application</D:Label></strong>
			</td>
		</tr>
		<tr>
			<td class="label"></td>
			<td class="control">
				<select onchange="javascript:document.getElementById('tbCloseAppPackage').value =  this.value; return false;">
					<option value="">Select an application...</option>
                    <option value="net.craveinteractive.android.client">Crave Emenu</option>
                    <option value="net.craveinteractive.craveagent">Crave Agent</option>
                    <option value="net.craveinteractive.supporttools">Crave SupportTools</option>
				</select>
            </td>
			<td class="label"></td>
			<td class="control"></td>
		</tr>
		<tr>
			<td class="label"><D:Label runat="server" id="lblCloseAppPackage" TranslationTag="lblCloseAppPackage" LocalizeText="true">Package Name</D:Label></td>
			<td class="control">
				<input type="text" id="tbCloseAppPackage" name="tbCloseAppPackage" style="width:100%" value="" />
			</td>
			<td class="label"></td>
			<td class="control">
			</td>
		</tr>
		<tr>
			<td class="control" colspan="4">
				<D:Button ID="btnCloseApp" OnClientClick="closeAppCommand(); return false;" Text="Close Application" UseSubmitBehavior="false" runat="server"></D:Button>
                <D:Button ID="Button2" OnClientClick="restartAppCommand(); return false;" Text="Restart Application" UseSubmitBehavior="false" runat="server"></D:Button>
			</td>
		</tr>
	</table>
</D:PlaceHolder>