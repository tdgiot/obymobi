﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.SubPanels.DeviceShellPanels.WindowsCommandPanel" Codebehind="WindowsCommandPanel.ascx.cs" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<table class="dataformV2">
	<tr>
        <td class="control">
		    <D:Button ID="btnHelp" OnClientClick="fireCommandFromButton('help'); return false;" Text="Help" UseSubmitBehavior="false" runat="server"></D:Button>
	    </td>
    </tr>
    <tr>
        <td class="control">
		    <D:Button ID="btnRebootDevice" OnClientClick="fireCommandFromButton('reboot'); return false;" Text="Reboot PC" UseSubmitBehavior="false" runat="server"></D:Button>
	    </td>
    </tr>
    <tr>
        <td class="control">
            <D:Button ID="btnGetOSSLatestLog" OnClientClick="fireCommandFromButton('getlog'); return false;" Text="Get Last Crave Onsite Server Log" UseSubmitBehavior="false" runat="server"></D:Button>
        </td>    
    </tr>
    <tr>
        <td class="control">
            <D:Button ID="btnGetOSSLog" OnClientClick="fireCommandFromButton('getlog -all'); return false;" Text="Get All Crave Onsite Server Logs" UseSubmitBehavior="false" runat="server"></D:Button>
        </td>    
    </tr>
    <tr>
        <td class="control">
            <D:Button ID="btnGetAgentLatestLog" OnClientClick="fireCommandFromButton('getlog -app CraveOnsiteAgent'); return false;" Text="Get Last Crave Onsite Agent Log" UseSubmitBehavior="false" runat="server"></D:Button>
        </td>    
    </tr>
    <tr>
        <td class="control">
            <D:Button ID="btnGetAgentLog" OnClientClick="fireCommandFromButton('getlog -all -app CraveOnsiteAgent'); return false;" Text="Get All Crave Onsite Agent Logs" UseSubmitBehavior="false" runat="server"></D:Button>
        </td>    
    </tr>
    <tr>
        <td class="control">
            <D:Button ID="btnGetSupportToolsLatestLog" OnClientClick="fireCommandFromButton('getlog -app CraveOnsiteSupportTools'); return false;" Text="Get Last Crave Onsite SupportTools Log" UseSubmitBehavior="false" runat="server"></D:Button>
        </td>    
    </tr>
    <tr>
        <td class="control">
            <D:Button ID="btnGetSupportToolsLog" OnClientClick="fireCommandFromButton('getlog -all -app CraveOnsiteSupportTools'); return false;" Text="Get All Crave Onsite SupportTools Logs" UseSubmitBehavior="false" runat="server"></D:Button>
        </td>    
    </tr>
    <tr>
        <td class="control">
            <D:Button ID="btnDownloadOSSConfig" OnClientClick="fireCommandFromButton('getconfig -app CraveOnsiteServer'); return false;" Text="Download CraveOnsiteServer Configuration" UseSubmitBehavior="false" runat="server"></D:Button>
        </td>    
    </tr>
    <tr>
        <td class="control">
            <D:Button ID="btnDownloadOSAConfig" OnClientClick="fireCommandFromButton('getconfig -app CraveOnsiteAgent'); return false;" Text="Download CraveOnsiteAgent Configuration" UseSubmitBehavior="false" runat="server"></D:Button>
        </td>    
    </tr>    
</table>
<table class="dataformV2">
    <tr>
		<td class="control" colspan="4">
			<br/><strong><D:Label runat="server" id="lblExecuteCommandOnCmd" TranslationTag="lblExecuteCommandOnCmd" LocalizeText="true">Execute command on command line</D:Label></strong>
		</td>
	</tr>
    <tr>
		<td class="label"><D:Label runat="server" id="lblExecuteCommandLabel" TranslationTag="lblExecuteCommandLabel" LocalizeText="true">Command</D:Label></td>
		<td class="control">
			<input type="text" id="tbCmdCommand" name="tbCmdCommand" style="width:100%" /><br />
        </td>
		<td class="label"></td>
		<td class="control"></td>
	</tr>
	<tr>
		<td class="control" colspan="4">
			<D:Button ID="btnCmdCommand" OnClientClick="fireCommandFromButton('cmd ' + document.getElementById('tbCmdCommand').value); return false;" Text="Execute Command" UseSubmitBehavior="false" runat="server"></D:Button>
		</td>
	</tr>
</table>

<table class="dataformV2">
	<tr>
		<td class="control" colspan="4">
			<br/><strong><D:Label runat="server" id="lblUploadConfig" TranslationTag="lblUploadConfig" LocalizeText="true">Upload configuration to pc</D:Label></strong>
		</td>
	</tr>
	<tr>
		<td class="label"><D:Label runat="server" id="lblUploadConfigFile" TranslationTag="lblUploadConfigFile" LocalizeText="true">File</D:Label></td>
		<td class="control">					
			<dx:ASPxUploadControl runat="server" ID="fuImportConfigFile" ClientInstanceName="fuImportConfigFile" ShowProgressPanel="True" Size="45" OnFileUploadComplete="fuImportFile_FileUploadComplete" FileUploadMode="OnPageLoad">
                <ClientSideEvents FileUploadComplete="function(s, e) { configUploaded(s, e, 'CraveOnsiteServer') }" />
            </dx:ASPxUploadControl>
		</td>
		<td class="label"></td>
		<td class="control"></td>
	</tr>
	<tr>
		<td class="control" colspan="4">
			<D:Button ID="Button1" OnClientClick="uploadConfigFile(); return false;" Text="Upload Config" UseSubmitBehavior="false" runat="server"></D:Button>
		</td>
	</tr>
</table>

<table class="dataformV2">
	<tr>
		<td class="control" colspan="4">
			<br/><strong><D:Label runat="server" id="lblPutFile" TranslationTag="lblPutFile" LocalizeText="true">Upload file to pc</D:Label></strong>
		</td>
	</tr>
	<tr>
		<td class="label"><D:Label runat="server" id="lblPutFileName" TranslationTag="lblPutFileName" LocalizeText="true">File</D:Label></td>
		<td class="control">					
			<dx:ASPxUploadControl runat="server" ID="fuImportFile" ClientInstanceName="fuImportFile" ShowProgressPanel="True" Size="45" OnFileUploadComplete="fuImportFile_FileUploadComplete" FileUploadMode="OnPageLoad">
                <ClientSideEvents FileUploadComplete="function(s, e) { uploadComplete(s, e, document.getElementById('tbPutFileTargetDirectory').value) }" />
            </dx:ASPxUploadControl>
		</td>
		<td class="label"></td>
		<td class="control"></td>
	</tr>
	<tr>
		<td class="label"><D:Label runat="server" id="lblPutFileTargetDirectory" TranslationTag="lblPutFileTargetDirectory" LocalizeText="true">Target Directory</D:Label></td>
		<td class="control">
			<input type="text" id="tbPutFileTargetDirectory" name="tbPutFileTargetDirectory" style="width:100%" value="C:\" />
		</td>
		<td class="label"></td>
		<td class="control"></td>
	</tr>
	<tr>
		<td class="control" colspan="4">
			<D:Button ID="btnUploadFile" OnClientClick="uploadFile(); return false;" Text="Upload File" UseSubmitBehavior="false" runat="server"></D:Button>
		</td>
	</tr>
</table>

<table class="dataformV2">
	<tr>
		<td class="control" colspan="4">
			<br/><strong><D:Label runat="server" id="lblGetFile" TranslationTag="lblGetFile" LocalizeText="true">Download file from device</D:Label></strong>
		</td>
	</tr>
	<tr>
		<td class="label"><D:Label runat="server" id="lblGetFilePath" TranslationTag="lblGetFilePath" LocalizeText="true">File Path</D:Label></td>
		<td class="control">
			<input type="text" id="tbGetFilePath" name="tbGetFilePath" style="width:100%" value="C:\" />
		</td>
		<td class="label"></td>
		<td class="control"></td>
	</tr>
	<tr>
		<td class="control" colspan="4">
			<D:Button ID="btnGetFile" OnClientClick="fireCommandFromButton('getfile ' + document.getElementById('tbGetFilePath').value); return false;" Text="Download File" UseSubmitBehavior="false" runat="server"></D:Button>
		</td>
	</tr>
</table>

<table class="dataformV2">
	<tr>
		<td class="control" colspan="4">
			<br/><strong><D:Label runat="server" id="lblStartService" TranslationTag="lblStartService" LocalizeText="true">Start Service</D:Label></strong>
		</td>
	</tr>
	<tr>
		<td class="label"></td>
		<td class="control">
			<select id="selectStartService">
			    <option value="">Select a service...</option>
                <option value="CraveOnsiteServer">Crave On-site Server</option>
                <option value="CraveOnsiteAgent">Crave On-site Agent</option>
			</select>
        </td>
		<td class="label"></td>
		<td class="control"></td>
	</tr>	
	<tr>
		<td class="control" colspan="4">
			<D:Button ID="btnStartService" OnClientClick="startServiceCommand(document.getElementById('selectStartService').value); return false;" Text="Start Service" UseSubmitBehavior="false" runat="server"></D:Button>
		</td>
	</tr>
</table>

<table class="dataformV2">
	<tr>
		<td class="control" colspan="4">
			<br/><strong><D:Label runat="server" id="lblCloseService" TranslationTag="lblCloseService" LocalizeText="true">Close/Restart Service</D:Label></strong>
		</td>
	</tr>
	<tr>
		<td class="label"></td>
		<td class="control">
			<select id="selectCloseService">
				<option value="">Select a service...</option>
                <option value="CraveOnsiteServer">Crave On-site Server</option>
                <option value="CraveOnsiteAgent">Crave On-site Agent</option>
			</select>
        </td>
		<td class="label"></td>
		<td class="control"></td>
	</tr>	
	<tr>
		<td class="control" colspan="4">
			<D:Button ID="btnCloseService" OnClientClick="closeServiceCommand(document.getElementById('selectCloseService').value); return false;" Text="Close Service" UseSubmitBehavior="false" runat="server"></D:Button>
            <D:Button ID="Button2" OnClientClick="restartServiceCommand(document.getElementById('selectCloseService').value); return false;" Text="Restart Service" UseSubmitBehavior="false" runat="server"></D:Button>
		</td>
	</tr>
</table>

<table class="dataformV2">
	<tr>
		<td class="control" colspan="4">
			<br/><strong><D:Label runat="server" id="lblUpdateService" TranslationTag="lblUpdateService" LocalizeText="true">Update Service</D:Label></strong>
		</td>
	</tr>
	<tr>
		<td class="label"></td>
		<td class="control">
			<select id="selectUpdateService">
				<option value="">Select a service...</option>
                <option value="CraveOnsiteServer">Crave On-site Server</option>
                <option value="CraveOnsiteAgent">Crave On-site Agent</option>
			</select>
        </td>
		<td class="label"></td>
		<td class="control"></td>
	</tr>	
	<tr>
		<td class="control" colspan="4">
			<D:Button ID="Button3" OnClientClick="updateServiceCommand(document.getElementById('selectUpdateService').value); return false;" Text="Update Service" UseSubmitBehavior="false" runat="server"></D:Button>            
		</td>
	</tr>
</table>