﻿using System;
using Obymobi.Enums;

namespace Obymobi.ObymobiCms.Company.Subpanels
{
    public partial class CompanyBrandCollection : Dionysos.Web.UI.DevExControls.SubPanelLLBLOverviewDataSourceCollection
    {
        #region Methods

        private void HookUpEvents()
        {
            //this.MainGridView.CustomColumnSort += new DevExpress.Web.ASPxGridViewCustomColumnSortEventHandler(MainGridView_CustomColumnSort);
        }

        #endregion

        #region Events

        protected override void OnInit(EventArgs e)
        {
            this.DataSourceLoaded += CompanyBrandCollection_DataSourceLoaded;

            base.OnInit(e);

            this.EntityName = "CompanyBrand";
            this.EntityPageUrl = "~/Company/CompanyBrand.aspx";

            this.MainGridView.ShowEditHyperlinkColumn = false;
            this.MainGridView.EnableRowDoubleClick = false;
            this.MainGridView.ShowColumnSelectorButton = false;
        }

        void CompanyBrandCollection_DataSourceLoaded(object sender)
        {
            BrandRole userBrandRole = CmsSessionHelper.GetUserRoleForBrand(this.ParentPrimaryKeyFieldValue).GetValueOrDefault(BrandRole.Viewer);

            this.MainGridView.ShowDeleteHyperlinkColumn = userBrandRole == BrandRole.Owner;
            this.MainGridView.ShowEditHyperlinkColumn = userBrandRole == BrandRole.Owner;
            this.MainGridView.EnableRowDoubleClick = userBrandRole == BrandRole.Owner;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.HookUpEvents();           
        }

        #endregion
    }
}