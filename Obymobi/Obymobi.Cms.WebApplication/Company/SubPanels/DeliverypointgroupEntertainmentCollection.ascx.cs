﻿using System;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using System.Linq;
using Obymobi.Enums;

namespace Obymobi.ObymobiCms.Company.SubPanels
{
    public partial class DeliverypointgroupEntertainmentCollection : Dionysos.Web.UI.DevExControls.SubPanelLLBLOverviewDataSourceCollection
    {
       #region methods

        private void SetGui()
        {
            var includeFieldsList = new IncludeFieldsList();
            includeFieldsList.Add(EntertainmentFields.Name);

            var companyAndGenericFilter = new PredicateExpression();
            companyAndGenericFilter.AddWithOr(EntertainmentFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            if (CmsSessionHelper.CurrentRole >= Role.Administrator)
            {
                companyAndGenericFilter.AddWithOr(EntertainmentFields.CompanyId == DBNull.Value);
            }

            var excludeUsedEntertainmentsFilter = new PredicateExpression(EntertainmentFields.EntertainmentId != this.DataSourceAsDeliverypointgroupEntity.DeliverypointgroupEntertainmentCollection.Select(dpge => dpge.EntertainmentId).ToList());
            
            var filter = new PredicateExpression();
            filter.Add(companyAndGenericFilter);
            filter.AddWithAnd(excludeUsedEntertainmentsFilter);

            var sort = new SortExpression(new SortClause(EntertainmentFields.Name, SortOperator.Ascending));

            var entertainments = new EntertainmentCollection();
            entertainments.GetMulti(filter, 0, sort, null, null, includeFieldsList, 0, 0);

            this.ddlEntertainmentId.DataSource = entertainments;
            this.ddlEntertainmentId.DataBind();
        }

        private void AddDeliverypointgroupEntertainment()
        {
            if (this.ddlEntertainmentId.ValidId > 0)
            {
                DeliverypointgroupEntertainmentEntity deliverypointgroupEntertainment = new DeliverypointgroupEntertainmentEntity();
                deliverypointgroupEntertainment.DeliverypointgroupId = this.ParentPrimaryKeyFieldValue;
                deliverypointgroupEntertainment.EntertainmentId = this.ddlEntertainmentId.ValidId;
                
                if (deliverypointgroupEntertainment.Save())
                    this.DataSourceAsDeliverypointgroupEntity.DeliverypointgroupEntertainmentCollection.Add(deliverypointgroupEntertainment);
                
                this.SetGui();
            }
        }

        #endregion

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            this.DataSourceLoaded += DeliverypointgroupEntertainmentPanel_DataSourceLoaded;
            base.OnInit(e);
            this.EntityName = "DeliverypointgroupEntertainment";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.btnAdd.Click += btnAdd_Click;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            this.AddDeliverypointgroupEntertainment();
        }

        private void DeliverypointgroupEntertainmentPanel_DataSourceLoaded(object sender)
        {
            this.SetGui();
        }

        #endregion        

        #region Properties

        /// <summary>
        /// Return the page's datasource as a DeliverypointgroupEntity
        /// </summary>
        public DeliverypointgroupEntity DataSourceAsDeliverypointgroupEntity
        {
            get
            {
                return this.ParentDataSource as DeliverypointgroupEntity;
            }
        }

        #endregion
    }
}