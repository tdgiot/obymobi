﻿using System;
using Obymobi.Data.EntityClasses;
using Dionysos.Web.Google.Geocoding;
using Dionysos.Web.UI.WebControls;

namespace Obymobi.ObymobiCms.Company.Subpanels
{
    public partial class DeliverypointgroupAddress : SubPanel
    {
        public void SetAddress(AddressEntity address)
        {
            address.Addressline1 = tbAddress_Addressline1.Value;
            address.Zipcode = tbAddress_Zipcode.Value;
            address.City = tbAddress_City.Value;

            LookupMapCoordinates(out double? latitude, out double? longitude, address);

            address.Longitude = longitude;
            address.Latitude = latitude;
        }

        public void RenderAddress(AddressEntity address)
        {
            tbAddress_Addressline1.Value = address.Addressline1;
            tbAddress_Zipcode.Value = address.Zipcode;
            tbAddress_City.Value = address.City;
            tbAddress_Longitude.Value = address.Longitude;
            tbAddress_Latitude.Value = address.Latitude;

            LookupMapCoordinates(out double? latitude, out double? longitude, address);

            plhMap.AddHtml("<a href=\"../Generic/LocationPicker.aspx?lat={0}&lon={1}&latInput={2}&lonInput={3}&viewOnly=true\" rel=\"lightbox[500 400]\">View map</a>", latitude ?? 51.50703296721856, longitude ?? -0.127716064453125, tbAddress_Latitude.ClientID, tbAddress_Longitude.ClientID);
        }

        private void LookupMapCoordinates(out double? lat, out double? lng, AddressEntity adressEntity)
        {
            lat = null;
            lng = null;

            string addressLine1 = (adressEntity == null ? tbAddress_Addressline1.Text : adressEntity.Addressline1);
            string zipcode = (adressEntity == null ? tbAddress_Zipcode.Text : adressEntity.Zipcode);
            string city = (adressEntity == null ? tbAddress_City.Text : adressEntity.City);

            string address = "";
            if (addressLine1.Length > 0)
                address += addressLine1 + "+";

            if (zipcode.Length > 0)
                address += zipcode + "+";

            // Only add city/country and lookup when we have an address
            if (address.Length > 0)
            {
                if (city.Length > 0)
                    address += city + "+";

                // Lookup address
                GeoResponse response = GeoCoder.LookupAddress(address, false, "AIzaSyDbNDzR5Wcl6wWy4uPRMqRraXgkErbZUnI");
                if (response != null)
                {
                    if (response.Status.Equals("OK") && response.Results.Length >= 1)
                    {
                        GeoResponse.CResult data = response.Results[0];
                        lat = (double?)data.Geometry.Location.Lat;
                        lng = (double?)data.Geometry.Location.Lng;
                    }
                }
            }
        }
    }
}
