﻿using Dionysos;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using Obymobi.Data.EntityClasses;
using Dionysos.Web.UI;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.ObymobiCms.Company.Subpanels
{
    public partial class RoomControlSectionItemCollection : Dionysos.Web.UI.DevExControls.SubPanelLLBLOverviewDataSourceCollection
    {
        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "RoomControlSectionItem";
            base.OnInit(e);
            this.MainGridView.LoadComplete += MainGridView_LoadComplete;
            this.PageAsPageLLBLGenEntity.DataSourceLoaded += PageAsPageLLBLGenEntity_DataSourceLoaded;
        }

        private void PageAsPageLLBLGenEntity_DataSourceLoaded(object sender)
        {
            if (this.ParentDataSourceAsRoomControlSectionEntity.Type == RoomControlSectionType.Custom)
            {
                this.ddlRoomControlSectionItemType.Items.Add(new DevExpress.Web.ListEditItem(EnumUtil.GetStringValue(RoomControlSectionItemType.StationListing, null), (int)RoomControlSectionItemType.StationListing));
                this.ddlRoomControlSectionItemType.Items.Add(new DevExpress.Web.ListEditItem(EnumUtil.GetStringValue(RoomControlSectionItemType.Scene, null), (int)RoomControlSectionItemType.Scene));
                this.ddlRoomControlSectionItemType.Items.Add(new DevExpress.Web.ListEditItem(EnumUtil.GetStringValue(RoomControlSectionItemType.RemoteControl, null), (int)RoomControlSectionItemType.RemoteControl));
            }             
            else if (this.ParentDataSourceAsRoomControlSectionEntity.Type == RoomControlSectionType.Timers)
            {
                this.ddlRoomControlSectionItemType.Items.Add(new DevExpress.Web.ListEditItem(EnumUtil.GetStringValue(RoomControlSectionItemType.WakeUpTimer, null), (int)RoomControlSectionItemType.WakeUpTimer));
                this.ddlRoomControlSectionItemType.Items.Add(new DevExpress.Web.ListEditItem(EnumUtil.GetStringValue(RoomControlSectionItemType.SleepTimer, null), (int)RoomControlSectionItemType.SleepTimer));
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.btAdd.Click += new EventHandler(btAdd_Click);

            if (this.MainGridView != null)
            {
                this.MainGridView.HtmlRowPrepared += MainGridView_HtmlRowPrepared;
            }
        }

        private void MainGridView_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType != GridViewRowType.Data) return;
            bool isVisible = (bool)e.GetValue("Visible");
            if (!isVisible)
                e.Row.ForeColor = Color.Gray;
        }

        void btAdd_Click(object sender, EventArgs e)
        {
            if (this.ddlRoomControlSectionItemType.ValidId <= 0)
            {
                this.PageAsPageLLBLGenEntity.AddInformator(Dionysos.Web.UI.InformatorType.Warning, this.PageAsPageLLBLGenEntity.Translate("ChooseARoomControlSectionItemTypeToAddARoomControlSectionItem", "Choose a room control section item type to add an item"));
            }
            else
            {
                this.Response.Redirect(string.Format("~/Company/RoomControlSectionItem.aspx?mode=add&RoomControlSectionId={0}&Type={1}", this.PageAsPageLLBLGenEntity.EntityId, this.ddlRoomControlSectionItemType.ValidId));
            }
        }

        private void MainGridView_LoadComplete(object sender, EventArgs e)
        {
            DevExpress.Web.GridViewDataColumn column = this.MainGridView.Columns["SortOrder"] as DevExpress.Web.GridViewDataColumn;
            if (column != null)
            {
                this.MainGridView.SortBy(column, DevExpress.Data.ColumnSortOrder.Ascending);
            }
        }

        public RoomControlSectionEntity ParentDataSourceAsRoomControlSectionEntity
        {
            get
            {
                return this.ParentDataSource as RoomControlSectionEntity;
            }
        }
    }
}