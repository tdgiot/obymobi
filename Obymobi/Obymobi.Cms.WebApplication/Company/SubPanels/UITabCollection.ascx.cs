﻿using System;
using System.Collections.Generic;
using Obymobi.Cms.Logic.HelperClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;

namespace Obymobi.ObymobiCms.Company.Subpanels
{
    public partial class UITabCollection : Dionysos.Web.UI.DevExControls.SubPanelLLBLOverviewDataSourceCollection
    {
        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "UITab";
            base.OnInit(e);
            this.MainGridView.LoadComplete += new EventHandler(MainGridView_LoadComplete);
            this.PageAsPageLLBLGenEntity.DataSourceLoaded += new Dionysos.Delegates.Web.DataSourceLoadedHandler(PageAsPageLLBLGenEntity_DataSourceLoaded);
        }

        void PageAsPageLLBLGenEntity_DataSourceLoaded(object sender)
        {
            if (this.UIModeOnParentDataSource.IsNew)
                return;

            List<UITabType> types = UITabHelper.GetSupportedUITabTypes(this.UIModeOnParentDataSource.Type);

            foreach (UITabType type in types)
            {
                this.ddlUITabType.Items.Add(new DevExpress.Web.ListEditItem(type.ToString(), (int)type));
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.btAdd.Click += new EventHandler(btAdd_Click);
        }

        void btAdd_Click(object sender, EventArgs e)
        {
            if (this.ddlUITabType.ValidId <= 0)
            {
                this.PageAsPageLLBLGenEntity.AddInformator(Dionysos.Web.UI.InformatorType.Warning, this.PageAsPageLLBLGenEntity.Translate("ChooseAUITabTypeToAddAUITab", "Kies een UI tab type om een UI tab toe te voegen"));
            }
            else
            {
                this.Response.Redirect(string.Format("~/Company/UITab.aspx?mode=add&UIModeId={0}&UIModeType={1}&Type={2}", this.PageAsPageLLBLGenEntity.EntityId, (int)this.UIModeOnParentDataSource.Type, this.ddlUITabType.ValidId));
            }
        }

        void MainGridView_LoadComplete(object sender, EventArgs e)
        {
            DevExpress.Web.GridViewDataColumn column = this.MainGridView.Columns["SortOrder"] as DevExpress.Web.GridViewDataColumn;
            if (column != null)
            {
                this.MainGridView.SortBy(column, DevExpress.Data.ColumnSortOrder.Ascending);
            }
        }

        public UIModeEntity UIModeOnParentDataSource
        {
            get
            {
                return this.Parent.DataSource as UIModeEntity;
            }
        }
    }
}