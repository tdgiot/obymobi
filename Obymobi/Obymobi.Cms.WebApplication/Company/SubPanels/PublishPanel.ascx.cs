﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Utils;
using DevExpress.Web;
using DevExpress.Web;
using DevExpress.Web.Internal;
using DevExpress.Web.ASPxTreeList;
using DevExpress.Web.ASPxTreeList.Internal;
using Dionysos;
using Dionysos.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic.Cms;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Logic.Model;
using Obymobi.Logic.Comet;

namespace Obymobi.ObymobiCms.Company.SubPanels
{
    public partial class PublishPanel : SubPanelCustomDataSource
    {
        #region  Fields

        private const string TRANSLATION_KEY_PREFIX = "Obymobi.ObymobiCms.Company.SubPanels.PublishPanel.";
        private MenuDataSource menuDataSource;
        private int Version = 1;

        #endregion

        #region Properties

        private string EndDragNodeMethod
        {
            get
            {
                return @"  function(s, e) {                                                                             
                                e.cancel = true;
		                        var key = s.GetNodeKeyByRow(e.targetElement);
		                        " + this.TreelistId + @".PerformCustomCallback(e.nodeKey + ':' + key);
                            }";
            }
        }

        private string NodeDblClickMethod
        {
            get { return @" function(s, e) {
	                            " + this.TreelistId + @".StartEdit(e.nodeKey);
		                        e.cancel = true;	                                                                    
                            }"; }
        }

        #endregion

        #region Methods

        private void RetrieveSelectedNodes(ref List<TreeListNode> selectedNodes, TreeListNode parent, bool includeChildren = true)
        {
            TreeListNodeCollection nodes = parent == null ? this.tlPublish.Nodes : parent.ChildNodes;
            foreach (TreeListNode node in nodes)
            {
                if (includeChildren)
                {
                    if (node.Selected || node.ParentNode.Selected)
                    {
                        if (node.ParentNode.Selected)
                        {
                            node.Selected = true;
                        }
                        selectedNodes.Add(node);
                    }
                }
                else
                {
                    if (node.Selected && !node.ParentNode.Selected)
                    {
                        selectedNodes.Add(node);
                    }
                }

                if (node.HasChildren)
                {
                    this.RetrieveSelectedNodes(ref selectedNodes, node, includeChildren);
                }
            }
        }

        private void CreateTreeList()
        {
            this.tlPublish.SettingsEditing.Mode = TreeListEditMode.Inline;
            this.tlPublish.SettingsPopupEditForm.Width = 600;
            this.tlPublish.SettingsBehavior.ColumnResizeMode = ColumnResizeMode.Disabled;
            this.tlPublish.SettingsBehavior.AllowSort = false;
            this.tlPublish.SettingsBehavior.AllowDragDrop = false;
            this.tlPublish.ClientInstanceName = this.TreelistId;
            this.tlPublish.AutoGenerateColumns = false;
            this.tlPublish.SettingsEditing.AllowNodeDragDrop = true;
            this.tlPublish.SettingsEditing.AllowRecursiveDelete = true;
            this.tlPublish.Settings.GridLines = GridLines.Both;
            this.tlPublish.ClientSideEvents.EndDragNode = this.EndDragNodeMethod;
            this.tlPublish.ClientSideEvents.NodeDblClick = this.NodeDblClickMethod;
            this.tlPublish.CustomErrorText += this.tlPublishStructure_CustomErrorText;

            this.tlPublish.KeyFieldName = "ItemId";
            this.tlPublish.ParentFieldName = "ParentItemId";

            TreeListTextColumn nameColumn = new TreeListTextColumn();
            nameColumn.Caption = "Name";
            nameColumn.Width = Unit.Pixel(167);
            nameColumn.VisibleIndex = 0;
            nameColumn.FieldName = "Name";
            nameColumn.CellStyle.Wrap = DefaultBoolean.True;
            this.tlPublish.Columns.Add(nameColumn);

            TreeListComboBoxColumn itemTypeColumn = new TreeListComboBoxColumn();
            itemTypeColumn.Caption = "Type";
            itemTypeColumn.Width = Unit.Pixel(70);
            itemTypeColumn.VisibleIndex = 1;
            itemTypeColumn.FieldName = "ItemType";
            itemTypeColumn.CellStyle.Wrap = DefaultBoolean.True;
            this.tlPublish.Columns.Add(itemTypeColumn);

            TreeListCommandColumn editCommandColumn = new TreeListCommandColumn();
            editCommandColumn.VisibleIndex = 4;
            editCommandColumn.Width = Unit.Pixel(60);
            editCommandColumn.ShowNewButtonInHeader = true;
            editCommandColumn.EditButton.Visible = true;
            editCommandColumn.NewButton.Visible = true;
            editCommandColumn.DeleteButton.Visible = true;
            editCommandColumn.ButtonType = ButtonType.Image;
            editCommandColumn.EditButton.Image.Url = this.ResolveUrl("~/images/icons/pencil.png");
            editCommandColumn.DeleteButton.Image.Url = this.ResolveUrl("~/images/icons/delete.png");
            editCommandColumn.CancelButton.Image.Url = this.ResolveUrl("~/images/icons/cross_grey.png");
            editCommandColumn.NewButton.Image.Url = this.ResolveUrl("~/images/icons/add2.png");
            editCommandColumn.UpdateButton.Image.Url = this.ResolveUrl("~/images/icons/disk.png");
            this.tlPublish.Columns.Add(editCommandColumn);

            TreeListHyperLinkColumn editPageV2Column = new TreeListHyperLinkColumn();
            editPageV2Column.Name = "EditPageV2";
            editPageV2Column.VisibleIndex = 5;
            editPageV2Column.Caption = "&nbsp;";
            editPageV2Column.Width = Unit.Pixel(15);
            editPageV2Column.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            editPageV2Column.PropertiesHyperLink.ImageUrl = "~/images/icons/page_gear.png";
            editPageV2Column.EditCellStyle.CssClass = "hidden";
            editPageV2Column.FieldName = "ItemId";
            this.tlPublish.Columns.Add(editPageV2Column);

            TreeListHyperLinkColumn editPageColumn = new TreeListHyperLinkColumn();
            editPageColumn.Name = "EditPage";
            editPageColumn.VisibleIndex = 5;
            editPageColumn.Caption = "&nbsp;";
            editPageColumn.Width = Unit.Pixel(15);
            editPageColumn.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            editPageColumn.PropertiesHyperLink.ImageUrl = "~/images/icons/page_edit.png";
            editPageColumn.EditCellStyle.CssClass = "hidden";
            editPageColumn.FieldName = "ItemId";
            this.tlPublish.Columns.Add(editPageColumn);

            this.tlPublish.CustomCallback += this.tlPublish_CustomCallback;
            this.tlPublish.NodeValidating += this.tlSiteStructure_NodeValidating;
            this.tlPublish.HtmlDataCellPrepared += this.tlPublish_HtmlDataCellPrepared;
            this.tlPublish.HtmlRowPrepared += this.tlPublish_HtmlRowPrepared;
            this.tlPublish.CommandColumnButtonInitialize += this.tlPublish_CommandColumnButtonInitialize;
            this.tlPublish.CellEditorInitialize += this.tlPublish_CellEditorInitialize;

            this.InitDataSource();

            this.tlPublish.Width = Unit.Pixel(736);
        }

        private void tlPublish_CustomCallback(object sender, TreeListCustomCallbackEventArgs e)
        {
            string[] nodes = e.Argument.Split(new char[] { ':' });

            string draggedItemId = nodes[0];
            string draggedUponItemId = nodes[1];

            this.menuDataSource.MenuManager.DraggedToSort(draggedItemId, draggedUponItemId);

            this.tlPublish.DataBind();            
        }

        private void tlPublish_HtmlRowPrepared(object sender, TreeListHtmlRowEventArgs e)
        {
            if (!e.NodeKey.IsNullOrWhiteSpace())
            {
                IMenuItem menuItem = this.menuDataSource.DataSource.SingleOrDefault(item => item.ItemId == e.NodeKey);
                if (menuItem != null)
                {
                    if (menuItem.VisibilityType == VisibilityType.Never)
                    {
                        e.Row.ForeColor = Color.Gray;
                        e.Row.Font.Strikeout = true;
                    }
                    else if (menuItem.IsLinkedToBrand)
                    {
                        e.Row.ForeColor = Color.Gray;
                    }
                }
            }
        }

        private void tlPublish_CommandColumnButtonInitialize(object sender, TreeListCommandColumnButtonEventArgs e)
        {
            if (e.NodeKey != null && e.ButtonType == TreeListCommandColumnButtonType.New)
            {
                string[] keyParts = e.NodeKey.Split(new[] {"-"}, StringSplitOptions.RemoveEmptyEntries);
                if (keyParts.Length > 0)
                {
                    string itemType = keyParts[0];
                    if (itemType == MenuItemType.Product)
                    {
                        e.Visible = DefaultBoolean.False;
                    }
                }
            }
        }

        private void tlPublish_HtmlDataCellPrepared(object sender, TreeListHtmlDataCellEventArgs e)
        {
            bool editPage = e.Column.Name.Equals("EditPage", StringComparison.InvariantCultureIgnoreCase);
            bool editPageV2 = e.Column.Name.Equals("EditPageV2", StringComparison.InvariantCultureIgnoreCase);

            if (e.NodeKey != null && (editPage || editPageV2))
            {
                string navigateUrl = string.Empty;
                string[] keyParts = e.NodeKey.Split(new[] {"-"}, StringSplitOptions.RemoveEmptyEntries);
                if (keyParts.Length > 1)
                {
                    string itemType = keyParts[0];
                    string itemId = keyParts[1];
                    if (itemType == MenuItemType.Category)
                    {
                        navigateUrl = string.Format("~/Catalog/Category.aspx?id={0}", itemId);
                        if (editPageV2)
                        {
                            e.Cell.Text = string.Empty;
                        }                        
                    }
                    else if (itemType == MenuItemType.Product)
                    {
                        if (editPage)
                            navigateUrl = string.Format("~/Catalog/Product.aspx?id={0}", itemId);
                        else
                            navigateUrl = string.Format("~/Catalog/ProductV2.aspx?id={0}&Version={1}", itemId, this.Version);
                    }
                }
                foreach (Control item in e.Cell.Controls)
                {
                    HyperLinkDisplayControl link = item as HyperLinkDisplayControl;
                    if (link != null)
                    {
                        link.NavigateUrl = navigateUrl;
                        break;
                    }
                }
            }
        }

        private void tlPublish_CellEditorInitialize(object sender, TreeListColumnEditorEventArgs e)
        {
            if (e.Column.FieldName == "ItemType")
            {
                TreeListNode parentNode = null;
                if (e.NodeKey != null)
                {
                    TreeListNode currentNode = this.tlPublish.FindNodeByKeyValue(e.NodeKey);
                    if (currentNode != null)
                    {
                        parentNode = currentNode.ParentNode;
                    }
                }
                else
                {
                    string parentItemId = this.tlPublish.NewNodeParentKey;
                    parentNode = this.tlPublish.FindNodeByKeyValue(parentItemId);
                }

                ASPxComboBox cbType = e.Editor as ASPxComboBox;
                if (parentNode is TreeListRootNode)
                {
                    // Root node
                    this.DataBindEnumValuesToCombBox(cbType, MenuItemType.Category);
                    cbType.SelectedItem = cbType.Items.FindByValue(MenuItemType.Category);
                }
                else if (parentNode != null && parentNode.DataItem is CategoryEntity)
                {
                    CategoryEntity parentCategory = (CategoryEntity)parentNode.DataItem;
                    if (parentCategory.ProductCategoryCollection.Count > 0)
                    {
                        // Products as siblings
                        this.DataBindEnumValuesToCombBox(cbType, MenuItemType.Product);
                        cbType.SelectedItem = cbType.Items.FindByValue(MenuItemType.Product);
                    }
                    else if (parentCategory.ChildCategoryCollection.Count > 0)
                    {
                        // Categories as sibling
                        this.DataBindEnumValuesToCombBox(cbType, MenuItemType.Category);
                        cbType.SelectedItem = cbType.Items.FindByValue(MenuItemType.Category);
                    }
                    else
                    {
                        // No children today
                        this.DataBindEnumValuesToCombBox(cbType, MenuItemType.Category, MenuItemType.Product);
                        cbType.SelectedItem = cbType.Items.FindByValue(MenuItemType.Category);
                    }
                }
            }
        }

        private void DataBindEnumValuesToCombBox(ASPxComboBox combobox, params object[] values)
        {
            combobox.Items.Clear();
            foreach (object value in values)
            {
                combobox.Items.Add(value.ToString(), value);
            }
            combobox.DataBind();
        }

        private void InitDataSource()
        {
            if (this.DataSource != null)
            {
                this.menuDataSource = new MenuDataSource(this.DataSourceAsMenuEntity.MenuId);
                this.tlPublish.DataSource = this.menuDataSource;
            }
        }

        public void Refresh()
        {
            this.InitDataSource();
            this.tlPublish.DataBind();
        }

        private void ExpandCategoryNodes(TreeListNodeCollection nodes)
        {
            foreach (TreeListNode node in nodes)
            {
                if (node is TreeListRootNode)
                {
                    node.Expanded = true;
                }
                else if (node != null && node.DataItem is CategoryEntity)
                {
                    CategoryEntity category = (CategoryEntity)node.DataItem;
                    if (category.ChildCategoryCollection.Count > 0)
                    {
                        node.Expanded = true;
                    }
                }

                if (node.HasChildren)
                {
                    this.ExpandCategoryNodes(node.ChildNodes);
                }
            }
        }

        public void RefreshDataSource(IEntity datasource)
        {
            this.DataSource = datasource;
            if (this.DataSource != null && !((IEntity)this.DataSource).IsNew)
            {
                this.Refresh();
            }
        }

        private void HideProductmenuItems()
        {
            List<TreeListNode> selectedNodes = new List<TreeListNode>();
            this.RetrieveSelectedNodes(ref selectedNodes, null, false);

            if (selectedNodes.Count <= 0)
            {
                this.PageAsPageDefault.MultiValidatorDefault.AddError(this.PageAsPageDefault.Translate(PublishPanel.TRANSLATION_KEY_PREFIX + "Error.NoItemSelectedToHide", "No item has been selected to hide.", true));
                this.PageAsPageDefault.Validate();
            }
            else
            {
                this.HideItems(selectedNodes);
                this.tlPublish.DataBind();

                if (selectedNodes.Count == 1)
                {
                    this.PageAsPageDefault.AddInformatorInfo(this.PageAsPageDefault.Translate(PublishPanel.TRANSLATION_KEY_PREFIX + "Success.ItemHidden", "The selected item is hidden.", true));
                }
                else
                {
                    this.PageAsPageDefault.AddInformatorInfo(this.PageAsPageDefault.Translate(PublishPanel.TRANSLATION_KEY_PREFIX + "Success.ItemsHidden", "The selected items are hidden.", true));
                }

                this.PageAsPageDefault.Validate();
            }
        }

        private void HideItems(List<TreeListNode> nodes)
        {
            if (nodes.Count <= 0)
                return;

            int categoryIdAddition = 100000000;

            List<ProductmenuItem> productmenuItems = new List<ProductmenuItem>();
            foreach (TreeListNode node in nodes)
            {
                if (node.DataItem is CategoryEntity)
                {
                    CategoryEntity category = (CategoryEntity)node.DataItem;
                    if (category.VisibilityType != VisibilityType.Never)
                    {
                        category.VisibilityType = VisibilityType.Never;
                        category.Save();

                        int parentProductmenuItemId = category.ParentCategoryId + categoryIdAddition ?? -1;
                        productmenuItems.Add(this.CreateProductmenuItem(category.CategoryId + categoryIdAddition, parentProductmenuItemId, ProductmenuItemType.Category));
                    }                   
                }
                else if (node.DataItem is ProductCategoryEntity)
                {
                    ProductCategoryEntity productCategoryEntity = (ProductCategoryEntity)node.DataItem;

                    ProductEntity product = productCategoryEntity.ProductEntity;
                    if (product.VisibilityType != VisibilityType.Never)
                    {
                        product.VisibilityType = VisibilityType.Never;
                        product.Save();

                        productmenuItems.Add(this.CreateProductmenuItem(product.ProductId, productCategoryEntity.CategoryId + categoryIdAddition, ProductmenuItemType.Product));
                    }
                }

                node.Selected = false;
            }

            if (productmenuItems.Count > 0)
            {
                MenuUpdated menuUpdated = new MenuUpdated();
                menuUpdated.MenuUpdatedAction = (int)MenuUpdatedAction.Hide;
                menuUpdated.ProductmenuItems = productmenuItems.ToArray();

                CometHelper.MenuUpdated(CmsSessionHelper.CurrentCompanyId, menuUpdated);
            }            
        }

        private ProductmenuItem CreateProductmenuItem(int id, int parentProductmenuItemId, ProductmenuItemType type)
        {
            ProductmenuItem productmenuItem = new ProductmenuItem(id, parentProductmenuItemId);
            productmenuItem.VisibilityType = (int)VisibilityType.Never;
            productmenuItem.ItemType = (int)type;

            return productmenuItem;
        }

        private void DeleteSelected()
        {
            List<TreeListNode> selectedNodes = new List<TreeListNode>();
            this.RetrieveSelectedNodes(ref selectedNodes, null);

            if (selectedNodes.Count <= 0)
            {
                this.PageAsPageDefault.MultiValidatorDefault.AddError(this.PageAsPageDefault.Translate(PublishPanel.TRANSLATION_KEY_PREFIX + "Error.NoItemSelected", "No item has been selected to be deleted.", true));
                this.PageAsPageDefault.Validate();
            }
            else
            {
                this.DeleteItems(selectedNodes);
                this.tlPublish.DataBind();

                if (selectedNodes.Count == 1)
                {
                    this.PageAsPageDefault.AddInformatorInfo(this.PageAsPageDefault.Translate(PublishPanel.TRANSLATION_KEY_PREFIX + "Success.ItemsDeleted", "The selected item has been deleted.", true));
                }
                else
                {
                    this.PageAsPageDefault.AddInformatorInfo(this.PageAsPageDefault.Translate(PublishPanel.TRANSLATION_KEY_PREFIX + "Success.ItemsDeleted", "The selected items have been deleted.", true));
                }
                this.PageAsPageDefault.Validate();
            }
        }

        private void DeleteItems(List<TreeListNode> selectedNodes)
        {
            foreach (TreeListNode node in selectedNodes)
            {
                this.menuDataSource.DeleteParameters.Clear();
                this.menuDataSource.DeleteParameters.Add("ItemId", node.Key);
                this.menuDataSource.Delete();
            }

            this.DataSource = new MenuEntity(this.DataSourceAsMenuEntity.MenuId);
            this.InitDataSource();
        }

        #endregion

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            this.CreateTreeList();

            this.hlExpandAll.NavigateUrl = string.Format("javascript:{0}.ExpandAll()", this.TreelistId);
            this.hlCollapseAll.NavigateUrl = string.Format("javascript:{0}.CollapseAll()", this.TreelistId);
        }

        private void tlPublishStructure_CustomErrorText(object sender, TreeListCustomErrorTextEventArgs e)
        {
            if (e.Exception.InnerException != null)
            {
                ObymobiException exception = e.Exception.InnerException as ObymobiException;
                if (exception != null)
                {
                    e.ErrorText = exception.AdditionalMessage;
                }
                else
                {
                    e.ErrorText = e.Exception.InnerException.Message;
                }
            }
        }

        private void tlSiteStructure_NodeValidating(object sender, TreeListNodeValidationEventArgs e)
        {
            if (e.HasErrors)
            {
                e.NodeError = this.PageAsPageDefault.Translate(PublishPanel.TRANSLATION_KEY_PREFIX + "PleaseCorrectErrors", "Please correct the errors.", true);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Version = CmsSessionHelper.AlterationDialogMode == AlterationDialogMode.v3 ? 2 : 1;

            this.tlPublish.DataBind();
            this.ExpandCategoryNodes(this.tlPublish.Nodes);

            string deleteMessage = this.PageAsPageDefault.Translate(PublishPanel.TRANSLATION_KEY_PREFIX + "Warning.PreSubmit", "Are you sure you want to delete these items? Child items will be automatically removed if the parent item is selected.", true);
            string hideMessage = this.PageAsPageDefault.Translate(PublishPanel.TRANSLATION_KEY_PREFIX + "Warning.HidePreSubmit", "Are you sure you want to hide these items?", true);

            this.btDeleteItemsTop.PreSubmitWarning = deleteMessage;
            this.btDeleteItemsBottom.PreSubmitWarning = deleteMessage;

            this.btHideTop.PreSubmitWarning = hideMessage;
            this.btHideBottom.PreSubmitWarning = hideMessage;

            this.btDeleteItemsTop.Click += this.btDeleteSelected_Click;
            this.btDeleteItemsBottom.Click += this.btDeleteSelected_Click;

            this.btHideTop.Click += this.btHide_Click;
            this.btHideBottom.Click += this.btHide_Click;
        }

        private void btHide_Click(object sender, EventArgs e)
        {
            this.HideProductmenuItems();
        }

        private void btDeleteSelected_Click(object sender, EventArgs e)
        {
            this.DeleteSelected();
        }

        private string TreelistId
        {
            get { return string.Format("{0}_treelist", this.ID); }
        }

        private MenuEntity DataSourceAsMenuEntity
        {
            get { return (MenuEntity)this.DataSource; }
        }

        protected void cmbItem_Callback(object sender, CallbackEventArgsBase e)
        {
        }

        #endregion
    }
}