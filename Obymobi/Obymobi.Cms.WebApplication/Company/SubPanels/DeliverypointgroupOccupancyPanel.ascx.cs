﻿using DevExpress.Web.ASPxTreeList;
using Dionysos;
using Dionysos.Web.UI.WebControls;
using System;
using System.Web.UI.WebControls;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Company.SubPanels
{
    public partial class DeliverypointgroupOccupancyPanel : SubPanelEntity
    {
        private const string TRANSLATION_KEY_PREFIX = "Obymobi.ObymobiCms.Company.SubPanels.DeliverypointgroupOccupancyPanel.";

        private BaseDataSource<DeliverypointgroupOccupancyManager> deliverypointgroupOccupancyDataSource;

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            this.DataSourceLoaded += DeliverypointOccupancyPanel_DataSourceLoaded;
        }

        void DeliverypointOccupancyPanel_DataSourceLoaded(object sender)
        {
            if (!((IEntity)this.PageAsPageEntity.DataSource).IsNew)
            {
                this.CreateTreeList();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.tlOccupancy.DataBind();
        }

        public override bool Save()
        {
            return true;
        }

        public override bool Validate()
        {
            return true;
        }

        public override bool InitializeEntity(bool pageInitComplete)
        {
            return true;
        }

        private void CreateTreeList()
        {
            // Create Tree View
            this.tlOccupancy.SettingsEditing.Mode = TreeListEditMode.Inline;
            this.tlOccupancy.SettingsBehavior.AllowSort = true;
            this.tlOccupancy.SettingsBehavior.AllowDragDrop = false;
            this.tlOccupancy.ClientInstanceName = "treelist";
            this.tlOccupancy.AutoGenerateColumns = false;
            this.tlOccupancy.KeyFieldName = "DeliverypointgroupOccupancyId";
            this.tlOccupancy.SettingsBehavior.AutoExpandAllNodes = true;
            this.tlOccupancy.SettingsEditing.AllowNodeDragDrop = false;
            this.tlOccupancy.Settings.GridLines = GridLines.Both;
            this.tlOccupancy.SettingsPager.Mode = TreeListPagerMode.ShowPager;
            this.tlOccupancy.ClientSideEvents.NodeDblClick = @" function(s, e) {
	                                                                    treelist.StartEdit(e.nodeKey);
		                                                                e.cancel = true;	                                                                    
                                                                    }";

            // Date Column
            var dateColumn = new TreeListDateTimeColumn();
            dateColumn.Caption = "Date";
            dateColumn.FieldName = "Date";
            dateColumn.Width = Unit.Pixel(100);
            dateColumn.PropertiesDateEdit.AllowNull = false;
            dateColumn.PropertiesDateEdit.TimeSectionProperties.Visible = false;
            dateColumn.VisibleIndex = 0;
            this.tlOccupancy.Columns.Add(dateColumn);

            // Amount Column
            var amountColumn = new TreeListTextColumn();
            amountColumn.Caption = "Occupancy";
            amountColumn.FieldName = "Amount";
            amountColumn.Width = Unit.Pixel(50);
            amountColumn.VisibleIndex = 0;
            this.tlOccupancy.Columns.Add(amountColumn);

            // In-line Edit Column
            var editCommandColumn = new TreeListCommandColumn();
            editCommandColumn.VisibleIndex = 2;
            editCommandColumn.Width = Unit.Pixel(50);
            editCommandColumn.ShowNewButtonInHeader = true;
            editCommandColumn.EditButton.Visible = true;
            editCommandColumn.DeleteButton.Visible = true;
            editCommandColumn.ButtonType = ButtonType.Image;
            editCommandColumn.EditButton.Image.Url = this.ResolveUrl("~/images/icons/pencil.png");
            editCommandColumn.DeleteButton.Image.Url = this.ResolveUrl("~/images/icons/delete.png");
            editCommandColumn.CancelButton.Image.Url = this.ResolveUrl("~/images/icons/cross_grey.png");
            editCommandColumn.UpdateButton.Image.Url = this.ResolveUrl("~/images/icons/disk.png");
            editCommandColumn.NewButton.Image.Url = this.ResolveUrl("~/images/icons/add2.png");
            this.tlOccupancy.Columns.Add(editCommandColumn);

            // Events
            this.tlOccupancy.SettingsEditing.Mode = DevExpress.Web.ASPxTreeList.TreeListEditMode.Inline;
            this.tlOccupancy.NodeValidating += tlOccupancy_NodeValidating;
            this.tlOccupancy.CustomErrorText += tlOccupancy_CustomErrorText;
            
            // Data Source must be set in OnInit: http://documentation.devexpress.com/#AspNet/DevExpressWebASPxTreeListASPxTreeList_DataSourcetopic
            this.InitDataSource();

            this.tlOccupancy.Width = Unit.Pixel(360);
        }

        void tlOccupancy_CustomErrorText(object sender, TreeListCustomErrorTextEventArgs e)
        {
            if (e.Exception.InnerException != null)
            {
                var exception = e.Exception.InnerException as ObymobiException;
                if (exception != null)
                {
                    e.ErrorText = exception.AdditionalMessage;
                }
                else
                {
                    e.ErrorText = e.Exception.InnerException.Message;
                }
            }
        }

        void tlOccupancy_NodeValidating(object sender, TreeListNodeValidationEventArgs e)
        {
            int amount = 0;
            if (e.NewValues["Amount"] is int)
            {
                amount = (int)e.NewValues["Amount"];
                if (amount < 0)
                {
                    e.NodeError = this.PageAsPageEntity.Translate(TRANSLATION_KEY_PREFIX + "Error.AmountNotCorrect", "Geen geldig aantal ingevult.", true);
                }
            }
            else
            {
                if (!int.TryParse((string)e.NewValues["Amount"], out amount))
                {
                    e.NodeError = this.PageAsPageEntity.Translate(TRANSLATION_KEY_PREFIX + "Error.AmountNotEntered", "Occupancy moet een nummer zijn.", true);
                }
            }
        }

        private void InitDataSource()
        {
            var parentEntity = (IEntity)this.Parent.DataSource;
            var primaryKey = (int)parentEntity.PrimaryKeyFields[0].CurrentValue;

            var dataManager = new DeliverypointgroupOccupancyManager(primaryKey);
            this.deliverypointgroupOccupancyDataSource = new BaseDataSource<DeliverypointgroupOccupancyManager>(dataManager);
            this.tlOccupancy.DataSource = this.deliverypointgroupOccupancyDataSource;
        }
    }
}