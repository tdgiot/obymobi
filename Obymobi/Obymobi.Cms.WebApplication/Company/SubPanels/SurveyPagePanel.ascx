﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.SurveyPagePanel" Codebehind="SurveyPagePanel.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v20.1" Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dxwtl" %>
<%@ Register Assembly="Dionysos.Web" Namespace="Dionysos.Web.UI.WebControls" TagPrefix="cc1" %>
    <D:Button runat="server" ID="btAddPage" Text="Pagina toevoegen" /><br /><br />
&nbsp;
<table>
    <D:Label ID="lblDragTip" runat="server"><small>Note: sleep met de pagina's om de sorteer volgorde te veranderen.</small></D:Label>
    <X:TreeList ID="tlPages" ClientInstanceName="treelist" OnCustomCallback="Page_CustomCallback" runat="server" AutoGenerateColumns="False" KeyFieldName="SurveyPageId" Width="100%">            
        <settingsbehavior autoexpandallnodes="True" />
        <Settings GridLines="Horizontal" />
        <SettingsEditing AllowNodeDragDrop="true"/>
        <SettingsSelection Enabled="true" />
        <ClientSideEvents EndDragNode="function(s, e){
            e.cancel = true;
            var key = s.GetNodeKeyByRow(e.targetElement);
            treelist.PerformCustomCallback('reorder' + ':' + e.nodeKey + ':' + key);
        }" />
        <Columns>                    
            <dxwtl:TreeListHyperLinkColumn caption="Pagina" fieldname="SurveyPageId" visibleindex="1" CellStyle-HorizontalAlign="Left">
                <PropertiesHyperLink TextField="Name" NavigateUrlFormatString="../SurveyPage.aspx?id={0}" >
                </PropertiesHyperLink>           
                <EditFormSettings VisibleIndex="0" />
            </dxwtl:TreeListHyperLinkColumn>            
            <dxwtl:TreeListDataColumn caption="Volgorde" fieldname="SortOrder" visibleindex="2" CellStyle-HorizontalAlign="Right">
                <EditFormSettings VisibleIndex="1" />
            </dxwtl:TreeListDataColumn>  
        </Columns>        
    </X:TreeList><br />
    <div id="divSurveyPageBottomButtons">
        <D:Button runat="server" ID="btDeletePage" Text="Verwijder geselecteerd(e) pagina/pagina's" />
    </div>
</table>


