﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Enums;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Routing;

namespace Obymobi.ObymobiCms.Company.SubPanels
{
    public partial class RoutestephandlerCollection : Dionysos.Web.UI.DevExControls.SubPanelLLBLOverviewDataSourceCollection
    {
        #region Methods

        #endregion

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "Routestephandler";
            base.OnInit(e);
            this.PageAsPageLLBLGenEntity.DataSourceLoaded += PageAsPageLLBLGenEntity_DataSourceLoaded;
        }

        private void PageAsPageLLBLGenEntity_DataSourceLoaded(object sender)
        {
            // Check if the requested handler is compatible with the already existing steps
            // GK Exuses for the 2-level Linq... 
            // What it does: See if ANY steps have ANY stephandlers that are automatic (and therefore not compatible with console step)
            // If a step has an automatic handler, no additional steps can be added
            if (this.RoutestepOnParentDataSource.RoutestephandlerCollection.Any(rs => RoutingHelper.GetAutomaticlyHandledRoutestephandlers().Contains(rs.HandlerTypeAsEnum)))
            {
                // Remove Add controls
                this.ddlHandlerType.Visible = false;
                this.btAdd.Visible = false;
            }
            else if (this.RoutestepOnParentDataSource.RoutestephandlerCollection.Any(rs => RoutingHelper.GetManuallyHandledRoutestephandlers().Contains(rs.HandlerTypeAsEnum)))
            {
                // Restrict adding to Console only
                this.ddlHandlerType.Items.Clear();
                string text = RoutestephandlerType.Console.ToString();
                int value = (int)RoutestephandlerType.Console;
                this.ddlHandlerType.Items.Add(text, value);
            }            
            else
                this.ddlHandlerType.DataBindEnumStringValuesAsDataSource(typeof(RoutestephandlerType));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.btAdd.Click += new EventHandler(btAdd_Click);
        }

        private void btAdd_Click(object sender, EventArgs e)
        {
            if (this.ddlHandlerType.ValidId <= 0)
            {
                this.PageAsPageLLBLGenEntity.AddInformator(Dionysos.Web.UI.InformatorType.Warning, this.PageAsPageLLBLGenEntity.Translate("ChooseAHandleTypeToAddARouteStepHandler", "Kies een Route Step Handeler type om deze toe te voegen"));
            }
            else
            {
                this.Response.Redirect(string.Format("~/Company/Routestephandler.aspx?mode=add&RoutestepId={0}&HandlerType={1}",
                    this.PageAsPageLLBLGenEntity.EntityId, this.ddlHandlerType.ValidId));
            }
        }

        #endregion

        #region Properties

        public RoutestepEntity RoutestepOnParentDataSource
        {
            get
            {
                return this.Parent.DataSource as RoutestepEntity;
            }
        }

        #endregion        
    }
}
