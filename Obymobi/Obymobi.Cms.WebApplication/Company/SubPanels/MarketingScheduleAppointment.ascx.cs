﻿using System;
using DevExpress.Web;
using DevExpress.Web;
using DevExpress.Web.ASPxScheduler;
using DevExpress.Web.ASPxScheduler.Internal;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Data.EntityClasses;
using System.Web.UI;

public partial class Company_SubPanels_MarketingScheduleAppointment : SchedulerFormControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public override void DataBind()
    {
        base.DataBind();

        MarketingScheduleAppointmentForm container = (MarketingScheduleAppointmentForm)Parent;
        ASPxScheduler control = container.Control;
        
        this.DataBindWidgets(container);
        this.DataBindMedia(container);
        this.DataBindScheduledMessages(container);
        this.DataBindMessagegroups(container);

        if (container.IsNewAppointment)
        {
            this.ceBackgroundColor.Color = System.Drawing.Color.White;
            this.ceTextColor.Color = System.Drawing.Color.Black;
        }
        else
        {
            this.ceBackgroundColor.Color = System.Drawing.Color.FromArgb(container.BackgroundColor);
            this.ceTextColor.Color = System.Drawing.Color.FromArgb(container.TextColor);
        }

        if (this.ddlScheduledMessageId.ValidId > 0)
        {
            this.Page.ClientScript.RegisterClientScriptInclude("ShowMessagegroupId", "ShowMessagegroupRow();");
        }

        this.btnOk.ClientSideEvents.Click = container.SaveHandler;
        this.btnCancel.ClientSideEvents.Click = container.CancelHandler;
    }

    private void DataBindWidgets(MarketingScheduleAppointmentForm container)
    {
        PredicateExpression widgetsFilter = new PredicateExpression();
        widgetsFilter.Add(DeliverypointgroupFields.DeliverypointgroupId == container.DeliverypointgroupId);

        RelationCollection widgetsRelations = new RelationCollection();
        widgetsRelations.Add(UIWidgetEntity.Relations.UITabEntityUsingUITabId);
        widgetsRelations.Add(UITabEntity.Relations.UIModeEntityUsingUIModeId);
        widgetsRelations.Add(UIModeEntity.Relations.DeliverypointgroupEntityUsingUIModeId);

        UIWidgetCollection widgets = new UIWidgetCollection();
        widgets.GetMulti(widgetsFilter, 0, null, widgetsRelations);

        this.ddlUIWidgetId.DataSource = widgets;
        this.ddlUIWidgetId.DataBind();
        this.ddlUIWidgetId.Value = container.UIWidgetId;
    }

    private void DataBindMedia(MarketingScheduleAppointmentForm container)
    {
        PredicateExpression slideFilter = new PredicateExpression();
        slideFilter.Add(DeliverypointgroupFields.DeliverypointgroupId == container.DeliverypointgroupId);
        slideFilter.Add(MediaFields.AgnosticMediaId == DBNull.Value);

        PredicateExpression slideMediaTypeFilter = new PredicateExpression();
        slideMediaTypeFilter.Add(MediaRatioTypeMediaFields.MediaType == MediaType.Homepage1280x800);
        slideMediaTypeFilter.AddWithOr(MediaRatioTypeMediaFields.MediaType == MediaType.HomepageFullscreen1280x800);
        slideFilter.Add(slideMediaTypeFilter);

        RelationCollection slideRelations = new RelationCollection();
        slideRelations.Add(MediaEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId, JoinHint.Inner);
        slideRelations.Add(MediaEntity.Relations.MediaRatioTypeMediaEntityUsingMediaId, JoinHint.Inner);

        MediaCollection homepageSlides = new MediaCollection();
        homepageSlides.GetMulti(slideFilter, 0, null, slideRelations);

        this.ddlMediaId.DataSource = homepageSlides;
        this.ddlMediaId.DataBind();
        this.ddlMediaId.Value = container.MediaId;
    }

    private void DataBindScheduledMessages(MarketingScheduleAppointmentForm container)
    {
        PredicateExpression scheduledMessageFilter = new PredicateExpression();
        scheduledMessageFilter.Add(ScheduledMessageFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

        ScheduledMessageCollection scheduledMessages = new ScheduledMessageCollection();
        scheduledMessages.GetMulti(scheduledMessageFilter);

        this.ddlScheduledMessageId.DataSource = scheduledMessages;
        this.ddlScheduledMessageId.DataBind();
        this.ddlScheduledMessageId.Value = container.ScheduledMessageId;
    }

    private void DataBindMessagegroups(MarketingScheduleAppointmentForm container)
    {
        PredicateExpression messagegroupFilter = new PredicateExpression();
        messagegroupFilter.Add(MessagegroupFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

        MessagegroupCollection messagegroups = new MessagegroupCollection();
        messagegroups.GetMulti(messagegroupFilter);

        this.ddlMessagegroupId.DataSource = messagegroups;
        this.ddlMessagegroupId.DataBind();
        this.ddlMessagegroupId.Value = container.MessagegroupId;
    }

    protected override ASPxEditBase[] GetChildEditors()
    {
        return new ASPxEditBase[] { this.ddlUIWidgetId, this.ddlMediaId, this.ddlScheduledMessageId, this.ddlMessagegroupId, this.ceBackgroundColor, this.ceTextColor };
    }

    protected override ASPxButton[] GetChildButtons()
    {
        ASPxButton[] buttons = new ASPxButton[] { btnOk, btnCancel };
        return buttons;
    }
}