﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.SurveyAnswerPanel" Codebehind="SurveyAnswerPanel.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v20.1" Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dxwtl" %>
<%@ Register Assembly="Dionysos.Web" Namespace="Dionysos.Web.UI.WebControls" TagPrefix="cc1" %>
    <D:Button runat="server" ID="btAddAnswer" Text="Antwoord toevoegen" /><br /><br />
&nbsp;
<table>
    <D:Label ID="lblDragTip" runat="server"><small>Note: sleep met de antwoorden om de sorteer volgorde te veranderen.</small></D:Label>
    <X:TreeList ID="tlAnswers" ClientInstanceName="treelist" OnCustomCallback="Answers_CustomCallback" runat="server" AutoGenerateColumns="False" KeyFieldName="SurveyAnswerId" Width="100%">            
        <settingsbehavior autoexpandallnodes="True" />
        <Settings GridLines="Horizontal" />
        <SettingsEditing AllowNodeDragDrop="true"/>
        <SettingsSelection Enabled="true" />
        <ClientSideEvents EndDragNode="function(s, e){
            e.cancel = true;
            var key = s.GetNodeKeyByRow(e.targetElement);
            treelist.PerformCustomCallback('reorder' + ':' + e.nodeKey + ':' + key);
        }" />
        <Columns>                    
            <dxwtl:TreeListHyperLinkColumn caption="Antwoord" fieldname="SurveyAnswerId" visibleindex="1" CellStyle-HorizontalAlign="Left">
                <PropertiesHyperLink TextField="Answer" NavigateUrlFormatString="../SurveyAnswer.aspx?id={0}" >
                </PropertiesHyperLink>           
                <EditFormSettings VisibleIndex="0" />
            </dxwtl:TreeListHyperLinkColumn>            
            <dxwtl:TreeListDataColumn caption="Volgorde" fieldname="SortOrder" visibleindex="2" CellStyle-HorizontalAlign="Right">
                <EditFormSettings VisibleIndex="1" />
            </dxwtl:TreeListDataColumn>  
        </Columns>        
    </X:TreeList><br />
    <div id="divSurveyAnswerBottomButtons">
        <D:Button runat="server" ID="btDeleteAnswer" Text="Verwijder geselecteerd(e) antwoord(en)" />
    </div>
</table>


