﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.SubPanels.DeliverypointgroupAnnouncementCollection" Codebehind="DeliverypointgroupAnnouncementCollection.ascx.cs" %>
<div style="overflow:hidden;margin-bottom:10px;">
   <div style="float:left;margin-right:10px;">
   <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlAnnouncementId" UseDataBinding="true" EntityName="Announcement" TextField="Title" PreventEntityCollectionInitialization="true" Width="250px" notdirty="true"></X:ComboBoxLLBLGenEntityCollection>
   </div>
   <div style="float:left;">
   <D:Button runat="server" ID="btnAdd" Text="Add announcement" ToolTip="Add" />
   </div>
</div>

<X:GridViewColumnSelector runat="server" ID="gvcsDeliverypointgroupAnnouncement" >
	<Columns />
</X:GridViewColumnSelector>