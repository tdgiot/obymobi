﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Company_SubPanels_MarketingScheduleAppointment" Codebehind="MarketingScheduleAppointment.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.ASPxScheduler.v20.1" Namespace="DevExpress.Web.ASPxScheduler" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.XtraScheduler.v20.1.Core" Namespace="DevExpress.XtraScheduler" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxScheduler.v20.1" Namespace="DevExpress.Web.ASPxScheduler.Controls" TagPrefix="dx" %>
<table id="tbScheduleAppointment" class="dataformV2">
    <tr>
		<td class="label">
		    <D:LabelEntityFieldInfo runat="server" id="lblUIWidgetId">Widget</D:LabelEntityFieldInfo>
	    </td>
	    <td class="control" colspan="3">
            <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlUIWidgetId" ClientIDMode="Static" CssClass="input" UseDataBinding="true" PreventEntityCollectionInitialization="True" EntityName="UIWidget" TextField="NameOrTypeName" IsRequired="false">
                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" />
                <ClientSideEvents Validation="function(s, e) { OnWidgetValidate(s, e); }" ValueChanged="function(s, e) { OnUpdateControlValue(s, e); }" KeyUp="function(s,e) { OnUpdateControlValue(s,e); }" />
            </X:ComboBoxLLBLGenEntityCollection>
	    </td>		
	</tr>					    
    <tr>
		<td class="label">
		    <D:LabelEntityFieldInfo runat="server" id="lblMediaId">Homepage Slide</D:LabelEntityFieldInfo>
	    </td>
	    <td class="control" colspan="3">
            <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlMediaId" ClientIDMode="Static" CssClass="input" UseDataBinding="true" PreventEntityCollectionInitialization="True" EntityName="Media" IsRequired="false">
                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" />
                <ClientSideEvents Validation="function(s, e) { OnMediaValidate(s, e); }" ValueChanged="function(s, e) { OnUpdateControlValue(s, e); }" KeyUp="function(s,e) { OnUpdateControlValue(s,e); }" />
            </X:ComboBoxLLBLGenEntityCollection>
	    </td>		
	</tr>					    
    <tr>
		<td class="label">
		    <D:LabelEntityFieldInfo runat="server" id="lblScheduledMessageId">Message</D:LabelEntityFieldInfo>
	    </td>
	    <td class="control" colspan="3">
            <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlScheduledMessageId" ClientIDMode="Static" CssClass="input" UseDataBinding="true" PreventEntityCollectionInitialization="True" EntityName="ScheduledMessage" TextField="MessageName" IsRequired="false">
                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" />
                <ClientSideEvents Validation="function(s, e) { OnScheduledMessageValidate(s, e); }" ValueChanged="function(s, e) { OnUpdateControlValue(s, e); }" KeyUp="function(s,e) { OnUpdateControlValue(s,e); }" />
            </X:ComboBoxLLBLGenEntityCollection>
	    </td>
	</tr>
    <tr id="trMessagegroupId" style="display:none">
		<td class="label">
		    <D:LabelEntityFieldInfo runat="server" id="lblMessagegroupId">Send To</D:LabelEntityFieldInfo>
	    </td>
	    <td class="control" colspan="3">
            <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlMessagegroupId" ClientIDMode="Static" CssClass="input" UseDataBinding="true" PreventEntityCollectionInitialization="True" EntityName="Messagegroup" IsRequired="false">
                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" />
                <ClientSideEvents Validation="function(s, e) { OnMessagegroupValidate(s, e); }" ValueChanged="function(s, e) { OnUpdateControlValue(s, e); }" KeyUp="function(s,e) { OnUpdateControlValue(s,e); }" />
            </X:ComboBoxLLBLGenEntityCollection>
	    </td>
	</tr>
    <tr>
		<td class="label">
		    <D:LabelEntityFieldInfo runat="server" id="lblBackgroundColor">Background Color</D:LabelEntityFieldInfo>
	    </td>
	    <td class="control">
             <dxe:ASPxColorEdit runat="server" ID="ceBackgroundColor" UseDataBinding="false" EnableCustomColors="true" />
	    </td>
        <td class="label">
		    <D:LabelEntityFieldInfo runat="server" id="lblTextColor">Text Color</D:LabelEntityFieldInfo>
	    </td>
	    <td class="control">
             <dxe:ASPxColorEdit runat="server" ID="ceTextColor" UseDataBinding="false" EnableCustomColors="true" />
	    </td>
	</tr>
    <tr>
		<td class="label">
            <D:Label runat="server" id="lblStartTime">Start Time</D:Label>
        </td>
        <td class="control">
            <dxe:ASPxDateEdit ID="edtStartDate" ClientIDMode="Static" runat="server" EditFormat="DateTime" DateOnError="Undo" AllowNull="false" Date='<%# ((AppointmentFormTemplateContainer)Container).Start %>'>
                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" EnableCustomValidation="True" />
                <ClientSideEvents Validation="function(s, e) { OnStartTimeValidate(s, e); }" ValueChanged="function(s, e) { OnUpdateControlValue(s, e); }" />
            </dxe:ASPxDateEdit>
        </td>
        <td class="label">
            <D:Label runat="server" id="lblEndTime">End Time</D:Label>
        </td>
        <td class="control">
            <dxe:ASPxDateEdit ID="edtEndDate" ClientIDMode="Static" runat="server" EditFormat="DateTime" DateOnError="Undo" AllowNull="false" Date='<%# ((AppointmentFormTemplateContainer)Container).End %>'>
                <ValidationSettings EnableCustomValidation="True" ErrorDisplayMode="ImageWithTooltip" />
                <ClientSideEvents Validation="function(s, e) { OnEndTimeValidate(s, e); }" ValueChanged="function(s, e) { OnUpdateControlValue(s, e); }" />
            </dxe:ASPxDateEdit>
        </td>
    </tr>
    <tr>
        <td class="label">
	    </td>
	    <td class="control" colspan="3">
            <dx:AppointmentRecurrenceForm ID="AppointmentRecurrenceForm1" runat="server"
                IsRecurring='<%# ((AppointmentFormTemplateContainer)Container).Appointment.IsRecurring %>' 
                DayNumber='<%# ((AppointmentFormTemplateContainer)Container).RecurrenceDayNumber %>' 
                End='<%# ((AppointmentFormTemplateContainer)Container).RecurrenceEnd %>' 
                Month='<%# ((AppointmentFormTemplateContainer)Container).RecurrenceMonth %>' 
                OccurrenceCount='<%# ((AppointmentFormTemplateContainer)Container).RecurrenceOccurrenceCount %>' 
                Periodicity='<%# ((AppointmentFormTemplateContainer)Container).RecurrencePeriodicity %>' 
                RecurrenceRange='<%# ((AppointmentFormTemplateContainer)Container).RecurrenceRange %>' 
                Start='<%# ((AppointmentFormTemplateContainer)Container).RecurrenceStart %>' 
                WeekDays='<%# ((AppointmentFormTemplateContainer)Container).RecurrenceWeekDays %>' 
                WeekOfMonth='<%# ((AppointmentFormTemplateContainer)Container).RecurrenceWeekOfMonth %>' 
                RecurrenceType='<%# ((AppointmentFormTemplateContainer)Container).RecurrenceType %>'
                IsFormRecreated='<%# ((AppointmentFormTemplateContainer)Container).IsFormRecreated %>'>
            </dx:AppointmentRecurrenceForm>
        </td>
    </tr>
</tabl>
<table style="width: 100%; height: 35px;">
    <tr>
        <td style="width: 100%; height: 100%;" align="center">
            <table style="height: 100%;">
                <tr>
                    <td colspan="4">
                        <D:Label ID="lblError" ClientIDMode="Static" runat="server" ForeColor="Red" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <dxe:ASPxButton runat="server" ID="btnOk" Text="OK" UseSubmitBehavior="False" AutoPostBack="False" ClientInstanceName="btnOk" EnableViewState="False" Width="91px" />
                    </td>
                    <td style="padding: 0 4px;">
                        <dxe:ASPxButton runat="server" ID="btnCancel" Text="Cancel" UseSubmitBehavior="false" AutoPostBack="false" EnableViewState="false" Width="91px" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<table cellpadding="0" cellspacing="0" style="width: 100%;">
    <tr>
        <td style="width: 100%;" align="left">
            <dx:ASPxSchedulerStatusInfo runat="server" ID="schedulerStatusInfo" Priority="1" MasterControlId='<%# ((DevExpress.Web.ASPxScheduler.AppointmentFormTemplateContainer)Container).ControlId %>' />
        </td>
    </tr>
</table>
<script id="dxss_myAppoinmentForm007" type="text/javascript">
    // <![CDATA[
    function OnStartTimeValidate(s, e) {
        if (!e.isValid)
            return;
        var startDate = edtStartDate.GetDate();
        var endDate = edtEndDate.GetDate();
        e.isValid = startDate == null || endDate == null || startDate < endDate;
        e.errorText = "The Start Date must precede the End Date.";
    }
    function OnEndTimeValidate(s, e) {
        if (!e.isValid)
            return;
        var startDate = edtStartDate.GetDate();
        var endDate = edtEndDate.GetDate();
        e.isValid = startDate == null || endDate == null || startDate < endDate;
        e.errorText = "The Start Date must precede the End Date.";
    }
    function OnWidgetValidate(s, e) {
        if (!e.isValid)
            return;
        e.isValid = SelectedItemCount() == 1;
        e.errorText = "Please select one item";
        btnOk.SetEnabled(e.isValid);
    }
    function OnMediaValidate(s, e) {
        if (!e.isValid)
            return;
        e.isValid = SelectedItemCount() == 1;
        e.errorText = "Please select one item";
        btnOk.SetEnabled(e.isValid);
    }
    function OnScheduledMessageValidate(s, e) {
        if (!e.isValid)
            return;
        e.isValid = SelectedItemCount() == 1;
        e.errorText = "Please select one item";
        btnOk.SetEnabled(e.isValid);
    }
    function OnMessagegroupValidate(s, e) {
        if (!e.isValid)
            return;
        e.isValid = ddlScheduledMessageId.GetValue() != null && ddlMessagegroupId.GetValue() != null;
        e.errorText = "Please select a message group to send the message to";

        btnOk.SetEnabled(e.isValid);
    }
    function SelectedItemCount() {
        var count = 0;
        if (ddlUIWidgetId.GetValue() != null) {
            count++;
        }
        if (ddlMediaId.GetValue() != null) {
            count++;
        }
        if (ddlScheduledMessageId.GetValue() != null) {
            count++;
        }
        return count;
    }
    function OnUpdateControlValue(s, e) {
        var isValid = ASPxClientEdit.ValidateEditorsInContainer(null);
        
        if (ddlScheduledMessageId.GetValue() != null) {
            document.getElementById("trMessagegroupId").style.display = "table-row";

            if (ddlMessagegroupId.GetValue() == null) {
                ddlMessagegroupId.SetIsValid(false);
                ddlMessagegroupId.SetErrorText("Please select a message group to send the message to");

                isValid = false;
            }
        } else {
            document.getElementById("trMessagegroupId").style.display = "none";
        }

        btnOk.SetEnabled(isValid)
    }
    function ShowMessagegroupRow() {
        document.getElementById("trMessagegroupId").style.display = "table-row";
    }
    <%# ((MarketingScheduleAppointmentForm)Container).MessagegroupId > 0 ? "ShowMessagegroupRow();" : "" %>
    window.setTimeout("ASPxClientEdit.ValidateEditorsInContainer(null)", 0);
    // ]]> 
</script>