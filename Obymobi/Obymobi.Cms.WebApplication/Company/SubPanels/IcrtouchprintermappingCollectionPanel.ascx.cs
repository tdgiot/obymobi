﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Obymobi.ObymobiCms.Company.Subpanels
{
    public partial class IcrtouchprintermappingCollectionPanel : Dionysos.Web.UI.DevExControls.SubPanelLLBLOverviewDataSourceCollection
    {

        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "Icrtouchprintermapping";
            base.OnInit(e);
        }

    }
}