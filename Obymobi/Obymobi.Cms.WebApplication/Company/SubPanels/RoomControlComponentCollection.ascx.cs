﻿using Dionysos;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using Obymobi.Data.EntityClasses;
using Dionysos.Web.UI;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.ObymobiCms.Company.Subpanels
{
    public partial class RoomControlComponentCollection : Dionysos.Web.UI.DevExControls.SubPanelLLBLOverviewDataSourceCollection
    {
        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "RoomControlComponent";
            base.OnInit(e);
            this.MainGridView.LoadComplete += MainGridView_LoadComplete;
            this.PageAsPageLLBLGenEntity.DataSourceLoaded += PageAsPageLLBLGenEntity_DataSourceLoaded;
        }

        private void PageAsPageLLBLGenEntity_DataSourceLoaded(object sender)
        {
            if (this.ParentDataSource is RoomControlSectionEntity)
            {
                RoomControlSectionEntity sectionEntity = (RoomControlSectionEntity)this.ParentDataSource;
                foreach (RoomControlComponentType type in Enum.GetValues(typeof(RoomControlComponentType)))
                {
                    if ((type == RoomControlComponentType.Light && sectionEntity.Type == RoomControlSectionType.Lighting) ||
                        (type == RoomControlComponentType.LightingScene && sectionEntity.Type == RoomControlSectionType.Lighting) ||
                        (type == RoomControlComponentType.Thermostat && sectionEntity.Type == RoomControlSectionType.Comfort) ||
                        (type == RoomControlComponentType.Blind && sectionEntity.Type == RoomControlSectionType.Blinds) ||
                        (type == RoomControlComponentType.Service && sectionEntity.Type == RoomControlSectionType.Service))
                    {
                        this.ddlRoomControlComponentType.Items.Add(new DevExpress.Web.ListEditItem(EnumUtil.GetStringValue(type, null), (int)type));
                    }
                }
            }            
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.btAdd.Click += new EventHandler(btAdd_Click);

            if (this.MainGridView != null)
            {
                this.MainGridView.HtmlRowPrepared += MainGridView_HtmlRowPrepared;
            }
        }

        private void MainGridView_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType != GridViewRowType.Data) return;
            bool isVisible = (bool)e.GetValue("Visible");
            if (!isVisible)
                e.Row.ForeColor = Color.Gray;
        }

        void btAdd_Click(object sender, EventArgs e)
        {
            if (this.ddlRoomControlComponentType.ValidId <= 0)
            {
                this.PageAsPageLLBLGenEntity.AddInformator(Dionysos.Web.UI.InformatorType.Warning, this.PageAsPageLLBLGenEntity.Translate("ChooseARoomControlComponentTypeToAddARoomControlSectionItem", "Choose a room control component type to add an item"));
            }
            else if (this.ParentDataSource is RoomControlSectionEntity)
            {
                this.Response.Redirect(string.Format("~/Company/RoomControlComponent.aspx?mode=add&RoomControlSectionId={0}&Type={1}", this.PageAsPageLLBLGenEntity.EntityId, this.ddlRoomControlComponentType.ValidId));
            }
            else if (this.ParentDataSource is RoomControlSectionItemEntity)
            {
                this.Response.Redirect(string.Format("~/Company/RoomControlComponent.aspx?mode=add&RoomControlSectionItemId={0}&Type={1}", this.PageAsPageLLBLGenEntity.EntityId, this.ddlRoomControlComponentType.ValidId));
            }
        }

        private void MainGridView_LoadComplete(object sender, EventArgs e)
        {
            DevExpress.Web.GridViewDataColumn column = this.MainGridView.Columns["SortOrder"] as DevExpress.Web.GridViewDataColumn;            
            if (column != null)
            {
                this.MainGridView.SortBy(column, DevExpress.Data.ColumnSortOrder.Ascending);
            }
        }        
    }
}