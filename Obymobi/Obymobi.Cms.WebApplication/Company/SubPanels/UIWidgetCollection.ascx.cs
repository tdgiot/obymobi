﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using Dionysos.Web.UI;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.ObymobiCms.Company.Subpanels
{
    public partial class UIWidgetCollection : Dionysos.Web.UI.DevExControls.SubPanelLLBLOverviewDataSourceCollection
    {
        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "UIWidget";
            base.OnInit(e);
            this.MainGridView.LoadComplete += new EventHandler(MainGridView_LoadComplete);
            this.PageAsPageLLBLGenEntity.DataSourceLoaded += new Dionysos.Delegates.Web.DataSourceLoadedHandler(PageAsPageLLBLGenEntity_DataSourceLoaded);
        }

        void MainGridView_LoadComplete(object sender, EventArgs e)
        {
            DevExpress.Web.GridViewDataColumn column = this.MainGridView.Columns["SortOrder"] as DevExpress.Web.GridViewDataColumn;
            if (column != null)
            {
                this.MainGridView.SortBy(column, DevExpress.Data.ColumnSortOrder.Ascending);
            }
        }

        void PageAsPageLLBLGenEntity_DataSourceLoaded(object sender)
        {
            if (this.UITabOnParentDataSource.IsNew)
                return;

            this.ddlUIWidgetType.DataBindEnum<UIWidgetType>();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.btAdd.Click += new EventHandler(btAdd_Click);
        }

        void btAdd_Click(object sender, EventArgs e)
        {
            if (this.ddlUIWidgetType.ValidId <= 0)
            {
                this.PageAsPageLLBLGenEntity.AddInformator(Dionysos.Web.UI.InformatorType.Warning, this.PageAsPageLLBLGenEntity.Translate("ChooseAUIWidgetTypeToAddAUIWidget", "Kies een UI widget type om een UI widget toe te voegen"));
            }
            else
            {
                this.Response.Redirect(string.Format("~/Company/UIWidget.aspx?mode=add&UITabId={0}&Type={1}", this.PageAsPageLLBLGenEntity.EntityId, this.ddlUIWidgetType.ValidId));
            }
        }

        public UITabEntity UITabOnParentDataSource
        {
            get
            {
                return this.Parent.DataSource as UITabEntity;
            }
        }
    }
}