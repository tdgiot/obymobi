﻿using System;
using System.Collections;
using System.Data;
using Dionysos;
using Dionysos.Data.LLBLGen;
using Dionysos.Web.UI;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.ObymobiCms.Company.SubPanels
{
    public partial class RescheduleCommandsPanel : Dionysos.Web.UI.WebControls.SubPanelLLBLGenEntity
    {
        #region Properties

        public ScheduledCommandTaskEntity ParentDataSourceAsScheduledCommandTaskEntity
        {
            get
            {
                return this.PageAsPageLLBLGenEntity.DataSource as ScheduledCommandTaskEntity;
            }
        }        

        #endregion

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "ScheduledCommand";
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.HookUpEvents();
        }

        private void BtnRescheduleFailedCommands_Click(object sender, EventArgs e)
        {
            this.RescheduleFailedCommands();
        }

        protected override void OnParentDataSourceLoaded()
        {
            base.OnParentDataSourceLoaded();
            this.SetGui();
        }

        #endregion

        #region Methods

        private void SetGui()
        {
            int unsuccessfulCount = ScheduledCommandTaskHelper.GetUnsuccessfullCommands(this.ParentDataSourceAsScheduledCommandTaskEntity.ScheduledCommandTaskId).Count;
            int commandCount = this.ParentDataSourceAsScheduledCommandTaskEntity.CommandCount;

            this.lblUnsuccessfulValue.Text = string.Format("{0} of {1} ({2}%)", unsuccessfulCount, commandCount, Dionysos.Math.Percentage(unsuccessfulCount, commandCount));
        }

        private void HookUpEvents()
        {
            this.btnRescheduleFailedCommands.Click += BtnRescheduleFailedCommands_Click;
        }

        private void RescheduleFailedCommands()
        {
            if (!this.Validate())
            {
                // No can do
            }
            else
            {
                Transaction transaction = new Transaction(IsolationLevel.ReadUncommitted, string.Format("RescheduleFailedCommands-{0}", this.ParentDataSourceAsScheduledCommandTaskEntity.ScheduledCommandTaskId));
                try
                {
                    ScheduledCommandTaskEntity scheduledCommandTask = new ScheduledCommandTaskEntity();
                    scheduledCommandTask.AddToTransaction(transaction);
                    scheduledCommandTask.CompanyId = this.ParentDataSourceAsScheduledCommandTaskEntity.CompanyId;
                    scheduledCommandTask.StartUTC = this.tbStart.Value.Value.LocalTimeToUtc(this.ParentDataSourceAsScheduledCommandTaskEntity.CompanyEntity.TimeZoneInfo);
                    scheduledCommandTask.ExpiresUTC = this.tbExpires.Value.Value.LocalTimeToUtc(this.ParentDataSourceAsScheduledCommandTaskEntity.CompanyEntity.TimeZoneInfo);
                    scheduledCommandTask.Active = true;

                    Obymobi.Data.CollectionClasses.ScheduledCommandCollection failedScheduledCommands = ScheduledCommandTaskHelper.GetUnsuccessfullCommands(this.ParentDataSourceAsScheduledCommandTaskEntity.ScheduledCommandTaskId);

                    foreach (ScheduledCommandEntity failedScheduledCommand in failedScheduledCommands)
                    {
                        ScheduledCommandEntity newScheduledCommand = new ScheduledCommandEntity();
                        newScheduledCommand.AddToTransaction(transaction);
                        newScheduledCommand.Validator = null;
                        newScheduledCommand.ParentCompanyId = failedScheduledCommand.ParentCompanyId;
                        newScheduledCommand.ClientCommand = failedScheduledCommand.ClientCommand;
                        newScheduledCommand.ClientId = failedScheduledCommand.ClientId;
                        newScheduledCommand.TerminalCommand = failedScheduledCommand.TerminalCommand;
                        newScheduledCommand.TerminalId = failedScheduledCommand.TerminalId;
                        newScheduledCommand.Sort = failedScheduledCommand.Sort;
                        newScheduledCommand.ScheduledCommandTaskId = scheduledCommandTask.ScheduledCommandTaskId;
                        newScheduledCommand.Status = ScheduledCommandStatus.Pending;
                        scheduledCommandTask.ScheduledCommandCollection.Add(newScheduledCommand);                        
                    }
                    scheduledCommandTask.Save(true);
                    transaction.Commit();

                    this.PageAsPageEntity.AddInformator(InformatorType.Information, string.Format("Rescheduled the unsuccessful commands in a new <a href=\"{0}\">task</a>.",
                        this.ResolveUrl("~/Company/ScheduledCommandTask.aspx?id={0}".FormatSafe(scheduledCommandTask.ScheduledCommandTaskId)))); 
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    transaction.Dispose();
                }                
            }
        }

        private bool Validate()
        {
            bool valid = true;
            
            if (this.ParentDataSourceAsScheduledCommandTaskEntity.Status != ScheduledCommandTaskStatus.CompletedWithErrors && this.ParentDataSourceAsScheduledCommandTaskEntity.Status != ScheduledCommandTaskStatus.Expired)
            {
                this.PageAsPageEntity.AddInformator(InformatorType.Warning, "Please wait for the scheduled command task to end before rescheduling the failed commands.");
                valid = false;
            }
            else if (this.tbStart.Value <= DateTime.UtcNow.UtcToLocalTime(CmsSessionHelper.CompanyTimeZone))
            {
                this.PageAsPageEntity.AddInformator(InformatorType.Warning, "Starts on value has to be in the future.");
                valid = false;
            }
            else if (this.tbExpires.Value <= this.tbStart.Value)
            {
                this.PageAsPageEntity.AddInformator(InformatorType.Warning, "'Expires on' has to be further in time than value of field 'Starts on'.");
                valid = false;
            }
            else if (!ScheduledCommandTaskHelper.GetHasUnsuccessfullCommands(this.ParentDataSourceAsScheduledCommandTaskEntity.ScheduledCommandTaskId))
            {
                this.PageAsPageEntity.AddInformator(InformatorType.Warning, "The scheduled command task doesn't contain any failed commands to reschedule.");
                valid = false;
            }

            return valid;
        }

        public override bool Save()
        {
            return true;
        }        

        #endregion
    }
}