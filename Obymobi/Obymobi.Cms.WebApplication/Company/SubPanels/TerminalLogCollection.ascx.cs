﻿using System;
using DevExpress.Web;
using DevExpress.XtraGrid;

namespace Obymobi.ObymobiCms.Company.Subpanels
{
    public partial class TerminalLogCollection : Dionysos.Web.UI.DevExControls.SubPanelLLBLOverviewDataSourceCollection
    {
        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "TerminalLog";
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.MainGridView.DataBind();
        }
        
        //void MainGridView_LoadComplete(object sender, EventArgs e)
        //{
        //    var column = this.MainGridView.Columns["Created"] as GridViewDataColumn;
        //    if(column != null)
        //    {
        //        column.Settings.SortMode = ColumnSortMode.Custom;
        //    }
        //}

        //void MainGridView_CustomColumnSort(object sender, DevExpress.Web.CustomColumnSortEventArgs e)
        //{
        //    if (e.Column.FieldName == "Created")
        //    {
        //        DateTime? dateTimeVal1 = null, dateTimeVal2 = null;
        //        try
        //        {
        //            dateTimeVal1 = Convert.ToDateTime(e.Value1);
        //        }
        //        catch (FormatException ex) { }

        //        try
        //        {
        //            dateTimeVal2 = Convert.ToDateTime(e.Value2);
        //        }
        //        catch (FormatException ex) { }

        //        if (!dateTimeVal1.HasValue)
        //            e.Result = 1;
        //        else if (!dateTimeVal2.HasValue)
        //            e.Result = -1;
        //        else
        //        {
        //            if (dateTimeVal1.Value < dateTimeVal2.Value)
        //                e.Result = 1;
        //            else if (dateTimeVal1.Value > dateTimeVal2.Value)
        //                e.Result = -1;
        //            else
        //                e.Result = 0;
        //        }

        //        e.Handled = true;
        //    }
        //}
    }
}
