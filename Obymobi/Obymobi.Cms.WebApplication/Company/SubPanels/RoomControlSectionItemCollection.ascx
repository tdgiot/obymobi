﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.Subpanels.RoomControlSectionItemCollection" Codebehind="RoomControlSectionItemCollection.ascx.cs" %>
<div style="overflow:hidden;margin-bottom:10px;">
	<div style="float:left;margin-right:10px">
        <X:ComboBoxInt ID="ddlRoomControlSectionItemType" runat="server" UseDataBinding="true" Width="150px" notdirty="true"></X:ComboBoxInt>
	</div>
    <div style="float:left;">
        <D:Button runat="server" ID="btAdd" CommandName="Add" Text="Add item" ToolTip="Add" />
    </div>
</div>
<X:GridViewColumnSelector runat="server" ID="gvcsRoomControlSectionItems">
	<Columns />
</X:GridViewColumnSelector>

