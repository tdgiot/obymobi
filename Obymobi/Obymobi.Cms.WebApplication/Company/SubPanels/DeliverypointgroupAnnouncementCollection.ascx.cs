﻿using System;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using System.Linq;
using DevExpress.Web;

namespace Obymobi.ObymobiCms.Company.SubPanels
{
    public partial class DeliverypointgroupAnnouncementCollection : Dionysos.Web.UI.DevExControls.SubPanelLLBLOverviewDataSourceCollection
    {
        #region Methods

        private void SetGui()
        {
            var companyAndGenericFilter = new PredicateExpression();
            companyAndGenericFilter.AddWithOr(AnnouncementFields.CompanyId == CmsSessionHelper.CurrentCompanyId);     

            var excludeUsedAnnouncementFilter = new PredicateExpression(AnnouncementFields.AnnouncementId != this.DataSourceAsDeliverypointgroupEntity.DeliverypointgroupAnnouncementCollection.Select(dpga => dpga.AnnouncementId).ToList());

            var filter = new PredicateExpression();
            filter.Add(companyAndGenericFilter);
            filter.AddWithAnd(excludeUsedAnnouncementFilter);            

            var sort = new SortExpression(new SortClause(AnnouncementFields.Title, SortOperator.Ascending));

            var announcements = new Obymobi.Data.CollectionClasses.AnnouncementCollection();
            announcements.GetMulti(filter, 0, sort, null, null, null, 0, 0);

            this.ddlAnnouncementId.DataSource = announcements;
            this.ddlAnnouncementId.DataBind();
        }

        private void AddDeliverypointgroupAnnouncement()
        {
            if (this.ddlAnnouncementId.ValidId > 0)
            {
                DeliverypointgroupAnnouncementEntity deliverypointgroupAnnouncement = new DeliverypointgroupAnnouncementEntity();
                deliverypointgroupAnnouncement.DeliverypointgroupId = this.ParentPrimaryKeyFieldValue;
                deliverypointgroupAnnouncement.AnnouncementId = this.ddlAnnouncementId.ValidId;
                    
                if (deliverypointgroupAnnouncement.Save())
                    this.DataSourceAsDeliverypointgroupEntity.DeliverypointgroupAnnouncementCollection.Add(deliverypointgroupAnnouncement);

                this.SetGui();
            }
        }

        #endregion

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            this.DataSourceLoaded += DeliverypointgroupAnnouncementCollection_DataSourceLoaded;          
            base.OnInit(e);
            this.EntityName = "DeliverypointgroupAnnouncement";            
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.btnAdd.Click += btnAdd_Click;
            this.MainGridView.HtmlDataCellPrepared += MainGridView_HtmlDataCellPrepared;
        }

        void MainGridView_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.DataColumn.FieldName == "AnnouncementTimeToShow")
            {
                ASPxGridView gridView = (ASPxGridView)sender;
                bool isRecurring = (bool)gridView.GetRowValues(e.VisibleIndex, "AnnouncementRecurring");
                if (isRecurring)
                {
                    e.Cell.Text = "-";
                }
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            this.AddDeliverypointgroupAnnouncement();
        }

        private void DeliverypointgroupAnnouncementCollection_DataSourceLoaded(object sender)
        {
            this.SetGui();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Return the page's datasource as a DeliverypointgroupEntity
        /// </summary>
        public DeliverypointgroupEntity DataSourceAsDeliverypointgroupEntity
        {
            get
            {
                return this.ParentDataSource as DeliverypointgroupEntity;
            }
        }

        #endregion
    }
}