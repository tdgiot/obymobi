﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using DevExpress.Web;
using Dionysos;
using Dionysos.Web.UI;
using Dionysos.Web.UI.WebControls;
using Obymobi;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;

public partial class Company_SubPanels_CompanyCurrencyCollection : SubPanelLLBLGenEntity
{
    #region Fields

    private readonly List<TextBoxDecimal> exchangeRateTextBoxes = new List<TextBoxDecimal>();
    private readonly List<TextBoxString> overwriteSymbolTextBoxes = new List<TextBoxString>();

    #endregion

    #region Methods

    protected override void OnInit(EventArgs e)
    {
        this.EntityName = "CompanyCurrency";
        this.LoadUserControls();
        base.OnInit(e);        
    }

    private void LoadUserControls()
    {
        this.lbCurrencies.DataSource = Currency.Mappings.Values.OrderBy(x => x.Name);
        this.lbCurrencies.DataBind();
    }

    protected override void OnParentDataSourceLoaded()
    {
        base.OnParentDataSourceLoaded();

        this.FillAllCurrencies();
    }    

    protected override void CreateChildControls()
    {
        base.CreateChildControls();
        
        this.RenderSelectedCurrencies();       
    }

    private void FillAllCurrencies()
    {
        List<string> currencyCodes = this.DataSourceAsCompanyEntity.CompanyCurrencyCollection.Select(x => x.CurrencyCode).ToList();
        foreach (ListEditItem item in this.lbCurrencies.Items)
        {
            if (currencyCodes.Contains(item.Value))
            {
                item.Selected = true;
            }
        }
    }

    private void RenderSelectedCurrencies()
    {
        this.exchangeRateTextBoxes.Clear();

        Table table = new Table();
        table.Width = Unit.Percentage(50);
        table.CssClass = "dataformV2";

        string defaultCurrency = CmsSessionHelper.DefaultCurrencyCodeForCompany;

        List<CompanyCurrency> currencies = CompanyCurrencyHelper.ConvertCompanyCurrencyCollectionToArray(this.DataSourceAsCompanyEntity.CompanyCurrencyCollection).ToList();
        foreach (CompanyCurrency companyCurrency in currencies.OrderBy(x => x.Name))
        {
            CompanyCurrencyEntity companyCurrencyEntity = this.DataSourceAsCompanyEntity.CompanyCurrencyCollection.SingleOrDefault(x => x.CurrencyCode.Equals(companyCurrency.CurrencyCode, StringComparison.InvariantCultureIgnoreCase));

            TableRow rowEchange = new TableRow();
            table.Rows.Add(rowEchange);

            TableCell tcExchangeRateLabel = new TableCell();
            tcExchangeRateLabel.CssClass = "label";
            rowEchange.Cells.Add(tcExchangeRateLabel);

            ASPxLabel lblExchangeRate = new ASPxLabel();
            if (!companyCurrency.CurrencyCode.IsNullOrWhiteSpace())
            {
                lblExchangeRate.Text = Currency.Mappings[companyCurrency.CurrencyCode].Name;
            }
            tcExchangeRateLabel.Controls.Add(lblExchangeRate);

            TableCell tcExchangeRateControl = new TableCell();
            tcExchangeRateControl.CssClass = "control";
            rowEchange.Cells.Add(tcExchangeRateControl);

            TextBoxDecimal tbExchangeRate = new TextBoxDecimal();
            tbExchangeRate.ID = string.Format("Currency-{0}", companyCurrency.CurrencyCode);
            tbExchangeRate.Text = companyCurrency.ExchangeRate.ToString("N5");
            tbExchangeRate.Enabled = !companyCurrency.CurrencyCode.Equals(defaultCurrency, StringComparison.InvariantCultureIgnoreCase);
            tbExchangeRate.IsRequired = true;
            tcExchangeRateControl.Controls.Add(tbExchangeRate);

            TableRow rowOverwriteSymbol = new TableRow();
            //table.Rows.Add(rowOverwriteSymbol);

            TableCell tcOverwriteSymbolLabel = new TableCell();
            tcOverwriteSymbolLabel.CssClass = "label";
            rowOverwriteSymbol.Cells.Add(tcOverwriteSymbolLabel);

            ASPxLabel lblOverwriteSymbol = new ASPxLabel();
            lblOverwriteSymbol.Text = "Overwrite symbol";
            tcOverwriteSymbolLabel.Controls.Add(lblOverwriteSymbol);

            TableCell tcOverwriteSymbolControl = new TableCell();
            tcOverwriteSymbolControl.CssClass = "control";
            rowOverwriteSymbol.Cells.Add(tcOverwriteSymbolControl);

            TextBoxString tbOverwriteSymbol = new TextBoxString();
            tbOverwriteSymbol.ID = string.Format("Symbol-{0}", companyCurrency.CurrencyCode);
            tbOverwriteSymbol.Text = companyCurrencyEntity != null ? companyCurrencyEntity.OverwriteSymbol : string.Empty;
            tcOverwriteSymbolControl.Controls.Add(tbOverwriteSymbol);

            this.exchangeRateTextBoxes.Add(tbExchangeRate);
            //this.overwriteSymbolTextBoxes.Add(tbOverwriteSymbol);

        }

        this.plhCurrencies.Controls.Add(table);
    }

    public override bool Save()
    {
        this.SaveCurrencyExchangeRates();
        //this.SaveCurrencyOverwriteSymbols();
        this.SaveCurrencyCodes(this.lbCurrencies.Items);

        return true;
    }

    private void SaveCurrencyCodes(ListEditItemCollection items)
    {
        string defaultCurrencyCode = this.DataSourceAsCompanyEntity.CurrencyCode;

        foreach (ListEditItem item in items)
        {
            string currencyCode = item.Value.ToString();

            CompanyCurrencyEntity currency = this.DataSourceAsCompanyEntity.CompanyCurrencyCollection.FirstOrDefault(x => x.CurrencyCode == currencyCode);
            if (item.Selected && currency == null)
            {
                currency = new CompanyCurrencyEntity();
                currency.CompanyId = CmsSessionHelper.CurrentCompanyId;
                currency.CurrencyCode = currencyCode;
                currency.Save();
            }
            else if (!item.Selected && currency != null && !currency.CurrencyCode.Equals(defaultCurrencyCode, StringComparison.InvariantCultureIgnoreCase))
            {
                currency.Delete();
                this.DataSourceAsCompanyEntity.CompanyCurrencyCollection.Remove(currency);
            }
        }
    }

    private void SaveCurrencyExchangeRates()
    {
        foreach (TextBoxDecimal tbCurrency in this.exchangeRateTextBoxes)
        {
            string currencyCode = tbCurrency.ID.GetAllAfterFirstOccurenceOf("Currency-");

            CompanyCurrencyEntity companyCurrency = this.DataSourceAsCompanyEntity.CompanyCurrencyCollection.SingleOrDefault(x => x.CurrencyCode == currencyCode);
            if (companyCurrency == null)
            {
                companyCurrency = new CompanyCurrencyEntity();
                companyCurrency.CurrencyCode = currencyCode;
                companyCurrency.CompanyId = CmsSessionHelper.CurrentCompanyId;
            }

            decimal rate;
            if (Decimal.TryParse(tbCurrency.Text, out rate))
                companyCurrency.ExchangeRate = rate;
            else
                companyCurrency.ExchangeRate = 1;
            
            companyCurrency.Save();
        }
    }

    private void SaveCurrencyOverwriteSymbols()
    {
        foreach (TextBoxString tbSymbol in this.overwriteSymbolTextBoxes)
        {
            string currencyCode = tbSymbol.ID.GetAllAfterFirstOccurenceOf("Symbol-");

            CompanyCurrencyEntity companyCurrency = this.DataSourceAsCompanyEntity.CompanyCurrencyCollection.SingleOrDefault(x => x.CurrencyCode == currencyCode);
            if (companyCurrency != null)
            {
                companyCurrency.OverwriteSymbol = tbSymbol.Text;
                companyCurrency.Save();
            }
        }
    }

    #endregion

    #region Properties

    public new PageLLBLGenEntity Page
    {
        get
        {
            return base.Page as PageLLBLGenEntity;
        }
    }

    public CompanyEntity DataSourceAsCompanyEntity
    {
        get
        {
            return this.Page.DataSource as CompanyEntity;
        }
    }    

    #endregion
}