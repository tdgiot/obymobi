﻿using System;
using Obymobi.Data.EntityClasses;
using Dionysos.Web.UI.DevExControls;
using System.Linq;

namespace Obymobi.ObymobiCms.Company.SubPanels
{
    public partial class RoutestepCollection : SubPanelLLBLOverviewDataSourceCollection
    {
        #region Methods

        #endregion

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "Routestep";
            base.OnInit(e);
            this.DataSourceLoaded += RoutestepColection_DataSourceLoaded;
        }

        private void RoutestepColection_DataSourceLoaded(object sender)
        {
            if (this.RouteOnParentDataSource.RoutestepCollection.Count == 1)
            {
                if (this.RouteOnParentDataSource.RoutestepCollection[0].RoutestephandlerCollection.Any(rs => rs.RoutestephandlerTypeAsEnum == Enums.RoutestephandlerType.EmailDocument))
                {
                    this.plhAddRoutestep.Visible = false;
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.btAddStep.Visible = !this.ParentDataSource.IsNew;
            this.btAddStep.Click += btAddStep_Click;
        }

        private void btAddStep_Click(object sender, EventArgs e)
        {
            if (this.ParentDataSource.Save())
            {
                int routeId = ((RouteEntity)this.ParentDataSource).RouteId;
                Obymobi.Logic.Routing.RoutingHelper.AddStepsToRoute(routeId, 1, this.ParentDataSource.GetCurrentTransaction());
                this.Response.Redirect(string.Format("~/Company/Route.aspx?id={0}&{1}=RoutestepCollection", routeId, PageControl.queryStringElementToActiveTab));
            }
        }

        #endregion

        #region Properties

        private RouteEntity RouteOnParentDataSource
        {
            get
            {
                return this.ParentDataSource as RouteEntity;
            }
        }

        #endregion        
    }
}
