﻿using System;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;

namespace Obymobi.ObymobiCms.Company.Subpanels
{
    public partial class UIFooterItemCollection : Dionysos.Web.UI.DevExControls.SubPanelLLBLOverviewDataSourceCollection
    {
        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "UIFooterItem";
            base.OnInit(e);
            this.MainGridView.LoadComplete += (MainGridView_LoadComplete);
            this.PageAsPageLLBLGenEntity.DataSourceLoaded += (PageAsPageLLBLGenEntity_DataSourceLoaded);
        }

        void PageAsPageLLBLGenEntity_DataSourceLoaded(object sender)
        {
            if (this.UIModeOnParentDataSource.IsNew)
                return;

            this.ddlUIFooterItemType.DataBindEnumStringValuesAsDataSource(typeof(FooterbarItemType));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.btAdd.Click += (btAdd_Click);
        }

        void btAdd_Click(object sender, EventArgs e)
        {
            if (this.ddlUIFooterItemType.ValidId <= 0)
            {
                this.PageAsPageLLBLGenEntity.AddInformator(Dionysos.Web.UI.InformatorType.Warning, this.PageAsPageLLBLGenEntity.Translate("ChooseAUIFooterItemTypeToAddAUIMode", "Choose a UIFooterItemType to add to UIMode"));
            }
            else
            {
                this.Response.Redirect(string.Format("~/Company/UIFooterItem.aspx?mode=add&UIModeId={0}&Type={1}", this.PageAsPageLLBLGenEntity.EntityId, this.ddlUIFooterItemType.ValidId));
            }
        }

        void MainGridView_LoadComplete(object sender, EventArgs e)
        {
            DevExpress.Web.GridViewDataColumn column = this.MainGridView.Columns["Position"] as DevExpress.Web.GridViewDataColumn;
            if (column != null)
            {
                this.MainGridView.SortBy(column, DevExpress.Data.ColumnSortOrder.Ascending);
            }

            column = this.MainGridView.Columns["SortOrder"] as DevExpress.Web.GridViewDataColumn;
            if (column != null)
            {
                this.MainGridView.SortBy(column, DevExpress.Data.ColumnSortOrder.Ascending);
            }
        }

        public UIModeEntity UIModeOnParentDataSource
        {
            get
            {
                return this.Parent.DataSource as UIModeEntity;
            }
        }
    }
}