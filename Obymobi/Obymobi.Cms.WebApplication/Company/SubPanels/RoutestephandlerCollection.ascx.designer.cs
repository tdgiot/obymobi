﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Obymobi.ObymobiCms.Company.SubPanels
{


    public partial class RoutestephandlerCollection
    {

        /// <summary>
        /// plhAddRoutestephandler control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Dionysos.Web.UI.WebControls.PlaceHolder plhAddRoutestephandler;

        /// <summary>
        /// ddlHandlerType control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Dionysos.Web.UI.DevExControls.ComboBoxInt ddlHandlerType;

        /// <summary>
        /// btAdd control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Dionysos.Web.UI.WebControls.Button btAdd;

        /// <summary>
        /// gvcsRoutestephandler control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Dionysos.Web.UI.DevExControls.GridViewColumnSelector gvcsRoutestephandler;
    }
}
