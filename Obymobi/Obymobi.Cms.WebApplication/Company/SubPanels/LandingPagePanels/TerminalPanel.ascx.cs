﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using DevExpress.Web;
using Dionysos;
using Dionysos.Web.UI.WebControls;
using Obymobi.Constants;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;

namespace Obymobi.ObymobiCms.Company.SubPanels.LandingPagePanels
{
    public partial class TerminalPanel : SubPanelEntity
    {
        #region Methods

        public override bool Validate()
        {
            return true;
        }

        private void HookUpEvents()
        {
            this.terminalGrid.HtmlDataCellPrepared += Grid_HtmlDataCellPrepared;
        }

        private void SetGui()
        {
            this.terminalGrid.Settings.ShowFilterRow = false;

            // Load data
            DataTable terminals = this.LoadTerminals();

            // Set datasource
            this.terminalGrid.DataSource = terminals;
            this.terminalGrid.DataBind();
        }

        private DataTable LoadTerminals()
        {
            var dataTable = new DataTable();

            // Create the columns
            var terminalId = new DataColumn("TerminalId");
            var name = new DataColumn("Name");
            var type = new DataColumn("Type");
            var lastRequest = new DataColumn("LastRequest");
            var localIp = new DataColumn("LocalIp");
            var externalIp = new DataColumn("ExternalIp");
            var version = new DataColumn("Version");
            var versionSupportTools = new DataColumn("VersionST");
            var versionAgent = new DataColumn("VersionAgent");
            var versionOS = new DataColumn("VersionOS");

            // Translate the columns
            terminalId.Caption = this.PageAsPageEntity.Translate("clmnTerminalId", "TerminalId");
            name.Caption = this.PageAsPageEntity.Translate("clmnName", "Naam");
            type.Caption = this.PageAsPageEntity.Translate("clmnType", "Type");
            lastRequest.Caption = this.PageAsPageEntity.Translate("clmnLastRequest", "Laatst online");
            localIp.Caption = this.PageAsPageEntity.Translate("clmnLocalIp", "Lokaal ip");
            externalIp.Caption = this.PageAsPageEntity.Translate("clmnExternalIp", "Extern ip");
            version.Caption = this.PageAsPageEntity.Translate("clmnVersion", "Console");
            versionSupportTools.Caption = this.PageAsPageEntity.Translate("clmnVersionSupportTools", "SupportTools");
            versionAgent.Caption = this.PageAsPageEntity.Translate("clmnVersionAgent", "Agent");
            versionOS.Caption = this.PageAsPageEntity.Translate("clmnVersionOS", "OS");

            // Add the columns
            dataTable.Columns.Add(terminalId);
            dataTable.Columns.Add(name);
            dataTable.Columns.Add(type);
            dataTable.Columns.Add(lastRequest);
            dataTable.Columns.Add(localIp);
            dataTable.Columns.Add(externalIp);
            dataTable.Columns.Add(version);
            dataTable.Columns.Add(versionSupportTools);
            dataTable.Columns.Add(versionAgent);
            dataTable.Columns.Add(versionOS);

            foreach (TerminalEntity terminalEntity in this.DataSourceTyped.TerminalCollection)
            {
                DataRow row = dataTable.NewRow();
                row["TerminalId"] = terminalEntity.TerminalId;
                row["Name"] = terminalEntity.Name;
                row["Type"] = terminalEntity.HandlingMethod.ToEnum<TerminalType>().ToString();
                row["LastRequest"] = (terminalEntity.DeviceEntity.LastRequestUTC.HasValue ? terminalEntity.DeviceEntity.LastRequestUTC.Value.UtcToLocalTime().ToString(CultureInfo.InvariantCulture) : "Unknown");
                row["LocalIp"] = terminalEntity.DeviceEntity.PrivateIpAddresses;
                row["ExternalIp"] = terminalEntity.DeviceEntity.PublicIpAddress;
                row["Version"] = terminalEntity.DeviceEntity.ApplicationVersion;
                row["VersionST"] = terminalEntity.DeviceEntity.SupportToolsVersion;
                row["VersionAgent"] = terminalEntity.DeviceEntity.AgentVersion;
                row["VersionOS"] = terminalEntity.DeviceEntity.OsVersion;

                VersionNumber companyVersionNumberOs = null;
                if (terminalEntity.DeviceEntity != null)
                {
                    var deviceModel = terminalEntity.DeviceEntity.DeviceModel.GetValueOrDefault(DeviceModel.Unknown);
                    if (!versionNumbersOS.TryGetValue(deviceModel, out companyVersionNumberOs))
                    {
                        var releaseEntity = ReleaseHelper.GetCompanyRelease(terminalEntity.DeviceEntity.DeviceModel.GetValueOrDefault(DeviceModel.Unknown), terminalEntity.CompanyId);
                        if (releaseEntity != null)
                        {
                            companyVersionNumberOs = releaseEntity.VersionAsVersionNumber;
                            versionNumbersOS.Add(deviceModel, companyVersionNumberOs);
                        }
                    }
                }
                row["VersionOS"] += "|" + (companyVersionNumberOs == null ? "-1" : companyVersionNumberOs.NumberAsString);

                dataTable.Rows.Add(row);
            }

            return dataTable;
        }

        private void RenderGridColors(ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.DataColumn.FieldName.Equals("Version")) // Emenu version
            {
                Obymobi.VersionNumber.VersionState state = VersionNumber.CompareVersions(e.CellValue.ToString(), this.DataSourceTyped.TerminalApplicationVersion);
                switch (state)
                {
                    case VersionNumber.VersionState.Older:
                        e.Cell.ForeColor = Color.Orange;
                        break;
                    case VersionNumber.VersionState.Newer:
                        e.Cell.ForeColor = Color.Blue;
                        break;
                }
            }
            else if (e.DataColumn.FieldName.Equals("VersionST")) // SupportTools version
            {
                Obymobi.VersionNumber.VersionState state = VersionNumber.CompareVersions(e.CellValue.ToString(), this.DataSourceTyped.SupportToolsVersion);
                switch (state)
                {
                    case VersionNumber.VersionState.Older:
                        e.Cell.ForeColor = Color.Orange;
                        break;
                    case VersionNumber.VersionState.Newer:
                        e.Cell.ForeColor = Color.Blue;
                        break;
                }
            }
            else if (e.DataColumn.FieldName.Equals("VersionAgent")) // Agent version
            {
                Obymobi.VersionNumber.VersionState state = VersionNumber.CompareVersions(e.CellValue.ToString(), this.DataSourceTyped.AgentVersion);
                switch (state)
                {
                    case VersionNumber.VersionState.Older:
                        e.Cell.ForeColor = Color.Orange;
                        break;
                    case VersionNumber.VersionState.Newer:
                        e.Cell.ForeColor = Color.Blue;
                        break;
                }
            }
            else if (e.DataColumn.FieldName.Equals("VersionOS")) // OS version
            {
                string cellValue = e.CellValue.ToString();
                string[] versionNumbers = cellValue.Split('|');
                e.Cell.Text = versionNumbers[0];

                if (versionNumbers[1].Equals("-1"))
                {
                    e.Cell.ForeColor = Color.Gray;
                }
                else
                {
                    VersionNumber.VersionState state = VersionNumber.CompareVersions(versionNumbers[0], versionNumbers[1]);
                    switch (state)
                    {
                        case VersionNumber.VersionState.Older:
                            e.Cell.ForeColor = Color.Orange;
                            break;
                        case VersionNumber.VersionState.Newer:
                            e.Cell.ForeColor = Color.Blue;
                            break;
                    }
                }
            }
            else if (e.DataColumn.FieldName.Equals("LastRequestUTC"))
            {
                string lastRequestStr = e.CellValue.ToString();                
                if (lastRequestStr.Equals("Unknown", StringComparison.InvariantCultureIgnoreCase))
                {
                    e.Cell.ForeColor = Color.Red;                    
                }
                else
                {
                    DateTime lastRequest;
                    if (DateTime.TryParseExact(lastRequestStr, "MM/dd/yyyy HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out lastRequest))
                    {
                        if (lastRequest < DateTime.UtcNow.AddMilliseconds(ObymobiIntervals.DEVICE_OFFLINE_THRESHOLD * -1))
                            e.Cell.ForeColor = Color.Red;
                    }                    
                }
            }
        }

        #endregion

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            this.DataSourceLoaded += Company_DataSourceLoaded;
        }

        private void Company_DataSourceLoaded(object sender)
        {
            this.SetGui();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.HookUpEvents();
        }

        private void Grid_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            this.RenderGridColors(e);
        }

        public override bool Save()
        {
            // Not relevant, all AJAX / direct save.
            return true;
        }

        public override bool InitializeEntity(bool pageInitComplete)
        {
            // We don't really have an entity
            return true;
        }

        #endregion

        #region Properties

        private CompanyEntity DataSourceTyped
        {
            get { return this.Parent.DataSource as CompanyEntity; }
        }

        readonly Dictionary<DeviceModel, VersionNumber> versionNumbersOS = new Dictionary<DeviceModel, VersionNumber>();

        #endregion        
    }
}