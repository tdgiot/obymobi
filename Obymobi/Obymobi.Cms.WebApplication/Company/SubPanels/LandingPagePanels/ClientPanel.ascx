﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.SubPanels.LandingPagePanels.ClientPanel" Codebehind="ClientPanel.ascx.cs" %>
<div>
    <table cellspacing="3">
        <tr>
            <td style="font-weight: bold; padding-right: 5px;">Legend:</td>
            <td style="color: black; padding-right: 5px;">&#9632; OK</td>
            <td style="color: blue; padding-right: 5px;">&#9632; Newer version</td>
            <td style="color: orange; padding-right: 5px;">&#9632; Outdated</td>
            <td style="color: red;">&#9632; Offline</td>
        </tr>
    </table>
                                 
    <X:GridView ID="clientGrid" ClientInstanceName="clientGrid" runat="server" Width="100%" KeyFieldName="ClientId">        
        <SettingsPager PageSize="10"></SettingsPager>
        <SettingsBehavior AllowGroup="false" AllowDragDrop="false" AutoFilterRowInputDelay="350" />
        <Columns>
            <dxwgv:GridViewDataHyperLinkColumn FieldName="ClientId" VisibleIndex="1" SortIndex="0" SortOrder="Ascending" Width="9%">
				<Settings AutoFilterCondition="Contains" />
                <PropertiesHyperLinkEdit NavigateUrlFormatString="~/Company/Client.aspx?id={0}" Target="_blank" TextField="ClientId"  />
            </dxwgv:GridViewDataHyperLinkColumn>
            <dxwgv:GridViewDataHyperLinkColumn FieldName="DeliverypointId" VisibleIndex="2" Width="12%">
				<Settings AutoFilterCondition="Contains" />
                <PropertiesHyperLinkEdit NavigateUrlFormatString="~/Company/Deliverypoint.aspx?id={0}" Target="_blank" TextField="DeliverypointNumber"  />
            </dxwgv:GridViewDataHyperLinkColumn>
            <dxwgv:GridViewDataHyperLinkColumn FieldName="DeliverypointgroupId" VisibleIndex="3" Width="10%">
				<Settings AutoFilterCondition="Contains" />
                <PropertiesHyperLinkEdit NavigateUrlFormatString="~/Company/Deliverypointgroup.aspx?id={0}" Target="_blank" TextField="DeliverypointgroupName"  />
            </dxwgv:GridViewDataHyperLinkColumn>
            <dxwgv:GridViewDataColumn FieldName="LastRequest" VisibleIndex="4" Width="13%">
				<Settings AutoFilterCondition="Contains" />
            </dxwgv:GridViewDataColumn>
            <dxwgv:GridViewDataColumn FieldName="LocalIp" VisibleIndex="5" Width="12%">
				<Settings AutoFilterCondition="Contains" />
            </dxwgv:GridViewDataColumn>
            <dxwgv:GridViewDataColumn FieldName="ExternalIp" VisibleIndex="6" Width="12%">
				<Settings AutoFilterCondition="Contains" />
            </dxwgv:GridViewDataColumn>
            <dxwgv:GridViewDataColumn FieldName="Version" VisibleIndex="7" Width="8%">
				<Settings AutoFilterCondition="Contains" />
            </dxwgv:GridViewDataColumn>
            <dxwgv:GridViewDataColumn FieldName="VersionST" VisibleIndex="8" Width="8%">
				<Settings AutoFilterCondition="Contains" />
            </dxwgv:GridViewDataColumn>
            <dxwgv:GridViewDataColumn FieldName="VersionAgent" VisibleIndex="9" Width="8%">
				<Settings AutoFilterCondition="Contains" />
            </dxwgv:GridViewDataColumn>
            <dxwgv:GridViewDataColumn FieldName="VersionOS" VisibleIndex="10" Width="8%">
				<Settings AutoFilterCondition="Contains" />
            </dxwgv:GridViewDataColumn>            
        </Columns>
        <Settings ShowFilterRow="True" />
    </X:GridView>
</div>