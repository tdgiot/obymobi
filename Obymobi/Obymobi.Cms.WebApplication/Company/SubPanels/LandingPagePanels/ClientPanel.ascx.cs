﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using DevExpress.Web;
using Dionysos;
using Dionysos.Web.UI.WebControls;
using Obymobi.Constants;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.ObymobiCms.Company.SubPanels.LandingPagePanels
{
    public partial class ClientPanel : SubPanelEntity
    {
        #region Methods

        public override bool Validate()
        {
            return true;
        }

        private void HookUpEvents()
        {
            this.clientGrid.HtmlDataCellPrepared += Grid_HtmlDataCellPrepared;
        }

        private void SetGui()
        {
            this.clientGrid.Settings.ShowFilterRow = false;

            // Load data
            DataTable clients = this.LoadClients();

            // Set datasource
            this.clientGrid.DataSource = clients;
            this.clientGrid.DataBind();
        }

        private DataTable LoadClients()
        {
            var dataTable = new DataTable();

            // Create the columns
            var clientIdColumn = new DataColumn("ClientId");
            var deliverypointNumber = new DataColumn("DeliverypointNumber");            
            var deliverypointId = new DataColumn("DeliverypointId");
            var deliverypointgroupName = new DataColumn("DeliverypointgroupName");
            var deliverypointgroupId = new DataColumn("DeliverypointgroupId");
            var lastRequest = new DataColumn("LastRequest");
            var localIp = new DataColumn("LocalIp");
            var externalIp = new DataColumn("ExternalIp");
            var version = new DataColumn("Version");
            var versionSupportTools = new DataColumn("VersionST");
            var versionAgent = new DataColumn("VersionAgent");
            var versionOS = new DataColumn("VersionOS");

            // Translate the columns
            clientIdColumn.Caption = this.PageAsPageEntity.Translate("clmnClientId", "ClientId");
            deliverypointNumber.Caption = this.PageAsPageEntity.Translate("clmnDeliverypointNumber", "Deliverypoint number");
            deliverypointId.Caption = this.PageAsPageEntity.Translate("clmnDeliverypointId", "Deliverypoint number");
            deliverypointgroupName.Caption = this.PageAsPageEntity.Translate("clmnDeliverypointgroupName", "DPG");
            deliverypointgroupId.Caption = this.PageAsPageEntity.Translate("clmnDeliverypointgroup2Id", "DPG");
            lastRequest.Caption = this.PageAsPageEntity.Translate("clmnLastRequest", "Last request");
            localIp.Caption = this.PageAsPageEntity.Translate("clmnLocalIp", "Local IP");
            externalIp.Caption = this.PageAsPageEntity.Translate("clmnExternalIp", "External IP");
            version.Caption = this.PageAsPageEntity.Translate("clmnVersionEmenu", "Emenu");
            versionSupportTools.Caption = this.PageAsPageEntity.Translate("clmnVersionSupportTools", "SupportTools");
            versionAgent.Caption = this.PageAsPageEntity.Translate("clmnVersionAgent", "Agent");
            versionOS.Caption = this.PageAsPageEntity.Translate("clmnVersionOS", "OS");

            // Add the columns
            dataTable.Columns.Add(clientIdColumn);
            dataTable.Columns.Add(deliverypointNumber);
            dataTable.Columns.Add(deliverypointId);
            dataTable.Columns.Add(deliverypointgroupName);
            dataTable.Columns.Add(deliverypointgroupId);            
            dataTable.Columns.Add(lastRequest);
            dataTable.Columns.Add(localIp);
            dataTable.Columns.Add(externalIp);
            dataTable.Columns.Add(version);
            dataTable.Columns.Add(versionSupportTools);
            dataTable.Columns.Add(versionAgent);
            dataTable.Columns.Add(versionOS);

            foreach (ClientEntity clientEntity in this.DataSourceTyped.ClientCollection)
            {
                DataRow row = dataTable.NewRow();
                row["ClientId"] = clientEntity.ClientId;

                if (clientEntity.LastDeliverypointId.HasValue)
                {
                    row["DeliverypointNumber"] = clientEntity.LastDeliverypointEntity.Number;
                    row["DeliverypointId"] = clientEntity.LastDeliverypointId.Value;
                    row["DeliverypointgroupName"] = clientEntity.LastDeliverypointEntity.DeliverypointgroupEntity.Name;
                    row["deliverypointgroupId"] = clientEntity.LastDeliverypointEntity.DeliverypointgroupId;
                }

                if (clientEntity.DeviceId.HasValue)
                {
                    row["LastRequest"] = (clientEntity.DeviceEntity.LastRequestUTC.HasValue ? clientEntity.DeviceEntity.LastRequestUTC.Value.UtcToLocalTime().ToString(CultureInfo.InvariantCulture) : "Unknown");
                    row["LocalIp"] = clientEntity.DeviceEntity.PrivateIpAddresses;
                    row["ExternalIp"] = clientEntity.DeviceEntity.PublicIpAddress;
                    row["Version"] = clientEntity.DeviceEntity.ApplicationVersion;
                    row["VersionST"] = clientEntity.DeviceEntity.SupportToolsVersion;
                    row["VersionAgent"] = clientEntity.DeviceEntity.AgentVersion;
                    row["VersionOS"] = clientEntity.DeviceEntity.OsVersion;
                }

                VersionNumber companyVersionNumberOs = null;
                if (clientEntity.DeviceEntity != null)
                {
                    var deviceModel = clientEntity.DeviceEntity.DeviceModel.GetValueOrDefault(DeviceModel.Unknown);
                    if (!versionNumbersOS.TryGetValue(deviceModel, out companyVersionNumberOs))
                    {
                        var releaseEntity = ReleaseHelper.GetCompanyRelease(clientEntity.DeviceEntity.DeviceModel.GetValueOrDefault(DeviceModel.Unknown), clientEntity.CompanyId);
                        if (releaseEntity != null)
                        {
                            companyVersionNumberOs = releaseEntity.VersionAsVersionNumber;
                            versionNumbersOS.Add(deviceModel, companyVersionNumberOs);
                        }
                    }
                }
                row["VersionOS"] += "|" + (companyVersionNumberOs == null ? "-1" : companyVersionNumberOs.NumberAsString);

                dataTable.Rows.Add(row);
            }

            return dataTable;
        }

        private void RenderGridColors(ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.DataColumn.FieldName.Equals("Version")) // Emenu version
            {
                Obymobi.VersionNumber.VersionState state = VersionNumber.CompareVersions(e.CellValue.ToString(), this.DataSourceTyped.ClientApplicationVersion);
                switch (state)
                {
                    case VersionNumber.VersionState.Older:
                        e.Cell.ForeColor = Color.Orange;
                        break;
                    case VersionNumber.VersionState.Newer:
                        e.Cell.ForeColor = Color.Blue;
                        break;
                }
            }
            else if (e.DataColumn.FieldName.Equals("VersionST")) // SupportTools version
            {
                Obymobi.VersionNumber.VersionState state = VersionNumber.CompareVersions(e.CellValue.ToString(), this.DataSourceTyped.SupportToolsVersion);
                switch (state)
                {
                    case VersionNumber.VersionState.Older:
                        e.Cell.ForeColor = Color.Orange;
                        break;
                    case VersionNumber.VersionState.Newer:
                        e.Cell.ForeColor = Color.Blue;
                        break;
                }
            }
            else if (e.DataColumn.FieldName.Equals("VersionAgent")) // Agent version
            {
                Obymobi.VersionNumber.VersionState state = VersionNumber.CompareVersions(e.CellValue.ToString(), this.DataSourceTyped.AgentVersion);
                switch (state)
                {
                    case VersionNumber.VersionState.Older:
                        e.Cell.ForeColor = Color.Orange;
                        break;
                    case VersionNumber.VersionState.Newer:
                        e.Cell.ForeColor = Color.Blue;
                        break;
                }
            }
            else if (e.DataColumn.FieldName.Equals("VersionOS")) // OS version
            {
                string cellValue = e.CellValue.ToString();
                string[] versionNumbers = cellValue.Split('|');
                e.Cell.Text = versionNumbers[0];

                if (versionNumbers[1].Equals("-1"))
                {
                    e.Cell.ForeColor = Color.Gray;
                }
                else
                {
                    VersionNumber.VersionState state = VersionNumber.CompareVersions(versionNumbers[0], versionNumbers[1]);
                    switch (state)
                    {
                        case VersionNumber.VersionState.Older:
                            e.Cell.ForeColor = Color.Orange;
                            break;
                        case VersionNumber.VersionState.Newer:
                            e.Cell.ForeColor = Color.Blue;
                            break;
                    }
                }
            }
            else if (e.DataColumn.FieldName.Equals("LastRequestUTC"))
            {
                string lastRequestStr = e.CellValue.ToString();
                if (lastRequestStr.Equals("Unknown", StringComparison.InvariantCultureIgnoreCase))
                {
                    e.Cell.ForeColor = Color.Red;
                }
                else
                {
                    DateTime lastRequest;
                    if (DateTime.TryParseExact(lastRequestStr, "MM/dd/yyyy HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out lastRequest))
                    {
                        if (lastRequest < DateTime.UtcNow.AddMilliseconds(ObymobiIntervals.DEVICE_OFFLINE_THRESHOLD * -1))
                            e.Cell.ForeColor = Color.Red;
                    }
                }
            }
        }

        #endregion

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            this.DataSourceLoaded += Company_DataSourceLoaded;
        }

        private void Company_DataSourceLoaded(object sender)
        {
            this.SetGui();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.HookUpEvents();
        }

        private void Grid_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            this.RenderGridColors(e);
        }

        public override bool Save()
        {
            // Not relevant, all AJAX / direct save.
            return true;
        }

        public override bool InitializeEntity(bool pageInitComplete)
        {
            // We don't really have an entity
            return true;
        }

        #endregion

        #region Properties

        private CompanyEntity DataSourceTyped
        {
            get { return this.Parent.DataSource as CompanyEntity; }
        }

        readonly Dictionary<DeviceModel, VersionNumber> versionNumbersOS = new Dictionary<DeviceModel, VersionNumber>();

        #endregion        
    }
}