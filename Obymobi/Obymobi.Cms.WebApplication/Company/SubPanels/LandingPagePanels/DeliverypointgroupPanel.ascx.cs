﻿using System;
using System.Data;
using Dionysos;
using Dionysos.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;

namespace Obymobi.ObymobiCms.Company.SubPanels.LandingPagePanels
{
    public partial class DeliverypointgroupPanel : SubPanelEntity
    {
        #region Constants

        private const string DEFAULT_PINCODE = "1258";
        private const string DEFAULT_PINCODE_SU = "1259";
        private const string DEFAULT_PINCODE_GM = "1260";

        #endregion

        #region Methods

        public override bool Validate()
        {
            return true;
        }

        private void HookUpEvents()
        {

        }

        private void SetGui()
        {
            this.deliverypointgroupGrid.Settings.ShowFilterRow = false;

            // Load data
            DataTable deliverypointgroups = this.LoadDeliverypointgroups();

            // Set datasource
            this.deliverypointgroupGrid.DataSource = deliverypointgroups;
            this.deliverypointgroupGrid.DataBind();
        }

        private DataTable LoadDeliverypointgroups()
        {
            var dataTable = new DataTable();

            // Create the columns
            var deliverypointgroupIdColumn = new DataColumn("DeliverypointgroupId");
            var name = new DataColumn("Name");
            var menu = new DataColumn("Menu");
            var menuId = new DataColumn("MenuId");
            var route = new DataColumn("Route");
            var routeId = new DataColumn("RouteId");
            var pincode = new DataColumn("Pincode");
            var pincodeSU = new DataColumn("PincodeSU");
            var pincodeGM = new DataColumn("PincodeGM");
            var active = new DataColumn("Active");

            // Translate the columns
            deliverypointgroupIdColumn.Caption = this.PageAsPageEntity.Translate("clmnDeliverypointgroupId", "DeliverypointgroupId");
            name.Caption = this.PageAsPageEntity.Translate("clmnName", "Naam");
            menu.Caption = this.PageAsPageEntity.Translate("clmnMenu", "Menu");
            menuId.Caption = this.PageAsPageEntity.Translate("clmnMenuId", "Menu");
            route.Caption = this.PageAsPageEntity.Translate("clmnRoute", "Route");
            routeId.Caption = this.PageAsPageEntity.Translate("clmRouteId", "Route");
            pincode.Caption = this.PageAsPageEntity.Translate("clmnPincode", "Pincode");
            pincodeSU.Caption = this.PageAsPageEntity.Translate("clmnPincodeSU", "Pincode superuser");
            pincodeGM.Caption = this.PageAsPageEntity.Translate("clmnPincodeGM", "Pincode godmode");
            active.Caption = this.PageAsPageEntity.Translate("clmnActive", "Actief");

            // Add the columns
            dataTable.Columns.Add(deliverypointgroupIdColumn);
            dataTable.Columns.Add(name);
            dataTable.Columns.Add(menu);
            dataTable.Columns.Add(menuId);
            dataTable.Columns.Add(route);
            dataTable.Columns.Add(routeId);
            dataTable.Columns.Add(pincode);
            dataTable.Columns.Add(pincodeSU);
            dataTable.Columns.Add(pincodeGM);
            dataTable.Columns.Add(active);

            foreach (DeliverypointgroupEntity deliverypointgroupEntity in this.DataSourceTyped.DeliverypointgroupCollection)
            {
                DataRow row = dataTable.NewRow();
                row["DeliverypointgroupId"] = deliverypointgroupEntity.DeliverypointgroupId;
                row["Name"] = deliverypointgroupEntity.Name;
                if (deliverypointgroupEntity.MenuId.HasValue)
                {
                    row["Menu"] = deliverypointgroupEntity.MenuEntity.Name;
                    row["MenuId"] = deliverypointgroupEntity.MenuId.Value;
                }
                if (deliverypointgroupEntity.RouteId.HasValue)
                {
                    row["Route"] = deliverypointgroupEntity.RouteEntity.Name;
                    row["RouteId"] = deliverypointgroupEntity.RouteId.Value;
                }                
                row["Pincode"] = deliverypointgroupEntity.HasPincode ? deliverypointgroupEntity.Pincode : DEFAULT_PINCODE;
                row["PincodeSU"] = deliverypointgroupEntity.HasPincodeSU ? deliverypointgroupEntity.PincodeSU : DEFAULT_PINCODE_SU;
                row["PincodeGM"] = deliverypointgroupEntity.HasPincodeGM ? deliverypointgroupEntity.PincodeGM : DEFAULT_PINCODE_GM;
                row["Active"] = deliverypointgroupEntity.Active;

                dataTable.Rows.Add(row);
            }

            return dataTable;
        }

        #endregion

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            this.DataSourceLoaded += Company_DataSourceLoaded;
        }

        private void Company_DataSourceLoaded(object sender)
        {
            this.SetGui();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.HookUpEvents();
        }        

        public override bool Save()
        {
            // Not relevant, all AJAX / direct save.
            return true;
        }

        public override bool InitializeEntity(bool pageInitComplete)
        {
            // We don't really have an entity
            return true;
        }

        #endregion

        #region Properties

        private CompanyEntity DataSourceTyped
        {
            get { return this.Parent.DataSource as CompanyEntity; }
        }

        #endregion
    }
}