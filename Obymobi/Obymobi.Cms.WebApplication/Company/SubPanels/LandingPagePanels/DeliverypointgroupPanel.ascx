﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.SubPanels.LandingPagePanels.DeliverypointgroupPanel" Codebehind="DeliverypointgroupPanel.ascx.cs" %>
<div>
    <X:GridView ID="deliverypointgroupGrid" ClientInstanceName="deliverypointgroupGrid" runat="server" Width="100%" KeyFieldName="DeliverypointgroupId">
        <SettingsPager PageSize="12"></SettingsPager>
        <SettingsBehavior AllowGroup="false" AllowDragDrop="false" AutoFilterRowInputDelay="1000" />
        <Columns>
            <dxwgv:GridViewDataHyperLinkColumn FieldName="DeliverypointgroupId" VisibleIndex="1" SortIndex="0" SortOrder="Ascending" Width="9%">
				<Settings AutoFilterCondition="Contains" />
                <PropertiesHyperLinkEdit NavigateUrlFormatString="~/Company/Deliverypointgroup.aspx?id={0}" Target="_blank" TextField="DeliverypointgroupId"  />
            </dxwgv:GridViewDataHyperLinkColumn>
            <dxwgv:GridViewDataColumn FieldName="Name" VisibleIndex="2" Width="19%">
				<Settings AutoFilterCondition="Contains" />
            </dxwgv:GridViewDataColumn>
            <dxwgv:GridViewDataHyperLinkColumn FieldName="MenuId" VisibleIndex="3" Width="18%">
				<Settings AutoFilterCondition="Contains" />
                <PropertiesHyperLinkEdit NavigateUrlFormatString="~/Catalog/Menu.aspx?id={0}" Target="_blank" TextField="Menu"  />
            </dxwgv:GridViewDataHyperLinkColumn>
            <dxwgv:GridViewDataHyperLinkColumn FieldName="RouteId" VisibleIndex="4" Width="18%">
				<Settings AutoFilterCondition="Contains" />
                <PropertiesHyperLinkEdit NavigateUrlFormatString="~/Company/Route.aspx?id={0}" Target="_blank" TextField="Route"  />
            </dxwgv:GridViewDataHyperLinkColumn>
            <dxwgv:GridViewDataColumn FieldName="Pincode" VisibleIndex="5" Width="9%">
				<Settings AutoFilterCondition="Contains" />
            </dxwgv:GridViewDataColumn>            
            <dxwgv:GridViewDataColumn FieldName="PincodeSU" VisibleIndex="6" Width="9%">
				<Settings AutoFilterCondition="Contains" />
            </dxwgv:GridViewDataColumn>
            <dxwgv:GridViewDataColumn FieldName="PincodeGM" VisibleIndex="7" Width="9%">
				<Settings AutoFilterCondition="Contains" />
            </dxwgv:GridViewDataColumn>
            <dxwgv:GridViewDataColumn FieldName="Active" VisibleIndex="8" Width="9%">
				<Settings AutoFilterCondition="Contains" />
            </dxwgv:GridViewDataColumn>            
        </Columns>
        <Settings ShowFilterRow="True" />
    </X:GridView>
</div>