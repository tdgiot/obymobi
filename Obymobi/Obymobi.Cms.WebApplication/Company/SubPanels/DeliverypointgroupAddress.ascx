﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.Subpanels.DeliverypointgroupAddress" Codebehind="DeliverypointgroupAddress.ascx.cs" %>
<div style="padding-bottom: 11px;">
							<table class="dataformV2">
								<tr>
									<td class="label">
										<D:LabelEntityFieldInfo runat="server" ID="lblAddressline1" LocalizeText="false">Address</D:LabelEntityFieldInfo>
									</td>
									<td class="control">
										<D:TextBoxString ID="tbAddress_Addressline1" runat="server" useDataBinding="false"></D:TextBoxString>
									</td>
								</tr>
								<tr>
									<td class="label">
										<D:LabelEntityFieldInfo runat="server" ID="lblZipcode" LocalizeText="false">Zipcode</D:LabelEntityFieldInfo>
									</td>
									<td class="control">
										<D:TextBoxString ID="tbAddress_Zipcode" runat="server" IsRequired="false" useDataBinding="false"></D:TextBoxString>
									</td>
								</tr>
								<tr>
									<td class="label">
										<D:LabelEntityFieldInfo runat="server" ID="lblCity" LocalizeText="false">City</D:LabelEntityFieldInfo>
									</td>
									<td class="control">
										<D:TextBoxString ID="tbAddress_City" runat="server" useDataBinding="false"></D:TextBoxString>
									</td>
								</tr>
								<tr>
									<td class="label">
										<D:LabelEntityFieldInfo runat="server" ID="LabelEntityFieldInfo1" LocalizeText="false">Latitude / longitude</D:LabelEntityFieldInfo>
									</td>
									<td class="control">
										<D:TextBoxDouble ID="tbAddress_Latitude" runat="server" ReadOnly="True" useDataBinding="false" Width="100px"></D:TextBoxDouble>
										&nbsp;<D:TextBoxDouble ID="tbAddress_Longitude" runat="server" ReadOnly="True" useDataBinding="false" Width="100px"></D:TextBoxDouble>&nbsp;<D:PlaceHolder ID="plhMap" runat="server"></D:PlaceHolder>
									</td>
									<td class="label">&nbsp;
									</td>
									<td class="control">&nbsp;
									</td>
								</tr>
							</table>

</div>

