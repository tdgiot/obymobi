﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.SubPanels.ReportProcessingTaskPanel" Codebehind="ReportProcessingTaskPanel.ascx.cs" %>
<div style="padding-bottom: 11px;">
    <D:HyperLink runat="server" ID="hlGenerateTransactionReports" NavigateUrl="~/Analytics/Transactions.aspx" LocalizeText="True">Transactie rapporten maken</D:HyperLink><br/>
    <D:HyperLink runat="server" ID="hlGeneratePageviewsReports" NavigateUrl="~/Analytics/PageviewsGoogleAnalytics.aspx" LocalizeText="True">Pageviews (Google Analytics) rapporten maken</D:HyperLink>
</div>
<X:GridViewColumnSelector runat="server" ID="gvcsReportProcessingTask">
	<Columns />
</X:GridViewColumnSelector>