﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Data.CollectionClasses;
using Dionysos.Interfaces;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Dionysos.Web.UI.WebControls;
using Dionysos.Diagnostics;
using Obymobi.Data.EntityClasses;
using Dionysos.Web.UI;
using DevExpress.Web.ASPxTreeList;
using System.Data;
using Dionysos.Web;

namespace Obymobi.ObymobiCms.Company
{
    public partial class SurveyAnswerPanel : System.Web.UI.UserControl, Dionysos.Interfaces.ISaveableControl
    {
        #region Methods

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

        // Drag-and-drop to reorder nodes
        protected void Answers_CustomCallback(object sender, TreeListCustomCallbackEventArgs e)
        {
            if (e.Argument.StartsWith("reorder:"))
            {
                string[] arg = e.Argument.Split(':');

                // Swap the SortOrder of the answer nodes
                SwapNodes(tlAnswers.FindNodeByKeyValue(arg[1]), tlAnswers.FindNodeByKeyValue(arg[2]));

                // Rerender the treelist
                this.RenderSurveyAnswers();
            }
        }

        private void SwapNodes(TreeListNode node1, TreeListNode node2)
        {
            if (node1 == null || node2 == null) return;

            SurveyAnswerEntity surveyAnswer1 = node1.DataItem as SurveyAnswerEntity;
            SurveyAnswerEntity surveyAnswer2 = node2.DataItem as SurveyAnswerEntity;

            if (surveyAnswer1 != null && surveyAnswer2 != null)
            {
                // Get the sort order of both answers
                int tmpSortOrder1 = surveyAnswer1.SortOrder;
                int tmpSortOrder2 = surveyAnswer2.SortOrder;

                // Switch the sort orders
                surveyAnswer1.SortOrder = tmpSortOrder2;
                surveyAnswer2.SortOrder = tmpSortOrder1;

                // Save both answers
                surveyAnswer1.Save();
                surveyAnswer2.Save();
            }
        }

        private void RenderSurveyAnswers()
        {
            PredicateExpression filter = new PredicateExpression(SurveyAnswerFields.SurveyQuestionId == this.DataSourceAsSurveyQuestionEntity.SurveyQuestionId);

            SortExpression sort = new SortExpression(new SortClause(SurveyAnswerFields.SortOrder, SortOperator.Ascending));

            SurveyAnswerCollection questions = new SurveyAnswerCollection();
            questions.GetMulti(filter, 0, sort);

            this.tlAnswers.DataSource = questions;
            this.tlAnswers.DataBind();
        }

        void HookUpEvents()
        {
            this.btAddAnswer.Click += new EventHandler(btAddAnswer_Click);
            this.btDeleteAnswer.Click += new EventHandler(btDeleteQuestion_Click);
        }

        #endregion

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            this.HookUpEvents();
        }

        void btAddAnswer_Click(object sender, EventArgs e)
        {
            string url = string.Format("~/Company/SurveyAnswer.aspx?entity=SurveyAnswerEntity&mode=add&SurveyQuestionId={0}", this.DataSourceAsSurveyQuestionEntity.SurveyQuestionId);
            WebShortcuts.ResponseRedirect(url, true);           
        }

        void btDeleteQuestion_Click(object sender, EventArgs e)
        {
            // Delete the selected answer

            // Create entityview
            EntityView<SurveyAnswerEntity> sa = this.DataSourceAsSurveyQuestionEntity.SurveyAnswerCollection.DefaultView;

            foreach (TreeListNode node in this.tlAnswers.Nodes)
            {
                if (node.Selected)
                { 
                    int surveyAnswerId = Int32.Parse(node.Key);

                    // Get the Survey answer entity
                    sa.Filter = new PredicateExpression(SurveyAnswerFields.SurveyAnswerId == surveyAnswerId);

                    if (sa.Count > 0)
                    {
                        sa[0].Delete();
                        sa.RelatedCollection.Remove(sa[0]);
                    }
                }
            }

            this.RenderSurveyAnswers();
        }

        public bool Save()
        {
            // Save
            

            return true;
        }

        protected override void CreateChildControls()
        {
            this.RenderSurveyAnswers();

            base.CreateChildControls();
        }

        #endregion

        #region Properties

        public new PageLLBLGenEntity Page
        {
            get
            {
                return base.Page as PageLLBLGenEntity;
            }
        }

        public SurveyQuestionEntity DataSourceAsSurveyQuestionEntity
        {
            get
            {
                return this.Page.DataSource as SurveyQuestionEntity;
            }
        }

        #endregion

    }
}