﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.Subpanels.RoomControlSectionCollection" Codebehind="RoomControlSectionCollection.ascx.cs" %>
<div style="overflow:hidden;margin-bottom:10px;">
	<div style="float:left;margin-right:10px">
        <X:ComboBoxInt ID="ddlRoomControlSectionType" runat="server" UseDataBinding="true" Width="150px" notdirty="true"></X:ComboBoxInt>
	</div>
    <div style="float:left;">
        <D:Button runat="server" ID="btAdd" CommandName="Add" Text="Add section" ToolTip="Add" />
    </div>
</div>
<X:GridViewColumnSelector runat="server" ID="gvcsRoomControlSections">
	<Columns />
</X:GridViewColumnSelector>

