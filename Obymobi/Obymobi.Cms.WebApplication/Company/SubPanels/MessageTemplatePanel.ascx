﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.SubPanels.MessageTemplatePanel" Codebehind="MessageTemplatePanel.ascx.cs" %>
<div style="overflow:hidden;margin-bottom:10px;">
   <div style="float:left;margin-right:10px;">
       <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlMessageTemplateId" UseDataBinding="false" EntityName="MessageTemplate" TextField="Name" PreventEntityCollectionInitialization="true" Width="250px" notdirty="true"/>
   </div>
   <div style="float:left;">
        <D:Button runat="server" ID="btnAdd" Text="Add message template" ToolTip="Add" />
   </div>
</div>
<X:GridViewColumnSelector runat="server" ID="gvcsMessageTemplates">
	<Columns />
</X:GridViewColumnSelector>