﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using Dionysos.Web.UI;

namespace Obymobi.ObymobiCms.Company.Subpanels
{
	public partial class SurveyQuestionEntityPanel : System.Web.UI.UserControl
	{
		#region Fields

		private SurveyQuestionEntity surveyQuestion = null;


		#endregion

		#region Methods

		void BindEntityValuesToControls()
		{
			this.tbQuestionName.Text = this.SurveyQuestion.Question;

			if (this.SurveyQuestion.SurveyAnswerCollection.Count > 0)
				this.tbAnswer1.Text = this.SurveyQuestion.SurveyAnswerCollection[0].Answer;

			if (this.SurveyQuestion.SurveyAnswerCollection.Count > 1)
				this.tbAnswer2.Text = this.SurveyQuestion.SurveyAnswerCollection[1].Answer;

			if (this.SurveyQuestion.SurveyAnswerCollection.Count > 2)
				this.tbAnswer3.Text = this.SurveyQuestion.SurveyAnswerCollection[2].Answer;

			if (this.SurveyQuestion.SurveyAnswerCollection.Count > 3)
				this.tbAnswer4.Text = this.SurveyQuestion.SurveyAnswerCollection[3].Answer;

			if (this.SurveyQuestion.SurveyAnswerCollection.Count > 4)
				this.tbAnswer5.Text = this.SurveyQuestion.SurveyAnswerCollection[4].Answer;
		}

		void BindControlsToEntityValues()
		{
			this.tbQuestionName.Text = this.SurveyQuestion.Question;

			// Once saved an answer can be removed, only renamed
			int answerIndex = 0;
			if (this.tbAnswer1.Text.Length > 0)
			{
				if (this.SurveyQuestion.SurveyAnswerCollection.Count <= answerIndex)
					this.SurveyQuestion.SurveyAnswerCollection.AddNew();

				this.SurveyQuestion.SurveyAnswerCollection[answerIndex].Answer = this.tbAnswer1.Text;
				answerIndex++;
			}

			if (this.tbAnswer2.Text.Length > 0)
			{
				if (this.SurveyQuestion.SurveyAnswerCollection.Count <= answerIndex)
					this.SurveyQuestion.SurveyAnswerCollection.AddNew();

				this.SurveyQuestion.SurveyAnswerCollection[answerIndex].Answer = this.tbAnswer2.Text;
				answerIndex++;
			}

			if (this.tbAnswer3.Text.Length > 0)
			{
				if (this.SurveyQuestion.SurveyAnswerCollection.Count <= answerIndex)
					this.SurveyQuestion.SurveyAnswerCollection.AddNew();

				this.SurveyQuestion.SurveyAnswerCollection[answerIndex].Answer = this.tbAnswer3.Text;
				answerIndex++;
			}

			if (this.tbAnswer4.Text.Length > 0)
			{
				if (this.SurveyQuestion.SurveyAnswerCollection.Count <= answerIndex)
					this.SurveyQuestion.SurveyAnswerCollection.AddNew();

				this.SurveyQuestion.SurveyAnswerCollection[answerIndex].Answer = this.tbAnswer4.Text;
				answerIndex++;
			}

			if (this.tbAnswer5.Text.Length > 0)
			{
				if (this.SurveyQuestion.SurveyAnswerCollection.Count <= answerIndex)
					this.SurveyQuestion.SurveyAnswerCollection.AddNew();

				this.SurveyQuestion.SurveyAnswerCollection[answerIndex].Answer = this.tbAnswer5.Text;
				answerIndex++;
			}
		}

		public void Save()
		{
			if (this.tbQuestionName.Text.Length > 0)
			{
				//this.SurveyQuestion.SurveyId = ((SurveyEntity)((PageLLBLGenEntity)this.Page).DataSource).SurveyId;
				this.SurveyQuestion.Question = this.tbQuestionName.Text;
				this.SurveyQuestion.Type = 0;
				this.BindControlsToEntityValues();
				this.SurveyQuestion.Save(true);
			}
		}

		#endregion

		#region Event Handlers

		protected void Page_Load(object sender, EventArgs e)
		{



		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the questionAnswer
		/// </summary>
		public SurveyQuestionEntity SurveyQuestion
		{
			get
			{
				return this.surveyQuestion;
			}
			set
			{
				this.surveyQuestion = value;
				this.BindEntityValuesToControls();
			}
		}

		/// <summary>
		/// Gets or sets the questionNumber
		/// </summary>
		public string QuestionName
		{
			set
			{
				this.lblQuestionName.Text = value;
			}
		}


		#endregion

	}
}