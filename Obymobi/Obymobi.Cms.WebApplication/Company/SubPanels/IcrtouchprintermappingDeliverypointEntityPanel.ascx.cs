﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using Dionysos.Web.UI;

namespace Obymobi.ObymobiCms.Company.Subpanels
{
    public partial class IcrtouchprintermappingDeliverypointEntityPanel : System.Web.UI.UserControl
	{
		#region Fields

        private IcrtouchprintermappingDeliverypointEntity icrtouchprintermappingDeliverypoint = null;


		#endregion

		#region Methods

		void BindEntityValuesToControls()
		{
            this.tbPrinter1.Value = this.IcrtouchprintermappingDeliverypoint.Printer1;
            this.tbPrinter2.Value = this.IcrtouchprintermappingDeliverypoint.Printer2;
            this.tbPrinter3.Value = this.IcrtouchprintermappingDeliverypoint.Printer3;
            this.tbPrinter4.Value = this.IcrtouchprintermappingDeliverypoint.Printer4;
            this.tbPrinter5.Value = this.IcrtouchprintermappingDeliverypoint.Printer5;
            this.tbPrinter6.Value = this.IcrtouchprintermappingDeliverypoint.Printer6;
            this.tbPrinter7.Value = this.IcrtouchprintermappingDeliverypoint.Printer7;
            this.tbPrinter8.Value = this.IcrtouchprintermappingDeliverypoint.Printer8;
            this.tbPrinter9.Value = this.IcrtouchprintermappingDeliverypoint.Printer9;
            this.tbPrinter10.Value = this.IcrtouchprintermappingDeliverypoint.Printer10;            
		}

		void BindControlsToEntityValues()
		{
            this.IcrtouchprintermappingDeliverypoint.Printer1 = this.tbPrinter1.Value;
            this.IcrtouchprintermappingDeliverypoint.Printer2 = this.tbPrinter2.Value;
            this.IcrtouchprintermappingDeliverypoint.Printer3 = this.tbPrinter3.Value;
            this.IcrtouchprintermappingDeliverypoint.Printer4 = this.tbPrinter4.Value;
            this.IcrtouchprintermappingDeliverypoint.Printer5 = this.tbPrinter5.Value;
            this.IcrtouchprintermappingDeliverypoint.Printer6 = this.tbPrinter6.Value;
            this.IcrtouchprintermappingDeliverypoint.Printer7 = this.tbPrinter7.Value;
            this.IcrtouchprintermappingDeliverypoint.Printer8 = this.tbPrinter8.Value;
            this.IcrtouchprintermappingDeliverypoint.Printer9 = this.tbPrinter9.Value;
            this.IcrtouchprintermappingDeliverypoint.Printer10 = this.tbPrinter10.Value;		
		}

		public void Save()
		{            
			this.IcrtouchprintermappingDeliverypoint.IcrtouchprintermappingId = ((IcrtouchprintermappingEntity)((PageLLBLGenEntity)this.Page).DataSource).IcrtouchprintermappingId;
            this.IcrtouchprintermappingDeliverypoint.DeliverypointId = this.DeliveryPointId;
			this.BindControlsToEntityValues();
            this.IcrtouchprintermappingDeliverypoint.Save(true);			
		}

		#endregion

		#region Event Handlers

		protected void Page_Load(object sender, EventArgs e)
		{
            this.tbPrinter1.Width = Unit.Pixel(20);
            this.tbPrinter2.Width = Unit.Pixel(20);
            this.tbPrinter3.Width = Unit.Pixel(20);
            this.tbPrinter4.Width = Unit.Pixel(20);
            this.tbPrinter5.Width = Unit.Pixel(20);
            this.tbPrinter6.Width = Unit.Pixel(20);
            this.tbPrinter7.Width = Unit.Pixel(20);
            this.tbPrinter8.Width = Unit.Pixel(20);
            this.tbPrinter9.Width = Unit.Pixel(20);
            this.tbPrinter10.Width = Unit.Pixel(20);
		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the questionAnswer
		/// </summary>
        public IcrtouchprintermappingDeliverypointEntity IcrtouchprintermappingDeliverypoint
		{
			get
			{
                return this.icrtouchprintermappingDeliverypoint;
			}
			set
			{
                this.icrtouchprintermappingDeliverypoint = value;
				this.BindEntityValuesToControls();
			}
		}

		/// <summary>
		/// Gets or sets the questionNumber
		/// </summary>
		public string DeliverypointName
		{
			set
			{
                this.lblDeliverypointNameAndNumber.Text = value;
			}
		}


        private int deliveryPointId = -1;


        /// <summary>
        /// Gets or sets the deliveryPointId
        /// </summary>
        public int DeliveryPointId
        {
            get
            {
                return this.deliveryPointId;
            }
            set
            {
                this.deliveryPointId = value;
            }
        }



		#endregion

	}
}