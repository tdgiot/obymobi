﻿using System;
using System.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using System.Linq;
using Dionysos;
using System.Collections.Generic;
using Dionysos.Web.UI.DevExControls;

namespace Obymobi.ObymobiCms.Company.SubPanels
{
    public partial class UIWidgetCultureCollection : Dionysos.Web.UI.WebControls.SubPanelLLBLGenEntity
    {
        #region Fields

        private readonly List<UIWidgetCulturePanel> panels = new List<UIWidgetCulturePanel>();

        #endregion

        #region Methods        
        
        protected void HookUpEvents()
        {
            
        }        

        private void SetGui()
        {
            if (this.UIWidget.UITabEntity.UIModeEntity.CompanyId.HasValue)
            { 
                // Company related
                this.RenderCompanyCultures();
            }
            else if (this.UIWidget.UITabEntity.UIModeEntity.PointOfInterestId.HasValue)
            {
                // PointOfInterest related
                this.RenderTabs(this.UIWidget.UITabEntity.UIModeEntity.PointOfInterestEntity.CustomTextCollection.Select(x => x.CultureCode).Distinct().ToList(), string.Empty);
            }
        }        

        private void RenderCompanyCultures()
        {
            this.RenderTabs(CmsSessionHelper.Cultures.Select(x => x.CultureCode).ToList(), CmsSessionHelper.DefaultCultureCodeForCompany);
        }        

        private void RenderTabs(List<string> cultures, string defaultCultureCode)
        {
            PageControl page = this.CreatePage();            
            this.plhCultures.Controls.Add(page);

            cultures = cultures.OrderBy(x => x).ToList();

            if (!defaultCultureCode.IsNullOrWhiteSpace() && cultures.Contains(defaultCultureCode, StringComparison.InvariantCultureIgnoreCase))
            {
                cultures.Remove(defaultCultureCode);
                cultures.Insert(0, defaultCultureCode);
            }

            foreach (string cultureCode in cultures)
            {
                bool isDefaultCulture = !defaultCultureCode.IsNullOrWhiteSpace() && cultureCode.Equals(defaultCultureCode, StringComparison.InvariantCultureIgnoreCase);

                UIWidgetCulturePanel panel = page.AddTabPage(cultureCode.ToUpperInvariant(), String.Format("CulturePanel_{0}", cultureCode), "~/Company/SubPanels/UIWidgetCulturePanel.ascx", false) as UIWidgetCulturePanel;
                panel.CultureCode = cultureCode;
                panel.IsDefaultCultureCode = (defaultCultureCode.Equals(cultureCode, StringComparison.InvariantCultureIgnoreCase));

                this.panels.Add(panel);
            }            
        }

        public override bool Save()
        {            
            return true;
        }        

        private PageControl CreatePage()
        {
            PageControl page = new PageControl();
            page.ID = "CulturePageControl";
            page.Width = Unit.Percentage(100);
            return page;
        }

        private TabPage CreateTab(string cultureCode, bool defaultTab)
        {
            TabPage tab = new TabPage();
            tab.Name = string.Format("tab-{0}-{1}", cultureCode, CmsSessionHelper.CurrentCompanyId);
            tab.Text = cultureCode.ToUpperInvariant();
            tab.LocalizeText = false;
            if (defaultTab)
            {
                tab.Text += " (default)";
            }
            return tab;
        }        

        public void Initialize()
        {
            this.SetGui();
            this.Initialized = true;
        }        

        #endregion

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {            
            base.OnInit(e);            
            this.EntityName = "CustomTextEntity";
        }        

        protected void Page_Load(object sender, EventArgs e)
        {
            this.HookUpEvents();
            if (!this.Initialized)
            {
                this.SetGui();
            }
        }

        #endregion		        

        #region Properties        

        private UIWidgetEntity UIWidget
        {
            get
            {
                return this.PageAsPageLLBLGenEntity.DataSource as UIWidgetEntity;
            }
        }

        private bool Initialized { get; set; }

        #endregion        
}
}

