﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using Obymobi.Data.EntityClasses;
using Dionysos.Web.UI;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.ObymobiCms.Company.Subpanels
{
    public partial class RoomControlSectionCollection : Dionysos.Web.UI.DevExControls.SubPanelLLBLOverviewDataSourceCollection
    {
        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "RoomControlSection";
            base.OnInit(e);
            this.MainGridView.LoadComplete += new EventHandler(MainGridView_LoadComplete);
            this.PageAsPageLLBLGenEntity.DataSourceLoaded += new Dionysos.Delegates.Web.DataSourceLoadedHandler(PageAsPageLLBLGenEntity_DataSourceLoaded);
        }

        void PageAsPageLLBLGenEntity_DataSourceLoaded(object sender)
        {
            foreach (RoomControlSectionType type in Enum.GetValues(typeof(RoomControlSectionType)))
            {
                this.ddlRoomControlSectionType.Items.Add(new DevExpress.Web.ListEditItem(type.ToString(), (int)type));
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.btAdd.Click += new EventHandler(btAdd_Click);

            if (this.MainGridView != null)
            {
                this.MainGridView.HtmlRowPrepared += MainGridView_HtmlRowPrepared;
            }
        }

        private void MainGridView_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType != GridViewRowType.Data) return;
            bool isVisible = (bool)e.GetValue("Visible");
            if (!isVisible)
                e.Row.ForeColor = Color.Gray;
        }

        void btAdd_Click(object sender, EventArgs e)
        {
            if (this.ddlRoomControlSectionType.ValidId <= 0)
            {
                this.PageAsPageLLBLGenEntity.AddInformator(Dionysos.Web.UI.InformatorType.Warning, this.PageAsPageLLBLGenEntity.Translate("ChooseARoomControlSectionTypeToAddARoomControlSection", "Choose a room control section type to add a room control section"));
            }
            else
            {
                this.Response.Redirect(string.Format("~/Company/RoomControlSection.aspx?mode=add&RoomControlAreaId={0}&Type={1}", this.PageAsPageLLBLGenEntity.EntityId, this.ddlRoomControlSectionType.ValidId));
            }
        }

        void MainGridView_LoadComplete(object sender, EventArgs e)
        {
            DevExpress.Web.GridViewDataColumn column = this.MainGridView.Columns["SortOrder"] as DevExpress.Web.GridViewDataColumn;
            if (column != null)
            {
                this.MainGridView.SortBy(column, DevExpress.Data.ColumnSortOrder.Ascending);
            }
        }
    }
}