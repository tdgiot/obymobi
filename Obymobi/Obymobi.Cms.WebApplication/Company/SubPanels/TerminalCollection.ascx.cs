﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
          
namespace Obymobi.ObymobiCms.Company.SubPanels
{
    public partial class TerminalCollection : Dionysos.Web.UI.DevExControls.SubPanelLLBLOverviewDataSourceCollection
    {        
        #region Fields        

        #endregion

        #region Methods

        protected override void OnInit(EventArgs e)
        {            
            base.OnInit(e);
			
            this.EntityName = "Terminal";
            this.EntityPageUrl = "~/Company/Terminal.aspx";            
        }        

        #endregion
      
    }
}