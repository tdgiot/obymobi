﻿using Obymobi.Data.EntityClasses;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Company.SubPanels
{
    public partial class SetupCodePanel : Dionysos.Web.UI.DevExControls.SubPanelLLBLOverviewDataSourceCollection
    {
        static readonly Random Random = new Random();

        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "SetupCode";
            
            SetGui();

            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.DataSource.SorterToUse = new SortExpression(SetupCodeFields.ExpireDateUTC | SortOperator.Descending);

            this.MainGridView.ShowEditColumn = false;
            this.MainGridView.ShowEditHyperlinkColumn = false;
            this.MainGridView.HtmlDataCellPrepared += MainGridView_HtmlDataCellPrepared;
        }

        private void MainGridView_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridViewTableDataCellEventArgs e)
        {
            var expireDateTime = (DateTime)e.GetValue("ExpireDateUTC");
            if (expireDateTime <= DateTime.UtcNow)
            {
                e.Cell.ForeColor = Color.Red;
            }

            if (e.DataColumn.Name.Equals("ExpireDateUTC"))
            {
                var convertedTime = CmsSessionHelper.ConvertDateTimeToUserTimeZone(expireDateTime);
                e.Cell.Text = convertedTime.ToString("yyyy-MM-dd HH:mm");
            }
        }

        protected override void OnParentDataSourceLoaded()
        {
            this.cbDeliverypointgroup.DataSource = ParentDataSourceAsCompanyEntity.DeliverypointgroupCollection;
            this.cbDeliverypointgroup.DataBind();
            this.cbDeliverypointgroup.SelectedIndex = 0;

            base.OnParentDataSourceLoaded();
        }

        private void SetGui()
        {
            DateTime localTimeNow = CmsSessionHelper.ConvertDateTimeToUserTimeZone(DateTime.UtcNow);
            this.deExpireAt.Value = localTimeNow;
            this.deExpireAt.MinDateTime = localTimeNow;

            this.btnGenerateSetupCode.Click += btnGenerateSetupCode_Click;
        }

        void btnGenerateSetupCode_Click(object sender, EventArgs e)
        {
            DeliverypointgroupEntity deliverypointgroupEntity = ParentDataSourceAsCompanyEntity.DeliverypointgroupCollection.FirstOrDefault(dpgEntity => dpgEntity.Name.Equals(this.cbDeliverypointgroup.SelectedValueString));
            if (deliverypointgroupEntity == null)
            {
                return;
            }

            DateTime expireUtc = TimeZoneInfo.ConvertTime(this.deExpireAt.Value.GetValueOrDefault(DateTime.MaxValue), TimeZoneInfo.Utc);

            // Create new setup code
            var setupCodeEntity = new SetupCodeEntity();
            setupCodeEntity.CompanyId = ParentDataSourceAsCompanyEntity.CompanyId;
            setupCodeEntity.DeliverypointgroupId = deliverypointgroupEntity.DeliverypointgroupId;
            setupCodeEntity.Code = GenerateRandomCode(ParentDataSourceAsCompanyEntity.CompanyId);
            setupCodeEntity.ExpireDateUTC = expireUtc;
            setupCodeEntity.Save();
        }

        private string GenerateRandomCode(int commpanyId)
        {
            string randomCode;

            PredicateExpression filter;
            SetupCodeCollection setupCodeCollection;

            do
            {
                randomCode = commpanyId.ToString();

                IEnumerable<int> output = Enumerable.Range(0, 9).OrderBy(x => Random.Next()).Take(8 - randomCode.Length);
                randomCode += String.Join("", output);

                setupCodeCollection = new SetupCodeCollection();
                filter = new PredicateExpression(SetupCodeFields.Code == randomCode);

            } while (setupCodeCollection.GetDbCount(filter) > 0);

            return randomCode;
        }

        private CompanyEntity ParentDataSourceAsCompanyEntity
        {
            get { return (CompanyEntity)this.Parent.DataSource; }
        }
    }
}