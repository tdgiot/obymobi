﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.Subpanels.UIWidgetAvailabilityCollection" Codebehind="UIWidgetAvailabilityCollection.ascx.cs" %>
<table class="dataformV2">
    <tr>
        <td class="label">
            <D:Label runat="server" ID="lblAvailability">Availability</D:Label>    
        </td>
        <td class="control">
            <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlAvailabilityId" IncrementalFilteringMode="StartsWith" EntityName="Availability" TextField="Name" ValueField="AvailabilityId" PreventEntityCollectionInitialization="true" notdirty="true" />    
        </td>
        <td>
            <D:Button runat="server" ID="btAddAvailability" CommandName="Add" Text="Add" ToolTip="Add" />    
            <a href="Availability.aspx?mode=add" target="_blank">Create</a>            
        </td>
    </tr>
</table>
<X:GridViewColumnSelector runat="server" ID="gvcsUIWidgetAvailability">
	<Columns />
</X:GridViewColumnSelector>