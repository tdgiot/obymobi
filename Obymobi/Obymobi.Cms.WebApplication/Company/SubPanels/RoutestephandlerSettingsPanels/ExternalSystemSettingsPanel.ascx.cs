﻿using System;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.QuerySpec;

namespace Obymobi.ObymobiCms.Company.Subpanels.RoutestephandlerSettingsPanels
{
    public partial class ExternalSystemSettingsPanel : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BindExternalSystemDropdown();
        }

        private void BindExternalSystemDropdown()
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ExternalSystemFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            filter.Add(ExternalSystemFields.Type == ExternalSystemType.Deliverect);

            SortExpression sort = new SortExpression(ExternalSystemFields.Name.Ascending());
            IncludeFieldsList includes = new IncludeFieldsList(ExternalSystemFields.ExternalSystemId, ExternalSystemFields.Name);

            ExternalSystemCollection collection = new ExternalSystemCollection();
            collection.GetMulti(filter, 0, sort, null, null, includes, 0, 0);

            ddlExternalSystemId.DisplayEmptyItem = false;
            ddlExternalSystemId.DataSource = collection;
            ddlExternalSystemId.DataBind();
        }
    }
}