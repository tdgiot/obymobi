﻿using Dionysos;
using System;
using System.Web.UI;
using Dionysos.Interfaces;
using Obymobi.Data.EntityClasses;

namespace Obymobi.ObymobiCms.Company.Subpanels.RoutestephandlerSettingsPanels
{
    public partial class EmailDocumentSettingsPanel : UserControl, ISaveableControl, IValidator
    {
        #region Methods

        private void SetGui()
        {
            
        }

        public void Validate()
        {

        }

        #endregion

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            ((Dionysos.Web.UI.PageDefault)this.Page).Validators.Add(this);            
        }

        public bool Save()
        {
            return this.RoutestephandlerEntity.Save();
        }

        #endregion

        #region Properties

        public bool IsValid
        {
            get
            {
                if (this.IsPostBack)
                    return (!this.tbEmail.Value.IsNullOrWhiteSpace() && this.tbEmail.IsValid);
                else
                    return true;
            }
            set
            {
                // void
            }
        }

        public string ErrorMessage
        {
            get
            {
                return string.Empty;
            }
            set
            {
                // void
            }
        }

        private RoutestephandlerEntity routestephandlerEntity = null;
        public RoutestephandlerEntity RoutestephandlerEntity
        {
            get
            {
                return this.routestephandlerEntity;
            }
            set
            {
                this.routestephandlerEntity = value;
                this.SetGui();
            }
        }

        #endregion
    }
}