﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmailOrderLegendPanel.ascx.cs" Inherits="Obymobi.Cms.WebApplication.Company.SubPanels.RoutestephandlerSettingsPanels.EmailOrderLegendPanel" %>

<style type="text/css">
    .label-legend {
        padding-top: 5px !important;
        font-weight: bold;
    }
</style>

<D:Panel ID="pnlTemplateLegend" GroupingText="Template Variables Legend" runat="server">
    <asp:Table ID="tblLegendOverview" CssClass="dataformV2" runat="server" Style="margin-top: 8px;">
    </asp:Table>
</D:Panel>

<D:PlaceHolder ID="phlPopupControls" runat="server">
</D:PlaceHolder>
