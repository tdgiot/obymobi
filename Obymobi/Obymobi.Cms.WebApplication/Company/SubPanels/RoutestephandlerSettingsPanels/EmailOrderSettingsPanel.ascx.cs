﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using Dionysos;
using Dionysos.Parsing;
using Obymobi.Data.EntityClasses;
using Dionysos.Interfaces;
using Obymobi.Cms.WebApplication.Company.SubPanels.RoutestephandlerSettingsPanels;

namespace Obymobi.ObymobiCms.Company.Subpanels.RoutestephandlerSettingsPanels
{
    public partial class EmailOrderSettingsPanel : UserControl, ISaveableControl, IValidator
    {
        #region Properties

        public bool IsValid
        {
            get => this.IsPostBack ? (!this.tbFieldValue1.Value.IsNullOrWhiteSpace() && this.tbFieldValue1.IsValid) || this.cbToCustomer.Checked : true;
            set { }
        }

        public string ErrorMessage
        {
            get => !this.IsValid && this.IsPostBack ? "Enter an emailaddress and/or choose for 'To customer'." : string.Empty;
            set { }
        }

        private RoutestephandlerEntity routestephandlerEntity = null;

        public RoutestephandlerEntity RoutestephandlerEntity
        {
            get => this.routestephandlerEntity;
            set
            {
                this.routestephandlerEntity = value;
                this.SetGui();
            }
        }

        #endregion

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            ((Dionysos.Web.UI.PageDefault)this.Page).Validators.Add(this);

            if (this.RoutestephandlerEntity.FieldValue4.Contains("<html", StringComparison.InvariantCultureIgnoreCase))
            {
                this.ValidateHtmlImageAvailability();
            }
        }

        public bool Save()
        {
            this.routestephandlerEntity.FieldValue5 = this.cbDontUseDefaultTemplate.Checked ? "1" : "0";
            this.routestephandlerEntity.FieldValue6 = this.cbToCustomer.Checked ? "1" : "0";

            return this.RoutestephandlerEntity.Save();
        }

        #endregion

        #region Methods

        private void SetGui()
        {
            this.cbDontUseDefaultTemplate.Checked = this.ToBool(this.RoutestephandlerEntity.FieldValue5);
            this.cbToCustomer.Checked = this.ToBool(this.RoutestephandlerEntity.FieldValue6);

            if (this.Page.LoadControl("~/Company/SubPanels/RoutestephandlerSettingsPanels/EmailOrderLegendPanel.ascx") is EmailOrderLegendPanel emailOrderLegendPanel)
            {
                this.phlEmailTemplateLedgend.Controls.Add(emailOrderLegendPanel);
            }
        }

        private bool ToBool(string value) => !value.IsNullOrWhiteSpace() && value.Equals("1");

        public void Validate()
        { }

        private void ValidateHtmlImageAvailability()
        {
            // Get the images            
            ParseHTMLHelper.ParseImgSrcToCid(this.RoutestephandlerEntity.FieldValue4, false, string.Empty, out List<string[]> resources);

            // For each found image validate we got the image.
            List<string> missingFiles = new List<string>();
            foreach (var resource in resources)
            {
                if (!this.RoutestephandlerEntity.MediaCollection.Any(x => x.Name.Equals(resource[1], StringComparison.InvariantCultureIgnoreCase)))
                {
                    missingFiles.Add(resource[1]);
                }
            }

            if (missingFiles.Count > 0)
            {
                string errorMessage = ((Dionysos.Web.UI.PageDefault)this.Page).Translate("EmailSettingsPanel.ErrorMissingImagesHtmlTemplate", "Er ontbreken afbeeldingen voor het HTML-sjabloon:");
                errorMessage += $" {StringUtil.CombineWithCommaSpace(missingFiles.ToArray())}";

                ((Dionysos.Web.UI.PageDefault)this.Page).AddInformator(Dionysos.Web.UI.InformatorType.Warning, errorMessage);
            }
        }

        #endregion
    }
}
