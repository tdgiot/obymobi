﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.Subpanels.RoutestephandlerSettingsPanels.EmailDocumentSettingsPanel" Codebehind="EmailDocumentSettingsPanel.ascx.cs" %>
<tr>
    <td class="label">
        <D:Label runat="server" id="lblEmail">E-mail</D:Label>
    </td>
    <td class="control">
        <D:TextBoxEmail runat="server" ID="tbEmail" IsRequired="true"></D:TextBoxEmail>
    </td>    
</tr>