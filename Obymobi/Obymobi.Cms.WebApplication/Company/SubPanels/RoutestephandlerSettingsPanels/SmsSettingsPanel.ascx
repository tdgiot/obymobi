﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.Subpanels.RoutestephandlerSettingsPanels.SmsSettingsPanel" Codebehind="SmsSettingsPanel.ascx.cs" %>

<table class="dataformV2">
<tr>
    <td class="label">
        <D:Label runat="server" ID="lblPhoneNumber">Phone number</D:Label>
    </td>
    <td class="control">
        <D:TextBox runat="server" ID="tbFieldValue1" IsRequired="True" />
        <br />
        <D:Label ID="lblSmsComment" runat="server"><small>Add multiple numbers separated with a comma. Example: +3154875482, +448756924</small></D:Label>
    </td>
	<td class="label">
        <D:Label runat="server" ID="lblOriginator">Originator</D:Label>
    </td>
    <td class="control">
        <D:TextBoxString runat="server" ID="tbFieldValue2" Text="Crave" IsRequired="True" MaxLength="11" />
        <br />
        <D:Label ID="lblOriginatorHelpText" runat="server"><small>Can only contain a maximum of 11 (alpha)numeric or whitespace separated characters.</small></D:Label>
    </td>
</tr>
<tr>
    <td class="label">
        <D:Label runat="server" id="lblCustomText">Custom Text</D:Label>
    </td>
    <td class="control" colspan="1">
        <D:TextBoxMultiLine runat="server" ID="tbFieldValue4" Rows="16"></D:TextBoxMultiLine>
    </td>
    <td class="label">

    </td>
    <td class="control">

    </td>
</tr>
</table>

<D:PlaceHolder ID="phlSmsTemplateLedgend" runat="server" />