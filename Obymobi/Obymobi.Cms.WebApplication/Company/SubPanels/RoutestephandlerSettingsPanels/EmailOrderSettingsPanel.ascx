﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.Subpanels.RoutestephandlerSettingsPanels.EmailOrderSettingsPanel" Codebehind="EmailOrderSettingsPanel.ascx.cs" %>

<table class="dataformV2">
    <tr>
        <td class="label">
            <D:Label runat="server" id="lblEmail">To E-mail</D:Label>
        </td>
        <td class="control">
            <D:TextBoxEmail runat="server" ID="tbFieldValue1"></D:TextBoxEmail>
        </td>
        <td class="label">
            <D:Label runat="server" id="Label1">To customer</D:Label>
        </td>
        <td class="control">
            <D:CheckBox runat="server" ID="cbToCustomer" /> (Does not work for AppLess)
        </td>
    </tr>
    <tr>
	    <td class="label">
            <D:Label runat="server" id="lblSender">From E-mail</D:Label>
        </td>
        <td class="control">
            <D:TextBoxEmail runat="server" ID="tbFieldValue2" IsRequired="True"></D:TextBoxEmail>
        </td>
    </tr>
    <tr>
        <td class="label">
            <D:Label runat="server" id="lblEmailTitle">E-mail Title</D:Label>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbFieldValue3" Text="Order from Crave"></D:TextBoxString>
        </td>
    </tr>
    <tr>
        <td class="label">
            <D:Label runat="server" id="lblCustomText">Custom Text</D:Label>
        </td>
        <td class="control" colspan="1">
            <D:TextBoxMultiLine runat="server" ID="tbFieldValue4" Rows="16"></D:TextBoxMultiLine>
        </td>

        <td class="label">
            <D:Label runat="server" id="lblDontUseDefaultTemplate">Sjabloon niet gebruiken</D:Label>
        </td>
        <td class="control">
            <D:CheckBox runat="server" ID="cbDontUseDefaultTemplate" />
        </td>
    </tr>
</table>

<D:PlaceHolder ID="phlEmailTemplateLedgend" runat="server" />
