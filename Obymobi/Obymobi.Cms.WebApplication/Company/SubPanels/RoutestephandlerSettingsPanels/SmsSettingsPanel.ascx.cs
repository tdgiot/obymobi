﻿using System;
using System.Text.RegularExpressions;
using System.Web.UI;
using Obymobi.Cms.WebApplication.Company.SubPanels.RoutestephandlerSettingsPanels;
using Obymobi.Data.EntityClasses;

namespace Obymobi.ObymobiCms.Company.Subpanels.RoutestephandlerSettingsPanels
{
    public partial class SmsSettingsPanel : System.Web.UI.UserControl, Dionysos.Interfaces.ISaveableControl, IValidator
    {
        private static string DefaultText = @"------------- REGULAR ORDERS --------------

New Order - #[[ORDERNUMBER]]
[[DELIVERYPOINTCAPTION]]: [[DELIVERYPOINTNUMBER]]

------------- APPLESS ORDERS --------------

New Order - #[[ORDERNUMBER]]
Service: [[SERVICEMETHOD]]
Checkout: [[CHECKOUTMETHOD]]
[[DELIVERYPOINTCAPTION]]: [[DELIVERYPOINTNUMBER]]
";

        protected void Page_Load(object sender, EventArgs e)
        {
            this.tbFieldValue2.MaxLength = 11;
        }

        private void SetGui()
        {
            if (this.Page.LoadControl("~/Company/SubPanels/RoutestephandlerSettingsPanels/EmailOrderLegendPanel.ascx") is EmailOrderLegendPanel emailOrderLegendPanel)
            {
                this.phlSmsTemplateLedgend.Controls.Add(emailOrderLegendPanel);
            }

            this.tbFieldValue4.Placeholder = DefaultText;
        }

        public bool Save()
        {
            // Save

            return true;
        }

        private void IsValidOriginator(string originator)
        {
            const int ORIGINATOR_ALPHANUMERIC_MAXLENGTH = 11;

            // https://developers.messagebird.com/docs/messaging
            // ORIGINATOR The sender of the message.This can be a telephone number (including country code) or an alphanumeric string.
            // In case of an alphanumeric string, the maximum length is 11 characters.
            // https://github.com/messagebird/csharp-rest-api/blob/master/MessageBird/Utilities/Validation.cs

            ErrorMessage = string.Empty;
            if (!string.IsNullOrEmpty(originator))
            {
                var numeric = new Regex("^\\+?[0-9]+$");
                var alphanumericWithWhitespace = new Regex("^[A-Za-z0-9]+(?:\\s[A-Za-z0-9]+)*$");
                var isNumeric = numeric.IsMatch(originator);
                var isAlphanumericWithWhitespace = alphanumericWithWhitespace.IsMatch(originator);

                if (!isNumeric && !isAlphanumericWithWhitespace)
                {
                    ErrorMessage = "Originator can only contain numeric or whitespace separated alphanumeric characters.";
                }
                else if (!isNumeric && isAlphanumericWithWhitespace && originator.Length > ORIGINATOR_ALPHANUMERIC_MAXLENGTH)
                {
                    ErrorMessage = $"Alphanumeric originator is limited to {ORIGINATOR_ALPHANUMERIC_MAXLENGTH} characters.";
                }
            }
        }

        private RoutestephandlerEntity routestephandlerEntity = null;
        public RoutestephandlerEntity RoutestephandlerEntity
        {
            get => this.routestephandlerEntity;
            set
            {
                this.routestephandlerEntity = value;
                this.SetGui();
            }
        }

        public void Validate()
        {
            IsValidOriginator(this.tbFieldValue2.Text);
            IsValid = ErrorMessage.Length == 0;
        }

        public bool IsValid { get; set; }

        public string ErrorMessage { get; set; }
    }
}