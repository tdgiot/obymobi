﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.Subpanels.RoutestephandlerSettingsPanels.ExternalSystemSettingsPanel" Codebehind="ExternalSystemSettingsPanel.ascx.cs" %>

<table class="dataformV2">
	<tr>
		<td class="label">
			<D:Label runat="server" id="lblExternalSystemId">External System</D:Label>
		</td>
		<td class="control">
			<X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlExternalSystemId" EntityName="ExternalSystem" IncrementalFilteringMode="StartsWith" TextField="Name" ValueField="ExternalSystemId" PreventEntityCollectionInitialization="true"></X:ComboBoxLLBLGenEntityCollection>
		</td>
		<td class="label"></td>
		<td class="control"></td>
	</tr>
</table>