﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.Subpanels.RoutestephandlerSettingsPanels.PosSettingsPanel" Codebehind="PosSettingsPanel.ascx.cs" %>
<table class="dataformV2">
    <tr>
        <td class="label">
            <D:Label runat="server" ID="lblPosValue1">WebserviceUrl</D:Label>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbPosValue1"></D:TextBoxString>
        </td>
        <td class="label">
            <D:Label runat="server" ID="lblPosValue2">EmployeeNumber</D:Label>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbPosValue2"></D:TextBoxString>
        </td>
    </tr>
    <tr>
        <td class="label">
            <D:Label runat="server" ID="lblPosValue3">EmployeePassword</D:Label>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbPosValue3"></D:TextBoxString>
        </td>
        <td class="label">
            <D:Label runat="server" ID="lblPosValue4">JobCodeId</D:Label>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbPosValue4"></D:TextBoxString>
        </td>
    </tr>
    <tr>
        <td class="label">
            <D:Label runat="server" ID="lblPosValue5">SessionToken</D:Label>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbPosValue5"></D:TextBoxString>
        </td>
        <td class="label">
            <D:Label runat="server" ID="lblPosValue6">RequestId</D:Label>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbPosValue6"></D:TextBoxString>
        </td>
    </tr>
    <tr>
        <td class="label">
            <D:Label runat="server" ID="lblPosValue7">Identifier</D:Label>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbPosValue7"></D:TextBoxString>
        </td>
        <td class="label">
            &nbsp;
        </td>
        <td class="control">
            &nbsp;
        </td>
    </tr>
</table>
