﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using Google.Apis.Manual.Util;
using Obymobi.Cms.Logic.Constants;
using Obymobi.Cms.Logic.Extensions;
using Obymobi.Cms.Logic.Models;
using Obymobi.Cms.Logic.ViewModels;

namespace Obymobi.Cms.WebApplication.Company.SubPanels.RoutestephandlerSettingsPanels
{
    public partial class EmailOrderLegendPanel : UserControl
    {
        private IReadOnlyCollection<Tooltip> tooltips;

        protected void Page_Load(object sender, EventArgs e)
        {
            this.RenderLegendTable();
            this.RenderTooltips();
        }

        private void RenderLegendTable()
        {
            var constants = typeof(EmailRouteLegendConstants).GetConstantsFromType();
            var templateVariables = constants.ConvertToTemplateVaribales().OrderBy(tv => tv.Name);

            tooltips = templateVariables.Where(templateVariable => templateVariable.Tooltip.IsNotNullOrEmpty())
                .Select(x => new Tooltip(x.Name, x.Tooltip))
                .ToList()
                .AsReadOnly();

            int templateVariablesLeftSideCount = (int)Math.Ceiling(templateVariables.Count() / (double)2);

            var templateVariablesLeftSide = templateVariables.Take(templateVariablesLeftSideCount);
            var templateVariablesRightSide = templateVariables.Skip(templateVariablesLeftSideCount).Take(templateVariables.Count() - templateVariablesLeftSideCount);

            for (int index = 0; index < templateVariablesLeftSideCount; index++)
            {
                TemplateVariable templateVariableLeftSide = templateVariablesLeftSide.Skip(index).Take(1).FirstOrDefault();
                TemplateVariable templateVariableRightSide = templateVariablesRightSide.Skip(index).Take(1).FirstOrDefault();

                TableRow tableRow = this.GenerateTableRow(templateVariableLeftSide, templateVariableRightSide);

                this.tblLegendOverview.Rows.Add(tableRow);
            };

            this.tblLegendOverview.DataBind();
        }

        private TableRow GenerateTableRow(TemplateVariable templateVariableLeftSide, TemplateVariable templateVariableRightSide)
        {
            TableRow tableRow = new TableRow();

            this.ProcessTemplateVariableIntoTableRow(tableRow, templateVariableLeftSide);
            this.ProcessTemplateVariableIntoTableRow(tableRow, templateVariableRightSide);

            return tableRow;
        }

        private void ProcessTemplateVariableIntoTableRow(TableRow tableRow, TemplateVariable templateVariable)
        {
            if (templateVariable == null)
            {
                this.AddEmptyTableColumn(tableRow);

                return;
            }

            tableRow.Cells.Add(new TableCell { Text = templateVariable.Name, CssClass = "label label-legend" });

            string text = templateVariable.Description;

            Tooltip tooltip = tooltips.FirstOrDefault(t => t?.Name == templateVariable.Name);
            if (tooltip != null)
            {
                text += $"<a id='{tooltip.InstanceName}' href='#'>";
                text += $"<img src='{ResolveUrl("~/Images/Icons/information.png")}' style='float: right;' alt='info' />";
                text += "</a>";
            }

            tableRow.Cells.Add(new TableCell { Text = text, CssClass = "control" });
        }

        private void AddEmptyTableColumn(TableRow tableRow)
        {
            tableRow.Cells.Add(new TableCell { CssClass = "label label-legend" });
            tableRow.Cells.Add(new TableCell { CssClass = "control" });
        }

        private void RenderTooltips()
        {
            foreach (Tooltip tooltip in tooltips)
            {
                this.phlPopupControls.Controls.Add(new ASPxPopupControl
                {
                    PopupAction = PopupAction.MouseOver,
                    CloseAction = CloseAction.MouseOut,
                    PopupElementID = tooltip.InstanceName,
                    Text = tooltip.Description,
                    ShowHeader = false,
                    Width = Unit.Pixel(450)
                });
            }
        }
    }
}
