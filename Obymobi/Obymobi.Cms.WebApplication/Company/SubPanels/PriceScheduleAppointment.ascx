﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Company_SubPanels_PriceScheduleAppointment" Codebehind="PriceScheduleAppointment.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.ASPxScheduler.v20.1" Namespace="DevExpress.Web.ASPxScheduler" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.XtraScheduler.v20.1.Core" Namespace="DevExpress.XtraScheduler" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxScheduler.v20.1" Namespace="DevExpress.Web.ASPxScheduler.Controls" TagPrefix="dx" %>
<table id="tbPriceScheduleAppointment" class="dataformV2">
    <tr>
		<td class="label">
		    <D:LabelEntityFieldInfo runat="server" id="lblPriceLevelId">Price level</D:LabelEntityFieldInfo>
	    </td>
	    <td class="control" colspan="3">
            <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlPriceLevelId" ClientIDMode="Static" CssClass="input" UseDataBinding="true" PreventEntityCollectionInitialization="True" EntityName="PriceLevel" TextField="Name" IsRequired="false">
                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" />                
            </X:ComboBoxLLBLGenEntityCollection>
	    </td>		
	</tr>					    
    <tr>
		<td class="label">
		    <D:LabelEntityFieldInfo runat="server" id="lblBackgroundColor">Background Color</D:LabelEntityFieldInfo>
	    </td>
	    <td class="control">
             <dxe:ASPxColorEdit runat="server" ID="ceBackgroundColor" UseDataBinding="false" EnableCustomColors="true" />
	    </td>
        <td class="label">
		    <D:LabelEntityFieldInfo runat="server" id="lblTextColor">Text Color</D:LabelEntityFieldInfo>
	    </td>
	    <td class="control">
             <dxe:ASPxColorEdit runat="server" ID="ceTextColor" UseDataBinding="false" EnableCustomColors="true" />
	    </td>
	</tr>
    <tr>
		<td class="label">
            <D:Label runat="server" id="lblStartTime">Start Time</D:Label>
        </td>
        <td class="control">
            <dxe:ASPxDateEdit ID="edtStartDate" ClientIDMode="Static" runat="server" EditFormat="DateTime" DateOnError="Undo" AllowNull="false" Date='<%# ((AppointmentFormTemplateContainer)Container).Start %>'>
                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" EnableCustomValidation="True" />
                <ClientSideEvents Validation="function(s, e) { OnStartTimeValidate(s, e); }" ValueChanged="function(s, e) { OnUpdateControlValue(s, e); }" />
            </dxe:ASPxDateEdit>
        </td>
        <td class="label">
            <D:Label runat="server" id="lblEndTime">End Time</D:Label>
        </td>
        <td class="control">
            <dxe:ASPxDateEdit ID="edtEndDate" ClientIDMode="Static" runat="server" EditFormat="DateTime" DateOnError="Undo" AllowNull="false" Date='<%# ((AppointmentFormTemplateContainer)Container).End %>'>
                <ValidationSettings EnableCustomValidation="True" ErrorDisplayMode="ImageWithTooltip" />
                <ClientSideEvents Validation="function(s, e) { OnEndTimeValidate(s, e); }" ValueChanged="function(s, e) { OnUpdateControlValue(s, e); }" />
            </dxe:ASPxDateEdit>
        </td>
    </tr>
    <tr>
        <td class="label">
	    </td>
	    <td class="control" colspan="3">
            <dx:AppointmentRecurrenceForm ID="AppointmentRecurrenceForm1" runat="server"
                IsRecurring='<%# ((AppointmentFormTemplateContainer)Container).Appointment.IsRecurring %>' 
                DayNumber='<%# ((AppointmentFormTemplateContainer)Container).RecurrenceDayNumber %>' 
                End='<%# ((AppointmentFormTemplateContainer)Container).RecurrenceEnd %>' 
                Month='<%# ((AppointmentFormTemplateContainer)Container).RecurrenceMonth %>' 
                OccurrenceCount='<%# ((AppointmentFormTemplateContainer)Container).RecurrenceOccurrenceCount %>' 
                Periodicity='<%# ((AppointmentFormTemplateContainer)Container).RecurrencePeriodicity %>' 
                RecurrenceRange='<%# ((AppointmentFormTemplateContainer)Container).RecurrenceRange %>' 
                Start='<%# ((AppointmentFormTemplateContainer)Container).RecurrenceStart %>' 
                WeekDays='<%# ((AppointmentFormTemplateContainer)Container).RecurrenceWeekDays %>' 
                WeekOfMonth='<%# ((AppointmentFormTemplateContainer)Container).RecurrenceWeekOfMonth %>' 
                RecurrenceType='<%# ((AppointmentFormTemplateContainer)Container).RecurrenceType %>'
                IsFormRecreated='<%# ((AppointmentFormTemplateContainer)Container).IsFormRecreated %>' >
            </dx:AppointmentRecurrenceForm>
        </td>
    </tr>
</tabl>
<table style="width: 100%; height: 35px;">
    <tr>
        <td style="width: 100%; height: 100%;" align="center">
            <table style="height: 100%;">
                <tr>
                    <td colspan="4">
                        <D:Label ID="lblError" ClientIDMode="Static" runat="server" ForeColor="Red" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <dxe:ASPxButton runat="server" ID="btnOk" Text="OK" UseSubmitBehavior="False" AutoPostBack="False" ClientInstanceName="btnOk" EnableViewState="False" Width="91px" />
                    </td>
                    <td style="padding: 0 4px;">
                        <dxe:ASPxButton runat="server" ID="btnCancel" Text="Cancel" UseSubmitBehavior="false" AutoPostBack="false" EnableViewState="false" Width="91px" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<table cellpadding="0" cellspacing="0" style="width: 100%;">
    <tr>
        <td style="width: 100%;" align="left">
            <dx:ASPxSchedulerStatusInfo runat="server" ID="schedulerStatusInfo" Priority="1" MasterControlId='<%# ((DevExpress.Web.ASPxScheduler.AppointmentFormTemplateContainer)Container).ControlId %>' />
        </td>
    </tr>
</table>
<script id="dxss_myAppoinmentForm007" type="text/javascript">
    // <![CDATA[
    function OnStartTimeValidate(s, e) {
        if (!e.isValid)
            return;
        var startDate = edtStartDate.GetDate();
        var endDate = edtEndDate.GetDate();
        e.isValid = startDate == null || endDate == null || startDate < endDate;
        e.errorText = "The Start Date must precede the End Date.";
    }
    function OnEndTimeValidate(s, e) {
        if (!e.isValid)
            return;
        var startDate = edtStartDate.GetDate();
        var endDate = edtEndDate.GetDate();
        e.isValid = startDate == null || endDate == null || startDate < endDate;
        e.errorText = "The Start Date must precede the End Date.";
    }    
    // ]]> 
</script>