﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Enums;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Routing;

namespace Obymobi.ObymobiCms.Company.SubPanels
{
    public partial class SurveyQuestionCollection : Dionysos.Web.UI.DevExControls.SubPanelLLBLOverviewDataSourceCollection
    {
        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "SurveyQuestion";
            base.OnInit(e);
            this.PageAsPageLLBLGenEntity.DataSourceLoaded += new Dionysos.Delegates.Web.DataSourceLoadedHandler(PageAsPageLLBLGenEntity_DataSourceLoaded);
        }

        void PageAsPageLLBLGenEntity_DataSourceLoaded(object sender)
        {
            this.ddlQuestionType.DataBindEnumStringValuesAsDataSource(typeof(SurveyQuestionType));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.btAdd.Click += new EventHandler(btAdd_Click);
        }

        void btAdd_Click(object sender, EventArgs e)
        {
            if (this.ddlQuestionType.ValidId <= 0)
            {
                this.PageAsPageLLBLGenEntity.AddInformator(Dionysos.Web.UI.InformatorType.Warning, this.PageAsPageLLBLGenEntity.Translate("ChooseASurveyQuestionTypeToAdd", "Kies een vraag type om deze toe te voegen"));
            }            
            else
            {
                this.Response.Redirect(string.Format("~/Company/SurveyQuestion.aspx?mode=add&SurveyPageId={0}&Type={1}",
                    this.PageAsPageLLBLGenEntity.EntityId, this.ddlQuestionType.ValidId));
            }
        }

        public SurveyPageEntity SurveyPageOnParentDataSource
        {
            get
            {
                return this.Parent.DataSource as SurveyPageEntity;
            }
        }
    }
}
