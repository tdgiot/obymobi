﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Company_SubPanels_CompanyCurrencyCollection" Codebehind="CompanyCurrencyCollection.ascx.cs" %>

<D:Panel ID="pnlCurrencies" runat="server" GroupingText="Currencies">
    <table class="dataformV2" style="width: 50%; float: left;">
        <tr>
            <td class="label">
	            <D:Label runat="server" ID="lblCurrenciesAll" LocalizeText="true">All</D:Label> 
            </td>
            <td class="control">
	            <X:ListBox runat="server" ID="lbCurrencies" SelectionMode="CheckColumn" TextField="Name" ValueField="Code"></X:ListBox>
            </td>            
        </tr>
    </table>    
    <D:PlaceHolder runat="server" ID="plhCurrencies">        
    </D:PlaceHolder>
</D:Panel>