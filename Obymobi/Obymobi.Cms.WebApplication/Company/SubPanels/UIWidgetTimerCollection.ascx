﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.Subpanels.UIWidgetTimerCollection" Codebehind="UIWidgetTimerCollection.ascx.cs" %>
<div style="overflow:hidden;margin-bottom:10px;">
	<div style="float:left;margin-right:10px">        
        <X:TimeEdit runat="server" ID="teCountToTime"></X:TimeEdit>        
	</div>
    <div style="float:left;margin-right:10px">
        <X:DateEdit runat="server" ID="deCountToDate" AllowNull="true"></X:DateEdit>
    </div>
    <div style="float:left;">
        <D:Button runat="server" ID="btAdd" CommandName="Add" Text="Add Timer" ToolTip="Add" />
    </div>
</div>
<X:GridViewColumnSelector runat="server" ID="gvcsUIWidgetTimers">
	<Columns />
</X:GridViewColumnSelector>