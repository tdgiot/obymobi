﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.SubPanels.DeliverypointgroupOccupancyPanel" Codebehind="DeliverypointgroupOccupancyPanel.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v20.1" Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dxwtl" %>
<script type="text/javascript">
    function OnInit(s, e) {
        ASPxClientUtils.AttachEventToElement(s.GetMainElement(), "keypress",
        function (evt) {
            console.log("" + evt.keyCode);
            switch (evt.keyCode) {
                //ENTER              
                case 13:
                    if (s.IsEditing()) {
                        s.UpdateEdit();
                        evt.stopPropagation();
                    }
                    break;

                    //ESC               
                case 27:
                    if (s.IsEditing()) {
                        s.CancelEdit();
                        evt.stopPropagation();
                    }
                    break;
            }
        });
    }
</script>
<div>                               
    <dxwtl:ASPxTreeList ID="tlOccupancy" runat="server">
        <ClientSideEvents Init="OnInit" />
    </dxwtl:ASPxTreeList>                                    
</div>