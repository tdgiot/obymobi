﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Company_SubPanels_ScheduleAppointmentTooltip" Codebehind="ScheduleAppointmentTooltip.ascx.cs" %>
<style type="text/css">
    .cToolTipClearFlow {
        clear:both;
        width:0;
        height:0;
        overflow:hidden;
        font-size:1pt;
    }
    .cToolTipContainer {
        width:300px;
        font-family:Tahoma;
        font-size:12pt;
    }
    .cToolTipTopLeftCorner {
        background: url(../Images/ToolTipImages/TopLeft.png) no-repeat left top;
        height:7px;
        width:7px; 
        overflow:hidden; 
        float:left;    
    }
    .cToolTipTopSide {
        background: #eff5d7 url(../Images/ToolTipImages/dot.gif) repeat-x left top;
        height:7px;
        overflow:hidden;
        width:286px; 
        float:left;
    }
    .cToolTipTopRightCorner {
        background: url(../Images/ToolTipImages/TopRight.png) no-repeat left top;
        height:7px;
        width:7px;
        overflow:hidden;
        float:left;
    }
    .cToolTipLeftSide {
        background: #eff5d7 url(../Images/ToolTipImages/dot.gif) repeat-y left;
        width:300px;
        overflow:hidden;
    }
    .cToolTipRightSide {
        background: url(../Images/ToolTipImages/dot.gif) repeat-y right;
        width:300px;
        overflow:hidden;
    }
    .cToolTipBottomLeftCorner {
        background: url(../Images/ToolTipImages/BottomLeft.png) no-repeat;
        height:7px;
        width:7px; 
        overflow:hidden; 
        float:left;
    }
    .cToolTipBottomSide {
        background: #e4eac8 url(../Images/ToolTipImages/dot.gif) repeat-x bottom;
        height:7px;
        overflow:hidden;
        width:286px; 
        float:left;
    }
    .cToolTipBottomSideSizeFixer {
        width:0px;
        height:7px;
        overflow:hidden;
    }
    .cToolTipBottomRightCorner {
        background: url(../Images/ToolTipImages/BottomRight.png) no-repeat;
        height:7px;
        width:7px;
        overflow:hidden;
        float:left;
    }
    .cToolTipFooter {
        background-color: #e4eac8 !important;
    }
    .cToolTipSeparatingStrip {
        background: #d7ddc1;
        overflow:hidden;
        height:1px;
    }
</style>
<div class="cToolTipContainer">
    <div class="cToolTipTopLeftCorner">
         </div>
    <div class="cToolTipTopSide">
         </div>
    <div class="cToolTipTopRightCorner">
         </div>
    <div class="cToolTipClearFlow">
         </div>
   <div class="cToolTipLeftSide">
        <div class="cToolTipRightSide">
            <div style="padding: 5px 15px 10px 15px;">
                <div style="padding-bottom: 15px;">
                    <div style="padding-bottom: 5px;">
                        <dxe:ASPxLabel runat="server" ID="lblInterval" EnableClientSideAPI="true" Font-Size="10pt"
                            Font-Names="Tahoma" ForeColor="black">
                        </dxe:ASPxLabel>
                    </div>
                    <div>
                        <dxe:ASPxLabel runat="server" ID="lblSubject" EnableClientSideAPI="true" Font-Size="10pt"
                            Font-Names="Tahoma" ForeColor="black">
                        </dxe:ASPxLabel>
                    </div>
                    <div>
                        <dxe:ASPxLabel runat="server" ID="lblLocation" EnableClientSideAPI="true" Font-Size="10pt"
                            Font-Names="Tahoma" ForeColor="black">
                        </dxe:ASPxLabel>                                                
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="cToolTipLeftSide cToolTipFooter">
        <div class="cToolTipRightSide">
            <div style="padding: 0 1px;">
                <div class="cToolTipSeparatingStrip">
                     </div>
            </div>
            <div style="padding: 13px 14px 12px 13px;">
                <div style="float: right; width: 50%;">
                    <div style="float: right;">
                        <dxe:ASPxButton runat="server" ID="btnDelete" Text="Delete" EnableClientSideAPI="true" AutoPostBack="false" Visible="false"></dxe:ASPxButton>
                    </div>
                    <div style="float: right; padding-right: 5px;">
                        <dxe:ASPxButton runat="server" ID="btnEdit" Text="Edit" EnableClientSideAPI="true" AutoPostBack="false" Visible="false"></dxe:ASPxButton>
                    </div>
                </div>
                <div runat="server" id="lblShowMenu" style="cursor: pointer; text-decoration: underline;
                    width: 100px; color: #3e6b96; font-family: Tahoma; font-size: 9pt; padding-top: 4px;">
                    Show menu</div>
            </div>
        </div>
    </div>
    <div class="cToolTipClearFlow">
         </div>
    <div class="cToolTipBottomLeftCorner">
         </div>
    <div class="cToolTipBottomSide">
        <div class="cToolTipBottomSideSizeFixer">
             </div>
    </div>
    <div class="cToolTipBottomRightCorner">
         </div>
    <div class="cToolTipClearFlow">
         </div>
</div>

<script type="text/javascript" id="dxss_">
    ASPxClientAppointmentToolTip = _aspxCreateClass(ASPxClientToolTipBase, {
        Initialize: function () {
            //this.controls.btnEdit.Click.AddHandler(_aspxCreateDelegate(this.OnBtnEditClick, this));
            //this.controls.btnDelete.Click.AddHandler(_aspxCreateDelegate(this.OnBtnDeleteClick, this));
            ASPxClientUtils.AttachEventToElement(this.controls.lblShowMenu, "click", _aspxCreateDelegate(this.OnLblShowMenuClick, this));
        },
        Update: function (data) {
            var apt = data.GetAppointment();
            this.apt = apt;
            
            var textInterval = this.ConvertIntervalToString(apt.interval);            
            this.controls.lblInterval.SetText(textInterval);
            this.controls.lblSubject.SetText(apt.cpSubject);
            this.controls.lblLocation.SetText(apt.cpLocation);
        },
        OnButtonDivClick: function (s, e) {
            this.ShowAppointmentMenu(s);
        },
        OnBtnEditClick: function () {
            if (this.apt.appointmentType == "Occurrence") {
                this.scheduler.RefreshClientAppointmentProperties(this.apt, AppointmentPropertyNames.Pattern, _aspxCreateDelegate(this.OnAppointmentEditSeriesRefresh, this));
            } else {
                this.scheduler.ShowAppointmentFormByClientId(this.apt.GetId());
            }
            this.Close();
        },
        OnAppointmentEditSeriesRefresh: function (apt) {
            if (apt.GetRecurrencePattern()) {
                this.scheduler.ShowAppointmentFormByClientId(apt.GetRecurrencePattern().GetId());
            }
        },
        OnBtnDeleteClick: function () {            
            if (this.apt.appointmentType == "Occurrence") {
                this.scheduler.RefreshClientAppointmentProperties(this.apt, AppointmentPropertyNames.Pattern, _aspxCreateDelegate(this.OnAppointmentDeleteSeriesRefresh, this));
            } else {
                this.scheduler.DeleteAppointment(this.apt);
            }
        },
        OnAppointmentDeleteSeriesRefresh: function (apt) {
            if (apt.GetRecurrencePattern()) {
                this.scheduler.DeleteAppointment(apt.GetRecurrencePattern());
            }
        },
        OnLblShowMenuClick: function (e) {
            this.ShowAppointmentMenu(e);
        }
    });
</script>