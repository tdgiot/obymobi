﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.Station" Codebehind="Station.aspx.cs" %>
<%@ Reference VirtualPath="~/Generic/UserControls/CustomTextCollection.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">    
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<controls>
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblName">Name</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
							</td>	  
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblChannel">Channel</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <D:TextBoxString ID="tbChannel" runat="server" IsRequired="true"></D:TextBoxString>
                            </td>
						</tr>
                        <tr>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblScene">Scene</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <D:TextBoxString ID="tbScene" runat="server"></D:TextBoxString>
                            </td>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblSuccessMessage">Success message</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <D:TextBoxString ID="tbSuccessMessage" runat="server"></D:TextBoxString>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                <D:Label runat="server" id="lblUrl">Url</D:Label>
                            </td>
                            <td class="control" colspan="3">
                                <D:TextBoxString ID="tbUrl" runat="server"></D:TextBoxString>
                                <small>Supported URLs: .mp3, .aac, .pls, .ram, .m3u, xxx:1232</small><br/>
                            </td>
                        </tr>
                         <tr>
                            <td class="label">
								<D:Label runat="server" id="lblDescription">Beschrijving</D:Label>
							    </td>
                            <td colspan="3" class="control">
								<D:TextBox ID="tbDescription" runat="server" Rows="2" TextMode="MultiLine" CssClass="inputNoHeight"></D:TextBox>                
							</td>
                        </tr>
                    </table>
                </controls>                    
            </X:TabPage>
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>