﻿using Dionysos;
using Dionysos.Web.UI;
using Obymobi.Cms.Logic.HelperClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Integrations.RoomControl.Control4;
using Obymobi.Logic.CraveService;
using Obymobi.ObymobiCms.Company.SubPanels;
using Obymobi.Security;
using Obymobi.Web.HelperClasses;
using System;
using System.IO;

namespace Obymobi.ObymobiCms.Company
{
    public partial class RoomControlConfiguration : Dionysos.Web.UI.PageLLBLGenEntity
    {
        #region Fields

        private RoomControlConfigurationPanel configurationPanel = null;

        #endregion

        #region Methods

        private void LoadUserControls()
        {
            this.configurationPanel = (RoomControlConfigurationPanel)this.LoadControl("~/Company/SubPanels/RoomControlConfigurationPanel.ascx");
            this.plhConfigurationPanel.Controls.Add(this.configurationPanel);

            if (CmsSessionHelper.CurrentRole >= Role.Crave)
            {
                cbCopyConfigurationCompanyId.DataBindEntityCollection<CompanyEntity>(null, CompanyFields.Name, CompanyFields.Name);
                cbCopyConfigurationCompanyId.Visible = true;
            }
        }

        private void SetGui()
        {
            if (this.configurationPanel != null)
            {
                this.configurationPanel.RefreshDataSource(this.DataSourceAsRoomControlConfigurationEntity);
            }

            this.btCopyConfiguration.Click += this.btCopyConfiguration_Click;
            this.btnImportControl4Project.Click += btnImportControl4Project_Click;
            this.btnVerifyControl4Project.Click += btnVerifyControl4Project_Click;
            this.btnPush.Click += BtnPush_Click;
        }

        private void BtnPush_Click(object sender, EventArgs e)
        {
            CompanyEntity company = this.DataSourceAsRoomControlConfigurationEntity.CompanyEntity;

            bool success = true;

            PublishHelper.Publish(this.DataSourceAsRoomControlConfigurationEntity.CompanyId, (service, timestamp, mac, salt) =>
                            {
                                try
                                {
                                    
                                    File.AppendAllText(Server.MapPath("~/App_Data/Push.txt"), string.Format("{0} - {1} - {2} - Start room control configuration: {3}\n", DateTime.Now, company.Name, CmsSessionHelper.CurrentUser.Username, this.DataSourceAsRoomControlConfigurationEntity.Name));

                                    string hash = Hasher.GetHashFromParameters(salt, timestamp, mac, this.DataSourceAsRoomControlConfigurationEntity.RoomControlConfigurationId);
                                    ObyTypedResultOfSimpleWebserviceResult result = service.PublishRoomControlConfiguration(timestamp, mac, this.DataSourceAsRoomControlConfigurationEntity.RoomControlConfigurationId, hash);
                                    if (result.ModelCollection.Length > 0 && result.ModelCollection[0].Succes)
                                    {
                                        // Success
                                    }
                                    else
                                    {
                                        success = false;
                                    }

                                    File.AppendAllText(Server.MapPath("~/App_Data/Push.txt"), string.Format("{0} - {1} - {2} - End room control configuration: {3}\n", DateTime.Now, company.Name, CmsSessionHelper.CurrentUser.Username, this.DataSourceAsRoomControlConfigurationEntity.Name));
                                }
                                catch (Exception ex)
                                {
                                    File.AppendAllText(Server.MapPath("~/App_Data/Push.txt"), string.Format("{0} - {1} - {2} - Exception: {3}\n", DateTime.Now, company.Name, CmsSessionHelper.CurrentUser.Username, ex.ToString()));
                                }
                            });

            if (success)
            {
                this.AddInformator(InformatorType.Information, "Configuration has been pushed successfully.");
            }
            else
            {
                this.AddInformator(InformatorType.Warning, "Failed to push the configuration to one or more api's.");
            }

            this.Validate();
        }

        public override bool Save()
        {
            if (this.PageMode == Dionysos.Web.PageMode.Add)
            {
                this.DataSourceAsRoomControlConfigurationEntity.CompanyId = CmsSessionHelper.CurrentCompanyId;
            }
            return base.Save();
        }

        #endregion

		#region Properties

		
		public RoomControlConfigurationEntity DataSourceAsRoomControlConfigurationEntity
		{
			get
			{
                return this.DataSource as RoomControlConfigurationEntity;
			}
		}

		#endregion

		#region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            this.DataSourceLoaded += this.RoomControlConfiguration_DataSourceLoaded;
            base.OnInit(e);
        }

		private void RoomControlConfiguration_DataSourceLoaded(object sender)
		{
            this.SetGui();
		}

        private void btCopyConfiguration_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(tbCopyConfigurationName.Text))
                {
                    MultiValidatorDefault.AddError(Translate("Error.NameRequiredToCopyConfiguration", "Please enter a name for the new copy."));
                }
                else
                {
                    RoomControlConfigurationCopier copier = new RoomControlConfigurationCopier(
                        DataSourceAsRoomControlConfigurationEntity.RoomControlConfigurationId, 
                        tbCopyConfigurationName.Text,
                        cbCopyConfigurationCompanyId.Value);

                    RoomControlConfigurationEntity configuration = copier.Copy();

                    AddInformatorInfo(Translate("ConfigurationCopied", "The configuration copy has been made: ") + configuration.Name);
                }
            }
            catch (Exception ex)
            {
                MultiValidatorDefault.AddError(Translate("Error.ErrorDuringConfigurationCopy", "Er is een fout opgetreden tijdens het kopieëren van de configuratie: ") + ex.Message);
            }

            Validate();
        }

        private bool ValidControl4Project
        {
            get
            {
                string fileName = this.fuControl4Project.FileName;
                if (!fileName.EndsWith(".c4p", StringComparison.InvariantCultureIgnoreCase))
                {
                    this.MultiValidatorDefault.AddError(this.Translate("Error.InvalidC4ProjectFile", "Not a valid Control4 project file (.c4p)"));
                    this.Validate();
                    return false;
                }
                return true;
            }
        }

        void btnImportControl4Project_Click(object sender, EventArgs e)
        {
            if(!this.ValidControl4Project)
                return;

            Control4Configuration configuration = new Control4Configuration(new MemoryStream(this.fuControl4Project.FileBytes));

            foreach (Control4Configuration.Room room in configuration.Rooms)
            {
                RoomControlAreaEntity roomEntity = new RoomControlAreaEntity();
                roomEntity.Name = room.Name;
                roomEntity.NameSystem = room.Name;

                if (room.Lights.Count > 0)
                {
                    RoomControlSectionEntity lights = new RoomControlSectionEntity();
                    lights.Name = "Lighting";
                    lights.Type = Enums.RoomControlSectionType.Lighting;
                    roomEntity.RoomControlSectionCollection.Add(lights);

                    foreach (Control4Configuration.Device light in room.Lights)
                    {
                        RoomControlComponentEntity lightEntity = new RoomControlComponentEntity();
                        lightEntity.Type = Enums.RoomControlComponentType.Light;
                        lightEntity.Name = light.Name;
                        lightEntity.NameSystem = light.Name;
                        lights.RoomControlComponentCollection.Add(lightEntity);
                    }

                    if (configuration.Scenes.Count > 0)
                    {
                        foreach (Control4Configuration.Device scene in configuration.Scenes)
                        {
                            RoomControlComponentEntity sceneEntity = new RoomControlComponentEntity();
                            sceneEntity.Type = Enums.RoomControlComponentType.LightingScene;
                            sceneEntity.Name = scene.Name;
                            sceneEntity.NameSystem = scene.Name;
                            lights.RoomControlComponentCollection.Add(sceneEntity);
                        }
                    }
                }

                if (room.Thermostats.Count > 0)
                {
                    RoomControlSectionEntity thermostats = new RoomControlSectionEntity();
                    thermostats.Name = "Comfort";
                    thermostats.Type = Enums.RoomControlSectionType.Comfort;
                    roomEntity.RoomControlSectionCollection.Add(thermostats);

                    foreach (Control4Configuration.Device thermostat in room.Thermostats)
                    {
                        RoomControlComponentEntity thermostatEntity = new RoomControlComponentEntity();
                        thermostatEntity.Type = Enums.RoomControlComponentType.Thermostat;
                        thermostatEntity.Name = thermostat.Name;
                        thermostatEntity.NameSystem = thermostat.Name;
                        thermostats.RoomControlComponentCollection.Add(thermostatEntity);
                    }
                }

                if (room.Blinds.Count > 0)
                {
                    RoomControlSectionEntity blinds = new RoomControlSectionEntity();
                    blinds.Name = "Blinds";
                    blinds.Type = Enums.RoomControlSectionType.Blinds;
                    roomEntity.RoomControlSectionCollection.Add(blinds);

                    foreach (Control4Configuration.Device blind in room.Blinds)
                    {
                        RoomControlComponentEntity blindEntity = new RoomControlComponentEntity();
                        blindEntity.Type = Enums.RoomControlComponentType.Blind;
                        blindEntity.Name = blind.Name;
                        blindEntity.NameSystem = blind.Name;
                        blinds.RoomControlComponentCollection.Add(blindEntity);
                    }
                }

                if (configuration.Services.Count > 0)
                {
                    foreach (Control4Configuration.Device service in configuration.Services)
                    {
                        RoomControlSectionEntity serviceEntity = new RoomControlSectionEntity();
                        serviceEntity.Type = Enums.RoomControlSectionType.Service;
                        serviceEntity.Name = service.Name;
                        serviceEntity.NameSystem = service.Name;
                        roomEntity.RoomControlSectionCollection.Add(serviceEntity);
                    }
                }

                this.DataSourceAsRoomControlConfigurationEntity.RoomControlAreaCollection.Add(roomEntity);
            }
            this.DataSourceAsRoomControlConfigurationEntity.Save(true);

            this.tabsMain.ActiveTabIndex = 0;
            this.configurationPanel.RefreshDataSource(this.DataSourceAsRoomControlConfigurationEntity);
        }

        void btnVerifyControl4Project_Click(object sender, EventArgs e)
        {
            if (!this.ValidControl4Project)
                return;

            Control4Configuration configuration = new Control4Configuration(new MemoryStream(this.fuControl4Project.FileBytes));
            RoomControlConfigurationEntity configurationEntity = this.DataSourceAsRoomControlConfigurationEntity;

            string errors = string.Empty;

            // Check if any room is missing
            foreach(Control4Configuration.Room room in configuration.Rooms)
            {
                bool roomFound = false;
                foreach (RoomControlAreaEntity roomEntity in configurationEntity.RoomControlAreaCollection)
                {
                    if (roomEntity.NameSystem.Equals(room.Name, StringComparison.InvariantCultureIgnoreCase))
                    {
                        roomFound = true;

                        bool lightingSectionFound = false;
                        bool blindSectionFound = false;
                        bool comfortSectionFound = false;
                        foreach (RoomControlSectionEntity sectionEntity in roomEntity.RoomControlSectionCollection)
                        {
                            if (sectionEntity.Type == Enums.RoomControlSectionType.Lighting)
                            {
                                lightingSectionFound = true;

                                // Check if any light is missing
                                foreach(Control4Configuration.Device light in room.Lights)
                                {
                                    bool lightFound = false;
                                    foreach (RoomControlComponentEntity lightEntity in sectionEntity.RoomControlComponentCollection)
                                    {
                                        if (lightEntity.NameSystem.Equals(light.Name, StringComparison.InvariantCultureIgnoreCase))
                                        {
                                            lightFound = true;
                                            break;
                                        }
                                    }
                                    if (!lightFound)
                                    {
                                        errors += string.Format("Light \"{0}\" in Area \"{1}\" exists in the Control4 project, but not in the configuration.\n", light.Name, room.Name);
                                    }
                                }

                                // Check if any scene is missing
                                foreach (Control4Configuration.Device scene in configuration.Scenes)
                                {
                                    bool sceneFound = false;
                                    foreach (RoomControlComponentEntity sceneEntity in sectionEntity.RoomControlComponentCollection)
                                    {
                                        if (sceneEntity.NameSystem.Equals(scene.Name, StringComparison.InvariantCultureIgnoreCase))
                                        {
                                            sceneFound = true;
                                            break;
                                        }
                                    }
                                    if (!sceneFound)
                                    {
                                        errors += string.Format("Scene \"{0}\" in Area \"{1}\" exists in the Control4 project, but not in the configuration.\n", scene.Name, room.Name);
                                    }
                                }
                            }
                            // Check if any blind is missing
                            if (sectionEntity.Type == Enums.RoomControlSectionType.Blinds)
                            {
                                blindSectionFound = true;

                                foreach (Control4Configuration.Device blind in room.Blinds)
                                {
                                    bool blindFound = false;
                                    foreach (RoomControlComponentEntity blindEntity in sectionEntity.RoomControlComponentCollection)
                                    {
                                        if (blindEntity.NameSystem.Equals(blind.Name, StringComparison.InvariantCultureIgnoreCase))
                                        {
                                            blindFound = true;
                                            break;
                                        }
                                    }
                                    if (!blindFound)
                                    {
                                        errors += string.Format("Blind \"{0}\" in Area \"{1}\" exists in the Control4 project, but not in the configuration.\n", blind.Name, room.Name);
                                    }
                                }
                            }
                            // Check if any thermostat is missing
                            if (sectionEntity.Type == Enums.RoomControlSectionType.Comfort)
                            {
                                comfortSectionFound = true;

                                foreach (Control4Configuration.Device thermostat in room.Thermostats)
                                {
                                    bool thermostatFound = false;
                                    foreach (RoomControlComponentEntity thermostatEntity in sectionEntity.RoomControlComponentCollection)
                                    {
                                        if (thermostatEntity.NameSystem.Equals(thermostat.Name, StringComparison.InvariantCultureIgnoreCase))
                                        {
                                            thermostatFound = true;
                                            break;
                                        }
                                    }
                                    if (!thermostatFound)
                                    {
                                        errors += string.Format("Thermostat \"{0}\" in Area \"{1}\" exists in the Control4 project, but not in the configuration.\n", thermostat.Name, room.Name);
                                    }
                                }
                            }
                        }

                        if (!lightingSectionFound && (room.Lights.Count > 0 || configuration.Scenes.Count > 0))
                        {
                            errors += string.Format("The Control4 project has lights and/or scenes but the configuration does not have a Lighting section in Area \"{0}\".\n", room.Name);
                        }

                        if (!blindSectionFound && room.Blinds.Count > 0)
                        {
                            errors += string.Format("The Control4 project has blinds but the configuration does not have a Lighting section in Area \"{0}\".\n", room.Name);
                        }

                        if (!comfortSectionFound && room.Thermostats.Count > 0)
                        {
                            errors += string.Format("The Control4 project has thermostats but the configuration does not have a Comfort section in Area \"{0}\".\n", room.Name);
                        }

                        // Check if any services are missing
                        foreach (Control4Configuration.Device service in configuration.Services)
                        {
                            bool serviceFound = false;
                            foreach (RoomControlSectionEntity sectionEntity in roomEntity.RoomControlSectionCollection)
                            {
                                if (sectionEntity.Type == Enums.RoomControlSectionType.Service && sectionEntity.NameSystem.Equals(service.Name, StringComparison.InvariantCultureIgnoreCase))
                                {
                                    serviceFound = true;
                                    break;
                                }
                            }
                            if (!serviceFound)
                            {
                                errors += string.Format("Service \"{0}\" in Area \"{1}\" exists in the Control4 project, but not in the configuration.\n", service.Name, room.Name);
                            }
                        }

                        break;
                    }
                }
                if (!roomFound)
                {
                    errors += string.Format("Room \"{0}\" exists in the Control4 project, but not in the configuration.\n", room.Name);
                }
            }

            foreach (RoomControlAreaEntity roomEntity in configurationEntity.RoomControlAreaCollection)
            {
                Control4Configuration.Room room = configuration.GetRoom(roomEntity.NameSystem);
                if (room != null)
                {
                    foreach (RoomControlSectionEntity sectionEntity in roomEntity.RoomControlSectionCollection)
                    {
                        foreach (RoomControlComponentEntity componentEntity in sectionEntity.RoomControlComponentCollection)
                        {
                            if (componentEntity.Type == Enums.RoomControlComponentType.Light)
                            {
                                if (!room.ContainsLight(componentEntity.NameSystem))
                                {
                                    errors += string.Format("Light \"{0}\" in Room \"{1}\" exists in the configuration, but not the Control4 project.\n", componentEntity.NameSystem, room.Name);
                                }
                            }
                            else if (componentEntity.Type == Enums.RoomControlComponentType.LightingScene)
                            {
                                if (!configuration.ContainsScene(componentEntity.NameSystem))
                                {
                                    errors += string.Format("Scene \"{0}\" in Room \"{1}\" exists in the configuration, but not the Control4 project.\n", componentEntity.NameSystem, room.Name);
                                }
                            }
                            else if (componentEntity.Type == Enums.RoomControlComponentType.Blind)
                            {
                                if (!room.ContainsBlind(componentEntity.NameSystem))
                                {
                                    errors += string.Format("Blind \"{0}\" in Room \"{1}\" exists in the configuration, but not the Control4 project.\n", componentEntity.NameSystem, room.Name);
                                }
                            }
                            else if (componentEntity.Type == Enums.RoomControlComponentType.Thermostat)
                            {
                                if (!room.ContainsThermostat(componentEntity.NameSystem))
                                {
                                    errors += string.Format("Thermostat \"{0}\" in Room \"{1}\" exists in the configuration, but not the Control4 project.\n", componentEntity.NameSystem, room.Name);
                                }
                            }
                        }
                    }
                }
                else
                {
                    errors += string.Format("Room \"{0}\" exists in the configuration, but not the Control4 project.\n", room.Name);
                }

                foreach (RoomControlSectionEntity sectionEntity in roomEntity.RoomControlSectionCollection)
                {
                    if (sectionEntity.Type == Enums.RoomControlSectionType.Service && !configuration.ContainsService(sectionEntity.NameSystem))
                    {
                        errors += string.Format("Service \"{0}\" in Room \"{1}\" exists in the configuration, but not the Control4 project.\n", sectionEntity.NameSystem, room.Name);
                    }
                }
            }

            this.plhActionResults.Visible = true;
            this.tbActionResults.Text = string.Format("Configuration verified with Control4 project file \"{0}\":\n", this.fuControl4Project.FileName);
            this.tbActionResults.Text += "-------------------------------------------------------------------------------------------------------------------------\n";
            this.tbActionResults.Text += errors.Length > 0 ? errors.TrimEnd('\n') : "No errors found.";

            this.Validate();
        }

		#endregion
	}
}
