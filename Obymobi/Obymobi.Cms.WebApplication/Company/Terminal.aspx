<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.Terminal" Codebehind="Terminal.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblName">Name</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblTeamviewerId">TeamviewerId</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbTeamviewerId" runat="server"></D:TextBoxString>
							</td>
						</tr>                        
						<tr>
							<td class="label">
								<D:LabelTextOnly runat="server" id="lblDevice">Device</D:LabelTextOnly>
							</td>
							<td class="control">
								<D:Label runat="server" ID="lblDeviceMac" LocalizeText="false"></D:Label>
                                <D:Label runat="server" LocalizeText="true" ID="lblDeviceNotLinked">Geen</D:Label>                                
							</td>
                            <td>
                            </td>
                            <td>
                                <D:LinkButton runat="server" ID="btUnlinkDevice" LocalizeText="true" PreSubmitWarning="Unlink this terminal? Are you sure?">Unlink device</D:LinkButton>
                                - <D:LinkButton runat="server" ID="btDeviceShell" LocalizeText="true">Device Shell</D:LinkButton>
                                <D:PlaceHolder ID="plhRemoteControl" runat="server">
                                    - <D:LinkButton runat="server" ID="btRemoteControl">Remote Control</D:LinkButton>
                                </D:PlaceHolder>
                                <D:PlaceHolder ID="plhLogCat" runat="server">
                                    - <D:LinkButton runat="server" ID="btnLogCat">LogCat</D:LinkButton>
                                </D:PlaceHolder>
                            </td>
                        </tr>
						<tr>
							<td class="label">
								<D:Label runat="server" id="lblLastRequest">Last request</D:Label>
							</td>
							<td class="control">
								<D:Label runat="server" id="lblLastRequestValue" LocalizeText="false"></D:Label>
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblLastStatusText">Status</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:Label runat="server" id="lblLastStatusTextValue" LocalizeText="false"></D:Label>
							</td>	
					    </tr>
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblLastApplicationVersion">Application Version</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:Label runat="server" id="lblLastApplicationVersionValue" LocalizeText="false"></D:Label>
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblLastOsVersion">OS Version</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:Label runat="server" id="lblLastOsVersionValue" LocalizeText="false"></D:Label>
							</td>
                        </tr>
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblLastSupportToolsVersion">SupportTools Version</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:Label runat="server" id="lblLastSupportToolsVersionValue" LocalizeText="false"></D:Label>
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblLastSupportToolsRequest">Last SupportTools Request</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:Label runat="server" id="lblLastSupportToolsRequestValue" LocalizeText="false"></D:Label>
							</td>
                        </tr>
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblLastAgentVersion">Android Agent Version</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:Label runat="server" id="lblLastAgentVersionValue" LocalizeText="false"></D:Label>
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblLastCloudEnvironment">Cloud Environment</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:Label runat="server" id="lblLastCloudEnvironmentValue" LocalizeText="false"></D:Label>
							</td>
                        </tr>
                        <tr>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblLastMessagingServiceVersion">Messaging Service Version</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <D:Label runat="server" id="lblLastMessagingServiceVersionValue" LocalizeText="false"></D:Label>
                            </td>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblLastMessagingServiceRequest">Last Messaging Service Request</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <D:Label runat="server" id="lblLastMessagingServiceRequestValue" LocalizeText="false"></D:Label>
                            </td>
                        </tr>
                        <tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblLastPrivateIpAddresses">IP Adress (private)</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:Label runat="server" id="lblLastPrivateIpAddressesValue" LocalizeText="false"></D:Label>
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblLastPublicIpAddress">IP Adress (public)</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:Label runat="server" id="lblLastPublicIpAddressValue" LocalizeText="false"></D:Label>
							</td>		
					    </tr>
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblLastBatteryLevel">Battery level</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:Label runat="server" id="lblLastBatteryLevelValue" LocalizeText="false"></D:Label>
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblIsCharging">Charging?</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:Label runat="server" id="lblIsChargingYes" LocalizeText="true">Yes</D:Label>
                                <D:Label runat="server" id="lblIsChargingNo" LocalizeText="true">No</D:Label>
							</td>
                        </tr>
                        <tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblOperationState">Operation state</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:Label runat="server" id="lblOperationStateValue" LocalizeText="false"></D:Label>
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblLastCommunicationMethod">Communication method</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:Label runat="server" id="lblLastCommunicationMethodValue" LocalizeText="false"></D:Label>
							</td>
						</tr>    
                        <tr>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblLastWifiStrength">Wifi strength</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <D:Label runat="server" id="lblLastWifiStrengthValue" LocalizeText="false"></D:Label>
                            </td>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" ID="lblPrinterConnected">Printer Status</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <D:Label runat="server" ID="lblPrinterConnectedValue" LocalizeText="False"></D:Label>
                            </td>
                        </tr>           
                        <tr>
                            <td class="label">
                                <D:Label runat="server" id="lblDeviceType">Device type</D:Label>
                            </td>
                            <td class="control">
                                <D:Label runat="server" id="lblDeviceTypeValue" LocalizeText="false"></D:Label>
                            </td>
                            <td class="label">
                                <D:Label runat="server" ID="lblLoadedSuccessfullyLabel">Loaded successfully</D:Label>
                            </td>
                            <td class="control">
                                <D:Label runat="server" id="lblLoadedSuccessfullyLabelYes" LocalizeText="false" Visible="false">Yes</D:Label>
                                <D:Label runat="server" id="lblLoadedSuccessfullyLabelNo" LocalizeText="false">No</D:Label>
                            </td>
                        </tr>     					    					    
					 </table>			
				</Controls>
			</X:TabPage>		
			<X:TabPage Text="Technisch" Name="Settings">
				<Controls>
                    <D:PlaceHolder runat="server">
                    <D:Panel ID="pnlConsole" runat="server" GroupingText="Console">
                        <table class="dataformV2">
                            <tr>
                                <td class="label">
								    <D:Label runat="server" id="lblTerminalConfiguration">Terminal configuration</D:Label>
							    </td>
                                <td class="control">
                                    <X:ComboBoxInt runat="server" ID="cbTerminalConfigurationId" CssClass="input" TextField="Name" ValueField="TerminalConfigurationId" IncrementalFilteringMode="Contains" IncrementalFilteringDelay="1000"></X:ComboBoxInt>
                                </td>
                                <td class="label">                                
                                </td>
                                <td class="control"> 
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:Label runat="server" id="lblForwardToTerminalId">Forward to terminal</D:Label>
							    </td>
							    <td class="control">		
                                    <X:ComboBoxLLBLGenEntityCollection runat="server" ID="cbForwardToTerminalId" IncrementalFilteringMode="StartsWith" EntityName="Terminal" TextField="Name" ValueField="TerminalId" PreventEntityCollectionInitialization="true" />
							    </td>	          
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblAnnouncementDuration">Announcement duration</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
							        <D:TextBoxInt runat="server" ID="tbAnnouncementDuration" ThousandsSeperators="false"></D:TextBoxInt>
							    </td>       
                            </tr>
                            <tr>
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblNotificationForOverdueOrder">Notification order overdue</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <X:ComboBoxInt runat ="server" ID="cbNotificationForOverdueOrder"></X:ComboBoxInt>
							    </td>
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblNotificationForNewOrder">Notification new order</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <X:ComboBoxInt runat ="server" ID="cbNotificationForNewOrder"></X:ComboBoxInt>
							    </td>
                            </tr>
                            <tr>
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblMaxProcessTime">Maximale proces tijd</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <D:TextBoxInt ID="tbMaxProcessTime" runat="server"></D:TextBoxInt>
							    </td>
                                <td class="label">
                                    <D:Label runat="server" id="lblUIModeId">UI mode</D:Label>
							    </td>
							    <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlUIModeId" IncrementalFilteringMode="StartsWith" PreventEntityCollectionInitialization="true" EntityName="UIMode" TextField="Name" ValueField="UIModeId" />
							    </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:Label runat="server" id="lblSurveyResultNotifications">Enquête notificaties tonen</D:Label>
                                </td>
                                <td class="control">
                                    <D:CheckBox runat="server" ID="cbSurveyResultNotifications" />
                                </td>
                                <td class="label">
                                    <D:Label runat="server" id="lblOfflineNotificationEnabled">Receive offline notifications</D:Label>
                                </td>
                                <td class="control">
                                    <D:CheckBox runat="server" ID="cbOfflineNotificationEnabled" />
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:Label runat="server" id="lblMasterTab">Master tab</D:Label>
                                </td>
                                <td class="control">
                                    <D:CheckBox runat="server" ID="cbMasterTab" />
                                </td>
                                <td class="label">
                                    <D:Label runat="server" ID="lblMaxVolume">Maximum volume</D:Label>
                                </td>
                                <td class="control">
                                    <D:TextBoxInt runat="server" ID="tbMaxVolume" MinimalValue="0" MaximalValue="100"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:Label runat="server" id="lblUseHardKeyboard">Bluetooth keyboard</D:Label>
                                </td>
                                <td class="control">
                                    <X:ComboBoxInt runat="server" ID="ddlUseHardKeyboard" DisplayEmptyItem="false"></X:ComboBoxInt>
                                </td>
                                <td class="label">
                                    <D:Label runat="server" id="lblPmsTerminalOfflineNotificationEnabled">Receive PMS terminal offline notification</D:Label>
                                </td>
                                <td class="control">
                                    <D:CheckBox runat="server" ID="cbPmsTerminalOfflineNotificationEnabled" />
                                </td>
                            </tr>
                        </table>
                    </D:Panel>
                    <D:Panel ID="pnlPrinting" runat="server" GroupingText="Printen">
                        <table class="dataformV2">
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" ID="lblPrintingEnabled">Orders printen</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:CheckBox runat="server" ID="cbPrintingEnabled" />
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblPrinterName">Printer</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <D:TextBoxString ID="tbPrinterName" runat="server"></D:TextBoxString>
							    </td>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblReceiptTypes">Bonnen</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <X:ComboBoxString runat="server" ID="cbReceiptTypes"></X:ComboBoxString>								
							    </td>						
						    </tr>   
                        </table>
                    </D:Panel>
                    <D:Panel ID="pnlProcessing" runat="server" GroupingText="Verwerking">
                        <table class="dataformV2">
                            <tr>
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblHandlingMethod">Afhandelingsmethode</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <X:ComboBoxInt ID="ddlHandlingMethod" runat="server" UseDataBinding="true"></X:ComboBoxInt>
							    </td>
                                <td class="label">
                                </td>
                                <td class="control">
                                </td>
                            </tr>
                            <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblPOSConnectorType">POS-integratie</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <X:ComboBoxInt runat ="server" ID="cbPOSConnectorType"></X:ComboBoxInt>
							    </td>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblPMSConnectorType">PMS-integratie</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <X:ComboBoxInt runat ="server" ID="cbPMSConnectorType"></X:ComboBoxInt>
							    </td>
                            </tr>    
                        </table>
                    </D:Panel>
                    <D:Panel ID="pnlOnSiteServer" runat="server" GroupingText="On-site Server">
                        <table class="dataformV2">
                            <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblRequestInterval">Communicatieinterval (s)</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <D:TextBoxInt ID="tbRequestInterval" runat="server" IsRequired="true"></D:TextBoxInt>
							    </td>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblDeliverypointgroupId">Tafelgroep</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <X:ComboBoxInt runat ="server" ID="cbDeliverypointgroupId" TextField="Name" ValueField="DeliverypointgroupId"></X:ComboBoxInt>
							    </td>    
						    </tr> 
                            <tr>
                        	    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblSynchronizeWithPOSOnStart">POS synchronisatie bij opstarten</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <D:CheckBox runat ="server" ID="cbSynchronizeWithPOSOnStart" />
							    </td>
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblSynchronizeWithPMSOnStart">Synchronize with pms on start</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <D:CheckBox runat ="server" ID="cbSynchronizeWithPMSOnStart" />
							    </td>
                            </tr>

                            <D:PlaceHolder runat="server" ID="plhPosSettings" Visible="false">
						    <tr>
							    <td class="label">
								    <D:Label runat="server" id="lblSystemMessagesDeliverypointId" LocalizeText="true" TranslationTag="lblSystemMessagesDeliverypointId">Tafel voor communicatie</D:Label>
							    </td>
							    <td class="control">
								    <X:ComboBoxInt runat ="server" ID="cbSystemMessagesDeliverypointId" TextField="Name" ValueField="DeliverypointId"></X:ComboBoxInt>
							    </td>
							    <td class="label">
								    <D:Label runat="server" id="lblAltSystemMessagesDeliverypointId" LocalizeText="true" TranslationTag="lblAltSystemMessagesDeliverypointId">Alternatieve tafel voor communicatie</D:Label>
							    </td>
							    <td class="control">
								    <X:ComboBoxInt runat ="server" ID="cbAltSystemMessagesDeliverypointId" TextField="Name" ValueField="DeliverypointId"></X:ComboBoxInt>
							    </td>
						    </tr>
                            <tr>
							    <td class="label">
								    <D:Label runat="server" id="lblUnlockDeliverypointProductId" TranslationTag="lblUnlockDeliverypointProductId" TextField="Name" LocalizeText="true" ValueField="DeliverypointId">Product voor 'Sluiten Tafel'</D:Label>
							    </td>
							    <td class="control">                            
								    <X:ComboBoxInt runat ="server" ID="cbUnlockDeliverypointProductId"></X:ComboBoxInt>
							    </td>
							    <td class="label">
								    <D:Label runat="server" id="lblBatteryLowProductId" TranslationTag="lblBatteryLowProductId" TextField="Name" LocalizeText="true" ValueField="DeliverypointId">Product voor 'Battery leeg'</D:Label>
							    </td>
							    <td class="control">                            
								    <X:ComboBoxInt runat ="server" ID="cbBatteryLowProductId"></X:ComboBoxInt>
							    </td>
						    </tr>
                            <tr>
							    <td class="label">
								    <D:Label runat="server" id="lblOrderFailedProductId" TranslationTag="lblOrderFailedProductId" TextField="Name" LocalizeText="true" ValueField="DeliverypointId">Product voor 'Order mislukt'</D:Label>
							    </td>
							    <td class="control">                            
								    <X:ComboBoxInt runat ="server" ID="cbOrderFailedProductId"></X:ComboBoxInt>
							    </td>
							    <td class="label">
								    <D:Label runat="server" id="lblClientDisconnectedProductId" TranslationTag="lblClientDisconnectedProductId" LocalizeText="true" TextField="Name" ValueField="DeliverypointId">Product voor 'Tablet niet verbonden'</D:Label>
							    </td>
							    <td class="control">                            
								    <X:ComboBoxInt runat ="server" ID="cbClientDisconnectedProductId"></X:ComboBoxInt>
							    </td>
                            </tr>
                            </D:PlaceHolder>
                        </table>
                    </D:Panel>
                    <D:Panel ID="pnlLogs" runat="server" GroupingText="Logs">
                        <table class="dataformV2">
                            <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblMaxDaysLogHistory">Lokale log (dagen)</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <D:TextBoxInt runat="server" ID="tbMaxDaysLogHistory" />
							    </td>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblRecordAllRequestsToRequestLog">Log naar RequestLog</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <D:CheckBox runat="server" ID="cbRecordAllRequestsToRequestLog" />
							    </td>
						    </tr>
						    <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblMaxStatusReports">Max. status rapporten</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <D:TextBoxInt runat="server" ID="tbMaxStatusReports" />
							    </td>
						    </tr> 						                                                                 
                        </table>
                    </D:Panel>
                    <D:Panel ID="pnlMisc" runat="server" GroupingText="Misc">
                        <table class="dataformV2">
                            <tr>
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblResetTimeout">Reset na x minuten</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
							        <D:TextBoxInt runat="server" ID="tbResetTimeout"></D:TextBoxInt>
							    </td>
                                <td class="label">
                                    <D:Label runat="server" id="lblUseMonitoring" LocalizeText="False">Enable Monitoring</D:Label>
							    </td>
							    <td class="control">		
                                    <D:CheckBox runat="server" ID="cbUseMonitoring" />
							    </td>	          
                             </tr>
                        </table>
                    </D:Panel>
                    </D:PlaceHolder>
				</Controls>
			</X:TabPage>	
            <X:TabPage Text="Activiteit" Name="tabActivity">
				<controls>
					<D:PlaceHolder ID="PlaceHolder1" runat="server">						
						<table class="dataformV2">
							<tr>
								<td class="label">
									<D:Label runat="server" ID="lblTypesToShow">Show</D:Label>
								</td>
								<td class="control">						
                                    <D:CheckBoxList runat="server" ID="cblTypesToShow">                                        
                                        <asp:ListItem notdirty="true" Value="TerminalLog" Selected="true">Logs</asp:ListItem>
                                        <asp:ListItem notdirty="true" Value="Netmessages" Selected="true">Netmessages</asp:ListItem>
                                        <asp:ListItem notdirty="true" Value="TerminalState" Selected="true">State changes</asp:ListItem>
                                    </D:CheckBoxList>
								</td> 
								<td class="label">
                                    <D:Label runat="server" ID="lblCommand">Commando</D:Label>
								</td>
								<td class="control" rowspan="2">
                                    <X:ComboBoxInt runat="server" notdirty="true" ID="cbCommand" ></X:ComboBoxInt><br />
                                    <D:CheckBox runat="server" ID="cbCommandToSupportTools" Text="SupportTools" /><br /><br/>
                                    <X:Button runat="server" ID="btExecuteCommand" Text="Uitvoeren"></X:Button>
								</td>    			                              
							</tr>  
                            <tr>
								<td class="label">
									<D:Label runat="server" ID="lblAmountToShow">Limit</D:Label>
								</td>
								<td class="control">						
                                    <D:TextBoxInt runat="server" notdirty="true" ID="tbAmountToShow" Width="65px" Text="100"></D:TextBoxInt>
								</td>                               
							</tr>     
							<tr>
								<td class="label">
									&nbsp;
								</td>
								<td class="control">						
                                    <X:Button runat="server" ID="btUpdateActivityLog" Text="Refresh"></X:Button>
								</td>                               
							</tr>                                                                     
						</table>	
                        <X:GridView ID="gvTerminalActivityLog" ClientInstanceName="categoriesGrid" runat="server" Width="100%">        
                            <SettingsBehavior AutoFilterRowInputDelay="350" />                            
                            <SettingsPager PageSize="17"></SettingsPager>
                            <SettingsBehavior AllowGroup="false" AllowDragDrop="false" />
                            <Columns>
                                <dxwgv:GridViewDataDateColumn FieldName="DateTime" Caption="Time" VisibleIndex="0" Width="105px">
                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yy HH:mm"> </PropertiesDateEdit>
                                </dxwgv:GridViewDataDateColumn>
                                <dxwgv:GridViewDataColumn FieldName="TypeText" Caption="Type" VisibleIndex="1">
			                        <CellStyle HorizontalAlign="left" />
                                </dxwgv:GridViewDataColumn>
                                <dxwgv:GridViewDataColumn FieldName="DescriptionTextShort" Caption="Description" VisibleIndex="2">
                                    <CellStyle HorizontalAlign="left" />
                                </dxwgv:GridViewDataColumn>
							    <dxwgv:GridViewDataHyperLinkColumn FieldName="LinkToEntityPageCms" Caption="Details" VisibleIndex="10" >
                                    <PropertiesHyperLinkEdit NavigateUrlFormatString="{0}" Text="Details"  />
                                </dxwgv:GridViewDataHyperLinkColumn>
                            </Columns>
                        </X:GridView>   
					</D:PlaceHolder>
				</controls>
			</X:TabPage>            
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>

