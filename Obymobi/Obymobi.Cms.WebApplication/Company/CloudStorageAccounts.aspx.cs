﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Web.CloudStorage;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Company
{
    public partial class CloudStorageAccounts : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            LLBLGenProDataSource datasource = this.DataSource as LLBLGenProDataSource;
            if (datasource != null)
            {
                PredicateExpression filter = new PredicateExpression(CloudStorageAccountFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
                datasource.FilterToUse = filter;
            }
        }

        public void CreateAmazonAccount()
        {
            CompanyEntity company = new CompanyEntity(CmsSessionHelper.CurrentCompanyId);
            if (company.IsNew)
            {
                return;
            }

            CloudStorageAccountEntity amazonAccount = company.CloudStorageAccountCollection.SingleOrDefault(csa => csa.Type == CloudStorageAccountType.Amazon);
            if (amazonAccount == null)
            {
                bool success = false;
                string username, accessKey, accessKeySecret;
                if (AmazonCloudStorageHelper.CreateAmazonUser(company, out username, out accessKey, out accessKeySecret))
                {
                    CloudStorageAccountEntity storageAccount = AmazonCloudStorageHelper.CreateCloudStorageAccount(CloudStorageAccountType.Amazon, company.CompanyId, username, accessKey, accessKeySecret);
                    if (storageAccount.Save())
                    {
                        success = true;
                        this.AddInformatorInfo("Created Amazon storage account.");
                    }
                }

                if (!success)
                {
                    this.AddInformatorInfo("Oops... Something went wrong while creating Amazon storage account.");
                }
            }
            else
            {
                this.AddInformatorInfo("Storage account for Amazon already exists.");
            }
        }
    }
}