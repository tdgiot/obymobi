﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.Survey" Codebehind="Survey.aspx.cs" %>
<%@ Reference Control="~/Company/Subpanels/SurveyPagePanel.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblName">Naam</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
							</td>
							<td class="label">
								<D:Label runat="server" id="lblShowResult">Resultaat</D:Label>
							</td>
							<td class="control">
								<D:Button runat="server" ID="btShowResult" Text="Bekijk resultaat" />
							</td>
					    </tr>	
                        <tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblSavingTitle">Enquête wordt opgeslagen titel</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbSavingTitle" runat="server"></D:TextBoxString>
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblSavingMessage">Enquête wordt opgeslagen bericht</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbSavingMessage" runat="server"></D:TextBoxString>
							</td>
					    </tr>	
                        <tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblProcessedTitle">Enquête afgehandeld titel</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbProcessedTitle" runat="server"></D:TextBoxString>
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblProcessedMessage">Enquête afgehandeld bericht</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbProcessedMessage" runat="server"></D:TextBoxString>
							</td>
					    </tr>	
                        <tr>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblAnswerRequiredTitle">Antwoord noodzakelijk titel</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <D:TextBoxString ID="tbAnswerRequiredTitle" runat="server"></D:TextBoxString>
                            </td>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblAnswerRequiredMessage">Antwoord noodzakelijk bericht</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <D:TextBoxString ID="tbAnswerRequiredMessage" runat="server"></D:TextBoxString>
                            </td>
                        </tr>		
                         <tr>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblEmailResults">Email resultaten?</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <D:CheckBox ID="cbEmailResults" runat="server" />
                            </td>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblEmail">Email</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control" rowspan="3">
                                <D:TextBoxMultiLine ID="tbEmail" runat="server"></D:TextBoxMultiLine>
                                <br />
                                <D:Label ID="lblEmailComment" runat="server"><small>Meerdere e-mailadressen: Plaats elk e-mailadres op een nieuwe regel.</small></D:Label>
							</td>	
                        </tr>			    
					 </table>	
				</Controls>
			</X:TabPage>	
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>

