﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.RoomControlConfiguration" Codebehind="RoomControlConfiguration.aspx.cs" %>
<%@ Reference VirtualPath="~/Company/SubPanels/RoomControlConfigurationPanel.ascx" %>
<%@ Reference Control="~/Company/SubPanels/RoomControlConfigurationPanel.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">    
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
                    <D:PlaceHolder ID="PlaceHolder1" runat="server">
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblName">Naam</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
							</td>	  
                            <td class="label">
                                
                            </td>
                            <td class="control">
                                
                            </td>
                        </tr>
                        <D:PlaceHolder runat="server" ID="plhConfiguration">
                        <tr>
                            <td class="label">
                                <D:Label runat="server" id="lblConfigration">Configuration</D:Label>
                            </td>
                            <td class="control" colspan="3">
                                <div style="width: 736px;">
                                    <D:PlaceHolder runat="server" ID="plhConfigurationPanel"></D:PlaceHolder>
                                </div>
                            </td>
                        </tr>							                  
                        </D:PlaceHolder>                    
                    </table>
                    </D:PlaceHolder>   
                </Controls>                    
            </X:TabPage>
            <X:TabPage Text="Acties" Name="Actions">
				<Controls>
                    <D:PlaceHolder ID="PlaceHolder2" runat="server">
					<table class="dataformV2">
                        <tr>
							<td class="label">
								<D:LabelAssociated runat="server" id="lblCopyConfigurationName">Configuration Name</D:LabelAssociated>
							</td>
							<td class="control" colspan="3">
                                <div>
								    <div style="float:left;"><D:TextBox runat="server" ID="tbCopyConfigurationName" notdirty="true" Width="250px"></D:TextBox></div>
                                    <div style="float:left; margin-left: 5px;"><X:ComboBoxInt runat="server" ID="cbCopyConfigurationCompanyId" TextField="Name" ValueField="CompanyId" notdirty="true" Visible="false" Width="250px" /></div>
                                    <div style="float:left; margin-left: 5px;"><D:Button runat="server" ID="btCopyConfiguration" Text="Maak een kopie van deze configuratie" UseSubmitBehavior="false" /></div>
                                </div>
							</td>
                        </tr>
                        <tr>
                            <td class="label">
							    <D:Label runat="server" ID="lblControl4Project">Control4 Project</D:Label>
						    </td>
						    <td class="control">
							    <div style="float:left;"><D:FileUpload runat="server" ID="fuControl4Project"/></div>
                                <div style="float:left; margin-left: 17px;"><D:Button runat="server" ID="btnImportControl4Project" Text="Import Project" /></div>
                                <div style="float:left; margin-left: 5px;"><D:Button runat="server" ID="btnVerifyControl4Project" Text="Verify Configuration" /></div>
						    </td>
                        </tr>
                        <D:PlaceHolder ID="plhActionResults" runat="server" Visible="false">
                            <tr>
                            <td class="label">
                            </td>
                            <td class="control" colspan="3">
                                <D:TextBoxMultiLine runat="server" ID="tbActionResults" ReadOnly="true" Height="300px" />
                            </td>
                        </tr>
                        </D:PlaceHolder>
                    </table>
                    </D:PlaceHolder>       
                </Controls>
            </X:TabPage>
            <X:TabPage Text="Push" Name="Push">
                <Controls>
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                            </td>
                            <td class="control">
                                <p>This will push the room control configuration.</p>
                                <p>Once you press this button all your changes will be available for all clients!</p>
                                <br/>
                                <D:Button runat="server" ID="btnPush" LocalizeText="False" Text="Push Configuration"/><br/>                                
                            </td>
                            <td class="label">
                            </td>
                            <td class="control">
                            </td>
                        </tr>
                    </table>
                </Controls>
            </X:TabPage>
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>