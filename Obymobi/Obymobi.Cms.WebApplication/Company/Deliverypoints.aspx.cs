﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data;
using Obymobi.Data.EntityClasses;

namespace Obymobi.ObymobiCms.Company
{
    public partial class Deliverypoints : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            //this.PrefetchPath = new PrefetchPath(EntityType.DeliverypointEntity);
            //this.PrefetchPath.Add(DeliverypointEntity.PrefetchPathDeliverypointgroupEntity);

            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            LLBLGenProDataSource datasource = this.DataSource as LLBLGenProDataSource;
            if (datasource != null)
            {
                PredicateExpression filter = new PredicateExpression(DeliverypointFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
                datasource.FilterToUse = filter;                
            }

            this.MainGridView.CustomColumnSort += new DevExpress.Web.ASPxGridViewCustomColumnSortEventHandler(MainGridView_CustomColumnSort);            
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            DevExpress.Web.GridViewDataColumn column = this.MainGridView.Columns["Number"] as DevExpress.Web.GridViewDataColumn;
            if (column != null)
            {
                column.Settings.SortMode = DevExpress.XtraGrid.ColumnSortMode.Custom;
            }
        }

        void MainGridView_CustomColumnSort(object sender, DevExpress.Web.CustomColumnSortEventArgs e)
        {
            if (e.Column.FieldName == "Number")
            {
                long numVal1 = 0, numVal2 = 0;
                try
                {
                    numVal1 = Convert.ToInt64(e.Value1);
                    numVal2 = Convert.ToInt64(e.Value2);

                    if (numVal1 < numVal2)
                        e.Result = -1;
                    else if (numVal1 > numVal2)
                        e.Result = 1;
                    else
                        e.Result = 0;
                }
                catch 
                {
                    e.Result = 0;
                }

                e.Handled = true;
            }
        }

        #endregion
    }
}
