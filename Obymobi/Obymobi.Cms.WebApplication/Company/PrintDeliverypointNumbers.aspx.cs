﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dionysos;
using Dionysos.Data;
using System.Text;
using System.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dionysos.Web.UI;
using Obymobi.Data.EntityClasses;
using SpreadsheetLight;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Drawing;
using Obymobi.Enums;

namespace Obymobi.ObymobiCms.Company
{
    public partial class PrintDeliverypointNumbers : PageDefault
    {
        #region Methods 

        private void GenerateTableNumbers()
        {
            if (CmsSessionHelper.CurrentCompanyId <= 0)
                throw new ApplicationException("No company is selected!");

            List<int> tableNumbersAsInt = new List<int>();
            DataTable dt = new DataTable();

            PredicateExpression filter = new PredicateExpression(DeliverypointFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            SortExpression sort = new SortExpression(new SortClause(DeliverypointFields.Number, SortOperator.Ascending));

            DeliverypointCollection deliverypoints = new DeliverypointCollection();
            deliverypoints.GetMulti(filter, 0, sort);

            var sorted = deliverypoints.OrderBy(d => Convert.ToInt32(d.Number));

            // Prepare datatable
            dt.Columns.Add("Datum");
            //dt.Columns.Add("DateSalt");

            // Convert to int & create a column per table number in de DT
            foreach (var deliverypoint in sorted)
            {
                tableNumbersAsInt.Add(Convert.ToInt32(deliverypoint.Number));
                dt.Columns.Add("Tafel " + deliverypoint.Number);
                //dt.Columns.Add("TafelB " + number);
            }

            DateTime current = this.deTableNumbersFromDate.Value.Value;
            int count = 0;

            // Loop to create a row for each date
            int maxValue = int.MinValue;
            int minValue = int.MaxValue;
            while (current <= this.deTableNumbersToDate.Value.Value && count < 1000)
            {
                // Create de Row
                var row = dt.NewRow();
                row["Datum"] = current.ToString("dd-MM-yyyy");
                dt.Rows.Add(row);

                DeliverypointgroupCollection deliverypointgroups = new DeliverypointgroupCollection();
                deliverypointgroups.GetMulti(DeliverypointgroupFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

                if (deliverypointgroups.Count != 1)
                    throw new ApplicationException(string.Format("No deliverypoint group found for company '{1}'!", CmsSessionHelper.CurrentCompanyId));

                int dateSalt = this.GetDateSalt(current);
                //row["DateSalt"] = dateSalt.ToString();

                // Loop through columsn to create a value for each table for the current date
                foreach (var tableNumber in tableNumbersAsInt)
                {
                    // Perform the logic 

                    // Add the regular salt to the tablenumber
                    int tableNumberAndSalt = tableNumber + deliverypointgroups[0].EncryptionSalt + dateSalt;
                    string tableNumberAndSaltString = tableNumberAndSalt.ToString();

                    // Determine odd/even of the sum of all digits
                    int sum = 0;
                    for (int i = 0; i < tableNumberAndSaltString.Length; i++)
                        sum += Convert.ToInt32(tableNumberAndSaltString[i].ToString());

                    // Determine odd/even
                    bool even = (sum % 2 == 0);

                    // Create new string by repositioning values
                    string shuffled = string.Empty;

                    if (even)
                        shuffled = string.Format("{0}{1}{2}{3}", tableNumberAndSaltString[1], tableNumberAndSaltString[2], tableNumberAndSaltString[3], tableNumberAndSaltString[0]);
                    else
                        shuffled = string.Format("{0}{1}{2}{3}", tableNumberAndSaltString[3], tableNumberAndSaltString[1], tableNumberAndSaltString[2], tableNumberAndSaltString[0]);

                    row["Tafel " + tableNumber] = "'" + shuffled;

                    if (tableNumberAndSalt > maxValue)
                        maxValue = tableNumberAndSalt;

                    if (tableNumberAndSalt < minValue)
                        minValue = tableNumberAndSalt;


                    //row["TafelB " + tableNumber] = shuffled;
                }

                current = current.AddDays(1);
                count++;
            }



            if ((minValue < 1000 || maxValue > 9999) && !TestUtil.IsPcGabriel)
                throw new FunctionalException("Numbers cannot be generated. tableNumberAndSalt MaxValue: {0} & MinValue: {1}", maxValue, minValue);

            // Create a nice Excel sheet... nice... really nice... butter smooth... almost sexy... Hmmm.... 
            SLDocument excel = new SLDocument();

            // Import data table
            excel.ImportDataTable(1, 1, dt, true);

            // Lock header and left column
            excel.FreezePanes(1, 1);            

            // Make headers / left column bold
            SLStyle boldStyle = excel.CreateStyle();
            boldStyle.SetFontBold(true);
            excel.SetRowStyle(1, boldStyle);
            excel.SetColumnStyle(1, boldStyle);

            /*
            var stats = excel.GetWorksheetStatistics();
            excel.AutoFitColumn(1, stats.EndColumnIndex);
            excel.AutoFitRow(1, stats.EndRowIndex);            
            */                        
            // Get the max width in characters for each colum
            Dictionary<int, int> columnWidths = new Dictionary<int, int>();
            int characters = 0;
            for (int iRow = 1; iRow <= dt.Rows.Count; iRow++)
            {
                for (int iColumn = 1; iColumn < dt.Columns.Count; iColumn++)
                {
                    characters = excel.GetCellValueAsString(iRow, iColumn).Length;
                    if (!columnWidths.ContainsKey(iColumn))
                    {
                        columnWidths.Add(iColumn, characters);
                    }
                    else if (columnWidths[iColumn] < characters)
                    {
                        columnWidths[iColumn] = characters;
                    }
                }
            }

            // Set width of columns & right line
            SLStyle borderRight = excel.CreateStyle();
            borderRight.Border.RightBorder.BorderStyle = BorderStyleValues.Thin;
            borderRight.Border.RightBorder.Color = System.Drawing.Color.Black;
            foreach (KeyValuePair<int, int> pair in columnWidths)
            {
                excel.SetColumnStyle(pair.Key, borderRight);
                excel.SetColumnWidth(pair.Key, Convert.ToDouble(pair.Value * 1.4));
            }            
            
            // Create alternating backgrounds by make Even a bit grey
            SLStyle greyBackground = excel.CreateStyle();
            greyBackground.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.LightGray, System.Drawing.Color.LightGray);

            for (int iRow = 1; iRow <= dt.Rows.Count; iRow++)
            {
                if (iRow % 2 == 0)
                {
                    excel.SetRowStyle(iRow, greyBackground);
                }
            }

            var response = this.Response;

            CompanyEntity company = new CompanyEntity(CmsSessionHelper.CurrentCompanyId);
            string fileName = string.Format("Tafelnummers-{0}-van-{1}-tot-{2}", company.Name.Replace(" ",""),
                this.deTableNumbersFromDate.Value.Value.ToString("dd-MM-yy"), this.deTableNumbersToDate.Value.Value.ToString("dd-MM-yy"));                

            response.Clear();
            response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}.xlsx", fileName));                            
            excel.SaveAs(response.OutputStream);
            response.End();            
        }

        int GetDateSalt(DateTime current)
        {
            int sum = current.Month + current.Day;

            if (current.Day % 2 == 0)
                sum += 3;
            else
                sum += 7;

            return sum;
        }

       private void ValidateCompany(bool asError)
        {
            var company = new CompanyEntity(CmsSessionHelper.CurrentCompanyId);

            string message = string.Empty;
            
            if (company.IsNew)
            {
                message = this.Translate("lblNoCompanySelected", "Er is op dit moment geen bedrijf geselecteerd.");
            }
            else if (company.SystemType == SystemType.Crave)
            {
                message = this.Translate("lblCompanyHasWrongSystemType", "Het is alleen mogelijk om tafelnummers te genereren voor Otoucho bedrijven.");
            }
            else if(company.DeliverypointCollection.Count == 0)
            {
                message = this.Translate("lblCompanyHasNoTables", "Het bedrijf heeft geen tafels om codes voor te generen.");
            }
            else if (company.DeliverypointgroupCollection.Any(dpg => dpg.UseManualDeliverypoint == false))
            {
                message = this.Translate("lblDpgUseManualDeliverypointNotSet", "De instelling 'Handmatig tafel/rekeningnummer' is niet ingesteld voor een tafelgroep.");
            }
            else if (company.DeliverypointgroupCollection.Any(dpg => dpg.UseManualDeliverypointEncryption == false))
            {
                message = this.Translate("lblDpgUseManualTablenumberEncryptionNotSet", "De instelling 'Versleutelde nummers' is niet ingesteld voor een tafelgroep.");
            }
            else if(company.DeliverypointgroupCollection.Any(dpg => dpg.EncryptionSalt <= 0))
            {
                message = this.Translate("lblNoEncryptedCode", "Eén of meerdere tafelgroepen hebben geen versleuteld code.");
            }

            if(!message.IsNullOrWhiteSpace())
            {
                if(asError)
                    this.MultiValidatorDefault.AddError(message);
                else
                    this.AddInformator(InformatorType.Warning, message);
            }

            this.Validate();
        }

        #endregion

        #region Event handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.ValidateCompany(false);
                this.deTableNumbersFromDate.Value = DateTime.Now;
                this.deTableNumbersToDate.Value = DateTime.Now.AddDays(31);
            }

            this.btnGenerateDeliverypointNumbers.Click += new EventHandler(btnGenerateDeliverypointNumbers_Click);
        }

        private void btnGenerateDeliverypointNumbers_Click(object sender, EventArgs e)
        {
            // Validate period
            if (!this.deTableNumbersFromDate.Value.HasValue ||
                !this.deTableNumbersToDate.Value.HasValue)
            {
                this.MultiValidatorDefault.AddError(this.Translate("DateFromAndDateToAreRequired", "U dient een 'Van' en 'Tot' datum te kiezen"));
                return;
            }

            TimeSpan period = this.deTableNumbersToDate.Value.Value - this.deTableNumbersFromDate.Value.Value;

            if (period.TotalDays <= 0)
            {
                this.MultiValidatorDefault.AddError(this.Translate("PeriodMustBeAtLeastOneday", "De periode dient tenminste één dag lang te zijn."));
            }
            if (period.TotalDays > 62)
            {
                this.MultiValidatorDefault.AddError(this.Translate("PeriodMustBeAtLeastOneday", "De periode kan maximaal 2 maanden zijn."));
            }

            this.ValidateCompany(true);
            
            // Valid customer
            this.Validate();

            if (this.IsValid)
            {
                this.GenerateTableNumbers();
            }
        }

        #endregion
    }
}