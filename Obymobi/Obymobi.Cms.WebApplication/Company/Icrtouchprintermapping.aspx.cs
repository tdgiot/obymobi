﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.ObymobiCms.Company.Subpanels;

namespace Obymobi.ObymobiCms.Company
{
    public partial class Icrtouchprintermapping : Dionysos.Web.UI.PageLLBLGenEntity
    {

        protected override void OnInit(EventArgs e)
        {
            this.DataSourceLoaded += new Dionysos.Delegates.Web.DataSourceLoadedHandler(Icrtouchprintermapping_DataSourceLoaded);
            base.OnInit(e);
        }

        void RenderMatrix()
        {
            // Fill the terminal dropdownlist
            TerminalCollection terminals = new TerminalCollection();
            PredicateExpression terminalFilter = new PredicateExpression(TerminalFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            SortExpression terminalSort = new SortExpression(TerminalFields.Name | SortOperator.Ascending);
            terminals.GetMulti(terminalFilter, 0, terminalSort);

            this.ddlTerminalId.DataSource = terminals;
            this.ddlTerminalId.DataBind();

            // Retrieve all deliverypoints for company
            Data.CollectionClasses.DeliverypointCollection deliverypoints = new Data.CollectionClasses.DeliverypointCollection();
            
            PredicateExpression filter = new PredicateExpression();
            filter.Add(DeliverypointFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            
            SortExpression sort = new SortExpression();
            sort.Add(DeliverypointFields.Number | SortOperator.Ascending);
            
            deliverypoints.GetMulti(filter, 0, sort);

            

            var mappingsView = this.DataSourceAsIcrtouchprintermappingEntity.IcrtouchprintermappingDeliverypointCollection.DefaultView;

            var deliverypointsSorted = deliverypoints.OrderBy(dp => Convert.ToInt32(dp.Number));
            foreach (var deliverypoint in deliverypointsSorted)
            {
                filter = new PredicateExpression();
                filter.Add(IcrtouchprintermappingDeliverypointFields.DeliverypointId == deliverypoint.DeliverypointId);

                mappingsView.Filter = filter;

                var questionPanel = this.LoadControl<IcrtouchprintermappingDeliverypointEntityPanel>("~/Company/SubPanels/IcrtouchprintermappingDeliverypointEntityPanel.ascx");

                questionPanel.DeliverypointName = deliverypoint.Name;
                questionPanel.DeliveryPointId = deliverypoint.DeliverypointId;

                if (mappingsView.Count == 1)
                {
                    questionPanel.IcrtouchprintermappingDeliverypoint = mappingsView[0];
                    
                }
                else
                {
                    questionPanel.IcrtouchprintermappingDeliverypoint = new IcrtouchprintermappingDeliverypointEntity();
                }

                this.plhPrinterMappingsMatrix.Controls.Add(questionPanel);
            }
        }

        public override bool Save()
        {
            if (base.Save())
            {
                for (int i = 0; i < this.ControlList.Count; i++)
                {
                    if (this.ControlList[i] is IcrtouchprintermappingDeliverypointEntityPanel)
                    {
                        ((IcrtouchprintermappingDeliverypointEntityPanel)this.ControlList[i]).Save();
                    }
                }
            }

            return true;
        }

        void Icrtouchprintermapping_DataSourceLoaded(object sender)
        {
            this.RenderMatrix();
        }

        /// <summary>
        /// Return the page's datasource as a IcrtouchprintermappingEntity
        /// </summary>
        public IcrtouchprintermappingEntity DataSourceAsIcrtouchprintermappingEntity
        {
            get
            {
                return this.DataSource as IcrtouchprintermappingEntity;
            }
        }
    }
}