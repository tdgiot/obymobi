﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.Gallery" Title="Gallery" Codebehind="Gallery.aspx.cs" %>
<%@ Register Assembly="DevExpress.Web.v20.1" Namespace="DevExpress.Web" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitleHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhToolbarHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhContentHolder" runat="Server">
<X:PageControl runat="server" ID="tabsMain" Width="100%">
        <TabPages>
            <X:TabPage Text="Bronbestand" Name="Generic">
                <Controls>
				    <table class="dataformV2">
					    <tr>
						    <td class="label">
							    <D:Label runat="server" ID="lblDocument">Afbeelding</D:Label>
						    </td>
						    <td class="control">
							    <D:FileUpload runat="server" ID="fuDocument" IsRequired="true"/>
						    </td>           
						    <td class="label">

						    </td>
						    <td class="control">
		                        
						    </td>        
					    </tr>
                        <tr>
                            <td class="label">

                            </td>
                            <td class="control">
                                <D:Button runat="server" ID="btnAddImage" Text="Afbeelding toevoegen" />
                            </td>
                        </tr>
				    </table>   
			    </Controls>
            </X:TabPage>
        </TabPages>
    </X:PageControl>
    <dx:ASPxDataView ID="dvGallery" runat="server" AllowPaging="False" Layout="Flow" CssPostfix="1" Width="100%">
        <ItemTemplate>
            <div align="center" style="height: 100%">
                <table>
                    <tr>
                        <td class="thumbnailImageCell" onclick="document.location='<%# Eval("MediaUrl") %>'">
                            <D:Image ID="imgPhoto" ImageUrl='<%# Eval("ImageUrl") %>' AlternateText='<%# Eval("Name") %>' Height='150px' runat="server" />
                        </td>
                    </tr>
                </table>
                <asp:Label Text='<%# Eval("Name") %>' runat="server" ID="lbName" />
            </div>
        </ItemTemplate>
    </dx:ASPxDataView>
</asp:Content>
