﻿<%@ Page Title="Add company" Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.AddCompany" Codebehind="AddCompany.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitleHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhToolbarHolder" Runat="Server">
    <D:Button runat="server" ID="btAddCompany" Text="Bedrijf aanmaken"/>	
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhContentHolder" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
                    <D:Panel ID="pnlCompany" runat="server" GroupingText="Company">
                        <table class="dataformV2">
						    <tr>
                                <td class="label">
								    <D:Label runat="server" id="lblName">Name</D:Label>
							    </td>
							    <td class="control">
								    <D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
							    </td>
                                <td class="label">
								    <D:Label runat="server" id="lblSupportpoolId">Support pool</D:Label>
							    </td>
							    <td class="control">
								    <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlSupportpoolId" CssClass="input" UseDataBinding="true" EntityName="Supportpool" IsRequired="true"></X:ComboBoxLLBLGenEntityCollection>            
							    </td>
					        </tr>                            							    							   					        
                            <tr>
                                <td class="label">
								    <D:Label runat="server" id="lblUsername">Gebruikersnaam</D:Label>
							    </td>
							    <td class="control">
								    <D:TextBoxString ID="tbUsername" runat="server" IsRequired="true"></D:TextBoxString>
							    </td>
							    <td class="label">
								    <D:Label runat="server" id="lblPasswordDecrypted">Wachtwoord</D:Label>
							    </td>
							    <td class="control">
								    <D:TextBoxString ID="tbPasswordDecrypted" runat="server" IsRequired="true"></D:TextBoxString>
							    </td>	
					        </tr>
						    <tr>
                                <td class="label">
                                    <D:Label runat="server" ID="lblCountryId">Country</D:Label>
                                </td>
                                <td class="control">
                                    <X:ComboBox runat="server" ID="ddlCountry" IsRequired="true" TextField="Name" ValueField="CodeAlpha3"></X:ComboBox>
                                </td>
                                <td class="label">
								    <D:Label runat="server" id="lblZipcode">Zipcode</D:Label>
							    </td>
							    <td class="control">
								    <D:TextBoxString ID="tbZipcode" runat="server" IsRequired="true"></D:TextBoxString>
							    </td>                                
                            </tr>						    
                            <tr>
                                <td class="label">
                                    <D:Label runat="server" id="lblCurrencyCode">Default currency</D:Label>
                                </td>
                                <td class="control">
                                    <X:ComboBox runat="server" ID="ddlCurrencyCode" TextField="Name" ValueField="Code" IsRequired="true"></X:ComboBox>
                                </td>
                                <td class="label">
								    <D:Label runat="server" id="lblCultureCode">Default culture</D:Label>
							    </td>
							    <td class="control">
								    <X:ComboBox runat="server" ID="ddlCultureCode" IsRequired="true"></X:ComboBox>
							    </td>	          
                            </tr>                            
                            <tr>
                                <td class="label">
                                    <D:Label runat="server" id="lblTimeZone">Timezone</D:Label>
                                </td>
                                <td class="control">
                                    <X:ComboBox runat="server" ID="ddlTimeZoneOlsonId" IsRequired="true" TextField="Name" ValueField="OlsonTimeZoneId"></X:ComboBox>
                                </td>
                                <td class="label">
								    <D:Label runat="server" ID="lblCompanyCultures">Cultures</D:Label>
							    </td>
							    <td class="control">																																					 
								    <X:ListBox runat="server" ID="lbCompanyCultures" SelectionMode="CheckColumn" TextField="Name" ValueField="Code"></X:ListBox>
							    </td> 
                            </tr>
                        </table>
                    </D:Panel>	
                    <D:Panel ID="pnlTerminal" runat="server" GroupingText="Terminal">
                        <table class="dataformV2">
						    <tr>
                                <td class="label">
                                    <D:Label runat="server" ID="lblCreateTerminal">Create terminal</D:Label>
                                </td>
                                <td class="control">
                                    <D:CheckBox runat="server" ID="cbCreateTerminal" />
                                </td>
                                <td class="label">
                                </td>
                                <td class="control">
                                </td>
						    </tr>
                            <tr>
                                <td class="label">
                                    <D:Label runat="server" ID="lblTerminalName">Name</D:Label>
                                </td>
                                <td class="control">
                                    <D:TextBoxString runat="server" ID="tbTerminalName"></D:TextBoxString>
                                </td>
                                <td class="label">
								    <D:Label runat="server" id="lblTerminalType">Type</D:Label>
							    </td>
							    <td class="control">
								    <X:ComboBoxInt runat="server" ID="ddlTerminalType"></X:ComboBoxInt>
							    </td>
                            </tr>
                        </table>
                    </D:Panel>
                    <D:Panel ID="pnlDeliverypointgroup" runat="server" GroupingText="Deliverypoint group">
                        <table class="dataformV2">
						    <tr>
                                <td class="label">
                                    <D:Label runat="server" ID="lblCreateDeliverypointgroup">Create deliverypointgroup</D:Label>
                                </td>
                                <td class="control">
                                    <D:CheckBox runat="server" ID="cbCreateDeliverypointgroup" />
                                </td>
                                <td class="label">
                                </td>
                                <td class="control">
                                </td>
						    </tr>
                            <tr>
                                <td class="label">
                                    <D:Label runat="server" ID="lblDeliverypointgroupName">Name</D:Label>
                                </td>
                                <td class="control">
                                    <D:TextBoxString runat="server" ID="tbDeliverypointgroupName"></D:TextBoxString>
                                </td>
                                <td class="label">
                                    <D:Label runat="server" id="lblUITheme">UI theme</D:Label>
                                </td>
                                <td class="control">
                                    <X:ComboBoxInt runat="server" ID="cbUITheme" TextField="Name" ValueField="UIThemeId"></X:ComboBoxInt>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
								    <D:Label runat="server" id="lblPincode">Pincode</D:Label>
							    </td>
							    <td class="control">
							        <D:TextBoxString runat="server" ID="tbPincode" MaxLength="25" />
							    </td>        
                                <td class="label">
                                    <D:Label runat="server" id="lblPincodeGM">Pincode godmode</D:Label>
							    </td>
							    <td class="control">
                                    <D:TextBoxString runat="server" ID="tbPincodeGM" MaxLength="25" />
							    </td>
                            </tr>
                            <tr>
                                <td class="label">
								    <D:Label runat="server" id="lblPincodeSU">Pincode superuser</D:Label>
							    </td>
							    <td class="control">
							        <D:TextBoxString runat="server" ID="tbPincodeSU" MaxLength="25" />
							    </td>   
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:Label runat="server" ID="lblCreateDeliverypoints">Create deliverypoints</D:Label>
                                </td>
                                <td class="control">
                                    <D:CheckBox runat="server" ID="cbCreateDeliverypoints" />
                                </td>
                                <td class="label">

                                </td>
                                <td class="control">

                                </td>
						    </tr>
                            <tr>
                                <td class="label">
                                    <D:LabelTextOnly runat="server" id="lblDeliverypointRange">Range</D:LabelTextOnly>
		                        </td>
		                        <td class="control">
                                    <D:TextBoxInt ID="tbRangeStart" runat="server" ThousandsSeperators="false" style="width: 30px;" notdirty="true"></D:TextBoxInt>
                                    -
                                    <D:TextBoxInt ID="tbRangeEnd" runat="server" ThousandsSeperators="false" style="width: 30px;" notdirty="true"></D:TextBoxInt>                                    
		                        </td>
                            </tr>
                        </table>
                    </D:Panel>
                    <D:Panel ID="pnlRoute" runat="server" GroupingText="Route">
                        <table class="dataformV2">
						    <tr>
                                <td class="label">
                                    <D:Label runat="server" ID="lblCreateRoute">Create route</D:Label>
                                </td>
                                <td class="control">
                                    <D:CheckBox runat="server" ID="cbCreateRoute" />
                                </td>
                                <td class="label">
                                </td>
                                <td class="control">
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
								    <D:Label runat="server" id="lblRoutestephandlerType2">Routestep handler type</D:Label>
							    </td>
							    <td class="control">
								    <X:ComboBoxInt runat="server" ID="ddlRoutestephandlerType"></X:ComboBoxInt>
							    </td>	
                                <td class="label">
                                </td>
                                <td class="control">
                                </td>
                            </tr>
                        </table>
                    </D:Panel>
                <D:Panel ID="pnlVersioning" runat="server" GroupingText="Versioning">
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:Label runat="server" ID="lblApiVersion">API Version</D:Label>
                            </td>
                            <td class="control">
                                <D:DropDownListInt runat="server" ID="ddlApiVersion" />
                            </td>
                            <td class="label">
                                <D:Label runat="server" ID="lblMessagingVersion">Messaging Version</D:Label>
                            </td>
                            <td class="control">
                                <D:DropDownListInt runat="server" ID="ddlMessagingVersion" />
                            </td>
                        </tr>
                    </table>
                </D:Panel>
                </Controls>
            </X:TabPage>
        </TabPages>
    </X:PageControl>
</div>
</asp:Content>

