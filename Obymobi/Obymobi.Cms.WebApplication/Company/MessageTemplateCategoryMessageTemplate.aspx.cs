﻿using Dionysos.Web.UI;
using Obymobi.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Obymobi.ObymobiCms.Company
{
    public partial class MessageTemplateCategoryMessageTemplate : PageLLBLGenEntity
    {
        #region EventHandlers

        protected void Page_Load(object sender, EventArgs e)
        {
            // Redirect the user if it's not an administrator
            if (CmsSessionHelper.CurrentRole < Role.Administrator)
                Response.Redirect("~/401.aspx");
        }

        #endregion
    }
}