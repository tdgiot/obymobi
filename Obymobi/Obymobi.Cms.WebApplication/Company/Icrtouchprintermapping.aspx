﻿<%@ Page Language="C#" AutoEventWireup="true"
    MasterPageFile="~/MasterPages/MasterPageEntity.master" Inherits="Obymobi.ObymobiCms.Company.Icrtouchprintermapping" Codebehind="Icrtouchprintermapping.aspx.cs" %>
<%@ Reference VirtualPath="~/Company/SubPanels/IcrtouchprintermappingDeliverypointEntityPanel.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" runat="Server">
    <div>
        <X:PageControl ID="tabsMain" runat="server" Width="100%">
            <TabPages>
                <X:TabPage Text="Algemeen" Name="Generic">
                    <controls>
                    <D:Placeholder runat="server">
					    <table class="dataformV2">
						    <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblName">Naam</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
							    </td>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblTerminal">Terminal</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlTerminalId" IncrementalFilteringMode="StartsWith" EntityName="Terminal" TextField="Name" ValueField="TerminalId" IsRequired="true" />
							    </td>
						    </tr>
					    </table>
                        <table>
                            <tr>
                                <th>
                                    <D:Label runat="server" ID="lblHeadingDeliverypoint">Tafel</D:Label>
                                </th>
                                <th>
                                    <D:Label runat="server" ID="lblHeadingPrinter1" TranslationTag="Printer">KP</D:Label>1
                                </th>
                                <th>
                                    <D:Label runat="server" ID="lblHeadingPrinter2" TranslationTag="Printer">KP</D:Label>2
                                </th>
                                <th>
                                    <D:Label runat="server" ID="lblHeadingPrinter3" TranslationTag="Printer">KP</D:Label>3
                                </th>
                                <th>
                                    <D:Label runat="server" ID="lblHeadingPrinter4" TranslationTag="Printer">KP</D:Label>4
                                </th>
                                <th>
                                    <D:Label runat="server" ID="lblHeadingPrinter5" TranslationTag="Printer">KP</D:Label>5
                                </th>
                                <th>
                                    <D:Label runat="server" ID="lblHeadingPrinter6" TranslationTag="Printer">KP</D:Label>6
                                </th>
                                <th>
                                    <D:Label runat="server" ID="lblHeadingPrinter7" TranslationTag="Printer">KP</D:Label>7
                                </th>
                                <th>
                                    <D:Label runat="server" ID="lblHeadingPrinter8" TranslationTag="Printer">KP</D:Label>8
                                </th>
                                <th>
                                    <D:Label runat="server" ID="lblHeadingPrinter9" TranslationTag="Printer">KP</D:Label>9
                                </th>
                                <th>
                                    <D:Label runat="server" ID="lblHeadingPrinter10" TranslationTag="Printer">KP</D:Label>10
                                </th>
                            </tr>
					        <D:Placeholder runat="server" ID="plhPrinterMappingsMatrix">
					        </D:Placeholder>
                        </table>
                    </D:Placeholder>
				</controls>
                </X:TabPage>
            </TabPages>
        </X:PageControl>
    </div>
</asp:Content>
