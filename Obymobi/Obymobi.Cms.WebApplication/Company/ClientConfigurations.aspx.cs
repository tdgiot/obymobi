﻿using System;
using System.Web.UI.WebControls;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.ObymobiCms.MasterPages;

namespace Obymobi.ObymobiCms.Company
{
	public partial class ClientConfigurations : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            MasterPageEntityCollection masterPage = this.Master as MasterPages.MasterPageEntityCollection;
            if (masterPage != null)
            {
                masterPage.ToolBar.AddButton.Visible = false;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.MainGridView.ShowDeleteColumn = false;
            this.MainGridView.ShowDeleteHyperlinkColumn = false;

            LLBLGenProDataSource datasource = this.DataSource as LLBLGenProDataSource;
            if (datasource != null)
            {
                PredicateExpression filter = new PredicateExpression(ClientConfigurationFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
                datasource.FilterToUse = filter;
            }
            this.MainGridView.Styles.Cell.HorizontalAlign = HorizontalAlign.Left;
        }        

        #endregion
    }
}
