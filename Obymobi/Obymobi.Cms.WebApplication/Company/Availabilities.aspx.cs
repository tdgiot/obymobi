﻿using System;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Company
{
    public partial class Availabilities : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "Availability";
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            LLBLGenProDataSource datasource = this.DataSource as LLBLGenProDataSource;
            if (datasource != null)
            {
                PredicateExpression filter = new PredicateExpression(AvailabilityFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
                datasource.FilterToUse = filter;
            }
            this.MainGridView.DataBound += this.MainGridView_DataBound;
        }

        private void MainGridView_DataBound(object sender, EventArgs e)
        {
            if (this.MainGridView.Columns["StatusInt"] != null)
            {
                this.MainGridView.Columns["StatusInt"].Visible = false;
            }

            if (this.MainGridView.Columns["StatusText"] != null)
            {
                this.MainGridView.Columns["StatusText"].Visible = false;
            }
        }
    }
}