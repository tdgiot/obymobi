﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.Messagegroup" Codebehind="Messagegroup.aspx.cs" %>
<%@ Register TagPrefix="D1" Namespace="Dionysos.Web.UI.DevExControls" Assembly="Dionysos.Web.DevExpress" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" runat="Server">
<div>
	<X:PageControl ID="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>				    
                    <D:PlaceHolder runat="server" ID="placeholder">                    
                    <table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblName">Name</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
							</td>
							<td class="label">
							    <D:Label runat="server" id="lblType">Type</D:Label>
							</td>
							<td class="control">
							    <D1:ComboBoxInt runat="server" id="cbType" IncrementalFilteringMode="Contains" UseDataBinding="True" IsRequired="True"></D1:ComboBoxInt>
							</td>   
					    </tr>       
                        <tr>
							<td class="label">
								<D:Label runat="server" id="lblUpdatedUTCLabel">Last Updated</D:Label>
							</td>
							<td class="control">
							    <D:Label ID="lblLastUpdated" runat="server" LocalizeText="False" />
							</td>
							<td class="label">
							    
							</td>
							<td class="control">
							    
							</td>
					    </tr>                                         
                        <D:PlaceHolder runat="server" ID="plhGroupName" Visible="False">
				        <tr>
				             <td class="label">				             
				             </td>   
                            <td class="control">				             
				             </td>   
                            <td class="label">				             
                                <D:Label runat="server" ID="lblGroupName">Group name</D:Label>
				             </td>   
                            <td class="control">				             
                                <D:TextBoxString runat="server" ID="tbGroupName" />
				             </td>   
				        </tr>
				        </D:PlaceHolder>       
                    </table>
                     <D:Panel ID="pnlDeliverypoints" runat="server" GroupingText="Deliverypoints">
                         <table class="dataformV2">
                              <tr>
                                <td class="label">
                                    <D:LabelTextOnly runat="server" id="lblDeliverypointNumbers">Deliverypoint numbers</D:LabelTextOnly>
							    </td>
                                <td class="control">
                                    <D:TextBoxInt ID="tbFrom" runat="server" ThousandsSeperators="false" style="width: 30px;" notdirty="true"></D:TextBoxInt>
                                    -
                                    <D:TextBoxInt ID="tbTo" runat="server" ThousandsSeperators="false" style="width: 30px;" notdirty="true"></D:TextBoxInt>
                                    
                                </td>	
                                <td class="label">
							    </td>
							    <td class="control">
							    </td>
                            </tr>
                            <tr>
                                 <td class="label">
                                    <D:LabelTextOnly runat="server" id="lblDeliverypointgroup">Deliverypointgroup</D:LabelTextOnly>
							    </td>
							    <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlDeliverypointgroupId" IncrementalFilteringMode="StartsWith" EntityName="Deliverypointgroup" TextField="Name" ValueField="DeliverypointgroupId" PreventEntityCollectionInitialization="true" notdirty="true" />
							    </td>
                                <td class="label">
							    </td>
							    <td class="control">
							    </td>
                            </tr>
                             <tr>
                                 <td class="label">
							    </td>
							    <td class="control">
                                    <D:Button ID="btnAddRange" runat="server" Text="Add range" />
                                    <D:Button ID="btnRemoveRange" runat="server" Text="Remove range" />
							    </td>
                                <td class="label">
							    </td>
							    <td class="control">
							    </td>
                            </tr>
                         </table>
                         <br />
                         <X:GridView ID="dpGrid" ClientInstanceName="dpGrid" runat="server" Width="100%" KeyFieldName="DeliverypointId">
                            <SettingsPager PageSize="20"></SettingsPager>
                            <SettingsBehavior AllowGroup="false" AllowDragDrop="false" AutoFilterRowInputDelay="350" />
                            <Columns>
                                <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="30px">
                                    <HeaderTemplate>
                                        <input type="checkbox" onclick="dpGrid.SelectAllRowsOnPage(this.checked);" title="Selecteer/deselecteer alle rijen op de pagina" />
                                    </HeaderTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                </dxwgv:GridViewCommandColumn>
                                <dxwgv:GridViewDataHyperLinkColumn FieldName="DeliverypointId" VisibleIndex="1" SortIndex="0" SortOrder="Ascending" Width="20%">
				                    <Settings AutoFilterCondition="Contains" />
                                    <Settings SortMode="DisplayText" />
                                    <Settings FilterMode="DisplayText" />
                                    <PropertiesHyperLinkEdit NavigateUrlFormatString="~/Company/Deliverypoint.aspx?id={0}" Target="_blank" TextField="DeliverypointNumber"  />
                                </dxwgv:GridViewDataHyperLinkColumn>                            
                                <dxwgv:GridViewDataColumn FieldName="DeliverypointName" VisibleIndex="2" Width="20%">
				                    <Settings AutoFilterCondition="Contains" />
                                </dxwgv:GridViewDataColumn>
                                <dxwgv:GridViewDataHyperLinkColumn FieldName="DeliverypointgroupId" VisibleIndex="3" Width="20%">
				                    <Settings AutoFilterCondition="Contains" />
                                    <PropertiesHyperLinkEdit NavigateUrlFormatString="~/Company/Deliverypointgroup.aspx?id={0}" Target="_blank" TextField="DeliverypointgroupName"  />
                                </dxwgv:GridViewDataHyperLinkColumn>   
                                 <dxwgv:GridViewCommandColumn ButtonType="Image" VisibleIndex="4" Width="20">
                                    <CustomButtons>
                                        <dxwgv:GridViewCommandColumnCustomButton ID="deleteDp" Text="Delete" >
                                            <Image Url="~/Images/Icons/delete.png" />
                                        </dxwgv:GridViewCommandColumnCustomButton>
                                    </CustomButtons>
                                </dxwgv:GridViewCommandColumn>
                            </Columns>
                            <Settings ShowFilterRow="True" />
                        </X:GridView>   
                         <div id="divDeleteItems" style="margin-top: 4px;">
                            <D:Button runat="server" ID="btnDeleteSelected" style="float:right;" Text="Delete selected items(s)" />
                        </div>        
                    </D:Panel>
                    </D:PlaceHolder>
                </Controls>
            </X:TabPage>
            <X:TabPage Text="Import" Name="Import">
				<Controls>				    
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:LabelTextOnly runat="server" id="LabelTextOnly1">Deliverypointgroup</D:LabelTextOnly>
                            </td>
                            <td class="control">
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="cbImportDeliverypointgroup" IncrementalFilteringMode="StartsWith" EntityName="Deliverypointgroup" TextField="Name" ValueField="DeliverypointgroupId" PreventEntityCollectionInitialization="true" notdirty="true" />
                            </td>
                        </tr>
						<tr>
							<td class="label">
							    <D:Label runat="server" id="lblNumbers">Numbers</D:Label>
							</td>
							<td class="control" colspan="3">
							    <D:TextBoxMultiLine runat="server" ID="tbNumbers" />
							</td>   
                            <td class="label">
                            </td>
                            <td class="control">
                                <D1:Button ID="btnImport" runat="server" Text="Import" ></D1:Button>
                            </td>
					    </tr>                                                                 				        
                    </table>
                </Controls>
            </X:TabPage>
        </TabPages>
    </X:PageControl>
</div>
</asp:Content>