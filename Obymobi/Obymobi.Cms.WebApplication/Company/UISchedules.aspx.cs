﻿using System;
using Obymobi.Cms.Logic.Factories;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Enums;

namespace Obymobi.ObymobiCms.Company
{
    public partial class UISchedules : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        #region Event handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.DataSource is LLBLGenProDataSource dataSource)
            {
                dataSource.FilterToUse = PredicateFactory.CreateUIScheduleFilter(CmsSessionHelper.CurrentCompanyId, UIScheduleType.Legacy);
            }

            if (CmsSessionHelper.CurrentRole <= Role.Reseller)
            {
                this.MainGridView.ShowDeleteHyperlinkColumn = false;
                this.MainGridView.ShowDeleteColumn = false;
            }
        }

        #endregion
    }
}
