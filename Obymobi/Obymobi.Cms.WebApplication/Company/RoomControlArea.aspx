﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.RoomControlArea" Codebehind="RoomControlArea.aspx.cs" %>
<%@ Reference VirtualPath="~/Company/SubPanels/RoomControlSectionCollection.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">    
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<controls>
					<table class="dataformV2">
						<tr>
						    <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblNameSystem">System name</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbNameSystem" runat="server" IsRequired="true"></D:TextBoxString>
							</td>							                    
                            <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblType">Type</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
                                <X:ComboBoxInt ID="cbType" runat="server" IsRequired="true" UseDataBinding="true"></X:ComboBoxInt>
							</td>				
						</tr>    
                        <tr>
                            <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblName">Naam</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbName" runat="server"></D:TextBoxString>
							</td>
                             <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblSortOrder">Sort order</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <D:TextBoxInt ID="tbSortOrder" runat="server" IsRequired="true" ThousandsSeperators="false"></D:TextBoxInt>                                
                            </td>
                        </tr>    
                        <tr>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblVisible">Visible</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <D:CheckBox runat="server" ID="cbVisible" />
                            </td>
                        </tr>                                        
					    <tr>
					        <td class="label">
					            <D:LabelEntityFieldInfo runat="server" id="lblIsNameSystemBaseId">System name is base id</D:LabelEntityFieldInfo>
					        </td>
					        <td class="control">
					            <D:CheckBox runat="server" ID="cbIsNameSystemBaseId" />
					        </td>
					    </tr> 
                    </table>
                </controls>                    
            </X:TabPage>
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>