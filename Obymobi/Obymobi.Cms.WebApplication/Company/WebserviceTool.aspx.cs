﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Dionysos;
using Dionysos.Web.UI;
using Obymobi.Cms.WebApplication.CraveService;
using Obymobi.Constants;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Security;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Company
{
    public partial class WebserviceTool : PageDefault
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.SetGui();
        }

        #region Methods

        public void Show()
        {
            this.PerformWebserviceCall(false);
        }

        public void Download()
        {
            this.PerformWebserviceCall(true);
        }

        void SetGui()
        {
            this.cbTerminal.EmptyItemText = "TRUE";
            this.cbClient.EmptyItemText = "TRUE";
            this.cbDeliverypointId.EmptyItemText = "TRUE";
            this.cbMethod.EmptyItemText = "TRUE";

            // Load Clients (Show ID - Mac - DP)
            ClientCollection clients = new ClientCollection();

            PredicateExpression filter = new PredicateExpression();
            filter.Add(ClientFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            filter.Add(ClientFields.DeviceId != DBNull.Value);

            SortExpression sort = new SortExpression();
            sort.Add(ClientFields.ClientId | SortOperator.Ascending);

            PrefetchPath path = new PrefetchPath(EntityType.ClientEntity);
            path.Add(ClientEntity.PrefetchPathDeliverypointEntity);
            path.Add(ClientEntity.PrefetchPathDeviceEntity);

            clients.GetMulti(filter, 0, sort, null, path);

            foreach (var client in clients)
            {
                string name = client.ClientId.ToString();

                if (client.DeviceId.HasValue)
                {
                    name += " - " + client.DeviceEntity.Identifier;
                }

                if (client.DeliverypointId.HasValue)
                {
                    name += " - " + client.DeliverypointEntity.Number;
                }

                this.cbClient.Items.Add(name, client.ClientId);
            }

            // Load Terminals
            TerminalCollection terminals = new TerminalCollection();

            filter = new PredicateExpression();
            filter.Add(TerminalFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            filter.Add(TerminalFields.DeviceId != DBNull.Value);

            sort = new SortExpression();
            sort.Add(TerminalFields.Name | SortOperator.Ascending);

            terminals.GetMulti(filter, 0, sort);

            foreach (var terminal in terminals)
            {
                this.cbTerminal.Items.Add(terminal.Name, terminal.TerminalId);
            }

            // Load Applications
            ApplicationCollection apps = new ApplicationCollection();
            apps.GetMulti(null, 0, new SortExpression(ApplicationFields.Name | SortOperator.Ascending));

            foreach (var app in apps)
            {
                this.cbApplication.Items.Add(app.Name, app.Code);
            }
            this.cbApplication.SelectedIndex = 0;

            // Load Deliverypoints
            DeliverypointCollection deliverypoints = new DeliverypointCollection();

            filter = new PredicateExpression();
            filter.Add(DeliverypointFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

            sort = new SortExpression();
            sort.Add(DeliverypointgroupFields.Name | SortOperator.Ascending);
            sort.Add(DeliverypointFields.Number | SortOperator.Ascending);

            RelationCollection joins = new RelationCollection();
            joins.Add(DeliverypointEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId);

            PrefetchPath prefetch = new PrefetchPath(EntityType.DeliverypointEntity);
            prefetch.Add(DeliverypointEntity.PrefetchPathDeliverypointgroupEntity);

            deliverypoints.GetMulti(filter, 0, sort, joins, prefetch);

            foreach (var dp in deliverypoints)
            {
                this.cbDeliverypointId.Items.Add("{0} - {1}".FormatSafe(dp.DeliverypointgroupEntity.Name, dp.Number), dp.DeliverypointId);
            }

            // Load Sites
            IncludeFieldsList siteFields = new IncludeFieldsList();
            siteFields.Add(SiteFields.Name);
            sort = new SortExpression(SiteFields.Name | SortOperator.Ascending);
            SiteCollection sites = new SiteCollection();
            sites.GetMulti(null, 0, sort, null, null, siteFields, 0, 0);

            foreach (var site in sites)
                this.cbSiteId.Items.Add(new DevExpress.Web.ListEditItem(site.Name, site.SiteId));

            IEnumerable<Obymobi.Culture> cultures = Obymobi.Culture.Mappings.Values.OrderBy(x => x.ToString());
            foreach (Obymobi.Culture culture in cultures)
            {
                this.ddlCultureCode.Items.Add(culture.ToString(), culture.Code);
            }

            // Webservice methods
            this.cbMethod.Items.Add("GetAdvertisements");
            this.cbMethod.Items.Add("GetAnnouncements");
            this.cbMethod.Items.Add("GetCategories");
            this.cbMethod.Items.Add("GetClient");
            this.cbMethod.Items.Add("GetClients");
            this.cbMethod.Items.Add("GetClientStatuses");
            this.cbMethod.Items.Add("GetCompanies");
            this.cbMethod.Items.Add("GetCompany");
            this.cbMethod.Items.Add("GetCurrentTime");
            this.cbMethod.Items.Add("GetDeliverypoints");
            this.cbMethod.Items.Add("GetEntertainment");
            this.cbMethod.Items.Add("GetEstimatedDeliveryTime");
            this.cbMethod.Items.Add("GetEstimatedDeliveryTimes");
            this.cbMethod.Items.Add("GetMenu");
            this.cbMethod.Items.Add("GetMessages");
            this.cbMethod.Items.Add("GetNetMessages");
            this.cbMethod.Items.Add("GetOrderHistory");
            this.cbMethod.Items.Add("GetOrders");
            this.cbMethod.Items.Add("GetPaymentmethods");
            this.cbMethod.Items.Add("GetProducts");
            this.cbMethod.Items.Add("GetRatings");
            this.cbMethod.Items.Add("GetRelease");
            this.cbMethod.Items.Add("GetReleaseAndDownloadLocation");
            this.cbMethod.Items.Add("GetSocialmediaMessages");
            this.cbMethod.Items.Add("GetSurveys");
            this.cbMethod.Items.Add("GetSite");
            this.cbMethod.Items.Add("GetPointOfInterest");
            this.cbMethod.Items.Add("GetTerminal");
            this.cbMethod.Items.Add("GetTerminalOperationMode");
            this.cbMethod.Items.Add("GetTerminals");
            this.cbMethod.Items.Add("GetTranslations");
            this.cbMethod.Items.Add("GetRoomControlConfiguration");
            this.cbMethod.Items.Add("GetPosIntegrationInformation");
            this.cbMethod.Items.Add("GetUISchedule");
            this.cbMethod.Items.Add("IsAlive");
            this.cbMethod.Items.Add("isDatabaseAlive");
            this.cbMethod.Items.Add("IsWebserviceAlive");
            this.cbMethod.Items.Add("ManuallyProcessOrder");
            this.cbMethod.Items.Add("ProcessOrders");
            this.cbMethod.Items.Add("SetClientDeliverypoint");
            this.cbMethod.Items.Add("VerifyNetmessage");
            this.cbMethod.Items.Add("VerifyNetmessageByGuid");
            this.cbMethod.Items.Add("WriteToLog");
        }

        private void PerformWebserviceCall(bool download)
        {
            if (this.cbClient.ValidId <= 0 && this.cbTerminal.ValidId <= 0)
                this.AddInformator(InformatorType.Warning, "Please select a client or a terminal before calling the webservice");
            else if (this.cbMethod.Value == null || this.cbMethod.Value.ToString().IsNullOrWhiteSpace())
                this.AddInformator(InformatorType.Warning, "Please select a webservice method before calling the webservice");
            else
            {
                // Do the magic
                string method = this.cbMethod.Value.ToString();
                using (CraveService service = new CraveService())
                {
                    if (this.tbWebserviceUrl.Text.IsNullOrWhiteSpace())
                    {
                        service.Url = WebEnvironmentHelper.GetApiCraveServiceUrl();
                    }
                    else
                    {
                        service.Url = this.tbWebserviceUrl.Text;
                    }

                    long timestamp = DateTimeUtil.ToUnixTime(DateTime.Now);
                    string salt = string.Empty;
                    string mac = string.Empty;
                    int companyId = 0;
                    ClientEntity client = null;
                    TerminalEntity terminal = null;
                    if (this.cbClient.ValidId > 0)
                    {
                        client = new ClientEntity(this.cbClient.ValidId);
                        mac = client.DeviceEntity.Identifier;
                        companyId = client.CompanyId;
                        salt = client.CompanyEntity.Salt;
                    }

                    if (this.cbTerminal.ValidId > 0)
                    {
                        terminal = new TerminalEntity(this.cbTerminal.ValidId);
                        mac = terminal.DeviceEntity.Identifier;
                        companyId = terminal.CompanyId;
                        salt = terminal.CompanyEntity.Salt;
                    }

                    string username = this.tbUsername.Text;
                    string password = this.tbPassword.Text;
                    bool credentailsComplete = (!username.IsNullOrWhiteSpace() && !password.IsNullOrWhiteSpace());
                    string resultCode = "";
                    string resultMessage = "";
                    string resultType = "";
                    string xml = "";
                    string hash;
                    string methodRequiresAClient = this.Translate("MethodRequiresClient", "Deze methode moet voor een Client worden uitgevoerd.");
                    string methodRequiresATerminal = this.Translate("MethodRequiresTerminal", "Deze methode moet voor een Terminal worden uitgevoerd.");
                    string methodRequiresUsernameAndPassword = this.Translate("MethodRequiresUsernameAndPassword", "Voor deze methode zijn inloggegevens vereist.");
                    string methodRequiresOrderGuid = this.Translate("MethodRequiresOrderGuid", "Voor deze methode is een geldige Order Guid vereist.");
                    string methodRequiresDeliverypoint = this.Translate("MethodRequiresDeliverypoint", "Voor deze methode is een tafel vereist");
                    string methodRequiresNetmessageId = this.Translate("MethodRequiresNetmessageId", "Voor deze methode is NetmessageId vereist");
                    string methodRequiresNetmessageGuid = this.Translate("MethodRequiresNetmessageGuid", "Voor deze methode is Netmessage Guid vereist");
                    string methodRequiresACulture = "This method requires a culture to be selected first";

                    Stopwatch sw = null;

                    switch (method)
                    {
                        case "GetAdvertisements":
                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac);
                            var getAdvertisementsResponse = service.GetAdvertisements(timestamp, mac, hash);
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(getAdvertisementsResponse.ModelCollection);
                            resultCode = getAdvertisementsResponse.ResultCode.ToString();
                            resultMessage = getAdvertisementsResponse.ResultMessage;
                            resultType = getAdvertisementsResponse.ResultType;
                            break;
                        case "GetAnnouncements":
                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac);
                            var getAnnouncementsResponse = service.GetAnnouncements(timestamp, mac, hash);
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(getAnnouncementsResponse.ModelCollection);
                            resultCode = getAnnouncementsResponse.ResultCode.ToString();
                            resultMessage = getAnnouncementsResponse.ResultMessage;
                            resultType = getAnnouncementsResponse.ResultType;
                            break;
                        case "GetCategories":
                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac);
                            var getCategoriesResponse = service.GetCategories(timestamp, mac, hash);
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(getCategoriesResponse.ModelCollection);
                            resultCode = getCategoriesResponse.ResultCode.ToString();
                            resultMessage = getCategoriesResponse.ResultMessage;
                            resultType = getCategoriesResponse.ResultType;
                            break;
                        case "GetClient":
                            if (client == null)
                            {
                                this.AddInformator(InformatorType.Warning, methodRequiresAClient);
                                break;
                            }

                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac, client.ClientId, client.DeviceEntity.Identifier, client.DeliverypointGroupId ?? 0, client.DeliverypointId ?? 0, string.Empty, string.Empty, (int)client.DeviceEntity.Type, 0);
                            var getClientResponse = service.GetClient(timestamp, mac, client.ClientId, client.DeviceEntity.Identifier, client.DeliverypointGroupId ?? 0, client.DeliverypointId ?? 0, string.Empty, string.Empty, (int)client.DeviceEntity.Type, 0, hash);
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(getClientResponse.ModelCollection);
                            resultCode = getClientResponse.ResultCode.ToString();
                            resultMessage = getClientResponse.ResultMessage;
                            resultType = getClientResponse.ResultType;
                            break;
                        case "GetClients":
                            if (terminal == null)
                            {
                                this.AddInformator(InformatorType.Warning, methodRequiresATerminal);
                                break;
                            }

                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac);
                            var getClientsResponse = service.GetClients(timestamp, mac, hash);
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(getClientsResponse.ModelCollection);
                            resultCode = getClientsResponse.ResultCode.ToString();
                            resultMessage = getClientsResponse.ResultMessage;
                            resultType = getClientsResponse.ResultType;
                            break;
                        case "GetCompanies":
                            if (!credentailsComplete)
                            {
                                this.AddInformator(InformatorType.Warning, methodRequiresUsernameAndPassword);
                                break;
                            }
                            salt = ObymobiConstants.GenericSalt;
                            hash = Hasher.GetHashFromParameters(salt, timestamp, username, password);
                            var getCompaniesResponse = service.GetCompanies(timestamp, username, password, hash);
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(getCompaniesResponse.ModelCollection);
                            resultCode = getCompaniesResponse.ResultCode.ToString();
                            resultMessage = getCompaniesResponse.ResultMessage;
                            resultType = getCompaniesResponse.ResultType;
                            break;
                        case "GetCompany":
                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac, companyId);
                            var getCompanyResult = service.GetCompany(timestamp, mac, companyId, hash);
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(getCompanyResult.ModelCollection);
                            resultCode = getCompanyResult.ResultCode.ToString();
                            resultMessage = getCompanyResult.ResultMessage;
                            resultType = getCompanyResult.ResultType;
                            break;
                        case "GetCurrentTime":
                            var getCurrentTimeResponse = service.GetCurrentTime();
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(getCurrentTimeResponse.ModelCollection);
                            resultCode = getCurrentTimeResponse.ResultCode.ToString();
                            resultMessage = getCurrentTimeResponse.ResultMessage;
                            resultType = getCurrentTimeResponse.ResultType;
                            break;
                        case "GetDeliverypoints":
                            if (client == null)
                            {
                                this.AddInformator(InformatorType.Warning, methodRequiresAClient);
                                break;
                            }

                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac, client.DeliverypointGroupId ?? 0);
                            var getDeliverypointsResult = service.GetDeliverypoints(timestamp, mac, client.DeliverypointGroupId ?? 0, hash);
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(getDeliverypointsResult.ModelCollection);
                            resultCode = getDeliverypointsResult.ResultCode.ToString();
                            resultMessage = getDeliverypointsResult.ResultMessage;
                            resultType = getDeliverypointsResult.ResultType;
                            break;
                        case "GetEntertainment":
                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac);
                            var getEntertainmentResult = service.GetEntertainment(timestamp, mac, hash);
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(getEntertainmentResult.ModelCollection);
                            resultCode = getEntertainmentResult.ResultCode.ToString();
                            resultMessage = getEntertainmentResult.ResultMessage;
                            resultType = getEntertainmentResult.ResultType;
                            break;
                        case "GetEstimatedDeliveryTime":
                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac);
                            var getEstimatedDeliveryTimeResult = service.GetEstimatedDeliveryTime(timestamp, mac, client.DeliverypointGroupId ?? 0, hash);
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(getEstimatedDeliveryTimeResult.ModelCollection);
                            resultCode = getEstimatedDeliveryTimeResult.ResultCode.ToString();
                            resultMessage = getEstimatedDeliveryTimeResult.ResultMessage;
                            resultType = getEstimatedDeliveryTimeResult.ResultType;
                            break;
                        case "GetEstimatedDeliveryTimes":
                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac);
                            var getEstimatedDeliveryTimesResult = service.GetEstimatedDeliveryTimes(timestamp, mac, hash);
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(getEstimatedDeliveryTimesResult.ModelCollection);
                            resultCode = getEstimatedDeliveryTimesResult.ResultCode.ToString();
                            resultMessage = getEstimatedDeliveryTimesResult.ResultMessage;
                            resultType = getEstimatedDeliveryTimesResult.ResultType;
                            break;
                        case "GetMenu":
                            int deliverypointGroupId = 0;
                            if (client.DeliverypointId.HasValue)
                                deliverypointGroupId = client.DeliverypointEntity.DeliverypointgroupId;
                            else if (client.DeliverypointGroupId.HasValue)
                                deliverypointGroupId = client.DeliverypointGroupId.Value;
                            else if (terminal.CompanyEntity.DeliverypointgroupCollection.Count > 0)
                                deliverypointGroupId = terminal.CompanyEntity.DeliverypointgroupCollection[0].DeliverypointgroupId;

                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac, deliverypointGroupId);
                            var getMenuResponse = service.GetMenu(timestamp, mac, deliverypointGroupId, hash);
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(getMenuResponse.ModelCollection);
                            resultCode = getMenuResponse.ResultCode.ToString();
                            resultMessage = getMenuResponse.ResultMessage;
                            if (TestUtil.IsPcGabriel)
                                resultMessage += "- {0}, {1}, {2}, {3}".FormatSafe(timestamp, mac, deliverypointGroupId, hash);
                            resultType = getMenuResponse.ResultType;
                            break;
                        case "GetMessages":
                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac);
                            var getMessagesResponse = service.GetEntertainment(timestamp, mac, hash);
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(getMessagesResponse.ModelCollection);
                            resultCode = getMessagesResponse.ResultCode.ToString();
                            resultMessage = getMessagesResponse.ResultMessage;
                            resultType = getMessagesResponse.ResultType;
                            break;
                        case "GetNetMessages":
                            int clientId = 0;
                            int terminalId = 0;
                            if (client != null)
                            {
                                clientId = client.ClientId;
                                companyId = client.CompanyId;
                            }
                            if (terminal != null)
                            {
                                terminalId = terminal.TerminalId;
                                companyId = terminal.CompanyId;
                            }

                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac, 0, clientId, companyId, 0, terminalId);

                            sw = Stopwatch.StartNew();

                            var getNetMessagesResponse = service.GetNetmessages(timestamp, mac, 0, clientId, companyId, 0, terminalId, hash);

                            sw.Stop();

                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(getNetMessagesResponse.ModelCollection);
                            resultCode = getNetMessagesResponse.ResultCode.ToString();
                            resultMessage = getNetMessagesResponse.ResultMessage;
                            resultType = getNetMessagesResponse.ResultType;
                            break;
                        case "GetOrderHistory":
                            if (terminal == null)
                            {
                                this.AddInformator(InformatorType.Warning, methodRequiresATerminal);
                                break;
                            }
                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac);
                            var getOrderHistoryResponse = service.GetOrderHistory(timestamp, mac, hash);
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(getOrderHistoryResponse.ModelCollection);
                            resultCode = getOrderHistoryResponse.ResultCode.ToString();
                            resultMessage = getOrderHistoryResponse.ResultMessage;
                            resultType = getOrderHistoryResponse.ResultType;
                            break;
                        case "GetOrders":
                            if (terminal == null)
                            {
                                this.AddInformator(InformatorType.Warning, methodRequiresATerminal);
                                break;
                            }
                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac, 0, string.Empty, string.Empty, true, false);
                            var getOrdersResponse = service.GetOrders(timestamp, mac, 0, string.Empty, string.Empty, true, false, hash);
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(getOrdersResponse.ModelCollection);
                            resultCode = getOrdersResponse.ResultCode.ToString();
                            resultMessage = getOrdersResponse.ResultMessage;
                            resultType = getOrdersResponse.ResultType;
                            break;
                        case "GetPaymentmethods":
                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac);
                            var getPaymentmethodsResponse = service.GetPaymentmethods(timestamp, mac, hash);
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(getPaymentmethodsResponse.ModelCollection);
                            resultCode = getPaymentmethodsResponse.ResultCode.ToString();
                            resultMessage = getPaymentmethodsResponse.ResultMessage;
                            resultType = getPaymentmethodsResponse.ResultType;
                            break;
                        case "GetProducts":
                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac);
                            var getProductsResponse = service.GetProducts(timestamp, mac, hash);
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(getProductsResponse.ModelCollection);
                            resultCode = getProductsResponse.ResultCode.ToString();
                            resultMessage = getProductsResponse.ResultMessage;
                            resultType = getProductsResponse.ResultType;
                            break;
                        case "GetRelease":
                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac, this.cbApplication.Value.ToString());
                            var getReleaseResponse = service.GetRelease(timestamp, mac, this.cbApplication.Value.ToString(), hash);
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(getReleaseResponse.ModelCollection);
                            resultCode = getReleaseResponse.ResultCode.ToString();
                            resultMessage = getReleaseResponse.ResultMessage;
                            resultType = getReleaseResponse.ResultType;
                            break;
                        case "GetReleaseAndDownloadLocation":
                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac, this.cbApplication.Value.ToString());
                            var getReleaseAndLocationResponse = service.GetReleaseAndDownloadLocation(timestamp, mac, this.cbApplication.Value.ToString(), hash);
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(getReleaseAndLocationResponse);
                            break;
                        case "GetClientStatuses":
                            if (terminal == null)
                            {
                                this.AddInformator(InformatorType.Warning, methodRequiresATerminal);
                                break;
                            }
                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac);
                            var getSetClientStatusResponse = service.GetClientStatuses(timestamp, mac, hash);
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(getSetClientStatusResponse.ModelCollection);
                            resultCode = getSetClientStatusResponse.ResultCode.ToString();
                            resultMessage = getSetClientStatusResponse.ResultMessage;
                            resultType = getSetClientStatusResponse.ResultType;
                            break;
                        case "GetSocialmediaMessages":
                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac);
                            var getSocialmediaMessagesResponse = service.GetSocialmediaMessages(timestamp, mac, hash);
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(getSocialmediaMessagesResponse.ModelCollection);
                            resultCode = getSocialmediaMessagesResponse.ResultCode.ToString();
                            resultMessage = getSocialmediaMessagesResponse.ResultMessage;
                            resultType = getSocialmediaMessagesResponse.ResultType;
                            break;
                        case "GetSurveys":
                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac);
                            var getSurveysResponse = service.GetSurveys(timestamp, mac, hash);
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(getSurveysResponse.ModelCollection);
                            resultCode = getSurveysResponse.ResultCode.ToString();
                            resultMessage = getSurveysResponse.ResultMessage;
                            resultType = getSurveysResponse.ResultType;
                            break;
                        case "GetTerminal":
                            if (terminal == null)
                            {
                                this.AddInformator(InformatorType.Warning, methodRequiresATerminal);
                                break;
                            }
                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac, terminal.TerminalId, string.Empty, string.Empty, 0);
                            var getTerminalResponse = service.GetTerminal(timestamp, mac, terminal.TerminalId, string.Empty, string.Empty, 0, hash);
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(getTerminalResponse.ModelCollection);
                            resultCode = getTerminalResponse.ResultCode.ToString();
                            resultMessage = getTerminalResponse.ResultMessage;
                            resultType = getTerminalResponse.ResultType;
                            break;
                        case "GetTerminalOperationMode":
                            if (terminal == null)
                            {
                                this.AddInformator(InformatorType.Warning, methodRequiresATerminal);
                                break;
                            }
                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac);
                            var getTerminalOperationMode = service.GetTerminalOperationMode(timestamp, mac, hash);
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(getTerminalOperationMode.ModelCollection);
                            resultCode = getTerminalOperationMode.ResultCode.ToString();
                            resultMessage = getTerminalOperationMode.ResultMessage;
                            resultType = getTerminalOperationMode.ResultType;
                            break;
                        case "GetTerminals":
                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac);
                            var getTerminalsResponse = service.GetTerminals(timestamp, mac, hash);
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(getTerminalsResponse.ModelCollection);
                            resultCode = getTerminalsResponse.ResultCode.ToString();
                            resultMessage = getTerminalsResponse.ResultMessage;
                            resultType = getTerminalsResponse.ResultType;
                            break;
                        case "GetTranslations":
                            if (this.ddlCultureCode.Value == null || this.ddlCultureCode.Value.ToString().IsNullOrWhiteSpace())
                            {
                                this.AddInformator(InformatorType.Warning, methodRequiresACulture);
                                break;
                            }
                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac, this.ddlCultureCode.Value.ToString());
                            var getTranslationsResponse = service.GetTranslations(timestamp, mac, this.ddlCultureCode.Value.ToString(), hash);
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(getTranslationsResponse.ModelCollection);
                            resultCode = getTranslationsResponse.ResultCode.ToString();
                            resultMessage = getTranslationsResponse.ResultMessage;
                            resultType = getTranslationsResponse.ResultType;
                            break;
                        case "GetSite":
                            if (this.ddlCultureCode.Value == null || this.ddlCultureCode.Value.ToString().IsNullOrWhiteSpace())
                            {
                                this.AddInformator(InformatorType.Warning, methodRequiresACulture);
                                break;
                            }
                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac, this.cbSiteId.ValidId, (int)DeviceType.SamsungP5110, this.ddlCultureCode.Value.ToString());
                            sw = Stopwatch.StartNew();
                            var getSiteResponse = service.GetSite(timestamp, mac, this.cbSiteId.ValidId, (int)DeviceType.SamsungP5110, this.ddlCultureCode.Value.ToString(), hash);
                            sw.Stop();
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(getSiteResponse.ModelCollection);
                            resultCode = getSiteResponse.ResultCode.ToString();
                            resultMessage = getSiteResponse.ResultMessage;
                            resultType = getSiteResponse.ResultType;
                            break;
                        case "GetPointOfInterest":
                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac, 1, (int)DeviceType.SamsungP5110);
                            var getPoiResponse = service.GetPointOfInterest(timestamp, mac, 1, (int)DeviceType.SamsungP5110, hash);
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(getPoiResponse.ModelCollection);
                            resultCode = getPoiResponse.ResultCode.ToString();
                            resultMessage = getPoiResponse.ResultMessage;
                            resultType = getPoiResponse.ResultType;
                            break;
                        case "GetRoomControlConfiguration":
                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac);
                            var getRoomControlConfigurationResponse = service.GetRoomControlConfiguration(timestamp, mac, hash);
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(getRoomControlConfigurationResponse.ModelCollection);
                            resultCode = getRoomControlConfigurationResponse.ResultCode.ToString();
                            resultMessage = getRoomControlConfigurationResponse.ResultMessage;
                            resultType = getRoomControlConfigurationResponse.ResultType;
                            break;
                        case "GetPosIntegrationInformation":
                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac);
                            var getPosIntegrationInformationResponse = service.GetPosIntegrationInformation(timestamp, mac, hash);
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(getPosIntegrationInformationResponse.ModelCollection);
                            resultCode = getPosIntegrationInformationResponse.ResultCode.ToString();
                            resultMessage = getPosIntegrationInformationResponse.ResultMessage;
                            resultType = getPosIntegrationInformationResponse.ResultType;
                            break;
                        case "IsAlive":
                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac);
                            var isAliveResponse = service.IsAlive();
                            xml = isAliveResponse.ToString();
                            break;
                        case "isDatabaseAlive":
                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac);
                            var isDatabaseAliveResponse = service.IsDatabaseAlive();
                            xml = isDatabaseAliveResponse.ToString();
                            break;
                        case "IsWebserviceAlive":
                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac);
                            var isWebserviceAliveResponse = service.IsWebserviceAlive();
                            xml = isWebserviceAliveResponse.ToString();
                            break;
                        case "ManuallyProcessOrder":
                            if (terminal == null)
                            {
                                this.AddInformator(InformatorType.Warning, methodRequiresATerminal);
                                break;
                            }
                            else if (this.tbOrderGuid.Text.IsNullOrWhiteSpace())
                            {
                                this.AddInformator(InformatorType.Warning, methodRequiresOrderGuid);
                                break;
                            }

                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac, companyId, terminal.TerminalId, this.tbOrderGuid.Text);
                            var manuallyProcessOrderResponse = service.ManuallyProcessOrder(timestamp, mac, companyId, terminal.TerminalId, this.tbOrderGuid.Text, hash);
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(manuallyProcessOrderResponse.ModelCollection);
                            resultCode = manuallyProcessOrderResponse.ResultCode.ToString();
                            resultMessage = manuallyProcessOrderResponse.ResultMessage;
                            resultType = manuallyProcessOrderResponse.ResultType;
                            break;
                        case "ProcessOrders":
                            if (client == null)
                            {
                                this.AddInformator(InformatorType.Warning, methodRequiresAClient);
                                break;
                            }
                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac, companyId, client.ClientId);
                            var processOrdersResponse = service.ProcessOrders(timestamp, mac, companyId, client.ClientId, hash);
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(processOrdersResponse.ModelCollection);
                            resultCode = processOrdersResponse.ResultCode.ToString();
                            resultMessage = processOrdersResponse.ResultMessage;
                            resultType = processOrdersResponse.ResultType;
                            break;
                        case "SetClientDeliverypoint":
                            if (client == null)
                            {
                                this.AddInformator(InformatorType.Warning, methodRequiresAClient);
                                break;
                            }
                            else if (this.cbDeliverypointId.ValidId <= 0)
                            {
                                this.AddInformator(InformatorType.Warning, methodRequiresDeliverypoint);
                                break;
                            }

                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac, this.cbDeliverypointId.ValidId);
                            var setClientDeliverypoint = service.SetClientDeliverypoint(timestamp, mac, this.cbDeliverypointId.ValidId, -1, hash);
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(setClientDeliverypoint.ModelCollection);
                            resultCode = setClientDeliverypoint.ResultCode.ToString();
                            resultMessage = setClientDeliverypoint.ResultMessage;
                            resultType = setClientDeliverypoint.ResultType;
                            break;
                        case "VerifyNetmessage":
                            if (!this.tbNetmessageId.Value.HasValue)
                            {
                                this.AddInformator(InformatorType.Warning, methodRequiresNetmessageId);
                                break;
                            }
                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac, this.tbNetmessageId.Value.Value);

                            sw = Stopwatch.StartNew();

                            var verifyNetmessageResponse = service.VerifyNetmessage(timestamp, mac, this.tbNetmessageId.Value.Value, hash);

                            sw.Stop();

                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(verifyNetmessageResponse.ModelCollection);
                            resultCode = verifyNetmessageResponse.ResultCode.ToString();
                            resultMessage = verifyNetmessageResponse.ResultMessage;
                            resultType = verifyNetmessageResponse.ResultType;
                            break;
                        case "VerifyNetmessageByGuid":
                            if (this.tbNetmessageGuid.Text.IsNullOrWhiteSpace())
                            {
                                this.AddInformator(InformatorType.Warning, methodRequiresNetmessageGuid);
                                break;
                            }
                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac, this.tbNetmessageGuid.Text);
                            var verifyNetmessageByGuidResponse = service.VerifyNetmessageByGuid(timestamp, mac, this.tbNetmessageGuid.Text, "", hash);
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(verifyNetmessageByGuidResponse.ModelCollection);
                            resultCode = verifyNetmessageByGuidResponse.ResultCode.ToString();
                            resultMessage = verifyNetmessageByGuidResponse.ResultMessage;
                            resultType = verifyNetmessageByGuidResponse.ResultType;
                            break;
                        case "WriteToLog":
                            if (terminal == null)
                            {
                                this.AddInformator(InformatorType.Warning, methodRequiresATerminal);
                                break;
                            }
                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac, companyId, terminal.TerminalId, 0, string.Empty, (int)TerminalLogType.Message, "Test Message", "Status", "Test Log");
                            var writeToLogResponse = service.WriteToLog(timestamp, mac, companyId, terminal.TerminalId, 0, string.Empty, (int)TerminalLogType.Message, "Test Message", "Status", "Test Log", hash);
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(writeToLogResponse.ModelCollection);
                            resultCode = writeToLogResponse.ResultCode.ToString();
                            resultMessage = writeToLogResponse.ResultMessage;
                            resultType = writeToLogResponse.ResultType;
                            break;
                        case "GetUISchedule":
                            deliverypointGroupId = 0;
                            if (client.DeliverypointId.HasValue)
                                deliverypointGroupId = client.DeliverypointEntity.DeliverypointgroupId;
                            else if (client.DeliverypointGroupId.HasValue)
                                deliverypointGroupId = client.DeliverypointGroupId.Value;

                            hash = Hasher.GetHashFromParameters(salt, timestamp, mac, deliverypointGroupId);
                            var getUIScheduleResponse = service.GetUISchedule(timestamp, mac, deliverypointGroupId, hash);
                            xml = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(getUIScheduleResponse.ModelCollection);
                            resultCode = getUIScheduleResponse.ResultCode.ToString();
                            resultMessage = getUIScheduleResponse.ResultMessage;
                            resultType = getUIScheduleResponse.ResultType;
                            break;
                        default:
                            throw new NotImplementedException();
                    }

                    if (download)
                    {
                        this.Response.Clear();
                        Response.AddHeader("content-disposition", "attachment;filename={0}-{1}.xml".FormatSafe(this.cbMethod.Text, DateTime.Now.Ticks));
                        this.Response.Write(xml);
                        this.Response.ContentType = "text/xml";
                        this.Response.End();
                    }
                    else
                    {
                        this.plhReponse.AddHtml("\r\n", resultCode);
                        this.plhReponse.AddHtml("Webservice url: " + service.Url);
                        this.plhReponse.AddHtml("Result code: {0}\r\n", resultCode);
                        this.plhReponse.AddHtml("Result message: {0}\r\n", resultMessage);
                        this.plhReponse.AddHtml("Result type: {0}\r\n", resultType);
                        if (sw != null && sw.ElapsedMilliseconds > 0)
                            this.plhReponse.AddHtml("Elapsed: {0}\r\n", sw.Elapsed.ToString());
                        this.plhReponse.AddHtml(xml.HtmlEncode());
                    }
                }
            }
        }

        #endregion

        #region Event handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            //this.HookupEvents();
        }


        #endregion
    }
}

