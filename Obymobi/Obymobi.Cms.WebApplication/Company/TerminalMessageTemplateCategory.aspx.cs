﻿using Dionysos.Web.UI;
using Obymobi.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Cms.Logic.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.ObymobiCms.Company
{
    public partial class TerminalMessageTemplateCategory : PageLLBLGenEntity
    {
        #region EventHandlers

        protected void Page_Load(object sender, EventArgs e)
        {
            // Redirect the user if it's not an administrator
            if (CmsSessionHelper.CurrentRole < Role.Reseller)
                Response.Redirect("~/401.aspx");
            
            this.BindTerminalDropdown();
            this.BindMessageTemplateCategoryDropdown();
        }

        private void BindTerminalDropdown()
        {
            TerminalCollection collection = TerminalHelper.GetTerminalCollection(CmsSessionHelper.CurrentCompanyId);
            this.ddlTerminalId.DataSource = collection;
            this.ddlTerminalId.DataBind();
        }

        private void BindMessageTemplateCategoryDropdown()
        {
            MessageTemplateCategoryCollection collection = MessageTemplateCategoryHelper.GetMessageTemplateCategoryCollection(CmsSessionHelper.CurrentCompanyId);
            this.ddlMessageTemplateCategoryId.DataSource = collection;
            this.ddlMessageTemplateCategoryId.DataBind();
        }

        #endregion
    }
}