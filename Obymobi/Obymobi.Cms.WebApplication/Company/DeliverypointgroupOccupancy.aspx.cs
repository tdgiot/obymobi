﻿using System;
using System.Web.UI.WebControls;
using DevExpress.Web;
using Dionysos.Web;

namespace Obymobi.ObymobiCms.Company
{
    public partial class DeliverypointgroupOccupancy : Dionysos.Web.UI.PageQueryStringDataBinding
    {
        private const string DATE_SESSION_KEY = "Occupancy_Date_Filter";

        private DeliverypointgroupOccupancyGridManager deliverypointgroupOccupancyGridManager;
        private BaseDataSource<DeliverypointgroupOccupancyGridManager> deliverypointgroupOccupancyGridDataSource;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                de.Date = GetFilterDate();
                //CreateGridView();
            }

            this.de.DateChanged += de_DateChanged;
        }

        private DateTime GetFilterDate()
        {
            DateTime filterDate;
            if (!SessionHelper.TryGetValue(DATE_SESSION_KEY, out filterDate))
                filterDate = DateTime.Now;

            return filterDate;
        }

        protected void grid_Init(object sender, EventArgs e)
        {
            CreateGridView();
        }

        protected void grid_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            if (e.Column.FieldName == "Deliverypointgroup")
            {
                e.Editor.ReadOnly = true;
            }
        }

        void de_DateChanged(object sender, EventArgs e)
        {
            SessionHelper.SetValue(DATE_SESSION_KEY, de.Date);

            UpdateGridView();
        }

        protected override void SetDefaultValuesToControls()
        {
            UpdateGridView();
        }

        protected void CreateGridView()
        {
            this.grid.Columns.Clear();
            
            var dpgName = new GridViewDataTextColumn();
            dpgName.Caption = "Deliverypointgroup";
            dpgName.FieldName = "Deliverypointgroup";
            dpgName.Index = 0;
            dpgName.ReadOnly = true;
            this.grid.Columns.Add(dpgName);

            for (int i = 1; i <= 31; i++)
            {
                var dayColumn = new GridViewDataTextColumn();
                dayColumn.Caption = "" + i;
                dayColumn.FieldName = "d" + i;
                dayColumn.Width = 20;
                dayColumn.Index = i;
                dayColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                dayColumn.CellStyle.HorizontalAlign = HorizontalAlign.Center;
                this.grid.Columns.Add(dayColumn);
            }

            this.grid.KeyFieldName = "DeliverypointgroupId";
            this.grid.SettingsEditing.Mode = GridViewEditingMode.Batch;
            this.grid.SettingsBehavior.AllowSort = false;

            this.grid.CustomErrorText += grid_CustomErrorText;

            InitDataSource();
        }

        void grid_CustomErrorText(object sender, ASPxGridViewCustomErrorTextEventArgs e)
        {
            if (e.Exception.InnerException != null)
            {
                var exception = e.Exception.InnerException as ObymobiException;
                if (exception != null)
                {
                    e.ErrorText = exception.AdditionalMessage;
                }
                else
                {
                    e.ErrorText = e.Exception.InnerException.Message;
                }
            }
        }

        public void UpdateGridView()
        {
            int daysToRender = DateTime.DaysInMonth(GetFilterDate().Year, GetFilterDate().Month);
            if (GetFilterDate().Month == DateTime.Today.Month)
            {
                daysToRender = DateTime.Today.Day;
            }
            else if (GetFilterDate() > DateTime.Today)
            {
                daysToRender = 0;
            }

            for (int i = 1; i <= 31; i++)
            {
                var column = (GridViewDataTextColumn)this.grid.Columns["d" + i];
                column.ReadOnly = (i > daysToRender);
            }

            RefreshDataSource();
        }

        private void InitDataSource()
        {
            this.deliverypointgroupOccupancyGridManager = new DeliverypointgroupOccupancyGridManager(CmsSessionHelper.CurrentCompanyId, GetFilterDate());
            this.deliverypointgroupOccupancyGridDataSource = new BaseDataSource<DeliverypointgroupOccupancyGridManager>(deliverypointgroupOccupancyGridManager);
            this.deliverypointgroupOccupancyGridDataSource.DataObjectTypeName = typeof(DeliverypointgroupOccupancyGridManager.OccupancyRow).FullName;
        }

        public void RefreshDataSource()
        {
            this.deliverypointgroupOccupancyGridManager.SetOccupancyDate(GetFilterDate());
            this.grid.DataSource = this.deliverypointgroupOccupancyGridDataSource;
            this.grid.DataBind();
        }
    }
}