using System;
using System.Globalization;
using Dionysos;
using Dionysos.Data.LLBLGen;
using Obymobi.Cms.WebApplication.Extensions;
using Obymobi.Cms.WebApplication.Old_App_Code;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Extensions;
using Obymobi.Logic.Comet;
using Obymobi.Logic.Comet.Netmessages;
using Obymobi.Logic.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Company
{
    public partial class Client : Dionysos.Web.UI.PageLLBLGenEntity
    {
        private const int SWITCH_TO_WEBSERVICE_COMMAND = 99997;
        private const int SWITCH_TO_SIGNALR_COMMAND = 99998;
        private const int CLOSE_SUPPORT_TOOLS = 99999;

        #region Methods

        protected override void OnInit(EventArgs e)
        {
            LoadUserControls();
            this.DataSourceLoaded += Client_DataSourceLoaded;

            base.OnInit(e);
            SetGui();

            SetWarnings();
        }

        private void LoadUserControls()
        {
            //Only enable this if the button to populate this combo box with all possible DP & DPGs has been clicked
            this.cbDeliverypointgroupAndDeliverypoint.Enabled = false;

            this.btnChangeDeliverypointDeliverypointGroup.Visible = CmsSessionHelper.CurrentCompanyId > 0;
        }

        private void RenderDeviceInfo(DeviceEntity device)
        {
            this.lblLastRequestValue.Text = device.LastRequestUTC.HasValue ? device.LastRequestUTC.Value.UtcToLocalTime().ToString(CultureInfo.InvariantCulture) : "Unknown";
            this.lblLastSupportToolsRequestValue.Text = device.LastSupportToolsRequestUTC.HasValue ? device.LastSupportToolsRequestUTC.Value.UtcToLocalTime().ToString(CultureInfo.InvariantCulture) : "Unknown";
            this.lblLastPrivateIpAddressesValue.Text = device.PrivateIpAddresses;
            this.lblLastPublicIpAddressValue.Text = device.PublicIpAddress;
            this.lblLastBatteryLevelValue.Text = device.BatteryLevel.HasValue ? device.BatteryLevel.Value + "%" : "Unknown";
            this.lblLastApplicationVersionValue.Text = !device.ApplicationVersion.IsNullOrWhiteSpace() ? new VersionNumber(device.ApplicationVersion).NumberAsString : "Unknown";
            this.lblLastSupportToolsVersionValue.Text = !device.SupportToolsVersion.IsNullOrWhiteSpace() ? new VersionNumber(device.SupportToolsVersion).NumberAsString : "Unknown";
            this.lblLastAgentVersionValue.Text = !device.AgentVersion.IsNullOrWhiteSpace() ? new VersionNumber(device.AgentVersion).NumberAsString : "Unknown";
            this.lblLastMessagingServiceVersionValue.Text = !device.MessagingServiceVersion.IsNullOrWhiteSpace() ? new VersionNumber(device.MessagingServiceVersion).NumberAsString : "Unknown";
            this.lblLastMessagingServiceRequestValue.Text = device.LastMessagingServiceRequest.HasValue ? device.LastMessagingServiceRequest.Value.UtcToLocalTime().ToString(CultureInfo.InvariantCulture) : "Unknown";
            this.lblLastOsVersionValue.Text = !device.OsVersion.IsNullOrWhiteSpace() ? new VersionNumber(device.OsVersion).NumberAsString : "Unknown";
            this.lblLastWifiStrengthValue.Text = string.Format("{0} ({1})", WifiStrengthToText(device.WifiStrength), device.WifiStrength.GetValueOrDefault(-1));
            this.lblLastCommunicationMethodValue.Text = device.CommunicationMethod.ToEnum<ClientCommunicationMethod>().ToString();
            this.lblLastCloudEnvironmentValue.Text = device.CloudEnvironment;
            this.lblDeviceTypeValue.Text = device.TypeText;
            this.lblDeviceModelValue.Text = device.DeviceModelText ?? "Unknown";

            this.lblIsChargingYes.Visible = device.IsCharging;
            this.lblIsChargingNo.Visible = !device.IsCharging;
            this.lblDevelopmentModeNo.Visible = !device.IsDevelopment;
            this.lblDevelopmentModeYes.Visible = device.IsDevelopment;

            this.dllDeviceId.DataSource = new DeviceCollection(new[] { device });
            this.dllDeviceId.DataBind();

            if (!device.MessagingServiceVersion.IsNullOrWhiteSpace())
            {
                this.pnlMessagingService.Visible = true;

                this.lblLastMessagingConnectionType.Text = device.LastMessagingServiceConnectionType.ToEnum<MessagingServiceConnectionType>().ToString();
                this.lblLastMessagingConnectionStatus.Text = device.LastMessagingServiceConnectionStatus.ToEnum<MessagingServiceConnectionStatus>().ToString();
                this.lblLastMessagingSuccessfulConnection.Text = device.LastMessagingServiceSuccessfulConnection?.UtcToLocalTime().ToString(CultureInfo.InvariantCulture) ?? "Unknown";

                this.lblMessagingConnectingCount24h.Text = device.MessagingServiceConnectingCount24h.ToString();
                this.lblMessagingConnectedCount24h.Text = device.MessagingServiceConnectedCount24h.ToString();
                this.lblMessagingReconnectingCount24h.Text = device.MessagingServiceReconnectingCount24h.ToString();
                this.lblMessagingConnectionLostCount24h.Text = device.MessagingServiceConnectionLostCount24h.ToString();
            }
        }

        private void SetGui()
        {
            if (this.PageMode != Dionysos.Web.PageMode.Add)
            {
                ClientEntity clientEntity = this.DataSourceAsClientEntity;
                DeviceEntity deviceEntity = this.DataSourceAsClientEntity.DeviceEntity;

                this.cbDeliverypointgroupAndDeliverypoint.Items.Add(CreateListEditItemForCurrentDeliverypoint());

                if (this.DataSourceAsClientEntity.DeviceId.HasValue)
                {
                    RenderDeviceInfo(deviceEntity);
                }

                if (this.DataSourceAsClientEntity.LastStatus.HasValue)
                {
                    this.lblLastStatusText.Text = this.DataSourceAsClientEntity.LastStatusText;
                }

                this.lblLastOperationMode.Text = "?";
                if (this.DataSourceAsClientEntity.LastOperationMode.HasValue)
                {
                    if (EnumUtil.TryParse(this.DataSourceAsClientEntity.LastOperationMode.Value, out ClientOperationMode operationMode))
                    {
                        this.lblLastOperationModeValue.Text = operationMode.ToString();
                    }
                }

                if (this.DataSourceAsClientEntity.DeliverypointId.HasValue)
                {
                    this.lblLastDeliverypointNumberValue.Text = this.DataSourceAsClientEntity.DeliverypointEntity.Number;
                }

                this.lblLoadedSuccessfullyLabelYes.Visible = this.DataSourceAsClientEntity.LoadedSuccessfully;
                this.lblLoadedSuccessfullyLabelNo.Visible = !this.DataSourceAsClientEntity.LoadedSuccessfully;
                this.lblLastWifiSsidValue.Value = this.DataSourceAsClientEntity.LastWifiSsid.IsNullOrWhiteSpace() ? "Unknown" : this.DataSourceAsClientEntity.LastWifiSsid;
            }
            else
            {
                if (this.tabsMain.TabPages.FindByName("tabCustomer") != null)
                {
                    this.tabsMain.TabPages.FindByName("tabCustomer").Visible = false;
                }
            }

            // Load companies
            CompanyCollection companies = new CompanyCollection();
            PredicateExpression companyFilter = new PredicateExpression
            {
                CompanyFields.CompanyId == CmsSessionHelper.CurrentCompanyId
            };
            companies.GetMulti(companyFilter);

            this.ddlCompanyId.DataSource = companies;
            this.ddlCompanyId.DataBind();
            this.ddlCompanyId.SelectedIndex = 1;
            this.ddlCompanyId.Enabled = this.DataSource.IsNew;

            // Load Operation Modes
            foreach (ClientOperationMode type in Enum.GetValues(typeof(ClientOperationMode)))
            {
                string text = Translate("ClientOperationMode-" + (int)type, type.GetStringValue());
                this.ddlOperationMode.Items.Add(text, (int)type);
            }

            PredicateExpression entertainmentFilter = new PredicateExpression
            {
                EntertainmentFields.EntertainmentType == EntertainmentType.Android
            };

            PredicateExpression entertainmentSubFilter = new PredicateExpression
            {
                EntertainmentFields.CompanyId == CmsSessionHelper.CurrentCompanyId
            };
            entertainmentSubFilter.AddWithOr(EntertainmentFields.CompanyId == DBNull.Value);
            entertainmentFilter.Add(entertainmentSubFilter);

            RelationCollection entertainmentJoins = new RelationCollection
            {
                { EntertainmentEntityBase.Relations.ClientEntertainmentEntityUsingEntertainmentId, JoinHint.Left }
            };

            SortExpression entertainmentSort = new SortExpression
            {
                EntertainmentFields.Name | SortOperator.Ascending
            };

            EntertainmentCollection clientEntertainment = new EntertainmentCollection();
            clientEntertainment.GetMulti(entertainmentFilter, 0, entertainmentSort, entertainmentJoins);

            this.cblClientEntertainment.DataSource = clientEntertainment;
            this.cblClientEntertainment.DataBind();

            DeviceEntity device = this.DataSourceAsClientEntity.DeviceEntity;

            // Populate the types of info to show on Activity Tab            
            this.cbCommand.Items.Add(Translate("SendLogToWebservice", "Log versturen naar webservice"), (int)ClientCommand.SendLogToWebservice);
            this.cbCommand.Items.Add(Translate("RestartApplication", "Applicatie herstarten"), (int)ClientCommand.RestartApplication);
            this.cbCommand.Items.Add(Translate("RestartDevice", "Apparaat herstarten"), (int)ClientCommand.RestartDevice);
            this.cbCommand.Items.Add(Translate("RestartInRecovery", "Apparaat herstarten (recovery)"), (int)ClientCommand.RestartInRecovery);
            this.cbCommand.Items.Add(Translate("DailyOrderReset", "Dagelijkse order reset"), (int)ClientCommand.DailyOrderReset);
            this.cbCommand.Items.Add(Translate("DownloadEmenuUpdate", "Download Emenu update"), (int)ClientCommand.DownloadEmenuUpdate);
            this.cbCommand.Items.Add(Translate("DownloadAgentUpdate", "Download Agent update"), (int)ClientCommand.DownloadAgentUpdate);
            this.cbCommand.Items.Add(Translate("DownloadSupportToolsUpdate", "Download SupportTools update"), (int)ClientCommand.DownloadSupportToolsUpdate);
            this.cbCommand.Items.Add(Translate("UpdateMessagingService", "Update MessagingService"), (int)ClientCommand.DownloadInstallMessagingServiceUpdate);
            this.cbCommand.Items.Add(Translate("ClearBrowserCache", "Clear browser cache"), (int)ClientCommand.ClearBrowserCache);
            this.cbCommand.Items.Add(Translate("GetRoomControlStatus", "Get room control status"), (int)ClientCommand.GetRoomControlStatus);
            if (device?.UpdateEmenuDownloaded ?? this.DataSourceAsClientEntity.DeviceEntity.UpdateEmenuDownloaded)
            {
                this.cbCommand.Items.Add(Translate("InstallEmenuUpdate", "Install Emenu update"), (int)ClientCommand.InstallEmenuUpdate);
            }

            if (device?.UpdateAgentDownloaded ?? this.DataSourceAsClientEntity.DeviceEntity.UpdateAgentDownloaded)
            {
                this.cbCommand.Items.Add(Translate("InstallAgentUpdate", "Install Agent update"), (int)ClientCommand.InstallAgentUpdate);
            }

            if (device?.UpdateSupportToolsDownloaded ?? this.DataSourceAsClientEntity.DeviceEntity.UpdateSupportToolsDownloaded)
            {
                this.cbCommand.Items.Add(Translate("InstallSupportToolsUpdate", "Install SupportTools update"), (int)ClientCommand.InstallSupportToolsUpdate);
            }

            this.cbCommand.Items.Add("Switch to webservice mode", Client.SWITCH_TO_WEBSERVICE_COMMAND);
            this.cbCommand.Items.Add("Switch to signalr mode", Client.SWITCH_TO_SIGNALR_COMMAND);
            this.cbCommand.Items.Add("Close SupportTools", Client.CLOSE_SUPPORT_TOOLS);

            //if (CmsSessionHelper.CurrentRole <= Role.Reseller)
            //{
            //    this.btUnlinkDevice.Visible = false;
            //}
            if (CmsSessionHelper.CurrentRole < Role.Reseller)
            {
                this.tabsMain.TabPages.FindByName("tabAdvanced").Visible = false;
            }

            this.plhDeviceSettings.Visible = (this.DataSourceAsClientEntity.DeviceId.HasValue);
            this.plhRemoteControl.Visible = device.ShowRemoteControl(CmsSessionHelper.CurrentRole);
            this.plhLogCat.Visible = device.ShowLogCat(CmsSessionHelper.CurrentRole);

            if (this.DataSourceAsClientEntity.DeliverypointGroupId > 0 &&
                this.DataSourceAsClientEntity.DeliverypointId > 0)
            {
                string value = string.Format("{0}-{1}", this.DataSourceAsClientEntity.DeliverypointGroupId, this.DataSourceAsClientEntity.DeliverypointId);
                this.cbDeliverypointgroupAndDeliverypoint.Value = value;
            }

            if (this.DataSourceAsClientEntity.DeliverypointgroupEntity.UseHardKeyboard == HardwareKeyboardType.None)
            {
                this.lblBluetoothKeyboardConnectedValue.Text = "Disabled";
            }
            else
            {
                this.lblBluetoothKeyboardConnectedValue.Text = this.DataSourceAsClientEntity.BluetoothKeyboardConnected ? "Connected" : "Disconnected";
            }

            this.lblRoomControlConnectedValue.Text = this.DataSourceAsClientEntity.RoomControlConnected ? Translate("Connected", "Connected") : Translate("Disconnected", "Disconnected");
            this.lblDoNotDisturbActiveValue.Text = this.DataSourceAsClientEntity.DoNotDisturbActive ? Translate("Active", "Active") : Translate("Inactive", "Inactive");
            this.lblServiceRoomActiveValue.Text = this.DataSourceAsClientEntity.ServiceRoomActive ? Translate("Active", "Active") : Translate("Inactive", "Inactive");

            this.lblLastApiVersionValue.Text = this.DataSourceAsClientEntity.LastApiVersion.ToString();

            LoadRoomControlAreas();
        }

        private string WifiStrengthToText(int? strength)
        {
            if (!strength.HasValue)
            {
                return Translate("WifiStrengthUnknown", "Onbekend");
            }

            if (strength > -55)
            {
                return Translate("WifiStrengthExcellent", "Uitstekend");
            }
            else if (strength > -66)
            {
                return Translate("WifiStrengthGood", "Goed");
            }
            else if (strength > -77)
            {
                return Translate("WifiStrengthAverage", "Gemiddeld");
            }
            else if (strength > -88)
            {
                return Translate("WifiStrengthPoor", "Matig");
            }
            else
            {
                return Translate("WifiStrengthBad", "Slecht");
            }
        }

        private void LoadRoomControlAreas()
        {
            int roomControlConfigurationId = -1;
            if (this.DataSourceAsClientEntity.DeliverypointId.HasValue && this.DataSourceAsClientEntity.DeliverypointEntity.RoomControlConfigurationId.HasValue)
            {
                roomControlConfigurationId = this.DataSourceAsClientEntity.DeliverypointEntity.RoomControlConfigurationId.Value;
            }

            if (roomControlConfigurationId > -1)
            {
                SortExpression sort = new SortExpression(RoomControlAreaFields.Name | SortOperator.Ascending);
                PredicateExpression filter = new PredicateExpression(RoomControlAreaFields.RoomControlConfigurationId == roomControlConfigurationId)
                {
                    RoomControlAreaFields.Visible == true
                };

                RoomControlAreaCollection roomControlAreas = new RoomControlAreaCollection();
                roomControlAreas.GetMulti(filter, 0, sort);

                this.ddlRoomControlAreaId.DataSource = roomControlAreas;
                this.ddlRoomControlAreaId.DataBind();
            }
        }

        private void SetWarnings()
        {
            if (this.DataSourceAsClientEntity.DeviceId.HasValue && this.DataSourceAsClientEntity.DeviceEntity.IsDevelopment)
            {
                AddInformator(Dionysos.Web.UI.InformatorType.Information, Translate("Information.IsDevelopmentDevice", "This device is set in development mode."));
                Validate();
            }
        }

        private void HookUpEvents()
        {
            this.btnChangeDeliverypointDeliverypointGroup.Click += btChangeDeliverypointDeliverypointGroup_Click;
            this.btExecuteCommand.Click += btExecuteCommand_Click;
            this.btUpdateActivityLog.Click += btUpdateActivityLog_Click;
            this.btViewDevice.Click += btViewDevice_Click;
            //this.btUnlinkDevice.Click += this.btUnlinkDevice_Click;
            this.btnDeviceShell.Click += btnDeviceShell_Click;
            this.btRemoteControl.Click += (sender, args) =>
                                          {
                                              string url = new RemoteControlUrl(this.DataSourceAsClientEntity.DeviceEntity.Identifier);
                                              this.Response.Write($"<script>window.open('{url}','_self');</script>");
                                          };
            this.btnLogCat.Click += (sender, args) =>
                                    {
                                        string url = new LogCatUrl(this.DataSourceAsClientEntity.DeviceEntity.Identifier);
                                        this.Response.Write($"<script>window.open('{url}','_self');</script>");
                                    };
        }

        public override bool Save()
        {
            if (this.PageMode == Dionysos.Web.PageMode.Add)
            {
                this.DataSourceAsClientEntity.CompanyId = CmsSessionHelper.CurrentCompanyId;
            }

            if (this.DataSourceAsClientEntity.Fields[ClientFields.DeliverypointGroupId.Name].IsChanged)
            {
                this.DataSourceAsClientEntity.DeliverypointId = null;
            }

            if (this.cbDeliverypointgroupAndDeliverypoint.SelectedIndex > -1)
            {
                string[] values = this.cbDeliverypointgroupAndDeliverypoint.SelectedValueString.Split('-');

                if (int.TryParse(values[0], out int deliverypointgroupId))
                {
                    if (this.DataSourceAsClientEntity.DeliverypointGroupId != deliverypointgroupId)
                    {
                        this.DataSourceAsClientEntity.DeliverypointGroupId = deliverypointgroupId;
                    }
                }

                if (int.TryParse(values[1], out int deliverypointId))
                {
                    if (this.DataSourceAsClientEntity.DeliverypointId != deliverypointId)
                    {
                        this.DataSourceAsClientEntity.DeliverypointId = deliverypointId;
                    }
                }
            }

            try
            {
                return base.Save();
            }
            catch (ObymobiEntityException ex)
            {
                if (ex.ErrorEnumValue.Equals(ClientSaveResult.DeliverypointgroupDoesNotBelongToCompanyOfClient))
                {
                    this.MultiValidatorDefault.AddError(Translate("DeliverypointgroupDoesNotBelongToCompanyOfClient", "Het geselecteerde deliverypoint hoort niet bij de geselecteerde deliverypointgroup."));
                }
                else
                {
                    throw;
                }
            }

            return false;
        }

        private DevExpress.Web.ListEditItem CreateListEditItemForCurrentDeliverypoint()
        {
            return new DevExpress.Web.ListEditItem
            {
                Text = string.Format("{0} - {1}", this.clientEntityDataSource.DeliverypointEntity.DeliverypointgroupEntity.Name, this.clientEntityDataSource.DeliverypointEntity.Number),
                Value = string.Format("{0}-{1}", this.clientEntityDataSource.DeliverypointgroupEntity.DeliverypointgroupId,
                        this.clientEntityDataSource.DeliverypointId)
            };
        }

        #endregion

        #region Event handlers

        private void Client_DataSourceLoaded(object sender)
        {

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.lblLastStatusValue.Text = (!string.IsNullOrEmpty(this.DataSourceAsClientEntity.LastStatusText) ? this.DataSourceAsClientEntity.LastStatusText : Translate("Unknown", "Onbekend"));
            HookUpEvents();

            // Activity Gridview
            this.gvClientActivityLog.DataSource = this.DataSourceAsClientEntity.GetClientActivityLogs(this.cblTypesToShow.Items[1].Selected, this.cblTypesToShow.Items[0].Selected, this.cblTypesToShow.Items[2].Selected, this.tbAmountToShow.Value ?? 100);
            this.gvClientActivityLog.DataBind();
        }

        private void btExecuteCommand_Click(object sender, EventArgs e)
        {
            // Issue command
            if (this.cbCommand.ValidId > 0)
            {
                if (this.cbCommand.ValidId == Client.SWITCH_TO_WEBSERVICE_COMMAND)
                {
                    NetmessageSwitchCometHandler switchToWebservice = new NetmessageSwitchCometHandler
                    {
                        ReceiverClientId = this.EntityId,
                        Handler = ClientCommunicationMethod.Webservice
                    };

                    CometHelper.Instance.SendMessage(switchToWebservice);
                }
                else if (this.cbCommand.ValidId == Client.SWITCH_TO_SIGNALR_COMMAND)
                {
                    NetmessageSwitchCometHandler switchToSignalR = new NetmessageSwitchCometHandler
                    {
                        ReceiverClientId = this.EntityId,
                        Handler = ClientCommunicationMethod.SignalR
                    };

                    CometHelper.Instance.SendMessage(switchToSignalR);
                }
                else if (this.cbCommand.ValidId == Client.CLOSE_SUPPORT_TOOLS)
                {
                    NetmessageDeviceCommandExecute netmessage = new NetmessageDeviceCommandExecute
                    {
                        ReceiverClientId = this.EntityId,
                        Command = "am force-stop net.craveinteractive.supporttools"
                    };

                    CometHelper.Instance.SendMessage(netmessage);
                }
                else
                {
                    ClientCommand clientCommand = this.cbCommand.ValidId.ToEnum<ClientCommand>();

                    if (clientCommand == ClientCommand.InstallEmenuUpdate &&
                        this.DataSourceAsClientEntity.DeviceEntity.DeviceModel.IsT3() &&
                        VersionNumber.CompareVersions(this.DataSourceAsClientEntity.DeviceEntity.ApplicationVersion, "2022010602") == VersionNumber.VersionState.Older)
                    {
                        CometHelper.DeviceCommandExecuteClient(this.EntityId, "pm install -r sdcard/Download/CraveEmenu.apk");
                    }
                    else
                    {
                        CometHelper.ClientCommand(this.EntityId, clientCommand, this.cbCommandToSupportTools.Checked);
                    }
                }

                AddInformatorInfo(Translate("CommandIsRequested", "Het commando '{0}' is verzocht."), this.cbCommand.Text);
            }
            else
            {
                AddInformator(Dionysos.Web.UI.InformatorType.Warning, Translate("ErrorYouHaveToSelectACommandToRequest", "U dient een commando the kiezen om te verzoeken"));
            }
        }

        private void btUpdateActivityLog_Click(object sender, EventArgs e)
        {
            // Do nothing, postback is enough
        }

        //void btUnlinkDevice_Click(object sender, EventArgs e)
        //{
        //    this.DataSourceAsClientEntity.DeviceId = null;
        //    this.DataSourceAsClientEntity.Save();
        //}

        private void btnDeviceShell_Click(object sender, EventArgs e) => this.Response.Redirect(string.Format("DeviceShell.aspx?Device={0}&ClientId={1}", this.DataSourceAsClientEntity.MacAddress.Replace(":", ""), this.DataSourceAsClientEntity.ClientId));

        private void btViewDevice_Click(object sender, EventArgs e) => this.Response.Redirect(string.Format("~/Configuration/Device.aspx?id={0}", this.DataSourceAsClientEntity.DeviceId.GetValueOrDefault(-1)));

        private void btChangeDeliverypointDeliverypointGroup_Click(object sender, EventArgs e)
        {
            // Bind data to custom cb for posible deliverypointgroup + deliverypoint combinations
            PredicateExpression filter = new PredicateExpression
            {
                DeliverypointgroupFields.CompanyId == CmsSessionHelper.CurrentCompanyId,
                DeliverypointFields.CompanyId == CmsSessionHelper.CurrentCompanyId
            };

            PrefetchPath prefetch = new PrefetchPath(EntityType.DeliverypointEntity)
            {
                DeliverypointEntityBase.PrefetchPathDeliverypointgroupEntity
            };

            RelationCollection relations = new RelationCollection
            {
                DeliverypointEntityBase.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId
            };

            SortExpression sort = new SortExpression
            {
                new SortClause(DeliverypointgroupFields.Name, SortOperator.Ascending),
                new SortClause(DeliverypointFields.Number, SortOperator.Ascending)
            };

            DeliverypointCollection deliverypoints = new DeliverypointCollection();
            deliverypoints.GetMulti(filter, 0, sort, relations, prefetch);

            foreach (DeliverypointEntity dp in deliverypoints)
            {
                DevExpress.Web.ListEditItem item = new DevExpress.Web.ListEditItem
                {
                    Text = string.Format("{0} - {1}", dp.DeliverypointgroupEntity.Name, dp.Number),
                    Value = string.Format("{0}-{1}", dp.DeliverypointgroupEntity.DeliverypointgroupId,
                        dp.DeliverypointId)
                };

                this.cbDeliverypointgroupAndDeliverypoint.Items.Add(item);
            }

            this.cbDeliverypointgroupAndDeliverypoint.Enabled = true;
        }

        #endregion

        #region Properties

        private ClientEntity clientEntityDataSource;
        public ClientEntity DataSourceAsClientEntity => this.clientEntityDataSource ?? (this.clientEntityDataSource = this.DataSource as ClientEntity);

        private CustomerEntity customer;
        public CustomerEntity Customer
        {
            get
            {
                if (!this.DataSourceAsClientEntity.IsNew && this.customer == null)
                {
                    CustomerEntity c = new CustomerEntity();
                    PredicateExpression filterCustomer = new PredicateExpression
                    {
                        CustomerFields.ClientId == this.DataSourceAsClientEntity.ClientId
                    };
                    c = LLBLGenEntityUtil.GetSingleEntityUsingPredicateExpression(filterCustomer, c);
                    this.customer = c;
                }

                return this.customer;
            }
        }

        #endregion
    }
}
