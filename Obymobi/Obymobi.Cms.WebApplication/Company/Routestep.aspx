﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master"
    AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.Routestep" Codebehind="Routestep.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" runat="Server">
    <div>
        <X:PageControl ID="tabsMain" runat="server" Width="100%">
            <TabPages>
                <X:TabPage Text="Algemeen" Name="Generic">
                    <controls>
                        <table class="dataformV2">
                           <tr>
                               <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblNumber">Nummer</D:LabelEntityFieldInfo>
                               </td>
                               <td class="control">
                                    <D:TextBoxInt ID="tbNumber" runat="server" IsRequired="true" Enabled="false"></D:TextBoxInt>
                               </td>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblRouteId">Route</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlRouteId" IncrementalFilteringMode="StartsWith" EntityName="Route" TextField="Name" ValueField="RouteId" IsRequired="true" DisabledOnceSelectedAndSaved="true" DisabledOnceSelectedFromQueryString="true" PreventEntityCollectionInitialization="true" />
                                </td>
                           </tr>
                           <tr>
                               <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblTimeoutMinutes">Timeout (min.)</D:LabelEntityFieldInfo>
                               </td>
                               <td class="control">
                                    <D:TextBoxInt ID="tbTimeoutMinutes" runat="server"></D:TextBoxInt>
                               </td>
                               <td class="label">
                                    <D:Label runat="server" id="lblStepTimeoutMinutes">Standaard Timeout (min.)</D:Label>
                               </td>
                               <td class="control">
                                    <D:Label runat="server" id="lblStepTimeoutMinutesValue" LocalizeText="false"></D:Label>
                               </td>
                           </tr>
                           <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblContinueOnFailure">Falen toegestaan</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">                                    
                                    <D:CheckBox ID="cbContinueOnFailure" runat="server" />                                    
                                </td>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblCompleteRouteOnComplete">Route voltooien na deze stap</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:CheckBox ID="cbCompleteRouteOnComplete" runat="server" />
                                </td>                                
                           </tr>
                        </table>
                    </controls>
                </X:TabPage>
            </TabPages>
        </X:PageControl>
    </div>
</asp:Content>
