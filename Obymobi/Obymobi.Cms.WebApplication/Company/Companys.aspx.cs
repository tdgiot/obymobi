﻿using System;

namespace Obymobi.ObymobiCms.Company
{
    public partial class Companys : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            // As long as we don't show an overview of companies, don't bother loading the overview content.
            this.DataSourceLoad += (object sender, ref bool preventLoading) => preventLoading = true;

            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Response.Redirect($"~/Company/Company.aspx?id={CmsSessionHelper.CurrentCompanyId}");
        }

        #endregion
    }
}
