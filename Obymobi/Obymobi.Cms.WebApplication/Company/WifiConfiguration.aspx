﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" ValidateRequest="false" Inherits="Obymobi.ObymobiCms.Company.WifiConfiguration" Codebehind="WifiConfiguration.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
					<table class="dataformV2">
					<tr>
						<td class="label">
							<D:LabelEntityFieldInfo runat="server" id="lblSsid">SSID</D:LabelEntityFieldInfo>
						</td>
						<td class="control">
							<D:TextBoxString ID="tbSsid" runat="server" IsRequired="true"></D:TextBoxString>
						</td>
						<td class="label">
                            <D:LabelEntityFieldInfo runat="server" id="lblHiddenSsid">Hidden SSID</D:LabelEntityFieldInfo>
						</td>
						<td class="control">
                            <D:CheckBox runat="server" id="cbHiddenSsid" />
						</td>
					</tr>
                    <tr>
						<td class="label">
							<D:LabelEntityFieldInfo runat="server" id="lblSecurity">Security</D:LabelEntityFieldInfo>
						</td>
						<td class="control">
							<X:ComboBoxInt runat="server" ID="ddlSecurity" DisplayEmptyItem="false" IsRequired="true"></X:ComboBoxInt>
						</td>
					</tr>
                    <tr>
						<td class="label">
							<D:LabelEntityFieldInfo runat="server" id="lblSecurityKey">Security Key</D:LabelEntityFieldInfo>
						</td>
						<td class="control">
							<D:TextBoxString ID="tbSecurityKey" runat="server" IsRequired="false"></D:TextBoxString>
						</td>
						<td class="label">
							
						</td>
						<td class="control">
							
						</td>	
					</tr>
                    </table>
                    
                    <D:PlaceHolder runat="server" ID="plhWifiAdvanced">
                    <table class="dataformV2">
                    <tr>
						<td class="label">
							<D:LabelEntityFieldInfo runat="server" id="lblEapMethod">Eap Method</D:LabelEntityFieldInfo>
						</td>
						<td class="control">
							<X:ComboBoxInt runat="server" ID="ddlEapMethod" DisplayEmptyItem="false"></X:ComboBoxInt>
						</td>
						<td class="label">
							<D:LabelEntityFieldInfo runat="server" id="lblEapPhase2">Phase 2 authentication</D:LabelEntityFieldInfo>
						</td>
						<td class="control">
							<X:ComboBoxInt ID="ddlEapPhase2" DisplayEmptyItem="false" runat="server" IsRequired="false"></X:ComboBoxInt>
						</td>	
					</tr>
                    <tr>
						<td class="label">
							<D:LabelEntityFieldInfo runat="server" id="lblEapIdentity">Identity</D:LabelEntityFieldInfo>
						</td>
						<td class="control">
							<D:TextBoxString ID="tbWifiIdentity" runat="server" IsRequired="false"></D:TextBoxString>
						</td>
						<td class="label">
							<D:LabelEntityFieldInfo runat="server" id="lblEapAnonIdent">Anon Identity</D:LabelEntityFieldInfo>
						</td>
						<td class="control">
							<D:TextBoxString ID="tbEapAnonIdent" runat="server" IsRequired="false"></D:TextBoxString>
						</td>	
					</tr>
                </table>
                        </D:PlaceHolder>
				</Controls>
			</X:TabPage>						
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>