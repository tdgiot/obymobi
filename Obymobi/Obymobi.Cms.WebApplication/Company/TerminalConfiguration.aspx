﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.TerminalConfiguration" Codebehind="TerminalConfiguration.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%" EnableViewState="True">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:Label runat="server" id="lblName">Name</D:Label>
							</td>
							<td class="control">
							    <D:TextBoxString runat="server" ID="tbName" />
							</td>
                            <td class="label">
                                <D:Label runat="server" id="lblTerminalId">Terminal</D:Label>
                            </td>
                            <td class="control">
                                <X:ComboBoxInt runat="server" ID="cbTerminalId" CssClass="input" TextField="Name" ValueField="TerminalId" IncrementalFilteringMode="Contains" IncrementalFilteringDelay="1000"></X:ComboBoxInt>
                            </td>
					    </tr>                            
                        <tr>
                            <td class="label">
                                <D:Label runat="server" id="lblUIModeId">UI mode</D:Label>
                            </td>
                            <td class="control">
                                <X:ComboBoxInt runat="server" ID="cbUIModeId" IsRequired="True" CssClass="input" TextField="Name" ValueField="UIModeId" IncrementalFilteringMode="Contains" IncrementalFilteringDelay="1000"></X:ComboBoxInt>
                            </td>
                            <td class="label">
                                <D:Label runat="server" id="lblPrintingEnabled">Printing enabled</D:Label>
                            </td>
                            <td class="control">
                                <D:CheckBox runat="server" ID="cbPrintingEnabled" />
                            </td>
                        </tr>    
                        <tr>
                            <td class="label">
                                <D:Label runat="server" id="lblUseHardKeyboard">Use hard keyboard</D:Label>
                            </td>
                            <td class="control">
                                <X:ComboBoxInt runat="server" ID="ddlUseHardKeyboard" DisplayEmptyItem="false"></X:ComboBoxInt>
                            </td>
                            <td class="label">
                                <D:Label runat="server" id="lblMaxVolume">Max volume</D:Label>
                            </td>
                            <td class="control">
                                <D:TextBoxInt runat="server" ID="tbMaxVolume" />
                            </td>                            
                        </tr>
                    </table>
				</Controls>
			</X:TabPage>	
            <X:TabPage Text="Advanced" Name="advanced">
                <controls>
                    <D:Panel ID="pnlSecurity" runat="server" GroupingText="Security">
					    <table class="dataformV2">
                            <tr>
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblPincode">Pincode</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
							        <D:TextBoxString runat="server" ID="tbPincode" />
							    </td>        
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblPincodeGM">Pincode godmode</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
                                    <D:TextBoxString runat="server" ID="tbPincodeGM" />
							    </td>   				
                            </tr>		
                            <tr>
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblPincodeSU">Pincode superuser</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
							        <D:TextBoxString runat="server" ID="tbPincodeSU" />
							    </td>       
                            </tr>				
					     </table>			
                    </D:Panel>                    
				</Controls>
			</X:TabPage>             
            <X:TabPage Text="Linked" Name="Linked">
                <controls>
                    <D:PlaceHolder runat="server" ID="plhLinkedEntities"></D:PlaceHolder>
                </controls>
            </X:TabPage>
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>

