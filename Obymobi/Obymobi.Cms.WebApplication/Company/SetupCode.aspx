﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.SetupCode" Codebehind="SetupCode.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
<X:PageControl Id="tabsMain" runat="server" Width="100%">
<TabPages>
	<X:TabPage Text="Generic" Name="Generic">
		<Controls>
			<table class="dataformV2">
			    <tr>
			        <td class="label"><D:LabelEntityFieldInfo runat="server" id="lblCode">Code</D:LabelEntityFieldInfo></td>
                    <td class="control"><D:Label runat="server" id="lblCodeValue" LocalizeText="false"></D:Label></td>
                    <td class="label"></td>
                    <td class="control"></td>
			    </tr>
                <tr>
			        <td class="label"><D:LabelEntityFieldInfo runat="server" id="lblDeliverypointgroupText">Deliverypointgroup</D:LabelEntityFieldInfo></td>
                    <td class="control"><D:Label runat="server" id="lblDeliverypointgroupValue" LocalizeText="false"></D:Label></td>
                    <td class="label"></td>
                    <td class="control"></td>
			    </tr>
                <tr>
			        <td class="label"><D:LabelEntityFieldInfo runat="server" id="lblExpireDateText">Expires On</D:LabelEntityFieldInfo></td>
                    <td class="control"><D:Label runat="server" id="lblExpireDateValue" LocalizeText="false"></D:Label></td>
                    <td class="label"></td>
                    <td class="control"></td>
			    </tr>
			    <tr>
			        <td class="label"></td>
                    <td class="control">
                        <D:Button runat="server" ID="btnDownloadSetupConfig" Text="Download Setup Configuration" LocalizeText="True"/>
                    </td>
                    <td class="label"></td>
                    <td class="control"></td>
			    </tr>
            </table>
        </Controls>
    </X:TabPage>
</TabPages>
</X:PageControl>
</div>
</asp:Content>

