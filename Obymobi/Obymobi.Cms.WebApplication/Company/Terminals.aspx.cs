﻿using System;
using System.Web.UI.WebControls;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using System.Drawing;
using Dionysos;
using Obymobi.Data;

namespace Obymobi.ObymobiCms.Company
{
	public partial class Terminals : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        private readonly IncludeFieldsList deviceIncludeFieldsList = new IncludeFieldsList
        {
            DeviceFields.LastRequestUTC
        };

        #region Event handlers        

        protected void Page_Load(object sender, EventArgs e)
        {
            LLBLGenProDataSource datasource = this.DataSource as LLBLGenProDataSource;
            if (datasource != null)
            {
                PredicateExpression filter = new PredicateExpression(TerminalFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
                datasource.FilterToUse = filter;
            }
            
            this.MainGridView.HtmlRowPrepared += MainGridView_HtmlRowPrepared;
            this.MainGridView.Styles.Cell.HorizontalAlign = HorizontalAlign.Left;
        }        

        private void MainGridView_HtmlRowPrepared(object sender, DevExpress.Web.ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType != DevExpress.Web.GridViewRowType.Data) return;

            int terminalId = Convert.ToInt32(e.KeyValue);
            if (terminalId > 0)
            {
                PrefetchPath prefetch = new PrefetchPath(EntityType.TerminalEntity);
                prefetch.Add(TerminalEntityBase.PrefetchPathDeviceEntity, this.deviceIncludeFieldsList);

                TerminalEntity terminal = new TerminalEntity();
                terminal.FetchUsingPK(terminalId, prefetch, null, new IncludeFieldsList(TerminalFields.LastStatus));

                if (!terminal.IsNew)
                {
                    DeviceEntity device = terminal.DeviceEntity;
                    if (device != null)
                    {
                        this.SetCellValue(e.Row, "LastRequestUTC", device.LastRequestUTC.HasValue ? device.LastRequestUTC.Value.UtcToLocalTime().ToString("g") : string.Empty);
                    }

                    if (terminal.IsOnline)
                    {
                        // Everything is ok
                        e.Row.ForeColor = Color.Green;
                    }                    
                    else
                    {
                        e.Row.ForeColor = Color.Red;
                    }
                }
            }

            // Convert the operation mode enum to text
            int operationMode = (int)e.GetValue("OperationMode");
            if (operationMode != null)
            {
                TerminalOperationMode operationModeAsEnum = operationMode.ToEnum<Obymobi.Enums.TerminalOperationMode>();
                if (e.Row.Cells.Count > 1)
                    e.Row.Cells[1].Text = operationModeAsEnum.ToString();
            }
        }

        private void SetCellValue(TableRow row, string fieldName, string value)
        {
            if (this.MainGridView.Columns[fieldName] != null)
            {
                int index = this.MainGridView.Columns[fieldName].Index;
                if (index < row.Cells.Count)
                {
                    row.Cells[index].Text = value;
                }
            }
        }

        #endregion
    }
}