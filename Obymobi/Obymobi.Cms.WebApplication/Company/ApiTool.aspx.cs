﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using Crave.Api.Logic;
using Dionysos;
using Dionysos.Data.LLBLGen;
using Dionysos.Web.UI;
using Dionysos.Web.UI.DevExControls;
using Dionysos.Web.UI.WebControls;
using Obymobi.Constants;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ServiceStack.Text;
using Crave.Api.Logic.UseCases;
using System.Linq;
using Dionysos.Security.Cryptography;

namespace Obymobi.ObymobiCms.Company
{
    public partial class ApiTool : PageDefault
    {
        #region Methods

        public void Show()
        {
            this.PerformApiCall(false);
        }

        public void Download()
        {
            this.PerformApiCall(true);
        }

        private void SetGui()
        {
            this.LoadApiOperations();
            this.LoadApiBaseUrl();

            this.LoadAdvertisementConfigurations();
            this.LoadClientConfigurations();
            this.LoadClients();
            this.LoadDeliverypoints();
            this.LoadDeliverypointgroups();
            this.LoadEntertainmentConfigurations();
            this.LoadInfraredConfigurations();
            this.LoadMenus();
            this.LoadPointOfInterests();
            this.LoadPriceSchedules();
            this.LoadRoomControlConfigurations();
            this.LoadSites();
            this.LoadTerminals();
            this.LoadTerminalConfigurations();
            this.LoadUIModes();
            this.LoadUISchedules();
            this.LoadUIThemes();

            this.LoadApplications();
        }

        private void LoadApiOperations()
        {
            foreach (string apiCallName in ApiMetaData.Operations.Keys)
            {
                this.cbApiOperation.Items.Add(apiCallName);
            }
        }

        private void LoadApiBaseUrl()
        {
            if (!this.IsPostBack)
            {
                this.tbApiBaseUrl.Text = WebEnvironmentHelper.GetRestApiBaseUrl();
            }
        }

        private void LoadAdvertisementConfigurations()
        {
            AdvertisementConfigurationCollection advertisementConfigurationCollection = EntityCollection.GetMulti<AdvertisementConfigurationCollection>(new PredicateExpression(AdvertisementConfigurationFields.CompanyId == CmsSessionHelper.CurrentCompanyId), new SortExpression(AdvertisementConfigurationFields.Name | SortOperator.Ascending), null, null, new IncludeFieldsList { AdvertisementConfigurationFields.Name });
            this.AddItemsToComboBox(this.cbAdvertisementConfigurationId, advertisementConfigurationCollection, AdvertisementConfigurationFields.Name.Name, AdvertisementConfigurationFields.AdvertisementConfigurationId.Name);
        }

        private void LoadClientConfigurations()
        {
            ClientConfigurationCollection clientConfigurationCollection = EntityCollection.GetMulti<ClientConfigurationCollection>(new PredicateExpression(ClientConfigurationFields.CompanyId == CmsSessionHelper.CurrentCompanyId), new SortExpression(ClientConfigurationFields.Name | SortOperator.Ascending), null, null, new IncludeFieldsList { ClientConfigurationFields.Name });
            this.AddItemsToComboBox(this.cbClientConfigurationId, clientConfigurationCollection, ClientConfigurationFields.Name.Name, ClientConfigurationFields.ClientConfigurationId.Name);
        }

        private void LoadClients()
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ClientFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            filter.Add(ClientFields.DeviceId != DBNull.Value);
            filter.Add(DeviceFields.Token != DBNull.Value);
            filter.Add(DeviceFields.Token != string.Empty);

            RelationCollection relations = new RelationCollection();
            relations.Add(ClientEntity.Relations.DeviceEntityUsingDeviceId);

            SortExpression sort = new SortExpression();
            sort.Add(ClientFields.ClientId | SortOperator.Ascending);

            PrefetchPath path = new PrefetchPath(EntityType.ClientEntity);
            path.Add(ClientEntity.PrefetchPathDeliverypointEntity);
            path.Add(ClientEntity.PrefetchPathDeviceEntity);

            ClientCollection clients = new ClientCollection();
            clients.GetMulti(filter, 0, sort, relations, path);

            foreach (var client in clients)
            {
                string name = client.ClientId.ToString();

                if (client.DeviceId.HasValue)
                    name += " - " + client.DeviceEntity.Identifier;

                if (client.DeliverypointId.HasValue)
                    name += " - " + client.DeliverypointEntity.Number;

                this.cbClientId.Items.Add(name, client.ClientId);
            }
        }

        private void LoadDeliverypoints()
        {
            DeliverypointCollection deliverypoints = new DeliverypointCollection();

            PredicateExpression filter = new PredicateExpression();
            filter.Add(DeliverypointFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

            SortExpression sort = new SortExpression();
            sort.Add(DeliverypointgroupFields.Name | SortOperator.Ascending);
            sort.Add(DeliverypointFields.Number | SortOperator.Ascending);

            RelationCollection joins = new RelationCollection();
            joins.Add(DeliverypointEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId);

            PrefetchPath prefetch = new PrefetchPath(EntityType.DeliverypointEntity);
            prefetch.Add(DeliverypointEntity.PrefetchPathDeliverypointgroupEntity);

            deliverypoints.GetMulti(filter, 0, sort, joins, prefetch);

            foreach (var dp in deliverypoints)
            {
                this.cbDeliverypointId.Items.Add("{0} - {1}".FormatSafe(dp.DeliverypointgroupEntity.Name, dp.Number), dp.DeliverypointId);
            }
        }

        private void LoadDeliverypointgroups()
        {
            DeliverypointgroupCollection deliverypointgroupCollection = EntityCollection.GetMulti<DeliverypointgroupCollection>(new PredicateExpression(DeliverypointgroupFields.CompanyId == CmsSessionHelper.CurrentCompanyId), new SortExpression(DeliverypointgroupFields.Name | SortOperator.Ascending), null, null, new IncludeFieldsList { DeliverypointgroupFields.Name });
            this.AddItemsToComboBox(this.cbDeliverypointgroupId, deliverypointgroupCollection, DeliverypointgroupFields.Name.Name, DeliverypointgroupFields.DeliverypointgroupId.Name);
        }

        private void LoadEntertainmentConfigurations()
        {
            EntertainmentConfigurationCollection entertainmentConfigurationCollection = EntityCollection.GetMulti<EntertainmentConfigurationCollection>(new PredicateExpression(EntertainmentConfigurationFields.CompanyId == CmsSessionHelper.CurrentCompanyId), new SortExpression(EntertainmentConfigurationFields.Name | SortOperator.Ascending), null, null, new IncludeFieldsList { EntertainmentConfigurationFields.Name });
            this.AddItemsToComboBox(this.cbEntertainmentConfigurationId, entertainmentConfigurationCollection, EntertainmentConfigurationFields.Name.Name, EntertainmentConfigurationFields.EntertainmentConfigurationId.Name);
        }

        private void LoadInfraredConfigurations()
        {
            InfraredConfigurationCollection infraredConfigurationCollection = EntityCollection.GetMulti<InfraredConfigurationCollection>(null, new SortExpression(InfraredConfigurationFields.Name | SortOperator.Ascending), null, null, new IncludeFieldsList { InfraredConfigurationFields.Name });
            this.AddItemsToComboBox(this.cbInfraredConfigurationId, infraredConfigurationCollection, InfraredConfigurationFields.Name.Name, InfraredConfigurationFields.InfraredConfigurationId.Name);
        }

        private void LoadMenus()
        {
            MenuCollection menuCollection = EntityCollection.GetMulti<MenuCollection>(new PredicateExpression(MenuFields.CompanyId == CmsSessionHelper.CurrentCompanyId), new SortExpression(MenuFields.Name | SortOperator.Ascending), null, null, new IncludeFieldsList { MenuFields.Name });
            this.AddItemsToComboBox(this.cbMenuId, menuCollection, MenuFields.Name.Name, MenuFields.MenuId.Name);
        }

        private void LoadPointOfInterests()
        {
            PointOfInterestCollection pointOfInterestCollection = EntityCollection.GetMulti<PointOfInterestCollection>(null, new SortExpression(PointOfInterestFields.Name | SortOperator.Ascending), null, null, new IncludeFieldsList { PointOfInterestFields.Name });
            this.AddItemsToComboBox(this.cbPointOfInterestId, pointOfInterestCollection, PointOfInterestFields.Name.Name, PointOfInterestFields.PointOfInterestId.Name);
        }

        private void LoadPriceSchedules()
        {
            PriceScheduleCollection priceScheduleCollection = EntityCollection.GetMulti<PriceScheduleCollection>(new PredicateExpression(PriceScheduleFields.CompanyId == CmsSessionHelper.CurrentCompanyId), new SortExpression(PriceScheduleFields.Name | SortOperator.Ascending), null, null, new IncludeFieldsList { PriceScheduleFields.Name });
            this.AddItemsToComboBox(this.cbPriceScheduleId, priceScheduleCollection, PriceScheduleFields.Name.Name, PriceScheduleFields.PriceScheduleId.Name);
        }

        private void LoadRoomControlConfigurations()
        {
            RoomControlConfigurationCollection roomControlConfigurationCollection = EntityCollection.GetMulti<RoomControlConfigurationCollection>(new PredicateExpression(RoomControlConfigurationFields.CompanyId == CmsSessionHelper.CurrentCompanyId), new SortExpression(RoomControlConfigurationFields.Name | SortOperator.Ascending), null, null, new IncludeFieldsList { RoomControlConfigurationFields.Name });
            this.AddItemsToComboBox(this.cbRoomControlConfigurationId, roomControlConfigurationCollection, RoomControlConfigurationFields.Name.Name, RoomControlConfigurationFields.RoomControlConfigurationId.Name);
        }

        private void LoadSites()
        {
            SiteCollection siteCollection = EntityCollection.GetMulti<SiteCollection>(null, new SortExpression(SiteFields.Name | SortOperator.Ascending), null, null, new IncludeFieldsList { SiteFields.Name });
            this.AddItemsToComboBox(this.cbSiteId, siteCollection, SiteFields.Name.Name, SiteFields.SiteId.Name);
        }

        private void LoadTerminals()
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(TerminalFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            filter.Add(TerminalFields.DeviceId != DBNull.Value);
            filter.Add(DeviceFields.Token != DBNull.Value);
            filter.Add(DeviceFields.Token != string.Empty);

            RelationCollection relations = new RelationCollection();
            relations.Add(TerminalEntity.Relations.DeviceEntityUsingDeviceId);

            SortExpression sort = new SortExpression();
            sort.Add(TerminalFields.Name | SortOperator.Ascending);

            TerminalCollection terminals = new TerminalCollection();
            terminals.GetMulti(filter, 0, sort, relations);

            foreach (var terminal in terminals)
            {
                this.cbTerminalId.Items.Add(terminal.Name, terminal.TerminalId);
            }
        }

        private void LoadTerminalConfigurations()
        {
            TerminalConfigurationCollection terminalConfigurationCollection = EntityCollection.GetMulti<TerminalConfigurationCollection>(null, new SortExpression(TerminalConfigurationFields.Name | SortOperator.Ascending), null, null, new IncludeFieldsList { TerminalConfigurationFields.Name });
            this.AddItemsToComboBox(this.cbTerminalConfigurationId, terminalConfigurationCollection, TerminalConfigurationFields.Name.Name, TerminalConfigurationFields.TerminalConfigurationId.Name);
        }

        private void LoadUIModes()
        {
            UIModeCollection uiModeCollection = EntityCollection.GetMulti<UIModeCollection>(null, new SortExpression(UIModeFields.Name | SortOperator.Ascending), null, null, new IncludeFieldsList { UIModeFields.Name });
            this.AddItemsToComboBox(this.cbUIModeId, uiModeCollection, UIModeFields.Name.Name, UIModeFields.UIModeId.Name);
        }

        private void LoadUISchedules()
        {
            UIScheduleCollection uiScheduleCollection = EntityCollection.GetMulti<UIScheduleCollection>(null, new SortExpression(UIScheduleFields.Name | SortOperator.Ascending), null, null, new IncludeFieldsList { UIScheduleFields.Name });
            this.AddItemsToComboBox(this.cbUIScheduleId, uiScheduleCollection, UIScheduleFields.Name.Name, UIScheduleFields.UIScheduleId.Name);
        }

        private void LoadUIThemes()
        {
            UIThemeCollection uiThemeCollection = EntityCollection.GetMulti<UIThemeCollection>(null, new SortExpression(UIThemeFields.Name | SortOperator.Ascending), null, null, new IncludeFieldsList { UIThemeFields.Name });
            this.AddItemsToComboBox(this.cbUIThemeId, uiThemeCollection, UIThemeFields.Name.Name, UIThemeFields.UIThemeId.Name);
        }

        private void LoadApplications()
        {
            ApplicationCollection apps = new ApplicationCollection();
            apps.GetMulti(null, 0, new SortExpression(ApplicationFields.Name | SortOperator.Ascending));

            foreach (var app in apps)
            {
                this.cbApplicationCode.Items.Add(app.Name, app.Code);
            }
        }

        private void AddItemsToComboBox(ComboBoxInt comboBox, IEntityCollection entityCollection, string textField, string valueField)
        {
            foreach (IEntity entity in entityCollection)
            {
                comboBox.Items.Add(entity.Fields[textField].CurrentValue.ToString(), entity.Fields[valueField].CurrentValue);
            }
        }

        private void PerformApiCall(bool download)
        {
            if (this.Validate())
            {
                Dictionary<string, object> parameterNamesAndValues = this.GetParameterNamesAndValues();

                ApiCallAuthentication authentication;
                if (this.cbClientId.ValidId > -1)
                {
                    authentication = this.GetAuthenticationForClient(this.cbClientId.Value.Value, parameterNamesAndValues);
                }
                else if (this.cbTerminalId.ValidId > -1)
                {
                    authentication = this.GetAuthenticationForTerminal(this.cbTerminalId.Value.Value, parameterNamesAndValues);
                }
                else
                {
                    throw new InvalidOperationException("No client or terminal selected!");
                }

                string apiBaseUrl = this.tbApiBaseUrl.Text;
                string apiOperationName = this.cbApiOperation.Value.ToString();
                ApiOperation apiOperation = ApiMetaData.Operations[apiOperationName];
                string path = apiOperation.GetPathWithParameters(parameterNamesAndValues);

                ApiCall apiCall = new ApiCall
                {
                    BaseUrl = apiBaseUrl,
                    Path = path,
                    RequestName = apiOperation.RequestName,
                    Authentication = authentication
                };

                string json = string.Empty;

                try
                {
                    json = apiCall.Execute();

                    if (download)
                    {
                        this.Response.Clear();
                        Response.AddHeader("content-disposition", "attachment;filename={0}-{1}.json".FormatSafe(this.cbApiOperation.Text, DateTime.Now.Ticks));
                        this.Response.Write(json);
                        this.Response.ContentType = "application/json";
                        this.Response.End();
                    }
                    else
                    {
                        this.plhReponse.AddHtml("\r\n");
                        this.plhReponse.AddHtml("API url: " + apiCall.GetUrlForApiCall());
                        this.plhReponse.AddHtml(JsvFormatter.Format(json));
                    }
                }
                catch (Exception ex)
                {
                    this.plhReponse.AddHtml("An exception occurred when trying to execute the API call on {0}\r\n\r\n{1}", apiCall.GetUrlForApiCall(), ex.Message);
                }
            }
        }

        private ApiCallAuthentication GetAuthenticationForClient(int clientId, Dictionary<string, object> parameterNamesAndValues)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ClientFields.ClientId == clientId);
            filter.Add(DeviceFields.Token != DBNull.Value);
            filter.Add(DeviceFields.Token != string.Empty);

            RelationCollection relations = new RelationCollection();
            relations.Add(ClientEntity.Relations.DeviceEntityUsingDeviceId);

            PrefetchPath prefetch = new PrefetchPath(EntityType.ClientEntity);
            prefetch.Add(ClientEntity.PrefetchPathDeviceEntity, new IncludeFieldsList { DeviceFields.Identifier, DeviceFields.Token });

            ClientCollection clients = new ClientCollection();
            clients.GetMulti(filter, 0, null, relations, prefetch);

            if (clients.Count <= 0)
                throw new InvalidOperationException(string.Format("No client found for ClientId '{0}'!", clientId));

            return new ApiCallAuthentication
            {
                Identifier = clients.First().DeviceEntity.Identifier,
                DeviceToken = Cryptographer.DecryptUsingCBC(clients.First().DeviceEntity.Token),
                ParameterNamesAndValues = parameterNamesAndValues
            };
        }

        private ApiCallAuthentication GetAuthenticationForTerminal(int terminalId, Dictionary<string, object> parameterNamesAndValues)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(TerminalFields.TerminalId == terminalId);
            filter.Add(DeviceFields.Token != DBNull.Value);
            filter.Add(DeviceFields.Token != string.Empty);

            RelationCollection relations = new RelationCollection();
            relations.Add(TerminalEntity.Relations.DeviceEntityUsingDeviceId);

            PrefetchPath prefetch = new PrefetchPath(EntityType.TerminalEntity);
            prefetch.Add(TerminalEntity.PrefetchPathDeviceEntity, new IncludeFieldsList { DeviceFields.Identifier, DeviceFields.Token });

            TerminalCollection terminals = new TerminalCollection();
            terminals.GetMulti(filter, 0, null, relations, prefetch);

            if (terminals.Count <= 0)
                throw new InvalidOperationException(string.Format("No terminal found for TerminalId '{0}'!", terminalId));

            return new ApiCallAuthentication
            {
                Identifier = terminals.First().DeviceEntity.Identifier,
                DeviceToken = Cryptographer.DecryptUsingCBC(terminals.First().DeviceEntity.Token),
                ParameterNamesAndValues = parameterNamesAndValues
            };
        }

        private new bool Validate()
        {
            bool success = true;

            if (this.cbApiOperation.Value == null || this.cbApiOperation.SelectedIndex < 0)
            {
                this.AddInformator(InformatorType.Warning, "Please select an API operation");
                success = false;
            }
            else if (this.tbApiBaseUrl.Text.IsNullOrWhiteSpace())
            {
                this.AddInformator(InformatorType.Warning, "Please specify the API base URL");
                success = false;
            }
            else if (this.cbClientId.SelectedIndex == -1 && this.cbTerminalId.SelectedIndex == -1)
            {
                this.AddInformator(InformatorType.Warning, "Please select a client or a terminal");
                success = false;
            }
            else
            {
                string apiOperationName = this.cbApiOperation.Value.ToString();
                ApiOperation apiOperation = ApiMetaData.Operations[apiOperationName];

                foreach (string parameterName in apiOperation.Parameters)
                {
                    string userFriendlyParameterName = parameterName;
                    if (userFriendlyParameterName.EndsWith("Id"))
                        userFriendlyParameterName = userFriendlyParameterName.Substring(0, userFriendlyParameterName.Length - 2);

                    string controlName = "cb" + parameterName;

                    ComboBoxInt comboBoxInt = this.tblMain.FindControlRecursively(controlName) as ComboBoxInt;
                    if (comboBoxInt != null && comboBoxInt.SelectedIndex == -1)
                    {
                        this.AddInformator(InformatorType.Warning, "Please select a {0}", userFriendlyParameterName);
                        success = false;
                        break;
                    }

                    ComboBox comboBox = this.tblMain.FindControlRecursively(controlName) as ComboBox;
                    if (comboBox != null && comboBox.SelectedIndex == -1)
                    {
                        this.AddInformator(InformatorType.Warning, "Please select a {0}", userFriendlyParameterName);
                        success = false;
                        break;
                    }

                    controlName = "tb" + parameterName;

                    TextBoxPassword textboxPassword = this.tblMain.FindControlRecursively(controlName) as TextBoxPassword;
                    if (textboxPassword != null && textboxPassword.Text.IsNullOrWhiteSpace())
                    {
                        this.AddInformator(InformatorType.Warning, "Please specify the {0}", userFriendlyParameterName);
                        success = false;
                        break;
                    }

                    TextBoxString textboxString = this.tblMain.FindControlRecursively(controlName) as TextBoxString;
                    if (textboxString != null && textboxString.Text.IsNullOrWhiteSpace())
                    {
                        this.AddInformator(InformatorType.Warning, "Please specify the {0}", userFriendlyParameterName);
                        success = false;
                        break;
                    }
                }
            }

            return success;
        }

        private Dictionary<string, object> GetParameterNamesAndValues()
        {
            Dictionary<string, object> parameterNamesAndValues = new Dictionary<string, object>();

            if (this.cbApiOperation.SelectedIndex > -1)
            {
                string apiOperationName = this.cbApiOperation.Value.ToString();
                ApiOperation apiOperation = ApiMetaData.Operations[apiOperationName];

                foreach (string parameterName in apiOperation.Parameters)
                {
                    if (parameterName.Equals("CompanyId"))
                    {
                        parameterNamesAndValues.Add(parameterName, CmsSessionHelper.CurrentCompanyId);
                    }
                    else
                    {
                        string controlName = "cb" + parameterName;

                        ComboBoxInt comboBoxInt = this.tblMain.FindControlRecursively(controlName) as ComboBoxInt;
                        if (comboBoxInt != null && comboBoxInt.SelectedIndex > -1)
                        {
                            parameterNamesAndValues.Add(parameterName, comboBoxInt.ValidId);
                            continue;
                        }

                        ComboBox comboBox = this.tblMain.FindControlRecursively(controlName) as ComboBox;
                        if (comboBox != null && comboBox.SelectedIndex > -1)
                        {
                            parameterNamesAndValues.Add(parameterName, comboBox.SelectedValueString);
                            continue;
                        }

                        controlName = "tb" + parameterName;

                        TextBoxPassword textboxPassword = this.tblMain.FindControlRecursively(controlName) as TextBoxPassword;
                        if (textboxPassword != null && !textboxPassword.Text.IsNullOrWhiteSpace())
                        {
                            string plainPassword = textboxPassword.Text;
                            const string genericSalt = "d27392d3aa07a5d07486e7a2e864b29a57509d51244861a68badbb9d76ebaa50";
                            string hashedPassword = Obymobi.Security.Hasher.GetHash(genericSalt, plainPassword);

                            parameterNamesAndValues.Add(parameterName, hashedPassword);
                            continue;
                        }

                        TextBoxString textboxString = this.tblMain.FindControlRecursively(controlName) as TextBoxString;
                        if (textboxString != null && !textboxString.Text.IsNullOrWhiteSpace())
                        {
                            parameterNamesAndValues.Add(parameterName, textboxString.Text);
                            continue;
                        }
                    }
                }
            }

            return parameterNamesAndValues;
        }

        private void RegisterJavaScript()
        {
            StringBuilder scriptBuilder = new StringBuilder();
            List<string> parameterNames = new List<string>();

            foreach (string apiOperationName in ApiMetaData.Operations.Keys)
            {
                ApiOperation apiOperation = ApiMetaData.Operations[apiOperationName];
                this.RegisterApiOperation(scriptBuilder, parameterNames, apiOperation);
            }

            scriptBuilder.AppendLine();

            scriptBuilder.AppendLine("function HideAllTableRows() {");
            foreach (string parameterName in parameterNames)
            {
                scriptBuilder.AppendFormatLine("HideTableRow(\"{0}\");", parameterName);
            }
            scriptBuilder.AppendLine("}");

            scriptBuilder.AppendLine();

            if (!ClientScript.IsClientScriptBlockRegistered(this.GetType(), "apiOperationsAndParameters"))
                ClientScript.RegisterClientScriptBlock(this.GetType(), "apiOperationsAndParameters", scriptBuilder.ToString(), true);
        }

        private void RegisterApiOperation(StringBuilder scriptBuilder, List<string> parameterNames, ApiOperation apiOperation)
        {
            string line = string.Format("var {0} = [", apiOperation.RequestName);

            if (apiOperation.CallerType == ApiCallerType.Client)
                line += "\"ClientId\", ";
            else if (apiOperation.CallerType == ApiCallerType.Terminal)
                line += "\"TerminalId\", ";

            string parameters = string.Empty;
            foreach (string parameterName in apiOperation.Parameters)
            {
                if (!parameterNames.Contains(parameterName))
                    parameterNames.Add(parameterName);

                if (!parameters.IsNullOrWhiteSpace())
                    parameters += ", ";

                parameters += string.Format("\"{0}\"", parameterName);
            }

            line += parameters;
            line += "];";

            scriptBuilder.AppendLine(line);
        }

        #endregion

        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.SetGui();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.RegisterJavaScript();

            if (this.IsPostBack)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "showTableRowsForApiOperation", "ShowTableRowsForApiOperation(cbApiOperation.GetValue().toString());", true);
        }

        #endregion
    }
}

