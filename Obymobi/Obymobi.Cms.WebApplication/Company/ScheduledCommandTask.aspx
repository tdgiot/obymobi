﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.ScheduledCommandTask" Codebehind="ScheduledCommandTask.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
       <X:PageControl ID="tabsMain" runat="server" Width="100%">
           <TabPages>
               <X:TabPage Text="Algemeen" Name="Generic">
                   <Controls>
                       <D:Panel ID="pnlSettings" runat="server" GroupingText="Settings">
                            <table class="dataformV2">
                                <tr>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" id="lblActive">Active</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <D:CheckBox ID="cbActive" runat="server" />
                                    </td>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" id="lblStatus">Status</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <D:Label ID="lblStatusValue" runat="server" LocalizeText="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" id="lblCompanyId">Company</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <D:Label ID="lblCompanyIdValue" runat="server" LocalizeText="false" />
                                    </td>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" id="lblStart">Starts on</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <X:DateTimeEdit ID="tbStart" runat="server" IsRequired="true"></X:DateTimeEdit>
                                        <D:Label ID="lblStartValue" runat="server" LocalizeText="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" id="lblTimezone">Company Timezone</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <D:Label ID="lblTimezoneValue" runat="server" LocalizeText="false" />
                                    </td>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" id="lblExpires">Expires on</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <X:DateTimeEdit ID="tbExpires" runat="server" IsRequired="true"></X:DateTimeEdit>
                                        <D:Label ID="lblExpiresValue" runat="server" LocalizeText="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label" rowspan="3">
                                        <D:LabelEntityFieldInfo runat="server" id="lblNotificationEmailAddresses">Notification email addresses</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control" rowspan="3">
                                        <D:TextBoxMultiLine ID="tbNotificationEmailAddresses" runat="server" Rows="3"></D:TextBoxMultiLine>
                                        <D:Label ID="lblNotificationEmailAddressesComment" runat="server"><small>Multiple email addresses: Place a new email address on a new line.</small></D:Label>
                                    </td>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" id="lblCompleted">Completed on</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <D:Label ID="lblCompletedValue" runat="server" LocalizeText="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" id="lblProgress">Progress</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <D:Label ID="lblProgressValue" runat="server" LocalizeText="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        &nbsp;
                                    </td>
                                    <td class="control">
                                        <D:Button ID="btnStartValue" runat="server" Text="Refresh" LocalizeText="false"></D:Button>
                                    </td>
                                </tr>
                            </table>
                       </D:Panel>
                       <D:Panel ID="pnlStandardTasks" runat="server" GroupingText="Standard task (optional)" Visible="false">
                           <br />
                           <table class="dataformV2">
                               <tr>
                                   <td class="label">
                                   </td>           
                                   <td class="control">
                                       <D:Label ID="lblWarningScheduledCommandTasks" runat="server" Text="The commands below will be scheduled for all the clients or terminals of your company. If you wish to schedule commands for a specific group, then select the clients or terminals manually after saving the task."></D:Label>
                                   </td>                        
                               </tr>
                               <tr>
                                    <td class="label">
                                        <D:Label ID="lblCommands" runat="server" Text="Commands"></D:Label>
                                    </td>
                                    <td class="control">
                                        <X:ListBoxInt ID="lbCommands" runat="server" SelectionMode="CheckColumn" />
                                    </td>
                                    <td class="label">
                                    </td>
                                    <td class="control">
                                        <D:Button ID="btnCreateScheduledCommandTask" runat="server" Text="Create scheduled command task" />
                                    </td>
                               </tr>                                
                            </table>
                       </D:Panel>
                       <D:Panel ID="pnlAddCommands" runat="server" GroupingText="Add commands">
                           <br />
                           <X:PageControl ID="tabsClientsTerminals" runat="server" Width="100%">
                               <TabPages>
                                   <X:TabPage Text="Clients" Name="Clients">
                                       <Controls>
                                            <table class="dataformV2">
                                                <tr>
                                                    <td class="label">
                                                        <D:LabelEntityFieldInfo runat="server" id="lblClientCommand">Client command</D:LabelEntityFieldInfo>
                                                    </td>
                                                    <td class="control">
                                                        <X:ComboBoxInt ID="cbClientCommand" runat="server" UseDataBinding="false" notdirty="true"></X:ComboBoxInt>
                                                    </td>
                                                    <td class="control">
                                                    </td>
                                                    <td class="control">
                                                        <table cellspacing="3" width="400px">
                                                            <tr>
                                                                <td style="font-weight: bold; padding-right: 5px;">Legend:</td>
                                                                <td style="color: black; padding-right: 5px;">&#9632; OK</td>
                                                                <td style="color: blue; padding-right: 5px;">&#9632; Newer version</td>
                                                                <td style="color: orange; padding-right: 5px;">&#9632; Outdated</td>
                                                                <td style="color: red;">&#9632; Offline</td>
                                                                <td style="color: gray">&#9632; Unknown</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="label">
                                                    </td>
                                                    <td class="control" colspan="2">
                                                        <div style="float:left;"><D:Button ID="btnAddClients" runat="server" Text="Add command for selected clients" />&nbsp</div>
                                                        <div style="float:left;"><D:Button ID="btnAddAllClients" runat="server" Text="Add command for all clients"/>&nbsp</div>
                                                        <div style="float:left;"><D:Button ID="btnAddWebserviceClients" runat="server" Text="Add command for clients that are running on webservice mode"/></div>                                                        
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="label">
                                                    </td>
                                                    <td class="control" colspan="2">
                                                        <div style="float:left;"><D:Button ID="btnAddOutdatedClients" runat="server" Text="Add command for outdated clients"/>&nbsp</div>
                                                        <div style="float:left;"><D:Button ID="btnAddOutdatedSupportTools" runat="server" Text="Add command for outdated SupportTools"/>&nbsp</div>
                                                        <div style="float:left;"><D:Button ID="btnAddOutdatedAgents" runat="server" Text="Add command for outdated agents"/>&nbsp</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="control" colspan="4">
                                                        <X:GridView ID="clientGrid" ClientInstanceName="clientGrid" runat="server" Width="100%" KeyFieldName="ClientId">        
                                                            <SettingsBehavior AutoFilterRowInputDelay="350" />                            
                                                            <SettingsPager PageSize="100"></SettingsPager>
                                                            <SettingsBehavior AllowGroup="false" AllowDragDrop="false" />
                                                            <Columns>
                                                                <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="30px">
                                                                    <HeaderTemplate>
                                                                        <input type="checkbox" onclick="clientGrid.SelectAllRowsOnPage(this.checked);" title="Selecteer/deselecteer alle rijen op de pagina" />
                                                                    </HeaderTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                </dxwgv:GridViewCommandColumn>
                                                                <dxwgv:GridViewDataHyperLinkColumn FieldName="ClientId" VisibleIndex="1" SortIndex="0" SortOrder="Ascending">
								                                    <Settings AutoFilterCondition="Contains" />
                                                                    <PropertiesHyperLinkEdit NavigateUrlFormatString="Client.aspx?id={0}" Target="_blank" TextField="ClientId"  />
                                                                </dxwgv:GridViewDataHyperLinkColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="DeliverypointNumber" VisibleIndex="2" >
								                                    <Settings AutoFilterCondition="Contains" />
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="DeliverypointName" VisibleIndex="3" >
								                                    <Settings AutoFilterCondition="Contains" />
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataDateColumn FieldName="LastRequest" VisibleIndex="4" >
								                                    <Settings AutoFilterCondition="Contains" />
                                                                </dxwgv:GridViewDataDateColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="LocalIp" VisibleIndex="5" >
								                                    <Settings AutoFilterCondition="Contains" />
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="ExternalIp" VisibleIndex="6" >
								                                    <Settings AutoFilterCondition="Contains" />
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="Version" VisibleIndex="7" >
								                                    <Settings AutoFilterCondition="Contains" />
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="VersionST" VisibleIndex="8" >
								                                    <Settings AutoFilterCondition="Contains" />
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="VersionAgent" VisibleIndex="9" >
								                                    <Settings AutoFilterCondition="Contains" />
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="VersionOS" VisibleIndex="10" >
								                                    <Settings AutoFilterCondition="Contains" />
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="VersionMessaging" VisibleIndex="11" >
								                                    <Settings AutoFilterCondition="Contains" />
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="Downloaded" VisibleIndex="12" >
								                                    <Settings AutoFilterCondition="Contains" />
                                                                </dxwgv:GridViewDataColumn>
                                                            </Columns>
                                                            <Settings ShowFilterRow="True" />
                                                        </X:GridView>
                                                    </td>
                                                    <td class="label">
                                                        &nbsp;
                                                    </td>
                                                    <td class="control">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </Controls>
                                    </X:TabPage>
                                   <X:TabPage Text="Terminals" Name="Terminals">
                                       <Controls>
                                            <table class="dataformV2">
                                                <tr>
                                                    <td class="label">
                                                        <D:LabelEntityFieldInfo runat="server" id="lblTerminalCommand">Terminal command</D:LabelEntityFieldInfo>
                                                    </td>
                                                    <td class="control">
                                                        <X:ComboBoxInt ID="cbTerminalCommand" runat="server" UseDataBinding="false" notdirty="true"></X:ComboBoxInt>
                                                    </td>
                                                    <td class="control">
                                                        <D:Button ID="btnAddTerminals" runat="server" Text="Add command for selected terminals" />
                                                    </td>
                                                    <td class="control">
                                                        <table cellspacing="3" width="350px">
                                                            <tr>
                                                                <td style="font-weight: bold; padding-right: 5px;">Legend:</td>
                                                                <td style="color: black; padding-right: 5px;">&#9632; OK</td>
                                                                <td style="color: blue; padding-right: 5px;">&#9632; Newer version</td>
                                                                <td style="color: orange; padding-right: 5px;">&#9632; Outdated</td>
                                                                <td style="color: red;">&#9632; Offline</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="control" colspan="4">
                                                        <X:GridView ID="terminalGrid" ClientInstanceName="terminalGrid" runat="server" Width="100%" KeyFieldName="TerminalId">        
                                                            <SettingsBehavior AutoFilterRowInputDelay="350" />                            
                                                            <SettingsPager PageSize="100"></SettingsPager>
                                                            <SettingsBehavior AllowGroup="false" AllowDragDrop="false" />
                                                            <Columns>
                                                                <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0">                                    
                                                                    <HeaderTemplate>
                                                                        <input type="checkbox" onclick="terminalGrid.SelectAllRowsOnPage(this.checked);" title="Selecteer/deselecteer alle rijen op de pagina" />
                                                                    </HeaderTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                </dxwgv:GridViewCommandColumn>
                                                                <dxwgv:GridViewDataHyperLinkColumn FieldName="TerminalId" VisibleIndex="1" SortIndex="0" SortOrder="Ascending">
								                                    <Settings AutoFilterCondition="Contains" />
                                                                    <PropertiesHyperLinkEdit NavigateUrlFormatString="Terminal.aspx?id={0}" Target="_blank" TextField="TerminalId"  />
                                                                </dxwgv:GridViewDataHyperLinkColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="Name" VisibleIndex="2" >
								                                    <Settings AutoFilterCondition="Contains" />
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataDateColumn FieldName="LastRequest" VisibleIndex="3" >
								                                    <Settings AutoFilterCondition="Contains" />
                                                                </dxwgv:GridViewDataDateColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="LocalIp" VisibleIndex="4" >
								                                    <Settings AutoFilterCondition="Contains" />
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="ExternalIp" VisibleIndex="5" >
								                                    <Settings AutoFilterCondition="Contains" />
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="VersionTerminal" VisibleIndex="6" >
								                                    <Settings AutoFilterCondition="Contains" />
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="VersionST" VisibleIndex="7" >
								                                    <Settings AutoFilterCondition="Contains" />
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="VersionAgent" VisibleIndex="8" >
								                                    <Settings AutoFilterCondition="Contains" />
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="VersionOS" VisibleIndex="9" >
								                                    <Settings AutoFilterCondition="Contains" />
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="VersionMessaging" VisibleIndex="10" >
								                                    <Settings AutoFilterCondition="Contains" />
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="Downloaded" VisibleIndex="11" >
								                                    <Settings AutoFilterCondition="Contains" />
                                                                </dxwgv:GridViewDataColumn>
                                                            </Columns>
                                                            <Settings ShowFilterRow="True" />
                                                        </X:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </Controls>
                                    </X:TabPage>
                            </TabPages>
                        </X:PageControl>
                       </D:Panel>
                   </Controls>
               </X:TabPage>
           </TabPages>
       </X:PageControl>
    </div>
</asp:Content>
