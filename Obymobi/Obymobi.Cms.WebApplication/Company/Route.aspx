﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Company.Route" Codebehind="Route.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server"></asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
  <div>
    <X:PageControl ID="tabsMain" runat="server" Width="100%">
      <TabPages>
        <X:TabPage Text="Algemeen" Name="Generic">
          <controls>
            <D:PlaceHolder runat="server">
              <table class="dataformV2">
                <tr>
                  <td class="label"><D:LabelEntityFieldInfo runat="server" id="lblName">Name</D:LabelEntityFieldInfo></td>
                  <td class="control"><D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString></td>
                  <td class="label"></td>
                  <td class="control"></td>
                </tr>                
                <tr>
                  <td class="label"><D:LabelEntityFieldInfo runat="server" id="lblStepTimeoutMinutes">Timeout (min.)</D:LabelEntityFieldInfo></td>
                  <td class="control"><D:TextBoxInt ID="tbStepTimeoutMinutes" runat="server" IsRequired="true"></D:TextBoxInt></td>
                  <td class="label"><D:Label runat="server" TranslationTag="lblEscalationRouteId" id="lblEscalationRouteId">Escalatie route</D:Label></td>
                  <td class="control"><X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlEscalationRouteId" IncrementalFilteringMode="StartsWith" EntityName="Route" TextField="Name" ValueField="RouteId" PreventEntityCollectionInitialization="true" /></td>
                </tr>
                <tr>
                  <td class="label"><D:LabelEntityFieldInfo runat="server" id="lblRetrievalSupportNotificationTimeoutMinutes">'Ophaal'-timeout (min.)</D:LabelEntityFieldInfo></td>
                  <td class="control"><D:TextBoxInt ID="tbRetrievalSupportNotificationTimeoutMinutes" runat="server" IsRequired="false" AllowZero="true"></D:TextBoxInt></td>
                  <td class="label"><D:LabelEntityFieldInfo runat="server" id="lblBeingHandledSupportNotificationTimeoutMinutes">'Afhandeling'-timeout (min.)</D:LabelEntityFieldInfo></td>
                  <td class="control"><D:TextBoxInt ID="tbBeingHandledSupportNotificationTimeoutMinutes" runat="server" IsRequired="false" AllowZero="true"></D:TextBoxInt></td>
                </tr>
                <tr>
                  <td class="label"><D:LabelTextOnly runat="server" id="lblSummary">Samenvatting</D:LabelTextOnly></td>
                  <td class="control" colspan="3"><table>
                      <D:PlaceHolder runat="server" ID="plhSummary"></D:PlaceHolder>
                    </table></td>
                  <td class="label"></td>
                  <td class="control"></td>
                </tr>
              </table>
            </D:PlaceHolder>
          </controls>
        </X:TabPage>
      </TabPages>
    </X:PageControl>
  </div>
</asp:Content>
