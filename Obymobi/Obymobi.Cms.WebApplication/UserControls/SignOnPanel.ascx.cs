using System;
using System.Linq;
using System.Web;
using System.Web.Security;
using Dionysos.Web;
using Dionysos.Web.Security;
using Dionysos.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using Dionysos;
using Obymobi.Logic.Cms;
using Dionysos.Web.UI;
using Obymobi.Cms.WebApplication.Validation.UserPasswords.Interfaces;
using Obymobi.Cms.WebApplication.Validation.UserPasswords;

namespace Obymobi.ObymobiCms.UserControls
{
	public partial class SignOnPanel : SignOnControl
	{
        string redirectUrl = null;
        private SignedInPage currentPage = SignedInPage.SignOff;

        private readonly IUserPasswordValidator userPasswordValidator;

        public SignOnPanel(IUserPasswordValidator userPasswordValidator)
        {
            this.userPasswordValidator = userPasswordValidator;
        }

		#region Methods

		public override void SignOn(string username, string password)
		{
            // This is ONLY allowed localhost and IP of host same as server
            if (!ValidateSignOn(username, password))
            {
                // MDB TODO
                //HttpContext.Current.Response.Redirect(ResolveUrl(this.SignOnPageRelativeUrl + "?msg=" + InCodeMessages.UserControls_SignOn_CombinationNotActivated));
                HttpContext.Current.Response.Redirect(ResolveUrl(this.SignOnPageRelativeUrl + "?msg=InCodeMessages.UserControls_SignOn_CombinationNotActivated"));
            }
            else
            {
                UserManager.RefreshCurrentUser();
                base.SignOn(username, password);
            }
		}

		public override void AfterSignOn()
		{
            // Reset the password format to hashed (We need a clear passwordFormat for automatic login)
            UserManager.Instance.PasswordFormat = PasswordFormat.Hashed;

			// Only allow employees or non related users
			UserEntity user = this.User as UserEntity;
            			
            if (this.Result == AuthenticateResult.Succes && user.Roles.Length > 0)
            {
                // Clear the user rights
                UserRightsHelper.ClearUiElementRichtsCacheForUser(user.Role);

                if (user.AccountId.HasValue)
                {
                    if (user.AccountEntity.AccountCompanyCollection.Count == 0)
                    {
                        FormsAuthentication.SignOut();
                        HttpContext.Current.Response.Redirect(ResolveUrl(this.SignOnPageRelativeUrl + string.Format("?msg=No company is related to account {0}", user.AccountEntity.Name)), true);
                    }
                    else if (user.AccountEntity.AccountCompanyCollection.Count == 1)
                    {
                        CmsSessionHelper.CurrentCompanyId = user.AccountEntity.AccountCompanyCollection[0].CompanyId;
                    }
                }
                else if (!user.IsAdministrator)
                {
                    FormsAuthentication.SignOut();
                    HttpContext.Current.Response.Redirect(ResolveUrl(this.SignOnPageRelativeUrl + "?msg=No account assigned to user"), true);
                }
                
                if (this.redirectUrl != null)
                    base.AfterSignOn(this.redirectUrl);            
                else
                    base.AfterSignOn();            
            }
            else if (this.Result == AuthenticateResult.Succes && user.Roles.Length == 0)
            {
                FormsAuthentication.SignOut();
                HttpContext.Current.Response.Redirect(ResolveUrl(this.SignOnPageRelativeUrl + "?msg=No role assigned to user"), true);
            }
            else
            {
                string message = string.Empty;

                switch (this.Result)
	            {
                    case AuthenticateResult.LoginUnkown:
                    case AuthenticateResult.PasswordIncorrect:
                    case AuthenticateResult.UsernameEmpty:                        
                    case AuthenticateResult.PasswordEmpty:                        
                        message = ((PageDefault)this.Page).Translate("AuthenticateResult.LoginUnknown", "Gebruikersnaam en/of wachtwoord onbekend.");
                     break;
                    case AuthenticateResult.UserIsLockedOut:
                     message = ((PageDefault)this.Page).Translate("AuthenticateResult.UserIsLockedOut", "Uw account is (tijdelijk) afgesloten.");
                     break;
                    case AuthenticateResult.UserIsNotActivated:
                     message = ((PageDefault)this.Page).Translate("AuthenticateResult.UserIsNotActivated", "Uw account is niet geactiveerd.");
                     break;
                    case AuthenticateResult.TechnicalFailure:
                     message = ((PageDefault)this.Page).Translate("AuthenticateResult.TechnicalFailure", "Wij ondervinden technische inlog problemen.");
                     break;                    
                    case AuthenticateResult.AwaitingAttempts:
                     message = ((PageDefault)this.Page).Translate("AuthenticateResult.TechnicalFailure", "U kunt tijdelijk niet inloggen.");
                     break;
                    default:
                        message = ((PageDefault)this.Page).Translate("AuthenticateResult.Unknown", "You can't be signed in. Please contact support.");
                     break;
	            }
                FormsAuthentication.SignOut();                
                //HttpContext.Current.Response.Redirect(ResolveUrl(this.SignOnPageRelativeUrl + "?msg=" + InCodeMessages.UserControls_SignOn_InvalidUserType), true);
                HttpContext.Current.Response.Redirect(ResolveUrl(this.SignOnPageRelativeUrl + "?msg=" + message), true);
            }
		}

		private bool ValidateSignOn(string username, string password)
		{
			if ((username == "system" && password == "system"))
			{
				return this.Request.Url.Host.Equals("localhost", StringComparison.OrdinalIgnoreCase);
			}
			else
				return true;
		}
        
        private bool HasParameters(params string[] parameters)
        {
            bool toReturn = false;

            foreach (string param in parameters)
            {
                if (Request.Params[param] != null)
                {
                    toReturn = true;
                }
                else
                {
                    toReturn = false;
                    break;
                }
            }
            
            return toReturn;
        }

        private bool IsConsoleSignOnRequest()
        {
            return this.HasParameters("timestamp", "macAddress", "hash");
        }

        private bool IsEmailSignOnRequest()
        {
            return this.HasParameters("h", "SurveyId", "t");
        }

        private void HookupEvents()
        {
            this.lnkChangePassword.Click += lnkChangePassword_Click;            
            this.btBack.Click += btBack_Click;
        }

        private void SetPage()
        {
            if (this.currentPage == SignedInPage.SignOff)
            {
                this.panelLoggedIn.Visible = true;
                this.panelChangePassword.Visible = false;
                this.panelPasswordChanged.Visible = false;
            }
            else if (currentPage == SignedInPage.ChangePassword)
            {
                this.panelLoggedIn.Visible = false;
                this.panelChangePassword.Visible = true;
                this.panelPasswordChanged.Visible = false;
            }
            else if (currentPage == SignedInPage.PasswordChanged)
            {
                this.panelLoggedIn.Visible = false;
                this.panelChangePassword.Visible = false;
                this.panelPasswordChanged.Visible = true;
            }
        }

        private string CustomValidation(string currentPassword, string newPassword, string newPasswormConfirm)
        {
            return userPasswordValidator.ValidateNewAndCurrentPassword(currentPassword, newPassword, newPasswormConfirm);
        }

        public override void ChangePassword(string currentPassword, string newPassword, string newPasswordConfirm)
        {
            if (UserManager.CurrentUser != null)
            {
                string message = this.CustomValidation(currentPassword, newPassword, newPasswordConfirm); 

                if (message.Length > 0)
                    HttpContext.Current.Response.Redirect(ResolveUrl(this.SignOnPageRelativeUrl + "?msg=" + message + "&page=" + (int)SignedInPage.ChangePassword), true);
                else
                {
                    // Get the user
                    var user = new UserEntity(UserManager.CurrentUser.UserId);
                    if (user != null)
                    {
                        // Set the new password in db
                        Dionysos.Web.Security.UserManager.Instance.SetPassword(UserManager.CurrentUser, this.tbNewPassword.Text);

                        // Change the credentials of the companyOwner if this user has one
                        if (user.CompanyOwnerCollection.Count > 0)
                        {
                            var companyOwner = user.CompanyOwnerCollection[0];
                            companyOwner.IsPasswordEncrypted = true;
                            companyOwner.Password = Dionysos.Security.Cryptography.Cryptographer.EncryptStringUsingRijndael(newPassword);
                            companyOwner.Save();
                        }

                        // Show the password changed page
                        HttpContext.Current.Response.Redirect(ResolveUrl(this.SignOnPageRelativeUrl + "?page=" + (int)SignedInPage.PasswordChanged), true);
                    }
                }
            }
        }

		#endregion

        #region Event handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            this.HookupEvents();

            if (UserManager.CurrentUser != null)
                this.lblUsernameValue.Text = UserManager.CurrentUser.Username;

            if (base.IsAuthenticated)
            {
                // User signed in
                if (QueryStringHelper.HasValue("page"))
                {
                    var pageNumber = QueryStringHelper.GetInt("page");
                    this.currentPage = (SignedInPage)pageNumber;
                }

                this.SetPage();
            }
        }

        void lnkChangePassword_Click(object sender, EventArgs e)
        {
            HttpContext.Current.Response.Redirect(ResolveUrl(this.SignOnPageRelativeUrl + "?page=" + (int)SignedInPage.ChangePassword), true);
        }

        void btBack_Click(object sender, EventArgs e)
        {
            HttpContext.Current.Response.Redirect(ResolveUrl(this.SignOnPageRelativeUrl + "?page=" + (int)SignedInPage.SignOff), true);
        }

        private enum SignedInPage
        { 
            SignOff = 0,
            ChangePassword = 1,
            PasswordChanged = 2
        }

        #endregion
    }
}