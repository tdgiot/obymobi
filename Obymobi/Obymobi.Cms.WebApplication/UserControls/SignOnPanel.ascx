<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.UserControls.SignOnPanel" Codebehind="SignOnPanel.ascx.cs" %>
<%@ Register Assembly="Dionysos.Web" Namespace="Dionysos.Web.UI.WebControls" TagPrefix="D" %>
<asp:Panel runat="server" ID="panelLogin">
    <D:Label runat="server" AssociatedControlID="tbLogin" ID="lblUsername">Gebruikersnaam</D:Label>
    <D:TextBoxString runat="server" ID="tbLogin" UseDataBinding="false" DefaultButton="btLoginDevEx" RenderLabel="false" UseValidation="false"></D:TextBoxString>
    <D:Label runat="server" AssociatedControlID="tbPassword" ID="lblPassword">Wachtwoord</D:Label>
    <D:TextBoxPassword runat="server" ID="tbPassword" DefaultButton="btLoginDevEx" UseDataBinding="false" RenderLabel="false" UseValidation="false"></D:TextBoxPassword>
    <div class="blockLeft">&nbsp;</div><div class="blockRight"><D:CheckBox runat="server" CssClass="checkbox" ID="cbRememberLogin" SaveAndRetrieveValueCookieName="SaveUserName" Text="Gebruikersnaam onthouden" /></div>
    <div class="blockLeft">&nbsp;</div><div class="blockRight"><X:Button runat="server" ID="btLoginDevEx" CssClass="button" Text="Inloggen" /></div>		 
    <asp:PlaceHolder runat="server" Visible="false"><a href="RetrievePassword.aspx" class="colorLight" style="line-height:21px;">Wachtwoord&nbsp;vergeten?</a></asp:PlaceHolder><br />
</asp:Panel>

<asp:Panel runat="server" ID="panelLoggedIn">
    <div class="blockLeft"><strong><D:LabelTextOnly runat="server" ID="lblMakeAChoice">Maak een keuze</D:LabelTextOnly></strong></div><div class="blockRight"><D:HyperLink runat="server" NavigateUrl="~/Default.aspx" ID="hlGoToSystem">Doorgaan naar het systeem</D:HyperLink></D:LinkButton><br /></div>
    <div class="blockLeft">&nbsp;</div><div class="blockRight"><D:LinkButton runat="server" ID="lnkChangePassword">Wachtwoord wijzigen</D:LinkButton><br /></div>
    <div class="blockLeft">&nbsp;</div><div class="blockRight"><D:LinkButton runat="server" ID="lbSignOut">Uitloggen</D:LinkButton><br /></div>
    <div class="blockLeft">&nbsp;</div><div class="blockRight">&nbsp;</div>
    <div class="blockLeft"><D:LabelTextOnly runat="server" ID="lblSignedOnAs">Ingelogd als</D:LabelTextOnly></div><div class="blockRight"><D:Label NavigateUrl="~/Profile.aspx" runat="server" ID="lblUsernameValue" LocalizeText="false"></D:Label></div>    
</asp:Panel>

<asp:Panel runat="server" ID="panelChangePassword" Visible="false">    
    <D:Label runat="server" AssociatedControlID="tbCurrentPassword" ID="lblCurrentPassword">Huidige wachtwoord</D:Label>
    <D:TextBoxPassword runat="server" ID="tbCurrentPassword" DefaultButton="btChangePassword" UseDataBinding="false" RenderLabel="false" UseValidation="false"></D:TextBoxPassword>
    <D:Label runat="server" AssociatedControlID="tbNewPassword" ID="lblNewPassword">Nieuw wachtwoord</D:Label>
    <D:TextBoxPassword runat="server" ID="tbNewPassword" DefaultButton="btChangePassword" UseDataBinding="false" RenderLabel="false" UseValidation="false"></D:TextBoxPassword>
    <D:Label runat="server" AssociatedControlID="tbConfirmNewPassword" ID="lblConfirmNewPassword">Bevestig wachtwoord</D:Label>
    <D:TextBoxPassword runat="server" ID="tbConfirmNewPassword" DefaultButton="btChangePassword" UseDataBinding="false" RenderLabel="false" UseValidation="false"></D:TextBoxPassword>  
    <div class="blockLeft">&nbsp;</div><div class="blockRight"><X:Button runat="server" ID="btBack" CssClass="button" Text="Terug" />&nbsp;<X:Button runat="server" ID="btChangePassword" CssClass="button" Text="Wijzigen" /><br /></div>
</asp:Panel>

<asp:Panel runat="server" ID="panelPasswordChanged" visible="false">
    <div class="blockPadding">
        <D:LabelTextOnly runat="server" ID="lblPasswordChanged1">Uw wachtwoord is succesvol gewijzigd. </D:LabelTextOnly><br />
        <D:LabelTextOnly runat="server" ID="lblPasswordChanged2">Klik </D:LabelTextOnly>
        <D:HyperLink runat="server" NavigateUrl="~/Default.aspx" ID="hlHere">hier</D:HyperLink> 
        <D:LabelTextOnly runat="server" ID="lblPasswordChanged3"> om terug te gaan naar het systeem.</D:LabelTextOnly>
    </div>
</asp:Panel>

<script>
	document.addEventListener("keyup", function (e) {
		if (e.keyCode === 13) {
            
			var username = document.getElementById("SignOn1_tbLogin");
			var password = document.getElementById("SignOn1_tbPassword");
			var loginBtn = document.getElementById("SignOn1_btLoginDevEx");

			if (!username || !password || !loginBtn) {
				return;
			}

			if (username.value !== "" && password.value !== "") {
				loginBtn.click();
			}
		}
	});
</script>