﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Utils;
using DevExpress.Web;
using DevExpress.Web;
using DevExpress.Web.Internal;
using DevExpress.Web.ASPxTreeList;
using DevExpress.Web.ASPxTreeList.Internal;
using DevExpress.Web.Data;
using Dionysos;
using Obymobi.Cms.WebApplication.Old_App_Code.DatasSources.MenuPosItems;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;

namespace Obymobi.ObymobiCms.Tools
{
    public partial class PosMenu : Dionysos.Web.UI.PageLLBLGenEntity
    {
        #region Fields

        private MenuPosItemDataSource treelistDatasource;

        #endregion

        #region Methods

        private void CreateTreeList()
        {
            this.tlItems.SettingsEditing.Mode = TreeListEditMode.Inline;
            this.tlItems.SettingsPopupEditForm.Width = 600;
            this.tlItems.SettingsBehavior.ColumnResizeMode = ColumnResizeMode.Disabled;
            this.tlItems.SettingsBehavior.AllowSort = false;
            this.tlItems.SettingsBehavior.AllowDragDrop = false;
            this.tlItems.ClientInstanceName = this.TreelistId;
            this.tlItems.AutoGenerateColumns = false;
            this.tlItems.SettingsEditing.AllowNodeDragDrop = false;
            this.tlItems.SettingsEditing.AllowRecursiveDelete = false;
            this.tlItems.Settings.GridLines = GridLines.Both;
            this.tlItems.KeyFieldName = "MenuPosItemId";
            this.tlItems.ParentFieldName = "MenuPosParentItemId";
            this.tlItems.Width = Unit.Pixel(736);
            this.tlItems.ClientSideEvents.NodeDblClick = this.NodeDblClickMethod;
            this.tlItems.CustomErrorText += this.tlItems_CustomErrorText;

            TreeListTextColumn nameColumn = new TreeListTextColumn();
            nameColumn.Caption = "Name";
            nameColumn.VisibleIndex = 0;
            nameColumn.FieldName = "Name";
            nameColumn.CellStyle.Wrap = DefaultBoolean.True;
            nameColumn.EditFormSettings.VisibleIndex = 0;
            this.tlItems.Columns.Add(nameColumn);

            TreeListComboBoxColumn posItemColumn = new TreeListComboBoxColumn();
            posItemColumn.Caption = "POS item";
            posItemColumn.Width = Unit.Pixel(167);
            posItemColumn.VisibleIndex = 1;
            posItemColumn.FieldName = "PosItemName";
            posItemColumn.CellStyle.Wrap = DefaultBoolean.True;
            posItemColumn.EditFormSettings.VisibleIndex = 2;            
            this.tlItems.Columns.Add(posItemColumn);

            TreeListCommandColumn editCommandColumn = new TreeListCommandColumn();
            editCommandColumn.VisibleIndex = 4;
            editCommandColumn.Width = Unit.Pixel(15);
            editCommandColumn.ShowNewButtonInHeader = false;
            editCommandColumn.EditButton.Visible = true;
            editCommandColumn.NewButton.Visible = false;
            editCommandColumn.DeleteButton.Visible = false;
            editCommandColumn.ButtonType = ButtonType.Image;
            editCommandColumn.EditButton.Image.Url = this.ResolveUrl("~/images/icons/pencil.png");
            editCommandColumn.DeleteButton.Image.Url = this.ResolveUrl("~/images/icons/delete.png");
            editCommandColumn.CancelButton.Image.Url = this.ResolveUrl("~/images/icons/cross_grey.png");
            editCommandColumn.NewButton.Image.Url = this.ResolveUrl("~/images/icons/add2.png");
            editCommandColumn.UpdateButton.Image.Url = this.ResolveUrl("~/images/icons/disk.png");
            this.tlItems.Columns.Add(editCommandColumn);

            TreeListHyperLinkColumn editPageColumn = new TreeListHyperLinkColumn();
            editPageColumn.Name = "EditCommand";
            editPageColumn.Caption = " ";
            editPageColumn.VisibleIndex = 5;
            editPageColumn.Width = Unit.Pixel(15);
            editPageColumn.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            editPageColumn.PropertiesHyperLink.ImageUrl = "~/images/icons/page_edit.png";
            editPageColumn.EditCellStyle.CssClass = "hidden";
            editPageColumn.FieldName = "MenuPosItemId";
            editPageColumn.EditFormSettings.VisibleIndex = 2;
            editPageColumn.EditFormCaptionStyle.CssClass = "hidden";
            this.tlItems.Columns.Add(editPageColumn);

            this.tlItems.HtmlDataCellPrepared += this.tlItems_HtmlDataCellPrepared;
            this.tlItems.CellEditorInitialize += this.tlItems_CellEditorInitialize;            

            this.InitDataSource();
        }

        private void InitDataSource()
        {
            if (this.DataSource != null)
            {
                this.treelistDatasource = new MenuPosItemDataSource(this.DataSourceAsMenuEntity.MenuId);
                this.tlItems.DataSource = this.treelistDatasource;
            }

            this.hlExpandAll.NavigateUrl = string.Format("javascript:{0}.ExpandAll()", this.TreelistId);
            this.hlCollapseAll.NavigateUrl = string.Format("javascript:{0}.CollapseAll()", this.TreelistId);
        }

        private void LoadUserControls()
        {

        }

        private void SetGui()
        {
            if (!this.DataSourceAsMenuEntity.IsNew)
            {
                this.CreateTreeList();
            }
        }

        private void HookUpEvents()
        {

        }

        private void ExpandCategoryNodes(TreeListNodeCollection nodes, TreeListNode parentNode)
        {
            foreach (TreeListNode node in nodes)
            {
                if (node is TreeListRootNode)
                {
                    node.Expanded = true;
                }
                else if (node != null && 
                    parentNode != null &&  !parentNode.Expanded && 
                    node.DataItem is MenuPosItem &&  ((MenuPosItem)node.DataItem).EntityType.Equals(nameof(CategoryEntity)))
                {
                    parentNode.Expanded = true;
                }

                if (node.HasChildren)
                {
                    this.ExpandCategoryNodes(node.ChildNodes, node);
                }
            }
        }

        #endregion

        #region Properties

        public MenuEntity DataSourceAsMenuEntity
        {
            get
            {
                return this.DataSource as MenuEntity;
            }
        }

        private string TreelistId
        {
            get { return string.Format("{0}_treelist", this.ID); }
        }

        private string NodeDblClickMethod
        {
            get { return @" function(s, e) {
	                            " + this.TreelistId + @".StartEdit(e.nodeKey);
		                        e.cancel = true;	                                                                    
                            }"; }
        }

        #endregion

        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "Menu";
            this.LoadUserControls();
            this.DataSourceLoaded += this.Menu_DataSourceLoaded;
            base.OnInit(e);
        }

        private void Menu_DataSourceLoaded(object sender)
        {
            this.SetGui();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.tlItems.DataBind();
            this.ExpandCategoryNodes(this.tlItems.Nodes, null);

            ((MasterPages.MasterPageEntity)this.Master).ToolBar.SaveButton.Visible = false;
            ((MasterPages.MasterPageEntity)this.Master).ToolBar.SaveAndGoButton.Visible = false;
            ((MasterPages.MasterPageEntity)this.Master).ToolBar.SaveAndNewButton.Visible = false;
            ((MasterPages.MasterPageEntity)this.Master).ToolBar.DeleteButton.Visible = false;
        }

        private void tlItems_HtmlDataCellPrepared(object sender, TreeListHtmlDataCellEventArgs e)
        {
            if (e.Column.Name == "EditCommand" && e.NodeKey != null)
            {
                string navigateUrl = string.Empty;

                TreeListNode currentNode = this.tlItems.FindNodeByKeyValue(e.NodeKey);
                if (currentNode != null && currentNode.DataItem is MenuPosItem)
                {
                    MenuPosItem item = (MenuPosItem)currentNode.DataItem;
                    navigateUrl = this.GetNavigateUrlForMenuPosItem(item);
                }

                if (!navigateUrl.IsNullOrWhiteSpace())
                {
                    foreach (Control item in e.Cell.Controls)
                    {
                        HyperLinkDisplayControl link = item as HyperLinkDisplayControl;
                        if (link != null)
                        {
                            link.NavigateUrl = navigateUrl;
                            break;
                        }
                    }
                }
            }
        }

        private void tlItems_CellEditorInitialize(object sender, TreeListColumnEditorEventArgs e)
        {
            if (e.Column.FieldName.Equals("PosItemName", StringComparison.InvariantCultureIgnoreCase))
            {
                ASPxComboBox cbPosItem = e.Editor as ASPxComboBox;
                cbPosItem.IncrementalFilteringDelay = 100;
                cbPosItem.IncrementalFilteringMode = IncrementalFilteringMode.Contains;

                TreeListNode currentNode = this.tlItems.FindNodeByKeyValue(e.NodeKey);
                if (currentNode != null && currentNode.DataItem is MenuPosItem)
                {
                    MenuPosItem item = (MenuPosItem)currentNode.DataItem;

                    if (item.EntityType.Equals(nameof(CategoryEntity)))
                    {
                        cbPosItem.ValueField = nameof(PoscategoryFields.PoscategoryId);
                        cbPosItem.TextField = nameof(PoscategoryFields.Name);
                        cbPosItem.DataBindEntityCollection<PoscategoryEntity>(PoscategoryFields.CompanyId == CmsSessionHelper.CurrentCompanyId, PoscategoryFields.Name, PoscategoryFields.Name);
                    }
                    else if (item.EntityType.Equals(nameof(ProductEntity)))
                    {
                        cbPosItem.ValueField = nameof(PosproductFields.PosproductId);
                        cbPosItem.TextField = nameof(PosproductFields.ExternalId);//nameof(PosproductFields.Name);
                        cbPosItem.DataBindEntityCollection<PosproductEntity>(PosproductFields.CompanyId == CmsSessionHelper.CurrentCompanyId, PosproductFields.ExternalId, new SD.LLBLGen.Pro.ORMSupportClasses.IEntityFieldCore[] { PosproductFields.ExternalId, PosproductFields.ExternalId, PosproductFields.RevenueCenter});
                    }
                    else if (item.EntityType.Equals(nameof(AlterationEntity)))
                    {
                        cbPosItem.ValueField = nameof(PosalterationFields.PosalterationId);
                        cbPosItem.TextField = nameof(PosalterationFields.Name);
                        cbPosItem.DataBindEntityCollection<PosalterationEntity>(PosalterationFields.CompanyId == CmsSessionHelper.CurrentCompanyId, PosalterationFields.Name, PosalterationFields.Name);
                    }
                    else if (item.EntityType.Equals(nameof(AlterationoptionEntity)))
                    {
                        cbPosItem.ValueField = nameof(PosalterationoptionFields.PosalterationoptionId);
                        cbPosItem.TextField = nameof(PosalterationoptionFields.Name);
                        cbPosItem.DataBindEntityCollection<PosalterationoptionEntity>(PosalterationoptionFields.CompanyId == CmsSessionHelper.CurrentCompanyId, PosalterationoptionFields.Name, PosalterationoptionFields.Name);
                    }
                }
            }
        }

        private string GetNavigateUrlForMenuPosItem(MenuPosItem item)
        {
            string url = string.Empty;

            if (item.EntityType.Equals(nameof(CategoryEntity)))
            {
                url = string.Format("~/Catalog/Category.aspx?id={0}", item.PrimaryKey);
            }
            else if (item.EntityType.Equals(nameof(ProductEntity)))
            {
                url = string.Format("~/Catalog/Product.aspx?id={0}", item.PrimaryKey);
            }
            else if (item.EntityType.Equals(nameof(AlterationEntity)))
            {
                url = string.Format("~/Catalog/Alteration.aspx?id={0}", item.PrimaryKey);
            }
            else if (item.EntityType.Equals(nameof(AlterationoptionEntity)))
            {
                url = string.Format("~/Catalog/Alterationoption.aspx?id={0}", item.PrimaryKey);
            }

            return url;
        }

        private void tlItems_CustomErrorText(object sender, TreeListCustomErrorTextEventArgs e)
        {
            if (e.Exception.InnerException != null)
            {
                ObymobiException exception = e.Exception.InnerException as ObymobiException;
                if (exception != null)
                {
                    e.ErrorText = exception.AdditionalMessage;
                }
                else
                {
                    e.ErrorText = e.Exception.InnerException.Message;
                }
            }
        }

        #endregion
    }
}
