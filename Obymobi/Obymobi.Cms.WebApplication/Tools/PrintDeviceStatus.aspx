﻿<%@ Page Title="Print Device Status" Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Tools.PrintDeviceStatus" Codebehind="PrintDeviceStatus.aspx.cs" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cplhTitleHolder">Print Device Status</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="cplhToolbarHolder">        
    <X:ToolBarButton runat="server" ID="btnExport" CommandName="Export" ClientInstanceName="exportButton" Text="Export" Image-Url="~/images/icons/report.png" style="margin-left: 3px;float:left"></X:ToolBarButton> 
    <X:ToolBarButton runat="server" ID="btnView" CommandName="View" ClientInstanceName="viewButton" Text="View" Image-Url="~/images/icons/book_open.png" style="margin-left: 3px;float:left"></X:ToolBarButton> 
</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="cplhContentHolder">
<div>
	<X:PageControl ID="tabsMain" runat="server" Width="100%">
		<TabPages>
            <X:TabPage Text="Report" Name="Report">
				<Controls>                    
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
				                <D:Label runat="server" ID="lblIncludeOfflineDevices">Include offline devices</D:Label>
			                </td>
			                <td class="control">
				                <D:CheckBox runat="server" ID="cbIncludeOfflineDevices" />
			                </td>							
			                <td class="label"></td>
			                <td class="control"></td>
                        </tr>
                        <tr>
                            <td class="label">
				                <D:Label runat="server" ID="lblClientTypes">Client Types:</D:Label>
			                </td>
			                <td class="control">
				                <D:CheckBoxList runat="server" ID="cblClientTypes" />
			                </td>							
			                <td class="label"></td>
			                <td class="control"></td>
                        </tr>
                    </table>
                    <D:Panel ID="pnlDeviceStatus" runat="server" GroupingText="Device Status" Visible="False">
                        <D:PlaceHolder runat="server" ID="plhViewResult">
                        </D:PlaceHolder>
                    </D:Panel>
                </Controls>
            </X:TabPage>			
        </TabPages>
    </X:PageControl>    
</div>
</asp:Content>