﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dionysos.Web.UI;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dionysos.Web;
using Obymobi.Data;
using Obymobi.Data.EntityClasses;
using Obymobi.ObymobiCms.UI;
using System.Data;
using DevExpress.Data;
using Dionysos;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Constants;

namespace Obymobi.ObymobiCms.Tools
{
    public partial class RoomControlConfigurationManagement : PageLLBLGenEntityCms
    {
        #region Methods                

        private void LoadUserControls()
        {
            var sort = new SortExpression(RoomControlConfigurationFields.Name | SortOperator.Ascending);
            var filter = new PredicateExpression(RoomControlConfigurationFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

            var roomControlConfigurations = new RoomControlConfigurationCollection();
            roomControlConfigurations.GetMulti(filter, 0, sort);

            this.ddlRoomControlConfigurationId.DataSource = roomControlConfigurations;
            this.ddlRoomControlConfigurationId.DataBind();
        }

        private void SetGui()
        {
            DataTable deliverypoints = this.LoadDeliverypoints();            

            this.dpGrid.DataSource = deliverypoints;
            this.dpGrid.DataBind();

            this.dpDisconnectedClients.DataSource = this.LoadClients();
            this.dpDisconnectedClients.DataBind();
        }

        private DataTable LoadDeliverypoints()
        {
            var dataTable = new DataTable();

            var deliverypointIdColumn = new DataColumn("DeliverypointId");
            var deliverypointNumberColumn = new DataColumn("DeliverypointNumber");
            var deliverypointNameColumn = new DataColumn("DeliverypointName");
            var deliverypointgroupNameColumn = new DataColumn("DeliverypointgroupName");
            var deliverypointgroupIdColumn = new DataColumn("DeliverypointgroupId");
            var roomControllerIpColumn = new DataColumn("RoomControllerIp");
            var roomControlConfigurationIdColumn = new DataColumn("RoomControlConfigurationId");
            var roomControlConfigurationColumn = new DataColumn("RoomControlConfiguration");

            deliverypointIdColumn.Caption = this.Translate("clmnDeliverypointId", "Number");
            deliverypointNumberColumn.Caption = this.Translate("clmnDeliverypointNumber", "Number");
            deliverypointNameColumn.Caption = this.Translate("clmnDeliverypointName", "Name");
            deliverypointgroupNameColumn.Caption = this.Translate("clmnDeliverypointgroupName", "DPG");
            deliverypointgroupIdColumn.Caption = this.Translate("clmnDeliverypointgroupId", "DPG");
            roomControllerIpColumn.Caption = this.Translate("clmnRoomControllerIp", "Room controller ip");
            roomControlConfigurationIdColumn.Caption = this.Translate("clmnRoomControlConfigurationId", "Room control configuration");
            roomControlConfigurationColumn.Caption = this.Translate("clmnRoomControlConfiguration", "Room control configuration");

            dataTable.Columns.Add(deliverypointIdColumn);
            dataTable.Columns.Add(deliverypointNumberColumn);
            dataTable.Columns.Add(deliverypointNameColumn);
            dataTable.Columns.Add(deliverypointgroupNameColumn);
            dataTable.Columns.Add(deliverypointgroupIdColumn);
            dataTable.Columns.Add(roomControllerIpColumn);
            dataTable.Columns.Add(roomControlConfigurationIdColumn);
            dataTable.Columns.Add(roomControlConfigurationColumn);

            foreach (DeliverypointEntity deliverypointEntity in this.DataSourceTyped.DeliverypointCollection)
            {
                DataRow row = dataTable.NewRow();
                row["DeliverypointId"] = deliverypointEntity.DeliverypointId;
                row["DeliverypointNumber"] = deliverypointEntity.Number;
                row["DeliverypointName"] = deliverypointEntity.Name;
                row["DeliverypointgroupName"] = deliverypointEntity.DeliverypointgroupEntity.Name;
                row["DeliverypointgroupId"] = deliverypointEntity.DeliverypointgroupId;
                row["RoomControllerIp"] = deliverypointEntity.RoomControllerIp;
                row["RoomControlConfigurationId"] = deliverypointEntity.RoomControlConfigurationId;
                row["RoomControlConfiguration"] = deliverypointEntity.RoomControlConfigurationId.HasValue ? deliverypointEntity.RoomControlConfigurationEntity.Name : "";

                dataTable.Rows.Add(row);
            }

            return dataTable;
        }

        private DataTable LoadClients()
        {
            var dataTable = new DataTable();

            var clientIdColumn = new DataColumn("ClientId");
            var lastDeliverypointNumberColumn = new DataColumn("LastDeliverypointNumber");

            clientIdColumn.Caption = "Client";
            lastDeliverypointNumberColumn.Caption = "Deliverypoint";

            dataTable.Columns.Add(clientIdColumn);
            dataTable.Columns.Add(lastDeliverypointNumberColumn);

            PredicateExpression filter = new PredicateExpression();
            filter.Add(ClientFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            filter.Add(DeviceFields.LastRequestUTC >= DateTime.UtcNow.AddMilliseconds(ObymobiIntervals.DEVICE_OFFLINE_THRESHOLD * -1));
            filter.Add(ClientFields.RoomControlConnected == false);

            var relation = new RelationCollection(ClientEntity.Relations.DeviceEntityUsingDeviceId);

            ClientCollection clients = new ClientCollection();
            clients.GetMulti(filter, relation);

            foreach (ClientEntity client in clients)
            {
                DataRow row = dataTable.NewRow();
                row["ClientId"] = client.ClientId;
                row["LastDeliverypointNumber"] = client.LastDeliverypointId.HasValue ? client.LastDeliverypointEntity.Name : string.Empty;

                dataTable.Rows.Add(row);
            }

            return dataTable;
        }

        private new void Refresh()
        {
            WebShortcuts.Redirect(Request.RawUrl);
        }

        private void SetConfiguration()
        {
            int from;
            int to;
            if (!int.TryParse(this.tbFrom.Text, out from))
            {
                string errorMessage = ((Dionysos.Web.UI.PageDefault)this.Page).Translate("NoFromValueSet", "Vul een start nummer in");
                ((Dionysos.Web.UI.PageDefault)this.Page).AddInformator(Dionysos.Web.UI.InformatorType.Warning, errorMessage);
            }
            else if (!int.TryParse(this.tbTo.Text, out to))
            {
                string errorMessage = ((Dionysos.Web.UI.PageDefault)this.Page).Translate("NoToValueSet", "Vul een eind nummer in");
                ((Dionysos.Web.UI.PageDefault)this.Page).AddInformator(Dionysos.Web.UI.InformatorType.Warning, errorMessage);
            }
            else
            {
                int tmpTo = to;
                if (from > to)
                {
                    to = from;
                    from = tmpTo;
                    this.tbTo.Text = to.ToString();
                    this.tbFrom.Text = from.ToString();
                }

                var deliverypointsInRange = this.DataSourceTyped.DeliverypointCollection.Where(dp => int.Parse(dp.Number) >= from && int.Parse(dp.Number) <= to).ToList();
                for (int i = 0; i < deliverypointsInRange.Count; i++)
                {
                    if (this.ddlRoomControlConfigurationId.ValidId > 0)
                        deliverypointsInRange[i].RoomControlConfigurationId = this.ddlRoomControlConfigurationId.ValidId;
                    else
                        deliverypointsInRange[i].RoomControlConfigurationId = null;

                    deliverypointsInRange[i].Save();
                }

                this.SetGui();

                string message = ((Dionysos.Web.UI.PageDefault)this.Page).Translate("ConfigurationSet", "De room control configuratie is gezet voor de deliverypoints");
                ((Dionysos.Web.UI.PageDefault)this.Page).AddInformator(Dionysos.Web.UI.InformatorType.Information, message);                               
            }

            this.Validate();
        }

        #endregion

        #region Event handlers

        protected override void OnPreInit(EventArgs e)
        {
            this.EntityId = CmsSessionHelper.CurrentCompanyId;
            this.EntityName = "CompanyEntity";
            base.OnPreInit(e);
        }

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            this.DataSourceLoaded += Company_DataSourceLoaded;
            base.OnInit(e);
        }

        public override bool InitializeEntity()
        {
            if (this.EntityId > 0)
            {                
                var prefetch = new PrefetchPath(EntityType.CompanyEntity);
                IPrefetchPathElement prefetchDeliverypoints = prefetch.Add(CompanyEntityBase.PrefetchPathDeliverypointCollection);
                IPrefetchPathElement prefetchDeliverypointsRoomControlConfiguration = prefetchDeliverypoints.SubPath.Add(DeliverypointEntityBase.PrefetchPathRoomControlConfigurationEntity);
                IPrefetchPathElement prefetchRoomControlConfiguration = prefetch.Add(CompanyEntityBase.PrefetchPathRoomControlConfigurationCollection);
                
                this.DataSource = new CompanyEntity(this.EntityId, prefetch);
            }

            return base.InitializeEntity();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.HookupEvents();            
        }

        private void Company_DataSourceLoaded(object sender)
        {
            this.SetGui();
        }

        private void HookupEvents()
        {
            this.btnRefresh.Click += btnRefresh_Click;
            this.btnSetConfiguration.Click += btnSetConfiguration_Click;
        }

        private void btnSetConfiguration_Click(object sender, EventArgs e)
        {
            this.SetConfiguration();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            this.Refresh();
        }

        #endregion

        #region Properties

        public CompanyEntity DataSourceTyped
        {
            get { return this.DataSource as CompanyEntity; }
        }
        
        #endregion
    }
}
