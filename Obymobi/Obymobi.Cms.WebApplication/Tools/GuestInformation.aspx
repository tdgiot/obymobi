﻿<%@ Page Title="Guest information" Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Tools.GuestInformation" Codebehind="GuestInformation.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitleHolder" runat="Server">Guest information</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhToolbarHolder" runat="Server">
	<span class="toolbar">
		<X:ToolBarButton runat="server" ID="btGenerate" CommandName="GenerateReport" Text="Generate report" Image-Url="~/Images/Icons/report.png" />		
	</span>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhContentHolder" runat="Server">
	<D:PlaceHolder runat="server">
		<div>
			<X:PageControl ID="tabsMain" runat="server" Width="100%">
				<TabPages>
					<X:TabPage Text="Algemeen" Name="Generic">
						<controls>
                            <Table class="dataformV2">
					            <tr>
                                    <td class="label">
                                        <D:Label runat="server" ID="lblCheckin">Check-in</D:Label>
                                    </td>
                                    <td class="control">
                                        <X:DateEdit runat="server" ID="deCheckin" AllowNull="true"></X:DateEdit>
                                    </td>
                                    <td class="label">
                                        <D:Label runat="server" ID="lblCheckout">Check-out</D:Label>
                                    </td>
                                    <td class="control">
                                        <X:DateEdit runat="server" ID="deCheckout" AllowNull="true"></X:DateEdit>
                                    </td>
					            </tr>
                                <tr>
                                    <td class="label">
                                        <D:Label runat="server" ID="lblGroupName">Group name</D:Label>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxString runat="server" ID="tbGroupName"></D:TextBoxString>
                                    </td>
                                </tr>
                            </Table>
					    </controls>
					</X:TabPage>					
				</TabPages>
			</X:PageControl>			
		</div>
	</D:PlaceHolder>
</asp:Content>
