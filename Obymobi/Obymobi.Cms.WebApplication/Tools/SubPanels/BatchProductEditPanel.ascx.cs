﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using DevExpress.Web;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Tools.SubPanels
{
    public partial class BatchProductEditPanel : UserControl
    {
        private bool DisplayAdvancedOptions => CmsSessionHelper.CurrentRole == Role.GodMode;

        private void LoadUserControls()
        {
            cbMenuId.DataBindEntityCollection<MenuEntity>(MenuFields.CompanyId == CmsSessionHelper.CurrentCompanyId, MenuFields.Name, MenuFields.Name);
            cbMenuId.DisplayEmptyItem = DisplayAdvancedOptions;

            cbCategoryId.DisplayEmptyItem = DisplayAdvancedOptions;

            grid.SettingsPager.Mode = GridViewPagerMode.ShowPager;
            grid.SettingsPager.PageSize = 20;
            grid.SettingsEditing.BatchEditSettings.EditMode = GridViewBatchEditMode.Cell;
            grid.SettingsEditing.BatchEditSettings.StartEditAction = GridViewBatchStartEditAction.Click;

            ShowHideUpdateAll();
        }

        private void HookUpEvents()
        {
            cbMenuId.ValueChanged += CbMenuId_OnValueChanged;
            cbCategoryId.ValueChanged += CbCategoryId_OnValueChanged;
            grid.CellEditorInitialize += GridOnCellEditorInitialize;
            grid.DataBinding += GridOnDataBinding;
            btnTaxTariffUpdateAll.Click += (sender, args) => UpdateAllProducts(ProductFields.TaxTariffId.Name, cbAllTaxTariffId.Value);
            btnIsAlcoholUpdateAll.Click += (sender, args) => UpdateAllProducts(ProductFields.IsAlcoholic.Name, cbAllIsAlcohol.Checked);
        }

        private void SetGui()
        {
            SetTaxTariffDataSource();
            RefreshGrid();
        }

        private void ShowHideUpdateAll()
        {
            pnlUpdateAll.Visible = DisplayAdvancedOptions;
        }

        private LLBLGenProDataSource GetProductDataSource(int companyId, int? categoryId)
        {
            IPredicateExpression filter = new PredicateExpression();
            filter.Add(ProductFields.CompanyId == companyId);

            IRelationCollection relationCollection = new RelationCollection();

            if (categoryId.HasValue)
            {
                List<int> categoryIds = new List<int>();
                categoryIds.Add(cbCategoryId.ValidId);
                categoryIds.AddRange(new CategoryEntity(cbCategoryId.ValidId).GetChildrenCategoryIds());

                filter.Add(ProductCategoryFields.CategoryId == categoryIds);
                relationCollection.Add(ProductEntityBase.Relations.ProductCategoryEntityUsingProductId);
            }

            LLBLGenProDataSource dataSource = new LLBLGenProDataSource();
            dataSource.DataContainerType = DataSourceDataContainerType.EntityCollection;
            dataSource.EntityCollectionTypeName = typeof(ProductCollection).AssemblyQualifiedName;
            dataSource.FilterToUse = filter;
            dataSource.RelationsToUse = relationCollection;
            dataSource.SorterToUse = new SortExpression(ProductFields.Name | SortOperator.Ascending);

            return dataSource;
        }

        private void SetTaxTariffDataSource()
        {
            TaxTariffDataSource.FilterToUse = new PredicateExpression(TaxTariffFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            TaxTariffDataSource.SorterToUse = new SortExpression(TaxTariffFields.Name | SortOperator.Ascending);
        }

        private void RefreshCategories()
        {
            int menuId = cbMenuId.ValidId;
            if (menuId <= 0)
            {
                ClearCategories();
            }
            else
            {
                RenderCategories(FetchCategories(menuId));
            }
        }

        private void ClearCategories()
        {
            cbCategoryId.DataSource = new CategoryCollection();
            cbCategoryId.DataBind();
            cbCategoryId.Value = 0;
        }

        private void RenderCategories(CategoryCollection categoryCollection)
        {
            int? selectedValue = cbCategoryId.Value;
            cbCategoryId.DataSource = categoryCollection;
            cbCategoryId.DataBind();

            ListEditItem selectedItem = cbCategoryId.Items.FindByValue(selectedValue);
            cbCategoryId.Value = selectedItem == null ? null : selectedValue;
        }

        private static void AddCategories(CategoryCollection allCategoryCollection, CategoryCollection categoryCollection)
        {
            foreach (CategoryEntity categoryEntity in categoryCollection)
            {
                allCategoryCollection.Add(categoryEntity);

                AddCategories(allCategoryCollection, categoryEntity.ChildCategoryCollection);
            }
        }

        private static CategoryCollection FetchCategories(int menuId)
        {
            CategoryCollection flattenCategories = new CategoryCollection();
            AddCategories(flattenCategories, FetchRootCategories(menuId));

            return flattenCategories;
        }

        private static CategoryCollection FetchRootCategories(int menuId)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(CategoryFields.MenuId == menuId);
            filter.Add(CategoryFields.ParentCategoryId == DBNull.Value);

            SortExpression sort = new SortExpression(CategoryFields.SortOrder | SortOperator.Ascending);

            IncludeFieldsList includes = new IncludeFieldsList(CategoryFields.Name);

            IPrefetchPath prefetchPath = new PrefetchPath(EntityType.CategoryEntity);
            prefetchPath.Add(CategoryEntityBase.PrefetchPathChildCategoryCollection, 0, null, null, sort, includes)
                        .SubPath.Add(CategoryEntityBase.PrefetchPathChildCategoryCollection, 0, null, null, sort, includes)
                        .SubPath.Add(CategoryEntityBase.PrefetchPathChildCategoryCollection, 0, null, null, sort, includes)
                        .SubPath.Add(CategoryEntityBase.PrefetchPathChildCategoryCollection, 0, null, null, sort, includes);

            CategoryCollection categoryCollection = new CategoryCollection();
            categoryCollection.GetMulti(filter, 0, sort, null, null, includes, 0, 0);

            return categoryCollection;
        }

        private void UpdateAllProducts(string fieldName, object value)
        {
            if (!(grid.DataSource is LLBLGenProDataSource dataSource))
                return;

            if (!(dataSource.EntityCollection is ProductCollection productCollection))
                return;

            foreach (ProductEntity productEntity in productCollection)
            {
                productEntity.SetNewFieldValue(fieldName, value);
            }

            productCollection.SaveMulti();
            RefreshGrid();
        }

        private void RefreshGrid()
        {
            grid.DataBind();
        }

        protected override void OnInit(EventArgs e)
        {
            LoadUserControls();
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            HookUpEvents();
            SetGui();
        }

        private void CbMenuId_OnValueChanged(object sender, EventArgs e)
        {
            RefreshCategories();
        }

        private void CbCategoryId_OnValueChanged(object sender, EventArgs e)
        {
            RefreshGrid();
        }

        private void GridOnCellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            if (e.Column.FieldName == "TaxTariffId")
            {
                ASPxComboBox combo = (ASPxComboBox)e.Editor;
                combo.DataBindItems();

                ListEditItem item = new ListEditItem("None - Select ...", null);
                combo.Items.Insert(0, item);
            }
        }

        private void GridOnDataBinding(object sender, EventArgs e)
        {
            int companyId = CmsSessionHelper.CurrentCompanyId;
            int? categoryId = cbCategoryId.Value;

            if (DisplayAdvancedOptions || categoryId.HasValue)
            {
                grid.DataSource = GetProductDataSource(companyId, categoryId);
            }
        }
    }
}