﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using DevExpress.Web;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Tools.SubPanels
{
    public partial class BatchAlterationoptionEditPanel : UserControl
    {
        private int? currentCategoryId;
        private bool DisplayAdvancedOptions => CmsSessionHelper.CurrentRole == Role.GodMode;

        private void LoadUserControls()
        {
            cbMenuId.DataBindEntityCollection<MenuEntity>(MenuFields.CompanyId == CmsSessionHelper.CurrentCompanyId, MenuFields.Name, MenuFields.Name);
            cbMenuId.DisplayEmptyItem = DisplayAdvancedOptions;

            cbCategoryId.DisplayEmptyItem = DisplayAdvancedOptions;

            grid.SettingsPager.Mode = GridViewPagerMode.ShowPager;
            grid.SettingsPager.PageSize = 20;
            grid.SettingsEditing.BatchEditSettings.EditMode = GridViewBatchEditMode.Cell;
            grid.SettingsEditing.BatchEditSettings.StartEditAction = GridViewBatchStartEditAction.Click;
            grid.CustomUnboundColumnData += GridOnCustomUnboundColumnData;

            ShowHideUpdateAll();
        }

        private void HookUpEvents()
        {
            cbMenuId.ValueChanged += CbMenuId_OnValueChanged;
            cbCategoryId.ValueChanged += CbCategoryId_OnValueChanged;

            grid.DataBinding += GridOnDataBinding;
            grid.CellEditorInitialize += GridOnCellEditorInitialize;

            btnTaxTariffUpdateAll.Click += (sender, args) => UpdateAllAlterationoptions(AlterationoptionFields.TaxTariffId.Name, cbAllTaxTariffId.Value);
            btnIsAlcoholUpdateAll.Click += (sender, args) => UpdateAllAlterationoptions(AlterationoptionFields.IsAlcoholic.Name, cbAllIsAlcohol.Checked);
        }

        private void ShowHideUpdateAll()
        {
            pnlUpdateAll.Visible = DisplayAdvancedOptions;
        }

        private void SetGui()
        {
            DataBindCategories();
            DataBindTaxTariffDataSource();

            RefreshGrid();
        }

        private LLBLGenProDataSource GetAlterationoptionDataSource(int companyId, int? categoryId)
        {
            IPredicateExpression filter = new PredicateExpression();
            filter.Add(AlterationoptionFields.CompanyId == companyId);

            IRelationCollection relations = new RelationCollection
            {
                AlterationoptionEntityBase.Relations.AlterationitemEntityUsingAlterationoptionId,
                AlterationitemEntityBase.Relations.AlterationEntityUsingAlterationId
            };

            if (categoryId.HasValue)
            {
                IEnumerable<int> productAlterationIds = FetchAlterationIdsLinkedViaProducts(companyId, categoryId.Value);
                IEnumerable<int> categoryAlterationIds = FetchAlterationIdsLinkedViaCategory(companyId, categoryId.Value);
                IEnumerable<int> uniqueFilteredAlterationIds = categoryAlterationIds.Concat(productAlterationIds).Distinct();

                filter.Add(new FieldCompareRangePredicate(AlterationFields.AlterationId, uniqueFilteredAlterationIds));
            }

            IPrefetchPath prefetch = new PrefetchPath(EntityType.AlterationoptionEntity);
            prefetch.Add(AlterationoptionEntityBase.PrefetchPathAlterationitemCollection, new IncludeFieldsList(AlterationitemFields.AlterationId)).SubPath
                    .Add(AlterationitemEntityBase.PrefetchPathAlterationEntity, new IncludeFieldsList(AlterationFields.Name));

            return new LLBLGenProDataSource
            {
                DataContainerType = DataSourceDataContainerType.EntityCollection,
                EntityCollectionTypeName = typeof(AlterationoptionCollection).AssemblyQualifiedName,
                FilterToUse = filter,
                RelationsToUse = relations,
                SorterToUse = new SortExpression(AlterationoptionFields.Name | SortOperator.Ascending),
                PrefetchPathToUse = prefetch
            };
        }

        private void DataBindCategories()
        {
            IPredicateExpression filter = new PredicateExpression();
            filter.Add(CategoryFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

            CategoryCollection categoryCollection = new CategoryCollection();
            categoryCollection.GetMulti(filter);

            cbCategoryId.DataSource = categoryCollection;
            cbCategoryId.DataBind();
        }

        private void DataBindTaxTariffDataSource()
        {
            TaxTariffDataSource.FilterToUse = new PredicateExpression(TaxTariffFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            TaxTariffDataSource.SorterToUse = new SortExpression(TaxTariffFields.Name | SortOperator.Ascending);
        }

        private void UpdateAllAlterationoptions(string fieldName, object value)
        {
            AlterationoptionCollection alterationoptionCollection = GetGridDataSourceAsAlterationoptionCollection();
            if (alterationoptionCollection == null)
            {
                return;
            }

            foreach (AlterationoptionEntity alterationoptionEntity in alterationoptionCollection)
            {
                alterationoptionEntity.SetNewFieldValue(fieldName, value);
            }

            alterationoptionCollection.SaveMulti();
            RefreshGrid();
        }

        private AlterationoptionCollection GetGridDataSourceAsAlterationoptionCollection()
        {
            if (!(grid.DataSource is LLBLGenProDataSource dataSource))
            {
                return null;
            }

            if (!(dataSource.EntityCollection is AlterationoptionCollection alterationoptionCollection))
            {
                return null;
            }

            return alterationoptionCollection;
        }

        private ProductCollection FetchProductsUsingAlterationoption(int alterationoptionId)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(AlterationitemFields.AlterationoptionId == alterationoptionId);

            RelationCollection joins = new RelationCollection();
            joins.Add(ProductEntityBase.Relations.ProductAlterationEntityUsingProductId);
            joins.Add(ProductAlterationEntityBase.Relations.AlterationEntityUsingAlterationId);
            joins.Add(AlterationEntityBase.Relations.AlterationitemEntityUsingAlterationId);

            SortExpression sort = new SortExpression();
            sort.Add(ProductFields.Name | SortOperator.Ascending);

            IncludeFieldsList includes = new IncludeFieldsList(ProductFields.Name);

            ProductCollection products = new ProductCollection();
            products.GetMulti(filter, 0, null, joins, null, includes, 0, 0);

            return products;            
        }

        private void RefreshGrid()
        {
            grid.DataBind();
        }

        protected override void OnInit(EventArgs e)
        {
            LoadUserControls();

            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            HookUpEvents();
            SetGui();
        }

        private void GridOnCellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            if (e.Column.FieldName == "TaxTariffId")
            {
                ASPxComboBox combo = (ASPxComboBox)e.Editor;
                combo.DataBindItems();

                ListEditItem item = new ListEditItem("Inherit from Product", null);
                combo.Items.Insert(0, item);
            }
        }

        private void CbMenuId_OnValueChanged(object sender, EventArgs e)
        {
            RefreshCategories();
        }

        private void CbCategoryId_OnValueChanged(object sender, EventArgs e)
        {
            if (currentCategoryId != cbCategoryId.Value)
            {
                grid.DataSource = GetAlterationoptionDataSource(CmsSessionHelper.CurrentCompanyId, currentCategoryId);
            }

            currentCategoryId = cbCategoryId.IsValid ? cbCategoryId.Value : null;

            RefreshGrid();
        }

        private void RefreshCategories()
        {
            int menuId = cbMenuId.ValidId;
            if (menuId <= 0)
            {
                ClearCategories();
            }
            else
            {
                RenderCategories(FetchCategories(menuId));
            }
        }

        private void ClearCategories()
        {
            cbCategoryId.DataSource = new CategoryCollection();
            cbCategoryId.DataBind();
            cbCategoryId.Value = 0;
        }

        private void RenderCategories(CategoryCollection categoryCollection)
        {
            int? selectedValue = cbCategoryId.Value;
            cbCategoryId.DataSource = categoryCollection;
            cbCategoryId.DataBind();

            ListEditItem selectedItem = cbCategoryId.Items.FindByValue(selectedValue);
            cbCategoryId.Value = selectedItem == null ? null : selectedValue;
        }

        private static void AddCategories(CategoryCollection allCategoryCollection, CategoryCollection categoryCollection)
        {
            foreach (CategoryEntity categoryEntity in categoryCollection)
            {
                allCategoryCollection.Add(categoryEntity);

                AddCategories(allCategoryCollection, categoryEntity.ChildCategoryCollection);
            }
        }

        private static CategoryCollection FetchCategories(int menuId)
        {
            CategoryCollection flattenCategories = new CategoryCollection();
            AddCategories(flattenCategories, FetchRootCategories(menuId));

            return flattenCategories;
        }

        private static CategoryCollection FetchRootCategories(int menuId)
        {
            PredicateExpression filter = new PredicateExpression
            {
                CategoryFields.MenuId == menuId,
                CategoryFields.ParentCategoryId == DBNull.Value
            };

            SortExpression sort = new SortExpression(CategoryFields.SortOrder | SortOperator.Ascending);

            IncludeFieldsList includes = new IncludeFieldsList(CategoryFields.Name);

            IPrefetchPath prefetchPath = new PrefetchPath(EntityType.CategoryEntity);
            prefetchPath.Add(CategoryEntityBase.PrefetchPathChildCategoryCollection, 0, null, null, sort, includes)
                        .SubPath.Add(CategoryEntityBase.PrefetchPathChildCategoryCollection, 0, null, null, sort, includes)
                        .SubPath.Add(CategoryEntityBase.PrefetchPathChildCategoryCollection, 0, null, null, sort, includes)
                        .SubPath.Add(CategoryEntityBase.PrefetchPathChildCategoryCollection, 0, null, null, sort, includes);

            CategoryCollection categoryCollection = new CategoryCollection();
            categoryCollection.GetMulti(filter, 0, sort, null, null, includes, 0, 0);

            return categoryCollection;
        }

        private void GridOnDataBinding(object sender, EventArgs e)
        {
            int companyId = CmsSessionHelper.CurrentCompanyId;
            int? categoryId = this.cbCategoryId.Value;

            grid.DataSource = GetAlterationoptionDataSource(companyId, categoryId);
        }

        private void GridOnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
        {
            if (e.IsGetData && e.Column.FieldName == "Alterations")
            {
                object key = e.GetListSourceFieldValue(e.ListSourceRowIndex, grid.KeyFieldName);
                e.Value = GetAlterationNames(key);                
            }
        }

        protected void Popup_WindowCallback(object source, PopupWindowCallbackArgs e)
        {
            if (!int.TryParse(e.Parameter, out int alterationoptionId))
            {
                return;
            }
            
            ProductCollection productCollection = FetchProductsUsingAlterationoption(alterationoptionId);

            IEnumerable<ProductEntity> distinctProducts = productCollection.GroupBy(product => product.ProductId)
                .Select(groupedProducts => groupedProducts.First())
                .ToList();

            if (!distinctProducts.Any())
            {
                plhProducts.AddHtml("No products found");
                return;
            }

            plhProducts.AddHtml("<ul>");
            foreach (ProductEntity product in distinctProducts)
            {
                string url = this.ResolveUrl($"~/Catalog/Product.aspx?Id={product.ProductId}");

                plhProducts.AddHtml($"<li style='list-style: disc; margin-left:20px'><a href=\"{url}\" target=\"_blank\">{product.Name}</a></li>");
            }
            plhProducts.AddHtml("</ul>");
        }        

        private string GetAlterationNames(object key)
        {
            AlterationoptionCollection alterationoptionCollection = GetGridDataSourceAsAlterationoptionCollection();
            if (alterationoptionCollection == null || !int.TryParse(key.ToString(), out int alterationoptionId))
            {
                return string.Empty;
            }

            AlterationoptionEntity alterationoptionEntity = alterationoptionCollection.FirstOrDefault(alterationoption => alterationoption.AlterationoptionId == alterationoptionId);
            if (alterationoptionEntity == null)
            {
                return string.Empty;
            }

            return string.Join(", ", alterationoptionEntity.AlterationitemCollection.Select(alterationitem => alterationitem.AlterationName));
        }

        private IEnumerable<int> FetchAlterationIdsLinkedViaProducts(int companyId, int categoryId)
        {
            List<int> categoryIds = new List<int> { categoryId };
            categoryIds.AddRange(new CategoryEntity(categoryId).GetChildrenCategoryIds());

            IPredicateExpression filter = new PredicateExpression()
            {
                ProductCategoryFields.ParentCompanyId == companyId,
                ProductCategoryFields.CategoryId == categoryIds
            };

            IRelationCollection relations = new RelationCollection
            {
                ProductAlterationEntityBase.Relations.ProductEntityUsingProductId,
                ProductEntityBase.Relations.ProductCategoryEntityUsingProductId,
            };

            ProductAlterationCollection productAlterationCollection = new ProductAlterationCollection();
            productAlterationCollection.GetMulti(filter, relations);

            return productAlterationCollection.Select(productAlterationEntity => productAlterationEntity.AlterationId);
        }

        private IEnumerable<int> FetchAlterationIdsLinkedViaCategory(int companyId, int categoryId)
        {
            List<int> categoryIds = new List<int> { categoryId };
            categoryIds.AddRange(new CategoryEntity(categoryId).GetChildrenCategoryIds());

            IPredicateExpression filter = new PredicateExpression
            {
                CategoryAlterationFields.ParentCompanyId == companyId,
                CategoryAlterationFields.CategoryId == categoryIds
            };

            CategoryAlterationCollection categoryAlterationCollection = new CategoryAlterationCollection();
            categoryAlterationCollection.GetMulti(filter);

            return categoryAlterationCollection.Select(categoryAlterationEntity => categoryAlterationEntity.AlterationId);
        }
    }
}
