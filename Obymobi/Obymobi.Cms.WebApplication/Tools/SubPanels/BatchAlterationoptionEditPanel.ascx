﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Tools.SubPanels.BatchAlterationoptionEditPanel" CodeBehind="BatchAlterationoptionEditPanel.ascx.cs" %>
<%@ Register TagPrefix="llblgenpro" Namespace="SD.LLBLGen.Pro.ORMSupportClasses" Assembly="SD.LLBLGen.Pro.ORMSupportClasses.Web, Version=4.2.0.0, Culture=neutral, PublicKeyToken=ca73b74ba4e3ff27" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<script type="text/javascript">
    function OnCustomButtonClick(s, e) {
        OnFindAllReferencesClick(grid.GetRowKey(e.visibleIndex))
    }

    function OnFindAllReferencesClick(keyValue) {
        pcPopup.Show();
        pcPopup.PerformCallback(keyValue);
    }
</script>

<D:Panel ID="pnlFilter" GroupingText="Filter" LocalizeText="false" runat="server">
    <table class="dataformV2">
        <tr>
            <td class="label">
                <D:Label runat="server" ID="lblMenu" Text="Choose a menu" LocalizeText="False"></D:Label>
            </td>
            <td class="control">
                <X:ComboBoxInt runat="server" ID="cbMenuId" TextField="Name" ValueField="MenuId" IncrementalFilteringMode="Contains" IncrementalFilteringDelay="1000" AutoPostBack="True" UseDataBinding="true" DisplayEmptyItem="False" EmptyItemText="All menus" LocalizeEmptyItemText="False" notdirty="true" />
            </td>
            <td class="label">
                <a id="filter-info-popup" href="#" style="float: left;">
                    <img src="<%= ResolveUrl("~/Images/Icons/information.png") %>" style="float: right;" alt="Filter information" />
                </a>
            </td>
            <td class="control"></td>
        </tr>
        <tr>
            <td class="label">
                <D:Label runat="server" ID="lblCategory" Text="Choose a category" LocalizeText="False"></D:Label>
            </td>
            <td class="control">
                <X:ComboBoxInt runat="server" ID="cbCategoryId" TextField="FullCategoryName" ValueField="CategoryId" IncrementalFilteringMode="Contains" IncrementalFilteringDelay="1000" AutoPostBack="True" UseDataBinding="true" DisplayEmptyItem="False" EmptyItemText="All categories" LocalizeEmptyItemText="False" notdirty="true" />
            </td>
            <td class="label"></td>
            <td class="control"></td>
        </tr>
    </table>
</D:Panel>

<D:Panel runat="server" ID="pnlUpdateAll" GroupingText="Update all results" LocalizeText="false">
    <table class="dataformV2">
        <tr>
            <td class="label">
                <D:Label runat="server" ID="lblTaxTariff" Text="Tax Tariff" />
            </td>
            <td class="control">
                <X:ComboBoxInt runat="server" ID="cbAllTaxTariffId" DataSourceID="TaxTariffDataSource" ValueType="System.Int32" ValueField="TaxTariffId" TextField="Name" EmptyItemText="None - Select ..." />
            </td>
            <td>
                <D:Button runat="server" ID="btnTaxTariffUpdateAll" Text="Update All" PreSubmitWarning="All results will be automatically saved. Are you sure you want to continue?" />
            </td>
            <td />
        </tr>
        <tr>
            <td class="label">
                <D:Label runat="server" ID="lblIsAlcohol" Text="Is Alcohol" />
            </td>
            <td class="control">
                <dx:ASPxCheckBox ID="cbAllIsAlcohol" runat="server" Text="Check/Uncheck all" />
            </td>
            <td>
                <D:Button runat="server" ID="btnIsAlcoholUpdateAll" Text="Update All" PreSubmitWarning="All results will be automatically saved. Are you sure you want to continue?" />
            </td>
            <td />
        </tr>
    </table>
</D:Panel>

<D:Panel runat="server" ID="pnlResults" GroupingText="Results" LocalizeText="false">
    <div class="batch_edit_header">
        <div class="batch_edit_column" style="width: 78%">
            &nbsp;
        </div>
        <div class="batch_edit_column" style="width: 11%">
            <X:ComboBoxInt runat="server" ID="cbTaxTariffId" DataSourceID="TaxTariffDataSource" ValueType="System.Int32" ValueField="TaxTariffId" TextField="Name" LocalizeEmptyItemText="False" EmptyItemText="Inherit from Product">
                <ClientSideEvents SelectedIndexChanged="function(s, e) { setValueForAllRows('TaxTariffId', s.GetSelectedItem().value) }" />
            </X:ComboBoxInt>
        </div>
        <div class="batch_edit_column" style="width: 11%">
            <dx:ASPxCheckBox ID="cbAlcoholAll" runat="server" Text="Check/Uncheck all" Style="margin-left: auto; margin-right: auto;">
                <ClientSideEvents CheckedChanged="function (s,e) { setValueForAllRows('IsAlcoholic', s.GetChecked()) }" />
            </dx:ASPxCheckBox>
        </div>
    </div>
</D:Panel>

<dx:ASPxGridView ID="grid" ClientInstanceName="grid" runat="server" KeyFieldName="AlterationoptionId" EnableRowsCache="false" Width="100%">
    <SettingsBehavior AutoFilterRowInputDelay="350" />
    <Settings ShowFilterRow="True" VerticalScrollableHeight="594" />
    <SettingsEditing Mode="Batch" />
    <ClientSideEvents CustomButtonClick="OnCustomButtonClick" />
    <Columns>
        <dx:GridViewDataTextColumn FieldName="Name" Caption="Alterationoption" ReadOnly="True" Width="26%">
            <Settings AutoFilterCondition="Contains" />
            <PropertiesTextEdit>
                <ValidationSettings Display="Dynamic" RequiredField-IsRequired="true" />
            </PropertiesTextEdit>
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="Alterations" Caption="Alterations" UnboundType="String" ReadOnly="True" Width="26%">
            <Settings AutoFilterCondition="Contains" />
        </dx:GridViewDataTextColumn>
        <dx:GridViewCommandColumn Caption="Products" Width="26%">
            <CustomButtons>
                <dx:GridViewCommandColumnCustomButton ID="btnFindAllReferences" Text="Find All References" />
            </CustomButtons>
        </dx:GridViewCommandColumn>
        <dx:GridViewDataComboBoxColumn FieldName="TaxTariffId" Caption="Tax Tariff" Width="11%">
            <Settings AutoFilterCondition="Contains" FilterMode="DisplayText" SortMode="DisplayText" />
            <PropertiesComboBox DataSourceID="TaxTariffDataSource" ValueType="System.Int32" ValueField="TaxTariffId" TextField="Name" NullDisplayText="Inherit from Product" />
        </dx:GridViewDataComboBoxColumn>
        <dx:GridViewDataCheckColumn FieldName="IsAlcoholic" Caption="Is Alcohol" Width="11%" />
    </Columns>
</dx:ASPxGridView>

<dx:ASPxPopupControl ID="pcMenuFilter" ClientInstanceName="pcMenuFilter" PopupElementID="filter-info-popup" Width="350" PopupAction="MouseOver" CloseAction="MouseOut" CloseOnEscape="false" Modal="false" ShowOnPageLoad="false" HeaderText="Filter information" runat="server">
    <ContentCollection>
        <dx:PopupControlContentControl runat="server">
            <p>
                Note: This filter only affects the category filter.
                The grid below won't be affected.
            </p>
        </dx:PopupControlContentControl>
    </ContentCollection>
</dx:ASPxPopupControl>

<dx:ASPxPopupControl ID="pcPopup" ClientInstanceName="pcPopup" runat="server" Width="500" CloseAction="CloseButton" CloseOnEscape="true" Modal="True" ShowOnPageLoad="False" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
    HeaderText="Products" AllowDragging="False" PopupAnimationType="None" EnableViewState="False" AutoUpdatePosition="true" OnWindowCallback="Popup_WindowCallback">
    <ContentCollection>
        <dx:PopupControlContentControl runat="server">
            <dx:ASPxPanel ID="ASPxPanel1" runat="server" DefaultButton="btOK">
                <PanelCollection>
                    <dx:PanelContent runat="server">
                        <div style="margin-bottom: 8px">
                            <D:PlaceHolder runat="server" ID="plhProducts"></D:PlaceHolder>
                        </div>
                    </dx:PanelContent>
                </PanelCollection>
            </dx:ASPxPanel>
        </dx:PopupControlContentControl>
    </ContentCollection>
    <ContentStyle>
        <Paddings PaddingBottom="5px" />
    </ContentStyle>
</dx:ASPxPopupControl>

<llblgenpro:LLBLGenProDataSource
    runat="server"
    ID="TaxTariffDataSource"
    DataContainerType="EntityCollection"
    EntityCollectionTypeName="Obymobi.Data.CollectionClasses.TaxTariffCollection, Obymobi.Data" />
