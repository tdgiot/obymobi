﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Tools.SubPanels.BatchProductEditPanel" Codebehind="BatchProductEditPanel.ascx.cs" %>
<%@ Register TagPrefix="llblgenpro" Namespace="SD.LLBLGen.Pro.ORMSupportClasses" Assembly="SD.LLBLGen.Pro.ORMSupportClasses.Web, Version=4.2.0.0, Culture=neutral, PublicKeyToken=ca73b74ba4e3ff27" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<D:Panel runat="server" ID="pnlFilter" GroupingText="Filter" LocalizeText="false">
    <table class="dataformV2">
        <tr>
            <td class="label">
                <D:Label runat="server" ID="lblMenu" Text="Choose a menu" LocalizeText="False"></D:Label>
            </td>
            <td class="control">
                <X:ComboBoxInt runat="server" ID="cbMenuId" TextField="Name" ValueField="MenuId" IncrementalFilteringMode="Contains" IncrementalFilteringDelay="1000" AutoPostBack="True" UseDataBinding="true" DisplayEmptyItem="False" EmptyItemText="All menus" LocalizeEmptyItemText="False" notdirty="true" />
            </td>
            <td class="label">
                <a id="filter-info-popup" href="#" style="float: left;">
                    <img src="<%= ResolveUrl("~/Images/Icons/information.png") %>" style="float: right;" alt="Filter information" />
                </a>
            </td>
            <td class="control"></td>
        </tr>
        <tr>
            <td class="label">
                <D:Label runat="server" ID="lblCategory" Text="Choose a category" LocalizeText="False"></D:Label>
            </td>
            <td class="control">
                <X:ComboBoxInt runat="server" ID="cbCategoryId" TextField="FullCategoryName" ValueField="CategoryId" IncrementalFilteringMode="Contains" IncrementalFilteringDelay="1000" AutoPostBack="True" UseDataBinding="true" DisplayEmptyItem="False" EmptyItemText="All categories" LocalizeEmptyItemText="False" notdirty="true" />
            </td>
            <td class="label"></td>
            <td class="control"></td>
        </tr>
    </table>
</D:Panel>

<D:Panel runat="server" ID="pnlUpdateAll" GroupingText="Update all results" LocalizeText="false">
    <table class="dataformV2">
        <tr>
            <td class="label">
                <D:Label runat="server" ID="lblTaxTariff" Text="Tax Tariff" />
            </td>
            <td class="control">
                <X:ComboBoxInt runat="server" ID="cbAllTaxTariffId" DataSourceID="TaxTariffDataSource" ValueType="System.Int32" ValueField="TaxTariffId" TextField="Name" EmptyItemText="None - Select ..." />
            </td>
            <td>
                <D:Button runat="server" ID="btnTaxTariffUpdateAll" Text="Update All" PreSubmitWarning="All results will be automatically saved. Are you sure you want to continue?" />
            </td>
            <td/>
        </tr>
        <tr>
            <td class="label">
                <D:Label runat="server" ID="lblIsAlcohol" Text="Is Alcohol" />
            </td>
            <td class="control">
                <dx:ASPxCheckBox ID="cbAllIsAlcohol" runat="server" Text="Check/Uncheck all" /> 
            </td>
            <td>
                <D:Button runat="server" ID="btnIsAlcoholUpdateAll" Text="Update All" PreSubmitWarning="All results will be automatically saved. Are you sure you want to continue?" />
            </td>
            <td/>
        </tr>
    </table>
</D:Panel>

<D:Panel runat="server" ID="pnlResults" GroupingText="Results" LocalizeText="false">
    <div class="batch_edit_header">
        <div class="batch_edit_column" style="width: 80%">
            &nbsp;
        </div>
        <div class="batch_edit_column" style="width: 10%">
            <X:ComboBoxInt runat="server" ID="cbHeaderTaxTariffId" DataSourceID="TaxTariffDataSource" ValueType="System.Int32" ValueField="TaxTariffId" TextField="Name" EmptyItemText="None - Select ..." LocalizeEmptyItemText="false">
                <ClientSideEvents SelectedIndexChanged="function(s, e) { setValueForAllRows('TaxTariffId', s.GetSelectedItem().value) }" />
            </X:ComboBoxInt>
        </div>
        <div class="batch_edit_column" style="width: 10%;">
            <dx:ASPxCheckBox ID="cbHeaderIsAlcohol" runat="server" Text="Check/Uncheck all" style="margin-left:auto;margin-right:auto;"> 
                <ClientSideEvents CheckedChanged="function (s,e) { setValueForAllRows('IsAlcoholic', s.GetChecked()) }" />  
            </dx:ASPxCheckBox>
        </div>
    </div>
</D:Panel>

<dx:ASPxGridView ID="grid" ClientInstanceName="grid" runat="server" KeyFieldName="ProductId" EnableRowsCache="false" Width="100%">
    <SettingsBehavior AutoFilterRowInputDelay="350" />  
    <Settings ShowFilterRow="True" VerticalScrollableHeight="594" />
    <SettingsEditing Mode="Batch" />
    <Columns>
        <dx:GridViewDataTextColumn FieldName="Name" Caption="Product" ReadOnly="True" Width="80%">
            <Settings AutoFilterCondition="Contains" />
            <PropertiesTextEdit>
                <ValidationSettings Display="Dynamic" RequiredField-IsRequired="true" />
            </PropertiesTextEdit>
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataComboBoxColumn FieldName="TaxTariffId" Caption="Tax Tariff" Width="10%">
            <Settings AutoFilterCondition="Contains" FilterMode="DisplayText" SortMode="DisplayText" />
            <PropertiesComboBox DataSourceID="TaxTariffDataSource" ValueType="System.Int32" ValueField="TaxTariffId" TextField="Name" />
        </dx:GridViewDataComboBoxColumn>
        <dx:GridViewDataCheckColumn FieldName="IsAlcoholic" Caption="Is Alcohol" Width="10%" />
    </Columns>
</dx:ASPxGridView>

<dx:ASPxPopupControl ID="pcMenuFilter" ClientInstanceName="pcMenuFilter" PopupElementID="filter-info-popup" Width="350" PopupAction="MouseOver" CloseAction="MouseOut" CloseOnEscape="false" Modal="false" ShowOnPageLoad="false" HeaderText="Filter information" runat="server">
    <ContentCollection>
        <dx:PopupControlContentControl runat="server">
            <p>
                Note: This filter only affects the category filter.
                The grid below won't be affected.
            </p>
        </dx:PopupControlContentControl>
    </ContentCollection>
</dx:ASPxPopupControl>

<llblgenpro:LLBLGenProDataSource 
    runat="server" 
    ID="TaxTariffDataSource" 
    DataContainerType="EntityCollection"
    EntityCollectionTypeName="Obymobi.Data.CollectionClasses.TaxTariffCollection, Obymobi.Data" />