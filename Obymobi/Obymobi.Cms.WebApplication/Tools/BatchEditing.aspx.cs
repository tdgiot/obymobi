﻿using System;
using System.Web.UI;
using Dionysos.Web.UI;

namespace Obymobi.ObymobiCms.Tools
{
    public partial class BatchEditing : PageDefault
    {
        private void LoadUserControls()
        {
            cbBatchEditType.DataBindEnum<BatchEditType>();
            cbBatchEditType.SelectedIndexChanged += CbBatchEditTypeOnSelectedIndexChanged;
        }

        private void SetGui()
        {
            LoadUserControlForSelectedBatchEditType();
        }

        private void CbBatchEditTypeOnSelectedIndexChanged(object sender, EventArgs e)
        {
            LoadUserControlForSelectedBatchEditType();
        }

        private BatchEditType? GetSelectedBatchEditType()
        {
            return cbBatchEditType.Value?.ToEnum<BatchEditType>();
        }

        private void LoadUserControlForSelectedBatchEditType()
        {
            BatchEditType? selectedBatchEditType = GetSelectedBatchEditType();
            if (!selectedBatchEditType.HasValue)
            {
                return;
            }

            switch (selectedBatchEditType.Value)
            {
                case BatchEditType.Alterationoptions:
                    LoadUserControl(BatchEditType.Alterationoptions);
                    break;
                case BatchEditType.Products:
                    LoadUserControl(BatchEditType.Products);
                    break;
                default:
                    throw new ArgumentOutOfRangeException($"Unknown batch edit type {selectedBatchEditType}");
            }
        }

        private void LoadUserControl(BatchEditType batchEditType)
        {
            string panel = string.Empty;

            switch (batchEditType)
            {
                case BatchEditType.Alterationoptions:
                    panel = "~/Tools/SubPanels/BatchAlterationoptionEditPanel.ascx";
                    break;
                case BatchEditType.Products:
                    panel = "~/Tools/SubPanels/BatchProductEditPanel.ascx";
                    break;
            }

            LoadUserControl(panel, batchEditType);
        }

        private void LoadUserControl(string panel, BatchEditType batchEditType)
        {
            callbackPanel.Controls.Clear();

            Control control = LoadControl(panel);
            control.ID = $"uc-{batchEditType}";
            callbackPanel.Controls.Add(control);
        }

        protected override void OnInit(EventArgs e)
        {
            LoadUserControls();
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetGui();
        }

        public enum BatchEditType
        {
            Alterationoptions,
            Products
        }
    }
}