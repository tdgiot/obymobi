﻿<%@ Page Title="Generic SCT" Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Tools.GenericSCT" Codebehind="GenericSCT.aspx.cs" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cplhTitleHolder">Generic SCT</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="cplhToolbarHolder">            
    <X:ToolBarButton runat="server" ID="btGenerate" CommandName="GenerateSCT" Text="Generate SCT" Image-Url="~/Images/Icons/cog.png" Style="margin-left: 5px" />
    <X:ToolBarButton runat="server" ID="btGenerateTest" CommandName="GenerateSCTTest" Text="Generate SCT (test)" Image-Url="~/Images/Icons/cog.png" />
</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="cplhContentHolder">
<div>
    <X:PageControl ID="tabsMain" runat="server" Width="100%">
        <TabPages>
            <X:TabPage Text="Generic" Name="Generic">
                <Controls>                    
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:Label runat="server" id="lblStart">Starts on (UK time)</D:Label>
                            </td>
                            <td class="control">
                                <X:DateTimeEdit ID="tbStart" runat="server" IsRequired="true"></X:DateTimeEdit>
                            </td>
                            <td class="label">
                                <D:Label runat="server" id="lblExpires">Expires on (UK time)</D:Label>
                            </td>
                            <td class="control">
                                <X:DateTimeEdit ID="tbExpires" runat="server" IsRequired="true"></X:DateTimeEdit>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                <D:Label runat="server" ID="lblEmenu">Emenu</D:Label>
                            </td>
                            <td class="control">
                                <X:ComboBoxInt ID="cbEmenuReleaseId" runat="server" TextField="Version" ValueField="ReleaseId" CssClass="input" IncrementalFilteringMode="Contains" IncrementalFilteringDelay="1000" />
                            </td>							
                            <td class="label">
                                <D:Label runat="server" ID="lblSupportTools">Support tools</D:Label>
                            </td>			                
                            <td class="control">
                                <X:ComboBoxInt ID="cbSupportToolsReleaseId" runat="server" TextField="Version" ValueField="ReleaseId" CssClass="input" IncrementalFilteringMode="Contains" IncrementalFilteringDelay="1000" />
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                <D:Label runat="server" ID="lblConsole">Console</D:Label>
                            </td>
                            <td class="control">
                                <X:ComboBoxInt ID="cbConsoleReleaseId" runat="server" TextField="Version" ValueField="ReleaseId" CssClass="input" IncrementalFilteringMode="Contains" IncrementalFilteringDelay="1000" />
                            </td>							
                            <td class="label">
                                <D:Label runat="server" ID="lblAgent">Agent</D:Label>
                            </td>
                            <td class="control">
                                <X:ComboBoxInt ID="cbAgentReleaseId" runat="server" TextField="Version" ValueField="ReleaseId" CssClass="input" IncrementalFilteringMode="Contains" IncrementalFilteringDelay="1000" />
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                <D:Label runat="server" ID="lblOsT2ReleaseId">OS version T2</D:Label>
                            </td>
                            <td class="control">
                                <X:ComboBoxInt ID="cbOsT2ReleaseId" runat="server" TextField="Version" ValueField="ReleaseId" CssClass="input" IncrementalFilteringMode="Contains" IncrementalFilteringDelay="1000" />
                            </td>							
                            <td class="label">
                                <D:Label runat="server" ID="lblMessagingReleaseId">Messaging Version</D:Label>
                            </td>
                            <td class="control">
                                <X:ComboBoxInt ID="cbMessagingReleaseId" runat="server" TextField="Version" ValueField="ReleaseId" CssClass="input" IncrementalFilteringMode="Contains" IncrementalFilteringDelay="1000" />
                            </td>				
                        </tr>
                        <tr>
                            <td class="label">
                                <D:Label runat="server" ID="lblOsTminiReleaseId">OS version T-mini</D:Label>
                            </td>
                            <td class="control">
                                <X:ComboBoxInt ID="cbOsTminiReleaseId" runat="server" TextField="Version" ValueField="ReleaseId" CssClass="input" IncrementalFilteringMode="Contains" IncrementalFilteringDelay="1000" />
                            </td>
                            <td class="label">
                                <D:Label runat="server" ID="lblOsTminiV2ReleaseId">OS version T-mini V2</D:Label>
                            </td>
                            <td class="control">
                                <X:ComboBoxInt ID="cbOsTminiV2ReleaseId" runat="server" TextField="Version" ValueField="ReleaseId" CssClass="input" IncrementalFilteringMode="Contains" IncrementalFilteringDelay="1000" />
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                <D:Label runat="server" ID="lblCompanies">Companies</D:Label>
                            </td>
                            <td class="control">				                
                                <X:ListBox runat="server" ID="lbCompanies" SelectionMode="CheckColumn" ClientInstanceName="lbCompanies" TextField="Name" ValueField="CompanyId" EnableCheckAll="true" />
                            </td>							
                            <td class="label">                                                                
                                <D:Label runat="server" ID="lblOutput">Output</D:Label>
                            </td>
                            <td class="control">                                
                                <D:Label runat="server" ID="lblOutputText" LocalizeText="false">No output</D:Label>
                            </td>
                        </tr>
                    </table>                    
                </Controls>
            </X:TabPage>			
        </TabPages>
    </X:PageControl>    
</div>
</asp:Content>