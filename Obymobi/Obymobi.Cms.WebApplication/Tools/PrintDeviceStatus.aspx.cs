﻿using System.Drawing;
using System.Globalization;
using Dionysos;
using Dionysos.Web.UI;
using System;
using DocumentFormat.OpenXml.Spreadsheet;
using Obymobi.Constants;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SpreadsheetLight;

namespace Obymobi.ObymobiCms.Tools
{
    public partial class PrintDeviceStatus : PageDefault
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SetGui();
        }

        private void SetGui()
        {
            if (!this.IsPostBack)
            {
                this.cblClientTypes.Items.Add("Clients");
                this.cblClientTypes.Items.Add("Terminals");

                this.cblClientTypes.SelectAll();
            }
        }

        public void Export()
        {
            this.pnlDeviceStatus.Visible = false;

            var sl = new SLDocument();

            int row = 1;

            sl.SetColumnWidth("A", 13);
            sl.SetColumnWidth("B", 30);
            sl.SetColumnWidth("C", 25);
            sl.SetColumnWidth("D", 20);
            sl.SetColumnWidth("E", 20);

            sl.SetColumnStyle(1, HorizontalAlignmentValues.Center);
            sl.SetColumnStyle(4, HorizontalAlignmentValues.Center);

            if (this.cblClientTypes.SelectedValues(",").Contains("Terminals"))
            {
                var terminalCollection = GetTerminals(this.cbIncludeOfflineDevices.Checked);

                sl.SetCellValue("A" + row, "Terminals");
                sl.SetStyle("A" + row, HorizontalAlignmentValues.Left, fontSize: 16, fontStyle: FontStyle.Bold);
                row++;

                sl.SetCellValue("A" + row, "Terminal ID");
                sl.SetCellValue("B" + row, "Name");
                sl.SetCellValue("C" + row, "MAC Address");
                sl.SetCellValue("D" + row, "Printer Connected");
                sl.SetCellValue("E" + row, "Last Request");
                sl.SetStyle("A" + row, "E" + row, fontStyle: FontStyle.Bold);
                row++;

                foreach (var terminalEntity in terminalCollection)
                {
                    var deviceIdentifier = (terminalEntity.DeviceId > 0) ? terminalEntity.DeviceEntity.Identifier : "Not Linked";
                    
                    sl.SetCellValue("A" + row, terminalEntity.TerminalId.ToString(CultureInfo.InvariantCulture));
                    sl.SetCellValue("B" + row, terminalEntity.Name);
                    sl.SetCellValue("C" + row, deviceIdentifier);
                    sl.SetCellValue("D" + row, (terminalEntity.PrinterConnected ? "Yes" : "No"));
                    sl.SetCellValue("E" + row, terminalEntity.DeviceId > 0 ? terminalEntity.DeviceEntity.LastRequestUTC.GetValueOrDefault().ToString("dd-MM-yyyy HH:mm:ss") : "Not Linked");

                    row++;
                }

                row++;
            }

            if (this.cblClientTypes.SelectedValues(",").Contains("Clients"))
            {
                var deliverypointgroupCollection = GetClients(this.cbIncludeOfflineDevices.Checked);

                foreach (var deliverypointgroupEntity in deliverypointgroupCollection)
                {
                    sl.SetCellValue("A" + row, deliverypointgroupEntity.Name);
                    sl.SetStyle("A" + row, HorizontalAlignmentValues.Left, fontSize: 16, fontStyle: FontStyle.Bold);
                    row++;

                    sl.SetCellValue("A" + row, "Client ID");
                    sl.SetCellValue("B" + row, "MAC Address");
                    sl.SetCellValue("C" + row, "Deliverypoint");
                    sl.SetCellValue("D" + row, "Keyboard Connected");
                    sl.SetCellValue("E" + row, "Last Request");
                    sl.SetStyle("A" + row, "E" + row, fontStyle: FontStyle.Bold);
                    row++;

                    foreach (var clientEntity in deliverypointgroupEntity.ClientCollection)
                    {
                        string deviceIdentifier = "Not Linked";
                        bool isOnline = false;

                        if (clientEntity.DeviceId > 0)
                        {
                            deviceIdentifier = clientEntity.DeviceEntity.Identifier;
                            isOnline = clientEntity.DeviceEntity.LastRequestUTC > DateTime.UtcNow.AddSeconds(-90);
                        }

                        if (this.cbIncludeOfflineDevices.Checked && !isOnline || isOnline)
                        {
                            sl.SetCellValue("A" + row, clientEntity.ClientId.ToString(CultureInfo.InvariantCulture));
                            sl.SetCellValue("B" + row, deviceIdentifier);
                            sl.SetCellValue("C" + row, string.Format("{0} ({1})", clientEntity.DeliverypointEntity.Name, clientEntity.DeliverypointEntity.Number));
                            sl.SetCellValue("D" + row, (clientEntity.BluetoothKeyboardConnected ? "Yes" : "No"));
                            sl.SetCellValue("E" + row, clientEntity.DeviceId > 0 ? clientEntity.DeviceEntity.LastRequestUTC.GetValueOrDefault().UtcToLocalTime().ToString("dd-MM-yyyy HH:mm:ss") : "Not Linked");

                            row++;
                        }
                    }

                    row++;
                }
            }


            // Scale 1x1 Page
            SLPageSettings pageSettings = sl.GetPageSettings();
            pageSettings.ScalePage(1, 1);
            sl.SetPageSettings(pageSettings);

            // Set all print sizes to A4
            sl.SetPageSizeOfWorksheets(SLPaperSizeValues.A4Paper);

            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("Content-Disposition", "attachment; filename=DeviceStatus-{0}-{1}.xlsx".FormatSafe(CmsSessionHelper.CurrentCompanyId, DateTime.Now.DateTimeToSimpleDateTimeStamp()));
            sl.SaveAs(Response.OutputStream);
            Response.End();
        }

        public void View()
        {
            this.pnlDeviceStatus.Visible = true;

            if (this.cblClientTypes.SelectedValues(",").Contains("Clients"))
            {
                var deliverypointgroupCollection = GetClients(this.cbIncludeOfflineDevices.Checked);

                if (deliverypointgroupCollection.Count > 0)
                {
                    this.plhViewResult.AddHtml("<table class=\"dataformV2\">");

                    foreach (var deliverypointgroupEntity in deliverypointgroupCollection)
                    {
                        this.plhViewResult.AddHtml("<tr><td colspan='5'><h2>{0}</h2></td></tr>", deliverypointgroupEntity.Name);
                        this.plhViewResult.AddHtml("<tr><td width='100'><strong>Client ID</strong></td><td width='150'><strong>MAC Address</strong></td><td width='150'><strong>Deliverypoint</strong></td><td width='150'><strong>Keyboard Connected</strong></td><td><strong>Last Request</strong></td></tr>");

                        foreach (var clientEntity in deliverypointgroupEntity.ClientCollection)
                        {
                            string deviceIdentifier = "Not Linked";
                            bool isOnline = false;

                            if (clientEntity.DeviceId > 0)
                            {
                                deviceIdentifier = clientEntity.DeviceEntity.Identifier;
                                isOnline = clientEntity.DeviceEntity.LastRequestUTC > DateTime.UtcNow.AddSeconds(-90);
                            }

                            if (this.cbIncludeOfflineDevices.Checked && !isOnline || isOnline)
                            {
                                this.plhViewResult.AddHtml("<tr><td>{0}</td><td>{1}</td><td>{2} ({3})</td><td>{4}</td><td>{5}</td></tr>",
                                    clientEntity.ClientId,
                                    deviceIdentifier,
                                    clientEntity.DeliverypointId > 0 ? clientEntity.DeliverypointEntity.Name : "Not set",
                                    clientEntity.DeliverypointId > 0 ? clientEntity.DeliverypointEntity.Number : "Not set",
                                    clientEntity.BluetoothKeyboardConnected,
                                    clientEntity.DeviceId > 0 ? clientEntity.DeviceEntity.LastRequestUTC.GetValueOrDefault().UtcToLocalTime().ToString("dd-MM-yyyy HH:mm:ss") : "Not Linked");
                            }
                        }

                        this.plhViewResult.AddHtml("<tr><td colspan='5'>&nbsp;</td></tr>");
                    }

                    this.plhViewResult.AddHtml("</table>");
                }
            }

            if (this.cblClientTypes.SelectedValues(",").Contains("Terminals"))
            {
                var terminalCollection = GetTerminals(this.cbIncludeOfflineDevices.Checked);

                if (terminalCollection.Count > 0)
                {
                    this.plhViewResult.AddHtml("<table class=\"dataformV2\">");
                    this.plhViewResult.AddHtml("<tr><td colspan='5'><h2>Terminals</h2></td></tr>");
                    this.plhViewResult.AddHtml("<tr><td width='100'><strong>Terminal ID</strong></td><td width='150'><strong>Name</strong></td><td width='150'><strong>MAC Address</strong></td><td width='150'><strong>Printer Connected</strong></td><td><strong>Last Request</strong></td></tr>");
                    foreach (var terminalEntity in terminalCollection)
                    {
                        var deviceIdentifier = (terminalEntity.DeviceId > 0) ? terminalEntity.DeviceEntity.Identifier : "Not Linked";
                        this.plhViewResult.AddHtml("<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td></tr>",
                                                   terminalEntity.TerminalId,
                                                   terminalEntity.Name,
                                                   deviceIdentifier,
                                                   terminalEntity.PrinterConnected,
                                                   terminalEntity.DeviceId > 0 ? terminalEntity.DeviceEntity.LastRequestUTC.GetValueOrDefault().UtcToLocalTime().ToString("dd-MM-yyyy HH:mm:ss") : "Not Linked");
                    }

                    this.plhViewResult.AddHtml("</table>");
                }
            }
        }

        private DeliverypointgroupCollection GetClients(bool includeOfflineClients)
        {
            var filter = new PredicateExpression();
            filter.Add(DeliverypointgroupFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            if (!includeOfflineClients)
                filter.Add(DeviceFields.LastRequestUTC > DateTime.UtcNow.AddSeconds(-90));

            var sort = new SortExpression(DeliverypointFields.Number | SortOperator.Ascending);

            var relation = new RelationCollection();
            relation.Add(DeliverypointgroupEntityBase.Relations.ClientEntityUsingDeliverypointGroupId);
            relation.Add(DeliverypointgroupEntityBase.Relations.DeliverypointEntityUsingDeliverypointgroupId);
            relation.Add(ClientEntity.Relations.DeviceEntityUsingDeviceId);

            var prefetch = new PrefetchPath(EntityType.DeliverypointgroupEntity);
            prefetch.Add(DeliverypointgroupEntityBase.PrefetchPathClientCollection).SubPath.Add(ClientEntityBase.PrefetchPathDeviceEntity);

            var deliverypointgroupCollection = new DeliverypointgroupCollection();
            deliverypointgroupCollection.GetMulti(filter, 0, sort, relation, prefetch);

            return deliverypointgroupCollection;
        }

        private TerminalCollection GetTerminals(bool includeOfflineClients)
        {
            var filter = new PredicateExpression();
            filter.Add(TerminalFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            if (!includeOfflineClients)
                filter.Add(DeviceFields.LastRequestUTC > DateTime.UtcNow.AddSeconds(-90));

            var sort = new SortExpression(TerminalFields.Name | SortOperator.Ascending);

            var prefetch = new PrefetchPath(EntityType.TerminalEntity);
            prefetch.Add(TerminalEntityBase.PrefetchPathDeviceEntity);

            var relations = new RelationCollection();
            relations.Add(TerminalEntity.Relations.DeviceEntityUsingDeviceId);

            var terminalCollection = new TerminalCollection();
            terminalCollection.GetMulti(filter, 0, sort, relations, prefetch);

            return terminalCollection;
        }
    }
}