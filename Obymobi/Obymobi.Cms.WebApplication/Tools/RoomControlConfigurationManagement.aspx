﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Tools.RoomControlConfigurationManagement" Title="Room control configuration management" Codebehind="RoomControlConfigurationManagement.aspx.cs" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cplhTitleHolder">Room control configuration management</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="cplhToolbarHolder">
    <D:Button runat="server" ID="btnRefresh" Text="Verversen" CausesValidation="true" style="margin-left: 3px;" />
</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="cplhContentHolder">
<div>
	<X:PageControl ID="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
                    <X:GridView ID="dpGrid" ClientInstanceName="dpGrid" runat="server" Width="100%" KeyFieldName="DeliverypointId">
                        <SettingsPager PageSize="20"></SettingsPager>
                        <SettingsBehavior AllowGroup="false" AllowDragDrop="false" AutoFilterRowInputDelay="350" />
                        <Columns>
                            <dxwgv:GridViewDataHyperLinkColumn FieldName="DeliverypointId" VisibleIndex="1" SortIndex="0" SortOrder="Ascending" Width="20%">
				                <Settings AutoFilterCondition="Contains" SortMode="DisplayText" FilterMode="DisplayText" />
                                <PropertiesHyperLinkEdit NavigateUrlFormatString="~/Company/Deliverypoint.aspx?id={0}" Target="_blank" TextField="DeliverypointNumber"  />
                            </dxwgv:GridViewDataHyperLinkColumn>                            
                            <dxwgv:GridViewDataColumn FieldName="DeliverypointName" VisibleIndex="2" Width="20%">
				                <Settings AutoFilterCondition="Contains" />
                            </dxwgv:GridViewDataColumn>
                            <dxwgv:GridViewDataHyperLinkColumn FieldName="DeliverypointgroupId" VisibleIndex="3" Width="20%">
				                <Settings AutoFilterCondition="Contains" SortMode="DisplayText" FilterMode="DisplayText" />
                                <PropertiesHyperLinkEdit NavigateUrlFormatString="~/Company/Deliverypointgroup.aspx?id={0}" Target="_blank" TextField="DeliverypointgroupName"  />
                            </dxwgv:GridViewDataHyperLinkColumn>                                                        
                            <dxwgv:GridViewDataColumn FieldName="RoomControllerIp" VisibleIndex="4" Width="20%">
				                <Settings AutoFilterCondition="Contains" />
                            </dxwgv:GridViewDataColumn>
                            <dxwgv:GridViewDataHyperLinkColumn FieldName="RoomControlConfigurationId" VisibleIndex="5" Width="20%">
				                <Settings AutoFilterCondition="Contains" SortMode="DisplayText" FilterMode="DisplayText" />
                                <PropertiesHyperLinkEdit NavigateUrlFormatString="~/Company/RoomControlConfiguration.aspx?id={0}" Target="_blank" TextField="RoomControlConfiguration"  />
                            </dxwgv:GridViewDataHyperLinkColumn>                                       
                        </Columns>
                        <Settings ShowFilterRow="True" />
                    </X:GridView>          
                </Controls>
            </X:TabPage>            
            <X:TabPage Text="Acties" Name="Actions">
				<Controls>
				    <table class="dataformV2">
						<tr>
						    <td class="label">
                                <D:LabelTextOnly runat="server" id="lblRoomControlConfiguration">Room control configuration</D:LabelTextOnly>    
							</td>
							<td class="control">
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlRoomControlConfigurationId" IncrementalFilteringMode="StartsWith" EntityName="RoomControlConfiguration" TextField="Name" ValueField="RoomControlConfigurationId" PreventEntityCollectionInitialization="true" />
							</td>
							<td class="label">
                                <D:LabelTextOnly runat="server" id="lblSetForDeliverypoints">Set for deliverypoints</D:LabelTextOnly>
		                    </td>
		                    <td class="control">
                                <D:TextBoxInt ID="tbFrom" runat="server" ThousandsSeperators="false" style="width: 30px;" notdirty="true"></D:TextBoxInt>
                                -
                                <D:TextBoxInt ID="tbTo" runat="server" ThousandsSeperators="false" style="width: 30px;" notdirty="true"></D:TextBoxInt>
                                <D:Button ID="btnSetConfiguration" runat="server" Text="Set configuratie" />
		                    </td>							
					    </tr>
                    </table>
                </Controls>
            </X:TabPage>
             <X:TabPage Text="Disconnected clients" Name="DisconnectedClients">
				<Controls>
                    <X:GridView ID="dpDisconnectedClients" ClientInstanceName="dpDisconnectedClients" runat="server" Width="100%" KeyFieldName="ClientId">
                        <SettingsPager PageSize="20"></SettingsPager>
                        <SettingsBehavior AllowGroup="false" AllowDragDrop="false" AutoFilterRowInputDelay="350" />
                        <Columns>
                            <dxwgv:GridViewDataHyperLinkColumn FieldName="ClientId" VisibleIndex="1" SortIndex="0" SortOrder="Ascending" Width="20%">
				                <Settings AutoFilterCondition="Contains" SortMode="DisplayText" FilterMode="DisplayText" />
                                <PropertiesHyperLinkEdit NavigateUrlFormatString="~/Company/Client.aspx?id={0}" Target="_blank" TextField="ClientId"  />
                            </dxwgv:GridViewDataHyperLinkColumn>                            
                            <dxwgv:GridViewDataColumn FieldName="LastDeliverypointNumber" VisibleIndex="2" Width="20%">
				                <Settings AutoFilterCondition="Contains" />
                            </dxwgv:GridViewDataColumn>
                        </Columns>
                        <Settings ShowFilterRow="True" />
                    </X:GridView>          
                </Controls>
            </X:TabPage>
        </TabPages>
    </X:PageControl>
</div>
</asp:Content>