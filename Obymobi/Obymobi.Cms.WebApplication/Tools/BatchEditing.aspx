﻿<%@ Page Title="Batch editing" Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Tools.BatchEditing" Codebehind="BatchEditing.aspx.cs" LocalizeTitle="false" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitleHolder" Runat="Server">
	Batch Editing
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhToolbarHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhContentHolder" Runat="Server">
    <script type="text/javascript">
        var rowsPerPage = 20;

        function setValueForAllRows(column, value) {
            var pageIndex = grid.GetPageIndex();            
            var startRowIndex = pageIndex * rowsPerPage;
            var endRowIndex = startRowIndex + rowsPerPage;            

            for (let i = startRowIndex; i < endRowIndex; i++) {
                grid.batchEditApi.SetCellValue(i, column, value);
            }
        }

        function switchBatchEditPanel(batchEditType) {
            callbackPanel.PerformCallback(batchEditType);
        }
    </script>
    <X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Generic" Name="Generic">
				<Controls>
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:Label runat="server" ID="lblBatchEditType" Text="What would you like to edit?" LocalizeText="False"></D:Label>
                            </td>
                            <td class="control">
                                <X:ComboBoxInt runat="server" ID="cbBatchEditType" UseDataBinding="True" AutoPostBack="True" />
                            </td>
                            <td class="label">

                            </td>
                            <td class="control">

                            </td>
                        </tr>
                    </table>

                    <dx:ASPxCallbackPanel ID="callbackPanel" runat="server" ClientInstanceName="callbackPanel">
                        <PanelCollection>
                            <dx:PanelContent runat="server">
                            </dx:PanelContent>
                        </PanelCollection>
                    </dx:ASPxCallbackPanel>

                </Controls>
			</X:TabPage>			
		</TabPages>
	</X:PageControl>
</asp:Content>
