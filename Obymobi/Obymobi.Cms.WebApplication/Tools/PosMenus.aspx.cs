﻿using System;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Enums;

namespace Obymobi.ObymobiCms.Tools
{
    public partial class PosMenus : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        #region Fields

        #endregion

        #region Methods

        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "Menu";
            this.EntityPageUrl = "~/Tools/PosMenu.aspx";
            base.OnInit(e);
        }

        #endregion

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            LLBLGenProDataSource datasource = this.DataSource as LLBLGenProDataSource;
            if (datasource != null)
            {
                PredicateExpression filter = new PredicateExpression(MenuFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
                datasource.FilterToUse = filter;
            }

            this.MainGridView.ShowDeleteHyperlinkColumn = false;
            this.MainGridView.ShowDeleteColumn = false;
            ((MasterPages.MasterPageEntityCollection)this.Master).ToolBar.AddButton.Visible = false;
        }

        #endregion

        #region Properties
        
        public MenuCollection DataSourceAsMenuCollection
        {
            get
            {
                return this.DataSource as MenuCollection;
            }
        }

        #endregion

    }
}
