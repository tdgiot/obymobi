﻿using System;
using Dionysos;
using Obymobi.Logic.HelperClasses;
using SpreadsheetLight;
using Obymobi.ObymobiCms.Reports;

namespace Obymobi.ObymobiCms.Tools
{
    public partial class GuestInformation : BaseReportingPage
    {
        protected override void OnInit(EventArgs e)
        {
            this.InitGui();
            base.OnInit(e);
        }

        private void Page_Load(object sender, EventArgs e)
        {
               
        }

        private void InitGui()
        {
            
        }

        private GuestInformationReportFilter CreateFilter()
        {
            GuestInformationReportFilter filter = new GuestInformationReportFilter();
            filter.CompanyId = CmsSessionHelper.CurrentCompanyId;
            filter.GroupName = this.tbGroupName.Text;

            TimeZoneInfo companyTimeZoneInfo = CmsSessionHelper.CompanyTimeZone;

            if (this.deCheckin.Value.HasValue)
            {
                filter.CheckInUtc = this.deCheckin.Value.Value.LocalTimeToUtc(companyTimeZoneInfo);
            }

            if (this.deCheckout.Value.HasValue)
            {
                filter.CheckOutUtc = this.deCheckout.Value.Value.LocalTimeToUtc(companyTimeZoneInfo);
            }

            return filter;
        }

        public void GenerateReport()
        {
            GuestInformationReportFilter filter = this.CreateFilter();

            GuestInformationReport report = new GuestInformationReport(filter);
            this.GenerateReport(report);
        }

        public void GenerateReport(GuestInformationReport report)
        {
            SLDocument spreadsheet;
            report.RunReport(out spreadsheet);

            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("Content-Disposition", "attachment; filename=GuestInformation-{0}.xlsx".FormatSafe(DateTime.Now.DateTimeToSimpleDateTimeStamp()));
            spreadsheet.SaveAs(Response.OutputStream);
            Response.End();
        }        
    }
}