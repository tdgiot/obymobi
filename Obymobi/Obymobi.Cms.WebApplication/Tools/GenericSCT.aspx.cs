﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Crave.Api.Logic.Requests.Publishing;
using Crave.Api.Logic.UseCases;
using Dionysos;
using Dionysos.Web.UI;
using Dionysos.Web.UI.DevExControls;
using Obymobi.Constants;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using Obymobi.Logic.ScheduledCommands;
using Obymobi.Logic.ScheduledCommands.Client;
using Obymobi.Logic.ScheduledCommands.Terminal;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Tools
{
    public partial class GenericSCT : PageDefault
    {
        private const int GENERIC_SCT_COMPANY_ID = 310; // Otto's Pub
        private const string GENERIC_SCT_TIME_ZONE_OLSON_ID = "Europe/London";

        private void LoadUserControls()
        {
            DataBindCompanies();
            DataBindReleases(cbEmenuReleaseId, ApplicationCode.Emenu);
            DataBindReleases(cbConsoleReleaseId, ApplicationCode.Console);
            DataBindReleases(cbSupportToolsReleaseId, ApplicationCode.SupportTools);
            DataBindReleases(cbAgentReleaseId, ApplicationCode.Agent);
            DataBindReleases(cbOsT2ReleaseId, ApplicationCode.CraveOsIntelAnzhen4);
            DataBindReleases(cbOsTminiReleaseId, ApplicationCode.CraveOsTmini);
            DataBindReleases(cbOsTminiV2ReleaseId, ApplicationCode.CraveOsTminiV2);
            DataBindReleases(cbMessagingReleaseId, ApplicationCode.MessagingService);
        }

        private void SetGui()
        {
            btGenerate.Enabled = !IsScheduledCommandTaskActive(GENERIC_SCT_COMPANY_ID);

            if (!IsScheduledCommandTaskActive(GENERIC_SCT_COMPANY_ID))
            {
                return;
            }

            MultiValidatorDefault.AddInformation("No SCT can be created since there is already one active on Otto's Pub.");
            Validate();
        }

        public void GenerateSCT() => Generate();

        public void GenerateSCTTest() => Generate(true);

        private void Generate(bool test = false)
        {
            if (!IsValid(out ReleaseCollection releaseCollection, out CompanyCollection companyCollection, out ClientCollection clientCollection, out TerminalCollection terminalCollection))
            {
	            Validate();
                return;
            }

            Transaction transaction = new Transaction(IsolationLevel.ReadUncommitted, "Create-GenericSCT");

            try
            {
                CompanyReleaseCollection companyReleaseCollection = CreateCompanyReleases(releaseCollection, companyCollection);

                companyReleaseCollection.AddToTransaction(transaction);
                companyReleaseCollection.SaveMulti();

                ScheduledCommandTaskEntity sct = CreateScheduledCommandTask(releaseCollection, clientCollection, terminalCollection, GENERIC_SCT_COMPANY_ID, GENERIC_SCT_TIME_ZONE_OLSON_ID);

                sct.AddToTransaction(transaction);
                sct.Save(true);

                if (test)
                {
                    StringBuilder builder = new StringBuilder();
                    builder.AppendFormatLine("Companies: {0}<br />", companyCollection.Count);
                    builder.AppendFormatLine("Releases: {0}<br />", releaseCollection.Count);
                    builder.AppendFormatLine("Clients: {0}<br />", clientCollection.Count);
                    builder.AppendFormatLine("Terminals: {0}<br />", terminalCollection.Count);
                    builder.AppendFormatLine("SCT commands: {0}<br />", sct.ScheduledCommandCollection.Count);

                    lblOutputText.Text = builder.ToString();

                    transaction.Rollback();
                }
                else
                {
                    transaction.Commit();

                    PublishCompanyReleasesForRestApi(companyCollection);

                    string url = ResolveUrl("~/Company/ScheduledCommandTask.aspx?id={0}".FormatSafe(sct.ScheduledCommandTaskId));
                    AddInformatorInfo("<a href=\"{0}\">SCT</a> successfully created.", url);
                    Validate();
                }
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
            finally
            {
                transaction.Dispose();
            }
        }

        private static void PublishCompanyReleasesForRestApi(CompanyCollection companyCollection)
        {
            string restApiBaseUrl = WebEnvironmentHelper.GetRestApiBaseUrl();
            UserEntity currentUser = CmsSessionHelper.CurrentUser;

            PublishUseCase publishUseCase = new PublishUseCase();

            foreach (CompanyEntity companyEntity in companyCollection)
            {
                PublishCompanyReleases publishRequest = new PublishCompanyReleases
                {
                    CompanyId = companyEntity.CompanyId,
                    User = currentUser,
                    BaseUrl = restApiBaseUrl
                };

                publishUseCase.Execute(publishRequest);
            }
        }

        private static CompanyReleaseCollection CreateCompanyReleases(ReleaseCollection releaseCollection, CompanyCollection companyCollection)
        {
            CompanyReleaseCollection companyReleaseCollection = new CompanyReleaseCollection();

            foreach (ReleaseEntity release in releaseCollection)
            {
                CompanyReleaseCollection existingCompanyReleaseCollection = GetCompanyReleaseCollection(companyCollection, release.ApplicationEntity.Code);

                foreach (CompanyReleaseEntity existingCompanyRelease in existingCompanyReleaseCollection)
                {
                    existingCompanyRelease.ReleaseId = release.ReleaseId;
                    existingCompanyRelease.AutoUpdate = false;

                    companyReleaseCollection.Add(existingCompanyRelease);
                }
            }

            return companyReleaseCollection;
        }

        private static CompanyReleaseCollection GetCompanyReleaseCollection(CompanyCollection companyCollection, string applicationCode)
        {
            List<int> companyIds = companyCollection.Select(x => x.CompanyId).ToList();

            PredicateExpression filter = new PredicateExpression
            {
                CompanyReleaseFields.CompanyId == companyIds,
                ApplicationFields.Code == applicationCode
            };

            RelationCollection relationCollection = new RelationCollection
            {
                CompanyReleaseEntityBase.Relations.ReleaseEntityUsingReleaseId,
                ReleaseEntityBase.Relations.ApplicationEntityUsingApplicationId
            };

            CompanyReleaseCollection companyReleaseCollection = new CompanyReleaseCollection();
            companyReleaseCollection.GetMulti(filter, relationCollection);

            return companyReleaseCollection;
        }

        private ScheduledCommandTaskEntity CreateScheduledCommandTask(ReleaseCollection releaseCollection, ClientCollection clientCollection, TerminalCollection terminalCollection, int companyId,
            string timeZoneOlsonId)
        {
            GetCommandsForReleaseCollection(releaseCollection, out List<ClientCommand> clientCommands, out List<TerminalCommand> terminalCommands);

            TimeZoneInfo timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(TimeZone.Mappings[timeZoneOlsonId].WindowsTimeZoneId);

            ScheduledCommandTaskEntity scheduledCommandTaskEntity = new ScheduledCommandTaskEntity();
            scheduledCommandTaskEntity.CompanyId = companyId;
            scheduledCommandTaskEntity.StartUTC = GetUtcTime(tbStart, timeZoneInfo, scheduledCommandTaskEntity.StartUTC);
            scheduledCommandTaskEntity.ExpiresUTC = GetUtcTime(tbExpires, timeZoneInfo, scheduledCommandTaskEntity.ExpiresUTC);
            scheduledCommandTaskEntity.Active = true;

            AddScheduledCommandsForClients(scheduledCommandTaskEntity, clientCollection, clientCommands);
            AddScheduledCommandsForTerminals(scheduledCommandTaskEntity, terminalCollection, terminalCommands);

            return scheduledCommandTaskEntity;
        }

        private static DateTime GetUtcTime(DateTimeEdit dateTimeEdit, TimeZoneInfo timeZoneInfo, DateTime defaultValue) => !dateTimeEdit.Value.HasValue ? defaultValue : dateTimeEdit.Value.LocalTimeToUtc(timeZoneInfo) ?? defaultValue;

        private static void AddScheduledCommandsForClients(ScheduledCommandTaskEntity scheduledCommandTask, ClientCollection clientCollection, List<ClientCommand> commands)
        {
            foreach (ClientCommand command in commands)
            {
                int sort = (int) command;
                if (command == ClientCommand.DownloadEmenuUpdate || command == ClientCommand.InstallEmenuUpdate)
                {
                    sort += 50;
                }

                ClientCommandSchedulerBase scheduler = CommandSchedulers.GetClientCommandScheduler(command);

                foreach (ClientEntity clientEntity in clientCollection)
                {
                    if (!scheduler.CanCommandBeAdded(clientEntity))
                    {
                        continue;
                    }

                    ScheduledCommandEntity scheduledCommandEntity = new ScheduledCommandEntity();
                    scheduledCommandEntity.Validator = null;
                    scheduledCommandEntity.ParentCompanyId = scheduledCommandTask.CompanyId;
                    scheduledCommandEntity.ClientCommand = command;
                    scheduledCommandEntity.ClientId = clientEntity.ClientId;
                    scheduledCommandEntity.Status = ScheduledCommandStatus.Pending;
                    scheduledCommandEntity.Sort = sort;

                    scheduledCommandTask.ScheduledCommandCollection.Add(scheduledCommandEntity);
                }
            }
        }

        private static void AddScheduledCommandsForTerminals(ScheduledCommandTaskEntity scheduledCommandTask, TerminalCollection terminalCollection, List<TerminalCommand> commands)
        {
            foreach (TerminalCommand command in commands)
            {
                int sort = (int) command + 100;
                if (command == TerminalCommand.DownloadConsoleUpdate || command == TerminalCommand.InstallConsoleUpdate)
                {
                    sort += 50;
                }

                TerminalCommandSchedulerBase scheduler = CommandSchedulers.GetTerminalCommandScheduler(command);

                foreach (TerminalEntity terminalEntity in terminalCollection)
                {
                    if (!scheduler.CanCommandBeAdded(terminalEntity))
                    {
                        continue;
                    }

                    ScheduledCommandEntity scheduledCommandEntity = new ScheduledCommandEntity();
                    scheduledCommandEntity.Validator = null;
                    scheduledCommandEntity.ParentCompanyId = scheduledCommandTask.CompanyId;
                    scheduledCommandEntity.TerminalCommand = command;
                    scheduledCommandEntity.TerminalId = terminalEntity.TerminalId;
                    scheduledCommandEntity.Status = ScheduledCommandStatus.Pending;
                    scheduledCommandEntity.Sort = sort;

                    scheduledCommandTask.ScheduledCommandCollection.Add(scheduledCommandEntity);
                }
            }
        }

        private bool IsValid(out ReleaseCollection releaseCollection, out CompanyCollection companyCollection, out ClientCollection clientCollection, out TerminalCollection terminalCollection)
        {
            List<int> releaseIds = GetSelectedReleaseIds();
            List<int> companyIds = GetCompaniesToUpgrade();

            releaseCollection = GetReleasesToApply(releaseIds);
            companyCollection = GetCompaniesToUpgrade(companyIds);
            clientCollection = GetClientsToUpgrade(companyIds);
            terminalCollection = GetTerminalsToUpgrade(companyIds);

            if (tbStart.Value == null || tbExpires.Value == null)
            {
                MultiValidatorDefault.AddInformation("Please enter a start on and expire on value");
                return false;
            }

            if (tbExpires.Value <= tbStart.Value)
            {
                MultiValidatorDefault.AddInformation("Expires on value has to be further in time than value of field starts on");
                return false;
            }

            if (!releaseCollection.Any())
            {
                MultiValidatorDefault.AddInformation("Please select a release to download and install");
                return false;
            }

            if (!companyCollection.Any())
            {
                MultiValidatorDefault.AddInformation("Please select a company to upgrade");
                return false;
            }

            if (!clientCollection.Any() && !terminalCollection.Any())
            {
                MultiValidatorDefault.AddInformation("Please select a company with terminals or clients online");
                return false;
            }

            return true;
        }

        private static void GetCommandsForReleaseCollection(ReleaseCollection releaseCollection, out List<ClientCommand> clientCommands, out List<TerminalCommand> terminalCommands)
        {
            clientCommands = new List<ClientCommand>();
            terminalCommands = new List<TerminalCommand>();

            foreach (ReleaseEntity release in releaseCollection)
            {
                clientCommands.AddRange(GetClientCommands(release.ApplicationEntity.Code));
                terminalCommands.AddRange(GetTerminalCommands(release.ApplicationEntity.Code));
            }
        }

        private static IEnumerable<ClientCommand> GetClientCommands(string applicationCode)
        {
            List<ClientCommand> commands = new List<ClientCommand>();

            switch (applicationCode)
            {
                case ApplicationCode.Emenu:
                    commands.Add(ClientCommand.DownloadEmenuUpdate);
                    commands.Add(ClientCommand.InstallEmenuUpdate);
                    break;
                case ApplicationCode.SupportTools:
                    commands.Add(ClientCommand.DownloadSupportToolsUpdate);
                    commands.Add(ClientCommand.InstallSupportToolsUpdate);
                    break;
                case ApplicationCode.Agent:
                    commands.Add(ClientCommand.DownloadAgentUpdate);
                    commands.Add(ClientCommand.InstallAgentUpdate);
                    break;
                case ApplicationCode.CraveOsIntelAnzhen4:
                    commands.Add(ClientCommand.DownloadOSUpdate);
                    commands.Add(ClientCommand.InstallOSUpdate);
                    break;
                case ApplicationCode.MessagingService:
                    commands.Add(ClientCommand.DownloadInstallMessagingServiceUpdate);
                    break;
            }

            return commands;
        }

        private static IEnumerable<TerminalCommand> GetTerminalCommands(string applicationCode)
        {
            List<TerminalCommand> commands = new List<TerminalCommand>();

            switch (applicationCode)
            {
                case ApplicationCode.Console:
                    commands.Add(TerminalCommand.DownloadConsoleUpdate);
                    commands.Add(TerminalCommand.InstallConsoleUpdate);
                    break;
                case ApplicationCode.SupportTools:
                    commands.Add(TerminalCommand.DownloadSupportToolsUpdate);
                    commands.Add(TerminalCommand.InstallSupportToolsUpdate);
                    break;
                case ApplicationCode.Agent:
                    commands.Add(TerminalCommand.DownloadAgentUpdate);
                    commands.Add(TerminalCommand.InstallAgentUpdate);
                    break;
                case ApplicationCode.CraveOsIntelAnzhen4:
                    commands.Add(TerminalCommand.DownloadOSUpdate);
                    commands.Add(TerminalCommand.InstallOSUpdate);
                    break;
                case ApplicationCode.MessagingService:
                    commands.Add(TerminalCommand.DownloadInstallMessagingServiceUpdate);
                    break;
            }

            return commands;
        }

        private static bool IsScheduledCommandTaskActive(int companyId)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ScheduledCommandTaskFields.CompanyId == companyId);
            filter.Add(ScheduledCommandTaskFields.Active == true);

            ScheduledCommandTaskCollection scheduledCommandTaskCollection = new ScheduledCommandTaskCollection();
            return scheduledCommandTaskCollection.GetDbCount(filter) > 0;
        }

        private static ClientCollection GetClientsToUpgrade(IList companyIdsToUpgrade)
        {
            PredicateExpression filter = new PredicateExpression
            {
                ClientFields.CompanyId == companyIdsToUpgrade,
                DeviceFields.LastRequestUTC > DateTime.UtcNow.AddMinutes(-3),
            };

            RelationCollection relations = new RelationCollection {DeviceEntityBase.Relations.ClientEntityUsingDeviceId};
            IncludeFieldsList includes = new IncludeFieldsList {ClientFields.CompanyId, ClientFields.ClientId};
            PrefetchPath prefetch = new PrefetchPath(EntityType.ClientEntity);
            prefetch.Add(ClientEntityBase.PrefetchPathDeviceEntity, new IncludeFieldsList
            {
                DeviceFields.DeviceModel,
                DeviceFields.ApplicationVersion,
                DeviceFields.SupportToolsVersion,
                DeviceFields.AgentVersion,
                DeviceFields.MessagingServiceVersion,
                DeviceFields.OsVersion
            });

            ClientCollection clientCollection = new ClientCollection();
            clientCollection.GetMulti(filter, 0, null, relations, prefetch, includes, 0, 0);

            return clientCollection;
        }

        private static TerminalCollection GetTerminalsToUpgrade(IList companyIdsToUpgrade)
        {
            PredicateExpression filter = new PredicateExpression
            {
                TerminalFields.CompanyId == companyIdsToUpgrade,
                DeviceFields.LastRequestUTC > DateTime.UtcNow.AddMinutes(-3),
                TerminalFields.Type == (int) TerminalType.Console
            };

            RelationCollection relations = new RelationCollection {DeviceEntityBase.Relations.TerminalEntityUsingDeviceId};
            IncludeFieldsList includes = new IncludeFieldsList {TerminalFields.CompanyId, TerminalFields.TerminalId};
            PrefetchPath prefetch = new PrefetchPath(EntityType.TerminalEntity);
            prefetch.Add(TerminalEntityBase.PrefetchPathDeviceEntity, new IncludeFieldsList
            {
                DeviceFields.DeviceModel,
                DeviceFields.ApplicationVersion,
                DeviceFields.SupportToolsVersion,
                DeviceFields.AgentVersion,
                DeviceFields.MessagingServiceVersion,
                DeviceFields.OsVersion
            });

            TerminalCollection terminalCollection = new TerminalCollection();
            terminalCollection.GetMulti(filter, 0, null, relations, prefetch, includes, 0, 0);

            return terminalCollection;
        }

        private static ReleaseCollection GetReleasesToApply(IList releaseIds)
        {
            PredicateExpression filter = new PredicateExpression {ReleaseFields.ReleaseId == releaseIds};
            IncludeFieldsList includes = new IncludeFieldsList {ReleaseFields.ReleaseId, ReleaseFields.Version};

            ReleaseCollection releaseCollection = new ReleaseCollection();
            releaseCollection.GetMulti(filter, 0, null, null, null, includes, 0, 0);

            return releaseCollection;
        }

        private static CompanyCollection GetCompaniesToUpgrade(IList companyIds)
        {
            PredicateExpression filter = new PredicateExpression {CompanyFields.CompanyId == companyIds};
            IncludeFieldsList includes = new IncludeFieldsList {CompanyFields.CompanyId, CompanyFields.Name};
            PrefetchPath prefetch = new PrefetchPath(EntityType.CompanyEntity);
            IPrefetchPathElement prefetchCompanyRelease = prefetch.Add(CompanyEntityBase.PrefetchPathCompanyReleaseCollection,
                new IncludeFieldsList {CompanyReleaseFields.CompanyId, CompanyReleaseFields.ReleaseId});
            IPrefetchPathElement prefetchRelease =
                prefetchCompanyRelease.SubPath.Add(CompanyReleaseEntityBase.PrefetchPathReleaseEntity, new IncludeFieldsList {ReleaseFields.ApplicationId, ReleaseFields.Version});
            IPrefetchPathElement prefetchApplication = prefetchRelease.SubPath.Add(ReleaseEntityBase.PrefetchPathApplicationEntity, new IncludeFieldsList {ApplicationFields.Code});

            CompanyCollection companyCollection = new CompanyCollection();
            companyCollection.GetMulti(filter, 0, null, null, null, includes, 0, 0);

            return companyCollection;
        }

        private void DataBindCompanies()
        {
            IncludeFieldsList includes = new IncludeFieldsList {CompanyFields.CompanyId, CompanyFields.Name};
            SortExpression sort = new SortExpression(CompanyFields.Name | SortOperator.Ascending);

            CompanyCollection companyCollection = new CompanyCollection();
            companyCollection.GetMulti(null, 0, sort, null, null, includes, 0, 0);

            lbCompanies.DataSource = companyCollection;
            lbCompanies.DataBind();
        }

        private static void DataBindReleases(ComboBoxInt comboBox, string applicationCode)
        {
            comboBox.DataSource = GetReleases(applicationCode);
            comboBox.DataBind();
        }

        private static ReleaseCollection GetReleases(string applicationCode)
        {
            PredicateExpression filter = new PredicateExpression
            {
                ApplicationFields.Code == applicationCode,
                ReleaseFields.Intermediate == false
            };

            RelationCollection relations = new RelationCollection {ReleaseEntityBase.Relations.ApplicationEntityUsingApplicationId};
            IncludeFieldsList includes = new IncludeFieldsList {ReleaseFields.ReleaseId, ReleaseFields.Version};

            SortExpression sort = new SortExpression(ReleaseFields.Version | SortOperator.Descending);

            ReleaseCollection releaseCollection = new ReleaseCollection();
            releaseCollection.GetMulti(filter, 0, sort, relations, null, includes, 0, 0);

            return releaseCollection;
        }

        private List<int> GetCompaniesToUpgrade() => lbCompanies.SelectedItemsValuesAsIntList;

        private List<int> GetSelectedReleaseIds() =>
            new[]
            {
                cbEmenuReleaseId.ValidId,
                cbConsoleReleaseId.ValidId,
                cbSupportToolsReleaseId.ValidId,
                cbAgentReleaseId.ValidId,
                cbMessagingReleaseId.ValidId,
                cbOsT2ReleaseId.ValidId,
                cbOsTminiReleaseId.ValidId,
                cbOsTminiV2ReleaseId.ValidId
            }.Where(x => x > 0).ToList();

        protected override void OnInit(EventArgs e)
        {
            LoadUserControls();
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e) => SetGui();
    }
}
