﻿<%@ Page Title="Default" Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms._Default" Codebehind="Default.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitleHolder" Runat="Server">
    <D:LabelTextOnly runat="server" ID="lblWelcome">Welkom</D:LabelTextOnly>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhToolbarHolder" Runat="Server">
	<D:Label runat="server" ID="lblChooseAModule" CssClass="gotomodule">Ga naar de door u gewenste module:</D:Label>	
	<ul>
		<D:PlaceHolder runat="server" ID="plhLinksToModules"></D:PlaceHolder>
	</ul>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhContentHolder" Runat="Server">
    <D:PlaceHolder runat="server" ID="plhDatabaseClearingRequired">
        <div style="width:450px;margin:0 auto;border:5px solid red;">
            <h1 style="display:block;background:red;color:white;padding:10px;text-align:center">Database is not cleared yet</h1>
            <div style="padding:10px;">
                <p>The database you're currently using on a non-Production environment isn't cleared yet. Please hit the button below to clear the database.</p>
                <p>&nbsp;</p>
                <D:Button runat="server" ID="btClearDatabase" Text="Clear Database" />
            </div>
        </div>
    </D:PlaceHolder>
    <D:PlaceHolder runat="server" ID="plhAmazonUsersToBeRemoved">
        <div style="width:450px;margin:0 auto;border:5px solid orange; margin-top: 10px;">
            <h1 style="display:block;background:orange;color:white;padding:10px;text-align:center">Amazon users not removed</h1>
            <div style="padding:10px;">
                <p>There are still users on amazon which were used by the old database. Remove them manually. </p><br/>
                <p>Users:</p>
                <D:PlaceHolder runat="server" ID="plhAmazonUsers"></D:PlaceHolder>
                <p>&nbsp;</p> 
                <D:Button runat="server" ID="btRecheckAmazonUsers" Text="Recheck" />
            </div>
        </div>
    </D:PlaceHolder>
    <D:Panel ID="pnlRecentCompanies" runat="server" GroupingText="Recent bekeken bedrijven">
        <ol class="olRecentCompanies">
            <D:PlaceHolder ID="plhRecentCompanies"  runat="server"></D:PlaceHolder>
        </ol>
    </D:Panel>    
    <D:PlaceHolder runat="server" ID="plhRestarts" Visible="false">
    <D:Label runat="server" ID="lblRestarterCompany" LocalizeText="false"></D:Label><br /><br />
    <D:Button runat="server" ID="btStart" Text="Start / Resume Restart" LocalizeText="false" /><br />
    <D:Button runat="server" ID="btStartClean" Text="Start Full (All) Restart" LocalizeText="false" /><br />
    <D:Button runat="server" ID="btStop" Text="Stop Restart" LocalizeText="false" /><br />
    <D:PlaceHolder runat="server" ID="plhRestartStatus"></D:PlaceHolder>
    </D:PlaceHolder>
    
    <D:PlaceHolder ID="plhOverview" runat="server" ></D:PlaceHolder>
    <!--
    <h1>What's new?</h1>
    <strong>v1.2011092317</strong>
    <ul>
        <li>New feature!</li>
    </ul>
        -->
</asp:Content>