﻿<%@ WebHandler Language="C#" Class="MediaCheck" %>

using System.Web;
using Obymobi;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Obymobi.Web.CloudStorage;
using SD.LLBLGen.Pro.ORMSupportClasses;

public class MediaCheck : IHttpHandler
{
    #region Properties

    public bool IsReusable
    {
        get { return false; }
    }

    #endregion

    #region Methods

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/plain";
        
        PrefetchPath prefetch = new PrefetchPath(EntityType.MediaEntity);
        prefetch.Add(MediaEntityBase.PrefetchPathMediaRatioTypeMediaCollection);
        prefetch.Add(MediaEntity.PrefetchPathAttachmentEntity).SubPath.Add(AttachmentEntity.PrefetchPathProductEntity);

        RelationCollection relations = new RelationCollection();
        relations.Add(MediaEntity.Relations.AttachmentEntityUsingAttachmentId);
        relations.Add(AttachmentEntity.Relations.ProductEntityUsingProductId);

        PredicateExpression filter = new PredicateExpression();
        filter.Add(ProductFields.BrandId == 2);

        MediaCollection mediaCollection = new MediaCollection();
        mediaCollection.GetMulti(filter, 0, null, relations, prefetch);

        context.Response.Write("Product, Attachment, MediaType, CDN Url");
        foreach (MediaEntity mediaEntity in mediaCollection)
        {
            ProductEntity product = mediaEntity.AttachmentEntity.ProductEntity;
            AttachmentEntity attachment = mediaEntity.AttachmentEntity;

            string urlOriginal = Obymobi.Web.CloudStorage.MediaExtensions.GetCdnBaseUrl(CloudStorageProviderIdentifier.Amazon, mediaEntity.GetMediaPath(FileNameType.Cdn));
            
            context.Response.Write(string.Format("{0},{1},Original,{2}\n", product.Name, attachment.Name, urlOriginal));

            foreach (MediaRatioTypeMediaEntity mediaRatioTypeMediaEntity in mediaEntity.MediaRatioTypeMediaCollection)
            {
                string cdnPath = MediaHelper.GetMediaRatioTypeMediaPath(mediaRatioTypeMediaEntity);
                string amazonUrl = WebEnvironmentHelper.GetCdnBaseUrlMedia(CloudStorageProviderIdentifier.Amazon, cdnPath, true);

                context.Response.Write(string.Format("{0},{1},{2},{3}\n", product.Name, attachment.Name, mediaRatioTypeMediaEntity.MediaTypeName, amazonUrl));
            }
        }

        context.Response.Write("Hello World");
    }

    #endregion
}