//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option or rebuild the Visual Studio project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class strings {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal strings() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Resources.strings", global::System.Reflection.Assembly.Load("App_GlobalResources"));
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No printer can be selected because this company does not have a linked Google account..
        /// </summary>
        internal static string Company_Deliverypointgroup_No_printer_can_be_selected {
            get {
                return ResourceManager.GetString("Company_Deliverypointgroup_No_printer_can_be_selected", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No printer can be selected because this company does not have a linked Google account..
        /// </summary>
        internal static string Company_Deliverypoint_No_printer_can_be_selected {
            get {
                return ResourceManager.GetString("Company_Deliverypoint_No_printer_can_be_selected", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Could not retrieve printers from Google Cloud Print..
        /// </summary>
        internal static string Configuration_Printers_Could_not_retrieve_printers {
            get {
                return ResourceManager.GetString("Configuration_Printers_Could_not_retrieve_printers", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Could not retrieve print jobs from Google Cloud Print..
        /// </summary>
        internal static string Configuration_Printers_Could_not_retrieve_print_jobs {
            get {
                return ResourceManager.GetString("Configuration_Printers_Could_not_retrieve_print_jobs", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This company does not have a linked Google account..
        /// </summary>
        internal static string Configuration_Printers_No_linked_Google_account {
            get {
                return ResourceManager.GetString("Configuration_Printers_No_linked_Google_account", resourceCulture);
            }
        }
    }
}
