﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Cms.Site" Codebehind="Site.aspx.cs" %>
<%@ Reference VirtualPath="~/Generic/SubPanels/Cms/PageManagerPanel.ascx" %>
<%@ Reference VirtualPath="~/Generic/UserControls/CustomTextCollection.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true"/>
<div>    
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
                    <D:PlaceHolder runat="server">
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblName">Name</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
							</td>
                            <td class="label">
                                <D:Label runat="server" id="lblSiteType">Site Type</D:Label>
                            </td>
                            <td class="control">
                                <X:ComboBoxInt runat="server" ID="ddlSiteType" IsRequired="true"></X:ComboBoxInt>                                
                            </td>
						</tr>
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblCompanyId">Bedrijf</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<X:ComboBoxLLBLGenEntityCollection runat="server" ID="cbCompanyId" PreventEntityCollectionInitialization="true" EntityName="Company"></X:ComboBoxLLBLGenEntityCollection>
                                <D:Label runat="server" ID="lblUsedByMultipleCompanies" Text="Deze site wordt door meerdere bedrijven gebruikt."></D:Label>
							</td>                     
                            <D:PlaceHolder runat="server" ID="plhLanguageSelectionOnAdd" />      
                            <D:PlaceHolder runat="server" ID="plhPreviewLinks">
                            <td class="label">
                            </td>
                            <td class="control">  
                            </td>
                            </D:PlaceHolder>                              
						</tr>
	                    <tr>
	                        <td class="label">
		                        <D:LabelEntityFieldInfo runat="server" ID="lblDescription">Description</D:LabelEntityFieldInfo>
	                        </td>
		                    <td colspan="3" class="control">
			                    <D:TextBox ID="tbDescription" runat="server" Rows="5" TextMode="MultiLine" CssClass="inputNoHeight"></D:TextBox>                
		                    </td>
	                    </tr>                                                			
                        <D:PlaceHolder runat="server" ID="plhPageManager">
                        <tr>
                            <td class="label">
                                <D:Label runat="server" id="lblSiteStructure">Site Structuur</D:Label>
                            </td>
                            <td class="control" colspan="3">
                                <div style="width: 736px;">
                                    <D:PlaceHolder runat="server" ID="plhPageManagerControl"></D:PlaceHolder>
                                </div>
                            </td>
                        </tr>							                  
                        </D:PlaceHolder> 
					</table>
                    </D:PlaceHolder>
				</Controls>
			</X:TabPage>     			
        </TabPages>
	</X:PageControl>
</div>
</asp:Content>