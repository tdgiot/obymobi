﻿<%@ Page Title="Site Conversion" Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Cms.SiteConversion"  Codebehind="SiteConversion.aspx.cs" %>
<%@ Register Namespace="DevExpress.Web" TagPrefix="dx" Assembly="DevExpress.Web.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitleHolder" Runat="Server">Site Conversion</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhToolbarHolder" Runat="Server"></asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhContentHolder" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Generic" Name="Generic">
				<controls>
					<D:PlaceHolder runat="server">					    
					    <table class="dataformV2">
                            <tr>
                                <td class="label">
                                    <D:Label ID="lblSite" runat="server">Site</D:Label>
                                </td>
                                <td class="control">
                                    <X:ComboBoxInt ID="cbSites" runat="server" TextField="Name" ValueField="SiteId" UseDataBinding="true">                                        
                                    </X:ComboBoxInt>
                                </td>
                                <td class="label">                                    
                                    <D:Label ID="lblVersionCaption" runat="server">Version</D:Label>
                                </td>
                                <td class="control">                 
                                    <D:Label ID="lblVersion" runat="server" LocalizeText="False">No site selected</D:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                </td>
                                <td class="control">
                                    <D:Button ID="btRefresh" Text="Review" runat="server" />
                                </td>
                                <td class="label">                                    
                                </td>
                                <td class="control">                                    
                                    <D:Button ID="btSwitchVersion" runat="server" Text="Switch version" />
                                </td>
                            </tr>                            
                            <tr>
                                <td colspan="4">
                                    <dx:ASPxGridView ID="grid" ClientInstanceName="grid" runat="server" OnInit="Grid_init" Width="100%" EnableCallBacks="True" OnCustomCallback="grid_CustomCallback">
                                        <Columns>
                                            <dx:GridViewDataColumn FieldName="RelatedEntityName" Caption="Image Name" Width="35%" VisibleIndex="0"></dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn FieldName="MediaRatioType" Caption="Ratio Type" Width="20%" VisibleIndex="1"></dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn FieldName="RelatedEntityType" Caption="Related Entity Type" Width="20%" VisibleIndex="2"></dx:GridViewDataColumn>                                            
                                            <dx:GridViewDataHyperLinkColumn FieldName="Link" Caption="Url" Width="20%" VisibleIndex="3">
                                                <PropertiesHyperLinkEdit Target="_blank"></PropertiesHyperLinkEdit>
                                            </dx:GridViewDataHyperLinkColumn>
                                            <dx:GridViewDataColumn FieldName="Verify" Caption="#" Width="5%" VisibleIndex="4">
                                                <DataItemTemplate>
                                                    <a href="javascript:void(0);" onclick="grid.PerformCallback('<%# Container.KeyValue %>')">
                                                        <%# Container.Text %>
                                                    </a>
                                                </DataItemTemplate>
                                            </dx:GridViewDataColumn>
                                        </Columns>
                                    </dx:ASPxGridView>
                                </td>
                            </tr>
					    </table>
                    </D:PlaceHolder>
                </controls>
            </X:TabPage>            
        </TabPages>
    </X:PageControl>
</div>
</asp:Content>