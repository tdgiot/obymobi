﻿using System;
using System.Collections.Generic;
using System.Linq;
using Crave.Api.Logic.Requests.Publishing;
using Crave.Api.Logic.Timestamps;
using Crave.Api.Logic.UseCases;
using Dionysos.Web.UI;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Cms;
using Obymobi.Logic.HelperClasses;
using Obymobi.ObymobiCms.Generic.SubPanels.Cms;
using Obymobi.ObymobiCms.UI;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Cms
{
    public partial class Site : PageLLBLGenEntityCms
    {
        #region Fields

        private PageManagerPanel pageManagerPanel;

        #endregion

        #region Methods

        protected override void OnInit(EventArgs e)
		{
            this.LoadUserControls();
            this.DataSourceLoaded += Site_DataSourceLoaded;
			base.OnInit(e);            
		}       

        public override bool InitializeEntity()
        {
            if (this.EntityId > 0)
            {
                this.DataSource = Obymobi.Cms.Logic.Sites.SiteHelper.GetSite(this.EntityId, true);
            }

            return base.InitializeEntity();
        }

        private void LoadUserControls()
        {
            this.tabsMain.AddTabPage("Cultures", "SiteCultureCollection", "~/Cms/SubPanels/SiteCultureCollection.ascx");

            if (this.PageMode != Dionysos.Web.PageMode.Add)
            {
                Generic.UserControls.CustomTextCollection translationsPanel = this.tabsMain.AddTabPage("Translations", "CustomTextCollection", "~/Generic/UserControls/CustomTextCollection.ascx", "Generic") as Generic.UserControls.CustomTextCollection;
                translationsPanel.FullWidth = true;

                this.tabsMain.AddTabPage("Afbeeldingen", "Media", "~/Generic/UserControls/MediaCollection.ascx");

                this.pageManagerPanel = (PageManagerPanel)this.LoadControl("~/Generic/SubPanels/Cms/PageManagerPanel.ascx");
                this.pageManagerPanel.Init(EntityType.SiteEntity);
                this.plhPageManagerControl.Controls.Add(this.pageManagerPanel);
            }
        }

        private void SetGui()
        {
            // Load Compatible Site Types
            IOrderedEnumerable<SiteType> siteTypes = Obymobi.Cms.Logic.Sites.SiteHelper.SiteTypesCompatibleWithSite(this.DataSourceAsSiteEntity).OrderBy(x => x.ToString());
            foreach (SiteType siteType in siteTypes)
            {
                this.ddlSiteType.Items.Add(siteType.ToString(), (int)siteType);
            }
            
            // Companies
            List<int> companyIds = CmsSessionHelper.CompanyCollectionForUser.Select(c => c.CompanyId).ToList();
            CompanyCollection companiesUsingSiteOnUiTab = Obymobi.Cms.Logic.Sites.SiteHelper.CompaniesUsingSiteAsUITab(this.DataSourceAsSiteEntity, null, companyIds);
            this.lblUsedByMultipleCompanies.Visible = companiesUsingSiteOnUiTab.Count > 1;
            this.cbCompanyId.Visible = !this.lblUsedByMultipleCompanies.Visible;
            if (companiesUsingSiteOnUiTab.Count <= 1)
            {
                CompanyCollection companies = new CompanyCollection();
                IncludeFieldsList fields = new IncludeFieldsList(CompanyFields.Name);
                SortExpression sort = new SortExpression(CompanyFields.Name | SortOperator.Ascending);
                PredicateExpression filter = new PredicateExpression();
                if (CmsSessionHelper.CurrentRole < Role.Administrator)
                {
                    filter.Add(CompanyFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

                    this.cbCompanyId.DisplayEmptyItem = false;
                    this.cbCompanyId.SelectedIndex = 0;
                    this.cbCompanyId.Enabled = false;
                }
                else if (companiesUsingSiteOnUiTab.Count == 1)
                {
                    filter.Add(CompanyFields.CompanyId == companiesUsingSiteOnUiTab[0].CompanyId);
                }
                else
                {
                    filter.Add(CompanyFields.CompanyId == companyIds);
                }

                companies.GetMulti(filter, 0, sort, null, null, fields, 0, 0);
                this.cbCompanyId.DataSource = companies;
                this.cbCompanyId.DataBind();
            }

            // Hide Page Manager when adding
            bool addMode = (this.PageMode == Dionysos.Web.PageMode.Add);
            this.plhPageManager.Visible = !addMode;            
            
            //this.SetLanguagesRelatedGui();

            if (this.pageManagerPanel != null)
            {
                this.pageManagerPanel.RefreshDataSource(this.DataSource);
            }
        }
        
        #endregion

		#region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {            
            bool addMode = (this.PageMode == Dionysos.Web.PageMode.Add);
            if (!addMode)
            {
                if (this.DataSourceAsSiteEntity.SiteCultureCollection.Count == 0)
                {
                    this.AddInformator(InformatorType.Warning, this.Translate("NoLanguagesHaveBeenChosen", "Er zijn voor deze site nog geen talen ingesteld."));
                    this.Validate();
                }
            }
		}

        private void Site_DataSourceLoaded(object sender)
        {
            this.SetGui();
        }

        public override bool Save()
        {
            if (base.Save())
            {
                CustomTextHelper.UpdateCustomTexts(this.DataSource);
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region Properties

        public SiteEntity DataSourceAsSiteEntity
        {
            get
            {
                return this.DataSource as SiteEntity;
            }
        }

		#endregion
    }
}
