﻿using System;
using System.Collections.Generic;
using Dionysos.Web.UI;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using DevExpress.Web;
using Obymobi.Data.CollectionClasses;
using System.Data;
using System.Linq;
using Dionysos.Data.LLBLGen;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data;
using Obymobi.Enums;
using System.Web.UI;
using DevExpress.Web.Internal;
using Obymobi.Logic.Cms;

namespace Obymobi.ObymobiCms.Cms
{
    public partial class SiteConversion : PageDefault
    {
        #region Fields

        private static readonly List<MediaType> MediaTypesToCheck = new List<MediaType>
                                                                             {
                                                                                 MediaType.SiteMenuHeader1280x800,
                                                                                 MediaType.SiteMenuIcon1280x800,
                                                                                 MediaType.SitePageFull1280x800,
                                                                                 MediaType.SitePageTwoThirds1280x800,
                                                                                 MediaType.SitePageTwoThirdsPointOfInterest1280x800,
                                                                                 MediaType.SitePageTwoThirdsCompany1280x800
                                                                             };

        private static readonly string KEY_FIELD = "MediaRatioTypeMediaId";
        private static readonly string RELATED_ENTITY_NAME_FIELD = "RelatedEntityName";
        private static readonly string RELATED_ENTITY_TYPE_FIELD = "RelatedEntityType";
        private static readonly string MEDIA_RATIO_TYPE_FIELD = "MediaRatioType";
        private static readonly string LINK_FIELD = "Link";
        private static readonly string VERIFY_FIELD = "Verify";

        #endregion

        #region Methods

        private void LoadUserControls()
        {
            this.cbSites.DataBindEntityCollection<SiteEntity>(null, SiteFields.Name, SiteFields.Name);
        }

        private void RenderSite(int siteId)
        {
            if (siteId <= 0)
            {
                this.lblVersion.Text = "No site selected";
                return;
            }
            
            SiteEntity site = new SiteEntity(this.SiteId);
            this.lblVersion.Text = site.Version.ToEnum<SiteVersion>().ToString();

            MediaRatioTypeMediaCollection ratios = this.Fetch(siteId);
            DataTable table = this.ConvertToDataTable(ratios);

            this.grid.DataSource = table;
            this.grid.DataBind();
        }

        private void CreateGridView()
        {
            this.grid.KeyFieldName = "MediaRatioTypeMediaId";
            this.grid.SettingsEditing.Mode = GridViewEditingMode.Inline;
            this.grid.SettingsPager.PageSize = 30;

            this.grid.HtmlDataCellPrepared += Grid_HtmlDataCellPrepared;
            this.grid.HtmlRowPrepared += GridOnHtmlRowPrepared;
        }

        private void HookupEvents()
        {
            this.btRefresh.Click += BtRefresh_Click;
            this.btSwitchVersion.Click += BtSwitchVersionOnClick;
        }        

        private DataTable ConvertToDataTable(MediaRatioTypeMediaCollection ratios)
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add(new DataColumn(SiteConversion.KEY_FIELD));
            dataTable.Columns.Add(new DataColumn(SiteConversion.VERIFY_FIELD));
            dataTable.Columns.Add(new DataColumn(SiteConversion.RELATED_ENTITY_NAME_FIELD));
            dataTable.Columns.Add(new DataColumn(SiteConversion.RELATED_ENTITY_TYPE_FIELD));
            dataTable.Columns.Add(new DataColumn(SiteConversion.MEDIA_RATIO_TYPE_FIELD));
            dataTable.Columns.Add(new DataColumn(SiteConversion.LINK_FIELD));            

            foreach (MediaRatioTypeMediaEntity ratio in ratios)
            {
                this.AddRow(dataTable, ratio);
            }

            return dataTable;
        }

        private void AddRow(DataTable table, MediaRatioTypeMediaEntity ratio)
        {
            DataRow row = table.NewRow();
            row[SiteConversion.KEY_FIELD] = ratio.MediaRatioTypeMediaId + "-" + ratio.MediaId + "-" + ratio.ManuallyVerified + "-" + (int)ratio.MediaTypeAsEnum;
            row[SiteConversion.RELATED_ENTITY_NAME_FIELD] = ratio.RelatedMediaName;
            row[SiteConversion.RELATED_ENTITY_TYPE_FIELD] = ratio.RelatedEntityName;
            row[SiteConversion.MEDIA_RATIO_TYPE_FIELD] = ratio.MediaTypeName;
            row[SiteConversion.LINK_FIELD] = "Link";
            row[SiteConversion.VERIFY_FIELD] = ratio.ManuallyVerified ? "Verified" : "Verify";

            table.Rows.Add(row);
        }

        private MediaRatioTypeMediaCollection Fetch(int siteId)
        {
            if (siteId <= 0)
            {
                return new MediaRatioTypeMediaCollection();
            }
            
            SortExpression sort = new SortExpression(new SortClause(MediaRatioTypeMediaFields.MediaType, SortOperator.Ascending));
            sort.Add(new SortClause(MediaRatioTypeMediaFields.MediaRatioTypeMediaId, SortOperator.Ascending));

            PrefetchPath prefetch = new PrefetchPath(EntityType.MediaRatioTypeMediaEntity);
            prefetch.Add(MediaRatioTypeMediaEntityBase.PrefetchPathMediaEntity, new IncludeFieldsList(MediaFields.Name));

            List<int> pointOfInterestIds;
            List<int> companyIds;

            this.FetchPageElementVenueIds(siteId, out pointOfInterestIds, out companyIds);

            MediaRatioTypeMediaCollection ratios = new MediaRatioTypeMediaCollection();

            ratios.AddRange(this.FetchForSite(siteId, sort, prefetch));
            ratios.AddRange(this.FetchForPages(siteId, sort, prefetch));
            ratios.AddRange(this.FetchForPageElements(siteId, sort, prefetch));
            ratios.AddRange(this.FetchForPointOfInterests(pointOfInterestIds, sort, prefetch));
            ratios.AddRange(this.FetchForCompanies(companyIds, sort, prefetch));

            return ratios;
        }

        private PredicateExpression CreateFilter(IPredicateExpression extraFilter)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(MediaRatioTypeMediaFields.MediaType == SiteConversion.MediaTypesToCheck);
            filter.Add(extraFilter);
            return filter;
        }

        private MediaRatioTypeMediaCollection FetchForSite(int siteId, SortExpression sort, PrefetchPath prefetch)
        {
            PredicateExpression filter = this.CreateFilter(new PredicateExpression(SiteFields.SiteId == siteId));
            RelationCollection relations = new RelationCollection(MediaRatioTypeMediaEntityBase.Relations.MediaEntityUsingMediaId);
            relations.Add(MediaEntityBase.Relations.SiteEntityUsingSiteId);
            return EntityCollection.GetMulti<MediaRatioTypeMediaCollection>(filter, sort, relations, prefetch, null);
        }

        private MediaRatioTypeMediaCollection FetchForPages(int siteId, SortExpression sort, PrefetchPath prefetch)
        {
            PredicateExpression filter = this.CreateFilter(new PredicateExpression(PageFields.SiteId == siteId));
            RelationCollection relations = new RelationCollection(MediaRatioTypeMediaEntityBase.Relations.MediaEntityUsingMediaId);
            relations.Add(MediaEntityBase.Relations.PageEntityUsingPageId);
            return EntityCollection.GetMulti<MediaRatioTypeMediaCollection>(filter, sort, relations, prefetch, null);
        }

        private MediaRatioTypeMediaCollection FetchForPageElements(int siteId, SortExpression sort, PrefetchPath prefetch)
        {
            PredicateExpression filter = this.CreateFilter(new PredicateExpression(PageFields.SiteId == siteId));
            RelationCollection relations = new RelationCollection(MediaRatioTypeMediaEntityBase.Relations.MediaEntityUsingMediaId);
            relations.Add(MediaEntityBase.Relations.PageElementEntityUsingPageElementId);
            relations.Add(PageElementEntityBase.Relations.PageEntityUsingPageId);
            return EntityCollection.GetMulti<MediaRatioTypeMediaCollection>(filter, sort, relations, prefetch, null);
        }

        private MediaRatioTypeMediaCollection FetchForPointOfInterests(List<int> pointOfInterestIds, SortExpression sort, PrefetchPath prefetch)
        {
            PredicateExpression filter = this.CreateFilter(new PredicateExpression(MediaFields.PointOfInterestId == pointOfInterestIds));
            RelationCollection relations = new RelationCollection(MediaRatioTypeMediaEntityBase.Relations.MediaEntityUsingMediaId);
            return EntityCollection.GetMulti<MediaRatioTypeMediaCollection>(filter, sort, relations, prefetch, null);
        }

        private MediaRatioTypeMediaCollection FetchForCompanies(List<int> companyIds, SortExpression sort, PrefetchPath prefetch)
        {
            PredicateExpression filter = this.CreateFilter(new PredicateExpression(MediaFields.CompanyId == companyIds));
            RelationCollection relations = new RelationCollection(MediaRatioTypeMediaEntityBase.Relations.MediaEntityUsingMediaId);
            return EntityCollection.GetMulti<MediaRatioTypeMediaCollection>(filter, sort, relations, prefetch, null);
        }

        private void FetchPageElementVenueIds(int siteId, out List<int> pointOfInterestIds, out List<int> companyIds)
        {
            PredicateExpression filter = new PredicateExpression(PageElementFields.PageElementType == (int)PageElementType.Venue);
            filter.Add(PageFields.SiteId == siteId);
            RelationCollection relations = new RelationCollection(PageElementEntityBase.Relations.PageEntityUsingPageId);
            IncludeFieldsList includes = new IncludeFieldsList(PageElementFields.IntValue1, PageElementFields.IntValue2);
            PageElementCollection pageElements = EntityCollection.GetMulti<PageElementCollection>(filter, null, relations, null, includes);
            pointOfInterestIds = pageElements.Where(x => x.IntValue2.GetValueOrDefault(0) > 0).Select(x => x.IntValue2.GetValueOrDefault()).ToList();
            companyIds = pageElements.Where(x => x.IntValue1.GetValueOrDefault(0) > 0).Select(x => x.IntValue1.GetValueOrDefault()).ToList();
        }

        private void VerifyMediaRatioTypeMedia(int mediaRatioTypeMediaId)
        {
            MediaRatioTypeMediaEntity entity = new MediaRatioTypeMediaEntity(mediaRatioTypeMediaId);
            entity.ManuallyVerified = true;
            entity.Save();
        }

        private void RefreshSite()
        {
            if (this.cbSites.ValidId > 0)
            {
                this.SiteId = this.cbSites.ValidId;
                this.RenderSite(this.SiteId);
            }
        }

        private void SetGui()
        {
            
        }

        #endregion

        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.HookupEvents();
            this.SetGui();
            this.RefreshSite();
        }

        protected void Grid_init(object sender, EventArgs e)
        {
            this.CreateGridView();
        }

        private void BtRefresh_Click(object sender, EventArgs e)
        {
            this.SiteId = this.cbSites.ValidId;
        }

        protected void grid_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            string[] keyValues = e.Parameters.Split('-');
            if (keyValues.Length > 0)
            {
                int mediaRatioTypeMediaId = int.Parse(keyValues[0]);
                this.VerifyMediaRatioTypeMedia(mediaRatioTypeMediaId);
                this.RenderSite(this.SiteId);
            }            
        }

        private void Grid_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.DataColumn.FieldName.Equals(SiteConversion.LINK_FIELD, StringComparison.InvariantCultureIgnoreCase))
            {
                string key = e.KeyValue.ToString();
                string[] keyValues = key.Split('-');
                if (keyValues.Length > 3)
                {
                    string url = string.Format("~/Generic/Media.aspx?id={0}&tab={1}", keyValues[1], keyValues[3]);
                    foreach (Control item in e.Cell.Controls)
                    {
                        HyperLinkDisplayControl link = item as HyperLinkDisplayControl;
                        if (link != null)
                        {
                            link.NavigateUrl = url;
                            break;
                        }
                    }
                }                               
            }             
        }

        private void GridOnHtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.KeyValue == null)
                return;

            string[] keyValues = e.KeyValue.ToString().Split('-');
            if (keyValues.Length > 2)
            {
                bool verified;
                if (!bool.TryParse(keyValues[2], out verified))
                {
                    // Failed man :(
                }
                e.Row.ForeColor = verified ? System.Drawing.Color.Green : System.Drawing.Color.Red;
            }
        }        

        private void BtSwitchVersionOnClick(object sender, EventArgs eventArgs)
        {
            if (this.SiteId > 0)
            {
                SiteEntity siteEntity = new SiteEntity(this.SiteId);
                siteEntity.Version = siteEntity.Version == 0 ? 1 : 0;
                siteEntity.Save();
                this.RenderSite(this.SiteId);
            }
        }

        #endregion

        #region Properties

        public int SiteId
        {
            get
            {
                if (this.ViewState[this.ID] == null)
                    this.ViewState[this.ID] = -1;

                return (int)this.ViewState[this.ID];
            }
            set
            {
                this.ViewState[this.ID] = value;
            }
        }

        #endregion        
    }    
}