﻿using Obymobi.Logic.Cms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using Dionysos;
using Obymobi.Data.EntityClasses;
using PageEntity = Dionysos.Web.UI.PageEntity;

namespace Obymobi.ObymobiCms.Cms.SubPanels
{
    public partial class PageTypeElementsCollectionPanel : UserControl
    {
        #region Methods

        private void RenderEditors()
        {
            // Render Editors 
            PageTypeBase pageType = PageTypeHelper.GetPageTypeInstance(this.DataSource.PageType.ToEnum<PageType>());
            pageType.PageElementsCollection = this.GetPageTypeDataSource(this.CultureCode);

            this.tbCulture.Text = this.CultureCode.IsNullOrWhiteSpace() ? "Lang. Agnostic" : Obymobi.Culture.Mappings[this.CultureCode].NameAndCultureCode;

            this.plhEditors.Controls.Clear();
            foreach (PageTypeElement pageTypeElement in pageType.PageTypeElements)
            {
                // Skip if element is not CMS editable
                if (!pageTypeElement.CmsEditable)
                    continue;

                // Skip if this is the LanguageAgnostic Panel - Only render the Language Agnostic PageTypeElements         
                if (this.CultureCode.IsNullOrWhiteSpace() && pageTypeElement.DataSource == null)
                    continue;
                
                // Skip if it's a language panel only render the non-agnostic pagetypeelements
                if (!this.CultureCode.IsNullOrWhiteSpace() && this.GetPageTypeDataSource(null).Any(x => x.SystemName == pageTypeElement.SystemName && x.CultureCode.IsNullOrWhiteSpace()))
                    continue;

                // Ensure a PageElement is available for each PageTypeElement, if not, create it.       
                if (pageTypeElement.DataSource == null)
                {
                    if (this.DataSource is Data.EntityClasses.PageEntity)
                    {
                        var pageElementEntity = new PageElementEntity();
                        pageElementEntity.PageId = this.DataSource.PrimaryId;
                        pageElementEntity.SystemName = pageTypeElement.SystemName;
                        pageElementEntity.CultureCode = this.CultureCode;
                        pageElementEntity.PageElementType = (int)pageTypeElement.PageTypeElementType;
                        pageElementEntity.Save();

                        ((Data.EntityClasses.PageEntity)this.DataSource).PageElementCollection.Add(pageElementEntity);
                        pageTypeElement.DataSource = pageElementEntity;
                    }
                    else if (this.DataSource is Data.EntityClasses.PageTemplateEntity)
                    {
                        var pageTemplateElementEntity = new PageTemplateElementEntity();
                        pageTemplateElementEntity.PageTemplateId = this.DataSource.PrimaryId;
                        pageTemplateElementEntity.SystemName = pageTypeElement.SystemName;
                        pageTemplateElementEntity.CultureCode = this.CultureCode;
                        pageTemplateElementEntity.PageElementType = (int)pageTypeElement.PageTypeElementType;
                        pageTemplateElementEntity.Save();

                        ((Data.EntityClasses.PageTemplateEntity)this.DataSource).PageTemplateElementCollection.Add(pageTemplateElementEntity);
                        pageTypeElement.DataSource = pageTemplateElementEntity;
                    }                    
                }

                PageTypeElementEditorBase editorControl;
                switch (pageTypeElement.PageTypeElementType)
                {
                    case PageElementType.SingleLineText:
                        editorControl = this.LoadControl("~/Cms/SubPanels/PageTypeElementEditors/SingleLineTextEditor.ascx") as PageTypeElementEditorBase;
                        break;
                    case PageElementType.MultiLineText:
                        editorControl = this.LoadControl("~/Cms/SubPanels/PageTypeElementEditors/MultiLineTextEditor.ascx") as PageTypeElementEditorBase;
                        break;
                    case PageElementType.Image:
                        editorControl = this.LoadControl("~/Cms/SubPanels/PageTypeElementEditors/ImageEditorsPanel.ascx") as PageTypeElementEditorBase;
                        break;
                    case PageElementType.WebView:
                        editorControl = this.LoadControl("~/Cms/SubPanels/PageTypeElementEditors/WebViewEditor.ascx") as PageTypeElementEditorBase;
                        break;
                    case PageElementType.Map:
                        editorControl = this.LoadControl("~/Cms/SubPanels/PageTypeElementEditors/MapEditor.ascx") as PageTypeElementEditorBase;
                        break;
                    case PageElementType.YouTube:
                        editorControl = this.LoadControl("~/Cms/SubPanels/PageTypeElementEditors/YouTubeEditor.ascx") as PageTypeElementEditorBase;
                        break;
                    case PageElementType.Venue:
                        editorControl = this.LoadControl("~/Cms/SubPanels/PageTypeElementEditors/VenueEditor.ascx") as PageTypeElementEditorBase;
                        break;
                    case PageElementType.PageLink:
                        if (this.DataSource is Data.EntityClasses.PageTemplateEntity)
                        {
                            editorControl = this.LoadControl("~/Cms/SubPanels/PageTypeElementEditors/PageTemplateLinkEditor.ascx") as PageTypeElementEditorBase;
                        }
                        else
                        {
                            editorControl = this.LoadControl("~/Cms/SubPanels/PageTypeElementEditors/PageLinkEditor.ascx") as PageTypeElementEditorBase;
                        }
                        break;
                    default:
                        throw new NotImplementedException("PageTypeElementType: " + pageTypeElement.PageTypeElementType);
                }

                editorControl.ID = $"PageTypeElementEditor_{pageTypeElement.PageTypeElementType}_{this.CultureCode}_{pageTypeElement.SystemName}";
                editorControl.PageTypeElement = pageTypeElement;                
                this.plhEditors.Controls.Add(editorControl);
            }
        }

        public List<IPageTypeElementData> GetPageTypeDataSource(string cultureCode)
        {
            if (this.DataSource is Data.EntityClasses.PageEntity)
            {
                return ((Data.EntityClasses.PageEntity)this.DataSource).GetPageTypeDataSource(cultureCode);
            }
            else if (this.DataSource is Data.EntityClasses.PageTemplateEntity)
            {
                return ((Data.EntityClasses.PageTemplateEntity)this.DataSource).GetPageTypeDataSource(cultureCode);
            }
            return new List<IPageTypeElementData>();
        }

        #endregion

        #region Properties

        private IPage dataSource;
        public IPage DataSource
        {
            get { return this.dataSource; }
            set
            {
                this.dataSource = value;
                this.RenderEditors();
            }
        }

        public string CultureCode { get; set; }

        #endregion
    }
}