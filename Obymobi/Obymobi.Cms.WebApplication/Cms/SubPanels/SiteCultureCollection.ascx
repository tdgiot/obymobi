﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Cms_SubPanels_SiteCultureCollection" Codebehind="SiteCultureCollection.ascx.cs" %>

<D:Panel ID="pnlCultures" runat="server" GroupingText="Cultures">
    <table class="dataformV2" style="width: 50%; float: left;">
        <tr>
            <td class="label">
	            <D:Label runat="server" ID="lblCultures" LocalizeText="true">Cultures</D:Label>
            </td>
            <td class="control">
	            <X:ListBox runat="server" ID="lbCultures" SelectionMode="CheckColumn" TextField="NameAndCultureCode" ValueField="Code"></X:ListBox>
            </td>            
        </tr>
    </table>        
</D:Panel>