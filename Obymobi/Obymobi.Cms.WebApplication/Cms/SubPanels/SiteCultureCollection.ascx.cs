﻿using Dionysos.Web.UI;
using Obymobi.Data.EntityClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.Web;
using Obymobi;
using Dionysos.Web.UI.WebControls;

public partial class Cms_SubPanels_SiteCultureCollection : Dionysos.Web.UI.WebControls.SubPanelLLBLGenEntity
{
    #region Fields

    private readonly List<TextBoxDecimal> cultureTextBoxes = new List<TextBoxDecimal>();

    #endregion

    #region Methods

    protected override void OnInit(EventArgs e)
    {
        this.EntityName = "SiteCulture";
        this.LoadUserControls();
        base.OnInit(e);
    }

    private void LoadUserControls()
    {
        this.lbCultures.DataSource = Obymobi.Culture.Mappings.Values.OrderBy(x => x.NameAndCultureCode);
        this.lbCultures.DataBind();
    }

    protected override void OnParentDataSourceLoaded()
    {
        base.OnParentDataSourceLoaded();

        this.FillAllCultures();
    }    

    private void FillAllCultures()
    {
        List<string> cultureCodes = this.DataSourceAsSiteEntity.SiteCultureCollection.Select(x => x.CultureCode).ToList();
        foreach (ListEditItem item in this.lbCultures.Items)
        {
            if (cultureCodes.Contains(item.Value))
            {
                item.Selected = true;
            }
        }
    }    

    public override bool Save()
    {        
        this.SaveCultureCodes(this.lbCultures.Items);

        return true;
    }

    private void SaveCultureCodes(ListEditItemCollection items)
    {
        foreach (ListEditItem item in items)
        {
            string cultureCode = item.Value.ToString();

            if (cultureCode.Equals(Obymobi.Culture.English_United_Kingdom.Code, StringComparison.InvariantCultureIgnoreCase))
            {
                // en-GB is the default culture code, can't remove this one. (check api, GetSite first retrieves a site using this culture code hardcoded)
                item.Selected = true;
            }

            SiteCultureEntity culture = this.DataSourceAsSiteEntity.SiteCultureCollection.FirstOrDefault(x => x.CultureCode == cultureCode);
            if (item.Selected && culture == null)
            {
                culture = new SiteCultureEntity();
                culture.SiteId = this.DataSourceAsSiteEntity.SiteId;
                culture.CultureCode = cultureCode;
                culture.Save();
            }
            else if (!item.Selected && culture != null)
            {
                culture.Delete();
                this.DataSourceAsSiteEntity.SiteCultureCollection.Remove(culture);
            }
        }
    }    

    #endregion
    
    #region Properties

    public new PageLLBLGenEntity Page
    {
        get
        {
            return base.Page as PageLLBLGenEntity;
        }
    }

    public SiteEntity DataSourceAsSiteEntity
    {
        get
        {
            return this.Page.DataSource as SiteEntity;
        }
    }    

    #endregion
}