﻿using Dionysos.Web.UI;
using Obymobi.Data.EntityClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.Web;
using Obymobi;
using Dionysos.Web.UI.WebControls;

public partial class Cms_SubPanels_SiteTemplateCultureCollection : Dionysos.Web.UI.WebControls.SubPanelLLBLGenEntity
{
    #region Fields

    private readonly List<TextBoxDecimal> cultureTextBoxes = new List<TextBoxDecimal>();

    #endregion

    #region Methods

    protected override void OnInit(EventArgs e)
    {
        this.EntityName = "SiteTemplateCulture";
        this.LoadUserControls();
        base.OnInit(e);        
    }

    private void LoadUserControls()
    {
        this.lbCultures.DataSource = Obymobi.Culture.Mappings.Values.OrderBy(x => x.ToString());
        this.lbCultures.DataBind();
    }

    protected override void OnParentDataSourceLoaded()
    {
        base.OnParentDataSourceLoaded();

        this.FillAllCultures();
    }    

    private void FillAllCultures()
    {
        List<string> cultureCodes = this.DataSourceAsSiteTemplateEntity.SiteTemplateCultureCollection.Select(x => x.CultureCode).ToList();
        foreach (ListEditItem item in this.lbCultures.Items)
        {
            if (cultureCodes.Contains(item.Value))
            {
                item.Selected = true;
            }
        }
    }    

    public override bool Save()
    {        
        this.SaveCultureCodes(this.lbCultures.Items);

        return true;
    }

    private void SaveCultureCodes(ListEditItemCollection items)
    {
        foreach (ListEditItem item in items)
        {
            string currencyCode = item.Value.ToString();

            SiteTemplateCultureEntity culture = this.DataSourceAsSiteTemplateEntity.SiteTemplateCultureCollection.FirstOrDefault(x => x.CultureCode == currencyCode);
            if (item.Selected && culture == null)
            {
                culture = new SiteTemplateCultureEntity();
                culture.SiteTemplateId = this.DataSourceAsSiteTemplateEntity.SiteTemplateId;
                culture.CultureCode = currencyCode;
                culture.Save();
            }
            else if (!item.Selected && culture != null)
            {
                culture.Delete();
                this.DataSourceAsSiteTemplateEntity.SiteTemplateCultureCollection.Remove(culture);
            }
        }
    }    

    #endregion
    
    #region Properties

    public new PageLLBLGenEntity Page
    {
        get
        {
            return base.Page as PageLLBLGenEntity;
        }
    }

    public SiteTemplateEntity DataSourceAsSiteTemplateEntity
    {
        get
        {
            return this.Page.DataSource as SiteTemplateEntity;
        }
    }    

    #endregion
}