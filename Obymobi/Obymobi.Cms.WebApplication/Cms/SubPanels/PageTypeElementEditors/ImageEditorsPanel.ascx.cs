﻿using Obymobi.Logic.Cms;
using System;
using Dionysos;
using Obymobi.Data.EntityClasses;
using Dionysos.Web;

namespace Obymobi.ObymobiCms.Cms.SubPanels.PageTypeElementEditors
{
    public partial class ImageEditorsPanel : PageTypeElementEditorBase<ImageElement>
    {
        #region Methods

        private void RenderEditors()
        {
            var twoMedia = new Data.CollectionClasses.MediaCollection();
            for (int i = 0; i < this.PageTypeElementDataSourceAsMediaContainingEntity.MediaCollection.Count; i++)
            {
                twoMedia.Add(this.PageTypeElementDataSourceAsMediaContainingEntity.MediaCollection[i]);

                if (twoMedia.Count == 2 || ((i + 1) == this.PageTypeElementDataSourceAsMediaContainingEntity.MediaCollection.Count))
                {
                    var control = this.LoadControl("~/Cms/SubPanels/PageTypeElementEditors/ImageEditor.ascx") as ImageEditor;
                    control.MediaCollection = twoMedia;
                    this.plhMedia.Controls.Add(control);
                    
                    twoMedia.Clear();
                }                                    
            }                            
        }

        #endregion

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            this.lblPageTypeElementName.Text = this.PageTypeElement.Name;
            this.hlUploadImage.Click += hlUploadImage_Click;            
            this.hlUploadImage.OnClientClick = "pageIsDirty = false;";            
        }

        protected override void CreateChildControls()
        {
            this.RenderEditors();
        }

        protected override void DataBindPageTypeElement()
        {
            
        }

        protected override void DataBindControls()
        {
            
        }        

        private void hlUploadImage_Click(object sender, EventArgs e)
        {
            this.PageAsPageEntity.Validate();
            if (this.PageAsPageEntity.IsValid && this.PageAsPageEntity.Save())
            {
                int primaryId = this.PageTypeElement.DataSource.PrimaryId;

                if (this.PageTypeElement.DataSource is PageElementEntity)
                {
                    WebShortcuts.Redirect(this.ResolveUrl(string.Format("~/Generic/Media.aspx?mode=add&RelatedEntityType=PageElement&RelatedEntityId={0}", primaryId)));
                }                
                else if (this.PageTypeElement.DataSource is PageTemplateElementEntity)
                {
                    WebShortcuts.Redirect(this.ResolveUrl(string.Format("~/Generic/Media.aspx?mode=add&RelatedEntityType=PageTemplateElement&RelatedEntityId={0}", primaryId)));
                }                
            }
        }

        #endregion

        #region Properties

        

        #endregion
    }
}