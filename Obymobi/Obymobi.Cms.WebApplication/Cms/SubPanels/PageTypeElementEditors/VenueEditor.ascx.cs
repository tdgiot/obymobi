﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logic.Cms;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Obymobi.ObymobiCms.Cms.SubPanels.PageTypeElementEditors
{
    public partial class VenueEditor : PageTypeElementEditorBase<VenueElement>
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.lblPageTypeElementName.Text = this.PageTypeElement.Name;
        }

        protected override void DataBindPageTypeElement()
        {
            this.PageTypeElement.CompanyId = this.cbCompanyId.ValidId;
            this.PageTypeElement.PointOfInterestId = this.cbPointOfInterestId.ValidId;
            //this.PageTypeElement.FallBackSiteId = this.cbSiteId.ValidId;
        }

        protected override void DataBindControls()
        {
            this.cbCompanyId.DataSource = CmsSessionHelper.CompanyCollectionForUser;
            this.cbCompanyId.DataBind();

            if(this.PageTypeElement.CompanyId > 0)
                this.cbCompanyId.Value = this.PageTypeElement.CompanyId;

            if(this.PageTypeElement.PointOfInterestId > 0)
                this.cbPointOfInterestId.Value = this.PageTypeElement.PointOfInterestId;

            /*if (this.PageTypeElement.FallBackSiteId > 0)
                this.cbSiteId.Value = this.PageTypeElement.FallBackSiteId;*/
        }
    }
}