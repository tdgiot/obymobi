﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Cms.SubPanels.PageTypeElementEditors.ImageEditor" Codebehind="ImageEditor.ascx.cs" %>                     

<tr>
    <td class="label">
    </td>
    <td class="control">
        
            <div>
                <div style="float:left;margin-right:10px;">
                    <D:Image runat="server" ID="imgFirstThumbnail" />
                </div>
                <div style="float:left;">
                    <D:Label runat="server" CssClass="nobold" ID="lblFirstMediaLanguages" LocalizeText="false"></D:Label>
                    <span class="nobold"><D:HyperLink runat="server" ID="hlFirstEditImage">>Afbeelding aanpassen</D:HyperLink></span>
                    <span class="nobold"><D:HyperLink runat="server" ID="hlFirstDeleteImage">Afbeelding verwijderen</D:HyperLink></span>
                </div>
            </div>
        
    </td>
    <td class="label">
    </td>
    <td class="control">
        <D:PlaceHolder runat="server" ID="plhImage">
            <div>
                <div style="float:left;margin-right:10px;">
                    <D:Image runat="server" ID="imgSecondThumbnail" />
                </div>
                <div style="float:left;">
                    <D:Label runat="server" CssClass="nobold" ID="lblSecondMediaLanguages" LocalizeText="false"></D:Label>
                    <span class="nobold"><D:HyperLink runat="server" ID="hlSecondEditImage">Afbeelding aanpassen</D:HyperLink></span>
                    <span class="nobold"><D:HyperLink runat="server" ID="hlSecondDeleteImage">Afbeelding verwijderen</D:HyperLink></span>
                </div>
            </div>
        </D:PlaceHolder>
    </td>
</tr>						                               