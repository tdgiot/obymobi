﻿using Obymobi.Logic.Cms;
using System;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Cms.SubPanels.PageTypeElementEditors
{
    public partial class PageLinkEditor : PageTypeElementEditorBase<PageLinkElement>
    {
        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            base.OnInit(e);
        }

        private void LoadUserControls()
        {
            if (this.PageTypeElement.DataSource is PageElementEntity && this.PageTypeElementDataSourceAsPageElementEntity.PageEntity != null)
            {
                PageElementEntity pageElementEntity = this.PageTypeElementDataSourceAsPageElementEntity;

                this.cbPageId.DataSource = this.GetPageCollection(pageElementEntity.PageEntity.SiteId, pageElementEntity.PageId);
                this.cbPageId.DataBind();
            }
        }

        private PageCollection GetPageCollection(int siteId, int pageId)
        {
            PredicateExpression filter = new PredicateExpression(PageFields.SiteId == siteId);
            filter.Add(PageFields.Visible == true);

            if (pageId > 0)
            {
                filter.Add(PageFields.PageId != pageId);
            }

            PageCollection pages = new PageCollection();
            pages.GetMulti(filter);

            return pages;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.lblPageTypeElementName.Text = this.PageTypeElement.Name;
        }

        protected override void DataBindPageTypeElement()
        {
            this.PageTypeElement.PageId = this.cbPageId.ValidId;
        }

        protected override void DataBindControls()
        {
            if (this.PageTypeElement.PageId > 0)
                this.cbPageId.Value = this.PageTypeElement.PageId;
        }
    }
}