﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Cms.SubPanels.PageTypeElementEditors.MultiLineTextEditor" Codebehind="MultiLineTextEditor.ascx.cs" %>
                        <tr>
							<td class="label">
								<D:Label runat="server" ID="lblPageTypeElementName" LocalizeText="false"></D:Label>
							</td>
							<td class="control" colspan="2">
								<D:TextBoxMultiLine ID="tbText" Rows="15" AutoGrow="true" runat="server" ClientIDMode="Static"></D:TextBoxMultiLine>
                                <small><a href="http://en.wikipedia.org/wiki/Markdown" target="_markdown">Markdown</a>-markup can be used. (bold: **bold**, italic: *italic*).</small>
							</td>
                            <td class="control">
                                <asp:Panel ID="markdownoutput" ClientIDMode="Static" class="markdown_preview markdown-body"  runat="server"/>
                            </td>
						</tr>					
<script type="text/javascript">
    parseMarkdown(document.getElementsByClassName("<%= tbText.FriendlyName %>")[0].value, '<%= markdownoutput.ClientID %>');
</script>