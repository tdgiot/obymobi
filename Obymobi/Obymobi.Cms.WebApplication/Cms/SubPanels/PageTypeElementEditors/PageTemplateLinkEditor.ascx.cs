﻿using Obymobi.Logic.Cms;
using System;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Cms.SubPanels.PageTypeElementEditors
{
    public partial class PageTemplateLinkEditor : PageTypeElementEditorBase<PageLinkElement>
    {
        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            base.OnInit(e);
        }

        private void LoadUserControls()
        {
            if (this.PageTypeElement.DataSource is PageTemplateElementEntity && this.PageTypeElementDataSourceAsPageTemplateElementEntity.PageTemplateEntity != null)
            {
                PageTemplateElementEntity pageTemplateElementEntity = this.PageTypeElementDataSourceAsPageTemplateElementEntity;

                this.cbPageTemplateId.DataSource = this.GetPageTemplateCollection(pageTemplateElementEntity.PageTemplateEntity.SiteTemplateId, pageTemplateElementEntity.PageTemplateId);
                this.cbPageTemplateId.DataBind();
            }
        }

        private PageTemplateCollection GetPageTemplateCollection(int siteTemplateId, int pageTemplateId)
        {
            PredicateExpression filter = new PredicateExpression(PageTemplateFields.SiteTemplateId == siteTemplateId);

            if (pageTemplateId > 0)
            {
                filter.Add(PageTemplateFields.PageTemplateId != pageTemplateId);
            }

            PageTemplateCollection pageTemplates = new PageTemplateCollection();
            pageTemplates.GetMulti(filter);

            return pageTemplates;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.lblPageTypeElementName.Text = this.PageTypeElement.Name;
        }

        protected override void DataBindPageTypeElement()
        {
            this.PageTypeElement.PageId = this.cbPageTemplateId.ValidId;
        }

        protected override void DataBindControls()
        {
            if (this.PageTypeElement.PageTemplateId > 0)
                this.cbPageTemplateId.Value = this.PageTypeElement.PageId;
        }
    }
}