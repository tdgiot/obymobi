﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Cms.SubPanels.PageTypeElementEditors.VenueEditor" Codebehind="VenueEditor.ascx.cs" %>
                        <tr>
							<td class="label">
								<D:Label runat="server" ID="lblPageTypeElementName" LocalizeText="false"></D:Label>
							</td>
							<td class="control subTable" colspan="3">
								<table>
                                    <tr>
                                        <td class="label"><D:LabelAssociated runat="server" ID="lblCompanyId">Company</D:LabelAssociated></td>
                                        <td class="control"><X:ComboBoxLLBLGenEntityCollection EntityName="Company" runat="server" ID="cbCompanyId" ValueField="CompanyId" UseDataBinding="false" PreventEntityCollectionInitialization="true"></X:ComboBoxLLBLGenEntityCollection></td>
                                        <td class="label"><D:LabelAssociated runat="server" ID="lblPointOfInterestId">Point of Interest</D:LabelAssociated></td>
                                        <td class="control"><X:ComboBoxLLBLGenEntityCollection EntityName="PointOfInterest" runat="server" ValueField="PointOfInterestId" ID="cbPointOfInterestId" UseDataBinding="false"></X:ComboBoxLLBLGenEntityCollection></td>
                                    </tr>                               
                                    <tr>
                                        <td class="label">&nbsp;</td>
                                        <td class="control">&nbsp;</td>
                                        <td class="label">&nbsp;</td>
                                        <td class="control">&nbsp;</td>
                                    </tr>
								</table>
							</td>
						</tr>					