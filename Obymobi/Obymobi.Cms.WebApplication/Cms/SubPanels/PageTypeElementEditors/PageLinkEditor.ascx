﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Cms.SubPanels.PageTypeElementEditors.PageLinkEditor" Codebehind="PageLinkEditor.ascx.cs" %>
                        <tr>
							<td class="label">
								<D:Label runat="server" ID="lblPageTypeElementName" LocalizeText="false"></D:Label>
							</td>
							<td class="control" colspan="3">
                                <X:ComboBoxLLBLGenEntityCollection EntityName="Page" runat="server" ID="cbPageId" ValueField="PageId" TextField="Name" UseDataBinding="false" PreventEntityCollectionInitialization="true"></X:ComboBoxLLBLGenEntityCollection>
							</td>
						</tr>					