﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Cms.SubPanels.PageTypeElementEditors.ImageEditorsPanel" Codebehind="ImageEditorsPanel.ascx.cs" %>                       
<%@ Reference VirtualPath="~/Cms/SubPanels/PageTypeElementEditors/ImageEditor.ascx" %>
<tr>
	<td class="label">
		<D:Label runat="server" ID="lblPageTypeElementName" LocalizeText="false"></D:Label>
	</td>
	<td class="control" colspan="3">                                								
        <D:PlaceHolder runat="server" ID="plhNoImage">
            <span class="nobold">
                <D:LinkButton runat="server" ID="hlUploadImage" Text="Afbeelding uploaden"></D:LinkButton>
            </span>
        </D:PlaceHolder> 								                               
	</td>                            
</tr>	                        
<D:PlaceHolder runat="server" ID="plhMedia"></D:PlaceHolder>
                        
                        