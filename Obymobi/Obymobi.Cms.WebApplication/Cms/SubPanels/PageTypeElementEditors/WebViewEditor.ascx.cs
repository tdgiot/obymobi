﻿using Obymobi.Logic.Cms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Obymobi.ObymobiCms.Cms.SubPanels.PageTypeElementEditors
{
    public partial class WebViewEditor : PageTypeElementEditorBase<WebViewElement>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.lblPageTypeElementName.Text = this.PageTypeElement.Name;
        }

        protected override void DataBindPageTypeElement()
        {
            this.PageTypeElement.Url = this.tbText.Text;            
        }

        protected override void DataBindControls()
        {
            this.tbText.Text = this.PageTypeElement.Url;
        }
    }
}