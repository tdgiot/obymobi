﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Cms.SubPanels.PageTypeElementEditors.MapEditor" Codebehind="MapEditor.ascx.cs" %>
                        <tr>
							<td class="label">
								<D:Label runat="server" ID="lblPageTypeElementName" LocalizeText="false"></D:Label>
							</td>
							<td class="control subTable" colspan="3">
								<table>
                                    <tr>
                                        <td class="label"><D:LabelAssociated runat="server" ID="lblMarkerPosition">Location</D:LabelAssociated></td>
                                        <td class="control" colspan="3"><D:TextBoxString runat="server" ID="tbMarkerPosition"></D:TextBoxString></td>
                                    </tr>
                                    <tr>
                                        <td class="label"><D:LabelAssociated runat="server" ID="lblMarkerLabel">Marker Label</D:LabelAssociated></td>
                                        <td class="control"><D:TextBoxString runat="server" ID="tbMarkerLabel"></D:TextBoxString></td>
                                        <td class="label"><D:LabelAssociated runat="server" ID="lblZoomLevel">Zoom Level</D:LabelAssociated></td>
                                        <td class="control"><X:ComboBoxInt runat="server" ID="cbZoomLevel"></X:ComboBoxInt></td>
                                    </tr>
                                    <tr>
                                        <td class="label"><D:LabelAssociated runat="server" ID="lblMarkerText">Marker Text</D:LabelAssociated></td>
                                        <td class="control" colspan="3"><D:TextBoxMultiLine ID="tbMarkerText" AutoGrow="true" runat="server"></D:TextBoxMultiLine></td>
                                    </tr>
                                    <tr>
                                        <td class="label">&nbsp;</td>
                                        <td class="control">&nbsp;</td>
                                        <td class="label">&nbsp;</td>
                                        <td class="control">&nbsp;</td>
                                    </tr>
								</table>
							</td>
						</tr>					