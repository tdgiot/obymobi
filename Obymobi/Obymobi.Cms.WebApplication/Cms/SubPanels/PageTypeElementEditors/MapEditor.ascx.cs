﻿using Obymobi.Logic.Cms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Obymobi.ObymobiCms.Cms.SubPanels.PageTypeElementEditors
{
    public partial class MapEditor : PageTypeElementEditorBase<MapElement>
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            for (int i = 1; i < 18; i++)
            {
                this.cbZoomLevel.Items.Add(i.ToString(), i);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.lblPageTypeElementName.Text = this.PageTypeElement.Name;
        }

        protected override void DataBindPageTypeElement()
        {
            this.PageTypeElement.MarkerPosition = this.tbMarkerPosition.Text;
            this.PageTypeElement.MarkerLabel = this.tbMarkerLabel.Text;
            this.PageTypeElement.MarkerText = this.tbMarkerText.Text;
            this.PageTypeElement.ZoomLevel = this.cbZoomLevel.ValidId;
        }

        protected override void DataBindControls()
        {
            this.tbMarkerPosition.Text = this.PageTypeElement.MarkerPosition;
            this.tbMarkerLabel.Text = this.PageTypeElement.MarkerLabel;
            this.tbMarkerText.Text = this.PageTypeElement.MarkerText;
            this.cbZoomLevel.Value = this.PageTypeElement.ZoomLevel;

        }
    }
}