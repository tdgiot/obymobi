﻿using Dionysos;
using Obymobi.Logic.Cms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;

namespace Obymobi.ObymobiCms.Cms.SubPanels.PageTypeElementEditors
{
    public partial class MultiLineTextEditor : PageTypeElementEditorBase<MultiLineTextElement>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.lblPageTypeElementName.Text = this.PageTypeElement.Name;
        }

        protected override void DataBindPageTypeElement()
        {            
            this.PageTypeElement.Text = this.tbText.Text;
        }

        protected override void DataBindControls()
        {
            if (this.pageTypeElement.DataSource is PageTemplateElementEntity)
            {
                this.markdownoutput.ID += "_" + ((PageTemplateElementEntity)this.PageTypeElement.DataSource).CultureCode;
                this.tbText.FriendlyName = "tbText_" + ((PageTemplateElementEntity)this.PageTypeElement.DataSource).CultureCode;
                this.tbText.CssClass += " tbText_" + ((PageTemplateElementEntity)this.PageTypeElement.DataSource).CultureCode;
            }
            else
            {
                this.markdownoutput.ID += "_" + ((PageElementEntity)this.PageTypeElement.DataSource).CultureCode;
                this.tbText.FriendlyName = "tbText_" + ((PageElementEntity)this.PageTypeElement.DataSource).CultureCode;
                this.tbText.CssClass += " tbText_" + ((PageElementEntity)this.PageTypeElement.DataSource).CultureCode;
            }
            
            this.tbText.Attributes["onkeyup"] += "parseMarkdown(this.value, \"" + this.markdownoutput.ID + "\");";
            
            this.tbText.Text = this.PageTypeElement.Text;
            
        }
    }
}