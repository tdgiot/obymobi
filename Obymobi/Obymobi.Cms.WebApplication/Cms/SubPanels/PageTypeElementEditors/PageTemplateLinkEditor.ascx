﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Cms.SubPanels.PageTypeElementEditors.PageTemplateLinkEditor" Codebehind="PageTemplateLinkEditor.ascx.cs" %>
                        <tr>
							<td class="label">
								<D:Label runat="server" ID="lblPageTypeElementName" LocalizeText="false"></D:Label>
							</td>
							<td class="control" colspan="3">
                                <X:ComboBoxLLBLGenEntityCollection EntityName="PageTemplate" runat="server" ID="cbPageTemplateId" ValueField="PageTemplateId" TextField="Name" UseDataBinding="false" PreventEntityCollectionInitialization="true"></X:ComboBoxLLBLGenEntityCollection>
							</td>
						</tr>					