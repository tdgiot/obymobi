﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Cms.SubPanels.PageTypeElementEditors.YouTubeEditor" Codebehind="YouTubeEditor.ascx.cs" %>
<tr>
    <td class="label">
        <D:Label runat="server" ID="lblPageTypeElementName" LocalizeText="false"></D:Label>
    </td>
    <td class="control subTable" colspan="3">
        <table>
            <tr>
                <td class="label">
                    <D:LabelAssociated runat="server" ID="lblVideoId" LocalizeText="false">Video Id</D:LabelAssociated>
                </td>
                <td class="control" colspan="3">
                    <D:TextBoxString ID="tbVideoId" runat="server"></D:TextBoxString>
                </td>
            </tr>
            <tr>
                <td class="label">
                    <D:LabelAssociated runat="server" ID="lblAutoPlay">Direct afspelen</D:LabelAssociated></td>
                <td class="control">
                    <D:CheckBox runat="server" ID="cbAutoPlay" LocalizeText="false" /></td>
                <td class="label">
                    <D:LabelAssociated runat="server" ID="lblAutoHide">Knoppen verbergen</D:LabelAssociated></td>
                <td class="control">
                    <D:CheckBox runat="server" ID="cbAutoHide" LocalizeText="false" /></td>
            </tr>            
                <!--
            <tr>                
                <td class="label">
                    <D:LabelAssociated runat="server" ID="lblClosedCaptions">Closed Captions</D:LabelAssociated></td>
                <td class="control">
                    <D:CheckBox runat="server" ID="cbClosedCaptions" LocalizeText="false" /></td>
                <td class="label">
                    <D:LabelAssociated runat="server" ID="lblLoop">Loop</D:LabelAssociated></td>
                <td class="control">
                    <D:CheckBox runat="server" ID="cbLoop" LocalizeText="false" /></td>
            </tr>
                -->            
        </table>
    </td>
</tr>
