﻿using Obymobi.Logic.Cms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dionysos;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Interfaces;
using Image = Dionysos.Web.UI.WebControls.Image;
using Label = Dionysos.Web.UI.WebControls.Label;
using HyperLink = Dionysos.Web.UI.WebControls.HyperLink;
using Dionysos.Web;
using Obymobi.Enums;

namespace Obymobi.ObymobiCms.Cms.SubPanels.PageTypeElementEditors
{
    public partial class ImageEditor : System.Web.UI.UserControl
    {
        #region Methods

        private void DeleteImage(MediaEntity media)
        {
            this.PageAsPageEntity.Validate();
            if (this.PageAsPageEntity.IsValid && this.PageAsPageEntity.Save())
            {
                media.Delete();
            }
        }

        private void RenderImage(Image image, Label label, HyperLink edit, HyperLink delete, MediaEntity media)
        {
            var url = this.PageAsPageEntity.Request.Url.AbsoluteUri;
            int startIndex = url.IndexOf("management") + 10;                        
            var pageUrl = url.Substring(startIndex, url.IndexOf(".aspx") - startIndex);
            
            string returnUrl = this.ResolveUrl("~{0}.aspx?id={1}&tabToActivate=editor-agnostic".FormatSafe(pageUrl, this.PageAsPageEntity.EntityId));

            string deleteUrl = this.ResolveUrl("~/Generic/Media.aspx?mode=delete&id={0}&ReturnUrl={1}".FormatSafe(media.MediaId, returnUrl));            
            string imageUrl = this.ResolveUrl("~/Generic/ThumbnailGenerator.ashx?mediaId={0}&width={1}&height={2}&fitInDimensions=true".FormatSafe(media.MediaId, 400, 150));
            string countriesString = media.MediaCountriesString;

            string editUrl = this.ResolveUrl("~/Generic/Media.aspx?id={0}".FormatSafe(media.MediaId));

            if (this.exclusiveMediaType != MediaType.NoMediaTypeSpecified)
                editUrl += string.Format("&MediaType={0}", (int)this.exclusiveMediaType);
            
            edit.NavigateUrl = editUrl;
            delete.NavigateUrl = deleteUrl;
            image.ImageUrl = imageUrl;
            label.Text = countriesString.IsNullOrWhiteSpace() ? "Agnostic media" : countriesString;
        }

        private void RenderImages()
        {           
            if (this.MediaCollection.Count > 0)
            {
                // Render first image
                this.RenderImage(this.imgFirstThumbnail, this.lblFirstMediaLanguages, this.hlFirstEditImage, this.hlFirstDeleteImage, this.mediaCollection[0]);
            }

            if (this.mediaCollection.Count > 1)
            {
                // Render second image
                this.RenderImage(this.imgSecondThumbnail, this.lblSecondMediaLanguages, this.hlSecondEditImage, this.hlSecondDeleteImage, this.mediaCollection[1]);
            }
            else
            {
                this.plhImage.Visible = false;
            }
        }

        #endregion

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            string presubmitWarning = this.PageAsPageEntity.Translate("AreYouSureYouWantToDeleteYourImage", "Weet u zeker dat u de afbeelding wilt verwijderen?");;
        }        
       
        void hlFirstDeleteImage_Click(object sender, EventArgs e)
        {            
            this.DeleteImage(this.mediaCollection[0]);
        }

        void hlSecondDeleteImage_Click(object sender, EventArgs e)
        {
            this.DeleteImage(this.mediaCollection[1]);
        }

        #endregion      
  
        #region Properties

        protected MediaCollection mediaCollection;

        public MediaCollection MediaCollection
        {
            get
            {
                return this.mediaCollection;
            }
            set
            {
                this.mediaCollection = value;
                this.RenderImages();
            }
        }

        protected Dionysos.Web.UI.PageEntity PageAsPageEntity
        {
            get
            {
                return (Dionysos.Web.UI.PageEntity)this.Page;
            }
        }

        private MediaType exclusiveMediaType = MediaType.NoMediaTypeSpecified;

        public MediaType ExclusiveMediaType
        {
            set
            {
                this.exclusiveMediaType = value;
            }
        }

        #endregion
    }
}