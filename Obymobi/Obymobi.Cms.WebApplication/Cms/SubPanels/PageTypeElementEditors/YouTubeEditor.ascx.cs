﻿using Obymobi.Logic.Cms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Obymobi.ObymobiCms.Cms.SubPanels.PageTypeElementEditors
{
    public partial class YouTubeEditor : PageTypeElementEditorBase<YouTubeElement>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.lblPageTypeElementName.Text = this.PageTypeElement.Name;
        }

        protected override void DataBindPageTypeElement()
        {
            this.PageTypeElement.VideoId = this.tbVideoId.Text;
            this.PageTypeElement.AutoHide = this.cbAutoHide.Checked;
            this.PageTypeElement.Loop = this.cbLoop.Checked;
            this.PageTypeElement.ClosedCaptions = this.cbClosedCaptions.Checked;
            this.PageTypeElement.AutoPlay = this.cbAutoPlay.Checked;
        }

        protected override void DataBindControls()
        {
            this.tbVideoId.Text= this.PageTypeElement.VideoId;
            this.cbAutoHide.Checked = this.PageTypeElement.AutoHide;
            this.cbLoop.Checked = this.PageTypeElement.Loop;
            this.cbClosedCaptions.Checked = this.PageTypeElement.ClosedCaptions;
            this.cbAutoPlay.Checked = this.PageTypeElement.AutoPlay;            
        }
    }
}