﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using Dionysos;
using Dionysos.Web.UI.WebControls;
using Obymobi.ObymobiCms.Cms.SubPanels;
using Obymobi.ObymobiCms.UI;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Cms;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Cms
{
    public partial class PageTemplate : PageLLBLGenEntityCms
    {
        #region Fields
        
        private bool hasLanguageSpecificContent;
        
        private SubPanels.PageTypeElementsCollectionPanel pageTypeElementsCollectionPanel;

        private FastDictionary<string, System.Web.UI.Control> cultureControls = new FastDictionary<string, System.Web.UI.Control>();

        #endregion

        #region Methods

        protected override void OnInit(EventArgs e)
		{
            this.RetrieveSiteTemplateCultures();
            this.LoadUserControls();
            this.DataSourceLoaded += Page_DataSourceLoaded;
			base.OnInit(e);			
		}

        private void RetrieveSiteTemplateCultures()
        { 
            int pageTemplateId;
            if (Dionysos.Web.QueryStringHelper.GetInt("id", out pageTemplateId))
            {
                PredicateExpression filter = new PredicateExpression(PageTemplateFields.PageTemplateId == pageTemplateId);

                PrefetchPath prefetch = new PrefetchPath(EntityType.SiteTemplateCultureEntity);                

                RelationCollection relations = new RelationCollection();
                relations.Add(SiteTemplateCultureEntityBase.Relations.SiteTemplateEntityUsingSiteTemplateId);
                relations.Add(SiteTemplateEntityBase.Relations.PageTemplateEntityUsingSiteTemplateId);

                SiteTemplateCultureCollection siteTemplateCultures = new SiteTemplateCultureCollection();
                siteTemplateCultures.GetMulti(filter, 0, null, relations, prefetch);

                this.siteTemplateCultures = siteTemplateCultures;                
            }
        }

        private void LoadUserControls()
        {
            this.pageTypeElementsCollectionPanel = this.tabsMain.AddTabPage(this.Translate("Agnostic", "Taal Onafh."), "editor-agnostic", "~/Cms/SubPanels/PageTypeElementsCollectionPanel.ascx") as SubPanels.PageTypeElementsCollectionPanel;
            if (this.siteTemplateCultures != null)
            {
                foreach (SiteTemplateCultureEntity siteTemplateCulture in this.siteTemplateCultures)
                {
                    Control control = this.tabsMain.AddTabPage(siteTemplateCulture.CultureCode.ToUpperInvariant(), "editor_" + siteTemplateCulture.CultureCode, "~/Cms/SubPanels/PageTypeElementsCollectionPanel.ascx");
                    this.cultureControls.Add(siteTemplateCulture.CultureCode, control);
                }
            }

            if (this.PageMode != Dionysos.Web.PageMode.Add)
            {
                this.tabsMain.AddTabPage("Bijlagen", "Attachments", "~/Generic/SubPanels/AttachmentCollection.ascx");   
            }
            this.tabsMain.AddTabPage("Afbeeldingen", "Media", "~/Generic/UserControls/MediaCollection.ascx");
        }

        private void SetGui()
        {
            this.lblPageTypeText.Text = PageTypeHelper.GetPageTypeName(this.DataSourceTyped.PageType.ToEnum<PageType>());

            PageTypeBase pageType = PageTypeHelper.GetPageTypeInstance(this.DataSourceTyped.PageType.ToEnum<PageType>());
            pageType.PageElementsCollection = this.DataSourceTyped.PageTemplateElementCollection;

            // Show Language Agnostic Checkboxlist
            foreach (var pageTypeElement in pageType.PageTypeElements)
            {
                if (!pageTypeElement.CmsEditable)
                    continue;
                
                ListItem li = new ListItem(pageTypeElement.Name, pageTypeElement.SystemName);
                li.Selected = this.DataSourceTyped.PageTemplateElementCollection.Any(x => x.SystemName == li.Value && x.CultureCode.IsNullOrWhiteSpace());
                if (!li.Selected)
                {
                    this.hasLanguageSpecificContent = true;
                }
                this.cblLanguageAgnosticPageElements.Items.Add(li);
            }

            // Render attached pages
            var pageFilter = new PredicateExpression(PageFields.PageTemplateId == this.DataSourceTyped.PageTemplateId);

            var relations = new RelationCollection();
            relations.Add(PageEntityBase.Relations.SiteEntityUsingSiteId);

            var sort = new SortExpression(SiteFields.Name | SortOperator.Ascending);
            sort.Add(PageFields.Name | SortOperator.Ascending);

            var pages = new PageCollection();
            pages.GetMulti(pageFilter, 0, sort, relations, null, null, 0, 0);

            if (pages.Count > 0)
            {
                var tabForPages = this.tabsMain.TabPages.FindByName("tabForPages");
                if (tabForPages != null)
                {
                    foreach (var page in pages)
                    {
                        this.plhPages.AddHtml("<tr><td><a href=\"{0}\" target=\"_blank\">{1}</a></td></tr>", this.ResolveUrl("~/Cms/Page.aspx?Id=" + page.PageId), page.SiteNamePageName);
                    }

                    tabForPages.Visible = true;
                }
            }
        }

        private void LoadEditors()
        { 
            // If there are any Language Agnostic Elements, add that tab first
            if (this.DataSourceTyped.PageTemplateElementCollection.Any(x => x.CultureCode.IsNullOrWhiteSpace()))
            {
                this.pageTypeElementsCollectionPanel.DataSource = this.DataSourceTyped;
                this.pageTypeElementsCollectionPanel.CultureCode = null;
            }            
    
            foreach (string cultureCode in this.cultureControls.Keys)
            {
                if (!this.hasLanguageSpecificContent)
                {
                    // hide the tab since we dont have anything to show on it
                    SiteTemplateCultureEntity siteTemplateCulture = this.siteTemplateCultures.SingleOrDefault(stc => stc.CultureCode.Equals(cultureCode, StringComparison.InvariantCultureIgnoreCase));
                    if (siteTemplateCulture != null)
                    {
                        TabPage languageTab = this.tabsMain.TabPages.FindByName("editor_" + siteTemplateCulture.CultureCode);
                        if (languageTab != null)
                        {
                            this.tabsMain.TabPages.Remove(languageTab);
                        }
                    }                    
                }
                else
                {
                    // Load a panel for each language that was enabled on the Site                    
                    PageTypeElementsCollectionPanel editor = (SubPanels.PageTypeElementsCollectionPanel)this.cultureControls[cultureCode];
                    editor.CultureCode = cultureCode;
                    editor.DataSource = this.DataSourceTyped;
                }
            }
            
            // Load the Editors for the translations of the Page.Name
            foreach (SiteTemplateCultureEntity siteTemplateCulture in this.DataSourceTyped.SiteTemplateEntity.SiteTemplateCultureCollection)
            {
                this.plhNameTranslations.AddHtml("<tr><td class=\"label\">{0}</td><td class=\"control\">", Obymobi.Culture.Mappings[siteTemplateCulture.CultureCode].NameAndCultureCode);

                TextBoxString tb = new TextBoxString();
                tb.ID = "tbName_" + siteTemplateCulture.CultureCode;

                CustomTextRenderer renderer = new CustomTextRenderer(this.DataSourceTyped, siteTemplateCulture.CultureCode);
                renderer.RenderCustomText(tb, CustomTextType.PageTemplateName);
                
                this.plhNameTranslations.Controls.Add(tb);
                this.plhNameTranslations.AddHtml("</td></tr>");
            }            
        }      

        public override bool Save()
        {
            bool toReturn = false;
            if (base.Save())
            {
                toReturn = true;

                // Save Language Agnostic stuff
                PageTypeBase pageType = PageTypeHelper.GetPageTypeInstance(this.DataSourceTyped.PageType.ToEnum<PageType>());
                foreach (ListItem item in this.cblLanguageAgnosticPageElements.Items)
                {
                    // Get related PageTypeElement
                    PageTypeElement pageTypeElement = pageType.PageTypeElements.FirstOrDefault(x => x.SystemName == item.Value);
                    if (pageTypeElement == null)
                        throw new Exception("Page Type Elements were changed during post back, missing: " + item.Value);

                    PageTemplateElementEntity languageAgnosticEntity = this.DataSourceTyped.PageTemplateElementCollection.FirstOrDefault(x => x.SystemName == item.Value && x.CultureCode.IsNullOrWhiteSpace());
                    List<PageTemplateElementEntity> languageSpecificEntities = this.DataSourceTyped.PageTemplateElementCollection.Where(x => x.SystemName == item.Value && !x.CultureCode.IsNullOrWhiteSpace()).ToList();
                    
                    if (item.Selected)
                    {
                        // It becomes (or is) language agnostic
                        // Delete any Language Specific items
                        for (int i = 0; i < languageSpecificEntities.Count; i++)
                        {
                            languageSpecificEntities[i].Delete();
                            this.DataSourceTyped.PageTemplateElementCollection.Remove(languageSpecificEntities[i]);                                               
                        }

                        if (languageAgnosticEntity == null)
                        {
                            PageTemplateElementEntity pageTemplateElementEntity = new PageTemplateElementEntity();
                            pageTemplateElementEntity.PageTemplateId = this.DataSourceTyped.PageTemplateId;
                            pageTemplateElementEntity.SystemName = pageTypeElement.SystemName;
                            pageTemplateElementEntity.CultureCode = null;
                            pageTemplateElementEntity.PageElementType = (int)pageTypeElement.PageTypeElementType;
                            pageTemplateElementEntity.Save();

                            this.DataSourceTyped.PageTemplateElementCollection.Add(pageTemplateElementEntity);
                            pageTypeElement.DataSource = pageTemplateElementEntity;
                        }
                    }
                    else
                    { 
                        // This is not (or no longer) language agnostic, remove the language agnostic content if it was created                        
                        if (languageAgnosticEntity != null)
                        {
                            languageAgnosticEntity.Delete();
                            this.DataSourceTyped.PageTemplateElementCollection.Remove(languageAgnosticEntity);
                        }                        
                    }
                }   
             
                // Save the 'Page.Name' translations
                foreach (SiteTemplateCultureEntity siteTemplateCulture in this.DataSourceTyped.SiteTemplateEntity.SiteTemplateCultureCollection)
                {                    
                    TextBoxString tb = this.plhNameTranslations.FindControl("tbName_" + siteTemplateCulture.CultureCode) as TextBoxString;
                    if (tb != null)
                    {
                        CustomTextRenderer renderer = new CustomTextRenderer(this.DataSourceTyped, siteTemplateCulture.CultureCode);
                        renderer.SaveCustomText(tb, CustomTextType.PageTemplateName);
                    }
                }

                this.pageTypeElementsCollectionPanel.DataSource = this.DataSourceTyped;

                foreach (string cultureCode in this.cultureControls.Keys)
                {
                    if (this.hasLanguageSpecificContent)
                    {
                        PageTypeElementsCollectionPanel editor = (SubPanels.PageTypeElementsCollectionPanel)this.cultureControls[cultureCode];
                        editor.CultureCode = cultureCode;
                        editor.DataSource = this.DataSourceTyped;
                    }
                }
            }

            return toReturn;
        }

        private void HookUpEvents()
        {
            
        }        

        #endregion

		#region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            bool addMode = (this.PageMode == Dionysos.Web.PageMode.Add);
            if (!addMode)
            {
                if (this.DataSourceTyped.SiteTemplateEntity.SiteTemplateCultureCollection.Count == 0)
                {
                    this.lblPageNameTranslations.Visible = false;
                    this.AddInformatorInfo(this.Translate("NoLanguagesHaveBeenChosen", "Er zijn voor de site template waar deze pagina deel van uit maakt nog geen talen ingesteld."));
                    this.Validate();
                }                
            }

            this.HookUpEvents();
        }

        private void Page_DataSourceLoaded(object sender)
        {
            this.SetGui();
            this.LoadEditors();
        }       

		#endregion

		#region Properties

        public PageTemplateEntity DataSourceTyped
        {
            get
            {
                return this.DataSource as PageTemplateEntity;
            }
        }

        private SiteTemplateCultureCollection siteTemplateCultures = null;        

		#endregion
    }
}
