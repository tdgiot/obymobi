﻿<%@ Page Title="Translation management" Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Cms.SiteManagement"  Codebehind="SiteManagement.aspx.cs" %>
<%@ Reference VirtualPath="~/Generic/SubPanels/Cms/CopySitePagesPanel.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitleHolder" Runat="Server">
    Site management
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhToolbarHolder" Runat="Server">
    <span class="toolbar">
        <X:ToolBarButton runat="server" ID="tbbRefresh" CommandName="Refresh" Text="Verversen" PreSubmitWarning="Are you sure you want to refresh this page?" Image-Url="~/Images/Icons/arrow_refresh.png" />
    </span>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhContentHolder" Runat="Server">
<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true"/>
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%" style="min-width: 900px;">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<controls>
					<D:PlaceHolder runat="server">
					<table class="dataformV2">
					    <tr>
					        <td class="label">
					            <D:Label id="lblSiteId" runat="server">Site</D:Label>
					        </td>
                            <td class="control">
                                <X:ComboBoxLLBLGenEntityCollection ID="ddlSiteId" runat="server" EntityName="Site" TextField="Name" PreventEntityCollectionInitialization="true" notdirty="true">
                                    <ClientSideEvents SelectedIndexChanged="function(s, e) { cbSortingOptions.PerformCallback('1'); cbSiteTemplateToCopy.PerformCallback('1'); }" />
                                </X:ComboBoxLLBLGenEntityCollection>
                            </td>
                            <td class="label">
								<D:LabelAssociated runat="server" id="lblSiteTemplateToCopy">Site template</D:LabelAssociated>
							</td>
							<td class="control">
                                <div>
								    <div style="float:left;"><X:ComboBoxInt runat="server" ID="cbSiteTemplateToCopy" ClientInstanceName="cbSiteTemplateToCopy" OnCallback="cbSiteTemplateToCopy_OnCallback" notdirty="true" TextField="Name" ValueField="SiteTemplateId" Width="250px"></X:ComboBoxInt></div>
                                    <div style="float:left; margin-left: 5px;"><D:Button runat="server" ID="btCopySiteTemplate" Text="Kopieer pagina's van site template" UseSubmitBehavior="false"/></div>
                                    <label id="lblSiteShouldntHavePages" runat="server" style="float: left; margin-top: 6px;"><small>Note: This option is only available when the selected site doesn't have any pages configured.</small></label>
                                </div>
							</td>
					    </tr>                                                
						<tr>
						    <td class="label"></td>
                            <td class="control"></td>
							<td class="label">
								<D:LabelAssociated runat="server" id="lblExtractSiteTemplateName">Template naam</D:LabelAssociated>
							</td>
							<td class="control">
                                <div>
								    <div style="float:left;"><D:TextBox runat="server" ID="tbExtractSiteTemplateName" notdirty="true" Width="250px"></D:TextBox></div>
                                    <div style="float:left; margin-left: 5px;"><D:Button runat="server" ID="btExtractSiteTemplate" Text="Maak een site template van deze site" UseSubmitBehavior="false" /></div>
                                </div>
							</td>
                        </tr>
                        <tr>
                            <td class="label"></td>
                            <td class="control"></td>
							<td class="label">
								<D:LabelAssociated runat="server" id="lblCopySiteName">Site naam</D:LabelAssociated>
							</td>
							<td class="control">
                                <div>
								    <div style="float:left;"><D:TextBox runat="server" ID="tbCopySiteName" notdirty="true" Width="250px"></D:TextBox></div>
                                    <div style="float:left; margin-left: 5px;"><D:Button runat="server" ID="btCopySite" Text="Maak een kopie van deze site" UseSubmitBehavior="false" /></div>
                                </div>
							</td>
                        </tr>                        
                        <D:PlaceHolder runat="server" ID="plhDeleteCulture">
                        <tr>
                            <td class="label"></td>
                            <td class="control"></td>
							<td class="label">
								<D:LabelAssociated runat="server" id="lblDeleteCulture">Delete culture</D:LabelAssociated>
							</td>
							<td class="control">
                                <div>
								    <div style="float:left;"><X:ComboBox runat="server" ID="cbDeleteCulture" Width="250px" notdirty="true" /></div>
                                    <div style="float:left; margin-left: 5px;"><D:Button runat="server" ID="btDeleteCulture" Text="Delete" UseSubmitBehavior="false" /></div>
                                </div>
							</td>
                        </tr>
                        </D:PlaceHolder>
                        <tr>
                            <td class="label"></td>
                            <td class="control"></td>
							<td class="label">
								<D:LabelAssociated runat="server" id="lblSortPages">Pagina's sorteren</D:LabelAssociated>
							</td>
							<td class="control">
                                <div>
								    <div style="float:left;"><X:ComboBoxInt runat="server" EntityName="Language" ID="cbSortingOptions" ClientInstanceName="cbSortingOptions" OnCallback="cbSortingOptions_OnCallback" Width="250px" notdirty="true" /></div>
                                    <div style="float:left; margin-left: 5px;"><D:Button runat="server" ID="btnSortPages" Text="Pagina's sorteren" UseSubmitBehavior="false" /></div>
                                </div>
							</td>
                        </tr>
                    </table>
                    </D:PlaceHolder>
                </controls>
            </X:TabPage>            
            <X:TabPage Text="Copy pages" Name="Copy pages">
				<controls>
					<D:PlaceHolder ID="plhCopySitePages" runat="server">                        
                    </D:PlaceHolder>
                </controls>
            </X:TabPage>
        </TabPages>
    </X:PageControl>
</div>
</asp:Content>

