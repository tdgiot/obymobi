﻿using System;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;

namespace Obymobi.ObymobiCms.Cms
{
	public partial class Sites : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        #region Event handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            var datasource = this.DataSource as LLBLGenProDataSource;
            if (datasource != null)
            {
                var filter = new PredicateExpression(SiteFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

                if (CmsSessionHelper.CurrentRole >= Role.Administrator)
                    filter.AddWithOr(SiteFields.CompanyId == DBNull.Value);

                datasource.FilterToUse = filter;
            }
        }

        #endregion
    }
}
