﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using DevExpress.Web;
using Dionysos;
using Dionysos.Web.UI;
using Obymobi.Cms.Logic.Sites;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.ObymobiCms.Generic.SubPanels.Cms;
using Obymobi.Web.Media;
using SD.LLBLGen.Pro.ORMSupportClasses;
using PageEntity = Obymobi.Data.EntityClasses.PageEntity;
using SiteHelper = Obymobi.Cms.Logic.Sites.SiteHelper;
using SiteTemplateHelper = Obymobi.Cms.Logic.Sites.SiteTemplateHelper;

namespace Obymobi.ObymobiCms.Cms
{
    public partial class SiteManagement : PageDefault
    {
        #region Methods

        private void LoadUserControls()
        {
            IEnumerable<Obymobi.Culture> cultures = Obymobi.Culture.Mappings.Values.OrderBy(x => x.ToString());
            foreach (Obymobi.Culture culture in cultures)
            {
                this.cbDeleteCulture.Items.Add(culture.ToString(), culture.Code);
            }

            // Sites ddl
            var filter = new PredicateExpression(SiteFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            if (CmsSessionHelper.CurrentRole >= Role.Administrator)
                filter.AddWithOr(SiteFields.CompanyId == DBNull.Value);

            var includeFieldsList = new IncludeFieldsList();
            includeFieldsList.Add(SiteFields.Name);

            var sort = new SortExpression(new SortClause(SiteFields.Name, SortOperator.Ascending));

            var sites = new SiteCollection();
            sites.GetMulti(filter, 0, sort, null, null, includeFieldsList, 0, 0);

            this.ddlSiteId.DataSource = sites;
            this.ddlSiteId.DataBind();

            // Copy site panel
            CopySitePagesPanel copySitePagesPanel = (CopySitePagesPanel)this.LoadControl("~/Generic/SubPanels/Cms/CopySitePagesPanel.ascx");
            this.plhCopySitePages.Controls.Add(copySitePagesPanel);
        }

        private void SetGui()
        {
            
        }

        private new void Validate()
        {
            SiteEntity site;
            if (!this.TryGetSite(out site))
                this.MultiValidatorDefault.AddError(this.Translate("Error.SiteRequired", "Er dient een site geselecteerd te zijn."));

            base.Validate();
        }

        private void GenerateSubPages(PageEntity page, int level = 0)
        {
            if (page.PageCollection.Count <= 1)
                return;

            var pageName = string.Empty;
            for (int i = 0; i < level; i++) pageName += "- ";

            pageName += page.Name;
            cbSortingOptions.Items.Add(pageName, page.PageId);

            level++;
            foreach (var child in page.PageCollection.OrderBy(x => x.SortOrder))
            {
                GenerateSubPages(child, level);
            }
        }

        private void HookupEvents()
        {
            this.btCopySite.Click += btCopySite_Click;
            this.btCopySiteTemplate.Click += btCopySiteTemplate_Click;
            this.btExtractSiteTemplate.Click += btExtractSiteTemplate_Click;            
            this.btDeleteCulture.Click += BtDeleteCulture_Click;
            this.btnSortPages.Click += btnSortPages_Click;            
        }        

        public void Refresh()
        {
            Response.Redirect(Request.RawUrl);
        }

        private void SortPageCollection(IEnumerable<PageEntity> pageCollection, bool sortChildPages, ITransaction transaction)
        {
            int sortOrder = 1;
            foreach (var page in pageCollection.OrderBy(x => x.Name))
            {
                page.AddToTransaction(transaction);
                page.SortOrder = sortOrder;
                page.Save();

                sortOrder++;

                if (!sortChildPages || page.PageCollection.Count == 0)
                    continue;

                SortPageCollection(page.PageCollection, true, transaction);
            }
        }

        private bool TryGetSite(out SiteEntity site)
        {
            bool toReturn = false;
            if (this.ddlSiteId.ValidId <= 0)
            {
                site = null;
            }
            else
            {
                site = SiteHelper.GetSite(this.ddlSiteId.ValidId, true);
                toReturn = true;
            }
            return toReturn;
        }

        #endregion

        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            base.OnInit(e);
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            this.SetGui();
        }        

        protected void Page_Load(object sender, EventArgs e)
        {
            this.HookupEvents();
        }

        protected void cbSortingOptions_OnCallback(object sender, CallbackEventArgsBase e)
        {
            SiteEntity site;
            if (this.TryGetSite(out site))
            {
                cbSortingOptions.Items.Clear();
                cbSortingOptions.Items.Add(this.Translate("Sort_Everything", "Everything"), -1);
                cbSortingOptions.Items.Add(this.Translate("Sort_Root", "Root categories"), -2);
                cbSortingOptions.Items.Add("------------------------------------------------------", -9999);

                foreach (var page in site.PageCollection.Where(x => !x.ParentPageId.HasValue).OrderBy(x => x.SortOrder))
                {
                    GenerateSubPages(page);
                }
            }                
        }

        protected void cbSiteTemplateToCopy_OnCallback(object sender, CallbackEventArgsBase e)
        {
            SiteEntity site;
            if (this.TryGetSite(out site))
            {
                bool hasSites = (site.PageCollection.Count > 0);
                if (!hasSites)
                {
                    var filter = new PredicateExpression(SiteTemplateFields.SiteType == site.SiteType);
                    var sort = new SortExpression(SiteTemplateFields.Name | SortOperator.Ascending);

                    var templates = new SiteTemplateCollection();
                    templates.GetMulti(filter, 0, sort);

                    this.cbSiteTemplateToCopy.DataSource = templates;
                    this.cbSiteTemplateToCopy.DataBind();
                }        
            }               
        }

        private void btnSortPages_Click(object sender, EventArgs e)
        {
            if (this.cbSortingOptions.ValidId == -9999 || !this.cbSortingOptions.Value.HasValue)
                return;

            this.Validate();

            SiteEntity site;
            if (this.TryGetSite(out site))
            {
                var transaction = new Transaction(IsolationLevel.ReadUncommitted, "PageSortOrdering");

                if (this.cbSortingOptions.Value.Value == -1) // Sort everything
                {
                    SortPageCollection(site.PageCollection.Where(x => !x.ParentPageId.HasValue), true, transaction);
                }
                else if (this.cbSortingOptions.Value.Value == -2) // Sort only root categories
                {
                    SortPageCollection(site.PageCollection.Where(x => !x.ParentPageId.HasValue), false, transaction);
                }
                else
                {
                    var pageEntity = site.PageCollection.FirstOrDefault(x => x.PageId == this.cbSortingOptions.ValidId);
                    if (pageEntity == null)
                        return;

                    SortPageCollection(pageEntity.PageCollection, true, transaction);
                }

                try
                {
                    transaction.Commit();

                    this.AddInformatorInfo(this.Translate("PagesSortedSuccessfull", "De pagina's zijn gesorteerd op naam.")); 
                }
                catch (Exception)
                {
                    transaction.Rollback();
                }
            }
        }        

        private void BtDeleteCulture_Click(object sender, EventArgs e)
        {
            this.Validate();

            SiteEntity site;
            if (this.TryGetSite(out site))
            {
                if (this.cbDeleteCulture.Value == null || this.cbDeleteCulture.Value.ToString().IsNullOrWhiteSpace())
                {
                    this.MultiValidatorDefault.AddError("Please select a culture first.");
                }
                else
                {
                    // User is pretty sure that he wants to get rid of this language, so we don't ask questions,
                    // we just delete.
                    Transaction transaction = new Transaction(System.Data.IsolationLevel.ReadUncommitted, "DeleteCulture-{0}-Site{1}".FormatSafe(this.cbDeleteCulture.Value.ToString(), site.SiteId));
                    try
                    {
                        // Delete all PageElements for this Language
                        RelationCollection joinPts = new RelationCollection();
                        joinPts.Add(PageElementEntityBase.Relations.PageEntityUsingPageId);

                        PredicateExpression filter = new PredicateExpression();
                        filter.Add(PageFields.SiteId == site.SiteId);
                        filter.Add(PageElementFields.CultureCode == this.cbDeleteCulture.Value.ToString());

                        PageElementCollection pes = new PageElementCollection();
                        pes.AddToTransaction(transaction);
                        pes.GetMulti(filter, joinPts);

                        for (int i = 0; i < pes.Count; i++)
                        {
                            pes.AddToTransaction(transaction);
                            pes[i].Delete();
                        }

                        // Delete all Page custom texts
                        filter = new PredicateExpression();
                        filter.Add(PageFields.SiteId == site.SiteId);
                        filter.Add(CustomTextFields.CultureCode == this.cbDeleteCulture.Value.ToString());

                        RelationCollection joinPls = new RelationCollection();
                        joinPls.Add(CustomTextEntityBase.Relations.PageEntityUsingPageId);

                        CustomTextCollection pageCustomTexts = new CustomTextCollection();
                        pageCustomTexts.AddToTransaction(transaction);
                        pageCustomTexts.GetMulti(filter, joinPls);

                        for (int i = 0; i < pageCustomTexts.Count; i++)
                        {
                            pageCustomTexts.AddToTransaction(transaction);
                            pageCustomTexts[i].Delete();
                        }

                        var siteCulture = site.SiteCultureCollection.FirstOrDefault(x => x.CultureCode == this.cbDeleteCulture.Value.ToString());
                        if (siteCulture != null)
                        {
                            siteCulture.AddToTransaction(transaction);
                            siteCulture.Delete();
                        }

                        transaction.Commit();

                        this.AddInformatorInfo("The culture is deleted successfully.");
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        this.MultiValidatorDefault.AddError(this.Translate("Error.ErrorDuringCultureDelete", "An error occurred while deleting the culture: ") + ex.Message);
                    }
                    finally
                    {
                        transaction.Dispose();
                    }
                }
                base.Validate();
            }            
        }

        private void btCopySiteTemplate_Click(object sender, EventArgs e)
        {
            this.Validate();

            SiteEntity site;
            if (this.TryGetSite(out site))
            {
                try
                {
                    if (this.cbSiteTemplateToCopy.ValidId <= 0)
                    {
                        this.MultiValidatorDefault.AddError(this.Translate("Error.SiteTemplateRequiredToCopy", "Er dient een Site Sjabloon te worden gekozen om de pagina's vanaf te kopieëren."));
                    }                    
                    else
                    {
                        SiteTemplateEntity siteTemplate = new SiteTemplateEntity(this.cbSiteTemplateToCopy.ValidId);

                        CopySiteTemplateRequest request = new CopySiteTemplateRequest
                        {
                            SiteTemplate = siteTemplate,
                            Site = site,
                            CopyMediaOnCdn = MediaCloudHelper.CopyMediaOnCdn
                        };
                        SiteTemplateHelper.CopySiteTemplateToSite(request);

                        this.AddInformatorInfo(this.Translate("SiteCreated", "De pagina's zijn succesvol gekopieerd van de site template naar de site."));
                    }
                }
                catch (Exception ex)
                {
                    this.MultiValidatorDefault.AddError(this.Translate("Error.ErrorDuringSiteTemplateToSiteCopy", "Er is een fout opgetreden tijdens het kopieëren van het Site Sjabloon naar de Site: ") + ex.Message);
                }

                base.Validate();
            }            
        }

        private void btCopySite_Click(object sender, EventArgs e)
        {
            this.Validate();

            SiteEntity site;
            if (this.TryGetSite(out site))
            {
                try
                {
                    if (this.tbCopySiteName.Text.IsNullOrWhiteSpace())
                    {
                        this.MultiValidatorDefault.AddError(this.Translate("Error.NameRequiredToCopySite", "Er dient een naam te worden ingevoerd voor het maken van een kopie."));
                    }                    
                    else
                    {
                        CopySiteRequest request = new CopySiteRequest
                        {
                            SiteId = site.SiteId,
                            SiteName = this.tbCopySiteName.Text,
                            CopyMediaOnCdn = MediaCloudHelper.CopyMediaOnCdn
                        };
                        SiteEntity newSite = SiteHelper.CopySite(request);

                        this.AddInformatorInfo(this.Translate("SiteCopied", "De Site kopie is gemaakt: ") + "<a href=\"{0}\">{1}</a>".FormatSafe(
                            this.ResolveUrl("~/Cms/Site.aspx?id={0}".FormatSafe(newSite.SiteId)), newSite.Name));
                    }
                }
                catch (Exception ex)
                {
                    this.MultiValidatorDefault.AddError(this.Translate("Error.ErrorDuringSiteCopy", "Er is een fout opgetreden tijdens het kopieëren van de Site: ") + ex.Message);
                }

                base.Validate();
            }            
        }

        private void btExtractSiteTemplate_Click(object sender, EventArgs e)
        {
            this.Validate();

            SiteEntity site;
            if (this.TryGetSite(out site))
            {
                try
                {
                    if (this.tbExtractSiteTemplateName.Text.IsNullOrWhiteSpace())
                        this.MultiValidatorDefault.AddError(this.Translate("Error.NameRequiredToExtractSiteTemplateFromSite", "Er dient een naam te worden ingevoerd voor het te maken Site Sjabloon."));
                    else
                    {
                        var siteTemplate = SiteTemplateHelper.CreateSiteTemplateFromSite(site, this.tbExtractSiteTemplateName.Text);

                        this.AddInformatorInfo(this.Translate("SiteTemplateExtracted", "Het Site Sjabloon is gemaakt: ") + "<a href=\"{0}\">{1}</a>".FormatSafe(
                            this.ResolveUrl("~/Generic/SiteTemplate.aspx?id={0}".FormatSafe(siteTemplate.SiteTemplateId)), siteTemplate.Name));
                    }
                }
                catch (Exception ex)
                {
                    this.MultiValidatorDefault.AddError(this.Translate("Error.ErrorDuringSiteTemplateExtract", "Er is een fout opgetreden tijdens het aanmaken van het Site Sjabloon: ") + ex.Message);
                }

                base.Validate();
            }            
        }

        #endregion
    }
}