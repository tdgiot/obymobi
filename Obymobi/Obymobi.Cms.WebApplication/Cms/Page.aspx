﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" ValidateRequest="false" Inherits="Obymobi.ObymobiCms.Cms.Page" Codebehind="Page.aspx.cs" %>
<%@ Reference VirtualPath="~/Cms/SubPanels/PageTypeElementsCollectionPanel.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
    <D:Button runat="server" ID="btUncoupleGenericPage" Text="Niet generiek" Visible="false" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">    
<div>
    <script type="text/javascript">
        function parseMarkdown(text, outputId) {
            document.getElementById(outputId).innerHTML = markdown.toHTML(text);
        }
    </script>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
                    <D:PlaceHolder runat="server">
					    <table class="dataformV2">
						    <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblName">Name</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
							    </td>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" ID="lblPageTemplateId">Page template</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection ID="ddlPageTemplateId" runat="server" IncrementalFilteringMode="StartsWith" EntityName="PageTemplate" ValueField="PageTemplateId" TextField="SiteNamePageTemplateName" PreventEntityCollectionInitialization="true"/>
                                </td>
						    </tr>	
						    <tr>
							    <td class="label">
								    <D:Label runat="server" id="lblVisible">Visible</D:Label>
							    </td>
							    <td class="control">
								    <D:CheckBox runat="server" ID="cbVisible"></D:CheckBox>
							    </td>
                                <td class="label">
                                    <D:Label runat="server" id="lblPageTypeLabel">Page Type</D:Label>
                                </td>
                                <td class="control">
                                    <D:Label runat="server" ID="lblPageTypeText" LocalizeText="false" UseDataBinding="false"></D:Label>
                                </td>							    
						    </tr>	
						    <tr>
							    <td class="label">
								   
							    </td>
							    <td class="control">								    
                                     <strong><D:LabelTextOnly runat="server" id="lblPageNameTranslations">'Naam' vertalingen</D:LabelTextOnly></strong>
							    </td>
                                <td class="label">
								    <D:Label runat="server" id="lblLanguageAgnostic">Taal onafhankelijk</D:Label>
							    </td>
							    <td class="control">
								    <D:CheckBoxList runat="server" ID="cblLanguageAgnosticPageElements"></D:CheckBoxList>
							    </td>
						    </tr>	                            
                            <D:PlaceHolder runat="server" ID="plhNameTranslations">
                            </D:PlaceHolder>
					    </table>
                    </D:PlaceHolder>
				</controls>
			</X:TabPage>          
        </TabPages>
	</X:PageControl>
</div>
</asp:Content>