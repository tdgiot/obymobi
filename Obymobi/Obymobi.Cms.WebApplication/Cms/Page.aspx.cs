﻿using Dionysos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using Obymobi.Data.EntityClasses;
using Dionysos.Web.UI.WebControls;
using Obymobi.ObymobiCms.UI;
using Obymobi.Logic.Cms;
using Dionysos.Web;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data;
using Obymobi.Enums;
using Obymobi.ObymobiCms.Cms.SubPanels;

namespace Obymobi.ObymobiCms.Cms
{
    public partial class Page : PageLLBLGenEntityCms
    {
        #region Fields
        
        private bool hasLanguageSpecificContent = false;
        
        private SubPanels.PageTypeElementsCollectionPanel pageTypeElementsCollectionPanel = null;

        private FastDictionary<string, System.Web.UI.Control> cultureControls = new FastDictionary<string, System.Web.UI.Control>();

        #endregion

        #region Methods

        protected override void OnInit(EventArgs e)
		{
            this.RetrieveSiteCultures();
            this.LoadUserControls();
            this.DataSourceLoaded += Page_DataSourceLoaded;
			base.OnInit(e);			            
		}

        private void RetrieveSiteCultures()
        { 
            int pageId;
            if (QueryStringHelper.GetInt("id", out pageId))
            {
                PredicateExpression filter = new PredicateExpression(PageFields.PageId == pageId);

                PrefetchPath prefetch = new PrefetchPath(EntityType.SiteCultureEntity);

                RelationCollection relations = new RelationCollection();
                relations.Add(SiteCultureEntityBase.Relations.SiteEntityUsingSiteId);
                relations.Add(SiteEntityBase.Relations.PageEntityUsingSiteId);

                SiteCultureCollection siteCultures = new SiteCultureCollection();
                siteCultures.GetMulti(filter, 0, null, relations, prefetch);

                this.siteCultures = siteCultures;
            }
        }

        private void LoadUserControls()
        {           
            this.pageTypeElementsCollectionPanel = this.tabsMain.AddTabPage(this.Translate("Agnostic", "Taal Onafh."), "editor-agnostic", "~/Cms/SubPanels/PageTypeElementsCollectionPanel.ascx") as SubPanels.PageTypeElementsCollectionPanel;
            if (this.siteCultures != null)
            {
                foreach (SiteCultureEntity siteCulture in this.siteCultures)
                {
                    Control control = this.tabsMain.AddTabPage(siteCulture.CultureCode.ToUpperInvariant(), "editor_" + siteCulture.CultureCode, "~/Cms/SubPanels/PageTypeElementsCollectionPanel.ascx");
                    this.cultureControls.Add(siteCulture.CultureCode, control);
                }
            }

            if (this.PageMode != Dionysos.Web.PageMode.Add)
            {
                this.tabsMain.AddTabPage("Bijlagen", "Attachments", "~/Generic/SubPanels/AttachmentCollection.ascx");
            }
            this.tabsMain.AddTabPage("Afbeeldingen", "Media", "~/Generic/UserControls/MediaCollection.ascx");
        }

        private void SetGui()
        {
            if (PageMode == PageMode.Add) 
            { return; }

            this.lblPageTypeText.Text = PageTypeHelper.GetPageTypeName(this.DataSourceTyped.PageTypeAsEnum);

            PageTypeBase pageType = PageTypeHelper.GetPageTypeInstance(this.DataSourceTyped.PageTypeAsEnum);
            pageType.PageElementsCollection = this.DataSourceTyped.PageElementCollection;

            // Show Language Agnostic Checkboxlist
            foreach (var pageTypeElement in pageType.PageTypeElements)
            {
                if (!pageTypeElement.CmsEditable)
                    continue;
                
                ListItem li = new ListItem(pageTypeElement.Name, pageTypeElement.SystemName);
                li.Selected = this.DataSourceTyped.PageElementCollection.Any(x => x.SystemName == li.Value && x.CultureCode.IsNullOrWhiteSpace());
                if (!li.Selected)
                {
                    this.hasLanguageSpecificContent = true;
                }
                this.cblLanguageAgnosticPageElements.Items.Add(li);
            }

            if (this.DataSourceTyped.PageTemplateId.HasValue)
            {
                this.btUncoupleGenericPage.Visible = true;
                this.ddlPageTemplateId.Enabled = false;
                this.tbName.IsRequired = false;
                //this.tabsMain.TabPages.Remove(this.tabsMain.TabPages.FindByName("Media"));
            }
            else
            {
                this.btUncoupleGenericPage.Visible = false;
                this.tbName.IsRequired = true;
            }

            // Page templates
            var pageTemplateFilter = new PredicateExpression();
            pageTemplateFilter.Add(PageTemplateFields.PageType == this.DataSourceTyped.PageType);
            
            var genericPageTemplateFields = new IncludeFieldsList();
            genericPageTemplateFields.Add(PageTemplateFields.Name);
            genericPageTemplateFields.Add(PageTemplateFields.PageType);

            var relations = new RelationCollection();
            relations.Add(PageTemplateEntityBase.Relations.SiteTemplateEntityUsingSiteTemplateId);

            var sort = new SortExpression(SiteTemplateFields.Name | SortOperator.Ascending);
            sort.Add(PageTemplateFields.Name | SortOperator.Ascending);

            var pageTemplates = new PageTemplateCollection();
            pageTemplates.GetMulti(pageTemplateFilter, 0, sort, relations, null, genericPageTemplateFields, 0, 0);

            this.ddlPageTemplateId.DataSource = pageTemplates;
            this.ddlPageTemplateId.DataBind();
        }

        private void LoadEditors()
        { 
            // If there are any Language Agnostic Elements, add that tab first
            if (this.DataSourceTyped.PageElementCollection.Any(x => x.CultureCode.IsNullOrWhiteSpace()))
            {
                this.pageTypeElementsCollectionPanel.DataSource = this.DataSourceTyped;
                this.pageTypeElementsCollectionPanel.CultureCode = null;
            }
    
            foreach (string cultureCode in this.cultureControls.Keys)
            {
                if (!this.hasLanguageSpecificContent)
                {
                    // hide the tab since we dont have anything to show on it
                    SiteCultureEntity siteCulture = this.siteCultures.SingleOrDefault(sc => sc.CultureCode.Equals(cultureCode, StringComparison.InvariantCultureIgnoreCase));
                    if (siteCulture != null)
                    {
                        TabPage cultureTab = this.tabsMain.TabPages.FindByName("editor_" + siteCulture.CultureCode);
                        if (cultureTab != null)
                        {
                            cultureTab.Visible = false;
                        }
                    }                    
                }
                else
                {
                    // Load a panel for each culture that was enabled on the Site
                    PageTypeElementsCollectionPanel editor = (SubPanels.PageTypeElementsCollectionPanel)this.cultureControls[cultureCode];
                    editor.CultureCode = cultureCode;
                    editor.DataSource = this.DataSourceTyped;
                }
            }

            // Load the Editors for the translations of the Page.Name            
            foreach (SiteCultureEntity siteCulture in this.DataSourceTyped.SiteEntity.SiteCultureCollection)
            {
                this.plhNameTranslations.AddHtml("<tr><td class=\"label\">{0}</td><td class=\"control\">", Obymobi.Culture.Mappings[siteCulture.CultureCode].NameAndCultureCode);

                TextBoxString tb = new TextBoxString();
                tb.ID = "tbName_" + siteCulture.CultureCode;

                CustomTextRenderer renderer = new CustomTextRenderer(this.DataSourceTyped, siteCulture.CultureCode);
                renderer.RenderCustomText(tb, CustomTextType.PageName);
                
                this.plhNameTranslations.Controls.Add(tb);
                this.plhNameTranslations.AddHtml("</td></tr>");
            }            
        }      

        public override bool Save()
        {
            if (DataSourceTyped.SiteId == 0)
            {
                AddInformator(Dionysos.Web.UI.InformatorType.Warning, "SiteId cannot be null. Please add Pages through the Site page.");
                return false;
            }

            bool toReturn = false;
            if (base.Save())
            {
                toReturn = true;

                // Save Language Agnostic stuff
                PageTypeBase pageType = PageTypeHelper.GetPageTypeInstance(this.DataSourceTyped.PageTypeAsEnum);
                foreach (ListItem item in this.cblLanguageAgnosticPageElements.Items)
                {
                    // Get related PageTypeElement
                    PageTypeElement pageTypeElement = pageType.PageTypeElements.FirstOrDefault(x => x.SystemName == item.Value);
                    if (pageTypeElement == null)
                        throw new Exception("Page Type Elements were changed during post back, missing: " + item.Value);

                    PageElementEntity languageAgnosticEntity = this.DataSourceTyped.PageElementCollection.FirstOrDefault(x => x.SystemName == item.Value && x.CultureCode.IsNullOrWhiteSpace());
                    List<PageElementEntity> languageSpecificEntities = this.DataSourceTyped.PageElementCollection.Where(x => x.SystemName == item.Value && !x.CultureCode.IsNullOrWhiteSpace()).ToList();
                    
                    if (item.Selected)
                    {
                        // It becomes (or is) language agnostic
                        // Delete any Language Specific items
                        for (int i = 0; i < languageSpecificEntities.Count; i++)
                        {
                            languageSpecificEntities[i].Delete();         
                            this.DataSourceTyped.PageElementCollection.Remove(languageSpecificEntities[i]);                                     
                        }

                        if (languageAgnosticEntity == null)
                        {
                            PageElementEntity pageElementEntity = new PageElementEntity();
                            pageElementEntity.PageId = this.DataSourceTyped.PageId;
                            pageElementEntity.SystemName = pageTypeElement.SystemName;
                            pageElementEntity.CultureCode = null;
                            pageElementEntity.PageElementType = (int)pageTypeElement.PageTypeElementType;
                            pageElementEntity.Save();

                            this.DataSourceTyped.PageElementCollection.Add(pageElementEntity);
                            pageTypeElement.DataSource = pageElementEntity;
                        }
                    }
                    else
                    { 
                        // This is not (or no longer) language agnostic, remove the language agnostic content if it was created                        
                        if (languageAgnosticEntity != null)
                        {
                            languageAgnosticEntity.Delete();
                            this.DataSourceTyped.PageElementCollection.Remove(languageAgnosticEntity);
                        }                        
                    }
                }   
             
                // Save the 'Page.Name' translations                
                foreach (SiteCultureEntity siteCulture in this.DataSourceTyped.SiteEntity.SiteCultureCollection)
                {                    
                    TextBoxString tb = this.plhNameTranslations.FindControl("tbName_" + siteCulture.CultureCode) as TextBoxString;
                    if (tb != null)
                    {
                        CustomTextRenderer renderer = new CustomTextRenderer(this.DataSourceTyped, siteCulture.CultureCode);
                        renderer.SaveCustomText(tb, CustomTextType.PageName);                        
                    }
                }

                this.pageTypeElementsCollectionPanel.DataSource = this.DataSourceTyped;

                foreach (string cultureCode in this.cultureControls.Keys)
                {
                    if (this.hasLanguageSpecificContent)
                    {
                        PageTypeElementsCollectionPanel editor = (SubPanels.PageTypeElementsCollectionPanel)this.cultureControls[cultureCode];
                        editor.CultureCode = cultureCode;
                        editor.DataSource = this.DataSourceTyped;
                    }
                }
            }

            return toReturn;
        }

        private void HookUpEvents()
        {
            this.btUncoupleGenericPage.Click += btUncoupleGenericPage_Click;
        }

        private void UncoupleGenericPage()
        {
            if (this.DataSourceTyped.PageTemplateId.HasValue)
            {
                this.DataSourceTyped.PageTemplateId = null;
                this.DataSourceTyped.Save();

                this.RedirectUsingRawUrl();
            }            
        }        

        #endregion

		#region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            bool addMode = (this.PageMode == PageMode.Add);
            if (!addMode)
            {
                if (this.DataSourceTyped.SiteEntity.SiteCultureCollection.Count == 0)
                {
                    this.lblPageNameTranslations.Visible = false;
                    this.AddInformatorInfo(this.Translate("NoLanguagesHaveBeenChosen", "Er zijn voor de Site waar deze Pagina deel van uit maakt nog geen talen ingesteld."));
                    this.Validate();
                }

                if (!this.DataSourceTyped.Visible)
                {
                    this.AddInformatorInfo(this.Translate("PageIsNotVisible", "Deze pagina is niet zichtbaar en wordt daarom niet getoond."));
                    this.Validate();
                }
            }

            this.HookUpEvents();
        }

        private void Page_DataSourceLoaded(object sender)
        {
            this.SetGui();
            this.LoadEditors();
        }

        private void btUncoupleGenericPage_Click(object sender, EventArgs e)
        {
            this.UncoupleGenericPage();
        }

		#endregion

		#region Properties

        public PageEntity DataSourceTyped
        {
            get
            {
                return this.DataSource as PageEntity;
            }
        }
        
        private SiteCultureCollection siteCultures = null;

		#endregion
    }
}
