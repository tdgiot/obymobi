﻿using System;
using Dionysos.Web;

public partial class FastPage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SessionHelper.SetValue("DERP", "FAST PAGE");
        Response.Write("Hello, SessionId " + SessionHelper.SessionID + " - " + SessionHelper.GetValue<string>("DERP"));
    }
}