﻿using System;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;

namespace Obymobi.ObymobiCms.Advertising
{
	public partial class Advertisements : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        #region Event handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            LLBLGenProDataSource datasource = this.DataSource as LLBLGenProDataSource;
            if (datasource != null)
            {
                PredicateExpression filter = new PredicateExpression();
                filter.Add(AdvertisementFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
                if (CmsSessionHelper.CurrentRole == Role.Administrator)
                    filter.AddWithOr(AdvertisementFields.CompanyId == DBNull.Value);
                datasource.FilterToUse = filter;
            }
        }

        #endregion
    }
}
