﻿using System;
using Crave.Api.Logic.Requests.Publishing;
using Crave.Api.Logic.Timestamps;
using Crave.Api.Logic.UseCases;
using Dionysos.Web.UI;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Advertising
{
    public partial class AdvertisementConfiguration : Dionysos.Web.UI.PageLLBLGenEntity
	{
        #region Properties

        public AdvertisementConfigurationEntity DataSourceAsAdvertisementConfigurationEntity
        {
            get
            {
                return this.DataSource as AdvertisementConfigurationEntity;
            }
        }

        #endregion

        #region Methods

        private void LoadUserControls()
        {
            PredicateExpression filter = new PredicateExpression(AdvertisementFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            SortExpression sort = new SortExpression(new SortClause(AdvertisementFields.Name, SortOperator.Ascending));

            AdvertisementCollection advertisements = new AdvertisementCollection();
            advertisements.GetMulti(filter, 0, sort);

            this.cblAdvertisements.DataSource = advertisements;
            this.cblAdvertisements.DataBind();
        }        

        public override bool Save()
        {
            if (this.PageMode == Dionysos.Web.PageMode.Add)
            {
                this.DataSourceAsAdvertisementConfigurationEntity.CompanyId = CmsSessionHelper.CurrentCompanyId;
            }        
            return base.Save();
        }

        #endregion

        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            base.OnInit(e);
        }

        #endregion
    }
}
