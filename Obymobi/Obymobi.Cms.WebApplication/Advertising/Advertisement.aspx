﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Advertising.Advertisement" Codebehind="Advertisement.aspx.cs" %>
<%@ Reference VirtualPath="~/Generic/UserControls/CustomTextCollection.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Generic" Name="Generic">
				<controls>
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblName"></D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblVisible"></D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:CheckBox runat="server" ID="cbVisible" />
							</td>	
					    </tr>
                        <tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblDescription" />
							</td>
							<td class="control threeCol" colspan="3">
								<D:TextBoxMultiLine ID="tbDescription" runat="server" Rows="10" UseDataBinding="true" MaxLength="500"></D:TextBoxMultiLine>
							</td>
						</tr>
					    <tr>									    					              				
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblCompanyId">Bedrijf</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
							    <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlCompanyId" IncrementalFilteringMode="StartsWith" EntityName="Company" TextField="Name" ValueField="CompanyId" PreventEntityCollectionInitialization="True" />    
							</td> 					    					              				
						</tr>
					 </table>			
				</Controls>
			</X:TabPage>
            <X:TabPage Text="Action" Name="Action">
                <Controls>
                    <D:Panel ID="pnlOtoucho" runat="server" GroupingText="Otoucho">
                        <table class="dataformV2">
                            <tr>
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblGenericproductId">Basisproduct</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
							        <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlGenericproductId" IncrementalFilteringMode="StartsWith" EntityName="Genericproduct" TextField="Name" ValueField="GenericproductId" />
							    </td> 	
                                <td class="label">
                                </td>			
                                <td class="control">
                                </td>
                            </tr>
                        </table>
                    </D:Panel>
                    <D:Panel ID="pnlCrave" runat="server" GroupingText="Crave">
                        <table class="dataformV2">
					        <tr>
						        <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblProductId">Product</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlProductId" IncrementalFilteringMode="StartsWith" EntityName="Product" TextField="Name" ValueField="ProductId" PreventEntityCollectionInitialization="true" />
                                </td>       
						        <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblActionCategoryId">Category</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlActionCategoryId" IncrementalFilteringMode="StartsWith" EntityName="Category" TextField="Name" ValueField="CategoryId" PreventEntityCollectionInitialization="true" />
                                </td>       
					        </tr>
                            <tr>
						        <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblActionEntertainmentId">Entertainment</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlActionEntertainmentId" IncrementalFilteringMode="StartsWith" EntityName="Entertainment" TextField="Name" ValueField="EntertainmentId" PreventEntityCollectionInitialization="true" />
                                </td>       
						        <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblActionEntertainmentCategoryId">Entertainmentcategory</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlActionEntertainmentCategoryId" IncrementalFilteringMode="StartsWith" EntityName="Entertainmentcategory" TextField="Name" ValueField="EntertainmentcategoryId" PreventEntityCollectionInitialization="true" />
                                </td>
					        </tr>
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" ID="lblActionSiteId">Site</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlActionSiteId" IncrementalFilteringMode="StartsWith" EntityName="Site" TextField="Name" ValueField="SiteId" PreventEntityCollectionInitialization="true">
                                        <ClientSideEvents SelectedIndexChanged="function(s, e) {ddlActionPageId.PerformCallback('1');}" />
                                    </X:ComboBoxLLBLGenEntityCollection>
                                </td>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" ID="lblActionPageId">Site pagina</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlActionPageId" ClientInstanceName="ddlActionPageId" OnCallback="ddlActionPageId_OnCallback" IncrementalFilteringMode="StartsWith" EntityName="Page" TextField="Name" ValueField="PageId" PreventEntityCollectionInitialization="true" />
                                </td>
                            </tr>
                            <tr>
						        <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblActionUrl">URL</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:TextBoxString ID="tbActionUrl" runat="server" IsRequired="false"></D:TextBoxString>
                                </td>       
						        <td class="label">
						        </td>
						        <td class="control">
						        </td>        
					        </tr>
				        </table> 
                    </D:Panel>
			    </Controls>
            </X:TabPage>
			<X:TabPage Text="Advertisements" Name="Advertisements">
				<controls>
					<D:PlaceHolder runat="server">
                        <p><D:Label runat="server" ID="lblAdvertisementCaption">Hieronder kunt u de tags kiezen die specifiek voor deze advertentie gelden.</D:Label></p>					
						<table class="dataformV2">
							<tr>
								<td class="label">
									<D:Label runat="server" ID="lblAdvertisementTagAdvertisement">Tags</D:Label>
								</td>
								<td class="control">
									<D:CheckBoxListLLBLGenEntityCollection runat="server" ID="cblAdvertisementTagAdvertisement" EntityName="AdvertisementTag" DataTextField="FriendlyName" LinkCollectionPropertyOnParentDataSource="AdvertisementTagAdvertisementCollection" LinkEntityName="AdvertisementTagAdvertisementEntity" />
								</td> 
								<td class="label">
								</td>
								<td class="control">
								</td>    			                              
							</tr>
						</table>						
					</D:PlaceHolder>
				</controls>
			</X:TabPage>            						
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>

