﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.ObymobiCms.Advertising
{
	public partial class AdvertisementTag : Dionysos.Web.UI.PageLLBLGenEntity
    {

		#region Fields
		#endregion

		#region Methods

		protected override void OnInit(EventArgs e)
		{
            this.LoadUserControls();
			this.DataSourceLoaded += new Dionysos.Delegates.Web.DataSourceLoadedHandler(AdvertisementTag_DataSourceLoaded);
			base.OnInit(e);
		}

		void SetGui()
		{
            if (!this.DataSourceAsAdvertisementTagEntity.IsNew)
            {
                this.tbName.IsRequired = false;
                this.tbName.UseDataBinding = false;
                this.tbName.Enabled = false;
                this.tbName.Text = this.DataSourceAsAdvertisementTagEntity.Name;
            }                            
		}

        private void LoadUserControls()
        {

        }

		#endregion

		#region Event Handlers

		protected void Page_Load(object sender, EventArgs e)
		{
		}

		void AdvertisementTag_DataSourceLoaded(object sender)
		{
			this.SetGui();
		}

		#endregion

		#region Properties

        /// <summary>
        /// Return the page's datasource as a AdvertisementTagEntity
        /// </summary>
        public AdvertisementTagEntity DataSourceAsAdvertisementTagEntity
        {
            get
            {
                return this.DataSource as AdvertisementTagEntity;
            }
        }

		#endregion


    }
}
