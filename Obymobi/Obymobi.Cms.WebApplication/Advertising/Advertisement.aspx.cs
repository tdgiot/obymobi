﻿using System;
using DevExpress.Web;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.ObymobiCms.Advertising
{
	public partial class Advertisement : Dionysos.Web.UI.PageLLBLGenEntity
    {
		#region Methods

		protected override void OnInit(EventArgs e)
		{
            this.LoadUserControls();
            this.DataSourceLoaded += Advertisement_DataSourceLoaded;
			base.OnInit(e);
		}

        private void LoadUserControls()
        {
            Generic.UserControls.CustomTextCollection translationsPanel = this.tabsMain.AddTabPage("Translations", "CustomTextCollection", "~/Generic/UserControls/CustomTextCollection.ascx", "Generic") as Generic.UserControls.CustomTextCollection;
            translationsPanel.FullWidth = true;            

            this.tabsMain.AddTabPage("Images", "Media", "~/Generic/UserControls/MediaCollection.ascx");
        }

        private void SetFilters()
        {
            this.SetProductFilter();
            this.SetCategoryFilter();
            this.setEntertainmentFilter();
            this.SetEntertainmentCategoryFilter();
            this.SetCompanyFilter();
            this.SetSiteFilter();
            this.SetPageFilter();
        }

        private void SetProductFilter()
        {
            // Create a filter for retrieving the active prod
            PredicateExpression productsFilter = new PredicateExpression();
            productsFilter.Add(ProductFields.VisibilityType != VisibilityType.Never);           // Product should be visible
            productsFilter.Add(ProductCategoryFields.ProductCategoryId != DBNull.Value);        // Product should be in a category
            productsFilter.Add(ProductFields.CompanyId == CmsSessionHelper.CurrentCompanyId);   // Product should belong to the specified company

            // The join to get linked to category products only
            RelationCollection joins = new RelationCollection();
            joins.Add(ProductEntity.Relations.ProductCategoryEntityUsingProductId, JoinHint.Inner);

            // Sort the products
            SortExpression sort = new SortExpression(new SortClause(ProductFields.Name, SortOperator.Ascending));

            // Get the products
            ProductCollection products = new ProductCollection();
            products.GetMulti(productsFilter, 0, sort, joins);

            ProductCollection productsFiltered = new ProductCollection();
            foreach (ProductEntity product in products)
            {
                // Check if the category of which the product is in, is actually used in the menu and is visible
                foreach (ProductCategoryEntity productCategory in product.ProductCategoryCollection)
                {
                    if (productCategory.CategoryEntity.ParentCategoryId.HasValue && productCategory.CategoryEntity.ParentCategoryEntity.Visible)
                    {
                        productsFiltered.Add(product);
                        break;
                    }
                }   
            }

            // Bind the data
            this.ddlProductId.DataSource = productsFiltered;
            this.ddlProductId.DataBind();
        }

        private void SetCategoryFilter()
        {
            // Create the filter
            PredicateExpression filter = new PredicateExpression();
            filter.Add(CategoryFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            filter.Add(CategoryFields.Visible == true);

            // Sort the results
            SortExpression sort = new SortExpression(new SortClause(CategoryFields.Name, SortOperator.Ascending));

            // Categories
            CategoryCollection categoryCollection = new CategoryCollection();
            categoryCollection.GetMulti(filter, 0, sort);
            this.ddlActionCategoryId.DataSource = categoryCollection;
            this.ddlActionCategoryId.DataBind();
        }

        private void setEntertainmentFilter()
        {
            if (CmsSessionHelper.CurrentCompanyId > 0)
            {
                // Filter
                PredicateExpression filter = new PredicateExpression();
                filter.Add(DeliverypointgroupFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

                // Relations
                RelationCollection relations = new RelationCollection();
                relations.Add(EntertainmentEntity.Relations.DeliverypointgroupEntertainmentEntityUsingEntertainmentId, JoinHint.Left);
                relations.Add(DeliverypointgroupEntertainmentEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId);

                // Sort 
                SortExpression sort = new SortExpression(new SortClause(EntertainmentFields.Name, SortOperator.Ascending));

                // Get the entertainments
                EntertainmentCollection entertainment = new EntertainmentCollection();
                entertainment.GetMulti(filter, 0, sort, relations);

                // Bind the results
                this.ddlActionEntertainmentId.DataSource = entertainment;
                this.ddlActionEntertainmentId.DataBind();
            }
        }

        private void SetEntertainmentCategoryFilter()
        {
            // Sort 
            SortExpression sort = new SortExpression(new SortClause(EntertainmentcategoryFields.Name, SortOperator.Ascending));

            // Entertainmentcategories
            EntertainmentcategoryCollection entertainmentcategoryCollection = new EntertainmentcategoryCollection();
            entertainmentcategoryCollection.GetMulti(new PredicateExpression(), 0, sort);

            this.ddlActionEntertainmentCategoryId.DataSource = entertainmentcategoryCollection;
            this.ddlActionEntertainmentCategoryId.DataBind();
        }

        private void SetCompanyFilter()
        {
            // Filter
            PredicateExpression companyFilter = new PredicateExpression(CompanyFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

            // Get results
            CompanyCollection companyCollection = new CompanyCollection();
            companyCollection.GetMulti(companyFilter);

            // Bind results
            this.ddlCompanyId.DataSource = companyCollection;
            this.ddlCompanyId.DataBind();
        }

        private void SetSiteFilter()
        { 
            // Filter
            PredicateExpression filter = new PredicateExpression();
            filter.Add(UIModeFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

            // Relations
            RelationCollection relations = new RelationCollection();
            relations.Add(SiteEntity.Relations.UITabEntityUsingSiteId);
            relations.Add(UITabEntity.Relations.UIModeEntityUsingUIModeId);           

            // Sort
            SortExpression sort = new SortExpression(new SortClause(SiteFields.Name, SortOperator.Ascending));

            // Get results
            SiteCollection sites = new SiteCollection();
            sites.GetMulti(filter, 0, sort, relations);

            // Bind results
            this.ddlActionSiteId.DataSource = sites;
            this.ddlActionSiteId.DataBind();
        }

        private void SetPageFilter()
        {
            if (this.DataSourceAsAdvertisementEntity.ActionSiteId.HasValue)
            {
                var pages = this.LoadPages(this.DataSourceAsAdvertisementEntity.ActionSiteId.Value);                
                this.ddlActionPageId.DataSource = pages;
                this.ddlActionPageId.DataBind();
            }
        }

        private PageCollection LoadPages(int siteId)
        {
            // Filter
            PredicateExpression filter = new PredicateExpression(PageFields.SiteId == siteId);

            // Sort
            SortExpression sort = new SortExpression(new SortClause(PageFields.Name, SortOperator.Ascending));

            // Get results
            PageCollection pages = new PageCollection();
            pages.GetMulti(filter, 0, sort);

            // Return results
            return pages;
        }

		void SetGui()
		{

		}

		#endregion

		#region Event Handlers

		protected void Page_Load(object sender, EventArgs e)
		{

		}

		private void Advertisement_DataSourceLoaded(object sender)
		{
            // Set default to visible
            if (this.DataSourceAsAdvertisementEntity.IsNew)
                this.DataSourceAsAdvertisementEntity.Visible = true;

            this.SetFilters();
			this.SetGui();
		}

        public override bool Save()
        {
            int numActions = 0;
            if (this.ddlProductId.ValidId > 0)
                numActions++;

            if (this.ddlActionCategoryId.ValidId > 0)
                numActions++;

            if (this.ddlActionEntertainmentId.ValidId > 0)
                numActions++;

            if (this.ddlActionEntertainmentCategoryId.ValidId > 0)
                numActions++;

            if (this.tbActionUrl.Value.Length > 0)
                numActions++;

            if (this.ddlActionSiteId.ValidId > 0)
                numActions++;

            if (numActions > 1)
            {
                this.MultiValidatorDefault.AddError(this.Translate("AdvertisementOnlyOneAction", "Multiple actions have been selected. Only one action is allowed per advertisement."));
            }
            this.Validate();

            if (this.PageMode == Dionysos.Web.PageMode.Add && CmsSessionHelper.CurrentRole < Role.Administrator)
                this.DataSourceAsAdvertisementEntity.CompanyId = CmsSessionHelper.CurrentCompanyId;

            bool success = base.Save();

            if (!this.DataSourceAsAdvertisementEntity.CompanyId.HasValue)
            {
                CustomTextHelper.UpdateCustomTexts(this.DataSource);
            }
            else
            {
                CustomTextHelper.UpdateCustomTexts(this.DataSource, this.DataSourceAsAdvertisementEntity.CompanyEntity);
            }

            return success;            
        }

        protected void ddlActionPageId_OnCallback(object sender, CallbackEventArgsBase e)
        {
            if (this.ddlActionSiteId.ValidId > 0)
            {                
                var pages = this.LoadPages(this.ddlActionSiteId.ValidId);
                this.ddlActionPageId.DataSource = pages;
                this.ddlActionPageId.DataBind();                
            }                
        }

        public AdvertisementEntity DataSourceAsAdvertisementEntity
        {
            get
            {
                return this.DataSource as AdvertisementEntity;
            }
        }

		#endregion
    }
}
