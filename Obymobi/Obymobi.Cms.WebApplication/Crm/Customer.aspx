﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Crm.Customer" Codebehind="Customer.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen">
				<Controls>
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblName">Naam</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
							</td>
							<td class="label">
								
							</td>
							<td class="control">
								
							</td>	
					    </tr>
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblReceiptTypes">Bonnen</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbReceiptTypes" runat="server" IsRequired="true"></D:TextBoxString>
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblCheckInterval">Communicatieinterval</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxInt ID="tbCheckInterval" runat="server" IsRequired="true"></D:TextBoxInt>
							</td>
					    </tr>
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblPrinterName">Printer</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbPrinterName" runat="server" IsRequired="true"></D:TextBoxString>
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblRecordAllRequestToRequestLog">Logging</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:CheckBox runat="server" ID="cbRecordAllRequestToRequestLog" />
							</td>
					    </tr>
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblLastRequest">Laatste contact</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:Label runat="server" id="lblLastRequestValue"></D:Label>
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblLastStatus">Laatste status</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:Label runat="server" id="lblLastStatusValue"></D:Label>
							</td>	
					    </tr>                        					    					    
					 </table>			
				</Controls>
			</X:TabPage>						
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>

