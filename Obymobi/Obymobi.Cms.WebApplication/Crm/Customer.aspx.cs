﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;

namespace Obymobi.ObymobiCms.Crm
{
    public partial class Customer : Dionysos.Web.UI.PageLLBLGenEntity
    {
        #region Event handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.PageMode == Dionysos.Web.PageMode.Edit)
            {
                this.lblLastRequestValue.Text = (this.DataSourceAsTerminalEntity.DeviceEntity.LastRequestUTC.HasValue ? this.DataSourceAsTerminalEntity.DeviceEntity.LastRequestUTC.Value.ToString() : "Onbekend");
                this.lblLastStatusValue.Text = (!string.IsNullOrEmpty(this.DataSourceAsTerminalEntity.LastStatusText) ? this.DataSourceAsTerminalEntity.LastStatusText : "Onbekend");
            }
        }

        #endregion

        #region Properties

        public TerminalEntity DataSourceAsTerminalEntity
        {
            get
            {
                return this.DataSource as TerminalEntity;
            }
        }

        #endregion
    }
}
