﻿using System;
using System.IO;
using System.Text;
using System.Web;
using Dionysos;
using Dionysos.Net.Mail;
using Dionysos.Web;

namespace Obymobi.ObymobiCms
{
    public partial class _404 : Dionysos.Web.UI.PageDefault
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.btnBack.Attributes.Add("onClick", "javascript:history.back(); return false;");
            this.btnEmail.Click += new EventHandler(btnEmail_Click);

            // Get the exception
            HttpApplicationState contents = HttpContext.Current.Application.Contents;
            Exception lastError = (Exception)contents.Get("Exception");
            if (lastError != null)
            {
                contents.Remove("Exception");
            }

            if (lastError != null && lastError.GetBaseException() != null)
            {
                lastError = lastError.GetBaseException();
            }

            if (lastError != null)
            {
                // Set the exception to the session
                SessionHelper.SetValue("LastError", lastError);
            }

            Exception ex;
            if (SessionHelper.TryGetValue("LastError", out ex))
            {
                this.plhMessage.AddHtml(this.GetMessage(ex));
            }
        }

        private string GetMessage(Exception ex) => string.Format("<b>Message:</b> {0} <br /><br />", ex.Message);

        private string GetDetails(Exception ex)
        {
            StringBuilder details = new StringBuilder()
                .AppendFormat("<b>Stack trace:</b> <br />");

            StringReader reader = new StringReader(ex.StackTrace.ToString());
            string line;
            while ((line = reader.ReadLine()) != null)
            {
                details.AppendFormatLine(line + "<br />");
            }
            details.Append("<br />");

            return details.ToString();
        }

        private void btnEmail_Click(object sender, EventArgs e)
        {
            Exception ex;
            if (SessionHelper.TryGetValue("LastError", out ex))
            {
                try
                {
                    MailUtilV2 mailer = new MailUtilV2("ErrorLogger@crave-emenu.com", "technology@crave-emenu.com", ex.Message)
                    {
                        BodyHtml = GetMessage(ex) + GetDetails(ex)
                    };
                    mailer.SendMail();

                    this.plhEmailSent.Visible = true;
                }
                catch (Exception exc)
                {
                    ErrorLoggerWeb.LogError(exc, exc.Message);
                }
            }
        }
    }
}
