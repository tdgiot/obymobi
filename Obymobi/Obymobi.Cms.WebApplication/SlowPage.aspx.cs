﻿using System;
using Dionysos.Web;

public partial class SlowPage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SessionHelper.SetValue("DERP", "SLOW PAGE");
        System.Threading.Thread.Sleep(10000);
        Response.Write("Hello, SessionId " + SessionHelper.SessionID + " - " + SessionHelper.GetValue<string>("DERP"));
    }
}