﻿using System;
using System.Web;
using System.Text;
using Dionysos;
using System.IO;
using Dionysos.Net.Mail;
using Dionysos.Web;

namespace Obymobi.ObymobiCms
{
    public partial class _401 : Dionysos.Web.UI.PageDefault
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.btnBack.Attributes.Add("onClick", "javascript:history.back(); return false;");
            this.btnEmail.Click += new EventHandler(btnEmail_Click);

            // Get the exception
            HttpApplicationState contents = HttpContext.Current.Application.Contents;
            Exception lastError = (Exception)contents.Get("Exception");
            if (lastError != null)
            {
                contents.Remove("Exception");
            }

            if (lastError != null && lastError.GetBaseException() != null)
            {
                lastError = lastError.GetBaseException();
            }

            if (lastError != null)
            {
                // Set the exception to the session
                SessionHelper.SetValue("LastError", lastError);
            }

            Exception ex;
            if (SessionHelper.TryGetValue("LastError", out ex))
            {
                this.plhMessage.AddHtml(this.GetMessage(ex));
                this.plhDetails.AddHtml(this.GetDetails(ex));
            }
        }

        private string GetMessage(Exception ex)
        {
            return string.Format("<b>Message:</b> {0} <br /><br />", ex.Message);
        }

        private string GetDetails(Exception ex)
        {
            StringBuilder details = new StringBuilder();
            if (ex is ObymobiException)
            {
                ObymobiException obyex = ex as ObymobiException;
                if (obyex != null)
                {
                    details.AppendFormat("<b>Error type:</b> {0} <br /><br />", obyex.ErrorEnumType);
                    details.AppendFormat("<b>Error code:</b> {0} <br /><br />", obyex.ErrorEnumValueInt);
                    details.AppendFormat("<b>Error text:</b> {0} <br /><br />", obyex.ErrorEnumValueText);
                    //details.AppendFormat("<b>Additional information:</b> {0} <br /><br />", AdditionalInformation);
                }
            }
            details.AppendFormat("<b>Stack trace:</b> <br />");
            StringReader reader = new StringReader(ex.StackTrace.ToString());
            String line;
            while ((line = reader.ReadLine()) != null)
            {
                details.AppendFormatLine(line + "<br />");
            }
            details.Append("<br />");

            return details.ToString();
        }

        private void btnEmail_Click(object sender, EventArgs e)
        {
            Exception ex;
            if (SessionHelper.TryGetValue("LastError", out ex))
            {
                try
                {
                    var mailer = new MailUtilV2("ErrorLogger@crave-emenu.com", "technology@crave-emenu.com", ex.Message);
                    mailer.BodyHtml = this.GetMessage(ex) + this.GetDetails(ex);
                    mailer.SendMail();

                    this.plhEmailSent.Visible = true;
                }
                catch (Exception exc)
                {
                    ErrorLoggerWeb.LogError(exc, exc.Message);
                }
            }
        }
    }
}
