﻿using Obymobi.Data.EntityClasses;
using Obymobi.ObymobiCms.UI;

namespace Obymobi.ObymobiCms.External
{
    public partial class ExternalDeliverypoint : PageLLBLGenEntityCms
    {
        public ExternalDeliverypointEntity DataSourceAsExternalDeliverypointEntity
        {
            get
            {
                return this.DataSource as ExternalDeliverypointEntity;
            }
        }
    }
}
