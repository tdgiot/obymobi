﻿using System;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.ObymobiCms.UI;

namespace Obymobi.ObymobiCms.External
{
    public partial class ExternalProduct : PageLLBLGenEntityCms
    {
        protected override void OnInit(EventArgs e)
        {
            DataSourceLoaded += ExternalProduct_DataSourceLoaded;
            base.OnInit(e);
        }

        private void ExternalProduct_DataSourceLoaded(object sender)
        {
            plhModifierGroup.Visible = DataSourceAsExternalProductEntity.Type == ExternalProductType.ModifierGroup;
        }

        public ExternalProductEntity DataSourceAsExternalProductEntity => this.DataSource as ExternalProductEntity;

        public override void Dispose()
        {
            DataSourceLoaded -= ExternalProduct_DataSourceLoaded;
            base.Dispose();
        }
    }
}
