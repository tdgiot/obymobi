﻿using System;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;

namespace Obymobi.ObymobiCms.External
{
    public partial class ExternalSystems : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "ExternalSystem";
            this.EntityPageUrl = "~/External/ExternalSystem.aspx";
            
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            LLBLGenProDataSource datasource = this.DataSource as LLBLGenProDataSource;
            if (datasource != null)
            {
                PredicateExpression filter = new PredicateExpression(ExternalSystemFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
                datasource.FilterToUse = filter;
            }

            MainGridView.ShowDeleteHyperlinkColumn = false;
            MainGridView.ShowDeleteColumn = false;
        }

        #endregion
    }
}
