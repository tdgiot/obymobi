﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Utils;
using DevExpress.Web;
using DevExpress.Web.Internal;
using DevExpress.Web.ASPxTreeList;
using DevExpress.Web.ASPxTreeList.Internal;
using Dionysos;
using Obymobi.Cms.Logic.DataSources;
using Obymobi.Cms.WebApplication.Old_App_Code.DatasSources.MenuExternalItems;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.QuerySpec;
using System.Linq;
using System.Drawing;

namespace Obymobi.ObymobiCms.External
{
    public partial class ExternalMenu : Dionysos.Web.UI.PageLLBLGenEntity
    {
        #region Fields

        private MenuExternalItemDataSource treelistDatasource;
        private readonly ExternalProductDataSource externalProductDataSource = new ExternalProductDataSource();
        private AlterationDialogMode alterationDialogMode = AlterationDialogMode.v3;

        #endregion

        #region Methods

        private void CreateTreeList()
        {
            this.tlItems.SettingsEditing.Mode = TreeListEditMode.Inline;
            this.tlItems.SettingsPopupEditForm.Width = 600;
            this.tlItems.SettingsBehavior.ColumnResizeMode = ColumnResizeMode.Disabled;
            this.tlItems.SettingsBehavior.AllowSort = false;
            this.tlItems.SettingsBehavior.AllowDragDrop = false;
            this.tlItems.ClientInstanceName = this.TreelistId;
            this.tlItems.AutoGenerateColumns = false;
            this.tlItems.SettingsEditing.AllowNodeDragDrop = false;
            this.tlItems.SettingsEditing.AllowRecursiveDelete = false;
            this.tlItems.Settings.GridLines = GridLines.Both;
            this.tlItems.KeyFieldName = nameof(MenuExternalItem.MenuExternalItemId);
            this.tlItems.ParentFieldName = nameof(MenuExternalItem.MenuExternalParentItemId);
            this.tlItems.Width = Unit.Pixel(736);
            this.tlItems.ClientSideEvents.NodeDblClick = this.NodeDblClickMethod;
            this.tlItems.CustomErrorText += this.tlItems_CustomErrorText;

            TreeListTextColumn nameColumn = new TreeListTextColumn();
            nameColumn.Caption = "Name";
            nameColumn.VisibleIndex = 0;
            nameColumn.FieldName = nameof(MenuExternalItem.Name);
            nameColumn.CellStyle.Wrap = DefaultBoolean.True;
            nameColumn.EditFormSettings.VisibleIndex = 0;
            this.tlItems.Columns.Add(nameColumn);

            TreeListComboBoxColumn typeColumn = new TreeListComboBoxColumn();
            typeColumn.Caption = "Type";
            typeColumn.VisibleIndex = 1;
            typeColumn.FieldName = nameof(MenuExternalItem.TypeText);
            typeColumn.CellStyle.Wrap = DefaultBoolean.True;
            typeColumn.EditFormSettings.VisibleIndex = 1;
            this.tlItems.Columns.Add(typeColumn);

            TreeListComboBoxColumn externalItemColumn = new TreeListComboBoxColumn();
            externalItemColumn.Caption = "External item";
            externalItemColumn.Width = Unit.Pixel(167);
            externalItemColumn.VisibleIndex = 2;
            externalItemColumn.FieldName = nameof(MenuExternalItem.ExternalItemName);
            externalItemColumn.CellStyle.Wrap = DefaultBoolean.True;
            externalItemColumn.EditFormSettings.VisibleIndex = 2;
            this.tlItems.Columns.Add(externalItemColumn);

            TreeListCommandColumn editCommandColumn = new TreeListCommandColumn();
            editCommandColumn.Name = "EditRowCommand";
            editCommandColumn.VisibleIndex = 4;
            editCommandColumn.Width = Unit.Pixel(15);
            editCommandColumn.ShowNewButtonInHeader = false;
            editCommandColumn.EditButton.Visible = true;
            editCommandColumn.NewButton.Visible = false;
            editCommandColumn.DeleteButton.Visible = false;
            editCommandColumn.ButtonType = ButtonType.Image;
            editCommandColumn.EditButton.Image.Url = this.ResolveUrl("~/images/icons/pencil.png");
            editCommandColumn.DeleteButton.Image.Url = this.ResolveUrl("~/images/icons/delete.png");
            editCommandColumn.CancelButton.Image.Url = this.ResolveUrl("~/images/icons/cross_grey.png");
            editCommandColumn.NewButton.Image.Url = this.ResolveUrl("~/images/icons/add2.png");
            editCommandColumn.UpdateButton.Image.Url = this.ResolveUrl("~/images/icons/disk.png");
            this.tlItems.Columns.Add(editCommandColumn);

            TreeListHyperLinkColumn editPageColumn = new TreeListHyperLinkColumn();
            editPageColumn.Name = "EditCommand";
            editPageColumn.Caption = " ";
            editPageColumn.VisibleIndex = 5;
            editPageColumn.Width = Unit.Pixel(15);
            editPageColumn.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            editPageColumn.PropertiesHyperLink.ImageUrl = "~/images/icons/page_edit.png";
            editPageColumn.EditCellStyle.CssClass = "hidden";
            editPageColumn.FieldName = nameof(MenuExternalItem.MenuExternalItemId);
            editPageColumn.EditFormSettings.VisibleIndex = 2;
            editPageColumn.EditFormCaptionStyle.CssClass = "hidden";
            this.tlItems.Columns.Add(editPageColumn);

            this.tlItems.HtmlRowPrepared += this.tlItems_HtmlRowPrepared;
            this.tlItems.HtmlDataCellPrepared += this.tlItems_HtmlDataCellPrepared;
            this.tlItems.HtmlCommandCellPrepared += this.tlItems_HtmlCommandCellPrepared;
            this.tlItems.CellEditorInitialize += this.tlItems_CellEditorInitialize;            

            this.InitDataSource();
        }

        

        private void InitDataSource()
        {
            if (this.DataSource != null)
            {
                this.treelistDatasource = new MenuExternalItemDataSource(this.DataSourceAsMenuEntity.MenuId);
                this.tlItems.DataSource = this.treelistDatasource;
            }

            this.hlExpandAll.NavigateUrl = $"javascript:{this.TreelistId}.ExpandAll()";
            this.hlCollapseAll.NavigateUrl = $"javascript:{this.TreelistId}.CollapseAll()";
        }

        private void LoadUserControls()
        {

        }

        private void SetGui()
        {
            if (!DataSourceAsMenuEntity.IsNew)
            {
                CreateTreeList();
                alterationDialogMode = CmsSessionHelper.AlterationDialogMode;
            }
        }

        private void ExpandCategoryNodes(TreeListNodeCollection nodes, TreeListNode parentNode)
        {
            foreach (TreeListNode node in nodes)
            {
                if (node is TreeListRootNode)
                {
                    node.Expanded = true;
                }
                else if (parentNode != null && node.DataItem is MenuExternalItem menuExternalItem)
                {
                    parentNode.Expanded = menuExternalItem.EntityType.Equals(nameof(CategoryEntity), StringComparison.InvariantCultureIgnoreCase);
                }

                if (node.HasChildren)
                {
                    this.ExpandCategoryNodes(node.ChildNodes, node);
                }
            }
        }

        #endregion

        #region Properties

        private MenuEntity DataSourceAsMenuEntity => this.DataSource as MenuEntity;

        private string TreelistId => $"{this.ID}_treelist";

        private string NodeDblClickMethod =>
            @" function(s, e) {
	                            " + this.TreelistId + @".StartEdit(e.nodeKey);
		                        e.cancel = true;	                                                                    
                            }";

        #endregion

        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "Menu";
            this.LoadUserControls();
            this.DataSourceLoaded += this.Menu_DataSourceLoaded;
            base.OnInit(e);
        }

        private void Menu_DataSourceLoaded(object sender)
        {
            this.SetGui();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.tlItems.DataBind();
            this.ExpandCategoryNodes(this.tlItems.Nodes, null);

            if (this.Master is MasterPages.MasterPageEntity masterPage)
            {
                masterPage.ToolBar.SaveButton.Visible = false;
                masterPage.ToolBar.SaveAndGoButton.Visible = false;
                masterPage.ToolBar.SaveAndNewButton.Visible = false;
                masterPage.ToolBar.DeleteButton.Visible = false;
            }
        }

        private void tlItems_HtmlRowPrepared(object sender, TreeListHtmlRowEventArgs e)
        {
            if (!e.NodeKey.IsNullOrWhiteSpace())
            {
                MenuExternalItem menuItem = this.treelistDatasource.DataSource.Single(item => item.MenuExternalItemId == e.NodeKey);
                if (menuItem != null)
                {

                    if (menuItem.EntityType.Equals(nameof(ProductEntity)) && menuItem.IsVisible.HasValue && !menuItem.IsVisible.Value)
                    {
                        e.Row.ForeColor = Color.Gray;
                        e.Row.Font.Strikeout = true;
                    }
                  
                }
            }
        }

        private void tlItems_HtmlDataCellPrepared(object sender, TreeListHtmlDataCellEventArgs e)
        {
            if (e.NodeKey == null)
            {
                return;
            }

            if (e.Column.Name == "EditCommand")
            {
                string navigateUrl = string.Empty;

                TreeListNode currentNode = this.tlItems.FindNodeByKeyValue(e.NodeKey);
                if (currentNode?.DataItem is MenuExternalItem menuExternalItem)
                {
                    navigateUrl = this.GetNavigateUrlForMenuExternalItem(menuExternalItem);
                }

                if (!navigateUrl.IsNullOrWhiteSpace())
                {
                    foreach (Control item in e.Cell.Controls)
                    {
                        if (item is HyperLinkDisplayControl link)
                        {
                            link.NavigateUrl = navigateUrl;
                            break;
                        }
                    }
                }
            }

            if (e.Column.Caption.Equals("Type", StringComparison.InvariantCultureIgnoreCase))
            {
                e.Cell.Style["background"] = e.NodeKey.Contains(MenuItemType.Product.ToLower()) ? string.Empty : "#d7f4f7";
            }
        }

        private bool IsMenuExternalItemEditable(MenuExternalItem menuExternalItem) => menuExternalItem.EntityType.Equals(nameof(ProductEntity)) ||
                                                                                      menuExternalItem.EntityType.Equals(nameof(AlterationEntity)) ||
                                                                                      menuExternalItem.EntityType.Equals(nameof(AlterationoptionEntity));

        private void tlItems_HtmlCommandCellPrepared(object sender, TreeListHtmlCommandCellEventArgs e)
        {
            if (e.NodeKey == null)
            {
                return;
            }
            
            if (e.Column.Name == "EditRowCommand")
            {
                TreeListNode currentNode = this.tlItems.FindNodeByKeyValue(e.NodeKey);
                if (currentNode?.DataItem is MenuExternalItem menuExternalItem)
                {
                    if (!IsMenuExternalItemEditable(menuExternalItem))
                    {
                        e.Cell.Text = String.Empty;
                    }
                }
            }
        }

        private void tlItems_CellEditorInitialize(object sender, TreeListColumnEditorEventArgs e)
        {
            if (!e.Column.FieldName.Equals(nameof(MenuExternalItem.ExternalItemName), StringComparison.InvariantCultureIgnoreCase))
            {
                return;
            }

            ASPxComboBox cbExternalItem = e.Editor as ASPxComboBox;
            cbExternalItem.IncrementalFilteringDelay = 100;
            cbExternalItem.IncrementalFilteringMode = IncrementalFilteringMode.Contains;

            TreeListNode currentNode = this.tlItems.FindNodeByKeyValue(e.NodeKey);
            if (currentNode?.DataItem is MenuExternalItem menuExternalItem)
            {
                if (IsMenuExternalItemEditable(menuExternalItem))
                {
                    if (menuExternalItem.EntityType.Equals(nameof(ProductEntity)))
                    {
                        cbExternalItem.DataSource = externalProductDataSource.GetDropdownDataSource(DataSourceAsMenuEntity.CompanyId, ExternalProductType.Product);
                    }
                    else if (menuExternalItem.EntityType.Equals(nameof(AlterationEntity)))
                    {
                        cbExternalItem.DataSource = externalProductDataSource.GetDropdownDataSource(DataSourceAsMenuEntity.CompanyId, ExternalProductType.ModifierGroup);
                    }
                    else if (menuExternalItem.EntityType.Equals(nameof(AlterationoptionEntity)))
                    {
                        cbExternalItem.DataSource = externalProductDataSource.GetDropdownDataSource(DataSourceAsMenuEntity.CompanyId, ExternalProductType.Modifier);
                    }
                    
                    cbExternalItem.ValueField = nameof(ExternalProductFields.ExternalProductId);
                    cbExternalItem.TextField = nameof(ExternalProductEntity.CombinedNameWithMenuAndSystemName);
                    cbExternalItem.DataBind();

                    cbExternalItem.AllowNull = true;
                    cbExternalItem.Items.Insert(0, new ListEditItem("None - Select ...", null));
                }
            }
        }

        private string GetNavigateUrlForMenuExternalItem(MenuExternalItem item)
        {
            string url = string.Empty;

            if (item.EntityType.Equals(nameof(CategoryEntity)))
            {
                url = $"~/Catalog/Category.aspx?id={item.PrimaryKey}";
            }
            else if (item.EntityType.Equals(nameof(ProductEntity)))
            {
                url = $"~/Catalog/Product.aspx?id={item.PrimaryKey}";
            }
            else if (item.EntityType.Equals(nameof(AlterationEntity)))
            {
                url = $"~/Catalog/{(alterationDialogMode == AlterationDialogMode.v3 ? "AlterationV3" : "Alteration")}.aspx?id={item.PrimaryKey}";
            }
            else if (item.EntityType.Equals(nameof(AlterationoptionEntity)))
            {
                url = $"~/Catalog/Alterationoption.aspx?id={item.PrimaryKey}";
            }

            return url;
        }

        private void tlItems_CustomErrorText(object sender, TreeListCustomErrorTextEventArgs e)
        {
            if (e.Exception.InnerException != null)
            {
                if (e.Exception.InnerException is ObymobiException exception)
                {
                    e.ErrorText = exception.AdditionalMessage;
                }
                else
                {
                    e.ErrorText = e.Exception.InnerException.Message;
                }
            }
        }

        #endregion
    }
}
