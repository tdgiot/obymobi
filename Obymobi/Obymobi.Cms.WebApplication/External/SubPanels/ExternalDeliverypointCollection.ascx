﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.External.SubPanels.ExternalDeliverypointCollection" Codebehind="ExternalDeliverypointCollection.ascx.cs" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<D:Panel ID="pnlExternalDeliverypoints" LocalizeText="false" runat="server" style="padding-bottom: 11px;">
    <table class="dataformV2">
        <tr>
            <td class="control">
                <D:CommandButton runat="server" ID="CommandButton1" CommandName="Add" Text="Add deliverypoint" ToolTip="Add deliverypoint" />
            </td>
            <td></td>
           <td class="label">
                <D:LabelTextOnly runat="server" ID="lblBatchExternalDeliverypoints" LocalizeText="false">Batch external deliverypoints (.csv)</D:LabelTextOnly>
            </td>
            <td class="control">
                <D:FileUpload runat="server" ID="fuExternalDeliverypoints" notdirty="true" style="float: left;" /> 
                <a id="csv-info-popup" href="#" style="float: left; position: relative; top: 0; right: 55px;">
                    <img src="<%= ResolveUrl("~/Images/Icons/information.png") %>" style="float: right;" alt="csv information" />
                </a><br /><br />
                <D:Button ID="btnBatchCreateExternalDeliverypoints" runat="server" Text="Create" notdirty="true" style="float: left; margin-right: 4px" />
            </td>
        </tr>        
    </table>
</D:Panel>

<X:GridViewColumnSelector runat="server" ID="gvcsExternalDeliverypoints">
	<Columns />
</X:GridViewColumnSelector>

<dx:ASPxPopupControl ID="pcExternalDeliverypointsCsv" ClientInstanceName="pcExternalDeliverypointsCsv" PopupElementID="csv-info-popup" Width="350" PopupAction="MouseOver" CloseAction="MouseOut" CloseOnEscape="false" Modal="false" ShowOnPageLoad="false" HeaderText="CSV file format" runat="server">
    <ContentCollection>
        <dx:PopupControlContentControl runat="server">
            <p>
                Example.csv<br /><br />
                Id,Name<br />
                1,Room 1<br />
                2,Room 2<br />
                ...<br />
                <br />
                <b>Note: Name column is optional</b>
            </p>
        </dx:PopupControlContentControl>
    </ContentCollection>
</dx:ASPxPopupControl>
