﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using Dionysos.Web.UI;
using Obymobi.ObymobiCms.UI;
using Obymobi.Enums;
using Dionysos;
using System.Diagnostics;
using Obymobi.Logic.Cms;
using Dionysos.Interfaces.Data;
using Dionysos.Web;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.ObymobiCms.External.SubPanels.SettingsPanels.Other
{
    public partial class GoogleSettingsPanel : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.btnConnect.Click += new EventHandler(btnConnect_Click);
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            string baseUrl = HttpContext.Current.Request.Url.ToString();
            baseUrl = baseUrl.Substring(0, baseUrl.IndexOf("management/"));

            string clientId = Dionysos.ConfigurationManager.GetString(DionysosWebConfigurationConstants.GoogleClientId);
            string scope = "https://www.googleapis.com/auth/cloudprint";

            string redirect = string.Format("{0}api/ApiAuthenticationWebService.asmx/SaveGoogleCode", baseUrl);
            if (WebEnvironmentHelper.CloudEnvironmentIsProduction)
            {
                redirect = "http://rambo.crave-emenu.com/api/ApiAuthenticationWebService.asmx/SaveGoogleCode";
            }

            // Because Google likes to cry me a river:
            redirect = redirect.Replace("localhost", "127.0.0.1");

            int companyId = this.DataSourceAsExternalSystemEntity.CompanyId;

            string loginUrl = string.Format("https://accounts.google.com/o/oauth2/auth?client_id={0}&response_type=code&state={1}&scope={2}&access_type=offline&approval_prompt=force&redirect_uri={3}&display=popup", clientId, companyId, scope, redirect);

            string html = "<script>window.open('" + loginUrl + "', '', '');</script>";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "popupgoogle", html);
        }

        public ExternalSystemEntity DataSourceAsExternalSystemEntity
        {
            get
            {
                return (this.Page as PageLLBLGenEntityCms).DataSource as ExternalSystemEntity;
            }
        }

    }
}