﻿using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Integrations;
using Obymobi.Integrations.Service.HotSOS;
using Obymobi.ObymobiCms.UI;

namespace Obymobi.ObymobiCms.External.SubPanels.SettingsPanels.Other
{
    public partial class HotSOSSettingsPanel : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.HookUpEvents();

            HidePOSSynchronizeKMZ();
        }

        /// <summary>
        /// Temporary code to disable the POS synchronize on start for Kimpton (KMZ).
        /// </summary>
        private void HidePOSSynchronizeKMZ()
        {
            if (IsKimptonHotelAndNotGodMode())
                this.btnSyncProducts.Visible = false;
        }

        /// <summary>
        /// Temoorary code to decide if the hotel is from Kipton and the user is not in GodMode.
        /// </summary>
        /// <returns>Returns the condition if the hotel is from Kimpton and the user is not in GodMode</returns>
        private bool IsKimptonHotelAndNotGodMode()
        {
            return (CmsSessionHelper.CurrentCompanyId == 509 || CmsSessionHelper.CurrentCompanyId == 514) && CmsSessionHelper.CurrentRole != Role.GodMode;
        }

        private void HookUpEvents()
        {
            this.btnSyncProducts.Click += btnSyncProducts_Click;
            this.btnLinkDeliverypoints.Click += btnLinkDeliverypoints_Click;
        }

        private void btnSyncProducts_Click(object sender, EventArgs e)
        {
            // MTV/RL: Temporay code to disbale the synchronize button for Kimpton.
            if (IsKimptonHotelAndNotGodMode())
                return;

            HotSOSConnector hotSOSConnector = new HotSOSConnector(this.DataSourceAsExternalSystemEntity.CompanyId);
            IEnumerable<Integrations.ExternalProduct> externalProducts = hotSOSConnector.GetExternalProducts();
            hotSOSConnector.ExternalProductSynchronizer.Synchronize(externalProducts);
        }

        private void btnLinkDeliverypoints_Click(object sender, EventArgs e)
        {
            HotSOSConnector hotSOSConnector = new HotSOSConnector(this.DataSourceAsExternalSystemEntity.CompanyId);
            (hotSOSConnector.ExternalDeliverypointSynchronizer as HotSOSDeliverypointSynchronizer).LinkToDeliverypoints();
        }

        public ExternalSystemEntity DataSourceAsExternalSystemEntity
        {
            get
            {
                return (this.Page as PageLLBLGenEntityCms).DataSource as ExternalSystemEntity;
            }
        }
    }
}