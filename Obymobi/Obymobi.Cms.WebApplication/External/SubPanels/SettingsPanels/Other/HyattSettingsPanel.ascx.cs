﻿using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Integrations;
using Obymobi.Integrations.Service.Hyatt;
using Obymobi.ObymobiCms.UI;

namespace Obymobi.ObymobiCms.External.SubPanels.SettingsPanels.Other
{
    public partial class HyattSettingsPanel : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.HookUpEvents();
        }

        private void HookUpEvents()
        {
            this.btnSyncProducts.Click += btnSyncProducts_Click;
        }

        private void btnSyncProducts_Click(object sender, EventArgs e)
        {
            HyattConnector hyattConnector = new HyattConnector(this.DataSourceAsExternalSystemEntity.CompanyId);
            hyattConnector.Authorize().Wait();

            IEnumerable<Integrations.ExternalProduct> externalProducts = hyattConnector.GetExternalProducts();
            hyattConnector.ExternalProductSynchronizer.Synchronize(externalProducts);
        }

        public ExternalSystemEntity DataSourceAsExternalSystemEntity
        {
            get
            {
                return (this.Page as PageLLBLGenEntityCms).DataSource as ExternalSystemEntity;
            }
        }
    }
}