<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.External.SubPanels.SettingsPanels.Other.DeliverectSettingsPanel" Codebehind="DeliverectSettingsPanel.ascx.cs" %>
<table class="dataformV2">
	<tr>
		<td class="label">
			<D:Label runat="server" ID="lblStringValue1">Client ID</D:Label>
		</td>
		<td class="control">
			<D:TextBoxString runat="server" ID="tbStringValue1"></D:TextBoxString>
		</td>
		<td class="label">
			<D:Label runat="server" ID="lblClientSecretKeyDecrypted">Client Key</D:Label>
		</td>
		<td class="control">
			<D:Button ID="btClientSecretKey" Text="Change" runat="server" CssClass="change-button" />
			<div class="encrypted-input">
				<D:TextBoxString ID="tbClientSecretKeyDecrypted" runat="server" CssClass="input encrypted-input"></D:TextBoxString>
			</div>
		</td>
	</tr>
	<tr>
		<td class="label">
			<D:Label runat="server" ID="lblStringValue3">Channel Link ID</D:Label>
		</td>
		<td class="control">
			<D:TextBoxString runat="server" ID="tbStringValue3" ReadOnly="True"></D:TextBoxString>
		</td>
		<td class="label">
			<D:Label runat="server" ID="lblStringValue4">External Location ID</D:Label>
		</td>
		<td class="control">
			<D:TextBoxString runat="server" ID="tbStringValue4" ReadOnly="True"></D:TextBoxString>
		</td>
	</tr>
	<tr>
		<td class="label">
			<D:Label runat="server" ID="lblStringValue5">HMAC Key</D:Label>
		</td>
		<td class="control">
			<D:Button ID="btHmacKey" Text="Change" runat="server" CssClass="change-button" />
			<div class="encrypted-input">
				<D:TextBoxString ID="tbHmacKeyDecrypted" runat="server" CssClass="input encrypted-input"></D:TextBoxString>
			</div>
		</td>
		<td class="label">
			<D:Label runat="server" ID="lblEnvironment">Environment</D:Label>
		</td>
		<td class="control">
			<X:ComboBoxEnum runat="server" ID="cbEnvironment" Type="Obymobi.Enums.ExternalSystemEnvironment, Obymobi" Width="150" />
		</td>
	</tr>
    <tr>
        <td class="label">
            <D:Label runat="server" ID="lblBoolValue2">Prices including taxes</D:Label>
        </td>
        <td class="control">
            <D:CheckBox runat="server" ID="cbBoolValue2"/>
        </td>
        <td class="label"></td>
        <td class="control"></td>
    </tr>
</table>

<D:Panel ID="pnlExternalState" runat="server" GroupingText="Status (Managed by deliverect)">
	<table class="dataformV2">
		<tr>
			<td class="label">
				<D:Label runat="server" ID="lblBoolValue1">Active</D:Label>
			</td>
			<td class="control">
				<D:CheckBox runat="server" ID="cbBoolValue1" Enabled="False" />
			</td>
			<td class="label">
				<D:LabelEntityFieldInfo runat="server" id="lblIsBusy">Busy</D:LabelEntityFieldInfo>
			</td>
			<td class="control">
				<D:CheckBox runat="server" id="cbIsBusy" Enabled="False"/>
			</td>
		</tr>
	</table>
</D:Panel>
