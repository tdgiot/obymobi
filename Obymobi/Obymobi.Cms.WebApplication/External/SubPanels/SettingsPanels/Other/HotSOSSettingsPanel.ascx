﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.External.SubPanels.SettingsPanels.Other.HotSOSSettingsPanel" Codebehind="HotSOSSettingsPanel.ascx.cs" %>
<table class="dataformV2">
    <tr>
        <td class="label">
            <D:Label runat="server" ID="lblStringValue1">Username</D:Label>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbStringValue1"></D:TextBoxString>
        </td>
        <td class="label">
            <D:Label runat="server" ID="lblStringValue2">Password</D:Label>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbStringValue2"></D:TextBoxString>
        </td>
    </tr>
    <tr>
	    <td class="label">
            <D:Label runat="server" ID="lblStringValue3">API Url</D:Label>
	    </td>
	    <td class="control">
		    <D:TextBoxString runat="server" ID="tbStringValue3"></D:TextBoxString>
	    </td>
	    <td class="label">
	    </td>
	    <td class="control">
	    </td>
    </tr>
    <tr>
		<td class="label">
		</td>
		<td class="control">
			<D:Button ID="btnSyncProducts" runat="server" Text="Synchronize products" />
            &nbsp;
            <D:Button ID="btnLinkDeliverypoints" runat="server" Text="Link deliverypoints" />
		</td>
		<td class="label">
			&nbsp;
		</td>
		<td class="control">
			&nbsp;
		</td>
	</tr>
</table>
