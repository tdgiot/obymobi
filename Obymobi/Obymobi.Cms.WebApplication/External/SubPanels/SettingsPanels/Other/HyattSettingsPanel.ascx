﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.External.SubPanels.SettingsPanels.Other.HyattSettingsPanel" Codebehind="HyattSettingsPanel.ascx.cs" %>
<table class="dataformV2">
    <tr>
        <td class="label">
            <D:Label runat="server" ID="lblStringValue1">Application ID</D:Label>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbStringValue1"></D:TextBoxString>
        </td>
        <td class="label">
            <D:Label runat="server" ID="lblStringValue2">API url</D:Label>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbStringValue2"></D:TextBoxString>
        </td>
    </tr>
    <tr>
	    <td class="label">
            <D:Label runat="server" ID="lblStringValue3">Public key</D:Label>
	    </td>
	    <td class="control">
		    <D:TextBoxString runat="server" ID="tbStringValue3"></D:TextBoxString>
	    </td>
	    <td class="label">
            <D:Label runat="server" ID="lblStringValue4">Private key</D:Label>
	    </td>
	    <td class="control">
		    <D:TextBoxString runat="server" ID="tbStringValue4"></D:TextBoxString>
	    </td>
    </tr>
    <tr>
	    <td class="label">
            <D:Label runat="server" ID="lblStringValue5">Hotel ID</D:Label>
	    </td>
	    <td class="control">
		    <D:TextBoxString runat="server" ID="tbStringValue5"></D:TextBoxString>
	    </td>
	    <td class="label">
	    </td>
	    <td class="control">
	    </td>
    </tr>
    <tr>
		<td class="label">
		</td>
		<td class="control">
			<D:Button ID="btnSyncProducts" runat="server" Text="Synchronize products" />
		</td>
		<td class="label">
			&nbsp;
		</td>
		<td class="control">
			&nbsp;
		</td>
	</tr>
</table>
