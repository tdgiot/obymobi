﻿using System;
using System.Web.UI.WebControls;
using Dionysos;
using Dionysos.Interfaces;
using Dionysos.Security.Cryptography;
using Dionysos.Web;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.ObymobiCms.UI;

namespace Obymobi.ObymobiCms.External.SubPanels.SettingsPanels.Other
{
    public partial class DeliverectSettingsPanel : System.Web.UI.UserControl, ISaveableControl
    {
        private ExternalSystemEntity DataSourceAsExternalSystemEntity => (this.Page as PageLLBLGenEntityCms)?.DataSource as ExternalSystemEntity;
        
        public bool Save()
        {
            StoreTextEncrypted(tbClientSecretKeyDecrypted.Text, encryptedClientSecretKey => DataSourceAsExternalSystemEntity.StringValue2 = encryptedClientSecretKey);
            StoreTextEncrypted(tbHmacKeyDecrypted.Text, encryptedHmacKeyDecrypted => DataSourceAsExternalSystemEntity.StringValue5 = encryptedHmacKeyDecrypted);

            void StoreTextEncrypted(string text, Action<string> setEncryptedProperty)
            {
                if (!text.IsNullOrWhiteSpace())
                {
                    setEncryptedProperty(Cryptographer.EncryptUsingCBC(text));
                }
            }

            return true;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetEncryptedControls(tbClientSecretKeyDecrypted, btClientSecretKey, DataSourceAsExternalSystemEntity.StringValue2, BtChangeClientSecretKey_Click);
            SetEncryptedControls(tbHmacKeyDecrypted, btHmacKey, DataSourceAsExternalSystemEntity.StringValue5, BtHmacKey_Click);

            void SetEncryptedControls(WebControl tbDecrypted, Button btChange, string propertyValue, EventHandler clickEventHandler)
            {
                btChange.Click += clickEventHandler;
                bool hasValue = !propertyValue.IsNullOrWhiteSpace();
                tbDecrypted.Enabled = !hasValue;
                btChange.Visible = hasValue;
            }
        }

        private void BtChangeClientSecretKey_Click(object sender, EventArgs e)
        {
            tbClientSecretKeyDecrypted.Enabled = true;
            btClientSecretKey.Visible = false;
        }

        private void BtHmacKey_Click(object sender, EventArgs e)
        {
            tbHmacKeyDecrypted.Enabled = true;
            btHmacKey.Visible = false;
        }

        public override void Dispose()
        {
            btClientSecretKey.Click -= BtChangeClientSecretKey_Click;
            btHmacKey.Click -= BtHmacKey_Click;

            base.Dispose();
        }
    }
}
