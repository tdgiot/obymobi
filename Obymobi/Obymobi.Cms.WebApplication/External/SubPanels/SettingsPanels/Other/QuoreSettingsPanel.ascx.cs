﻿using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Integrations;
using Obymobi.Integrations.Service.Quore;
using Obymobi.ObymobiCms.UI;

namespace Obymobi.ObymobiCms.External.SubPanels.SettingsPanels.Other
{
    public partial class QuoreSettingsPanel : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.HookUpEvents();
        }        

        private void HookUpEvents()
        {
            this.btnSyncProducts.Click += btnSyncProducts_Click;
            this.btnSyncDeliverypoints.Click += btnSyncDeliverypoints_Click;
            this.btnLinkDeliverypoints.Click += btnLinkDeliverypoints_Click;
        }

        private void btnSyncProducts_Click(object sender, EventArgs e)
        {
            try
            {
                QuoreConnector quoreConnector = new QuoreConnector(this.DataSourceAsExternalSystemEntity.CompanyId);
                IEnumerable<Integrations.ExternalProduct> externalProducts = quoreConnector.GetExternalProducts();
                quoreConnector.ExternalProductSynchronizer.Synchronize(externalProducts);
            }
            catch (Exception ex)
            {
                this.tbErrorMessage.Visible = true;
                this.tbErrorMessage.Text = ex.Message;
            }
        }

        private void btnSyncDeliverypoints_Click(object sender, EventArgs e)
        {
            try
            {
                QuoreConnector quoreConnector = new QuoreConnector(this.DataSourceAsExternalSystemEntity.CompanyId);
                IEnumerable<Integrations.ExternalDeliverypoint> externalDeliverypoints = quoreConnector.GetExternalDeliverypoints();
                quoreConnector.ExternalDeliverypointSynchronizer.Synchronize(externalDeliverypoints);
            }
            catch (Exception ex)
            {
                this.tbErrorMessage.Visible = true;
                this.tbErrorMessage.Text = ex.Message;
            }
        }

        private void btnLinkDeliverypoints_Click(object sender, EventArgs e)
        {
            QuoreConnector quoreConnector = new QuoreConnector(this.DataSourceAsExternalSystemEntity.CompanyId);
            (quoreConnector.ExternalDeliverypointSynchronizer as QuoreDeliverypointSynchronizer).LinkToDeliverypoints();
        }

        public ExternalSystemEntity DataSourceAsExternalSystemEntity
        {
            get
            {
                return (this.Page as PageLLBLGenEntityCms).DataSource as ExternalSystemEntity;
            }
        }
    }
}