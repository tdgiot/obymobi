﻿using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Integrations.Service.Alice;
using Obymobi.ObymobiCms.UI;

namespace Obymobi.ObymobiCms.External.SubPanels.SettingsPanels.Other
{
    public partial class AliceSettingsPanel : System.Web.UI.UserControl
    {
        protected void SyncProducts(object sender, EventArgs e)
        {
            AliceConnector connector = new AliceConnector(this.DataSourceAsExternalSystemEntity.CompanyId);

            IEnumerable<Integrations.ExternalProduct> externalProducts = connector.GetExternalProducts();
            connector.ExternalProductSynchronizer.Synchronize(externalProducts);
        }

        protected void SyncDeliverypoints(object sender, EventArgs e)
        {
            AliceConnector connector = new AliceConnector(this.DataSourceAsExternalSystemEntity.CompanyId);

            IEnumerable<Integrations.ExternalDeliverypoint> externalDeliverypoints = connector.GetExternalDeliverypoints();
            connector.ExternalDeliverypointSynchronizer.Synchronize(externalDeliverypoints);
        }

        protected void LinkDeliverypoints(object sender, EventArgs e)
        {
            AliceConnector connector = new AliceConnector(this.DataSourceAsExternalSystemEntity.CompanyId);
            ((AliceDeliverypointSynchronizer) connector.ExternalDeliverypointSynchronizer).LinkToDeliverypoints();
        }

        private ExternalSystemEntity DataSourceAsExternalSystemEntity => (this.Page as PageLLBLGenEntityCms)?.DataSource as ExternalSystemEntity;
    }
}