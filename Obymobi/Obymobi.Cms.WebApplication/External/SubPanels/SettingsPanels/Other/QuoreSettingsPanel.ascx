﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.External.SubPanels.SettingsPanels.Other.QuoreSettingsPanel" Codebehind="QuoreSettingsPanel.ascx.cs" %>
                    <table class="dataformV2">
                        <tr>
							<td class="label">
								<D:Label runat="server" ID="lblStringValue1" LocalizeText="false" Text="API url"></D:Label>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbStringValue1" runat="server"></D:TextBoxString>
							</td>
							<td class="label">
								<D:Label runat="server" ID="lblStringValue2" LocalizeText="false" Text="API token"></D:Label>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbStringValue2" runat="server"></D:TextBoxString>
							</td>
						</tr>	
                        <tr>
							<td class="label">
							</td>
							<td class="control">
								<D:Button ID="btnSyncProducts" runat="server" Text="Synchronize products" />
                                &nbsp;
                                <D:Button ID="btnSyncDeliverypoints" runat="server" Text="Synchronize deliverypoints" />
                                &nbsp;
                                <D:Button ID="btnLinkDeliverypoints" runat="server" Text="Link deliverypoints" />
							</td>
							<td class="label">
                                <D:TextBoxMultiLine ID="tbErrorMessage" Rows="10" ReadOnly="true" runat="server" Visible="false"/>
							</td>
							<td class="control">
								&nbsp;
							</td>
						</tr>
                    </table>