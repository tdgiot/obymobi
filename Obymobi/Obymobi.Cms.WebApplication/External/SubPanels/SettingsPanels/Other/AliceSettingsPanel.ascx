﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.External.SubPanels.SettingsPanels.Other.AliceSettingsPanel" Codebehind="AliceSettingsPanel.ascx.cs" %>
<table class="dataformV2">
	<tr>
		<td class="label">
			<D:Label runat="server" ID="lblStringValue1">API url</D:Label>
		</td>
		<td class="control">
			<D:TextBoxString runat="server" ID="tbStringValue1"></D:TextBoxString>
		</td>
		<td class="label">
			<D:Label runat="server" ID="lblStringValue2">API key</D:Label>
		</td>
		<td class="control">
			<D:TextBoxString runat="server" ID="tbStringValue2"></D:TextBoxString>
		</td>
	</tr>
	<tr>
		<td class="label">
			<D:Label runat="server" ID="lblStringValue3">API Username</D:Label>
		</td>
		<td class="control">
			<D:TextBoxString runat="server" ID="tbStringValue3"></D:TextBoxString>
		</td>
		<td class="label">
			<D:Label runat="server" ID="lblStringValue4">API Password</D:Label>
		</td>
		<td class="control">
			<D:TextBoxString runat="server" ID="tbStringValue4"></D:TextBoxString>
		</td>
	</tr>
	<tr>
		<td class="label">
			<D:Label runat="server" ID="lblStringValue5">Hotel ID</D:Label>
		</td>
		<td class="control">
			<D:TextBoxString runat="server" ID="tbStringValue5"></D:TextBoxString>
		</td>
		<td class="label"></td>
		<td class="control"></td>
	</tr>
	<tr>
		<td class="label"></td>
		<td class="control">
			<D:Button ID="btnSyncProducts" runat="server" OnClick="SyncProducts" Text="Synchronize products" />
			<D:Button ID="btnSyncDeliverypoints" runat="server" OnClick="SyncDeliverypoints" Text="Synchronize deliverypoints" />
			<D:Button ID="btnLinkDeliverypoints" runat="server" OnClick="LinkDeliverypoints" Text="Link deliverypoints" />
		</td>
		<td class="label"></td>
		<td class="control"></td>
	</tr>
	<tr>
		<td class="label"></td>
		<td class="control">
			<D:TextBoxMultiLine ID="tbTestResult" Rows="10" runat="server" Visible="false"/>
		</td>
		<td class="label"></td>
		<td class="control"></td>
	</tr>
</table>
