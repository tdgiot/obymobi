﻿using System;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;

namespace Obymobi.ObymobiCms.External.SubPanels
{
    public partial class ExternalProductCollection : Dionysos.Web.UI.DevExControls.SubPanelLLBLOverviewDataSourceCollection
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            
            EntityName = "ExternalProduct";
            EntityPageUrl = "~/External/ExternalProduct.aspx";

            PageAsPageLLBLGenEntity.DataSourceLoaded += PageAsPageLLBLGenEntity_DataSourceLoaded;
        }

        private void PageAsPageLLBLGenEntity_DataSourceLoaded(object sender)
        {
            btnAddExternalProduct.Visible = ExternalSystemDataSource.Type != ExternalSystemType.Deliverect;
        }

        public ExternalSystemEntity ExternalSystemDataSource => Parent.DataSource as ExternalSystemEntity;
    }
}