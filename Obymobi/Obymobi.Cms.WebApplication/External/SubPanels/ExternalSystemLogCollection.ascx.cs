﻿using System;
using DevExpress.Data;
using DevExpress.Web;
using Dionysos.Web.UI.DevExControls;
using Obymobi.Data.HelperClasses;

namespace Obymobi.ObymobiCms.External.SubPanels
{
    public partial class ExternalSystemLogCollection : SubPanelLLBLOverviewDataSourceCollection
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            
            EntityName = "ExternalSystemLog";

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            MainGridView.ShowColumnSelectorButton = false;
            MainGridView.ShowEditColumn = false;
            MainGridView.ShowEditHyperlinkColumn = false;
            MainGridView.ShowDeleteHyperlinkColumn = false;
            MainGridView.ShowDeleteColumn = false;

            MainGridView.DataBound += OnDataBound;
            MainGridView.HtmlDataCellPrepared += OnHtmlDataCellPrepared;
            //MainGridView.HtmlRowPrepared += OnHtmlRowPrepared;

            btnRefresh.Click += (s, args) => {};
        }

        private void OnDataBound(object o, EventArgs args)
        {
            if (MainGridView.Columns[nameof(ExternalSystemLogFields.CreatedUTC)] is GridViewDataColumn createdColumn)
            {
                MainGridView.SortBy(createdColumn, ColumnSortOrder.Descending);
            }
        }

        private void OnHtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            switch (e.DataColumn.FieldName)
            {
                case "OrderEntity.OrderId":
                    PrepareOrderIdCellValue(e); 
                    break;
            }
        }

        private static void PrepareOrderIdCellValue(ASPxGridViewTableDataCellEventArgs e)
        {
            if (!(e.CellValue is int orderId))
            {
                return;
            }

            e.Cell.Text = orderId == 0
                ? string.Empty
                : $"<a href=\"../Company/Order.aspx?id={orderId}\" target=\"_blank\">{orderId}</a>";
        }

        //private void OnHtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        //{
        //    SetCellUrl("OrderEntity.OrderId", $"../Company/Order.aspx?id=",  e);
        //}

        //private void SetCellUrl(string fieldName, string url, ASPxGridViewTableRowEventArgs e)
        //{
        //    if (MainGridView.Columns[fieldName] == null || e.GetValue(fieldName) == null)
        //        return;

        //    string text = e.GetValue(fieldName).ToString();
        //    string value = $"<a href=\"{url}\" target=\"_blank\">{text}</a>";

        //    SetCellValue(e.Row, fieldName, value);
        //}
    }
}