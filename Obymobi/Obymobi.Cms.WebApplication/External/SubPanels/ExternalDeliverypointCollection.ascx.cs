﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Dionysos;
using Dionysos.Data.LLBLGen;
using Dionysos.Web.UI;
using Dionysos.Web.UI.DevExControls;
using Obymobi.Data.EntityClasses;
using ServiceStack.Text;

namespace Obymobi.ObymobiCms.External.SubPanels
{
    public partial class ExternalDeliverypointCollection : SubPanelLLBLOverviewDataSourceCollection
    {
        private ExternalSystemEntity DataSourceAsExternalSystemEntity => PageAsPageLLBLGenEntity.DataSource as ExternalSystemEntity;

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            EntityName = "ExternalDeliverypoint";
            EntityPageUrl = "~/External/ExternalDeliverypoint.aspx";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            HookUpEvents();
        }

        private void HookUpEvents()
        {
            btnBatchCreateExternalDeliverypoints.Click += (sender, args) => TryExecuteActionForImportedDeliverypoints();
        }

        private void TryExecuteActionForImportedDeliverypoints()
        {
            if (!fuExternalDeliverypoints.HasFile)
            {
                AddInformator(InformatorType.Warning, "No file selected");
                return;
            }

            if (!Path.GetExtension(fuExternalDeliverypoints.FileName).EndsWith(".csv"))
            {
                AddInformator(InformatorType.Warning, "Only csv files are supported");
                return;
            }

            IEnumerable<ExternalDeliverypoint> externalDeliverypoints =
                CsvSerializer.DeserializeFromStream<List<ExternalDeliverypoint>>(fuExternalDeliverypoints.FileContent)
                .Where(externalDeliverypoint => !externalDeliverypoint.Id.IsNullOrWhiteSpace());

            ExecuteActionForImportedExternalDeliverypoints(externalDeliverypoints);
        }

        private void ExecuteActionForImportedExternalDeliverypoints(IEnumerable<ExternalDeliverypoint> externalDeliverypoints)
        {
            CreateExternalDeliverypoints(externalDeliverypoints);
        }

        private void CreateExternalDeliverypoints(IEnumerable<ExternalDeliverypoint> externalDeliverypoints)
        {

            int deliverypointsCreated = externalDeliverypoints
                                .Where(externalDeliverypoint => !DataSourceAsExternalSystemEntity.ExternalDeliverypointCollection
                                    .Select(deliverypointEntity => deliverypointEntity.Id)
                                    .Contains(externalDeliverypoint.Id))
                                .Select(externalDeliverypoint => new ExternalDeliverypointEntity
                                {
                                    ExternalSystemId = DataSourceAsExternalSystemEntity.ExternalSystemId,
                                    ParentCompanyId = DataSourceAsExternalSystemEntity.CompanyId,
                                    Id = externalDeliverypoint.Id,
                                    Name = !externalDeliverypoint.Name.IsNullOrWhiteSpace()
                                        ? externalDeliverypoint.Name
                                        : externalDeliverypoint.Id
                                })
                                .ToEntityCollection<Data.CollectionClasses.ExternalDeliverypointCollection>()
                                .SaveMulti();

            AddInformator(InformatorType.Information, $"{deliverypointsCreated} external deliverypoint(s) created");
        }

        private class ExternalDeliverypoint
        {
            public string Id { get; set; }
            public string Name { get; set; }
        }
    }
}
