﻿using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.ObymobiCms.UI;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;

namespace Obymobi.ObymobiCms.External
{
    public partial class DeliverypointExternalDeliverypoint : PageLLBLGenEntityCms
    {
        protected override void OnInit(EventArgs e)
        {
            this.DataSourceLoaded += DeliverypointExternalDeliverypoint_DataSourceLoaded;
            base.OnInit(e);
        }

        private void LoadUserControls()
        {
            this.LoadDeliverypoints();
            this.LoadExternalDeliverypoints();
        }

        private void LoadDeliverypoints()
        {
            PredicateExpression filter = new PredicateExpression { DeliverypointFields.CompanyId == CmsSessionHelper.CurrentCompanyId };
            RelationCollection relations = new RelationCollection { DeliverypointEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId };

            PrefetchPath prefetch = new PrefetchPath(EntityType.DeliverypointEntity);
            prefetch.Add(DeliverypointEntity.PrefetchPathDeliverypointgroupEntity, new IncludeFieldsList(DeliverypointgroupFields.Name));

            IncludeFieldsList fields = new IncludeFieldsList { DeliverypointFields.Name };

            SortExpression sort = new SortExpression();
            sort.Add(new SortClause(DeliverypointgroupFields.Name, SortOperator.Ascending));
            sort.Add(new SortClause(DeliverypointFields.Name, SortOperator.Ascending));

            DeliverypointCollection deliverypointCollection = new DeliverypointCollection();
            deliverypointCollection.GetMulti(filter, 0, sort, relations, prefetch, fields, 0, 0);

            this.cbDeliverypointId.DataSource = deliverypointCollection;
            this.cbDeliverypointId.DataBind();
        }

        private void LoadExternalDeliverypoints()
        {
            PredicateExpression filter = new PredicateExpression { ExternalSystemFields.CompanyId == CmsSessionHelper.CurrentCompanyId };
            RelationCollection relations = new RelationCollection { ExternalSystemEntity.Relations.ExternalDeliverypointEntityUsingExternalSystemId };

            PrefetchPath prefetch = new PrefetchPath(EntityType.ExternalDeliverypointEntity);
            prefetch.Add(ExternalDeliverypointEntity.PrefetchPathExternalSystemEntity, new IncludeFieldsList { ExternalSystemFields.Name });

            SortExpression sort = new SortExpression();
            sort.Add(new SortClause(ExternalSystemFields.Name, SortOperator.Ascending));
            sort.Add(new SortClause(ExternalDeliverypointFields.Name, SortOperator.Ascending));
            sort.Add(new SortClause(ExternalDeliverypointFields.Id, SortOperator.Ascending));

            ExternalDeliverypointCollection externalDeliverypointCollection = new ExternalDeliverypointCollection();
            externalDeliverypointCollection.GetMulti(filter, 0, sort, relations, prefetch, 0, 0);

            this.cbExternalDeliverypointId.DataSource = externalDeliverypointCollection;
            this.cbExternalDeliverypointId.DataBind();
        }

        private void DeliverypointExternalDeliverypoint_DataSourceLoaded(object sender)
        {
            this.LoadUserControls();
        }

        public DeliverypointExternalDeliverypointEntity DataSourceAsDeliverypointExternalDeliverypointEntity
        {
            get
            {
                return this.DataSource as DeliverypointExternalDeliverypointEntity;
            }
        }
    }
}
