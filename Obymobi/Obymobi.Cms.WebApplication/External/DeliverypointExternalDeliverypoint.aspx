﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" EnableSessionState="ReadOnly" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.External.DeliverypointExternalDeliverypoint" Title="Deliverypoint external deliverypoint" Codebehind="DeliverypointExternalDeliverypoint.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" runat="Server">
</asp:Content>
<asp:Content ID="Conent3" ContentPlaceHolderID="cplhAdditionalToolBarButtons" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhPageContent" runat="Server">
    <div>
        <X:PageControl ID="tabsMain" runat="server" Width="100%">
            <TabPages>
                <X:TabPage Text="Algemeen" Name="Generic">
                    <Controls>
					    <table class="dataformV2">
						    <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblDeliverypointId">Delivery point</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <X:ComboBoxLLBLGenEntityCollection ID="cbDeliverypointId" runat="server" EntityName="Deliverypoint" IsRequired="true" PreventEntityCollectionInitialization="true" TextField="DeliverypointgroupNameDeliverypointNumber" ></X:ComboBoxLLBLGenEntityCollection>
							    </td>
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblExternalDeliverypointId">External delivery point</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <X:ComboBoxLLBLGenEntityCollection ID="cbExternalDeliverypointId" runat="server" EntityName="ExternalDeliverypoint" IsRequired="true" PreventEntityCollectionInitialization="true" TextField="NameFull" ></X:ComboBoxLLBLGenEntityCollection>
							    </td>
						    </tr>
                        </table>
                    </Controls>
                </X:TabPage>             		
            </TabPages>
        </X:PageControl>
    </div>
</asp:Content>
