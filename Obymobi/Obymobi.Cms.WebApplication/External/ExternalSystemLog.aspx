﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" EnableSessionState="ReadOnly" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.External.ExternalSystemLog" Title="External system" Codebehind="ExternalSystemLog.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" runat="Server">
</asp:Content>
<asp:Content ID="Conent3" ContentPlaceHolderID="cplhAdditionalToolBarButtons" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhPageContent" runat="Server">
    <div>
        <X:PageControl ID="tabsMain" runat="server" Width="100%">
            <TabPages>
                <X:TabPage Text="Algemeen" Name="Generic">
                    <Controls>
					    <table class="dataformV2">
						    <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblMessage">Name</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <D:Label ID="lblMessageValue" runat="server" LocalizeText="false" />
							    </td>
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblLogType">Type</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <D:Label ID="lblLogTypeValue" runat="server" LocalizeText="false" />
							    </td>
						    </tr>
							<tr>
								<td class="label">
									<D:LabelEntityFieldInfo runat="server" id="lblSuccess">Success</D:LabelEntityFieldInfo>
								</td>
								<td class="control">
									<D:CheckBox runat="server" id="cbSuccess" Enabled="False" />
								</td>
								<td class="label">
									<D:LabelEntityFieldInfo runat="server" id="lblOrderId"></D:LabelEntityFieldInfo>
								</td>
								<td class="control">
									<D:Label runat="server" id="lblOrderIdValue" LocalizeText="false"></D:Label>
								</td>
							</tr>
                            <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" ID="lblRaw" LocalizeText="False">Raw</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control" colspan="3">
								    <D:TextBoxMultiLine ID="tbRaw" runat="server" PrettyPrintJson="true" LocalizeText="false" Rows="30" ReadOnly="true" BackColor="White" />
							    </td>
						    </tr>
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" ID="lblLog" LocalizeText="False">Log</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control" colspan="3">
                                    <D:TextBoxMultiLine ID="tbLog" runat="server" LocalizeText="false" Rows="30" ReadOnly="true" BackColor="White" />
                                </td>
                            </tr>
                        </table>
                    </Controls>
                </X:TabPage>             		
            </TabPages>
        </X:PageControl>
    </div>
</asp:Content>
