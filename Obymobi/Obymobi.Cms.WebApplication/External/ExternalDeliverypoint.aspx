﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" EnableSessionState="ReadOnly" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.External.ExternalDeliverypoint" Title="External deliverypoint" Codebehind="ExternalDeliverypoint.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" runat="Server">
</asp:Content>
<asp:Content ID="Conent3" ContentPlaceHolderID="cplhAdditionalToolBarButtons" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhPageContent" runat="Server">
    <div>
        <X:PageControl ID="tabsMain" runat="server" Width="100%">
            <TabPages>
                <X:TabPage Text="Algemeen" Name="Generic">
                    <Controls>
					    <table class="dataformV2">
						    <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblName">Name</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
							    </td>
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblExternalSystemId">Type</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <X:ComboBoxLLBLGenEntityCollection ID="cbExternalSystemId" runat="server" EntityName="ExternalSystem" ReadOnly="true" IsRequired="true" ></X:ComboBoxLLBLGenEntityCollection>
							    </td>
						    </tr>
						    <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblId">ID</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <D:TextBoxString ID="tbId" runat="server" IsRequired="true"></D:TextBoxString>
							    </td>
                                <td class="label">
							    </td>
							    <td class="control">
							    </td>
						    </tr>
                        </table>
                    </Controls>
                </X:TabPage>             		
            </TabPages>
        </X:PageControl>
    </div>
</asp:Content>
