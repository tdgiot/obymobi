﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.External.ExternalMenu" ValidateRequest="false" Codebehind="ExternalMenu.aspx.cs" %>
<%@ Register TagPrefix="dxwtl" Namespace="DevExpress.Web.ASPxTreeList" Assembly="DevExpress.Web.ASPxTreeList.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
	            <Controls>
                    <D:PlaceHolder runat="server" ID="plhGeneric">
		            <table class="dataformV2">
			            <tr>
				            <td class="label">
					            <D:Label runat="server" id="lblName">Name</D:Label>
				            </td>
                            <td class="control">
					            <D:TextBoxString ID="tbName" runat="server" IsRequired="true" Enabled="false"></D:TextBoxString>
				            </td>
                            <td class="label">
				            </td>
				            <td class="control">                                
				            </td>				            
			            </tr>                   
                        <tr>
                            <td class="label">
                                <D:Label runat="server" id="lblCategories">Categories</D:Label>
				            </td>
                            <td class="control" colspan="3">
                                <div style="width: 736px;">
                                    <span class="nobold" style="width: 100%; height: 24px; margin-left: 0;">
                                        <D:HyperLink runat="server" ID="hlExpandAll">Expand all</D:HyperLink> | <D:HyperLink runat="server" ID="hlCollapseAll">Collapse all</D:HyperLink>                                                                                
                                    </span>                                    
                                    <dxwtl:ASPxTreeList ID="tlItems" ClientInstanceName="tlItems" runat="server" EnableViewState="false" AutoGenerateColumns="False">
                                        <SettingsSelection Enabled="true" Recursive="false" />                                                
                                    </dxwtl:ASPxTreeList>    
                                </div>
                            </td>
                        </tr>
			        </table>	
                    </D:PlaceHolder>
	            </Controls>
            </X:TabPage>            
        </TabPages>
    </X:PageControl>
</div> 
</asp:Content>