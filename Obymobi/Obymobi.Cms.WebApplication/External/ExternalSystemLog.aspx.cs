﻿using Obymobi.Data.EntityClasses;
using Obymobi.ObymobiCms.UI;
using System;
using Obymobi.ObymobiCms.MasterPages;

namespace Obymobi.ObymobiCms.External
{
    public partial class ExternalSystemLog : PageLLBLGenEntityCms
    {
        protected override void OnInit(EventArgs e)
        {
            this.DataSourceLoaded += ExternalSystemLog_DataSourceLoaded;
            base.OnInit(e);
        }

        private void ExternalSystemLog_DataSourceLoaded(object sender)
        {
            lblMessageValue.Text = DataSourceAsExternalSystemLogEntity.Message;
            lblLogTypeValue.Text = DataSourceAsExternalSystemLogEntity.TypeText;
            cbSuccess.Value = DataSourceAsExternalSystemLogEntity.Success;
            tbRaw.Text = DataSourceAsExternalSystemLogEntity.Raw;
            lblOrderIdValue.Text = $"{DataSourceAsExternalSystemLogEntity.OrderId}";
            tbLog.Text = DataSourceAsExternalSystemLogEntity.Log;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Master is MasterPageEntity masterPage)
            {
                masterPage.ToolBar.SaveButton.Visible = false;
                masterPage.ToolBar.SaveAndGoButton.Visible = false;
                masterPage.ToolBar.SaveAndNewButton.Visible = false;
                masterPage.ToolBar.DeleteButton.Visible = false;
            }
        }

        public ExternalSystemLogEntity DataSourceAsExternalSystemLogEntity => this.DataSource as ExternalSystemLogEntity;
    }
}
