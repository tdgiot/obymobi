﻿using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.ObymobiCms.UI;
using System;
using System.Linq;
using Dionysos.Interfaces;
using Dionysos.Web;
using Obymobi.ObymobiCms.External.SubPanels.SettingsPanels.Other;
using Obymobi.ObymobiCms.MasterPages;

namespace Obymobi.ObymobiCms.External
{
    public partial class ExternalSystem : PageLLBLGenEntityCms
    {
        private readonly bool CanViewLogs = CmsSessionHelper.CurrentRole >= Role.Crave;

        private DisplayStrategy displayStrategy;

        protected override void OnInit(EventArgs e)
        {
            if (PageMode != PageMode.Add)
            {
                tabsMain.AddTabPage("Products", "Products", "~/External/SubPanels/ExternalProductCollection.ascx");
                tabsMain.AddTabPage("Deliverypoints", "Deliverypoints", "~/External/SubPanels/ExternalDeliverypointCollection.ascx");

                if (CanViewLogs)
                {
                    tabsMain.AddTabPage("Logs", "Logs", "~/External/SubPanels/ExternalSystemLogCollection.ascx");
                }
            }

            this.DataSourceLoaded += ExternalSystem_DataSourceLoaded;
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Master is MasterPageEntity masterPage && masterPage.ToolBar.DeleteButton != null)
            {
                masterPage.ToolBar.DeleteButton.Visible = false;
            }
        }

        public override bool Save()
        {
            if (displayStrategy != null && !displayStrategy.Save())
            {
                return false;
            }

            if (PageMode == PageMode.Add && DataSourceAsExternalSystemEntity.Type == ExternalSystemType.Deliverect)
            {
                DataSourceAsExternalSystemEntity.BoolValue2 = true;
            }

            return base.Save();
        }

        private void ExternalSystem_DataSourceLoaded(object sender)
        {
            displayStrategy = DisplayStrategyFactory.Create(PageMode, DataSourceAsExternalSystemEntity.Type);
            displayStrategy.ConfigureVisibility(this);
        }
        
        public ExternalSystemEntity DataSourceAsExternalSystemEntity => this.DataSource as ExternalSystemEntity;

        private abstract class DisplayStrategy
        {
            protected ISaveableControl Panel { get; set; }

            public virtual void ConfigureVisibility(ExternalSystem externalSystem)
            {
                externalSystem.ddlType.Enabled = false;
            }

            public bool Save()
            {
                if (Panel is ISaveableControl panel)
                {
                    return panel.Save();
                }

                return true;
            }
        }

        private static class DisplayStrategyFactory
        {
            public static DisplayStrategy Create(PageMode pageMode, ExternalSystemType externalSystemType)
            {
                if (pageMode == PageMode.Add)
                {
                    return new AddExternalSystemStrategy();
                }

                if (pageMode != PageMode.View && 
                    pageMode != PageMode.Edit)
                {
                    return new DefaultDisplayStrategy();
                }

                switch (externalSystemType)
                {
                    case ExternalSystemType.Deliverect: return new DeliverectStrategy();
                    case ExternalSystemType.Alice: return new AliceDisplayStrategy();
                    case ExternalSystemType.HotSOS: return new HotSOSStrategy();
                    case ExternalSystemType.Quore: return new QuoreStrategy();
                    case ExternalSystemType.Google: return new GoogleStrategy();
                    case ExternalSystemType.Hyatt_HotSOS: return new HyattStrategy();
                    default: return new DefaultDisplayStrategy();
                }
            }
        }

        private class AddExternalSystemStrategy : DisplayStrategy
        {
            public override void ConfigureVisibility(ExternalSystem externalSystem)
            {
                externalSystem.ddlType.Enabled = true;
            }
        }

        private class DefaultDisplayStrategy : DisplayStrategy
        {

        }

        private class AliceDisplayStrategy : DisplayStrategy
        {
            public override void ConfigureVisibility(ExternalSystem externalSystem)
            {
                base.ConfigureVisibility(externalSystem);

                AliceSettingsPanel alicePanel = externalSystem.LoadControl<AliceSettingsPanel>("~/External/SubPanels/SettingsPanels/Other/AliceSettingsPanel.ascx");
                externalSystem.plhSettings.Controls.Add(alicePanel);
            }
        }

        private class HotSOSStrategy : DisplayStrategy
        {
            public override void ConfigureVisibility(ExternalSystem externalSystem)
            {
                base.ConfigureVisibility(externalSystem);

                HotSOSSettingsPanel hotSOSPanel = externalSystem.LoadControl<HotSOSSettingsPanel>("~/External/SubPanels/SettingsPanels/Other/HotSOSSettingsPanel.ascx");
                externalSystem.plhSettings.Controls.Add(hotSOSPanel);
            }
        }

        private class QuoreStrategy : DisplayStrategy
        {
            public override void ConfigureVisibility(ExternalSystem externalSystem)
            {
                base.ConfigureVisibility(externalSystem);
                
                QuoreSettingsPanel quorePanel = externalSystem.LoadControl<QuoreSettingsPanel>("~/External/SubPanels/SettingsPanels/Other/QuoreSettingsPanel.ascx");
                externalSystem.plhSettings.Controls.Add(quorePanel);
            }
        }

        private class GoogleStrategy : DisplayStrategy
        {
            public override void ConfigureVisibility(ExternalSystem externalSystem)
            {
                base.ConfigureVisibility(externalSystem);

                GoogleSettingsPanel googlePanel = externalSystem.LoadControl<GoogleSettingsPanel>("~/External/SubPanels/SettingsPanels/Other/GoogleSettingsPanel.ascx");
                externalSystem.plhSettings.Controls.Add(googlePanel);
            }
        }
        
        private class HyattStrategy : DisplayStrategy
        {
            public override void ConfigureVisibility(ExternalSystem externalSystem)
            {
                base.ConfigureVisibility(externalSystem);

                HyattSettingsPanel hyattPanel = externalSystem.LoadControl<HyattSettingsPanel>("~/External/SubPanels/SettingsPanels/Other/HyattSettingsPanel.ascx");
                externalSystem.plhSettings.Controls.Add(hyattPanel);
            }
        }

        private class DeliverectStrategy : DisplayStrategy
        {
            public override void ConfigureVisibility(ExternalSystem externalSystem)
            {
                base.ConfigureVisibility(externalSystem);

                externalSystem.tabsMain.TabPages.FindByName("Deliverypoints").Visible = false;

                DeliverectSettingsPanel deliverectPanel = externalSystem.LoadControl<DeliverectSettingsPanel>("~/External/SubPanels/SettingsPanels/Other/DeliverectSettingsPanel.ascx");
                externalSystem.plhSettings.Controls.Add(deliverectPanel);

                Panel = deliverectPanel;
            }
        }
    }
}
