﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" EnableSessionState="ReadOnly" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.External.ExternalProduct" Title="External product" Codebehind="ExternalProduct.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" runat="Server">
</asp:Content>
<asp:Content ID="Conent3" ContentPlaceHolderID="cplhAdditionalToolBarButtons" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhPageContent" runat="Server">
    <div>
        <X:PageControl ID="tabsMain" runat="server" Width="100%">
            <TabPages>
                <X:TabPage Text="Algemeen" Name="Generic">
                    <Controls>
					    <table class="dataformV2">
						    <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblName">Name</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
							    </td>
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblExternalSystemId">Type</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <X:ComboBoxLLBLGenEntityCollection ID="cbExternalSystemId" runat="server" EntityName="ExternalSystem" Enabled="False"></X:ComboBoxLLBLGenEntityCollection>
							    </td>
						    </tr>
						    <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblId">ID</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <D:TextBoxString ID="tbId" runat="server" IsRequired="true"></D:TextBoxString>
							    </td>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblType">Type</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <X:ComboBoxEnum ID="ddlType" runat="server" Type="Obymobi.Enums.ExternalProductType, Obymobi" Enabled="False"></X:ComboBoxEnum> 
							    </td>
						    </tr>
						    <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblPrice">Price</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <D:TextBoxDecimal ID="tbPrice" runat="server" IsRequired="true"></D:TextBoxDecimal>
							    </td>
							    <td class="label"></td>
							    <td class="control"></td>
						    </tr>
					    </table>
						<D:PlaceHolder ID="plhModifierGroup" runat="server">
							<table class="dataformV2">
								<tr>
									<td class="label">
										<D:LabelEntityFieldInfo runat="server" id="lblMinOptions">Minimum Options</D:LabelEntityFieldInfo>
									</td>
									<td class="control">
										<D:TextBoxInt runat="server" ID="tbMinOptions" AllowZero="true" AllowNegative="false"></D:TextBoxInt>
									</td>
									<td class="label">
										<D:LabelEntityFieldInfo runat="server" id="lblMaxOptions">Maximum Options</D:LabelEntityFieldInfo>
									</td>
									<td class="control">
										<D:TextBoxInt runat="server" ID="tbMaxOptions" AllowZero="true" AllowNegative="false"></D:TextBoxInt>
									</td>
								</tr>
								<tr>
									<td class="label">
										<D:LabelEntityFieldInfo id="lblAllowDuplicates" runat="server">Allow Duplicates</D:LabelEntityFieldInfo>
									</td>
									<td class="control">
										<D:CheckBox runat="server" ID="cbAllowDuplicates" />
									</td>
									<td class="label"></td>
									<td class="control"></td>
								</tr>
							</table>
	                    </D:PlaceHolder>
                    </Controls>
                </X:TabPage>             		
            </TabPages>
        </X:PageControl>
    </div>
</asp:Content>
