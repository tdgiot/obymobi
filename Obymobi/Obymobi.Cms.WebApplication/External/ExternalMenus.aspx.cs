﻿using System;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.External
{
    public partial class ExternalMenus : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        #region Fields

        #endregion

        #region Methods

        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "Menu";
            this.EntityPageUrl = "~/External/ExternalMenu.aspx";
            base.OnInit(e);
        }

        #endregion

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.DataSource is LLBLGenProDataSource dataSource)
            {
                dataSource.FilterToUse = new PredicateExpression(MenuFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            }

            this.MainGridView.ShowDeleteHyperlinkColumn = false;
            this.MainGridView.ShowDeleteColumn = false;

            if (this.Master is MasterPages.MasterPageEntityCollection masterPageEntityCollection)
            {
                masterPageEntityCollection.ToolBar.AddButton.Visible = false;
            }
        }

        #endregion

        #region Properties
        
        public MenuCollection DataSourceAsMenuCollection => this.DataSource as MenuCollection;

        #endregion

    }
}
