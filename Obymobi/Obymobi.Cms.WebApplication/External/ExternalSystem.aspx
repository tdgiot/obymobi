﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" EnableSessionState="ReadOnly" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.External.ExternalSystem" Title="External system" Codebehind="ExternalSystem.aspx.cs" %>

<asp:Content ID="StylesheetContent" ContentPlaceHolderID="cphlStylesheets" runat="server">
	<style type="text/css">
		.change-button {
			float: right;
			padding: 3px 2px 4px 2px;
			font-size: 12px;
			width: 50px;
		}
		.encrypted-input {
			overflow: hidden;
		}
	</style>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" runat="Server">
</asp:Content>
<asp:Content ID="Conent3" ContentPlaceHolderID="cplhAdditionalToolBarButtons" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhPageContent" runat="Server">
    <div>
        <X:PageControl ID="tabsMain" runat="server" Width="100%">
            <TabPages>
                <X:TabPage Text="Algemeen" Name="Generic">
                    <Controls>
					    <table class="dataformV2">
						    <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblName">Name</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
							    </td>
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblType">Type</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <X:ComboBoxEnum ID="ddlType" runat="server" Type="Obymobi.Enums.ExternalSystemType, Obymobi"></X:ComboBoxEnum> 
							    </td>
						    </tr>
                        </table>
                        <D:PlaceHolder ID="plhSettings" runat="server"></D:PlaceHolder>
                    </Controls>
                </X:TabPage>             		
            </TabPages>
        </X:PageControl>
    </div>
</asp:Content>
