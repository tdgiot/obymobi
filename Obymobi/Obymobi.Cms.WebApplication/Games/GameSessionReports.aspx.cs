﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using System.Drawing;
using Dionysos;
using Dionysos.Web;
using Obymobi.Data.EntityClasses;

namespace Obymobi.ObymobiCms.Games
{
    public partial class GameSessionReports : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        #region Fields

        private TimeZoneInfo timeZoneInfo;

        #endregion

        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "GameSessionReport";
            this.EntityPageUrl = "~/Games/GameSessionReport.aspx";
            base.OnInit(e);

            var masterPage = this.Master as MasterPages.MasterPageEntityCollection;
            if (masterPage != null)
                masterPage.ToolBar.AddButton.Visible = false;

            this.DataSourceLoaded += GameSessionReports_DataSourceLoaded;
        }

        private void GameSessionReports_DataSourceLoaded(object sender)
        {
            CompanyEntity company = new CompanyEntity(CmsSessionHelper.CurrentCompanyId);
            this.timeZoneInfo = company.TimeZoneInfo;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            LLBLGenProDataSource datasource = this.DataSource as LLBLGenProDataSource;
            if (datasource != null)
            {
                PredicateExpression filter = new PredicateExpression(GameSessionReportFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
                datasource.FilterToUse = filter;
            }

            this.MainGridView.LoadComplete += MainGridView_LoadComplete;
            this.MainGridView.HtmlDataCellPrepared += MainGridView_HtmlDataCellPrepared;
        }

        private void MainGridView_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.DataColumn.FieldName == "CreatedUTC")
            {
                DateTime? dateTimeValue = (DateTime?)e.CellValue;
                if (dateTimeValue.HasValue)
                {
                    DateTime convertedDateTime = dateTimeValue.Value.UtcToLocalTime(this.timeZoneInfo);
                    e.Cell.Text = convertedDateTime.ToShortDateString() + " " + convertedDateTime.ToShortTimeString();
                }
            }
        }

        private void MainGridView_LoadComplete(object sender, EventArgs e)
        {
            DevExpress.Web.GridViewDataColumn column = this.MainGridView.Columns["CreatedUTC"] as DevExpress.Web.GridViewDataColumn;
            if (column != null)
            {
                this.MainGridView.SortBy(column, DevExpress.Data.ColumnSortOrder.Descending);
            }
        }

        #endregion
    }
}
