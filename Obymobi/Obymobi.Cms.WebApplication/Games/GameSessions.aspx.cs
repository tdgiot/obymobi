﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using System.Drawing;
using Dionysos;
using Dionysos.Web;
using Obymobi.Data.EntityClasses;

namespace Obymobi.ObymobiCms.Games
{
    public partial class GameSessions : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        #region Fields

        private TimeZoneInfo timeZoneInfo;

        #endregion

        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "GameSession";
            this.EntityPageUrl = "~/Games/GameSession.aspx";
            base.OnInit(e);

            var masterPage = this.Master as MasterPages.MasterPageEntityCollection;
            if (masterPage != null)
                masterPage.ToolBar.AddButton.Visible = false;

            this.DataSourceLoaded += GameSessions_DataSourceLoaded;
        }

        private void GameSessions_DataSourceLoaded(object sender)
        {
            CompanyEntity company = new CompanyEntity(CmsSessionHelper.CurrentCompanyId);
            this.timeZoneInfo = company.TimeZoneInfo;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            LLBLGenProDataSource datasource = this.DataSource as LLBLGenProDataSource;
            if (datasource != null)
            {
                PredicateExpression filter = new PredicateExpression(ClientFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
                RelationCollection relations = new RelationCollection(ClientEntity.Relations.GameSessionEntityUsingClientId);
                datasource.FilterToUse = filter;
                datasource.RelationsToUse = relations;
            }

            this.MainGridView.ShowDeleteColumn = false;
            this.MainGridView.ShowDeleteHyperlinkColumn = false;

            this.MainGridView.LoadComplete += MainGridView_LoadComplete;
            this.MainGridView.HtmlDataCellPrepared += MainGridView_HtmlDataCellPrepared;
        }

        private void MainGridView_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridViewTableDataCellEventArgs e)
        {
            bool isActive = (bool)e.GetValue("IsActive");
            if (isActive)
                e.Cell.ForeColor = Color.Green;

            if (e.DataColumn.FieldName == "ClientEntity.ClientId")
            {
                string id = e.CellValue.ToString();
                string url = WebShortcuts.ResolveUrl(string.Format("~/Company/Client.aspx?id={0}", id));
                e.Cell.Text = string.Format("<a href=\"{0}\" target=\"_blank\" >{1}</a>", url, e.GetValue("ClientIdentifier"));
            }
            else if (e.DataColumn.FieldName == "StartUTC" || e.DataColumn.FieldName == "EndUTC")
            {
                DateTime? dateTimeValue = (DateTime?)e.CellValue;
                if (dateTimeValue.HasValue)
                {
                    DateTime convertedDateTime = dateTimeValue.Value.UtcToLocalTime(this.timeZoneInfo);
                    e.Cell.Text = convertedDateTime.ToShortDateString() + " " + convertedDateTime.ToShortTimeString();
                }
            }
        }

        private void MainGridView_LoadComplete(object sender, EventArgs e)
        {
            DevExpress.Web.GridViewDataColumn column = this.MainGridView.Columns["StartUTC"] as DevExpress.Web.GridViewDataColumn;
            if (column != null)
            {
                this.MainGridView.SortBy(column, DevExpress.Data.ColumnSortOrder.Descending);
            }
        }

        #endregion
    }
}
