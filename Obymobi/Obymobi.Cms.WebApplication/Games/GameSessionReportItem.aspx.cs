﻿using System;
using Obymobi.Data.EntityClasses;
using Obymobi.ObymobiCms.UI;

namespace Obymobi.ObymobiCms.Games
{
    public partial class GameSessionReportItem : PageLLBLGenEntityCms
    {
		#region Properties
		
		public GameSessionReportItemEntity DataSourceAsGameSessionReportItemEntity
		{
			get
			{
                return this.DataSource as GameSessionReportItemEntity;
			}
		}

		#endregion

        #region Methods

        private void LoadUserControls()
        {
        
        }

        private void SetGui()
        {
            
        }

        #endregion

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            this.DataSourceLoaded += this.GameSessionReportItem_DataSourceLoaded;
            base.OnInit(e);
        }

        private void GameSessionReportItem_DataSourceLoaded(object sender)
        {
            this.SetGui();
        }

		#endregion
	}
}
