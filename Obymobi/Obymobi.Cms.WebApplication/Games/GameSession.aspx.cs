﻿using System;
using Dionysos;
using Dionysos.Web.UI;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;
using Obymobi.ObymobiCms.UI;
using Obymobi.Enums;
using Dionysos.Web;

namespace Obymobi.ObymobiCms.Games
{
    public partial class GameSession : PageLLBLGenEntityCms
    {
        #region Fields

        private TimeZoneInfo timeZoneInfo;

        #endregion

		#region Properties
		
		public GameSessionEntity DataSourceAsGameSessionEntity
		{
			get
			{
                return this.DataSource as GameSessionEntity;
			}
		}

		#endregion

        #region Methods

        private void SetGui()
        {
            if (this.DataSourceAsGameSessionEntity.GameId.HasValue)
            {
                this.hlGameName.Visible = true;
                this.hlGameName.NavigateUrl = WebShortcuts.ResolveUrl(string.Format("~/Games/Game.aspx?id={0}", this.DataSourceAsGameSessionEntity.GameId.Value));
                this.hlGameName.Text = this.DataSourceAsGameSessionEntity.GameName;
            }
            else
            {
                this.lblGameName.Visible = true;
                this.lblGameName.Text = this.DataSourceAsGameSessionEntity.GameName;
            }

            if (this.DataSourceAsGameSessionEntity.ClientId.HasValue)
            {
                this.hlClientIdentifier.Visible = true;
                this.hlClientIdentifier.NavigateUrl = WebShortcuts.ResolveUrl(string.Format("~/Company/Client.aspx?id={0}", this.DataSourceAsGameSessionEntity.ClientId.Value));
                this.hlClientIdentifier.Text = this.DataSourceAsGameSessionEntity.ClientIdentifier;
            }
            else
            {
                this.lblClientIdentifier.Visible = true;
                this.lblClientIdentifier.Text = this.DataSourceAsGameSessionEntity.ClientIdentifier;
            }

            if (this.DataSourceAsGameSessionEntity.DeliverypointId.HasValue)
            {
                this.hlDeliverypointNumber.Visible = true;
                this.hlDeliverypointNumber.NavigateUrl = WebShortcuts.ResolveUrl(string.Format("~/Company/Deliverypoint.aspx?id={0}", this.DataSourceAsGameSessionEntity.DeliverypointId.Value));
                this.hlDeliverypointNumber.Text = this.DataSourceAsGameSessionEntity.DeliverypointNumber;
            }
            else
            {
                this.lblDeliverypointNumber.Visible = true;
                this.lblDeliverypointNumber.Text = this.DataSourceAsGameSessionEntity.DeliverypointNumber;
            }

            this.lblStartValue.Text = this.DataSourceAsGameSessionEntity.StartUTC.UtcToLocalTime(this.timeZoneInfo).ToString();
            this.lblEndValue.Text = this.DataSourceAsGameSessionEntity.EndUTC.UtcToLocalTime(this.timeZoneInfo).ToString();
            this.lblLengthValue.Text = this.DataSourceAsGameSessionEntity.Length.ToString();
        }

        #endregion

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            //this.LoadUserControls();
            this.DataSourceLoaded += this.GameSession_DataSourceLoaded;
            base.OnInit(e);
        }

        private void Page_Load(object sender, EventArgs e)
        {
        }

        private void GameSession_DataSourceLoaded(object sender)
        {
            CompanyEntity company = new CompanyEntity(CmsSessionHelper.CurrentCompanyId);
            this.timeZoneInfo = company.TimeZoneInfo;

            this.SetGui();
        }

		#endregion
	}
}
