﻿using System;
using Dionysos;
using Dionysos.Web.UI;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;
using Obymobi.ObymobiCms.UI;
using Obymobi.Enums;

namespace Obymobi.ObymobiCms.Games
{
    public partial class GameSessionReportConfiguration : PageLLBLGenEntityCms
    {
		#region Properties
		
		public GameSessionReportConfigurationEntity DataSourceAsGameSessionReportConfigurationEntity
		{
			get
			{
                return this.DataSource as GameSessionReportConfigurationEntity;
			}
		}

		#endregion

        #region Methods

        private void SetGui()
        {
            this.ddlRecurrenceType.DataBindEnum<GameSessionReportRecurrenceType>();

            if (!this.DataSourceAsGameSessionReportConfigurationEntity.IsNew && this.DataSourceAsGameSessionReportConfigurationEntity.RecurrenceType == GameSessionReportRecurrenceType.Daily)
            {
                this.lblSendTime.Visible = true;
                this.teSendTime.Visible = true;
            }
        }

        #endregion

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            //this.LoadUserControls();
            this.DataSourceLoaded += this.GameSessionReportConfiguration_DataSourceLoaded;
            base.OnInit(e);
        }

        private void Page_Load(object sender, EventArgs e)
        {
        }

        private void GameSessionReportConfiguration_DataSourceLoaded(object sender)
        {
            this.SetGui();
        }

		#endregion
	}
}
