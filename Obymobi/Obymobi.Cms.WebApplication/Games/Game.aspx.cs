﻿using System;
using Dionysos;
using Dionysos.Web.UI;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;
using Obymobi.ObymobiCms.UI;
using Obymobi.Enums;

namespace Obymobi.ObymobiCms.Games
{
    public partial class Game : PageLLBLGenEntityCms
    {
		#region Properties
		
		public GameEntity DataSourceAsGameEntity
		{
			get
			{
                return this.DataSource as GameEntity;
			}
		}

		#endregion

        #region Methods

        private void SetGui()
        {
            this.ddlType.DataBindEnum<GameType>();
        }

        #endregion

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            //this.LoadUserControls();
            this.DataSourceLoaded += this.Game_DataSourceLoaded;
            base.OnInit(e);
        }

        private void Page_Load(object sender, EventArgs e)
        {
        }

        private void Game_DataSourceLoaded(object sender)
        {
            this.SetGui();
        }

		#endregion
	}
}
