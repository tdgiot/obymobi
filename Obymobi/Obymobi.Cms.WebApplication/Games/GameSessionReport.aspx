﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Games.GameSessionReport" Codebehind="GameSessionReport.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
    &nbsp;<X:ToolBarButton runat="server" ID="btnSendReport" CommandName="SendReport" Text="Send Report" Image-Url="~/Images/Icons/email.png" />     
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">    
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Generic" Name="Generic">
				<Controls>
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblCompanyId">Company</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:Label runat="server" id="lblCompanyName" LocalizeText="false"></D:Label>
							</td>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblCreatedUTC">Created</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <D:Label runat="server" id="lblCreatedValue" LocalizeText="false"></D:Label>
                            </td>                            	                              
                        </tr>                        
                        <tr>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblRecipients">Recipients</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <D:Label runat="server" id="lblRecipientsValue" LocalizeText="false" RenderAsLinkIfLinkable="false"></D:Label>
                            </td>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblSent">Sent</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <D:Label runat="server" id="lblSentValue" LocalizeText="false"></D:Label>
                            </td>
                        </tr>							                  
                    </table>
                </Controls>                    
            </X:TabPage>            
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>