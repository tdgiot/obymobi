﻿using System;
using Dionysos;
using Dionysos.Web.UI;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;
using Obymobi.ObymobiCms.UI;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;

namespace Obymobi.ObymobiCms.Games
{
    public partial class GameSessionReport : PageLLBLGenEntityCms
    {
        #region Fields

        private CompanyEntity companyEntity;
        private TimeZoneInfo timeZoneInfo;

        #endregion

		#region Properties
		
		public GameSessionReportEntity DataSourceAsGameSessionReportEntity
		{
			get
			{
                return this.DataSource as GameSessionReportEntity;
			}
		}

		#endregion

        #region Methods

        private void LoadUserControls()
        {
            this.tabsMain.AddTabPage("Sessions", "Sessions", "~/Games/SubPanels/GameSessionReportItemCollection.ascx");
        }

        private void SetGui()
        {
            this.lblCompanyName.Text = this.companyEntity.Name;
            this.lblCreatedValue.Text = this.DataSourceAsGameSessionReportEntity.CreatedUTC.UtcToLocalTime(this.timeZoneInfo).ToString();
            this.lblRecipientsValue.Text = this.DataSourceAsGameSessionReportEntity.Recipients;

            if (this.DataSourceAsGameSessionReportEntity.Sent.HasValue)
                this.lblSentValue.Text = this.DataSourceAsGameSessionReportEntity.Sent.UtcToLocalTime(this.timeZoneInfo).ToString();
        }

        public void SendReport()
        {
            GameSessionReportHelper.SendGameSessionReport(this.DataSourceAsGameSessionReportEntity, true);
            this.AddInformatorInfo(string.Format("Report has been emailed to {0}.", this.DataSourceAsGameSessionReportEntity.Recipients));
            this.Validate();
        }

        #endregion

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            this.DataSourceLoaded += this.GameSessionReport_DataSourceLoaded;
            base.OnInit(e);
        }

        private void GameSessionReport_DataSourceLoaded(object sender)
        {
            this.companyEntity = new CompanyEntity(CmsSessionHelper.CurrentCompanyId);
            this.timeZoneInfo = this.companyEntity.TimeZoneInfo;

            this.SetGui();
        }

		#endregion
	}
}
