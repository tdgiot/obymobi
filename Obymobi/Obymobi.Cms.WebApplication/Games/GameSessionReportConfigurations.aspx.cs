﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using System.Drawing;
using Dionysos;
using Dionysos.Web;

namespace Obymobi.ObymobiCms.Games
{
    public partial class GameSessionReportConfigurations : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "GameSessionReportConfiguration";
            this.EntityPageUrl = "~/Games/GameSessionReportConfiguration.aspx";
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            LLBLGenProDataSource datasource = this.DataSource as LLBLGenProDataSource;
            if (datasource != null)
            {
                PredicateExpression filter = new PredicateExpression(GameSessionReportConfigurationFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
                datasource.FilterToUse = filter;
            }

            this.MainGridView.HtmlDataCellPrepared += MainGridView_HtmlDataCellPrepared;
        }

        private void MainGridView_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridViewTableDataCellEventArgs e)
        {
            string recipients = (string)e.GetValue("Recipients");
            if (recipients.IsNullOrWhiteSpace())
                e.Cell.ForeColor = Color.Red;

            bool active = (bool)e.GetValue("Active");
            if (!active)
                e.Cell.ForeColor = Color.Gray;

            if (e.DataColumn.FieldName == "SendTime")
            {
                if ((string)e.GetValue("RecurrenceTypeText") == "Hourly")
                    e.Cell.Text = "Every round hour";
                else
                {
                    DateTime? dateTimeValue = (DateTime?)e.CellValue;
                    if (dateTimeValue.HasValue)
                    {
                        DateTime convertedDateTime = dateTimeValue.Value;
                        e.Cell.Text = convertedDateTime.ToShortTimeString();
                    }
                }
            }
        }

        #endregion
    }
}
