﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Games.GameSession" Codebehind="GameSession.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">    
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Generic" Name="Generic">
				<Controls>
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblGameId">Game</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
                                <span><D:HyperLink runat="server" ID="hlGameName" LocalizeText="false" Visible="false" Target="_blank"></D:HyperLink></span>
                                <D:Label runat="server" id="lblGameName" LocalizeText="false" Visible="false"></D:Label>
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblLength">Length</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:Label runat="server" id="lblLengthValue" LocalizeText="false"></D:Label>
							</td>
                        </tr>                        
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblDeliverypointId">Deliverypoint</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
                                <span><D:HyperLink runat="server" ID="hlDeliverypointNumber" LocalizeText="false" Visible="false" Target="_blank"></D:HyperLink></span>
                                <D:Label runat="server" id="lblDeliverypointNumber" LocalizeText="false" Visible="false"></D:Label>
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblClientId">Client</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<span><D:HyperLink ID="hlClientIdentifier" runat="server" LocalizeText="false" Target="_blank"></D:HyperLink></span>
                                <D:Label runat="server" id="lblClientIdentifier" LocalizeText="false" Visible="false"></D:Label>
							</td>
                        </tr>                        
                        <tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblStartUTC">Start</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:Label runat="server" id="lblStartValue" LocalizeText="false"></D:Label>
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblEndUTC">End</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:Label runat="server" id="lblEndValue" LocalizeText="false"></D:Label>
							</td>
                        </tr>
                    </table>
                </Controls>                    
            </X:TabPage>            
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>