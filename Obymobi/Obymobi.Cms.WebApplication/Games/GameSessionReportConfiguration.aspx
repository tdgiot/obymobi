﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Games.GameSessionReportConfiguration" Codebehind="GameSessionReportConfiguration.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">    
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Generic" Name="Generic">
				<Controls>
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblActive">Active</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
                                <D:CheckBox runat="server" ID="cbActive" />
							</td>	                              
                            <td class="label">
                            </td>
                            <td class="control">
                            </td>
                        </tr>  
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblName">Name</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
							</td>	                              
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblRecurrenceType">Type</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <X:ComboBoxInt runat="server" ID="ddlRecurrenceType"></X:ComboBoxInt>
                            </td>
                        </tr>                        
                        <tr>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblDailyResetTime">Daily reset time</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <D:TimePicker2 ID="teDailyResetTime" runat="server"></D:TimePicker2>
                            </td>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblSendTime" Visible="false">Send time</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <D:TimePicker2 ID="teSendTime" runat="server" Visible="false"></D:TimePicker2>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblRecipients">Recipients</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <D:TextBoxMultiLine runat="server" ID="tbRecipients"></D:TextBoxMultiLine>
                                <small>Multiple email addresses can be separated by commas.</small>
                            </td>
                            <td class="label">
                                &nbsp;
                            </td>
                            <td class="control">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </Controls>                    
            </X:TabPage>            
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>