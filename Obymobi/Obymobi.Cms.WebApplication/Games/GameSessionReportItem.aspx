﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Games.GameSessionReportItem" Codebehind="GameSessionReportItem.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">    
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Generic" Name="Generic">
				<Controls>
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblGameNameCaption">Game name</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:Label runat="server" id="lblGameName" LocalizeText="false"></D:Label>
							</td>                            
                        </tr>                                                
                    </table>
                </Controls>                    
            </X:TabPage>            
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>