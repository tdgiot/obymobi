﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using System.Drawing;
using Dionysos.Web;

namespace Obymobi.ObymobiCms.Games
{
    public partial class Games : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "Game";
            this.EntityPageUrl = "~/Games/Game.aspx";
            
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.MainGridView.HtmlDataCellPrepared += MainGridView_HtmlDataCellPrepared;
        }

        private void MainGridView_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.DataColumn.FieldName == "Url")
            {
                string url = e.CellValue.ToString();
                e.Cell.Text = string.Format("<a href=\"{0}\" target=\"_blank\" >{0}</a>", url);
            }
        }

        #endregion
    }
}
