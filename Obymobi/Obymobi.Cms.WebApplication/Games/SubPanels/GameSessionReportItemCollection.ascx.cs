﻿using System;
using DevExpress.Web;
using DevExpress.XtraGrid;
using System.Drawing;
using Dionysos;
using Obymobi.Data.EntityClasses;

namespace Obymobi.ObymobiCms.Games.SubPanels
{
    public partial class GameSessionReportItemCollection : Dionysos.Web.UI.DevExControls.SubPanelLLBLOverviewDataSourceCollection
    {
        #region Fields

        private TimeZoneInfo timeZoneInfo;

        #endregion

        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "GameSessionReportItem";

            this.MainGridView.LoadComplete += MainGridView_LoadComplete;
            this.MainGridView.HtmlDataCellPrepared += MainGridView_HtmlDataCellPrepared;

            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.MainGridView.ShowEditColumn = false;
            this.MainGridView.ShowEditHyperlinkColumn = false;
            this.MainGridView.EnableRowDoubleClick = false;
        }

        protected override void OnParentDataSourceLoaded()
        {
            CompanyEntity companyEntity = new CompanyEntity((this.ParentDataSource as GameSessionReportEntity).CompanyId);
            this.timeZoneInfo = companyEntity.TimeZoneInfo;

            base.OnParentDataSourceLoaded();
        }

        private void MainGridView_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.DataColumn.FieldName == "StartUTC" || e.DataColumn.FieldName == "EndUTC")
            {
                DateTime? dateTimeValue = (DateTime?)e.CellValue;
                if (dateTimeValue.HasValue)
                {
                    DateTime convertedDateTime = dateTimeValue.Value.UtcToLocalTime(this.timeZoneInfo);
                    e.Cell.Text = convertedDateTime.ToShortDateString() + " " + convertedDateTime.ToShortTimeString();
                }
            }
        }

        private void MainGridView_LoadComplete(object sender, EventArgs e)
        {
            DevExpress.Web.GridViewDataColumn column = this.MainGridView.Columns["StartUTC"] as DevExpress.Web.GridViewDataColumn;
            if (column != null)
            {
                this.MainGridView.SortBy(column, DevExpress.Data.ColumnSortOrder.Descending);
            }
        }
    }
}