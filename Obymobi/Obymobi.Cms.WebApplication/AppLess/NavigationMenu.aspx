﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.AppLess.NavigationMenu" ValidateRequest="false" Codebehind="NavigationMenu.aspx.cs" %>
<%@ Register TagPrefix="dxwtl" Namespace="DevExpress.Web.ASPxTreeList" Assembly="DevExpress.Web.ASPxTreeList.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
	            <Controls>
		            <D:PlaceHolder runat="server" ID="plhNavigationMenuItemList">
		            <table class="dataformV2">
			            <tr>
				            <td class="label">
					            <D:LabelEntityFieldInfo runat="server" id="lblName" MaxLength="255">Name</D:LabelEntityFieldInfo>
				            </td>
				            <td class="control">
					            <D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
				            </td>
				            <td class="label">
                                <D:Label runat="server" id="lblLanguageSwitcher">Language switcher</D:Label>
                            </td>
				            <td class="control">
                                <X:ComboBoxInt runat="server" ID="cbWidgetLanguageSwitcher" CssClass="input" TextField="Name" ValueField="WidgetId" IncrementalFilteringMode="Contains" IncrementalFilteringDelay="1000"></X:ComboBoxInt>
                            </td>
			            </tr>
			            <tr>
				            <D:PlaceHolder ID="plhNavigationMenu" runat="server" Visible="false">
					            <td class="label">
						            <D:Label runat="server" id="lblMenuItems">Menu items</D:Label>
					            </td>
					            <td class="control" colspan="3">
						            <div style="width: 788px;">
							            <dxwtl:ASPxTreeList ID="tlNavigationMenuItems" ClientInstanceName="tlNavigationMenuItems" runat="server" EnableViewState="false" AutoGenerateColumns="False">
								            <SettingsSelection Enabled="true" Recursive="false" />                                                
										</dxwtl:ASPxTreeList>    
										<D:Button runat="server" ID="btDeleteNavigationItemBottom" Text="Delete selected menu item(s)" PreSubmitWarning="Are you sure you want to delete these items?" Style="float:right; margin-top:10px" />
							        </div>
					            </td>
				            </D:PlaceHolder>
			            </tr>
			        </table>
		            </D:PlaceHolder>					
	            </Controls>
            </X:TabPage>
        </TabPages>
    </X:PageControl>
</div>
</asp:Content>

