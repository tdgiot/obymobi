﻿using System;
using Obymobi.Data.EntityClasses;

namespace Obymobi.ObymobiCms.AppLess
{
    public partial class ReceiptTemplate : Dionysos.Web.UI.PageLLBLGenEntity
    {
        #region Methods

        private void LoadUserControls()
        {
         
        }

        private void SetGui()
        {
            this.tbEmail.Text = this.DataSourceAsReceiptTemplateEntity.Email.Replace(", ", "\n");
            this.tbPhonenumber.Text = this.DataSourceAsReceiptTemplateEntity.Phonenumber.Replace(", ", "\n");
        }
        
        #endregion

        #region Properties

        public ReceiptTemplateEntity DataSourceAsReceiptTemplateEntity => this.DataSource as ReceiptTemplateEntity;

        #endregion

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            this.DataSourceLoaded += ReceiptTemplate_DataSourceLoaded;
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        { }

        private void ReceiptTemplate_DataSourceLoaded(object sender)
        {
            this.SetGui();
        }

        public override bool Save()
        {
            if (this.DataSourceAsReceiptTemplateEntity.IsNew)
                this.DataSourceAsReceiptTemplateEntity.CompanyId = CmsSessionHelper.CurrentCompanyId;
            
            this.DataSourceAsReceiptTemplateEntity.Email = string.Join(", ", this.tbEmail.Text.Replace("\r", string.Empty).Split('\n'));
            this.DataSourceAsReceiptTemplateEntity.Phonenumber = string.Join(", ", this.tbPhonenumber.Text.Replace("\r", string.Empty).Split('\n'));

            return base.Save();
        }

        #endregion
    }
}
