﻿using System;
using Dionysos.Web;
using Obymobi.Cms.Logic.DataSources;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;

namespace Obymobi.ObymobiCms.AppLess
{
	public partial class Theme : Dionysos.Web.UI.PageLLBLGenEntity
    {
        #region Fields

        private ThemeDataSource themeDataSource = new ThemeDataSource();

        #endregion

        #region Methods

        private void LoadUserControls()
        {
            
        }

		private void SetGui()
		{
			if (this.DataSourceAsThemeEntity.IsNew)
			{
				if (QueryStringHelper.HasValue(nameof(ApplicationConfigurationFields.ApplicationConfigurationId)))
				{
					this.themeDataSource.SetDefaultValuesOnTheme(this.DataSourceAsThemeEntity, QueryStringHelper.GetInt(nameof(ApplicationConfigurationFields.ApplicationConfigurationId)));
				}
			}

			this.tbPrimaryColor.Text = RgbaToHex(this.DataSourceAsThemeEntity.PrimaryColor);
			this.tbSignalColor.Text = RgbaToHex(this.DataSourceAsThemeEntity.SignalColor);
			this.tbTextColor.Text = RgbaToHex(this.DataSourceAsThemeEntity.TextColor);
			this.tbDeviderColor.Text = RgbaToHex(this.DataSourceAsThemeEntity.DividerColor);
			this.tbBackgroundColor.Text = RgbaToHex(this.DataSourceAsThemeEntity.BackgroundColor);
			try
			{
				this.tbFontFamilyList.Value = (int)EnumUtil.ToEnum<ApplessFontTypes>(this.DataSourceAsThemeEntity.FontFamily); 
			}
			catch{
				this.tbFontFamilyList.Value = 0;
			}

		}

		public override bool Save()
		{		  
			this.DataSourceAsThemeEntity.PrimaryColor = HexToInt(this.tbPrimaryColor.Text);
			this.DataSourceAsThemeEntity.SignalColor = HexToInt(this.tbSignalColor.Text);
			this.DataSourceAsThemeEntity.TextColor = HexToInt(this.tbTextColor.Text);
			this.DataSourceAsThemeEntity.DividerColor = HexToInt(this.tbDeviderColor.Text);
			this.DataSourceAsThemeEntity.BackgroundColor = HexToInt(this.tbBackgroundColor.Text);
			this.DataSourceAsThemeEntity.FontFamily = EnumUtil.GetStringValue((ApplessFontTypes)this.tbFontFamilyList.Value);
			return base.Save();
		}

	    #endregion

        #region Properties

		public ThemeEntity DataSourceAsThemeEntity
		{
			get
			{
				return this.DataSource as ThemeEntity;
			}
		}

		#endregion

		#region Event handlers

        protected override void OnInit(EventArgs e)
        {
			this.EntityName = "Theme";
            this.LoadUserControls();

            this.DataSourceLoaded += this.Page_DataSourceLoaded;
            base.OnInit(e);
        }

	    private void Page_DataSourceLoaded(object sender)
	    {
	        this.SetGui();
	    }

	    protected void Page_Load(object sender, EventArgs e)
		{

		}


		#endregion

		#region Helpers

		private static int HexToInt(string input)
			=> Convert.ToInt32(input?.Replace("#","") ?? "000000", 16);

		private static string RgbaToHex(int rgba)
		{
			byte red = (byte)(rgba >> 16);
			byte green = (byte)(rgba >> 8);
			byte blue = (byte)rgba;

			return $"#{red:X2}{green:X2}{blue:X2}";
		}

		#endregion
	}
}
