﻿using System;
using Obymobi.Cms.WebApplication.Old_App_Code;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;

namespace Obymobi.ObymobiCms.AppLess
{
    public partial class Outlets : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        #region Event handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            if (DataSource is LLBLGenProDataSource datasource)
            {
                PredicateExpression filter = new PredicateExpression(OutletFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

                datasource.FilterToUse = filter;
            }

            if (!RoleRights.AllowedToDeleteOutlet(CmsSessionHelper.CurrentRole))
            {
                MainGridView.ShowDeleteHyperlinkColumn = false;
                MainGridView.ShowDeleteColumn = false;
            }
        }

        #endregion
    }
}
