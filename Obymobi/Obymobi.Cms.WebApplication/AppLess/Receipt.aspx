﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.AppLess.Receipt" Codebehind="Receipt.aspx.cs" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">    
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<controls>
                    <D:Panel ID="pnlGeneric" runat="server" GroupingText="Summary" LocalizeText="False">
                        <table class="dataformV2">
						    <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblOrderIdCaption">Order Id</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:Label ID="lblOrderId" runat="server" LocalizeText="false" CssClass="lblOrderValue"></D:Label>
                                </td>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblCreatedLocalCaption">Created (local)</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:Label ID="lblCreatedLocal" runat="server" LocalizeText="false" CssClass="lblOrderValue"></D:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblNumberCaption" LocalizeText="False">Receipt number</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:Label ID="lblNumber" runat="server" LocalizeText="false" CssClass="lblOrderValue"></D:Label>
                                </td>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblOutletCaption">Outlet</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:HyperLink runat="server" ID="hlOutletId" LocalizeText="False"></D:HyperLink>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblServiceMethodNameCaption">Service method</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:HyperLink runat="server" ID="hlServiceMethodName" LocalizeText="False"></D:HyperLink>
                                </td>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblCheckoutMethodNameCaption">Checkout method</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:HyperLink runat="server" ID="hlCheckoutMethod" LocalizeText="False"></D:HyperLink>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblPaymentProviderNameCaption">Payment provider</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:HyperLink runat="server" ID="hlPaymentProvider" LocalizeText="False"></D:HyperLink>
                                </td>
                                <td class="label">
                                </td>
                                <td class="control">
                                </td>
                            </tr>
                        </table>
                    </D:Panel>
                    <D:Panel ID="pnlVendor" runat="server" GroupingText="Vendor">
                        <table class="dataformV2">
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblVendorNameCaption">Name</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:Label ID="lblVendorName" runat="server" LocalizeText="false" CssClass="lblOrderValue"></D:Label>
                                </td>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblVendorAddressCaption">Address</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:Label ID="lblVendorAddress" runat="server" LocalizeText="false" CssClass="lblOrderValue"></D:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblVendorPhoneCaption">Phone</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:Label ID="lblVendorPhone" runat="server" LocalizeText="false" CssClass="lblOrderValue"></D:Label>
                                </td>
                            </tr>
                        </table>
                    </D:Panel>
                    <D:Panel ID="pnlCustomer" runat="server" GroupingText="Customer">
                        <table class="dataformV2">
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblCustomerNameCaption">Name</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:Label ID="lblCustomerName" runat="server" LocalizeText="false" CssClass="lblOrderValue"></D:Label>
                                </td>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblCustomerEmailCaption">Email</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:Label ID="lblCustomerEmail" runat="server" LocalizeText="false" CssClass="lblOrderValue"></D:Label> <D:HyperLink runat="server" ID="hlResendReceipt" LocalizeText="False" OnClick="showPopup();return false;" NavigateUrl="~">[Resend Receipt]</D:HyperLink>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblCustomerPhoneCaption">Phone</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:Label ID="lblCustomerPhone" runat="server" LocalizeText="false" CssClass="lblOrderValue"></D:Label>
                                </td>
                            </tr>
                        </table>
                    </D:Panel>
                    <D:Panel ID="pnlDelivery" runat="server" GroupingText="Delivery information" Visible="False">
                        <table class="dataformV2">
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblDeliveryBuildingNameCaption">Flat number/building name</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:Label ID="lblDeliveryBuildingName" runat="server" LocalizeText="false" CssClass="lblOrderValue"></D:Label>
                                </td>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblDeliveryAddressCaption">Address</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:Label ID="lblDeliveryAddress" runat="server" LocalizeText="false" CssClass="lblOrderValue"></D:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblDeliveryZipcodeCaption">Zipcode</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:Label ID="lblDeliveryZipcode" runat="server" LocalizeText="false" CssClass="lblOrderValue"></D:Label>
                                </td>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblDeliveryCityCaption">City</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:Label ID="lblDeliveryCity" runat="server" LocalizeText="false" CssClass="lblOrderValue"></D:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblDeliveryInstructionsCaption">Instructions</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:Label ID="lblDeliveryInstructions" runat="server" LocalizeText="false" CssClass="lblOrderValue"></D:Label>
                                </td>
                            </tr>
                        </table>
                    </D:Panel>
                    <D:Panel ID="pnlOrder" runat="server" GroupingText="Order">
                        <table class="dataformV2">
                            <tr>
                                <td>
                                    <X:GridView ID="gvOrderitems" ClientInstanceName="gvOrderitems" runat="server" Width="100%" KeyFieldName="OrderitemId" Style="margin-top: 10px;">
                                        <SettingsBehavior AutoFilterRowInputDelay="350" />                            
                                        <SettingsPager PageSize="100"></SettingsPager>
                                        <SettingsBehavior AllowGroup="false" AllowDragDrop="false" AllowSort="false" />
                                        <Columns>
                                            <dxwgv:GridViewDataColumn FieldName="Description" VisibleIndex="0"/>
                                            <dxwgv:GridViewDataColumn FieldName="Quantity" VisibleIndex="1" Width="120px">
                                                <CellStyle HorizontalAlign="Right"></CellStyle>
                                                <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="Price" VisibleIndex="2" Width="120px">
                                                <CellStyle HorizontalAlign="Right"></CellStyle>
                                                <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                            </dxwgv:GridViewDataColumn>
                                        </Columns>
                                    </X:GridView>

                                    <div style="width: 100%; text-align: right; text-align: -webkit-right;">
                                        <X:GridView ID="gvTotal" ClientInstanceName="gvTotal" runat="server" Width="25%" Style="margin: 10px 0;" Visible="false">
                                            <SettingsBehavior AllowGroup="false"
                                                AllowDragDrop="false"
                                                AllowSort="false"
                                                AutoFilterRowInputDelay="350" />
                                            <Columns>
                                                <dxwgv:GridViewDataColumn FieldName="Description" VisibleIndex="0"/>

                                                <dxwgv:GridViewDataColumn FieldName="Total" VisibleIndex="1" Width="240px">
                                                    <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Right"></CellStyle>
                                                </dxwgv:GridViewDataColumn>
                                            </Columns>
                                        </X:GridView>
                                    </div>

                                    <div style="width: 100%; text-align: right; text-align: -webkit-right;">
                                        <X:GridView ID="gvTaxBreakdown" ClientInstanceName="gvTaxBreakdown" runat="server" Width="25%" Style="margin: 10px 0;" Visible="false">
                                            <SettingsBehavior AllowGroup="false"
                                                AllowDragDrop="false"
                                                AllowSort="false"
                                                AutoFilterRowInputDelay="350" />
                                            <Columns>
                                                <dxwgv:GridViewDataColumn FieldName="Rate" VisibleIndex="0"/>

                                                <dxwgv:GridViewDataColumn FieldName="Net" VisibleIndex="1" Width="120px">
                                                    <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Right"></CellStyle>
                                                </dxwgv:GridViewDataColumn>

                                                <dxwgv:GridViewDataColumn FieldName="VAT" VisibleIndex="2" Width="120px">
                                                    <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Right"></CellStyle>
                                                </dxwgv:GridViewDataColumn>
                                            </Columns>
                                        </X:GridView>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </D:Panel>
                    <D:Panel ID="pnlPaymentTransaction" runat="server" GroupingText="Payment transaction">
                        <table class="dataformV2">
                            <tr>
                                <td colspan="4">
                                    <X:GridView ID="gvPaymentTransaction" ClientInstanceName="gvPaymentTransaction" runat="server" Width="100%" KeyFieldName="PaymentTransactionId" Style="margin-top: 10px">
                                        <SettingsBehavior AutoFilterRowInputDelay="350" />                            
                                        <SettingsPager PageSize="100"></SettingsPager>
                                        <SettingsBehavior AllowGroup="false" AllowDragDrop="false" />
                                        <Columns>
                                            <dxwgv:GridViewDataColumn FieldName="CreatedLocal" VisibleIndex="0" />
                                            <dxwgv:GridViewDataColumn FieldName="ReferenceId" VisibleIndex="1" />
                                            <dxwgv:GridViewDataColumn FieldName="MerchantReference" VisibleIndex="2" />
                                            <dxwgv:GridViewDataColumn FieldName="MerchantAccount" VisibleIndex="3" />
                                            <dxwgv:GridViewDataColumn FieldName="PaymentMethod" VisibleIndex="4" />
                                            <dxwgv:GridViewDataColumn FieldName="Status" VisibleIndex="5" />
                                            <dxwgv:GridViewDataColumn FieldName="CardSummary" VisibleIndex="6" />
                                            <dxwgv:GridViewDataColumn FieldName="AuthCode" VisibleIndex="7" />
                                            <dxwgv:GridViewDataColumn FieldName="Provider" VisibleIndex="8" />
                                            <dxwgv:GridViewDataColumn FieldName="Environment" VisibleIndex="9" />
                                        </Columns>
                                    </X:GridView>  
                                </td>
                            </tr>
                        </table>
                    </D:Panel>
                <D:Panel ID="pnlPaymentSplit" runat="server" GroupingText="Payment split" Visible="False">
                        <table class="dataformV2">
                            <tr>
                                <td colspan="4">
                                    <X:GridView ID="gvPaymentSplit" ClientInstanceName="gvPaymentSplit" runat="server" Width="100%" KeyFieldName="PaymentTransactionSplitId" Style="margin-top: 10px">
                                        <SettingsBehavior AutoFilterRowInputDelay="350" />                            
                                        <SettingsPager PageSize="100"></SettingsPager>
                                        <SettingsBehavior AllowGroup="false" AllowDragDrop="false" />
                                        <Columns>
                                            <dxwgv:GridViewDataColumn FieldName="Reference" VisibleIndex="0" />
                                            <dxwgv:GridViewDataColumn FieldName="AccountCode" VisibleIndex="1" />
                                            <dxwgv:GridViewDataColumn FieldName="Type" VisibleIndex="2" />
                                            <dxwgv:GridViewDataColumn FieldName="Amount" VisibleIndex="3" />
                                        </Columns>
                                    </X:GridView>  
                                </td>
                            </tr>
                        </table>
                    </D:Panel>
                    <D:Panel ID="pnlOrderNotificationLogs" runat="server" GroupingText="Notifications">
                        <table class="dataformV2">
                            <tr>
                                <td colspan="4">
                                    <X:GridView ID="gvOrderNotificationLog" ClientInstanceName="gvOrderNotificationLog" runat="server" Width="100%" KeyFieldName="OrderNotificationLogId" Style="margin-top: 10px">
                                        <SettingsBehavior AutoFilterRowInputDelay="350" />                            
                                        <SettingsPager PageSize="100"></SettingsPager>
                                        <SettingsBehavior AllowGroup="false" AllowDragDrop="false" />
                                        <Columns>
                                            <dxwgv:GridViewDataColumn FieldName="CreatedLocal" VisibleIndex="0" />
                                            <dxwgv:GridViewDataColumn FieldName="Type" VisibleIndex="1" />
                                            <dxwgv:GridViewDataColumn FieldName="Method" VisibleIndex="2" />
                                            <dxwgv:GridViewDataColumn FieldName="Status" VisibleIndex="3" />
                                            <dxwgv:GridViewDataColumn FieldName="Receiver" VisibleIndex="4" />
                                            <dxwgv:GridViewDataColumn FieldName="Log" VisibleIndex="5" />
                                            <dxwgv:GridViewDataColumn FieldName="SentLocal" VisibleIndex="6" />
                                        </Columns>
                                    </X:GridView>  
                                </td>
                            </tr>
                        </table>
                    </D:Panel>
                </controls>
            </X:TabPage>
		</TabPages>
	</X:PageControl>
</div>
<dx:ASPxPopupControl ID="pcPopup" runat="server" Width="500" CloseAction="CloseButton" CloseOnEscape="true" Modal="True" ShowOnPageLoad="False"
PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" ClientInstanceName="pcPopup"
HeaderText="Resend Receipt Email" AllowDragging="False" PopupAnimationType="None" EnableViewState="False" AutoUpdatePosition="true">
<ContentCollection>
    <dx:PopupControlContentControl runat="server">
        <dx:ASPxPanel ID="ASPxPanel1" runat="server" DefaultButton="btOK">
            <PanelCollection>
                <dx:PanelContent runat="server">
                    <table class="dataformV2" width="500px">
                        <tr>
                            <td class="label">
                                <D:Label runat="server" ID="lblExtraEmailAddresses">Additional Email Addresses</D:Label>
                            </td>
                            <td class="control">
                                <D:TextBoxMultiLine runat="server" ID="tbAdditionalEmail" ClientIDMode="Static" IsRequired="False" CausesValidation="False" /><br/>
                                <small>(Separate each email addresses with a comma)</small>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                <D:Label runat="server" ID="Label1">Send to original receiver</D:Label>
                            </td>
                            <td class="control">
                                <D:CheckBox runat="server" ID="cbSendToReceiver" ClientIDMode="Static" CausesValidation="False" />
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                
                            </td>
                            <td class="control" style="float: right; text-align: right">
                                <D:Button runat="server" ID="btnSendReceipt" ClientIDMode="Static" Text="Send Receipt" LocalizeText="False" OnClientClick="pcPopup.Hide();" />
                            </td>
                        </tr>
                    </table>
                </dx:PanelContent>
            </PanelCollection>
        </dx:ASPxPanel>
    </dx:PopupControlContentControl>
</ContentCollection>
<ContentStyle>
    <Paddings PaddingBottom="5px" />
</ContentStyle>
</dx:ASPxPopupControl>
<script language="javascript">
    function showPopup() {
        document.getElementById("tbAdditionalEmail").value = "";
        document.getElementById("cbSendToReceiver").checked = false;
        pcPopup.Show();
    }
</script>
</asp:Content>
