using Dionysos;
using Dionysos.Web;
using Dionysos.Web.UI;
using Dionysos.Web.UI.DevExControls;
using Obymobi.Cms.Logic.HelperClasses;
using Obymobi.Cms.WebApplication.AppLess;
using Obymobi.Cms.WebApplication.AppLess.SubPanels.ServiceMethods;
using Obymobi.Cms.WebApplication.Old_App_Code.Extensions;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Extensions;
using Obymobi.Logic.Extensions;
using Obymobi.Logic.HelperClasses;
using Obymobi.ObymobiCms.MasterPages;
using Resources;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Obymobi.ObymobiCms.AppLess
{
    public partial class ServiceMethod : PageLLBLGenEntity
    {
        private Generic.UserControls.CustomTextCollection translationsTab;

        public ServiceMethodEntity DataSourceAsServiceMethodEntity => this.DataSource as ServiceMethodEntity;

        protected override void OnInit(EventArgs e)
        {
            this.LoadTabPages();
            this.LoadUserControls();

            this.DataSourceLoaded += this.ServiceMethod_DataSourceLoaded;

            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.DataSourceAsServiceMethodEntity.IsNew &&
                !this.IsPostBack)
            {
                this.SetWarnings();
            }

            if (this.Master is MasterPageEntity masterPage)
            {
                masterPage.ToolBar.SaveButton.Visible = true;
                masterPage.ToolBar.SaveAndGoButton.Visible = true;
                masterPage.ToolBar.SaveAndNewButton.Visible = false;

                if (masterPage.ToolBar?.DeleteButton?.Visible != null)
                {
                    masterPage.ToolBar.DeleteButton.Visible = true;
                }
            }

            // This should stay in sync with the Resource file in Crave.Ordering
            this.tbOrderCompleteTextMessage.Placeholder = "Your order #[[ORDERNUMBER]] at [[SELLERNAME]] is complete.";
            this.tbOrderCompleteMailSubject.Placeholder = "Your order #[[ORDERNUMBER]] at [[SELLERNAME]] is complete.";
            this.tbOrderCompleteMailMessage.Placeholder = "Your order #[[ORDERNUMBER]] at [[SELLERNAME]] is complete.";
        }

        private void ServiceMethod_DataSourceLoaded(object sender)
        {
            SetGui();

            if (PageMode == PageMode.Add)
            {
                DataSourceAsServiceMethodEntity.Active = true;

                int relatedEntityId = GetRelatedEntityIdFromQueryString();

                DataSourceAsServiceMethodEntity.CompanyId = CmsSessionHelper.CurrentCompanyId;
                DataSourceAsServiceMethodEntity.OutletId = relatedEntityId;
                DataSourceAsServiceMethodEntity.CoversType = CoversType.DependentOnProducts;
            }
        }

        private void SaveSelectedDeliverypointgroups()
        {
            List<int> selectedDeliverypointgroupIds = this.lbDeliverypointgroups.SelectedItemsValuesAsIntList;

            ServiceMethodDeliverypointgroupCollection deliverypointgroups = this.DataSourceAsServiceMethodEntity.ServiceMethodDeliverypointgroupCollection;

            // Save the newly selected deliverypointgroups
            foreach (int deliverypointgroupId in selectedDeliverypointgroupIds)
            {
                // Skip already present deliverypointgroups
                if (deliverypointgroups.Any(dpg => dpg.DeliverypointgroupId == deliverypointgroupId))
                {
                    continue;
                }

                ServiceMethodDeliverypointgroupEntity serviceMethodDeliverypointgroup = new ServiceMethodDeliverypointgroupEntity
                {
                    ServiceMethodId = this.EntityId,
                    DeliverypointgroupId = deliverypointgroupId,
                    ParentCompanyId = this.DataSourceAsServiceMethodEntity.CompanyId
                };

                serviceMethodDeliverypointgroup.Save();
            }

            // Remove unselected deliverypointgroups
            foreach (ServiceMethodDeliverypointgroupEntity deliverypointgroup in deliverypointgroups)
            {
                if (selectedDeliverypointgroupIds.All(id => id != deliverypointgroup.DeliverypointgroupId))
                {
                    deliverypointgroup.Delete();
                }
            }
        }

        public int GetRelatedEntityIdFromQueryString()
        {
            int.TryParse(QueryStringHelper.GetString("RelatedEntityId"), out int relatedEntityId);

            return relatedEntityId;
        }

        public override bool Save()
        {
            if (this.DataSourceAsServiceMethodEntity.IsNew)
            {
                this.DataSourceAsServiceMethodEntity.Label = this.tbLabel.Value;
            }

            if (!this.ValidateBeforeSave())
            {
                return false;
            }

            bool success = base.Save();

            if (success)
            {
                this.SaveCustomServiceChargeProperties();

                CustomTextHelper.UpdateCustomTexts(this.DataSource, this.DataSourceAsServiceMethodEntity.CompanyEntity);

                TranslationHelper.SetDefaultTranslations(ServiceMethodDefaultTranslations.ResourceManager,
                    this.DataSourceAsServiceMethodEntity,
                    this.DataSourceAsServiceMethodEntity.CompanyEntity.GetDefaultCompanyCulture(),
                    this.DataSourceAsServiceMethodEntity.CompanyEntity.GetCompanyCultures());

                this.SaveSelectedDeliverypointgroups();
            }

            return success;
        }

        private bool ValidateBeforeSave()
        {
            if (this.tbMinimumOrderAmount.Value == null || this.tbMinimumOrderAmount.Value < decimal.Zero)
            {
                this.tbMinimumOrderAmount.Value = decimal.Zero;
                this.AddInformator(InformatorType.Warning, $"The minimal ordering amount cannot be null or less than zero. Default '{decimal.Zero}' has been applied.");
            }

            if (tbMaxCovers.Value == null || tbMaxCovers.Value < 1)
            {
                tbMaxCovers.Value = 10;

                if (tbMaxCovers.Visible)
                {
                    AddInformator(InformatorType.Warning, $"Maximum covers amount cannot be null or less than one. Default value '{tbMaxCovers.Value}' has been applied.");
                }
            }

            this.ValidateServiceChargeSettings();

            if (this.DataSourceAsServiceMethodEntity.Type == ServiceMethodType.Delivery)
            {
                this.ValidateDeliveryChargeSettings();
            }

            OrderNotificationMethod orderNotificationMethod = this.cbOrderCompleteNotificationMethod.Value?.ToEnum<OrderNotificationMethod>() ?? OrderNotificationMethod.None;
            if (orderNotificationMethod.HasFlag(OrderNotificationMethod.SMS) && this.tbOrderCompleteTextMessage.Value.IsNullOrWhiteSpace())
            {
                this.AddInformator(InformatorType.Warning, "Order complete text message is activated, but the message is empty");
            }
            if (orderNotificationMethod.HasFlag(OrderNotificationMethod.Email) && (this.tbOrderCompleteMailSubject.Value.IsNullOrWhiteSpace() || this.tbOrderCompleteMailMessage.Value.IsNullOrWhiteSpace()))
            {
                this.AddInformator(InformatorType.Warning, "Order complete email message is activated, but the subject and/or message are empty");
            }

            this.Validate();

            return this.IsValid;
        }

        private void ValidateServiceChargeSettings()
        {
            if (!this.cbServiceChargeType.Value.HasValue)
            {
                this.cbServiceChargeType.Value = (int)ChargeType.None;
            }

            if (!this.tbServiceCharge.Value.HasValue || this.tbServiceCharge.Value < decimal.Zero)
            {
                this.tbServiceCharge.Value = decimal.Zero;

                if (this.cbServiceChargeType.Value == (int)ChargeType.Percentage)
                {
                    this.AddInformator(InformatorType.Warning, "No percentage specified for the service charge. Default value '{0}' has been applied", 0d);
                }
                else
                {
                    this.AddInformator(InformatorType.Warning, "No fixed price specified for the service charge. Default value '{0}' has been applied", decimal.Zero);
                }
            }

            if (this.cbServiceChargeType.Value != (int)ChargeType.None && (!this.tbMinimumAmountForFreeServiceCharge.Value.HasValue || this.tbMinimumAmountForFreeServiceCharge.Value < decimal.Zero))
            {
                this.tbMinimumAmountForFreeServiceCharge.Value = decimal.Zero;

                this.AddInformator(InformatorType.Warning, "No minimum amount for free service charge specified. Default value '{0}' has been applied", decimal.Zero);
            }
        }

        private void ValidateDeliveryChargeSettings()
        {
            if (this.tbMinimumAmountForFreeDelivery.Value == null || this.tbMinimumAmountForFreeDelivery.Value < decimal.Zero)
            {
                this.tbMinimumAmountForFreeDelivery.Value = decimal.Zero;
                this.AddInformator(InformatorType.Warning, "No value has been specified for the minimal amount for free delivery charge .Default value '{0}' has been applied", decimal.Zero);
            }
        }

        private void LoadTabPages()
        {
            if (this.PageMode == PageMode.View || this.PageMode == PageMode.Edit)
            {
                this.LoadTab("Delivery", nameof(Delivery), "~/AppLess/SubPanels/ServiceMethods/Delivery.ascx", accessRole: Role.Supervisor);

                this.translationsTab = this.LoadTranslationsTab(accessRole: Role.Supervisor);

                this.lbDeliverypointgroups.DataBindEntityCollection<DeliverypointgroupEntity>(DeliverypointgroupFields.CompanyId == CmsSessionHelper.CurrentCompanyId, DeliverypointgroupFields.Name, DeliverypointgroupFields.Name);
            }
        }

        private void LoadUserControls()
        {
            this.cbType.DataBindEnum<ServiceMethodType>();
            this.cbServiceChargeType.DataBindEnum<ChargeType>();
            cbCoversType.DataBindEnumValues(CoversType.DependentOnProducts, CoversType.AlwaysAsk, CoversType.NeverAsk);
        }

        private void SaveCustomServiceChargeProperties()
        {
            ServiceMethodEntity serviceMethodEntity = this.DataSourceAsServiceMethodEntity;

            if (this.cbServiceChargeType.Value == (int)ChargeType.Fixed && this.tbServiceCharge.Value.HasValue)
            {
                serviceMethodEntity.ServiceChargeAmount = this.tbServiceCharge.Value.Value;
                serviceMethodEntity.ServiceChargePercentage = 0d;
            }
            else if (this.cbServiceChargeType.Value == (int)ChargeType.Percentage && this.tbServiceCharge.Value.HasValue)
            {
                serviceMethodEntity.ServiceChargePercentage = (double?)this.tbServiceCharge.Value;
                serviceMethodEntity.ServiceChargeAmount = decimal.Zero;
            }
            else
            {
                serviceMethodEntity.ServiceChargePercentage = 0d;
                serviceMethodEntity.ServiceChargeAmount = decimal.Zero;
                serviceMethodEntity.MinimumAmountForFreeServiceCharge = 0;
            }

            serviceMethodEntity.Save();
        }

        private void SetGui()
        {
            if (this.PageMode == PageMode.Add)
            {
                this.plhEdit.Visible = false;
                this.pnlServiceCharge.Visible = false;
                this.pnlDeliverypointgroups.Visible = false;

                this.pnlRoomService.Visible = false;
                this.pnlDeliveryCharge.Visible = false;
                this.pnlOrderCompleteMessage.Visible = false;
            }
            else
            {
                this.cbType.Enabled = false;
                this.SetCustomProperties();

                this.pnlRoomService.Visible = this.DataSourceAsServiceMethodEntity.Type == ServiceMethodType.RoomService;
                this.pnlDeliverypointgroups.Visible = this.DataSourceAsServiceMethodEntity.Type == ServiceMethodType.RoomService || this.DataSourceAsServiceMethodEntity.Type == ServiceMethodType.TableService;

                if (this.DataSourceAsServiceMethodEntity.Type != ServiceMethodType.Delivery)
                {
                    this.pnlDeliveryCharge.Visible = false;

                    TabPage deliveryTabPage = tabsMain.GetTabPageByName(nameof(Delivery));
                    if (deliveryTabPage != null)
                    {
                        deliveryTabPage.Visible = false;
                    }
                }

                this.RemoveUnrelatedCustomTextTypes();

                this.lbDeliverypointgroups.SelectValues(this.DataSourceAsServiceMethodEntity.ServiceMethodDeliverypointgroupCollection.Select(dpg => dpg.DeliverypointgroupId));
            }

            SetCoversType();
        }

        private void SetCoversType()
        {
            bool isCoversVisible = CmsSessionHelper.CurrentUser.HasFeature(FeatureToggle.Covers);
            lblCoversType.Visible = isCoversVisible;

            cbCoversType.Visible = isCoversVisible;
            cbCoversType.IsRequired = isCoversVisible;

            lblMaxCovers.Visible = isCoversVisible;
            tbMaxCovers.Visible = isCoversVisible;
        }

        private void RemoveUnrelatedCustomTextTypes()
        {
            if (this.translationsTab == null)
            {
                return;
            }

            switch (this.DataSourceAsServiceMethodEntity.Type)
            {
                case ServiceMethodType.TableService:
                case ServiceMethodType.RoomService:
                    this.translationsTab.ExcludeCustomTextTypes(
                        CustomTextType.InstructionsLabel,
                        CustomTextType.InstructionsPlaceholder,
                        CustomTextType.AddressMissingText,
                        CustomTextType.CityRequiredText,
                        CustomTextType.PostalCodeRequiredText,
                        CustomTextType.AddressOutsideRangeText,
                        CustomTextType.HouseNumberMissingText,
                        CustomTextType.AddressLookupLabel,
                        CustomTextType.AddressOutsideRangeText,
                        CustomTextType.AddressOutsideRangeText
                    );
                    break;
                case ServiceMethodType.Delivery:
                    this.translationsTab.ExcludeCustomTextTypes(
                        CustomTextType.RoomEntryText,
                        CustomTextType.DeliverypointLabel,
                        CustomTextType.DeliveryPointGroupLabel
                    );
                    break;
                case ServiceMethodType.PickUp:
                    this.translationsTab.ExcludeCustomTextTypes(
                        CustomTextType.RoomEntryText,
                        CustomTextType.DeliverypointLabel,
                        CustomTextType.DeliveryPointGroupLabel,
                        CustomTextType.InstructionsLabel,
                        CustomTextType.InstructionsPlaceholder,
                        CustomTextType.AddressMissingText,
                        CustomTextType.CityRequiredText,
                        CustomTextType.PostalCodeRequiredText,
                        CustomTextType.AddressOutsideRangeText,
                        CustomTextType.HouseNumberMissingText,
                        CustomTextType.AddressLookupLabel,
                        CustomTextType.AddressOutsideRangeText,
                        CustomTextType.AddressOutsideRangeText
                    );
                    break;
            }

            if (!CmsSessionHelper.CurrentUser.HasFeature(FeatureToggle.Covers))
            { translationsTab.ExcludeCustomTextTypes(CustomTextType.CoversTitleText, CustomTextType.CoversDescText); }
        }

        private void SetCustomProperties()
        {
            ServiceMethodEntity serviceMethodEntity = this.DataSourceAsServiceMethodEntity;

            if (serviceMethodEntity.ServiceChargeType == ChargeType.Fixed)
            {
                this.tbServiceCharge.Value = serviceMethodEntity.ServiceChargeAmount;
            }
            else if (serviceMethodEntity.ServiceChargeType == ChargeType.Percentage)
            {
                this.tbServiceCharge.Value = (decimal?)serviceMethodEntity.ServiceChargePercentage;
            }
            else
            {
                this.tbServiceCharge.Value = decimal.Zero;
            }
        }

        private void SetWarnings()
        {
            if (!this.DataSourceAsServiceMethodEntity.Active)
            {
                this.AddInformator(InformatorType.Warning, "Service method is not active.");
            }

            if (this.DataSourceAsServiceMethodEntity.ServiceChargeType == ChargeType.None)
            {
                this.AddInformator(InformatorType.Warning, "The service charge type is set to none which will prevent it from appearing on the checkout.");
            }

            if (!this.DataSourceAsServiceMethodEntity.Type.IsIn(ServiceMethodType.PickUp, ServiceMethodType.Delivery)
                && !this.DataSourceAsServiceMethodEntity.ServiceMethodDeliverypointgroupCollection.Any())
            {
                this.AddInformator(InformatorType.Warning, "The service method won't be available until a delivery point group has been configured.");
            }

            if (this.DataSourceAsServiceMethodEntity.Type == ServiceMethodType.Delivery && this.DataSourceAsServiceMethodEntity.MinimumAmountForFreeDelivery == 0)
            {
                this.AddInformator(InformatorType.Warning, "The amount for free delivery is set to 0 (zero).");
            }

            this.Validate();
        }
    }
}
