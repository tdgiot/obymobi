﻿using System;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Enums;

namespace Obymobi.ObymobiCms.AppLess
{
    public partial class Themes : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        #region Fields

        #endregion

        #region Methods

        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "Theme";
            this.EntityPageUrl = "~/AppLess/Theme.aspx";
            base.OnInit(e);
        }

        #endregion

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            LLBLGenProDataSource datasource = this.DataSource as LLBLGenProDataSource;
            if (datasource != null)
            {
                PredicateExpression filter = new PredicateExpression(ThemeFields.ParentCompanyId == CmsSessionHelper.CurrentCompanyId);
                datasource.FilterToUse = filter;             
            }
        }

        #endregion

        #region Properties
        
        public ThemeCollection DataSourceAsPageCollection
        {
            get
            {
                return this.DataSource as ThemeCollection;
            }
        }

        #endregion

    }
}
