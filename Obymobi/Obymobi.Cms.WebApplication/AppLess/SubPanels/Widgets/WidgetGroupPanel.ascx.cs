﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Utils;
using DevExpress.Web;
using DevExpress.Web.ASPxTreeList;
using DevExpress.Web.Data;
using DevExpress.Web.Internal;
using Dionysos.Web.UI.WebControls;
using Obymobi.Cms.WebApplication.AppLess.SubPanels.Widgets;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.ObymobiCms.AppLess.SubPanels
{
    public partial class WidgetGroupPanel : SubPanelCustomDataSource, Dionysos.Interfaces.ISaveableControl, IWidgetPanel
    {
        #region Fields

        private WidgetGroupWidgetDataSource widgetGroupWidgetDataSource;
        private TreeListCommandColumn editCommandColumn;

        #endregion

        #region Methods

        private void CreateTreeList()
        {
            this.tlWidgets.SettingsEditing.Mode = TreeListEditMode.Inline;
            this.tlWidgets.SettingsBehavior.ColumnResizeMode = ColumnResizeMode.Disabled;
            this.tlWidgets.SettingsBehavior.AllowSort = false;
            this.tlWidgets.SettingsBehavior.AllowDragDrop = false;
            this.tlWidgets.ClientInstanceName = this.TreelistId;
            this.tlWidgets.AutoGenerateColumns = false;
            this.tlWidgets.SettingsEditing.AllowNodeDragDrop = true;
            this.tlWidgets.SettingsEditing.AllowRecursiveDelete = true;
            this.tlWidgets.Settings.GridLines = GridLines.Both;
            this.tlWidgets.KeyFieldName = "WidgetId";
            this.tlWidgets.ClientSideEvents.EndDragNode = this.EndDragNodeMethod;
            this.tlWidgets.ClientSideEvents.NodeDblClick = this.NodeDblClickMethod;
            this.tlWidgets.NodeUpdating += NodeUpdating;
            this.tlWidgets.CustomErrorText += this.tlItems_CustomErrorText;

            TreeListTextColumn nameColumn = new TreeListTextColumn();
            nameColumn.Caption = "Name";
            nameColumn.VisibleIndex = 0;
            nameColumn.FieldName = "Name";
            nameColumn.CellStyle.Wrap = DefaultBoolean.True;
            nameColumn.EditFormSettings.VisibleIndex = 0;
            this.tlWidgets.Columns.Add(nameColumn);

            TreeListTextColumn textColumn = new TreeListTextColumn();
            textColumn.Caption = "Text";
            textColumn.VisibleIndex = 1;
            textColumn.FieldName = "Text";
            textColumn.CellStyle.Wrap = DefaultBoolean.True;
            textColumn.EditFormSettings.VisibleIndex = 2;
            this.tlWidgets.Columns.Add(textColumn);

            TreeListComboBoxColumn iconTypeColumn = new TreeListComboBoxColumn();
            iconTypeColumn.Caption = "Icon type";
            iconTypeColumn.VisibleIndex = 2;
            iconTypeColumn.FieldName = "IconType";
            iconTypeColumn.Width = Unit.Pixel(100);
            iconTypeColumn.VisibleIndex = 3;
            iconTypeColumn.CellStyle.Wrap = DefaultBoolean.True;
            iconTypeColumn.EditFormSettings.VisibleIndex = 3;
            this.tlWidgets.Columns.Add(iconTypeColumn);

            this.editCommandColumn = new TreeListCommandColumn();
            this.editCommandColumn.VisibleIndex = 4;
            this.editCommandColumn.Width = Unit.Pixel(60);
            this.editCommandColumn.ShowNewButtonInHeader = true;
            this.editCommandColumn.EditButton.Visible = true;
            this.editCommandColumn.NewButton.Visible = false;
            this.editCommandColumn.DeleteButton.Visible = true;
            this.editCommandColumn.ButtonType = ButtonType.Image;
            this.editCommandColumn.EditButton.Image.Url = this.ResolveUrl("~/images/icons/pencil.png");
            this.editCommandColumn.DeleteButton.Image.Url = this.ResolveUrl("~/images/icons/delete.png");
            this.editCommandColumn.CancelButton.Image.Url = this.ResolveUrl("~/images/icons/cross_grey.png");
            this.editCommandColumn.NewButton.Image.Url = this.ResolveUrl("~/images/icons/add2.png");
            this.editCommandColumn.UpdateButton.Image.Url = this.ResolveUrl("~/images/icons/disk.png");
            this.tlWidgets.Columns.Add(this.editCommandColumn);

            TreeListHyperLinkColumn editPageColumn = new TreeListHyperLinkColumn();
            editPageColumn.Name = "EditCommand";
            editPageColumn.Caption = " ";
            editPageColumn.VisibleIndex = 5;
            editPageColumn.Width = Unit.Pixel(15);
            editPageColumn.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            editPageColumn.PropertiesHyperLink.ImageUrl = "~/images/icons/page_edit.png";
            editPageColumn.EditCellStyle.CssClass = "hidden";
            editPageColumn.FieldName = "WidgetId";
            editPageColumn.EditFormSettings.VisibleIndex = 3;
            editPageColumn.EditFormCaptionStyle.CssClass = "hidden";
            this.tlWidgets.Columns.Add(editPageColumn);

            this.tlWidgets.HtmlDataCellPrepared += this.TlWidgets_HtmlDataCellPrepared;
            this.tlWidgets.CustomCallback += this.TlWidgets_CustomCallback;
            this.tlWidgets.CellEditorInitialize += this.TlWidgets_CellEditorInitialize;
            this.tlWidgets.NodeDeleted += this.TlWidgetsOnNodeDeleted;
            this.tlWidgets.NodeInserted += this.TlWidgetsOnNodeInserted;
        }

        public bool Save()
        {
            return true;
        }

        private void DeleteSelectedItems()
        {
            List<TreeListNode> selectedNodes = this.GetSelectedNodes();
            if (selectedNodes.Count <= 0)
            {
                this.PageAsPageDefault.MultiValidatorDefault.AddError("No item has been selected to be deleted.");
                this.PageAsPageDefault.Validate();
            }
            else
            {
                this.DeleteItems(selectedNodes);
                Response.Redirect(this.Request.RawUrl);
            }
        }

        private void DeleteItems(List<TreeListNode> nodes)
        {
            foreach (TreeListNode node in nodes)
            {
                if (int.TryParse(node.Key, out int widgetId))
                {
                    this.widgetGroupWidgetDataSource.Manager.Delete(widgetId);
                }
            }
        }

        private List<TreeListNode> GetSelectedNodes()
        {
            List<TreeListNode> selectedNodes = new List<TreeListNode>();

            foreach (TreeListNode node in this.tlWidgets.Nodes)
            {
                if (node.Selected)
                {
                    selectedNodes.Add(node);
                }
            }

            return selectedNodes;
        }

        private void ShowHideNewButtonInTreeListHeader(WidgetEntity widgetEntity)
        {
            if (this.editCommandColumn == null || !(widgetEntity is WidgetGroupEntity))
                return;

            WidgetGroupEntity widgetGroupEntity = (WidgetGroupEntity)widgetEntity;
            this.editCommandColumn.ShowNewButtonInHeader = widgetGroupEntity.ChildWidgetGroupWidgetCollection.Count < 3;
        }


        #endregion

        #region Properties

        private WidgetGroupEntity DataSourceAsWidgetGroupEntity
        {
            get { return (WidgetGroupEntity)this.DataSource; }
        }

        private string TreelistId
        {
            get { return string.Format("{0}_treelist", this.ID); }
        }

        private string EndDragNodeMethod
        {
            get
            {
                return @" function(s, e) {                                                                
                                e.cancel = true;
		                        var key = s.GetNodeKeyByRow(e.targetElement);
		                        " + this.TreelistId + @".PerformCustomCallback(e.nodeKey + ':' + key);	                            
                            }";
            }
        }

        private string NodeDblClickMethod
        {
            get { return @" function(s, e) {
	                            " + this.TreelistId + @".StartEdit(e.nodeKey);
		                        e.cancel = true;	                                                                    
                            }"; }
        }

        #endregion

        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.CreateTreeList();
        }

        public new void Load(WidgetEntity widgetEntity)
        {
            this.widgetGroupWidgetDataSource = new WidgetGroupWidgetDataSource(widgetEntity.WidgetId);
            this.tlWidgets.DataSource = this.widgetGroupWidgetDataSource;

            this.ShowHideNewButtonInTreeListHeader(widgetEntity);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.tlWidgets.DataBind();

            this.btDeleteBottom.Click += this.BtDeleteItems_Click;
        }        

        private void tlItems_CustomErrorText(object sender, TreeListCustomErrorTextEventArgs e)
        {
            if (e.Exception.InnerException != null)
            {
                ObymobiException exception = e.Exception.InnerException as ObymobiException;
                if (exception != null)
                {
                    e.ErrorText = exception.AdditionalMessage;
                }
                else
                {
                    e.ErrorText = e.Exception.InnerException.Message;
                }
            }
        }

        private void TlWidgets_HtmlDataCellPrepared(object sender, TreeListHtmlDataCellEventArgs e)
        {
            if (e.Column.Name == "EditCommand" && e.NodeKey != null)
            {
                string navigateUrl = string.Format("~/AppLess/Widget.aspx?id={0}&EntityName=WidgetActionButton", e.NodeKey);

                foreach (Control item in e.Cell.Controls)
                {
                    HyperLinkDisplayControl link = item as HyperLinkDisplayControl;
                    if (link != null)
                    {
                        link.NavigateUrl = navigateUrl;
                        break;
                    }
                }
            }
        }

        private void TlWidgets_CustomCallback(object sender, TreeListCustomCallbackEventArgs e)
        {
            // Node dragged callback
            string[] nodes = e.Argument.Split(new char[] { ':' });
            if (nodes.Length <= 1)
            {
                return;
            }

            if (!int.TryParse(nodes[0], out int draggedWidgetId) || !int.TryParse(nodes[1], out int draggedUponWidgetId))
            {
                return;
            }

            this.widgetGroupWidgetDataSource.Manager.DraggedToSort(draggedWidgetId, draggedUponWidgetId);
            this.tlWidgets.DataBind();
        }

        protected void TlWidgets_CellEditorInitialize(object sender, TreeListColumnEditorEventArgs e)
        {
            if (e.Column.FieldName == "IconType")
            {
                ASPxComboBox cbType = e.Editor as ASPxComboBox;
                cbType.DataBindEnum<ActionButtonIconType>(EnumUtil.ToArray<ActionButtonIconType>());                
            }
        }

        private void BtDeleteItems_Click(object sender, EventArgs e)
        {
            this.DeleteSelectedItems();
        }

        private void TlWidgetsOnNodeInserted(object sender, ASPxDataInsertedEventArgs e)
        {
            ASPxWebControl.RedirectOnCallback(this.Request.RawUrl);
        }

        private void TlWidgetsOnNodeDeleted(object sender, ASPxDataDeletedEventArgs e)
        {
            ASPxWebControl.RedirectOnCallback(this.Request.RawUrl);
        }

        private void NodeUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {
            int.TryParse(e.NewValues[nameof(WidgetActionButtonFields.WidgetId)].ToString(), out int widgetId);
            string updatedText = e.NewValues[nameof(WidgetActionButtonFields.Text)].ToString();

            WidgetEntity widgetEntity = new WidgetEntity(widgetId);
            WidgetActionButtonEntity widgetActionButtonEntity = new WidgetActionButtonEntity(widgetId);

            if (!widgetActionButtonEntity.IsNew && widgetActionButtonEntity.Text != updatedText)
            {
                 widgetActionButtonEntity.Text = updatedText;
                 bool isSaved = widgetActionButtonEntity.Save();

                 if (isSaved)
                 {
                     CompanyEntity companyEntity = new CompanyEntity(widgetEntity.ParentCompanyId);
                     CustomTextHelper.UpdateCustomTexts(widgetActionButtonEntity, companyEntity);
                 }
            }
        }

        #endregion        
    }
}