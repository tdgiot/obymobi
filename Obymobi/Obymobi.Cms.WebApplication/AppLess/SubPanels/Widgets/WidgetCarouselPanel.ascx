﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.AppLess.SubPanels.WidgetCarouselPanel" Codebehind="WidgetCarouselPanel.ascx.cs" %>
<%@ Register TagPrefix="dxwtl" Namespace="DevExpress.Web.ASPxTreeList" Assembly="DevExpress.Web.ASPxTreeList.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<tr>
	<td class="label">
        <D:Label runat="server" id="lblItems">Items</D:Label>
    </td>
    <td class="control" colspan="3">
        <div style="width: 788px;">                
            <dxwtl:ASPxTreeList ID="tlItems" ClientInstanceName="tlItems" runat="server" EnableViewState="false" AutoGenerateColumns="False">
                <SettingsSelection Enabled="true" Recursive="false" />                                                
            </dxwtl:ASPxTreeList>    
            <D:Button runat="server" ID="btDeleteBottom" Text="Delete selected item(s)" PreSubmitWarning="Are you sure you want to delete these items?" Style="float:right; margin-top:10px" />
        </div>
    </td>
</tr>