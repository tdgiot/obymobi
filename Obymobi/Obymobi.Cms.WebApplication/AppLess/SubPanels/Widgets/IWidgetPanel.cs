﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Obymobi.Data.EntityClasses;

namespace Obymobi.Cms.WebApplication.AppLess.SubPanels.Widgets
{
    public interface IWidgetPanel
    {
        void Load(WidgetEntity widgetEntity);
    }
}
