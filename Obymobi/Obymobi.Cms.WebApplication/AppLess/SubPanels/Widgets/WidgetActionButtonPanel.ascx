﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.AppLess.SubPanels.WidgetActionButtonPanel" Codebehind="WidgetActionButtonPanel.ascx.cs" %>
<tr>
	<td class="label">
        <D:Label runat="server" id="lblText">Text</D:Label>
    </td>
    <td class="control">
        <D:TextBoxString runat="server" ID="tbText" IsRequired="True" MaxLength="100"></D:TextBoxString>
    </td>
	<td class="label">
        <D:Label runat="server" id="lblIconType">Icon type</D:Label>
    </td>
    <td class="control">
        <X:ComboBoxInt runat="server" ID="cbIconType" UseDataBinding="True" DisplayEmptyItem="false" IsRequired="true"></X:ComboBoxInt>
    </td>
</tr>