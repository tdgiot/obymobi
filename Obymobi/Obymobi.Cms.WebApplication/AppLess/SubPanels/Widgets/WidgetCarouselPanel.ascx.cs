﻿using DevExpress.Utils;
using DevExpress.Web;
using DevExpress.Web.Internal;
using DevExpress.Web.ASPxTreeList;
using Dionysos.Web.UI.WebControls;
using Obymobi.Cms.WebApplication.AppLess.SubPanels.Widgets;
using Obymobi.Data.EntityClasses;
using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Obymobi.ObymobiCms.AppLess.SubPanels
{
    public partial class WidgetCarouselPanel : SubPanelCustomDataSource, Dionysos.Interfaces.ISaveableControl, IWidgetPanel
    {
        #region Fields

        private CarouselItemDataSource carouselItemDataSource;

        #endregion

        #region Methods

        private void CreateTreeList()
        {
            this.tlItems.SettingsEditing.Mode = TreeListEditMode.EditFormAndDisplayNode;
            this.tlItems.SettingsBehavior.ColumnResizeMode = ColumnResizeMode.Disabled;
            this.tlItems.SettingsBehavior.AllowSort = false;
            this.tlItems.SettingsBehavior.AllowDragDrop = false;
            this.tlItems.ClientInstanceName = this.TreelistId;
            this.tlItems.AutoGenerateColumns = false;
            this.tlItems.SettingsEditing.AllowNodeDragDrop = true;
            this.tlItems.SettingsEditing.AllowRecursiveDelete = true;
            this.tlItems.Settings.GridLines = GridLines.Both;
            this.tlItems.KeyFieldName = "CarouselItemId";
            this.tlItems.ClientSideEvents.EndDragNode = this.EndDragNodeMethod;
            this.tlItems.ClientSideEvents.NodeDblClick = this.NodeDblClickMethod;
            this.tlItems.CustomErrorText += this.tlItems_CustomErrorText;

            TreeListTextColumn nameColumn = new TreeListTextColumn();
            nameColumn.Caption = "Name";
            nameColumn.VisibleIndex = 0;
            nameColumn.FieldName = "Name";
            nameColumn.CellStyle.Wrap = DefaultBoolean.True;
            nameColumn.EditFormSettings.VisibleIndex = 0;
            this.tlItems.Columns.Add(nameColumn);

            TreeListTextColumn messageColumn = new TreeListTextColumn();
            messageColumn.Caption = "Message";
            messageColumn.VisibleIndex = 1;
            messageColumn.FieldName = "Message";
            messageColumn.CellStyle.Wrap = DefaultBoolean.True;
            messageColumn.EditFormSettings.VisibleIndex = 2;
            this.tlItems.Columns.Add(messageColumn);

            TreeListCommandColumn editCommandColumn = new TreeListCommandColumn();
            editCommandColumn.VisibleIndex = 4;
            editCommandColumn.Width = Unit.Pixel(60);
            editCommandColumn.ShowNewButtonInHeader = true;
            editCommandColumn.EditButton.Visible = true;
            editCommandColumn.NewButton.Visible = false;
            editCommandColumn.DeleteButton.Visible = true;
            editCommandColumn.ButtonType = ButtonType.Image;
            editCommandColumn.EditButton.Image.Url = this.ResolveUrl("~/images/icons/pencil.png");
            editCommandColumn.DeleteButton.Image.Url = this.ResolveUrl("~/images/icons/delete.png");
            editCommandColumn.CancelButton.Image.Url = this.ResolveUrl("~/images/icons/cross_grey.png");
            editCommandColumn.NewButton.Image.Url = this.ResolveUrl("~/images/icons/add2.png");
            editCommandColumn.UpdateButton.Image.Url = this.ResolveUrl("~/images/icons/disk.png");
            this.tlItems.Columns.Add(editCommandColumn);

            TreeListHyperLinkColumn editPageColumn = new TreeListHyperLinkColumn();
            editPageColumn.Name = "EditCommand";
            editPageColumn.Caption = " ";
            editPageColumn.VisibleIndex = 5;
            editPageColumn.Width = Unit.Pixel(15);
            editPageColumn.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            editPageColumn.PropertiesHyperLink.ImageUrl = "~/images/icons/page_edit.png";
            editPageColumn.EditCellStyle.CssClass = "hidden";
            editPageColumn.FieldName = "CarouselItemId";
            editPageColumn.EditFormSettings.VisibleIndex = 3;
            editPageColumn.EditFormCaptionStyle.CssClass = "hidden";
            this.tlItems.Columns.Add(editPageColumn);

            this.tlItems.HtmlDataCellPrepared += this.tlItems_HtmlDataCellPrepared;
            this.tlItems.CustomCallback += this.tlItems_CustomCallback;

            this.InitDataSource();
        }

        private void InitDataSource()
        {
            if (this.DataSource != null)
            {
                this.carouselItemDataSource = new CarouselItemDataSource(this.DataSourceAsWidgetCarouselEntity.WidgetId);
                this.tlItems.DataSource = this.carouselItemDataSource;
            }
        }        

        public bool Save()
        {
            return true;
        }

        private void DeleteSelectedItems()
        {
            List<TreeListNode> selectedNodes = this.GetSelectedNodes();
            if (selectedNodes.Count <= 0)
            {
                this.PageAsPageDefault.MultiValidatorDefault.AddError("No item has been selected to be deleted.");
                this.PageAsPageDefault.Validate();
            }
            else
            {
                this.DeleteItems(selectedNodes);

                this.InitDataSource();
                this.tlItems.DataBind();

                this.PageAsPageDefault.AddInformatorInfo("The selected item(s) has been deleted.");
                this.PageAsPageDefault.Validate();
            }
        }

        private void DeleteItems(List<TreeListNode> nodes)
        {
            foreach (TreeListNode node in nodes)
            {
                if (int.TryParse(node.Key, out int carouselItemId))
                {
                    this.carouselItemDataSource.Manager.Delete(carouselItemId);
                }
            }
        }

        private List<TreeListNode> GetSelectedNodes()
        {
            List<TreeListNode> selectedNodes = new List<TreeListNode>();

            foreach (TreeListNode node in this.tlItems.Nodes)
            {
                if (node.Selected)
                {
                    selectedNodes.Add(node);
                }
            }

            return selectedNodes;
        }

        #endregion

        #region Properties

        private WidgetCarouselEntity DataSourceAsWidgetCarouselEntity
        {
            get { return (WidgetCarouselEntity)this.DataSource; }
        }

        private string TreelistId
        {
            get { return string.Format("{0}_treelist", this.ID); }
        }

        private string EndDragNodeMethod
        {
            get
            {
                return @" function(s, e) {                                                                
                                e.cancel = true;
		                        var key = s.GetNodeKeyByRow(e.targetElement);
		                        " + this.TreelistId + @".PerformCustomCallback(e.nodeKey + ':' + key);	                            
                            }";
            }
        }

        private string NodeDblClickMethod
        {
            get { return @" function(s, e) {
	                            " + this.TreelistId + @".StartEdit(e.nodeKey);
		                        e.cancel = true;	                                                                    
                            }"; }
        }

        #endregion

        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.CreateTreeList();
        }

        public new void Load(WidgetEntity widgetEntity)
        {
            this.carouselItemDataSource = new CarouselItemDataSource(widgetEntity.WidgetId);
            this.tlItems.DataSource = this.carouselItemDataSource;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.tlItems.DataBind();

            this.btDeleteBottom.Click += this.BtDeleteItems_Click;
        }        

        private void tlItems_CustomErrorText(object sender, TreeListCustomErrorTextEventArgs e)
        {
            if (e.Exception.InnerException != null)
            {
                ObymobiException exception = e.Exception.InnerException as ObymobiException;
                if (exception != null)
                {
                    e.ErrorText = exception.AdditionalMessage;
                }
                else
                {
                    e.ErrorText = e.Exception.InnerException.Message;
                }
            }
        }

        private void tlItems_HtmlDataCellPrepared(object sender, TreeListHtmlDataCellEventArgs e)
        {
            if (e.Column.Name == "EditCommand" && e.NodeKey != null)
            {
                string navigateUrl = string.Format("~/AppLess/CarouselItem.aspx?id={0}", e.NodeKey);

                foreach (Control item in e.Cell.Controls)
                {
                    HyperLinkDisplayControl link = item as HyperLinkDisplayControl;
                    if (link != null)
                    {
                        link.NavigateUrl = navigateUrl;
                        break;
                    }
                }
            }
        }

        private void tlItems_CustomCallback(object sender, TreeListCustomCallbackEventArgs e)
        {
            // Node dragged callback
            string[] nodes = e.Argument.Split(new char[] { ':' });
            if (nodes.Length <= 1)
            {
                return;
            }

            if (!int.TryParse(nodes[0], out int draggedCarouselItemId) || !int.TryParse(nodes[1], out int draggedUponCarouselItemId))
            {
                return;
            }

            this.carouselItemDataSource.Manager.DraggedToSort(draggedCarouselItemId, draggedUponCarouselItemId);
            this.tlItems.DataBind();
        }

        private void BtDeleteItems_Click(object sender, EventArgs e)
        {
            this.DeleteSelectedItems();
        }        

        #endregion        
    }
}