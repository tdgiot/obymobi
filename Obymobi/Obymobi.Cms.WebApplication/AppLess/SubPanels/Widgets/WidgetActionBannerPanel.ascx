﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.AppLess.SubPanels.WidgetActionBannerPanel" Codebehind="WidgetActionBannerPanel.ascx.cs" %>
<tr>
	<td class="label">
        <D:Label runat="server" id="lblText">Text</D:Label>
    </td>
    <td class="control">
        <D:TextBoxString runat="server" ID="tbText" MaxLength="100"></D:TextBoxString>
    </td>
    <td class="label">
	    <D:Label runat="server" id="lblOverlayType">Overlay Type</D:Label>
    </td>
    <td class="control">
	    <X:ComboBoxEnum runat="server" ID="cbOverlayType" Type="Obymobi.Enums.OverlayType, Obymobi" />
    </td>
</tr>