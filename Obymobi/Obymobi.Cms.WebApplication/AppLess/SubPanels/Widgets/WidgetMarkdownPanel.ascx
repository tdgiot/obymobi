﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WidgetMarkdownPanel.ascx.cs" Inherits="Obymobi.Cms.WebApplication.AppLess.SubPanels.Widgets.WidgetMarkdownPanel" %>

<tr>
    <td class="label">
        <D:Label runat="server" ID="lblTitle">Title</D:Label>
    </td>
    <td class="control">
        <D:TextBoxString runat="server" ID="tbTitle" IsRequired="true" MaxLength="255"></D:TextBoxString>
    </td>
</tr>

<tr>
    <td class="label">
        <D:Label runat="server" ID="lblText">Text</D:Label>
    </td>
    <td class="control threeCol" colspan="2">
        <D:TextBoxMultiLine ID="tbText" runat="server" Rows="15" UseDataBinding="false" ClientIDMode="Static" onkeyup="parseMarkdown(this);" IsRequired="true"></D:TextBoxMultiLine>
	    <span style="display: inline-block; margin-left: 0; font-size: smaller; font-weight: normal">
		    <a href="https://guides.github.com/features/mastering-markdown/" style="display: inline" target="_blank">Markdown</a>-markup can be used. By default links will open in the embedded browser, this can changed by post-fixing the url with: <strong>#internal</strong> to open in a the same page, <strong>#external</strong> to open in a new browser tab.
	    </span>
        <br />
    </td>
    <td class="control">
        <div id="markdown-output" class="markdown_preview markdown-body">
        </div>
    </td>
</tr>

<script type="text/javascript">
    function setDescription() {
        let description = document.getElementById("tbText").innerHTML;
        document.getElementById("tbText").scrollTop = 99999;
        document.getElementById("markdown-output").innerHTML = markdown.toHTML(description);
    }

    function parseMarkdown(obj) {
        let text = obj.value;
        document.getElementById("markdown-output").innerHTML = markdown.toHTML(text);
    }

    let description = document.getElementById("tbText");
    if (description != null) {
        parseMarkdown(description);
    }
</script>
