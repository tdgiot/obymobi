﻿using System;
using System.Web.UI;
using Dionysos.Interfaces;
using MarkdownSharp.Extensions;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;
using DionysosUI = Dionysos.Web.UI;

namespace Obymobi.Cms.WebApplication.AppLess.SubPanels.Widgets
{
    public partial class WidgetMarkdownPanel : UserControl, ISaveableControl, IWidgetPanel
    {
        private WidgetMarkdownEntity _widgetMarkdownEntity;

        public new void Load(WidgetEntity widgetEntity)
        {
            this._widgetMarkdownEntity = widgetEntity as WidgetMarkdownEntity;

            this.tbText.Value = MarkdownHelper.RemoveMarkdown(this._widgetMarkdownEntity.Text);
        }

        public bool Save()
        {
            if (string.IsNullOrEmpty(this.tbText.Value))
            {
                return false;
            }

            this._widgetMarkdownEntity.Text = MarkdownHelper.AddMarkdown(this.tbText.Value);

            if (this._widgetMarkdownEntity.Save())
            {
                CompanyEntity companyEntity = new CompanyEntity(this._widgetMarkdownEntity.ParentCompanyId);

                CustomTextHelper.UpdateCustomTexts(this._widgetMarkdownEntity, companyEntity.CultureCode);

                return true;
            }

            return false;
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            DionysosUI.MasterPage.LinkedJsFiles.Add("~/js/markdown.js", 100);
            DionysosUI.MasterPage.LinkedCssFiles.Add("~/css/markdown.css", 100);
        }
    }
}
