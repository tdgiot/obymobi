﻿using Obymobi.Cms.WebApplication.AppLess.SubPanels.Widgets;
using System;
using System.Web.UI;
using Obymobi.Data.EntityClasses;

namespace Obymobi.ObymobiCms.AppLess.SubPanels
{
    public partial class WidgetActionBannerPanel : UserControl, Dionysos.Interfaces.ISaveableControl, IWidgetPanel
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public bool Save()
        {
            return true;
        }

        public new void Load(WidgetEntity widgetEntity)
        {           
        }
    }
}