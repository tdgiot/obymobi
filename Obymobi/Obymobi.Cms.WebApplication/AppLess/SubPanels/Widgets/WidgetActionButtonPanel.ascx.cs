﻿using Obymobi.Cms.WebApplication.AppLess.SubPanels.Widgets;
using Obymobi.Enums;
using System;
using System.Web.UI;
using Obymobi.Data.EntityClasses;

namespace Obymobi.ObymobiCms.AppLess.SubPanels
{
    public partial class WidgetActionButtonPanel : UserControl, Dionysos.Interfaces.ISaveableControl, IWidgetPanel
    {
        protected override void OnInit(EventArgs e)
        {
            this.cbIconType.DataBindEnum<ActionButtonIconType>();
            base.OnInit(e);
        }        

        protected void Page_Load(object sender, EventArgs e)
        {
        
        }

        public new void Load(WidgetEntity widgetEntity)
        {
            
        }

        public void HideIconTypeCombobox()
        {
            this.lblIconType.Visible = false;
            this.cbIconType.Visible = false;
        }

        public bool Save()
        {
            return true;
        }
    }
}