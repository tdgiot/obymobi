﻿using System;
using Dionysos.Data;
using Dionysos.Web;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dionysos.Web.UI.DevExControls;
using Obymobi.Enums;
using System.Web.UI.WebControls;

namespace Obymobi.ObymobiCms.AppLess.SubPanels
{
    public partial class ServiceMethodCollection : SubPanelLLBLOverviewDataSourceCollection
    {
        #region Fields

        private int relatedEntityId;

        #endregion

        #region Methods

        private void HookupEvents()
        {
            this.gvcsServiceMethodCollection.GridView.DataBound += GridView_DataBound;
            this.btAdd.Click += BtAdd_Click;
            this.MainGridView.HtmlRowPrepared += this.MainGridView_HtmlRowPrepared;
        }

        private void BtAdd_Click(object sender, EventArgs e)
        {
            this.Response.Redirect($"{this.EntityPageUrl}?mode=add&relatedEntityId={this.relatedEntityId}&id=");
        }

        private void SetDataSourceFilter()
        {
            if (this.DataSource is LLBLGenProDataSource datasource)
            {
                PredicateExpression filter = new PredicateExpression();
                filter.Add(ServiceMethodFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
                filter.Add(ServiceMethodFields.OutletId == this.relatedEntityId);

                datasource.FilterToUse = filter;
            }
        }

        #endregion

        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            this.EntityName = "ServiceMethod";
            this.EntityPageUrl = EntityInformationUtil.GetEntityInformation(this.EntityName).DefaultEntityEditPage;

            this.relatedEntityId = QueryStringHelper.GetInt("id");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.HookupEvents();
            this.SetDataSourceFilter();
        }

        private void GridView_DataBound(object sender, EventArgs e)
        {
            this.gvcsServiceMethodCollection.GridView.SortBy(this.gvcsServiceMethodCollection.GridView.Columns[0], DevExpress.Data.ColumnSortOrder.Descending);
        }

        private void MainGridView_HtmlRowPrepared(object sender, DevExpress.Web.ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType != DevExpress.Web.GridViewRowType.Data) return;

            object type = e.GetValue("Type");
            if (type == null)
                return;

            ServiceMethodType typeAsEnum = ((int)type).ToEnum<ServiceMethodType>();
            this.SetCellValue(e.Row, "Type", typeAsEnum.GetStringValue());
        }

        #endregion
    }
}