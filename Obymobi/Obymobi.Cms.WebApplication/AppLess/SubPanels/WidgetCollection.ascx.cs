﻿using System;
using Dionysos.Web;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.AppLess.SubPanels
{
    public partial class WidgetCollection : Dionysos.Web.UI.DevExControls.SubPanelLLBLOverviewDataSourceCollection
    {
        #region Methods

        private void HookupEvents()
        {
            this.cbType.DataBindEnum<ApplessWidgetType>();

            this.gvcsWidgetCollection.GridView.DataBound += GridView_DataBound;
            this.btAdd.Click += BtAdd_Click;
        }

        private void BtAdd_Click(object sender, EventArgs e)
        {
            if (this.cbType.ValidId <= -1)
            {
                this.PageAsPageLLBLGenEntity.MultiValidatorDefault.AddInformation("Please select a widget type first");
                this.PageAsPageLLBLGenEntity.Validate();
            }
            else if (!QueryStringHelper.HasValue("id"))
            {
                this.PageAsPageLLBLGenEntity.MultiValidatorDefault.AddInformation("No valid application configuration");
                this.PageAsPageLLBLGenEntity.Validate();
            }
            else
            {
                int applicationConfigurationId = QueryStringHelper.GetInt("id");
                ApplessWidgetType widgetType = this.cbType.ValidId.ToEnum<ApplessWidgetType>();

                this.Response.Redirect($"~/AppLess/Widget.aspx?mode=add&entity=Widget&ApplicationConfigurationId={applicationConfigurationId}&id=&EntityName={widgetType}");
            }
        }

        private void SetDataSourceFilter()
        {
            IEntity entity = this.PageAsPageLLBLGenEntity.DataSource;

            LLBLGenProDataSource datasource = this.DataSource as LLBLGenProDataSource;
            if (datasource != null)
            {
                PredicateExpression filter = new PredicateExpression();

                if (entity is ApplicationConfigurationEntity)
                {
                    filter.Add(WidgetFields.ApplicationConfigurationId == ((ApplicationConfigurationEntity)entity).ApplicationConfigurationId);
                }
                else 
                {
                    filter.Add(WidgetFields.ParentCompanyId == CmsSessionHelper.CurrentCompanyId);
                }
                    
                datasource.FilterToUse = filter;
            }
        }

        public void SetGui()
        {
            
        }

        #endregion

        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.EntityName = "Widget";
            this.EntityPageUrl = "~/AppLess/Widget.aspx";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.HookupEvents();
            this.SetDataSourceFilter();
            this.SetGui();
        }

        private void GridView_DataBound(object sender, EventArgs e)
        {
            this.gvcsWidgetCollection.GridView.SortBy(this.gvcsWidgetCollection.GridView.Columns[0], DevExpress.Data.ColumnSortOrder.Descending);
        }

        #endregion
    }
}