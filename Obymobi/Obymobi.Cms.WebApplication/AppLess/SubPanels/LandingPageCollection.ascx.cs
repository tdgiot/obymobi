﻿using System;
using Dionysos.Web;
using Obymobi.Cms.Logic.Models;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.AppLess.SubPanels
{
    public partial class LandingPageCollection : Dionysos.Web.UI.DevExControls.SubPanelLLBLOverviewDataSourceCollection
    {
        private const string FIELD_NAME_EXTERNAL_ID = "ExternalId";
        private const string FIELD_NAME_OUTLET_NAME = "OutletEntity.Name";
        private const string FIELD_NAME_NAVIGATION_MENU_NAME = "NavigationMenuEntity.Name";
        private const string FIELD_NAME_THEME_NAME = "ThemeEntity.Name";

        private CloudEnvironment CloudEnvironment => WebEnvironmentHelper.CloudEnvironment;
        private ApplicationConfigurationEntity DataSourceAsApplicationConfigurationEntity => this.PageAsPageLLBLGenEntity.DataSource as ApplicationConfigurationEntity;

        private void HookupEvents()
        {
            this.gvcsLandingPageCollection.GridView.DataBound += GridView_DataBound;
            this.btAdd.Click += BtAdd_Click;
            this.MainGridView.HtmlRowPrepared += MainGridView_HtmlRowPrepared;
        }

        private void BtAdd_Click(object sender, EventArgs e)
        {
            int applicationConfigurationId = QueryStringHelper.GetInt("id");
            this.Response.Redirect($"~/AppLess/Page.aspx?mode=add&entity=LandingPage&ApplicationConfigurationId={applicationConfigurationId}&id=");
        }

        private void SetDataSourceFilter()
        {
            IEntity entity = this.PageAsPageLLBLGenEntity.DataSource;

            if (this.DataSource is LLBLGenProDataSource dataSource)
            {
                PredicateExpression filter = new PredicateExpression();
                if (entity is ApplicationConfigurationEntity applicationConfigurationEntity)
                {
                    filter.Add(LandingPageFields.ApplicationConfigurationId == applicationConfigurationEntity.ApplicationConfigurationId);
                    filter.Add(LandingPageFields.ParentCompanyId == applicationConfigurationEntity.CompanyId);
                }
                else
                {
                    filter.Add(LandingPageFields.ParentCompanyId == CmsSessionHelper.CurrentCompanyId);
                }

                dataSource.FilterToUse = filter;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.EntityName = "LandingPage";
            this.EntityPageUrl = "~/AppLess/Page.aspx";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.HookupEvents();
            this.SetDataSourceFilter();
        }

        private void GridView_DataBound(object sender, EventArgs e)
        {
            this.gvcsLandingPageCollection.GridView.SortBy(this.gvcsLandingPageCollection.GridView.Columns[0], DevExpress.Data.ColumnSortOrder.Descending);
        }

        private void MainGridView_HtmlRowPrepared(object sender, DevExpress.Web.ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType != DevExpress.Web.GridViewRowType.Data) return;

            if (!int.TryParse(e.KeyValue.ToString(), out int landingPageId)) return;

            LandingPageEntity landingPageEntity = new LandingPageEntity(landingPageId);

            SetCellUrl(FIELD_NAME_EXTERNAL_ID, new AppLessPageUrl(CloudEnvironment, DataSourceAsApplicationConfigurationEntity.ExternalId, landingPageEntity.ExternalId), e);
            SetCellUrl(FIELD_NAME_OUTLET_NAME, $"../AppLess/Outlet.aspx?id={landingPageEntity.OutletId}", e);
            SetCellUrl(FIELD_NAME_NAVIGATION_MENU_NAME, $"../AppLess/NavigationMenu.aspx?id={landingPageEntity.NavigationMenuId}", e);
            SetCellUrl(FIELD_NAME_THEME_NAME, $"../AppLess/Theme.aspx?id={landingPageEntity.ThemeId}", e);
        }

        private void SetCellUrl(string fieldName, string url, DevExpress.Web.ASPxGridViewTableRowEventArgs e)
        {
            if (MainGridView.Columns[fieldName] == null || e.GetValue(fieldName) == null)
                return;

            string text = e.GetValue(fieldName).ToString();
            string value = $"<a href=\"{url}\" target=\"_blank\">{text}</a>";

            SetCellValue(e.Row, fieldName, value);
        }
    }
}