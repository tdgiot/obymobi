﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.AppLess.SubPanels.WidgetTreeListPanel" Codebehind="WidgetTreeListPanel.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v20.1" Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dxwtl" %>
<%@ Register TagPrefix="dxwtl" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<script type="text/javascript">

    function OnInit(s, e) {        
        ASPxClientUtils.AttachEventToElement(s.GetMainElement(), "keypress",
        function (evt) {
            switch (evt.keyCode) {
                    //ENTER
                case 13:
                    if (s.IsEditing()) {
                        s.UpdateEdit();
                        evt.stopPropagation();
                    }
                    break;

                    //ESC               
                case 27:
                    if (s.IsEditing()) {
                        s.CancelEdit();
                        evt.stopPropagation();
                    }
                    break;
            }
        });
    }

</script>
<div style="width: 788px;">
    <dxwtl:ASPxTreeList ID="tlWidgets" ClientInstanceName="tlWidgets" runat="server" EnableViewState="false" AutoGenerateColumns="False">
        <SettingsSelection Enabled="true" Recursive="false" />
        <ClientSideEvents Init="OnInit" />
        <Templates>
            <EditForm>
                <dxwtl:ASPxPageControl ID="tabs" runat="server" ActiveTabIndex="0" Width="100%">
                    <TabPages>
                        <dxwtl:TabPage Text="New">
                            <ContentCollection>
                                 <dxwtl:ContentControl runat="server">
                                    <table class="dataformV2">
                                        <tr>
                                            <td class="label">Friendly Name</td>
                                            <td class="control" colspan="3">
                                                <dxe:ASPxTextBox ID="tbName" style="width: 100%" runat="server" Value='<%# Bind("Name") %>' />
                                            </td>
	                                        <td class="label">Type</td>
	                                        <td class="control">
		                                        <dxe:ASPxComboBox ID="cbType" ClientInstanceName="cbType" runat="server" IncrementalFilteringMode="Contains" Value='<%# Bind("Type") %>' OnInit="CbType_Init" />
	                                        </td>
                                        </tr>                                    
                                    </table>
                                </dxwtl:ContentControl>
                            </ContentCollection>
                        </dxwtl:TabPage>
                        <dxwtl:TabPage Text="Existing">
                            <ContentCollection>
                                <dxwtl:ContentControl runat="server">                                    
                                    <table class="dataformV2">
	                                    <tr>
                                            <td class="label">Friendly Name</td>
                                            <td class="control" colspan="3">
                                                <dxe:ASPxComboBox ID="cbExistingWidgetId" ClientInstanceName="cbExistingWidgetId" style="width: 100%" runat="server" IncrementalFilteringMode="Contains" EnableCallbackMode="True" CallbackPageSize="10" OnInit="CbExistingWidgetId_Init" />
                                            </td>
                                        </tr>                                     
                                    </table>
                                </dxwtl:ContentControl>
                            </ContentCollection>
                        </dxwtl:TabPage>
                    </TabPages>
                </dxwtl:ASPxPageControl>
                <div style="text-align: right; padding-top: 8px">
                    <dxwtl:ASPxTreeListTemplateReplacement runat="server" ReplacementType="UpdateButton"  />
                    <dxwtl:ASPxTreeListTemplateReplacement runat="server" ReplacementType="CancelButton" />
                </div>
            </EditForm>
        </Templates>
    </dxwtl:ASPxTreeList>
    <div id="divDeletePages" style="margin-top: 4px;">
        <D:Button runat="server" ID="btDeleteItemsBottom" style="float:right;" Text="Delete" />        
    </div> 
</div>