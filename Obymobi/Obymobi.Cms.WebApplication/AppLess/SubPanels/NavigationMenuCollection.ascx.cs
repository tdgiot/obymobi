﻿using System;
using Dionysos.Web;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.AppLess.SubPanels
{
    public partial class NavigationMenuCollection : Dionysos.Web.UI.DevExControls.SubPanelLLBLOverviewDataSourceCollection
    {
        #region Methods

        private void HookupEvents()
        {
            this.gvcsNavigationMenuCollection.GridView.DataBound += GridView_DataBound;
            this.btAdd.Click += BtAdd_Click;
        }

        private void BtAdd_Click(object sender, EventArgs e)
        {
            int applicationConfigurationId = QueryStringHelper.GetInt("id");
            this.Response.Redirect($"~/AppLess/NavigationMenu.aspx?mode=add&entity=NavigationMenu&ApplicationConfigurationId={applicationConfigurationId}&id=");
        }

        private void SetDataSourceFilter()
        {
            IEntity entity = this.PageAsPageLLBLGenEntity.DataSource;

            LLBLGenProDataSource datasource = this.DataSource as LLBLGenProDataSource;
            if (datasource != null)
            {
                PredicateExpression filter = new PredicateExpression();

                if (entity is ApplicationConfigurationEntity)
                {
                    filter.Add(NavigationMenuFields.ApplicationConfigurationId == ((ApplicationConfigurationEntity)entity).ApplicationConfigurationId);
                }
                else
                {
                    filter.Add(NavigationMenuFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
                }

                datasource.FilterToUse = filter;
            }
        }

        public void SetGui()
        {
            
        }

        #endregion

        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.EntityName = "NavigationMenu";
            this.EntityPageUrl = "~/AppLess/NavigationMenu.aspx";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.HookupEvents();
            this.SetDataSourceFilter();
            this.SetGui();
        }

        private void GridView_DataBound(object sender, EventArgs e)
        {
            this.gvcsNavigationMenuCollection.GridView.SortBy(this.gvcsNavigationMenuCollection.GridView.Columns[0], DevExpress.Data.ColumnSortOrder.Descending);
        }

        #endregion
    }
}