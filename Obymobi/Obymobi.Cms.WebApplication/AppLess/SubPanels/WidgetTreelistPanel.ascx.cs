﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Utils;
using DevExpress.Web;
using DevExpress.Web;
using DevExpress.Web.Internal;
using DevExpress.Web.ASPxTreeList;
using DevExpress.Web.Data;
using Dionysos;
using Dionysos.Data.LLBLGen;
using Dionysos.Web.UI.WebControls;
using Obymobi.Cms.Logic.DataSources;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.AppLess.SubPanels
{
    public partial class WidgetTreeListPanel : SubPanelCustomDataSource
    {
        #region  Fields

        private WidgetDataSource widgetDataSource = new WidgetDataSource();
        private LandingPageWidgetDataSource landingPageWidgetDataSource;
        
        private const string DeleteMessage = "Are you sure you want to delete these items? Child items will be automatically removed if the parent item is selected.";

        #endregion

        #region Properties

        private LandingPageEntity ParentDataSourceAsPageEntity => this.DataSource as LandingPageEntity;

        private string EndDragNodeMethod
        {
            get
            {
                return @" function(s, e) {                                                                
                            e.cancel = true;
		                    var key = s.GetNodeKeyByRow(e.targetElement);
		                    " + this.TreelistId + @".PerformCustomCallback(e.nodeKey + ':' + key);
	                        }";
            }
        }

        private string NodeDblClickMethod
        {
            get { return @" function(s, e) {
	                            " + this.TreelistId + @".StartEdit(e.nodeKey);
		                        e.cancel = true;	                                                                    
                            }"; }
        }

        #endregion

        #region Methods

        public void InitDataSource()
        {
            if (this.DataSource != null)
            {
                this.landingPageWidgetDataSource = new LandingPageWidgetDataSource(this.ParentDataSourceAsPageEntity.LandingPageId);

                this.tlWidgets.DataSource = this.landingPageWidgetDataSource;
                this.tlWidgets.DataBind();
            }
        }

        private void CreateTreeList()
        {
            this.tlWidgets.SettingsEditing.Mode = TreeListEditMode.EditFormAndDisplayNode;
            this.tlWidgets.SettingsBehavior.ColumnResizeMode = ColumnResizeMode.Disabled;
            this.tlWidgets.SettingsBehavior.AllowSort = false;
            this.tlWidgets.SettingsBehavior.AllowDragDrop = false;
            this.tlWidgets.ClientInstanceName = this.TreelistId;
            this.tlWidgets.AutoGenerateColumns = false;
            this.tlWidgets.SettingsEditing.AllowNodeDragDrop = true;
            this.tlWidgets.SettingsEditing.AllowRecursiveDelete = true;
            this.tlWidgets.Settings.GridLines = GridLines.Both;
            this.tlWidgets.KeyFieldName = nameof(WidgetFields.WidgetId);
            this.tlWidgets.ClientSideEvents.EndDragNode = this.EndDragNodeMethod;
            this.tlWidgets.ClientSideEvents.NodeDblClick = this.NodeDblClickMethod;
            this.tlWidgets.CustomErrorText += this.CustomErrorText;

            TreeListTextColumn nameColumn = new TreeListTextColumn();
            nameColumn.Name = "Name";
            nameColumn.Caption = "Friendly Name";
            nameColumn.VisibleIndex = 0;
            nameColumn.FieldName = nameof(WidgetFields.Name);
            nameColumn.CellStyle.Wrap = DefaultBoolean.True;
            nameColumn.EditFormSettings.VisibleIndex = 0;
            this.tlWidgets.Columns.Add(nameColumn);

            TreeListComboBoxColumn typeColumn = new TreeListComboBoxColumn();
            typeColumn.Name = "Type";
            typeColumn.Caption = "Type";
            typeColumn.VisibleIndex = 1;
            typeColumn.FieldName = "Type";
            typeColumn.CellStyle.Wrap = DefaultBoolean.True;
            typeColumn.EditFormSettings.VisibleIndex = 1;
            this.tlWidgets.Columns.Add(typeColumn);

            TreeListCommandColumn editCommandColumn = new TreeListCommandColumn();
            editCommandColumn.VisibleIndex = 2;
            editCommandColumn.Width = Unit.Pixel(60);
            editCommandColumn.ShowNewButtonInHeader = true;
            editCommandColumn.EditButton.Visible = true;
            editCommandColumn.NewButton.Visible = false;
            editCommandColumn.DeleteButton.Visible = true;
            editCommandColumn.ButtonType = ButtonType.Image;
            editCommandColumn.EditButton.Image.Url = this.ResolveUrl("~/images/icons/pencil.png");
            editCommandColumn.DeleteButton.Image.Url = this.ResolveUrl("~/images/icons/delete.png");
            editCommandColumn.CancelButton.Image.Url = this.ResolveUrl("~/images/icons/cross_grey.png");
            editCommandColumn.NewButton.Image.Url = this.ResolveUrl("~/images/icons/add2.png");
            editCommandColumn.UpdateButton.Image.Url = this.ResolveUrl("~/images/icons/disk.png");
            this.tlWidgets.Columns.Add(editCommandColumn);

            TreeListHyperLinkColumn editPageColumn = new TreeListHyperLinkColumn();
            editPageColumn.Name = "EditCommand";
            editPageColumn.Caption = " ";
            editPageColumn.VisibleIndex = 3;
            editPageColumn.Width = Unit.Pixel(15);
            editPageColumn.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            editPageColumn.PropertiesHyperLink.ImageUrl = "~/images/icons/page_edit.png";
            editPageColumn.EditCellStyle.CssClass = "hidden";
            editPageColumn.FieldName = nameof(WidgetFields.WidgetId);
            editPageColumn.EditFormSettings.VisibleIndex = 2;
            editPageColumn.EditFormCaptionStyle.CssClass = "hidden";
            this.tlWidgets.Columns.Add(editPageColumn);

            this.tlWidgets.HtmlRowPrepared += this.HtmlRowPrepared;
            this.tlWidgets.HtmlDataCellPrepared += this.HtmlDataCellPrepared;
            this.tlWidgets.CustomCallback += this.CustomCallback;
            
            this.tlWidgets.NodeValidating += this.NodeValidating;
            this.tlWidgets.NodeInserting += this.NodeInserting;
            this.tlWidgets.NodeUpdating += this.NodeUpdating;

            this.InitDataSource();
        }

        private void HtmlRowPrepared(object sender, TreeListHtmlRowEventArgs e)
        {
            if (!e.NodeKey.IsNullOrWhiteSpace())
            {
                WidgetEntity widgetEntity = this.widgetDataSource.GetWidget(int.Parse(e.NodeKey));
                if (widgetEntity != null)
                {
                    if (!widgetEntity.Visible)
                    {
                        e.Row.ForeColor = Color.Gray;
                        e.Row.Font.Strikeout = true;
                    }
                }
            }
        }

        private void HtmlDataCellPrepared(object sender, TreeListHtmlDataCellEventArgs e)
        {
            if (e.Column.Name == "Type")
            {
                e.Cell.Text = e.CellValue.ToString().FormatPascalCase().TurnFirstToUpper();
            }
            else if (e.Column.Name == "EditCommand" && e.NodeKey != null)
            {
                WidgetEntity widgetEntity = this.widgetDataSource.GetWidget(int.Parse(e.NodeKey));
                if (!widgetEntity.IsNew)
                {
                    string entityName = widgetEntity.GetType().Name.Replace("Entity", "");

                    foreach (Control item in e.Cell.Controls)
                    {
                        if (item is HyperLinkDisplayControl link)
                        {
                            link.NavigateUrl = $"~/AppLess/Widget.aspx?id={e.NodeKey}&EntityName={entityName}";
                            break;
                        }
                    }
                }
            }
        }

        protected void CbType_Init(object sender, EventArgs e)
        {
            ASPxComboBox cbType = sender as ASPxComboBox;

            var configurableWidgets = new[]
            {
                ApplessWidgetType.Hero,
                ApplessWidgetType.PageTitle,
                ApplessWidgetType.Carousel,
                ApplessWidgetType.ActionBanner,
                ApplessWidgetType.ActionButtons,
                ApplessWidgetType.LanguageSwitcher,
                ApplessWidgetType.Markdown
            };
            
            if (this.ParentDataSourceAsPageEntity.OutletId.HasValue)
            {
                var outletWidgets = new[]
                {
                    ApplessWidgetType.WaitTime,
                    ApplessWidgetType.OpeningTime
                };

                configurableWidgets = configurableWidgets.Concat(outletWidgets).ToArray();
            }

            cbType.DataBindEnum<ApplessWidgetType>(configurableWidgets);
            cbType.Enabled = this.tlWidgets.IsNewNodeEditing;

            this.UpdateGuiToEditMode();
        }

        protected void CbExistingWidgetId_Init(object sender, EventArgs e)
        {
            ASPxComboBox cbExistingWidgetId = sender as ASPxComboBox;
            cbExistingWidgetId.TextField = nameof(WidgetFields.Name);
            cbExistingWidgetId.ValueField = nameof(WidgetFields.WidgetId);
            cbExistingWidgetId.ValueType = typeof(int);
            cbExistingWidgetId.DataSource = this.GetWidgets();
            cbExistingWidgetId.DataBind();
        }

        private void UpdateGuiToEditMode()
        {
            if (!this.tlWidgets.IsNewNodeEditing)
            {
                ASPxPageControl pageControl = this.tlWidgets.FindEditFormTemplateControl("tabs") as ASPxPageControl;

                pageControl.TabPages[0].Text = "Edit";
                pageControl.TabPages[1].ClientVisible = false;
            }
        }

        private void CustomCallback(object sender, TreeListCustomCallbackEventArgs e)
        {
            // Node dragged callback
            string[] nodes = e.Argument.Split(':');

            if (int.TryParse(nodes[0], out int draggedId) &&
                int.TryParse(nodes[1], out int draggedUponId))
            {
                this.landingPageWidgetDataSource.Manager.DraggedToSort(draggedId, draggedUponId);
                this.tlWidgets.DataBind();
            }
        }

        #endregion

        #region Event Handlers

        public void Load(int landingPageId)
        {
            this.DataSource = new LandingPageEntity(landingPageId);
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.CreateTreeList();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.btDeleteItemsBottom.PreSubmitWarning = DeleteMessage;
            this.btDeleteItemsBottom.Click += btDeleteSelected_Click;
        }

        private void NodeInserting(object sender, ASPxDataInsertingEventArgs e)
        {
            ASPxPageControl pageControl = this.tlWidgets.FindEditFormTemplateControl("tabs") as ASPxPageControl;

            if (pageControl.ActiveTabIndex == 0) // New tab
            {
                e.NewValues["Name"] = ExtractNameValue();
                e.NewValues["Type"] = ExtractTypeValue();
            }
            else // Existing tab
            {
                e.NewValues["ForeignKey"] = ExtractExistingWidgetId();
            }
        }

        private void NodeUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {
            e.NewValues["Name"] = ExtractNameValue();
        }

        private void CustomErrorText(object sender, TreeListCustomErrorTextEventArgs e)
        {
            if (e.Exception.InnerException != null)
            {
                ObymobiException exception = e.Exception.InnerException as ObymobiException;
                if (exception != null)
                {
                    e.ErrorText = exception.AdditionalMessage;
                }
                else
                {
                    e.ErrorText = e.Exception.InnerException.Message;
                }
            }
        }

        private void NodeValidating(object sender, TreeListNodeValidationEventArgs e)
        {
            if (e.HasErrors)
            {
                e.NodeError = "Please correct the errors.";
            }
        }

        private void btDeleteSelected_Click(object sender, EventArgs e)
        {
            this.DeleteSelected();
        }

        private void DeleteSelected()
        {
            List<TreeListNode> selectedNodes = this.tlWidgets.GetSelectedNodes();

            if (selectedNodes.Count <= 0)
            {
                return;
            }

            foreach (TreeListNode node in selectedNodes)
            {
                node.Selected = false;
                this.landingPageWidgetDataSource.DeleteParameters.Clear();
                this.landingPageWidgetDataSource.DeleteParameters.Add("WidgetId", node.Key);
                this.landingPageWidgetDataSource.Delete();
            }
            this.DataBind();
        }

        private string TreelistId => $"{this.ID}_treelist";

        private Data.CollectionClasses.WidgetCollection GetWidgets()
        {
            RelationCollection relations = new RelationCollection();
            relations.Add(WidgetEntity.Relations.LandingPageWidgetEntityUsingWidgetId, JoinHint.Left);
            
            PredicateExpression filter = new PredicateExpression();
            filter.Add(WidgetFields.ApplicationConfigurationId == this.ParentDataSourceAsPageEntity.ApplicationConfigurationId);

            // TODO: Would be better to figure out a way to make use of the ConfigurableWidgetTypes property.
            filter.Add(WidgetActionButtonFields.WidgetId == DBNull.Value); 

            PredicateExpression landingPageFilter = new PredicateExpression();
            landingPageFilter.Add(LandingPageWidgetFields.LandingPageId != this.ParentDataSourceAsPageEntity.LandingPageId);
            landingPageFilter.AddWithOr(LandingPageWidgetFields.LandingPageId == DBNull.Value);

            filter.Add(landingPageFilter);

            SortExpression sort = new SortExpression(new SortClause(WidgetFields.Name, SortOperator.Ascending));
            
            var widgetCollection = EntityCollection.GetMulti<Data.CollectionClasses.WidgetCollection>(filter, sort, relations, null, new IncludeFieldsList(WidgetFields.Name, WidgetFields.ParentCompanyId));

            return widgetCollection;
        }

        private string ExtractNameValue()
        {
            ASPxPageControl pageControl = this.tlWidgets.FindEditFormTemplateControl("tabs") as ASPxPageControl;
            ASPxTextBox tbName = pageControl.TabPages[0].FindControl("tbName") as ASPxTextBox;

            return tbName.Text;
        }

        private int ExtractTypeValue()
        {
            ASPxPageControl pageControl = this.tlWidgets.FindEditFormTemplateControl("tabs") as ASPxPageControl;
            ASPxComboBox cbType = pageControl.TabPages[0].FindControl("cbType") as ASPxComboBox;

            return int.Parse(cbType.Value.ToString());
        }

        private int ExtractExistingWidgetId()
        {
            ASPxPageControl pageControl = this.tlWidgets.FindEditFormTemplateControl("tabs") as ASPxPageControl;
            ASPxComboBox cbExistingWidgetId = pageControl.TabPages[1].FindControl("cbExistingWidgetId") as ASPxComboBox;

            return int.Parse(cbExistingWidgetId.Value.ToString());
        }

        #endregion
    }
}