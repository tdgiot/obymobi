﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdyenForPlatformsPaymentIntegrationConfiguration.ascx.cs" Inherits="Obymobi.Cms.WebApplication.AppLess.SubPanels.PaymentIntegrationConfigurations.AdyenForPlatformsPaymentIntegrationConfiguration" %>

<D:Panel ID="AfPPanel" GroupingText="Adyen for platforms" runat="server">
    <D:PlaceHolder ID="plhAccounts" runat="server" Visible="false">
		<table class="dataformV2">
			<tr>
				<td class="label">
					<D:LabelEntityFieldInfo runat="server" ID="lblAdyenAccountHolderCode">Account holder</D:LabelEntityFieldInfo>
				</td>
				<td class="control">
					<X:ComboBoxString ID="cbAdyenAccountHolderCode" runat="server" AutoPostBack="True" notdirty="true" IsRequired="true"></X:ComboBoxString>                    
				</td>
				<td class="label">
					<D:LabelEntityFieldInfo runat="server" ID="lblAdyenAccountCode">Account</D:LabelEntityFieldInfo>
				</td>
				<td class="control">
					<X:ComboBoxString ID="cbAdyenAccountCode" runat="server" IsRequired="true"></X:ComboBoxString>
				</td>
			</tr>
		</table>
	</D:PlaceHolder>
	<D:PlaceHolder ID="plhCognitoLogin" runat="server" Visible="false">
		<table class="dataformV2">
			<tr>
				<td class="label" />
				<td class="control">
					<div style="float: left;">
						<asp:HyperLink runat="server" ID="hlLoginCbo" NavigateUrl="~/CognitoOAuth.aspx" Target="_blank" ForeColor="Black">Login</asp:HyperLink> | 
						<D:LinkButton ID="btRefreshAccountHolders" runat="server" Text="Refresh" LocalizeText="false" ForeColor="Black" />
					</div>
				</td>
				<td class="label" />
				<td class="control" />
			</tr>
		</table>
    </D:PlaceHolder>
</D:Panel>

<D:Panel runat="server" ID="pnlAdyenPaymentMethods" GroupingText="Payment Methods" LocalizeText="false" />