﻿using Dionysos;
using Dionysos.Interfaces;
using Dionysos.Security.Cryptography;
using Dionysos.Web.UI;
using Dionysos.Web.UI.WebControls;
using Obymobi.Cms.WebApplication.Old_App_Code.Payments;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Integrations.Payment;
using Obymobi.Integrations.Payment.Models;
using System;
using System.Linq;

namespace Obymobi.Cms.WebApplication.AppLess.SubPanels.PaymentIntegrationConfigurations
{
    public partial class AdyenPaymentIntegrationConfiguration : SubPanelCustomDataSource, ISaveableControl
    {
        private AdyenPaymentMethods adyenPaymentMethods;

        public PaymentIntegrationConfigurationEntity PaymentIntegrationConfigurationEntity
        {
            get => DataSource as PaymentIntegrationConfigurationEntity;
            set => DataSource = value;
        }

        protected override void OnInit(EventArgs e)
        {
            LoadUserControls();
            base.OnInit(e);            
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            tbAdyenMerchantCode.Value = PaymentIntegrationConfigurationEntity.AdyenMerchantCode;
            tbGooglePayMerchantIdentifier.Value = PaymentIntegrationConfigurationEntity.GooglePayMerchantIdentifier;
            tbApplePayMerchantIdentifier.Value = PaymentIntegrationConfigurationEntity.ApplePayMerchantIdentifier;
        }

        public bool Save()
        {
            PaymentIntegrationConfigurationEntity.AdyenMerchantCode = tbAdyenMerchantCode.Value;
            PaymentIntegrationConfigurationEntity.GooglePayMerchantIdentifier = tbGooglePayMerchantIdentifier.Value;
            PaymentIntegrationConfigurationEntity.ApplePayMerchantIdentifier = tbApplePayMerchantIdentifier.Value;

            if (!ValidateAdyenPaymentMethods(PaymentIntegrationConfigurationEntity))
            {
                return false;
            }

            return adyenPaymentMethods.Save();
        }

        private void LoadUserControls()
        {
            adyenPaymentMethods = (AdyenPaymentMethods)LoadControl("~/AppLess/SubPanels/PaymentIntegrationConfigurations/AdyenPaymentMethods.ascx");
            pnlAdyenPaymentMethods.Controls.Add(adyenPaymentMethods);
        }

        private bool ValidateAdyenPaymentMethods(PaymentIntegrationConfigurationEntity paymentIntegrationConfigurationEntity)
        {
            if (paymentIntegrationConfigurationEntity.PaymentProviderEntity.ApiKey.IsNullOrWhiteSpace())
            {
                PageAsPageDefault.AddInformator(InformatorType.Warning, $"No API key configured for payment provider ({paymentIntegrationConfigurationEntity.PaymentProviderEntity.Name}).");
                return false;
            }

            IPaymentProviderConfiguration configuration = new PaymentProviderConfiguration
            {
                PaymentProviderType = paymentIntegrationConfigurationEntity.PaymentProviderEntity.Type,
                ApiKey = Cryptographer.DecryptUsingCBC(paymentIntegrationConfigurationEntity.PaymentProviderEntity.ApiKey),
                IsProduction = paymentIntegrationConfigurationEntity.PaymentProviderEntity.Environment == PaymentProviderEnvironment.Live,
                LiveEndpointUrlPrefix = paymentIntegrationConfigurationEntity.PaymentProviderEntity.LiveEndpointUrlPrefix,
                AdyenMerchantCode = paymentIntegrationConfigurationEntity.AdyenMerchantCode
            };

            IClient client = new PaymentFactory().CreateClient(configuration);
            GetPaymentMethodsResponse response = client.GetPaymentMethods();

            bool hasPaymentMethods = response?.PaymentMethods != null && response.PaymentMethods.Any();
            if (!hasPaymentMethods)
            {
                PageAsPageDefault.AddInformator(InformatorType.Warning, "Failed to retrieve payment methods from Adyen.");
                return false;
            }

            return true;
        }
    }
}