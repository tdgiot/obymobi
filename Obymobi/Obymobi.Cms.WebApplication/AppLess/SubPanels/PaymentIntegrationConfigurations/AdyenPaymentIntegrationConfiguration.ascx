﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdyenPaymentIntegrationConfiguration.ascx.cs" Inherits="Obymobi.Cms.WebApplication.AppLess.SubPanels.PaymentIntegrationConfigurations.AdyenPaymentIntegrationConfiguration" %>

<D:Panel runat="server" ID="pnlAdyen" GroupingText="Adyen" >
    <table class="dataformV2">
        <tr>
            <td class="label">
                <D:LabelEntityFieldInfo runat="server" id="lbAdyenMerchantCode">Adyen merchant code</D:LabelEntityFieldInfo>
            </td>
            <td class="control">
                <D:TextBoxString ID="tbAdyenMerchantCode" runat="server" IsRequired="True"></D:TextBoxString>
            </td>
            <td class="label"></td>						
            <td class="control"></td>
        </tr>
        <tr>
            <td class="label">
                <D:LabelEntityFieldInfo runat="server" id="lbGooglePayMerchantIdentifier">Google Pay merchant identifier</D:LabelEntityFieldInfo>
            </td>
            <td class="control">
                <D:TextBoxString ID="tbGooglePayMerchantIdentifier" runat="server"></D:TextBoxString>
            </td>
            <td class="label"></td>						
            <td class="control"></td>
        </tr>
        <tr>
            <td class="label">
                <D:LabelEntityFieldInfo runat="server" id="lbApplePayMerchantIdentifier">Apple Pay merchant code</D:LabelEntityFieldInfo>
            </td>
            <td class="control">
                <D:TextBoxString ID="tbApplePayMerchantIdentifier" runat="server"></D:TextBoxString>
            </td>
            <td class="label"></td>						
            <td class="control"></td>
        </tr>
    </table>
</D:Panel>

<D:Panel runat="server" ID="pnlAdyenPaymentMethods" GroupingText="Payment Methods" LocalizeText="false" />