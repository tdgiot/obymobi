﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Obymobi.Cms.WebApplication.AppLess.SubPanels.PaymentIntegrationConfigurations
{


    public partial class AdyenPaymentIntegrationConfiguration
    {

        /// <summary>
        /// pnlAdyen control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Dionysos.Web.UI.WebControls.Panel pnlAdyen;

        /// <summary>
        /// lbAdyenMerchantCode control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Dionysos.Web.UI.WebControls.LabelEntityFieldInfo lbAdyenMerchantCode;

        /// <summary>
        /// tbAdyenMerchantCode control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Dionysos.Web.UI.WebControls.TextBoxString tbAdyenMerchantCode;

        /// <summary>
        /// lbGooglePayMerchantIdentifier control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Dionysos.Web.UI.WebControls.LabelEntityFieldInfo lbGooglePayMerchantIdentifier;

        /// <summary>
        /// tbGooglePayMerchantIdentifier control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Dionysos.Web.UI.WebControls.TextBoxString tbGooglePayMerchantIdentifier;

        /// <summary>
        /// lbApplePayMerchantIdentifier control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Dionysos.Web.UI.WebControls.LabelEntityFieldInfo lbApplePayMerchantIdentifier;

        /// <summary>
        /// tbApplePayMerchantIdentifier control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Dionysos.Web.UI.WebControls.TextBoxString tbApplePayMerchantIdentifier;

        /// <summary>
        /// pnlAdyenPaymentMethods control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Dionysos.Web.UI.WebControls.Panel pnlAdyenPaymentMethods;
    }
}
