﻿using Dionysos.Interfaces;
using Dionysos.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using System;

namespace Obymobi.Cms.WebApplication.AppLess.SubPanels.PaymentIntegrationConfigurations
{
    public partial class AdyenHppPaymentIntegrationConfiguration : SubPanelCustomDataSource, ISaveableControl
    {
        public PaymentIntegrationConfigurationEntity PaymentIntegrationConfigurationEntity 
        { 
            get => DataSource as PaymentIntegrationConfigurationEntity;
            set => DataSource = value; 
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            tbAdyenMerchantCode.Value = PaymentIntegrationConfigurationEntity.AdyenMerchantCode;
        }

        public bool Save()
        {
            PaymentIntegrationConfigurationEntity.AdyenMerchantCode = tbAdyenMerchantCode.Value;
            return true;
        }        
    }
}