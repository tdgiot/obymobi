﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdyenHppPaymentIntegrationConfiguration.ascx.cs" Inherits="Obymobi.Cms.WebApplication.AppLess.SubPanels.PaymentIntegrationConfigurations.AdyenHppPaymentIntegrationConfiguration" %>

<D:Panel runat="server" ID="pnlAdyenHpp" GroupingText="Adyen HPP" >
    <table class="dataformV2">
        <tr>
            <td class="label">
                <D:LabelEntityFieldInfo runat="server" id="lbAdyenMerchantCode">Adyen merchant code</D:LabelEntityFieldInfo>
            </td>
            <td class="control">
                <D:TextBoxString ID="tbAdyenMerchantCode" runat="server" IsRequired="true"></D:TextBoxString>
            </td>
            <td class="label"></td>						
            <td class="control"></td>
        </tr>
    </table>
</D:Panel>