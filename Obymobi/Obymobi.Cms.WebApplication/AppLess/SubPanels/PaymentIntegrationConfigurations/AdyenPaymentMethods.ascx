﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdyenPaymentMethods.ascx.cs" Inherits="Obymobi.Cms.WebApplication.AppLess.SubPanels.PaymentIntegrationConfigurations.AdyenPaymentMethods" %>
<%@ Import Namespace="Obymobi.Enums" %>

<style type="text/css">    
    table tbody tr td {
        text-align: center;
        min-width: 100px;
    }

    table tbody tr td:first-child {
        min-width: 20px;
    }

    table tbody tr td.text-left {
        text-align: left;
        margin-left: 8px;
    }
</style>

<table class="dxgvControl_Glass" cellspacing="0" cellpadding="0" border="0" style="width: 100%; border-collapse: collapse; border-collapse: separate; margin-top: 10px;">
    <tbody>
        <tr>
            <td>
                <table class="dxgvTable_Glass" cellspacing="0" cellpadding="0" border="0" style="width: 100%; border-collapse: collapse; empty-cells: show; overflow: hidden;">
                    <tbody>
                        <tr>
                            <td class="dxgvHeader_Glass" style="border-top-width: 0px; border-left-width: 0px;">
                                <table border="0" style="width: 100%; border-collapse: collapse;">
                                    <tbody>
                                        <tr>
                                            <td style="width: 20px !important;">
                                                <strong>Active</strong>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>

                            <td class="dxgvHeader_Glass" style="border-top-width: 0px; border-left-width: 0px;">
                                <table border="0" style="width: 100%; border-collapse: collapse;">
                                    <tbody>
                                        <tr>
                                            <td class="text-left">
                                                <strong>Payment method</strong>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>

                            <% foreach (AdyenPaymentMethodBrand adyenPaymentMethodBrand in this.UniquePaymentMethodBrands) { %>
                                <td class="dxgvHeader_Glass" style="border-top-width: 0px; border-left-width: 0px;">
                                    <table border="0" style="width: 100%; border-collapse: collapse;">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <strong><%= adyenPaymentMethodBrand.GetStringValue() %></strong>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            <% } %>
                        </tr>

                        <% foreach (AdyenPaymentMethod adyenPaymentMethod in this.PaymentMethodConfigurations.Keys) { %>
                            <% bool paymentMethodIsChecked = this.GetAdyenPaymentMethodStatus(adyenPaymentMethod); %>
                            <tr class="dxgvFilterRow_Glass">
                                <td class="dxgv" style="width: 20px !important;">
                                    <input type="checkbox" name="<%= adyenPaymentMethod %>" <%= paymentMethodIsChecked ? "checked=\"checked\"" : string.Empty %> />
                                </td>

                                <td class="dxgv text-left" style="padding-left: 5px;">
                                    <%= adyenPaymentMethod.GetStringValue() %>
                                </td>

                                <% if (!this.PaymentMethodConfigurations[adyenPaymentMethod].Any()) { %>
                                    <td colspan="<%= this.UniquePaymentMethodBrands.Count() %>" class="dxgv">
                                        <p>No payment method brands configurable for <%= adyenPaymentMethod.GetStringValue() %></p>
                                    </td>
                                <% } else { %>
                                    <% foreach (AdyenPaymentMethodBrand adyenPaymentMethodBrand in this.UniquePaymentMethodBrands) { %>
                                        <% bool paymentMethodBrandIsChecked = this.GetAdyenPaymentMethodBrandStatus(adyenPaymentMethod, adyenPaymentMethodBrand); %>

                                        <td class="dxgv">
                                            <% if (this.PaymentMethodConfigurations[adyenPaymentMethod].Contains(adyenPaymentMethodBrand)) { %>
                                                <input type="checkbox" name="<%= adyenPaymentMethod %>_<%= adyenPaymentMethodBrand %>" <%= paymentMethodBrandIsChecked ? "checked=\"Checked\"" : string.Empty %> />
                                            <% } %>
                                        </td>
                                    <% } %>
                                <% } %>
                            </tr>
                        <% } %>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>
