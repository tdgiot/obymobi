﻿using Crave.Adyen.Api.Client;
using DevExpress.Web;
using Dionysos;
using Dionysos.Interfaces;
using Dionysos.Web.UI;
using Dionysos.Web.UI.WebControls;
using Obymobi.Cms.Logic.HelperClasses;
using Obymobi.Data.EntityClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.UI;

namespace Obymobi.Cms.WebApplication.AppLess.SubPanels.PaymentIntegrationConfigurations
{
    public partial class AdyenForPlatformsPaymentIntegrationConfiguration : SubPanelCustomDataSource, ISaveableControl
    {
        private AdyenPaymentMethods adyenPaymentMethods;

        public PaymentIntegrationConfigurationEntity PaymentIntegrationConfigurationEntity
        {
            get => DataSource as PaymentIntegrationConfigurationEntity;
            set => DataSource = value;
        }

        private void LoadUserControls()
        {
            adyenPaymentMethods = (AdyenPaymentMethods)LoadControl("~/AppLess/SubPanels/PaymentIntegrationConfigurations/AdyenPaymentMethods.ascx");
            pnlAdyenPaymentMethods.Controls.Add(adyenPaymentMethods);
        }

        public async Task LoadAccountHolders()
        {
            ICollection<AccountHolder> allAccountHolders = await AdyenClientFactory
                                                                              .CreateAccountHoldersClient(PaymentIntegrationConfigurationEntity.PaymentProviderEntity)
                                                                              .GetAllAsync();

            IOrderedEnumerable<AccountHolder> accountHolders = allAccountHolders
                .Where(accountHolder => accountHolder.Status != "Closed")
                .OrderBy(accountHolder => accountHolder.LegalBusinessName);

            cbAdyenAccountHolderCode.Items.Clear();

            cbAdyenAccountHolderCode.HelpText = !accountHolders.Any() ? "No account holders found" : "";

            foreach (AccountHolder accountHolder in accountHolders)
            {
                ListEditItem item = cbAdyenAccountHolderCode.Items.Add(accountHolder.LegalBusinessName, accountHolder.AccountHolderCode);
                item.Selected = accountHolder.AccountHolderCode.ToString().Equals(PaymentIntegrationConfigurationEntity.AdyenAccountHolderCode, StringComparison.InvariantCultureIgnoreCase);
            }
        }

        public async Task LoadAccounts()
        {
            cbAdyenAccountCode.Value = string.Empty;
            cbAdyenAccountCode.Items.Clear();

            string accountHolderCode = cbAdyenAccountHolderCode.Value;
            if (accountHolderCode.IsNullOrWhiteSpace())
            {
                return;
            }

            GetAccountHolderResponse accountHolder = await AdyenClientFactory
                                                           .CreateAccountHoldersClient(PaymentIntegrationConfigurationEntity.PaymentProviderEntity)
                                                           .GetAsync(accountHolderCode);

            IOrderedEnumerable<Account> accounts = accountHolder.Accounts
                .Where(account => account.Status != "Closed")
                .OrderBy(account => account.Description);

            cbAdyenAccountCode.HelpText = !accounts.Any() ? "Unable to find accounts for selected account holder" : "";

            foreach (Account account in accounts)
            {
                ListEditItem item = cbAdyenAccountCode.Items.Add(account.Description, account.AccountCode);
                item.Selected = account.AccountCode.Equals(PaymentIntegrationConfigurationEntity.AdyenAccountCode, StringComparison.InvariantCultureIgnoreCase);
            }
        }

        private void ShowConfiguredAdyenAccount()
        {
            plhAccounts.Visible = true;

            cbAdyenAccountHolderCode.Enabled = false;
            cbAdyenAccountCode.Enabled = false;

            cbAdyenAccountHolderCode.Items.Clear();
            cbAdyenAccountHolderCode.Items.Add(new ListEditItem
            {
                Text = PaymentIntegrationConfigurationEntity.AdyenAccountHolderName,
                Value = PaymentIntegrationConfigurationEntity.AdyenAccountHolderCode,
                Selected = true
            });

            cbAdyenAccountCode.Items.Clear();
            cbAdyenAccountCode.Items.Add(new ListEditItem
            {
                Text = PaymentIntegrationConfigurationEntity.AdyenAccountName,
                Value = PaymentIntegrationConfigurationEntity.AdyenAccountCode,
                Selected = true
            });
        }

        protected override void OnInit(EventArgs e)
        {
            LoadUserControls();
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (PaymentIntegrationConfigurationEntity.IsNew)
            {
                plhAccounts.Visible = CognitoHelper.IsLoggedIn;
                plhCognitoLogin.Visible = !CognitoHelper.IsLoggedIn;

                if (CognitoHelper.IsLoggedIn)
                {
                    Page.RegisterAsyncTask(new PageAsyncTask(LoadAccountHolders));
                    Page.RegisterAsyncTask(new PageAsyncTask(LoadAccounts));
                }
            }
            else
            {
                ShowConfiguredAdyenAccount();
            }
        }

        public bool Save()
        {
            if (PaymentIntegrationConfigurationEntity.IsNew)
            {
                if (!ValidateAdyenAccount())
                {
                    return false;
                }

                PaymentIntegrationConfigurationEntity.AdyenAccountHolderCode = cbAdyenAccountHolderCode.SelectedValueString;
                PaymentIntegrationConfigurationEntity.AdyenAccountHolderName = cbAdyenAccountHolderCode.SelectedItem.Text;

                PaymentIntegrationConfigurationEntity.AdyenAccountCode = cbAdyenAccountCode.SelectedValueString;
                PaymentIntegrationConfigurationEntity.AdyenAccountName = cbAdyenAccountCode?.SelectedItem?.Text;
            }

            return adyenPaymentMethods.Save();
        }

        private bool ValidateAdyenAccount()
        {
            if (cbAdyenAccountCode?.SelectedItem == null)
            {
                if (!CognitoHelper.IsLoggedIn)
                {
                    PageAsPageDefault.AddInformator(InformatorType.Warning, $"Please sign in and configure an Adyen Account");
                }

                return false;
            }

            return true;
        }
    }
}