﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dionysos.Interfaces;
using Dionysos.Web.UI.WebControls;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Extensions;

namespace Obymobi.Cms.WebApplication.AppLess.SubPanels.PaymentIntegrationConfigurations
{
    public partial class AdyenPaymentMethods : SubPanel, ISaveableControl
    {
        public IEnumerable<AdyenPaymentMethodBrand> UniquePaymentMethodBrands = new List<AdyenPaymentMethodBrand>();
        public Dictionary<AdyenPaymentMethod, IEnumerable<AdyenPaymentMethodBrand>> PaymentMethodConfigurations = new Dictionary<AdyenPaymentMethod, IEnumerable<AdyenPaymentMethodBrand>>();

        public PaymentIntegrationConfigurationEntity PaymentIntegrationConfigurationEntity => PageAsPageLLBLGenEntity.DataSource as PaymentIntegrationConfigurationEntity;        

        private void LoadUserControls()
        {            
            IEnumerable<AdyenPaymentMethod> adyenPaymentMethods = EnumUtil.GetValues<AdyenPaymentMethod>();
            Dictionary<AdyenPaymentMethod, IEnumerable<AdyenPaymentMethodBrand>> adyenPaymentMethodDictionary = new Dictionary<AdyenPaymentMethod, IEnumerable<AdyenPaymentMethodBrand>>();

            foreach (AdyenPaymentMethod adyenPaymentMethod in adyenPaymentMethods)
            {
                adyenPaymentMethodDictionary.Add(adyenPaymentMethod, adyenPaymentMethod.GetAdyenPaymentMethodBrands());
            }

            UniquePaymentMethodBrands = adyenPaymentMethodDictionary.Values.SelectMany(apm => apm).Distinct();
            PaymentMethodConfigurations = adyenPaymentMethodDictionary;
        }

        private void ProcessAdyenPaymentMethods(PaymentIntegrationConfigurationEntity paymentIntegrationConfigurationEntity)
        {
            AdyenPaymentMethodCollection adyenPaymentMethodCollection = paymentIntegrationConfigurationEntity.AdyenPaymentMethodCollection;

            foreach (AdyenPaymentMethod adyenPaymentMethod in EnumUtil.GetValues<AdyenPaymentMethod>())
            {
                AdyenPaymentMethodEntity adyenPaymentMethodEntity = adyenPaymentMethodCollection.FirstOrDefault(apm => apm.Type == adyenPaymentMethod);
                if (adyenPaymentMethodEntity == null)
                {
                    adyenPaymentMethodEntity = new AdyenPaymentMethodEntity
                    {
                        Type = adyenPaymentMethod,
                        ParentCompanyId = paymentIntegrationConfigurationEntity.CompanyId
                    };

                    adyenPaymentMethodEntity.PaymentIntegrationConfigurationEntity = paymentIntegrationConfigurationEntity;
                }

                adyenPaymentMethodEntity.Active = Request.Form.Get(adyenPaymentMethod.ToString()) == "on";

                ProcessAdyenPaymentMethodBrands(adyenPaymentMethodEntity);
            }
        }

        private void ProcessAdyenPaymentMethodBrands(AdyenPaymentMethodEntity adyenPaymentMethodEntity)
        {
            AdyenPaymentMethodBrandCollection adyenPaymentMethodBrandCollection = adyenPaymentMethodEntity.AdyenPaymentMethodBrandCollection;

            foreach (AdyenPaymentMethodBrand adyenPaymentMethodBrand in PaymentMethodConfigurations[adyenPaymentMethodEntity.Type])
            {
                AdyenPaymentMethodBrandEntity adyenPaymentMethodBrandEntity = adyenPaymentMethodBrandCollection.FirstOrDefault(apmb => apmb.Brand == adyenPaymentMethodBrand);
                if (adyenPaymentMethodBrandEntity == null)
                {
                    adyenPaymentMethodBrandEntity = new AdyenPaymentMethodBrandEntity
                    {
                        Brand = adyenPaymentMethodBrand,
                        ParentCompanyId = adyenPaymentMethodEntity.ParentCompanyId
                    };

                    adyenPaymentMethodBrandEntity.AdyenPaymentMethodEntity = adyenPaymentMethodEntity;
                }

                adyenPaymentMethodBrandEntity.Active = Request.Form.Get($"{adyenPaymentMethodEntity.Type}_{adyenPaymentMethodBrand}") == "on";                
            }
        }

        public bool GetAdyenPaymentMethodStatus(AdyenPaymentMethod adyenPaymentMethod) => PaymentIntegrationConfigurationEntity != null && PaymentIntegrationConfigurationEntity.AdyenPaymentMethodCollection.Any(apm => apm.Type == adyenPaymentMethod && apm.Active);

        public bool GetAdyenPaymentMethodBrandStatus(AdyenPaymentMethod adyenPaymentMethod, AdyenPaymentMethodBrand adyenPaymentMethodBrand) => PaymentIntegrationConfigurationEntity != null && PaymentIntegrationConfigurationEntity.AdyenPaymentMethodCollection
                .Where(apm => apm.Type == adyenPaymentMethod)
                .SelectMany(apm => apm.AdyenPaymentMethodBrandCollection)
                .Any(apmb => apmb.Brand == adyenPaymentMethodBrand && apmb.Active);

        public bool Save()
        {
            ProcessAdyenPaymentMethods(PaymentIntegrationConfigurationEntity);
            return true;
        }

        protected override void OnInit(EventArgs args)
        {
            LoadUserControls();            
            base.OnInit(args);
        }
    }
}
