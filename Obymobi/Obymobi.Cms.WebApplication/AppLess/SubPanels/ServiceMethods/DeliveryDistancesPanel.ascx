﻿<%@ Control Language="C#" AutoEventWireup="true" Codebehind="DeliveryDistancesPanel.ascx.cs" Inherits="Obymobi.Cms.WebApplication.AppLess.SubPanels.ServiceMethods.DeliveryDistancesPanel" %>
<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v20.1" Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dxwtl" %>
<%@ Register TagPrefix="dxwtl" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<script type="text/javascript">
    function OnInit(s, e) {
        ASPxClientUtils.AttachEventToElement(s.GetMainElement(), "keypress",
            function (evt) {
                switch (evt.keyCode) {
                    //ENTER
                    case 13:
                        if (s.IsEditing()) {
                            s.UpdateEdit();
                            evt.stopPropagation();
                        }
                        break;

                    //ESC               
                    case 27:
                        if (s.IsEditing()) {
                            s.CancelEdit();
                            evt.stopPropagation();
                        }
                        break;
                }
            });
    }
</script>

<div style="width: 788px;">
    <dxwtl:ASPxTreeList ID="tlDeliveryDistances" ClientInstanceName="tlDeliveryDistances" runat="server" EnableViewState="false" AutoGenerateColumns="False">
        <SettingsSelection Enabled="true" Recursive="false" />
        <ClientSideEvents Init="OnInit" />

        <Templates>
            <EditForm>
                <dxwtl:ASPxPageControl ID="tabs" runat="server" ActiveTabIndex="0" Width="100%">
                    <TabPages>
                        <dxwtl:TabPage Text="New">
                            <ContentCollection>
                                 <dxwtl:ContentControl runat="server">
                                    <table class="dataformV2">
                                        <tr>
                                            <td class="label">Delivery distance</td>
                                            <td class="control" colspan="3">
                                                <dxe:ASPxTextBox ID="tbMaximumInMetres" style="width: 100%" runat="server" Value='<%# Bind("MaximumInMetres") %>' />
                                            </td>

	                                        <td class="label">Price</td>
	                                        <td class="control">
		                                        <dxe:ASPxTextBox ID="tbPrice" style="width: 100%" runat="server" DisplayFormatString="{0:c2}" Value='<%# Bind("Price", "{0:n2}") %>' />
	                                        </td>
                                        </tr>                                    
                                    </table>
                                </dxwtl:ContentControl>
                            </ContentCollection>
                        </dxwtl:TabPage>
                    </TabPages>
                </dxwtl:ASPxPageControl>

                <div style="text-align: right; padding-top: 8px">
                    <dxwtl:ASPxTreeListTemplateReplacement runat="server" ReplacementType="UpdateButton"  />
                    <dxwtl:ASPxTreeListTemplateReplacement runat="server" ReplacementType="CancelButton" />
                </div>
            </EditForm>
        </Templates>
    </dxwtl:ASPxTreeList>

    <div id="divDeletePages" style="margin-top: 4px;">
        <D:Button runat="server" ID="btDeleteItemsBottom" style="float:right;" Text="Delete selection" />        
    </div> 
</div>
