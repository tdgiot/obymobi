﻿using System;
using System.Web.UI;

namespace Obymobi.Cms.WebApplication.AppLess.SubPanels.ServiceMethods
{
    public partial class Delivery : UserControl
    {
        #region Fields

        private DeliveryDistancesPanel deliveryDistancesPanel;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            this.deliveryDistancesPanel = (DeliveryDistancesPanel)this.LoadControl("~/AppLess/SubPanels/ServiceMethods/DeliveryDistancesPanel.ascx");

            int serviceMethodId = int.Parse(((Control)sender).Page.Request.QueryString["id"]);
            this.deliveryDistancesPanel.Load(serviceMethodId);

            this.pnlDelivery.Controls.Add(this.deliveryDistancesPanel);
            this.pnlDelivery.Visible = true;
        }

        #endregion
    }
}