﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using DevExpress.Utils;
using DevExpress.Web;
using DevExpress.Web;
using DevExpress.Web.ASPxTreeList;
using DevExpress.Web.Data;
using Dionysos.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;

namespace Obymobi.Cms.WebApplication.AppLess.SubPanels.ServiceMethods
{
    public partial class DeliveryDistancesPanel : SubPanelCustomDataSource
    {
        #region  Fields

        private DeliveryDistanceDataSource _deliveryDistanceDataSource;
        
        private const string DeleteMessage = "Are you sure you want to delete this item?";

        #endregion

        #region Properties

        private ServiceMethodEntity DataSourceAsServiceMethodEntity => this.DataSource as ServiceMethodEntity;

        private string NodeDblClickMethod
        {
            get { return @" function(s, e) {
	                            " + this.TreelistId + @".StartEdit(e.nodeKey);
		                        e.cancel = true;	                                                                    
                            }"; }
        }

        #endregion

        #region Event Handlers

        public new void Load(int serviceMethodId) => this.DataSource = new ServiceMethodEntity(serviceMethodId);

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            this.CreateTreeList();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.btDeleteItemsBottom.PreSubmitWarning = DeleteMessage;
            this.btDeleteItemsBottom.Click += btDeleteSelected_Click;
        }

        private void NodeInserting(object sender, ASPxDataInsertingEventArgs e)
        {
            ASPxPageControl pageControl = this.tlDeliveryDistances.FindEditFormTemplateControl("tabs") as ASPxPageControl;

            e.NewValues["MaximumInMetres"] = ExtractMaximumInMetresValue();
            e.NewValues["Price"] = ExtractPriceValue();
        }

        private void NodeUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {
            e.NewValues["MaximumInMetres"] = ExtractMaximumInMetresValue();
            e.NewValues["Price"] = ExtractPriceValue();
        }

        private void CustomErrorText(object sender, TreeListCustomErrorTextEventArgs e)
        {
            if (e.Exception.InnerException != null)
            {
                ObymobiException exception = e.Exception.InnerException as ObymobiException;
                if (exception != null)
                {
                    e.ErrorText = exception.AdditionalMessage;
                }
                else
                {
                    e.ErrorText = e.Exception.InnerException.Message;
                }
            }
        }

        private void NodeValidating(object sender, TreeListNodeValidationEventArgs e)
        {
            if (e.HasErrors)
            {
                e.NodeError = "Please correct the errors.";
            }
        }

        private void btDeleteSelected_Click(object sender, EventArgs e) => this.DeleteSelected();

        private void DeleteSelected()
        {
            List<TreeListNode> selectedNodes = this.tlDeliveryDistances.GetSelectedNodes();

            if (selectedNodes.Count <= 0)
            {
                return;
            }

            foreach (TreeListNode node in selectedNodes)
            {
                node.Selected = false;

                this._deliveryDistanceDataSource.DeleteParameters.Clear();
                this._deliveryDistanceDataSource.DeleteParameters.Add("DeliveryDistanceId", node.Key);
                this._deliveryDistanceDataSource.Delete();
            }

            this.DataBind();
        }

        private string TreelistId => $"{this.ID}_treelist";

        private int ExtractMaximumInMetresValue()
        {
            ASPxPageControl pageControl = this.tlDeliveryDistances.FindEditFormTemplateControl("tabs") as ASPxPageControl;
            ASPxTextBox tbMaximumInMetres = pageControl.TabPages[0].FindControl("tbMaximumInMetres") as ASPxTextBox;

            bool isParsed = int.TryParse(tbMaximumInMetres.Text, out int result);

            return isParsed ? result : 0;
        }

        private decimal ExtractPriceValue()
        {
            ASPxPageControl pageControl = this.tlDeliveryDistances.FindEditFormTemplateControl("tabs") as ASPxPageControl;
            ASPxTextBox tbPrice = pageControl.TabPages[0].FindControl("tbPrice") as ASPxTextBox;

            bool isParsed = decimal.TryParse(tbPrice.Value.ToString(), out decimal result);

            return isParsed ? result : decimal.Zero;
        }

        #endregion

        #region Methods

        public void InitDataSource()
        {
            if (this.DataSource != null)
            {
                this._deliveryDistanceDataSource = new DeliveryDistanceDataSource(this.DataSourceAsServiceMethodEntity.ServiceMethodId);

                this.tlDeliveryDistances.DataSource = this._deliveryDistanceDataSource;
                this.tlDeliveryDistances.DataBind();
            }
        }

        private void CreateTreeList()
        {
            this.tlDeliveryDistances.SettingsEditing.Mode = TreeListEditMode.EditFormAndDisplayNode;
            this.tlDeliveryDistances.SettingsBehavior.ColumnResizeMode = ColumnResizeMode.Disabled;
            this.tlDeliveryDistances.SettingsBehavior.AllowSort = false;
            this.tlDeliveryDistances.SettingsBehavior.AllowDragDrop = false;
            this.tlDeliveryDistances.ClientInstanceName = this.TreelistId;
            this.tlDeliveryDistances.AutoGenerateColumns = false;
            this.tlDeliveryDistances.SettingsEditing.AllowNodeDragDrop = false;
            this.tlDeliveryDistances.SettingsEditing.AllowRecursiveDelete = true;
            this.tlDeliveryDistances.Settings.GridLines = GridLines.Both;
            this.tlDeliveryDistances.KeyFieldName = nameof(DeliveryDistanceFields.DeliveryDistanceId);
            this.tlDeliveryDistances.ClientSideEvents.NodeDblClick = this.NodeDblClickMethod;
            this.tlDeliveryDistances.CustomErrorText += this.CustomErrorText;

            TreeListTextColumn maximumInMetresColumn = new TreeListTextColumn();
            maximumInMetresColumn.Name = "MaximumInMetres";
            maximumInMetresColumn.Caption = "Maximum delivery distance in metres";
            maximumInMetresColumn.VisibleIndex = 0;
            maximumInMetresColumn.FieldName = nameof(DeliveryDistanceFields.MaximumInMetres);
            maximumInMetresColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Right;
            maximumInMetresColumn.CellStyle.Wrap = DefaultBoolean.True;
            maximumInMetresColumn.EditFormSettings.VisibleIndex = 0;
            this.tlDeliveryDistances.Columns.Add(maximumInMetresColumn);

            TreeListDataColumn priceColumn = new TreeListDataColumn();
            priceColumn.Name = "Price";
            priceColumn.Caption = "Price";
            priceColumn.VisibleIndex = 1;
            priceColumn.FieldName = nameof(DeliveryDistanceFields.Price);
            priceColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Right;
            priceColumn.CellStyle.Wrap = DefaultBoolean.True;
            priceColumn.EditFormSettings.VisibleIndex = 1;
            priceColumn.DisplayFormat = "{0:c2}";
            priceColumn.PropertiesEdit.DisplayFormatString = "{0:c2}";
            this.tlDeliveryDistances.Columns.Add(priceColumn);

            TreeListCommandColumn editCommandColumn = new TreeListCommandColumn();
            editCommandColumn.VisibleIndex = 2;
            editCommandColumn.Width = Unit.Pixel(60);
            editCommandColumn.ShowNewButtonInHeader = true;
            editCommandColumn.EditButton.Visible = true;
            editCommandColumn.NewButton.Visible = false;
            editCommandColumn.DeleteButton.Visible = true;
            editCommandColumn.ButtonType = ButtonType.Image;
            editCommandColumn.EditButton.Image.Url = this.ResolveUrl("~/images/icons/pencil.png");
            editCommandColumn.DeleteButton.Image.Url = this.ResolveUrl("~/images/icons/delete.png");
            editCommandColumn.CancelButton.Image.Url = this.ResolveUrl("~/images/icons/cross_grey.png");
            editCommandColumn.NewButton.Image.Url = this.ResolveUrl("~/images/icons/add2.png");
            editCommandColumn.UpdateButton.Image.Url = this.ResolveUrl("~/images/icons/disk.png");
            this.tlDeliveryDistances.Columns.Add(editCommandColumn);
            
            this.tlDeliveryDistances.NodeValidating += this.NodeValidating;
            this.tlDeliveryDistances.NodeInserting += this.NodeInserting;
            this.tlDeliveryDistances.NodeUpdating += this.NodeUpdating;

            this.InitDataSource();
        }

        #endregion
    }
}