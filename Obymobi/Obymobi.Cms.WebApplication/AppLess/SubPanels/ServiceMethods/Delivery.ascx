﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Delivery.ascx.cs" Inherits="Obymobi.Cms.WebApplication.AppLess.SubPanels.ServiceMethods.Delivery" %>
<D:Panel ID="pnlDelivery" GroupingText="Delivery distances" runat="server">
    <table class="dataformV2">
        <tr>
            <td class="label"></td>
            <td class="control">
                <D:PlaceHolder runat="server" ID="pnlDeliveryDistances"></D:PlaceHolder>
            </td>

            <td class="label"></td>
            <td class="control"></td>
        </tr>
    </table>
</D:Panel>
