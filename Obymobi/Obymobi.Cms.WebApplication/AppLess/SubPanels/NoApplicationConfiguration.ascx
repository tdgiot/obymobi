﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NoApplicationConfiguration.ascx.cs" Inherits="Obymobi.Cms.WebApplication.AppLess.SubPanels.NoApplicationConfiguration" %>

<p>
    The company doesn't contain a AppLess configuration profile.
    Please create a profile <a href="<%= ResolveUrl("~/AppLess/ApplicationConfigurations.aspx") %>">here</a> before you can configure AppLess related settings.
</p>
