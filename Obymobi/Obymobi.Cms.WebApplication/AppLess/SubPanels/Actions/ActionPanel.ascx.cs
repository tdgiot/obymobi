﻿using System;
using System.Linq;
using System.Net;
using System.Web.UI;
using Dionysos;
using Dionysos.Interfaces;
using Dionysos.Web.UI;
using Dionysos.Web.UI.WebControls;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.AppLess.SubPanels
{
    public partial class ActionPanel : UserControl, ISaveableControl
    {
        #region Fields

        private ActionEntity actionEntity;

        #endregion

        #region Methods

        protected void Page_Load(object sender, EventArgs e)
        { }

        public void SetGui()
        {
            this.LoadUserControls();

            if (!this.actionEntity.IsNew)
            {
                cbLandingPageId.Value = this.actionEntity.LandingPageId;
                cbCategoryId.Value = this.actionEntity.CategoryId;
                cbProductCategoryId.Value = this.actionEntity.ProductCategoryId;
                tbResource.Value = this.actionEntity.Resource;
            }
        }

        public void LoadAction(ActionEntity actionEntity, int applicationConfigurationId)
        {
            this.actionEntity = actionEntity;
            this.actionEntity.ApplicationConfigurationId = applicationConfigurationId;

            this.SetGui();
        }
        
        public void LoadUserControls()
        {
            this.DataBindCategories();
            if (CmsSessionHelper.CurrentUser.Role > Role.Supervisor)
            {
                this.DataBindProductCategory();
                this.plhProduct.Visible = true;
            }
            this.DataBindLandingPages();
            this.InitActionType();
        }

        private ActionType? DetermineActionType()
        {
            if (cbLandingPageId.Value.HasValue || cbCategoryId.Value.HasValue || cbProductCategoryId.Value.HasValue)
            {
                return ActionType.InternalLink;
            }
            else if (this.ddlActionType.Value.HasValue && this.ddlActionType.Value > 0 && Enum.TryParse(this.ddlActionType.Value.ToString(), out ActionType actionType))
            {
                return actionType;
            }

            return null;
        }

        public bool Save()
        {
            ActionType? actionType = this.DetermineActionType();
            
            if (!actionType.HasValue)
            {
                this.MultiValidator.AddError("An action must be configured!");
                return false;
            }

            this.actionEntity.ActionType = actionType.Value;

            if (actionType == ActionType.InternalLink)
            {
                this.actionEntity.LandingPageId = cbLandingPageId.Value;
                this.actionEntity.CategoryId = cbCategoryId.Value;
                this.actionEntity.ProductCategoryId = cbProductCategoryId.Value;
            }
            else if (actionType == ActionType.EmbeddedLink || 
                     actionType == ActionType.ExternalLink ||
                     actionType == ActionType.Download ||
                     actionType == ActionType.PhoneCall)
            {
                if (tbResource.Value.IsNullOrWhiteSpace())
                {
                    this.MultiValidator.AddError("You must specify the resource.");
                    return false;
                }

                this.actionEntity.LandingPageId = null;
                this.actionEntity.CategoryId = null;
                this.actionEntity.ProductCategoryId = null;

                this.actionEntity.Resource = tbResource.Value;
            }
            
            if (actionType == ActionType.EmbeddedLink)
            {
                if (!tbResource.Value.StartsWith("https://", StringComparison.InvariantCultureIgnoreCase))
                {
                    this.MultiValidator.AddError("Only HTTPS url's can be embedded.");
                    return false;
                }
                else if (!IsIframingAllowed(tbResource.Value))
                {
                    this.MultiValidator.AddWarning("By protection headers the owner of the embedded website has forbidden to embed this page");
                    return false;
                }
            }

            return this.actionEntity.Save();
        }

        private MultiValidator MultiValidator => (this.Page as PageDefault)?.MultiValidatorDefault;

        private static bool IsIframingAllowed(string url)
        {
            using (WebClient client = new WebClient())
            {
                try
                {
                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                    
                    client.DownloadString(url);
                    return !client.ResponseHeaders.AllKeys.Any(a => a == "X-Frame-Options");
                }
                catch
                {
                    return true;
                }
            }
        }

        #endregion

        #region DataBinding

        private void DataBindCategories()
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(CategoryFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

            IncludeFieldsList includes = new IncludeFieldsList();
            includes.Add(CategoryFields.Name);

            PrefetchPath prefetch = new PrefetchPath(EntityType.CategoryEntity);
            prefetch.Add(CategoryEntityBase.PrefetchPathMenuEntity, new IncludeFieldsList(MenuFields.Name, MenuFields.CompanyId));

            RelationCollection relations = new RelationCollection();
            relations.Add(CategoryEntityBase.Relations.MenuEntityUsingMenuId);

            SortExpression sort = new SortExpression();
            sort.Add(MenuFields.Name | SortOperator.Ascending);
            sort.Add(CategoryFields.Name | SortOperator.Ascending);

            CategoryCollection categoryCollection = new CategoryCollection();
            categoryCollection.GetMulti(filter, 0, sort, relations, prefetch, includes, 0, 0);

            this.cbCategoryId.DataSource = categoryCollection;
            this.cbCategoryId.DataBind();
        }

        private void DataBindProductCategory()
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ProductCategoryFields.ParentCompanyId == CmsSessionHelper.CurrentCompanyId);

            IncludeFieldsList includes = new IncludeFieldsList();
            includes.Add(ProductCategoryFields.ProductCategoryId);
            includes.Add(ProductCategoryFields.ParentCompanyId);

            PrefetchPath prefetch = new PrefetchPath(EntityType.ProductCategoryEntity);
            IPrefetchPathElement prefetchProduct = prefetch.Add(ProductCategoryEntityBase.PrefetchPathProductEntity, new IncludeFieldsList(ProductFields.Name, ProductFields.CompanyId));
            IPrefetchPathElement prefetchCategory = prefetch.Add(ProductCategoryEntityBase.PrefetchPathCategoryEntity, new IncludeFieldsList(CategoryFields.Name, CategoryFields.CompanyId));
            IPrefetchPathElement prefetchMenu = prefetchCategory.SubPath.Add(CategoryEntityBase.PrefetchPathMenuEntity, new IncludeFieldsList(MenuFields.Name, MenuFields.CompanyId));

            RelationCollection relations = new RelationCollection();
            relations.Add(ProductCategoryEntityBase.Relations.CategoryEntityUsingCategoryId);
            relations.Add(CategoryEntityBase.Relations.MenuEntityUsingMenuId);
            relations.Add(ProductCategoryEntityBase.Relations.ProductEntityUsingProductId);

            SortExpression sort = new SortExpression();
            sort.Add(MenuFields.Name | SortOperator.Ascending);
            sort.Add(CategoryFields.Name | SortOperator.Ascending);
            sort.Add(ProductFields.Name | SortOperator.Ascending);

            ProductCategoryCollection productCategoryCollection = new ProductCategoryCollection();
            productCategoryCollection.GetMulti(filter, 0, sort, relations, prefetch, includes, 0, 0);

            this.cbProductCategoryId.DataSource = productCategoryCollection;
            this.cbProductCategoryId.DataBind();
        }

        private void InitActionType()
        {
            ActionType[] valuesToSkip = { ActionType.InternalLink };

            this.ddlActionType.DataBindEnum<ActionType>(valuesToSkip);

            if (!actionEntity.IsNew && !valuesToSkip.Contains(actionEntity.ActionType))
            {
                this.ddlActionType.Value = (int)this.actionEntity.ActionType;
            }
        }

        private void DataBindLandingPages()
        {
            this.cbLandingPageId.DataBindEntityCollection<LandingPageEntity>(LandingPageFields.ParentCompanyId == CmsSessionHelper.CurrentCompanyId, LandingPageFields.Name, LandingPageFields.Name, LandingPageFields.ParentCompanyId);
        }

        #endregion
    }
}