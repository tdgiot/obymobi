﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.AppLess.SubPanels.ActionPanel" CodeBehind="ActionPanel.ascx.cs" %>
<table class="dataformV2">
	<tr>
		<td class="label">
			<D:LabelEntityFieldInfo runat="server" ID="lblLandingPageId">Page</D:LabelEntityFieldInfo>
		</td>
		<td class="control">
			<X:ComboBoxLLBLGenEntityCollection ID="cbLandingPageId" runat="server" IncrementalFilteringMode="Contains" EntityName="LandingPage" ValueField="LandingPageId" TextField="Name" PreventEntityCollectionInitialization="true" UseDataBinding="true" />
		</td>
	</tr>
	<tr>
		<td class="label">
			<D:LabelEntityFieldInfo runat="server" ID="lblCategory">Category</D:LabelEntityFieldInfo>
		</td>
		<td class="control">
			<X:ComboBoxLLBLGenEntityCollection ID="cbCategoryId" runat="server" IncrementalFilteringMode="Contains" EntityName="Category" ValueField="CategoryId" TextField="MenuCategoryName" PreventEntityCollectionInitialization="true" UseDataBinding="true" />
		</td>
		<td class="label"></td>
		<td class="control"></td>
	</tr>
	<D:PlaceHolder runat="server" ID="plhProduct" visible="false">
		<tr>
			<td class="label">
				<D:LabelEntityFieldInfo runat="server" ID="lblProductCategory">Product</D:LabelEntityFieldInfo>
			</td>
			<td class="control">
				<X:ComboBoxLLBLGenEntityCollection ID="cbProductCategoryId" runat="server" IncrementalFilteringMode="Contains" EntityName="ProductCategory" ValueField="ProductCategoryId" TextField="MenuCategoryProductName" PreventEntityCollectionInitialization="true" UseDataBinding="true" />
			</td>
			<td class="label"></td>
			<td class="control"></td>
		</tr>
	</D:PlaceHolder>
	<tr>
		<td class="label">
			<D:LabelEntityFieldInfo runat="server" ID="lblActionType">Resource Type</D:LabelEntityFieldInfo>
		</td>
		<td class="control">
			<X:ComboBoxInt runat="server" ID="ddlActionType" UseDataBinding="False"></X:ComboBoxInt>
		</td>
		<td class="label">
			<D:LabelEntityFieldInfo runat="server" ID="lblResource">Resource</D:LabelEntityFieldInfo>
		</td>
		<td class="control">
			<D:TextBoxString ID="tbResource" runat="server" IsRequired="false" MaxLength="1024"></D:TextBoxString>
		</td>
	</tr>
</table>
