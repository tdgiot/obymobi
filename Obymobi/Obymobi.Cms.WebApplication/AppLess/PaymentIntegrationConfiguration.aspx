﻿<%@ Page Title="Payment Provider" Async="true" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" CodeBehind="PaymentIntegrationConfiguration.aspx.cs" Inherits="Obymobi.ObymobiCms.AppLess.PaymentIntegrationConfiguration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" runat="server">
	<div>
		<X:PageControl ID="tabsMain" runat="server" Width="100%">
			<TabPages>
				<X:TabPage Text="Generic" Name="Generic">
					<controls>
						<table class="dataformV2">
							<tr>
								<td class="label">
									<D:LabelEntityFieldInfo runat="server" ID="lblDisplayName">Display name</D:LabelEntityFieldInfo>
								</td>
								<td class="control">
									<D:TextBoxString ID="tbDisplayName" runat="server" IsRequired="true" notdirty="true"></D:TextBoxString>
								</td>
								<td class="label">
									<D:LabelEntityFieldInfo runat="server" ID="lblPaymentProvider">Payment provider</D:LabelEntityFieldInfo>
								</td>
								<td class="control">
									<X:ComboBoxInt runat="server" ID="cbPaymentProviderId" CssClass="input" TextField="Name" ValueField="PaymentProviderId" IncrementalFilteringMode="Contains" IncrementalFilteringDelay="1000" IsRequired="True" AutoPostBack="True" notdirty="true"></X:ComboBoxInt>
								</td>
							</tr>
							<tr>
								<td class="label">
									<D:LabelEntityFieldInfo runat="server" ID="lblMerchantName">Merchant name</D:LabelEntityFieldInfo>
								</td>
								<td class="control">
									<D:TextBoxString ID="tbMerchantName" runat="server" IsRequired="true" notdirty="true"></D:TextBoxString>
								</td>
								<td class="label"></td>
								<td class="control"></td>
							</tr>
						</table>
						
						<D:PlaceHolder runat="server" ID="plhPaymentProviderInfo" Visible="false" />

						<D:Panel ID="pnlPaymentProcessorInfo" GroupingText="Payment processor info" runat="server"></D:Panel>
					</controls>
				</X:TabPage>
			</TabPages>
		</X:PageControl>
	</div>
</asp:Content>
