﻿using System;
using System.Web.UI.WebControls;
using DevExpress.Data;
using Dionysos.Web.UI;
using Obymobi.Data;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.ObymobiCms.MasterPages;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.AppLess
{
    public partial class Receipts : PageLLBLOverviewDataSourceCollection
    {
        #region fields

        private readonly IncludeFieldsList paymentTransactionIncludeFieldList = new IncludeFieldsList
        {
            PaymentTransactionFields.ReferenceId,
            PaymentTransactionFields.PaymentProviderName
        };

        #endregion

        #region Events

        protected override void OnInit(EventArgs e)
        {
            this.Filter = new PredicateExpression();
            this.Filter.AddWithOr(ReceiptFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

            this.PrefetchPath = new PrefetchPath(EntityType.ReceiptEntity);

            IPrefetchPathElement prefetchOrder = this.PrefetchPath.Add(ReceiptEntityBase.PrefetchPathOrderEntity);
            prefetchOrder.SubPath.Add(OrderEntityBase.PrefetchPathPaymentTransactionCollection, paymentTransactionIncludeFieldList);

            this.IncludeFieldsList = new IncludeFieldsList
            {
                ReceiptFields.CreatedUTC,
            };

            if (this.Master is MasterPageEntityCollection masterPage)
            {
                masterPage.ToolBar.AddButton.Visible = false;
            }

            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.MainGridView.DataBound += MainGridViewOnDataBound;
        }

        private void MainGridViewOnDataBound(object sender, EventArgs e)
        {
            if (this.MainGridView.Columns["CreatedInCompanyTimeZone"] != null)
            {
                this.MainGridView.SortBy(this.MainGridView.Columns["CreatedInCompanyTimeZone"], ColumnSortOrder.Descending);
            }

            if (this.MainGridView.Columns["Number"] != null)
            {
                this.MainGridView.Columns["Number"].CellStyle.HorizontalAlign = HorizontalAlign.Left;
            }
        }

        #endregion
    }
}
