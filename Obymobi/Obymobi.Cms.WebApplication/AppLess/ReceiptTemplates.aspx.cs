﻿using System;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;

namespace Obymobi.ObymobiCms.AppLess
{
    public partial class ReceiptTemplates : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        #region Event handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.DataSource is LLBLGenProDataSource dataSource)
            {
                dataSource.FilterToUse = new PredicateExpression(ReceiptTemplateFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            }
        }

        #endregion
    }
}
