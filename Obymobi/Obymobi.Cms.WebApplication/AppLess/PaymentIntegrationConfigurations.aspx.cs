﻿using System;
using System.Web.UI.WebControls;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.AppLess
{
    public partial class PaymentIntegrationConfigurations : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        protected override void OnInit(EventArgs e)
        {
            this.Filter = new PredicateExpression();
            this.Filter.AddWithOr(PaymentIntegrationConfigurationFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            MainGridView.ShowDeleteHyperlinkColumn = CmsSessionHelper.CurrentRole == Role.GodMode;

            this.MainGridView.HtmlRowPrepared += this.MainGridView_HtmlRowPrepared;
            this.MainGridView.Styles.Cell.HorizontalAlign = HorizontalAlign.Left;
        }

        private void MainGridView_HtmlRowPrepared(object sender, DevExpress.Web.ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType != DevExpress.Web.GridViewRowType.Data) return;

            //// Convert the operation mode enum to text
            //int type = (int)e.GetValue("Type");
            //{
            //    PaymentProviderType typeAsEnum = type.ToEnum<PaymentProviderType>();
            //    this.SetCellValue(e.Row, "Type", typeAsEnum.ToString());
            //}

            //int environment = (int)e.GetValue("Environment");
            //{
            //    PaymentProviderEnvironment typeAsEnum = environment.ToEnum<PaymentProviderEnvironment>();
            //    this.SetCellValue(e.Row, "Environment", typeAsEnum.ToString());
            //}
        }

        private void SetCellValue(TableRow row, string fieldName, string value)
        {
            if (this.MainGridView.Columns[fieldName] != null)
            {
                int index = this.MainGridView.Columns[fieldName].Index;
                if (index < row.Cells.Count)
                {
                    row.Cells[index].Text = value;
                }
            }
        }
    }
}