﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Net;
using Crave.Api.Logic;
using Crave.Api.Logic.Requests;
using Crave.Api.Logic.UseCases;
using Dionysos;
using Dionysos.Web.UI;
using Google.Apis.Manual.Util;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Obymobi.Cms.Logic.Extensions;
using Obymobi.Data;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Extensions;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.AppLess
{
    public partial class Receipt : Dionysos.Web.UI.PageLLBLGenEntity
    {
        private void LoadUserControls()
        {

        }

        private void SetGui()
        {
            this.btnSendReceipt.Click += this.BtnSendReceipt_Click;

            this.RenderReceiptSummary(this.DataSourceAsReceiptEntity);
            this.RenderVendorInformation(this.DataSourceAsReceiptEntity);
            this.RenderCustomerInformation(this.DataSourceAsReceiptEntity);
            this.RenderDeliveryInformation(this.DataSourceAsReceiptEntity);
            this.RenderOrder(this.DataSourceAsReceiptEntity);
            this.RenderPaymentTransactions(this.DataSourceAsReceiptEntity);
            this.RenderOrderNotificationLogs(this.DataSourceAsReceiptEntity);

            if (CmsSessionHelper.CurrentUser.Role >= Role.Crave)
            {
                this.RenderPaymentSplit(this.DataSourceAsReceiptEntity);
            }
        }

        private void BtnSendReceipt_Click(object sender, EventArgs e)
        {
            string additionalEmail = this.tbAdditionalEmail.Text.Trim();
            bool sendToOriginalReceipient = this.cbSendToReceiver.Checked;

            if (additionalEmail.Length == 0 && !sendToOriginalReceipient)
            {
                return;
            }

            DoApiCall(sendToOriginalReceipient, additionalEmail);
        }

        private void RenderReceiptSummary(ReceiptEntity receipt)
        {
            this.lblOrderId.Text = receipt.OrderId.ToString();
            this.lblNumber.Text = receipt.Number.ToString();

            this.hlOutletId.Text = receipt.OrderEntity.OutletEntity?.Name;
            this.hlOutletId.NavigateUrl = $"~/AppLess/Outlet.aspx?id={receipt.OrderEntity.OutletId}";

            this.hlServiceMethodName.Text = receipt.ServiceMethodName;
            this.hlServiceMethodName.NavigateUrl = $"~/AppLess/ServiceMethod.aspx?id={receipt.OrderEntity.ServiceMethodId}";

            this.hlCheckoutMethod.Text = receipt.CheckoutMethodName;
            this.hlCheckoutMethod.NavigateUrl = $"~/AppLess/CheckoutMethod.aspx?id={receipt.OrderEntity.CheckoutMethodId}";
            this.hlCheckoutMethod.RenderAsText = CmsSessionHelper.CurrentRole < Role.Crave;

            this.lblCreatedLocal.Text = receipt.CreatedInCompanyTimeZone.GetValueOrDefault().ToString("G");
        }

        private void RenderVendorInformation(ReceiptEntity receipt)
        {
            this.lblVendorName.Text = receipt.SellerName;
            this.lblVendorAddress.Text = receipt.SellerAddress;
            this.lblVendorPhone.Text = receipt.SellerContactInfo;
        }

        private void RenderCustomerInformation(ReceiptEntity receipt)
        {
            OrderEntity order = receipt.OrderEntity;
            this.lblCustomerName.Text = order.CustomerLastname;
            this.lblCustomerEmail.Text = order.Email;
            this.hlResendReceipt.Visible = order.Email.IsNotNullOrEmpty() && CmsSessionHelper.CurrentRole >= Role.Supervisor;
            this.lblCustomerPhone.Text = order.CustomerPhonenumber;
        }

        private void RenderDeliveryInformation(ReceiptEntity receipt)
        {
            if (!receipt.OrderEntity.DeliveryInformationId.HasValue)
                return;

            DeliveryInformationEntity deliveryInformation = receipt.OrderEntity.DeliveryInformationEntity;
            this.lblDeliveryBuildingName.Text = deliveryInformation.BuildingName;
            this.lblDeliveryAddress.Text = deliveryInformation.Address;
            this.lblDeliveryZipcode.Text = deliveryInformation.Zipcode;
            this.lblDeliveryCity.Text = deliveryInformation.City;
            this.lblDeliveryInstructions.Text = deliveryInformation.Instructions;
            this.pnlDelivery.Visible = true;
        }

        private void RenderOrder(ReceiptEntity receipt)
        {
            this.gvOrderitems.DataSource = this.CreateOrderDataTable(receipt.OrderEntity);
            this.gvOrderitems.DataBind();

            if (!receipt.OrderEntity.PricesIncludeTaxes)
            {
                this.gvTotal.Visible = true;
                this.gvTotal.DataSource = this.CreateTotalDataTable(receipt.OrderEntity);
                this.gvTotal.DataBind();
            }
            else
            {
                this.gvTaxBreakdown.Visible = true;
                this.gvTaxBreakdown.DataSource = this.CreateTaxBreakdownDataTable(receipt.OrderEntity);
                this.gvTaxBreakdown.DataBind();
            }
        }

        private void RenderPaymentTransactions(ReceiptEntity receipt)
        {
            PaymentTransactionEntity paidTransactionEntity = receipt.OrderEntity.PaymentTransactionCollection.FirstOrDefault();
            if (paidTransactionEntity != null)
            {
                this.hlPaymentProvider.Text = paidTransactionEntity.PaymentProviderName;

                if (paidTransactionEntity.PaymentProviderId.HasValue)
                {
                    this.hlPaymentProvider.NavigateUrl = $"~/Configuration/Sys/PaymentProvider.aspx?id={paidTransactionEntity.PaymentProviderId.Value}";
                }
            }

            this.gvPaymentTransaction.DataSource = this.CreatePaymentTransactionDataTable(receipt.OrderEntity);
            this.gvPaymentTransaction.DataBind();
        }

        private void RenderPaymentSplit(ReceiptEntity receipt)
        {
            IOrderedEnumerable<PaymentTransactionSplitEntity> paymentSplits = receipt.OrderEntity.PaymentTransactionCollection
                .SelectMany(x => x.PaymentTransactionSplitCollection)
                .OrderByDescending(x => x.CreatedUTC);

            pnlPaymentSplit.Visible = paymentSplits.Any();

            if (pnlPaymentSplit.Visible)
            {
                gvPaymentSplit.DataSource = this.CreatePaymentSplitDataTable(paymentSplits, receipt.OrderEntity.CultureCode);
                gvPaymentSplit.DataBind();
            }
        }

        private void RenderOrderNotificationLogs(ReceiptEntity receipt)
        {
            this.gvOrderNotificationLog.DataSource = this.CreateOrderNotificationDataTable(receipt.OrderEntity);
            this.gvOrderNotificationLog.DataBind();
        }

        private DataTable CreateOrderDataTable(OrderEntity order)
        {
            DataTable table = new DataTable();

            table.Columns.Add("Description");
            table.Columns.Add("Quantity");
            table.Columns.Add("Price");

            var orderItems = order.OrderitemCollection
                .Where(x => x.Type == OrderitemType.Product)
                .OrderBy(x => x.OrderitemId);

            foreach (OrderitemEntity orderitem in orderItems)
            {
                DataRow itemRow = table.NewRow();
                itemRow[0] = orderitem.ProductName;
                itemRow[1] = $"{orderitem.Quantity} x {orderitem.GetDisplayPriceFormatted()}";
                itemRow[2] = orderitem.GetTotalDisplayPriceFormatted();

                table.Rows.Add(itemRow);

                var alterationItems = orderitem.OrderitemAlterationitemCollection
                    .Where(x => !x.OrderLevelEnabled)
                    .OrderBy(x => x.OrderitemAlterationitemId).ToArray();

                for (int i = 0; i < alterationItems.Length; i++)
                {
                    DataRow alterationRow = table.NewRow();

                    string prefix = i == alterationItems.Length - 1 ? "└" : "├";
                    alterationRow[0] = $" {prefix} {this.GetAlterationDescription(alterationItems[i])}";

                    if (alterationItems[i].AlterationoptionPriceIn > 0)
                    {
                        alterationRow[1] = $"{alterationItems[i].GetQuantity()} x {alterationItems[i].GetDisplayPriceFormatted()}";
                        alterationRow[2] = alterationItems[i].GetTotalDisplayPriceFormatted();
                    }

                    table.Rows.Add(alterationRow);
                }
            }

            var orderLevelAlterations = order.OrderitemCollection
                .SelectMany(x => x.OrderitemAlterationitemCollection)
                .Where(x => x.OrderLevelEnabled)
                .OrderBy(x => x.OrderitemAlterationitemId);

            foreach (OrderitemAlterationitemEntity orderLevelAlteration in orderLevelAlterations)
            {
                DataRow alterationRow = table.NewRow();
                alterationRow[0] = $" » {this.GetAlterationDescription(orderLevelAlteration)}";

                if (orderLevelAlteration.AlterationoptionPriceIn > 0)
                {
                    alterationRow[1] = $"{orderLevelAlteration.GetQuantity()} x {orderLevelAlteration.GetDisplayPriceFormatted()}";
                    alterationRow[2] = orderLevelAlteration.GetTotalDisplayPriceFormatted();
                }

                table.Rows.Add(alterationRow);
            }

            var systemProducts = order.OrderitemCollection
                .Where(x => x.Type != OrderitemType.Product)
                .OrderBy(x => x.Type);

            bool systemProductSpacing = true;
            foreach (OrderitemEntity systemProduct in systemProducts)
            {
                if (systemProductSpacing)
                {
                    systemProductSpacing = false;
                    table.Rows.Add(table.NewRow());
                }

                DataRow itemRow = table.NewRow();
                itemRow[0] = systemProduct.ProductName;
                itemRow[2] = systemProduct.GetTotalDisplayPriceFormatted();

                table.Rows.Add(itemRow);

                var alterationItems = systemProduct.OrderitemAlterationitemCollection
                    .Where(x => !x.OrderLevelEnabled)
                    .OrderBy(x => x.OrderitemAlterationitemId).ToArray();

                for (int i = 0; i < alterationItems.Length; i++)
                {
                    DataRow alterationRow = table.NewRow();

                    string prefix = i == alterationItems.Length - 1 ? "└" : "├";
                    alterationRow[0] = $" {prefix} {this.GetAlterationDescription(alterationItems[i])}";

                    if (alterationItems[i].AlterationoptionPriceIn > 0)
                    {
                        alterationRow[1] = $"{alterationItems[i].GetQuantity()} x {alterationItems[i].GetDisplayPriceFormatted()}";
                        alterationRow[2] = alterationItems[i].GetTotalDisplayPriceFormatted();
                    }

                    table.Rows.Add(alterationRow);
                }
            }

            if (order.PricesIncludeTaxes)
            {
                table.Rows.Add(table.NewRow());

                DataRow totalRow = table.NewRow();
                totalRow[0] = "Total";
                totalRow[1] = string.Empty;
                totalRow[2] = order.TotalFormatted;
                table.Rows.Add(totalRow);
            }

            return table;
        }

        private DataTable CreateTotalDataTable(OrderEntity orderEntity)
        {
            DataTable dataTable = new DataTable();

            dataTable.Columns.Add("Description");
            dataTable.Columns.Add("Total");

            DataRow subTotalRow = dataTable.NewRow();
            subTotalRow[0] = "SubTotal";
            subTotalRow[1] = orderEntity.SubTotalFormatted;
            dataTable.Rows.Add(subTotalRow);

            foreach (TaxTariffRecord taxTariffRecord in this.GetTaxTariffRecords(orderEntity, true))
            {
                DataRow taxRow = dataTable.NewRow();

                if (taxTariffRecord.Name.IsNotNullOrEmpty())
                {
                    taxRow[0] = $"{taxTariffRecord.Name} ({taxTariffRecord.Percentage}%)";
                }
                else
                {
                    taxRow[0] = $"{taxTariffRecord.Percentage}%";
                }

                taxRow[1] = taxTariffRecord.VatPrice.Format(orderEntity.CultureCode);
                dataTable.Rows.Add(taxRow);
            }

            DataRow totalRow = dataTable.NewRow();
            totalRow[0] = "Total";
            totalRow[1] = orderEntity.TotalFormatted;
            dataTable.Rows.Add(totalRow);

            return dataTable;
        }

        private DataTable CreateTaxBreakdownDataTable(OrderEntity orderEntity)
        {
            DataTable dataTable = new DataTable();

            dataTable.Columns.Add("Rate");
            dataTable.Columns.Add("Net");
            dataTable.Columns.Add("VAT");

            foreach (TaxTariffRecord taxTariffRecord in this.GetTaxTariffRecords(orderEntity))
            {
                DataRow dataRow = dataTable.NewRow();

                if (taxTariffRecord.Name.IsNotNullOrEmpty())
                {
                    dataRow[0] = $"{taxTariffRecord.Name} ({taxTariffRecord.Percentage}%)";
                }
                else
                {
                    dataRow[0] = $"{taxTariffRecord.Percentage}%";
                }

                dataRow[1] = taxTariffRecord.NetPrice.Format(orderEntity.CultureCode);
                dataRow[2] = taxTariffRecord.VatPrice.Format(orderEntity.CultureCode);

                dataTable.Rows.Add(dataRow);
            }

            return dataTable;
        }

        private IOrderedEnumerable<TaxTariffRecord> GetTaxTariffRecords(OrderEntity order, bool excludeZeroPercentage = false)
        {
            Dictionary<int, TaxTariffRecord> taxTariffRecords = new Dictionary<int, TaxTariffRecord>();

            foreach (OrderitemEntity orderItem in order.OrderitemCollection)
            {
                foreach (OrderitemAlterationitemEntity alterationItem in orderItem.OrderitemAlterationitemCollection)
                {
                    if ((excludeZeroPercentage && alterationItem.TaxPercentage <= 0) || !alterationItem.TaxTariffId.HasValue) continue;

                    AppendTaxTariffRecord(taxTariffRecords, alterationItem.TaxTariffId.Value, alterationItem.TaxPercentage,
                        alterationItem.PriceTotalExTax, alterationItem.TaxTotal, alterationItem.TaxName);
                }

                if ((excludeZeroPercentage && orderItem.TaxPercentage <= 0) || !orderItem.TaxTariffId.HasValue) continue;

                AppendTaxTariffRecord(taxTariffRecords, orderItem.TaxTariffId.Value, orderItem.TaxPercentage,
                    orderItem.PriceTotalExTax, orderItem.TaxTotal, orderItem.TaxName);
            }

            return taxTariffRecords.Values.OrderByDescending(taxTariffRecord => taxTariffRecord.Percentage);
        }

        private static TaxTariffRecord AppendTaxTariffRecord(IDictionary<int, TaxTariffRecord> taxTariffRecords, int taxTariffId, double percentage,
            decimal priceTotalExTax, decimal taxTotalPrice, string name = null)
        {
            if (!taxTariffRecords.TryGetValue(taxTariffId, out TaxTariffRecord taxTariffRecord))
            {
                taxTariffRecord = new TaxTariffRecord(taxTariffId, percentage, decimal.Zero, decimal.Zero, name);
                taxTariffRecords.Add(taxTariffId, taxTariffRecord);
            }

            taxTariffRecord.NetPrice += priceTotalExTax;
            taxTariffRecord.VatPrice += taxTotalPrice;

            return taxTariffRecord;
        }

        private string GetAlterationDescription(OrderitemAlterationitemEntity orderitemAlterationitem)
        {
            string description = string.Empty;

            if (!orderitemAlterationitem.AlterationName.IsNullOrWhiteSpace())
            {
                description += orderitemAlterationitem.AlterationName;
            }

            if (!orderitemAlterationitem.AlterationoptionName.IsNullOrWhiteSpace())
            {
                description += $": {orderitemAlterationitem.AlterationoptionName}";
            }

            if (!orderitemAlterationitem.Value.IsNullOrWhiteSpace())
            {
                description += $": {orderitemAlterationitem.Value}";
            }

            return description;
        }


        private DataTable CreatePaymentTransactionDataTable(OrderEntity order)
        {
            DataTable table = new DataTable();

            table.Columns.AddRange(new[]
            {
                new DataColumn("ReferenceId"),
                new DataColumn("MerchantReference"),
                new DataColumn("MerchantAccount"),
                new DataColumn("PaymentMethod"),
                new DataColumn("Status"),
                new DataColumn("CardSummary"),
                new DataColumn("AuthCode"),
                new DataColumn("CreatedLocal"),
                new DataColumn("Provider"),
                new DataColumn("Environment"),
            });

            foreach (PaymentTransactionEntity paymentTransaction in order.PaymentTransactionCollection.OrderByDescending(x => x.CreatedUTC))
            {
                DataRow row = table.NewRow();
                row[0] = paymentTransaction.ReferenceId;
                row[1] = paymentTransaction.MerchantReference;
                row[2] = paymentTransaction.MerchantAccount;
                row[3] = paymentTransaction.PaymentMethod;
                row[4] = paymentTransaction.StatusText;
                row[5] = paymentTransaction.CardSummary;
                row[6] = paymentTransaction.AuthCode;
                row[7] = paymentTransaction.CreatedLocal.GetValueOrDefault().ToString("G");
                row[8] = paymentTransaction.PaymentProviderType.ToString();
                row[9] = paymentTransaction.PaymentEnvironment.ToString();
                table.Rows.Add(row);
            }

            return table;
        }

        private DataTable CreatePaymentSplitDataTable(IOrderedEnumerable<PaymentTransactionSplitEntity> paymentSplits, string cultureCode)
        {
            DataTable table = new DataTable();

            table.Columns.AddRange(new[]
            {
                new DataColumn("Reference"),
                new DataColumn("AccountCode"),
                new DataColumn("Type"),
                new DataColumn("Amount"),
            });

            foreach (PaymentTransactionSplitEntity paymentSplit in paymentSplits)
            {
                DataRow row = table.NewRow();
                row[0] = paymentSplit.ReferenceId;
                row[1] = paymentSplit.AccountCode;
                row[2] = paymentSplit.SplitTypeText;
                row[3] = paymentSplit.Amount.Format(cultureCode);
                table.Rows.Add(row);
            }

            return table;
        }

        private DataTable CreateOrderNotificationDataTable(OrderEntity order)
        {
            DataTable table = new DataTable();

            table.Columns.AddRange(new[]
            {
                new DataColumn("Type"),
                new DataColumn("Method"),
                new DataColumn("Status"),
                new DataColumn("Receiver"),
                new DataColumn("Log"),
                new DataColumn("CreatedLocal"),
                new DataColumn("SentLocal"),
            });

            foreach (OrderNotificationLogEntity notification in order.OrderNotificationLogCollection.OrderByDescending(x => x.CreatedUTC))
            {
                DataRow row = table.NewRow();
                row[0] = notification.Type;
                row[1] = notification.Method;
                row[2] = notification.Status;
                row[3] = notification.Receiver;
                row[4] = notification.Log;
                row[5] = notification.CreatedUTC.UtcToLocalTime(order.TimeZoneInfo).ToString("G");
                row[6] = notification.SentUTC.HasValue ? notification.SentUTC.Value.UtcToLocalTime(order.TimeZoneInfo).ToString("G") : "";
                table.Rows.Add(row);
            }

            return table;
        }

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            this.DataSourceLoaded += Receipt_DataSourceLoaded;
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        { }

        private void Receipt_DataSourceLoaded(object sender)
        {
            this.SetGui();
        }

        public ReceiptEntity DataSourceAsReceiptEntity => this.DataSource as ReceiptEntity;


        private ApiCallAuthentication GetApiCallAuthentication(bool sendToOriginalReceiver, string additionalReceivers)
        {
            GetApiCallAuthenticationRequest getApiCallAuthenticationRequest = new GetApiCallAuthenticationRequest
            {
                CompanyId = CmsSessionHelper.CurrentCompanyId,
                CallerType = ApiCallerType.Terminal,
                ParameterNamesAndValues = new Dictionary<string, object>
                {
                    {"OrderId", this.DataSourceAsReceiptEntity.OrderId},
                    {"SendToOriginalReceiver", sendToOriginalReceiver},
                    {"AdditionalReceivers", additionalReceivers}
                }
            };
            return new GetApiCallAuthenticationUseCase().Execute(getApiCallAuthenticationRequest);
        }

        private void DoApiCall(bool sendToOriginalReceiver, string additionalReceivers)
        {
            ResendReceiptRequest requestModel = new ResendReceiptRequest
            {
                OrderId = this.DataSourceAsReceiptEntity.OrderId,
                AdditionalReceivers = additionalReceivers,
                SendToOriginalReceiver = sendToOriginalReceiver
            };

            ApiCall apiCall = new ApiCall
            {
                BaseUrl = WebEnvironmentHelper.GetRestApiBaseUrl(),
                Path = "/receipt/resend",
                RequestName = "ManualResendReceipt",
                Authentication = GetApiCallAuthentication(sendToOriginalReceiver, additionalReceivers)
            };

            string apiResponse = string.Empty;
            string url = apiCall.GetUrlForApiCall();
            try
            {
                using (WebClient webClient = new WebClient())
                {
                    webClient.Headers[HttpRequestHeader.ContentType] = "application/json";
                    apiResponse = webClient.UploadString(url, JsonConvert.SerializeObject(requestModel));
                }
            }
            catch (Exception ex)
            {
                this.AddInformator(InformatorType.Warning, "An exception occurred while executing the API call on url: {0}. Message: {1}", url, ex.Message);
                return;
            }

            ApiResult<SimpleApiResult> result = new ApiResult<SimpleApiResult>();
            if (!apiResponse.IsNullOrWhiteSpace())
            {
                JsonSerializerSettings settings = new JsonSerializerSettings();
                settings.NullValueHandling = NullValueHandling.Ignore;
                settings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                result = JsonConvert.DeserializeObject<ApiResult<SimpleApiResult>>(apiResponse, settings);
            }

            if (result.Code != 100)
            {
                string errorMessage = (result.Models != null && result.Models.ErrorMessage.IsNullOrWhiteSpace()) ? result.Models.ErrorMessage : result.Message;
                this.AddInformator(InformatorType.Warning, "Failed to resend receipt. Error: {0}", errorMessage);
            }
            else
            {
                this.AddInformatorInfo("Receipt has been sent");
            }
        }

        public class ResendReceiptRequest
        {
            public int OrderId { get; set; }
            public bool SendToOriginalReceiver { get; set; }
            public string AdditionalReceivers { get; set; }
        }

        public class TaxTariffRecord
        {
            public TaxTariffRecord(int taxTariffId, double taxPercentage, decimal netPrice, decimal vatPrice, string taxName = null)
            {
                this.TaxTariffId = taxTariffId;
                this.Name = taxName;
                this.Percentage = taxPercentage;
                this.NetPrice = netPrice;
                this.VatPrice = vatPrice;
            }

            [Required]
            public int TaxTariffId { get; }

            public string Name { get; }

            [Required]
            public double Percentage { get; }

            [Required]
            public decimal NetPrice { get; set; }

            [Required]
            public decimal VatPrice { get; set; }
        }
    }
}
