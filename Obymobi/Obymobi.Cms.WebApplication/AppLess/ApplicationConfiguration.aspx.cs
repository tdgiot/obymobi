using Dionysos;
using Dionysos.Web;
using Dionysos.Web.UI;
using Obymobi.Cms.Logic.DataSources;
using Obymobi.Cms.Logic.HelperClasses;
using Obymobi.Cms.WebApplication.AppLess;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Extensions;
using Obymobi.Logic.HelperClasses;
using Obymobi.ObymobiCms.AppLess.SubPanels;
using Obymobi.ObymobiCms.MasterPages;
using Resources;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Obymobi.ObymobiCms.AppLess
{
    public partial class ApplicationConfiguration : PageLLBLGenEntity
    {
        #region Fields

        private ApplicationConfigurationDataSource applicationConfigurationDataSource = new ApplicationConfigurationDataSource();

        #endregion

        #region Methods

        private void LoadUserControls()
        {
            this.cbAnalyticsEnvironment.DataBindEnum<AnalyticsEnvironment>();



            if (this.PageMode == PageMode.View ||
                this.PageMode == PageMode.Edit)
            {
                this.LoadMediaTab(mediaTabText: "Icons", supportMultipleImages: false, supportCultureSpecificImages: false, accessRole: Role.Supervisor);

                this.LoadTab("Pages", nameof(LandingPageCollection), "~/AppLess/SubPanels/LandingPageCollection.ascx", accessRole: Role.Supervisor);
                this.LoadTab("Navigation Menus", nameof(NavigationMenuCollection), "~/AppLess/SubPanels/NavigationMenuCollection.ascx", accessRole: Role.Supervisor);
                this.LoadTab("Themes", nameof(ThemeCollection), "~/AppLess/SubPanels/ThemeCollection.ascx", accessRole: Role.Supervisor);

                this.LoadTranslationsTab();

                this.cbLandingPageId.IsRequired = true;
            }
            else
            {
                this.lblLandingPageId.Visible = false;
                this.cbLandingPageId.Visible = false;
                this.cbEnableCookiewall.Enabled = true;
            }
        }

        private void SetGui()
        {
            CompanyEntity companyEntity;
            if (this.DataSourceAsApplicationConfigurationEntity.IsNew)
            {
                companyEntity = new CompanyEntity(CmsSessionHelper.CurrentCompanyId);
                this.applicationConfigurationDataSource.SetDefaultValuesOnApplicationConfiguration(this.DataSourceAsApplicationConfigurationEntity, CmsSessionHelper.CurrentCompanyId);
            }
            else
            {
                companyEntity = this.DataSourceAsApplicationConfigurationEntity.CompanyEntity;
            }

            this.cbLandingPageId.DataBindEntityCollection<LandingPageEntity>(LandingPageFields.ApplicationConfigurationId == this.DataSourceAsApplicationConfigurationEntity.ApplicationConfigurationId, LandingPageFields.Name, LandingPageFields.Name, LandingPageFields.ParentCompanyId);

            this.pwaDisabled.Checked = !HasFeatureFlag(ApplessFeatureFlags.PwaDisabled);

            if ((this.DataSourceAsApplicationConfigurationEntity?.LandingPageId ?? 0) == 0)
            {
                this.AddInformator(InformatorType.Warning, "No default page configured. Appless will not work as expected.");
            }

            // Set cultures
            Data.CollectionClasses.CompanyCultureCollection companyCultures = companyEntity.CompanyCultureCollection;
            IEnumerable<Culture> cultures = Obymobi.Culture.Mappings.Values.Where(x => companyCultures.Select(c => c.CultureCode).Contains(x.Code)).OrderBy(x => x.NameAndCultureCode);
            this.ddlCultureCode.DataSource = cultures;
            this.ddlCultureCode.DataBind();
            this.ddlCultureCode.SelectedItem = this.ddlCultureCode.Items.FindByValue(this.DataSourceAsApplicationConfigurationEntity.CultureCode);
            this.plhCraveAnalytics.Visible = CmsSessionHelper.CurrentRole >= Role.Crave;

            if (this.PageMode == PageMode.Add || this.DataSourceAsApplicationConfigurationEntity.IsNew)
            {
                this.DataSourceAsApplicationConfigurationEntity.EnableCookiewall = true;
                this.cbEnableCookiewall.Value = true;
            }
        }

        private bool HasFeatureFlag(ApplessFeatureFlags featureFlag) => this.DataSourceAsApplicationConfigurationEntity.FeatureFlagCollection.Any(a => a.ApplessFeatureFlags == featureFlag);

        private void SetWarnings()
        {
            if (this.DataSourceAsApplicationConfigurationEntity.MediaCollection.Count == 0)
            {
                this.AddInformator(InformatorType.Warning, "Please configure an icon or the application won't be available as a progressive web application for offline use.");
            }

            this.Validate();
        }

        #endregion

        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            this.DataSourceLoaded += this.ApplicationConfiguration_DataSourceLoaded;
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.DataSourceAsApplicationConfigurationEntity.IsNew &&
                !this.IsPostBack)
            {
                this.SetWarnings();
            }

            if (this.Master is MasterPageEntity masterPage)
            {
                masterPage.ToolBar.SaveButton.Visible = true;
                masterPage.ToolBar.SaveAndGoButton.Visible = false;
                masterPage.ToolBar.SaveAndNewButton.Visible = false;

                if (masterPage.ToolBar.DeleteButton != null)
                {
                    masterPage.ToolBar.DeleteButton.Visible = this.MayShowDeleteOption();
                }
            }
        }

        private bool MayShowDeleteOption()
        {
            CompanyEntity companyEntity = this.DataSourceAsApplicationConfigurationEntity?.CompanyEntity;
            if (companyEntity == null)
            {
                return false;
            }
            else
            {
                return (companyEntity.IsTestingCompany() || companyEntity.IsDevelopmentCompany())
                    && CmsSessionHelper.CurrentRole >= Enums.Role.Crave;
            }
        }

        private void ApplicationConfiguration_DataSourceLoaded(object sender)
        {
            this.SetGui();
        }

        public override bool Delete()
        {
            // Normally the landing page is required and we therefore prevent deletes of pages that are in use. We do however want a cascading delete in this specific case.
            this.DataSourceAsApplicationConfigurationEntity.LandingPageId = null;
            this.DataSourceAsApplicationConfigurationEntity.Save();

            return base.Delete();
        }

        public override bool Save()
        {
            this.DataSourceAsApplicationConfigurationEntity.CultureCode = !string.IsNullOrWhiteSpace(ddlCultureCode.SelectedValueString) ? ddlCultureCode.SelectedValueString : null;

            bool result = base.Save(true);
            if (result)
            {
                if (this.PageMode == PageMode.View ||
                    this.PageMode == PageMode.Edit)
                {
                    CustomTextHelper.UpdateCustomTexts(this.DataSource, this.DataSourceAsApplicationConfigurationEntity.CompanyEntity);

                    TranslationHelper.SetDefaultTranslations(ApplicationConfigurationDefaultTranslations.ResourceManager,
                        this.DataSourceAsApplicationConfigurationEntity,
                        this.DataSourceAsApplicationConfigurationEntity.CompanyEntity.GetDefaultCompanyCulture(),
                        this.DataSourceAsApplicationConfigurationEntity.CompanyEntity.GetCompanyCultures());
                }
                else if (this.PageMode == PageMode.Add)
                {
                    AddUnderlyingDefaultData();
                }
            }

            this.SaveFeatureFlags();

            return result;
        }

        private void SaveFeatureFlags()
        {
            FeatureFlagEntity pwaFlag = this.DataSourceAsApplicationConfigurationEntity.FeatureFlagCollection.FirstOrDefault(a => a.ApplessFeatureFlags == ApplessFeatureFlags.PwaDisabled);
            if (!this.pwaDisabled.Checked && pwaFlag == null)
            {
                pwaFlag = new FeatureFlagEntity
                {
                    ApplessFeatureFlags = ApplessFeatureFlags.PwaDisabled,
                    CompanyId = this.DataSourceAsApplicationConfigurationEntity.CompanyId
                };
                pwaFlag.ApplicationConfigurationEntity = this.DataSourceAsApplicationConfigurationEntity;
                pwaFlag.Save();
            }
            else if (this.pwaDisabled.Checked && pwaFlag != null)
            {
                pwaFlag.Delete();
                this.DataSourceAsApplicationConfigurationEntity.FeatureFlagCollection.Remove(pwaFlag);
            }
        }

        private void AddUnderlyingDefaultData()
        {
            ThemeEntity theme = new ThemeEntity
            {
                ApplicationConfigurationId = this.DataSourceAsApplicationConfigurationEntity.ApplicationConfigurationId,
                Name = "Light theme",
                FontFamily = "impact",
                ParentCompanyId = DataSourceAsApplicationConfigurationEntity.CompanyId,
                CreatedUTC = DateTime.Now,
                PrimaryColor = 13914932,
                SignalColor = 10150325,
                TextColor = 7041658,
                DividerColor = 13686494,
                SubnavColor = 15856114,
                BackgroundColor = 16777215,
            };

            NavigationMenuEntity navigationMenu = new NavigationMenuEntity()
            {
                CompanyId = DataSourceAsApplicationConfigurationEntity.CompanyId,
                ApplicationConfigurationId = this.DataSourceAsApplicationConfigurationEntity.ApplicationConfigurationId,
                Name = "Default navigation menu"
            };

            this.DataSourceAsApplicationConfigurationEntity.NavigationMenuCollection.Add(navigationMenu);

            LandingPageEntity page = new LandingPageEntity()
            {
                Name = "Default page",
                ThemeEntity = theme,
                NavigationMenuEntity = navigationMenu,
                ExternalId = RandomUtil.GetRandomString(6).ToUpperInvariant(),
                ParentCompanyId = DataSourceAsApplicationConfigurationEntity.CompanyId,
                ApplicationConfigurationId = this.DataSourceAsApplicationConfigurationEntity.ApplicationConfigurationId
            };

            this.DataSourceAsApplicationConfigurationEntity.LandingPageCollection.Add(page);
            this.DataSourceAsApplicationConfigurationEntity.DefaultLandingPageEntity = page;

            this.DataSourceAsApplicationConfigurationEntity.Save(true);
        }


        #endregion

        #region Properties

        public ApplicationConfigurationEntity DataSourceAsApplicationConfigurationEntity => this.DataSource as ApplicationConfigurationEntity;

        #endregion
    }
}
