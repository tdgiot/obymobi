﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.AppLess.ServiceMethod" CodeBehind="ServiceMethod.aspx.cs" %>
<%@ Reference VirtualPath="~/Generic/UserControls/CustomTextCollection.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" runat="Server">
    <X:PageControl ID="tabsMain" runat="server" Width="100%">
        <TabPages>
            <X:TabPage Text="Algemeen" Name="Generic">
                <controls>
					<D:Panel ID="pnlGeneral" GroupingText="General" runat="server">
						<table class="dataformV2">
							<tr>
								<td class="label">
									<D:LabelEntityFieldInfo runat="server" id="lblName">Friendly Name</D:LabelEntityFieldInfo>
								</td>
								<td class="control">
									<D:TextboxString runat="server" ID="tbName" IsRequired="true" />
								</td>
								<td class="label">
									<D:LabelEntityFieldInfo runat="server" ID="lblType">Type</D:LabelEntityFieldInfo>
								</td>
								<td class="control">
									<X:ComboBoxInt runat="server" ID="cbType" UseDataBinding="true" IsRequired="true"></X:ComboBoxInt>
								</td>
							</tr>

							<tr>
								<td class="label">
									<D:LabelEntityFieldInfo runat="server" id="lblLabel">Label</D:LabelEntityFieldInfo>
								</td>
								<td class="control">
									<D:TextboxString runat="server" ID="tbLabel" IsRequired="true" />
								</td>
								<td class="label">
									<D:LabelEntityFieldInfo runat="server" id="lblMinimumOrderAmount">Minimum order amount</D:LabelEntityFieldInfo>
								</td>
								<td class="control">
									<D:TextBoxDecimal ID="tbMinimumOrderAmount" runat="server"></D:TextBoxDecimal>
								</td>
							</tr>

							<tr>
								<td class="label">
									<D:LabelEntityFieldInfo runat="server" id="lblCoversType">Ask for Covers</D:LabelEntityFieldInfo>
								</td>
								<td class="control">
									<X:ComboBoxInt runat="server" ID="cbCoversType" UseDataBinding="true" DisplayEmptyItem="false" IsRequired="true"></X:ComboBoxInt>
								</td>
								<td class="label">
									<D:LabelEntityFieldInfo runat="server" id="lblMaxCovers">Maximum covers amount</D:LabelEntityFieldInfo>
								</td>
								<td class="control">
									<D:TextBoxInt ID="tbMaxCovers" runat="server"></D:TextBoxInt>
								</td>
							</tr>
						</table>
					</D:Panel>

					<D:PlaceHolder ID="plhEdit" runat="server">
						<table class="dataformV2">
							<tr>
								<td class="label">
									<D:LabelEntityFieldInfo runat="server" id="lblDescription">Description</D:LabelEntityFieldInfo>
								</td>
								<td class="control">
									<D:TextBoxMultiLine ID="tbDescription" runat="server" Rows="3" UseDataBinding="true" ClientIDMode="Static"></D:TextBoxMultiLine>
								</td>
								<td class="label"></td>
								<td class="control"></td>
							</tr>

							<tr>
								<td class="label">
									<D:LabelEntityFieldInfo runat="server" id="lblActive">Active</D:LabelEntityFieldInfo>
								</td>
								<td class="control">
									<D:CheckBox runat="server" ID="cbActive" />
								</td>
								
							</tr>
						</table>
					</D:placeholder>

					<D:Panel ID="pnlRoomService" runat="server" GroupingText="Room service">
						<table class="dataformV2">
							<tr>
								<td class="label">
									<D:LabelEntityFieldInfo runat="server" id="lblAskEntryPermission">Ask Entry Permission</D:LabelEntityFieldInfo>
								</td>
								<td class="control">
									<D:CheckBox runat="server" ID="cbAskEntryPermission" />
								</td>

								<td class="label"></td>
								<td class="control"></td>
							</tr>
						</table>
					</D:Panel>

					<D:Panel ID="pnlServiceCharge" runat="server" GroupingText="Service Charge">
						<table class="dataformV2">
							<tr>
								<td class="label">
									<D:LabelEntityFieldInfo runat="server" id="lblServiceCharge">Service charge</D:LabelEntityFieldInfo>
								</td>
								<td class="control">
									<D:TextBoxDecimal ID="tbServiceCharge" AllowNegative="false" MinimalValue="0" runat="server"></D:TextBoxDecimal>
								</td>

								<td class="label">
									<D:LabelEntityFieldInfo runat="server" id="lblServiceChargeType">Service charge type</D:LabelEntityFieldInfo>
								</td>
								<td class="control">
									<X:ComboBoxInt runat="server" ID="cbServiceChargeType" DisplayEmptyItem="false" UseDataBinding="true"></X:ComboBoxInt>
								</td>
							</tr>

							<tr>
								<td class="label">
									<D:LabelEntityFieldInfo ID="lblMinimumAmountForFreeServiceCharge" UseDataBinding="false" runat="server">Minimum amount for free service charge</D:LabelEntityFieldInfo>
								</td>
								<td class="control">
									<D:TextBoxDecimal ID="tbMinimumAmountForFreeServiceCharge" runat="server"></D:TextBoxDecimal>
								</td>

								<td class="label">
									<D:LabelEntityFieldInfo runat="server" id="LabelEntityFieldInfo1">Service charge Product</D:LabelEntityFieldInfo>
								</td>
								<td class="control link-list">
									<a href="<%= ResolveUrl("~/Catalog/Product.aspx") %>?id=<%= this.DataSourceAsServiceMethodEntity.OutletEntity.ServiceChargeProductId %>" target="_new">
										#<%= this.DataSourceAsServiceMethodEntity.OutletEntity.ServiceChargeProductId %>
									</a>
								</td>
							</tr>
						</table>
					</D:Panel>

					<D:Panel ID="pnlDeliveryCharge" runat="server" GroupingText="Delivery Charge">
						<table class="dataformV2">
							<tr>
								<td class="label">
									<D:LabelEntityFieldInfo ID="lblFreeDeliveryAmount" UseDataBinding="false" runat="server">Minimum amount for free delivery</D:LabelEntityFieldInfo>
								</td>
								<td class="control">
									<D:TextBoxDecimal ID="tbMinimumAmountForFreeDelivery" AllowNegative="false"  runat="server"></D:TextBoxDecimal>
								</td>

								<td class="label">
									<D:LabelEntityFieldInfo runat="server" id="lblTippingProductId">Delivery charge Product</D:LabelEntityFieldInfo>
								</td>
								<td class="control link-list">
									<a href="<%= ResolveUrl("~/Catalog/Product.aspx") %>?id=<%= this.DataSourceAsServiceMethodEntity.OutletEntity.DeliveryChargeProductId %>" target="_new">
										#<%= this.DataSourceAsServiceMethodEntity.OutletEntity.DeliveryChargeProductId %>
									</a>
								</td>
							</tr>
						</table>
					</D:Panel>
					
                    <D:Panel ID="pnlOrderCompleteMessage" runat="server" GroupingText="Order Complete Messages">
                        <table class="dataformV2">
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" ID="lblOrderCompleteNotificationMethod">Order complete message</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <X:ComboBoxEnum runat="server" ID="cbOrderCompleteNotificationMethod" Type="Obymobi.Enums.OrderNotificationMethod, Obymobi" Width="150" />
                                </td>
                                <td class="label">
                                    
                                </td>
                                <td class="control">
                                    
                                </td>
                            </tr>
							<tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" ID="lblOrderCompleteMailSubject">Email Subject</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:TextBoxString runat="server" ID="tbOrderCompleteMailSubject" ClientIDMode="Static" />
                                </td>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" ID="lblOrderCompleteSmsMessage">SMS Message</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control" rowspan="3">
                                    <D:TextBoxMultiLine runat="server" ID="tbOrderCompleteTextMessage" MaxLength="160" Rows="3" onkeyup="LimitCharacters();" ClientIDMode="Static"/><br/>
                                    <D:Label ID="lblCount" runat="server" LocalizeText="False" CssClass="small">characters remaining: 160</D:Label>
                                </td>
                            </tr>
							<tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" ID="lblOrderCompleteMailMessage">Email Message</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:TextBoxMultiLine runat="server" ID="tbOrderCompleteMailMessage" Rows="10" ClientIDMode="Static" />
                                </td>
                                
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:Label runat="server" ID="lblLegend">Message Template Legenda</D:Label>
                                </td>
                                <td class="control">
                                    <table style="width:100%">
                                        <tr><td style="width: 165px;">[[ORDERNUMBER]]</td><td>Displays the order number.</td></tr>
                                        <tr><td>[[SELLERNAME]]</td><td>Displays the name of the seller.</td></tr>
                                    </table>
                                </td>
                            </tr>
							<tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" ID="lblOrderMessageTemplate">Template</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:DropDownList runat="server" ClientIDMode="Static" ID="cbTemplate" onchange="setTemplate(this)">
                                        <asp:ListItem Text="Use a template..." Value="0" Selected="True" />
                                        <asp:ListItem Text="Collection" Value="1" />
                                        <asp:ListItem Text="Delivery" Value="2" />
                                    </D:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </D:Panel>

					<D:Panel ID="pnlDeliverypointgroups" runat="server" GroupingText="Deliverypointgroups">
						<table class="dataformV2">
							<tr>
								<td class="label">
									<D:Label runat="server" ID="lblDeliverypointgroups"></D:Label>
								</td>
								<td class="control">                                    
									<X:ListBox runat="server" ID="lbDeliverypointgroups" SelectionMode="CheckColumn" ClientInstanceName="lbDeliverypointgroups" TextField="Name" ValueField="DeliverypointgroupId"></X:ListBox>
								</td>
								<td class="label"></td>
								<td class="control"></td>
							</tr>
						</table>
					</D:Panel>
				</controls>
            </X:TabPage>
        </TabPages>
    </X:PageControl>
<style>
    .small {
        font-size: x-small;
    }
</style>
<script type="text/javascript">
    var tbText = document.getElementById('<%=tbOrderCompleteTextMessage.ClientID %>');
    tbText.maxLength = 160;
    
    var countLabel = document.getElementById('<%=lblCount.ClientID %>');

    function LimitCharacters() {
        var chars = tbText.value.length;
        var labelString = countLabel.innerHTML.split(":");

        countLabel.innerHTML = labelString[0] + ": " + (tbText.maxLength - chars);
    }

    function setTemplate(ddl) {
        if (ddl.value === "0") {
            return;
        }

        let textMessageField = document.getElementById("tbOrderCompleteTextMessage");
        let mailSubjectField = document.getElementById("tbOrderCompleteMailSubject");
        let mailMessageField = document.getElementById("tbOrderCompleteMailMessage");

        switch (ddl.value) {
        case '2':
            setDeliveryTemplate(textMessageField, mailSubjectField, mailMessageField);
            break;
        case '1':
        default:
            setPickupTemplate(textMessageField, mailSubjectField, mailMessageField);
            break;
        }

        ddl.selectedIndex = 0;
        LimitCharacters();
    }

    function setDeliveryTemplate(textMessageObj, mailSubjectObj, mailMessageObj) {
		textMessageObj.value = 'Your order ([[ORDERNUMBER]]) with [[SELLERNAME]] is ready for delivery';

	    mailSubjectObj.value = 'Your order ([[ORDERNUMBER]]) with [[SELLERNAME]] is ready for delivery';
	    mailMessageObj.value = 'Your order [[ORDERNUMBER]] has been completed and is ready for delivery.';
    }
    function setPickupTemplate(textMessageObj, mailSubjectObj, mailMessageObj) {
		textMessageObj.value = 'Your order ([[ORDERNUMBER]]) with [[SELLERNAME]] is ready for collection';

	    mailSubjectObj.value = 'Your order ([[ORDERNUMBER]]) with [[SELLERNAME]] is ready for collection';
	    mailMessageObj.value = 'Your order [[ORDERNUMBER]] at [[SELLERNAME]] is complete.';
    }

    LimitCharacters();
</script>
</asp:Content>
