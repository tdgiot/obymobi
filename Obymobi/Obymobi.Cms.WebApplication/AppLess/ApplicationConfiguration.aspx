<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.AppLess.ApplicationConfiguration" ValidateRequest="false" Codebehind="ApplicationConfiguration.aspx.cs" %>
<%@ Reference VirtualPath="~/AppLess/SubPanels/LandingPageCollection.ascx" %>
<%@ Reference VirtualPath="~/AppLess/SubPanels/ThemeCollection.ascx" %>
<%@ Reference VirtualPath="~/AppLess/SubPanels/NavigationMenuCollection.ascx" %>
<%@ Reference VirtualPath="~/AppLess/SubPanels/WidgetCollection.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Generic" Name="Generic">
				<Controls>
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblName">Name</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbName" runat="server" IsRequired="true" MaxLength="255"></D:TextBoxString>
							</td>
                            
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblExternalId">External Id</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbExternalId" runat="server" IsRequired="true" Enabled="False"></D:TextBoxString>
							</td>
						</tr>
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblDescription">Description</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbDescription" runat="server" IsRequired="false" MaxLength="1024"></D:TextBoxString>
							</td>
							<td class="label"></td>
                            <td class="control"></td>
						</tr>
						<tr>
							<td class="label">
								<D:Label runat="server" id="lblLandingPageId">Default page</D:Label>
							</td>
							<td class="control">
								<X:ComboBoxInt runat="server" ID="cbLandingPageId" CssClass="input" TextField="Name" ValueField="LandingPageId" DisplayEmptyItem="false" IncrementalFilteringMode="Contains" IncrementalFilteringDelay="1000" />
							</td>
                            <td class="label"></td>
                            <td class="control"></td>
						</tr>
						<tr>
							<td class="label">
								<D:Label ID="lblLanguage" runat="server" Text="Language" />
							</td>
							<td class="control">
								<X:ComboBox runat="server" ID="ddlCultureCode" TextField="NameAndCultureCode" ValueField="Code"></X:ComboBox>
							</td>
						</tr>
					</table>
					<D:Panel ID="pnlFeatures" GroupingText="Features" LocalizeText="false" runat="server">
						<table class="dataformV2">
							<tr>
								<td class="label">
									<D:LabelEntityFieldInfo ID="lblpwaDisabled" runat="server">Progressive Web App (PWA)</D:LabelEntityFieldInfo>
                                </td>
								<td class="control">
                                    <input type="checkbox" runat="server" id="pwaDisabled" />
                                </td>
                                <td class="label"></td>
                                <td class="control"></td>
                            </tr>
							<tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo ID="lblEnableCookiewall" runat="server">Cookie Wall</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:CheckBox runat="server" ID="cbEnableCookiewall"/>
                                </td>
                                <td class="label"></td>
                                <td class="control"></td>
                            </tr>
                            <tr>
                                <td class="label">
									<D:LabelEntityFieldInfo ID="lblProductSwipingEnabled" runat="server">Product Swiping</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
									<D:CheckBox ID="cbProductSwipingEnabled" runat="server"/>
                                </td>
                                <td class="label"></td>
                                <td class="control"></td>
                            </tr>
                        </table>
                    </D:Panel>
                    <D:Panel ID="pnlAnalytics" GroupingText="Analytics" LocalizeText="false" runat="server">
						<D:PlaceHolder ID="plhCraveAnalytics" runat="server">
							<table class="dataformV2">
								<tr>
									<td class="label">
										<D:Label id="lblAnalyticsEnvironment" runat="server">Crave Analytics environment</D:Label>
									</td>
									<td class="control">
										<X:ComboBoxInt ID="cbAnalyticsEnvironment" UseDataBinding="True" DisplayEmptyItem="false" IsRequired="true" runat="server"></X:ComboBoxInt>
									</td>

									<td class="label"></td>
									<td class="control"></td>
								</tr>
							</table>
						</D:placeholder>

						<table class="dataformV2">
							<tr>
								<td class="label">
									<D:Label id="lblGoogleAnalyticsId" runat="server">Google Analytics id</D:Label>
								</td>
								<td class="control">
									<D:TextBoxString ID="tbGoogleAnalyticsId" IsRequired="false" runat="server"></D:TextBoxString>
								</td>

								<td class="label"></td>
								<td class="control"></td>
							</tr>

							<tr>
								<td class="label">
									<D:Label id="lblGoogleTagManagerId" runat="server">Google Tag Manager id</D:Label>
								</td>
								<td class="control">
									<D:TextBoxString ID="tbGoogleTagManagerId" IsRequired="false" runat="server"></D:TextBoxString>
								</td>

								<td class="label"></td>
								<td class="control"></td>
							</tr>
						</table>
					</D:Panel>
				</Controls>
			</X:TabPage>
		</TabPages>
	</X:PageControl>
</asp:Content>

