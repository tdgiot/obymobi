﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.AppLess.CheckoutMethod" Codebehind="CheckoutMethod.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">    
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<controls>
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblName">Friendly Name</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" ID="lblCheckoutType">Type</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<X:ComboBoxInt runat="server" ID="cbCheckoutType" UseDataBinding="true" IsRequired="true"></X:ComboBoxInt>
							</td>
						</tr>
                    </table>
					<D:PlaceHolder ID="plhEdit" runat="server">
						<table class="dataformV2">
							<tr>
								<td class="label">
									<D:LabelEntityFieldInfo runat="server" ID="lblActive">Active</D:LabelEntityFieldInfo>
								</td>
								<td class="control">
									<D:CheckBox runat="server" ID="cbActive" />
								</td>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblPaymentIntegrationConfigurationId" Visible="false">Payment Integration Configuration</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <X:ComboBoxInt runat="server" ID="cbPaymentIntegrationConfigurationId" CssClass="input" TextField="DisplayName" ValueField="PaymentIntegrationConfigurationId" IncrementalFilteringMode="Contains" IncrementalFilteringDelay="1000" Visible="False"></X:ComboBoxInt>
                                </td>
                            </tr>
							<tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" ID="lblLabel">Label</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:TextBoxString ID="tbLabel" runat="server"></D:TextBoxString>
                                </td>
                                <td class="label">
									<D:LabelEntityFieldInfo ID="lblTermsAndConditionsRequired" runat="server" >Require terms and conditions</D:LabelEntityFieldInfo>
								</td>
								<td class="control">
									<D:CheckBox ID="cbTermsAndConditionsRequired" UseDataBinding="true" runat="server" />
								</td>
							</tr>
							<tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" ID="lblDescription">Description</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:TextBoxMultiLine ID="tbDescription" runat="server" Rows="3" UseDataBinding="true" ClientIDMode="Static"></D:TextBoxMultiLine>
                                </td>

                                <td class="label">
									<D:LabelEntityFieldInfo ID="lblTermsAndConditionsLabel" runat="server">Terms and conditions label</D:LabelEntityFieldInfo>
                                </td>
								<td class="control">
									<D:PlaceHolder ID="plhTermsAndConditionsLabel" runat="server">
										<D:TextBoxMultiLine ID="tbTermsAndConditionsLabel" Rows="5" runat="server"></D:TextBoxMultiLine>
										<span style="display: inline-block; margin-left: 0; font-size: smaller; font-weight: normal">
											<a href="https://guides.github.com/features/mastering-markdown/" style="display: inline" target="_blank">Markdown</a>-markup can be used. By default links will open in the embedded browser, this can changed by post-fixing the url with: <strong>#internal</strong> to open in a the same page, <strong>#external</strong> to open in a new browser tab.
										</span>
									</D:PlaceHolder>
								</td>
                            </tr>
						</table>
					</D:PlaceHolder>
					<D:Panel ID="pnlChargeConfirmation" runat="server" GroupingText="Charge Confirmation">
						<table class="dataformV2">
							<tr>
								<td class="label">
									<D:LabelEntityFieldInfo runat="server" ID="lblConfirmationActive">Confirmation Required</D:LabelEntityFieldInfo>
								</td>
								<td class="control">
									<D:CheckBox runat="server" ID="cbConfirmationActive" />
								</td>
								<td class="label">
									<D:LabelEntityFieldInfo runat="server" ID="lblConfirmationDescription">Confirmation Description</D:LabelEntityFieldInfo>
								</td>
								<td class="control">
									<D:TextBoxString ID="tbConfirmationDescription" runat="server"></D:TextBoxString>
								</td>
							</tr>
						</table>
					</D:Panel>
                    <D:Panel ID="pnlOrderUpdateMessage" runat="server" GroupingText="Order Update Messages">
                        <table class="dataformV2">
                            <D:PlaceHolder runat="server" ID="plhOrderConfirmationNotification" Visible="True">
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" ID="lblOrderConfirmationNotificationMethod">Order confirmation</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <X:ComboBoxEnum runat="server" ID="cbOrderConfirmationNotificationMethod" Type="Obymobi.Enums.OrderNotificationMethod, Obymobi" Width="150" />
                                </td>
                                <td class="label"></td>
                                <td class="control"></td>
                            </tr>
                            </D:PlaceHolder>
                            <D:PlaceHolder runat="server" ID="plhOrderPaymentNotifications" Visible="False">
                                <tr>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblOrderPaymentPendingNotificationMethod">Payment pending</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <X:ComboBoxEnum runat="server" ID="cbOrderPaymentPendingNotificationMethod" Type="Obymobi.Enums.OrderNotificationMethod, Obymobi" Width="150" />
                                    </td>
                                    <td class="label"></td>
                                    <td class="control"></td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblOrderPaymentFailedNotificationMethod">Payment failed</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <X:ComboBoxEnum runat="server" ID="cbOrderPaymentFailedNotificationMethod" Type="Obymobi.Enums.OrderNotificationMethod, Obymobi" Width="150" />
                                    </td>
                                    <td class="label"></td>
                                    <td class="control"></td>
                                </tr>
							    <tr>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" ID="lblOrderReceiptNotificationMethod">Payment completed (Receipt)</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <X:ComboBoxEnum runat="server" ID="cbOrderReceiptNotificationMethod" Type="Obymobi.Enums.OrderNotificationMethod, Obymobi" Width="150" />
                                    </td>
								    <td class="label"></td>
                                    <td class="control"></td>
                                </tr>
                            </D:PlaceHolder>
                        </table>
                    </D:Panel>
					<D:Panel ID="pnlDeliverypointgroups" runat="server" GroupingText="Deliverypointgroups">
						<table class="dataformV2">
							<tr>
								<td class="label">
									<D:Label runat="server" ID="lblDeliverypointgroups"></D:Label>
								</td>
								<td class="control">                                    
									<X:ListBox runat="server" ID="lbDeliverypointgroups" SelectionMode="CheckColumn" ClientInstanceName="lbDeliverypointgroups" TextField="Name" ValueField="DeliverypointgroupId"></X:ListBox>
								</td>
								<td class="label"></td>
								<td class="control"></td>
							</tr>
						</table>
					</D:Panel>
					<CC:FeatureTogglePanel ToggleType="CheckoutMethod_Cms_TestOverrides" runat="server">
						<D:Panel ID="pnlTestCheckoutMethodOverrides" runat="server" GroupingText="Testing">
						<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblCountryCode">Country</D:LabelEntityFieldInfo>
							</td>
							<td class="control">                                    
								<X:ComboBox runat="server" ID="ddlCountryCode" TextField="Name" ValueField="CodeAlpha3"></X:ComboBox>
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblCurrencyCode">Currency</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<X:ComboBox runat="server" ID="ddlCurrencyCode" TextField="Name" ValueField="Code"></X:ComboBox>
							</td>
						</tr>
						</table>
						</D:Panel>
					</CC:FeatureTogglePanel>
                </controls>
            </X:TabPage>
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>
