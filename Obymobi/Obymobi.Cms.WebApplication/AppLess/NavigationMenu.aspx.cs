﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using DevExpress.Utils;
using DevExpress.Web;
using DevExpress.Web.Internal;
using DevExpress.Web.ASPxTreeList;
using Obymobi.Cms.Logic.DataSources;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using DevExpress.Web.Data;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.ObymobiCms.AppLess
{
	public partial class NavigationMenu : Dionysos.Web.UI.PageLLBLGenEntity
    {
        #region Fields

        private NavigationMenuDataSource navigationMenuDataSource = new NavigationMenuDataSource();
        private ApplicationConfigurationDataSource applicationConfigurationDataSource = new ApplicationConfigurationDataSource();
        private NavigationMenuTreeListDataSource navigationMenuTreeListDataSource;

        #endregion

        #region Methods

        private void LoadUserControls()
        {
            this.btDeleteNavigationItemBottom.Click += BtDeleteNavigationMenuItem_Click;
        }

        private void CreateTreeList()
        {
            this.tlNavigationMenuItems.SettingsEditing.Mode = TreeListEditMode.Inline;
            this.tlNavigationMenuItems.SettingsBehavior.ColumnResizeMode = ColumnResizeMode.Disabled;
            this.tlNavigationMenuItems.SettingsBehavior.AllowSort = false;
            this.tlNavigationMenuItems.SettingsBehavior.AllowDragDrop = false;
            this.tlNavigationMenuItems.ClientInstanceName = this.TreelistId;
            this.tlNavigationMenuItems.AutoGenerateColumns = false;
            this.tlNavigationMenuItems.SettingsEditing.AllowNodeDragDrop = true;
            this.tlNavigationMenuItems.SettingsEditing.AllowRecursiveDelete = true;
            this.tlNavigationMenuItems.Settings.GridLines = GridLines.Both;
            this.tlNavigationMenuItems.KeyFieldName = "NavigationMenuItemId";
            this.tlNavigationMenuItems.ClientSideEvents.EndDragNode = this.EndDragNodeMethod;
            this.tlNavigationMenuItems.ClientSideEvents.NodeDblClick = this.NodeDblClickMethod;
            this.tlNavigationMenuItems.CustomErrorText += this.tlNavigationMenu_CustomErrorText;
            this.tlNavigationMenuItems.NodeUpdating += NodeUpdating;

            TreeListTextColumn nameColumn = new TreeListTextColumn();
            nameColumn.Caption = "Text";
            nameColumn.VisibleIndex = 0;
            nameColumn.FieldName = "Text";
            nameColumn.CellStyle.Wrap = DefaultBoolean.True;
            nameColumn.EditFormSettings.VisibleIndex = 0;
            this.tlNavigationMenuItems.Columns.Add(nameColumn);

            TreeListCommandColumn editCommandColumn = new TreeListCommandColumn();
            editCommandColumn.VisibleIndex = 1;
            editCommandColumn.Width = Unit.Pixel(60);
            editCommandColumn.ShowNewButtonInHeader = true;
            editCommandColumn.EditButton.Visible = true;
            editCommandColumn.NewButton.Visible = false;
            editCommandColumn.DeleteButton.Visible = true;
            editCommandColumn.ButtonType = ButtonType.Image;
            editCommandColumn.EditButton.Image.Url = this.ResolveUrl("~/images/icons/pencil.png");
            editCommandColumn.DeleteButton.Image.Url = this.ResolveUrl("~/images/icons/delete.png");
            editCommandColumn.CancelButton.Image.Url = this.ResolveUrl("~/images/icons/cross_grey.png");
            editCommandColumn.NewButton.Image.Url = this.ResolveUrl("~/images/icons/add2.png");
            editCommandColumn.UpdateButton.Image.Url = this.ResolveUrl("~/images/icons/disk.png");
            this.tlNavigationMenuItems.Columns.Add(editCommandColumn);

            TreeListHyperLinkColumn editPageColumn = new TreeListHyperLinkColumn();
            editPageColumn.Name = "EditCommand";
            editPageColumn.Caption = " ";
            editPageColumn.VisibleIndex = 2;
            editPageColumn.Width = Unit.Pixel(15);
            editPageColumn.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            editPageColumn.PropertiesHyperLink.ImageUrl = "~/images/icons/page_edit.png";
            editPageColumn.EditCellStyle.CssClass = "hidden";
            editPageColumn.FieldName = "NavigationMenuItemId";
            editPageColumn.EditFormSettings.VisibleIndex = 2;
            editPageColumn.EditFormCaptionStyle.CssClass = "hidden";
            this.tlNavigationMenuItems.Columns.Add(editPageColumn);

            this.tlNavigationMenuItems.HtmlDataCellPrepared += this.tlNavigationMenuItems_HtmlDataCellPrepared;
            this.tlNavigationMenuItems.CustomCallback += this.tlNavigationMenuItems_CustomCallback;

            this.InitDataSource();
        }

        private void InitDataSource()
        {
            if (this.DataSource != null)
            {
                this.navigationMenuTreeListDataSource = new NavigationMenuTreeListDataSource(this.DataSourceAsNavigationMenuEntity.NavigationMenuId);
                this.tlNavigationMenuItems.DataSource = this.navigationMenuTreeListDataSource;
            }
        }

        private void SetGui()
        {
            if (this.DataSourceAsNavigationMenuEntity.IsNew)
            {
                int applicationConfigurationId =  this.applicationConfigurationDataSource.GetFirstApplicationConfigurationEntity(CmsSessionHelper.CurrentCompanyId).ApplicationConfigurationId;
                this.navigationMenuDataSource.SetDefaultValuesOnNavigationMenu(this.DataSourceAsNavigationMenuEntity, applicationConfigurationId);
            }
            else
            {
                this.CreateTreeList();
                this.plhNavigationMenu.Visible = true;
            }

            this.cbWidgetLanguageSwitcher.DataBindEntityCollection<WidgetLanguageSwitcherEntity>(WidgetLanguageSwitcherFields.ApplicationConfigurationId == this.DataSourceAsNavigationMenuEntity.ApplicationConfigurationId, WidgetLanguageSwitcherFields.Name, WidgetLanguageSwitcherFields.Name, WidgetLanguageSwitcherFields.ParentCompanyId);
            this.RenderLanguageSwitcherWidget();
        }

        private void RenderLanguageSwitcherWidget()
        {
            NavigationMenuWidgetEntity languageSwitcherNavigationWidget = this.GetLanguageSwitcherFromDataSource();
            if (languageSwitcherNavigationWidget != null)
            {
                this.cbWidgetLanguageSwitcher.Value = languageSwitcherNavigationWidget.WidgetId;
            }
        }

        private void SaveLanguageSwitcherWidget()
        {
            NavigationMenuWidgetEntity currentLanguageSwitcher = this.GetLanguageSwitcherFromDataSource();
            NavigationMenuWidgetEntity newLanguageSwitcher = this.GetLanguageSwitcherFromPage();

            if (NavigationMenu.IsNewLanguageSwitcherSelected(currentLanguageSwitcher, newLanguageSwitcher))
            {
                newLanguageSwitcher.Save();
            }
            else if (NavigationMenu.IsLanguageSwitcherDeselected(currentLanguageSwitcher, newLanguageSwitcher))
            {
                currentLanguageSwitcher.Delete();
            }
            else if (NavigationMenu.IsLanguageSwitcherUpdated(currentLanguageSwitcher, newLanguageSwitcher))
            {
                currentLanguageSwitcher.WidgetId = newLanguageSwitcher.WidgetId;
                currentLanguageSwitcher.Save();
            }
        }

        private static bool IsNewLanguageSwitcherSelected(NavigationMenuWidgetEntity oldNavigationMenuWidget, NavigationMenuWidgetEntity newNavigationMenuWidget)
        {
            return oldNavigationMenuWidget == null && newNavigationMenuWidget != null;
        }

        private static bool IsLanguageSwitcherDeselected(NavigationMenuWidgetEntity oldNavigationMenuWidget, NavigationMenuWidgetEntity newNavigationMenuWidget)
        {
            return oldNavigationMenuWidget != null && newNavigationMenuWidget == null;
        }

        private static bool IsLanguageSwitcherUpdated(NavigationMenuWidgetEntity oldNavigationMenuWidget, NavigationMenuWidgetEntity newNavigationMenuWidget)
        {
            return oldNavigationMenuWidget != null && newNavigationMenuWidget != null && oldNavigationMenuWidget.WidgetId != newNavigationMenuWidget.WidgetId;
        }

        private NavigationMenuWidgetEntity GetLanguageSwitcherFromDataSource()
        {
            return this.DataSourceAsNavigationMenuEntity.NavigationMenuWidgetCollection.FirstOrDefault(x => x.WidgetEntity.Type == ApplessWidgetType.LanguageSwitcher);
        }

        private NavigationMenuWidgetEntity GetLanguageSwitcherFromPage()
        {
            if (this.cbWidgetLanguageSwitcher.ValidId <= -1)
                return null;

            return new NavigationMenuWidgetEntity
            {
                NavigationMenuId = this.DataSourceAsNavigationMenuEntity.NavigationMenuId,
                WidgetId = this.cbWidgetLanguageSwitcher.ValidId,
                ParentCompanyId = this.DataSourceAsNavigationMenuEntity.CompanyId,
                SortOrder = this.DataSourceAsNavigationMenuEntity.NavigationMenuWidgetCollection.Any() ? (this.DataSourceAsNavigationMenuEntity.NavigationMenuWidgetCollection.Max(x => x.SortOrder) + 1) : 1
            };
        }

        #endregion

        #region Properties

		public NavigationMenuEntity DataSourceAsNavigationMenuEntity => this.DataSource as NavigationMenuEntity;

        private string TreelistId => $"{this.ID}_treelist";

        #endregion

        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "NavigationMenu";
            this.LoadUserControls();

            this.DataSourceLoaded += this.NavigationMenu_DataSourceLoaded;
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.tlNavigationMenuItems.DataBind();
        }   

        private void NavigationMenu_DataSourceLoaded(object sender)
	    {
	        this.SetGui();
	    }

        public override bool Save()
        {
            if (this.PageMode == Dionysos.Web.PageMode.Add)
                this.DataSourceAsNavigationMenuEntity.CompanyId = CmsSessionHelper.CurrentCompanyId;

            bool saved = base.Save();
            if (saved)
            {
                this.SaveLanguageSwitcherWidget();
            }

            return saved;
        }

        private void tlNavigationMenu_CustomErrorText(object sender, TreeListCustomErrorTextEventArgs e)
        {
            if (e.Exception.InnerException is ObymobiException obymobiException)
            {
                e.ErrorText = obymobiException.AdditionalMessage;
            }
            else if (e.Exception.InnerException is Exception exception)
            {
                e.ErrorText = exception.Message;
            }
        }

        private void tlNavigationMenuItems_HtmlDataCellPrepared(object sender, TreeListHtmlDataCellEventArgs e)
        {
            if (e.Column.Name == "EditCommand" && e.NodeKey != null)
            {
                string navigateUrl = $"~/AppLess/NavigationMenuItem.aspx?id={e.NodeKey}";

                foreach (Control item in e.Cell.Controls)
                {
                    if (item is HyperLinkDisplayControl link)
                    {
                        link.NavigateUrl = navigateUrl;
                        break;
                    }
                }
            }
        }

        private void tlNavigationMenuItems_CustomCallback(object sender, TreeListCustomCallbackEventArgs e)
        {
            // Node dragged callback
            string[] nodes = e.Argument.Split(new char[] { ':' });
            if (nodes.Length <= 1)
            {
                return;
            }

            if (!int.TryParse(nodes[0], out int draggedNavigationMenuItemId) || !int.TryParse(nodes[1], out int draggedUponNavigationMenuItemId))
            {
                return;
            }

            this.navigationMenuTreeListDataSource.Manager.DraggedToSort(draggedNavigationMenuItemId, draggedUponNavigationMenuItemId);
            this.tlNavigationMenuItems.DataBind();
        }

        private void BtDeleteNavigationMenuItem_Click(object sender, EventArgs e)
        {
            this.DeleteSelectedNavigationMenuItems();
        }

        private void NodeUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {
            int.TryParse(e.NewValues[nameof(NavigationMenuItemFields.NavigationMenuItemId)].ToString(), out int navigationMenuItemId);
            string updatedText = e.NewValues[nameof(NavigationMenuItemFields.Text)].ToString();

            NavigationMenuItemEntity navigationMenuItemEntity = new NavigationMenuItemEntity(navigationMenuItemId);
            if (!navigationMenuItemEntity.IsNew && navigationMenuItemEntity.Text != updatedText)
            {
                navigationMenuItemEntity.Text = updatedText;
                bool isSaved = navigationMenuItemEntity.Save();

                if (isSaved)
                {
                    CompanyEntity companyEntity = new CompanyEntity(navigationMenuItemEntity.ParentCompanyId);
                    CustomTextHelper.UpdateCustomTexts(navigationMenuItemEntity, companyEntity);
                }
            }
        }

        private void DeleteSelectedNavigationMenuItems()
        {
            List<TreeListNode> selectedNodes = this.GetSelectedNodes();
            if (selectedNodes.Count <= 0)
            {
                this.MultiValidatorDefault.AddError("No item has been selected to be deleted.");
                this.Validate();
            }
            else
            {
                this.DeleteItems(selectedNodes);

                this.InitDataSource();
                this.tlNavigationMenuItems.DataBind();

                this.AddInformatorInfo("The selected item(s) has been deleted.");
                this.Validate();
            }
        }

        private void DeleteItems(List<TreeListNode> nodes)
        {
            foreach (TreeListNode node in nodes)
            {
                if (int.TryParse(node.Key, out int navigationMenuItemId))
                {
                    this.navigationMenuTreeListDataSource.Manager.Delete(navigationMenuItemId);
                }
            }
        }

        private List<TreeListNode> GetSelectedNodes()
        {
            List<TreeListNode> selectedNodes = new List<TreeListNode>();

            foreach (TreeListNode node in this.tlNavigationMenuItems.Nodes)
            {
                if (node.Selected)
                {
                    selectedNodes.Add(node);
                }
            }

            return selectedNodes;
        }

        private string EndDragNodeMethod
        {
            get
            {
                return @" function(s, e) {                                                                
                                e.cancel = true;
		                        var key = s.GetNodeKeyByRow(e.targetElement);
		                        " + this.TreelistId + @".PerformCustomCallback(e.nodeKey + ':' + key);	                            
                            }";
            }
        }

        private string NodeDblClickMethod
        {
            get { return @" function(s, e) {
	                            " + this.TreelistId + @".StartEdit(e.nodeKey);
		                        e.cancel = true;	                                                                    
                            }"; }
        }

        #endregion

    }
}
