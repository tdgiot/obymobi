﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using Crave.AuditService.AuditEventModelBuilder.Models;
using Crave.AuditService.Client.Interfaces;
using Crave.AuditService.NetFrameworkAuditEventModelDecorator.Interfaces;
using Dionysos.Data.Extensions;
using Dionysos.Web;
using Dionysos.Web.UI;
using Obymobi.Cms.Logic.Extensions;
using Obymobi.Cms.Logic.HelperClasses;
using Obymobi.Cms.Logic.Models;
using Obymobi.Cms.Logic.UseCases;
using Obymobi.Cms.WebApplication.AppLess;
using Obymobi.Cms.WebApplication.Old_App_Code;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Extensions;
using Obymobi.Logic.HelperClasses;
using Obymobi.ObymobiCms.MasterPages;
using Resources;

namespace Obymobi.ObymobiCms.AppLess
{
    public partial class Outlet : PageLLBLGenEntity
    {
        private const int DEFAULT_WAIT_TIME = 60;
        private const int DEFAULT_VAT_TARIFF_ID = 3;

        private bool exportInProgress = false;

        private readonly IAuditEventModelDecorator auditEventModelDecorator;
        private readonly IAuditServiceClient auditServiceClient;

        private Generic.UserControls.CustomTextCollection translationsTab;

        public Outlet(IAuditEventModelDecorator auditEventModelDecorator, IAuditServiceClient auditServiceClient)
        {
            this.auditEventModelDecorator = auditEventModelDecorator;
            this.auditServiceClient = auditServiceClient;
        }

        #region Methods

        private void LoadUserControls()
        {
            if (this.PageMode == PageMode.Add)
            {
                this.plhOperationalState.Visible = false;
                this.pnlCustomerDetails.Visible = false;
                this.pnlTipping.Visible = false;
                this.pnlTaxPricing.Visible = false;
            }
            else if (this.PageMode == PageMode.View || this.PageMode == PageMode.Edit)
            {
                this.LoadTab("Service Methods", nameof(SubPanels.ServiceMethodCollection), "~/AppLess/SubPanels/ServiceMethodCollection.ascx", accessRole: Role.Supervisor);
                this.LoadTab("Checkout Methods", nameof(SubPanels.CheckoutMethodCollection), "~/AppLess/SubPanels/CheckoutMethodCollection.ascx", accessRole: Role.Crave);
                this.LoadTab("Business hours", nameof(Catalog.SubPanels.BusinesshourPanel), "~/Company/SubPanels/BusinesshourPanel.ascx", accessRole: Role.Supervisor);

                translationsTab = this.LoadTranslationsTab(accessRole: Role.Supervisor);
            }

            this.pnlOrderNotifications.Visible = CmsSessionHelper.CurrentRole >= Role.Administrator;
        }

        private void SetGui()
        {
            if (this.PageMode == PageMode.Add)
            {
                this.DataSourceAsOutletEntity.TaxDisclaimer = "This is not a VAT invoice. To request a VAT invoice, please contact the venue.";
                this.DataSourceAsOutletEntity.TaxNumberTitle = "VAT Number";
            }

            if (translationsTab != null && !CmsSessionHelper.CurrentUser.HasFeature(FeatureToggle.Covers))
            { translationsTab.ExcludeCustomTextTypes(CustomTextType.CoversDefaultOptionText, CustomTextType.CoversNumericPlaceholderText, CustomTextType.CoversOtherOptionText); }

            this.LoadOperationalState();
            this.RenderNotificationReceivers();
            this.ddlShowTaxBreakdown.DataBindEnum<TaxBreakdown>();
        }

        private void RenderNotificationReceivers()
        {
            this.tbReceiptReceiversEmail.Text = this.DataSourceAsOutletEntity?.OutletSellerInformationEntity?.ReceiptReceiversEmail.Replace(", ", "\n");
            this.tbReceiptReceiversSms.Text = this.DataSourceAsOutletEntity?.OutletSellerInformationEntity?.ReceiptReceiversSms.Replace(", ", "\n");
        }

        private void SetNotificationReceiversOnOutletSellerInformation(OutletSellerInformationEntity outletSellerInformationEntity)
        {
            outletSellerInformationEntity.ReceiptReceiversEmail = string.Join(", ", this.tbReceiptReceiversEmail.Text.Replace("\r", string.Empty).Split('\n'));
            outletSellerInformationEntity.ReceiptReceiversSms = string.Join(", ", this.tbReceiptReceiversSms.Text.Replace("\r", string.Empty).Split('\n'));
        }

        #endregion Methods

        #region Properties

        public OutletEntity DataSourceAsOutletEntity => this.DataSource as OutletEntity;

        #endregion Properties

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            this.DataSourceLoaded += Outlet_DataSourceLoaded;
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.DataSourceAsOutletEntity.CompanyEntity.PricesIncludeTaxes)
            {
                this.pnlTaxPricing.Visible = false;
            }

            if (Master is MasterPageEntity masterPage && masterPage.ToolBar.DeleteButton != null)
            {
                masterPage.ToolBar.DeleteButton.Visible = RoleRights.AllowedToDeleteOutlet(CmsSessionHelper.CurrentRole);
            }

            if (!this.DataSourceAsOutletEntity.IsNew &&
                !this.IsPostBack)
            {
                this.SetWarnings();
            }
        }

        private void SetWarnings()
        {
            if (this.DataSourceAsOutletEntity?.OutletOperationalStateEntity?.OrderIntakeDisabled ?? false)
            {
                this.AddInformator(InformatorType.Warning, "Guests will not be able to place any orders because ordering is disabled.");
            }

            if (CmsSessionHelper.CurrentRole >= Role.Crave)
            {
                if (this.DataSourceAsOutletEntity?.CheckoutMethodCollection == null ||
                    !this.DataSourceAsOutletEntity.CheckoutMethodCollection.Any())
                {
                    this.AddInformator(InformatorType.Warning, "No checkout methods configured.");
                }
                else
                {
                    foreach (CheckoutMethodEntity checkoutMethod in this.DataSourceAsOutletEntity.CheckoutMethodCollection.Where(s => !s.Active))
                    {
                        string url = Page.ResolveUrl($"{checkoutMethod.GetDefaultEntityEditPage()}?id={checkoutMethod.CheckoutMethodId}");

                        this.AddInformator(InformatorType.Information, $"Checkout method <a href=\"{url}\">{checkoutMethod.Name}</a> is not active.");
                    }
                }
            }

            if (this.DataSourceAsOutletEntity?.ServiceMethodCollection == null ||
                !this.DataSourceAsOutletEntity.ServiceMethodCollection.Any())
            {
                this.AddInformator(InformatorType.Warning, "No service methods configured.");
            }
            else
            {
                foreach (ServiceMethodEntity serviceMethod in this.DataSourceAsOutletEntity.ServiceMethodCollection.Where(s => !s.Active))
                {
                    string url = Page.ResolveUrl($"{serviceMethod.GetDefaultEntityEditPage()}?id={serviceMethod.ServiceMethodId}");

                    this.AddInformator(InformatorType.Information, $"Service method <a href=\"{url}\">{serviceMethod.Name}</a> is not active.");
                }
            }

            this.Validate();
        }

        private void Outlet_DataSourceLoaded(object sender)
        {
            if (this.DataSourceAsOutletEntity.IsNew)
            {
                this.DataSourceAsOutletEntity.CustomerDetailsNameRequired = true;
            }

            if (!this.DataSourceAsOutletEntity.OutletSellerInformationId.HasValue)
            {
                this.DataSourceAsOutletEntity.OutletSellerInformationEntity = new OutletSellerInformationEntity();
            }

            this.SetGui();
        }

        private void SaveOperationalState()
        {
            OutletOperationalStateEntity operationalState = this.DataSourceAsOutletEntity.OutletOperationalStateEntity;

            operationalState.OrderIntakeDisabled = this.cbOrderIntakeDisabled.Value;
            if (this.tbWaitTime.Value.HasValue)
            {
                operationalState.WaitTime = TimeSpan.FromMinutes(this.tbWaitTime.Value.Value);
            }

            this.DataSourceAsOutletEntity.Save(true);
        }

        private void LoadOperationalState()
        {
            OutletOperationalStateEntity operationalState = this.DataSourceAsOutletEntity?.OutletOperationalStateEntity;

            if (operationalState?.WaitTime != null)
            {
                this.tbWaitTime.Value = Convert.ToInt32(operationalState.WaitTime.Value.TotalMinutes);
            }

            this.cbOrderIntakeDisabled.Value = operationalState?.OrderIntakeDisabled ?? false;
        }

        private bool IsCustomerDetailsSectionActive => this.cbCustomerDetailsPhoneRequired.Value ||
                                                       this.cbCustomerDetailsEmailRequired.Value ||
                                                       this.cbCustomerDetailsNameRequired.Value;

        private bool IsTippingActive => this.tbTippingDefaultPercentage.Value.HasValue ||
                                        this.tbTippingMinimumPercentage.Value.HasValue ||
                                        this.tbTippingStepSize.Value.HasValue;

        public override bool Save()
        {
            if (this.PageMode == PageMode.Add)
            {
                this.DataSourceAsOutletEntity.CompanyId = CmsSessionHelper.CurrentCompanyId;

                TaxTariffEntity taxTariffEntity = GetOrCreateZeroPercentageTaxTariff();
                this.DataSourceAsOutletEntity.TippingProductId = this.GenerateTippingProduct(taxTariffEntity);
                this.DataSourceAsOutletEntity.ServiceChargeProductId = this.GenerateServiceChargeProduct(taxTariffEntity);
                this.DataSourceAsOutletEntity.DeliveryChargeProductId = this.GenerateDeliveryChargeProduct(taxTariffEntity);

                CheckoutMethodEntity freeOfChargeCheckoutMethod = this.CreateFreeOfChargeCheckoutMethod();
                this.DataSourceAsOutletEntity.CheckoutMethodCollection.Add(freeOfChargeCheckoutMethod);
            }

            this.DataSourceAsOutletEntity.CustomerDetailsSectionActive = this.IsCustomerDetailsSectionActive;
            this.DataSourceAsOutletEntity.TippingActive = this.IsTippingActive;

            this.SetNotificationReceiversOnOutletSellerInformation(this.DataSourceAsOutletEntity.OutletSellerInformationEntity);

            if (!this.ValidateBeforeSave())
            {
                return false;
            }

            //ToDo: CB: Thought I'd fixed this but throws a FileNotFoundException for one of the DLLs 
            /*if (DataSourceAsOutletEntity.OutletOperationalStateEntity.OrderIntakeDisabled != cbOrderIntakeDisabled.Value)
            {
                AuditEnablingDisablingOrdering();
            }*/

            bool success = base.Save(true);

            if (success)
            {
                this.SaveOperationalState();
                CustomTextHelper.UpdateCustomTexts(this.DataSource, this.DataSourceAsOutletEntity.CompanyEntity);

                TranslationHelper.SetDefaultTranslations(OutletDefaultTranslations.ResourceManager,
                    this.DataSourceAsOutletEntity,
                    this.DataSourceAsOutletEntity.CompanyEntity.GetDefaultCompanyCulture(),
                    this.DataSourceAsOutletEntity.CompanyEntity.GetCompanyCultures());
            }

            return success;
        }

        private void AuditEnablingDisablingOrdering()
        {
            try
            {
                AuditEventModel model = auditEventModelDecorator.BuildModelForDatabaseAudit(HttpContext.Current, "Outlet", DataSourceAsOutletEntity.OutletId.ToString(), this.DataSourceAsOutletEntity?.OutletOperationalStateEntity.OrderIntakeDisabled, cbOrderIntakeDisabled.Value);
                auditServiceClient.PostAuditEntry(model);
            }
            catch
            {
                if (CmsSessionHelper.CurrentRole < Role.Crave)
                {
                    return;
                }

                AddInformator(InformatorType.Warning, "Unable to notify Audit Service of Disable Ordering change.");
                Validate();
            }
        }

        private CheckoutMethodEntity CreateFreeOfChargeCheckoutMethod() => new CheckoutMethodEntity
        {
            Active = true,
            Name = "Free Of Charge",
            Label = "Free Of Charge",
            TermsAndConditionsRequired = false,
            CheckoutType = CheckoutType.FreeOfCharge,
            CompanyId = CmsSessionHelper.CurrentCompanyId
        };

        private int GenerateTippingProduct(TaxTariffEntity taxTariffEntity)
        {
            ProductEntity tippingProductEntity = new ProductEntity
            {
                CompanyId = this.DataSourceAsOutletEntity.CompanyId,
                Name = $"Tipping",
                SubType = (int)ProductSubType.SystemProduct,
                VattariffId = DEFAULT_VAT_TARIFF_ID,
                TaxTariffId = taxTariffEntity.TaxTariffId
            };

            tippingProductEntity.Save();

            return tippingProductEntity.ProductId;
        }

        private int GenerateServiceChargeProduct(TaxTariffEntity taxTariffEntity)
        {
            ProductEntity serviceChargeProductEntity = new ProductEntity
            {
                CompanyId = this.DataSourceAsOutletEntity.CompanyId,
                Name = $"Service charge",
                SubType = (int)ProductSubType.SystemProduct,
                VattariffId = DEFAULT_VAT_TARIFF_ID,
                TaxTariffId = taxTariffEntity.TaxTariffId
            };

            serviceChargeProductEntity.Save();

            return serviceChargeProductEntity.ProductId;
        }

        private int GenerateDeliveryChargeProduct(TaxTariffEntity taxTariffEntity)
        {
            ProductEntity deliveryChargeProductEntity = new ProductEntity
            {
                CompanyId = this.DataSourceAsOutletEntity.CompanyId,
                Name = $"Delivery charge",
                SubType = (int)ProductSubType.SystemProduct,
                VattariffId = DEFAULT_VAT_TARIFF_ID,
                TaxTariffId = taxTariffEntity.TaxTariffId
            };

            deliveryChargeProductEntity.Save();

            return deliveryChargeProductEntity.ProductId;
        }

        private bool ValidateBeforeSave()
        {
            if (this.tbTippingMinimumPercentage.Value > 0 && this.tbTippingDefaultPercentage.Value > 0 && this.tbTippingStepSize.Value <= 0)
            {
                this.MultiValidatorDefault.AddError("The tipping step size cannot be equals or less then zero based on the provided configuration");
            }

            if (tbWaitTime.Value == null || tbWaitTime.Value <= 0)
            {
                tbWaitTime.Value = DEFAULT_WAIT_TIME;
                this.AddInformator(InformatorType.Warning, "No wait time has configured so the default of {0} been applied", DEFAULT_WAIT_TIME);
            }

            this.Validate();

            return this.IsValid;
        }

        protected void ExportOptIns(object sender, EventArgs e)
        {
            if (this.exportInProgress)
            {
                return;
            }

            this.exportInProgress = true;

            OptInCollection optInCollection = new OptInCollection();
            optInCollection.GetMulti(OptInFields.OutletId == DataSourceAsOutletEntity.OutletId);

            DateTimeFormatInfo dateTimeFormatInfo = DataSourceAsOutletEntity.CompanyEntity.Culture.CultureInfo.DateTimeFormat;

            CreateOptInExportDataUseCase createOptInExport = new CreateOptInExportDataUseCase();
            ICollection<OptInRecord> optInRecords = createOptInExport.Execute(optInCollection, dateTimeFormatInfo);

            if (!optInRecords.Any())
            {
                this.exportInProgress = false;
                this.AddInformator(InformatorType.Information, "No marketing data found to export");
                this.Validate();
                return;
            }

            this.ExportToCsv($"opt-ins-{DateTime.Now.ToShortDateString()}", optInRecords);
            this.exportInProgress = false;
        }

        private TaxTariffEntity GetOrCreateZeroPercentageTaxTariff()
        {
            TaxTariffEntity taxTariffEntity = DataSourceAsOutletEntity.CompanyEntity.TaxTariffCollection.FirstOrDefault(taxTariff => taxTariff.Percentage == 0);
            if (taxTariffEntity == null)
            {
                taxTariffEntity = new TaxTariffEntity
                {
                    Name = "Tax free",
                    Percentage = 0,
                    CompanyEntity = DataSourceAsOutletEntity.CompanyEntity
                };

                taxTariffEntity.Save();
            }

            return taxTariffEntity;
        }

        #endregion Event Handlers
    }
}