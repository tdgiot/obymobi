﻿using System;
using Obymobi.Cms.Logic.DataSources;
using Obymobi.Data.EntityClasses;

namespace Obymobi.ObymobiCms.AppLess
{
    public partial class ApplicationConfigurations : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        #region Fields

        private ApplicationConfigurationDataSource applicationConfigurationDataSource = new ApplicationConfigurationDataSource();

        #endregion

        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            // As long as we don't show an overview of application configurations, don't bother loading the overview content.
            this.DataSourceLoad += (object sender, ref bool preventLoading) => preventLoading = true;

            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ApplicationConfigurationEntity applicationConfigurationEntity = this.applicationConfigurationDataSource.GetFirstApplicationConfigurationEntity(CmsSessionHelper.CurrentCompanyId);
            if (applicationConfigurationEntity == null)
            {
                this.Response.Redirect("~/AppLess/ApplicationConfiguration.aspx?mode=add&entity=ApplicationConfiguration&id=");
            }
            else
            {
                this.Response.Redirect(string.Format("~/AppLess/ApplicationConfiguration.aspx?id={0}", applicationConfigurationEntity.ApplicationConfigurationId));
            }
        }

        #endregion
    }
}