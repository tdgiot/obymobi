﻿using System;
using System.Linq;
using System.Web.UI;
using Dionysos;
using Dionysos.Web;
using Dionysos.Web.UI;
using Obymobi.Cms.Logic.HelperClasses;
using Obymobi.Cms.WebApplication.AppLess;
using Obymobi.Cms.WebApplication.AppLess.SubPanels.Widgets;
using Obymobi.Data;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic.Extensions;
using Obymobi.Logic.HelperClasses;
using Obymobi.ObymobiCms.AppLess.SubPanels;
using Obymobi.ObymobiCms.Generic.UserControls;
using Obymobi.ObymobiCms.MasterPages;
using Resources;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.AppLess
{
    public partial class Widget : PageLLBLGenEntity
    {
        #region Fields

        private UserControl widgetUserControl;
        private ActionPanel actionPanel;

        #endregion

        #region Properties

        private CustomTextCollection TranslationTabPage { get; set; }

        #endregion

        #region Methods

        private void LoadUserControls()
        {
            IEntity entity = this.GetEntityFromQueryString();

            this.RenderTranslationsTab(entity);
            this.RenderMediaTab(entity);
            this.RenderWidgetUserControl(entity);

            this.cbType.DataBindEnum<ApplessWidgetType>();
        }

        private void RenderMediaTab(IEntity entity)
        {
            if (entity is IMediaContainingEntity)
            {
                this.LoadMediaTab(supportMultipleImages: false, supportCultureSpecificImages: false);
            }
        }

        private void RenderTranslationsTab(IEntity entity)
        {
            if (entity is ICustomTextContainingEntity)
            {
                TranslationTabPage = this.tabsMain.AddTabPage("Translations", "CustomTextCollection", "~/Generic/UserControls/CustomTextCollection.ascx") as CustomTextCollection;
            }
        }

        private void RenderWidgetUserControl(IEntity entity)
        {
            this.plhType.Controls.Clear();

            if (entity is WidgetPageTitleEntity)
            {
                this.widgetUserControl = (UserControl)this.LoadControl("~/AppLess/SubPanels/Widgets/WidgetPageTitlePanel.ascx");
            }
            else if (entity is WidgetCarouselEntity)
            {
                this.widgetUserControl = (UserControl)this.LoadControl("~/AppLess/SubPanels/Widgets/WidgetCarouselPanel.ascx");
            }
            else if (entity is WidgetActionButtonEntity)
            {
                this.widgetUserControl = (UserControl)this.LoadControl("~/AppLess/SubPanels/Widgets/WidgetActionButtonPanel.ascx");
            }
            else if (entity is WidgetActionBannerEntity)
            {
                this.widgetUserControl = (UserControl)this.LoadControl("~/AppLess/SubPanels/Widgets/WidgetActionBannerPanel.ascx");
            }
            else if (entity is WidgetLanguageSwitcherEntity)
            {
                this.widgetUserControl = (UserControl)this.LoadControl("~/AppLess/SubPanels/Widgets/WidgetLanguageSwitcherPanel.ascx");
            }
            else if (entity is WidgetGroupEntity)
            {
                this.widgetUserControl = (UserControl)this.LoadControl("~/AppLess/SubPanels/Widgets/WidgetGroupPanel.ascx");
            }
            else if (entity is WidgetWaitTimeEntity)
            {
                this.widgetUserControl = (UserControl)this.LoadControl("~/AppLess/SubPanels/Widgets/WidgetWaitTimePanel.ascx");
            }
            else if (entity is WidgetOpeningTimeEntity)
            {
                this.widgetUserControl = (UserControl)this.LoadControl("~/AppLess/SubPanels/Widgets/WidgetOpeningTimePanel.ascx");
            }
            else if (entity is WidgetMarkdownEntity)
            {
                this.TranslationTabPage.FullWidth = true;

                this.widgetUserControl = (UserControl)this.LoadControl("~/AppLess/SubPanels/Widgets/WidgetMarkdownPanel.ascx");
            }

            if (this.widgetUserControl != null)
            {
                this.plhType.Controls.Add(this.widgetUserControl);
            }
        }

        private void SetGui()
        {
            if (this.DataSourceAsWidgetEntity.IsNew)
            {
                return;
            }

            if (this.widgetUserControl is IWidgetPanel widgetPanel)
            {
                widgetPanel.Load(this.DataSourceAsWidgetEntity);
            }

            if (this.DataSource is WidgetActionButtonEntity actionButton)
            {
                if (actionButton.CarouselItemCollection.Any())
                {
                    ((WidgetActionButtonPanel)this.widgetUserControl).HideIconTypeCombobox();
                    this.plhBasicWidgetDetails.Visible = false;
                }
            }

            if (IsWidgetActionable())
            {
                this.AddWidgetActionPanel();
            }
        }

        private void AddWidgetActionPanel()
        {
            this.pnlAction.Visible = true;

            this.actionPanel = (ActionPanel)this.LoadControl("~/AppLess/SubPanels/Actions/ActionPanel.ascx");
            this.actionPanel.LoadAction(this.DataSourceAsWidgetEntity.ActionEntity, this.DataSourceAsWidgetEntity.ApplicationConfigurationId);

            this.plhAction.Controls.Add(this.actionPanel);
        }

        private bool IsWidgetActionable()
        {
            IEntity entity = this.GetEntityFromQueryString();

            return entity is WidgetActionButtonEntity ||
                entity is WidgetActionBannerEntity ||
                entity is WidgetHeroEntity;
        }

        private bool IsWidgetLinkedToMultiplePages() => this.DataSourceAsWidgetEntity?.LandingPageWidgetCollection?.Count > 1;

        #endregion

        #region Properties

        public WidgetEntity DataSourceAsWidgetEntity => this.DataSource as WidgetEntity;

        public IEntity GetEntityFromQueryString()
        {
            return DataFactory.EntityFactory.GetEntity(this.GetEntityNameFromQueryString() + "Entity") as IEntity;
        }

        public string GetEntityNameFromQueryString()
        {
            return QueryStringHelper.HasValue("EntityName") ? QueryStringHelper.GetString("EntityName") : "Widget";
        }

        private void SetWarnings()
        {
            if (this.IsWidgetLinkedToMultiplePages())
            {
                this.AddInformator(InformatorType.Information, "Widget is linked to multiple pages, changes will reflect on all.");
            }

            if (this.DataSourceAsWidgetEntity is WidgetCarouselEntity carousel)
            {
                foreach (CarouselItemEntity carouselItem in carousel.CarouselItemCollection)
                {
                    if (carouselItem.MediaCollection.Count == 0)
                    {
                        this.AddInformator(InformatorType.Warning, $"Carousel item '{carouselItem.Name}' will not be shown in the application because it is missing an image.");
                    }
                }
            }

            this.Validate();
        }

        #endregion

        #region Event handlers

        public override bool Save()
        {
            if (actionPanel != null && !actionPanel.Save())
            {
                this.Validate();
                return false;
            }

            bool success = base.Save(true);
            if (success && this.DataSource is ICustomTextContainingEntity entity)
            {
                CompanyEntity companyEntity = new CompanyEntity(this.DataSourceAsWidgetEntity.ParentCompanyId);

                CustomTextHelper.UpdateCustomTexts(entity, companyEntity);
                TranslationHelper.SetDefaultTranslations(WidgetDefaultTranslations.ResourceManager,
                    entity,
                    companyEntity.GetDefaultCompanyCulture(),
                    companyEntity.GetCompanyCultures());
            }

            return success;
        }

        protected override void OnInit(EventArgs e)
        {
            this.EntityName = this.GetEntityNameFromQueryString();
            this.LoadUserControls();
            this.DataSourceLoaded += this.Widget_DataSourceLoaded;
            base.OnInit(e);
        }

        private void Widget_DataSourceLoaded(object sender)
        {
            this.SetGui();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.DataSourceAsWidgetEntity.IsNew &&
                !this.IsPostBack)
            {
                this.SetWarnings();
            }

            if (this.Master is MasterPageEntity masterPage)
            {
                masterPage.ToolBar.SaveButton.Visible = true;
                masterPage.ToolBar.SaveAndGoButton.Visible = true;
                masterPage.ToolBar.SaveAndNewButton.Visible = false;
                masterPage.ToolBar.DeleteButton.Visible = true;
            }
        }

        #endregion
    }
}
