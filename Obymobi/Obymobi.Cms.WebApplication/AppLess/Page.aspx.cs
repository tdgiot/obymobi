﻿using System;
using System.Linq;
using Dionysos.Web;
using Dionysos.Web.UI;
using Obymobi.Cms.Logic.DataSources;
using Obymobi.Cms.Logic.Extensions;
using Obymobi.Cms.Logic.Models;
using Obymobi.Cms.WebApplication.AppLess;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Obymobi.ObymobiCms.AppLess.SubPanels;

namespace Obymobi.ObymobiCms.AppLess
{
    public partial class Page : PageLLBLGenEntity
    {
        #region Fields

        private LandingPageDataSource landingPageDataSource = new LandingPageDataSource();
        private WidgetTreeListPanel widgetTreeListPanel;

        #endregion

        #region Methods

        private void LoadUserControls()
        {
            this.LoadMediaTab(supportMultipleImages: false, supportCultureSpecificImages: false);

            this.cbThemeId.DataBindEntityCollection<ThemeEntity>(ThemeFields.ParentCompanyId == CmsSessionHelper.CurrentCompanyId, ThemeFields.Name, ThemeFields.Name, ThemeFields.ParentCompanyId);
            this.cbNavigationMenuId.DataBindEntityCollection<NavigationMenuEntity>(NavigationMenuFields.CompanyId == CmsSessionHelper.CurrentCompanyId, NavigationMenuFields.Name, NavigationMenuFields.Name, NavigationMenuFields.CompanyId);
            this.cbOutletId.DataBindEntityCollection<OutletEntity>(OutletFields.CompanyId == CmsSessionHelper.CurrentCompanyId, OutletFields.Name, OutletFields.Name, OutletFields.CompanyId);

            this.LoadTranslationsTab();

        }

        private void SetGui()
        {
            if (this.DataSourceAsPageEntity.IsNew)
            {
                if (QueryStringHelper.HasValue(nameof(ApplicationConfigurationFields.ApplicationConfigurationId)))
                {
                    this.landingPageDataSource.SetDefaultValuesOnPage(this.DataSourceAsPageEntity, QueryStringHelper.GetInt(nameof(ApplicationConfigurationFields.ApplicationConfigurationId)));

                    this.cbNavigationMenuId.Value = this.DataSourceAsPageEntity.NavigationMenuId;
                    this.cbOutletId.Value = this.DataSourceAsPageEntity.OutletId;
                    this.cbThemeId.Value = this.DataSourceAsPageEntity.ThemeId > 0
                        ? this.DataSourceAsPageEntity.ThemeId
                        : (int?)null;
                }

                this.DataSourceAsPageEntity.HomeNavigationEnabled = true;
            }
            else
            {
                this.widgetTreeListPanel = this.LoadControl<WidgetTreeListPanel>("~/AppLess/Subpanels/WidgetTreeListPanel.ascx");
                this.widgetTreeListPanel.Load(this.DataSourceAsPageEntity.LandingPageId);

                this.plhWidgetControl.Controls.Add(this.widgetTreeListPanel);
                this.plhWidgetPanel.Visible = true;

                SetPageHrefLink();
            }

        }

        private void SetPageHrefLink()
        {
            if (WebEnvironmentHelper.CloudEnvironment == CloudEnvironment.Demo)
            {
                return;
            }

            string url = new AppLessPageUrl(WebEnvironmentHelper.CloudEnvironment, this.DataSourceAsPageEntity.ApplicationConfigurationEntity.ExternalId, this.DataSourceAsPageEntity.ExternalId);

            this.plhPageLink.Visible = true;
            this.hlApplessLink.NavigateUrl = url;
            this.hlApplessLink.Text = url;
        }

        private void SetWarnings()
        {
            if (this.DataSourceAsPageEntity.MediaCollection.Count == 0)
            {
                this.AddInformator(InformatorType.Information, "The page is missing a header logo.");
            }

            foreach (WidgetEntity widgetEntity in this.DataSourceAsPageEntity.LandingPageWidgetCollection.Select(w => w.WidgetEntity))
            {
                if (widgetEntity is WidgetHeroEntity hero && hero.MediaCollection.Count == 0)
                {
                    this.AddInformator(InformatorType.Warning, $"Widget '{hero.Name}' will not be shown in the application because no image is configured.");
                }
                else if (widgetEntity is WidgetCarouselEntity carousel)
                {
                    if (carousel.CarouselItemCollection.Count == 0)
                    {
                        this.AddInformator(InformatorType.Warning, $"Widget '{carousel.Name}' will not be shown in the application because no carousel items are configured.");
                    }
                    else if (carousel.CarouselItemCollection.Any(c => c.MediaCollection.Count == 0))
                    {
                        this.AddInformator(InformatorType.Warning, $"Widget '{carousel.Name}' has some carousel items which will not be shown in the application because the have no image configured.");
                    }
                }
                else if (widgetEntity is WidgetActionBannerEntity actionBanner && actionBanner.MediaCollection.Count == 0)
                {
                    this.AddInformator(InformatorType.Warning, $"Widget '{actionBanner.Name}' will not be shown in the application because it is missing an image.");
                }
            }

            this.Validate();
        }


        public override bool Save()
        {

            bool success = base.Save();

            if (success)
            {
                 CustomTextHelper.UpdateCustomTexts(this.DataSource, this.DataSourceAsPageEntity.ApplicationConfigurationEntity.CompanyEntity);

            }

            return success;
        }

        #endregion

        #region Properties

        public LandingPageEntity DataSourceAsPageEntity => this.DataSource as LandingPageEntity;

        #endregion

        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "LandingPage";
            this.LoadUserControls();

            this.DataSourceLoaded += this.Page_DataSourceLoaded;
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.DataSourceAsPageEntity.IsNew &&
                !this.IsPostBack)
            {
                this.SetWarnings();
            }
        }

        private void Page_DataSourceLoaded(object sender)
        {
            this.SetGui();
        }

        #endregion
    }
}
