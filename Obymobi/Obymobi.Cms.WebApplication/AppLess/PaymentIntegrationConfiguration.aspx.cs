﻿using Dionysos.Web.UI;
using Obymobi.Cms.WebApplication.Configuration.Sys.SubPanels;
using Obymobi.Cms.WebApplication.Strategies.PaymentProvider;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.ObymobiCms.MasterPages;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Linq;
using System.Web.UI.WebControls;

namespace Obymobi.ObymobiCms.AppLess
{
    public partial class PaymentIntegrationConfiguration : PageLLBLGenEntity
    {
        private PaymentIntegrationConfigurationPanelStrategy paymentIntegrationConfigurationPanelStrategy;
        public PaymentIntegrationConfigurationEntity DataSourceAsPaymentIntegrationConfigurationEntity => DataSource as PaymentIntegrationConfigurationEntity;

        protected override void OnInit(EventArgs args)
        {
            LoadUserControls();
            DataSourceLoaded += PaymentIntegrationConfiguration_DataSourceLoaded;
            base.OnInit(args);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!cbPaymentProviderId.ValueGreaterThanZero)
            {
                return;
            }

            PaymentProviderEntity paymentProvider = new PaymentProviderEntity(cbPaymentProviderId.ValidId);

            paymentIntegrationConfigurationPanelStrategy = PaymentPanelStrategyFactory
                .Get(paymentProvider.Type)
                .GetPaymentIntegrationConfigurationPanelStrategy();

            paymentIntegrationConfigurationPanelStrategy.AddPanel(this);
        }

        private void PaymentIntegrationConfiguration_DataSourceLoaded(object sender)
        {
            plhPaymentProviderInfo.Visible = false;

            if (!DataSourceAsPaymentIntegrationConfigurationEntity.IsNew)
            {
                cbPaymentProviderId.Enabled = false;
                ((MasterPageEntity) Master).ToolBar.DeleteButton.Visible = CmsSessionHelper.CurrentRole == Role.GodMode;
            }

            AddPaymentProcessorInfoPanel();
        }

        public override bool Save()
        {
            if (PageMode == Dionysos.Web.PageMode.Add)
            {
                DataSourceAsPaymentIntegrationConfigurationEntity.CompanyId = CmsSessionHelper.CurrentCompanyId;
            }
            
            if (Informators.Any(info => info.InformatorType != InformatorType.Warning))
            {
                Validate();
                return false;
            }

            if (paymentIntegrationConfigurationPanelStrategy == null || !paymentIntegrationConfigurationPanelStrategy.Save())
            {
                Validate();
                return false;
            }

            return Save(true);
        }        

        private void AddPaymentProcessorInfoPanel()
        {
            pnlPaymentProcessorInfo.Visible = true;

            PaymentProcessorInfoPanel.PaymentProcessorSettings settings = new PaymentProcessorInfoPanel.PaymentProcessorSettings(false, DataSourceAsPaymentIntegrationConfigurationEntity.PaymentProcessorInfo, DataSourceAsPaymentIntegrationConfigurationEntity.PaymentProcessorInfoFooter);
            PaymentProcessorInfoPanel paymentProcessorInfoPanel = (PaymentProcessorInfoPanel)LoadControl("~/Configuration/Sys/SubPanels/PaymentProcessorInfoPanel.ascx");
            paymentProcessorInfoPanel.Load(settings);

            pnlPaymentProcessorInfo.Controls.Add(paymentProcessorInfoPanel);
        }

        private void LoadUserControls()
        {
            PredicateExpression companyFilter = new PredicateExpression();
            companyFilter.Add(PaymentProviderCompanyFields.CompanyId == DBNull.Value);
            companyFilter.AddWithOr(PaymentProviderCompanyFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

            PredicateExpression filter = new PredicateExpression();
            filter.Add(companyFilter);

            if (!CmsSessionHelper.CurrentUser.HasFeature(FeatureToggle.CreatingAdyenForPlatformsPaymentIntegrationConfigurations))
            {
                filter.Add(PaymentProviderFields.Type != PaymentProviderType.AdyenForPlatforms);
            }

            RelationCollection relations = new RelationCollection();
            relations.Add(PaymentProviderEntityBase.Relations.PaymentProviderCompanyEntityUsingPaymentProviderId, JoinHint.Left);

            cbPaymentProviderId.DataBindEntityCollection<PaymentProviderEntity>(filter, relations, PaymentProviderFields.Name, PaymentProviderFields.Name);
        }        

        public PlaceHolder PaymentIntegrationConfigurationPlaceHolder => plhPaymentProviderInfo;
    }
}
