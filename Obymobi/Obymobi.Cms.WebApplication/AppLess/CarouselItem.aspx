﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.AppLess.CarouselItem" ValidateRequest="false" Codebehind="CarouselItem.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
	            <Controls>
                    <D:PlaceHolder runat="server" ID="plhCarouselItem">
		            <table class="dataformV2">
			            <tr>
				            <td class="label">
					            <D:LabelEntityFieldInfo runat="server" id="lblName">Name</D:LabelEntityFieldInfo>
				            </td>
				            <td class="control">
					            <D:TextBoxString ID="tbName" runat="server" IsRequired="true" MaxLength="255"></D:TextBoxString>
				            </td>
				            <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblMessage">Message</D:LabelEntityFieldInfo>
				            </td>
				            <td class="control">
					            <D:TextBoxString ID="tbMessage" runat="server" MaxLength="255"></D:TextBoxString>
				            </td>
			            </tr>
						<tr>
							<td class="label">
								<D:Label runat="server" id="lblOverlayType">Overlay Type</D:Label>
							</td>
							<td class="control">
								<X:ComboBoxEnum runat="server" ID="cbOverlayType" Type="Obymobi.Enums.OverlayType, Obymobi" />
							</td>
							<td class="label"></td>
							<td class="control"></td>
						</tr>
						<D:PlaceHolder runat="server" ID="plhAddAction">
							<tr>
								<td class="label">
								</td>
								<td class="control">
									<D:Button ID="btnAddAction" runat="server" Text="Add action" />
								</td>
								<td class="label">
								</td>
								<td class="control">
								</td>
							</tr>
						</D:PlaceHolder>
						<D:PlaceHolder runat="server" ID="plhManageAction">
							<tr>
								<td class="label">
								</td>
								<td class="control">
									<D:Button ID="btnManageAction" runat="server" Text="Manage action" />
									&nbsp;
									<D:Button ID="btnDeleteAction" runat="server" Text="Delete action" />
								</td>
								<td class="label">
								</td>
								<td class="control">
								</td>
							</tr>
						</D:PlaceHolder>
			        </table>	
                    </D:PlaceHolder>
	            </Controls>
            </X:TabPage>
        </TabPages>
    </X:PageControl>
</div>
</asp:Content>

