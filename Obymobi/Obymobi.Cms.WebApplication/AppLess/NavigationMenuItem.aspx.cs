﻿using System;
using Obymobi.Cms.WebApplication.AppLess;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;
using Obymobi.ObymobiCms.AppLess.SubPanels;
using Obymobi.ObymobiCms.MasterPages;

namespace Obymobi.ObymobiCms.AppLess
{
    public partial class NavigationMenuItem : Dionysos.Web.UI.PageLLBLGenEntity
    {
        #region Fields

        private ActionPanel actionPanel;

        #endregion

        #region Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Master is MasterPageEntity masterPage)
            {
                masterPage.ToolBar.SaveButton.Visible = true;
                masterPage.ToolBar.SaveAndGoButton.Visible = true;
                masterPage.ToolBar.SaveAndNewButton.Visible = false;
                masterPage.ToolBar.DeleteButton.Visible = false;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            this.DataSourceLoaded += NavigationMenuItem_DataSourceLoaded;
            base.OnInit(e);
        }

        private void NavigationMenuItem_DataSourceLoaded(object sender)
        {
            this.SetGui();
        }

        private void LoadUserControls()
        {
            NavigationMenuItemEntity navigationMenuItemEntity = new NavigationMenuItemEntity(this.EntityId);
            if (navigationMenuItemEntity.IsNew)
            {
                return;
            }

            this.LoadTranslationsTab();
        }

        private void SetGui()
        {
            if (this.DataSourceAsNavigationMenuItemEntity.IsNew)
            {
                return;
            }

            this.RenderActionPanel();
        }

        private void RenderActionPanel()
        {
            this.pnlAction.Visible = true;

            this.actionPanel = this.LoadControl<ActionPanel>("~/AppLess/SubPanels/Actions/ActionPanel.ascx");
            this.actionPanel.LoadAction(this.DataSourceAsNavigationMenuItemEntity.ActionEntity, this.DataSourceAsNavigationMenuItemEntity.NavigationMenuEntity.ApplicationConfigurationId);

            this.plhAction.Controls.Add(this.actionPanel);
        }

        public override bool Save()
        {
            this.actionPanel?.Save();

            if (base.Save())
            {
                CompanyEntity companyEntity = new CompanyEntity(this.DataSourceAsNavigationMenuItemEntity.ParentCompanyId);
                
                CustomTextHelper.UpdateCustomTexts(this.DataSource, companyEntity);
                
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region Properties

        public NavigationMenuItemEntity DataSourceAsNavigationMenuItemEntity => this.DataSource as NavigationMenuItemEntity;

		#endregion
	}
}
