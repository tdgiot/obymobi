﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.AppLess.ReceiptTemplate" Codebehind="ReceiptTemplate.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">    
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<controls>
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblName" LocalizeText="False">Friendly name</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
							</td>
                            <td class="label"></td>
                            <td class="control"></td>
						</tr>
                    </table>
                    <D:Panel ID="pnlVendorInformation" runat="server" GroupingText="Vendor Information">
                        <table class="dataformV2">
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblSellerName" LocalizeText="False">Vendor name</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:TextBoxString ID="tbSellerName" runat="server" IsRequired="true"></D:TextBoxString>
                                </td>
                                <td class="label"></td>
                                <td class="control"></td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblSellerAddress" LocalizeText="False">Vendor address</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:TextBoxString ID="tbSellerAddress" runat="server" IsRequired="true"></D:TextBoxString>
                                </td>
                            </tr>
							<tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblSellerContactInfo" LocalizeText="False">Vendor telephone number</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:TextBoxString ID="tbSellerContactInfo" runat="server" IsRequired="true"></D:TextBoxString>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblSellerContactEmail" LocalizeText="False">Vendor email address</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:TextBoxString ID="tbSellerContactEmail" runat="server" IsRequired="true"></D:TextBoxString>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblSmsOriginator" LocalizeText="False">SMS originator number</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:TextBoxString ID="tbSmsOriginator" runat="server"></D:TextBoxString>
                                </td>
                            </tr>
						</table>
					</D:Panel>
                    <D:Panel ID="pnlDefaultReceivers" runat="server" GroupingText="Default Receipt Receivers">
                        <table class="dataformV2">
						    <tr>
							    <td class="label">
								    <D:Label runat="server" id="lblEmail" LocalizeText="False">Email receivers</D:Label>
							    </td>
							    <td class="control" rowspan="5">
								    <D:TextBoxMultiLine ID="tbEmail" runat="server" IsRequired="false" UseDataBinding="false"></D:TextBoxMultiLine>
								    <br />
								    <D:Label ID="lblEmailComment" runat="server"><small>Multiple email addresses: Put each email address on a different line.</small></D:Label>
							    </td>
							    <td class="label">
								    <D:Label runat="server" id="lblPhonenumber" LocalizeText="False">Sms receivers</D:Label>
							    </td>
							    <td class="control" rowspan="5">
								    <D:TextBoxMultiLine ID="tbPhonenumber" runat="server" IsRequired="false" UseDataBinding="false"></D:TextBoxMultiLine>
								    <br />
								    <D:Label ID="lblPhonenumberComment" runat="server"><small>Multiple phone numbers: Put each phone number on a different line.</small></D:Label>
							    </td>
						    </tr>
					    </table>
					</D:Panel>
                </controls>
            </X:TabPage>
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>