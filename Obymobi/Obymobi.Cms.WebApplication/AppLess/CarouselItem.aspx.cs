﻿using System;
using Obymobi.Cms.WebApplication.AppLess;
using Obymobi.Data;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.ObymobiCms.AppLess
{
    public partial class CarouselItem : Dionysos.Web.UI.PageLLBLGenEntity
    {
        #region Methods

        private void LoadUserControls()
        {
            this.LoadTranslationsTab();
            this.LoadMediaTab(supportMultipleImages: false, supportCultureSpecificImages: false);
        }

        private void SetGui()
        {
            if (this.DataSourceAsCarouselItemEntity.IsNew)
            {
                this.plhAddAction.Visible = false;
                this.plhManageAction.Visible = false;
            }
            else if (this.DataSourceAsCarouselItemEntity.WidgetActionButtonEntity.IsNew)
            {
                this.plhAddAction.Visible = true;
                this.plhManageAction.Visible = false;
            }
            else
            {
                this.plhAddAction.Visible = false;
                this.plhManageAction.Visible = true;
            }
        }

        #endregion

        #region Properties

        public CarouselItemEntity DataSourceAsCarouselItemEntity => this.DataSource as CarouselItemEntity;

        #endregion

        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "CarouselItem";
            this.LoadUserControls();
            this.DataSourceLoaded += this.CarouselItem_DataSourceLoaded;
            base.OnInit(e);
        }

        private void CarouselItem_DataSourceLoaded(object sender)
        {
            this.SetGui();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.btnAddAction.Click += this.AddAction;
            this.btnManageAction.Click += this.ManageAction;
            this.btnDeleteAction.Click += this.DeleteAction;
        }

        public override bool Save()
        {
            bool success = base.Save();

            if (success && this.DataSource is ICustomTextContainingEntity)
            {
                CompanyEntity companyEntity = new CompanyEntity(this.DataSourceAsCarouselItemEntity.ParentCompanyId);
                CustomTextHelper.UpdateCustomTexts(this.DataSource, companyEntity);
            }

            return success;
        }

        private void AddAction(object sender, EventArgs e)
        {
            WidgetActionButtonEntity actionButton = new WidgetActionButtonEntity();
            actionButton.ApplicationConfigurationId = this.DataSourceAsCarouselItemEntity.WidgetCarouselEntity.ApplicationConfigurationId;
            actionButton.ParentCompanyId = this.DataSourceAsCarouselItemEntity.ParentCompanyId;
            actionButton.Name = this.DataSourceAsCarouselItemEntity.Name;
            actionButton.Text = this.DataSourceAsCarouselItemEntity.Name;
            actionButton.IconType = ActionButtonIconType.None;

            this.DataSourceAsCarouselItemEntity.WidgetActionButtonEntity = actionButton;

            if (this.DataSourceAsCarouselItemEntity.Save(true))
            {
                int widgetId = this.DataSourceAsCarouselItemEntity.WidgetActionButtonEntity.WidgetId;

                string entityName = actionButton.GetType().Name.Replace("Entity", "");

                Response.Redirect($"~/AppLess/Widget.aspx?id={widgetId}&EntityName={entityName}");
            }
        }

        private void ManageAction(object sender, EventArgs e)
        {
            WidgetActionButtonEntity actionButton = this.DataSourceAsCarouselItemEntity.WidgetActionButtonEntity;

            int widgetId = actionButton.WidgetId;
            string entityName = actionButton.GetType().Name.Replace("Entity", "");

            Response.Redirect($"~/AppLess/Widget.aspx?id={widgetId}&EntityName={entityName}");
        }

        private void DeleteAction(object sender, EventArgs e)
        {
            WidgetActionButtonEntity actionButton = this.DataSourceAsCarouselItemEntity.WidgetActionButtonEntity;

            if (actionButton.Delete())
            {
                this.Refresh();
            }
        }

        #endregion
    }
}
