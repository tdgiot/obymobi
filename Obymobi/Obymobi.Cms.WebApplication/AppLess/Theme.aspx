﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true"
    Inherits="Obymobi.ObymobiCms.AppLess.Theme" ValidateRequest="false" CodeBehind="Theme.aspx.cs" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" runat="Server">
    <div>
        <X:PageControl ID="tabsMain" runat="server" Width="100%">
            <TabPages>
                <X:TabPage Text="Algemeen" Name="Generic">
                    <controls>
		            <table class="dataformV2">
						<tr>
				            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" ID="lblName">Name</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
								<D:TextBoxString ID="tbName" runat="server" IsRequired="true" MaxLength="255"></D:TextBoxString>
                            </td>   	
                            <td class="label">
                                <D:Label runat="server" ID="lblFontfamilyLabel">Font family</D:Label>
                            </td>
                            <td class="control">
                                <X:ComboBoxEnum ID="tbFontFamilyList"  runat="server" UseDataBinding="false" Type="Obymobi.Enums.ApplessFontTypes, Obymobi" ></X:ComboBoxEnum>
						    </td>   	
			            </tr>                            
			            <tr>
                            <td class="label">
                                <D:Label runat="server" ID="lblPrimaryColor">Primary color</D:Label>
                            </td>
                            <td class="control">
								<dxe:ASPxColorEdit ID="tbPrimaryColor" runat="server" ></dxe:ASPxColorEdit>
                            </td>   
							<td class="label">
                                <D:Label runat="server" ID="lblSignalColor">Signal color</D:Label>
                            </td>
                            <td class="control">
								<dxe:ASPxColorEdit ID="tbSignalColor" runat="server"></dxe:ASPxColorEdit>
                            </td>   
			            </tr>    
						<tr>
				            <td class="label">
                                <D:Label runat="server" ID="lblTextColor">Text color</D:Label>
                            </td>
                            <td class="control">
								<dxe:ASPxColorEdit ID="tbTextColor" runat="server" ></dxe:ASPxColorEdit>
                            </td>   
							<td class="label">
                                <D:Label runat="server" ID="lblDeviderColor">Divider color</D:Label>
                            </td>
                            <td class="control">
								<dxe:ASPxColorEdit ID="tbDeviderColor" runat="server"></dxe:ASPxColorEdit>
                             </td>   
			            </tr>    
						<tr>
						    <td class="label">
                                <D:Label runat="server" ID="lblBackgroundColor">Background color</D:Label>
                            </td>
                            <td class="control">
							    <dxe:ASPxColorEdit ID="tbBackgroundColor" runat="server"></dxe:ASPxColorEdit>
                            </td>
                            <td class="label">
                            </td>
                            <td class="control">
                            </td>
			            </tr>               
			        </table>	
	            </controls>
                </X:TabPage>
            </TabPages>
        </X:PageControl>
    </div>
</asp:Content>

