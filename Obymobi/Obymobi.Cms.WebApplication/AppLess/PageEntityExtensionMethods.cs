﻿using Dionysos;
using Dionysos.Web.UI;
using Dionysos.Web.UI.DevExControls;
using Obymobi.Enums;
using CustomTextCollection = Obymobi.ObymobiCms.Generic.UserControls.CustomTextCollection;
using MediaCollection = Obymobi.ObymobiCms.Generic.UserControls.MediaCollection;

namespace Obymobi.Cms.WebApplication.AppLess
{
    public static class PageEntityExtensionMethods
    {
        public static void LoadMediaTab(this PageLLBLGenEntity pageEntity,
            string mediaTabName = "Media",
            string mediaTabText = "Images",
            bool supportMultipleImages = true,
            bool supportCultureSpecificImages = true,
            Role? accessRole = null)
        {
            if (!CurrentUserHasRole(accessRole))
            {
                return;
            }

            PageControl pageControl = GetPageControl(pageEntity);
            if (pageControl.HasTabPage(mediaTabName))
            {
                return;
            }

            MediaCollection mediaCollection = (MediaCollection)pageControl.AddTabPage(mediaTabText, mediaTabName, "~/Generic/UserControls/MediaCollection.ascx");
            mediaCollection.SetImageConstraints(supportMultipleImages, supportCultureSpecificImages);
        }

        public static CustomTextCollection LoadTranslationsTab(this PageLLBLGenEntity pageEntity,
            string tabName = "Translations",
            bool fullWidth = false,
            Role? accessRole = null)
        {
            if (!CurrentUserHasRole(accessRole))
            {
                return null;
            }

            PageControl pageControl = GetPageControl(pageEntity);
            if (pageControl.HasTabPage(tabName))
            {
                return null;
            }
            
            CustomTextCollection customTextCollection = (CustomTextCollection)pageControl.AddTabPage(tabName, nameof(CustomTextCollection), "~/Generic/UserControls/CustomTextCollection.ascx");
            customTextCollection.FullWidth = fullWidth;

            return customTextCollection;
        }

        public static void LoadTab(this PageLLBLGenEntity pageEntity,
            string tabName,
            string systemName,
            string controlPath,
            Role? accessRole = null)
        {
            if (!CurrentUserHasRole(accessRole))
            {
                return;
            }

            PageControl pageControl = GetPageControl(pageEntity);
            if (pageControl.HasTabPage(tabName))
            {
                return;
            }
            
            pageControl.AddTabPage(tabName, systemName, controlPath);
        }

        private static bool CurrentUserHasRole(Role? role) => !role.HasValue || CmsSessionHelper.CurrentRole >= role;

        private static PageControl GetPageControl(PageLLBLGenEntity pageEntity)
        {
            PageControl control = pageEntity.FindControlRecursively<PageControl>();
            if (control == null)
            {
                throw new TechnicalException("No PageControl found, you can only add a tab if there is a PageControl available.");
            }
            return control;
        }
    }
}
