﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.AppLess.NavigationMenuItem" Codebehind="NavigationMenuItem.aspx.cs" %>
<%@ Reference VirtualPath="~/Generic/UserControls/CustomTextCollection.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">    
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<controls>
					<D:PlaceHolder runat="server" ID="plhNavigationMenuItem">
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblText">Text</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbText" runat="server" IsRequired="true" MaxLength="100"></D:TextBoxString>
							</td>
							<td class="label"></td>
							<td class="control"></td>
						</tr>
                    </table>
					</D:PlaceHolder>
					<D:Panel ID="pnlAction" runat="server" GroupingText="Action" Visible="false">
						<D:PlaceHolder runat="server" ID="plhAction"></D:PlaceHolder>
					</D:Panel>
                </controls>                    
            </X:TabPage>
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>