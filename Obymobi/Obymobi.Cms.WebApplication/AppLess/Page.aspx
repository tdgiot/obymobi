﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.AppLess.Page" ValidateRequest="false" Codebehind="Page.aspx.cs" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
	            <Controls>
                    <D:PlaceHolder runat="server" ID="plhPage">
		            <table class="dataformV2">
			            <tr>
				            <td class="label">
					            <D:LabelEntityFieldInfo runat="server" id="lblName" MaxLength="255">Name</D:LabelEntityFieldInfo>
				            </td>
				            <td class="control">
					            <D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
				            </td>
				            <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblExternalId">External id</D:LabelEntityFieldInfo>
				            </td>
				            <td class="control">
					            <D:TextBoxString ID="tbExternalId" runat="server" IsRequired="true" Enabled="false"></D:TextBoxString>
				            </td>
			            </tr>    
                        <tr>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblNavigationMenuId">Navigation menu</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <X:ComboBoxInt runat="server" ID="cbNavigationMenuId" CssClass="input" TextField="Name" ValueField="NavigationMenuId" IncrementalFilteringMode="Contains" IncrementalFilteringDelay="1000"></X:ComboBoxInt>
                            </td>
                            <td class="label">
					            <D:LabelEntityFieldInfo runat="server" id="lblThemeId">Theme</D:LabelEntityFieldInfo>
				            </td>
				            <td class="control">
					            <X:ComboBoxInt runat="server" ID="cbThemeId" CssClass="input" TextField="Name" IsRequired="true" ValueField="ThemeId" IncrementalFilteringMode="Contains" IncrementalFilteringDelay="1000"></X:ComboBoxInt>
				            </td>
                        </tr>
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblOutletId">Outlet</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<X:ComboBoxInt runat="server" ID="cbOutletId" CssClass="input" TextField="Name" ValueField="OutletId" IncrementalFilteringMode="Contains" IncrementalFilteringDelay="1000"></X:ComboBoxInt>
							</td>
							<td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblHomeNavigationEnabled">Allow home screen navigation</D:LabelEntityFieldInfo>
								<a id="home-navigation-info-popup" href="#" style="margin-left:2px">
									<img src="<%= ResolveUrl("~/Images/Icons/information.png") %>" style="float: right;" alt="Allow home screen navigation" />
								</a>
                            </td>
							<td class="control">
								<D:CheckBox runat="server" ID="cbHomeNavigationEnabled"/>
                            </td>
						</tr>
						<D:PlaceHolder runat="server" id="plhPageLink" Visible="false">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblVisit">Link</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:HyperLink runat="server" id="hlApplessLink" Target="_blank" LocalizeText="false" ></D:HyperLink>
							</td>
							<td class="label"></td>
							<td class="control"></td>
						</tr>
						</D:PlaceHolder>
			            <D:PlaceHolder ID="plhWidgetPanel" runat="server" Visible="false">
							<tr>
								<td class="label">
									<D:Label runat="server" ID="lblWidget">Widgets</D:Label>
								</td>
								<td class="control" colspan="3">
									<D:PlaceHolder runat="server" ID="plhWidgetControl"></D:PlaceHolder>
								</td>
							</tr>
			            </D:PlaceHolder>
			        </table>	
                    </D:PlaceHolder>
	            </Controls>
            </X:TabPage>
        </TabPages>
    </X:PageControl>
</div>

<dx:ASPxPopupControl ID="pcAllowHomeScreenNavigation" ClientInstanceName="pcAllowHomeScreenNavigation" PopupElementID="home-navigation-info-popup" 
Width="400" PopupAction="MouseOver" CloseAction="MouseOut" CloseOnEscape="false" Modal="false" ShowOnPageLoad="false" HeaderText="Home screen navigation" runat="server">
<ContentCollection>
	<dx:PopupControlContentControl runat="server">
		<p>Notice that this setting will control:</p>
		<ul>
			<li>
				- Ability to navigate to the home screen using the header image
			</li>
			<li>
				- Navigate to either the same page or home screen after checkout
			</li>
		</ul>
	</dx:PopupControlContentControl>
</ContentCollection>
</dx:ASPxPopupControl>

</asp:Content>

