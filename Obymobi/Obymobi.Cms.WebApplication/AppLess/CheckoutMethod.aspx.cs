﻿using Dionysos;
using Dionysos.Web;
using Dionysos.Web.UI;
using Dionysos.Web.UI.DevExControls;
using Obymobi.Cms.Logic.HelperClasses;
using Obymobi.Cms.WebApplication.AppLess;
using Obymobi.Cms.WebApplication.Old_App_Code.Extensions;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Extensions;
using Obymobi.Logic.HelperClasses;
using Obymobi.ObymobiCms.MasterPages;
using Resources;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Obymobi.ObymobiCms.AppLess
{
    public partial class CheckoutMethod : PageLLBLGenEntity
    {
        private Generic.UserControls.CustomTextCollection translationsTab;

        private void LoadUserControls()
        {
            if (PageMode == PageMode.Add)
            {
                cbCheckoutType.DataBindEnum<CheckoutType>(valuesToSkip: CheckoutType.FreeOfCharge);
            }
            else if (PageMode == PageMode.View ||
                     PageMode == PageMode.Edit)
            {
                translationsTab = this.LoadTranslationsTab(fullWidth: true);

                cbCheckoutType.DataBindEnum<CheckoutType>();
                lbDeliverypointgroups.DataBindEntityCollection<DeliverypointgroupEntity>(DeliverypointgroupFields.CompanyId == CmsSessionHelper.CurrentCompanyId, DeliverypointgroupFields.Name, DeliverypointgroupFields.Name);




                IPredicateExpression normal = new PredicateExpression(PaymentIntegrationConfigurationFields.CompanyId == CmsSessionHelper.CurrentCompanyId)
                {
                    PaymentProviderFields.Type != PaymentProviderType.AdyenForPlatforms
                };

                IPredicateExpression normalAFP = new PredicateExpression(PaymentIntegrationConfigurationFields.CompanyId == CmsSessionHelper.CurrentCompanyId)
                {
                    PaymentProviderFields.Type == PaymentProviderType.AdyenForPlatforms,
                    new FieldCompareValuePredicate(PaymentIntegrationConfigurationFields.AdyenAccountCode, ComparisonOperator.NotEqual, DBNull.Value)
                };
                IPredicateExpression filter = new PredicateExpression();
                filter.AddWithOr(normal);
                filter.AddWithOr(normalAFP);

                RelationCollection relations = new RelationCollection();
                relations.Add(PaymentIntegrationConfigurationEntityBase.Relations.PaymentProviderEntityUsingPaymentProviderId, JoinHint.Left);


                cbPaymentIntegrationConfigurationId.DataBindEntityCollection<PaymentIntegrationConfigurationEntity>(filter, relations, PaymentIntegrationConfigurationFields.DisplayName, PaymentIntegrationConfigurationFields.DisplayName);

                IEnumerable<Obymobi.Currency> currencies = Obymobi.Currency.Mappings.Values.OrderBy(x => x.Name);

                ddlCurrencyCode.DataSource = currencies;
                ddlCurrencyCode.DataBind();

                IEnumerable<Obymobi.Country> countries = Obymobi.Country.Mappings.Values.OrderBy(x => x.Name);

                ddlCountryCode.DataSource = countries;
                ddlCountryCode.DataBind();
            }
        }

        private void SetGui()
        {
            DisplayStrategy displayStrategy = DisplayStrategyFactory.Create(PageMode, DataSourceAsCheckoutMethodEntity.CheckoutType);
            displayStrategy.ConfigureVisibility(this);
        }

        private static class DisplayStrategyFactory
        {
            public static DisplayStrategy Create(PageMode pageMode, CheckoutType checkoutType)
            {
                if (pageMode == PageMode.Add)
                {
                    return new AddCheckoutMethodDisplayStrategy();
                }

                if (pageMode != PageMode.View && pageMode != PageMode.Edit)
                {
                    return new DefaultDisplayStrategy();
                }

                switch (checkoutType)
                {
                    case CheckoutType.ChargeToRoom:
                        return new ChargeToRoomStrategy();
                    case CheckoutType.PaymentProvider:
                        return new PaymentProviderDisplayStrategy();
                    case CheckoutType.PayLater:
                        return new PayLaterDisplayStrategy();
                    case CheckoutType.FreeOfCharge:
                        return new FreeOfChargeDisplayStrategy();
                    default:
                        return new DefaultDisplayStrategy();
                }
            }
        }

        private abstract class DisplayStrategy
        {
            public virtual void ConfigureVisibility(CheckoutMethod checkoutMethod) => checkoutMethod.pnlChargeConfirmation.Visible = false;
        }

        private class AddCheckoutMethodDisplayStrategy : DisplayStrategy
        {
            public override void ConfigureVisibility(CheckoutMethod checkoutMethod)
            {
                base.ConfigureVisibility(checkoutMethod);
                checkoutMethod.plhEdit.Visible = false;
                checkoutMethod.pnlOrderUpdateMessage.Visible = false;
                checkoutMethod.pnlDeliverypointgroups.Visible = false;
            }
        }

        private class DefaultDisplayStrategy : DisplayStrategy
        {

        }

        private class ChargeToRoomStrategy : DisplayStrategy
        {
            public override void ConfigureVisibility(CheckoutMethod checkoutMethod)
            {
                base.ConfigureVisibility(checkoutMethod);

                checkoutMethod.cbCheckoutType.Enabled = false;
                checkoutMethod.pnlChargeConfirmation.Visible = true;
            }
        }

        private class PayLaterDisplayStrategy : DisplayStrategy
        {
            public override void ConfigureVisibility(CheckoutMethod checkoutMethod)
            {
                base.ConfigureVisibility(checkoutMethod);

                checkoutMethod.cbCheckoutType.Enabled = false;
                checkoutMethod.pnlDeliverypointgroups.Visible = false;
                checkoutMethod.pnlChargeConfirmation.Visible = false;

                checkoutMethod.translationsTab.ExcludeCustomTextTypes(
                    CustomTextType.CheckoutMethodDeliveryPointGroupLabel,
                    CustomTextType.CheckoutMethodDeliverypointLabel,
                    CustomTextType.CheckoutMethodLastNameLabel,
                    CustomTextType.CheckoutMethodConfirmationDescription
                );
            }
        }

        private class FreeOfChargeDisplayStrategy : DisplayStrategy
        {
            public override void ConfigureVisibility(CheckoutMethod checkoutMethod)
            {
                base.ConfigureVisibility(checkoutMethod);

                checkoutMethod.cbCheckoutType.Enabled = false;
                checkoutMethod.pnlDeliverypointgroups.Visible = false;
                checkoutMethod.pnlChargeConfirmation.Visible = false;
                checkoutMethod.lblTermsAndConditionsRequired.Visible = false;
                checkoutMethod.cbTermsAndConditionsRequired.Visible = false;
                checkoutMethod.lblLabel.Visible = false;
                checkoutMethod.tbLabel.Visible = false;
                checkoutMethod.lblDescription.Visible = false;
                checkoutMethod.tbDescription.Visible = false;
                checkoutMethod.lblTermsAndConditionsLabel.Visible = false;
                checkoutMethod.plhTermsAndConditionsLabel.Visible = false;

                TabPage translationsTabPage = checkoutMethod.tabsMain.GetTabPageByName(nameof(CustomTextCollection));
                if (translationsTabPage != null)
                {
                    translationsTabPage.Visible = false;
                }
            }
        }

        private class PaymentProviderDisplayStrategy : DisplayStrategy
        {
            public override void ConfigureVisibility(CheckoutMethod checkoutMethod)
            {
                base.ConfigureVisibility(checkoutMethod);

                checkoutMethod.cbCheckoutType.Enabled = false;
                checkoutMethod.lblPaymentIntegrationConfigurationId.Visible = true;
                checkoutMethod.cbPaymentIntegrationConfigurationId.Visible = true;
                checkoutMethod.cbPaymentIntegrationConfigurationId.IsRequired = true;
                checkoutMethod.cbTermsAndConditionsRequired.Enabled = false;
                checkoutMethod.cbTermsAndConditionsRequired.Value = true;
                checkoutMethod.pnlDeliverypointgroups.Visible = false;

                checkoutMethod.cbOrderReceiptNotificationMethod.Items.RemoveAt(0);
                checkoutMethod.plhOrderPaymentNotifications.Visible = true;

                checkoutMethod.plhOrderConfirmationNotification.Visible = false;

                checkoutMethod.translationsTab.ExcludeCustomTextTypes(
                    CustomTextType.CheckoutMethodDeliveryPointGroupLabel,
                    CustomTextType.CheckoutMethodDeliverypointLabel,
                    CustomTextType.CheckoutMethodLastNameLabel,
                    CustomTextType.CheckoutMethodConfirmationDescription
                );
            }
        }

        private void SetWarnings()
        {
            if (!DataSourceAsCheckoutMethodEntity.Active)
            {
                AddInformator(InformatorType.Warning, "Checkout method is not active.");
            }

            Validate();
        }

        public CheckoutMethodEntity DataSourceAsCheckoutMethodEntity => DataSource as CheckoutMethodEntity;

        protected override void OnInit(EventArgs e)
        {
            LoadUserControls();
            DataSourceLoaded += CheckoutMethod_DataSourceLoaded;
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!DataSourceAsCheckoutMethodEntity.IsNew &&
                !IsPostBack)
            {
                SetWarnings();
            }

            if (Master is MasterPageEntity masterPage)
            {
                masterPage.ToolBar.SaveButton.Visible = true;
                masterPage.ToolBar.SaveAndGoButton.Visible = true;
                masterPage.ToolBar.SaveAndNewButton.Visible = false;
                if (masterPage.ToolBar.DeleteButton != null)
                {
                    masterPage.ToolBar.DeleteButton.Visible = DataSourceAsCheckoutMethodEntity.CheckoutType != CheckoutType.FreeOfCharge;
                }
            }
        }

        private void CheckoutMethod_DataSourceLoaded(object sender)
        {
            SetGui();

            if (PageMode == PageMode.Add)
            {
                DataSourceAsCheckoutMethodEntity.Active = true;

                int relatedEntityId = GetRelatedEntityIdFromQueryString();

                DataSourceAsCheckoutMethodEntity.CompanyId = CmsSessionHelper.CurrentCompanyId;
                DataSourceAsCheckoutMethodEntity.OutletId = relatedEntityId;
            }
            else
            {
                lbDeliverypointgroups.SelectValues(DataSourceAsCheckoutMethodEntity.CheckoutMethodDeliverypointgroupCollection.Select(dpg => dpg.DeliverypointgroupId));

                ddlCountryCode.SelectedItem = ddlCountryCode.Items.FindByValue(DataSourceAsCheckoutMethodEntity.CountryCode);
                ddlCurrencyCode.SelectedItem = ddlCurrencyCode.Items.FindByValue(DataSourceAsCheckoutMethodEntity.CurrencyCode);
            }
        }

        private void SaveSelectedDeliverypointgroups()
        {
            ICollection<int> selectedDeliverypointgroupIds = lbDeliverypointgroups.SelectedItemsValuesAsIntList;

            CheckoutMethodDeliverypointgroupCollection deliverypointgroups = DataSourceAsCheckoutMethodEntity.CheckoutMethodDeliverypointgroupCollection;

            // Save the newly selected deliverypointgroups
            foreach (int deliverypointgroupId in selectedDeliverypointgroupIds)
            {
                // Skip already present deliverypointgroups
                if (deliverypointgroups.Any(dpg => dpg.DeliverypointgroupId == deliverypointgroupId))
                {
                    continue;
                }

                CheckoutMethodDeliverypointgroupEntity checkoutMethodDeliverypointgroup = new CheckoutMethodDeliverypointgroupEntity
                {
                    CheckoutMethodId = EntityId,
                    DeliverypointgroupId = deliverypointgroupId,
                    ParentCompanyId = DataSourceAsCheckoutMethodEntity.CompanyId
                };
                checkoutMethodDeliverypointgroup.Save();
            }

            // Remove unselected deliverypointgroups
            foreach (CheckoutMethodDeliverypointgroupEntity deliverypointgroup in deliverypointgroups)
            {
                if (selectedDeliverypointgroupIds.All(id => id != deliverypointgroup.DeliverypointgroupId))
                {
                    deliverypointgroup.Delete();
                }
            }
        }

        public int GetRelatedEntityIdFromQueryString()
        {
            string queryString = QueryStringHelper.GetString("RelatedEntityId");

            int.TryParse(queryString, out int relatedEntityId);

            return relatedEntityId;
        }

        public override bool Save()
        {
            if (DataSourceAsCheckoutMethodEntity.CheckoutType == CheckoutType.PaymentProvider)
            {
                if (DataSourceAsCheckoutMethodEntity.IsNew)
                {
                    DataSourceAsCheckoutMethodEntity.OrderReceiptNotificationMethod = OrderNotificationMethod.Email;
                }

                DataSourceAsCheckoutMethodEntity.TermsAndConditionsRequired = true;
                if (tbTermsAndConditionsLabel.Value.IsNullOrWhiteSpace())
                {
                    DataSourceAsCheckoutMethodEntity.TermsAndConditionsLabel = CheckoutMethodDefaultTranslations.CheckoutMethodTermsAndConditionsLabel;
                }
            }
            else if (DataSourceAsCheckoutMethodEntity.CheckoutType == CheckoutType.FreeOfCharge)
            {
                DataSourceAsCheckoutMethodEntity.TermsAndConditionsRequired = false;
            }

            DataSourceAsCheckoutMethodEntity.CurrencyCode = ddlCurrencyCode.SelectedValueString;
            DataSourceAsCheckoutMethodEntity.CountryCode = ddlCountryCode.SelectedValueString;

            bool success = base.Save();

            if (!DataSourceAsCheckoutMethodEntity.IsNew && success)
            {
                CustomTextHelper.UpdateCustomTexts(DataSource, DataSourceAsCheckoutMethodEntity.CompanyEntity);

                TranslationHelper.SetDefaultTranslations(CheckoutMethodDefaultTranslations.ResourceManager,
                    DataSourceAsCheckoutMethodEntity,
                    DataSourceAsCheckoutMethodEntity.CompanyEntity.GetDefaultCompanyCulture(),
                    DataSourceAsCheckoutMethodEntity.CompanyEntity.GetCompanyCultures());

                SaveSelectedDeliverypointgroups();
            }

            return success;
        }
    }
}
