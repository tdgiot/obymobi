﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.AppLess.Widget" ValidateRequest="false" Codebehind="Widget.aspx.cs" %>
<%@ Reference VirtualPath="~/Generic/UserControls/CustomTextCollection.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
	            <Controls>
                    <D:PlaceHolder runat="server" ID="plhWidget">
		            <table class="dataformV2">
			            <D:PlaceHolder runat="server" ID="plhBasicWidgetDetails">
				            <tr>
					            <td class="label">
						            <D:LabelEntityFieldInfo runat="server" id="lblName">Name</D:LabelEntityFieldInfo>
					            </td>
					            <td class="control">
						            <D:TextBoxString ID="tbName" runat="server" IsRequired="true" MaxLength="255"></D:TextBoxString>
					            </td>
								<td class="label">
	                                <D:Label runat="server" id="lblType">Type</D:Label>
	                            </td>
	                            <td class="control">
	                                <X:ComboBoxInt runat="server" ID="cbType" UseDataBinding="True" AutoPostBack="True" Enabled="false" IsRequired="true"></X:ComboBoxInt>
	                            </td>
				            </tr>
				            <tr>
								<td class="label">
						            <D:Label runat="server" id="lblVisible">Visible</D:Label>
					            </td>
					            <td class="control">
						            <D:CheckBox runat="server" ID="cbVisible" UseDataBinding="true" />
					            </td>
								<td class="label"></td>
								<td class="control"></td>
				            </tr>
						</D:PlaceHolder>
                        <D:PlaceHolder ID="plhType" runat="server" />
			        </table>	
                    </D:PlaceHolder>
		            <D:Panel ID="pnlAction" runat="server" GroupingText="Action" Visible="false">
			            <D:PlaceHolder runat="server" ID="plhAction"></D:PlaceHolder>
		            </D:Panel>
	            </Controls>
            </X:TabPage>
        </TabPages>
    </X:PageControl>
</div>
</asp:Content>

