<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.AppLess.Outlet" Codebehind="Outlet.aspx.cs" %>
<%@ Reference VirtualPath="~/AppLess/SubPanels/ServiceMethodCollection.ascx" %>
<%@ Reference VirtualPath="~/Generic/UserControls/CustomTextCollection.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">    
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<controls>
					<D:Panel ID="OrderSettingsPanel" runat="server" GroupingText="General">
						<table class="dataformV2">
							<tr>
								<td class="label">
									<D:LabelEntityFieldInfo runat="server" id="lblName">Name</D:LabelEntityFieldInfo>
								</td>
								<td class="control">
									<D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
								</td>
								<td class="label">
									<D:LabelEntityFieldInfo runat="server" id="lblbWaitTime" LocalizeText="False">Wait time (in minutes)</D:LabelEntityFieldInfo>
								</td>
								<td class="control">
									<D:TextBoxInt ID="tbWaitTime" runat="server" UseDataBinding="false"></D:TextBoxInt>
								</td>
							</tr>

							<tr>
								<D:Panel ID="plhOperationalState" runat="server">
									<td class="label">
										<D:LabelEntityFieldInfo runat="server" id="lblOrderIntakeDisabled" LocalizeText="False">Disable ordering</D:LabelEntityFieldInfo>
									</td>
									<td class="control">
										<D:CheckBox runat="server" ID="cbOrderIntakeDisabled" UseDataBinding="false" />
									</td>
								</D:Panel>
							</tr>
						</table>
					</D:Panel>
					
                    <D:Panel ID="pnlSellerInformation" runat="server" GroupingText="Seller Information">
                        <table class="dataformV2">
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblOutletSellerInformation_SellerName" LocalizeText="False">Vendor name</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:TextBoxString ID="tbOutletSellerInformation_SellerName" runat="server" IsRequired="true" />
                                </td>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblOutletSellerInformation_VatNumber" LocalizeText="False">VAT number</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:TextBoxString ID="tbOutletSellerInformation_VatNumber" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblOutletSellerInformation_Address" LocalizeText="False">Vendor address</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:TextBoxString ID="tbOutletSellerInformation_Address" runat="server" IsRequired="true" />
                                </td>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblSmsOriginator" LocalizeText="False">SMS originator</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:TextBoxString ID="tbOutletSellerInformation_SmsOriginator" runat="server" />
                                </td>
                            </tr>
                    		<tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblOutletSellerInformation_Phonenumber" LocalizeText="False">Vendor telephone number</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:TextBoxString ID="tbOutletSellerInformation_Phonenumber" runat="server" IsRequired="true" />
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblOutletSellerInformation_Email" LocalizeText="False">Vendor email address</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:TextBoxString ID="tbOutletSellerInformation_Email" runat="server" IsRequired="true" />
                                </td>
                            </tr>
                    	</table>
                    </D:Panel>
                    
                    <D:Panel ID="pnlOrderNotifications" runat="server" GroupingText="Notifications" Visible="false">
                        <table class="dataformV2">
							<tr>
                                <td class="label">
                                    <D:Label runat="server" id="lblOutletSellerInformation_ReceiveOrderConfirmationNotifications" LocalizeText="False">Receive all order confirmations</D:Label>
                                    
                                </td>
                                <td class="control">
                                    <D:CheckBox runat="server" ID="cbOutletSellerInformation_ReceiveOrderConfirmationNotifications" />
                                </td>
                                <td class="label">
                                    <D:Label runat="server" id="lblOutletSellerInformation_ReceiveOrderReceiptNotifications" LocalizeText="False">Receive all order receipts</D:Label>
                                </td>
                                <td class="control">
                                    <D:CheckBox runat="server" ID="cbOutletSellerInformation_ReceiveOrderReceiptNotifications" />
                                </td>
                            </tr>
                            <tr>
                    			<td class="label">
                    				<D:Label runat="server" id="lblEmail" LocalizeText="False">Email receivers</D:Label>
                    			</td>
                    			<td class="control" rowspan="5">
                    				<D:TextBoxMultiLine ID="tbReceiptReceiversEmail" runat="server" IsRequired="false" UseDataBinding="false"></D:TextBoxMultiLine>
                    				<br />
                    				<D:Label ID="lblEmailComment" runat="server"><small>Multiple email addresses: Put each email address on a different line.</small></D:Label>
                    			</td>
                    			<td class="label">
                    				<D:Label runat="server" id="lblPhonenumber" LocalizeText="False">Sms receivers</D:Label>
                    			</td>
                    			<td class="control" rowspan="5">
                    				<D:TextBoxMultiLine ID="tbReceiptReceiversSms" runat="server" IsRequired="false" UseDataBinding="false"></D:TextBoxMultiLine>
                    				<br />
                    				<D:Label ID="lblPhonenumberComment" runat="server"><small>Multiple phone numbers: Put each phone number on a different line.</small></D:Label>
                    			</td>
                    		</tr>
                    	</table>
                    </D:Panel>

					<D:Panel ID="pnlCustomerDetails" runat="server" GroupingText="Customer Details">
						<table class="dataformV2">
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblCustomerDetailsNameRequired">Ask for customer name</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:CheckBox runat="server" ID="cbCustomerDetailsNameRequired" />
                                </td>
	                            <td class="label"></td>
	                            <td class="control"></td>
                            </tr>
							<tr>
								<td class="label">
									<D:LabelEntityFieldInfo runat="server" id="lblCustomerDetailsPhoneRequired">Ask for phone number</D:LabelEntityFieldInfo>
								</td>
								<td class="control">
									<D:CheckBox runat="server" ID="cbCustomerDetailsPhoneRequired" />
								</td>
								<td class="label">
									<D:LabelEntityFieldInfo runat="server" id="lblCustomerDetailsEmailRequired">Ask for email address</D:LabelEntityFieldInfo>
								</td>
								<td class="control">
									<D:CheckBox runat="server" ID="cbCustomerDetailsEmailRequired" />
								</td>
							</tr>
						</table>
					</D:Panel>

					<D:Panel ID="pnlTipping" runat="server" GroupingText="Tipping">
						<table class="dataformV2">
							<tr>
								<td class="label">
									<D:LabelEntityFieldInfo runat="server" id="lblTippingDefaultPercentage" LocalizeText="False">Default tip percentage</D:LabelEntityFieldInfo>
								</td>
								<td class="control">
									<D:TextBoxDouble ID="tbTippingDefaultPercentage" runat="server"></D:TextBoxDouble>
								</td>
								<td class="label">
									<D:LabelEntityFieldInfo runat="server" id="lblTippingStepSize" LocalizeText="False">Step size</D:LabelEntityFieldInfo>
								</td>
								<td class="control">
									<D:TextBoxDouble ID="tbTippingStepSize" runat="server"></D:TextBoxDouble>
								</td>
							</tr>
							<tr>
								<td class="label">
									<D:LabelEntityFieldInfo runat="server" id="lblTippingMinimumPercentage" LocalizeText="False">Minimum tip percentage</D:LabelEntityFieldInfo>
								</td>
								<td class="control">
									<D:TextBoxDouble ID="tbTippingMinimumPercentage" runat="server"></D:TextBoxDouble>
								</td>

								<td class="label">
									<D:LabelEntityFieldInfo runat="server" id="lblTippingProductId">Tip Product</D:LabelEntityFieldInfo>
								</td>
								<td class="control link-list">
									<a href="<%= ResolveUrl("~/Catalog/Product.aspx") %>?id=<%= this.DataSourceAsOutletEntity.TippingProductId %>" target="_new">
										#<%= this.DataSourceAsOutletEntity.TippingProductId %>
									</a>
								</td>
							</tr>
						</table>
					</D:Panel>

					<D:Panel ID="pnlTaxPricing" runat="server" GroupingText="Pricing display settings">
						<table class="dataformV2">
							<tr>
								<td class="label">
									<D:LabelEntityFieldInfo runat="server" id="lblShowTaxBreakdown">Tax breakdown</D:LabelEntityFieldInfo>
								</td>
								<td class="control">
									<X:ComboBoxInt runat="server" ID="ddlShowTaxBreakdown"></X:ComboBoxInt>
								</td>
								<td class="label"></td>
								<td class="control"></td>
							</tr>
						</table>
					</D:Panel>

					<D:Panel ID="pnlMarketingData" runat="server" GroupingText="Marketing Data">
						<table class="dataformV2">
							<tr>
								<td class="label">
									<D:LabelEntityFieldInfo runat="server" id="lblOptInType">Marketing Opt-In</D:LabelEntityFieldInfo>
								</td>
								<td class="control">
									<X:ComboBoxEnum runat="server" ID="cbOptInType" Type="Obymobi.Enums.OptInType, Obymobi" />
								</td>
								<td class="label">Download Opt-Ins</td>
								<td class="control">
									<D:Button runat="server" ID="btnExportOptInsToCsv" OnClick="ExportOptIns" Text="CSV" />
								</td>
                            </tr>
						</table>
					</D:Panel>
				
                <D:Panel ID="Panel1" runat="server" GroupingText="Receipt">
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="LabelEntityFieldInfo1">Tax Disclaimer</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <D:TextBoxString ID="tbTaxDisclaimer" runat="server" DefaultValue="This is not a VAT invoice. To request a VAT invoice, please contact the venue."></D:TextBoxString>
                            </td>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="LabelEntityFieldInfo2">Tax Number Title</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <D:TextBoxString ID="tbTaxNumberTitle" runat="server"></D:TextBoxString>
                            </td>
                        </tr>
                    </table>
                </D:Panel>

                </controls>
            </X:TabPage>
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>
