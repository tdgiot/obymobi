﻿using System;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.AppLess
{
    public partial class NavigationMenus : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        #region Fields

        #endregion

        #region Methods

        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "NavigationMenu";
            this.EntityPageUrl = "~/AppLess/NavigationMenu.aspx";
            base.OnInit(e);
        }

        #endregion

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            LLBLGenProDataSource datasource = this.DataSource as LLBLGenProDataSource;
            if (datasource != null)
            {
                PredicateExpression filter = new PredicateExpression(NavigationMenuFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
                datasource.FilterToUse = filter;             
            }
        }

        #endregion

        #region Properties
        
        public NavigationMenuCollection DataSourceAsNavigationMenuCollection
        {
            get
            {
                return this.DataSource as NavigationMenuCollection;
            }
        }

        #endregion

    }
}
