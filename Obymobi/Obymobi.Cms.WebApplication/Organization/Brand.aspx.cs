﻿using System;
using System.Collections.Generic;
using System.Linq;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Dionysos.Web;
using ServiceStack;

namespace Obymobi.ObymobiCms.Organization
{
    public partial class Brand : Dionysos.Web.UI.PageLLBLGenEntity
	{
		#region Methods

		protected override void OnInit(EventArgs e)
		{
            this.LoadUserControls();
            this.DataSourceLoaded += Brands_OnDataSourceLoaded;            
			base.OnInit(e);
		}

        private void Brands_OnDataSourceLoaded(object sender)
        {
            if (PageMode != PageMode.Add)
            {
                BrandRole? brandRole = CmsSessionHelper.GetUserRoleForBrand(this.DataSourceAsBrandEntity.BrandId);
                if (CmsSessionHelper.CurrentRole >= Role.Administrator || (brandRole.HasValue && brandRole.Value == BrandRole.Owner))
                {
                    this.tabsMain.GetTabPageByName("UserBrand").Visible = true;
                }
                else
                {
                    this.tabsMain.GetTabPageByName("UserBrand").Visible = false;
                }

                if (brandRole == null || brandRole <= BrandRole.Editor)
                {
                    this.tabsMain.GetTabPageByName("BrandCultureCollection").Visible = false;
                    this.tabsMain.GetTabPageByName("UserBrand").Visible = false;
                    this.tabsMain.GetTabPageByName("CompanyBrands").Visible = false;
                }

                if (!this.DataSourceAsBrandEntity.CultureCode.IsNullOrEmpty())
                {
                    this.ddlCultureCode.Enabled = false;
                    this.ddlCultureCode.SelectedItem = this.ddlCultureCode.Items.FindByValue(this.DataSourceAsBrandEntity.CultureCode);
                }
            }
        }        

        private void LoadUserControls()
        {
            this.tabsMain.AddTabPage("Cultures", "BrandCultureCollection", "~/Organization/SubPanels/BrandCultureCollection.ascx");

            if (PageMode != PageMode.Add)
            {
                this.tabsMain.AddTabPage("Users", "UserBrand", "~/Generic/Subpanels/UserBrandCollection.ascx");
                this.tabsMain.AddTabPage("Companies", "CompanyBrands", "~/Generic/Subpanels/CompanyBrandCollection.ascx");
                this.tabsMain.AddTabPage("Products", "ProductBrand", "~/Organization/Subpanels/ProductBrandCollection.ascx");
                this.tabsMain.AddTabPage("Alterations V1 & V2", "AlterationBrand", "~/Organization/Subpanels/AlterationBrandCollection.ascx");
                this.tabsMain.AddTabPage("Alterations V3", "AlterationV3Brand", "~/Organization/Subpanels/AlterationV3BrandCollection.ascx");
            }

            IEnumerable<Culture> cultures = Obymobi.Culture.Mappings.Values.OrderBy(x => x.NameAndCultureCode);

            this.ddlCultureCode.DataSource = cultures;
            this.ddlCultureCode.DataBind();
        }

        public override bool Save()
        {
            this.DataSourceAsBrandEntity.CultureCode = this.ddlCultureCode.SelectedValueString;

            if (base.Save())
            {
                if (PageMode == PageMode.Add || this.DataSourceAsBrandEntity.IsNew)
                {
                    this.DataSourceAsBrandEntity.Refetch();

                    UserBrandEntity accountBrandEntity = new UserBrandEntity();
                    accountBrandEntity.BrandId = this.DataSourceAsBrandEntity.BrandId;
                    accountBrandEntity.UserId = CmsSessionHelper.CurrentUser.UserId;
                    accountBrandEntity.Role = BrandRole.Owner;
                    accountBrandEntity.Save();

                    BrandCultureEntity culture = new BrandCultureEntity();
                    culture.BrandId = this.DataSourceAsBrandEntity.BrandId;
                    culture.CultureCode = this.DataSourceAsBrandEntity.CultureCode;
                    culture.Save();
                }

                return true;
            }

            
            return false;
        }

        #endregion

		#region Event handlers

	    protected void Page_Load(object sender, EventArgs e)
	    {
            bool allowedToEdit = CmsSessionHelper.GetUserRoleForBrand(this.DataSourceAsBrandEntity.BrandId).GetValueOrDefault(BrandRole.Viewer) > BrandRole.Editor;            

            if (PageMode != PageMode.Add)
	        {
	            if (!allowedToEdit)
	            {
	                MasterPages.MasterPageEntity masterPage = this.Master as MasterPages.MasterPageEntity;
	                if (masterPage != null)
	                    masterPage.ToolBar.IsReadOnly = true;

	                Validate();
	            }
	        }
	        else if (CmsSessionHelper.CurrentRole < Role.Crave)
	        {
	            throw new ApplicationException("You don't have permissions to add a brand");
	        }
	    }
        
        #endregion

        #region Properties

        private BrandEntity brandEntityDataSource;
        public BrandEntity DataSourceAsBrandEntity
        {
            get
            {
                return this.brandEntityDataSource ?? (this.brandEntityDataSource = this.DataSource as BrandEntity);
            }
        }

        #endregion
    }
}
