﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Cms;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Organization
{
    public partial class Brandalterations : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            MasterPages.MasterPageEntityCollection masterPage = this.Master as MasterPages.MasterPageEntityCollection;
            if (masterPage != null)
            {
                masterPage.SetPageTitle("Brand Product Alterations");
            }

            this.EntityName = "Genericalteration";
            this.EntityPageUrl = "~/Genericproducts/Genericalteration.aspx";
            base.OnInit(e);

            this.ddlBrands.DataSource = CmsSessionHelper.GetBrandCollectionForUser();
            this.ddlBrands.DataBind();
            this.ddlBrands.SelectedIndex = 1;

            this.MainGridView.LoadComplete += MainGridView_LoadComplete;
        }

        void MainGridView_LoadComplete(object sender, EventArgs e)
        {
            if (this.MainGridView.Columns["BrandName"] != null)
            {
                this.MainGridView.Columns["BrandName"].Visible = true;
            }
        }   

        protected void Page_Load(object sender, EventArgs e)
        {
            this.MainGridView.ShowDeleteHyperlinkColumn = false;

            BrandRole? brandRole = CmsSessionHelper.GetUserRoleForBrand(this.ddlBrands.Value.GetValueOrDefault(0));
            bool isEditor = (brandRole.GetValueOrDefault(BrandRole.Viewer) >= BrandRole.Editor);

            MasterPages.MasterPageEntityCollection masterPage = this.Master as MasterPages.MasterPageEntityCollection;
            if (masterPage != null)
            {
                masterPage.ToolBar.AddButton.Visible = isEditor;
            }

            if (this.MainGridView != null)
            {
                this.MainGridView.Styles.Cell.HorizontalAlign = HorizontalAlign.Left;
            }

            LLBLGenProDataSource datasource = this.DataSource as LLBLGenProDataSource;
            if (datasource != null)
            {
                PredicateExpression filter = new PredicateExpression(GenericalterationFields.BrandId == this.ddlBrands.Value);
                datasource.FilterToUse = filter;
                datasource.RelationsToUse = new RelationCollection(GenericalterationEntity.Relations.BrandEntityUsingBrandId);
            }
        }

        #endregion
    }
}
