﻿using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;

namespace Obymobi.ObymobiCms.Organization
{
    public partial class Brands : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "Brand";
            this.EntityPageUrl = "~/Organization/Brand.aspx";

            base.OnInit(e);

            if (CmsSessionHelper.CurrentRole < Role.Crave)
            {
                MasterPages.MasterPageEntityCollection masterPage = this.Master as MasterPages.MasterPageEntityCollection;
                if (masterPage != null)
                    masterPage.ToolBar.AddButton.Visible = false;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.MainGridView.ShowDeleteColumn = false;
            this.MainGridView.ShowDeleteHyperlinkColumn = false;

            if (CmsSessionHelper.CurrentRole < Role.Crave)
            {
                LLBLGenProDataSource datasource = this.DataSource as LLBLGenProDataSource;
                if (datasource != null)
                {
                    PredicateExpression filter = new PredicateExpression(UserBrandFields.UserId == CmsSessionHelper.CurrentUser.UserId);
                    datasource.FilterToUse = filter;
                    datasource.RelationsToUse = new RelationCollection(BrandEntity.Relations.UserBrandEntityUsingBrandId);
                }
            }
        }
    }
}