﻿using System;
using Dionysos.Web;

namespace Obymobi.ObymobiCms.Organization.SubPanels
{
    public partial class ProductBrandCollection : Dionysos.Web.UI.DevExControls.SubPanelLLBLOverviewDataSourceCollection
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            this.EntityName = "Product";
            this.EntityPageUrl = "~/Catalog/Product.aspx";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.btnCreateProduct.Click += btnCreateProduct_Click;
        }

        void btnCreateProduct_Click(object sender, EventArgs e)
        {
            string url = string.Format("{0}?mode={1}&entity={2}&id=&BrandId={3}", this.EntityPageUrl, "add", this.EntityName, this.ParentPrimaryKeyFieldValue);
            WebShortcuts.ResponseRedirect(url, true);
        }
    }
}