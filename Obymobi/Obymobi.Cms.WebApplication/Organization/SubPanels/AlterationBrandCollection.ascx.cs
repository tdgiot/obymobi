﻿using System;
using Dionysos.Web;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Organization.SubPanels
{
    public partial class AlterationBrandCollection : Dionysos.Web.UI.DevExControls.SubPanelLLBLOverviewDataSourceCollection
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            this.EntityName = "Alteration";
            this.EntityPageUrl = "~/Catalog/Alteration.aspx";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.btnCreateAlteration.Click += btnCreateAlteration_Click;

            if (this.DataSource != null)
            {
                PredicateExpression filter = new PredicateExpression();
                filter.Add(AlterationFields.ParentAlterationId == DBNull.Value);
                filter.Add(AlterationFields.Version == 1);

                this.DataSource.FilterToUse = filter;
            }
        }

        void btnCreateAlteration_Click(object sender, EventArgs e)
        {
            string url = string.Format("{0}?mode={1}&entity={2}&id=&BrandId={3}", this.EntityPageUrl, "add", this.EntityName, this.ParentPrimaryKeyFieldValue);
            WebShortcuts.ResponseRedirect(url, true, string.Empty);
        }
    }
}