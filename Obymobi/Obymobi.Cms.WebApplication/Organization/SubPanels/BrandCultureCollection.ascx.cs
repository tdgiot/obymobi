﻿using Dionysos.Web.UI;
using Obymobi.Data.EntityClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.Web;
using Obymobi;
using Dionysos.Web.UI.WebControls;

public partial class Organization_SubPanels_BrandCultureCollection : Dionysos.Web.UI.WebControls.SubPanelLLBLGenEntity
{
    #region Fields

    private readonly List<TextBoxDecimal> cultureTextBoxes = new List<TextBoxDecimal>();

    #endregion

    #region Methods

    protected override void OnInit(EventArgs e)
    {
        this.EntityName = "BrandCulture";
        this.LoadUserControls();
        base.OnInit(e);        
    }

    private void LoadUserControls()
    {
        
    }

    protected override void OnParentDataSourceLoaded()
    {
        base.OnParentDataSourceLoaded();

        this.FillAllCultures();
    }    

    private void FillAllCultures()
    {
        List<string> cultureCodes = this.DataSourceAsBrandEntity.BrandCultureCollection.Select(x => x.CultureCode).ToList();

        this.lbCultures.DataSource = Obymobi.Culture.Mappings.Values.OrderByDescending(x => cultureCodes.Contains(x.Code)).ThenBy(x => x.NameAndCultureCode);
        this.lbCultures.DataBind();

        foreach (ListEditItem item in this.lbCultures.Items)
        {
            if (cultureCodes.Contains(item.Value))
            {
                item.Selected = true;
            }
        }
    }

    public override bool Validate()
    {
        if (this.lbCultures.SelectedItemsCount == 0)
        {
            this.Page.MultiValidatorDefault.AddWarning("At least 1 (one) culture has to be selected");
            return false;
        }

        return true;
    }

    public override bool Save()
    {        
        this.SaveCultureCodes(this.lbCultures.Items);

        return true;
    }

    private void SaveCultureCodes(ListEditItemCollection items)
    {
        string defaultCultureCode = this.DataSourceAsBrandEntity.CultureCode;

        foreach (ListEditItem item in items)
        {
            string cultureCode = item.Value.ToString();

            BrandCultureEntity culture = this.DataSourceAsBrandEntity.BrandCultureCollection.FirstOrDefault(x => x.CultureCode == cultureCode);
            if (culture == null && (item.Selected || cultureCode == defaultCultureCode))
            {
                culture = new BrandCultureEntity();
                culture.BrandId = this.DataSourceAsBrandEntity.BrandId;
                culture.CultureCode = cultureCode;
                culture.Save();
            }
            else if (!item.Selected && culture != null && !culture.CultureCode.Equals(defaultCultureCode, StringComparison.InvariantCultureIgnoreCase))
            {
                culture.Delete();
                this.DataSourceAsBrandEntity.BrandCultureCollection.Remove(culture);
            }
        }
    }    

    #endregion
    
    #region Properties

    public new PageLLBLGenEntity Page
    {
        get
        {
            return base.Page as PageLLBLGenEntity;
        }
    }

    public BrandEntity DataSourceAsBrandEntity
    {
        get
        {
            return this.Page.DataSource as BrandEntity;
        }
    }    

    #endregion
}