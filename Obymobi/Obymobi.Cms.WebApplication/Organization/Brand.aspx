﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Organization.Brand" Codebehind="Brand.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%" EnableViewState="True">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblName">Name</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBox runat="server" ID="tbName" IsRequired="True"/>
							</td>
							<td class="label">
								
							</td>
							<td class="control">
                                
							</td>	
					    </tr>
					    <tr>
					        <td class="label">
					            <D:Label ID="lblCulture" runat="server" Text="Default culture" />
					        </td>
					        <td class="control">
					            <X:ComboBox runat="server" ID="ddlCultureCode" IsRequired="true" TextField="NameAndCultureCode" ValueField="Code"></X:ComboBox>
					        </td>
					    </tr>
					 </table>			
				</Controls>
			</X:TabPage>
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>

