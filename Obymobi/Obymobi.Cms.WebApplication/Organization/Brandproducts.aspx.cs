﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dionysos.Web.UI;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.ObymobiCms.Web.UI;

namespace Obymobi.ObymobiCms.Organization
{
	public partial class Brandproducts : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            MasterPages.MasterPageEntityCollection masterPage = this.Master as MasterPages.MasterPageEntityCollection;
            if (masterPage != null)
            {
                masterPage.SetPageTitle("Brand Products");
            }

            this.EntityName = "Genericproduct";
            this.EntityPageUrl = "~/Genericproducts/Genericproduct.aspx";

            base.OnInit(e);

            this.ddlBrands.DataSource = CmsSessionHelper.GetBrandCollectionForUser();
            this.ddlBrands.DataBind();
            this.ddlBrands.SelectedIndex = 1;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.MainGridView.ShowDeleteHyperlinkColumn = false;

            BrandRole? brandRole = CmsSessionHelper.GetUserRoleForBrand(this.ddlBrands.Value.GetValueOrDefault(0));
            bool isEditor = (brandRole.GetValueOrDefault(BrandRole.Viewer) >= BrandRole.Editor);

            MasterPages.MasterPageEntityCollection masterPage = this.Master as MasterPages.MasterPageEntityCollection;
            if (masterPage != null)
            {   
                masterPage.ToolBar.AddButton.Visible = isEditor;
            }

            LLBLGenProDataSource datasource = this.DataSource as LLBLGenProDataSource;
            if (datasource != null)
            {
                PredicateExpression filter = new PredicateExpression(GenericproductFields.BrandId == this.ddlBrands.Value);
                datasource.FilterToUse = filter;
                datasource.RelationsToUse = new RelationCollection(GenericproductEntity.Relations.BrandEntityUsingBrandId);
            }
        }

        #endregion
    }
}
