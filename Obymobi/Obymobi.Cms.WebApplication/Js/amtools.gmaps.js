/*
 * amTools.GMaps
 *
 * Syntax:
 *
 * var gmaps = new amTools.GMaps();
 *
 * Arguments:
 *
 * element - (element) The draggable elements
 *
 * Options:
 *
 *
 * Functions:
 *
 *
 * Events:
 *
 *
 * Variables:
 *
 * map - (GMap2) Google Maps
 * mgr - (MarkerManager) Open Source marker mananger
 * markerData - (Hash) Key is id
 * markers - (Array) All markers
 *
 */
amTools.GMaps = new Class({
	Implements: [Events, Options],
	
	options: {
		canvas: 'map_canvas',
		zoom: {
			min: 11,
			max: 15
		},
		startPosition: {
			lat: 52,
			lng: 4.2,
			zoom: 11
		},
		largeControls: true,
		onComplete: $empty,
		onChange: $empty
	},
	
	initialize: function(options){
		this.setOptions(options);
		if (!GBrowserIsCompatible()) return false;
		// Initialize GMap2
		this.map = new GMap2(document.getElementById(this.options.canvas), {
			mapTypes: ([G_NORMAL_MAP, G_SATELLITE_MAP, G_HYBRID_MAP]).map(function(mapType){
				mapType.getMinimumResolution = function(){
					return this.options.zoom.min;
				}.bind(this);
				mapType.getMaximumResolution = function(){
					return this.options.zoom.max;
				}.bind(this);
				return mapType;
			}, this),
			backgroundColor: $(this.options.canvas).getStyle('background-color')
		});
		// Initialize variables
		this.markerData = new Hash();
		this.markers = [];
		// Add events
		GEvent.addListener(this.map, 'load', function(){
			// Initialize MarkerManager
			this.mgr = new MarkerManager(this.map);
			this.fireEvent('complete');
			GEvent.addListener(this.map, 'moveend', function(){
				this.fireEvent('change');
			}.bind(this));
			GEvent.addListener(this.map, 'resize', function(){
				this.fireEvent('change');
			}.bind(this));
		}.bind(this));
		// Default settings
		if (this.options.largeControls){
			this.map.addControl(new GLargeMapControl());
		} else {
			this.map.addControl(new GSmallMapControl());
		}
		this.map.addControl(new GMapTypeControl());
		this.map.enableDoubleClickZoom();
		this.map.setCenter(new GLatLng(this.options.startPosition.lat, this.options.startPosition.lng), this.options.startPosition.zoom);
		// Set unloader te reduce browser memory leaks
		window.addEvent('unload', GUnload);
	},
	
	addMarker: function(marker, minZoom, maxZoom){
		// Add marker
		this.markers.push(marker);
		this.mgr.addMarker(marker, minZoom, maxZoom);
	},
	
	addMarkers: function(markers, minZoom, maxZoom){
		// Add markers
		this.markers.extend(markers);
		this.mgr.addMarkers(markers, minZoom, maxZoom);
		this.mgr.refresh();
	},
	
	getMarkers: function(){
		return this.markers;
	},
	
	getActiveMarkers: function(){
		var mapBounds = this.map.getBounds();
		return this.markers.filter(function(marker){
			return mapBounds.containsLatLng(marker.getLatLng());
		});
	},
	
	isActiveMarker: function(marker){
		var mapBounds = this.map.getBounds();
		return mapBounds.containsLatLng(marker.getLatLng());
	},
	
	getPoints: function(){	
		return this.markers.map(function(marker){
			return marker.getLatLng();
		});
	},
	
	getActivePoints: function(){
		this.getActiveMarkers().map(function(marker){
			return marker.getLatLng();
		});
	},
	
	isActivePoint: function(point){
		var mapBounds = this.map.getBounds();
		return mapBounds.containsLatLng(point);
	},
	
	getMarkerData: function(id){
		return this.markerData.get(id);
	}
});