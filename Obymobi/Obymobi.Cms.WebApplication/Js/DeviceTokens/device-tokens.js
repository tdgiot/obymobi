﻿const refreshTimeInMiliseconds = 30000;

var devicesCtx;
var renewedDeviceTokensCtx;
var revokedDeviceTokensCtx;
var runningTasksDeviceTokensCtx;

var devicesChart
var renewedDeviceTokensChart;
var revokedDeviceTokensChart;
var runningTasksDeviceTokensChart;

/*
 * DevExpres
 */
function OnEndCallback(s, e) {
    devicesGrid.SelectRowsByKey(devicesGrid.GetRowKey(e.visibleIndex));

    setNotes("");

    if (e.buttonID === "renewToken") {
        setEventSender(2);
        setTokenValuesOnPopup(true);
    } else if (e.buttonID === "revokeToken") {
        setEventSender(3);
        setTokenValuesOnPopup(false);
    }

    showPopup();
}

function renewSelectedDeviceTokens() {
    setTokenValuesOnPopup(true);
    setEventSender(0);
    showPopup();
}

function revokeSelectedDeviceTokens() {
    setTokenValuesOnPopup(false);
    setEventSender(1);
    showPopup();
}

function setTokenValuesOnPopup(renew) {
    setNotes("");
    if (renew) {
        setHeaderText("Renew device tokens");
        setCreateButtonText("Renew");
    } else {
        setHeaderText("Revoke device tokens");
        setCreateButtonText("Revoke");
    }
}

function setEventSender(eventSender) {
    cbEventSender.SetValue(eventSender);
}

function setNotes(notes) {
    document.getElementById("tbNotes").value = notes;
}

function setCreateButtonText(text) {
    document.getElementById("btnCreateDeviceTokenTask").value = text;
}

function setHeaderText(text) {
    pcPopup.SetHeaderText(text);
}

function showPopup() {
    pcPopup.Show();
}

/*
 * ChartJS
 */
function destroyCharts() {
    if (devicesChart !== undefined) {
        devicesChart.destroy()
    }

    if (renewedDeviceTokensChart !== undefined) {
        renewedDeviceTokensChart.destroy()
    }

    if (revokedDeviceTokensChart !== undefined) {
        revokedDeviceTokensChart.destroy()
    }

    if (runningTasksDeviceTokensChart !== undefined) {
        runningTasksDeviceTokensChart.destroy()
    }
}

function generateChart(chartWrapper, chartContext) {
    let chart = new Chart(chartContext, chartWrapper.chart);

    if (chartWrapper.applyTotalsToLegend) {
        chart.config.options.legend.labels.generateLabels = renderTotalsInLegend;
    }

    if (chartWrapper.hideSlices) {
        let ilen;

        for (i = 0, ilen = (chart.data.datasets || []).length; i < ilen; ++i) {
            let metadata = chart.getDatasetMeta(i);

            for (j = 0; j < metadata.data.length; j++) {
                if (chartWrapper.chart.data.hiddenSlices.includes(j)) {
                    metadata.data[j].hidden = true;
                }
            }
        }
    }

    chart.update();

    return chart;
}

function getModel(chartId) {
    return $.ajax({
        method: "GET",
        url: "DeviceTokens/Handlers/BuildChart.ashx?chartId=" + chartId,
    });
}

function renderTotalsInLegend(chart) {
    var data = chart.data;

    if (data.labels.length && data.datasets.length) {
        return data.labels.map(function (label, i) {
            var meta = chart.getDatasetMeta(0);
            var style = meta.controller.getStyle(i);

            return {
                text: `${label}: ${data.datasets[0].data[i]}`,
                fillStyle: style.backgroundColor,
                strokeStyle: style.borderColor,
                lineWidth: style.borderWidth,
                hidden: isNaN(data.datasets[0].data[i]) || meta.data[i].hidden,
                index: i
            };
        });
    }

    return [];
}

function updateCharts() {
    destroyCharts();

    getModel(1).done(response => {
        devicesChart = generateChart(response, devicesCtx);
    }).fail(error => {
        console.error(error);
    });

    getModel(2).done(response => {
        runningTasksDeviceTokensChart = generateChart(response, runningTasksDeviceTokensCtx);
    }).fail(error => {
        console.error(error);
    });

    getModel(3).done(response => {
        renewedDeviceTokensChart = generateChart(response, renewedDeviceTokensCtx);
    }).fail(error => {
        console.error(error);
    });

    getModel(4).done(response => {
        revokedDeviceTokensChart = generateChart(response, revokedDeviceTokensCtx);
    }).fail(error => {
        console.error(error);
    });
}

/*
 * Global
 */
function initialize() {
    devicesCtx = document.getElementById("sandboxed-devices-chart");
    runningTasksDeviceTokensCtx = document.getElementById("running-tasks-chart");
    renewedDeviceTokensCtx = document.getElementById("renewed-devices-chart");
    revokedDeviceTokensCtx = document.getElementById("revoked-devices-chart");
}

$(function () {
    initialize();
    updateCharts();

    setInterval(updateCharts, refreshTimeInMiliseconds);
});
