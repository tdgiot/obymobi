﻿// First Example
document.addEvent('domready', function () {

    var sorted = false;

    // Apply to all .sortable Elements
    sortableListCollection = $$('ul.sortable');
    $each(sortableListCollection, function (sortableList, index) {
        var sortables = new Sortables(sortableList, {
            constrain: true,
            handle: '.left',
            clone: function (event, e, ul) {

                var toClone = e;
                var clone = toClone.clone()
					.setStyles(toClone.getCoordinates()) // this returns an object with left/top/bottom/right, so its perfect
					.setStyles({ 'opacity': 0.7, 'position': 'absolute', 'z-index': 10000 })
					.inject(e);
                
                // Set all element's on low opacity
                ul.getElements('li').setStyles({ 'opacity': 0.3 });

                // Set to be sorted listitem & the draggable opacity on 100%
                e.setStyles({ 'opacity': 1 });
                clone.setStyles({ 'opacity': 0 });

                // Hide body element and reszie clone, because it was copied with height when holding the body
                var bodyElement = clone.getElement('.body');
                var newHeight = clone.getSize().y - bodyElement.getSize().y;
                bodyElement.setStyle('display', 'none');
                clone.setStyles({ 'height': newHeight + 'px' });

                // Hide the controls in the header
                clone.getElement('.right').setStyle('display', 'none');

                return clone;
            }
        });

        // Event called on drag
        sortables.addEvent('start', function (e) {
            document.body.style.cursor = 'move';
        });

        // Event on drag done
        sortables.addEvent('complete', function (e) {
            document.body.style.cursor = 'default';

            // Reset styles after since is done:
            e.getParent('ul').getElements('li').setStyles({ 'opacity': 1 });

            // Check if any sorting happened			
            if (sorted)
                amSortablePanel.handleSortingComplete(sortables, e, sortableList);
        });

        // Event called when any positions have been changed.
        sortables.addEvent('sort', function (e) {
            sorted = true;
        });

        // Make all listitems collapsable		
        listItems = sortableList.getElements('li');

        // Do actions which are needed per list item
        $each(listItems, function (listItem, index) {

            listItem.getElement('.collapse span').set('html', $('collapseText').get('text'));

            // Attach click event for moveup
            var linkMoveUp = listItem.getElements('a.moveup')
            linkMoveUp.addEvent('click', function (e) { amSortablePanel.moveItem(sortables, listItem, true) });

            // Attach click event for move down
            var linkMoveDown = listItem.getElements('a.movedown');
            linkMoveDown.addEvent('click', function (e) { amSortablePanel.moveItem(sortables, listItem, false) });

            // Add events to delete buttons
            listItem.getElements('a.delete').addEvent('click', function (e) { if (confirm('Weet u zeker dat u het item wilt verwijderen?')) { amSortablePanel.handleDeleteItem(listItem) } });

            // Create collapsing panel
            var verticalSlider = new Fx.Slide(listItem.getElement('.body'));

            // Create the button for collapsing
            listItem.getElements('a.collapse').addEvent('click', function (e) {
                e.stop();
                verticalSlider.toggle();
            });

            // Add event to communicate back when done & change text of link
            verticalSlider.addEvent('complete', function (e) {

                // Process on server							
                amSortablePanel.handleSliderComplete(e, verticalSlider.open);

                // Arrange switching button from collapse to expand
                iconImage = listItem.getElement('.collapse img');
                iconImageSrc = iconImage.get('src').toString();

                var buttonText = "";
                // Determine new values for the button				
                if (verticalSlider.open) {
                    buttonText = $('collapseText').get('text'); // 'inklappen';
                    iconImageSrc.replace('expand', 'collapse');
                }
                else {
                    buttonText = $('expandText').get('text');
                    iconImageSrc.replace('collapse', 'expand');
                }

                // Set new values
                listItem.getElement('.collapse span').set('html', buttonText);
                iconImage.set('src', iconImageSrc);
            });
        });
    });
});	
	

	
var amSortablePanel = {

	_getFunctionName : function(list)
	{		
		var functionPrefix = list.get('rel').toString();		
		if(functionPrefix.length = 0)
			alert('Er is voor de sorteerbare lijst geen \'rel\' ingevuld als prefix van de callback verwerkte methode');					
		var funcName = functionPrefix.toString() + "DoCallBack";
		    				
        if (typeof eval(funcName) == 'function') {			
			// Ok, function found
		}					
		else
		{			
			alert('CallBack methode niet gevonden');
		}		    				
		    						
		return funcName;
		
	},

	handleSortingComplete : function(sortable, listItem, underlyingList)		
	{
	    // Do overlay effect
		var clone = listItem.clone()
			.setStyles(listItem.getCoordinates()) // this returns an object with left/top/bottom/right, so its perfect
			.setStyles({ 'opacity': 0.3, 'position': 'absolute', 'z-index': 10000 })
			.inject(listItem.getParent('ul'));		
			
		// Make empty and red
		clone.getElements("div").destroy();					
		clone.setStyle("background-color", "#ff0000");						
		
		var listItemFx = new Fx.Tween(clone);
		listItemFx.set('tween', {duration: 5});					
		listItemFx.start('opacity', 0).chain(function() { clone.destroy(); });		
		
		var argumentToProces = 'sorted$id:' + underlyingList.get('id') + ';sorting:' + sortable.serialize() + ';';		
		var funcName = amSortablePanel._getFunctionName(underlyingList);							

        // Call function
        eval(funcName + "('" + argumentToProces  + "', '" + funcName  + "')");		
	},
		
	handleSliderComplete : function(slidedElement, isOpen)
	{		
	    //console.log('slidercomplete');
		holdingListItem = slidedElement.getParent('li');											
		var argumentToProces = 'slider$id:' + holdingListItem.get('id') + ';open:' + isOpen + ';';
						
		holdingList = holdingListItem.getParent('ul'); 				
		var funcName = amSortablePanel._getFunctionName(holdingList);							
		
        // Call function
		eval(funcName + "('" + argumentToProces  + "', '" + funcName  + "')");					
	},	
	
	handleDeleteItem: function(listItem) {		
	    //console.log('delete:' + listItem.get('id'));
	    var argumentToProces = 'delete$id:' + listItem.get('id');
	    
		holdingList = listItem.getParent('ul'); 				
		var funcName = amSortablePanel._getFunctionName(holdingList);							
		
        // Call function
		eval(funcName + "('" + argumentToProces  + "', '" + funcName  + "')");						    
	    
	    listItem.destroy();
	},	
	
	closeSliderPanel : function(listItemId)
	{	    
	    document.addEvent('domready', function() {	        
	        listItem = $(listItemId);
	        var verticalSlider = new Fx.Slide(listItem.getElement('.body'));			
	        verticalSlider.hide();
	        
            iconImage = listItem.getElement('.collapse img');
            iconImageSrc = iconImage.get('src').toString();

            buttonText = 'uitklappen';
	        iconImageSrc.replace('collapse','expand');				
            
            listItem.getElement('.collapse span').set('html', buttonText);							
            iconImage.set('src', iconImageSrc);						    
	    });
	},	
	
	moveItem: function(fromSortables, listItem, moveUp)	{	
		// First find the position of the item
		// Get UL of this item				
		var holdingListItem = listItem;
		var currentLiId = holdingListItem.get('id');
		var holdingList = holdingListItem.getParent('ul');
						
		// Get all listItems
		listItems = holdingList.getElements('li');
		
		// Loop till we find this one				
		itemIndex = -1;		
		//for(var i = 0; i < listItems.length; i++) 
		//{
		listItems.each(function(listItem, index) {							
			if(listItem.id == currentLiId)
			{								
				itemIndex = index;					
			}
		//}	
		});					
		
		if(itemIndex == -1)
		{
			alert('Het item kon niet worden verplaatst');
		}
		else
		{			
			// Move to correct location
			if(moveUp)
			{
				if(itemIndex != 0)
				{									
					holdingListItem.inject(listItems[itemIndex-1], 'before');				
				}
			}
			else
			{
				if(itemIndex < listItems.length-1)
				{
					holdingListItem.inject(listItems[itemIndex+1], 'after');									
				}
			}
									
			// Call the function to process this change			
			amSortablePanel.handleSortingComplete(fromSortables, document.getElementById(holdingListItem.id), holdingList);			
		}		
	}
};			