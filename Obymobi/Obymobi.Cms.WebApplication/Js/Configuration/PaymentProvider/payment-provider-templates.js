﻿function setTemplate(ddl) {
    if (ddl.value === "0") {
        return;
    }

    var receiptTemplate = '';
    var footerTemplate = '';

    switch (ddl.value) {
        case '2':
            receiptTemplate = getEnglishReceiptTemplate();
            footerTemplate = getEnglishFooterTemplate();
            break;
        case '3':
            receiptTemplate = getAmericanReceiptTemplate();
            footerTemplate = getAmericanFooterTemplate();
            break;
        case '1':
        default:
            receiptTemplate = getDutchReceiptTemplate();
            footerTemplate = getDutchFooterTemplate();
            break;
    }

    receiptTemplate += '\n';
    receiptTemplate += 'ttsupport@crave-emenu.com';

    document.getElementById("tbPaymentProcessorInfo").value = receiptTemplate;
    document.getElementById("tbPaymentProcessorInfoFooter").value = footerTemplate;

    ddl.selectedIndex = 0;
}

function getDutchReceiptTemplate() {
    var text = 'Crave Interactive B.V. trading as TakeawayToday';
    text += '\n';
    text += 'Naaldwijk, Netherlands';

    return text;
}

function getDutchFooterTemplate() {
    return 'Processed by Crave Interactive B.V. (NL) - Registered in the Netherlands 50259407';
}

function getEnglishReceiptTemplate() {
    var text = "Crave Interactive Ltd. trading as TakeawayToday";
    text += '\n';
    text += 'Cranfield, United Kingdom';

    return text;
}

function getEnglishFooterTemplate() {
    return 'Orders Processed by Crave Interactive Ltd (UK) - Registered in England 07035427';
}

function getAmericanReceiptTemplate() {
    var text = 'Crave Interactive Inc. trading as TakeawayToday';
    text += '\n';
    text += 'New York, USA';

    return text;
}

function getAmericanFooterTemplate() {
    return 'Orders processed by Crave Interactive Inc. - registered in New York, USA';
}
