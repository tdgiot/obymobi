var pageIsDirty = false; // Keep this for backward-compatibility
var dirtyComponents = {};

var dirtyComponentOptions = {
    ignoreAttribute: 'notdirty',
    ignoreTypes: 'hidden|button|submit'
};

function changeTrackingDisabled(el) {
    // is element or parent marked with ignore attribute
    if (el.getAttribute(dirtyComponentOptions.ignoreAttribute)) return true;
    if (el.parentNode.getAttribute(dirtyComponentOptions.ignoreAttribute)) return true;
    if (el.classList.contains('dxeEditAreaSys')) {
        var parentTable = el.closest('table');
        if (parentTable && parentTable.getAttribute(dirtyComponentOptions.ignoreAttribute)) return true;
    }

    // is element type in the ignore list
    if (dirtyComponentOptions.ignoreTypes.includes(el.type)) return true;

    // is gridview header filter
    if (el.name && el.name.includes('DXFREditorcol')) return true;
    
    return false;
}

function trackChanges(tagName) {
    var elements = document.getElementsByTagName(tagName);

    for (let el of elements) {
        if (changeTrackingDisabled(el)) {
            continue;
        }

        var eventType = el.classList.contains('dxeEditAreaSys') ? 'blur' : 'change';
        el.addEvent(eventType, function () { componentChanged(this); });
    }
}

window.addEvent('domready', function () {
    trackChanges('input');
    trackChanges('textarea');
    trackChanges('select');

    window.onbeforeunload = showPageIsDirtyWarning;
});

function dirtyPageReset(s, e) {
    pageIsDirty = false;
    dirtyComponents = {};
}

function updatePageState() {
    pageIsDirty = Object.keys(dirtyComponents).length > 0;

    if (pageIsDirty) {
        makePageDirty();
    } else {
        clearPageDirty();
    }
}

function getDefaultValue(el) {
    if (el.type == 'checkbox') {
        return el.defaultChecked;
    } else {
        return el.defaultValue;
    }
}

function getCurrentValue(el) {
    if (el.type == 'checkbox') {
        return el.checked;
    } else {
        return el.value;
    }
}

function hasChanged(el) {
    var defaultValue = getDefaultValue(el);
    var currentValue = getCurrentValue(el);
    return defaultValue !== currentValue;
}

function componentChanged(el) {
    var currentValue = getCurrentValue(el);

    if (dirtyComponents.hasOwnProperty(el.id)) {
        var previousValue = dirtyComponents[el.id];
        if (previousValue === currentValue) {
            delete dirtyComponents[el.id];
            updatePageState();
        }
    } else if (hasChanged(el)) {
        dirtyComponents[el.id] = getDefaultValue(el);
        updatePageState();
    }
}

function makePageDirty() {
    if (pageIsDirty) {
	    $(document.body).getElements('.ToolbarSave span').setStyle('color', '#ff0000');
	    $(document.body).getElements('.ToolbarSave span').setStyle('font-weight', 'bold');
    }
}

function clearPageDirty() {
	$(document.body).getElements('.ToolbarSave span').setStyle('color', '#000000');
	$(document.body).getElements('.ToolbarSave span').setStyle('font-weight', 'normal');
}

function showPageIsDirtyWarning(e) {
    console.log("Show page is dirty warning?: " + pageIsDirty);
    if (pageIsDirty) {
        e.returnValue = 'There are changes on this page which aren\'t saved yet, are you sure you want to continue without saving these changes first?';
        return e.returnValue;
    }
}

var isPostbackInitiated = false;
function OnToolBarClick(s, e) {
    e.processOnServer = !isPostbackInitiated;
    isPostbackInitiated = true;
} 
