/*!
 * amTools
 * Copyright (c) 2008, AMteam vormgevers + programmeurs
 */
var amTools = {
	version: '0.3'
};



Number.implement({
	format: function(decimals, dec_point, thousands_sep){
		decimals = Math.abs(decimals) + 1 ? decimals : 2;
		dec_point = dec_point || ',';
		thousands_sep = thousands_sep || '.';
	
		var matches = /(-)?(\d+)(\.\d+)?/.exec((isNaN(this) ? 0 : this) + ''); // returns matches[1] as sign, matches[2] as numbers and matches[2] as decimals
		var remainder = matches[2].length > 3 ? matches[2].length % 3 : 0;
		return (matches[1] ? matches[1] : '') + (remainder ? matches[2].substr(0, remainder) + thousands_sep : '') + matches[2].substr(remainder).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep) + (decimals ? dec_point + (+matches[3] || 0).toFixed(decimals).substr(2) : '');
	}
});