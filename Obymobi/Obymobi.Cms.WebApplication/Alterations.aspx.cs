﻿using System;
using System.Linq;
using Dionysos.Web;
using Obymobi.Data.EntityClasses;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Data.CollectionClasses;
using Obymobi.Logic.HelperClasses;
using Dionysos.Data.LLBLGen;
using System.Collections.Generic;
using System.IO;
using System.Drawing;
using Dionysos;
using Obymobi.Web.CloudStorage;

public partial class Alterations : System.Web.UI.Page
{
    private static AlterationType[] regularAlterationTypes = { AlterationType.Options, AlterationType.Date, AlterationType.DateTime, AlterationType.Email, AlterationType.Numeric, AlterationType.Text, AlterationType.Instruction };

    protected void Page_Load(object sender, EventArgs e)
    {
        int alterationId = -1;
        int companyId = -1;
        bool preview = false;
        bool convert = false;

        QueryStringHelper.TryGetValue<int>("AlterationId", out alterationId);
        QueryStringHelper.TryGetValue<int>("CompanyId", out companyId);
        QueryStringHelper.TryGetValue<bool>("Preview", out preview);
        QueryStringHelper.TryGetValue<bool>("Convert", out convert);

        if (alterationId > 0)
        {
            if (preview)
            {
                this.PreviewAlteration(alterationId);
            }
            else if (convert)
            {
                this.ConvertAlteration(alterationId);
            }
            else
            {
                this.DisplayAlteration(alterationId);
            }
        }
        else if (companyId > 0)
        {
            if (convert)
            {
                //this.ConvertAlterations(companyId, true);
                this.ConvertAlterations(companyId, false);
            }
            else
            {
                this.ListPackageAlterations(companyId);
            }
        }
    }

    #region Display

    private void DisplayAlteration(int alterationId)
    {
        AlterationEntity alterationEntity = new AlterationEntity(alterationId);
        if (alterationEntity.IsNew)
            return;

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.AppendFormat("<ul>");
        stringBuilder.AppendFormat("<li>");

        stringBuilder.AppendFormat(alterationEntity.Name);
        stringBuilder.AppendFormat(" [" + alterationEntity.Type.ToString() + "]");

        stringBuilder.AppendFormat(" <a href='{0}'>Preview</a>", WebShortcuts.ResolveUrl("~/Alterations.aspx?AlterationId={0}&Preview=true", alterationId));
        stringBuilder.AppendFormat(" <a href='{0}'>Convert</a>", WebShortcuts.ResolveUrl("~/Alterations.aspx?AlterationId={0}&Convert=true", alterationId));

        stringBuilder.AppendFormat(this.RenderChildren(alterationEntity));

        stringBuilder.AppendFormat("</li>");
        stringBuilder.AppendFormat("</ul>");

        this.plhOutput.AddHtml(stringBuilder.ToString());
    }

    private string RenderChildren(AlterationEntity alterationEntity)
    {
        string html = string.Empty;

        if (alterationEntity.AlterationCollection.Count > 0)
        {
            html += "<ul>";

            foreach (AlterationEntity childAlterationEntity in alterationEntity.AlterationCollection)
            {
                html += "<li>";
                html += childAlterationEntity.Name;
                html += string.Format(" [" + childAlterationEntity.Type.ToString() + "]");

                html += this.RenderChildren(childAlterationEntity);
                html += "</li>";
            }

            html += "</ul>";
        }

        if (alterationEntity.AlterationProductCollection.Count > 0)
        {
            html += "<ul>";

            foreach (AlterationProductEntity childAlterationProductEntity in alterationEntity.AlterationProductCollection)
            {
                html += "<li>";
                html += childAlterationProductEntity.ProductEntity.Name;

                foreach (ProductAlterationEntity grandChildProductAlterationEntity in childAlterationProductEntity.ProductEntity.ProductAlterationCollection)
                {
                    this.RenderChildren(grandChildProductAlterationEntity.AlterationEntity);
                }

                html += "</li>";
            }

            html += "</ul>";
        }

        return html;
    }

    #endregion

    #region Preview

    private void PreviewAlteration(int alterationId)
    {
        AlterationEntity alterationEntity = new AlterationEntity(alterationId);
        if (alterationEntity.IsNew || alterationEntity.ParentAlterationId.HasValue)
            return;

        List<AlterationEntity> bottomLevelAlterations = new List<AlterationEntity>();
        if (Alterations.regularAlterationTypes.Contains(alterationEntity.Type))
        {
            bottomLevelAlterations.Add(alterationEntity);
        }
        else if (alterationEntity.Type == AlterationType.Package)
        {
            this.GetBottomLevelAlterations(alterationEntity, bottomLevelAlterations);
        }

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.AppendFormat("<ul>");

        foreach (AlterationEntity bottomLevelAlterationEntity in bottomLevelAlterations)
        {
            stringBuilder.AppendFormat("<li>");
            stringBuilder.AppendFormat(bottomLevelAlterationEntity.Name);
            stringBuilder.AppendFormat(" [" + bottomLevelAlterationEntity.Type + "]");
            stringBuilder.AppendFormat("</li>");
        }

        stringBuilder.AppendFormat("</ul>");

        this.plhOutput.AddHtml(stringBuilder.ToString());
    }

    #endregion

    #region Convert

    private void ConvertAlterations(int companyId, bool test)
    {
        this.ConvertPackageAlterations(companyId, test);
    }

    private void ConvertPackageAlterations(int companyId, bool test)
    {
        PredicateExpression filter = new PredicateExpression(AlterationFields.CompanyId == companyId);
        filter.Add(AlterationFields.ParentAlterationId == DBNull.Value);
        filter.Add(AlterationFields.Version != 2);

        AlterationCollection alterationCollection = new AlterationCollection();
        alterationCollection.GetMulti(filter);

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.AppendFormat("<ul>");

        foreach (AlterationEntity alterationEntity in alterationCollection)
        {
            if (test)
            {
                List<AlterationEntity> bottomLevelAlterations = new List<AlterationEntity>();
                this.GetBottomLevelAlterations(alterationEntity, bottomLevelAlterations);

                for (int i = 0; i < bottomLevelAlterations.Count; i++)
                {
                    AlterationEntity bottomLevelAlterationEntity = bottomLevelAlterations[i];

                    if (bottomLevelAlterationEntity.Type == AlterationType.Category)
                    {
                        if (bottomLevelAlterations.All(x => x.Type == AlterationType.Category))
                        {
                        }
                        else
                        {
                        }
                    }
                    else if (bottomLevelAlterationEntity.Type == AlterationType.Options)
                    {
                    }
                    else if (bottomLevelAlterationEntity.Type == AlterationType.Instruction)
                    {
                    }
                    else if (bottomLevelAlterationEntity.Type == AlterationType.Image)
                    {
                    }
                    else if (bottomLevelAlterationEntity.Type == AlterationType.DateTime)
                    {
                    }
                    else if (bottomLevelAlterationEntity.Type == AlterationType.Date)
                    {
                    }
                    else if (bottomLevelAlterationEntity.Type == AlterationType.Notes)
                    {
                    }
                    else if (bottomLevelAlterationEntity.Type == AlterationType.Text)
                    {
                    }
                    else if (bottomLevelAlterationEntity.Type == AlterationType.Numeric)
                    {
                    }
                    else if (bottomLevelAlterationEntity.Type == AlterationType.Email)
                    {
                    }
                    else
                    {
                        stringBuilder.AppendFormat("<li>");
                        stringBuilder.AppendFormat(alterationEntity.Name);
                        stringBuilder.AppendFormat("&nbsp;<a href='{0}'>Current</a>", WebShortcuts.ResolveUrl("~/Alterations.aspx?AlterationId={0}", alterationEntity.AlterationId));
                        stringBuilder.AppendFormat("</li>");
                        break;
                    }
                }
            }
            else
            {
                foreach (ProductAlterationEntity productAlterationEntity in alterationEntity.ProductAlterationCollection)
                {
                    if (!productAlterationEntity.ProductEntity.ProductAlterationCollection.Any(x => x.Version == 2))
                    {
                        this.ConvertAlteration(alterationEntity.AlterationId);
                        break;
                    }
                }
            }
        }

        stringBuilder.AppendFormat("</ul>");

        this.plhOutput.AddHtml(stringBuilder.ToString());
    }

    private void ConvertFormAlterations(int companyId, bool test)
    {
        PredicateExpression filter = new PredicateExpression(AlterationFields.CompanyId == companyId);

        PredicateExpression typeFilter = new PredicateExpression();
        typeFilter.AddWithOr(AlterationFields.Type == AlterationType.Text);
        typeFilter.AddWithOr(AlterationFields.Type == AlterationType.Numeric);
        typeFilter.AddWithOr(AlterationFields.Type == AlterationType.Email);

        AlterationCollection alterationCollection = new AlterationCollection();
        alterationCollection.GetMulti(filter);

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.AppendFormat("<ul>");

        foreach (AlterationEntity alterationEntity in alterationCollection)
        {
        }

        stringBuilder.AppendFormat("</ul>");

        this.plhOutput.AddHtml(stringBuilder.ToString());
    }

    private void ConvertAlteration(int alterationId)
    {
        AlterationEntity sourceAlterationEntity = new AlterationEntity(alterationId);
        if (sourceAlterationEntity.IsNew || sourceAlterationEntity.ParentAlterationId.HasValue)
            return;

        List<AlterationEntity> bottomLevelAlterations = new List<AlterationEntity>();
        if (Alterations.regularAlterationTypes.Contains(sourceAlterationEntity.Type))
        {
            bottomLevelAlterations.Add(sourceAlterationEntity);
        }
        else if (sourceAlterationEntity.Type == AlterationType.Package)
        {
            this.GetBottomLevelAlterations(sourceAlterationEntity, bottomLevelAlterations);
        }

        List<AlterationEntity> optionAlterations = new List<AlterationEntity>();

        for (int i = 0; i < bottomLevelAlterations.Count; i++)
        {
            AlterationEntity bottomLevelAlterationEntity = bottomLevelAlterations[i];

            if (bottomLevelAlterationEntity.Type == AlterationType.Category)
            {
                AlterationEntity optionAlterationEntity = this.ConvertCategoryAlterationToOptionsAlteration(sourceAlterationEntity, bottomLevelAlterationEntity);
                if (optionAlterationEntity != null)
                    optionAlterations.Add(optionAlterationEntity);
            }
            else if (bottomLevelAlterationEntity.Type == AlterationType.Options)
            {
                AlterationEntity optionAlterationEntity = this.ConvertOptionsAlteration(sourceAlterationEntity, bottomLevelAlterationEntity);
                if (optionAlterationEntity != null)
                    optionAlterations.Add(optionAlterationEntity);
            }
            else if (bottomLevelAlterationEntity.Type == AlterationType.Instruction)
            {
                this.ConvertInstructionAlteration(sourceAlterationEntity, bottomLevelAlterationEntity);
            }
            else if (bottomLevelAlterationEntity.Type == AlterationType.Image)
            {
                if (optionAlterations.Count > 0)
                    this.ConvertImageAlteration(sourceAlterationEntity, bottomLevelAlterationEntity, optionAlterations[optionAlterations.Count-1]);
            }
            else if (bottomLevelAlterationEntity.Type == AlterationType.DateTime)
            {
                this.ConvertDateTimeAlteration(sourceAlterationEntity, bottomLevelAlterationEntity);
            }
            else if (bottomLevelAlterationEntity.Type == AlterationType.Date)
            {
                this.ConvertDateAlteration(sourceAlterationEntity, bottomLevelAlterationEntity);
            }
            else if (bottomLevelAlterationEntity.Type == AlterationType.Notes)
            {
                this.ConvertNotesAlteration(sourceAlterationEntity, bottomLevelAlterationEntity);
            }
            else if (bottomLevelAlterationEntity.Type == AlterationType.Text)
            {
                this.ConvertTextAlteration(sourceAlterationEntity, bottomLevelAlterationEntity);
            }
            else if (bottomLevelAlterationEntity.Type == AlterationType.Numeric)
            {
                this.ConvertNumericAlteration(sourceAlterationEntity, bottomLevelAlterationEntity);
            }
            else if (bottomLevelAlterationEntity.Type == AlterationType.Email)
            {
                this.ConvertEmailAlteration(sourceAlterationEntity, bottomLevelAlterationEntity);
            }
        }
    }

    private AlterationEntity ConvertCategoryAlterationToOptionsAlteration(AlterationEntity sourceAlterationEntity, AlterationEntity bottomLevelAlterationEntity)
    {
        // Copy the alteration
        AlterationEntity alterationEntity = new AlterationEntity();
        LLBLGenEntityUtil.CopyFields(bottomLevelAlterationEntity, alterationEntity);
        alterationEntity.ParentAlterationId = null;
        alterationEntity.Version = 2;
        alterationEntity.Type = AlterationType.Options;

        if (alterationEntity.Save())
        {
            alterationEntity.Refetch();

            foreach (AlterationProductEntity alterationProductEntity in bottomLevelAlterationEntity.AlterationProductCollection)
            {
                // Create the alteration option
                AlterationoptionEntity alterationoptionEntity = new AlterationoptionEntity();
                alterationoptionEntity.Version = 2;
                alterationoptionEntity.Name = alterationProductEntity.Name;
                alterationoptionEntity.CompanyId = sourceAlterationEntity.CompanyId;
                alterationoptionEntity.PriceIn = alterationProductEntity.ProductEntity.PriceIn;
                if (alterationoptionEntity.Save())
                {
                    alterationoptionEntity.Refetch();

                    // Create the alteration item
                    AlterationitemEntity alterationitemEntity = new AlterationitemEntity();
                    alterationitemEntity.Version = 2;
                    alterationitemEntity.AlterationId = alterationEntity.AlterationId;
                    alterationitemEntity.AlterationoptionId = alterationoptionEntity.AlterationoptionId;                    
                    if (alterationitemEntity.Save())
                    {
                        alterationitemEntity.Refetch();

                        int count = 1;
                        foreach (ProductAlterationEntity temp in alterationProductEntity.ProductEntity.ProductAlterationCollection)
                        {
                            AlterationEntity originalNestedAlterationEntity = new AlterationEntity(temp.AlterationId);
                            if (!originalNestedAlterationEntity.IsNew)
                            {
                                AlterationEntity nestedAlterationEntity = CopyAlteration(originalNestedAlterationEntity);
                                if (nestedAlterationEntity != null)
                                {
                                    AlterationitemAlterationEntity alterationitemAlterationEntity = new AlterationitemAlterationEntity();
                                    alterationitemAlterationEntity.AlterationitemId = alterationitemEntity.AlterationitemId;
                                    alterationitemAlterationEntity.AlterationId = nestedAlterationEntity.AlterationId;
                                    alterationitemAlterationEntity.SortOrder = count;
                                    alterationitemAlterationEntity.Save();
                                }
                            }
                        }
                    }
                }
            }

            // Link the newly created alteration to the products it belongs to
            foreach (ProductAlterationEntity temp in sourceAlterationEntity.ProductAlterationCollection)
            {
                if (!temp.ProductEntity.ProductAlterationCollection.Any(x => x.AlterationId == alterationEntity.AlterationId))
                {
                    ProductAlterationEntity productAlterationEntity = new ProductAlterationEntity();
                    productAlterationEntity.Version = 2;
                    productAlterationEntity.ProductId = temp.ProductId;
                    productAlterationEntity.AlterationId = alterationEntity.AlterationId;
                    productAlterationEntity.Save();
                }
            }

            return alterationEntity;
        }

        return null;
    }

    private void ConvertCategoryAlterationToProductgroup(AlterationEntity sourceAlterationEntity, AlterationEntity bottomLevelAlterationEntity)
    {
        // Link the newly created alteration to the products it belongs to
        foreach (ProductAlterationEntity temp in sourceAlterationEntity.ProductAlterationCollection)
        {
            ProductEntity productEntity = temp.ProductEntity;

            if (!productEntity.ProductgroupId.HasValue && productEntity.CompanyId.HasValue)
            {
                // Create a productgroup for the product
                ProductgroupEntity productgroupEntity = new ProductgroupEntity();
                productgroupEntity.CompanyId = productEntity.CompanyId.Value;
                productgroupEntity.Name = productEntity.Name;
                if (productgroupEntity.Save())
                {
                    productgroupEntity.Refetch();

                    // Link the productgroup to the product
                    productEntity.ProductgroupId = productgroupEntity.ProductgroupId;
                    productEntity.Save();

                    foreach (ProductAlterationEntity productAlterationEntity in productEntity.ProductAlterationCollection)
                    {
                        this.CreateProductgroupForChild(productAlterationEntity.AlterationEntity, productgroupEntity.ProductgroupId);
                    }
                }
            }
        }
    }

    private void CreateProductgroupForChild(AlterationEntity alterationEntity, int parentProductgroupId)
    {
        if (alterationEntity.Type == AlterationType.Category)
        {
            // Create the productgroup
            ProductgroupEntity productgroupEntity = new ProductgroupEntity();
            productgroupEntity.CompanyId = alterationEntity.CompanyId.Value;
            productgroupEntity.Name = alterationEntity.Name;
            if (productgroupEntity.Save())
            {
                productgroupEntity.Refetch();

                ProductgroupItemEntity parentProductgroupItemEntity = new ProductgroupItemEntity();
                parentProductgroupItemEntity.ProductgroupId = parentProductgroupId;
                parentProductgroupItemEntity.NestedProductgroupId = productgroupEntity.ProductgroupId;
                parentProductgroupItemEntity.Save();

                parentProductgroupId = productgroupEntity.ProductgroupId;

                if (alterationEntity.AlterationProductCollection.Count > 0)
                {
                    for (int i = 0; i < alterationEntity.AlterationProductCollection.Count; i++)
                    {
                        AlterationProductEntity alterationProductEntity = alterationEntity.AlterationProductCollection[i];

                        ProductgroupItemEntity productgroupItemEntity = new ProductgroupItemEntity();
                        productgroupItemEntity.ProductgroupId = parentProductgroupId;
                        productgroupItemEntity.ProductId = alterationProductEntity.ProductId;
                        productgroupItemEntity.SortOrder = i+1;
                        productgroupItemEntity.Save();
                    }
                }
            }
        }

        if (alterationEntity.AlterationCollection.Count > 0)
        {
            foreach (AlterationEntity childAlterationEntity in alterationEntity.AlterationCollection)
            {
                this.CreateProductgroupForChild(childAlterationEntity, parentProductgroupId);
            }
        }
    }

    private AlterationEntity CopyAlteration(AlterationEntity originAlterationEntity)
    {
        AlterationEntity alterationEntity = null;

        PredicateExpression filter = new PredicateExpression();
        filter.Add(AlterationFields.Version == 2);
        filter.Add(AlterationFields.OriginalAlterationId == originAlterationEntity.AlterationId);

        AlterationCollection convertedAlterations = new AlterationCollection();
        convertedAlterations.GetMulti(filter);
        if (convertedAlterations.Count > 0)
        {
            // We already copied and converted this alteration before, so reuse it here
            alterationEntity = convertedAlterations[0];
        }
        else
        {
            // Copy the alteration
            alterationEntity = new AlterationEntity();
            LLBLGenEntityUtil.CopyFields(originAlterationEntity, alterationEntity);
            alterationEntity.ParentAlterationId = null;
            alterationEntity.Version = 2;
            alterationEntity.OriginalAlterationId = originAlterationEntity.AlterationId;

            if (alterationEntity.Save())
            {
                alterationEntity.Refetch();

                // Set the options
                foreach (AlterationitemEntity originalAlterationitemEntity in originAlterationEntity.AlterationitemCollection)
                {
                    AlterationoptionEntity alterationoptionEntity = new AlterationoptionEntity();
                    LLBLGenEntityUtil.CopyFields(originalAlterationitemEntity.AlterationoptionEntity, alterationoptionEntity);
                    alterationoptionEntity.Version = 2;
                    if (alterationoptionEntity.Save())
                    {
                        alterationoptionEntity.Refetch();

                        AlterationitemEntity alterationitemEntity = new AlterationitemEntity();
                        alterationitemEntity.Version = 2;
                        alterationitemEntity.AlterationId = alterationEntity.AlterationId;
                        alterationitemEntity.AlterationoptionId = alterationoptionEntity.AlterationoptionId;
                        alterationitemEntity.SelectedOnDefault = originalAlterationitemEntity.SelectedOnDefault;
                        alterationitemEntity.SortOrder = originalAlterationitemEntity.SortOrder;
                        alterationitemEntity.Save();
                    }
                }
            }
            else
            {
                alterationEntity = null;
            }
        }
        return alterationEntity;
    }

    private AlterationEntity ConvertOptionsAlteration(AlterationEntity sourceAlterationEntity, AlterationEntity bottomLevelAlterationEntity)
    {
        // Copy the alteration
        AlterationEntity alterationEntity = CopyAlteration(bottomLevelAlterationEntity);
        if (alterationEntity != null)
        {
            // Link the newly created alteration to the products it belongs to
            foreach (ProductAlterationEntity temp in sourceAlterationEntity.ProductAlterationCollection)
            {
                if (!temp.ProductEntity.ProductAlterationCollection.Any(x => x.AlterationId == alterationEntity.AlterationId))
                {
                    ProductAlterationEntity productAlterationEntity = new ProductAlterationEntity();
                    productAlterationEntity.Version = 2;
                    productAlterationEntity.ProductId = temp.ProductId;
                    productAlterationEntity.AlterationId = alterationEntity.AlterationId;
                    productAlterationEntity.Save();
                }
            }

            return alterationEntity;
        }

        return null;
    }

    private void ConvertInstructionAlteration(AlterationEntity sourceAlterationEntity, AlterationEntity bottomLevelAlterationEntity)
    {
        if (!bottomLevelAlterationEntity.Description.IsNullOrWhiteSpace())
        {
            // Set the instruction on the product
            foreach (ProductAlterationEntity temp in sourceAlterationEntity.ProductAlterationCollection)
            {
                if (!temp.ProductEntity.CustomTextCollection.Any(x => x.Type == CustomTextType.ProductInstructionsText))
                {
                    CustomTextEntity customTextEntity = new CustomTextEntity();
                    customTextEntity.CultureCode = temp.ProductEntity.CompanyEntity.CultureCode;
                    customTextEntity.Text = bottomLevelAlterationEntity.Description;
                    customTextEntity.Type = CustomTextType.ProductInstructionsText;
                    customTextEntity.ProductId = temp.ProductId;
                    customTextEntity.Save();
                }
            }
        }
    }

    private void ConvertImageAlteration(AlterationEntity sourceAlterationEntity, AlterationEntity bottomLevelAlterationEntity, AlterationEntity optionAlterationEntity)
    {
        foreach (MediaEntity sourceMediaEntity in bottomLevelAlterationEntity.MediaCollection)
        {
            // Copy the media
            MediaEntity mediaEntity = new MediaEntity();
            LLBLGenEntityUtil.CopyFields(sourceMediaEntity, mediaEntity);
            mediaEntity.AlterationId = optionAlterationEntity.AlterationId;
            if (mediaEntity.Save())
            {
                mediaEntity.Refetch();

                mediaEntity.FileDownloadDelegate = () => mediaEntity.Download();

                MediaRatioTypeMediaEntity sourceMediaRatioTypeMediaEntity = sourceMediaEntity.MediaRatioTypeMediaCollection.FirstOrDefault(x => x.MediaTypeAsEnum == MediaType.AlterationDialogColumn1280x800);
                if (sourceMediaRatioTypeMediaEntity != null)
                {
                    MediaRatioTypeMediaEntity mrtm = new MediaRatioTypeMediaEntity();
                    mrtm.MediaId = mediaEntity.MediaId;
                    mrtm.MediaTypeAsEnum = MediaType.ProductDialogAlterationColumn1280x800;
                    mrtm.Width = sourceMediaRatioTypeMediaEntity.Width;
                    mrtm.Height = sourceMediaRatioTypeMediaEntity.Height;
                    mrtm.Left = sourceMediaRatioTypeMediaEntity.Left;
                    mrtm.Top = sourceMediaRatioTypeMediaEntity.Top;
                    mrtm.Save();

                    mrtm.ResizeAndPublishFile(mediaEntity, true);
                }
            }
        }
    }

    private void ConvertDateTimeAlteration(AlterationEntity sourceAlterationEntity, AlterationEntity bottomLevelAlterationEntity)
    {
        // Copy the alteration
        AlterationEntity alterationEntity = CopyAlteration(bottomLevelAlterationEntity);
        if (alterationEntity != null)
        { 
            // Link the newly created alteration to the products it belongs to
            foreach (ProductAlterationEntity temp in sourceAlterationEntity.ProductAlterationCollection)
            {
                if (!temp.ProductEntity.ProductAlterationCollection.Any(x => x.AlterationId == alterationEntity.AlterationId))
                {
                    ProductAlterationEntity productAlterationEntity = new ProductAlterationEntity();
                    productAlterationEntity.Version = 2;
                    productAlterationEntity.ProductId = temp.ProductId;
                    productAlterationEntity.AlterationId = alterationEntity.AlterationId;
                    productAlterationEntity.Save();
                }
            }
        }
    }

    private void ConvertDateAlteration(AlterationEntity sourceAlterationEntity, AlterationEntity bottomLevelAlterationEntity)
    {
        // Copy the alteration
        AlterationEntity alterationEntity = CopyAlteration(bottomLevelAlterationEntity);
        if (alterationEntity != null)
        {
            alterationEntity.Refetch();

            // Link the newly created alteration to the products it belongs to
            foreach (ProductAlterationEntity temp in sourceAlterationEntity.ProductAlterationCollection)
            {
                if (!temp.ProductEntity.ProductAlterationCollection.Any(x => x.AlterationId == alterationEntity.AlterationId))
                {
                    ProductAlterationEntity productAlterationEntity = new ProductAlterationEntity();
                    productAlterationEntity.Version = 2;
                    productAlterationEntity.ProductId = temp.ProductId;
                    productAlterationEntity.AlterationId = alterationEntity.AlterationId;
                    productAlterationEntity.Save();
                }
            }
        }
    }

    private void ConvertNotesAlteration(AlterationEntity sourceAlterationEntity, AlterationEntity bottomLevelAlterationEntity)
    {
        foreach (ProductAlterationEntity productAlterationEntity in sourceAlterationEntity.ProductAlterationCollection)
        {
            productAlterationEntity.ProductEntity.AllowFreeText = true;
            productAlterationEntity.ProductEntity.Save();
        }
    }

    private void ConvertTextAlteration(AlterationEntity sourceAlterationEntity, AlterationEntity bottomLevelAlterationEntity)
    {
        foreach (ProductAlterationEntity temp in sourceAlterationEntity.ProductAlterationCollection)
        {
            AlterationEntity formAlterationEntity = null;

            ProductAlterationEntity productAlterationEntity = temp.ProductEntity.ProductAlterationCollection.FirstOrDefault(x => x.AlterationEntity.Type == AlterationType.Form);
            if (productAlterationEntity != null)
            {
                formAlterationEntity = productAlterationEntity.AlterationEntity;
            }
            else
            {
                formAlterationEntity = new AlterationEntity();
                formAlterationEntity.Version = 2;
                formAlterationEntity.Name = temp.ProductEntity.Name + " Form";
                formAlterationEntity.Type = AlterationType.Form;
                formAlterationEntity.CompanyId = temp.ProductEntity.CompanyId;
                if (formAlterationEntity.Save())
                    formAlterationEntity.Refetch();
            }

            AlterationoptionEntity textAlterationoptionEntity = new AlterationoptionEntity();
            textAlterationoptionEntity.Version = 2;
            textAlterationoptionEntity.Name = bottomLevelAlterationEntity.Name;
            textAlterationoptionEntity.Type = 5; // Text
            textAlterationoptionEntity.CompanyId = sourceAlterationEntity.CompanyId.Value;
            if (textAlterationoptionEntity.Save())
            {
                textAlterationoptionEntity.Refetch();

                AlterationitemEntity textAlterationitemEntity = new AlterationitemEntity();
                textAlterationitemEntity.Version = 2;
                textAlterationitemEntity.AlterationId = formAlterationEntity.AlterationId;
                textAlterationitemEntity.AlterationoptionId = textAlterationoptionEntity.AlterationoptionId;
                textAlterationitemEntity.Save();
            }
        }
    }

    private void ConvertNumericAlteration(AlterationEntity sourceAlterationEntity, AlterationEntity bottomLevelAlterationEntity)
    {
        foreach (ProductAlterationEntity temp in sourceAlterationEntity.ProductAlterationCollection)
        {
            AlterationEntity formAlterationEntity = null;

            ProductAlterationEntity productAlterationEntity = temp.ProductEntity.ProductAlterationCollection.FirstOrDefault(x => x.AlterationEntity.Type == AlterationType.Form);
            if (productAlterationEntity != null)
            {
                formAlterationEntity = productAlterationEntity.AlterationEntity;
            }
            else
            {
                formAlterationEntity = new AlterationEntity();
                formAlterationEntity.Version = 2;
                formAlterationEntity.Name = temp.ProductEntity.Name + " Form";
                formAlterationEntity.Type = AlterationType.Form;
                formAlterationEntity.CompanyId = temp.ProductEntity.CompanyId;
                if (formAlterationEntity.Save())
                    formAlterationEntity.Refetch();
            }

            AlterationoptionEntity textAlterationoptionEntity = new AlterationoptionEntity();
            textAlterationoptionEntity.Version = 2;
            textAlterationoptionEntity.Name = bottomLevelAlterationEntity.Name;
            textAlterationoptionEntity.Type = 6; // Numeric
            textAlterationoptionEntity.CompanyId = sourceAlterationEntity.CompanyId.Value;
            if (textAlterationoptionEntity.Save())
            {
                textAlterationoptionEntity.Refetch();

                AlterationitemEntity textAlterationitemEntity = new AlterationitemEntity();
                textAlterationitemEntity.Version = 2;
                textAlterationitemEntity.AlterationId = formAlterationEntity.AlterationId;
                textAlterationitemEntity.AlterationoptionId = textAlterationoptionEntity.AlterationoptionId;
                textAlterationitemEntity.Save();
            }
        }
    }

    private void ConvertEmailAlteration(AlterationEntity sourceAlterationEntity, AlterationEntity bottomLevelAlterationEntity)
    {
        foreach (ProductAlterationEntity temp in sourceAlterationEntity.ProductAlterationCollection)
        {
            AlterationEntity formAlterationEntity = null;

            ProductAlterationEntity productAlterationEntity = temp.ProductEntity.ProductAlterationCollection.FirstOrDefault(x => x.AlterationEntity.Type == AlterationType.Form);
            if (productAlterationEntity != null)
            {
                formAlterationEntity = productAlterationEntity.AlterationEntity;
            }
            else
            {
                formAlterationEntity = new AlterationEntity();
                formAlterationEntity.Version = 2;
                formAlterationEntity.Name = temp.ProductEntity.Name + " Form";
                formAlterationEntity.Type = AlterationType.Form;
                formAlterationEntity.CompanyId = temp.ProductEntity.CompanyId;
                if (formAlterationEntity.Save())
                    formAlterationEntity.Refetch();
            }

            AlterationoptionEntity textAlterationoptionEntity = new AlterationoptionEntity();
            textAlterationoptionEntity.Version = 2;
            textAlterationoptionEntity.Name = bottomLevelAlterationEntity.Name;
            textAlterationoptionEntity.Type = 4; // Email
            textAlterationoptionEntity.CompanyId = sourceAlterationEntity.CompanyId.Value;
            if (textAlterationoptionEntity.Save())
            {
                textAlterationoptionEntity.Refetch();

                AlterationitemEntity textAlterationitemEntity = new AlterationitemEntity();
                textAlterationitemEntity.Version = 2;
                textAlterationitemEntity.AlterationId = formAlterationEntity.AlterationId;
                textAlterationitemEntity.AlterationoptionId = textAlterationoptionEntity.AlterationoptionId;
                textAlterationitemEntity.Save();
            }
        }
    }

    #endregion

    #region List

    private void ListPackageAlterations(int companyId)
    {
        PredicateExpression filter = new PredicateExpression(AlterationFields.CompanyId == companyId);
        filter.Add(AlterationFields.ParentAlterationId == DBNull.Value);
        filter.Add(AlterationFields.Version != 2);

        AlterationCollection alterationCollection = new AlterationCollection();
        alterationCollection.GetMulti(filter);

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.AppendFormat("<ul>");

        foreach (AlterationEntity alterationEntity in alterationCollection)
        {
            stringBuilder.AppendFormat("<li>");
            stringBuilder.AppendFormat(alterationEntity.Name);
            stringBuilder.AppendFormat("&nbsp;<a href='{0}'>Current</a>", WebShortcuts.ResolveUrl("~/Alterations.aspx?AlterationId={0}", alterationEntity.AlterationId));
            stringBuilder.AppendFormat("&nbsp;<a href='{0}'>Preview</a>", WebShortcuts.ResolveUrl("~/Alterations.aspx?AlterationId={0}&Preview=true", alterationEntity.AlterationId));
            stringBuilder.AppendFormat("&nbsp;<a href='{0}'>Convert</a>", WebShortcuts.ResolveUrl("~/Alterations.aspx?AlterationId={0}&Convert=true", alterationEntity.AlterationId));
            stringBuilder.AppendFormat("</li>");            
        }

        stringBuilder.AppendFormat("</ul>");

        this.plhOutput.AddHtml(stringBuilder.ToString());
    }

    #endregion

    private void GetBottomLevelAlterations(AlterationEntity alterationEntity, List<AlterationEntity> bottomLevelAlterations)
    {
        foreach (AlterationEntity childAlterationEntity in alterationEntity.AlterationCollection)
        {
            if (childAlterationEntity.Type == AlterationType.Options)
            {
                if (!bottomLevelAlterations.Any(x => x.AlterationId == childAlterationEntity.AlterationId))
                    bottomLevelAlterations.Add(childAlterationEntity);
            }
            else if (childAlterationEntity.Type == AlterationType.Category)
            {
                if (childAlterationEntity.AlterationProductCollection.Count > 0)
                {
                    if (!bottomLevelAlterations.Any(x => x.AlterationId == childAlterationEntity.AlterationId))
                        bottomLevelAlterations.Add(childAlterationEntity);
                }
                else
                {
                    this.GetBottomLevelAlterations(childAlterationEntity, bottomLevelAlterations);
                }
            }
            else if (childAlterationEntity.Type == AlterationType.Date || childAlterationEntity.Type == AlterationType.DateTime || childAlterationEntity.Type == AlterationType.Email || childAlterationEntity.Type == AlterationType.Image || childAlterationEntity.Type == AlterationType.Instruction || childAlterationEntity.Type == AlterationType.Notes || childAlterationEntity.Type == AlterationType.Numeric || childAlterationEntity.Type == AlterationType.Text)
            {
                if (!bottomLevelAlterations.Any(x => x.AlterationId == childAlterationEntity.AlterationId))
                    bottomLevelAlterations.Add(childAlterationEntity);
            }
            else if (childAlterationEntity.Type == AlterationType.Page)
            {
                this.GetBottomLevelAlterations(childAlterationEntity, bottomLevelAlterations);
            }
        }
    }

    private AlterationEntity GetRootCategoryAlteration(AlterationEntity alterationEntity)
    {
        if (alterationEntity.ParentAlterationId.HasValue)
        {
            AlterationEntity parentAlterationEntity = alterationEntity.ParentAlterationEntity;

            if (parentAlterationEntity.Type != AlterationType.Category)
            {
                return parentAlterationEntity;
            }
            else
            {
                return GetRootCategoryAlteration(parentAlterationEntity);
            }
        }

        return null;
    }
}