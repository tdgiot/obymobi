﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data;
using Dionysos.Diagnostics;
using Obymobi.Web.Google;
using System.Text;
using System.Data;
using Dionysos.Web.UI;
using Obymobi.Web.Google.CloudPrint;
using Dionysos.Web;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.ObymobiCms.Configuration
{
    public partial class Printers : PageDefault
    {
        private GetPrinters printerList = null;
        private GetPrintJobs printJobs = null;
        private GoogleAPI googleApi = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            this.ProcessPrinters();

            this.jobsGrid.CustomButtonCallback += new DevExpress.Web.ASPxGridViewCustomButtonCallbackEventHandler(jobsGrid_CustomButtonCallback);
        }

        void jobsGrid_CustomButtonCallback(object sender, DevExpress.Web.ASPxGridViewCustomButtonCallbackEventArgs e)
        {
            if (e.ButtonID == "deleteJob")
            {
                if (printJobs != null && googleApi != null)
                {
                    PrintJob job = printJobs.jobs[e.VisibleIndex];
                    if (job != null)
                    {
                        googleApi.DeletePrintJob(job.id);

                        List<PrintJob> jobList = new List<PrintJob>(printJobs.jobs);
                        jobList.Remove(job);
                        printJobs.jobs = jobList.ToArray();
                        SetPrintJobs();
                    }
                }
            }
        }

        private void SetPrinters()
        {
            var table = new DataTable();

            // Create columns
            DataColumn nameColumn = new DataColumn("Name");
            DataColumn displayNameColumn = new DataColumn("DisplayName");
            DataColumn statusColumn = new DataColumn("Status");

            // Translations
            nameColumn.Caption = this.Translate("clmnName", "Naam");
            displayNameColumn.Caption = this.Translate("clmnDisplayName", "Display naam");
            statusColumn.Caption = this.Translate("clmnStatus", "Status");

            // Add columns
            table.Columns.Add(nameColumn);
            table.Columns.Add(displayNameColumn);
            table.Columns.Add(statusColumn);

            if (printerList.printers.Length > 0)
            {
                for (int i = 0; i < printerList.printers.Length; i++)
                {
                    DataRow row = table.NewRow();
                    row["Name"] = printerList.printers[i].name;
                    row["DisplayName"] = printerList.printers[i].displayName;
                    row["Status"] = printerList.printers[i].connectionStatus;

                    table.Rows.Add(row);
                }
                this.printersGrid.DataSource = table;
                this.printersGrid.DataBind();
            }
        }

        private void SetPrintJobs()
        {
            var table = new DataTable();

            // Create columns
            DataColumn printerColumn = new DataColumn("Printer");
            DataColumn titleColumn = new DataColumn("Title");
            DataColumn statusColumn = new DataColumn("Status");
            DataColumn viewFileColumn = new DataColumn("ViewFile");
            DataColumn fileColumn = new DataColumn("File");

            // Translations
            printerColumn.Caption = this.Translate("clmnPrinter", "Printer");
            titleColumn.Caption = this.Translate("clmnTitle", "Titel");
            statusColumn.Caption = this.Translate("clmnStatus", "Status");
            viewFileColumn.Caption = this.Translate("clmnViewFile", "Toon bestand");
            fileColumn.Caption = this.Translate("clmnFile", "Bestand");

            // Add columns
            table.Columns.Add(printerColumn);
            table.Columns.Add(titleColumn);
            table.Columns.Add(statusColumn);
            table.Columns.Add(viewFileColumn);
            table.Columns.Add(fileColumn);

            if (printJobs.jobs.Length > 0)
            {
                for (int i = 0; i < printJobs.jobs.Length; i++)
                {
                    DataRow row = table.NewRow();
                    string printerName = printJobs.jobs[i].printerid;
                    if (printJobs != null)
                    {
                        foreach (Printer printer in printerList.printers)
                        {
                            if (printer.id == printerName)
                            {
                                printerName = printer.name;
                                break;
                            }
                        }
                    }
                    row["Printer"] = printerName;
                    row["Title"] = printJobs.jobs[i].title;
                    row["Status"] = printJobs.jobs[i].status;
                    row["ViewFile"] = "File";
                    row["File"] = string.Format("Printers.aspx?file={0}", printJobs.jobs[i].id);

                    table.Rows.Add(row);
                }
                this.jobsGrid.DataSource = table;
                this.jobsGrid.DataBind();
            }
        }

        private void ProcessPrinters()
        {
            StringBuilder builder = new StringBuilder();

            ApiAuthenticationEntity googleApiAuthenticationEntity = ApiAuthenticationHelper.GetApiAuthenticationEntityByType(ApiAuthenticationType.Google, CmsSessionHelper.CurrentCompanyId);
            if (googleApiAuthenticationEntity != null && !string.IsNullOrWhiteSpace(googleApiAuthenticationEntity.FieldValue1))
            {
                string clientId = Dionysos.ConfigurationManager.GetString(DionysosWebConfigurationConstants.GoogleClientId);
                string clientSecret = Dionysos.ConfigurationManager.GetString(DionysosWebConfigurationConstants.GoogleClientSecret);

                this.googleApi = new GoogleAPI(clientId, clientSecret, googleApiAuthenticationEntity.FieldValue1);

                this.printerList = googleApi.GetPrinters();
                if (this.printerList != null)
                {
                    SetPrinters();
                }
                else
                {
                    this.MultiValidatorDefault.AddError("Could not retrieve printers");
                }

                this.printJobs = googleApi.GetPrintJobs();
                if (this.printJobs != null)
                {
                    SetPrintJobs();
                }
                else
                {
                    this.MultiValidatorDefault.AddError("Could not retrieve print jobs");
                }
            }
            else
            {
                this.MultiValidatorDefault.AddError("Printers not linked to google account");
            }

            this.Validate();    
        }

    }
}