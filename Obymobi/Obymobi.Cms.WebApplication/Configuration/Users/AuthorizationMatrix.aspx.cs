﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using DevExpress.Web;
using DevExpress.Web.ASPxTreeList;
using Dionysos;
using Dionysos.Interfaces;
using Dionysos.Web.UI;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Cms;
using Obymobi.ObymobiCms.AppLess;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Configuration.Users
{
    public partial class AuthorizationMatrix : PageDefault
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            RenderEntities();
            RenderPages();
            HookUpEvents();
        }

        private void HookUpEvents()
        {
            hlExpandAll.NavigateUrl = string.Format("javascript:{0}.ExpandAll()", tlPages.ClientID);
            hlCollapseAll.NavigateUrl = string.Format("javascript:{0}.CollapseAll()", tlPages.ClientID);
        }

        private void RenderEntities()
        {
            IReadOnlyCollection<Role> allRoles = GetAllRoles();
            IReadOnlyCollection<EntityBase> entities = GetAllEntities();

            gvEntityAuthorisation.DataSource = CreateEntitiesDataTable(allRoles, entities);
            gvEntityAuthorisation.DataBind();

            SetAutoFilterConditionOnColumns(AutoFilterCondition.Contains);
        }

        private void RenderPages()
        {
            IReadOnlyCollection<Role> allRoles = GetAllRoles();
            IReadOnlyCollection<ModuleEntity> modules = GetModules();

            if (!IsPostBack)
            {
                AddPageColumns(allRoles);
            }

            IReadOnlyCollection<Section> sections = CreateSectionsForModules(modules, allRoles);

            tlPages.KeyFieldName = "Id";
            tlPages.ParentFieldName = "ParentId";
            tlPages.DataSource = CreateSectionsDataTable(allRoles, sections);
            tlPages.DataBind();
            tlPages.ExpandAll();
        }

        private void SetAutoFilterConditionOnColumns(AutoFilterCondition autoFilterCondition)
        {
            foreach (GridViewDataColumn column in gvEntityAuthorisation.Columns)
            {
                column.Settings.AutoFilterCondition = autoFilterCondition;
            }
        }

        private DataTable CreateEntitiesDataTable(IReadOnlyCollection<Role> roles, IReadOnlyCollection<EntityBase> entities)
        {
            DataTable table = new DataTable();

            table.Columns.Add(new DataColumn("Entity"));
            table.Columns.AddRange(roles.Select(role => new DataColumn(role.ToString())).ToArray());

            foreach (EntityBase entity in entities)
            {
                DataRow row = table.NewRow();
                row["Entity"] = entity.GetType().Name.Replace("Entity", string.Empty);

                Role minRoleAllowedToSaveNewEntity = GetRolePropertyFromAuthorizer(entity.AuthorizerToUse, "RoleAllowedToSaveNewEntity");
                Role minRoleAllowedToLoadEntity = GetRolePropertyFromAuthorizer(entity.AuthorizerToUse, "RoleAllowedToLoadEntity");
                Role minRoleAllowedToSaveExistingEntity = GetRolePropertyFromAuthorizer(entity.AuthorizerToUse, "RoleAllowedToSaveExistingEntity");
                Role minRoleAllowedToDeleteEntity = GetRolePropertyFromAuthorizer(entity.AuthorizerToUse, "RoleAllowedToDeleteEntity");

                foreach (Role role in roles)
                {
                    List<string> roleOperations = new List<string>();

                    if (role >= minRoleAllowedToSaveNewEntity)
                    {
                        roleOperations.Add("C");
                    }

                    if (role >= minRoleAllowedToLoadEntity)
                    {
                        roleOperations.Add("R");
                    }

                    if (role >= minRoleAllowedToSaveExistingEntity)
                    {
                        roleOperations.Add("U");
                    }

                    if (role >= minRoleAllowedToDeleteEntity)
                    {
                        roleOperations.Add("D");
                    }

                    row[role.ToString()] = string.Join(",", roleOperations);
                }

                table.Rows.Add(row);
            }

            return table;
        }

        private void AddPageColumns(IReadOnlyCollection<Role> roles)
        {
            tlPages.Columns.Add(new TreeListDataColumn("Name"));

            foreach (Role role in roles)
            {
                TreeListImageColumn roleColumn = new TreeListImageColumn();
                roleColumn.FieldName = role.ToString();
                roleColumn.PropertiesImage.AlternateText = "role";
                roleColumn.PropertiesImage.ImageUrlFormatString = "{0}";
                tlPages.Columns.Add(roleColumn);
            }
        }

        private IReadOnlyCollection<Section> CreateSectionsForModules(IReadOnlyCollection<ModuleEntity> modules, IReadOnlyCollection<Role> allRoles)
        {
            List<Section> sections = new List<Section>();

            foreach (ModuleEntity module in modules)
            {
                Section section = new Section(
                    $"Module-{module.ModuleId}",
                    string.Empty,
                    GlobalizationHelper.GetTranslation(module, ModuleFields.NameFull),
                    Enumerable.Empty<Role>());

                IReadOnlyCollection<UIElementEntity> uiElements = module.UIElements
                                                                        .OfType<UIElementEntity>()
                                                                        .Where(element => element.DisplayInMenu)
                                                                        .OrderBy(element => element.SortOrder)
                                                                        .ToList();

                IReadOnlyCollection<Section> elementSections = CreateSectionsForElements(uiElements, allRoles, section);

                IEnumerable<Role> elementSectionRoles = elementSections
                                                        .SelectMany(elementSection => elementSection.AllowedRoles)
                                                        .Distinct();

                sections.Add(section.W(elementSectionRoles));
                sections.AddRange(elementSections);
            }

            return sections;
        }

        private IReadOnlyCollection<Section> CreateSectionsForElements(IReadOnlyCollection<UIElementEntity> uiElements, IReadOnlyCollection<Role> allRoles, Section parentSection)
        {
            List<Section> sections = new List<Section>();

            foreach (UIElementEntity uiElement in uiElements)
            {
                IEnumerable<Role> allowedRoles = allRoles
                                                 .Where(role => UserRightsHelper.IsUiElementAllowedForRole(role, uiElement))
                                                 .ToList();

                Section section = new Section(
                    $"UIElement-{uiElement.UIElementId}",
                    parentSection.Id,
                    GlobalizationHelper.GetTranslation(uiElement, UIElementFields.NameFull),
                    allowedRoles);

                sections.Add(section);

                if (CustomAuthorizations.ContainsKey(uiElement.NameSystem))
                {
                    SectionAuthorization[] customSections = CustomAuthorizations[uiElement.NameSystem].Sections;
                    sections.AddRange(CreateSectionsForCustomSections(customSections, allRoles, section));
                }
            }

            return sections;
        }

        private IReadOnlyCollection<Section> CreateSectionsForCustomSections(IReadOnlyCollection<SectionAuthorization> sections, IReadOnlyCollection<Role> allRoles, Section parentSection)
        {
            return sections.Select(section =>
            {
                return new Section($"Section-{section.SectionName}",
                                   parentSection.Id,
                                   section.SectionName,
                                   allRoles.Where(role => role >= section.MinimumRole));
            }).ToList();
        }

        private DataTable CreateSectionsDataTable(IReadOnlyCollection<Role> allRoles, IReadOnlyCollection<Section> sections)
        {
            DataTable table = new DataTable();

            table.Columns.Add(new DataColumn(nameof(Section.Id)));
            table.Columns.Add(new DataColumn(nameof(Section.ParentId)));
            table.Columns.Add(new DataColumn(nameof(Section.Name)));
            table.Columns.AddRange(allRoles.Select(role => new DataColumn(role.ToString())).ToArray());

            foreach (Section section in sections)
            {
                DataRow row = table.NewRow();
                row[nameof(Section.Id)] = section.Id;
                row[nameof(Section.ParentId)] = section.ParentId;
                row[nameof(Section.Name)] = section.Name;

                foreach (Role role in allRoles)
                {
                    bool sectionAllowedForRole = section.AllowedRoles.Contains(role);

                    row[role.ToString()] = sectionAllowedForRole ? "~/Images/Icons/valid.png" : "~/Images/Icons/false.png";
                }

                table.Rows.Add(row);
            }

            return table;
        }

        private Role GetRolePropertyFromAuthorizer(IAuthorizer authorizer, string propertyName)
        {
            return (Role)authorizer.GetType()
                                   .GetProperty(propertyName, BindingFlags.Instance | BindingFlags.NonPublic)
                                   .GetValue(authorizer);
        }

        private IReadOnlyCollection<Role> GetAllRoles()
        {
            return EnumUtil.GetValues<Role>()
                           .OrderByDescending(role => role)
                           .ToList()
                           .AsReadOnly();
        }

        private IReadOnlyCollection<ModuleEntity> GetModules()
        {
            return LicenseUtil
                   .GetLicensedModules()
                   .OfType<ModuleEntity>()
                   .Where(module => module.UIElements.Any(element => element.DisplayInMenu))
                   .ToList()
                   .AsReadOnly();
        }

        private IReadOnlyCollection<EntityBase> GetAllEntities()
        {
            Type entityType = typeof(CompanyEntity);
            Assembly assembly = Assembly.GetAssembly(entityType);

            List<EntityBase> entities = new List<EntityBase>();

            foreach (Type type in assembly.GetTypes())
            {
                if (type.IsAbstract ||
                    !type.Name.EndsWith("Entity", StringComparison.InvariantCultureIgnoreCase) ||
                    !type.Namespace.Equals(entityType.Namespace, StringComparison.InvariantCultureIgnoreCase))
                {
                    continue;
                }

                object instance = InstanceFactory.CreateInstance(assembly, type);
                if (instance is EntityBase entityBase)
                {
                    entities.Add(entityBase);
                }
            }

            return entities
                .OrderBy(entity => entity.GetType().Name)
                .ToList()
                .AsReadOnly();
        }

        private class Section
        {
            public Section(string id, 
                           string parentId, 
                           string name, 
                           IEnumerable<Role> allowedRoles)
            {
                Id = id;
                ParentId = parentId;
                Name = name;
                AllowedRoles = allowedRoles;
            }

            public string Id { get; }
            public string ParentId { get; }
            public string Name { get; }
            public IEnumerable<Role> AllowedRoles { get; }
            public Section W(IEnumerable<Role> allowedRoles) => new Section(Id, ParentId, Name, allowedRoles);
        }

        private const string ICONS_SECTION = "Icons";
        private const string PAGES_SECTION = "Pages";
        private const string WIDGETS_SECTION = "Widgets";
        private const string NAVIGATION_MENUS_SECTION = "Navigation menus";
        private const string THEMES_SECTION = "Themes";
        private const string GENERIC_SECTION = "Generic";
        private const string CHECKOUT_METHODS_SECTION = "Checkout methods";
        private const string SERVICE_METHODS_SECTION = "Service methods";
        private const string BUSINESS_HOURS_SECTION = "Business hours";
        private const string TRANSLATIONS_SECTION = "Translations";

        private static readonly Dictionary<string, PageAuthorization> CustomAuthorizations = new Dictionary<string, PageAuthorization>();

        private static readonly PageAuthorization AppLessConfigurationAuth = new PageAuthorization(nameof(ApplicationConfigurations), new []
        {
            new SectionAuthorization(ICONS_SECTION, Role.Supervisor),
            new SectionAuthorization(PAGES_SECTION, Role.Supervisor),
            new SectionAuthorization(AuthorizationMatrix.WIDGETS_SECTION, Role.Supervisor),
            new SectionAuthorization(AuthorizationMatrix.NAVIGATION_MENUS_SECTION, Role.Supervisor),
            new SectionAuthorization(AuthorizationMatrix.THEMES_SECTION, Role.Supervisor)
        });

        private static readonly PageAuthorization OutletAuth = new PageAuthorization(nameof(Outlets), new[]
        {
            new SectionAuthorization(AuthorizationMatrix.GENERIC_SECTION, Role.Supervisor),
            new SectionAuthorization(AuthorizationMatrix.CHECKOUT_METHODS_SECTION, Role.Crave),
            new SectionAuthorization(AuthorizationMatrix.SERVICE_METHODS_SECTION, Role.Supervisor),
            new SectionAuthorization(AuthorizationMatrix.BUSINESS_HOURS_SECTION, Role.Supervisor),
            new SectionAuthorization(AuthorizationMatrix.TRANSLATIONS_SECTION, Role.Supervisor)
        });

        private class PageAuthorization
        {
            public PageAuthorization(string pageName, 
                                     SectionAuthorization[] sections)
            {
                PageName = pageName;
                Sections = sections;

                CustomAuthorizations[pageName] = this;
            }

            public string PageName { get; }
            public SectionAuthorization[] Sections { get; }
            
        }

        private class SectionAuthorization
        {
            public SectionAuthorization(string sectionName,
                                        Role minimumRole)
            {
                SectionName = sectionName;
                MinimumRole = minimumRole;
            }

            public string SectionName { get; }
            public Role MinimumRole { get; }
        } 
    }
}