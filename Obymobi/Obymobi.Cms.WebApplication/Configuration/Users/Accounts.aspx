﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Configuration.Users.Accounts" Codebehind="Accounts.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitleHolder" runat="Server">
    Accounts
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhToolbarHolder" runat="Server">
    <X:ToolBarButton runat="server" ID="btAdd" CommandName="Add" Text="Add" Image-Url="~/Images/Icons/add.png" Style="margin-left: 4px" />    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhContentHolder" runat="Server">
<div>
    <X:GridView ID="gridAccounts" ClientInstanceName="gridAccounts" runat="server" Width="100%" KeyFieldName="AccountId">
            <SettingsPager PageSize="20"></SettingsPager>
            <SettingsBehavior AllowGroup="false" AllowDragDrop="false" AutoFilterRowInputDelay="350" />        
            <Columns>
                <dxwgv:GridViewDataColumn FieldName="Name" VisibleIndex="1" SortIndex="0" SortOrder="Ascending" Width="96%">
				    <Settings AutoFilterCondition="Contains" />
                    <Settings SortMode="DisplayText" />
                    <Settings FilterMode="DisplayText" />
                </dxwgv:GridViewDataColumn>   
                <dxwgv:GridViewCommandColumn ButtonType="Image" VisibleIndex="2" Width="2%">
                    <CustomButtons>
                        <dxwgv:GridViewCommandColumnCustomButton ID="openAccount" Text="Open">
                            <Image Url="~/Images/Icons/pencil.png" />
                        </dxwgv:GridViewCommandColumnCustomButton>
                    </CustomButtons>
                </dxwgv:GridViewCommandColumn>                         
                <dxwgv:GridViewCommandColumn ButtonType="Image" VisibleIndex="3" Width="2%">
                    <CustomButtons>
                        <dxwgv:GridViewCommandColumnCustomButton ID="deleteAccount" Text="Delete">
                            <Image Url="~/Images/Icons/delete.png" />
                        </dxwgv:GridViewCommandColumnCustomButton>
                    </CustomButtons>
                </dxwgv:GridViewCommandColumn>
            </Columns>
            <Settings ShowFilterRow="True" />
        </X:GridView>   
</div>
</asp:Content>