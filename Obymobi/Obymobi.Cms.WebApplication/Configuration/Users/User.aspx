<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Configuration.Users.User" Title="Gebruiker" CodeBehind="User.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" runat="Server">
	<D:Label runat="server" ID="lblTitleProducts">Gebruikers</D:Label>
	-
	<D:Label runat="server" ID="lblTitleEdit">Bewerken</D:Label>
	-
	<D:Label runat="server" ID="lblTitleBrandName" LocalizeText="false">Bewerken</D:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" runat="Server">
	<div>
		<X:PageControl ID="tabsMain" runat="server" Width="100%">
			<TabPages>
				<X:TabPage Text="Algemeen" Name="Generic">
					<controls>
						<table class="dataformV2">
							<tr>
								<td class="label">
									<D:LabelEntityFieldInfo runat="server" ID="lblAccountId">Account</D:LabelEntityFieldInfo>
								</td>
								<td class="control">
									<X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlAccountId" UseDataBinding="true" EntityName="Account" IsRequired="false" PreventEntityCollectionInitialization="true"></X:ComboBoxLLBLGenEntityCollection>
								</td>
								<td class="label">
									<D:LabelEntityFieldInfo runat="server" ID="lblFirstname">Voornaam</D:LabelEntityFieldInfo>
								</td>
								<td class="control">
									<D:TextBoxString ID="tbFirstname" runat="server"></D:TextBoxString>
								</td>
							</tr>
							<tr>
								<td class="label">
									<D:LabelEntityFieldInfo runat="server" ID="lblLastnamePrefix">Tussenvoegsel</D:LabelEntityFieldInfo>
								</td>
								<td class="control">
									<D:TextBoxString ID="tbLastnamePrefix" runat="server"></D:TextBoxString>
								</td>
								<td class="label">
									<D:LabelEntityFieldInfo runat="server" ID="lblLastname">Achternaam</D:LabelEntityFieldInfo>
								</td>
								<td class="control">
									<D:TextBoxString ID="tbLastname" runat="server"></D:TextBoxString>
								</td>
							</tr>
							<tr>
								<td class="label">
									<D:LabelEntityFieldInfo runat="server" ID="lblUsername">Gebruikersnaam</D:LabelEntityFieldInfo>
								</td>
								<td class="control">
									<D:TextBoxString ID="tbUsername" runat="server"></D:TextBoxString>
								</td>
								<td class="label">
									<D:LabelEntityFieldInfo runat="server" ID="lblEmail">Email</D:LabelEntityFieldInfo>
								</td>
								<td class="control">
									<D:TextBoxString ID="tbEmail" runat="server"></D:TextBoxString>
								</td>
							</tr>
							<tr>
								<td class="label">
									<D:Label runat="server" ID="lblPasswordOne">Wachtwoord</D:Label>
								</td>
								<td class="control">
									<D:TextBoxPassword ID="tbPasswordOne" runat="server" autocomplete="new-password"></D:TextBoxPassword>
								</td>
								<td class="label">
									<D:Label runat="server" ID="lblPasswordTwo">Wachtwoord (herhalen)</D:Label>
								</td>
								<td class="control">
									<D:TextBoxPassword ID="tbPasswordTwo" runat="server"></D:TextBoxPassword>
								</td>
							</tr>
							<tr>
								<td class="label">
									<D:Label runat="server" ID="lblRole">Role</D:Label>
								</td>
								<td class="control">
									<X:ComboBoxInt ID="cbRole" runat="server" IsRequired="true" UseDataBinding="true"></X:ComboBoxInt>
								</td>
								<td class="label">
									<D:Label runat="server" ID="lblPageSize">Aantal rijen in overzichten</D:Label>
								</td>
								<td class="control">
									<D:TextBoxInt ID="tbPageSize" runat="server" IsRequired="true"></D:TextBoxInt>
									<br />
									<D:Label ID="lblPageSizeComment" runat="server"><small>U moet opnieuw inloggen voordat dit effect heeft.</small></D:Label>
								</td>
							</tr>
							<tr>
								<td class="label">
									<D:Label runat="server" ID="lblTimeZoneId">Timezone</D:Label>
								</td>
								<td class="control">
									<X:ComboBox runat="server" ID="ddlTimeZoneOlsonId" IsRequired="true" TextField="Name" ValueField="OlsonTimeZoneId"></X:ComboBox>
								</td>
								<td class="label"></td>
								<td class="control"></td>
							</tr>
							<tr id="trTestDeveloperFlags">
								<td class="label">
									<D:Label runat="server" ID="lblIsTester">Tester</D:Label>
								</td>
								<td class="control">
									<D:CheckBox runat="server" ID="cbIsTester" />
								</td>
								<td class="label">
									<D:Label runat="server" ID="lblIsDeveloper">Developer</D:Label>
								</td>
								<td class="control">
									<D:CheckBox runat="server" ID="cbIsDeveloper" />
								</td>
							</tr>
							<tr>
								<td class="label">
									<D:LabelEntityFieldInfo runat="server" ID="lblIsApproved">Bevestigd</D:LabelEntityFieldInfo>
								</td>
								<td class="control">
									<D:CheckBox runat="server" ID="cbIsApproved" />
								</td>
								<td class="label">
									<D:LabelEntityFieldInfo runat="server" ID="lblIsLockedOut">Afgesloten</D:LabelEntityFieldInfo>
								</td>
								<td class="control">
									<D:CheckBox runat="server" ID="cbIsLockedOut" />
								</td>
							</tr>
							<tr>
								<td class="label">
									<D:LabelEntityFieldInfo runat="server" ID="lblAllowPublishing">Allow publishing</D:LabelEntityFieldInfo>
								</td>
								<td class="control">
									<D:CheckBox runat="server" ID="cbAllowPublishing" />
								</td>								
							</tr>
						</table>
					</controls>
				</X:TabPage>
			</TabPages>
		</X:PageControl>
	</div>
</asp:Content>
