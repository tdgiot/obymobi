<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Configuration.Users.AccountCompany" Title="Account company" Codebehind="AccountCompany.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
<D:Label runat="server" ID="lblTitleProducts">Account users</D:Label> -
<D:Label runat="server" ID="lblTitleEdit">Bewerken</D:Label> -
<D:Label runat="server" ID="lblTitleBrandName" LocalizeText="false">Bewerken</D:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<X:PageControl Id="tabsMain" runat="server" Width="100%">
	<TabPages>
		<X:TabPage Text="Algemeen" Name="Generic">
			<Controls>
				<table class="dataformV2">
					<tr>
						<td class="label">
							<D:LabelEntityFieldInfo runat="server" id="lblAccountId">Account</D:LabelEntityFieldInfo>
						</td>
						<td class="control">
							<X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlAccountId" UseDataBinding="true" EntityName="Account" TextField="Name" IsRequired="true"></X:ComboBoxLLBLGenEntityCollection>
						</td>
						<td class="label">
                            <D:LabelEntityFieldInfo runat="server" id="lblCompanyId">Company</D:LabelEntityFieldInfo>
						</td>
						<td class="control">
                            <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlCompanyId" UseDataBinding="true" EntityName="Company" TextField="Name" IsRequired="true" PreventEntityCollectionInitialization="true"></X:ComboBoxLLBLGenEntityCollection>
						</td>        
					</tr> 
				 </table>			
			</Controls>
		</X:TabPage>
	</TabPages>
</X:PageControl>
  
</asp:Content>

