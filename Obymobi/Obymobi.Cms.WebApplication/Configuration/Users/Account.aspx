<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Configuration.Users.Account" Title="Account" Codebehind="Account.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
<D:Label runat="server" ID="lblTitleProducts">Accounts</D:Label> -
<D:Label runat="server" ID="lblTitleEdit">Bewerken</D:Label> -
<D:Label runat="server" ID="lblTitleBrandName" LocalizeText="false">Bewerken</D:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<X:PageControl Id="tabsMain" runat="server" Width="100%">
	<TabPages>
		<X:TabPage Text="Algemeen" Name="Generic">
			<Controls>
				<table class="dataformV2">
					<tr>
						<td class="label">
							<D:LabelEntityFieldInfo runat="server" id="lblName">Naam</D:LabelEntityFieldInfo>
						</td>
						<td class="control">
							<D:TextBoxString ID="tbName" runat="server"></D:TextBoxString>
						</td>
						<td class="label">
						</td>
						<td class="control">
						</td>        
					</tr> 
				 </table>			
			</Controls>
		</X:TabPage>
	</TabPages>
</X:PageControl>
  
</asp:Content>

