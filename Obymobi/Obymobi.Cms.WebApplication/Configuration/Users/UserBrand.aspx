<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Configuration.Users.UserBrand" Title="User brand" Codebehind="UserBrand.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
<D:Label runat="server" ID="lblTitleProducts">Account users</D:Label> -
<D:Label runat="server" ID="lblTitleEdit">Bewerken</D:Label> -
<D:Label runat="server" ID="lblTitleBrandName" LocalizeText="false">Bewerken</D:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<X:PageControl Id="tabsMain" runat="server" Width="100%">
	<TabPages>
		<X:TabPage Text="Algemeen" Name="Generic">
			<Controls>
				<table class="dataformV2">
					<tr>
						<td class="label">
							<D:Label runat="server" id="lblUser">User</D:Label>
						</td>
						<td class="control">
							<D:Label runat="server" id="lblUserName" LocalizeText="false"></D:Label>
						</td>
						<td class="label">
                            <D:Label runat="server" id="lblBrand">Brand</D:Label>
						</td>
						<td class="control">
                            <D:Label runat="server" id="lblBrandName" LocalizeText="false"></D:Label>
						</td>        
					</tr> 
                    <tr>
						<td class="label">
							<D:Label runat="server" id="lblRole">Role</D:Label>
						</td>
						<td class="control">
							<X:ComboBoxInt ID="cbRole" runat="server" IsRequired="true" UseDataBinding="true" />
						</td>
						<td class="label">
                            
						</td>
						<td class="control">
                            
						</td>        
					</tr> 
				 </table>			
			</Controls>
		</X:TabPage>
	</TabPages>
</X:PageControl>
  
</asp:Content>

