﻿using DevExpress.Web;
using Dionysos.Web;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web.UI.WebControls;

namespace Obymobi.ObymobiCms.Configuration.Users
{
    public partial class Users : Dionysos.Web.UI.Page
    {
        private UserCollection users;

        public void Add()
        {
            string baseUrl = this.Request.Path.Substring(0, this.Request.Path.IndexOf("Users.aspx", StringComparison.InvariantCultureIgnoreCase));
            string url = string.Format("{0}/User.aspx?mode={1}&entity={2}&id=", baseUrl, "add", "User");
            WebShortcuts.ResponseRedirect(url, true);
        }

        private void OpenUserPage(int userId)
        {
            string baseUrl = this.Request.Path.Substring(0, this.Request.Path.IndexOf("Users.aspx", StringComparison.InvariantCultureIgnoreCase));
            string url = string.Format("{0}User.aspx?id={1}", baseUrl, userId);
            ASPxWebControl.RedirectOnCallback(url);
        }

        private void DeleteUser(int userId)
        {
            UserEntity user = new UserEntity(userId);
            if (!user.IsNew)
            {
                user.Delete();
            }
            ASPxWebControl.RedirectOnCallback(this.Request.RawUrl);
        }
        
        private void LoadDataSource()
        {
            bool includeLockecOutUsers = false;

            if (CmsSessionHelper.CurrentRole >= Role.Crave)
            {
                this.plhShowLockedOutUsers.Visible = true;
                includeLockecOutUsers = this.cbIncludeLockedOutUsers.Checked;
            }

            users = new UserCollection();

            if (CmsSessionHelper.CurrentRole == Role.Reseller)
            {
                List<int> companyIds = CmsSessionHelper.CompanyCollectionForUser.Select(c => c.CompanyId).ToList();

                PredicateExpression filter = new PredicateExpression(UserFields.Role == Role.Customer);
                if (!includeLockecOutUsers)
                {
                    filter.Add(UserFields.IsLockedOut == false);
                }

                PrefetchPath prefetch = new PrefetchPath(EntityType.UserEntity);
                prefetch.Add(UserEntityBase.PrefetchPathAccountEntity).SubPath.Add(AccountEntityBase.PrefetchPathAccountCompanyCollection);

                UserCollection unfilteredUsers = new UserCollection();
                unfilteredUsers.GetMulti(filter, prefetch);

                foreach (UserEntity user in unfilteredUsers)
                {
                    if (user.AccountEntity == null)
                    {
                        continue;
                    }
                    if (user.AccountEntity.AccountCompanyCollection.Any(c => !companyIds.Contains(c.CompanyId)))
                    {
                        continue;
                    }

                    users.Add(user);
                }
            }
            else
            {
                PredicateExpression filter = new PredicateExpression(UserFields.Role <= CmsSessionHelper.CurrentRole);
                if (!includeLockecOutUsers)
                {
                    filter.Add(UserFields.IsLockedOut == false);
                }

                users.GetMulti(filter);
            }

            this.gridUsers.DataSource = users;
            this.gridUsers.DataBind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (CmsSessionHelper.CurrentRole < Role.Reseller)
                this.Response.Redirect("~/401.aspx");

            LoadDataSource();

            this.gridUsers.CustomCallback += this.gridUsers_CustomCallback;
            this.gridUsers.ClientSideEvents.RowDblClick = this.NodeDblClickMethod;
            this.gridUsers.HtmlRowCreated += this.GridUsers_HtmlRowCreated;
            this.gridUsers.HtmlDataCellPrepared += GridUsersOnHtmlDataCellPrepared;
            this.gridUsers.CustomButtonCallback += this.gridUsers_CustomButtonCallback;
        }

        private void GridUsersOnHtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.DataColumn.FieldName == "Username")
            {
                UserEntity user = this.users.FirstOrDefault(x => x.UserId == (int) e.KeyValue);
                if (user != null && user.IsLockedOut)
                {
                    e.Cell.ForeColor = Color.Gray;
                    e.Cell.Font.Strikeout = true;
                }
            }
        }

        private void GridUsers_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Data)
            {
                e.Row.Height = Unit.Pixel(20);
            }
        }

        private void gridUsers_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            int userId = int.Parse(e.Parameters);
            this.OpenUserPage(userId);
        }

        private void gridUsers_CustomButtonCallback(object sender, ASPxGridViewCustomButtonCallbackEventArgs e)
        {
            object objUserId = this.gridUsers.GetRowValues(e.VisibleIndex, "UserId");
            int userId = int.Parse(objUserId.ToString());

            if (e.ButtonID.Equals("openUserPage", StringComparison.InvariantCultureIgnoreCase))
            {
                this.OpenUserPage(userId);
            }
            else if (e.ButtonID.Equals("deleteUser", StringComparison.InvariantCultureIgnoreCase))
            {
                this.DeleteUser(userId);
            }
        }

        private string NodeDblClickMethod
        {
            get
            {
                return @" function(s, e) {
                                var key = s.GetRowKey(e.visibleIndex);
	                            gridUsers.PerformCallback(key);
		                        e.cancel = true;	                                                                    
                            }";
            }
        }
    }
}
