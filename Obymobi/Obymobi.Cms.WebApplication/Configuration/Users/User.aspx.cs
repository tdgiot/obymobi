using System;
using System.Collections.Generic;
using System.Linq;
using Dionysos;
using Dionysos.Data.LLBLGen;
using Obymobi.Cms.WebApplication.Validation.UserPasswords.Interfaces;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Configuration.Users
{
    public partial class User : Dionysos.Web.UI.PageLLBLGenEntity
    {
        private readonly IUserPasswordValidator userPasswordValidator;

        public User(IUserPasswordValidator userPasswordValidator)
        {
            this.userPasswordValidator = userPasswordValidator;
        }

        #region Methods

        private void LoadUserControls()
        {
            if (CmsSessionHelper.CurrentRole == Role.Reseller)
            {
                this.cbRole.Visible = false;
                this.lblRole.Visible = false;
                this.cbRole.IsRequired = false;
            }
            else
            {
                this.ddlAccountId.Enabled = CmsSessionHelper.CurrentRole >= Role.Crave;

                switch (CmsSessionHelper.CurrentRole)
                {
                    case Role.GodMode:
                        this.cbRole.DataBindEnum<Role>();
                        break;
                    case Role.Administrator:
                        this.cbRole.DataBindEnum(valuesToSkip: new[] { Role.GodMode, Role.Crave });
                        break;
                    case Role.Crave:
                        this.cbRole.DataBindEnum(valuesToSkip: new[] { Role.GodMode });
                        break;
                }
            }

            this.trTestDeveloperFlags.Visible = CmsSessionHelper.CurrentRole == Role.GodMode;

            DataBindAccounts();

            IEnumerable<TimeZone> timezones = TimeZone.Mappings.Values.OrderByDescending(x => x.UtcOffset);

            this.ddlTimeZoneOlsonId.DataSource = timezones;
            this.ddlTimeZoneOlsonId.DataBind();
        }

        private void DataBindAccounts()
        {
            AccountCollection accounts = FetchAllAccounts();

            if (CmsSessionHelper.CurrentRole == Role.Reseller)
            {
                IEnumerable<int> companyIds = CmsSessionHelper.CompanyCollectionForUser
                                                              .Select(c => c.CompanyId);

                accounts = FilterAccountsOnCompanyIds(accounts, companyIds);
            }

            this.ddlAccountId.DataSource = accounts;
            this.ddlAccountId.DataBind();
        }

        private static AccountCollection FetchAllAccounts()
        {
            ISortExpression sort = new SortExpression(AccountFields.Name | SortOperator.Ascending);

            IPrefetchPath prefetch = new PrefetchPath(EntityType.AccountEntity)
            {
                AccountEntityBase.PrefetchPathAccountCompanyCollection
            };

            AccountCollection accounts = new AccountCollection();
            accounts.GetMulti(null, 0, sort, null, prefetch);

            return accounts;
        }

        private static AccountCollection FilterAccountsOnCompanyIds(AccountCollection accountCollection, IEnumerable<int> companyIds)
        {
            return accountCollection.Where(account => account.AccountCompanyCollection
                                                             .Any(accountCompany => companyIds.Contains(accountCompany.CompanyId)))
                                    .ToEntityCollection<AccountCollection>();
        }

        private void User_DataSourceLoaded(object sender)
        {
            if (!this.DataSourceAsUserEntity.IsNew)
            {
                if (CmsSessionHelper.CurrentRole < this.DataSourceAsUserEntity.Role)
                {
                    throw new UnauthorizedAccessException("Attempted to access a user whose role is higher than the logged in user");
                }
            }

            if (this.DataSourceAsUserEntity.IsNew)
            {
                this.DataSourceAsUserEntity.IsApproved = true;
            }

            this.ddlTimeZoneOlsonId.SelectedItem = this.ddlTimeZoneOlsonId.Items.FindByValue(this.DataSourceAsUserEntity.TimeZoneOlsonId);
        }

        public override bool Save()
        {
            bool isSaved = false;

            if (CmsSessionHelper.CurrentRole == Role.Reseller && DataSourceAsUserEntity.IsNew)
            { DataSourceAsUserEntity.Role = Role.Customer; }

            CustomValidation();

            if (!IsValid)
            { return false; }

            DataSourceAsUserEntity.TimeZoneOlsonId = ddlTimeZoneOlsonId.SelectedValueString;

            if (DataSourceAsUserEntity.IsLockedOut != cbIsLockedOut.Checked)
            {
                // Reset failed password attempt after changing the locked out status
                DataSourceAsUserEntity.FailedPasswordAttemptCount = 0;
            }

            if (DataSourceAsUserEntity.IsNew && IsNewUserDuplicate())
            {
                MultiValidatorDefault.AddError("A user with this username already exists.");
                Validate();
                return false; 
            }

            if (base.Save())
            {
                if (tbPasswordOne.Text.Length > 0 && (tbPasswordOne.Text == tbPasswordTwo.Text))
                {
                    Dionysos.Web.Security.UserManager.Instance.SetPassword(DataSourceAsUserEntity, tbPasswordOne.Text);
                }

                isSaved = true;
            }

            if (!isSaved)
            {
                tbPasswordOne.Text = string.Empty;
                tbPasswordTwo.Text = string.Empty;
                MultiValidatorDefault.AddError(Translate("UserSaveError", "Er is een fout opgetreden waardoor de gegevens niet zijn opgeslagen"));

                Validate();
            }

            return IsValid;
        }

        private void CustomValidation()
        {
            string passwordValidationMessage = string.Empty;

            if (this.tbUsername.Text.Trim().Length < 8)
            {
                this.MultiValidatorDefault.AddError("Username needs to be at least 8 characters long");
            }
            else if (this.tbUsername.Text.Trim().Split(' ').Length > 1)
            {
                this.MultiValidatorDefault.AddError("Please enter a valid username");
            }

            if (this.DataSourceAsUserEntity.IsNew)
            {
                passwordValidationMessage = userPasswordValidator.ValidateNewPassword(tbPasswordOne.Text, tbPasswordTwo.Text);
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(this.tbPasswordOne.Text))
                {
                    passwordValidationMessage = userPasswordValidator.ValidateChangesToExistingPassword(tbPasswordOne.Text, tbPasswordTwo.Text);
                }
            }

            if (!passwordValidationMessage.IsNullOrWhiteSpace())
            {
                MultiValidatorDefault.AddError(passwordValidationMessage);
            }

            Validate();
        }

        private bool IsNewUserDuplicate()
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(UserFields.Username == tbUsername.Text);
            UserCollection users = new UserCollection();
            return users.GetDbCount(filter) > 0;
        }

        private void SetWarnings()
        {
            UserEntity userEntity = this.DataSourceAsUserEntity;

            if (!userEntity.IsNew)
            {
                if (userEntity.IsLockedOut)
                {
                    AddInformatorInfo("This user is locked out");
                }

                if (userEntity.Password.Length == 0)
                {
                    AddInformatorInfo("No password set for user");
                }

                if (!userEntity.AccountId.HasValue && !userEntity.Roles.Any(role => role.Contains("Administrator", StringComparison.OrdinalIgnoreCase) || role.Contains("Crave", StringComparison.OrdinalIgnoreCase) || role.Contains("GodMode", StringComparison.OrdinalIgnoreCase)))
                {
                    AddInformator(Dionysos.Web.UI.InformatorType.Warning, "This user isn't linked to an account");
                }
            }

            Validate();
        }

        #endregion

        #region Properties

        public UserEntity DataSourceAsUserEntity => this.DataSource as UserEntity;

        #endregion

        #region EventHandlers

        protected override void OnInit(EventArgs e)
        {
            LoadUserControls();
            DataSourceLoaded += User_DataSourceLoaded;

            this.tabsMain.AddTabPage("Brands", "UserBrands", "~/Configuration/SubPanels/UserBrandCollection.ascx");

            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (CmsSessionHelper.CurrentRole < Role.Reseller)
            {
                this.Response.Redirect("~/401.aspx");
            }

            if (this.DataSourceAsUserEntity.IsNew)
            {
                this.tbPageSize.Text = "20";
            }

            if (this.DataSourceAsUserEntity != null)
            {
                SetWarnings();
            }
        }

        #endregion
    }
}
