﻿using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.Web;
using Dionysos.Web;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using System.Web.UI.WebControls;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data;

namespace Obymobi.ObymobiCms.Configuration.Users
{
    public partial class Accounts : Dionysos.Web.UI.Page
    {
        #region Methods

        private void LoadUserControls()
        {
            AccountCollection accounts = new AccountCollection();

            if (CmsSessionHelper.CurrentRole >= Role.Administrator)
            {
                accounts.GetMulti(null);
            }
            else if (CmsSessionHelper.CurrentRole == Role.Reseller)
            {
                List<int> companyIds = CmsSessionHelper.CompanyCollectionForUser.Select(c => c.CompanyId).ToList();

                PrefetchPath prefetch = new PrefetchPath(EntityType.AccountEntity);
                prefetch.Add(AccountEntityBase.PrefetchPathAccountCompanyCollection);

                AccountCollection unfilteredAccounts = new AccountCollection();
                unfilteredAccounts.GetMulti(null, prefetch);

                foreach (AccountEntity account in unfilteredAccounts)
                {
                    if (account.AccountCompanyCollection.Any(c => !companyIds.Contains(c.CompanyId)))
                    {
                        continue;
                    }

                    accounts.Add(account);
                }
            }
            
            this.gridAccounts.DataSource = accounts;
            this.gridAccounts.DataBind();
        }

        public void Add()
        {
            string baseUrl = this.Request.Path.Replace("Accounts", "Account");
            string url = string.Format("{0}?mode={1}&entity={2}&id=", baseUrl, "add", "Account");
            WebShortcuts.ResponseRedirect(url, true);
        }

        private void OpenAccount(int accountId)
        {
            string baseUrl = this.Request.Path.Replace("Accounts", "Account");
            string url = string.Format("{0}?id={1}", baseUrl, accountId);
            DevExpress.Web.ASPxWebControl.RedirectOnCallback(url);
        }

        private void DeleteAccount(int accountId)
        {
            AccountEntity account = new AccountEntity(accountId);
            if (account != null && !account.IsNew)
            {
                account.Delete();
            }
            DevExpress.Web.ASPxWebControl.RedirectOnCallback(this.Request.RawUrl);
        }

        #endregion

        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (CmsSessionHelper.CurrentRole < Role.Reseller)
            {
                this.Response.Redirect("~/401.aspx");
            }

            this.gridAccounts.CustomCallback += this.gridAccounts_CustomCallback;
            this.gridAccounts.ClientSideEvents.RowDblClick = this.NodeDblClickMethod;
            this.gridAccounts.HtmlRowCreated += this.GridAccount_HtmlRowCreated;
            this.gridAccounts.CustomButtonCallback += this.gridAccounts_CustomButtonCallback;            
        }

        private void gridAccounts_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            int accountId = int.Parse(e.Parameters);
            this.OpenAccount(accountId);
        }

        private void GridAccount_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Data)
            {                
                e.Row.Height = Unit.Pixel(20);
            }
        }

        private void gridAccounts_CustomButtonCallback(object sender, ASPxGridViewCustomButtonCallbackEventArgs e)
        {
            object objAccountId = this.gridAccounts.GetRowValues(e.VisibleIndex, "AccountId");
            int accountId = int.Parse(objAccountId.ToString());

            if (e.ButtonID.Equals("openAccount", StringComparison.InvariantCultureIgnoreCase))
            {
                this.OpenAccount(accountId);
            }
            else if (e.ButtonID.Equals("deleteAccount", StringComparison.InvariantCultureIgnoreCase))
            {
                this.DeleteAccount(accountId);
            }
        }

        #endregion

        #region Properties

        private string NodeDblClickMethod
        {
            get
            {
                return @" function(s, e) {
                                var key = s.GetRowKey(e.visibleIndex);
	                            gridAccounts.PerformCallback(key);
		                        e.cancel = true;	                                                                    
                            }";
            }
        }

        #endregion
    }
}
