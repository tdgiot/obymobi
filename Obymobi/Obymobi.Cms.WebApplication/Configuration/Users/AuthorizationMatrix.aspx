﻿<%@ Page Title="User authorization matrix" Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Configuration.Users.AuthorizationMatrix" Codebehind="AuthorizationMatrix.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitleHolder" Runat="Server">
    User authorization matrix
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhToolbarHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhContentHolder" Runat="Server">
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Pages" Name="Pages">
				<Controls>
                    <span class="nobold" style="width: 100%; height: 26px;">
                        <D:HyperLink runat="server" ID="hlExpandAll">Expand all</D:HyperLink> | <D:HyperLink runat="server" ID="hlCollapseAll">Collapse all</D:HyperLink>
                    </span>                                    
                    <X:TreeList ID="tlPages" ClientInstanceName="tlPages" ClientIDMode="Static" runat="server" Width="100%" AutoGenerateColumns="false" style="margin-top: 9px;">
                        <Settings GridLines="Both" />
                        <Settings ShowColumnHeaders="true" />
                    </X:TreeList>
                </Controls>
			</X:TabPage>
            <X:TabPage Text="Entities" Name="Entities">
				<Controls>
                    <div style="margin-bottom: 10px">
                        <p style="font-weight: bold; margin-bottom:4px">Legend:</p>
                        <p>C = Create authorization - role allowed to create new entity</p>
                        <p>R = Read authorization - role allowed to view entity</p>
                        <p>U = Update authorization - role allowed to update existing entity</p>
                        <p>D = Delete authorization - role allowed to delete entity</p>
                    </div>

                    <X:GridView ID="gvEntityAuthorisation" runat="server" Width="100%">
                        <SettingsPager PageSize="100" />
                        <SettingsBehavior AutoFilterRowInputDelay="350" /> 
                        <Settings ShowFilterRow="True" />
                    </X:GridView>
				</Controls>
			</X:TabPage>
        </TabPages>
    </X:PageControl>
</asp:Content>

