﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Configuration.Users.Users" Codebehind="Users.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitleHolder" runat="Server">
    Users
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhToolbarHolder" runat="Server">
    <X:ToolBarButton runat="server" ID="btAdd" CommandName="Add" Text="Add" Image-Url="~/Images/Icons/add.png" Style="margin-left: 4px" />    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhContentHolder" runat="Server">
<div>
    <D:PlaceHolder runat="server" ID="plhShowLockedOutUsers" Visible="False">
        <table cellpadding="0" cellspacing="0" style="margin-bottom: 14px">
            <tr>
                <td>Include locked out users</td>
                <td style="padding-left: 4px"><D:CheckBox runat="server" ID="cbIncludeLockedOutUsers" AutoPostBack="True" /></td>
            </tr>
        </table>
    </D:PlaceHolder>

    <X:GridView ID="gridUsers" ClientInstanceName="gridUsers" runat="server" Width="100%" KeyFieldName="UserId">
            <SettingsPager PageSize="20"></SettingsPager>
            <SettingsBehavior AllowGroup="false" AllowDragDrop="false" AutoFilterRowInputDelay="350" />        
            <Columns>
                <dxwgv:GridViewDataColumn FieldName="Username" VisibleIndex="1" SortIndex="0" SortOrder="Ascending" Width="48%">
				    <Settings AutoFilterCondition="Contains" />
                    <Settings SortMode="DisplayText" />
                    <Settings FilterMode="DisplayText" />
                </dxwgv:GridViewDataColumn>
                <dxwgv:GridViewDataColumn FieldName="Role" VisibleIndex="2" Width="48%">
				    <Settings AutoFilterCondition="Contains" />                    
                </dxwgv:GridViewDataColumn>
                <dxwgv:GridViewCommandColumn ButtonType="Image" VisibleIndex="3" Width="2%">
                    <CustomButtons>
                        <dxwgv:GridViewCommandColumnCustomButton ID="openUserPage" Text="Open">
                            <Image Url="~/Images/Icons/pencil.png" />
                        </dxwgv:GridViewCommandColumnCustomButton>
                    </CustomButtons>
                </dxwgv:GridViewCommandColumn>                         
                <dxwgv:GridViewCommandColumn ButtonType="Image" VisibleIndex="4" Width="2%">
                    <CustomButtons>
                        <dxwgv:GridViewCommandColumnCustomButton ID="deleteUser" Text="Delete">
                            <Image Url="~/Images/Icons/delete.png" />
                        </dxwgv:GridViewCommandColumnCustomButton>
                    </CustomButtons>
                </dxwgv:GridViewCommandColumn>
            </Columns>
            <Settings ShowFilterRow="True" />
        </X:GridView>   
</div>
</asp:Content>