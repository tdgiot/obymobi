﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Obymobi.ObymobiCms.Configuration.Users
{


    public partial class AccountCompany
    {

        /// <summary>
        /// lblTitleProducts control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Dionysos.Web.UI.WebControls.Label lblTitleProducts;

        /// <summary>
        /// lblTitleEdit control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Dionysos.Web.UI.WebControls.Label lblTitleEdit;

        /// <summary>
        /// lblTitleBrandName control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Dionysos.Web.UI.WebControls.Label lblTitleBrandName;

        /// <summary>
        /// tabsMain control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Dionysos.Web.UI.DevExControls.PageControl tabsMain;

        /// <summary>
        /// lblAccountId control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Dionysos.Web.UI.WebControls.LabelEntityFieldInfo lblAccountId;

        /// <summary>
        /// ddlAccountId control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Dionysos.Web.UI.DevExControls.ComboBoxLLBLGenEntityCollection ddlAccountId;

        /// <summary>
        /// lblCompanyId control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Dionysos.Web.UI.WebControls.LabelEntityFieldInfo lblCompanyId;

        /// <summary>
        /// ddlCompanyId control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Dionysos.Web.UI.DevExControls.ComboBoxLLBLGenEntityCollection ddlCompanyId;
    }
}
