using System;
using System.Linq;
using Obymobi.Enums;
using Obymobi.Data.EntityClasses;

namespace Obymobi.ObymobiCms.Configuration.Users
{
    public partial class UserBrand : Dionysos.Web.UI.PageLLBLGenEntity
    {
        #region Methods

        private void LoadUserControls()
        {
            this.cbRole.DataBindEnum<BrandRole>();
        }

        #endregion

        #region EventHandlers

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.lblUserName.Value = this.DataSourceAsUserBrand.UserUsername;
            this.lblBrandName.Value = this.DataSourceAsUserBrand.BrandName;

            UserBrandEntity userBrand = this.DataSourceAsUserBrand.BrandEntity.UserBrandCollection.FirstOrDefault(a => a.UserId == CmsSessionHelper.CurrentUser.UserId);
            if (userBrand == null || userBrand.Role < BrandRole.Owner)
            {
                Response.Redirect("~/401.aspx");
                return;
            }
        }

        private UserBrandEntity DataSourceAsUserBrand
        {
            get { return (UserBrandEntity)this.DataSource; }
        }

        #endregion
    }
}
