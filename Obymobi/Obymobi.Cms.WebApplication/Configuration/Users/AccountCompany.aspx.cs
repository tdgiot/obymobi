using System;
using System.Collections.Generic;
using System.Linq;
using Dionysos.Web;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.ObymobiCms.MasterPages;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Configuration.Users
{
    public partial class AccountCompany : Dionysos.Web.UI.PageLLBLGenEntity
    {
        #region Methods

        private void LoadUserControls()
        {
            CompanyCollection companies = new CompanyCollection();

            if (CmsSessionHelper.CurrentRole >= Role.Administrator)
            {
                companies.GetMulti(null);
            }
            else if (CmsSessionHelper.CurrentRole == Role.Reseller)
            {
                List<int> companyIds = CmsSessionHelper.CompanyCollectionForUser.Select(c => c.CompanyId).ToList();
                PredicateExpression filter = new PredicateExpression(CompanyFields.CompanyId == companyIds);

                companies.GetMulti(filter);
            }

            this.ddlCompanyId.DataSource = companies;
            this.ddlAccountId.DataBind();
        }

        #endregion

        #region EventHandlers

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // Redirect the user if it's not an administrator
            if (CmsSessionHelper.CurrentRole < Role.Reseller)
                Response.Redirect("~/401.aspx");
        }

        public override bool Delete()
        {
            int accountId = ((AccountCompanyEntity)this.DataSource).AccountId;
            MasterPageBase.ClearCompaniesCacheForAccount(accountId);

            return base.Delete();
        }

        #endregion
    }
}
