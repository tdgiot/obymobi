using System;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.ObymobiCms.MasterPages;

namespace Obymobi.ObymobiCms.Configuration.Users
{
    public partial class Account : Dionysos.Web.UI.PageLLBLGenEntity
    {
        public AccountEntity DataSourceAsAccountEntity
		{
			get
			{
				return this.DataSource as AccountEntity;
			}
		}

        protected override void OnInit(EventArgs e)
        {
            this.tabsMain.AddTabPage("Companies", "AccountCompanies", "~/Configuration/SubPanels/AccountCompanyCollection.ascx");
            this.tabsMain.AddTabPage("Users", "Users", "~/Configuration/SubPanels/UserCollection.ascx");

            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // Redirect the user if it's not an administrator
            if (CmsSessionHelper.CurrentRole < Role.Reseller)
                Response.Redirect("~/401.aspx");
        }

        public override bool Save()
        {
            bool ret = base.Save();

            if (ret)
            {
                MasterPageBase.ClearCompaniesCacheForAccount(this.DataSourceAsAccountEntity.AccountId);
            }

            return ret;
        }
    }
}
