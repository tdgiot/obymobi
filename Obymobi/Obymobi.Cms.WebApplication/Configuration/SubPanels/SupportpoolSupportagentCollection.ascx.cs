﻿using System;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using System.Linq;

namespace Obymobi.ObymobiCms.Configuration.SubPanels
{
    public partial class SupportpoolSupportagentCollection : Dionysos.Web.UI.DevExControls.SubPanelLLBLOverviewDataSourceCollection
    {
        #region Methods

        private void SetGui()
        {
            var sort = new SortExpression(new SortClause(SupportagentFields.Firstname, SortOperator.Ascending));

            var supportagents = new SupportagentCollection();
            supportagents.GetMulti(null, 0, sort, null, null, null, 0, 0);

            this.ddlSupportagentId.DataSource = supportagents;
            this.ddlSupportagentId.DataBind();
        }

        private void AddSupportpoolSupportagent()
        {
            if (this.ddlSupportagentId.ValidId > 0)
            {
                SupportpoolSupportagentEntity supportpoolSupportagent = new SupportpoolSupportagentEntity();
                supportpoolSupportagent.SupportpoolId = this.ParentPrimaryKeyFieldValue;
                supportpoolSupportagent.SupportagentId = this.ddlSupportagentId.ValidId;

                if (supportpoolSupportagent.Save())
                    this.DataSourceAsSupportpoolEntity.SupportpoolSupportagentCollection.Add(supportpoolSupportagent);

                this.SetGui();
            }
        }

        #endregion

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            this.DataSourceLoaded += SupportpoolSupportagentCollection_DataSourceLoaded;
            base.OnInit(e);
            this.EntityName = "SupportpoolSupportagent";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.btnAdd.Click += btnAdd_Click;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            this.AddSupportpoolSupportagent();
        }

        private void SupportpoolSupportagentCollection_DataSourceLoaded(object sender)
        {
            this.SetGui();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Return the page's datasource as a SupportpoolEntity
        /// </summary>
        public SupportpoolEntity DataSourceAsSupportpoolEntity
        {
            get
            {
                return this.ParentDataSource as SupportpoolEntity;
            }
        }

        #endregion
    }
}