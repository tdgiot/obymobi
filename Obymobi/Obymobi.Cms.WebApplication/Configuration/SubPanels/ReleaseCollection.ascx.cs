﻿using System;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Configuration.Subpanels
{
    public partial class ReleaseCollection : Dionysos.Web.UI.DevExControls.SubPanelLLBLOverviewDataSourceCollection
    {
        protected override void OnInit(EventArgs e)
        {
            EntityName = "Release";
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (DataSource is LLBLGenProDataSource datasource)
            {
                // Sort the releases bases on the version descending
                DataSource.SorterToUse = new SortExpression(new SortClause(ReleaseFields.Version, SortOperator.Descending));
            }

            if (MainGridView != null)
            {
                MainGridView.LoadComplete += new EventHandler(MainGridView_LoadComplete);
                MainGridView.CustomColumnDisplayText += new DevExpress.Web.ASPxGridViewColumnDisplayTextEventHandler(OnCustomColumnDisplayText);
            }
        }

        void MainGridView_LoadComplete(object sender, EventArgs e)
        {
            // Hide the comment column
            if (MainGridView.Columns["Comment"] != null)
            {
                MainGridView.Columns["Comment"].SetColVisible(false);
            }
        }

        private void OnCustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs e)
        {
            if (e.Column.FieldName == "ReleaseGroup")
            {
                int releaseGroup = (int)e.GetFieldValue("ReleaseGroup");
                e.DisplayText = releaseGroup > 0 ? Enum.GetName(typeof(ReleaseGroup), releaseGroup) : "Unknown";
            }
        }
    }
}