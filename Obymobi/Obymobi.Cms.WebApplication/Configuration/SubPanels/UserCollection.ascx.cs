﻿using Dionysos.Data.LLBLGen;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using Obymobi.Enums;

namespace Obymobi.ObymobiCms.Configuration.Subpanels
{
    public partial class UserCollection : Dionysos.Web.UI.DevExControls.SubPanelLLBLOverviewDataSourceCollection
    {        
        #region Methods

        protected override void OnInit(EventArgs e)
        {            
            base.OnInit(e);
			
            this.EntityName = "User";

            this.MainGridView.ShowDeleteHyperlinkColumn = false;

            PredicateExpression filter = new PredicateExpression(UserFields.AccountId == DBNull.Value);
            if (CmsSessionHelper.CurrentRole == Role.Reseller)
            {
                filter.Add(UserFields.Role == Role.Customer);
            }

            Obymobi.Data.CollectionClasses.UserCollection userCollection = EntityCollection.GetMulti<Obymobi.Data.CollectionClasses.UserCollection>(filter);
            this.ddlUserId.DataSource = userCollection;
            this.ddlUserId.DataBind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.btnAdd.Click += new EventHandler(btnAdd_Click);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (this.ddlUserId.ValidId > 0)
            {
                UserEntity userEntity = new UserEntity(this.ddlUserId.ValidId);
                if (!userEntity.IsNew)
                {
                    bool wasNew = this.ParentDataSource.IsNew;
                    if (wasNew)
                        this.ParentDataSource.Save();

                    userEntity.AccountId = this.ParentPrimaryKeyFieldValue;
                    userEntity.Save();

                    if (wasNew)
                        this.Parent.Save();
                }
            }
        }

        #endregion
      
    }
}