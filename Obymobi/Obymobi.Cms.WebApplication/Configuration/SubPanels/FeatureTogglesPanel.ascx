﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FeatureTogglesPanel.ascx.cs" Inherits="Obymobi.Cms.WebApplication.Configuration.SubPanels.FeatureTogglesPanel" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Import Namespace="Obymobi.Data.EntityClasses" %>
<%@ Import Namespace="Obymobi.Enums" %>

<style type="text/css">    
    table tbody tr td {
        text-align: center;
        min-width: 100px;
    }

    table select {
        text-align: center;
        width: 100%;
    }

    table tbody tr td:first-child {
        min-width: 20px;
    }

    table tbody tr td.text-left {
        text-align: left;
        margin-left: 8px;
    }
</style>

<table class="dxgvControl_Glass" cellspacing="0" cellpadding="0" border="0" style="width: 100%; border-collapse: collapse; border-collapse: separate; margin-top: 10px;">
    <tbody>
        <tr>
            <td>
                <table class="dxgvTable_Glass" cellspacing="0" cellpadding="0" border="0" style="width: 100%; border-collapse: collapse; empty-cells: show; overflow: hidden;">
                    <tbody>
                        <tr>
                            <td class="dxgvHeader_Glass" style="border-top-width: 0px; border-left-width: 0px;">
                                <table border="0" style="width: 100%; border-collapse: collapse;">
                                    <tbody>
                                        <tr>
                                            <td class="text-left">
                                                <strong>Feature</strong>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>

                            <td class="dxgvHeader_Glass" style="border-top-width: 0px; border-left-width: 0px;">
                                <table border="0" style="width: 100%; border-collapse: collapse;">
                                    <tbody>
                                    <tr>
                                        <td>
                                            <strong>Companies</strong>
                                            <a id="filter-info-popup-release-group">
                                                <img src="<%= ResolveUrl("~/Images/Icons/information.png") %>" style="float: right;" alt="Filter information" />
                                            </a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                            
                            <td class="dxgvHeader_Glass" style="border-top-width: 0px; border-left-width: 0px;">
                                <table border="0" style="width: 100%; border-collapse: collapse;">
                                    <tbody>
                                    <tr>
                                        <td>
                                            <strong>Users</strong>
                                            <a id="filter-info-popup-user-role">
                                                <img src="<%= ResolveUrl("~/Images/Icons/information.png") %>" style="float: right;" alt="Filter information" />
                                            </a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    
                        <% string disabled = !AllowEditing ? "disabled" : string.Empty; %>

                        <% foreach (FeatureToggle featureToggle in EnumUtil.GetValues<FeatureToggle>()) { %>
                            <% FeatureToggleAvailabilityEntity availability = GetFeatureToggleOrDefault(featureToggle); %>
                            <tr class="dxgvFilterRow_Glass">
                                <td class="dxgv text-left" style="padding-left: 5px;">
                                    <%= featureToggle.GetStringValue() %>
                                </td>
                                
                                <td class="dxgv">
                                    <select name="<%= featureToggle %>_release_group" <%= disabled %>>
                                        <% foreach (ReleaseGroup releaseGroup in EnumUtil.GetValues<ReleaseGroup>()) { %>
                                            <option value="<%= releaseGroup %>" <%= availability.ReleaseGroup == releaseGroup ? "selected" : string.Empty %>><%= releaseGroup.GetStringValue() %></option>
                                        <% } %>
                                    </select>
                                </td>

                                <td class="dxgv">
                                    <select name="<%= featureToggle %>_user_role" <%= disabled %>>
                                        <% foreach (Role role in GetAvailableRoles()) { %>
                                            <option value="<%= role %>" <%= availability.Role == role ? "selected" : string.Empty %>><%= role.GetStringValue() %></option>
                                        <% } %>
                                    </select>
                                </td>
                            </tr>
                        <% } %>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>

<dx:ASPxPopupControl PopupElementID="filter-info-popup-release-group" Width="350" PopupAction="MouseOver" CloseAction="MouseOut" CloseOnEscape="false" Modal="false" ShowOnPageLoad="false" HeaderText="Companies" runat="server">
    <ContentCollection>
        <dx:PopupControlContentControl runat="server">
            <p>
                The feature will be available for the selected company group and the groups above it in the list.
            </p>
        </dx:PopupControlContentControl>
    </ContentCollection>
</dx:ASPxPopupControl>

<dx:ASPxPopupControl PopupElementID="filter-info-popup-user-role" Width="350" PopupAction="MouseOver" CloseAction="MouseOut" CloseOnEscape="false" Modal="false" ShowOnPageLoad="false" HeaderText="Users" runat="server">
    <ContentCollection>
        <dx:PopupControlContentControl runat="server">
            <p>
                The feature will be available for the selected user role and the roles above it in the list.
            </p>
        </dx:PopupControlContentControl>
    </ContentCollection>
</dx:ASPxPopupControl>