﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Configuration.SubPanels.SupportpoolSupportagentCollection" Codebehind="SupportpoolSupportagentCollection.ascx.cs" %>
<div style="overflow:hidden;margin-bottom:10px;">
   <div style="float:left;margin-right:10px;">
   <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlSupportagentId" UseDataBinding="true" EntityName="Supportagent" TextField="Name" PreventEntityCollectionInitialization="true" Width="250px" notdirty="true"></X:ComboBoxLLBLGenEntityCollection>
   </div>
   <div style="float:left;">
   <D:Button runat="server" ID="btnAdd" Text="Add agent" ToolTip="Add" />
   </div>
</div>

<X:GridViewColumnSelector runat="server" ID="gvcsSupportpoolSupportagent" >
	<Columns />
</X:GridViewColumnSelector>