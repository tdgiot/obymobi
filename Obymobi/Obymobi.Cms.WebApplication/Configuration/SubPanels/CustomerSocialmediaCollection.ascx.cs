﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using Dionysos.Web.UI;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.ObymobiCms.Configuration.Subpanels
{
    public partial class CustomerSocialmediaCollection : Dionysos.Web.UI.DevExControls.SubPanelLLBLOverviewDataSourceCollection
    {
        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "CustomerSocialmedia";
            base.OnInit(e);            
            this.PageAsPageLLBLGenEntity.DataSourceLoaded += new Dionysos.Delegates.Web.DataSourceLoadedHandler(PageAsPageLLBLGenEntity_DataSourceLoaded);
        }

        void PageAsPageLLBLGenEntity_DataSourceLoaded(object sender)
        {
            
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }        
    }
}