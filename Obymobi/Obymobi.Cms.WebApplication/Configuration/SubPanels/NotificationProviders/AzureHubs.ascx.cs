﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxTreeList;
using Dionysos;
using Dionysos.Web.UI.WebControls;

namespace Obymobi.ObymobiCms.Configuration.SubPanels.NotificationProviders
{
    public partial class AzureHubs : UserControl
    {
        private const string TRANSLATION_KEY_PREFIX = "Obymobi.ObymobiCms.Generic.SubPanels.Sms.KeywordPanel.";

        private BaseDataSource<AzureHubManager> hubDataSource;

        #region Events

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            CreateTreeList();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.tlAzureHubs.DataBind();
        }

        #endregion

        private void InitDataSource()
        {
            this.hubDataSource = new BaseDataSource<AzureHubManager>(new AzureHubManager());
            this.tlAzureHubs.DataSource = this.hubDataSource;
        }

        private void CreateTreeList()
        {
            // Create Tree View
            this.tlAzureHubs.SettingsEditing.Mode = TreeListEditMode.Inline;
            this.tlAzureHubs.SettingsBehavior.AllowSort = false;
            this.tlAzureHubs.SettingsBehavior.AllowDragDrop = false;
            this.tlAzureHubs.ClientInstanceName = "treelist";
            this.tlAzureHubs.AutoGenerateColumns = false;
            this.tlAzureHubs.KeyFieldName = "NotificationHubId";
            this.tlAzureHubs.SettingsBehavior.AutoExpandAllNodes = true;
            this.tlAzureHubs.SettingsEditing.AllowNodeDragDrop = false;
            this.tlAzureHubs.Settings.GridLines = GridLines.Both;
            this.tlAzureHubs.ClientSideEvents.NodeDblClick = @" function(s, e) {
	                                                                    treelist.StartEdit(e.nodeKey);
		                                                                e.cancel = true;	                                                                    
                                                                    }";

            // Connection String Column
            var nameColumn = new TreeListTextColumn();
            nameColumn.Caption = "Connection String";
            nameColumn.FieldName = "ConnectionString";
            nameColumn.Width = Unit.Empty;
            nameColumn.VisibleIndex = 0;
            this.tlAzureHubs.Columns.Add(nameColumn);

            // Hub Path Column
            var pathColumn = new TreeListTextColumn();
            pathColumn.Caption = "Hub Path";
            pathColumn.FieldName = "HubPath";
            pathColumn.Width = Unit.Percentage(25);
            pathColumn.VisibleIndex = 0;
            this.tlAzureHubs.Columns.Add(pathColumn);

            // In-line Edit Column
            var editCommandColumn = new TreeListCommandColumn();
            editCommandColumn.VisibleIndex = 2;
            editCommandColumn.Width = Unit.Pixel(50);
            editCommandColumn.ShowNewButtonInHeader = true;
            editCommandColumn.EditButton.Visible = true;
            editCommandColumn.DeleteButton.Visible = true;
            editCommandColumn.ButtonType = ButtonType.Image;
            editCommandColumn.EditButton.Image.Url = this.ResolveUrl("~/images/icons/pencil.png");
            editCommandColumn.DeleteButton.Image.Url = this.ResolveUrl("~/images/icons/delete.png");
            editCommandColumn.CancelButton.Image.Url = this.ResolveUrl("~/images/icons/cross_grey.png");
            editCommandColumn.UpdateButton.Image.Url = this.ResolveUrl("~/images/icons/disk.png");
            editCommandColumn.NewButton.Image.Url = this.ResolveUrl("~/images/icons/add2.png");
            this.tlAzureHubs.Columns.Add(editCommandColumn);

            // Events
            this.tlAzureHubs.SettingsEditing.Mode = TreeListEditMode.Inline;
            this.tlAzureHubs.NodeValidating += TlAzureHubsOnNodeValidating;
            this.tlAzureHubs.CustomErrorText += TlAzureHubsOnCustomErrorText;

            // Data Source must be set in OnInit: http://documentation.devexpress.com/#AspNet/DevExpressWebASPxTreeListASPxTreeList_DataSourcetopic
            this.InitDataSource();
        }

        private void TlAzureHubsOnCustomErrorText(object sender, TreeListCustomErrorTextEventArgs e)
        {
            if (e.Exception.InnerException != null)
            {
                var exception = e.Exception.InnerException as ObymobiException;
                e.ErrorText = exception != null ? exception.AdditionalMessage : e.Exception.InnerException.Message;
            }
        }

        private void TlAzureHubsOnNodeValidating(object sender, TreeListNodeValidationEventArgs e)
        {
            if (((string)e.NewValues["ConnectionString"]).IsNullOrWhiteSpace() || ((string)e.NewValues["HubPath"]).IsNullOrWhiteSpace())
                e.NodeError = "Not all fields have been entered.";
        }
    }
}