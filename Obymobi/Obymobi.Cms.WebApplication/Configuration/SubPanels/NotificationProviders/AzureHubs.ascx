﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Configuration.SubPanels.NotificationProviders.AzureHubs" Codebehind="AzureHubs.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v20.1" Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dxwtl" %>
<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true"/>
<script type="text/javascript">    

    function OnInit(s, e) {        
        ASPxClientUtils.AttachEventToElement(s.GetMainElement(), "keypress",
        function (evt) {
            switch (evt.keyCode) {
                    //ENTER
                case 13:
                    if (s.IsEditing()) {
                        s.UpdateEdit();
                        evt.stopPropagation();
                    }
                    break;

                    //ESC               
                case 27:
                    if (s.IsEditing()) {
                        s.CancelEdit();
                        evt.stopPropagation();
                    }
                    break;
            }
        });
    }
</script>
<div>
    <dxwtl:ASPxTreeList ID="tlAzureHubs" runat="server">
        <SettingsSelection Enabled="false" Recursive="false" />        
        <ClientSideEvents Init="OnInit" />                
    </dxwtl:ASPxTreeList>               
</div>