﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Configuration.Subpanels.AccountCompanyCollection" Codebehind="AccountCompanyCollection.ascx.cs" %>
<div style="overflow:hidden;margin-bottom:10px;">
   <div style="float:left;margin-right:10px;">
   <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlCompanyId" UseDataBinding="true" EntityName="Company" TextField="Name" PreventEntityCollectionInitialization="true" Width="250px" notdirty="true"></X:ComboBoxLLBLGenEntityCollection>
   </div>
   <div style="float:left;">
   <D:Button runat="server" ID="btnAdd" Text="Add company" ToolTip="Add" />
   </div>
</div>

<X:GridViewColumnSelector runat="server" ID="gvcsAccountCompanies">
	<Columns />
</X:GridViewColumnSelector>