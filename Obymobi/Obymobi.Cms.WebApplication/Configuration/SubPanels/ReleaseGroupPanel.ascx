﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReleaseGroupPanel.ascx.cs" Inherits="Obymobi.Cms.WebApplication.Configuration.SubPanels.ReleaseGroupPanel" %>
<%@ Import Namespace="Obymobi.Data.EntityClasses" %>
<%@ Import Namespace="Obymobi.Enums" %>

<style type="text/css">    
    table tbody tr td {
        text-align: center;
        min-width: 100px;
    }

    table select {
        text-align: center;
        width: 100%;
    }

    table tbody tr td:first-child {
        min-width: 20px;
    }

    table tbody tr td.text-left {
        text-align: left;
        margin-left: 8px;
    }
</style>

<table class="dxgvControl_Glass" cellspacing="0" cellpadding="0" border="0" style="width: 100%; border-collapse: collapse; border-collapse: separate; margin-top: 10px;">
    <tbody>
        <tr>
            <td>
                <table class="dxgvTable_Glass" cellspacing="0" cellpadding="0" border="0" style="width: 100%; border-collapse: collapse; empty-cells: show; overflow: hidden;">
                    <tbody>
                        <tr>
                            <td class="dxgvHeader_Glass" style="border-top-width: 0px; border-left-width: 0px;">
                                <table border="0" style="width: 100%; border-collapse: collapse;">
                                    <tbody>
                                        <tr>
                                            <td class="text-left">
                                                <strong>Company</strong>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>

                            <td class="dxgvHeader_Glass" style="border-top-width: 0px; border-left-width: 0px;">
                                <table border="0" style="width: 100%; border-collapse: collapse;">
                                    <tbody>
                                    <tr>
                                        <td>
                                            <strong>Group</strong>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>

                        <% string disabled = !AllowEditing ? "disabled" : string.Empty; %>

                        <% foreach (CompanyEntity company in GetCompanies()) { %>
                            <tr class="dxgvFilterRow_Glass">
                                <td class="dxgv text-left" style="padding-left: 5px;">
                                    <%= company.Name %>
                                </td>
                                
                                <td class="dxgv">
                                    <select name="<%= company.CompanyId %>_options" <%= disabled %> >
                                        <option value=""></option>
                                        <% foreach (ReleaseGroup releaseGroup in EnumUtil.GetValues<ReleaseGroup>()) { %>
                                            <% if (releaseGroup == ReleaseGroup.All) continue; %>
                                            <option value="<%= releaseGroup %>" <%= company.ReleaseGroup == releaseGroup ? "selected" : string.Empty %>><%= releaseGroup.GetStringValue() %></option>
                                        <% } %>
                                    </select>
                                </td>
                            </tr>
                        <% } %>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>