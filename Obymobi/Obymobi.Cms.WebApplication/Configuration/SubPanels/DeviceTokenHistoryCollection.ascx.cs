﻿using System;

namespace Obymobi.ObymobiCms.Configuration.Subpanels
{
    public partial class DeviceTokenHistoryCollection : Dionysos.Web.UI.DevExControls.SubPanelLLBLOverviewDataSourceCollection
    {
        private void HookupEvents()
        {
            this.gvcsDeviceTokenHistory.GridView.DataBound += GridView_DataBound;
        }

        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "DeviceTokenHistory";
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.HookupEvents();

            this.MainGridView.EnableRowDoubleClick = false;
            this.MainGridView.ShowColumnSelectorButton = false;
            this.MainGridView.ShowDeleteHyperlinkColumn = false;
            this.MainGridView.ShowEditHyperlinkColumn = false;
        }

        private void GridView_DataBound(object sender, EventArgs e)
        {
            this.gvcsDeviceTokenHistory.GridView.SortBy(this.gvcsDeviceTokenHistory.GridView.Columns[0], DevExpress.Data.ColumnSortOrder.Descending);
        }
    }
}