﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Configuration.Subpanels.UserBrandCollection" Codebehind="UserBrandCollection.ascx.cs" %>
<D:PlaceHolder runat="server" ID="phlAddBrand">
<div style="overflow:hidden;margin-bottom:10px;">
Brand: <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlBrandId" UseDataBinding="true" EntityName="Brand" TextField="Name" PreventEntityCollectionInitialization="true" Width="250px" notdirty="true"></X:ComboBoxLLBLGenEntityCollection><br />
Role: <X:ComboBoxEnum runat="server" ID="ddlRole" Type="Obymobi.Enums.BrandRole, Obymobi" UseDataBinding="false" Width="250px" notdirty="true" /><br />
<D:Button runat="server" ID="btnAdd" Text="Add brand" ToolTip="Add" />   
</div>
</D:PlaceHolder>

<X:GridViewColumnSelector runat="server" ID="gvcsAccountBrands">
	<Columns />
</X:GridViewColumnSelector>