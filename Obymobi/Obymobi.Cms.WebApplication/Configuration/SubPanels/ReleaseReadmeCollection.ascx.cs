﻿using System;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Configuration.Subpanels
{
    public partial class ReleaseReadmeCollection : Dionysos.Web.UI.DevExControls.SubPanelLLBLOverviewDataSourceCollection
    {
        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "Release";
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // Sort the releases bases on the version descending
            this.DataSource.SorterToUse = new SortExpression(new SortClause(ReleaseFields.Version, SD.LLBLGen.Pro.ORMSupportClasses.SortOperator.Descending));

            this.MainGridView.LoadComplete += new EventHandler(MainGridView_LoadComplete);
        }

        void MainGridView_LoadComplete(object sender, EventArgs e)
        {
            // Hide the application name
            if (this.MainGridView.Columns[0] != null)
                this.MainGridView.Columns[0].SetColVisible(false);

            // Make the version column smaller
            if (this.MainGridView.Columns["Version"] != null)
                this.MainGridView.Columns["Version"].Width = 100;

            // Hide the Filename column
            if (this.MainGridView.Columns["Filename"] != null)
                this.MainGridView.Columns["Filename"].SetColVisible(false);

            // Show the comment column
            if (this.MainGridView.Columns["Comment"] != null)
                this.MainGridView.Columns["Comment"].SetColVisible(true);
        }
    }
}