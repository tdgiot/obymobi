﻿using Dionysos.Data.LLBLGen;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using Obymobi.Data.CollectionClasses;
using Obymobi.Enums;
using Obymobi.ObymobiCms.MasterPages;

namespace Obymobi.ObymobiCms.Configuration.Subpanels
{
    public partial class AccountCompanyCollection : Dionysos.Web.UI.DevExControls.SubPanelLLBLOverviewDataSourceCollection
    {   
        protected override void OnInit(EventArgs e)
        {
            this.DataSourceLoaded += AccountCompanyCollection_DataSourceLoaded;
            base.OnInit(e);
			
            this.EntityName = "AccountCompany";
            this.MainGridView.ShowEditHyperlinkColumn = false;
            this.MainGridView.EnableRowDoubleClick = false;
        }

        private void AccountCompanyCollection_DataSourceLoaded(object sender)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(new FieldCompareSetPredicate(CompanyFields.CompanyId, AccountCompanyFields.CompanyId, SetOperator.In, (AccountCompanyFields.AccountId == this.ParentPrimaryKeyFieldValue), true));

            if (CmsSessionHelper.CurrentRole == Role.Reseller)
            {
                List<int> companyIds = CmsSessionHelper.CompanyCollectionForUser.Select(c => c.CompanyId).ToList();
                filter.Add(CompanyFields.CompanyId == companyIds);
            }
            
            SortExpression sort = new SortExpression(new SortClause(CompanyFields.Name, SortOperator.Ascending));

            CompanyCollection companyCollection = EntityCollection.GetMulti<CompanyCollection>(filter, sort, null, null, null);
            this.ddlCompanyId.DataSource = companyCollection;
            this.ddlCompanyId.DataBind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.btnAdd.Click += new EventHandler(btnAdd_Click);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (this.ddlCompanyId.ValidId > 0)
            {
                bool wasNew = this.ParentDataSource.IsNew;
                if (wasNew)
                    this.ParentDataSource.Save();

                AccountCompanyEntity accountCompanyEntity = new AccountCompanyEntity();
                accountCompanyEntity.AccountId = this.ParentPrimaryKeyFieldValue;
                accountCompanyEntity.CompanyId = this.ddlCompanyId.ValidId;
                accountCompanyEntity.Save();

                if (wasNew)
                    this.Parent.Save();
                else
                    MasterPageBase.ClearCompaniesCacheForAccount(this.ParentPrimaryKeyFieldValue);
            }
        } 
    }
}