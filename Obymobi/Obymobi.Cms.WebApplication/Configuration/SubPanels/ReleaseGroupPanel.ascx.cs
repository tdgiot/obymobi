﻿using System;
using System.Linq;
using System.Web.UI;
using Dionysos;
using Dionysos.Data.LLBLGen;
using Obymobi.Cms.Logic.DataSources;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Cms.WebApplication.Configuration.SubPanels
{
    public partial class ReleaseGroupPanel : UserControl
    {
        private readonly CompanyDataSource companyDataSource = new CompanyDataSource();

        public void Save()
        {
            CompanyCollection companyCollection = GetCompanies();

            foreach (CompanyEntity company in companyCollection)
            {
                string releaseGroupName = Request.Form[$"{company.CompanyId}_options"];

                company.ReleaseGroup = !releaseGroupName.IsNullOrWhiteSpace()
                    ? releaseGroupName.ToEnum<ReleaseGroup>()
                    : (ReleaseGroup?)null;
            }

            companyDataSource.Save(companyCollection);
        }

        public CompanyCollection GetCompanies()
        {
            IncludeFieldsList includeFieldsList = new IncludeFieldsList { CompanyFields.Name, CompanyFields.ReleaseGroup };
            return companyDataSource.FetchAll(includeFieldsList)
                                    .OrderByDescending(company => company.ReleaseGroup.HasValue)
                                    .ThenBy(company => company.ReleaseGroup)
                                    .ThenBy(company => company.Name)
                                    .ToEntityCollection<CompanyCollection>();

        }

        public bool AllowEditing { get; internal set; }
    }
}
