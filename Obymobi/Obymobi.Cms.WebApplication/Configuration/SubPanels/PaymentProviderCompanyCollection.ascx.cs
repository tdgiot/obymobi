﻿using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using Obymobi.Cms.Logic.Factories;
using Obymobi.Enums;
using Obymobi.ObymobiCms.MasterPages;

namespace Obymobi.ObymobiCms.Configuration.Subpanels
{
    public partial class PaymentProviderCompanyCollection : Dionysos.Web.UI.DevExControls.SubPanelLLBLOverviewDataSourceCollection
    {
        private void DataBindCompanies()
        {
            PredicateExpression companyFilter = PredicateFactory.CreateCompanyIdFilter(CmsSessionHelper.CompanyIdsForUser, CmsSessionHelper.CurrentRole);
            companyFilter.Add(new FieldCompareSetPredicate(CompanyFields.CompanyId, PaymentProviderCompanyFields.CompanyId, SetOperator.In, (PaymentProviderCompanyFields.PaymentProviderId == ParentPrimaryKeyFieldValue), true));
            ddlCompanyId.DataBindEntityCollection<CompanyEntity>(companyFilter, CompanyFields.Name, CompanyFields.Name);
        }

        protected override void OnInit(EventArgs e)
        {
            DataSourceLoaded += PaymentProviderCompanyCollection_DataSourceLoaded;
            base.OnInit(e);
			
            EntityName = "PaymentProviderCompany";
            MainGridView.ShowEditHyperlinkColumn = false;
            MainGridView.EnableRowDoubleClick = false;
        }

        private void PaymentProviderCompanyCollection_DataSourceLoaded(object sender)
        {
            DataBindCompanies();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            btnAdd.Visible = CmsSessionHelper.CurrentRole == Role.GodMode;
            ddlCompanyId.Visible = CmsSessionHelper.CurrentRole == Role.GodMode;

            btnAdd.Click += btnAdd_Click;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (ddlCompanyId.ValidId <= 0)
                return;
            
            bool wasNew = ParentDataSource.IsNew;
            if (wasNew)
            {
                ParentDataSource.Save();
            }

            PaymentProviderCompanyEntity entity = new PaymentProviderCompanyEntity();
            entity.PaymentProviderId = ParentPrimaryKeyFieldValue;
            entity.CompanyId = ddlCompanyId.ValidId;
            entity.Save();

            if (wasNew)
            {
                Parent.Save();
            }
            else
            {
                MasterPageBase.ClearCompaniesCacheForAccount(ParentPrimaryKeyFieldValue);
            }

            DataBindCompanies();
        } 
    }
}