﻿using System;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using System.Linq;

namespace Obymobi.ObymobiCms.Configuration.SubPanels
{
    public partial class SupportpoolNotificationRecipientCollection : Dionysos.Web.UI.DevExControls.SubPanelLLBLOverviewDataSourceCollection
    {
        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.EntityName = "SupportpoolNotificationRecipient";            
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Return the page's datasource as a SupportpoolEntity
        /// </summary>
        public SupportpoolEntity DataSourceAsSupportpoolEntity
        {
            get
            {
                return this.ParentDataSource as SupportpoolEntity;
            }
        }

        #endregion
    }
}