﻿using System;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Web.CDN;
using Obymobi.Data;

namespace Obymobi.ObymobiCms.Configuration.SubPanels.CDN
{
    public partial class OverviewPanel : System.Web.UI.UserControl
    {
        #region Fields

        #endregion

        #region Methods

        private void LoadUserControls()
        {
            // Generic media            
            this.RenderGenericMedia(new UploadGenericMediaRequest()
                                    {
                                        UploadGenericAdvertisements = true,
                                        UploadGenericCategories = true,
                                        UploadGenericEntertainment = true,
                                        UploadGenericPointsOfInterest = true,
                                        UploadGenericProducts = true,
                                        UploadGenericSiteTemplates = true,
                                        UploadGenericSites = true
                                    });

            if (!this.IsPostBack)
            {
                this.ddlCompany.SelectedValue = CmsSessionHelper.CurrentCompanyId.ToString();

                // Default company
                this.RenderCompanyMedia(new UploadCompanyMediaRequest(CmsSessionHelper.CurrentCompanyId)
                {
                    UploadAdvertisements = true,
                    UploadAlterations = true,
                    UploadAlterationoptions = true,
                    UploadCategories = true,
                    UploadCompany = true,
                    UploadDeliverypointgroups = true,
                    UploadEntertainment = true,
                    UploadProducts = true,
                    UploadSites = true,
                    UploadSurveys = true,
                    UploadUIWidgets = true
                });
            }            
        }

        private void HookupEvents()
        {
            this.btGetUnpublishedMedia.Click += btGetUnpublishedMedia_Click;            
        }

        private void SetGui()
        {
            
        }

        private void RenderGenericMedia(UploadGenericMediaRequest uploadGenericMediaRequest)
        {
            // Prefetch
            var prefetch = new PrefetchPath(EntityType.MediaRatioTypeMediaEntity);
            prefetch.Add(MediaRatioTypeMediaEntity.PrefetchPathMediaEntity);

            var filter = new PredicateExpression();
            var relations = new RelationCollection();

            relations.Add(MediaRatioTypeMediaEntity.Relations.MediaEntityUsingMediaId);

            var environmentFilter = new PredicateExpression();
            environmentFilter.AddWithOr(this.GetAmazonFilter());

            filter.Add(environmentFilter);

            // Filter
            var subFilter = new PredicateExpression();
            if (uploadGenericMediaRequest.UploadGenericProducts)
                subFilter.AddWithOr(MediaFields.GenericproductId != DBNull.Value);
            if (uploadGenericMediaRequest.UploadGenericCategories)
                subFilter.AddWithOr(MediaFields.GenericcategoryId != DBNull.Value);
            if (uploadGenericMediaRequest.UploadGenericEntertainment)
            {
                var entertainmentSubFilter = new PredicateExpression();
                entertainmentSubFilter.Add(MediaFields.EntertainmentId != DBNull.Value);
                entertainmentSubFilter.Add(EntertainmentFields.CompanyId == DBNull.Value);
                subFilter.AddWithOr(entertainmentSubFilter);
            }
            if (uploadGenericMediaRequest.UploadGenericPointsOfInterest)
                subFilter.AddWithOr(MediaFields.PointOfInterestId != DBNull.Value);
            if (uploadGenericMediaRequest.UploadGenericSites)
                subFilter.AddWithOr(MediaFields.PageElementId != DBNull.Value);

            filter.Add(subFilter);

            // Relations
            if (uploadGenericMediaRequest.UploadGenericEntertainment)
                relations.Add(MediaEntity.Relations.EntertainmentEntityUsingEntertainmentId, JoinHint.Left);

            dsGenericMedia.PrefetchPathToUse = prefetch;
            dsGenericMedia.RelationsToUse = relations;
            dsGenericMedia.FilterToUse = filter;
        }

        private void RenderCompanyMedia(UploadCompanyMediaRequest uploadCompanyMediaRequest)
        { 
            // Prefetch
            var prefetch = new PrefetchPath(EntityType.MediaRatioTypeMediaEntity);
            prefetch.Add(MediaRatioTypeMediaEntity.PrefetchPathMediaEntity);

            var filter = new PredicateExpression();
            var relations = new RelationCollection();

            relations.Add(MediaRatioTypeMediaEntity.Relations.MediaEntityUsingMediaId);

            var environmentFilter = new PredicateExpression();
            environmentFilter.AddWithOr(this.GetAmazonFilter());            

            filter.Add(environmentFilter);

            // Filter
            var subFilter = new PredicateExpression();
            if (uploadCompanyMediaRequest.UploadCompany)
                subFilter.AddWithOr(MediaFields.CompanyId == uploadCompanyMediaRequest.CompanyId);
            if (uploadCompanyMediaRequest.UploadProducts)
                subFilter.AddWithOr(ProductFields.CompanyId == uploadCompanyMediaRequest.CompanyId);
            if (uploadCompanyMediaRequest.UploadCategories)
                subFilter.AddWithOr(CategoryFields.CompanyId == uploadCompanyMediaRequest.CompanyId);
            if (uploadCompanyMediaRequest.UploadDeliverypointgroups)
                subFilter.AddWithOr(DeliverypointgroupFields.CompanyId == uploadCompanyMediaRequest.CompanyId);
            if (uploadCompanyMediaRequest.UploadAdvertisements)
                subFilter.AddWithOr(AdvertisementFields.CompanyId == uploadCompanyMediaRequest.CompanyId);
            if (uploadCompanyMediaRequest.UploadEntertainment)
                subFilter.AddWithOr(EntertainmentFields.CompanyId == uploadCompanyMediaRequest.CompanyId);
            if (uploadCompanyMediaRequest.UploadAlterations)
                subFilter.AddWithOr(AlterationFields.CompanyId == uploadCompanyMediaRequest.CompanyId);
            if (uploadCompanyMediaRequest.UploadAlterationoptions)
                subFilter.AddWithOr(AlterationoptionFields.CompanyId == uploadCompanyMediaRequest.CompanyId);
            if (uploadCompanyMediaRequest.UploadSurveys)
            {
                subFilter.AddWithOr(SurveyFields.CompanyId == uploadCompanyMediaRequest.CompanyId);
                subFilter.AddWithOr(new FieldCompareValuePredicate(SurveyFields.CompanyId, ComparisonOperator.Equal, uploadCompanyMediaRequest.CompanyId, "SurveyOfPage"));
            }
            if (uploadCompanyMediaRequest.UploadUIWidgets)
                subFilter.AddWithOr(UIModeFields.CompanyId == uploadCompanyMediaRequest.CompanyId);
            filter.Add(subFilter);

            // Relations            
            if (uploadCompanyMediaRequest.UploadProducts)
                relations.Add(MediaEntity.Relations.ProductEntityUsingProductId, JoinHint.Left);
            if (uploadCompanyMediaRequest.UploadCategories)
                relations.Add(MediaEntity.Relations.CategoryEntityUsingCategoryId, JoinHint.Left);
            if (uploadCompanyMediaRequest.UploadDeliverypointgroups)
                relations.Add(MediaEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId, JoinHint.Left);
            if (uploadCompanyMediaRequest.UploadAdvertisements)
                relations.Add(MediaEntity.Relations.AdvertisementEntityUsingAdvertisementId, JoinHint.Left);
            if (uploadCompanyMediaRequest.UploadEntertainment)
                relations.Add(MediaEntity.Relations.EntertainmentEntityUsingEntertainmentId, JoinHint.Left);
            if (uploadCompanyMediaRequest.UploadAlterations)
                relations.Add(MediaEntity.Relations.AlterationEntityUsingAlterationId, JoinHint.Left);
            if (uploadCompanyMediaRequest.UploadAlterationoptions)
                relations.Add(MediaEntity.Relations.AlterationoptionEntityUsingAlterationoptionId, JoinHint.Left);
            if (uploadCompanyMediaRequest.UploadSurveys)
            {
                relations.Add(MediaEntity.Relations.SurveyEntityUsingSurveyId, JoinHint.Left);
                relations.Add(MediaEntity.Relations.SurveyPageEntityUsingSurveyPageId, JoinHint.Left);
                relations.Add(SurveyPageEntity.Relations.SurveyEntityUsingSurveyId, "SurveyOfPage", JoinHint.Left);
            }
            if (uploadCompanyMediaRequest.UploadUIWidgets)
            {
                relations.Add(MediaEntity.Relations.UIWidgetEntityUsingUIWidgetId);
                relations.Add(UIWidgetEntity.Relations.UITabEntityUsingUITabId);
                relations.Add(UITabEntity.Relations.UIModeEntityUsingUIModeId);
            }

            dsCompanyMedia.PrefetchPathToUse = prefetch;
            dsCompanyMedia.RelationsToUse = relations;
            dsCompanyMedia.FilterToUse = filter;
        }
        
        private PredicateExpression GetAmazonFilter()
        {
            // Secondary server filter
            var secondaryFilter = new PredicateExpression();
            secondaryFilter.Add(MediaRatioTypeMediaFields.LastDistributedVersionTicksAmazon == DBNull.Value);
            secondaryFilter.AddWithOr(MediaRatioTypeMediaFields.LastDistributedVersionTicksAmazon < MediaRatioTypeMediaFields.LastDistributedVersionTicks);

            return secondaryFilter;
        }

        #endregion

        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.HookupEvents();
            this.SetGui();            
        }

        void btGetUnpublishedMedia_Click(object sender, EventArgs e)
        {
            // Company data
            if (ddlCompany.HasValidId)
            {
                this.RenderCompanyMedia(new UploadCompanyMediaRequest(ddlCompany.ValidId)
                {
                    UploadAdvertisements = true,
                    UploadAlterations = true,
                    UploadAlterationoptions = true,
                    UploadCategories = true,
                    UploadCompany = true,
                    UploadDeliverypointgroups = true,
                    UploadEntertainment = true,
                    UploadProducts = true,
                    UploadSites = true,
                    UploadSurveys = true,
                    UploadUIWidgets = true
                });
            }
        }
        
        #endregion
        
        #region Properties

        #endregion        
    }
}