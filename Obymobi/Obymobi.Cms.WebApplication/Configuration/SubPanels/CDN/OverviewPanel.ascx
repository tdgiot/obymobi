﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Configuration.SubPanels.CDN.OverviewPanel" Codebehind="OverviewPanel.ascx.cs" %>
<%@ Register Assembly="SD.LLBLGen.Pro.ORMSupportClasses.Web" Namespace="SD.LLBLGen.Pro.ORMSupportClasses" TagPrefix="llblgenpro" %>

<X:PageControl Id="tabsOverview" runat="server" Width="100%">
	<TabPages>
        <X:TabPage Text="Company media" Name="CompanyMedia">
            <Controls>                    
                <table class="dataformV2">
                    <tr>
                        <td class="label">
                            <D:LabelTextOnly ID="lblCompany" runat="server">Company</D:LabelTextOnly>
                        </td>
                        <td class="control">
                            <D:DropDownListLLBLGenEntityCollection ID="ddlCompany" runat="server" EntityName="Company" DataTextField="Name" DataValueField="CompanyId"></D:DropDownListLLBLGenEntityCollection>
                        </td>
                        <td class="label">
                                
                        </td>
                        <td class="control">
                            <D:Button ID="btGetUnpublishedMedia" runat="server" Text="Verversen"/>
                        </td>
                    </tr>
                </table>                                         
                <X:GridView ID="companyDataGrid" ClientInstanceName="taskGridCompany" runat="server" Width="100%" KeyFieldName="MediaRatioTypeMediaId" DataSourceID="dsCompanyMedia" AllowPaging="true" PageSize="40" AutoGenerateColumns="False" DataSourceForceStandardPaging="true" EnableRowsCache="false">
                    <SettingsBehavior AutoFilterRowInputDelay="350" />
                    <SettingsPager PageSize="40"></SettingsPager>
                    <SettingsBehavior AllowGroup="false" AllowDragDrop="false" />
                    <Columns>                            
                        <dxwgv:GridViewDataDateColumn FieldName="MediaRatioTypeMediaId" VisibleIndex="0" SortIndex="0" Visible="false"></dxwgv:GridViewDataDateColumn>
                        <dxwgv:GridViewDataHyperLinkColumn FieldName="MediaUrl" Caption="Name" VisibleIndex="1">
                            <PropertiesHyperLinkEdit TextField="RelatedMediaName" Target="_blank" />
                        </dxwgv:GridViewDataHyperLinkColumn> 
                        <dxwgv:GridViewDataTextColumn FieldName="FileName" VisibleIndex="2"></dxwgv:GridViewDataTextColumn>                                
                        <dxwgv:GridViewDataDateColumn FieldName="RelatedEntityName" VisibleIndex="3"></dxwgv:GridViewDataDateColumn>
                        <dxwgv:GridViewDataTextColumn FieldName="MediaTypeName" VisibleIndex="4"></dxwgv:GridViewDataTextColumn>                        
                    </Columns>
                </X:GridView>
            </Controls>
        </X:TabPage>
        <X:TabPage Text="Generic media" Name="GenericMedia">
			<Controls>                                             
                <X:GridView ID="genericDataGrid" ClientInstanceName="taskGridGeneric" runat="server" Width="100%" KeyFieldName="MediaRatioTypeMediaId" DataSourceID="dsGenericMedia" AllowPaging="true" PageSize="40" AutoGenerateColumns="False"  DataSourceForceStandardPaging="true" EnableRowsCache="false">
                    <SettingsBehavior AutoFilterRowInputDelay="350" />                            
                    <SettingsPager PageSize="40"></SettingsPager>
                    <SettingsBehavior AllowGroup="false" AllowDragDrop="false" />
                    <Columns>
                        <dxwgv:GridViewDataDateColumn FieldName="MediaRatioTypeMediaId" VisibleIndex="0" SortIndex="0" Visible="false"></dxwgv:GridViewDataDateColumn>                            
                        <dxwgv:GridViewDataHyperLinkColumn FieldName="MediaUrl" Caption="Name" VisibleIndex="1">
                            <PropertiesHyperLinkEdit TextField="RelatedMediaName" Target="_blank" />
                        </dxwgv:GridViewDataHyperLinkColumn> 
                        <dxwgv:GridViewDataTextColumn FieldName="FileName" VisibleIndex="2"></dxwgv:GridViewDataTextColumn>                                
                        <dxwgv:GridViewDataDateColumn FieldName="RelatedEntityName" VisibleIndex="3"></dxwgv:GridViewDataDateColumn>
                        <dxwgv:GridViewDataTextColumn FieldName="MediaTypeName" VisibleIndex="4"></dxwgv:GridViewDataTextColumn>                                                       
                    </Columns>
                </X:GridView>
            </Controls>
        </X:TabPage>
    </TabPages>
</X:PageControl>
<llblgenpro:LLBLGenProDataSource ID="dsCompanyMedia" runat="server" DataContainerType="EntityCollection"  
    EnablePaging="true" EntityCollectionTypeName="Obymobi.Data.CollectionClasses.MediaRatioTypeMediaCollection, Obymobi.Data">
</llblgenpro:LLBLGenProDataSource>
<llblgenpro:LLBLGenProDataSource ID="dsGenericMedia" runat="server" DataContainerType="EntityCollection" 
    EnablePaging="true" EntityCollectionTypeName="Obymobi.Data.CollectionClasses.MediaRatioTypeMediaCollection, Obymobi.Data">
</llblgenpro:LLBLGenProDataSource>
