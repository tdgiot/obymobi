﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Configuration.SubPanels.CDN.LogPanel" Codebehind="LogPanel.ascx.cs" %>
<table class="dataformV2">
    <tr>
        <td class="label">
            <D:LabelTextOnly runat="server" id="lblNumberOfLines">Aantal regels</D:LabelTextOnly>
        </td>
        <td class="control">
            <D:TextBoxInt runat="server" ID="tbNumberOfLines" ThousandsSeperators="false" IntValue="200"></D:TextBoxInt>
            
        </td>
        <td class="label">
            
        </td>
        <td class="control">
            <D:Button runat="server" ID="btRefresh" Text="Verversen" />
        </td>
    </tr>
    <tr>
        <td class="control" colspan="4">
            <D:TextBoxMultiLine ID="tbLog" style="font-family:Consolas,Monaco,Lucida Console,Liberation Mono,DejaVu Sans Mono,Bitstream Vera Sans Mono,Courier New, monospace;" runat="server" Rows="50" UseDataBinding="false"></D:TextBoxMultiLine>
        </td>            
    </tr>
</table>
