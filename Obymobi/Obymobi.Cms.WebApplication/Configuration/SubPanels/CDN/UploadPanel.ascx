﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Configuration.SubPanels.CDN.UploadPanel" Codebehind="UploadPanel.ascx.cs" %>
<script type="text/javascript">
    function OnListBoxSelectionChanged(listBox, args) {
        if (args.index == 0)
            args.isSelected ? listBox.SelectAll() : listBox.UnselectAll();
    }
</script>
<X:PageControl Id="tabsUpload" runat="server" Width="100%">
	<TabPages>		
        <X:TabPage Text="Company media" Name="CompanyMedia">
			<Controls>                               
                <table class="dataformV2">
	                <tr>
                        <td class="label">
			                <D:LabelTextOnly runat="server" id="lblCompanies">Bedrijven</D:LabelTextOnly>
		                </td>
		                <td class="control">           
                            <dxe:ASPxCheckBox ID="cbCompaniesAllNone" runat="server" CssClass="listbox-checkall-checkbox" ClientIDMode="Static" EnableTheming="false" ClientSideEvents-CheckedChanged="function () { if(cbCompaniesAllNone.GetChecked()) { cblCompanies.SelectAll(); } else { cblCompanies.SelectIndex(-1); } }" Text="Check all/none"/>        
                            <X:ListBoxLLBLGenEntityCollection ID="cblCompanies" runat="server" SelectionMode="CheckColumn" EntityName="Company" ClientIDMode="Static" />
		                </td>     
                        <td class="label">
                            <D:LabelTextOnly runat="server" id="lblCompanyData">Bedrijfsdata</D:LabelTextOnly>
		                </td>
		                <td class="control">      
                            <dxe:ASPxCheckBox ID="cbCompanyDataAllNone" runat="server" CssClass="listbox-checkall-checkbox" ClientIDMode="Static" EnableTheming="false" ClientSideEvents-CheckedChanged="function () { if(cbCompanyDataAllNone.GetChecked()) { lbCompanyData.SelectAll(); } else { lbCompanyData.SelectIndex(-1); } }" Text="Check all/none"/>        
                            <X:ListBox ID="lbCompanyData" runat="server" SelectionMode="CheckColumn" ClientIDMode="Static" />
		                </td>           
                    </tr>                
                    <tr>
                        <td class="label">
                            <D:LabelTextOnly runat="server" ID="lblUnpublishedCompanyMediaOnly">Alleen niet-gepubliceerde media</D:LabelTextOnly>
                        </td>
                        <td class="control">
                            <D:CheckBox ID="cbUnpublishedCompanyMediaOnly" runat="server" Checked="true" />            
                        </td>
                        <td class="label">

                        </td>
                        <td class="control">
                            <D:Button ID="btnUploadCompanyMedia" runat="server" Text="Upload media" PreSubmitWarning="Are you sure?" />
                            &nbsp;
                            <D:Button ID="btnCheckConsistencyCompanyMedia" runat="server" Text="Check consistency" PreSubmitWarning="Are you sure?" />
                        </td>
                    </tr>
                </table>
            </Controls>
        </X:TabPage>
        <X:TabPage Text="Generic media" Name="GenericMedia">
			<Controls> 
                <table class="dataformV2">
	                <tr>
                        <td class="label">
                            <D:LabelTextOnly runat="server" id="lblGenericData">Generieke data</D:LabelTextOnly>
                        </td>
                        <td class="control">
                            <dxe:ASPxCheckBox ID="cbGenericDataAllNone" runat="server" CssClass="listbox-checkall-checkbox" ClientIDMode="Static" EnableTheming="false" ClientSideEvents-CheckedChanged="function () { if(cbGenericDataAllNone.GetChecked()) { lbGenericData.SelectAll(); } else { lbGenericData.SelectIndex(-1); } }" Text="Check all/none"/>        
                            <X:ListBox ID="lbGenericData" runat="server" SelectionMode="CheckColumn" ClientIDMode="Static" />
                        </td>         
                        <td class="label">

                        </td>
                        <td class="control">

                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            <D:LabelTextOnly runat="server" ID="lblUnpublishedGenericMediaOnly">Alleen niet-gepubliceerde media</D:LabelTextOnly>
                        </td>
                        <td class="control">
                            <D:CheckBox ID="cbUnpublishedGenericMediaOnly" runat="server" Checked="true" />            
                        </td>
                        <td class="label">

                        </td>
                        <td class="control">
                            <D:Button ID="btnUploadGenericMedia" runat="server" Text="Upload media" />
                            &nbsp;
                            <D:Button ID="btnCheckConsistencyGenericMedia" runat="server" Text="Check consistency" PreSubmitWarning="Are you sure?" />
                        </td>
                    </tr>
                </table>                   
            </Controls>
        </X:TabPage>
        <X:TabPage Text="Temp for Floris" Name="SiteMedia">
			<Controls>
			    <table class="dataformV2">
			        <tr>
			            <td class="label">
			                <D:TextBoxInt ID="tbSiteId" runat="server" />
			            </td>
                        <td class="control">
			                <D:Button ID="btnUpladMediaForSite" runat="server" Text="Upload media for site" PreSubmitWarning="Are you sure?" />
                            <D:LabelTextOnly ID="lblMediaToUpload" runat="server" Text="Media: " />
			            </td>
			        </tr>
			    </table> 
            </Controls>
        </X:TabPage>
    </TabPages>
</X:PageControl>