﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using System.Threading;
using Obymobi.Data.Injectables.Authorizers;
using Obymobi.Web.Media;
using DevExpress.Web;
using Obymobi.Web.CDN;
using System.Net;
using Google.Apis.Manual.Util;
using Obymobi.Data;
using Obymobi.Web.HelperClasses;

namespace Obymobi.ObymobiCms.Configuration.SubPanels.CDN
{
    public partial class UploadPanel : System.Web.UI.UserControl
    {
        #region Fields

        // Company data
        private static readonly int ListItemCompanyIndex = 1;
        private static readonly int ListItemProductsIndex = 2;
        private static readonly int ListItemCategoriesIndex = 3;
        private static readonly int ListItemDeliverypointgroupsIndex = 4;
        private static readonly int ListItemAdvertisementsIndex = 5;
        private static readonly int ListItemEntertainmentsIndex = 6;
        private static readonly int ListItemSocialmediaIndex = 7;
        private static readonly int ListItemAlterationsIndex = 8;
        private static readonly int ListItemAlterationoptionsIndex = 9;
        private static readonly int ListItemSurveysIndex = 10;
        private static readonly int ListItemSitesIndex = 11;
        private static readonly int ListItemUIWidgetsIndex = 12;

        // Generic data
        private static readonly int ListItemGenericProductsIndex = 1;
        private static readonly int ListItemGenericCategoriesIndex = 2;
        private static readonly int ListItemGenericEntertainmentIndex = 3;
        private static readonly int ListItemGenericPointsofinterestIndex = 4;
        private static readonly int ListItemGenericSitesIndex = 5;
        private static readonly int ListItemGenericAdvertisementsIndex = 6;
        private static readonly int ListItemGenericSiteTemplatesIndex = 7;

        #endregion

        #region Methods

        private void LoadUserControls()
        {
            // Company data
            this.lbCompanyData.Items.Add(new ListEditItem("Company", ListItemCompanyIndex));
            this.lbCompanyData.Items.Add(new ListEditItem("Products", ListItemProductsIndex));
            this.lbCompanyData.Items.Add(new ListEditItem("Categories", ListItemCategoriesIndex));
            this.lbCompanyData.Items.Add(new ListEditItem("Deliverypointgroups", ListItemDeliverypointgroupsIndex));
            this.lbCompanyData.Items.Add(new ListEditItem("Advertisements", ListItemAdvertisementsIndex));
            this.lbCompanyData.Items.Add(new ListEditItem("Entertainment", ListItemEntertainmentsIndex));
            this.lbCompanyData.Items.Add(new ListEditItem("Socialmedia", ListItemSocialmediaIndex));
            this.lbCompanyData.Items.Add(new ListEditItem("Alterations", ListItemAlterationsIndex));
            this.lbCompanyData.Items.Add(new ListEditItem("Alteration options", ListItemAlterationoptionsIndex));
            this.lbCompanyData.Items.Add(new ListEditItem("Surveys", ListItemSurveysIndex));
            this.lbCompanyData.Items.Add(new ListEditItem("Sites", ListItemSitesIndex));
            this.lbCompanyData.Items.Add(new ListEditItem("UI widgets", ListItemUIWidgetsIndex));

            // Generic data
            this.lbGenericData.Items.Add(new ListEditItem("Generic products", ListItemGenericProductsIndex));
            this.lbGenericData.Items.Add(new ListEditItem("Generic categories", ListItemGenericCategoriesIndex));
            this.lbGenericData.Items.Add(new ListEditItem("Generic entertainment", ListItemGenericEntertainmentIndex));
            this.lbGenericData.Items.Add(new ListEditItem("Generic points of interest", ListItemGenericPointsofinterestIndex));
            this.lbGenericData.Items.Add(new ListEditItem("Generic sites", ListItemGenericSitesIndex));
            this.lbGenericData.Items.Add(new ListEditItem("Generic advertisements", ListItemGenericAdvertisementsIndex));
            this.lbGenericData.Items.Add(new ListEditItem("Generic site templates", ListItemGenericSiteTemplatesIndex));
        }

        private void HookupEvents()
        {
            this.btnUploadGenericMedia.Click += btnUploadGenericMedia_Click;
            this.btnCheckConsistencyGenericMedia.Click += btnCheckConsistencyGenericMedia_Click;
            this.btnUploadCompanyMedia.Click += btnUploadCompanyMedia_Click;
            this.btnCheckConsistencyCompanyMedia.Click += btnCheckConsistencyCompanyMedia_Click;
            this.lbCompanyData.Callback += ListBox_Callback;
            this.btnUpladMediaForSite.Click += btnUpladMediaForSite_Click;
        }

        private void btnUpladMediaForSite_Click(object sender, EventArgs e)
        {
            if (!this.tbSiteId.Text.IsNullOrEmpty())
            {
                int siteId = int.Parse(this.tbSiteId.Text);

                PrefetchPath prefetch = new PrefetchPath(EntityType.MediaEntity);
                var mediaRatioTypeMediaPrefetch = MediaEntityBase.PrefetchPathMediaRatioTypeMediaCollection;
                prefetch.Add(mediaRatioTypeMediaPrefetch).SubPath.Add(MediaRatioTypeMediaEntityBase.PrefetchPathMediaRatioTypeMediaFileEntity);

                var filter = new PredicateExpression();
                filter.AddWithOr(SiteFields.SiteId == siteId);
                filter.AddWithOr(new FieldCompareValuePredicate(SiteFields.SiteId, ComparisonOperator.Equal, siteId, "SiteOfPage"));
                filter.AddWithOr(new FieldCompareValuePredicate(SiteFields.SiteId, ComparisonOperator.Equal, siteId, "SiteOfPageOfPageElement"));

                var relations = new RelationCollection();
                relations.Add(MediaEntityBase.Relations.SiteEntityUsingSiteId, JoinHint.Left);

                relations.Add(MediaEntityBase.Relations.PageEntityUsingPageId, JoinHint.Left);
                relations.Add(PageEntityBase.Relations.SiteEntityUsingSiteId, "SiteOfPage", JoinHint.Left);

                relations.Add(MediaEntityBase.Relations.PageElementEntityUsingPageElementId, JoinHint.Left);
                relations.Add(PageElementEntityBase.Relations.PageEntityUsingPageId, "PageOfPageElement", JoinHint.Left);
                relations.Add(PageEntityBase.Relations.SiteEntityUsingSiteId, "PageOfPageElement", "SiteOfPageOfPageElement", JoinHint.Left);

                MediaCollection media = new MediaCollection();
                media.GetMulti(filter, 0, null, relations, prefetch);

                this.lblMediaToUpload.Text = "Media: " + media.Count;

                MediaRatioTypeMediaHelperWeb.ResetMediaRatioTypeMediaFiles(media, true, false, false);
            }
        }

        /// <summary>
        /// Callback for the listbox. Occurs when index is changed 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ListBox_Callback(object sender, global::DevExpress.Web.CallbackEventArgsBase e)
        {
            Dionysos.Web.UI.DevExControls.ListBox lb = (Dionysos.Web.UI.DevExControls.ListBox)sender;
            if (lb.SelectedIndex == 0)
            {   
                if (lb.SelectedItem.Selected)
                    lb.SelectAll();
                else
                    lb.UnselectAll();
            }
        }

        private void SetGui()
        {
        }

        private void UploadCompanyMedia()
        {
            if (this.cblCompanies.SelectedItems.Count > 0 &&
                this.lbCompanyData.SelectedItems.Count > 0)
            {
                var companyIds = this.cblCompanies.SelectedItemsValuesAsIntList;
                var companyDataValues = this.lbCompanyData.SelectedItemsValuesAsIntList;

                // Company data
                UploadCompanyMediaRequest uploadCompanyMediaRequest = new UploadCompanyMediaRequest()
                                                                      {
                                                                            UploadCompany = companyDataValues.IndexOf(ListItemCompanyIndex) > -1,
                                                                            UploadProducts = companyDataValues.IndexOf(ListItemProductsIndex) > -1,
                                                                            UploadCategories = companyDataValues.IndexOf(ListItemCategoriesIndex) > -1,
                                                                            UploadDeliverypointgroups = companyDataValues.IndexOf(ListItemDeliverypointgroupsIndex) > -1,
                                                                            UploadAdvertisements = companyDataValues.IndexOf(ListItemAdvertisementsIndex) > -1,
                                                                            UploadEntertainment = companyDataValues.IndexOf(ListItemEntertainmentsIndex) > -1,
                                                                            UploadAlterations = companyDataValues.IndexOf(ListItemAlterationsIndex) > -1,
                                                                            UploadAlterationoptions = companyDataValues.IndexOf(ListItemAlterationoptionsIndex) > -1,
                                                                            UploadSurveys = companyDataValues.IndexOf(ListItemSurveysIndex) > -1,
                                                                            UploadSites = companyDataValues.IndexOf(ListItemSitesIndex) > -1,
                                                                            UploadUIWidgets = companyDataValues.IndexOf(ListItemUIWidgetsIndex) > -1
                                                                    };
                

                // Force save?
                bool forceSave = true;
                bool unpublishedOnly = this.cbUnpublishedCompanyMediaOnly.Checked;

                new Thread((param) =>
                {
                    try
                    {
                        GeneralAuthorizer.SetThreadGodMode(true);

                        foreach (int companyId in companyIds)
                        {
                            // Company data
                            uploadCompanyMediaRequest.CompanyId = companyId;

                            UploadHelper.UploadCompanyDataMedia(uploadCompanyMediaRequest, forceSave, unpublishedOnly, true);

                            // Now set the Timestamps - because they are not set in the batch
                            CompanyEntity c = new CompanyEntity(companyId);
                            DateTime utcNow = DateTime.UtcNow;
                            c.AdvertisementDataLastModifiedUTC = utcNow;
                            c.AdvertisementMediaLastModifiedUTC = utcNow;
                            c.AnnouncementDataLastModifiedUTC = utcNow;
                            c.AnnouncementMediaLastModifiedUTC = utcNow;
                            c.CompanyDataLastModifiedUTC = utcNow;                            
                            c.CompanyMediaLastModifiedUTC = utcNow;
                            c.DeliverypointDataLastModifiedUTC = utcNow;
                            c.EntertainmentDataLastModifiedUTC = utcNow;
                            c.EntertainmentMediaLastModifiedUTC = utcNow;                            
                            //c.MenuDataLastModifiedUTC = utcNow;
                            //c.MenuMediaLastModifiedUTC = utcNow;
                            c.PosIntegrationInfoLastModifiedUTC = utcNow;
                            c.SurveyDataLastModifiedUTC = utcNow;
                            c.SurveyMediaLastModifiedUTC = utcNow;
                            c.Save();
                        }
                    }
                    catch
                    { }
                    finally
                    {
                        GeneralAuthorizer.SetThreadGodMode(false);
                    }
                }).Start(HttpContext.Current);

                string message = "Upload Media Task has started in the background can take between 5 minutes and 2 hours to complete!";
                ((Dionysos.Web.UI.PageDefault)this.Page).AddInformator(Dionysos.Web.UI.InformatorType.Information, message);
            }
            else
            {
                string message = "Upload Media Task not started, no company or company media selected!";
                ((Dionysos.Web.UI.PageDefault)this.Page).AddInformator(Dionysos.Web.UI.InformatorType.Information, message);
            }
        }

        private void btnCheckConsistencyCompanyMedia_Click(object sender, EventArgs e)
        {
            if (this.cblCompanies.SelectedItems.Count > 0 &&
                this.lbCompanyData.SelectedItems.Count > 0)
            {
                var companyIds = this.cblCompanies.SelectedItemsValuesAsIntList;
                var companyDataValues = this.lbCompanyData.SelectedItemsValuesAsIntList;

                // Company data
                UploadCompanyMediaRequest uploadCompanyMediaRequest = new UploadCompanyMediaRequest();
                uploadCompanyMediaRequest.UploadCompany = companyDataValues.IndexOf(ListItemCompanyIndex) > -1 ? true : false; ;
                uploadCompanyMediaRequest.UploadProducts = companyDataValues.IndexOf(ListItemProductsIndex) > -1 ? true : false;
                uploadCompanyMediaRequest.UploadCategories = companyDataValues.IndexOf(ListItemCategoriesIndex) > -1 ? true : false;
                uploadCompanyMediaRequest.UploadDeliverypointgroups = companyDataValues.IndexOf(ListItemDeliverypointgroupsIndex) > -1 ? true : false;
                uploadCompanyMediaRequest.UploadAdvertisements = companyDataValues.IndexOf(ListItemAdvertisementsIndex) > -1 ? true : false;
                uploadCompanyMediaRequest.UploadEntertainment = companyDataValues.IndexOf(ListItemEntertainmentsIndex) > -1 ? true : false;
                uploadCompanyMediaRequest.UploadAlterations = companyDataValues.IndexOf(ListItemAlterationsIndex) > -1 ? true : false;
                uploadCompanyMediaRequest.UploadAlterationoptions = companyDataValues.IndexOf(ListItemAlterationoptionsIndex) > -1 ? true : false;
                uploadCompanyMediaRequest.UploadSurveys = companyDataValues.IndexOf(ListItemSurveysIndex) > -1 ? true : false;
                uploadCompanyMediaRequest.UploadSites = companyDataValues.IndexOf(ListItemSitesIndex) > -1 ? true : false;

                new Thread((param) =>
                {
                    try
                    {
                        GeneralAuthorizer.SetThreadGodMode(true);

                        foreach (int companyId in companyIds)
                        {
                            // Company data
                            uploadCompanyMediaRequest.CompanyId = companyId;

                            UploadHelper.ConsistencyCheckCompanyDataMedia(uploadCompanyMediaRequest);

                            // Now set the Timestamps - because they are not set in the batch
                            CompanyEntity c = new CompanyEntity(companyId);
                            DateTime utcNow = DateTime.UtcNow;
                            c.AdvertisementDataLastModifiedUTC = utcNow;
                            c.AdvertisementMediaLastModifiedUTC = utcNow;
                            c.AnnouncementDataLastModifiedUTC = utcNow;
                            c.AnnouncementMediaLastModifiedUTC = utcNow;
                            c.CompanyDataLastModifiedUTC = utcNow;
                            c.CompanyMediaLastModifiedUTC = utcNow;
                            c.DeliverypointDataLastModifiedUTC = utcNow;
                            c.EntertainmentDataLastModifiedUTC = utcNow;
                            c.EntertainmentMediaLastModifiedUTC = utcNow;
                            //c.MenuDataLastModifiedUTC = utcNow;
                            //c.MenuMediaLastModifiedUTC = utcNow;
                            c.PosIntegrationInfoLastModifiedUTC = utcNow;
                            c.SurveyDataLastModifiedUTC = utcNow;
                            c.SurveyMediaLastModifiedUTC = utcNow;
                            c.Save();
                        }
                    }
                    catch
                    { }
                    finally
                    {
                        GeneralAuthorizer.SetThreadGodMode(false);
                    }
                }).Start(HttpContext.Current);

                string message = "Check Consistency Media Task has started in the background can take between 5 minutes and 2 hours to complete!";
                ((Dionysos.Web.UI.PageDefault)this.Page).AddInformator(Dionysos.Web.UI.InformatorType.Information, message);
            }
            else
            {
                string message = "Check Consistency Task not started, no company or company media selected!";
                ((Dionysos.Web.UI.PageDefault)this.Page).AddInformator(Dionysos.Web.UI.InformatorType.Information, message);
            }
        }

        private void UploadGenericMedia()
        {
            if (this.lbGenericData.SelectedItems.Count > 0)
            {
                var genericDataValues = this.lbGenericData.SelectedItemsValuesAsIntList;

                // Generic data
                UploadGenericMediaRequest uploadGenericMediaRequest = new UploadGenericMediaRequest();
                uploadGenericMediaRequest.UploadGenericProducts = genericDataValues.IndexOf(ListItemGenericProductsIndex) > -1 ? true : false;
                uploadGenericMediaRequest.UploadGenericCategories = genericDataValues.IndexOf(ListItemGenericCategoriesIndex) > -1 ? true : false;
                uploadGenericMediaRequest.UploadGenericEntertainment = genericDataValues.IndexOf(ListItemGenericEntertainmentIndex) > -1 ? true : false;
                uploadGenericMediaRequest.UploadGenericPointsOfInterest = genericDataValues.IndexOf(ListItemGenericPointsofinterestIndex) > -1 ? true : false;
                uploadGenericMediaRequest.UploadGenericSites = genericDataValues.IndexOf(ListItemGenericSitesIndex) > -1 ? true : false;
                uploadGenericMediaRequest.UploadGenericAdvertisements = genericDataValues.IndexOf(ListItemGenericAdvertisementsIndex) > -1 ? true : false;
                uploadGenericMediaRequest.UploadGenericSiteTemplates = genericDataValues.IndexOf(ListItemGenericSiteTemplatesIndex) > -1 ? true : false;

                // Force save?
                bool forceSave = true;
                bool unpublishedOnly = this.cbUnpublishedGenericMediaOnly.Checked;

                new Thread((param) =>
                {
                    try
                    {
                        GeneralAuthorizer.SetThreadGodMode(true);

                        if (this.lbGenericData.SelectedItems.Count > 0)
                        {
                            // Generic data

                            UploadHelper.UploadGenericMedia(uploadGenericMediaRequest, forceSave, unpublishedOnly);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    finally
                    {
                        GeneralAuthorizer.SetThreadGodMode(false);
                    }
                }).Start(HttpContext.Current);

                string errorMessage = "Upload Media Task has started in the background can take between 5 minutes and 2 hours to complete!";
                ((Dionysos.Web.UI.PageDefault)this.Page).AddInformator(Dionysos.Web.UI.InformatorType.Information, errorMessage);
            }
            else
            {
                string errorMessage = "Upload Media Task not started, no generic media selected!";
                ((Dionysos.Web.UI.PageDefault)this.Page).AddInformator(Dionysos.Web.UI.InformatorType.Information, errorMessage);
            }
                
        }

        private void btnCheckConsistencyGenericMedia_Click(object sender, EventArgs e)
        {
            if (this.lbGenericData.SelectedItems.Count > 0)
            {
                var genericDataValues = this.lbGenericData.SelectedItemsValuesAsIntList;

                // Generic data
                UploadGenericMediaRequest uploadGenericMediaRequest = new UploadGenericMediaRequest();
                uploadGenericMediaRequest.UploadGenericProducts = genericDataValues.IndexOf(ListItemGenericProductsIndex) > -1 ? true : false;
                uploadGenericMediaRequest.UploadGenericCategories = genericDataValues.IndexOf(ListItemGenericCategoriesIndex) > -1 ? true : false;
                uploadGenericMediaRequest.UploadGenericEntertainment = genericDataValues.IndexOf(ListItemGenericEntertainmentIndex) > -1 ? true : false;
                uploadGenericMediaRequest.UploadGenericPointsOfInterest = genericDataValues.IndexOf(ListItemGenericPointsofinterestIndex) > -1 ? true : false;
                uploadGenericMediaRequest.UploadGenericSites = genericDataValues.IndexOf(ListItemGenericSitesIndex) > -1 ? true : false;
                uploadGenericMediaRequest.UploadGenericAdvertisements = genericDataValues.IndexOf(ListItemGenericAdvertisementsIndex) > -1 ? true : false;

                new Thread((param) =>
                {
                    try
                    {
                        GeneralAuthorizer.SetThreadGodMode(true);

                        if (this.lbGenericData.SelectedItems.Count > 0)
                        {
                            // Generic data

                            UploadHelper.ConsistencyCheckGenericMedia(uploadGenericMediaRequest);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    finally
                    {
                        GeneralAuthorizer.SetThreadGodMode(false);
                    }
                }).Start(HttpContext.Current);

                string errorMessage = "Upload Media Task has started in the background can take between 5 minutes and 2 hours to complete!";
                ((Dionysos.Web.UI.PageDefault)this.Page).AddInformator(Dionysos.Web.UI.InformatorType.Information, errorMessage);
            }
            else
            {
                string errorMessage = "Upload Media Task not started, no generic media selected!";
                ((Dionysos.Web.UI.PageDefault)this.Page).AddInformator(Dionysos.Web.UI.InformatorType.Information, errorMessage);
            }
                
        }


        #endregion

        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.HookupEvents();
            this.SetGui();            
        }

        void btnUploadGenericMedia_Click(object sender, EventArgs e)
        {
            this.UploadGenericMedia();            
        }

        void btnUploadCompanyMedia_Click(object sender, EventArgs e)
        {
            this.UploadCompanyMedia();
        }

        protected void lb_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            Dionysos.Web.UI.DevExControls.ListBox lb = (Dionysos.Web.UI.DevExControls.ListBox)sender;
            bool isSelected = Convert.ToBoolean(e.Parameter);
            if (isSelected)
                lb.SelectAll();
            else
                lb.UnselectAll();            
        }

        #endregion
        
        #region Properties

        

        #endregion        
    }
}