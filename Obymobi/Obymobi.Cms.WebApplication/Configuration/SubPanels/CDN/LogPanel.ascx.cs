﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using System.Threading;
using Obymobi.Data.Injectables.Authorizers;
using Obymobi.Web.Media;
using DevExpress.Web;
using Obymobi.Web.CDN;
using System.Web.Hosting;
using System.IO;
using Obymobi.Constants;
using System.Text;

namespace Obymobi.ObymobiCms.Configuration.SubPanels.CDN
{
    public partial class LogPanel : System.Web.UI.UserControl
    {
        #region Fields

        private string logPathPostfix = @"App_Data\Logs";
        private string logFilenamePrefix = "MediaTaskPoller";

        #endregion

        #region Methods

        private void LoadUserControls()
        {
            
        }

        private void HookupEvents()
        {
            
        }

        private void SetGui()
        {
            try
            {
                this.tbLog.Text = string.Empty;

                string logPath = WebEnvironmentHelper.GetServicesPath() + logPathPostfix;
                string filepath = Path.Combine(logPath, this.GetFilename());

                var lines = File.ReadAllLines(filepath).Reverse();

                int counter = 0;
                foreach (string line in lines)
                {
                    this.tbLog.Text += (line + Environment.NewLine);

                    if (counter < this.NumberOfLines)
                        counter++;
                    else
                        break;
                } 
            }
            catch (Exception e)
            {
                this.tbLog.Text = string.Format("An exception occurred: {0}\n\nPlease check if 'Services' is running.", e.Message);
            }
        }

        /// <summary>
        /// Gets the filename.
        /// </summary>
        /// <param name="prefix">The prefix.</param>
        /// <returns></returns>
        public string GetFilename()
        {
            return string.Format("{0}-{1:yyyyMMdd}.log", this.logFilenamePrefix, DateTime.Now);
        }
        
        #endregion

        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.HookupEvents();
            this.SetGui();            
        }
        
        #endregion
        
        #region Properties

        private int NumberOfLines
        {
            get
            {
                int toReturn = 250;
                if (this.tbNumberOfLines.Text.Length > 0 && Int32.TryParse(this.tbNumberOfLines.Text, out toReturn))
                {
                }
                return toReturn;
            }
        }
        
        #endregion               
    }
}