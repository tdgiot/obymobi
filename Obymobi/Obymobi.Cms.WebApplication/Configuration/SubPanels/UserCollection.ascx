﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Configuration.Subpanels.UserCollection" Codebehind="UserCollection.ascx.cs" %>
<div style="overflow:hidden;margin-bottom:10px;">
   <div style="float:left;margin-right:10px;">
   <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlUserId" UseDataBinding="true" EntityName="User" TextField="Username" PreventEntityCollectionInitialization="true" Width="250px" notdirty="true"></X:ComboBoxLLBLGenEntityCollection>
   </div>
   <div style="float:left;">
   <D:Button runat="server" ID="btnAdd" CommandName="Add" Text="Add user" ToolTip="Add" />
   </div>
</div>

<X:GridViewColumnSelector runat="server" ID="gvcsUsers">
	<Columns />
</X:GridViewColumnSelector>