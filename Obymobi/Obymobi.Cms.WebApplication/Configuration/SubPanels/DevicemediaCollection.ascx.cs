﻿using System;

namespace Obymobi.ObymobiCms.Configuration.Subpanels
{
    public partial class DevicemediaCollection : Dionysos.Web.UI.DevExControls.SubPanelLLBLOverviewDataSourceCollection
    {
        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "Devicemedia";
            base.OnInit(e);
        }
    }
}