﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using Obymobi.Cms.Logic.DataSources;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;

namespace Obymobi.Cms.WebApplication.Configuration.SubPanels
{
    public partial class FeatureTogglesPanel : UserControl
    {
        private readonly FeatureToggleAvailabilityDataSource featureToggleAvailabilityDataSource = new FeatureToggleAvailabilityDataSource();

        public void Save()
        {
            if (!AllowEditing)
            {
                return;
            }

            foreach (FeatureToggle featureToggle in EnumUtil.GetValues<FeatureToggle>())
            {
                FeatureToggleAvailabilityEntity featureToggleAvailability = featureToggleAvailabilityDataSource.GetOrCreateDefault(featureToggle);

                string releaseGroupName = Request.Form[$"{featureToggle}_release_group"];
                string userRoleName = Request.Form[$"{featureToggle}_user_role"];

                featureToggleAvailability.ReleaseGroup = releaseGroupName.ToEnum<ReleaseGroup>();
                featureToggleAvailability.Role = userRoleName.ToEnum<Role>();

                featureToggleAvailabilityDataSource.Save(featureToggleAvailability);
            }
        }

        public FeatureToggleAvailabilityEntity GetFeatureToggleOrDefault(FeatureToggle featureToggle) => featureToggleAvailabilityDataSource.GetOrCreateDefault(featureToggle);

        public IEnumerable<Role> GetAvailableRoles()
        {
            return new []
            {
                Role.GodMode,
                Role.Crave,
                Role.Administrator,
                Role.Reseller,
                Role.Supervisor,
                Role.Customer
            };
        }

        public bool AllowEditing { get; internal set; }
    }
}
