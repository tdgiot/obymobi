﻿using Dionysos.Data.LLBLGen;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using Obymobi.Enums;
using Obymobi.Data.CollectionClasses;

namespace Obymobi.ObymobiCms.Configuration.Subpanels
{
    public partial class UserBrandCollection : Dionysos.Web.UI.DevExControls.SubPanelLLBLOverviewDataSourceCollection
    {        
        #region Methods

        protected override void OnInit(EventArgs e)
        {
            this.DataSourceLoaded += AccountBrandCollection_DataSourceLoaded;
            base.OnInit(e);
			
            this.EntityName = "UserBrand";
            this.EntityPageUrl = "~/Configuration/Users/UserBrand.aspx";

            this.MainGridView.ShowEditHyperlinkColumn = false;
            this.MainGridView.EnableRowDoubleClick = true;
            this.MainGridView.ShowDeleteHyperlinkColumn = false;
            this.MainGridView.ShowColumnSelectorButton = false;

            this.phlAddBrand.Visible = CmsSessionHelper.CurrentRole >= Role.Administrator;
        }

        private void AccountBrandCollection_DataSourceLoaded(object sender)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(new FieldCompareSetPredicate(BrandFields.BrandId, UserBrandFields.BrandId, SetOperator.In, (UserBrandFields.UserId == this.ParentPrimaryKeyFieldValue), true));

            SortExpression sort = new SortExpression(new SortClause(BrandFields.Name, SortOperator.Ascending));

            BrandCollection brandCollection = EntityCollection.GetMulti<BrandCollection>(filter, sort, null, null, null);
            this.ddlBrandId.DataSource = brandCollection;
            this.ddlBrandId.DataBind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.btnAdd.Click += new EventHandler(btnAdd_Click);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {         
            if (this.ddlBrandId.ValidId > 0)
            {
                if (this.ddlRole.SelectedItem == null)
                {
                    return;
                }

                BrandRole brandRole = (BrandRole)((int)this.ddlRole.SelectedItem.Value);

                bool wasNew = this.ParentDataSource.IsNew;
                if (wasNew)
                    this.ParentDataSource.Save();                

                // Check if connection doesn't already exists
                PredicateExpression filter = new PredicateExpression();
                filter.Add(UserBrandFields.UserId == this.ParentPrimaryKeyFieldValue);
                filter.Add(UserBrandFields.BrandId == this.ddlBrandId.Value);

                if (EntityCollection.GetDbCount<Data.CollectionClasses.UserBrandCollection>(filter) == 0)
                {
                    UserBrandEntity userBrandEntity = new UserBrandEntity();
                    userBrandEntity.UserId = this.ParentPrimaryKeyFieldValue;
                    userBrandEntity.BrandId = this.ddlBrandId.ValidId;
                    userBrandEntity.Role = brandRole;
                    userBrandEntity.Save();
                }

                if (wasNew)
                    this.Parent.Save();
            }
        }

        #endregion
      
    }
}