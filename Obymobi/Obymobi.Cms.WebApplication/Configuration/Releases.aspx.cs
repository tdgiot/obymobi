﻿using System;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using System.Drawing;
using Obymobi.Enums;

namespace Obymobi.ObymobiCms.Configuration
{
	public partial class Releases : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (DataSource is LLBLGenProDataSource datasource)
            {
                datasource.SorterToUse = new SortExpression(new SortClause(ReleaseFields.Version, SortOperator.Descending));
            }

            if (MainGridView != null)
            {
                MainGridView.HtmlRowPrepared += new DevExpress.Web.ASPxGridViewTableRowEventHandler(MainGridView_HtmlRowPrepared);
                MainGridView.CustomColumnDisplayText += new DevExpress.Web.ASPxGridViewColumnDisplayTextEventHandler(OnCustomColumnDisplayText);
            }
        }

        private void MainGridView_HtmlRowPrepared(object sender, DevExpress.Web.ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType != DevExpress.Web.GridViewRowType.Data)
            { return; }

            bool isStableRelease = (bool)e.GetValue("IsStableRelease");

            if (!isStableRelease)
            { e.Row.ForeColor = Color.Gray; }
        }

        private void OnCustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs e)
        {
            if (e.Column.FieldName == "ReleaseGroup")
            {
                int releaseGroup = (int)e.GetFieldValue("ReleaseGroup");
                e.DisplayText = releaseGroup > 0 ? Enum.GetName(typeof(ReleaseGroup), releaseGroup) : "Unknown";
            }
        }
    }
}
