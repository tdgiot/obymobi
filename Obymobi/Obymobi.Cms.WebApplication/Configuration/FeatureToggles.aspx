﻿<%@ Page Title="Feature Toggles" Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Configuration.FeatureToggles" Codebehind="FeatureToggles.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitleHolder" Runat="Server">
    Feature Toggles
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhToolbarHolder" Runat="Server">
    <span class="toolbar">
        <X:ToolBarButton runat="server" ID="btSave" CommandName="Save" Text="Save" Image-Url="~/Images/Icons/disk.png" />
    </span>
	<style type="text/css">
        .dxgvFilterRow_Glass {
			background-color: unset;
		}

		tr:nth-child(even) {
			background: #EDF3F4;
		}
	</style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhContentHolder" Runat="Server">
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Features" Name="Features" LocalizeText="False">
                <Controls>
                    <D:PlaceHolder runat="server" ID="plhFeatureToggles" />
                </Controls>
			</X:TabPage>
            <X:TabPage Text="Companies" Name="Companies" LocalizeText="False">
                <Controls>
                    <D:PlaceHolder runat="server" ID="plhReleaseGroups" />
                </Controls>
            </X:TabPage>
        </TabPages>
    </X:PageControl>
</asp:Content>

