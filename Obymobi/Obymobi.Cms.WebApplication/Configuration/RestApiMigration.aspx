﻿<%@ Page Title="REST API migration" Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Configuration.RestApiMigration" CodeBehind="RestApiMigration.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitleHolder" runat="Server">
    REST API migration
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhToolbarHolder" runat="Server">
    <span class="toolbar">
        <X:ToolBarButton runat="server" ID="btMigrate" CommandName="Migrate" Text="Migrate selected deliverypoints" Image-Url="~/Images/Icons/cog.png" LocalizeText="false" />
        <X:ToolBarButton runat="server" ID="btRefresh" CommandName="Refresh" Text="Refresh" Image-Url="~/Images/Icons/arrow_refresh.png" />
    </span>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhContentHolder" runat="Server">
    <X:PageControl ID="tabsMain" runat="server" Width="100%">
        <TabPages>
            <X:TabPage Text="Generic" Name="Generic" LocalizeText="False">
                <controls>
                    <div style="float: left; width: 530px; text-align: center">
                        <D:Label ID="lblClientsToMigrate" runat="server" LocalizeText="false" Style="font-weight: bold; font-size: 24px; padding: 40px;"></D:Label>
                        <X:GridView ID="gvClientsToMigrate" ClientInstanceName="gvClientsToMigrate" runat="server" KeyFieldName="ClientId">
                            <SettingsBehavior AutoFilterRowInputDelay="500" />
                            <SettingsPager PageSize="100"></SettingsPager>
                            <SettingsBehavior AllowGroup="false" AllowDragDrop="false" />
                            <Columns>
                                <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="30px">
                                    <HeaderTemplate>
                                        <input type="checkbox" onclick="gvClientsToMigrate.SelectAllRowsOnPage(this.checked);" title="Select all rows on this page" />
                                    </HeaderTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                </dxwgv:GridViewCommandColumn>
                                <dxwgv:GridViewDataHyperLinkColumn FieldName="ClientId" Caption="DP" VisibleIndex="1">
                                    <Settings AutoFilterCondition="BeginsWith" FilterMode="DisplayText" />
                                    <PropertiesHyperLinkEdit NavigateUrlFormatString="~/Company/Client.aspx?id={0}" Target="_blank" TextField="DeliverypointName" />
                                </dxwgv:GridViewDataHyperLinkColumn>
                                <dxwgv:GridViewDataColumn FieldName="DeliverypointgroupName" Caption="DPG" VisibleIndex="2" SortIndex="0" SortOrder="Ascending">
                                    <Settings AutoFilterCondition="BeginsWith" />
                                </dxwgv:GridViewDataColumn>
                            </Columns>
                            <Settings ShowFilterRow="True" />
                        </X:GridView>
                    </div>

                    <div style="float: left; width: 530px; text-align: center; margin-left: 8px">
                        <D:Label ID="lblClientsMigrating" runat="server" LocalizeText="false" Style="font-weight: bold; font-size: 24px; padding: 40px;"></D:Label>
                        <X:GridView ID="gvClientsMigrating" ClientInstanceName="gvClientsMigrating" runat="server" KeyFieldName="ClientId">
                            <SettingsBehavior AutoFilterRowInputDelay="500" />
                            <SettingsPager PageSize="100"></SettingsPager>
                            <SettingsBehavior AllowGroup="false" AllowDragDrop="false" />
                            <Columns>
                                <dxwgv:GridViewDataHyperLinkColumn FieldName="ClientId" Caption="DP" VisibleIndex="1">
                                    <Settings AutoFilterCondition="BeginsWith" FilterMode="DisplayText" />
                                    <PropertiesHyperLinkEdit NavigateUrlFormatString="~/Company/Client.aspx?id={0}" Target="_blank" TextField="DeliverypointName" />
                                </dxwgv:GridViewDataHyperLinkColumn>
                                <dxwgv:GridViewDataColumn FieldName="DeliverypointgroupName" Caption="DPG" VisibleIndex="2" SortIndex="0" SortOrder="Ascending">
                                    <Settings AutoFilterCondition="BeginsWith" />
                                </dxwgv:GridViewDataColumn>
                                <dxwgv:GridViewDataColumn FieldName="Elapsed" Caption="Started" VisibleIndex="3">
                                </dxwgv:GridViewDataColumn>
                            </Columns>
                            <Settings ShowFilterRow="True" />
                        </X:GridView>
                    </div>

                    <div style="float: left; width: 530px; text-align: center; margin-left: 8px">
                        <D:Label ID="lblClientsMigrated" runat="server" LocalizeText="false" Style="font-weight: bold; font-size: 24px; padding: 40px;"></D:Label>
                        <X:GridView ID="gvClientsMigrated" ClientInstanceName="gvClientsMigrated" runat="server" KeyFieldName="ClientId">
                            <SettingsBehavior AutoFilterRowInputDelay="500" />
                            <SettingsPager PageSize="100"></SettingsPager>
                            <SettingsBehavior AllowGroup="false" AllowDragDrop="false" />
                            <Columns>
                                <dxwgv:GridViewDataHyperLinkColumn FieldName="ClientId" Caption="DP" VisibleIndex="1">
                                    <Settings AutoFilterCondition="BeginsWith" FilterMode="DisplayText" />
                                    <PropertiesHyperLinkEdit NavigateUrlFormatString="~/Company/Client.aspx?id={0}" Target="_blank" TextField="DeliverypointName" />
                                </dxwgv:GridViewDataHyperLinkColumn>
                                <dxwgv:GridViewDataColumn FieldName="DeliverypointgroupName" Caption="DPG" VisibleIndex="2" SortIndex="0" SortOrder="Ascending">
                                    <Settings AutoFilterCondition="BeginsWith" />
                                </dxwgv:GridViewDataColumn>
                            </Columns>
                            <Settings ShowFilterRow="True" />
                        </X:GridView>
                    </div>
                </controls>
            </X:TabPage>
        </TabPages>
    </X:PageControl>
</asp:Content>

