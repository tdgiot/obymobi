<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Configuration.SupportpoolNotificationRecipient" Title="Supportpool notification recipient" Codebehind="SupportpoolNotificationRecipient.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
<D:Label runat="server" ID="lblTitleProducts">Support pool notification recipient</D:Label> -
<D:Label runat="server" ID="lblTitleEdit">Bewerken</D:Label> -
<D:Label runat="server" ID="lblTitleBrandName" LocalizeText="false">Bewerken</D:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<X:PageControl Id="tabsMain" runat="server" Width="100%">
	<TabPages>
		<X:TabPage Text="Algemeen" Name="Generic">
			<Controls>
				<table class="dataformV2">
					<tr>
						<td class="label">
							<D:LabelEntityFieldInfo runat="server" id="lblName">Name</D:LabelEntityFieldInfo>
						</td>
						<td class="control">
							<D:TextBoxString ID="tbName" runat="server"></D:TextBoxString>
						</td>
						<td class="label">
							<D:LabelEntityFieldInfo runat="server" id="lblSupportpoolId">Support pool</D:LabelEntityFieldInfo>
						</td>
						<td class="control">
							<X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlSupportpoolId" UseDataBinding="true" EntityName="Supportpool" TextField="Name" PreventEntityCollectionInitialization="true" ReadOnly="true"></X:ComboBoxLLBLGenEntityCollection>
						</td>
					</tr>
                    <tr>
						<td class="label">
							<D:LabelEntityFieldInfo runat="server" id="lblPhonenumber">Phonenumber</D:LabelEntityFieldInfo>
						</td>
						<td class="control">
							<D:TextBoxString ID="tbPhonenumber" runat="server"></D:TextBoxString>
						</td>
						<td class="label">
							<D:LabelEntityFieldInfo runat="server" id="lblEmail">Email</D:LabelEntityFieldInfo>
						</td>
						<td class="control">
							<D:TextBoxString ID="tbEmail" runat="server"></D:TextBoxString>
						</td>
                    </tr>
				 </table>			
                 <D:Panel ID="pnlNotifications" runat="server" GroupingText="Notifications">
				    <table class="dataformV2">
					    <tr>
						    <td class="label">
							    <D:LabelEntityFieldInfo runat="server" id="lblNotifyOfflineTerminals">Offline terminals</D:LabelEntityFieldInfo>
						    </td>
						    <td class="control">
							    <D:CheckBox ID="cbNotifyOfflineTerminals" runat="server" />
						    </td>
						    <td class="label">
							    <D:LabelEntityFieldInfo runat="server" id="lblNotifyOfflineClients">Offline clients</D:LabelEntityFieldInfo>
						    </td>
						    <td class="control">
							    <D:CheckBox ID="cbNotifyOfflineClients" runat="server" />
						    </td>
					    </tr>
                        <tr>
                            <td class="label">
							    <D:LabelEntityFieldInfo runat="server" id="lblNotifyUnprocessableOrders">Unprocessable orders</D:LabelEntityFieldInfo>
						    </td>
						    <td class="control">
							    <D:CheckBox ID="cbNotifyUnprocessableOrders" runat="server" />
						    </td>
						    <td class="label">
							    <D:LabelEntityFieldInfo runat="server" id="lblNotifyExpiredSteps">Expired steps</D:LabelEntityFieldInfo>
						    </td>
						    <td class="control">
							    <D:CheckBox ID="cbNotifyExpiredSteps" runat="server" />
						    </td>
                        </tr>
                        <tr>
						    <td class="label">
							    <D:LabelEntityFieldInfo runat="server" id="lblNotifyTooMuchOfflineClientsJump">Offline clients jump</D:LabelEntityFieldInfo>
						    </td>
						    <td class="control">
							    <D:CheckBox ID="cbNotifyTooMuchOfflineClientsJump" runat="server" />
						    </td>
						    <td class="label">
							    <D:LabelEntityFieldInfo runat="server" id="lblNotifyBouncedEmails">Bounced emails</D:LabelEntityFieldInfo>
						    </td>
						    <td class="control">
							    <D:CheckBox ID="cbNotifyBouncedEmails" runat="server" />
						    </td>
                        </tr>
                    </table>
                 </D:Panel>
			</Controls>
		</X:TabPage>
	</TabPages>
</X:PageControl>
  
</asp:Content>

