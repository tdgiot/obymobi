﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dionysos;
using Dionysos.Security.Cryptography;
using Dionysos.Web;
using Dionysos.Web.UI;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.ObymobiCms.MasterPages;
using Obymobi.ObymobiCms.UI;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Configuration
{
    public partial class DeviceTokens : PageLLBLGenEntityCms
    {
        #region Event handlers

        /// <summary>
        /// Executes the pre initialization which needs to be done before rendering the page.
        /// </summary>
        /// <param name="e">Event arguments which have been applied for the page.</param>
        protected override void OnPreInit(EventArgs e)
        {
            this.EntityId = CmsSessionHelper.CurrentCompanyId;
            this.EntityName = "CompanyEntity";
            base.OnPreInit(e);
        }

        /// <summary>
        /// Executes the initialization which needs to be done before rendering the page.
        /// </summary>
        /// <param name="args">Event arguments which has been applied for the page.</param>
        protected override void OnInit(EventArgs args)
        {
            LoadUserControls();
            base.OnInit(args);
        }

        /// <summary>
        /// Allows some necessary modification after the page has been loaded successfully.
        /// </summary>
        /// <param name="sender">The sender which triggered the page load event.</param>
        /// <param name="args">Some arguments which has been applied by the sender object.</param>
        protected void Page_Load(object sender, EventArgs args)
        {
            this.SetGui();
        }

        /// <summary>
        /// Event triggered when device filter index is changed
        /// </summary>
        /// <param name="sender">The combobox triggering the event.</param>
        /// <param name="e">The event arguments.</param>
        protected void CbDeviceFilter_ValueChanged(object sender, EventArgs e)
        {
            this.LoadDevices();
        }

        /// <summary>
        /// Event triggered when company filter index is changed
        /// </summary>
        /// <param name="sender">The combobox triggering the event.</param>
        /// <param name="e">The event arguments</param>
        private void CbCompanyOnValueChanged(object sender, EventArgs e)
        {
            this.LoadDevices();
        }

        /// <summary>
        /// Event triggered when the create device token task button is clicked on the popup dialog.
        /// </summary>
        /// <param name="sender">The button triggering the event.</param>
        /// <param name="e">The event arguments</param>
        private void BtnCreateDeviceTokenTaskOnClick(object sender, EventArgs e)
        {
            EventSender eventSender = this.cbEventSender.SelectedIndex.ToEnum<EventSender>();
            this.CreateDeviceTokenTasksForEventSender(eventSender);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Load user controls onto the page before the page is loaded.
        /// </summary>
        private void LoadUserControls()
        {
            this.tabsMain.AddTabPage("History", "DeviceTokenHistory", "~/Configuration/SubPanels/DeviceTokenHistoryCollection.ascx");

            this.cbCompany.DataBindEntityCollection<CompanyEntity>(null, CompanyFields.Name, CompanyFields.CompanyId, CompanyFields.Name);
            this.cbCompany.Value = CmsSessionHelper.CurrentCompanyId;

            this.cbDeviceFilter.DataBindEnumStringValuesAsDataSource(typeof(DevicesFilter));
            this.cbDeviceFilter.Value = (int)DevicesFilter.SandboxDevices;

            this.cbEventSender.DataBindEnum<EventSender>();
        }

        /// <summary>
        /// Attache all listener events on the user-interface (UI) elements of the page.
        /// </summary>
        private void HookupEvents()
        {
            this.btnCreateDeviceTokenTask.Click += BtnCreateDeviceTokenTaskOnClick;
            this.cbDeviceFilter.ValueChanged += CbDeviceFilter_ValueChanged;
            this.cbCompany.ValueChanged += CbCompanyOnValueChanged;
        }

        /// <summary>
        /// Gets all selected devices from the grid view.
        /// </summary>
        /// <returns>Device collection which contains all selected devices from the datagrid.</returns>
        private DeviceCollection GetSelectedDevices()
        {
            IEnumerable<int> selectedDeviceIdentities = this.devicesGrid.GetSelectedFieldValues(nameof(DeviceEntity.DeviceId))
                .FindAll(id => id != null && id != 0 as object)
                .ConvertAll(id => int.Parse(id.ToString()));

            PredicateExpression filter = new PredicateExpression();
            filter.Add(new FieldCompareRangePredicate(DeviceFields.DeviceId, selectedDeviceIdentities));

            IncludeFieldsList includeFieldsList = new IncludeFieldsList
            {
                DeviceFields.Identifier,
                DeviceFields.Token
            };

            DeviceCollection deviceCollection = new DeviceCollection();
            deviceCollection.GetMulti(filter, 0, null, null, null, includeFieldsList, 0, 0);

            return deviceCollection;
        }

        /// <summary>
        /// Loads all devices into the data grid manually.
        /// </summary>
        private void LoadDevices()
        {
            PredicateExpression filter = new PredicateExpression();
            RelationCollection relations = new RelationCollection();

            this.AddCompanyFilterAndRelation(filter, relations);
            this.AddDeviceFilterAndRelation(filter, relations);

            DeviceCollection devices = this.GetDevices(filter, relations);
            DataTable dataTable = this.CreateDataTable(devices);

            this.devicesGrid.DataSource = dataTable;
            this.devicesGrid.DataBind();
        }

        /// <summary>
        /// Add a filter depending on the company selected in the combobox.
        /// </summary>
        /// <param name="filter">The filter to use.</param>
        /// <param name="relations">The relations to use.</param>
        private void AddCompanyFilterAndRelation(PredicateExpression filter, RelationCollection relations)
        {
            if (this.cbCompany.ValueGreaterThanZero)
            {
                filter.Add(ClientFields.CompanyId == this.cbCompany.ValidId);
                relations.Add(DeviceEntity.Relations.ClientEntityUsingDeviceId);
            }
        }

        /// <summary>
        /// Add a filter depending on the device filter selected in the combobox.
        /// </summary>
        /// <param name="filter">The filter to use.</param>
        /// <param name="relations">The relations to use.</param>
        private void AddDeviceFilterAndRelation(PredicateExpression filter, RelationCollection relations)
        {
            DevicesFilter devicesFilter = this.cbDeviceFilter.SelectedIndex.ToEnum<DevicesFilter>();
            switch (devicesFilter)
            {
                case DevicesFilter.AllDevices:
                    filter.Add(DeviceFields.CustomerId == DBNull.Value);
                    filter.Add(DeviceFields.Token != DBNull.Value);
                    break;
                case DevicesFilter.RevokePendingDevices:
                    filter.Add(DeviceTokenTaskFields.Action == DeviceTokenAction.Revoke);
                    relations.Add(DeviceEntity.Relations.DeviceTokenTaskEntityUsingDeviceId);
                    break;
                case DevicesFilter.RenewPendingDevices:
                    filter.Add(DeviceTokenTaskFields.Action == DeviceTokenAction.Renew);
                    relations.Add(DeviceEntity.Relations.DeviceTokenTaskEntityUsingDeviceId);
                    break;
                default:
                    filter.Add(DeviceFields.SandboxMode == true);
                    break;
            }
        }

        /// <summary>
        /// Creates a data table based on the the supplied device collection.
        /// </summary>
        /// <param name="deviceCollection">Device collection to create the data table for.</param>
        /// <returns>Data table</returns>
        private DataTable CreateDataTable(DeviceCollection deviceCollection)
        {
            DataTable dataTable = new DataTable();

            DataColumn deviceIdColumn = new DataColumn(nameof(DeviceEntity.DeviceId));
            DataColumn identifierColumn = new DataColumn(nameof(DeviceEntity.Identifier));
            DataColumn tokenColumn = new DataColumn(nameof(DeviceEntity.Token));
            DataColumn stateColumn = new DataColumn("State");

            deviceIdColumn.Caption = this.Translate("cmlDeviceId", "Device ID");
            identifierColumn.Caption = this.Translate("clmIdentifuer", "Identifier");
            tokenColumn.Caption = this.Translate("clmToken", "Token");
            stateColumn.Caption = this.Translate("clmState", "State");

            dataTable.Columns.Add(deviceIdColumn);
            dataTable.Columns.Add(identifierColumn);
            dataTable.Columns.Add(tokenColumn);
            dataTable.Columns.Add(stateColumn);

            foreach (DeviceEntity deviceEntity in deviceCollection)
            {
                DataRow dataRow = dataTable.NewRow();

                dataRow[deviceIdColumn] = deviceEntity.DeviceId;
                dataRow[identifierColumn] = deviceEntity.Identifier;
                dataRow[tokenColumn] = deviceEntity.Token;
                dataRow[stateColumn] = this.GetDeviceState(deviceEntity).GetStringValue();

                dataTable.Rows.Add(dataRow);
            }

            return dataTable;
        }

        /// <summary>
        /// Get the current device state; Sandbox, Revoke pending, Renew pending, OK
        /// </summary>
        /// <param name="deviceEntity">The device to get the state for.</param>
        /// <returns>Sandbox, Revoke pending, Renew pending, OK</returns>
        private DeviceState GetDeviceState(DeviceEntity deviceEntity)
        {
            if (deviceEntity.DeviceTokenTaskCollection.Any(x => x.Action == DeviceTokenAction.Renew))
            {
                return DeviceState.RenewPending;
            }
            else if (deviceEntity.DeviceTokenTaskCollection.Any(x => x.Action == DeviceTokenAction.Revoke))
            {
                return DeviceState.RevokePending;
            }
            else if (deviceEntity.SandboxMode)
            {
                return DeviceState.Sandbox;
            }

            return DeviceState.OK;
        }

        /// <summary>
        /// Returns all the devices from the database.
        /// </summary>
        /// <returns>Device collection.</returns>
        private DeviceCollection GetDevices(PredicateExpression filter, RelationCollection relation)
        {
            IncludeFieldsList includeFieldsList = new IncludeFieldsList
            {
                DeviceFields.Identifier,
                DeviceFields.Token,
                DeviceFields.SandboxMode
            };

            PrefetchPath prefetch = new PrefetchPath(EntityType.DeviceEntity);
            prefetch.Add(DeviceEntity.PrefetchPathDeviceTokenTaskCollection, new IncludeFieldsList(DeviceTokenTaskFields.Action));

            DeviceCollection deviceCollection = new DeviceCollection();
            deviceCollection.GetMulti(filter, 0, null, relation, prefetch, includeFieldsList, 0, 0);

            return deviceCollection;
        }

        /// <summary>
        /// Create device token tasks based on the event sender that triggered the event.
        /// </summary>
        /// <param name="eventSender">The event that triggered this action.</param>
        private void CreateDeviceTokenTasksForEventSender(EventSender eventSender)
        {
            this.GetDevicesAndDeviceTokenAction(eventSender, out DeviceCollection deviceCollection, out DeviceTokenAction deviceTokenAction);

            if (!deviceCollection.Any())
            {
                this.MultiValidatorDefault.AddError(string.Format("No device token(s) selected to {0}", deviceTokenAction.ToString().ToLowerInvariant()));
                this.Validate();

                return;
            }

            if (string.IsNullOrWhiteSpace(tbNotes.Text))
            {
                this.MultiValidatorDefault.AddError(string.Format("Notes for the {0} action is required. Please provide a note in order to continue the process.", deviceTokenAction.ToString().ToLower()));
                this.Validate();

                return;
            }

            this.CreateDeviceTokenTasks(deviceCollection, deviceTokenAction, this.tbNotes.Text);
        }

        /// <summary>
        /// Gets the device and token action based on the event that triggered the creation of device token tasks.
        /// </summary>
        /// <param name="eventSender">The event triggering the creation.</param>
        /// <param name="deviceCollection">The device collection.</param>
        /// <param name="action">The device token action</param>
        private void GetDevicesAndDeviceTokenAction(EventSender eventSender, out DeviceCollection deviceCollection, out DeviceTokenAction action)
        {
            deviceCollection = new DeviceCollection();
            action = DeviceTokenAction.Renew;

            switch (eventSender)
            {
                case EventSender.RevokeSelectedTokensButton:
                case EventSender.RevokeTokenGridButton:
                    deviceCollection = this.GetSelectedDevices();
                    action = DeviceTokenAction.Revoke;
                    break;
                case EventSender.RenewSelectedTokensButton:
                case EventSender.RenewTokenGridButton:
                    deviceCollection = this.GetSelectedDevices();
                    action = DeviceTokenAction.Renew;
                    break;
            }
        }

        /// <summary>
        /// Creates device token tasks which will be processed by a background worker.
        /// </summary>
        /// <param name="deviceCollection">List of devices which needs to be processed.</param>
        /// <param name="deviceTokenAction">Action of the task.</param>
        /// <param name="notes">Note to save with the action.</param>
        private void CreateDeviceTokenTasks(DeviceCollection deviceCollection, DeviceTokenAction deviceTokenAction, string notes)
        {
            foreach (DeviceEntity deviceEntity in deviceCollection)
            {
                this.CreateDeviceTokenTask(deviceEntity, deviceTokenAction, notes);
            }

            this.AddInformator(InformatorType.Information, string.Format("{0} device token(s) added to be {1}", deviceCollection.Count, deviceTokenAction == DeviceTokenAction.Renew ? "renewed" : "revoked"));
            this.Validate();
        }

        /// <summary>
        /// Creates a device token task to be picked up by a background worker.
        /// </summary>
        /// <param name="deviceEntity">The device entity for which to create the task.</param>
        /// <param name="deviceTokenAction">The device token action of the task.</param>
        /// <param name="notes">The notes of the task.</param>
        private void CreateDeviceTokenTask(DeviceEntity deviceEntity, DeviceTokenAction deviceTokenAction, string notes)
        {
            if (deviceEntity.DeviceTokenTaskCollection.Any())
                return;

            DeviceTokenTaskEntity deviceTokenTaskEntity = new DeviceTokenTaskEntity();
            deviceTokenTaskEntity.DeviceId = deviceEntity.DeviceId;
            deviceTokenTaskEntity.UserId = CmsSessionHelper.CurrentUser.UserId;
            deviceTokenTaskEntity.Action = deviceTokenAction;
            deviceTokenTaskEntity.State = DeviceTokenState.Queued;
            deviceTokenTaskEntity.StateUpdatedUTC = DateTime.UtcNow;
            deviceTokenTaskEntity.Notes = notes;

            switch (deviceTokenAction)
            {
                case DeviceTokenAction.Revoke:
                    deviceTokenTaskEntity.Token = string.Empty;
                    break;
                case DeviceTokenAction.Renew:
                    deviceTokenTaskEntity.Token = Cryptographer.EncryptUsingCBC(RandomUtil.GetRandomLowerCaseString(16));
                    break;
            }

            deviceTokenTaskEntity.Save();
        }

        /// <summary>
        /// Prepares the Graphical User Interface (GUI) for usage.
        /// </summary>
        private void SetGui()
        {
            this.HookupEvents();
            this.LoadDevices();

            if (this.Master is MasterPageBase masterPage)
            {
                masterPage.SetPageTitle("Device tokens");
            }
        }

        /// <summary>
        /// Device filters used to render devices.
        /// </summary>
        public enum DevicesFilter : int
        {
            [StringValue("Devices in sandbox mode")]
            SandboxDevices,
            [StringValue("Devices with revoke pending")]
            RevokePendingDevices,
            [StringValue("Devices with renew pending")]
            RenewPendingDevices,
            [StringValue("All devices")]
            AllDevices,
        }

        /// <summary>
        /// Device states.
        /// </summary>
        public enum DeviceState : int
        {
            [StringValue("Sandbox")]
            Sandbox,
            [StringValue("Revoke pending")]
            RevokePending,
            [StringValue("Renew pending")]
            RenewPending,
            [StringValue("OK")]
            OK,
        }

        /// <summary>
        /// The event sender which triggered the action to create device token tasks.
        /// </summary>
        public enum EventSender : int
        {
            RenewSelectedTokensButton,
            RevokeSelectedTokensButton,
            RenewTokenGridButton,
            RevokeTokenGridButton
        }

        #endregion
    }
}
