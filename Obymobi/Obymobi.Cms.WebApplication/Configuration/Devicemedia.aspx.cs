﻿using System;
using System.IO;
using Dionysos;
using Obymobi.Data.EntityClasses;

namespace Obymobi.ObymobiCms.Configuration
{
	public partial class Devicemedia : Dionysos.Web.UI.PageLLBLGenEntity
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			LoadUserControls();
		}

		public override bool Save()
		{
			return true;
		}

		private void LoadUserControls()
		{
            string filename = DataSourceAsDevicemediaEntity.Filename;
            string ext = filename.Substring(filename.LastIndexOf('.') + 1).ToLower();
            string webservice = DataSourceAsDevicemediaEntity.WebserviceUrl;

            string fullUrl = webservice + "/Downloads/Devicemedia/" + filename;
            if (webservice.IsNullOrWhiteSpace())
            {
                fullUrl = filename;
                filename = filename.Substring(filename.LastIndexOf("/", StringComparison.Ordinal) + 1);
            }

			string html;
			if (ext.Equals("jpg") || ext.Equals("jpeg") || ext.Equals("png") || ext.Equals("bmp"))
			{
				html = "<img src='" + fullUrl + "' border='0'>";
			}
			else
			{
				html = "Unable to parse file: " + filename + "</br>";
				html += "Try to download and open the file locally: <a href='" + fullUrl + "' target='_blank'>DOWNLOAD</a>";
			}

			plhFile.AddHtml(html);
		}
        
        #region Properties

        /// <summary>
        /// Return the page's datasource as a SurveyQuestionEntity
        /// </summary>
        public DevicemediaEntity DataSourceAsDevicemediaEntity
        {
            get
            {
				return this.DataSource as DevicemediaEntity;
            }
        }

        #endregion
	}
}
