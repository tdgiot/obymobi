<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Configuration.Supportagent" Codebehind="Supportagent.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
					<table class="dataformV2">
					    <tr>
						    <td class="label">
							    <D:LabelEntityFieldInfo runat="server" id="lblFirstname">Voornaam</D:LabelEntityFieldInfo>
						    </td>
						    <td class="control">
							    <D:TextBoxString ID="tbFirstname" runat="server"></D:TextBoxString>
						    </td>
						    <td class="label">
							    <D:LabelEntityFieldInfo runat="server" id="lblLastname">Achternaam</D:LabelEntityFieldInfo>
						    </td>
						    <td class="control">
						        <D:TextBoxString ID="tbLastname" runat="server"></D:TextBoxString>
						    </td>       
					    </tr> 
					    <tr>
						    <td class="label">
							    <D:LabelEntityFieldInfo runat="server" id="lblEmail">Email</D:LabelEntityFieldInfo>
						    </td>
						    <td class="control">
						        <D:TextBoxString ID="tbEmail" runat="server" autocomplete="off"></D:TextBoxString>
						    </td>  
						    <td class="label">
							    <D:LabelEntityFieldInfo runat="server" id="lblLastnamePrefix">Tussenvoegsel</D:LabelEntityFieldInfo>
						    </td>
						    <td class="control">
							    <D:TextBoxString ID="tbLastnamePrefix" runat="server"></D:TextBoxString>
						    </td>
 					    </tr>
					    <tr>
						    <td class="label">
							    <D:LabelEntityFieldInfo runat="server" id="lblPhonenumber">Phonenumber</D:LabelEntityFieldInfo>
						    </td>
						    <td class="control">
						        <D:TextBoxString ID="tbPhonenumber" runat="server" autocomplete="off" Enabled="false"></D:TextBoxString>
						    </td>  
						    <td class="label">
							    &nbsp;
						    </td>
						    <td class="control">
							    &nbsp;
						    </td>
 					    </tr> 					    
					 </table>					
				</Controls>
			</X:TabPage>											
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>

