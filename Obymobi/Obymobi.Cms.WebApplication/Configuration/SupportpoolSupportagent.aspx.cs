using System;
using Obymobi.Enums;
using Obymobi.Logic.Cms;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;

namespace Obymobi.ObymobiCms.Configuration
{
    public partial class SupportpoolSupportagent : Dionysos.Web.UI.PageLLBLGenEntity
    {
        #region EventHandlers

        protected override void OnInit(EventArgs e)
        {
            this.DataSourceLoaded += SupportpoolSupportagent_DataSourceLoaded;
            base.OnInit(e);
        }

        private void SupportpoolSupportagent_DataSourceLoaded(object sender)
        {
            this.SetGui();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        #endregion

        #region Methods

        private void SetGui()
        {
            // Init the supportpools
            SupportpoolCollection supportpoolCollection = new SupportpoolCollection();
            SortExpression supportpoolSort = new SortExpression(SupportpoolFields.Name | SortOperator.Ascending);
            supportpoolCollection.GetMulti(null, 0, supportpoolSort, null, null, null, 0, 0);
            this.ddlSupportpoolId.DataSource = supportpoolCollection;
            this.ddlSupportpoolId.DataBind();            

            // Init the supportagents
            SupportagentCollection supportagentCollection = new SupportagentCollection();
            SortExpression supportagentSort = new SortExpression(SupportagentFields.Firstname | SortOperator.Ascending);
            supportagentCollection.GetMulti(null, 0, supportagentSort, null, null, null, 0, 0);
            this.ddlSupportagentId.DataSource = supportagentCollection;
            this.ddlSupportagentId.DataBind();
        }

        #endregion
    }
}
