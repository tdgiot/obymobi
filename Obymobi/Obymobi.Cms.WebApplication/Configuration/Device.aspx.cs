using System;
using System.Globalization;
using Dionysos;
using Dionysos.Security.Cryptography;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Comet;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Configuration
{
    public partial class Device : Dionysos.Web.UI.PageLLBLGenEntity
    {
        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            LoadUserControls();
            this.DataSourceLoaded += Device_DataSourceLoaded;
            base.OnInit(e);
        }

        private void Device_DataSourceLoaded(object sender) => SetGui();

        private void HookupEvents() => this.lbGenerateIdentifier.Click += lbGenerateIdentifier_Click;

        private void lbGenerateIdentifier_Click(object sender, EventArgs e)
        {
            // Get the unix time
            // and convert it to convert
            long unixTime = DateTime.UtcNow.ToUnixTime();

            string physicalAddressString = "00FF" + unixTime.ToString("X");
            string address = string.Empty;
            for (int i = 0; i < physicalAddressString.Length; i++)
            {
                address += physicalAddressString[i];

                if (i % 2 == 1 && (i != physicalAddressString.Length - 1))
                {
                    address += ":";
                }
            }

            this.tbIdentifier.Text = address.ToLower();
        }

        private void LoadUserControls()
        {
            this.tabsMain.AddTabPage("Media", "DevicemediaCollection", "~/Configuration/SubPanels/DevicemediaCollection.ascx");
            this.tabsMain.AddTabPage("Tokens", "DeviceTokenHistory", "~/Configuration/SubPanels/DeviceTokenHistoryCollection.ascx");
        }

        private void SetGui()
        {
            this.cbSandboxMode.Enabled = this.DataSourceAsDeviceEntity.SandboxMode;

            this.lblLastMessagingServiceVersionValue.Text = !this.DataSourceAsDeviceEntity.MessagingServiceVersion.IsNullOrWhiteSpace() ? new VersionNumber(this.DataSourceAsDeviceEntity.MessagingServiceVersion).NumberAsString : "Unknown";
            this.lblLastMessagingServiceRequestValue.Text = this.DataSourceAsDeviceEntity.LastMessagingServiceRequest.HasValue ? this.DataSourceAsDeviceEntity.LastMessagingServiceRequest.Value.UtcToLocalTime().ToString(CultureInfo.InvariantCulture) : "Unknown";

            this.lblLastMessagingConnectionType.Text = this.DataSourceAsDeviceEntity.LastMessagingServiceConnectionType.ToEnum<MessagingServiceConnectionType>().ToString();
            this.lblLastMessagingConnectionStatus.Text = this.DataSourceAsDeviceEntity.LastMessagingServiceConnectionStatus.ToEnum<MessagingServiceConnectionStatus>().ToString();
            this.lblLastMessagingSuccessfulConnection.Text = this.DataSourceAsDeviceEntity.LastMessagingServiceSuccessfulConnection?.UtcToLocalTime().ToString(CultureInfo.InvariantCulture) ?? "Unknown";

            this.lblMessagingConnectingCount24h.Text = this.DataSourceAsDeviceEntity.MessagingServiceConnectingCount24h.ToString();
            this.lblMessagingConnectedCount24h.Text = this.DataSourceAsDeviceEntity.MessagingServiceConnectedCount24h.ToString();
            this.lblMessagingReconnectingCount24h.Text = this.DataSourceAsDeviceEntity.MessagingServiceReconnectingCount24h.ToString();
            this.lblMessagingConnectionLostCount24h.Text = this.DataSourceAsDeviceEntity.MessagingServiceConnectionLostCount24h.ToString();
        }

        public override bool Save()
        {
            bool tokenRenewed = false;

            if (!this.DataSourceAsDeviceEntity.SandboxMode && this.DataSourceAsDeviceEntity.Fields[(int)DeviceFieldIndex.SandboxMode].IsChanged)
            {
                this.DataSourceAsDeviceEntity.Token = Cryptographer.EncryptUsingCBC(RandomUtil.GetRandomLowerCaseString(16));
                tokenRenewed = true;
            }

            if (base.Save())
            {
                if (tokenRenewed)
                {
                    CreateTokenHistoryEntity(this.DataSourceAsDeviceEntity, CmsSessionHelper.CurrentUser, this.DataSourceAsDeviceEntity.Token, DeviceTokenAction.Renew);
                    CometHelper.UpdateDeviceToken(this.DataSourceAsDeviceEntity, DeviceTokenAction.Renew);
                }

                if (this.DataSourceAsDeviceEntity.ClientCollection.Count == 0)
                {
                    // Get selected client entity
                    int? clientId = this.dllClientId.Value;
                    if (clientId.HasValue)
                    {
                        ClientEntity clientEntity = new ClientEntity(clientId.Value)
                        {
                            DeviceId = this.DataSourceAsDeviceEntity.DeviceId
                        };
                        clientEntity.Save();
                    }
                }

                return true;
            }

            return false;
        }

        private void CreateTokenHistoryEntity(DeviceEntity deviceEntity, UserEntity userEntity, string token, DeviceTokenAction action)
        {
            DeviceTokenHistoryEntity deviceTokenHistoryEntity = new DeviceTokenHistoryEntity
            {
                DeviceId = deviceEntity.DeviceId,
                Identifier = deviceEntity.Identifier,
                UserId = userEntity.UserId,
                Username = userEntity.Username,
                Token = token,
                Action = action,
                Notes = string.Format("{0} through CMS - device page", action)
            };
            deviceTokenHistoryEntity.Save();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            HookupEvents();

            if (!this.IsPostBack)
            {
                if (this.DataSourceAsDeviceEntity.ClientCollection.Count == 1)
                {
                    this.hlClient.Visible = true;
                    this.hlClient.NavigateUrl = ResolveUrl(string.Format("~/Company/Client.aspx?id={0}", this.DataSourceAsDeviceEntity.ClientCollection[0].ClientId));
                }
                else
                {
                    ClientCollection clients = new ClientCollection();
                    PredicateExpression clientFilter = new PredicateExpression
                    {
                        ClientFields.CompanyId == CmsSessionHelper.CurrentCompanyId
                    };
                    clientFilter.AddWithAnd(ClientFields.DeviceId == DBNull.Value);
                    clients.GetMulti(clientFilter);

                    this.dllClientId.DataSource = clients;
                    this.dllClientId.DataBind();
                    this.dllClientId.Visible = true;
                }

                if (this.DataSourceAsDeviceEntity.IsNew)
                {
                    this.ddlType.SelectedIndex = 3;
                    this.ddlUpdateStatus.SelectedIndex = 0;
                }
            }
        }

        #endregion

        #region Properties

        public DeviceEntity DataSourceAsDeviceEntity => this.DataSource as DeviceEntity;

        #endregion
    }
}
