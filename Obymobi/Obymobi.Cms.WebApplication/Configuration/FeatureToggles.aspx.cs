﻿using System;
using Dionysos.Web.UI;
using Obymobi.Cms.WebApplication.Configuration.SubPanels;
using Obymobi.Enums;

namespace Obymobi.ObymobiCms.Configuration
{
    public partial class FeatureToggles : PageDefault
    {
        private FeatureTogglesPanel featureTogglesPanel;
        private ReleaseGroupPanel releaseGroupPanel;

        private void LoadUserControls()
        {
            bool currentUserHasEditorRights = CmsSessionHelper.CurrentRole == Role.GodMode;

            this.btSave.Visible = currentUserHasEditorRights;

            featureTogglesPanel = (FeatureTogglesPanel)LoadControl("~/Configuration/SubPanels/FeatureTogglesPanel.ascx");
            featureTogglesPanel.AllowEditing = currentUserHasEditorRights;
            plhFeatureToggles.Controls.Add(featureTogglesPanel);

            releaseGroupPanel = (ReleaseGroupPanel)LoadControl("~/Configuration/SubPanels/ReleaseGroupPanel.ascx");
            releaseGroupPanel.AllowEditing = currentUserHasEditorRights;
            plhReleaseGroups.Controls.Add(releaseGroupPanel);
        }

        public void Save()
        {
            featureTogglesPanel?.Save();
            releaseGroupPanel?.Save();
        }

        protected override void OnInit(EventArgs e)
        {
            LoadUserControls();
            base.OnInit(e);
        }
    }
}