using System;
using System.IO;
using System.Web;
using Dionysos.Security.Cryptography;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos;
using Obymobi.Data.Injectables.Validators;
using Obymobi.Logic.HelperClasses;
using Obymobi.ObymobiCms.UI;
using DevExpress.Web;
using System.Linq;

namespace Obymobi.ObymobiCms.Configuration
{
    public partial class Release : PageLLBLGenEntityCms
    {
        #region Methods

        protected override void OnInit(EventArgs e)
        {
            LoadUserControls();
            base.OnInit(e);
            UpdateUploadEnabled();
        }

        private void LoadUserControls()
        {
            tabsMain.AddTabPage("CDN", "CloudProcessingTaskCollection", "~/Generic/UserControls/CloudTaskCollection.ascx");
            tbVersion.TextChanged += (s, args) => UpdateUploadEnabled();
            tbComment.TextChanged += (s, args) => UpdateUploadEnabled();
            ddlApplicationId.ValueChanged += (s, args) => UpdateUploadEnabled();
        }

        private void UpdateUploadEnabled() 
            => ucFile.Enabled = !string.IsNullOrEmpty(DataSourceAsReleaseEntity.Version) && !string.IsNullOrEmpty(DataSourceAsReleaseEntity.Comment) && DataSourceAsReleaseEntity.ApplicationId != 0;
        #endregion

        #region Event Handlers

        public override bool Save()
        {
            if (!ValidateRelease())
            { return false; }

            return SaveRelease();
        }

        public void ucFile_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            UploadedFile file = e.UploadedFile;

            if (!ValidateRelease())
            { return; }

            ApplicationEntity application = GetSelectedApplicationEntity();

            if (application == null)
            { return; }

            DataSourceAsReleaseEntity.File = file.FileBytes;
            DataSourceAsReleaseEntity.Crc32 = Crc32.ComputeHashBuffer(file.FileBytes);
            DataSourceAsReleaseEntity.Filename = $"builds/{application.Code}/{tbVersion.Value}/{Path.GetFileName(file.FileName)}";

            bool savedSuccessfully = SaveRelease(true);

            if (savedSuccessfully)
            {
                CloudTaskHelper.UploadRelease(DataSourceAsReleaseEntity);
            }

            e.CallbackData = DataSourceAsReleaseEntity.Filename;
        }

        public override bool Delete()
        {
            bool deleted = base.Delete();
            if (deleted)
            {
                if (!string.IsNullOrEmpty(this.DataSourceAsReleaseEntity.Filename))
                {
                    string saveDir = string.Empty;

                    CloudTaskHelper.DeleteRelease(this.DataSourceAsReleaseEntity);

                    if (this.Request.Url.Authority.Contains("localhost"))
                    {
                        // Local
                        saveDir = HttpContext.Current.Request.MapPath(string.Format("~/"));
                        saveDir = saveDir.Replace("ObymobiCms", "ObymobiWebService");
                    }
                    else if (HttpContext.Current.Request.Url.Host.Contains("test."))
                    {
                        // Live
                        saveDir = "c:\\inetpub\\app.crave-emenu.com\\api\\";
                    }
                    else if (HttpContext.Current.Request.Url.Host.Contains("dev."))
                    {
                        // Dev
                        saveDir = "c:\\inetpub\\dev.crave-emenu.com\\api\\";
                    }
                    else if (HttpContext.Current.Request.Url.Host.Contains("demo."))
                    {
                        // Demo
                        saveDir = "c:\\inetpub\\demo.crave-emenu.com\\api\\";
                    }
                    else
                    {
                        // Test
                        saveDir = "c:\\inetpub\\test.crave-emenu.com\\api\\";
                    }

                    string path = saveDir + "/" + this.DataSourceAsReleaseEntity.Filename;
                    if (File.Exists(path))
                    {
                        File.Delete(path);
                        Directory.Delete(Directory.GetParent(path).FullName);
                    }
                }
            }
            return deleted;
        }

        private bool ValidateRelease()
        {
            bool isValid = true;

            // Check whether the version number contains digits only
            if (!tbVersion.Value.IsNumeric())
            {
                MultiValidatorDefault.AddError(Translate("VersionNumberDigitsOnly", "Versie nummers kunnen alleen cijfers bevatten"));
                isValid = false;
            }

            if (ddlApplicationId.Value == null)
            { isValid = false; }

            if (!SelectedApplicationExists())
            {
                MultiValidatorDefault.AddError("The selected Application no longer exists.");
                isValid = false;
            }

            Validate();

            if (!IsValid)
            { isValid = false; }

            return isValid;
        }

        private bool SaveRelease(bool saveEntityOnly = false)
        {
            bool success;

            try
            {
                success = saveEntityOnly ? DataSourceAsReleaseEntity.Save() : base.Save();
            }
            catch (ObymobiException ex)
            {
                if (ex.ErrorEnumValue.Equals(ReleaseValidator.ReleaseValidatorResult.FullReleaseDoesNotExists))
                {
                    MultiValidatorDefault.AddError(Translate("FullReleaseDoesNotExists", string.Format("Full installation file for version '{0}' could not be found. Please add full install first before adding incremental update.", tbVersion.Value)));
                }
                else if (ex.ErrorEnumValue.Equals(ReleaseValidator.ReleaseValidatorResult.IntermediateReleaseAlreadyLinked))
                {
                    MultiValidatorDefault.AddError(Translate("IntermediateReleaseAlreadyLinked", "The full installation for this version already has a intermediate update linked to it."));
                }
                else if (ex.ErrorEnumValue.Equals(ReleaseValidator.ReleaseValidatorResult.VersionAlreadyExistsForApplication))
                {
                    MultiValidatorDefault.AddError(Translate("VersionAlreadyExistsForApplication", "A release with this version number already exists."));
                }

                Validate();
                success = false;
            }

            return success;
        }

        private ApplicationEntity GetSelectedApplicationEntity()
        {
            if (ddlApplicationId.Value == null)
            { return null; }

            ApplicationCollection applications = new ApplicationCollection();
            applications.GetMulti(new PredicateExpression(ApplicationFields.ApplicationId == ddlApplicationId.Value));
            return applications.FirstOrDefault();
        }

        private bool SelectedApplicationExists()
        {
            if (ddlApplicationId.Value == null)
            { return false; }

            ApplicationCollection applications = new ApplicationCollection();
            return applications.GetDbCount(new PredicateExpression(ApplicationFields.ApplicationId == ddlApplicationId.Value)) > 0;
        }
        #endregion

        #region Properties

        public ReleaseEntity DataSourceAsReleaseEntity => DataSource as ReleaseEntity;

        #endregion
    }
}
