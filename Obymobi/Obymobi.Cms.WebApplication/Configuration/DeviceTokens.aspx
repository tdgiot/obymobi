﻿<%@ Page Title="Device tokens" Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Configuration.DeviceTokens" CodeBehind="DeviceTokens.aspx.cs" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<asp:Content ContentPlaceHolderID="cplhStylesheets" runat="server">
    <link rel="Stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.css" />
    <link rel="Stylesheet" type="text/css" href="../Css/DeviceTokens/device-tokens.css" />
    <script type="text/javascript">
        function OnEndCallback(s, e) {
            devicesGrid.UnselectAllRowsOnPage();
            devicesGrid.SelectRowOnPage(e.visibleIndex);

            setNotes("")
            if (e.buttonID === 'renewToken') {
                setEventSender(2);
                setTokenValuesOnPopup(true);
            } else if (e.buttonID === 'revokeToken') {
                setEventSender(3);
                setTokenValuesOnPopup(false);
            }
            showPopup();
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="cplhToolbarHolder"/>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhContentHolder" runat="Server">
    <div>
        <X:PageControl ID="tabsMain" runat="server" Width="100%">
            <TabPages>
                <X:TabPage Text="Dashboard" Name="Dashboard" LocalizeText="false">
                    <controls>
                        <D:Panel ID="pnlDeviceTokenStatuses" runat="server" GroupingText="Current">
                            <div class="full-div">
                                <div class="div-side">
                                    <canvas class="chart-js-canvas" id="sandboxed-devices-chart"></canvas>
                                </div>

                                <div class="div-side">
                                    <canvas class="chart-js-canvas" id="running-tasks-chart"></canvas>
                                </div>
                            </div>
                        </D:Panel>

                        <D:Panel ID="pnlDeviceTokensHistories" runat="server" GroupingText="History">
                            <div class="full-div">
                                <div class="div-side">
                                    <canvas class="chart-js-canvas" id="renewed-devices-chart"></canvas>
                                </div>

                                <div class="div-side">
                                    <canvas class="chart-js-canvas" id="revoked-devices-chart"></canvas>
                                </div>
                            </div>
                        </D:Panel>
                    </controls>
                </X:TabPage>

                <X:TabPage Text="Devices" Name="Tokens" LocalizeText="false">
                    <controls>
                        <div style="height: 32px;">
                            <X:ComboBoxInt runat="server" ID="cbCompany" Style="float: left; width: 200px;" TextField="Name" ValueField="CompanyId" IncrementalFilteringMode="Contains" IncrementalFilteringDelay="1000" AutoPostBack="True" DisplayEmptyItem="True" EmptyItemText="All companies" LocalizeEmptyItemText="False" />
                            <X:ComboBoxInt runat="server" ID="cbDeviceFilter" Style="float: left; width: 200px; margin-left: 7px" UseDataBinding="True" DisplayEmptyItem="False" AutoPostBack="True"/>
                            <D:Button runat="server" ID="btnRenewSelectedDeviceTokens" Style="float: left; height: 25px; margin-left: 7px" Text="Renew selected tokens" LocalizeText="false" OnClientClick="renewSelectedDeviceTokens(); return false;" />
                            <D:Button runat="server" ID="btnRevokeSelectedDeviceTokens" Style="float: left; height: 25px; margin-left: 7px" Text="Revoke selected tokens" LocalizeText="false" OnClientClick="revokeSelectedDeviceTokens(); return false;" />
                        </div>

                        <X:GridView ID="devicesGrid" ClientInstanceName="devicesGrid" runat="server" Width="100%" KeyFieldName="DeviceId">
                            <SettingsBehavior AutoFilterRowInputDelay="350" /> 
                            <SettingsPager PageSize="100"></SettingsPager>
                            <SettingsBehavior AllowGroup="false" AllowDragDrop="false" />
                            <ClientSideEvents CustomButtonClick="OnEndCallback" />
                            <Columns>
                                <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="60" SelectAllCheckboxMode="AllPages">                                    
                                    <HeaderTemplate>
                                        <input type="checkbox" onclick="devicesGrid.SelectRows()" title="Select/Deselect all rows on the page" />
                                    </HeaderTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                </dxwgv:GridViewCommandColumn>
                                <dxwgv:GridViewDataHyperLinkColumn FieldName="DeviceId" VisibleIndex="1" SortIndex="0" SortOrder="Ascending">
                                    <Settings AutoFilterCondition="Contains" FilterMode="DisplayText" />
                                    <PropertiesHyperLinkEdit NavigateUrlFormatString="~/Configuration/Device.aspx?id={0}" Target="_blank" TextField="Identifier"  />
                                </dxwgv:GridViewDataHyperLinkColumn>
                                <dxwgv:GridViewDataColumn FieldName="Token" VisibleIndex="2" >
								    <Settings AutoFilterCondition="Contains" />
                                </dxwgv:GridViewDataColumn>
                                <dxwgv:GridViewDataColumn FieldName="State" VisibleIndex="3" >
                                    <Settings AutoFilterCondition="Contains" />
                                </dxwgv:GridViewDataColumn>
                                <dxwgv:GridViewCommandColumn ButtonType="Image" VisibleIndex="5" Width="75">
								    <HeaderTemplate>Renew</HeaderTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <CustomButtons>
                                        <dxwgv:GridViewCommandColumnCustomButton ID="renewToken" Text="Renew token">
                                            <Image Url="~/Images/Icons/arrow_refresh.png"></Image>
                                        </dxwgv:GridViewCommandColumnCustomButton>
                                    </CustomButtons>
                                </dxwgv:GridViewCommandColumn>
                                <dxwgv:GridViewCommandColumn ButtonType="Image" VisibleIndex="5" Width="75">
								    <HeaderTemplate>Revoke</HeaderTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <CustomButtons>
                                        <dxwgv:GridViewCommandColumnCustomButton ID="revokeToken" Text="Revoke token">
                                            <Image Url="~/Images/Icons/delete.png"></Image>
                                        </dxwgv:GridViewCommandColumnCustomButton>
                                    </CustomButtons>
                                </dxwgv:GridViewCommandColumn>
                            </Columns>
                            <Settings ShowFilterRow="True" />
                        </X:GridView>
                    </controls>
                </X:TabPage>
            </TabPages>
        </X:PageControl>
    </div>
    <dx:ASPxPopupControl ID="pcPopup" runat="server" Width="460" CloseAction="CloseButton" CloseOnEscape="true" Modal="True" ShowOnPageLoad="False"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" ClientInstanceName="pcPopup"
        HeaderText="Revoke/renew device token" AllowDragging="False" PopupAnimationType="None" EnableViewState="False" AutoUpdatePosition="true">
        <ContentCollection>
            <dx:PopupControlContentControl runat="server">
                <dx:ASPxPanel ID="ASPxPanel1" runat="server" DefaultButton="btOK">
                    <PanelCollection>
                        <dx:PanelContent runat="server">
                            <table class="dataformV2" width="460px">
                                <tr>
                                    <td class="label">
                                        <D:Label runat="server" ID="lblNotes">Notes</D:Label>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxMultiLine runat="server" ID="tbNotes" ClientIDMode="Static" IsRequired="True" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        <X:ComboBoxInt runat="server" ID="cbEventSender" ClientInstanceName="cbEventSender" ClientIDMode="Static" Style="float: left; width: 200px;" UseDataBinding="True" DisplayEmptyItem="False" ClientVisible="False"  />
                                    </td>
                                    <td class="control" style="float: right; text-align: right">
                                        <D:Button runat="server" ID="btnCreateDeviceTokenTask" ClientIDMode="Static" Text="Create" LocalizeText="False" OnClientClick="pcPopup.Hide();" />
                                    </td>
                                </tr>
                            </table>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxPanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <ContentStyle>
            <Paddings PaddingBottom="5px" />
        </ContentStyle>
    </dx:ASPxPopupControl>
</asp:Content>

<asp:Content ContentPlaceHolderID="cplhScripts" runat="server">
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.bundle.min.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="../Js/DeviceTokens/device-tokens.js"></script>
</asp:Content>
