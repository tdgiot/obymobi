<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Configuration.Customers.Customer" Title="Klant" Codebehind="Customer.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<X:PageControl Id="tabsMain" runat="server" Width="100%">
	<TabPages>
		<X:TabPage Text="Algemeen" Name="Generic">
			<Controls>
				<table class="dataformV2">
					<tr>
						<td class="label">
							<D:LabelEntityFieldInfo runat="server" id="lblFirstname">Voornaam</D:LabelEntityFieldInfo>
						</td>
						<td class="control">
							<D:TextBoxString ID="tbFirstname" runat="server"></D:TextBoxString>
						</td>
						<td class="label">
							<D:LabelEntityFieldInfo runat="server" id="lblLastname">Achternaam</D:LabelEntityFieldInfo>
						</td>
						<td class="control">
						    <D:TextBoxString ID="tbLastname" runat="server"></D:TextBoxString>
						</td>        
					</tr> 
					<tr>
                        <td class="label">
							<D:LabelEntityFieldInfo runat="server" id="lblEmail">Email</D:LabelEntityFieldInfo>
						</td>
						<td class="control">
						    <D:TextBoxString ID="tbEmail" runat="server" autocomplete="off" IsRequired="true"></D:TextBoxString>
						</td>        
						<td class="label">
							<D:LabelEntityFieldInfo runat="server" id="lblLastnamePrefix">Tussenvoegsel</D:LabelEntityFieldInfo>
						</td>
						<td class="control">
							<D:TextBoxString ID="tbLastnamePrefix" runat="server"></D:TextBoxString>
						</td>
					</tr> 					                    
					<tr>
						<td class="label">
							<D:LabelEntityFieldInfo runat="server" id="lblVerified">Bevestigd</D:LabelEntityFieldInfo>
						</td>
						<td class="control">
							<D:CheckBox runat="server" ID="cbVerified" />
						</td>
						<td class="label">
							<D:LabelEntityFieldInfo runat="server" id="lblBlacklisted">Blacklisted</D:LabelEntityFieldInfo>
						</td>
						<td class="control">
						    <D:CheckBox runat="server" ID="cbBlacklisted" />
						</td>        
					</tr>					 
                    <tr>
                        <td class="label">
                            <D:LabelEntityFieldInfo runat="server" id="lblAnonymousAccount">Anonieme customer</D:LabelEntityFieldInfo>
                        </td>
                        <td class="control">
                            <D:CheckBox runat="server" ID="cbAnonymousAccount" />
                        </td>
                    </tr>															 					 																				
				 </table>			
			</Controls>
		</X:TabPage>			
	</TabPages>
</X:PageControl>  
</asp:Content>

