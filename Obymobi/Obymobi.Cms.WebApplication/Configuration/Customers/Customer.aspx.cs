using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Dionysos;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Logic.Cms;

namespace Obymobi.ObymobiCms.Configuration.Customers
{
    public partial class Customer : Dionysos.Web.UI.PageLLBLGenEntity
    {
        #region Methods

        protected override void OnInit(EventArgs e)
        {
            this.LoadControls();
            this.DataSourceLoaded += new Dionysos.Delegates.Web.DataSourceLoadedHandler(Customer_DataSourceLoaded);
            base.OnInit(e);
        }

        private void LoadControls()
        {
            
        }

        private void Customer_DataSourceLoaded(object sender)
        {
            
        }

        #endregion

        #region Properties

        /// <summary>
		/// Return the page's datasource as a CustomerEntity
		/// </summary>
		public CustomerEntity DataSourceAsCustomerEntity
		{
			get
			{
				return this.DataSource as CustomerEntity;
			}
		}

        #endregion

        #region EventHandlers

        protected void Page_Load(object sender, EventArgs e)
        {
            // Redirect the user if it's not an administrator
            if (CmsSessionHelper.CurrentRole < Role.Administrator)
                Response.Redirect("~/401.aspx");
        }

        #endregion
    }
}
