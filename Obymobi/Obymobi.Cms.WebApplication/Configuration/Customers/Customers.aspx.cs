﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Cms;
using Dionysos.Web;
using System.Drawing;
using Obymobi.Enums;

namespace Obymobi.ObymobiCms.Configuration.Customers
{
    public partial class Customers : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "Customer";
            this.EntityPageUrl = "~/Configuration/Customers/Customer.aspx";
            base.OnInit(e);

            int showAnonymousCustomers;
            if (QueryStringHelper.TryGetValue("showanonymouscustomers", out showAnonymousCustomers))
            {
                if (showAnonymousCustomers == 1)
                    this.cbShowAnonymousCustomer.Checked = true;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (CmsSessionHelper.CurrentRole < Role.Administrator)
                Response.Redirect("~/401.aspx");

            LLBLGenProDataSource datasource = this.DataSource as LLBLGenProDataSource;
            if (datasource != null)
            {
                PredicateExpression filter = new PredicateExpression();
                filter.Add(CustomerFields.AnonymousAccount == this.cbShowAnonymousCustomer.Checked);

                datasource.FilterToUse = filter;
            }

            if (this.MainGridView != null)
            {
                this.MainGridView.HtmlRowPrepared += new DevExpress.Web.ASPxGridViewTableRowEventHandler(MainGridView_HtmlRowPrepared);
            }

            ((MasterPages.MasterPageEntityCollection)this.Master).ToolBar.AddButton.Visible = false;
        }

        private void MainGridView_HtmlRowPrepared(object sender, DevExpress.Web.ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType != DevExpress.Web.GridViewRowType.Data) return;
            bool isVerified = (bool)e.GetValue("Verified");
            if (!isVerified)
                e.Row.ForeColor = Color.Gray;
            else
                e.Row.ForeColor = Color.Green;            
        }

        public void Refresh()
        {
            QueryStringHelper qs = new QueryStringHelper();

            if (this.cbShowAnonymousCustomer.Checked)
                qs.AddItem("showanonymouscustomers", 1);
            else
                qs.AddItem("showanonymouscustomers", 0);

            WebShortcuts.Redirect(qs.MergeQuerystringWithRawUrl());
        }

        #endregion
    }
}
