﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Configuration.Application" Codebehind="Application.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblName">Name</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblCode">Code</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbCode" runat="server" IsRequired="true"></D:TextBoxString>
							</td>  
						</tr>
                        <tr>
                            <td class="label">
								<D:Label runat="server" id="lblDeviceModel">Device Model</D:Label>
							</td>
							<td class="control">
                                <X:ComboBoxEnum ID="ddlDeviceModel" runat="server" IsRequired="true" Type="Obymobi.Enums.DeviceModel, Obymobi" ></X:ComboBoxEnum>
							</td> 
                        </tr>
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblDescription">Description</D:LabelEntityFieldInfo>
							</td>						
							<td colspan="3" class="control">
								<D:TextBox ID="tbDescription" runat="server" Rows="5" TextMode="MultiLine" CssClass="inputNoHeight"></D:TextBox>                
							</td>
						</tr> 
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblReleaseId">Latest stable release</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlReleaseId" UseDataBinding="true" EntityName="Release" TextField="Version" IsRequired="false"></X:ComboBoxLLBLGenEntityCollection>            
							</td>
                            <td class="label"></td>
                            <td class="control"></td>
						</tr>                                                															
					 </table>					
				</Controls>
			</X:TabPage>											
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>

