﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Configuration.Release" CodeBehind="Release.aspx.cs" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" runat="Server">
    <script type="text/javascript">
        function onFileUploadComplete(s, e) {
            document.getElementById("ctl00_ctl00_cplhContentHolder_cplhPageContent_tabsMain_tbFilename").value = e.callbackData;
            document.getElementById("toolbarDiv").style.pointerEvents = "auto";
            document.getElementById("toolbarDiv").style.opacity = 1;
            document.getElementsByClassName("buttons")[0].title = "";
        }

        function onFileUploadStart(s, e) {
            document.getElementById("toolbarDiv").style.pointerEvents = "none";
            document.getElementById("toolbarDiv").style.opacity = 0.5;
            document.getElementsByClassName("buttons")[0].title = "Release actions disabled while file is uploading";
        }
    </script>
    <style>
        #ctl00_ctl00_cplhContentHolder_cplhPageContent_tabsMain_ucFile_Browse0 {
            width: 100%;
            text-align: center;
        }
    </style>
    <div>
        <X:PageControl ID="tabsMain" runat="server" Width="100%">
            <TabPages>
                <X:TabPage Text="Algemeen" Name="Generic">
                    <controls>
                        <table class="dataformV2">
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" ID="lblVersion">Version</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:TextBoxString ID="tbVersion" runat="server" IsRequired="true" AutoPostBack="true" notdirty="true">
                                    </D:TextBoxString>
                                </td>
                                <td class="label">
                                    <D:Label runat="server" ID="lblApplication">Applicatie</D:Label>
                                </td>
                                <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlApplicationId" UseDataBinding="true" EntityName="Application" IsRequired="true" AutoPostBack="true" notdirty="true">
                                    </X:ComboBoxLLBLGenEntityCollection>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" ID="lblComment">Readme</D:LabelEntityFieldInfo>
                                </td>
                                <td colspan="3" class="control">
                                    <D:TextBox ID="tbComment" runat="server" Rows="5" TextMode="MultiLine" CssClass="inputNoHeight" IsRequired="true" UseValidation="true" AutoPostBack="True" notdirty="true">
                                    </D:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:Label runat="server" ID="lblReleaseGroup">Release Group</D:Label>
                                </td>
                                <td class="control">
                                    <X:ComboBoxEnum runat="server" ID="cbReleaseGroup" UseDataBinding="true" Type="Obymobi.Enums.ReleaseGroup, Obymobi" IsRequired="true" AutoPostBack="true" notdirty="true">
                                    </X:ComboBoxEnum>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" ID="lblFilename">File</D:LabelEntityFieldInfo>
                                </td>
                                <td colspan="2" class="control">
                                    <D:TextBoxString ID="tbFilename" runat="server" ReadOnly="true"></D:TextBoxString>
                                </td>
                                <td class="control">
                                    <dx:ASPxUploadControl runat="server" ID="ucFile" ClientInstanceName="ucFile"
                                        ShowTextBox="false" BrowseButton-Text="Upload" NullText="Click here to browse files..."
                                        UploadMode="Advanced" FileUploadMode="OnPageLoad" ShowProgressPanel="True"
                                        OnFileUploadComplete="ucFile_FileUploadComplete" AutoStartUpload="true" Width="25%">
                                        <AdvancedModeSettings PacketSize="5242880" />
                                        <ClientSideEvents
                                            FileUploadComplete="onFileUploadComplete"
                                            FileUploadStart="onFileUploadStart" />
                                    </dx:ASPxUploadControl>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" ID="lblIntermediate">Intermediate update</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:CheckBox runat="server" ID="cbIntermediate" />
                                </td>
                            </tr>
                        </table>
                    </controls>
                </X:TabPage>
            </TabPages>
        </X:PageControl>
    </div>
</asp:Content>

