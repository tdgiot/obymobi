﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Obymobi.ObymobiCms.Configuration
{


    public partial class FeatureToggles
    {

        /// <summary>
        /// btSave control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Dionysos.Web.UI.DevExControls.ToolBarButton btSave;

        /// <summary>
        /// tabsMain control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Dionysos.Web.UI.DevExControls.PageControl tabsMain;

        /// <summary>
        /// plhFeatureToggles control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Dionysos.Web.UI.WebControls.PlaceHolder plhFeatureToggles;

        /// <summary>
        /// plhReleaseGroups control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Dionysos.Web.UI.WebControls.PlaceHolder plhReleaseGroups;
    }
}
