using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.Injectables.Validators;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Web.UI.WebControls;
using Obymobi.Enums;

namespace Obymobi.ObymobiCms.Configuration
{
    public partial class Supportagent : Dionysos.Web.UI.PageLLBLGenEntity
    {
        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        #endregion

        #region Properties

        public SupportagentEntity DataSourceAsSupportagentEntity
        {
            get
            {
                return this.DataSource as SupportagentEntity;
            }
        }

        #endregion
    }
}