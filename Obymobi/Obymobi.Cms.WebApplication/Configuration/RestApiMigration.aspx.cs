﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dionysos;
using Dionysos.Data.LLBLGen;
using Dionysos.Security.Cryptography;
using Dionysos.Web.UI;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Comet;
using Obymobi.Logic.Comet.Netmessages;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Configuration
{
    public partial class RestApiMigration : PageDefault
    {
        private const string MINIMAL_APPLICATION_VERSION = "2022071501";

        private const int COMPANY_ARIA = 343;
        private const int COMPANY_ARIA_2021 = 975;

        private const int COMPANY_ARIA_DPG_ARIA_2020 = 1410;
        private const int COMPANY_ARIA_DPG_ARIA_2020_EVENTS = 2155;
        private const int COMPANY_ARIA_DPG_ARIA_2020_SKY_SUITES = 1442;
        private const int COMPANY_ARIA_DPG_ARIA_2020_VILLAS = 1584;
        private const int COMPANY_ARIA_DPG_FRONT_DESK = 754;

        private const int COMPANY_ARIA_2021_DPG_ARIA_2021_NEW = 2007;
        private const int COMPANY_ARIA_2021_DPG_ARIA_2021_SPECIAL_EVENTS = 2123;
        private const int COMPANY_ARIA_2021_DPG_ARIA_SKY_SUITES_NEW = 1995;
        private const int COMPANY_ARIA_2021_DPG_ARIA_2021_VILLAS = 2009;
        private const int COMPANY_ARIA_2021_DPG_FRONT_DESK = 2093;

        public readonly static IDictionary<int, int> AriaDeliverypointgroupMapping = new Dictionary<int, int>()
        {
            { COMPANY_ARIA_DPG_ARIA_2020, COMPANY_ARIA_2021_DPG_ARIA_2021_NEW },
            { COMPANY_ARIA_DPG_ARIA_2020_EVENTS, COMPANY_ARIA_2021_DPG_ARIA_2021_SPECIAL_EVENTS },
            { COMPANY_ARIA_DPG_ARIA_2020_SKY_SUITES, COMPANY_ARIA_2021_DPG_ARIA_SKY_SUITES_NEW },
            { COMPANY_ARIA_DPG_ARIA_2020_VILLAS, COMPANY_ARIA_2021_DPG_ARIA_2021_VILLAS },
            { COMPANY_ARIA_DPG_FRONT_DESK, COMPANY_ARIA_2021_DPG_FRONT_DESK }
        };

        private void LoadUserControls()
        {
            DataBindGridviews();
        }

        private static ClientCollection FetchPendingClients(int companyId)
        {
            IPredicateExpression subFilter = new PredicateExpression();
            subFilter.Add(NetmessageFields.Status == NetmessageStatus.WaitingForVerification);
            subFilter.AddWithOr(NetmessageFields.Status == NetmessageStatus.Delivered);

            IPredicateExpression filter = new PredicateExpression();
            filter.Add(ClientFields.CompanyId == companyId);
            filter.Add(DeviceFields.LastRequestUTC >= DateTime.UtcNow.AddMinutes(-3));
            filter.Add(DeviceFields.ApplicationVersion >= MINIMAL_APPLICATION_VERSION);
            filter.Add(NetmessageFields.MessageType == NetmessageType.MigrateToRestApi);
            filter.Add(subFilter);

            IRelationCollection relations = new RelationCollection();
            relations.Add(ClientEntity.Relations.DeviceEntityUsingDeviceId);
            relations.Add(ClientEntity.Relations.DeliverypointEntityUsingDeliverypointId);
            relations.Add(ClientEntity.Relations.NetmessageEntityUsingReceiverClientId);

            IPrefetchPath prefetch = new PrefetchPath(EntityType.ClientEntity);
            prefetch.Add(ClientEntity.PrefetchPathReceivedNetmessageCollection, new IncludeFieldsList(NetmessageFields.MessageType, NetmessageFields.CreatedUTC));
            prefetch.Add(ClientEntity.PrefetchPathDeviceEntity, new IncludeFieldsList(DeviceFields.Identifier));
            prefetch.Add(ClientEntity.PrefetchPathDeliverypointEntity, new IncludeFieldsList(DeliverypointFields.Name))
                .SubPath.Add(DeliverypointEntity.PrefetchPathDeliverypointgroupEntity, new IncludeFieldsList(DeliverypointgroupFields.Name));

            ClientCollection clients = new ClientCollection();
            clients.GetMulti(filter, 0, null, relations, prefetch);

            return clients;
        }

        private static ClientCollection FetchClients(int companyId)
        {
            IPredicateExpression filter = new PredicateExpression();
            filter.Add(ClientFields.CompanyId == companyId);
            filter.Add(DeviceFields.LastRequestUTC >= DateTime.UtcNow.AddMinutes(-3));
            filter.Add(DeviceFields.ApplicationVersion >= MINIMAL_APPLICATION_VERSION);

            IRelationCollection relations = new RelationCollection();
            relations.Add(ClientEntity.Relations.DeviceEntityUsingDeviceId);
            relations.Add(ClientEntity.Relations.DeliverypointEntityUsingDeliverypointId);

            IPrefetchPath prefetch = new PrefetchPath(EntityType.ClientEntity);
            prefetch.Add(ClientEntity.PrefetchPathReceivedNetmessageCollection, new IncludeFieldsList(NetmessageFields.MessageType, NetmessageFields.CreatedUTC));
            prefetch.Add(ClientEntity.PrefetchPathDeviceEntity, new IncludeFieldsList(DeviceFields.Identifier));
            prefetch.Add(ClientEntity.PrefetchPathDeliverypointEntity, new IncludeFieldsList(DeliverypointFields.Name))
                .SubPath.Add(DeliverypointEntity.PrefetchPathDeliverypointgroupEntity, new IncludeFieldsList(DeliverypointgroupFields.Name));

            ClientCollection clients = new ClientCollection();
            clients.GetMulti(filter, 0, null, relations, prefetch);

            return clients;
        }

        private static DataTable ConvertToDataTable(ClientCollection clients)
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add(new DataColumn("ClientId"));
            dataTable.Columns.Add(new DataColumn("DeliverypointgroupId"));
            dataTable.Columns.Add(new DataColumn("DeliverypointName"));
            dataTable.Columns.Add(new DataColumn("DeliverypointgroupName"));
            dataTable.Columns.Add(new DataColumn("Elapsed"));

            foreach (ClientEntity client in clients)
            {
                NetmessageEntity migrateToRestApiNetmessage = client.ReceivedNetmessageCollection
                    .OrderByDescending(netmessage => netmessage.CreatedUTC)
                    .FirstOrDefault(netmessage => netmessage.MessageType == ((int)NetmessageType.MigrateToRestApi));

                DataRow row = dataTable.NewRow();
                row[0] = client.ClientId;
                row[1] = client.DeliverypointEntity.DeliverypointgroupId;
                row[2] = client.DeliverypointEntity.Name;
                row[3] = client.DeliverypointEntity.DeliverypointgroupEntity.Name;
                row[4] = migrateToRestApiNetmessage != null
                    ? DateTimeUtil.GetSpanTextBetweenDateTimeAndUtcNow(migrateToRestApiNetmessage.CreatedUTC.Value)
                    : string.Empty;

                dataTable.Rows.Add(row);
            }

            return dataTable;
        }

        public void Refresh()
        {
            DataBindGridviews();
        }

        private void DataBindGridviews()
        {
            ClientCollection pendingClients = FetchPendingClients(COMPANY_ARIA);
            ClientCollection ariaClients = FetchClients(COMPANY_ARIA);
            ClientCollection aria2021Clients = FetchClients(COMPANY_ARIA_2021);

            ariaClients = ariaClients
                .Where(client => !pendingClients.Select(x => x.ClientId).Contains(client.ClientId))
                .ToEntityCollection<ClientCollection>();

            lblClientsToMigrate.Text = $"ARIA: {ariaClients.Count}";
            lblClientsMigrating.Text = $"Pending: {pendingClients.Count}";
            lblClientsMigrated.Text = $"ARIA 2021: {aria2021Clients.Count}";

            gvClientsToMigrate.DataSource = ConvertToDataTable(ariaClients);
            gvClientsToMigrate.DataBind();

            gvClientsMigrating.DataSource = ConvertToDataTable(pendingClients);
            gvClientsMigrating.DataBind();

            gvClientsMigrated.DataSource = ConvertToDataTable(aria2021Clients);
            gvClientsMigrated.DataBind();
        }

        public void Migrate()
        {
            CompanyEntity company = new CompanyEntity(COMPANY_ARIA_2021);

            string companyOwnerUsername = company.CompanyOwnerEntity.Username;
            string companyOwnerPasswordEncrypted = Cryptographer.EncryptUsingCBC(company.CompanyOwnerEntity.DecryptedPassword);

            foreach (string clientId in gvClientsToMigrate.GetSelectedFieldValues("ClientId"))
            {
                string deliverypointgroupId = gvClientsToMigrate.GetRowValuesByKeyValue(clientId, "DeliverypointgroupId").ToString();

                if (!int.TryParse(clientId, out int parsedClientId))
                {
                    throw new InvalidOperationException($"Client id '${clientId}' is not in the expected format");
                }

                if (!int.TryParse(deliverypointgroupId, out int parsedDeliverypointgroupId))
                {
                    throw new InvalidOperationException($"Deliverypointgroup id '${deliverypointgroupId}' is not in the expected format");
                }

                if (!AriaDeliverypointgroupMapping.ContainsKey(parsedDeliverypointgroupId))
                {
                    throw new InvalidOperationException($"Deliverypointgroup '${deliverypointgroupId}' is not mapped to a DPG on the new company");
                }

                CometHelper.Instance.SendMessage(new NetmessageMigrateToRestApi
                {
                    ReceiverClientId = parsedClientId,
                    CompanyOwnerUsername = companyOwnerUsername,
                    CompanyOwnerPasswordEncrypted = companyOwnerPasswordEncrypted,
                    DeliverypointgroupId = AriaDeliverypointgroupMapping[parsedDeliverypointgroupId]
                });
            }

            DataBindGridviews();
        }

        protected override void OnInit(EventArgs e)
        {
            LoadUserControls();
            base.OnInit(e);
        }
    }
}