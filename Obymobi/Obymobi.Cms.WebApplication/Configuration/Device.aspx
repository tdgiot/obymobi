﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Configuration.Device" Codebehind="Device.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblIdentifier">Identifier</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbIdentifier" runat="server" IsRequired="true"></D:TextBoxString>
                                <br />
                                <D:LinkButton ID="lbGenerateIdentifier" runat="server" LocalizeText="false">Generate identifier</D:LinkButton>
							</td>
                            <td class="label">
								<D:Label runat="server" id="lblClient">Tablet</D:Label>
							</td>						
							<td class="control">
								<D:HyperLink runat="server" ID="hlClient" Visible="false" Text="Open tablet &raquo;"></D:HyperLink>
								<X:ComboBoxLLBLGenEntityCollection runat="server" ID="dllClientId" EntityName="Client" Visible="false" />
							</td>
						</tr>
                        <tr>
                            <td class="label">
								<D:Label runat="server" id="lblType">Type</D:Label>
							</td>
							<td class="control">
                                <X:ComboBoxEnum ID="ddlType" runat="server" IsRequired="true" Type="Obymobi.Enums.DeviceType, Obymobi" ></X:ComboBoxEnum>
							</td> 
                            <td class="label">
								<D:Label runat="server" id="lblDeviceModel">Device model</D:Label>
							</td>
							<td class="control">
                                <X:ComboBoxEnum ID="ddlDeviceModel" runat="server" IsRequired="true" Type="Obymobi.Enums.DeviceModel, Obymobi" ></X:ComboBoxEnum>
							</td> 
                        </tr>
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblSsid">SSID</D:LabelEntityFieldInfo>
							</td>						
							<td class="control">
								<D:Label runat="server" id="lblSsidValue" LocalizeText="false"></D:Label>
							</td>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblSandboxMode">Sandbox mode</D:LabelEntityFieldInfo>
                            </td>						
                            <td class="control">
                                <D:CheckBox runat="server" id="cbSandboxMode" UseDataBinding="true"></D:CheckBox>
                            </td>
						</tr>  
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblUpdateStatus">Update status</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
                                <X:ComboBoxEnum ID="ddlUpdateStatus" runat="server" IsRequired="true" Type="Obymobi.Enums.UpdateStatus, Obymobi" ></X:ComboBoxEnum>
							</td>
                            <td class="label">
                            </td>
                            <td class="control">
                            </td>  
                        </tr>                                              					
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblNotes" >Notes</D:LabelEntityFieldInfo>
							</td>
							<td class="control threeCol" colspan="3">
								<D:TextBoxMultiLine ID="tbNotes" runat="server" Rows="10" UseDataBinding="true" MaxLength="500"></D:TextBoxMultiLine>
							</td>
						</tr>                        					                        										
					 </table>
					<D:Panel ID="pnlMessagingService" runat="server" GroupingText="Messaging Service Stats">
                    <table class="dataformV2">
                        <tr>
                            <td class="label">Messaging Service Version</td>
                            <td class="control"><D:Label runat="server" id="lblLastMessagingServiceVersionValue" LocalizeText="false"></D:Label></td>
                            <td class="label">IoT Connecting (last 24h)</td>
                            <td class="control"><D:Label runat="server" ID="lblMessagingConnectingCount24h" LocalizeText="false"></D:Label></td> 
                        </tr>
                        <tr>
                            <td class="label">Last Messaging Service Request</td>
                            <td class="control"><D:Label runat="server" id="lblLastMessagingServiceRequestValue" LocalizeText="false"></D:Label></td>
                            <td class="label">IoT Connected (last 24h)</td>
                            <td class="control"><D:Label runat="server" ID="lblMessagingConnectedCount24h" LocalizeText="false"></D:Label></td> 
                        </tr>
                        <tr>
                            <td class="label">Last connection type</td>
                            <td class="control"><D:Label runat="server" ID="lblLastMessagingConnectionType" LocalizeText="false"></D:Label></td>
                            <td class="label">IoT Reconnecting (last 24h)</td>
                            <td class="control"><D:Label runat="server" ID="lblMessagingReconnectingCount24h" LocalizeText="false"></D:Label></td> 
				        </tr>
                        <tr>
                            <td class="label">Last IoT connection status</td>
                            <td class="control"><D:Label runat="server" ID="lblLastMessagingConnectionStatus" LocalizeText="false"></D:Label></td>
                            <td class="label">IoT Connection Lost (last 24h)</td>
                            <td class="control"><D:Label runat="server" ID="lblMessagingConnectionLostCount24h" LocalizeText="false"></D:Label></td> 
                        </tr>
                        <tr>
                            <td class="label">Last successful IoT connection</td>
                            <td class="control"><D:Label runat="server" ID="lblLastMessagingSuccessfulConnection" LocalizeText="false"></D:Label></td>
                            <td class="label"></td>
                            <td class="control"></td>
                        </tr>
                    </table>
				</D:Panel>
				</Controls>
			</X:TabPage>
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>

