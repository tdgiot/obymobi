﻿using System;
using Dionysos.Web.UI;

namespace Obymobi.ObymobiCms.Configuration.Sys
{
    public partial class NotificationProviders : PageDefault
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void OnInit(EventArgs e)
        {
            // Azure Notification Hubs Panel
            this.plhAzureHubs.Controls.Add(this.LoadControl("~/Configuration/SubPanels/NotificationProviders/AzureHubs.ascx"));
        }
    }
}