﻿<%@ Page Title="Cloud Tasks Queue" Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Configuration.Sys.NotificationProviders" Codebehind="NotificationProviders.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitleHolder" Runat="Server"><D:Label runat="server" id="lblTitle">Notification Providers</D:Label></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhToolbarHolder" Runat="Server"></asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhContentHolder" Runat="Server">
<div>
	<X:PageControl Id="tabsProviders" runat="server" Width="100%">
	<TabPages>
        <X:TabPage Text="Azure Hubs" Name="AzureHubs">
			<Controls>
				<D:PlaceHolder runat="server" ID="plhAzureHubs">
                </D:PlaceHolder>
            </Controls>
        </X:TabPage>
    </TabPages>
   </X:PageControl>
</div>
</asp:Content>