﻿<%@ Page Title="Cloud Tasks Queue" Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Configuration.Sys.CloudTaskQueue" Codebehind="CloudTaskQueue.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitleHolder" Runat="Server"><D:Label runat="server" id="lblTitle">Cloud Task Queue</D:Label></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhToolbarHolder" Runat="Server">
<span class="toolbar">
    <X:ToolBarButton runat="server" ID="btChangeFilter" CommandName="Refresh" Text="Update Filter" Image-Url="~/Images/Icons/table_refresh.png" />
    <X:ToolBarButton runat="server" ID="btRefresh" CommandName="Refresh" Text="Refresh" Image-Url="~/Images/Icons/arrow_refresh.png" />
</span>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhContentHolder" Runat="Server">
<div>
	<X:PageControl Id="tabsTasks" runat="server" Width="100%">
	<TabPages>
        <X:TabPage Text="Tasks" Name="Tasks">
			<Controls>
				<D:PlaceHolder runat="server" ID="plhFilter">
				    <table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelTextOnly runat="server" id="lblType">Type</D:LabelTextOnly>
							</td>
							<td class="control">
								<D:CheckBoxList runat="server" ID="cblType">
								    <asp:ListItem Text="Entertainment" Value="1" Selected="True" />
								</D:CheckBoxList>
							</td>
                            <td class="label">
                                <D:LabelTextOnly runat="server" id="lblAction">Action</D:LabelTextOnly>
							</td>
							<td class="control">
							    <D:CheckBoxList runat="server" ID="cblAction">
								    <asp:ListItem Text="Upload" Value="1" Selected="True" />
                                    <asp:ListItem Text="Delete" Value="2" Selected="True" />
								</D:CheckBoxList>
							</td>  
						</tr>
                        <tr>
                            <td class="label">
								
							</td>
							<td class="control">
								<X:ComboBoxInt ID="ddlFilter" runat="server" UseDataBinding="true"></X:ComboBoxInt>
							</td>
                            <td class="label">
							</td>
							<td class="control">
							</td>  
                        </tr>
					 </table>
                </D:PlaceHolder>
                
                <D:PlaceHolder runat="server" ID="plhTasks">
                    <X:GridView ID="taskGrid" ClientInstanceName="tasksGrid" runat="server" Width="100%">
                        <SettingsBehavior AutoFilterRowInputDelay="350" />                            
                        <SettingsPager PageSize="60"></SettingsPager>
                        <SettingsBehavior AllowGroup="false" AllowDragDrop="false" />
                        <Columns>
                            <dxwgv:GridViewDataDateColumn FieldName="Created" VisibleIndex="0" SortIndex="0"></dxwgv:GridViewDataDateColumn>
                            <dxwgv:GridViewDataHyperLinkColumn FieldName="TypeUrl" Caption="Type" VisibleIndex="1">
                                <PropertiesHyperLinkEdit TextField="Type" Target="_blank" />
                            </dxwgv:GridViewDataHyperLinkColumn> 
                            <dxwgv:GridViewDataTextColumn FieldName="Filename" VisibleIndex="2"></dxwgv:GridViewDataTextColumn> 
                            <dxwgv:GridViewDataTextColumn FieldName="Action" VisibleIndex="3"></dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataHyperLinkColumn FieldName="AmazonUrl" Caption="Amazon" VisibleIndex="5">
                                <PropertiesHyperLinkEdit ImageUrlField="Amazon" Target="_blank" />
                            </dxwgv:GridViewDataHyperLinkColumn>
                            <dxwgv:GridViewDataTextColumn FieldName="Attempts" VisibleIndex="6"></dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataDateColumn FieldName="NextAttempt" VisibleIndex="7"></dxwgv:GridViewDataDateColumn>
                        </Columns>
                    </X:GridView>
                </D:PlaceHolder>
            </Controls>
        </X:TabPage>
        
        <X:TabPage Text="Log" Name="Log" Visible="false">
			<Controls>
				<table class="dataformV2">
					<tr>
						<td class="label">
							<D:LabelTextOnly runat="server" id="lblLogLines">Log Lines</D:LabelTextOnly>
						</td>
						<td class="control">
							<D:TextBoxInt runat="server" ID="tbLogLines" IntValue="200" />
						</td>
                        <td class="label">
                            
						</td>
						<td class="control">
							
						</td>  
					</tr>
                    <tr>
						<td class="control" colspan="4">
							<div class="editable" id="taTerminal" runat="server" />
						</td>
                    </tr>
					</table>			    
            </Controls>
        </X:TabPage>
    </TabPages>
   </X:PageControl>
</div>
<style>
div.editable {
    width: 99%;
    height: 500px;
    border: 1px solid #ccc;
	background-color: #fff;
    padding: 5px;
    font-family: consolas;
    font-size: 10pt;
    overflow: auto;
}​
</style>
</asp:Content>