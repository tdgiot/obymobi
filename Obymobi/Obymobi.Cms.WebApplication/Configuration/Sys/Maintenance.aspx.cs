﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI.WebControls;
using Crave.Domain.UseCases;
using Crave.Domain.UseCases.Requests;
using Crave.Domain.UseCases.Responses;
using DevExpress.Data.PLinq.Helpers;
using DevExpress.XtraScheduler.Drawing;
using Dionysos;
using Dionysos.Data;
using Dionysos.Data.LLBLGen;
using Dionysos.Interfaces.Data;
using Dionysos.Security.Cryptography;
using Dionysos.Web.UI;
using Dionysos.Web.UI.WebControls;
using Google.Apis.Manual.Util;
using Newtonsoft.Json;
using Obymobi.Cms.Logic.Factories;
using Obymobi.Cms.Logic.HelperClasses;
using Obymobi.Cms.Logic.UseCases;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.Injectables.Authorizers;
using Obymobi.Data.Master;
using Obymobi.Enums;
using Obymobi.Logic.Cms;
using Obymobi.Logic.Comet;
using Obymobi.Logic.Comet.Netmessages;
using Obymobi.Logic.Extensions;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;
using Obymobi.Logic.Routing;
using Obymobi.Web.Analytics.Model.TransactionsAppLess;
using Obymobi.Web.Analytics.Requests;
using Obymobi.Web.CloudStorage;
using Resources;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.QuerySpec;
using Button = Dionysos.Web.UI.WebControls.Button;
using CacheHelper = Dionysos.Web.CacheHelper;
using EntityType = Obymobi.Data.EntityType;
using PageEntity = Obymobi.Data.EntityClasses.PageEntity;
using ReferentialConstraintCollection = Obymobi.Data.CollectionClasses.ReferentialConstraintCollection;

namespace Obymobi.ObymobiCms.Configuration.Sys
{
    public partial class Maintenance : PageDefault
    {
        const int removeAfterDaysOfInactivity = 60;

        #region Event Handlers

        /// <summary>
        ///     Handles the Init event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">
        ///     The <see cref="System.EventArgs" /> instance containing the event data.
        /// </param>
        protected void Page_Init(object sender, EventArgs e)
        {
            tabsMain.GetTabPageByName("Master").Visible = Request.IsLocal;
            gvCache.Visible = false;
            SetGui();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.HookUpEvents();
            this.DisplayUnprocessableOrderCount();
        }

        private void HookUpEvents()
        {
            this.btnGetEmenusRunningWebserviceMode.Click += BtnGetEmenusRunningWebserviceModeOnClick;
            this.btnSwitchEmenuToWebservice.Click += BtnSwitchEmenusToWebserviceOnClick;
            this.btnSwitchEmenuToSignalR.Click += BtnSwitchEmenusToSignalROnClick;
            this.btnSwitchEmenusToWebserviceTest.Click += BtnSwitchEmenusToWebserviceTestOnClick;
            this.btnSwitchEmenusToSignalRTest.Click += BtnSwitchEmenusToSignalRTestOnClick;

            this.btnGetSupportToolsRunningWebserviceMode.Click += BtnGetSupportToolsRunningWebserviceModeOnClick;
            this.btnSwitchSupportToolsToSignalR.Click += BtnSwitchSupportToolsToSignalROnClick;
            this.btnSwitchSupportToolsToSignalRTest.Click += BtnSwitchSupportToolsToSignalRTestOnClick;

            btShowCache.Click += btShowCache_Click;
            btnCopyMediaRatioTypeMedia.Click += btnCopyMediaRatioTypeMedia_Click;
            btnCopyAllMediaRatioTypeMedia.Click += btnCopyAllMediaRatioTypeMedia_Click;

            this.btnConvertCreatedUpdatedToUtc.Click += btnConvertCreatedUpdatedToUtc_Click;
            this.btnConvertDateTimesToUtc.Click += btnConvertDateTimesToUtc_Click;

            this.btnConvertOrderHours.Click += btnConvertOrderHours_Click;
            this.btnConvertContactPagesToWebLinkPages.Click += btnConvertContactPagesToWebLinkPages_Click;
            this.btConvertMenuImages.Click += btConvertMenuImages_Click;
            this.btnGenerateCategorySuggestionsOverview.Click += btnGenerateCategorySuggestionsOverview_Click;
            this.btnLinkCategorySuggestionsToProductCategories.Click += btnLinkCategorySuggestionsToProductCategories_Click;
            this.btnEncryptCompanyOwnerPasswords.Click += btnEncryptCompanyOwnerPasswords_Click;
            this.btnConvertProductSuggestions.Click += btnConvertProductSuggestions_Click;
            this.btnPrepareForAmazon.Click += btnPrepareForAmazon_Click;
            this.btnSetDefaultAlterationDialogColors.Click += btnSetDefaultAlterationDialogColors_Click;
            this.btnSetRoomControllerTypes.Click += btnSetRoomControllerTypes_Click;
            this.btnClearEmptyPincodes.Click += btnClearEmptyPincodes_Click;

            this.btnLinkRoomControlAreasToClients.Click += btnLinkRoomControlAreasToClients_Click;
            this.btnLinkRoomControlAreasToClientsTest.Click += btnLinkRoomControlAreasToClientsTest_Click;

            this.btnDownloadUpdateDevicesLive.Click += btnDownloadUpdateDevicesLive_Click;
            this.btnDownloadUpdateDevicesLiveTest.Click += btnDownloadUpdateDevicesLiveTest_Click;
            this.btnUpdateDevicesLive.Click += btnUpdateDevicesLive_Click;
            this.btnRestartSucceededLive.Click += btnRestartSucceededLive_Click;
            this.btnUpdateDevicesLiveTest.Click += btnUpdateDevicesLiveTest_Click;
            this.btnRestartSucceededLiveTest.Click += btnRestartSucceededLiveTest_Click;

            this.btnSupportToolsReboot.Click += btnSupportToolsReboot_Click;
            this.btnSupportToolsRebootTest.Click += btnSupportToolsRebootTest_Click;

            this.btnRestartEmenu.Click += btnRestartEmenu_Click;
            this.btnRestartEmenuTest.Click += btnRestartEmenuTest_Click;
            btnRestartEmenuNew.Click += btnRestartEmenuNew_Click;
            btnRestartEmenuNewTest.Click += btnRestartEmenuNewTest_Click;

            this.btnRebootFloor.Click += btnRebootFloor_Click;
            this.btnREbootFloorTest.Click += btnREbootFloorTest_Click;
            this.btnRebootFloorNew.Click += btnRebootFloorNew_Click;
            this.btnRebootFloorNewTest.Click += btnRebootFloorNewTest_Click;

            this.btnChangeDpg.Click += btnChangeDpg_Click;
            this.btnChangeDpgTest.Click += btnChangeDpgTest_Click;

            this.btnCleanStorage.Click += btnCleanStorage_Click;
            this.btnCleanStorageTest.Click += btnCleanStorageTest_Click;

            this.btnSetLoadedSuccessfully.Click += btnSetLoadedSuccessfully_Click;

            this.btnRestartUnloadedClients.Click += btnRestartUnloadedClients_Click;
            this.btnRestartUnloadedClientsTest.Click += btnRestartUnloadedClientsTest_Click;

            this.btnCopyValuesToCustomTextEntities.Click += btnCopyValuesToCustomTextEntities_Click;
            this.btnCopyValuesToCustomTextEntitiesTest.Click += btnCopyValuesToCustomTextEntitiesTest_Click;

            this.btnSetVisibilityType.Click += btnSetVisibilityType_Click;

            this.btnCreateDefaultCompanyCurrencies.Click += btnCreateDefaultCompanyCurrencies_Click;
            this.btnSetCustomTextParentCompanyId.Click += btnSetCustomTextParentCompanyId_Click;

            this.btnCustomTextCleanupDelete.Click += btnCustomTextCleanup_Click;
            this.btnConvertTimeZones.Click += BtnConvertTimeZones_Click;

            this.btnCreateDefaultCustomTextsForRoomControl.Click += BtnCreateDefaultCustomTextsForRoomControl_Click;
            this.btnCreateDefaultCustomTextsForSites.Click += BtnCreateDefaultCustomTextsForSites_Click;
            this.btnCreateDefaultCustomTextsForMenu.Click += BtnCreateDefaultCustomTextsForMenu_Click;
            this.btnCreateDefaultCustomTextsForGeneric.Click += BtnCreateDefaultCustomTextsForGeneric_Click;
            this.btnCreateDefaultCustomTextsForCompany.Click += BtnCreateDefaultCustomTextsForCompany_Click;
            this.btnCreateDefaultCustomTextsTest.Click += BtnCreateDefaultCustomTextsTest_Click;

            this.btnConvertUIThemes.Click += btnConvertUIThemes_Click;

            this.btnSetMediaRelatedCompanyId.Click += btnSetMediaRelatedCompanyId_Click;

            this.btnConvertDeliverypointgroupToClientConfiguration.Click += btnConvertDeliverypointgroupToClientConfiguration_Click;

            this.btnSetTerminalType.Click += btnSetTerminalType_Click;

            this.btnConvertTerminalToTerminalConfiguration.Click += btnConvertTerminalToTerminalConfiguration_Click;

            this.btnSetCultureCodesForCustomTextsWithout.Click += BtnSetCultureCodesForCustomTextsWithout_Click;
            this.btnSetCultureCodesForCustomTextsWithoutTest.Click += BtnSetCultureCodesForCustomTextsWithoutTest_Click;

            this.btnFixFormattingIssueForItalicAndBold.Click += BtnFixFormattingIssueForItalicAndBold_Click;
            this.btnFixFormattingIssueForItalicAndBoldTest.Click += BtnFixFormattingIssueForItalicAndBoldTest_Click;

            this.btnReuploadMediaForUnknownDeviceType.Click += btnReuploadMediaForUnknownDeviceType_Click;
            this.btnReuploadMediaForUnknownDeviceTypeTest.Click += btnReuploadMediaForUnknownDeviceTypeTest_Click;

            this.btnCopySiteImages.Click += btnCopySiteImages_Click;

            this.btnDecodeSites.Click += BtnDecodeSites_Click;

            this.btnSyncCountryCodes.Click += btnSyncCountryCodes_Click;
            this.btnUploadDeliveryTime.Click += BtnUploadDeliveryTimeOnClick;

            this.btnNewSiteImagesCutouts.Click += BtnNewSiteImagesCutoutsOnClick;
            this.btnNewSiteImagesCutoutsTest.Click += BtnNewSiteImagesCutoutsTestOnClick;

            this.btnSetOverrideSubTypeOnProducts.Click += BtnSetOverrideSubTypeOnProductsOnClick;
            this.btnSetCompanyNamesInIrConfigurations.Click += BtnSetCompanyNamesInIrConfigurationsOnClick;

            this.btnConvertGenericBrandProducts.Click += btnConvertGenericBrandProducts_Click;
            this.btnConvertGenericBrandProductsMedia.Click += btnConvertGenericBrandProductsMedia_Click;
            this.btnFixLeBonne.Click += BtnFixLeBonne_Click;

            this.btnSetUtcFieldsOnOccurrenceEntities.Click += BtnSetUtcFieldsOnOccurrenceEntities_Click;
            this.btnCreatePmsReportConfigurations.Click += BtnCreatePmsReportConfigurations_Click;
            this.btnSetUtcFields.Click += BtnSetUtcFields_Click;

            this.btnCopyRoomControlInfo.Click += btnCopyRoomControlInfo_Click;
            this.btnCopyRoomControlInfoTest.Click += btnCopyRoomControlInfoTest_Click;

            this.btnSetAllowPublishingFlagsOnUsers.Click += btnSetAllowPublishingFlagsOnUsers_Click;

            this.btnClearClientConfigurations.Click += btnClearClientConfigurationsOnClick;
            this.btnClearTerminalConfigurations.Click += btnClearTerminalConfigurationsOnClick;

            this.btnConvertChannels.Click += BtnConvertChannelsOnClick;

            this.btnConvertBrandProducts.Click += BtnConvertBrandProductsOnClick;

            this.btnInitOutletOperationalState.Click += BtnInitOutletOperationalStateOnClick;

            this.btnFillReceiptTemplateEmail.Click += BtnFillReceiptTemplateEmailOnClick;
            this.btnGenerateOutletSystemProducts.Click += BtnGenerateOutletSystemProductsOnClick;
            this.btnGenerateOutletSystemProductsTest.Click += BtnGenerateOutletSystemProductsTestOnClick;

            this.btnRestoreOutOfSyncTippingSystemSubTypes.Click += BtnRestoreOutOfSyncTippingSystemSubTypesOnClick;
            this.btnRestoreOutOfSyncTippingSystemSubTypesTest.Click += BtnRestoreOutOfSyncTippingSystemSubTypesTestOnClick;

            this.btnSetMinimumAmountForFreeServiceCharge.Click += BtnSetMinimumAmountForFreeServiceChargeOnClick;
            this.btnSetMinimumAmountForFreeServiceChargeTest.Click += BtnSetMinimumAmountForFreeServiceChargeTestOnClick;
            this.btnGenerateDeliveryDistanceForDeliveryServiceMethods.Click += BtnGenerateDeliveryDistanceForDeliveryServiceMethodsOnClick;
            this.btnGenerateDeliveryDistanceForDeliveryServiceMethodsTest.Click += BtnGenerateDeliveryDistanceForDeliveryServiceMethodsTestOnClick;

            this.btnSetServiceMethodTypes.Click += BtnSetServiceMethodTypesOnClick;
            this.btnTestServiceMethodTypes.Click += BtnTestServiceMethodTypesOnClick;

            this.btnPaymentIntegrationConfigurationConversion.Click += BtnPaymentIntegrationConfigurationConversionOnClick;
            this.btnTestPaymentIntegrationConfigurationConversion.Click += BtnTestPaymentIntegrationConfigurationConversionOnClick;
            
        	this.btnSetMissingTranslations.Click += btnSetMissingTranslationsOnClick;

            this.btnConvertTermsAndConditionsCustomText.Click += btnConvertTermsAndConditionsCustomTextOnClick;
            this.btnTestConvertTermsAndConditionsCustomText.Click += btnTestConvertTermsAndConditionsCustomTextOnClick;

            this.btDeleteInactiveClientsOfCompany.Click += (sender, args) => CleanInactiveClients();
        }

        private void btnTestConvertTermsAndConditionsCustomTextOnClick(object sender, EventArgs e) => ConvertTermsAndConditionsCustomText(true);

        private void btnConvertTermsAndConditionsCustomTextOnClick(object sender, EventArgs e) => ConvertTermsAndConditionsCustomText();

        private void btnSetMissingTranslationsOnClick(object sender, EventArgs e) => this.SetMissingAppLessTranslations();
        
        private void BtnTestPaymentIntegrationConfigurationConversionOnClick(object sender, EventArgs e) => this.PaymentIntegrationConfigurationConversion(true);

        private void BtnPaymentIntegrationConfigurationConversionOnClick(object sender, EventArgs e) => this.PaymentIntegrationConfigurationConversion();

        private void BtnTestServiceMethodTypesOnClick(object sender, EventArgs e) => this.SetServiceMethodTypes(true);

        private void BtnSetServiceMethodTypesOnClick(object sender, EventArgs e) => this.SetServiceMethodTypes();

        private void BtnGenerateDeliveryDistanceForDeliveryServiceMethodsOnClick(object sender, EventArgs e)
        {
            this.GenerateDeliveryDistanceForDeliveryServiceMethods();
        }

        private void BtnGenerateDeliveryDistanceForDeliveryServiceMethodsTestOnClick(object sender, EventArgs e)
        {
            this.GenerateDeliveryDistanceForDeliveryServiceMethods(true);
        }

        private void BtnSetMinimumAmountForFreeServiceChargeOnClick(object sender, EventArgs e)
        {
            this.SetMinimumAmountForFreeServiceCharge();
        }

        private void BtnSetMinimumAmountForFreeServiceChargeTestOnClick(object sender, EventArgs e)
        {
            this.SetMinimumAmountForFreeServiceCharge(true);
        }

        private void BtnRestoreOutOfSyncTippingSystemSubTypesTestOnClick(object sender, EventArgs eventArgs) => this.RestoreAllSystemSubTypes(true);

        private void BtnRestoreOutOfSyncTippingSystemSubTypesOnClick(object sender, EventArgs eventArgs) => this.RestoreAllSystemSubTypes();

        private void BtnGenerateOutletSystemProductsOnClick(object sender, EventArgs eventArgs)
        {
            this.GenerateOutletSystemProducts();
        }

        private void BtnGenerateOutletSystemProductsTestOnClick(object sender, EventArgs eventArgs)
        {
            this.GenerateOutletSystemProducts(true);
        }

        private void BtnInitOutletOperationalStateOnClick(object sender, EventArgs e)
        {
            this.InitializeOutletOperationalStates();
        }

        private void BtnConvertBrandProductsOnClick(object sender, EventArgs e)
        {
            this.ConvertBrandProducts();
        }

        private void BtnConvertChannelsOnClick(object sender, EventArgs e)
        {
            this.ConvertChannels();
        }

        private void btnClearTerminalConfigurationsOnClick(object sender, EventArgs e)
        {
            this.ClearTerminalConfigurations();
        }

        private void btnClearClientConfigurationsOnClick(object sender, EventArgs e)
        {
            this.ClearClientConfigurations();
        }

        private void btnSetAllowPublishingFlagsOnUsers_Click(object sender, EventArgs e)
        {
            this.SetAllowPublishingFlagsOnUsers();
        }

        void btnCopyRoomControlInfoTest_Click(object sender, EventArgs e)
        {
            CopyRoomControlInfo(true);
        }

        void btnCopyRoomControlInfo_Click(object sender, EventArgs e)
        {
            CopyRoomControlInfo(false);
        }

        #region Button event handlers

        private void btnCopyAllMediaRatioTypeMedia_Click(object sender, EventArgs e)
        {
            CopyAllMediaRatioTypeMedia();
        }

        private void btnCopyMediaRatioTypeMedia_Click(object sender, EventArgs e)
        {
            //CopyMediaRatioTypeMedia();
        }

        protected void btShowCache_Click(object sender, EventArgs e)
        {
            DataTable table = new DataTable();

            List<string> keyList = new List<string>();
            IDictionaryEnumerator cacheEnumerator = HttpContext.Current.Cache.GetEnumerator();
            while (cacheEnumerator.MoveNext())
            {
                DataRow row = table.NewRow();
                row.AppendValueToRow("Key", cacheEnumerator.Key);
                row.AppendValueToRow("Value", (cacheEnumerator.Value != null ? cacheEnumerator.Value.ToString() : string.Empty).HtmlEncode());
                table.Rows.Add(row);
            }

            if (table.Rows.Count > 0)
            {
                gvCache.DataSource = table;
                gvCache.DataBind();
                gvCache.Visible = true;
            }
        }

        private void btDeleteCompany_Click(object sender, EventArgs e)
        {
            if (CmsSessionHelper.CurrentCompanyId <= 0)
            {
                MultiValidatorDefault.AddError(Translate("DeleteCompanyNoCompanySelected", "Er is geen bedrijf gekozen om te verwijderen."));
            }
            else if (CmsSessionHelper.CurrentRole < Role.GodMode)
            {
                MultiValidatorDefault.AddError(Translate("DeleteCompanyNotEnoughRights", "U heeft niet voldoende rechten om een bedrijf te verwijderen."));
            }
            else
            {
                CompanyEntity company = new CompanyEntity(CmsSessionHelper.CurrentCompanyId);
                string companyName = company.Name;
                company.OverruleDeletionPrevention = true;
                company.Delete();

                string info = Translate("DeleteCompanyCompleted", "Bedrijf '{0}' is verwijderd.");
                AddInformator(InformatorType.Information, info, companyName);
            }

            Validate();
        }

        private void btnConvertProductSuggestions_Click(object sender, EventArgs e)
        {
            this.ConvertProductSuggestions();
        }

        private void btnGenerateCategorySuggestionsOverview_Click(object sender, EventArgs e)
        {
            this.GenerateCategorySuggestionsOverview();
        }

        private void btnConvertContactPagesToWebLinkPages_Click(object sender, EventArgs e)
        {
            this.ConvertContactPagesToWebLinkPages();
        }

        private void btConvertMenuImages_Click(object sender, EventArgs e)
        {
            this.ConvertMenuImages();
        }

        private void btnLinkCategorySuggestionsToProductCategories_Click(object sender, EventArgs e)
        {
            this.LinkCategorySuggestionsToProductCategories();
        }

        private void btnConvertOrderHours_Click(object sender, EventArgs e)
        {
            // Get all of the order hours
            OrderHourCollection orderHourCollection = new OrderHourCollection();
            orderHourCollection.GetMulti(null);

            Dictionary<int, List<OrderHourEntity>> orderHoursPerProduct = new Dictionary<int, List<OrderHourEntity>>();
            Dictionary<int, List<OrderHourEntity>> orderHoursPerCategory = new Dictionary<int, List<OrderHourEntity>>();

            // Group the order hours per product and category
            foreach (OrderHourEntity orderHourEntity in orderHourCollection)
            {
                if (orderHourEntity.ProductId.HasValue)
                {
                    List<OrderHourEntity> orderHours = null;
                    if (orderHoursPerProduct.ContainsKey(orderHourEntity.ProductId.Value))
                    {
                        orderHours = orderHoursPerProduct[orderHourEntity.ProductId.Value];
                    }
                    else
                    {
                        orderHours = new List<OrderHourEntity>();
                        orderHoursPerProduct.Add(orderHourEntity.ProductId.Value, orderHours);
                    }
                    orderHours.Add(orderHourEntity);
                }
                else if (orderHourEntity.CategoryId.HasValue)
                {
                    List<OrderHourEntity> orderHours = null;
                    if (orderHoursPerCategory.ContainsKey(orderHourEntity.CategoryId.Value))
                    {
                        orderHours = orderHoursPerCategory[orderHourEntity.CategoryId.Value];
                    }
                    else
                    {
                        orderHours = new List<OrderHourEntity>();
                        orderHoursPerCategory.Add(orderHourEntity.CategoryId.Value, orderHours);
                    }
                    orderHours.Add(orderHourEntity);
                }
            }

            foreach (int productId in orderHoursPerProduct.Keys)
            {
                List<OrderHourEntity> orderHours = orderHoursPerProduct[productId];

                ScheduleEntity scheduleEntity = null;

                foreach (OrderHourEntity orderHourEntity in orderHours)
                {
                    if (!orderHourEntity.ProductEntity.CompanyId.HasValue)
                    {
                        continue;
                    }

                    if (scheduleEntity == null)
                    {
                        if (orderHourEntity.ProductEntity.ScheduleId.HasValue)
                        {
                            break;
                        }

                        scheduleEntity = new ScheduleEntity();
                        scheduleEntity.CompanyId = orderHourEntity.ProductEntity.CompanyId.Value;
                        scheduleEntity.Name = string.Format("Schedule for {0}", orderHourEntity.ProductEntity.Name);
                        if (scheduleEntity.Save())
                        {
                            orderHourEntity.ProductEntity.ScheduleId = scheduleEntity.ScheduleId;
                            orderHourEntity.ProductEntity.Save();
                        }
                    }

                    ScheduleitemEntity scheduleitemEntity = new ScheduleitemEntity();
                    scheduleitemEntity.ScheduleId = scheduleEntity.ScheduleId;
                    scheduleitemEntity.SortOrder = orderHourEntity.SortOrder;
                    if (orderHourEntity.DayOfWeek.HasValue)
                    {
                        scheduleitemEntity.DayOfWeek = (DayOfWeek)orderHourEntity.DayOfWeek.Value;
                    }
                    scheduleitemEntity.TimeStart = orderHourEntity.TimeStart;
                    scheduleitemEntity.TimeEnd = orderHourEntity.TimeEnd;
                    scheduleitemEntity.Save();
                }
            }

            foreach (int categoryId in orderHoursPerCategory.Keys)
            {
                List<OrderHourEntity> orderHours = orderHoursPerCategory[categoryId];

                ScheduleEntity scheduleEntity = null;

                foreach (OrderHourEntity orderHourEntity in orderHours)
                {
                    if (scheduleEntity == null)
                    {
                        if (orderHourEntity.CategoryEntity.ScheduleId.HasValue)
                        {
                            break;
                        }

                        scheduleEntity = new ScheduleEntity();
                        scheduleEntity.CompanyId = orderHourEntity.CategoryEntity.CompanyId;
                        scheduleEntity.Name = string.Format("Schedule for {0}", orderHourEntity.CategoryEntity.Name);
                        if (scheduleEntity.Save())
                        {
                            orderHourEntity.CategoryEntity.ScheduleId = scheduleEntity.ScheduleId;
                            orderHourEntity.CategoryEntity.Save();
                        }
                    }

                    ScheduleitemEntity scheduleitemEntity = new ScheduleitemEntity();
                    scheduleitemEntity.ScheduleId = scheduleEntity.ScheduleId;
                    scheduleitemEntity.SortOrder = orderHourEntity.SortOrder;
                    if (orderHourEntity.DayOfWeek.HasValue)
                    {
                        scheduleitemEntity.DayOfWeek = (DayOfWeek)orderHourEntity.DayOfWeek.Value;
                    }
                    scheduleitemEntity.TimeStart = orderHourEntity.TimeStart;
                    scheduleitemEntity.TimeEnd = orderHourEntity.TimeEnd;
                    scheduleitemEntity.Save();
                }
            }
        }

        private void btnEncryptCompanyOwnerPasswords_Click(object sender, EventArgs e)
        {
            this.EncryptCompanyOwnerPasswords();
        }

        private void btnClearEmptyPincodes_Click(object sender, EventArgs e)
        {
            // Deliverypointgroups
            PredicateExpression filter = new PredicateExpression(DeliverypointgroupFields.Pincode == "0");
            filter.AddWithOr(DeliverypointgroupFields.Pincode == "");
            DeliverypointgroupEntity entity = new DeliverypointgroupEntity {Pincode = "1258"};
            entity.Fields[(int)DeliverypointgroupFieldIndex.Pincode].IsChanged = true;
            new DeliverypointgroupCollection().UpdateMulti(entity, filter);

            filter = new PredicateExpression(DeliverypointgroupFields.PincodeSU == "0");
            filter.AddWithOr(DeliverypointgroupFields.PincodeSU == "");
            entity = new DeliverypointgroupEntity {PincodeSU = "1259"};
            entity.Fields[(int)DeliverypointgroupFieldIndex.PincodeSU].IsChanged = true;
            new DeliverypointgroupCollection().UpdateMulti(entity, filter);

            filter = new PredicateExpression(DeliverypointgroupFields.PincodeGM == "0");
            filter.AddWithOr(DeliverypointgroupFields.PincodeGM == "");
            entity = new DeliverypointgroupEntity {PincodeGM = "1260"};
            entity.Fields[(int)DeliverypointgroupFieldIndex.PincodeGM].IsChanged = true;
            new DeliverypointgroupCollection().UpdateMulti(entity, filter);

            // Company
            filter = new PredicateExpression(CompanyFields.Pincode == "0");
            filter.AddWithOr(CompanyFields.Pincode == "");
            CompanyEntity entity2 = new CompanyEntity {Pincode = "1258"};
            entity2.Fields[(int)CompanyFieldIndex.Pincode].IsChanged = true;
            new CompanyCollection().UpdateMulti(entity2, filter);

            filter = new PredicateExpression(CompanyFields.PincodeSU == "0");
            filter.AddWithOr(CompanyFields.PincodeSU == "");
            entity2 = new CompanyEntity {PincodeSU = "1259"};
            entity2.Fields[(int)CompanyFieldIndex.PincodeSU].IsChanged = true;
            new CompanyCollection().UpdateMulti(entity2, filter);

            filter = new PredicateExpression(CompanyFields.PincodeGM == "0");
            filter.AddWithOr(CompanyFields.PincodeGM == "");
            entity2 = new CompanyEntity {PincodeGM = "1260"};
            entity2.Fields[(int)CompanyFieldIndex.PincodeGM].IsChanged = true;
            new CompanyCollection().UpdateMulti(entity2, filter);

            AddInformator(InformatorType.Information, "Empty pincodes have been reset to default");
        }

        private void btnLinkRoomControlAreasToClients_Click(object sender, EventArgs e)
        {
            LinkRoomControlAreasToClients(false);
        }

        private void btnLinkRoomControlAreasToClientsTest_Click(object sender, EventArgs e)
        {
            LinkRoomControlAreasToClients(true);
        }

        private void LinkRoomControlAreasToClients(bool testMode)
        {
            int max = -1;
            int count = 0;

            int companyId;
            if (tbLinkRoomControlAreasCompanyId.Text.IsNullOrWhiteSpace() || !int.TryParse(tbLinkRoomControlAreasCompanyId.Text, out companyId))
            {
                lblLinkRoomControlAreas.Text = "Invalid company id";
                return;
            }

            PredicateExpression filter = new PredicateExpression();
            filter.Add(ClientFields.CompanyId == companyId);
            filter.Add(ClientFields.RoomControlAreaId == DBNull.Value);

            PrefetchPath prefetch = new PrefetchPath(EntityType.ClientEntity);
            prefetch.Add(ClientEntity.PrefetchPathDeliverypointEntity).SubPath.Add(DeliverypointEntity.PrefetchPathRoomControlConfigurationEntity).SubPath.Add(RoomControlConfigurationEntity.PrefetchPathRoomControlAreaCollection);

            ClientCollection clientCollection = new ClientCollection();
            clientCollection.GetMulti(filter, prefetch);

            string result = "";
            if (testMode)
            {
                result += "<b>Test run, no clients are affected</b><br/>";
            }

            foreach (ClientEntity clientEntity in clientCollection)
            {
                if (clientEntity.DeliverypointId != null && clientEntity.DeliverypointEntity.RoomControlConfigurationId != null)
                {
                    string deliverypointName = clientEntity.DeliverypointEntity.Name;
                    if (!deliverypointName.IsNullOrWhiteSpace())
                    {
                        string areaName;

                        if (deliverypointName.Contains(" BD")) // Board Room
                            areaName = "Board Room";
                        else if (deliverypointName.Contains(" BR")) // Bedroom Right
                            areaName = "Bedroom Right";
                        else if (deliverypointName.Contains(" BL")) // Bedroom Left
                            areaName = "Bedroom Left";
                        else if (deliverypointName.Contains(" L")) // Living, Living Room
                            areaName = "Living";
                        else if (deliverypointName.Contains(" B")) // Bedroom
                            areaName = "Bedroom";
                        else if (deliverypointName.Contains(" D")) // Dining
                            areaName = "Dining";
                        else // Bedroom
                            areaName = "Bedroom";

                        foreach (RoomControlAreaEntity roomControlAreaEntity in clientEntity.DeliverypointEntity.RoomControlConfigurationEntity.RoomControlAreaCollection)
                        {
                            if (roomControlAreaEntity.Name.StartsWith(areaName))
                            {
                                clientEntity.RoomControlAreaId = roomControlAreaEntity.RoomControlAreaId;
                                result += string.Format("Client: {0}, AreaId: {1}", clientEntity.ClientId, clientEntity.RoomControlAreaId);
                                if (!testMode)
                                {
                                    clientEntity.Save();
                                }
                                count++;
                                break;
                            }
                        }

                    }
                }

                if (max > 0 && count >= max)
                    break;
            }

            result = string.Format("Clients: {0}<br/>{1}", clientCollection.Count, result);

            this.lblLinkRoomControlAreas.Text = result;
        }

        #endregion

        #endregion

        private ClientCollection GetEmenusRunningInWebserviceMode(int amount = 0)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(DeviceFields.CommunicationMethod == ClientCommunicationMethod.Webservice);
            filter.Add(ClientFields.DeviceId > 0);
            filter.Add(DeviceFields.LastRequestUTC >= DateTime.UtcNow.AddMinutes(-3));

            if (this.cbSwitcherCompany.ValidId > 0)
            {
                filter.Add(ClientFields.CompanyId == this.cbSwitcherCompany.ValidId);
            }

            SortExpression sort = new SortExpression(new SortClause(ClientFields.ClientId, SortOperator.Ascending));
            RelationCollection relation = new RelationCollection(ClientEntity.Relations.DeviceEntityUsingDeviceId);

            ClientCollection clients = new ClientCollection();
            clients.GetMulti(filter, amount, sort, relation);

            return clients;
        }

        private ClientCollection GetSupportToolsRunningInWebserviceMode(int amount = 0)
        {
            PredicateExpression filter = new PredicateExpression();                        
            filter.Add(DeviceFields.LastRequestUTC >= DateTime.UtcNow.AddMinutes(-3));
            filter.Add(DeviceFields.LastSupportToolsRequestUTC < DateTime.UtcNow.AddMinutes(-3));
            filter.Add(ClientFields.DeviceId > 0);

            SortExpression sort = new SortExpression(new SortClause(ClientFields.ClientId, SortOperator.Ascending));

            RelationCollection relation = new RelationCollection(ClientEntity.Relations.DeviceEntityUsingDeviceId);

            ClientCollection clients = new ClientCollection();
            clients.GetMulti(filter, amount, sort, relation);

            return clients;
        }

        private int GetEmenuSwitcherAmount()
        {
            int amount;
            if (!int.TryParse(this.tbEmenuSwitcherAmount.Text, out amount))
            {
                amount = 50;
            }
            return amount;
        }

        private int GetSupportToolSwitcherAmount()
        {
            int amount;
            if (!int.TryParse(this.tbSupportToolsSwitcherAmount.Text, out amount))
            {
                amount = 50;
            }
            return amount;
        }

        private void RefreshEmenusRunningInWebserviceMode()
        {
            this.lblEmenuConnectionSwitcherResult.Text = string.Format("Total of {0} Emenus are running in webservice mode.", this.GetEmenusRunningInWebserviceMode().Count);
        }

        private void RefreshSupportToolsRunningInWebserviceMode()
        {
            this.lblSupportToolsConnectionSwitcherResult.Text = string.Format("Total of {0} SupportTools are running in webservice mode.", this.GetSupportToolsRunningInWebserviceMode().Count);
        }

        private void SwitchEmenusToWebserviceMode(bool test)
        {
            ClientCollection clients = this.GetEmenusRunningInWebserviceMode(this.GetEmenuSwitcherAmount());
            if (test)
            {
                string testString = "";
                foreach (ClientEntity client in clients)
                {
                    if (!testString.IsNullOrEmpty())
                    {
                        testString += ", ";
                    }
                    testString += client.ClientId;
                }                
                this.lblEmenuConnectionSwitcherResult.Text = string.Format("Clients: {0}", testString);
                return;
            }

            foreach (ClientEntity client in clients)
            {
                NetmessageSwitchCometHandler netmessage = new NetmessageSwitchCometHandler();
                netmessage.ReceiverClientId = client.ClientId;
                netmessage.Handler = ClientCommunicationMethod.Webservice;

                CometHelper.Instance.SendMessage(netmessage);
            }
        }

        private void SwitchEmenusToSignalR(bool test)
        {
            ClientCollection clients = this.GetEmenusRunningInWebserviceMode(this.GetEmenuSwitcherAmount());
            if (test)
            {
                string testString = "";
                foreach (ClientEntity client in clients)
                {
                    if (!testString.IsNullOrEmpty())
                    {
                        testString += ", ";
                    }
                    testString += client.ClientId;
                }
                this.lblEmenuConnectionSwitcherResult.Text = string.Format("Clients: {0}", testString);
                return;
            }

            foreach (ClientEntity client in clients)
            {
                NetmessageSwitchCometHandler netmessage = new NetmessageSwitchCometHandler();
                netmessage.ReceiverClientId = client.ClientId;
                netmessage.Handler = ClientCommunicationMethod.SignalR;

                CometHelper.Instance.SendMessage(netmessage);
            }
        }        

        private void SwitchSupportToolsToSignalR(bool test)
        {            
            ClientCollection clients = this.GetSupportToolsRunningInWebserviceMode(this.GetSupportToolSwitcherAmount());
            if (test)
            {
                string testString = "";
                foreach (ClientEntity client in clients)
                {
                    if (!testString.IsNullOrEmpty())
                    {
                        testString += ", ";
                    }
                    testString += client.ClientId;
                }
                this.lblSupportToolsConnectionSwitcherResult.Text = string.Format("Clients: {0}", testString);
                return;
            }

            foreach (ClientEntity client in clients)
            {
                NetmessageDeviceCommandExecute netmessage = new NetmessageDeviceCommandExecute();
                netmessage.ReceiverClientId = client.ClientId;
                netmessage.Command = "am force-stop net.craveinteractive.supporttools";

                CometHelper.Instance.SendMessage(netmessage);
            }
        }

        private void CreateCommands()
        {
            List<int> clientIds = new List<int> { 77670, 77667, 77161, 76059, 76055, 76001, 75754, 75593, 73996, 73883, 72406, 71852, 71801, 71797, 71758, 69265, 69264, 69043, 68965, 68889, 68800, 67983, 67957, 67945, 67904, 67726, 67167, 66839, 66817, 66591, 66078, 65200, 65102, 65085, 63809, 63716, 63563, 62987, 62755, 62413, 62268, 62179, 61925, 60296, 60294, 60025, 58781, 58667, 56329, 55453, 55449, 54204, 53830, 51994, 51941, 51018, 49608, 48255, 47353, 47258, 47252, 47031, 47029, 46950, 46818, 46778, 46757, 46745, 46635, 46438, 46121, 46070, 45733, 45516, 45238, 45221, 45207, 45206, 45202, 45177, 45150, 45149, 45148, 45144, 45143, 45142, 45137, 45116, 45107, 45105, 45103, 45093, 45092, 45085, 45081, 45078, 45075, 45069, 45049, 45041, 45035, 45030, 45022, 45012, 45011, 45008, 44963, 44958, 44945, 44940, 44918, 44917, 44907, 44906, 44905, 44899, 44894, 44889, 44888, 44884, 44883, 44880, 44877, 44876, 44822, 44801, 44799, 44792, 44791, 44752, 44725, 44709, 44695, 44564, 44533, 44529, 44525, 44523, 44514, 44513, 44502, 44495, 44493, 44487, 44472, 44463, 44461, 44454, 44445, 44438, 44422, 44419, 44330, 44318, 44316, 44308, 44300, 44296, 44292, 44279, 44263, 44262, 44257, 44254, 44246, 44245, 44244, 44242, 44240, 44224, 44218, 44216, 44210, 44206, 44201, 44200, 44196, 44189, 44187, 44183, 44154, 44148, 44143, 44130, 44123, 44116, 44098, 44092, 44081, 44070, 44069, 44045, 44040, 44037, 44033, 44027, 43962, 43953, 43911, 43908, 43904, 43892, 43885, 43856, 43849, 43845, 43831, 43829, 43722, 43695, 43684, 43683, 43682, 43681, 43672, 43671, 43662, 43647, 43642, 43641, 43609, 43573, 43560, 43559, 43557, 43341, 43340, 43332, 43329, 43317, 43285, 43284, 43282, 43266, 43136, 43125, 43123, 43113, 43036, 43035, 43028, 43020, 43016, 42876, 42867, 42852, 42849, 42833, 42828, 42797, 42785, 42771, 42760, 42750, 42742, 42689, 42688, 42684, 42667, 42665, 42663, 42655, 42654, 42647, 42645, 42621, 42592, 42586, 42585, 38884 };

            foreach (int clientId in clientIds)
            {
                ScheduledCommandEntity command1 = new ScheduledCommandEntity();
                command1.ScheduledCommandTaskId = 8255;
                command1.ClientId = clientId;
                command1.ClientCommand = ClientCommand.DownloadAgentUpdate;
                command1.Status = ScheduledCommandStatus.Pending;
                command1.Sort = 0;
                command1.Save();

                ScheduledCommandEntity command2 = new ScheduledCommandEntity();
                command2.ScheduledCommandTaskId = 8255;
                command2.ClientId = clientId;
                command2.ClientCommand = ClientCommand.DownloadSupportToolsUpdate;
                command2.Status = ScheduledCommandStatus.Pending;
                command2.Sort = 0;
                command2.Save();

                ScheduledCommandEntity command3 = new ScheduledCommandEntity();
                command3.ScheduledCommandTaskId = 8255;
                command3.ClientId = clientId;
                command3.ClientCommand = ClientCommand.InstallAgentUpdate;
                command3.Status = ScheduledCommandStatus.Pending;
                command3.Sort = 0;
                command3.Save();

                ScheduledCommandEntity command4 = new ScheduledCommandEntity();
                command4.ScheduledCommandTaskId = 8255;
                command4.ClientId = clientId;
                command4.ClientCommand = ClientCommand.InstallSupportToolsUpdate;
                command4.Status = ScheduledCommandStatus.Pending;
                command4.Sort = 0;
                command4.Save();
            }            
        }

        private void SetCompanyNamesInIrConfigurations()
        {
            InfraredConfigurationCollection infraredConfigurations = new InfraredConfigurationCollection();
            infraredConfigurations.GetMulti(null);

            foreach (InfraredConfigurationEntity configuration in infraredConfigurations)
            {
                CompanyEntity company;
                if (this.GetCompanyForInfraredConfiguration(configuration, out company) && !configuration.Name.StartsWith(company.Name, StringComparison.InvariantCultureIgnoreCase))
                {
                    configuration.Name = string.Format("{0} {1}", company.Name, configuration.Name);
                    configuration.Save();
                }
            }
        }

        private void FixLeBonne()
        {
            int companyId = 456;
            string cultureCode = "fr-CA";

            ProductCollection products = this.GetProductsWithCustomTextsForCompany(companyId);
            foreach (ProductEntity productEntity in products)
            {
                Dictionary<CustomTextType, string> customTexts = productEntity.CustomTextCollection.ToCultureCodeSpecificDictionary(cultureCode);

                if (!StringUtil.IsNullOrWhiteSpace(customTexts.GetValueOrDefault(CustomTextType.ProductName)))
                {
                    productEntity.Name = customTexts.GetValueOrDefault(CustomTextType.ProductName);
                }
                if (!StringUtil.IsNullOrWhiteSpace(customTexts.GetValueOrDefault(CustomTextType.ProductDescription)))
                {
                    productEntity.Description = customTexts.GetValueOrDefault(CustomTextType.ProductDescription);
                }
                if (!StringUtil.IsNullOrWhiteSpace(customTexts.GetValueOrDefault(CustomTextType.ProductButtonText)))
                {
                    productEntity.ButtonText = customTexts.GetValueOrDefault(CustomTextType.ProductButtonText);
                }

                productEntity.Save();
            }

            CategoryCollection categories = this.GetCategoriesWithCustomTextsForCompany(companyId);
            foreach (CategoryEntity categoryEntity in categories)
            {
                Dictionary<CustomTextType, string> customTexts = categoryEntity.CustomTextCollection.ToCultureCodeSpecificDictionary(cultureCode);

                if (!StringUtil.IsNullOrWhiteSpace(customTexts.GetValueOrDefault(CustomTextType.CategoryName)))
                {
                    categoryEntity.Name = customTexts.GetValueOrDefault(CustomTextType.CategoryName);
                }
                if (!StringUtil.IsNullOrWhiteSpace(customTexts.GetValueOrDefault(CustomTextType.CategoryDescription)))
                {
                    categoryEntity.Description = customTexts.GetValueOrDefault(CustomTextType.CategoryDescription);
                }
                if (!StringUtil.IsNullOrWhiteSpace(customTexts.GetValueOrDefault(CustomTextType.CategoryButtonText)))
                {
                    categoryEntity.ButtonText = customTexts.GetValueOrDefault(CustomTextType.CategoryButtonText);
                }

                categoryEntity.Save();
            }

            AlterationCollection alterations = this.GetAlterationsWithCustomTextsForCompany(companyId);
            foreach (AlterationEntity alterationEntity in alterations)
            {
                Dictionary<CustomTextType, string> customTexts = alterationEntity.CustomTextCollection.ToCultureCodeSpecificDictionary(cultureCode);

                if (!StringUtil.IsNullOrWhiteSpace(customTexts.GetValueOrDefault(CustomTextType.AlterationName)))
                {
                    alterationEntity.Name = customTexts.GetValueOrDefault(CustomTextType.AlterationName);
                }
                if (!StringUtil.IsNullOrWhiteSpace(customTexts.GetValueOrDefault(CustomTextType.AlterationDescription)))
                {
                    alterationEntity.Description = customTexts.GetValueOrDefault(CustomTextType.AlterationDescription);
                }

                alterationEntity.Save();
            }

            AlterationoptionCollection alterationoptions = this.GetAlterationoptionsWithCustomTextsForCompany(companyId);
            foreach (AlterationoptionEntity alterationoptionEntity in alterationoptions)
            {
                Dictionary<CustomTextType, string> customTexts = alterationoptionEntity.CustomTextCollection.ToCultureCodeSpecificDictionary(cultureCode);

                if (!StringUtil.IsNullOrWhiteSpace(customTexts.GetValueOrDefault(CustomTextType.AlterationoptionName)))
                {
                    alterationoptionEntity.Name = customTexts.GetValueOrDefault(CustomTextType.AlterationoptionName);
                }
                if (!StringUtil.IsNullOrWhiteSpace(customTexts.GetValueOrDefault(CustomTextType.AlterationoptionDescription)))
                {
                    alterationoptionEntity.Description = customTexts.GetValueOrDefault(CustomTextType.AlterationoptionDescription);
                }

                alterationoptionEntity.Save();
            }
        }

        private ProductCollection GetProductsWithCustomTextsForCompany(int companyId)
        {
            PredicateExpression filter = new PredicateExpression(ProductFields.CompanyId == companyId);
            PrefetchPath prefetch = new PrefetchPath(EntityType.ProductEntity);
            prefetch.Add(ProductEntityBase.PrefetchPathCustomTextCollection);

            ProductCollection products = new ProductCollection();
            products.GetMulti(filter, prefetch);

            return products;
        }

        private CategoryCollection GetCategoriesWithCustomTextsForCompany(int companyId)
        {
            PredicateExpression filter = new PredicateExpression(CategoryFields.CompanyId == companyId);
            PrefetchPath prefetch = new PrefetchPath(EntityType.CategoryEntity);
            prefetch.Add(CategoryEntityBase.PrefetchPathCustomTextCollection);

            CategoryCollection categories = new CategoryCollection();
            categories.GetMulti(filter, prefetch);

            return categories;
        }

        private AlterationCollection GetAlterationsWithCustomTextsForCompany(int companyId)
        {
            PredicateExpression filter = new PredicateExpression(AlterationFields.CompanyId == companyId);
            PrefetchPath prefetch = new PrefetchPath(EntityType.AlterationEntity);
            prefetch.Add(AlterationEntityBase.PrefetchPathCustomTextCollection);

            AlterationCollection alterations = new AlterationCollection();
            alterations.GetMulti(filter, prefetch);

            return alterations;
        }

        private AlterationoptionCollection GetAlterationoptionsWithCustomTextsForCompany(int companyId)
        {
            PredicateExpression filter = new PredicateExpression(AlterationoptionFields.CompanyId == companyId);
            PrefetchPath prefetch = new PrefetchPath(EntityType.AlterationoptionEntity);
            prefetch.Add(AlterationoptionEntityBase.PrefetchPathCustomTextCollection);

            AlterationoptionCollection alterationoptions = new AlterationoptionCollection();
            alterationoptions.GetMulti(filter, prefetch);

            return alterationoptions;
        }

        private bool GetCompanyForInfraredConfiguration(InfraredConfigurationEntity configuration, out CompanyEntity company)
        {
            company = null;

            foreach (RoomControlWidgetEntity widget in configuration.RoomControlWidgetCollection)
            {
                if (widget.RoomControlSectionId.GetValueOrDefault(0) > 0)
                {
                    company = widget.RoomControlSectionEntity.RoomControlAreaEntity.RoomControlConfigurationEntity.CompanyEntity;
                    return true;
                }
                else if (widget.RoomControlSectionItemId.GetValueOrDefault(0) > 0)
                {
                    company = widget.RoomControlSectionItemEntity.RoomControlSectionEntity.RoomControlAreaEntity.RoomControlConfigurationEntity.CompanyEntity;
                    return true;
                }
            }

            foreach (RoomControlComponentEntity component in configuration.RoomControlComponentCollection)
            {
                company = component.RoomControlSectionEntity.RoomControlAreaEntity.RoomControlConfigurationEntity.CompanyEntity;
                return true;                
            }

            foreach (RoomControlSectionItemEntity sectionItem in configuration.RoomControlSectionItemCollection)
            {
                company = sectionItem.RoomControlSectionEntity.RoomControlAreaEntity.RoomControlConfigurationEntity.CompanyEntity;
                return true;                
            }

            return false;
        }

        private void BtnSetUtcFields_Click(object sender, EventArgs e)
        {
            this.SetUtcFields();
        }

        private void BtnCreatePmsReportConfigurations_Click(object sender, EventArgs e)
        {
            this.CreatePmsReportConfigurations();
        }

        void btnConvertGenericBrandProductsMedia_Click(object sender, EventArgs e)
        {
            this.ConvertGenericProductsToBrandProductsMedia();
        }

        private void BtnSetUtcFieldsOnOccurrenceEntities_Click(object sender, EventArgs e)
        {
            this.SetUtcFieldsOnOccurrenceEntities();
        }

        void btnConvertGenericBrandProducts_Click(object sender, EventArgs e)
        {
            this.ConvertGenericProductsToBrandProducts();
        }

        private void BtnFixLeBonne_Click(object sender, EventArgs e)
        {
            this.FixLeBonne();
        }

        private void BtnSetCompanyNamesInIrConfigurationsOnClick(object sender, EventArgs eventArgs)
        {
            this.SetCompanyNamesInIrConfigurations();
        }

        private void BtnSwitchEmenusToSignalRTestOnClick(object sender, EventArgs eventArgs)
        {
            this.SwitchEmenusToSignalR(true);
        }

        private void BtnSwitchEmenusToWebserviceTestOnClick(object sender, EventArgs eventArgs)
        {
            this.SwitchEmenusToWebserviceMode(true);
        }

        private void BtnSwitchEmenusToSignalROnClick(object sender, EventArgs eventArgs)
        {
            this.SwitchEmenusToSignalR(false);
        }

        private void BtnSwitchEmenusToWebserviceOnClick(object sender, EventArgs eventArgs)
        {
            this.SwitchEmenusToWebserviceMode(false);
        }        

        private void BtnSwitchSupportToolsToSignalRTestOnClick(object sender, EventArgs eventArgs)
        {
            this.SwitchSupportToolsToSignalR(true);
        }        

        private void BtnSwitchSupportToolsToSignalROnClick(object sender, EventArgs eventArgs)
        {
            this.SwitchSupportToolsToSignalR(false);
        }

        private void BtnGetEmenusRunningWebserviceModeOnClick(object sender, EventArgs eventArgs)
        {
            this.RefreshEmenusRunningInWebserviceMode();
        }

        private void BtnGetSupportToolsRunningWebserviceModeOnClick(object sender, EventArgs eventArgs)
        {
            this.RefreshSupportToolsRunningInWebserviceMode();
        }

        private void BtnCreateCommandsOnClick(object sender, EventArgs eventArgs)
        {
            this.CreateCommands();
        }

        private void BtnNewSiteImagesCutoutsTestOnClick(object sender, EventArgs eventArgs)
        {
            this.CreateNewSiteImagesCutouts(true);
        }

        private void BtnNewSiteImagesCutoutsOnClick(object sender, EventArgs eventArgs)
        {
            this.CreateNewSiteImagesCutouts(false);
        }

        private void BtnUploadDeliveryTimeOnClick(object sender, EventArgs eventArgs)
        {
            this.UploadDeliveryTime();
        }

        private void BtnDecodeSites_Click(object sender, EventArgs e)
        {
            this.DecodeSites();
        }

        void btnCopySiteImages_Click(object sender, EventArgs e)
        {
            this.CopySiteImages();
        }

        void btnSyncCountryCodes_Click(object sender, EventArgs e)
        {
            this.SyncCountryCodes();
        }

        void btnReuploadMediaForUnknownDeviceType_Click(object sender, EventArgs e)
        {
            this.ReuploadMediaForUnknownDeviceType(false);
        }

        void btnReuploadMediaForUnknownDeviceTypeTest_Click(object sender, EventArgs e)
        {
            this.ReuploadMediaForUnknownDeviceType(true);
        }
        
        private void BtnFixFormattingIssueForItalicAndBoldTest_Click(object sender, EventArgs e)
        {
            this.FixFormattingIssueForItalicAndBold(true);
        }

        private void BtnFixFormattingIssueForItalicAndBold_Click(object sender, EventArgs e)
        {
            this.FixFormattingIssueForItalicAndBold(false);
        }

        private void BtnSetCultureCodesForCustomTextsWithoutTest_Click(object sender, EventArgs e)
        {
            this.SetCultureCodeForDefaultCustomTexts(true);            
        }

        private void BtnSetCultureCodesForCustomTextsWithout_Click(object sender, EventArgs e)
        {
            this.SetCultureCodeForDefaultCustomTexts(false);
        }        

        private void btnConvertDeliverypointgroupToClientConfiguration_Click(object sender, EventArgs e)
        {
            this.ConvertDeliverypointgroupToClientConfiguration();
        }

        private void btnSetTerminalType_Click(object sender, EventArgs e)
        {
            this.SetTerminalType();
        }

        private void btnConvertTerminalToTerminalConfiguration_Click(object sender, EventArgs e)
        {
            this.ConvertTerminalToTerminalConfiguration();
        }

        private void ClearClientConfigurations()
        {
            // Entertainments
            EntertainmentConfigurationEntertainmentCollection entertainmentConfigurationEntertainmentCollection = new EntertainmentConfigurationEntertainmentCollection();
            entertainmentConfigurationEntertainmentCollection.DeleteMulti(null);

            // Entertainment configuration timestamps
            PredicateExpression timestampFilter = new PredicateExpression(TimestampFields.EntertainmentConfigurationId != DBNull.Value);
            TimestampCollection timestampCollection = new TimestampCollection();
            timestampCollection.DeleteMulti(timestampFilter);

            // Set entertainment configuration id and advertisement configuration id to null for client configurations
            ClientConfigurationEntity clientConfigurationEntity = new ClientConfigurationEntity() { EntertainmentConfigurationId = null, AdvertisementConfigurationId = null};
            ClientConfigurationCollection clientConfigurationCollection = new ClientConfigurationCollection();
            clientConfigurationCollection.UpdateMulti(clientConfigurationEntity, null);

            // Set client configuration id to null on pms action rule
            PmsActionRuleEntity pmsActionRuleEntity = new PmsActionRuleEntity() {ClientConfigurationId = null};
            PmsActionRuleCollection pmsActionRuleCollection = new PmsActionRuleCollection();
            pmsActionRuleCollection.UpdateMulti(pmsActionRuleEntity, null);

            // Entertainment configurations
            EntertainmentConfigurationCollection entertainmentConfigurationCollection = new EntertainmentConfigurationCollection();
            entertainmentConfigurationCollection.DeleteMulti(null);

            // Advertisements
            AdvertisementConfigurationAdvertisementCollection advertisementConfigurationAdvertisementCollection = new AdvertisementConfigurationAdvertisementCollection();
            advertisementConfigurationAdvertisementCollection.DeleteMulti(null);

            // Advertisement configuration timestamps
            timestampFilter = new PredicateExpression(TimestampFields.AdvertisementConfigurationId != DBNull.Value);
            timestampCollection.DeleteMulti(timestampFilter);

            // Advertisement configurations
            AdvertisementConfigurationCollection advertisementConfigurationCollection = new AdvertisementConfigurationCollection();
            advertisementConfigurationCollection.DeleteMulti(null);

            // Client configuration routes
            ClientConfigurationRouteCollection clientConfigurationRouteCollection = new ClientConfigurationRouteCollection();
            clientConfigurationRouteCollection.DeleteMulti(null);

            // Set client configuration id to null for deliverypoint groups
            DeliverypointgroupEntity deliverypointgroupEntity = new DeliverypointgroupEntity() { ClientConfigurationId = null };
            DeliverypointgroupCollection deliverypointgroupCollection = new DeliverypointgroupCollection();
            deliverypointgroupCollection.UpdateMulti(deliverypointgroupEntity, null);

            // Set client configuration id to null for deliverypoints
            DeliverypointEntity deliverypointEntity = new DeliverypointEntity() { ClientConfigurationId = null };
            DeliverypointCollection deliverypointCollection = new DeliverypointCollection();
            deliverypointCollection.UpdateMulti(deliverypointEntity, null);

            // Custom texts
            PredicateExpression customTextFilter = new PredicateExpression(CustomTextFields.ClientConfigurationId != DBNull.Value);
            CustomTextCollection customTextCollection = new CustomTextCollection();
            customTextCollection.DeleteMulti(customTextFilter);

            // Timestamp
            timestampFilter = new PredicateExpression(TimestampFields.ClientConfigurationId != DBNull.Value);
            timestampCollection.DeleteMulti(timestampFilter);

            // Client configurations
            clientConfigurationCollection = new ClientConfigurationCollection();
            clientConfigurationCollection.DeleteMulti(null);
        }

        private void ClearTerminalConfigurations()
        {
            // Set terminal configuration id to null for terminals
            TerminalEntity terminalEntity = new TerminalEntity() { TerminalConfigurationId = null };
            TerminalCollection terminalCollection = new TerminalCollection();
            terminalCollection.UpdateMulti(terminalEntity, null);

            // Timestamp
            PredicateExpression timestampFilter = new PredicateExpression(TimestampFields.TerminalConfigurationId != DBNull.Value);
            TimestampCollection timestampCollection = new TimestampCollection();
            timestampCollection.DeleteMulti(timestampFilter);

            // Terminals configurations
            TerminalConfigurationCollection terminalConfigurationCollection = new TerminalConfigurationCollection();
            terminalConfigurationCollection.DeleteMulti(null);
        }

        private readonly UpdateProductWithBrandProductMaintenanceUseCase updateProductWithBrandProductMaintenanceUseCase = new UpdateProductWithBrandProductMaintenanceUseCase();

        private void ConvertBrandProducts()
        {
            using (Transaction transaction = new Transaction(IsolationLevel.ReadUncommitted, "ConvertBrandProducts"))
            {
                try
                {
                    ProductCollection brandProductCollection = this.GetBrandProductCollection();
                    this.UpdateProductsWithBrandProducts(brandProductCollection, transaction);

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }

        private void UpdateProductsWithBrandProducts(ProductCollection brandProductCollection, ITransaction transaction)
        {
            foreach (ProductEntity brandProduct in brandProductCollection)
            {
                this.UpdateProductsWithBrandProduct(brandProduct, transaction);
            }
        }

        private void UpdateProductsWithBrandProduct(ProductEntity brandProduct, ITransaction transaction)
        {
            foreach (ProductEntity product in brandProduct.BrandProductCollection)
            {
                product.AddToTransaction(transaction);

                this.SetBrandInheritPropertiesOnProduct(product, brandProduct);
                this.UpdateProductWithBrandProduct(product, brandProduct);

                product.Validator = null;
                product.Save();
            }
        }

        private void SetBrandInheritPropertiesOnProduct(ProductEntity product, ProductEntity brandProduct)
        {
            product.InheritName = product.Name.IsNullOrWhiteSpace();
            product.InheritPrice = !product.PriceIn.HasValue;
            product.InheritButtonText = product.ButtonText.IsNullOrWhiteSpace();
            product.InheritCustomizeButtonText = product.CustomizeButtonText.IsNullOrWhiteSpace();
            product.InheritWebTypeTabletUrl = product.WebTypeTabletUrl.IsNullOrWhiteSpace();
            product.InheritWebTypeSmartphoneUrl = product.WebTypeSmartphoneUrl.IsNullOrWhiteSpace();
            product.InheritDescription = product.Description.IsNullOrWhiteSpace();
            product.InheritColor = product.Color == 0;
            product.InheritMedia = true;

            if (product.MediaCollection.Any() && !brandProduct.MediaCollection.Any())
            {
                product.InheritMedia = false;
            }
        }

        private void UpdateProductWithBrandProduct(ProductEntity product, ProductEntity brandProduct)
        {
            SimpleResponse response = this.updateProductWithBrandProductMaintenanceUseCase.Execute(new UpdateProductWithBrandProductRequest
            {
                Product = product,
                BrandProduct = brandProduct
            });

            if (!response.Success)
            {
                throw new ObymobiException(ProductSaveResult.UpdateProductWithBrandProductFailed, response.Message);
            }
        }

        private ProductCollection GetBrandProductCollection()
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ProductFields.BrandId != DBNull.Value);

            // Brand product
            PrefetchPath prefetchPath = new PrefetchPath(EntityType.ProductEntity);
            prefetchPath.Add(ProductEntityBase.PrefetchPathProductAlterationCollection);
            prefetchPath.Add(ProductEntityBase.PrefetchPathAttachmentCollection);
            prefetchPath.Add(ProductEntityBase.PrefetchPathMediaCollection);
            prefetchPath.Add(ProductEntityBase.PrefetchPathCustomTextCollection);

            // Product
            IPrefetchPathElement prefetchProduct = prefetchPath.Add(ProductEntityBase.PrefetchPathBrandProductCollection);
            prefetchProduct.SubPath.Add(ProductEntityBase.PrefetchPathProductAlterationCollection);
            prefetchProduct.SubPath.Add(ProductEntityBase.PrefetchPathProductAttachmentCollection);
            prefetchProduct.SubPath.Add(ProductEntityBase.PrefetchPathMediaRelationshipCollection);
            prefetchProduct.SubPath.Add(ProductEntityBase.PrefetchPathCustomTextCollection);

            ProductCollection productCollection = new ProductCollection();
            productCollection.GetMulti(filter, prefetchPath);

            return productCollection;
        }

        private void ConvertChannels()
        {
            StationCollection stations = this.GetStationsWithoutChannels();

            foreach (StationEntity station in stations)
            {
                station.Channel = station.SortOrder.ToString();
                station.Save();
            }
        }

        private StationCollection GetStationsWithoutChannels()
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(StationFields.Channel == DBNull.Value);
            filter.AddWithOr(StationFields.Channel == string.Empty);

            StationCollection stationCollection = new StationCollection();
            stationCollection.GetMulti(filter);

            return stationCollection;
        }

        private void ConvertDeliverypointgroupToClientConfiguration()
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(DeliverypointgroupFields.ClientConfigurationId == DBNull.Value);
            filter.Add(DeliverypointgroupFields.MenuId != DBNull.Value);
            filter.Add(DeliverypointgroupFields.UIModeId != DBNull.Value);

            DeliverypointgroupCollection deliverypointgroupCollection = new DeliverypointgroupCollection();
            deliverypointgroupCollection.GetMulti(filter);

            foreach (DeliverypointgroupEntity deliverypointgroupEntity in deliverypointgroupCollection)
            {
                ClientConfigurationHelper.CreateFromDeliverypointgroup(deliverypointgroupEntity);
            }
        }

        private void SetAllowPublishingFlagsOnUsers()
        {
            string filePath = Server.MapPath("~/App_Data/Push.txt");
            if (!File.Exists(filePath))
                return;

            List<string> usernames = new List<string>();

            string[] lines = File.ReadAllLines(filePath);
            foreach (string line in lines)
            {
                if (!line.Contains(" - "))
                    continue;

                string[] parts = line.Split(" - ", StringSplitOptions.RemoveEmptyEntries);
                if (parts.Length >= 2)
                {
                    string username = parts[2];
                    if (!usernames.Contains(username))
                        usernames.Add(username);
                }
            }

            if (!usernames.Any())
                return;

            UserEntity userEntity = new UserEntity { AllowPublishing = true };
            PredicateExpression filter = new PredicateExpression(UserFields.Username == usernames.ToArray());
            new UserCollection().UpdateMulti(userEntity, filter);
        }

        private void SetTerminalType()
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(TerminalFields.Type == DBNull.Value);

            TerminalCollection terminalCollection = new TerminalCollection();
            terminalCollection.GetMulti(filter);

            foreach(TerminalEntity terminal in terminalCollection)
            {
                terminal.Type = terminal.HandlingMethod;
            }

            terminalCollection.SaveMulti();
        }

        private void ConvertTerminalToTerminalConfiguration()
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(TerminalFields.TerminalConfigurationId == DBNull.Value);

            TerminalCollection terminalCollection = new TerminalCollection();
            terminalCollection.GetMulti(filter);

            foreach (TerminalEntity terminalEntity in terminalCollection)
            {
                TerminalConfigurationHelper.CreateFromTerminal(terminalEntity);
            }
        }

        private void BtnCreateDefaultCustomTextsTest_Click(object sender, EventArgs e)
        {
            this.CreateDefaultCustomTextsTest();
        }

        private void BtnCreateDefaultCustomTextsForCompany_Click(object sender, EventArgs e)
        {
            this.CreateDefaultCustomTexts(DefaultCustomTextsSubSet.Company, false);
        }

        private void BtnCreateDefaultCustomTextsForGeneric_Click(object sender, EventArgs e)
        {
            this.CreateDefaultCustomTexts(DefaultCustomTextsSubSet.Generic, false);
        }

        private void BtnCreateDefaultCustomTextsForMenu_Click(object sender, EventArgs e)
        {
            this.CreateDefaultCustomTexts(DefaultCustomTextsSubSet.Menu, false);
        }

        private void BtnCreateDefaultCustomTextsForSites_Click(object sender, EventArgs e)
        {
            this.CreateDefaultCustomTexts(DefaultCustomTextsSubSet.Sites, false);
        }

        private void BtnCreateDefaultCustomTextsForRoomControl_Click(object sender, EventArgs e)
        {
            this.CreateDefaultCustomTexts(DefaultCustomTextsSubSet.RoomControl, false);
        }

        void btnConvertUIThemes_Click(object sender, EventArgs e)
        {
            this.ConvertUIThemes();
        }

        private void BtnConvertTimeZones_Click(object sender, EventArgs e)
        {
            this.ConvertTimeZones();
        }

        void btnCustomTextCleanup_Click(object sender, EventArgs e)
        {
            this.CustomTextCleanup();
        }

        void btnCreateDefaultCompanyCurrencies_Click(object sender, EventArgs e)
        {
            this.CreateDefaultCompanyCurrencies();
        }

        void btnCopyValuesToCustomTextEntitiesTest_Click(object sender, EventArgs e)
        {
            this.CopyValuesToCustomTextEntities(true);
        }

        void btnCopyValuesToCustomTextEntities_Click(object sender, EventArgs e)
        {
            this.CopyValuesToCustomTextEntities(false);
        }

        void btnRestartUnloadedClientsTest_Click(object sender, EventArgs e)
        {
            this.RestartClientsStuckInLoading(true);
        }

        void btnRestartUnloadedClients_Click(object sender, EventArgs e)
        {
            this.RestartClientsStuckInLoading(false);
        }

        private void btnConvertDateTimesToUtc_Click(object sender, EventArgs e)
        {
            const int PAGE_SIZE = 1000;

            Dictionary<string, string[]> entityNames = new Dictionary<string, string[]>();
            entityNames.Add("TerminalLogFileEntity", new [] { "LogDate" } );
            entityNames.Add("DeliverypointgroupOccupancyEntity", new[] { "Date" });
            entityNames.Add("DeviceEntity", new[] { "UpdateStatusChanged" });
            entityNames.Add("UIWidgetTimerEntity", new[] { "CountToTime", "CountToDate" });
            entityNames.Add("UserLogonEntity", new[] { "DateTime" });
            entityNames.Add("VenueCategoryEntity", new[] { "LastModified" });
            entityNames.Add("AmenityEntity", new[] { "LastModified" });
            entityNames.Add("BusinesshoursexceptionEntity", new[] { "FromDateTime", "TillDateTime" });
            entityNames.Add("ScheduledCommandTaskEntity", new[] { "Start", "Expires", "Completed" });
            entityNames.Add("SetupCodeEntity", new[] { "ExpireDate" });
            entityNames.Add("SiteEntity", new[] { "LastModified" });
            entityNames.Add("ClientLogFileEntity", new[] { "LogDate" });
            entityNames.Add("CompanyEntity", new[] { "CompanyDataLastModified", "CompanyMediaLastModified", "MenuDataLastModified", "MenuMediaLastModified", "PosIntegrationInfoLastModified", "DeliverypointDataLastModified", "SurveyDataLastModified", "SurveyMediaLastModified", "AnnouncementDataLastModified", "AnnouncementMediaLastModified", "EntertainmentDataLastModified", "EntertainmentMediaLastModified", "AdvertisementDataLastModified", "AdvertisementMediaLastModified", "PercentageDownNotificationLastSent", "PercentageDownJumpNotificationLastSent", "LastDailyReportSend", "DailyReportSendTime" });
            entityNames.Add("PointOfInterestEntity", new[] { "LastModified" });
            entityNames.Add("ProductEntity", new[] { "GenericProductChanged" });
            entityNames.Add("ReportProcessingTaskEntity", new[] { "From", "Till" });
            entityNames.Add("GenericalterationEntity", new[] { "StartTime", "EndTime" }); 
            entityNames.Add("UserEntity", new[] { "LastActivityDate", "LastLoginDate", "LastPasswordChangedDate", "LastLockedOutDate", "FailedPasswordAttemptWindowStart", "FailedPasswordAttemptLastDate", "PasswordResetLinkGenerated" });
            entityNames.Add("AnnouncementEntity", new[] { "DateToShow", "TimeToShow", "RecurringBeginDate", "RecurringBeginTime", "RecurringEndDate", "RecurringEndTime" });
            entityNames.Add("MediaProcessingTaskEntity", new[] { "CompletedOnAmazon", "LastAttempt", "NextAttempt" });
            entityNames.Add("ScheduledCommandEntity", new[] { "Started", "Expires", "Completed" });
            entityNames.Add("ClientEntity", new[] { "LastRequest", "LastRequestNotifiedBySMS", "LastSupportToolsRequest", "OutOfChargeNotificationLastSent", "WakeUpTime" });
            entityNames.Add("OrderEntity", new[] { "Processed", "PlaceTime" });
            entityNames.Add("CloudProcessingTaskEntity", new[] { "LastAttempt", "NextAttempt", "CompletedOnAmazon" });
            entityNames.Add("OrderitemAlterationitemEntity", new[] { "Time" });
            entityNames.Add("OrderRoutestephandlerEntity", new[] { "TimeoutExpires", "RetrievalSupportNotificationTimeout", "BeingHandledSupportNotificationTimeout", "WaitingToBeRetrieved", "RetrievedByHandler", "BeingHandled", "Completed" });
            entityNames.Add("OrderRoutestephandlerHistoryEntity", new[] { "TimeoutExpires", "RetrievalSupportNotificationTimeout", "BeingHandledSupportNotificationTimeout", "WaitingToBeRetrieved", "RetrievedByHandler", "BeingHandled", "Completed" });
            entityNames.Add("CustomerEntity", new[] { "PasswordResetLinkGenerated", "LastFailedPasswordAttempt" });
            entityNames.Add("SurveyResultEntity", new[] { "Submitted" });
            entityNames.Add("TerminalEntity", new[] { "LastRequest", "LastRequestNotifiedBySMS", "OutOfChargeNotificationLastSent", "LastSupportToolsRequest" });
            entityNames.Add("AlterationEntity", new[] { "StartTime", "EndTime" });
            entityNames.Add("UIScheduleItemOccurrenceEntity", new[] { "StartTime", "EndTime", "RecurrenceStart", "RecurrenceEnd" }); 
            
            string timestamp = "DateTime-conversion-"+TimeStamp.CreateTimeStamp();

            foreach (string entityName in entityNames.Keys)
            {
                AddToLog(timestamp, "Converting DateTimes for entity {0}", entityName);

                string[] fieldNames = entityNames[entityName];

                LLBLGenEntityCollectionFactory factory = new LLBLGenEntityCollectionFactory();
                IEntityCollection entityCollection = factory.GetEntityCollection(entityName) as IEntityCollection;
                if (entityCollection != null)
                {
                    // Get the record count of the table
                    int recordCount = entityCollection.GetDbCount();

                    IncludeFieldsList fields = null;

                    IEntity entity = new LLBLGenEntityFactory().GetEntity(entityName) as IEntity;
                    if (entity != null)
                    {
                        fields = new IncludeFieldsList();

                        foreach (string fieldName in fieldNames)
                        {
                            if (entity.Fields[fieldName] != null)
                                fields.Add(entity.Fields[fieldName]);
                            if (entity.Fields[fieldName+"UTC"] != null)
                                fields.Add(entity.Fields[fieldName+"UTC"]);
                        }
                    }

                    if (recordCount < PAGE_SIZE)
                    {
                        // Get the records from the database
                        entityCollection.GetMulti(null, fields, null);
                        AddToLog(timestamp, "Converting {0} records for entity {1}", entityCollection.Count, entityName);
                        UpdateDateTimesUtc(entityCollection);
                    }
                    else
                    {
                        int pageCount = recordCount / PAGE_SIZE;

                        int pageNumber;
                        for (pageNumber = 1; pageNumber <= pageCount; pageNumber++)
                        {
                            entityCollection.GetMulti(null, 0, null, null, null, fields, pageNumber, PAGE_SIZE);
                            AddToLog(timestamp, "Converting page {0} of {1} with {2} records for entity {3}", pageNumber, pageCount, entityCollection.Count, entityName);
                            UpdateDateTimesUtc(entityCollection);
                        }

                        if ((pageCount * PAGE_SIZE) < recordCount)
                        {
                            entityCollection.GetMulti(null, 0, null, null, null, fields, pageNumber, PAGE_SIZE);
                            AddToLog(timestamp, "Converting page {0} of {1} with {2} records for entity {3}", pageNumber, pageCount+1, entityCollection.Count, entityName);
                            UpdateDateTimesUtc(entityCollection);
                        }
                    }
                }

                AddToLog(timestamp, "Converting DateTimes done for entity {0}", entityName);
                AddToLog(timestamp, "========================================");
            }            
        }

        private void btnConvertCreatedUpdatedToUtc_Click(object sender, EventArgs e)
        {
            const int PAGE_SIZE = 1000;

            string timestamp = "Created-Updated-conversion-"+TimeStamp.CreateTimeStamp();

            string[] entityNames = Dionysos.Data.LLBLGen.LLBLGenUtil.GetEntityNames();
            foreach (string entityName in entityNames)
            {
                AddToLog(timestamp, "Converting Created/Updated for entity {0}", entityName);

                LLBLGenEntityCollectionFactory factory = new LLBLGenEntityCollectionFactory();
                IEntityCollection entityCollection = factory.GetEntityCollection(entityName) as IEntityCollection;
                if (entityCollection != null)
                {
                    PredicateExpression filter = null;

                    IEntity entity = new LLBLGenEntityFactory().GetEntity(entityName) as IEntity;
                    if (entity != null)
                    {
                        PredicateExpression createdFilter = null;
                        PredicateExpression updatedFilter = null;

                        if (entity.Fields["Created"] != null)
                        {
                            createdFilter = new PredicateExpression();
                            createdFilter.Add(new FieldCompareValuePredicate(entity.Fields["Created"], ComparisonOperator.NotEqual, DBNull.Value));
                            createdFilter.Add(new FieldCompareValuePredicate(entity.Fields["CreatedUTC"], ComparisonOperator.Equal, DBNull.Value));
                        }

                        if (entity.Fields["Updated"] != null)
                        {
                            updatedFilter = new PredicateExpression();
                            updatedFilter.Add(new FieldCompareValuePredicate(entity.Fields["Updated"], ComparisonOperator.NotEqual, DBNull.Value));
                            updatedFilter.Add(new FieldCompareValuePredicate(entity.Fields["UpdatedUTC"], ComparisonOperator.Equal, DBNull.Value));
                        }

                        if (createdFilter != null || updatedFilter != null)
                        {
                            filter = new PredicateExpression();
                            filter.Add(createdFilter);
                            filter.AddWithOr(updatedFilter);
                        }
                    }

                    // Get the record count of the table
                    int recordCount = entityCollection.GetDbCount(filter);

                    IncludeFieldsList fields = null;

                    if (entity != null && entity.Fields["Created"] != null && entity.Fields["Updated"] != null)
                    {
                        fields = new IncludeFieldsList();
                        fields.Add(entity.Fields["Created"]);
                        fields.Add(entity.Fields["Updated"]);
                        fields.Add(entity.Fields["CreatedUTC"]);
                        fields.Add(entity.Fields["UpdatedUTC"]);
                    }

                    if (recordCount < PAGE_SIZE)
                    {
                        // Get the records from the database
                        entityCollection.GetMulti(filter, fields, null);
                        AddToLog(timestamp, "Converting {0} records for entity {1}", entityCollection.Count, entityName);
                        UpdateCreatedUtcAndUpdatedUtc(entityCollection);
                    }
                    else
                    {
                        int pageCount = recordCount / PAGE_SIZE;

                        int pageNumber;
                        for (pageNumber = 1; pageNumber <= pageCount; pageNumber++)
                        {
                            entityCollection.GetMulti(filter, 0, null, null, null, fields, pageNumber, PAGE_SIZE);
                            AddToLog(timestamp, "Converting page {0} of {1} with {2} records for entity {3}", pageNumber, pageCount, entityCollection.Count, entityName);
                            UpdateCreatedUtcAndUpdatedUtc(entityCollection);
                        }

                        if ((pageCount * PAGE_SIZE) < recordCount)
                        {
                            entityCollection.GetMulti(filter, 0, null, null, null, fields, pageNumber, PAGE_SIZE);
                            AddToLog(timestamp, "Converting page {0} of {1} with {2} records for entity {3}", pageNumber, pageCount+1, entityCollection.Count, entityName);
                            UpdateCreatedUtcAndUpdatedUtc(entityCollection);                            
                        }
                    }
                }

                AddToLog(timestamp, "Converting Created/Updated done for entity {0}", entityName);
                AddToLog(timestamp, "========================================");
            }
        }

        private static void AddToLog(string timestamp, string format, params object[] args)
        {
            string filePath = HttpContext.Current.Request.MapPath(string.Format("~/App_Data/Log/{0}.txt", timestamp));
            File.AppendAllText(filePath, string.Format(format, args) + "\r\n");
        }

        private static readonly TimeZoneInfo TimeZone = TimeZoneInfo.FindSystemTimeZoneById("W. Europe Standard Time");

        private static void UpdateCreatedUtcAndUpdatedUtc(IEntityCollection entityCollection)
        {
            if (entityCollection.Count > 0)
            {
                bool hasCreated = entityCollection[0].Fields["Created"] != null && entityCollection[0].Fields["CreatedUTC"] != null;
                bool hasUpdated = entityCollection[0].Fields["Updated"] != null && entityCollection[0].Fields["UpdatedUTC"] != null;
                bool save = false;

                for (int i = 0; i < entityCollection.Count; i++)
                {
                    IEntity entity = entityCollection[i];

                    #region Created

                    if (hasCreated)
                    {
                        DateTime? createdUtcValue = entity.Fields["CreatedUTC"].CurrentValue as DateTime?;
                        if (!createdUtcValue.HasValue)
                        {
                            DateTime? createdValue = entity.Fields["Created"].CurrentValue as DateTime?;
                            if (createdValue.HasValue)
                            {
                                entity.SetNewFieldValue("CreatedUTC", TimeZoneInfo.ConvertTimeToUtc(createdValue.Value, TimeZone));
                                entity.Validator = null;
                                entity.AuthorizerToUse = null;
                                save = true;
                            }
                        }
                    }

                    #endregion

                    #region Updated

                    if (hasUpdated)
                    {
                        DateTime? updatedUtcValue = entity.Fields["UpdatedUTC"].CurrentValue as DateTime?;
                        if (!updatedUtcValue.HasValue)
                        {
                            DateTime? updatedValue = entity.Fields["Updated"].CurrentValue as DateTime?;
                            if (updatedValue.HasValue)
                            {
                                entity.SetNewFieldValue("UpdatedUTC", TimeZoneInfo.ConvertTimeToUtc(updatedValue.Value, TimeZone));
                                entity.Validator = null;
                                entity.AuthorizerToUse = null;
                                save = true;
                            }
                        }
                    }

                    #endregion
                }

                if (save)
                {
                    entityCollection.SaveMulti();
                }
            }
        }

        private static void UpdateDateTimesUtc(IEntityCollection entityCollection)
        {
            if (entityCollection.Count > 0)
            {
                bool save = false;

                for (int i = 0; i < entityCollection.Count; i++)
                {
                    IEntity entity = entityCollection[i];

                    if (entity is TerminalLogFileEntity)
                    {
                        SetEntityFieldValue<TerminalLogFileEntity>(ref entity, "LogDate");
                        save = true;
                    }
                    else if (entity is DeliverypointgroupOccupancyEntity)
                    {
                        SetEntityFieldValue<DeliverypointgroupOccupancyEntity>(ref entity, "Date");
                        save = true;
                    }
                    else if (entity is DeviceEntity)
                    {
                        SetEntityFieldValue<DeviceEntity>(ref entity, "UpdateStatusChanged");
                        save = true;
                    }
                    else if (entity is UIWidgetTimerEntity)
                    {
                        SetEntityFieldValue<UIWidgetTimerEntity>(ref entity, "CountToTime");
                        save = true;
                    }
                    else if (entity is UserLogonEntity)
                    {
                        SetEntityFieldValue<UserLogonEntity>(ref entity, "DateTime");
                        save = true;
                    }
                    else if (entity is VenueCategoryEntity)
                    {
                        SetEntityFieldValue<VenueCategoryEntity>(ref entity, "LastModified");
                        save = true;
                    }
                    else if (entity is AmenityEntity)
                    {
                        SetEntityFieldValue<AmenityEntity>(ref entity, "LastModified");
                        save = true;
                    }
                    else if (entity is BusinesshoursexceptionEntity)
                    {
                        SetEntityFieldValue<BusinesshoursexceptionEntity>(ref entity, "FromDateTime", "TillDateTime");
                        save = true;
                    }
                    else if (entity is ScheduledCommandTaskEntity)
                    {
                        SetEntityFieldValue<ScheduledCommandTaskEntity>(ref entity, "Start", "Expires");
                        save = true;
                    }
                    else if (entity is SetupCodeEntity)
                    {
                        SetEntityFieldValue<SetupCodeEntity>(ref entity, "ExpireDate");
                        save = true;
                    }
                    else if (entity is SiteEntity)
                    {
                        SetEntityFieldValue<SiteEntity>(ref entity, "LastModified");
                        save = true;
                    }
                    else if (entity is ClientLogFileEntity)
                    {
                        SetEntityFieldValue<ClientLogFileEntity>(ref entity, "LogDate");
                        save = true;
                    }
                    else if (entity is CompanyEntity)
                    {
                        SetEntityFieldValue<CompanyEntity>(ref entity, "CompanyDataLastModified", "CompanyMediaLastModified", "MenuDataLastModified", "MenuMediaLastModified", "PosIntegrationInfoLastModified", "DeliverypointDataLastModified", "SurveyDataLastModified", "SurveyMediaLastModified", "AnnouncementDataLastModified", "AnnouncementMediaLastModified", "EntertainmentDataLastModified", "EntertainmentMediaLastModified", "AdvertisementDataLastModified", "AdvertisementMediaLastModified");
                        save = true;
                    }
                    else if (entity is PointOfInterestEntity)
                    {
                        SetEntityFieldValue<PointOfInterestEntity>(ref entity, "LastModifiedUTC");
                        save = true;
                    }
                    else if (entity is ProductEntity)
                    {
                        SetEntityFieldValue<ProductEntity>(ref entity, "GenericProductChangedUTC");
                        save = true;
                    }
                    else if (entity is ReportProcessingTaskEntity)
                    {
                        SetEntityFieldValue<ReportProcessingTaskEntity>(ref entity, "FromUTC", "TillUTC");
                        save = true;
                    }
                    else if (entity is GenericalterationEntity)
                    {
                        SetEntityFieldValue<GenericalterationEntity>(ref entity, "StartTimeUTC", "EndTimeUTC");
                        save = true;
                    }
                    else if (entity is UIWidgetTimerEntity)
                    {
                        SetEntityFieldValue<UIWidgetTimerEntity>(ref entity, "CountToDateUTC");
                        save = true;
                    }
                    else if (entity is UserEntity)
                    {
                        SetEntityFieldValue<UserEntity>(ref entity, "LastActivityDateUTC", "LastLoginDateUTC", "LastPasswordChangedDateUTC", "LastLockedOutDateUTC", "FailedPasswordAttemptWindowStartUTC", "FailedPasswordAttemptLastDateUTC", "PasswordResetLinkGeneratedUTC");
                        save = true;
                    }
                    else if (entity is AnnouncementEntity)
                    {
                        SetEntityFieldValue<AnnouncementEntity>(ref entity, "DateToShowUTC", "TimeToShowUTC", "RecurringBeginDateUTC", "RecurringBeginTimeUTC", "RecurringEndDateUTC", "RecurringEndTimeUTC");
                        save = true;
                    }
                    else if (entity is MediaProcessingTaskEntity)
                    {
                        SetEntityFieldValue<MediaProcessingTaskEntity>(ref entity, "CompletedOnAmazonUTC", "LastAttemptUTC", "NextAttemptUTC");
                        save = true;
                    }
                    else if (entity is ScheduledCommandEntity)
                    {
                        SetEntityFieldValue<ScheduledCommandEntity>(ref entity, "StartedUTC", "ExpiresUTC", "CompletedUTC");
                        save = true;
                    }
                    else if (entity is ScheduledCommandTaskEntity)
                    {
                        SetEntityFieldValue<ScheduledCommandTaskEntity>(ref entity, "CompletedUTC");
                        save = true;
                    }
                    else if (entity is ClientEntity)
                    {
                        SetEntityFieldValue<ClientEntity>(ref entity, "LastRequestUTC", "LastRequestNotifiedBySMSUTC", "LastSupportToolsRequestUTC", "OutOfChargeNotificationLastSentUTC", "WakeUpTimeUtcUTC");
                        save = true;
                    }
                    else if (entity is OrderEntity)
                    {
                        SetEntityFieldValue<OrderEntity>(ref entity, "ProcessedUTC", "PlaceTimeUTC");
                        save = true;
                    }
                    else if (entity is CloudProcessingTaskEntity)
                    {
                        SetEntityFieldValue<CloudProcessingTaskEntity>(ref entity, "LastAttemptUTC", "NextAttemptUTC", "CompletedOnAmazonUTC");
                        save = true;
                    }
                    else if (entity is OrderitemAlterationitemEntity)
                    {
                        SetEntityFieldValue<OrderitemAlterationitemEntity>(ref entity, "Time");
                        save = true;
                    }
                    else if (entity is OrderRoutestephandlerEntity)
                    {
                        SetEntityFieldValue<OrderRoutestephandlerEntity>(ref entity, "TimeoutExpiresUTC", "RetrievalSupportNotificationTimeoutUTC", "BeingHandledSupportNotificationTimeoutUTC", "WaitingToBeRetrievedUTC", "RetrievedByHandlerUTC", "BeingHandledUTC", "CompletedUTC");
                        save = true;
                    }
                    else if (entity is CompanyEntity)
                    {
                        SetEntityFieldValue<CompanyEntity>(ref entity, "PercentageDownNotificationLastSentUTC", "PercentageDownJumpNotificationLastSentUTC", "LastDailyReportSendUTC", "DailyReportSendTimeUTC");
                        save = true;
                    }
                    else if (entity is OrderRoutestephandlerHistoryEntity)
                    {
                        SetEntityFieldValue<OrderRoutestephandlerHistoryEntity>(ref entity, "TimeoutExpiresUTC", "RetrievalSupportNotificationTimeoutUTC", "BeingHandledSupportNotificationTimeoutUTC", "WaitingToBeRetrievedUTC", "RetrievedByHandlerUTC", "BeingHandledUTC", "CompletedUTC");
                        save = true;
                    }
                    else if (entity is CustomerEntity)
                    {
                        SetEntityFieldValue<CustomerEntity>(ref entity, "PasswordResetLinkGeneratedUTC", "LastFailedPasswordAttemptUTC");
                        save = true;
                    }
                    else if (entity is SurveyResultEntity)
                    {
                        SetEntityFieldValue<SurveyResultEntity>(ref entity, "SubmittedUTC");
                        save = true;
                    }
                    else if (entity is TerminalEntity)
                    {
                        SetEntityFieldValue<TerminalEntity>(ref entity, "LastRequestUTC", "LastRequestNotifiedBySMSUTC", "OutOfChargeNotificationLastSentUTC", "LastSupportToolsRequestUTC");
                        save = true;
                    }
                    else if (entity is AlterationEntity)
                    {
                        SetEntityFieldValue<AlterationEntity>(ref entity, "StartTimeUTC", "EndTimeUTC");
                        save = true;
                    }
                    else if (entity is UIScheduleItemOccurrence)
                    {
                        SetEntityFieldValue<AlterationEntity>(ref entity, "StartTimeUTC", "EndTimeUTC", "RecurrenceStartUTC", "RecurrenceEndUTC");
                        save = true;
                    }
                }

                if (save)
                {
                    entityCollection.SaveMulti();
                }
            }
        }

        private static void SetEntityFieldValue<T>(ref IEntity entity, params string[] originalFieldNames) where T : IEntity
        {
            for (int i = 0; i < originalFieldNames.Length; i++)
            {
                string originalFieldName = originalFieldNames[i];

                if (originalFieldName.EndsWith("UTC"))
                    originalFieldName = originalFieldName.Substring(0, originalFieldName.Length - 3);

                try
                {
                    DateTime? value = entity.Fields[originalFieldName].CurrentValue as DateTime?;
                    if (value.HasValue)
                    {
                        entity.SetNewFieldValue(originalFieldName + "UTC", TimeZoneInfo.ConvertTimeToUtc(value.Value, TimeZone));
                        entity.Validator = null;
                        entity.AuthorizerToUse = null;
                    }
                }
                catch (Exception ex)
                {
                    AddToLog("Exceptions", "Exception for field {0}. Message {1}", originalFieldName, ex.Message);
                }
            }
        }

        #region ARIA SPECIFIC METHODS

        void btnCleanStorageTest_Click(object sender, EventArgs e)
        {
            CleanStorage(true);
        }

        void btnCleanStorage_Click(object sender, EventArgs e)
        {
            CleanStorage(false);
        }

        private void btnChangeDpgTest_Click(object sender, EventArgs e)
        {
            ChangeDeliverypointgroup2(true);
        }

        private void btnChangeDpg_Click(object sender, EventArgs e)
        {
            ChangeDeliverypointgroup2(false);
        }

        private void btnRebootFloorNewTest_Click(object sender, EventArgs e)
        {
            RebootFloor(true, true);
        }

        private void btnRestartEmenuNewTest_Click(object sender, EventArgs e)
        {
            RestartEmenu(true, true);
        }

        private void btnRebootFloorNew_Click(object sender, EventArgs e)
        {
            RebootFloor(false, true);
        }

        private void btnRestartEmenuNew_Click(object sender, EventArgs e)
        {
            RestartEmenu(false, true);
        }

        private void btnREbootFloorTest_Click(object sender, EventArgs e)
        {
            RebootFloor(true, false);
        }

        private void btnRebootFloor_Click(object sender, EventArgs e)
        {
            RebootFloor(false, false);
        }

        private void btnDownloadUpdateDevicesLiveTest_Click(object sender, EventArgs e)
        {
            UpdateDevicesDownloadApk(true);
        }

        private void btnDownloadUpdateDevicesLive_Click(object sender, EventArgs e)
        {
            UpdateDevicesDownloadApk(false);
        }

        private void btnRestartEmenuTest_Click(object sender, EventArgs e)
        {
            RestartEmenu(true, false);
        }

        private void btnRestartEmenu_Click(object sender, EventArgs e)
        {
            RestartEmenu(false, false);
        }

        private void btnSupportToolsRebootTest_Click(object sender, EventArgs e)
        {
            SupportToolsReboot(true);
        }

        private void btnSupportToolsReboot_Click(object sender, EventArgs e)
        {
            SupportToolsReboot(false);
        }

        private void btnRestartSucceededLiveTest_Click(object sender, EventArgs e)
        {
            RestartSucceededUpdate(true);
        }

        private void btnUpdateDevicesLiveTest_Click(object sender, EventArgs e)
        {
            InstallEmenuUpdate(true);
        }

        private void btnRestartSucceededLive_Click(object sender, EventArgs e)
        {
            RestartSucceededUpdate(false);
        }

        private void btnUpdateDevicesLive_Click(object sender, EventArgs e)
        {
            InstallEmenuUpdate(false);
        }

        private void ChangeDeliverypointgroup2(bool test)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(DeliverypointFields.CompanyId == CmsSessionHelper.CurrentCompanyId); // Aria

            string roomNumbersString = this.tbChangeDpgRoomNumbers.Text;
            string[] roomNumbers = new string[] {};

            string floor = this.tbChangeDpgFloor.Text;
            string amount = this.tbChangeDpgAmount.Text;
            int amountInt = 0;

            if (!roomNumbersString.IsNullOrWhiteSpace())
            {
                roomNumbers = roomNumbersString.Split(",", StringSplitOptions.RemoveEmptyEntries);
                if (roomNumbers.Length == 0)
                {
                    this.lblChangeDpgRoomsChanged.Text = "Invalid room numbers";
                    return;
                }
                if (roomNumbers.Length > 1500)
                {
                    this.lblChangeDpgRoomsChanged.Text = "Too many room numbers. Can only process up to 1500 rooms at the same time.";
                    return;
                }
            }
            else
            {
                if (!floor.IsNullOrWhiteSpace() && !amount.IsNullOrWhiteSpace() && int.TryParse(amount, out amountInt))
                {
                    floor = floor.Trim();
                    if (CmsSessionHelper.CurrentCompanyId == 343)
                    {
                        if (!floor.Contains("-"))
                        {
                            floor += "-";
                        }
                    }

                    floor += "%";

                    filter.Add(DeliverypointFields.Name % floor);
                }
                else
                {
                    this.lblChangeDpgRoomsChanged.Text = "Invalid floor or amount";
                    return;
                }
            }

            if (this.ddlChangeDpgFromDpgId.SelectedIndex <= 0)
            {
                this.lblChangeDpgRoomsChanged.Text = "Please select from DPG";
                return;
            }
            if (this.ddlChangeDpgToDpgId.SelectedIndex <= 0)
            {
                this.lblChangeDpgRoomsChanged.Text = "Please select to DPG";
                return;
            }

            int dpgFromDpgId = (int)this.ddlChangeDpgFromDpgId.SelectedItem.Value;
            int dpgToDpgId = (int)this.ddlChangeDpgToDpgId.SelectedItem.Value;

            // DPG Filter
            filter.Add(DeliverypointFields.DeliverypointgroupId == dpgFromDpgId);
            
            if (roomNumbers.Length > 0)
                filter.Add(DeliverypointFields.Number == roomNumbers);

            PrefetchPath prefetch = new PrefetchPath(EntityType.DeliverypointEntity);
            prefetch.Add(DeliverypointEntityBase.PrefetchPathClientCollection);

            DeliverypointCollection deliverypointCollection = new DeliverypointCollection();
            deliverypointCollection.GetMulti(filter, prefetch);

            string result = string.Empty;
            if (test)
            {
                result = "<b>Test run, clients are not affected!</b><br/><br/>";
            }

            result += "Clients:<br/>";

            int count = 0;

            bool useDeliverypointNumberLookup = roomNumbers.Length > 0;
            
            foreach (DeliverypointEntity deliverypointEntity in deliverypointCollection)
            {
                string deliverypointValue = useDeliverypointNumberLookup ? deliverypointEntity.Number : deliverypointEntity.Name;

                foreach (ClientEntity clientEntity in deliverypointEntity.ClientCollection)
                {
                    DeliverypointEntity deliverypontEntity = this.GetDeliverypointEntity(deliverypointValue, dpgToDpgId, useDeliverypointNumberLookup);
                    if (deliverypontEntity != null)
                    {
                        if (clientEntity.DeviceEntity.ApplicationVersionAsVersionNumber.CompareTo(15111026) == VersionNumber.VersionState.Older)
                        {
                            result += string.Format("Skipping client: {0} - {1} - {2}<br/>", deliverypointValue, clientEntity.ClientId, clientEntity.DeviceEntity.ApplicationVersion);
                            continue;
                        }

                        if (test)
                        {
                            result += deliverypointValue + "<br />";
                            count++;
                            break;
                        }

                        clientEntity.DeliverypointId = deliverypontEntity.DeliverypointId;
                        clientEntity.DeliverypointGroupId = deliverypontEntity.DeliverypointgroupId;
                        if (clientEntity.Save())
                        {
                            result += deliverypointValue + "<br />";
                            count++;
                            break;
                        }
                    }
                }

                if (amountInt > 0 && count >= amountInt)
                {
                    break;
                }
            }

            result += "<br/>DPs with clients processed: " + count + "<br />";

            this.lblChangeDpgRoomsChanged.Text = result;
        }

        private void CopyRoomControlInfo(bool test)
        {
            if (this.cbRoomControlCopyDpgFrom.SelectedIndex <= 0)
            {
                this.lblCopyRoomControlInfo.Text = "Please select from DPG";
                return;
            }
            if (this.cbRoomControlCopyDpgTo.SelectedIndex <= 0)
            {
                this.lblCopyRoomControlInfo.Text = "Please select to DPG";
                return;
            }

            int dpgFromDpgId = (int)this.cbRoomControlCopyDpgFrom.SelectedItem.Value;
            int dpgToDpgId = (int)this.cbRoomControlCopyDpgTo.SelectedItem.Value;

            if (dpgFromDpgId == dpgToDpgId)
            {
                this.lblCopyRoomControlInfo.Text = "DPG can not be the same";
                return;
            }

            PredicateExpression filter = new PredicateExpression();
            filter.Add(DeliverypointFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

            // DPG Filter
            filter.Add(DeliverypointFields.DeliverypointgroupId == dpgFromDpgId);

            // Online client filter
            filter.Add(DeviceFields.LastRequestUTC > DateTime.UtcNow.AddMinutes(-3));

            RelationCollection relations = new RelationCollection();
            relations.Add(DeliverypointEntityBase.Relations.ClientEntityUsingDeliverypointId);
            relations.Add(ClientEntityBase.Relations.DeviceEntityUsingDeviceId);

            DeliverypointCollection deliverypointCollection = new DeliverypointCollection();
            deliverypointCollection.GetMulti(filter, relations);

            // Get to DPG DPs
            PredicateExpression filter2 = new PredicateExpression();
            filter2.Add(DeliverypointFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            filter2.Add(DeliverypointFields.DeliverypointgroupId == dpgToDpgId);

            DeliverypointCollection deliverypointCollectionTo = new DeliverypointCollection();
            deliverypointCollectionTo.GetMulti(filter2);
            EntityView<DeliverypointEntity> dpToView = deliverypointCollectionTo.DefaultView;

            int count = 0;
            foreach (DeliverypointEntity dpFrom in deliverypointCollection)
            {
                if (!dpFrom.RoomControllerType.HasValue || 
                    dpFrom.RoomControllerIp.IsNullOrEmpty() ||
                    !dpFrom.RoomControlConfigurationId.HasValue)
                {
                    continue;
                }

                DeliverypointEntity dpTo = dpToView.FirstOrDefault(dp => dp.Number == dpFrom.Number);
                if (dpTo == null) continue;

                if (!test)
                {
                    dpTo.RoomControllerType = dpFrom.RoomControllerType;
                    dpTo.RoomControllerIp = dpFrom.RoomControllerIp;
                    dpTo.RoomControlConfigurationId = dpFrom.RoomControlConfigurationId;
                    dpTo.Save();
                }

                count++;
            }
            
            this.lblCopyRoomControlInfo.Text = "Copied " + count + " DPs (test=" + test + ")";
        }

        private DeliverypointEntity GetDeliverypointEntity(string dpLookupValue, int deliverypointgroupId, bool useDeliverypointNumberLookup)
        {
            PredicateExpression filter = new PredicateExpression();

            if (useDeliverypointNumberLookup)
            {
                filter.Add(DeliverypointFields.Number == dpLookupValue);
            }
            else
            {
                filter.Add(DeliverypointFields.Name == dpLookupValue);
            }

            filter.Add(DeliverypointFields.DeliverypointgroupId == deliverypointgroupId);
            filter.Add(DeliverypointFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

            DeliverypointCollection deliverypointCollection = new DeliverypointCollection();
            deliverypointCollection.GetMulti(filter);

            if (deliverypointCollection.Count > 0)
            {
                return deliverypointCollection[0];
            }
            return null;
        }

        private void UpdateDevicesDownloadApk(bool test)
        {
            int companyInt;
            if (tbCompanyUpdate.Text.IsNullOrWhiteSpace() || !int.TryParse(tbCompanyUpdate.Text, out companyInt))
            {
                lblUpdateRooms.Text = "Invalid company id";
                return;
            }

            PredicateExpression filter = new PredicateExpression();
            filter.Add(ClientFields.CompanyId == companyInt);
            filter.Add(ClientFields.DeviceId != DBNull.Value);
            filter.Add(DeviceFields.ApplicationVersion < "15111026");
            filter.Add(DeviceFields.LastSupportToolsRequestUTC >= DateTime.UtcNow.AddMinutes(-3));

            PrefetchPath prefetch = new PrefetchPath(EntityType.ClientEntity);
            prefetch.Add(ClientEntity.PrefetchPathDeviceEntity);
            prefetch.Add(ClientEntity.PrefetchPathDeliverypointEntity);

            RelationCollection relations = new RelationCollection();
            relations.Add(ClientEntity.Relations.DeliverypointEntityUsingDeliverypointId);
            relations.Add(ClientEntityBase.Relations.DeviceEntityUsingDeviceId);

            string floor = this.tbFloorUpdate.Text;
            if (!floor.IsNullOrWhiteSpace())
            {
                if (floor.Contains("-") || companyInt != 343)
                {
                    floor = floor.Trim() + "%";
                }
                else
                {
                    floor = floor.Trim() + "-%";
                }
                filter.Add(DeliverypointFields.Name % floor);
            }
            else if (companyInt == 343)
            {
                this.lblUpdateRooms.Text = "Unknown floor";

                return;
            }

            string amount = this.tbAmountUpdate.Text;
            int amountInt;
            if (amount.IsNullOrWhiteSpace() || !int.TryParse(amount, out amountInt))
            {
                amountInt = 10;
            }

            ClientCollection clientCollection = new ClientCollection();
            clientCollection.GetMulti(filter, amountInt, null, relations, prefetch);

            string result = "Downloading:<br/>";
            if (test)
            {
                result += "<b>Test run, no clients are affected</b><br/>";
            }

            foreach (ClientEntity clientEntity in clientCollection)
            {
                if (clientEntity.DeviceId.HasValue && !clientEntity.DeviceEntity.Identifier.IsNullOrWhiteSpace() && clientEntity.DeliverypointId.HasValue)
                {
                    result += string.Format("{0} - {1}<br/>", clientEntity.ClientId, clientEntity.DeliverypointEntity.Name);

                    if (!test)
                    {
                        CometHelper.ClientCommand(clientEntity.ClientId, ClientCommand.DownloadEmenuUpdate, false);
                    }
                }
            }

            this.lblUpdateRooms.Text = result;
        }

        private void InstallEmenuUpdate(bool test)
        {
            int companyInt;
            if (tbCompanyUpdate.Text.IsNullOrWhiteSpace() || !int.TryParse(tbCompanyUpdate.Text, out companyInt))
            {
                lblUpdateRooms.Text = "Invalid company id";
                return;
            }

            PredicateExpression filter = new PredicateExpression();
            filter.Add(ClientFields.CompanyId == companyInt);
            filter.Add(ClientFields.DeviceId != DBNull.Value);
            filter.Add(DeviceFields.ApplicationVersion < "15111026");
            filter.Add(DeviceFields.LastSupportToolsRequestUTC >= DateTime.UtcNow.AddMinutes(-3));

            PredicateExpression subfilter = new PredicateExpression();
            subfilter.Add(ClientLogFields.Message == "Download completed for release with id '376', application 'Emenu' and version '15111026'.");
            subfilter.AddWithOr(ClientLogFields.Message % "% completed for release with id '376'%");
            subfilter.Add(ClientLogFields.CreatedUTC > DateTime.UtcNow.AddMinutes(-10));
            filter.Add(subfilter);

            string floor = this.tbFloorUpdate.Text;
            if (!floor.IsNullOrWhiteSpace())
            {
                if (floor.Contains("-") || companyInt != 343)
                {
                    floor = floor.Trim() + "%";
                }
                else
                {
                    floor = floor.Trim() + "-%";
                }
                filter.Add(DeliverypointFields.Name % floor);
            }
            else if (companyInt == 343)
            {
                this.lblUpdateRooms.Text = "Unknown floor";

                return;
            }

            RelationCollection relations = new RelationCollection();
            relations.Add(ClientEntity.Relations.ClientLogEntityUsingClientId);
            relations.Add(ClientEntity.Relations.DeliverypointEntityUsingDeliverypointId);
            relations.Add(ClientEntityBase.Relations.DeviceEntityUsingDeviceId);

            PrefetchPath prefetch = new PrefetchPath(EntityType.ClientEntity);
            prefetch.Add(ClientEntity.PrefetchPathDeviceEntity);
            prefetch.Add(ClientEntity.PrefetchPathDeliverypointEntity);

            ClientCollection clientCollection = new ClientCollection();
            clientCollection.GetMulti(filter, 0, null, relations, prefetch);

            string result = "Installing:<br/>";
            if (test)
            {
                result += "<b>Test run, no clients are affected</b><br/>";
            }

            foreach (ClientEntity clientEntity in clientCollection)
            {
                if (clientEntity.DeviceId.HasValue && !clientEntity.DeviceEntity.Identifier.IsNullOrWhiteSpace() && clientEntity.DeliverypointId.HasValue)
                {
                    result += string.Format("{0} - {1}<br/>", clientEntity.ClientId, clientEntity.DeliverypointEntity.Name);

                    if (!test)
                    {
                        /*var netmessage = new NetmessageAgentCommandRequest();
                        netmessage.Guid = GuidUtil.CreateGuid();
                        netmessage.AgentMacAddress = clientEntity.DeviceEntity.Identifier;
                        netmessage.Command = "installapp sdcard/Download/CraveEmenu.apk";
                        netmessage.ReceiverClientId = clientEntity.ClientId;
                        netmessage.SaveToDatabase = true;

                        try
                        {
                            CometHelper.Instance.SendMessage(netmessage);
                        }
                        catch (Exception)
                        {
                        }*/

                        CometHelper.ClientCommand(clientEntity.ClientId, ClientCommand.InstallEmenuUpdate, false);
                    }
                }
            }

            this.lblUpdateRooms.Text = result;
        }
            
        private void RestartSucceededUpdate(bool test)
        {
            int companyInt;
            if (tbCompanyUpdate.Text.IsNullOrWhiteSpace() || !int.TryParse(tbCompanyUpdate.Text, out companyInt))
            {
                lblUpdateRooms.Text = "Invalid company id";
                return;
            }

            PredicateExpression filter = new PredicateExpression();
            filter.Add(ClientFields.CompanyId == companyInt);
            filter.Add(DeviceFields.ApplicationVersion < "15111026");
            filter.Add(DeviceFields.LastSupportToolsRequestUTC >= DateTime.UtcNow.AddMinutes(-3));
            filter.Add(ClientLogFields.Message % "Installation started for release with id '376'%");

            string floor = this.tbFloorUpdate.Text;
            if (!floor.IsNullOrWhiteSpace())
            {
                if (floor.Contains("-") || companyInt != 343)
                {
                    floor = floor.Trim() + "%";
                }
                else
                {
                    floor = floor.Trim() + "-%";
                }
                filter.Add(DeliverypointFields.Name % floor);
            }
            else if (companyInt == 343)
            {
                this.lblUpdateRooms.Text = "Unknown floor";

                return;
            }

            PrefetchPath prefetch = new PrefetchPath(EntityType.ClientEntity);
            prefetch.Add(ClientEntity.PrefetchPathDeviceEntity);
            prefetch.Add(ClientEntity.PrefetchPathDeliverypointEntity);

            RelationCollection relations = new RelationCollection();
            relations.Add(ClientEntity.Relations.ClientLogEntityUsingClientId);
            relations.Add(ClientEntity.Relations.DeliverypointEntityUsingDeliverypointId);
            relations.Add(ClientEntityBase.Relations.DeviceEntityUsingDeviceId);

            ClientCollection clientCollection = new ClientCollection();
            clientCollection.GetMulti(filter, 0, null, relations, prefetch);

            string result = "Restarting:<br/>";
            if (test)
            {
                result += "<b>Test run, no clients are affected</b><br/>";
            }
            foreach (ClientEntity clientEntity in clientCollection)
            {
                if (!test)
                {
                    if (clientEntity.DeviceId.HasValue && !clientEntity.DeviceEntity.Identifier.IsNullOrWhiteSpace() && clientEntity.DeliverypointId.HasValue)
                    {
                        NetmessageAgentCommandRequest netmessage = new NetmessageAgentCommandRequest();
                        netmessage.Guid = GuidUtil.CreateGuid();
                        netmessage.AgentMacAddress = clientEntity.DeviceEntity.Identifier;
                        netmessage.Command = "reboot";
                        netmessage.ReceiverClientId = clientEntity.ClientId;
                        netmessage.SaveToDatabase = true;

                        try
                        {
                            CometHelper.Instance.SendMessage(netmessage);
                        }
                        catch (Exception)
                        {
                        }

                        //CometHelper.ClientCommand(clientEntity.ClientId, ClientCommand.RestartDevice, false);
                    }
                }

                result += string.Format("{0} - {1}<br/>", clientEntity.ClientId, clientEntity.DeliverypointEntity.Name);
            }

            this.lblUpdateRooms.Text = result;
        }

        private void SupportToolsReboot(bool test)
        {
            int companyInt;
            if (tbSupportToolsCompany.Text.IsNullOrWhiteSpace() || !int.TryParse(tbSupportToolsCompany.Text, out companyInt))
            {
                lblSupportToolsDevices.Text = "Invalid company id";
                return;
            }

            PredicateExpression filter = new PredicateExpression();
            filter.Add(ClientFields.CompanyId == companyInt);
            filter.Add(ClientFields.DeviceId != DBNull.Value);
            //filter.Add(ClientFields.LastApplicationVersion < "15111026");
            filter.Add(DeviceFields.LastRequestUTC >= DateTime.UtcNow.AddMinutes(-3));
            filter.Add(DeviceFields.LastSupportToolsRequestUTC < DateTime.UtcNow.AddMinutes(-3));

            PrefetchPath prefetch = new PrefetchPath(EntityType.ClientEntity);
            prefetch.Add(ClientEntity.PrefetchPathDeviceEntity);
            prefetch.Add(ClientEntity.PrefetchPathDeliverypointEntity);

            RelationCollection relations = new RelationCollection();
            relations.Add(ClientEntity.Relations.DeliverypointEntityUsingDeliverypointId);
            relations.Add(ClientEntity.Relations.DeviceEntityUsingDeviceId);

            string floor = this.tbSupportToolsFloor.Text;
            if (!floor.IsNullOrWhiteSpace())
            {
                if (floor.Contains("-") || companyInt != 343)
                {
                    floor = floor.Trim() + "%";
                }
                else
                {
                    floor = floor.Trim() + "-%";
                }
                filter.Add(DeliverypointFields.Name % floor);
            }
            else if (companyInt == 343)
            {
                lblSupportToolsDevices.Text = "Unknown floor";
                return;
            }

            ClientCollection clientCollection = new ClientCollection();
            clientCollection.GetMulti(filter, 0, null, relations, prefetch);

            string result = "Clients:<br/>";
            if (test)
            {
                result += "<b>Test run, no clients are affected</b><br/>";
            }

            foreach (ClientEntity clientEntity in clientCollection)
            {
                if (clientEntity.DeviceId.HasValue && !clientEntity.DeviceEntity.Identifier.IsNullOrWhiteSpace() && clientEntity.DeliverypointId.HasValue)
                {
                    result += string.Format("{0} - {1}<br/>", clientEntity.ClientId, clientEntity.DeliverypointEntity.Name);

                    if (!test)
                    {
                        NetmessageAgentCommandRequest netmessage = new NetmessageAgentCommandRequest();
                        netmessage.Guid = GuidUtil.CreateGuid();
                        netmessage.AgentMacAddress = clientEntity.DeviceEntity.Identifier;
                        netmessage.Command = "reboot";
                        netmessage.ReceiverClientId = clientEntity.ClientId;
                        netmessage.SaveToDatabase = true;

                        try
                        {
                            CometHelper.Instance.SendMessage(netmessage);
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }

            lblSupportToolsDevices.Text = result;
        }

        private void RestartEmenu(bool test, bool onlyNewVersions)
        {
            int companyInt;
            if (tbRestartCompany.Text.IsNullOrWhiteSpace() || !int.TryParse(tbRestartCompany.Text, out companyInt))
            {
                lblRestartedEmenu.Text = "Invalid company id";
                return;
            }

            PredicateExpression filter = new PredicateExpression();
            filter.Add(ClientFields.CompanyId == companyInt);
            filter.Add(ClientFields.DeviceId != DBNull.Value);
            filter.Add(DeviceFields.LastRequestUTC >= DateTime.UtcNow.AddMinutes(-3));

            if (onlyNewVersions)
            {
                filter.Add(DeviceFields.ApplicationVersion == "15111026");
            }

            PrefetchPath prefetch = new PrefetchPath(EntityType.ClientEntity);
            prefetch.Add(ClientEntity.PrefetchPathDeviceEntity);
            prefetch.Add(ClientEntity.PrefetchPathDeliverypointEntity);

            RelationCollection relations = new RelationCollection();
            relations.Add(ClientEntity.Relations.DeliverypointEntityUsingDeliverypointId);
            relations.Add(ClientEntity.Relations.DeviceEntityUsingDeviceId);

            string floor = this.tbRestartFloor.Text;
            if (!floor.IsNullOrWhiteSpace())
            {
                if (floor.Contains("-") || companyInt != 343)
                {
                    floor = floor.Trim() + "%";
                }
                else
                {
                    floor = floor.Trim() + "-%";
                }
                filter.Add(DeliverypointFields.Name % floor);
            }
            else if (companyInt == 343)
            {
                lblRestartedEmenu.Text = "Invalid floor";
                return;
            }

            ClientCollection clientCollection = new ClientCollection();
            clientCollection.GetMulti(filter, 0, null, relations, prefetch);

            string result = string.Format("Clients: {0}<br/>", clientCollection.Count);
            if (test)
            {
                result += "<b>Test run, no clients are affected</b><br/>";
            }

            foreach (ClientEntity clientEntity in clientCollection)
            {
                if (clientEntity.DeviceId.HasValue && !clientEntity.DeviceEntity.Identifier.IsNullOrWhiteSpace() && clientEntity.DeliverypointId.HasValue)
                {
                    result += string.Format("{0} - {1}<br/>", clientEntity.ClientId, clientEntity.DeliverypointEntity.Name);

                    if (!test)
                    {
                        CometHelper.ClientCommand(clientEntity.ClientId, ClientCommand.RestartApplication, false);
                    }
                }
            }

            lblRestartedEmenu.Text = result;
        }

        private void RebootFloor(bool test, bool onlyOldVersions)
        {
            int companyInt;
            if (tbRebootCompany.Text.IsNullOrWhiteSpace() || !int.TryParse(tbRebootCompany.Text, out companyInt))
            {
                lblRebootFloor.Text = "Invalid company id";
                return;
            }

            DateTime lastRequestUtc = DateTime.UtcNow.AddMinutes(-3);

            PredicateExpression filter = new PredicateExpression();
            filter.Add(ClientFields.CompanyId == companyInt);
            filter.Add(ClientFields.DeviceId != DBNull.Value);

            if (onlyOldVersions)
            {
                filter.Add(DeviceFields.ApplicationVersion < "15111026");
            }

            PredicateExpression subFilter = new PredicateExpression();
            subFilter.Add(DeviceFields.LastRequestUTC >= lastRequestUtc);
            subFilter.AddWithOr(DeviceFields.LastSupportToolsRequestUTC >= lastRequestUtc);

            PrefetchPath prefetch = new PrefetchPath(EntityType.ClientEntity);
            prefetch.Add(ClientEntity.PrefetchPathDeviceEntity);
            prefetch.Add(ClientEntity.PrefetchPathDeliverypointEntity);

            RelationCollection relations = new RelationCollection();
            relations.Add(ClientEntity.Relations.DeliverypointEntityUsingDeliverypointId);
            relations.Add(ClientEntity.Relations.DeviceEntityUsingDeviceId);

            string floor = this.tbRebootFloor.Text;
            if (!floor.IsNullOrWhiteSpace())
            {
                if (floor.Contains("-") || companyInt != 343)
                {
                    floor = floor.Trim() + "%";
                }
                else
                {
                    floor = floor.Trim() + "-%";
                }
                filter.Add(DeliverypointFields.Name % floor);
            }
            else if (companyInt == 343)
            {
                lblRebootFloor.Text = "Unknown floor";
                return;
            }

            ClientCollection clientCollection = new ClientCollection();
            clientCollection.GetMulti(filter, 0, null, relations, prefetch);

            string result = "Rebooting devices:<br/>";
            if (test)
            {
                result += "<b>Test run, no devices are affected</b><br/>";
            }

            foreach (ClientEntity clientEntity in clientCollection)
            {
                if (clientEntity.DeviceId.HasValue && !clientEntity.DeviceEntity.Identifier.IsNullOrWhiteSpace() && clientEntity.DeliverypointId.HasValue)
                {
                    result += string.Format("{0} - {1}<br/>", clientEntity.ClientId, clientEntity.DeliverypointEntity.Name);

                    if (clientEntity.DeviceEntity.LastSupportToolsRequestUTC >= lastRequestUtc)
                    {
                        if (!test)
                        {
                            NetmessageAgentCommandRequest netmessage = new NetmessageAgentCommandRequest();
                            netmessage.Guid = GuidUtil.CreateGuid();
                            netmessage.AgentMacAddress = clientEntity.DeviceEntity.Identifier;
                            netmessage.Command = "reboot";
                            netmessage.ReceiverClientId = clientEntity.ClientId;
                            netmessage.SaveToDatabase = true;

                            try
                            {
                                CometHelper.Instance.SendMessage(netmessage);
                            }
                            catch (Exception)
                            {
                            }
                        }
                    }
                    else
                    {
                        CometHelper.ClientCommand(clientEntity.ClientId, ClientCommand.RestartDevice, false);
                    }
                }
            }

            lblRebootFloor.Text = result;
        }

        private void CleanStorage(bool isTest)
        {
            int companyInt;
            if (tbCleanStorageCompany.Text.IsNullOrWhiteSpace() || !int.TryParse(tbCleanStorageCompany.Text, out companyInt))
            {
                lblCleanStorage.Text = "Invalid company id";
                return;
            }

            PredicateExpression filter = new PredicateExpression();
            filter.Add(ClientFields.CompanyId == companyInt);
            filter.Add(DeviceFields.LastSupportToolsRequestUTC >= DateTime.UtcNow.AddMinutes(-3));

            PrefetchPath prefetch = new PrefetchPath(EntityType.ClientEntity);
            prefetch.Add(ClientEntity.PrefetchPathDeliverypointEntity);

            RelationCollection relations = new RelationCollection();
            relations.Add(ClientEntity.Relations.DeliverypointEntityUsingDeliverypointId);
            relations.Add(ClientEntityBase.Relations.DeviceEntityUsingDeviceId);

            ClientCollection clientCollection = new ClientCollection();
            clientCollection.GetMulti(filter, 0, null, relations, prefetch);

            string result = string.Format("Cleaning Storage ({0} clients):<br/>", clientCollection.Count);
            if (isTest)
                result += "<b>Test run, no clients are affected</b><br/>";

            foreach (ClientEntity clientEntity in clientCollection)
            {
                if (clientEntity.DeliverypointId > 0)
                    result += string.Format("{0} - {1} - {2}<br/>", clientEntity.ClientId, clientEntity.DeliverypointEntity.Number, clientEntity.DeliverypointEntity.Name);
                else
                    result += string.Format("{0} - NO DP<br/>", clientEntity.ClientId);

                if (!isTest)
                {
                    NetmessageAgentCommandRequest netmessage = new NetmessageAgentCommandRequest();
                    netmessage.Guid = GuidUtil.CreateGuid();
                    netmessage.ReceiverIdentifier = clientEntity.DeviceEntity.Identifier;
                    netmessage.AgentMacAddress = clientEntity.DeviceEntity.Identifier;
                    netmessage.Command = "shell rm -rf sdcard/Log/* && rm -rf sdcard/logs/*";
                    netmessage.ReceiverClientId = clientEntity.ClientId;
                    netmessage.SaveToDatabase = false;

                    try
                    {
                        CometHelper.Instance.SendMessage(netmessage);
                    }
                    catch (Exception)
                    {
                    }
                }
            }

            this.lblCleanStorage.Text = result;
        }

        private void RestartClientsStuckInLoading(bool isTest)
        {
            int companyInt;
            if (this.tbRestartUnloadedClientsCompany.Text.IsNullOrWhiteSpace() || !int.TryParse(this.tbRestartUnloadedClientsCompany.Text, out companyInt))
            {
                this.lblUnloadedClients.Text = "Invalid company id";
                return;
            }

            PredicateExpression filter = new PredicateExpression();
            filter.Add(ClientFields.CompanyId == companyInt);
            filter.Add(DeviceFields.LastSupportToolsRequestUTC >= DateTime.UtcNow.AddMinutes(-3));
            filter.Add(new FieldLikePredicate(ClientFields.Notes, "%loaded:false%"));

            PrefetchPath prefetch = new PrefetchPath(EntityType.ClientEntity);
            prefetch.Add(ClientEntityBase.PrefetchPathDeliverypointEntity);

            RelationCollection relations = new RelationCollection();
            relations.Add(ClientEntityBase.Relations.DeliverypointEntityUsingDeliverypointId);
            relations.Add(ClientEntityBase.Relations.DeviceEntityUsingDeviceId);

            ClientCollection clientCollection = new ClientCollection();
            clientCollection.GetMulti(filter, 0, null, relations, prefetch);

            string result = string.Format("Cleaning Storage ({0} clients):<br/>", clientCollection.Count);
            if (isTest)
                result += "<b>Test run, no clients are affected</b><br/>";

            string restartEmenuCommand = "closeapp -r net.craveinteractive.android.client";
            string clearLogsCommand = "shell rm -rf sdcard/Log/* ** shell rm -rf sdcard/logs/*";

            List<NetmessageAgentCommandRequest> netmessages = new List<NetmessageAgentCommandRequest>();
            foreach (ClientEntity clientEntity in clientCollection)
            {
                if (clientEntity.DeliverypointId > 0)
                    result += string.Format("{0} - {1} - {2}<br/>", clientEntity.ClientId, clientEntity.DeliverypointEntity.Number, clientEntity.DeliverypointEntity.Name);
                else
                    result += string.Format("{0} - NO DP<br/>", clientEntity.ClientId);

                if (!isTest)
                {
                    string command = string.Empty;

                    if (this.cbUnloadedClientsClearLogs.Checked)
                    {
                        command += clearLogsCommand;
                        command += " ** ";
                    }

                    if (this.cbUnloadedClientsClearCachedXmlFiles.Checked && clientEntity.DeliverypointGroupId.HasValue)
                    {
                        command += string.Format("shell rm -rf sdcard/CraveEmenu/{0}-{1}/* ** ", companyInt, clientEntity.DeliverypointGroupId);
                    }

                    command += restartEmenuCommand;

                    NetmessageAgentCommandRequest netmessage = new NetmessageAgentCommandRequest();
                    netmessage.Guid = GuidUtil.CreateGuid();
                    netmessage.ReceiverIdentifier = clientEntity.DeviceEntity.Identifier;
                    netmessage.AgentMacAddress = clientEntity.DeviceEntity.Identifier;
                    netmessage.Command = command;
                    netmessage.ReceiverClientId = clientEntity.ClientId;
                    netmessage.SaveToDatabase = false;

                    netmessages.Add(netmessage);
                }
            }

            int seconds = int.Parse(this.tbNumberOfSeconds.Text) * 1000;
            int clients = int.Parse(this.tbNumberOfClients.Text);

            int count = 0;
            Task.Factory.StartNew(() =>
            {
                foreach (NetmessageAgentCommandRequest netmessage in netmessages)
                {
                    if (count == clients)
                    {
                        Thread.Sleep(seconds);
                        count = 0;
                    }
                    CometHelper.Instance.SendMessage(netmessage);
                    count++;
                }
            }, TaskCreationOptions.LongRunning);

            this.lblUnloadedClients.Text = result;
        }

        public CompanyCollection GetCompanyWithoutCultureCode()
        {
            PredicateExpression filter = new PredicateExpression();

            PrefetchPath prefetch = new PrefetchPath(EntityType.CompanyEntity);
            prefetch.Add(CompanyEntityBase.PrefetchPathLanguageEntity);
            prefetch.Add(CompanyEntityBase.PrefetchPathCompanyLanguageCollection);
            prefetch.Add(CompanyEntityBase.PrefetchPathCompanyCultureCollection);

            CompanyCollection companies = new CompanyCollection();
            companies.GetMulti(filter, prefetch);

            return companies;
        }

        public SiteCollection GetSitesWithoutSiteCultures()
        {
            PrefetchPath prefetch = new PrefetchPath(EntityType.SiteEntity);
            prefetch.Add(SiteEntityBase.PrefetchPathSiteCultureCollection);
            prefetch.Add(SiteEntityBase.PrefetchPathSiteLanguageCollection);

            SiteCollection sites = new SiteCollection();
            sites.GetMulti(null, prefetch);

            SiteCollection toReturn = new SiteCollection();
            foreach (SiteEntity site in sites)
            {
                if (site.SiteCultureCollection.Count == 0)
                {
                    toReturn.Add(site);
                }
            }
            return toReturn;
        }

        public MediaCollection GetMediaWithoutMediaCultures()
        {
            PrefetchPath prefetch = new PrefetchPath(EntityType.MediaEntity);
            prefetch.Add(MediaEntityBase.PrefetchPathMediaCultureCollection);
            prefetch.Add(MediaEntityBase.PrefetchPathMediaLanguageCollection);
            prefetch.Add(MediaEntityBase.PrefetchPathAttachmentEntity);

            RelationCollection relations = new RelationCollection();
            relations.Add(MediaEntityBase.Relations.MediaLanguageEntityUsingMediaId);

            MediaCollection medias = new MediaCollection();
            medias.GetMulti(null, 0, null, relations, prefetch);

            MediaCollection toReturn = new MediaCollection();
            foreach (MediaEntity media in medias)
            {
                if (media.MediaCultureCollection.Count == 0)
                {
                    toReturn.Add(media);
                }

            }
            return toReturn;
        }

        public SiteTemplateCollection GetSiteTemplatesWithoutSiteTemplateCultures()
        {
            PrefetchPath prefetch = new PrefetchPath(EntityType.SiteTemplateEntity);
            prefetch.Add(SiteTemplateEntityBase.PrefetchPathSiteTemplateCultureCollection);
            prefetch.Add(SiteTemplateEntityBase.PrefetchPathSiteTemplateLanguageCollection);

            SiteTemplateCollection siteTemplates = new SiteTemplateCollection();
            siteTemplates.GetMulti(null, prefetch);

            SiteTemplateCollection toReturn = new SiteTemplateCollection();
            foreach (SiteTemplateEntity siteTemplate in siteTemplates)
            {
                if (siteTemplate.SiteTemplateCultureCollection.Count == 0)
                {
                    toReturn.Add(siteTemplate);
                }
            }
            return toReturn;
        }

        public PageElementCollection GetPageElementsWithLanguageButWithoutCultureCode()
        {
            PredicateExpression filter = new PredicateExpression(PageElementFields.CultureCode == DBNull.Value);
            filter.Add(PageElementFields.LanguageId != DBNull.Value);

            PageElementCollection pageElements = new PageElementCollection();
            pageElements.GetMulti(filter);

            return pageElements;
        }

        public PageTemplateElementCollection GetPageTemplateElementsWithLanguageButWithoutCultureCode()
        {
            PredicateExpression filter = new PredicateExpression(PageTemplateElementFields.CultureCode == DBNull.Value);
            filter.Add(PageTemplateElementFields.LanguageId != DBNull.Value);

            PageTemplateElementCollection pageTemplateElements = new PageTemplateElementCollection();
            pageTemplateElements.GetMulti(filter);

            return pageTemplateElements;
        }

        public PrefetchPath GetCustomTextsWithoutCultureCodePrefetch()
        {
            IncludeFieldsList includesActionButton = new IncludeFieldsList();
            IncludeFieldsList includesAdvertisement = new IncludeFieldsList(AdvertisementFields.CompanyId);
            IncludeFieldsList includesAlteration = new IncludeFieldsList(AlterationFields.CompanyId);
            IncludeFieldsList includesAlterationoption = new IncludeFieldsList(AlterationoptionFields.CompanyId);
            IncludeFieldsList includesAmenity = new IncludeFieldsList();
            IncludeFieldsList includesAnnouncement = new IncludeFieldsList(AnnouncementFields.CompanyId);
            IncludeFieldsList includesAvailability = new IncludeFieldsList(AvailabilityFields.CompanyId);
            IncludeFieldsList includesCategory = new IncludeFieldsList(CategoryFields.CompanyId);
            IncludeFieldsList includesCompany = new IncludeFieldsList(CompanyFields.CompanyId);
            IncludeFieldsList includesDeliverypointgroup = new IncludeFieldsList(DeliverypointgroupFields.CompanyId);
            IncludeFieldsList includesEntertainmentcategory = new IncludeFieldsList();
            IncludeFieldsList includesGenericcategory = new IncludeFieldsList();
            IncludeFieldsList includesGenericproduct = new IncludeFieldsList();
            IncludeFieldsList includesPage = new IncludeFieldsList(PageFields.ParentCompanyId);
            IncludeFieldsList includesPageTemplate = new IncludeFieldsList();
            IncludeFieldsList includesPointOfInterest = new IncludeFieldsList();
            IncludeFieldsList includesRoomControlArea = new IncludeFieldsList(RoomControlAreaFields.ParentCompanyId);
            IncludeFieldsList includesRoomControlComponent = new IncludeFieldsList(RoomControlComponentFields.ParentCompanyId);
            IncludeFieldsList includesRoomControlSection = new IncludeFieldsList(RoomControlSectionFields.ParentCompanyId);
            IncludeFieldsList includesRoomControlSectionItem = new IncludeFieldsList(RoomControlSectionItemFields.ParentCompanyId);
            IncludeFieldsList includesRoomControlWidget = new IncludeFieldsList(RoomControlWidgetFields.ParentCompanyId);
            IncludeFieldsList includesProduct = new IncludeFieldsList(ProductFields.CompanyId);
            IncludeFieldsList includesSite = new IncludeFieldsList(SiteFields.CompanyId);
            IncludeFieldsList includesSiteTemplate = new IncludeFieldsList();
            IncludeFieldsList includesStation = new IncludeFieldsList(StationFields.ParentCompanyId);
            IncludeFieldsList includesUIFooterItem = new IncludeFieldsList(UIFooterItemFields.ParentCompanyId);
            IncludeFieldsList includesUITab = new IncludeFieldsList(UITabFields.ParentCompanyId);
            IncludeFieldsList includesUIWidget = new IncludeFieldsList(UIWidgetFields.ParentCompanyId);
            IncludeFieldsList includesVenueCategory = new IncludeFieldsList();

            PrefetchPath prefetch = new PrefetchPath(EntityType.CustomTextEntity);
            IPrefetchPathElement prefetchActionButton = prefetch.Add(CustomTextEntityBase.PrefetchPathActionButtonEntity, includesActionButton);
            IPrefetchPathElement prefetchAdvertisement = prefetch.Add(CustomTextEntityBase.PrefetchPathAdvertisementEntity, includesAdvertisement);
            IPrefetchPathElement prefetchAlteration = prefetch.Add(CustomTextEntityBase.PrefetchPathAlterationEntity, includesAlteration);
            IPrefetchPathElement prefetchAlterationoption = prefetch.Add(CustomTextEntityBase.PrefetchPathAlterationoptionEntity, includesAlterationoption);
            IPrefetchPathElement prefetchAmenity = prefetch.Add(CustomTextEntityBase.PrefetchPathAmenityEntity, includesAmenity);
            IPrefetchPathElement prefetchAnnouncement = prefetch.Add(CustomTextEntityBase.PrefetchPathAnnouncementEntity, includesAnnouncement);
            IPrefetchPathElement prefetchAttachment = prefetch.Add(CustomTextEntityBase.PrefetchPathAttachmentEntity);
            IPrefetchPathElement prefetchAvailability = prefetch.Add(CustomTextEntityBase.PrefetchPathAvailabilityEntity, includesAvailability);
            IPrefetchPathElement prefetchCategory = prefetch.Add(CustomTextEntityBase.PrefetchPathCategoryEntity, includesCategory);
            IPrefetchPathElement prefetchCompany = prefetch.Add(CustomTextEntityBase.PrefetchPathCompanyEntity, includesCompany);
            IPrefetchPathElement prefetchDeliverypointgroup = prefetch.Add(CustomTextEntityBase.PrefetchPathDeliverypointgroupEntity, includesDeliverypointgroup);
            IPrefetchPathElement prefetchEntertainmentcategory = prefetch.Add(CustomTextEntityBase.PrefetchPathEntertainmentcategoryEntity, includesEntertainmentcategory);
            IPrefetchPathElement prefetchGenericcategory = prefetch.Add(CustomTextEntityBase.PrefetchPathGenericcategoryEntity, includesGenericcategory);
            IPrefetchPathElement prefetchGenericproduct = prefetch.Add(CustomTextEntityBase.PrefetchPathGenericproductEntity, includesGenericproduct);
            IPrefetchPathElement prefetchPage = prefetch.Add(CustomTextEntityBase.PrefetchPathPageEntity, includesPage);
            IPrefetchPathElement prefetchPageTemplate = prefetch.Add(CustomTextEntityBase.PrefetchPathPageTemplateEntity, includesPageTemplate);
            IPrefetchPathElement prefetchPointOfInterest = prefetch.Add(CustomTextEntityBase.PrefetchPathPointOfInterestEntity, includesPointOfInterest);
            IPrefetchPathElement prefetchRoomControlArea = prefetch.Add(CustomTextEntityBase.PrefetchPathRoomControlAreaEntity, includesRoomControlArea);
            IPrefetchPathElement prefetchRoomControlComponent = prefetch.Add(CustomTextEntityBase.PrefetchPathRoomControlComponentEntity, includesRoomControlComponent);
            IPrefetchPathElement prefetchRoomControlSection = prefetch.Add(CustomTextEntityBase.PrefetchPathRoomControlSectionEntity, includesRoomControlSection);
            IPrefetchPathElement prefetchRoomControlSectionItem = prefetch.Add(CustomTextEntityBase.PrefetchPathRoomControlSectionItemEntity, includesRoomControlSectionItem);
            IPrefetchPathElement prefetchRoomControlWidget = prefetch.Add(CustomTextEntityBase.PrefetchPathRoomControlWidgetEntity, includesRoomControlWidget);
            IPrefetchPathElement prefetchProduct = prefetch.Add(CustomTextEntityBase.PrefetchPathProductEntity, includesProduct);
            IPrefetchPathElement prefetchSite = prefetch.Add(CustomTextEntityBase.PrefetchPathSiteEntity, includesSite);
            IPrefetchPathElement prefetchSiteTemplate = prefetch.Add(CustomTextEntityBase.PrefetchPathSiteTemplateEntity, includesSiteTemplate);
            IPrefetchPathElement prefetchStation = prefetch.Add(CustomTextEntityBase.PrefetchPathStationEntity, includesStation);
            IPrefetchPathElement prefetchUIFooterItem = prefetch.Add(CustomTextEntityBase.PrefetchPathUIFooterItemEntity, includesUIFooterItem);
            IPrefetchPathElement prefetchUITab = prefetch.Add(CustomTextEntityBase.PrefetchPathUITabEntity, includesUITab);
            IPrefetchPathElement prefetchUIWidget = prefetch.Add(CustomTextEntityBase.PrefetchPathUIWidgetEntity, includesUIWidget);
            IPrefetchPathElement prefetchVenueCategory = prefetch.Add(CustomTextEntityBase.PrefetchPathVenueCategoryEntity, includesVenueCategory);

            return prefetch;
        }

        private void CreateDefaultCompanyCurrencies()
        {
            //Transaction transaction = new Transaction(IsolationLevel.ReadUncommitted, "CreateDefaultCompanyCurrencies");
            //try
            //{
                CompanyCollection companiesWithoutCurrencyCollection = this.GetCompaniesWithoutCompanyCurrencies();
                foreach (CompanyEntity company in companiesWithoutCurrencyCollection)
                {
                    // Company
                    company.CurrencyCode = company.CurrencyEntity.CodeIso4217;
                    //company.AddToTransaction(transaction);
                    company.Save();

                    // CompanyCurrency
                    CompanyCurrencyEntity currency = new CompanyCurrencyEntity();
                    currency.CompanyId = company.CompanyId;
                    currency.CurrencyCode = company.CurrencyEntity.CodeIso4217;                    
                    currency.ExchangeRate = 1;
                    //currency.AddToTransaction(transaction);
                    currency.Save();
                }

            //    transaction.Commit();
            //}
            //catch (Exception ex)
            //{
            //    transaction.Rollback();
            //    throw ex;
            //}
            //finally
            //{
            //    transaction.Dispose();
            //}
        }

        private CompanyCollection GetCompaniesWithoutCompanyCurrencies()
        {
            PrefetchPath prefetch = new PrefetchPath(EntityType.CompanyEntity);
            prefetch.Add(CompanyEntityBase.PrefetchPathCurrencyEntity);
            prefetch.Add(CompanyEntityBase.PrefetchPathCompanyCurrencyCollection);

            CompanyCollection allCompanies = new CompanyCollection();
            allCompanies.GetMulti(null, prefetch);

            CompanyCollection companiesWithoutCurrencyCollection = new CompanyCollection();
            
            foreach (CompanyEntity company in allCompanies)
            {
                if (company.CompanyCurrencyCollection.Count == 0)
                {
                    companiesWithoutCurrencyCollection.Add(company);
                }
            }

            return companiesWithoutCurrencyCollection;
        }

        private CompanyCollection GetCompaniesWithCompanyLanguages()
        {
            PrefetchPath prefetch = new PrefetchPath(EntityType.CompanyEntity);
            prefetch.Add(CompanyEntityBase.PrefetchPathCompanyLanguageCollection);

            CompanyCollection companies = new CompanyCollection();
            companies.GetMulti(null, prefetch);

            return companies;
        }

        private CustomTextCollection GetCustomTextsWithLanguageId()
        {
            PredicateExpression filter = new PredicateExpression(CustomTextFields.LanguageId != DBNull.Value);

            CustomTextCollection customTexts = new CustomTextCollection();
            customTexts.GetMulti(filter);

            return customTexts;
        }

        private void CustomTextCleanup()
        {
            //Transaction transaction = new Transaction(IsolationLevel.ReadUncommitted, "CustomTextCleanup");
            //try
            //{
                // Get companies with their CompanyLanguage collection                
                Dictionary<int, CompanyLanguageCollection> companyLanguages = new Dictionary<int, CompanyLanguageCollection>();
                foreach (CompanyEntity company in this.GetCompaniesWithCompanyLanguages())
                {
                    companyLanguages.Add(company.CompanyId, company.CompanyLanguageCollection);
                }

                // Get all custom texts with a languageId
                CustomTextCollection customTextsWithLanguageId = this.GetCustomTextsWithLanguageId();
                foreach (CustomTextEntity customText in customTextsWithLanguageId)
                {
                    if (customText.LanguageId != null)
                    {
                        //customText.AddToTransaction(transaction);

                        // Check if the related entity has a companyId and if that company even has the languageId configured
                        // Otherwise we can remove the custom text
                        int companyId = customText.ParentCompanyId.GetValueOrDefault(0);
                        if (companyId > 0 && companyLanguages.ContainsKey(companyId) && companyLanguages[companyId].All(x => x.LanguageId != customText.LanguageId))
                        {
                            customText.Delete();
                        }
                        else
                        {
                            customText.LanguageId = null;
                            customText.Save();
                        }
                    }
                }

            //    transaction.Commit();
            //}
            //catch (Exception ex)
            //{
            //    transaction.Rollback();
            //    throw ex;
            //}
            //finally
            //{
            //    transaction.Dispose();
            //}
        }

        private void ConvertTimeZones()
        {
            //Transaction transaction = new Transaction(IsolationLevel.ReadUncommitted, "ConvertTimeZones");
            //try
            //{
                // Companies
                CompanyCollection companies = this.GetCompaniesWithoutTimeZoneOlsonId();
                foreach (CompanyEntity company in companies)
                {
                    if (company.TimeZoneOlsonId.IsNullOrWhiteSpace() && company.TimeZoneId.HasValue)
                    {
                        company.TimeZoneOlsonId = this.GetTimeZoneOlsonIdForNameAndroid(company.TimeZoneEntity.NameAndroid);
                        //company.AddToTransaction(transaction);
                        company.Save();
                    }                    
                }

                // Users
                UserCollection users = this.GetUsersWithoutTimeZoneOlsonId();
                foreach (UserEntity user in users)
                {
                    if (user.TimeZoneOlsonId.IsNullOrWhiteSpace() && user.TimeZoneId.HasValue)
                    {
                        user.TimeZoneOlsonId = this.GetTimeZoneOlsonIdForNameAndroid(user.TimeZoneEntity.NameAndroid);
                        //user.AddToTransaction(transaction);
                        user.Save();
                    }
                }

            //    transaction.Commit();
            //}
            //catch (Exception ex)
            //{
            //    transaction.Rollback();
            //    throw ex;
            //}
            //finally
            //{
            //    transaction.Dispose();
            //}
        }

        private void CreateDefaultCustomTextsTest()
        {
            CustomTextMaintanceManager roomControl = this.CreateDefaultCustomTexts(DefaultCustomTextsSubSet.RoomControl, true);
            CustomTextMaintanceManager sites = this.CreateDefaultCustomTexts(DefaultCustomTextsSubSet.Sites, true);
            CustomTextMaintanceManager menu = this.CreateDefaultCustomTexts(DefaultCustomTextsSubSet.Menu, true);
            CustomTextMaintanceManager generic = this.CreateDefaultCustomTexts(DefaultCustomTextsSubSet.Generic, true);
            CustomTextMaintanceManager company = this.CreateDefaultCustomTexts(DefaultCustomTextsSubSet.Company, true);

            CustomTextCollection customTextsToCreate = new CustomTextCollection();
            customTextsToCreate.AddRange(roomControl.CustomTextCollection);
            customTextsToCreate.AddRange(sites.CustomTextCollection);
            customTextsToCreate.AddRange(menu.CustomTextCollection);
            customTextsToCreate.AddRange(generic.CustomTextCollection);
            customTextsToCreate.AddRange(company.CustomTextCollection);

            StringBuilder builder = new StringBuilder();

            builder.Append(string.Format("ActionButton related: {0}<br />", customTextsToCreate.Count(x => x.ActionButtonId.HasValue)));
            builder.Append(string.Format("Advertisement related: {0}<br />", customTextsToCreate.Count(x => x.AdvertisementId.HasValue)));
            builder.Append(string.Format("Alteration related: {0}<br />", customTextsToCreate.Count(x => x.AlterationId.HasValue)));
            builder.Append(string.Format("Alterationoption related: {0}<br />", customTextsToCreate.Count(x => x.AlterationoptionId.HasValue)));
            builder.Append(string.Format("Amenity related: {0}<br />", customTextsToCreate.Count(x => x.AmenityId.HasValue)));
            builder.Append(string.Format("Announcement related: {0}<br />", customTextsToCreate.Count(x => x.AnnouncementId.HasValue)));
            builder.Append(string.Format("Attachment related: {0}<br />", customTextsToCreate.Count(x => x.AttachmentId.HasValue)));
            builder.Append(string.Format("Availability related: {0}<br />", customTextsToCreate.Count(x => x.AvailabilityId.HasValue)));
            builder.Append(string.Format("Category related: {0}<br />", customTextsToCreate.Count(x => x.CategoryId.HasValue)));
            builder.Append(string.Format("Company related: {0}<br />", customTextsToCreate.Count(x => x.CompanyId.HasValue)));
            builder.Append(string.Format("Deliverypointgroup related: {0}<br />", customTextsToCreate.Count(x => x.DeliverypointgroupId.HasValue)));
            builder.Append(string.Format("Enteratainmentcategory related: {0}<br />", customTextsToCreate.Count(x => x.EntertainmentcategoryId.HasValue)));
            builder.Append(string.Format("Genericcategory related: {0}<br />", customTextsToCreate.Count(x => x.GenericcategoryId.HasValue)));
            builder.Append(string.Format("Genericproduct related: {0}<br />", customTextsToCreate.Count(x => x.GenericproductId.HasValue)));
            builder.Append(string.Format("Page related: {0}<br />", customTextsToCreate.Count(x => x.PageId.HasValue)));
            builder.Append(string.Format("PageTemplate related: {0}<br />", customTextsToCreate.Count(x => x.PageTemplateId.HasValue)));
            builder.Append(string.Format("PointOfInterest related: {0}<br />", customTextsToCreate.Count(x => x.PointOfInterestId.HasValue)));
            builder.Append(string.Format("Product related: {0}<br />", customTextsToCreate.Count(x => x.ProductId.HasValue)));
            builder.Append(string.Format("RoomControlArea related: {0}<br />", customTextsToCreate.Count(x => x.RoomControlAreaId.HasValue)));
            builder.Append(string.Format("RoomControlComponent related: {0}<br />", customTextsToCreate.Count(x => x.RoomControlComponentId.HasValue)));
            builder.Append(string.Format("RoomControlSection related: {0}<br />", customTextsToCreate.Count(x => x.RoomControlSectionId.HasValue)));
            builder.Append(string.Format("RoomControlSectionItem related: {0}<br />", customTextsToCreate.Count(x => x.RoomControlSectionItemId.HasValue)));
            builder.Append(string.Format("RoomControlWidget related: {0}<br />", customTextsToCreate.Count(x => x.RoomControlWidgetId.HasValue)));
            builder.Append(string.Format("Site related: {0}<br />", customTextsToCreate.Count(x => x.SiteId.HasValue)));
            builder.Append(string.Format("SiteTemplate related: {0}<br />", customTextsToCreate.Count(x => x.SiteTemplateId.HasValue)));
            builder.Append(string.Format("Station related: {0}<br />", customTextsToCreate.Count(x => x.StationId.HasValue)));
            builder.Append(string.Format("UIFooterItem related: {0}<br />", customTextsToCreate.Count(x => x.UIFooterItemId.HasValue)));
            builder.Append(string.Format("UITab related: {0}<br />", customTextsToCreate.Count(x => x.UITabId.HasValue)));
            builder.Append(string.Format("UIWidget related: {0}<br />", customTextsToCreate.Count(x => x.UIWidgetId.HasValue)));
            builder.Append(string.Format("VenueCategory related: {0}<br />", customTextsToCreate.Count(x => x.VenueCategoryId.HasValue)));

            this.lblCreateDefaultCustomTextsResult.Text = string.Format("Total of {0} default CustomText entities to be created:<br />{1}", customTextsToCreate.Count, builder);
        }

        private CustomTextMaintanceManager CreateDefaultCustomTexts(DefaultCustomTextsSubSet subSet, bool isTest)
        {
            CustomTextMaintanceManager manager = null;

            //Transaction transaction = new Transaction(IsolationLevel.ReadUncommitted, "CreateDefaultCustomTexts-" + subSet);
            //try
            //{
                switch (subSet)
                {
                    case DefaultCustomTextsSubSet.RoomControl:
                        manager = new RoomControlCustomTextMaintenanceManager();
                        break;
                    case DefaultCustomTextsSubSet.Sites:
                        manager = new SitesCustomTextMaintenanceManager();
                        break;
                    case DefaultCustomTextsSubSet.Menu:
                        manager = new MenuCustomTextMaintenanceManager();
                        break;
                    case DefaultCustomTextsSubSet.Generic:
                        manager = new GenericCustomTextMaintenanceManager();
                        break;
                    case DefaultCustomTextsSubSet.Company:
                        manager = new CompanyCustomTextMaintenanceManager();
                        break;
                }

                if (manager != null)
                {                    
                    manager.FetchData();
                    manager.CreateCustomTexts();
                    manager.SaveChanges(null, isTest);
                }

            //    transaction.Commit();
            //}
            //catch (Exception ex)
            //{
            //    transaction.Rollback();
            //    throw ex;
            //}
            //finally
            //{
            //    transaction.Dispose();
            //}
            return manager;
        }

        private enum DefaultCustomTextsSubSet
        {
            RoomControl,
            Sites,
            Menu,
            Generic,
            Company
        }

        private string GetTimeZoneOlsonIdForNameAndroid(string nameAndroid)
        {
            Obymobi.TimeZone timeZone = this.GetTimeZoneForNameAndroid(nameAndroid);

            if (timeZone == null)
            {
                throw new Exception(string.Format("Can't determine the time zone for NameAndroid: '{0}'.", nameAndroid));
            }

            return timeZone.OlsonTimeZoneId;
        }

        private Obymobi.TimeZone GetTimeZoneForNameAndroid(string nameAndroid)
        {
            Obymobi.TimeZone timeZone = null;

            if (Obymobi.TimeZone.Mappings.ContainsKey(nameAndroid))
            {
                timeZone = Obymobi.TimeZone.Mappings[nameAndroid];
            }

            if (nameAndroid.Equals("Canada/Mountain", StringComparison.InvariantCultureIgnoreCase))
            {
                timeZone = Obymobi.TimeZone.America_Edmonton;
            }
            else if (nameAndroid.Equals("US/Hawaii", StringComparison.InvariantCultureIgnoreCase))
            {
                timeZone = Obymobi.TimeZone.Pacific_Honolulu;
            }
            else if (nameAndroid.Equals("US/Pacific", StringComparison.InvariantCultureIgnoreCase))
            {
                timeZone = Obymobi.TimeZone.America_Los_Angeles;
            }
            else if (nameAndroid.Equals("US/Central", StringComparison.InvariantCultureIgnoreCase))
            {
                timeZone = Obymobi.TimeZone.America_Chicago;
            }
            else if (nameAndroid.Equals("US/Eastern", StringComparison.InvariantCultureIgnoreCase))
            {
                timeZone = Obymobi.TimeZone.America_New_York;
            }

            return timeZone;
        }        

        private CompanyCollection GetCompaniesWithoutTimeZoneOlsonId()
        {
            PredicateExpression filter = new PredicateExpression(CompanyFields.TimeZoneOlsonId == DBNull.Value);

            PrefetchPath prefetch = new PrefetchPath(EntityType.CompanyEntity);
            prefetch.Add(CompanyEntityBase.PrefetchPathTimeZoneEntity);

            CompanyCollection companies = new CompanyCollection();
            companies.GetMulti(filter, prefetch);

            return companies;
        }

        private UserCollection GetUsersWithoutTimeZoneOlsonId()
        {
            PredicateExpression filter = new PredicateExpression(UserFields.TimeZoneOlsonId == DBNull.Value);

            PrefetchPath prefetch = new PrefetchPath(EntityType.UserEntity);
            prefetch.Add(UserEntityBase.PrefetchPathTimeZoneEntity);

            UserCollection users = new UserCollection();
            users.GetMulti(filter, prefetch);

            return users;
        }

        private void ConvertUIThemes()
        {
            PrefetchPath path = new PrefetchPath(EntityType.UIThemeEntity);
            path.Add(UIThemeEntity.PrefetchPathUIThemeColorCollection);

            UIThemeCollection uiThemes = new UIThemeCollection();
            uiThemes.GetMulti(null, path);

            foreach(UIThemeEntity uiTheme in uiThemes)
            {
                if(uiTheme.UIThemeColorCollection.Count <= 0)
                {
                    foreach(IEntityFieldCore field in uiTheme.Fields)
                    {
                        UIColorType uiColor;
                        if(Enum.TryParse<UIColorType>(field.Name, out uiColor))
                        {
                            UIThemeColorEntity uiThemeColor = new UIThemeColorEntity();
                            uiThemeColor.UIThemeId = uiTheme.UIThemeId;
                            uiThemeColor.Type = uiColor;
                            uiThemeColor.Color = (int)uiTheme.Fields[field.FieldIndex].CurrentValue;
                            uiTheme.UIThemeColorCollection.Add(uiThemeColor);
                        }
                    }
                    uiTheme.UIThemeColorCollection.SaveMulti();
                }
            }
        }

        #region Copy LanguageEntity values to CustomText entities

        private void CopyValuesToCustomTextEntities(bool test)
        {
            List<IEntity> entities = new List<IEntity>();

            if (this.cbCompanyLanguages.Checked)
            {
                // Company languages
                entities.AddRange(this.GetCompanyLanguageEntitiesToCopy());
            }                

            if (this.cbDeliverypointgroups.Checked)
            {
                // Deliverypointgroups & DeliverypointgroupLanguages
                entities.AddRange(this.GetDeliverypointgroupsAndLanguagesToCopy());
            }

            if (this.cbCategoryLanguages.Checked)
            {
                // CategoryLanguages
                entities.AddRange(this.GetCategoryLanguagesToCopy());
            }

            if (this.cbProductLanguages.Checked)
            {
                // ProductLanguages
                entities.AddRange(this.GetProductLanguagesToCopy());
            }

            if (this.cbGenericcategoryLanguages.Checked)
            {
                // Genericcategory languages
                entities.AddRange(this.GetGenericcategoryLanguagesToCopy());
            }

            if (this.cbGenericproductLanguages.Checked)
            {
                // Genericproduct languages
                entities.AddRange(this.GetGenericproductLanguagesToCopy());
            }

            if (this.cbAlterationAndOptionLanguages.Checked)
            {
                // Alteration & Alterationoption languages
                entities.AddRange(this.GetAlterationAndAlterationoptionLanguagesToCopy());
            }

            if (this.cbSiteAndPagesLanguages.Checked)
            {
                // Site & Page languages
                entities.AddRange(this.GetSiteAndPageLanguagesToCopy());
            }

            if (this.cbSiteTemplateAndPageTemplateLanguages.Checked)
            {
                // SiteTemplate & PageTemplate languages
                entities.AddRange(this.GetSiteTemplateAndPageTemplateLanguagesToCopy());
            }

            if (this.cbUITabWidgetLanguages.Checked)
            {
                // UIMode, UITab & UIWidget languages
                entities.AddRange(this.GetUITabAndUIWidgetLanguagesToCopy());
            }

            if (this.cbRoomControlLanguages.Checked)
            {
                // RoomControlArea, RoomControlComponent, RoomControlSection, RoomControlSectionItem, RoomControlWidget languages
                entities.AddRange(this.GetRoomControlLanguagesToCopy());
            }

            if (this.cbPointOfInterestLanguages.Checked)
            {
                // PointOfInterest languages
                entities.AddRange(this.GetPointOfInterestLanguagesToCopy());
            }

            if (this.cbEntertainmentLanguages.Checked)
            {
                // Entertainment & Entertainmentcategory languages
                entities.AddRange(this.GetEntertainmentCategoryLanguagesToCopy());
            }

            if (this.cbAdvertisementLanguages.Checked)
            {
                // Advertisement languages
                entities.AddRange(this.GetAdvertisementLanguagesToCopy());
            }

            if (this.cbActionButtonLanguages.Checked)
            {
                // ActionButton languages
                entities.AddRange(this.GetActionButtonLanguagesToCopy());
            }

            if (this.cbAmenityLanguages.Checked)
            {
                // Amenity languages
                entities.AddRange(this.GetAmenityLanguagesToCopy());
            }

            if (this.cbAnnouncementLanguages.Checked)
            {
                // Announcement languages
                entities.AddRange(this.GetAnnouncementLanguagesToCopy());
            }

            if (this.cbAttachmentLanguages.Checked)
            {
                // Attachment languages
                entities.AddRange(this.GetAttachmentLanguagesToCopy());
            }

            if (this.cbUIFooterItemLanguages.Checked)
            {
                // UIFooterItem languages
                entities.AddRange(this.GetUIFooterItemLanguagesToCopy());
            }

            if (this.cbVenueCategoryLanguages.Checked)
            {
                // VenueCategory languages
                entities.AddRange(this.GetVenueCategoryLanguagesToCopy());
            }

            this.CreateCustomTextEntities(test, entities);
        }

        private List<IEntity> GetCompanyLanguageEntitiesToCopy()
        {
            PrefetchPath prefetch = new PrefetchPath(EntityType.CompanyEntity);
            prefetch.Add(CompanyEntityBase.PrefetchPathCompanyLanguageCollection);
            prefetch.Add(CompanyEntityBase.PrefetchPathCustomTextCollection);

            RelationCollection relations = new RelationCollection(CompanyEntityBase.Relations.CompanyLanguageEntityUsingCompanyId);

            CompanyCollection companies = new CompanyCollection();
            companies.GetMulti(null, 0, null, relations, prefetch);

            List<IEntity> entities = new List<IEntity>();
            foreach (CompanyEntity company in companies)
            {
                if (company.CustomTextCollection.Count == 0)
                {
                    entities.AddRange(company.CompanyLanguageCollection);
                }                
            }
            return entities;
        }

        private List<IEntity> GetDeliverypointgroupsAndLanguagesToCopy()
        {
            PrefetchPath prefetch = new PrefetchPath(EntityType.DeliverypointgroupEntity);
            prefetch.Add(DeliverypointgroupEntityBase.PrefetchPathCompanyEntity);
            prefetch.Add(DeliverypointgroupEntityBase.PrefetchPathDeliverypointgroupLanguageCollection);
            prefetch.Add(DeliverypointgroupEntityBase.PrefetchPathCustomTextCollection);

            DeliverypointgroupCollection deliverypointgroups = new DeliverypointgroupCollection();
            deliverypointgroups.GetMulti(null, prefetch);

            List<IEntity> entities = new List<IEntity>();
            foreach (DeliverypointgroupEntity deliverypointgroup in deliverypointgroups)
            {
                if (deliverypointgroup.CustomTextCollection.Count == 0)
                {
                    entities.Add(deliverypointgroup);
                    entities.AddRange(deliverypointgroup.DeliverypointgroupLanguageCollection);
                }
            }
            return entities;
        }

        private List<IEntity> GetCategoryLanguagesToCopy()
        {
            PrefetchPath prefetch = new PrefetchPath(EntityType.CategoryEntity);
            prefetch.Add(CategoryEntityBase.PrefetchPathCategoryLanguageCollection);
            prefetch.Add(CategoryEntityBase.PrefetchPathCustomTextCollection);

            RelationCollection relations = new RelationCollection(CategoryEntityBase.Relations.CategoryLanguageEntityUsingCategoryId);

            CategoryCollection categories = new CategoryCollection();
            categories.GetMulti(null, 0, null, relations, prefetch);

            List<IEntity> entities = new List<IEntity>();
            foreach (CategoryEntity category in categories)
            {
                if (category.CustomTextCollection.Count == 0)
                {
                    entities.AddRange(category.CategoryLanguageCollection);
                }
            }
            return entities;
        }

        private List<IEntity> GetProductLanguagesToCopy()
        {
            PrefetchPath prefetch = new PrefetchPath(EntityType.ProductEntity);
            prefetch.Add(ProductEntityBase.PrefetchPathProductLanguageCollection);
            prefetch.Add(ProductEntityBase.PrefetchPathCustomTextCollection);

            RelationCollection relations = new RelationCollection(ProductEntityBase.Relations.ProductLanguageEntityUsingProductId);

            ProductCollection products = new ProductCollection();
            products.GetMulti(null, 0, null, relations, prefetch);

            List<IEntity> entities = new List<IEntity>();
            foreach (ProductEntity product in products)
            {
                if (product.CustomTextCollection.Count == 0)
                {
                    entities.AddRange(product.ProductLanguageCollection);
                }
            }
            return entities;
        }

        private List<IEntity> GetGenericcategoryLanguagesToCopy()
        {
            PrefetchPath prefetch = new PrefetchPath(EntityType.GenericcategoryEntity);
            prefetch.Add(GenericcategoryEntityBase.PrefetchPathGenericcategoryLanguageCollection);
            prefetch.Add(GenericcategoryEntityBase.PrefetchPathCustomTextCollection);

            RelationCollection relations = new RelationCollection(GenericcategoryEntityBase.Relations.GenericcategoryLanguageEntityUsingGenericcategoryId);

            GenericcategoryCollection genericcategories = new GenericcategoryCollection();
            genericcategories.GetMulti(null, 0, null, relations, prefetch);

            List<IEntity> entities = new List<IEntity>();
            foreach (GenericcategoryEntity genericcategory in genericcategories)
            {
                if (genericcategory.CustomTextCollection.Count == 0)
                {
                    entities.AddRange(genericcategory.GenericcategoryLanguageCollection);
                }
            }
            return entities;
        }

        private List<IEntity> GetGenericproductLanguagesToCopy()
        {
            PrefetchPath prefetch = new PrefetchPath(EntityType.GenericproductEntity);
            prefetch.Add(GenericproductEntityBase.PrefetchPathGenericproductLanguageCollection);
            prefetch.Add(GenericproductEntityBase.PrefetchPathCustomTextCollection);

            RelationCollection relations = new RelationCollection(GenericproductEntityBase.Relations.GenericproductLanguageEntityUsingGenericproductId);

            GenericproductCollection genericproducts = new GenericproductCollection();
            genericproducts.GetMulti(null, 0, null, relations, prefetch);

            List<IEntity> entities = new List<IEntity>();
            foreach (GenericproductEntity genericproduct in genericproducts)
            {
                if (genericproduct.CustomTextCollection.Count == 0)
                {
                    entities.AddRange(genericproduct.GenericproductLanguageCollection);
                }
            }
            return entities;
        }

        private List<IEntity> GetAlterationAndAlterationoptionLanguagesToCopy()
        {
            PrefetchPath alterationPrefetch = new PrefetchPath(EntityType.AlterationEntity);
            alterationPrefetch.Add(AlterationEntityBase.PrefetchPathAlterationLanguageCollection);
            alterationPrefetch.Add(AlterationEntityBase.PrefetchPathCustomTextCollection);

            PredicateExpression alterationFilter = new PredicateExpression(AlterationFields.Version == 1);
            RelationCollection alterationRelations = new RelationCollection(AlterationEntityBase.Relations.AlterationLanguageEntityUsingAlterationId);
            
            AlterationCollection alterations = new AlterationCollection();
            alterations.GetMulti(alterationFilter, 0, null, alterationRelations, alterationPrefetch);

            PrefetchPath alterationoptionPrefetch = new PrefetchPath(EntityType.AlterationoptionEntity);
            alterationoptionPrefetch.Add(AlterationoptionEntityBase.PrefetchPathAlterationoptionLanguageCollection);
            alterationoptionPrefetch.Add(AlterationoptionEntityBase.PrefetchPathCustomTextCollection);

            PredicateExpression alterationoptionFilter = new PredicateExpression(AlterationoptionFields.Version == 1);
            RelationCollection alterationoptionRelations = new RelationCollection(AlterationoptionEntityBase.Relations.AlterationoptionLanguageEntityUsingAlterationoptionId);

            AlterationoptionCollection alterationoptions = new AlterationoptionCollection();
            alterationoptions.GetMulti(alterationoptionFilter, 0, null, alterationoptionRelations, alterationoptionPrefetch);

            List<IEntity> entities = new List<IEntity>();
            foreach (AlterationEntity alteration in alterations)
            {
                if (alteration.CustomTextCollection.Count == 0)
                {
                    entities.AddRange(alteration.AlterationLanguageCollection);
                }
            }
            foreach (AlterationoptionEntity alterationoption in alterationoptions)
            {
                if (alterationoption.CustomTextCollection.Count == 0)
                {
                    entities.AddRange(alterationoption.AlterationoptionLanguageCollection);
                }
            }
            return entities;
        }

        private List<IEntity> GetSiteAndPageLanguagesToCopy()
        {
            PrefetchPath sitePrefetch = new PrefetchPath(EntityType.SiteEntity);
            sitePrefetch.Add(SiteEntityBase.PrefetchPathSiteLanguageCollection);
            sitePrefetch.Add(SiteEntityBase.PrefetchPathCustomTextCollection);

            RelationCollection siteRelations = new RelationCollection(SiteEntityBase.Relations.SiteLanguageEntityUsingSiteId);

            SiteCollection sites = new SiteCollection();
            sites.GetMulti(null, 0, null, siteRelations, sitePrefetch);

            PrefetchPath pagePrefetch = new PrefetchPath(EntityType.PageEntity);
            pagePrefetch.Add(PageEntityBase.PrefetchPathPageLanguageCollection);
            pagePrefetch.Add(PageEntityBase.PrefetchPathCustomTextCollection);

            RelationCollection pageRelations = new RelationCollection(PageEntityBase.Relations.PageLanguageEntityUsingPageId);

            PageCollection pages = new PageCollection();
            pages.GetMulti(null, 0, null, pageRelations, pagePrefetch);

            List<IEntity> entities = new List<IEntity>();
            foreach (SiteEntity site in sites)
            {
                if (site.CustomTextCollection.Count == 0)
                {
                    entities.AddRange(site.SiteLanguageCollection);
                }
            }
            foreach (PageEntity page in pages)
            {
                if (page.CustomTextCollection.Count == 0)
                {
                    entities.AddRange(page.PageLanguageCollection);
                }
            }
            return entities;
        }

        private List<IEntity> GetSiteTemplateAndPageTemplateLanguagesToCopy()
        {
            PrefetchPath siteTemplatePrefetch = new PrefetchPath(EntityType.SiteTemplateEntity);
            siteTemplatePrefetch.Add(SiteTemplateEntityBase.PrefetchPathSiteTemplateLanguageCollection);
            siteTemplatePrefetch.Add(SiteTemplateEntityBase.PrefetchPathCustomTextCollection);

            RelationCollection siteTemplateRelations = new RelationCollection(SiteTemplateEntityBase.Relations.SiteTemplateLanguageEntityUsingSiteTemplateId);

            SiteTemplateCollection siteTemplates = new SiteTemplateCollection();
            siteTemplates.GetMulti(null, 0, null, siteTemplateRelations, siteTemplatePrefetch);

            PrefetchPath pageTemplatePrefetch = new PrefetchPath(EntityType.PageTemplateEntity);
            pageTemplatePrefetch.Add(PageTemplateEntityBase.PrefetchPathPageTemplateLanguageCollection);
            pageTemplatePrefetch.Add(PageTemplateEntityBase.PrefetchPathCustomTextCollection);

            RelationCollection pageTemplateRelations = new RelationCollection(PageTemplateEntityBase.Relations.PageTemplateLanguageEntityUsingPageTemplateId);

            PageTemplateCollection pageTemplates = new PageTemplateCollection();
            pageTemplates.GetMulti(null, 0, null, pageTemplateRelations, pageTemplatePrefetch);

            List<IEntity> entities = new List<IEntity>();
            foreach (SiteTemplateEntity siteTemplate in siteTemplates)
            {
                if (siteTemplate.CustomTextCollection.Count == 0)
                {
                    entities.AddRange(siteTemplate.SiteTemplateLanguageCollection);
                }
            }
            foreach (PageTemplateEntity pageTemplate in pageTemplates)
            {
                if (pageTemplate.CustomTextCollection.Count == 0)
                {
                    entities.AddRange(pageTemplate.PageTemplateLanguageCollection);
                }
            }
            return entities;
        }

        private List<IEntity> GetUITabAndUIWidgetLanguagesToCopy()
        {
            PrefetchPath uiTabPrefetch = new PrefetchPath(EntityType.UITabEntity);
            uiTabPrefetch.Add(UITabEntityBase.PrefetchPathUITabLanguageCollection);
            uiTabPrefetch.Add(UITabEntityBase.PrefetchPathCustomTextCollection);
            uiTabPrefetch.Add(UITabEntityBase.PrefetchPathUIModeEntity).SubPath.Add(UIModeEntityBase.PrefetchPathCompanyEntity);

            RelationCollection uiTabRelations = new RelationCollection(UITabEntityBase.Relations.UITabLanguageEntityUsingUITabId, JoinHint.Left);

            UITabCollection uiTabs = new UITabCollection();
            uiTabs.GetMulti(null, 0, null, uiTabRelations, uiTabPrefetch);

            List<IEntity> entities = new List<IEntity>();
            foreach (UITabEntity uiTab in uiTabs)
            {
                if (uiTab.CustomTextCollection.Count == 0)
                {
                    if (uiTab.UITabLanguageCollection.Count > 0)
                    {
                        entities.AddRange(uiTab.UITabLanguageCollection);
                    }
                    else
                    {
                        entities.Add(uiTab);
                    }
                }
            }

            PrefetchPath uiWidgetPrefetch = new PrefetchPath(EntityType.UIWidgetEntity);
            uiWidgetPrefetch.Add(UIWidgetEntityBase.PrefetchPathUIWidgetLanguageCollection);
            uiWidgetPrefetch.Add(UIWidgetEntityBase.PrefetchPathCustomTextCollection);

            RelationCollection uiWidgetRelations = new RelationCollection(UIWidgetEntityBase.Relations.UIWidgetLanguageEntityUsingUIWidgetId);

            UIWidgetCollection uiWidgets = new UIWidgetCollection();
            uiWidgets.GetMulti(null, 0, null, uiWidgetRelations, uiWidgetPrefetch);

            foreach (UIWidgetEntity uiWidget in uiWidgets)
            {
                if (uiWidget.CustomTextCollection.Count == 0)
                {
                    entities.AddRange(uiWidget.UIWidgetLanguageCollection);
                }
            }
            return entities;
        }

        private List<IEntity> GetRoomControlLanguagesToCopy()
        {
            // Room Control Areas
            PrefetchPath areaPrefetch = new PrefetchPath(EntityType.RoomControlAreaEntity);
            areaPrefetch.Add(RoomControlAreaEntityBase.PrefetchPathRoomControlAreaLanguageCollection);
            areaPrefetch.Add(RoomControlAreaEntityBase.PrefetchPathCustomTextCollection);

            RelationCollection areaRelations = new RelationCollection(RoomControlAreaEntityBase.Relations.RoomControlAreaLanguageEntityUsingRoomControlAreaId);

            RoomControlAreaCollection areas = new RoomControlAreaCollection();
            areas.GetMulti(null, 0, null, areaRelations, areaPrefetch);

            // Room Control Components
            PrefetchPath componentPrefetch = new PrefetchPath(EntityType.RoomControlComponentEntity);
            componentPrefetch.Add(RoomControlComponentEntityBase.PrefetchPathRoomControlComponentLanguageCollection);
            componentPrefetch.Add(RoomControlComponentEntityBase.PrefetchPathCustomTextCollection);

            RelationCollection componentRelations = new RelationCollection(RoomControlComponentEntityBase.Relations.RoomControlComponentLanguageEntityUsingRoomControlComponentId);

            RoomControlComponentCollection components = new RoomControlComponentCollection();
            components.GetMulti(null, 0, null, componentRelations, componentPrefetch);

            // Room Control Sections
            PrefetchPath sectionPrefetch = new PrefetchPath(EntityType.RoomControlSectionEntity);
            sectionPrefetch.Add(RoomControlSectionEntityBase.PrefetchPathRoomControlSectionLanguageCollection);
            sectionPrefetch.Add(RoomControlSectionEntityBase.PrefetchPathCustomTextCollection);

            RelationCollection sectionRelations = new RelationCollection(RoomControlSectionEntityBase.Relations.RoomControlSectionLanguageEntityUsingRoomControlSectionId);

            RoomControlSectionCollection sections = new RoomControlSectionCollection();
            sections.GetMulti(null, 0, null, sectionRelations, sectionPrefetch);

            // Room Control Section Items
            PrefetchPath sectionItemPrefetch = new PrefetchPath(EntityType.RoomControlSectionItemEntity);
            sectionItemPrefetch.Add(RoomControlSectionItemEntityBase.PrefetchPathRoomControlSectionItemLanguageCollection);
            sectionItemPrefetch.Add(RoomControlSectionItemEntityBase.PrefetchPathCustomTextCollection);

            RelationCollection sectionItemRelations = new RelationCollection(RoomControlSectionItemEntityBase.Relations.RoomControlSectionItemLanguageEntityUsingRoomControlSectionItemId);

            RoomControlSectionItemCollection sectionItems = new RoomControlSectionItemCollection();
            sectionItems.GetMulti(null, 0, null, sectionItemRelations, sectionItemPrefetch);

            // Room Control Widgets
            PrefetchPath widgetPrefetch = new PrefetchPath(EntityType.RoomControlWidgetEntity);
            widgetPrefetch.Add(RoomControlWidgetEntityBase.PrefetchPathRoomControlWidgetLanguageCollection);
            widgetPrefetch.Add(RoomControlWidgetEntityBase.PrefetchPathCustomTextCollection);

            RelationCollection widgetRelations = new RelationCollection(RoomControlWidgetEntityBase.Relations.RoomControlWidgetLanguageEntityUsingRoomControlWidgetId);

            RoomControlWidgetCollection widgets = new RoomControlWidgetCollection();
            widgets.GetMulti(null, 0, null, widgetRelations, widgetPrefetch);

            // Stations
            PrefetchPath stationPrefetch = new PrefetchPath(EntityType.StationEntity);
            stationPrefetch.Add(StationEntityBase.PrefetchPathStationLanguageCollection);
            stationPrefetch.Add(StationEntityBase.PrefetchPathCustomTextCollection);

            RelationCollection stationRelations = new RelationCollection(StationEntityBase.Relations.StationLanguageEntityUsingStationId);

            StationCollection stations = new StationCollection();
            stations.GetMulti(null, 0, null, stationRelations, stationPrefetch);

            List<IEntity> entities = new List<IEntity>();
            foreach (RoomControlAreaEntity area in areas)
            {
                if (area.CustomTextCollection.Count == 0)
                {
                    entities.AddRange(area.RoomControlAreaLanguageCollection);
                }
            }
            foreach (RoomControlComponentEntity component in components)
            {
                if (component.CustomTextCollection.Count == 0)
                {
                    entities.AddRange(component.RoomControlComponentLanguageCollection);
                }
            }
            foreach (RoomControlSectionEntity section in sections)
            {
                if (section.CustomTextCollection.Count == 0)
                {
                    entities.AddRange(section.RoomControlSectionLanguageCollection);
                }
            }
            foreach (RoomControlSectionItemEntity sectionItem in sectionItems)
            {
                if (sectionItem.CustomTextCollection.Count == 0)
                {
                    entities.AddRange(sectionItem.RoomControlSectionItemLanguageCollection);
                }
            }
            foreach (RoomControlWidgetEntity widget in widgets)
            {
                if (widget.CustomTextCollection.Count == 0)
                {
                    entities.AddRange(widget.RoomControlWidgetLanguageCollection);
                }
            }
            foreach (StationEntity station in stations)
            {
                if (station.CustomTextCollection.Count == 0)
                {
                    entities.AddRange(station.StationLanguageCollection);
                }
            }
            return entities;
        }

        private List<IEntity> GetPointOfInterestLanguagesToCopy()
        {
            PrefetchPath prefetch = new PrefetchPath(EntityType.PointOfInterestEntity);
            prefetch.Add(PointOfInterestEntityBase.PrefetchPathPointOfInterestLanguageCollection);
            prefetch.Add(PointOfInterestEntityBase.PrefetchPathCustomTextCollection);

            RelationCollection relations = new RelationCollection(PointOfInterestEntityBase.Relations.PointOfInterestLanguageEntityUsingPointOfInterestId);

            PointOfInterestCollection pointOfInterests = new PointOfInterestCollection();
            pointOfInterests.GetMulti(null, 0, null, relations, prefetch);

            List<IEntity> entities = new List<IEntity>();
            foreach (PointOfInterestEntity pointOfInterest in pointOfInterests)
            {
                if (pointOfInterest.CustomTextCollection.Count == 0)
                {
                    entities.AddRange(pointOfInterest.PointOfInterestLanguageCollection);
                }
            }
            return entities;
        }

        private List<IEntity> GetEntertainmentCategoryLanguagesToCopy()
        {
            PrefetchPath prefetch = new PrefetchPath(EntityType.EntertainmentcategoryEntity);
            prefetch.Add(EntertainmentcategoryEntityBase.PrefetchPathEntertainmentcategoryLanguageCollection);
            prefetch.Add(EntertainmentcategoryEntityBase.PrefetchPathCustomTextCollection);

            RelationCollection relations = new RelationCollection(EntertainmentcategoryEntityBase.Relations.EntertainmentcategoryLanguageEntityUsingEntertainmentcategoryId);

            EntertainmentcategoryCollection entertainmentcategories = new EntertainmentcategoryCollection();
            entertainmentcategories.GetMulti(null, 0, null, relations, prefetch);

            List<IEntity> entities = new List<IEntity>();
            foreach (EntertainmentcategoryEntity entertainmentcategory in entertainmentcategories)
            {
                if (entertainmentcategory.CustomTextCollection.Count == 0)
                {
                    entities.AddRange(entertainmentcategory.EntertainmentcategoryLanguageCollection);
                }
            }
            return entities;
        }

        private List<IEntity> GetAdvertisementLanguagesToCopy()
        {
            PrefetchPath prefetch = new PrefetchPath(EntityType.AdvertisementEntity);
            prefetch.Add(AdvertisementEntityBase.PrefetchPathAdvertisementLanguageCollection);
            prefetch.Add(AdvertisementEntityBase.PrefetchPathCustomTextCollection);

            RelationCollection relations = new RelationCollection(AdvertisementEntityBase.Relations.AdvertisementLanguageEntityUsingAdvertisementId);

            AdvertisementCollection advertisements = new AdvertisementCollection();
            advertisements.GetMulti(null, 0, null, relations, prefetch);

            List<IEntity> entities = new List<IEntity>();
            foreach (AdvertisementEntity advertisement in advertisements)
            {
                if (advertisement.CustomTextCollection.Count == 0)
                {
                    entities.AddRange(advertisement.AdvertisementLanguageCollection);
                }
            }
            return entities;
        }

        private List<IEntity> GetActionButtonLanguagesToCopy()
        {
            PrefetchPath prefetch = new PrefetchPath(EntityType.ActionButtonEntity);
            prefetch.Add(ActionButtonEntityBase.PrefetchPathActionButtonLanguageCollection);
            prefetch.Add(ActionButtonEntityBase.PrefetchPathCustomTextCollection);

            RelationCollection relations = new RelationCollection(ActionButtonEntityBase.Relations.ActionButtonLanguageEntityUsingActionButtonId);

            ActionButtonCollection actionButtons = new ActionButtonCollection();
            actionButtons.GetMulti(null, 0, null, relations, prefetch);

            List<IEntity> entities = new List<IEntity>();
            foreach (ActionButtonEntity actionButton in actionButtons)
            {
                if (actionButton.CustomTextCollection.Count == 0)
                {
                    entities.AddRange(actionButton.ActionButtonLanguageCollection);
                }
            }
            return entities;
        }

        private List<IEntity> GetAmenityLanguagesToCopy()
        {
            PrefetchPath prefetch = new PrefetchPath(EntityType.AmenityEntity);
            prefetch.Add(AmenityEntityBase.PrefetchPathAmenityLanguageCollection);
            prefetch.Add(AmenityEntityBase.PrefetchPathCustomTextCollection);

            RelationCollection relations = new RelationCollection(AmenityEntityBase.Relations.AmenityLanguageEntityUsingAmenityId);

            AmenityCollection amenities = new AmenityCollection();
            amenities.GetMulti(null, 0, null, relations, prefetch);

            List<IEntity> entities = new List<IEntity>();
            foreach (AmenityEntity amenity in amenities)
            {
                if (amenity.CustomTextCollection.Count == 0)
                {
                    entities.AddRange(amenity.AmenityLanguageCollection);
                }
            }
            return entities;
        }

        private List<IEntity> GetAnnouncementLanguagesToCopy()
        {
            PrefetchPath prefetch = new PrefetchPath(EntityType.AnnouncementEntity);
            prefetch.Add(AnnouncementEntityBase.PrefetchPathAnnouncementLanguageCollection);
            prefetch.Add(AnnouncementEntityBase.PrefetchPathCustomTextCollection);

            RelationCollection relations = new RelationCollection(AnnouncementEntityBase.Relations.AnnouncementLanguageEntityUsingAnnouncementId);

            AnnouncementCollection announcements = new AnnouncementCollection();
            announcements.GetMulti(null, 0, null, relations, prefetch);

            List<IEntity> entities = new List<IEntity>();
            foreach (AnnouncementEntity announcement in announcements)
            {
                if (announcement.CustomTextCollection.Count == 0)
                {
                    entities.AddRange(announcement.AnnouncementLanguageCollection);
                }
            }
            return entities;
        }

        private List<IEntity> GetAttachmentLanguagesToCopy()
        {
            PrefetchPath prefetch = new PrefetchPath(EntityType.AttachmentEntity);
            prefetch.Add(AttachmentEntityBase.PrefetchPathAttachmentLanguageCollection);
            prefetch.Add(AttachmentEntityBase.PrefetchPathCustomTextCollection);

            RelationCollection relations = new RelationCollection(AttachmentEntityBase.Relations.AttachmentLanguageEntityUsingAttachmentId);

            AttachmentCollection attachments = new AttachmentCollection();
            attachments.GetMulti(null, 0, null, relations, prefetch);

            List<IEntity> entities = new List<IEntity>();
            foreach (AttachmentEntity attachment in attachments)
            {
                if (attachment.CustomTextCollection.Count == 0)
                {
                    entities.AddRange(attachment.AttachmentLanguageCollection);
                }
            }
            return entities;
        }

        private List<IEntity> GetUIFooterItemLanguagesToCopy()
        {
            PrefetchPath prefetch = new PrefetchPath(EntityType.UIFooterItemEntity);
            prefetch.Add(UIFooterItemEntityBase.PrefetchPathUIFooterItemLanguageCollection);
            prefetch.Add(UIFooterItemEntityBase.PrefetchPathCustomTextCollection);

            RelationCollection relations = new RelationCollection(UIFooterItemEntityBase.Relations.UIFooterItemLanguageEntityUsingUIFooterItemId);

            UIFooterItemCollection uiFooterItems = new UIFooterItemCollection();
            uiFooterItems.GetMulti(null, 0, null, relations, prefetch);

            List<IEntity> entities = new List<IEntity>();
            foreach (UIFooterItemEntity uiFooterItem in uiFooterItems)
            {
                if (uiFooterItem.CustomTextCollection.Count == 0)
                {
                    entities.AddRange(uiFooterItem.UIFooterItemLanguageCollection);
                }
            }
            return entities;
        }

        private List<IEntity> GetVenueCategoryLanguagesToCopy()
        {
            PrefetchPath prefetch = new PrefetchPath(EntityType.VenueCategoryEntity);
            prefetch.Add(VenueCategoryEntityBase.PrefetchPathVenueCategoryLanguageCollection);
            prefetch.Add(VenueCategoryEntityBase.PrefetchPathCustomTextCollection);

            RelationCollection relations = new RelationCollection(VenueCategoryEntityBase.Relations.VenueCategoryLanguageEntityUsingVenueCategoryId);

            VenueCategoryCollection venueCategories = new VenueCategoryCollection();
            venueCategories.GetMulti(null, 0, null, relations, prefetch);

            List<IEntity> entities = new List<IEntity>();
            foreach (VenueCategoryEntity venueCategory in venueCategories)
            {
                if (venueCategory.CustomTextCollection.Count == 0)
                {
                    entities.AddRange(venueCategory.VenueCategoryLanguageCollection);
                }
            }
            return entities;
        }

        private readonly List<string> uniqueRecords = new List<string>();        

        private void CreateCustomTextEntities(bool test, List<IEntity> entities)
        {
            this.uniqueRecords.Clear();

            //Transaction transaction = new Transaction(IsolationLevel.ReadUncommitted, "CopyValuesToCustomTextEntities");
            //try
            //{
                int count = 0;
                StringBuilder builder = new StringBuilder();

                CustomTextCollection customTextsToAdd = new CustomTextCollection();
                foreach (IEntity entity in entities)
                {
                    int languageId = entity.Fields["LanguageId"] != null ? int.Parse(entity.Fields["LanguageId"].CurrentValue.ToString()) : -1;
                    string foreignKey = entity.LLBLGenProEntityName.Contains("LanguageEntity") ? entity.LLBLGenProEntityName.Replace("LanguageEntity", "Id") : string.Empty;

                    if (foreignKey.StartsWith("x", StringComparison.InvariantCultureIgnoreCase))
                    {
                        foreignKey = foreignKey.Substring(1);
                    }

                    if (entity is DeliverypointgroupEntity && ((DeliverypointgroupEntity)entity).CompanyEntity.LanguageId.HasValue)
                    {
                        languageId = ((DeliverypointgroupEntity)entity).CompanyEntity.LanguageId.Value;
                        foreignKey = "DeliverypointgroupId";
                    }
                    else if (entity is UITabEntity && ((UITabEntity)entity).UIModeEntity != null && ((UITabEntity)entity).UIModeEntity.CompanyEntity != null && ((UITabEntity)entity).UIModeEntity.CompanyEntity.LanguageId.HasValue)
                    {
                        languageId = ((UITabEntity)entity).UIModeEntity.CompanyEntity.LanguageId.Value;
                        foreignKey = "UITabId";
                    }

                    if (languageId > 0 && !foreignKey.IsNullOrEmpty() && entity.Fields[foreignKey] != null)
                    {
                        Dictionary<int, CustomTextType> mappedCustomTextTypes = this.GetMappedDictionaryForEntity(entity);
                        if (mappedCustomTextTypes != null)
                        {
                            for (int i = 0; i < entity.Fields.Count; i++)
                            {
                                if (mappedCustomTextTypes.ContainsKey(entity.Fields[i].FieldIndex) &&
                                    (entity.Fields[i].CurrentValue != null && !entity.Fields[i].CurrentValue.ToString().IsNullOrWhiteSpace()))
                                {
                                    CustomTextType type = mappedCustomTextTypes[entity.Fields[i].FieldIndex];
                                    object foreignKeyValue = entity.Fields[foreignKey].CurrentValue;

                                    string uniqueKey = string.Format("{0}-{1}-{2}", languageId, (int)type, foreignKeyValue);
                                    if (!this.uniqueRecords.Contains(uniqueKey))
                                    {
                                        this.uniqueRecords.Add(uniqueKey);

                                        builder.Append(string.Format("EntityType: '{0}', ForeignKey: '{1}', LanguageId: '{2}', Field: '{3}'.<br />", entity.GetType(), entity.Fields[foreignKey].CurrentValue, languageId, entity.Fields[i].Name));

                                        CustomTextEntity customTextEntity = new CustomTextEntity();
                                        //customTextEntity.AddToTransaction(transaction);
                                        customTextEntity.Type = type;
                                        customTextEntity.Text = entity.Fields[i].CurrentValue.ToString();
                                        customTextEntity.LanguageId = languageId;
                                        customTextEntity.SetNewFieldValue(foreignKey, foreignKeyValue);
                                        customTextEntity.Save();


                                        //customTextsToAdd.Add(customTextEntity);
                                        count++;
                                    }
                                }
                            }
                        }
                    }                    
                }

                //customTextsToAdd.AddToTransaction(transaction);
                //customTextsToAdd.SaveMulti();

                if (test)
                {
                    this.lblEntitiesToCopyToCustomText.Text = string.Format("Total of {0} CustomText entities to be created:<br />{1}", count, builder);
                }
                //else
                //{
                //    transaction.Commit();
                //}
            //}
            //catch (Exception ex)
            //{
            //    transaction.Rollback();
            //    throw ex;
            //}
            //finally
            //{
            //    transaction.Dispose();
            //}
        }

        private void SetCultureCodeForDefaultCustomTexts(bool test)
        {
            CompanyCollection companies = new CompanyCollection();
            companies.GetMulti(null);

            Dictionary<int, string> companyCultureCodes = companies.Where(x => !x.CultureCode.IsNullOrWhiteSpace()).ToDictionary(x => x.CompanyId, x => x.CultureCode);

            CustomTextCollection customTexts = new CustomTextCollection();
            customTexts.GetMulti(CustomTextFields.CultureCode == DBNull.Value);

            foreach (CustomTextEntity customText in customTexts)
            {
                int relatedEntityCompanyId = customText.ParentCompanyId.GetValueOrDefault(0);

                if (customText.SiteId.HasValue || customText.PageId.HasValue ||
                    (customText.AttachmentId.HasValue && customText.AttachmentEntity.PageId.HasValue))
                {
                    // The default culture for sites is en-GB, we dont want to change the default culture to the company default
                    relatedEntityCompanyId = -1;
                }

                if (relatedEntityCompanyId <= 0)
                {
                    // en-GB?
                    customText.CultureCode = Obymobi.Culture.English_United_Kingdom.Code;
                }
                else if (companyCultureCodes.ContainsKey(relatedEntityCompanyId))
                {
                    customText.CultureCode = companyCultureCodes[relatedEntityCompanyId];
                }                
            }

            if (test)
            {

                this.lblEntitiesToCopyToCustomText.Text = string.Format("Total of {0} CustomText entities to be updated:<br />", customTexts.Count(x => !x.CultureCode.IsNullOrWhiteSpace()));
            }
            else
            {
                foreach (CustomTextEntity customText in customTexts)
                {
                    if (!customText.CultureCode.IsNullOrWhiteSpace())
                    {
                        try
                        {
                            customText.Save();
                        }
                        catch (ObymobiEntityException ex)
                        {
                            if (ex.ErrorEnumValueInt == 100)
                            {
                                customText.Delete();
                            }
                        }

                    }
                }
            }            
        }

        private void FixFormattingIssueForItalicAndBold(bool test)
        {
            CategoryCollection categories = new CategoryCollection();
            ProductCollection products = new ProductCollection();
            CustomTextCollection customTexts = new CustomTextCollection();

            StringBuilder builder = new StringBuilder();

            // Category description
            PredicateExpression filter = new PredicateExpression(new FieldLikePredicate(CategoryFields.Description, "% & %"));
            filter.Add(new FieldLikePredicate(CategoryFields.Description, "%*%"));

            CategoryCollection tempCategories = new CategoryCollection();
            tempCategories.GetMulti(filter);

            foreach (CategoryEntity tempCategory in tempCategories)
            {
                string modifiedText;
                if (this.ShouldSaveFixedFormattedText(tempCategory.Description, out modifiedText))
                {
                    categories.Add(tempCategory);
                }
                tempCategory.Description = modifiedText;
            }

            // Product description
            filter = new PredicateExpression(new FieldLikePredicate(ProductFields.Description, "% & %"));
            filter.Add(new FieldLikePredicate(ProductFields.Description, "%*%"));

            ProductCollection tempProducts = new ProductCollection();
            tempProducts.GetMulti(filter);

            foreach (ProductEntity tempProduct in tempProducts)
            {
                string modifiedText;
                if (this.ShouldSaveFixedFormattedText(tempProduct.Description, out modifiedText))
                {
                    products.Add(tempProduct);
                }
                tempProduct.Description = modifiedText;
            }

            // Custom text
            filter = new PredicateExpression(new FieldLikePredicate(CustomTextFields.Text, "% & %"));
            filter.Add(new FieldLikePredicate(CustomTextFields.Text, "%*%"));
            PredicateExpression subFilter = new PredicateExpression();
            subFilter.Add(CustomTextFields.CategoryId != DBNull.Value);
            subFilter.AddWithOr(CustomTextFields.ProductId != DBNull.Value);
            filter.Add(subFilter);
            

            CustomTextCollection tempCustomTexts = new CustomTextCollection();
            tempCustomTexts.GetMulti(filter);

            foreach (CustomTextEntity tempCustomText in tempCustomTexts)
            {
                string modifiedText;
                if (this.ShouldSaveFixedFormattedText(tempCustomText.Text, out modifiedText))
                {
                    customTexts.Add(tempCustomText);
                }
                tempCustomText.Text = modifiedText;
            }

            if (test)
            {
                builder.Append(string.Format("Total of {0} categories to be updated<br />", categories.Count));

                foreach (CategoryEntity category in categories)
                {
                    builder.Append(string.Format("Category {0}, {1}<br />", category.CategoryId, category.CompanyEntity.Name));
                    builder.Append(string.Format("{0}<br />", category.Description));
                    builder.Append("<br />");
                }

                builder.Append(string.Format("Total of {0} products to be updated<br />", products.Count));

                foreach (ProductEntity product in products)
                {
                    builder.Append(string.Format("Product {0}, {1}<br />", product.ProductId, product.CompanyEntity.Name));
                    builder.Append(string.Format("{0}<br />", product.Description));
                    builder.Append("<br />");
                }

                builder.Append(string.Format("Total of {0} custom texts to be updated<br />", customTexts.Count));

                foreach (CustomTextEntity customText in customTexts)
                {
                    builder.Append(string.Format("CustomText {0}, ProductId: {1}, CategoryId: {2}<br />", customText.CustomTextId, customText.ProductId, customText.CategoryId));
                    builder.Append(string.Format("{0}<br />", customText.Text));
                    builder.Append("<br />");
                }

                this.lblFixFormattingIssueForItalicAndBoldResult.Text = builder.ToString();
            }
            else
            {
                foreach (CategoryEntity category in categories)
                {
                    category.Save();
                }

                foreach (ProductEntity product in products)
                {
                    product.Save();
                }

                foreach (CustomTextEntity customText in customTexts)
                {
                    customText.Save();
                }
            }
        }

        private bool ShouldSaveFixedFormattedText(string text, out string modifiedText)
        {
            modifiedText = text;
            string[] lines = modifiedText.Split(new[] { "\r\n" }, StringSplitOptions.None);

            bool requireSave = false;

            foreach (string line in lines)
            {
                bool ampersandInItalic = false;
                bool ampersandInBold = false;

                if (line.Contains(" & ") && line.Contains("*"))
                {
                    bool italicActive = false;
                    bool boldActive = false;
                    bool ampersandFound = false;

                    int index = 0;
                    for (; index < line.Length; )
                    {
                        char currentChar = line[index];
                        char nextChar = (index + 1) < line.Length ? line[index + 1] : ' ';
                        char thirdChar = (index + 2) < line.Length ? line[index + 2] : ' ';

                        if (currentChar == '*' && nextChar == '*' && thirdChar == '*')
                        {
                            // Do nothing
                            index = index + 2;                            
                        }
                        else if (currentChar == '*' && nextChar == '*')
                        {
                            ampersandInBold = boldActive && ampersandFound;
                            boldActive = !boldActive;
                            index++;
                        }
                        else if (currentChar == '*' && (index > 0 || (index == 0 && nextChar != ' ')))
                        {
                            ampersandInItalic = italicActive && ampersandFound;
                            italicActive = !italicActive;
                        }
                        else if (currentChar == '&')
                        {
                            ampersandFound = true;
                        }
                        index++;
                    }
                }

                if (ampersandInBold || ampersandInItalic)
                {
                    if (ampersandInItalic)
                    {
                        line.Replace(" & ", "* *&* *");
                    }
                    else if (ampersandInBold)
                    {
                        line.Replace(" & ", "** **&** **");
                    }
                    requireSave = true;
                }
            }

            return requireSave;
        }

        private List<MediaType> unknownMediaTypes = new List<MediaType> { MediaType.CraveReceiptLogo, MediaType.CraveConsoleLogo, MediaType.Gallery, MediaType.GallerySmall, MediaType.GalleryLarge };

        private void ReuploadMediaForUnknownDeviceType(bool isTest)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(MediaRatioTypeMediaFields.MediaType == unknownMediaTypes);

            PrefetchPath prefetch = new PrefetchPath(EntityType.MediaRatioTypeMediaEntity);
            prefetch.Add(MediaRatioTypeMediaEntity.PrefetchPathMediaEntity);

            MediaRatioTypeMediaCollection mediaRatioTypeMediaCollection = new MediaRatioTypeMediaCollection();
            mediaRatioTypeMediaCollection.GetMulti(filter, prefetch);

            foreach (MediaRatioTypeMediaEntity mediaRatioTypeMediaEntity in mediaRatioTypeMediaCollection)
            {
                if (!isTest)
                {
                    MediaHelper.QueueMediaRatioTypeMediaFileTask(mediaRatioTypeMediaEntity, MediaProcessingTaskEntity.ProcessingAction.Upload, true, null, null);
                }                
            }

            if (isTest)
            {
                this.lblReuploadMediaForUnknownDeviceType.Text = string.Format("{0} media to upload", mediaRatioTypeMediaCollection.Count);
            }
        }

        private void SyncCountryCodes()
        {
            CompanyCollection companies = new CompanyCollection();
            companies.GetMulti(null);

            foreach (CompanyEntity company in companies)
            {
                if (Country.Mappings.ContainsKey(company.CountryEntity.CodeAlpha3))
                {
                    company.CountryCode = company.CountryEntity.CodeAlpha3;
                    company.Save();
                }
            }
        }

        private CustomTextCollection GetUpdatedCustomTextsWithoutCultureCode()
        {
            CustomTextCollection customTexts = new CustomTextCollection();

            if (this.cbSetCultureCodeForActionButton.Checked)
            {
                customTexts.AddRange(this.GetUpdatedCustomTextsWithoutCultureCodeForActionButton());
            }

            if (this.cbSetCultureCodeForAdvertisement.Checked)
            {
                customTexts.AddRange(this.GetUpdatedCustomTextsWithoutCultureCodeForAdvertisement());
            }

            if (this.cbSetCultureCodeForAlteration.Checked)
            {
                customTexts.AddRange(this.GetUpdatedCustomTextsWithoutCultureCodeForAlteration());
            }

            if (this.cbSetCultureCodeForAlterationoption.Checked)
            {
                customTexts.AddRange(this.GetUpdatedCustomTextsWithoutCultureCodeForAlterationoption());
            }

            if (this.cbSetCultureCodeForAmenity.Checked)
            {
                customTexts.AddRange(this.GetUpdatedCustomTextsWithoutCultureCodeForAmenity());
            }

            if (this.cbSetCultureCodeForAnnouncement.Checked)
            {
                customTexts.AddRange(this.GetUpdatedCustomTextsWithoutCultureCodeForAnnouncement());
            }

            if (this.cbSetCultureCodeForAttachment.Checked)
            {
                customTexts.AddRange(this.GetUpdatedCustomTextsWithoutCultureCodeForAttachment());
            }

            if (this.cbSetCultureCodeForCategory.Checked)
            {
                customTexts.AddRange(this.GetUpdatedCustomTextsWithoutCultureCodeForCategory());
            }

            if (this.cbSetCultureCodeForEntertainmentcategory.Checked)
            {
                customTexts.AddRange(this.GetUpdatedCustomTextsWithoutCultureCodeForEntertainmentcategory());
            }

            if (this.cbSetCultureCodeForGenericcategory.Checked)
            {
                customTexts.AddRange(this.GetUpdatedCustomTextsWithoutCultureCodeForGenericcategory());
            }

            if (this.cbSetCultureCodeForGenericproduct.Checked)
            {
                customTexts.AddRange(this.GetUpdatedCustomTextsWithoutCultureCodeForGenericproduct());
            }

            if (this.cbSetCultureCodeForPage.Checked)
            {
                customTexts.AddRange(this.GetUpdatedCustomTextsWithoutCultureCodeForPage());
            }

            if (this.cbSetCultureCodeForPageTemplate.Checked)
            {
                customTexts.AddRange(this.GetUpdatedCustomTextsWithoutCultureCodeForPageTemplate());
            }

            if (this.cbSetCultureCodeForPointOfInterest.Checked)
            {
                customTexts.AddRange(this.GetUpdatedCustomTextsWithoutCultureCodeForPointOfInterest());
            }

            if (this.cbSetCultureCodeForRoomControlArea.Checked)
            {
                customTexts.AddRange(this.GetUpdatedCustomTextsWithoutCultureCodeForRoomControlArea());
            }

            if (this.cbSetCultureCodeForRoomControlComponent.Checked)
            {
                customTexts.AddRange(this.GetUpdatedCustomTextsWithoutCultureCodeForRoomControlComponent());
            }

            if (this.cbSetCultureCodeForRoomControlSection.Checked)
            {
                customTexts.AddRange(this.GetUpdatedCustomTextsWithoutCultureCodeForRoomControlSection());
            }

            if (this.cbSetCultureCodeForRoomControlSectionItem.Checked)
            {
                customTexts.AddRange(this.GetUpdatedCustomTextsWithoutCultureCodeForRoomControlSectionItem());
            }

            if (this.cbSetCultureCodeForRoomControlWidget.Checked)
            {
                customTexts.AddRange(this.GetUpdatedCustomTextsWithoutCultureCodeForRoomControlWidget());
            }

            if (this.cbSetCultureCodeForProduct.Checked)
            {
                customTexts.AddRange(this.GetUpdatedCustomTextsWithoutCultureCodeForProduct());
            }

            if (this.cbSetCultureCodeForSite.Checked)
            {
                customTexts.AddRange(this.GetUpdatedCustomTextsWithoutCultureCodeForSite());
            }

            if (this.cbSetCultureCodeForSiteTemplate.Checked)
            {
                customTexts.AddRange(this.GetUpdatedCustomTextsWithoutCultureCodeForSiteTemplate());
            }

            if (this.cbSetCultureCodeForUIFooterItem.Checked)
            {
                customTexts.AddRange(this.GetUpdatedCustomTextsWithoutCultureCodeForUIFooterItem());
            }

            if (this.cbSetCultureCodeForUITab.Checked)
            {
                customTexts.AddRange(this.GetUpdatedCustomTextsWithoutCultureCodeForUITab());
            }

            if (this.cbSetCultureCodeForUIWidget.Checked)
            {
                customTexts.AddRange(this.GetUpdatedCustomTextsWithoutCultureCodeForUIWidget());
            }

            if (this.cbSetCultureCodeForVenueCategory.Checked)
            {
                customTexts.AddRange(this.GetUpdatedCustomTextsWithoutCultureCodeForVenueCategory());
            }

            return customTexts;
        }

        private CustomTextCollection GetUpdatedCustomTextsWithoutCultureCodeForActionButton()
        {
            PredicateExpression filter = new PredicateExpression(CustomTextFields.ActionButtonId != DBNull.Value);

            PrefetchPath prefetch = new PrefetchPath(EntityType.CustomTextEntity);
            prefetch.Add(CustomTextEntityBase.PrefetchPathActionButtonEntity).SubPath.Add(ActionButtonEntityBase.PrefetchPathActionButtonLanguageCollection);

            CustomTextCollection customTexts = this.GetCustomTextsWithCustomFilterAndPrefetch(filter, prefetch);

            foreach (CustomTextEntity customText in customTexts)
            {
                foreach (ActionButtonLanguageEntity actionButtonLanguage in customText.ActionButtonEntity.ActionButtonLanguageCollection)
                {
                    if (actionButtonLanguage.Name.Equals(customText.Text, StringComparison.InvariantCultureIgnoreCase) && 
                        !customTexts.Any(x => x.ActionButtonId == customText.ActionButtonId && x.Type == customText.Type && x.LanguageId == actionButtonLanguage.LanguageId))
                    {
                        customText.LanguageId = actionButtonLanguage.LanguageId;
                    }
                }                
            }

            return customTexts;
        }

        private CustomTextCollection GetUpdatedCustomTextsWithoutCultureCodeForAdvertisement()
        {
            PredicateExpression filter = new PredicateExpression(CustomTextFields.AdvertisementId != DBNull.Value);

            PrefetchPath prefetch = new PrefetchPath(EntityType.CustomTextEntity);
            prefetch.Add(CustomTextEntityBase.PrefetchPathAdvertisementEntity).SubPath.Add(AdvertisementEntityBase.PrefetchPathAdvertisementLanguageCollection);

            CustomTextCollection customTexts = this.GetCustomTextsWithCustomFilterAndPrefetch(filter, prefetch);

            foreach (CustomTextEntity customText in customTexts)
            {
                foreach (AdvertisementLanguageEntity advertisementLanguage in customText.AdvertisementEntity.AdvertisementLanguageCollection)
                {
                    if (customText.Type == CustomTextType.AdvertisementName && advertisementLanguage.Name.Equals(customText.Text, StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (!customTexts.Any(x => x.AdvertisementId == customText.AdvertisementId && x.Type == customText.Type && x.LanguageId == advertisementLanguage.LanguageId))
                        {
                            customText.LanguageId = advertisementLanguage.LanguageId;
                        }
                    }
                    else if (customText.Type == CustomTextType.AdvertisementDescription && advertisementLanguage.Description.Equals(customText.Text, StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (!customTexts.Any(x => x.AdvertisementId == customText.AdvertisementId && x.Type == customText.Type && x.LanguageId == advertisementLanguage.LanguageId))
                        {
                            customText.LanguageId = advertisementLanguage.LanguageId;
                        }
                    }
                }                
            }

            return customTexts;
        }

        private CustomTextCollection GetUpdatedCustomTextsWithoutCultureCodeForAlteration()
        {
            PredicateExpression filter = new PredicateExpression(CustomTextFields.AlterationId != DBNull.Value);

            PrefetchPath prefetch = new PrefetchPath(EntityType.CustomTextEntity);
            prefetch.Add(CustomTextEntityBase.PrefetchPathAlterationEntity).SubPath.Add(AlterationEntityBase.PrefetchPathAlterationLanguageCollection);

            CustomTextCollection customTexts = this.GetCustomTextsWithCustomFilterAndPrefetch(filter, prefetch);

            foreach (CustomTextEntity customText in customTexts)
            {
                foreach (AlterationLanguageEntity alterationLangauge in customText.AlterationEntity.AlterationLanguageCollection)
                {
                    if (customText.Type == CustomTextType.AlterationName && alterationLangauge.Name.Equals(customText.Text, StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (!customTexts.Any(x => x.AlterationId == customText.AlterationId && x.Type == customText.Type && x.LanguageId == alterationLangauge.LanguageId))
                        {
                            customText.LanguageId = alterationLangauge.LanguageId;
                        }
                    }
                    else if (customText.Type == CustomTextType.AlterationDescription && alterationLangauge.Description.Equals(customText.Text, StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (!customTexts.Any(x => x.AlterationId == customText.AlterationId && x.Type == customText.Type && x.LanguageId == alterationLangauge.LanguageId))
                        {
                            customText.LanguageId = alterationLangauge.LanguageId;
                        }
                    }
                }                
            }

            return customTexts;
        }

        private CustomTextCollection GetUpdatedCustomTextsWithoutCultureCodeForAlterationoption()
        {
            PredicateExpression filter = new PredicateExpression(CustomTextFields.AlterationoptionId != DBNull.Value);

            PrefetchPath prefetch = new PrefetchPath(EntityType.CustomTextEntity);
            prefetch.Add(CustomTextEntityBase.PrefetchPathAlterationoptionEntity).SubPath.Add(AlterationoptionEntityBase.PrefetchPathAlterationoptionLanguageCollection);

            CustomTextCollection customTexts = this.GetCustomTextsWithCustomFilterAndPrefetch(filter, prefetch);

            foreach (CustomTextEntity customText in customTexts)
            {
                foreach (AlterationoptionLanguageEntity alterationoptionLanguage in customText.AlterationoptionEntity.AlterationoptionLanguageCollection)
                {
                    if (customText.Type == CustomTextType.AlterationoptionName && alterationoptionLanguage.Name.Equals(customText.Text, StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (!customTexts.Any(x => x.AlterationoptionId == customText.AlterationoptionId && x.Type == customText.Type && x.LanguageId == alterationoptionLanguage.LanguageId))
                        {
                            customText.LanguageId = alterationoptionLanguage.LanguageId;
                        }
                    }
                    else if (customText.Type == CustomTextType.AlterationoptionDescription && alterationoptionLanguage.Description.Equals(customText.Text, StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (!customTexts.Any(x => x.AlterationoptionId == customText.AlterationoptionId && x.Type == customText.Type && x.LanguageId == alterationoptionLanguage.LanguageId))
                        {
                            customText.LanguageId = alterationoptionLanguage.LanguageId;
                        }
                    }
                }                
            }

            return customTexts;
        }

        private CustomTextCollection GetUpdatedCustomTextsWithoutCultureCodeForAmenity()
        {
            PredicateExpression filter = new PredicateExpression(CustomTextFields.AmenityId != DBNull.Value);

            PrefetchPath prefetch = new PrefetchPath(EntityType.CustomTextEntity);
            prefetch.Add(CustomTextEntityBase.PrefetchPathAmenityEntity).SubPath.Add(AmenityEntityBase.PrefetchPathAmenityLanguageCollection);

            CustomTextCollection customTexts = this.GetCustomTextsWithCustomFilterAndPrefetch(filter, prefetch);

            foreach (CustomTextEntity customText in customTexts)
            {
                foreach (AmenityLanguageEntity amenityLanguage in customText.AmenityEntity.AmenityLanguageCollection)
                {
                    if (customText.Type == CustomTextType.AmenityName && amenityLanguage.Name.Equals(customText.Text, StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (!customTexts.Any(x => x.AmenityId == customText.AmenityId && x.Type == customText.Type && x.LanguageId == amenityLanguage.LanguageId))
                        {
                            customText.LanguageId = amenityLanguage.LanguageId;
                        }
                    }
                }                
            }

            return customTexts;
        }

        private CustomTextCollection GetUpdatedCustomTextsWithoutCultureCodeForAnnouncement()
        {
            PredicateExpression filter = new PredicateExpression(CustomTextFields.AnnouncementId != DBNull.Value);

            PrefetchPath prefetch = new PrefetchPath(EntityType.CustomTextEntity);
            prefetch.Add(CustomTextEntityBase.PrefetchPathAnnouncementEntity).SubPath.Add(AnnouncementEntityBase.PrefetchPathAnnouncementLanguageCollection);

            CustomTextCollection customTexts = this.GetCustomTextsWithCustomFilterAndPrefetch(filter, prefetch);

            foreach (CustomTextEntity customText in customTexts)
            {
                foreach (AnnouncementLanguageEntity announcementLanguage in customText.AnnouncementEntity.AnnouncementLanguageCollection)
                {
                    if (customText.Type == CustomTextType.AnnouncementTitle && announcementLanguage.Title.Equals(customText.Text, StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (!customTexts.Any(x => x.AnnouncementId == customText.AnnouncementId && x.Type == customText.Type && x.LanguageId == announcementLanguage.LanguageId))
                        {
                            customText.LanguageId = announcementLanguage.LanguageId;
                        }
                    }
                    else if (customText.Type == CustomTextType.AnnouncementText && announcementLanguage.Text.Equals(customText.Text, StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (!customTexts.Any(x => x.AnnouncementId == customText.AnnouncementId && x.Type == customText.Type && x.LanguageId == announcementLanguage.LanguageId))
                        {
                            customText.LanguageId = announcementLanguage.LanguageId;
                        }
                    }
                }                
            }

            return customTexts;
        }

        private CustomTextCollection GetUpdatedCustomTextsWithoutCultureCodeForAttachment()
        {
            PredicateExpression filter = new PredicateExpression(CustomTextFields.AttachmentId != DBNull.Value);

            PrefetchPath prefetch = new PrefetchPath(EntityType.CustomTextEntity);
            prefetch.Add(CustomTextEntityBase.PrefetchPathAttachmentEntity).SubPath.Add(AttachmentEntityBase.PrefetchPathAttachmentLanguageCollection);

            CustomTextCollection customTexts = this.GetCustomTextsWithCustomFilterAndPrefetch(filter, prefetch);

            foreach (CustomTextEntity customText in customTexts)
            {
                foreach (AttachmentLanguageEntity attachmentLanguage in customText.AttachmentEntity.AttachmentLanguageCollection)
                {
                    if (customText.Type == CustomTextType.AttachmentName && attachmentLanguage.Name.Equals(customText.Text, StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (!customTexts.Any(x => x.AttachmentId == customText.AttachmentId && x.Type == customText.Type && x.LanguageId == attachmentLanguage.LanguageId))
                        {
                            customText.LanguageId = attachmentLanguage.LanguageId;
                        }
                    }
                }                
            }

            return customTexts;
        }

        private CustomTextCollection GetUpdatedCustomTextsWithoutCultureCodeForCategory()
        {
            PredicateExpression filter = new PredicateExpression(CustomTextFields.CategoryId != DBNull.Value);

            PrefetchPath prefetch = new PrefetchPath(EntityType.CustomTextEntity);
            prefetch.Add(CustomTextEntityBase.PrefetchPathCategoryEntity).SubPath.Add(CategoryEntityBase.PrefetchPathCategoryLanguageCollection);

            CustomTextCollection customTexts = this.GetCustomTextsWithCustomFilterAndPrefetch(filter, prefetch);

            foreach (CustomTextEntity customText in customTexts)
            {
                foreach (CategoryLanguageEntity categoryLanguage in customText.CategoryEntity.CategoryLanguageCollection)
                {
                    if (customText.Type == CustomTextType.CategoryName && categoryLanguage.Name.Equals(customText.Text, StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (!customTexts.Any(x => x.CategoryId == customText.CategoryId && x.Type == customText.Type && x.LanguageId == categoryLanguage.LanguageId))
                        {
                            customText.LanguageId = categoryLanguage.LanguageId;
                        }
                    }
                    else if (customText.Type == CustomTextType.CategoryDescription && categoryLanguage.Description.Equals(customText.Text, StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (!customTexts.Any(x => x.CategoryId == customText.CategoryId && x.Type == customText.Type && x.LanguageId == categoryLanguage.LanguageId))
                        {
                            customText.LanguageId = categoryLanguage.LanguageId;
                        }
                    }                    
                }                
            }

            return customTexts;
        }

        private CustomTextCollection GetUpdatedCustomTextsWithoutCultureCodeForEntertainmentcategory()
        {
            PredicateExpression filter = new PredicateExpression(CustomTextFields.EntertainmentcategoryId != DBNull.Value);

            PrefetchPath prefetch = new PrefetchPath(EntityType.CustomTextEntity);
            prefetch.Add(CustomTextEntityBase.PrefetchPathEntertainmentcategoryEntity).SubPath.Add(EntertainmentcategoryEntityBase.PrefetchPathEntertainmentcategoryLanguageCollection);

            CustomTextCollection customTexts = this.GetCustomTextsWithCustomFilterAndPrefetch(filter, prefetch);

            foreach (CustomTextEntity customText in customTexts)
            {
                foreach (EntertainmentcategoryLanguageEntity entertainmentcategoryLanguageEntity in customText.EntertainmentcategoryEntity.EntertainmentcategoryLanguageCollection)
                {
                    if (customText.Type == CustomTextType.EntertainmentcategoryName && entertainmentcategoryLanguageEntity.Name.Equals(customText.Text, StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (!customTexts.Any(x => x.EntertainmentcategoryId == customText.EntertainmentcategoryId && x.Type == customText.Type && x.LanguageId == entertainmentcategoryLanguageEntity.LanguageId))
                        {
                            customText.LanguageId = entertainmentcategoryLanguageEntity.LanguageId;
                        }
                    }
                }                
            }

            return customTexts;
        }

        private CustomTextCollection GetUpdatedCustomTextsWithoutCultureCodeForGenericcategory()
        {
            PredicateExpression filter = new PredicateExpression(CustomTextFields.GenericcategoryId != DBNull.Value);

            PrefetchPath prefetch = new PrefetchPath(EntityType.CustomTextEntity);
            prefetch.Add(CustomTextEntityBase.PrefetchPathGenericcategoryEntity).SubPath.Add(GenericcategoryEntityBase.PrefetchPathGenericcategoryLanguageCollection);

            CustomTextCollection customTexts = this.GetCustomTextsWithCustomFilterAndPrefetch(filter, prefetch);

            foreach (CustomTextEntity customText in customTexts)
            {
                foreach (GenericcategoryLanguageEntity genericcategoryLanguage in customText.GenericcategoryEntity.GenericcategoryLanguageCollection)
                {
                    if (customText.Type == CustomTextType.GenericcategoryName && genericcategoryLanguage.Name.Equals(customText.Text, StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (!customTexts.Any(x => x.GenericcategoryId == customText.GenericcategoryId && x.Type == customText.Type && x.LanguageId == genericcategoryLanguage.LanguageId))
                        {
                            customText.LanguageId = genericcategoryLanguage.LanguageId;
                        }
                    }
                }                
            }

            return customTexts;
        }

        private CustomTextCollection GetUpdatedCustomTextsWithoutCultureCodeForGenericproduct()
        {
            PredicateExpression filter = new PredicateExpression(CustomTextFields.GenericproductId != DBNull.Value);

            PrefetchPath prefetch = new PrefetchPath(EntityType.CustomTextEntity);
            prefetch.Add(CustomTextEntityBase.PrefetchPathGenericproductEntity).SubPath.Add(GenericproductEntityBase.PrefetchPathGenericproductLanguageCollection);

            CustomTextCollection customTexts = this.GetCustomTextsWithCustomFilterAndPrefetch(filter, prefetch);

            foreach (CustomTextEntity customText in customTexts)
            {
                foreach (GenericproductLanguageEntity genericproductLanguage in customText.GenericproductEntity.GenericproductLanguageCollection)
                {
                    if (customText.Type == CustomTextType.GenericproductName && genericproductLanguage.Name.Equals(customText.Text, StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (!customTexts.Any(x => x.GenericproductId == customText.GenericproductId && x.Type == customText.Type && x.LanguageId == genericproductLanguage.LanguageId))
                        {
                            customText.LanguageId = genericproductLanguage.LanguageId;
                        }
                    }
                    else if (customText.Type == CustomTextType.GenericproductDescription && genericproductLanguage.Description.Equals(customText.Text, StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (!customTexts.Any(x => x.GenericproductId == customText.GenericproductId && x.Type == customText.Type && x.LanguageId == genericproductLanguage.LanguageId))
                        {
                            customText.LanguageId = genericproductLanguage.LanguageId;
                        }
                    }
                }                
            }

            return customTexts;
        }

        private CustomTextCollection GetUpdatedCustomTextsWithoutCultureCodeForPage()
        {
            PredicateExpression filter = new PredicateExpression(CustomTextFields.PageId != DBNull.Value);

            PrefetchPath prefetch = new PrefetchPath(EntityType.CustomTextEntity);
            prefetch.Add(CustomTextEntityBase.PrefetchPathPageEntity).SubPath.Add(PageEntityBase.PrefetchPathPageLanguageCollection);

            CustomTextCollection customTexts = this.GetCustomTextsWithCustomFilterAndPrefetch(filter, prefetch);

            foreach (CustomTextEntity customText in customTexts)
            {
                foreach (PageLanguageEntity pageLanguage in customText.PageEntity.PageLanguageCollection)
                {
                    if (customText.Type == CustomTextType.PageName && pageLanguage.Name.Equals(customText.Text, StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (!customTexts.Any(x => x.PageId == customText.PageId && x.Type == customText.Type && x.LanguageId == pageLanguage.LanguageId))
                        {
                            customText.LanguageId = pageLanguage.LanguageId;
                        }
                    }
                }                
            }

            return customTexts;
        }

        private CustomTextCollection GetUpdatedCustomTextsWithoutCultureCodeForPageTemplate()
        {
            PredicateExpression filter = new PredicateExpression(CustomTextFields.PageTemplateId != DBNull.Value);

            PrefetchPath prefetch = new PrefetchPath(EntityType.CustomTextEntity);
            prefetch.Add(CustomTextEntityBase.PrefetchPathPageTemplateEntity).SubPath.Add(PageTemplateEntityBase.PrefetchPathPageTemplateLanguageCollection);

            CustomTextCollection customTexts = this.GetCustomTextsWithCustomFilterAndPrefetch(filter, prefetch);

            foreach (CustomTextEntity customText in customTexts)
            {
                foreach (PageTemplateLanguageEntity pageTemplateLanguage in customText.PageTemplateEntity.PageTemplateLanguageCollection)
                {
                    if (customText.Type == CustomTextType.PageTemplateName && pageTemplateLanguage.Name.Equals(customText.Text, StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (!customTexts.Any(x => x.PageTemplateId == customText.PageTemplateId && x.Type == customText.Type && x.LanguageId == pageTemplateLanguage.LanguageId))
                        {
                            customText.LanguageId = pageTemplateLanguage.LanguageId;
                        }
                    }
                }                
            }

            return customTexts;
        }

        private CustomTextCollection GetUpdatedCustomTextsWithoutCultureCodeForPointOfInterest()
        {
            PredicateExpression filter = new PredicateExpression(CustomTextFields.PointOfInterestId != DBNull.Value);

            PrefetchPath prefetch = new PrefetchPath(EntityType.CustomTextEntity);
            prefetch.Add(CustomTextEntityBase.PrefetchPathPointOfInterestEntity).SubPath.Add(PointOfInterestEntityBase.PrefetchPathPointOfInterestLanguageCollection);

            CustomTextCollection customTexts = this.GetCustomTextsWithCustomFilterAndPrefetch(filter, prefetch);

            foreach (CustomTextEntity customText in customTexts)
            {
                foreach (PointOfInterestLanguageEntity pointOfInterestLanguageEntity in customText.PointOfInterestEntity.PointOfInterestLanguageCollection)
                {
                    if (customText.Type == CustomTextType.PointOfInterestDescription && pointOfInterestLanguageEntity.Description.Equals(customText.Text, StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (!customTexts.Any(x => x.PointOfInterestId == customText.PointOfInterestId && x.Type == customText.Type && x.LanguageId == pointOfInterestLanguageEntity.LanguageId))
                        {
                            customText.LanguageId = pointOfInterestLanguageEntity.LanguageId;
                        }
                    }
                    else if (customText.Type == CustomTextType.PointOfInterestDescriptionSingleLine && pointOfInterestLanguageEntity.DescriptionSingleLine.Equals(customText.Text, StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (!customTexts.Any(x => x.PointOfInterestId == customText.PointOfInterestId && x.Type == customText.Type && x.LanguageId == pointOfInterestLanguageEntity.LanguageId))
                        {
                            customText.LanguageId = pointOfInterestLanguageEntity.LanguageId;
                        }
                    }
                    else if (customText.Type == CustomTextType.PointOfInterestVenuePageDescription && pointOfInterestLanguageEntity.VenuePageDescription.Equals(customText.Text, StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (!customTexts.Any(x => x.PointOfInterestId == customText.PointOfInterestId && x.Type == customText.Type && x.LanguageId == pointOfInterestLanguageEntity.LanguageId))
                        {
                            customText.LanguageId = pointOfInterestLanguageEntity.LanguageId;
                        }
                    }
                }                
            }

            return customTexts;
        }

        private CustomTextCollection GetUpdatedCustomTextsWithoutCultureCodeForRoomControlArea()
        {
            PredicateExpression filter = new PredicateExpression(CustomTextFields.RoomControlAreaId != DBNull.Value);

            PrefetchPath prefetch = new PrefetchPath(EntityType.CustomTextEntity);
            prefetch.Add(CustomTextEntityBase.PrefetchPathRoomControlAreaEntity).SubPath.Add(RoomControlAreaEntityBase.PrefetchPathRoomControlAreaLanguageCollection);

            CustomTextCollection customTexts = this.GetCustomTextsWithCustomFilterAndPrefetch(filter, prefetch);

            foreach (CustomTextEntity customText in customTexts)
            {
                foreach (RoomControlAreaLanguageEntity roomControlAreaLanguage in customText.RoomControlAreaEntity.RoomControlAreaLanguageCollection)
                {
                    if (customText.Type == CustomTextType.RoomControlAreaName && roomControlAreaLanguage.Name.Equals(customText.Text, StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (!customTexts.Any(x => x.RoomControlAreaId == customText.RoomControlAreaId && x.Type == customText.Type && x.LanguageId == roomControlAreaLanguage.LanguageId))
                        {
                            customText.LanguageId = roomControlAreaLanguage.LanguageId;
                        }
                    }
                }                
            }

            return customTexts;
        }

        private CustomTextCollection GetUpdatedCustomTextsWithoutCultureCodeForRoomControlComponent()
        {
            PredicateExpression filter = new PredicateExpression(CustomTextFields.RoomControlComponentId != DBNull.Value);

            PrefetchPath prefetch = new PrefetchPath(EntityType.CustomTextEntity);
            prefetch.Add(CustomTextEntityBase.PrefetchPathRoomControlComponentEntity).SubPath.Add(RoomControlComponentEntityBase.PrefetchPathRoomControlComponentLanguageCollection);

            CustomTextCollection customTexts = this.GetCustomTextsWithCustomFilterAndPrefetch(filter, prefetch);

            foreach (CustomTextEntity customText in customTexts)
            {
                foreach (RoomControlComponentLanguageEntity roomControlComponentLanguage in customText.RoomControlComponentEntity.RoomControlComponentLanguageCollection)
                {
                    if (customText.Type == CustomTextType.RoomControlComponentName && roomControlComponentLanguage.Name.Equals(customText.Text, StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (!customTexts.Any(x => x.RoomControlComponentId == customText.RoomControlComponentId && x.Type == customText.Type && x.LanguageId == roomControlComponentLanguage.LanguageId))
                        {
                            customText.LanguageId = roomControlComponentLanguage.LanguageId;
                        }
                    }
                }                
            }

            return customTexts;
        }

        private CustomTextCollection GetUpdatedCustomTextsWithoutCultureCodeForRoomControlSection()
        {
            PredicateExpression filter = new PredicateExpression(CustomTextFields.RoomControlSectionId != DBNull.Value);

            PrefetchPath prefetch = new PrefetchPath(EntityType.CustomTextEntity);
            prefetch.Add(CustomTextEntityBase.PrefetchPathRoomControlSectionEntity).SubPath.Add(RoomControlSectionEntityBase.PrefetchPathRoomControlSectionLanguageCollection);

            CustomTextCollection customTexts = this.GetCustomTextsWithCustomFilterAndPrefetch(filter, prefetch);

            foreach (CustomTextEntity customText in customTexts)
            {
                foreach (RoomControlSectionLanguageEntity roomControlSectionLanguage in customText.RoomControlSectionEntity.RoomControlSectionLanguageCollection)
                {
                    if (customText.Type == CustomTextType.RoomControlSectionName && roomControlSectionLanguage.Name.Equals(customText.Text, StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (!customTexts.Any(x => x.RoomControlSectionId == customText.RoomControlSectionId && x.Type == customText.Type && x.LanguageId == roomControlSectionLanguage.LanguageId))
                        {
                            customText.LanguageId = roomControlSectionLanguage.LanguageId;
                        }
                    }
                }                
            }

            return customTexts;
        }

        private CustomTextCollection GetUpdatedCustomTextsWithoutCultureCodeForRoomControlSectionItem()
        {
            PredicateExpression filter = new PredicateExpression(CustomTextFields.RoomControlSectionItemId != DBNull.Value);

            PrefetchPath prefetch = new PrefetchPath(EntityType.CustomTextEntity);
            prefetch.Add(CustomTextEntityBase.PrefetchPathRoomControlSectionItemEntity).SubPath.Add(RoomControlSectionItemEntityBase.PrefetchPathRoomControlSectionItemLanguageCollection);

            CustomTextCollection customTexts = this.GetCustomTextsWithCustomFilterAndPrefetch(filter, prefetch);

            foreach (CustomTextEntity customText in customTexts)
            {
                foreach (RoomControlSectionItemLanguageEntity roomControlSectionItemLanguage in customText.RoomControlSectionItemEntity.RoomControlSectionItemLanguageCollection)
                {
                    if (customText.Type == CustomTextType.RoomControlSectionItemName && roomControlSectionItemLanguage.Name.Equals(customText.Text, StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (!customTexts.Any(x => x.RoomControlSectionItemId == customText.RoomControlSectionItemId && x.Type == customText.Type && x.LanguageId == roomControlSectionItemLanguage.LanguageId))
                        {
                            customText.LanguageId = roomControlSectionItemLanguage.LanguageId;
                        }
                    }
                }                
            }

            return customTexts;
        }

        private CustomTextCollection GetUpdatedCustomTextsWithoutCultureCodeForRoomControlWidget()
        {
            PredicateExpression filter = new PredicateExpression(CustomTextFields.RoomControlWidgetId != DBNull.Value);

            PrefetchPath prefetch = new PrefetchPath(EntityType.CustomTextEntity);
            prefetch.Add(CustomTextEntityBase.PrefetchPathRoomControlWidgetEntity).SubPath.Add(RoomControlWidgetEntityBase.PrefetchPathRoomControlWidgetLanguageCollection);

            CustomTextCollection customTexts = this.GetCustomTextsWithCustomFilterAndPrefetch(filter, prefetch);

            foreach (CustomTextEntity customText in customTexts)
            {
                foreach (RoomControlWidgetLanguageEntity roomControlWidgetLanguage in customText.RoomControlWidgetEntity.RoomControlWidgetLanguageCollection)
                {
                    if (customText.Type == CustomTextType.RoomControlWidgetCaption && roomControlWidgetLanguage.Caption.Equals(customText.Text, StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (!customTexts.Any(x => x.RoomControlWidgetId == customText.RoomControlWidgetId && x.Type == customText.Type && x.LanguageId == roomControlWidgetLanguage.LanguageId))
                        {
                            customText.LanguageId = roomControlWidgetLanguage.LanguageId;
                        }
                    }
                }                
            }

            return customTexts;
        }

        private CustomTextCollection GetUpdatedCustomTextsWithoutCultureCodeForProduct()
        {
            PredicateExpression filter = new PredicateExpression(CustomTextFields.ProductId != DBNull.Value);

            PrefetchPath prefetch = new PrefetchPath(EntityType.CustomTextEntity);
            prefetch.Add(CustomTextEntityBase.PrefetchPathProductEntity).SubPath.Add(ProductEntityBase.PrefetchPathProductLanguageCollection);

            CustomTextCollection customTexts = this.GetCustomTextsWithCustomFilterAndPrefetch(filter, prefetch);

            foreach (CustomTextEntity customText in customTexts)
            {
                foreach (ProductLanguageEntity productLanguage in customText.ProductEntity.ProductLanguageCollection)
                {
                    if (customText.Type == CustomTextType.ProductName && productLanguage.Name.Equals(customText.Text, StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (!customTexts.Any(x => x.ProductId == customText.ProductId && x.Type == customText.Type && x.LanguageId == productLanguage.LanguageId))
                        {
                            customText.LanguageId = productLanguage.LanguageId;
                        }
                    }
                    else if (customText.Type == CustomTextType.ProductDescription && productLanguage.Description.Equals(customText.Text, StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (!customTexts.Any(x => x.ProductId == customText.ProductId && x.Type == customText.Type && x.LanguageId == productLanguage.LanguageId))
                        {
                            customText.LanguageId = productLanguage.LanguageId;
                        }
                    }
                }                
            }

            return customTexts;
        }

        private CustomTextCollection GetUpdatedCustomTextsWithoutCultureCodeForSite()
        {
            PredicateExpression filter = new PredicateExpression(CustomTextFields.SiteId != DBNull.Value);

            PrefetchPath prefetch = new PrefetchPath(EntityType.CustomTextEntity);
            prefetch.Add(CustomTextEntityBase.PrefetchPathSiteEntity).SubPath.Add(SiteEntityBase.PrefetchPathSiteLanguageCollection);

            CustomTextCollection customTexts = this.GetCustomTextsWithCustomFilterAndPrefetch(filter, prefetch);

            foreach (CustomTextEntity customText in customTexts)
            {
                foreach (SiteLanguageEntity siteLanguage in customText.SiteEntity.SiteLanguageCollection)
                {
                    if (customText.Type == CustomTextType.SiteDescription && siteLanguage.Description.Equals(customText.Text, StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (!customTexts.Any(x => x.SiteId == customText.SiteId && x.Type == customText.Type && x.LanguageId == siteLanguage.LanguageId))
                        {
                            customText.LanguageId = siteLanguage.LanguageId;
                        }
                    }
                }                
            }

            return customTexts;
        }

        private CustomTextCollection GetUpdatedCustomTextsWithoutCultureCodeForSiteTemplate()
        {
            PredicateExpression filter = new PredicateExpression(CustomTextFields.SiteTemplateId != DBNull.Value);

            PrefetchPath prefetch = new PrefetchPath(EntityType.CustomTextEntity);
            prefetch.Add(CustomTextEntityBase.PrefetchPathSiteTemplateEntity).SubPath.Add(SiteTemplateEntityBase.PrefetchPathSiteTemplateLanguageCollection);

            CustomTextCollection customTexts = this.GetCustomTextsWithCustomFilterAndPrefetch(filter, prefetch);

            foreach (CustomTextEntity customText in customTexts)
            {
                foreach (SiteTemplateLanguageEntity siteTemplateLanguage in customText.SiteTemplateEntity.SiteTemplateLanguageCollection)
                {
                    if (customText.Type == CustomTextType.SiteTemplateName && siteTemplateLanguage.Name.Equals(customText.Text, StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (!customTexts.Any(x => x.SiteTemplateId == customText.SiteTemplateId && x.Type == customText.Type && x.LanguageId == siteTemplateLanguage.LanguageId))
                        {
                            customText.LanguageId = siteTemplateLanguage.LanguageId;
                        }
                    }
                }                
            }

            return customTexts;
        }

        private CustomTextCollection GetUpdatedCustomTextsWithoutCultureCodeForUIFooterItem()
        {
            PredicateExpression filter = new PredicateExpression(CustomTextFields.UIFooterItemId != DBNull.Value);

            PrefetchPath prefetch = new PrefetchPath(EntityType.CustomTextEntity);
            prefetch.Add(CustomTextEntityBase.PrefetchPathUIFooterItemEntity).SubPath.Add(UIFooterItemEntityBase.PrefetchPathUIFooterItemLanguageCollection);

            CustomTextCollection customTexts = this.GetCustomTextsWithCustomFilterAndPrefetch(filter, prefetch);

            foreach (CustomTextEntity customText in customTexts)
            {
                foreach (UIFooterItemLanguageEntity uiFooterItemLanguage in customText.UIFooterItemEntity.UIFooterItemLanguageCollection)
                {
                    if (customText.Type == CustomTextType.UIFooterItemName && uiFooterItemLanguage.Name.Equals(customText.Text, StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (!customTexts.Any(x => x.UIFooterItemId == customText.UIFooterItemId && x.Type == customText.Type && x.LanguageId == uiFooterItemLanguage.LanguageId))
                        {
                            customText.LanguageId = uiFooterItemLanguage.LanguageId;
                        }
                    }
                }                
            }

            return customTexts;
        }

        private CustomTextCollection GetUpdatedCustomTextsWithoutCultureCodeForUITab()
        {
            PredicateExpression filter = new PredicateExpression(CustomTextFields.UITabId != DBNull.Value);

            PrefetchPath prefetch = new PrefetchPath(EntityType.CustomTextEntity);
            prefetch.Add(CustomTextEntityBase.PrefetchPathUITabEntity).SubPath.Add(UITabEntityBase.PrefetchPathUITabLanguageCollection);

            CustomTextCollection customTexts = this.GetCustomTextsWithCustomFilterAndPrefetch(filter, prefetch);

            foreach (CustomTextEntity customText in customTexts)
            {
                foreach (UITabLanguageEntity uiTabLanguage in customText.UITabEntity.UITabLanguageCollection)
                {
                    if (customText.Type == CustomTextType.UITabCaption && uiTabLanguage.Caption.Equals(customText.Text, StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (!customTexts.Any(x => x.UITabId == customText.UITabId && x.Type == customText.Type && x.LanguageId == uiTabLanguage.LanguageId))
                        {
                            customText.LanguageId = uiTabLanguage.LanguageId;
                        }
                    }
                    else if (customText.Type == CustomTextType.UITabUrl && uiTabLanguage.URL.Equals(customText.Text, StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (!customTexts.Any(x => x.UITabId == customText.UITabId && x.Type == customText.Type && x.LanguageId == uiTabLanguage.LanguageId))
                        {
                            customText.LanguageId = uiTabLanguage.LanguageId;
                        }
                    }
                }
            }

            return customTexts;
        }

        private CustomTextCollection GetUpdatedCustomTextsWithoutCultureCodeForUIWidget()
        {
            PredicateExpression filter = new PredicateExpression(CustomTextFields.UIWidgetId != DBNull.Value);

            PrefetchPath prefetch = new PrefetchPath(EntityType.CustomTextEntity);
            prefetch.Add(CustomTextEntityBase.PrefetchPathUIWidgetEntity).SubPath.Add(UIWidgetEntityBase.PrefetchPathUIWidgetLanguageCollection);

            CustomTextCollection customTexts = this.GetCustomTextsWithCustomFilterAndPrefetch(filter, prefetch);

            foreach (CustomTextEntity customText in customTexts)
            {
                foreach (UIWidgetLanguageEntity uiWidgetLanguage in customText.UIWidgetEntity.UIWidgetLanguageCollection)
                {
                    if (customText.Type == CustomTextType.UIWidgetCaption && uiWidgetLanguage.Caption.Equals(customText.Text, StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (!customTexts.Any(x => x.UIWidgetId == customText.UIWidgetId && x.Type == customText.Type && x.LanguageId == uiWidgetLanguage.LanguageId))
                        {
                            customText.LanguageId = uiWidgetLanguage.LanguageId;
                        }
                    }
                    else if (customText.Type == CustomTextType.UIWidgetFieldValue1 && uiWidgetLanguage.FieldValue1.Equals(customText.Text, StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (!customTexts.Any(x => x.UIWidgetId == customText.UIWidgetId && x.Type == customText.Type && x.LanguageId == uiWidgetLanguage.LanguageId))
                        {
                            customText.LanguageId = uiWidgetLanguage.LanguageId;
                        }
                    }
                    else if (customText.Type == CustomTextType.UIWidgetFieldValue2 && uiWidgetLanguage.FieldValue2.Equals(customText.Text, StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (!customTexts.Any(x => x.UIWidgetId == customText.UIWidgetId && x.Type == customText.Type && x.LanguageId == uiWidgetLanguage.LanguageId))
                        {
                            customText.LanguageId = uiWidgetLanguage.LanguageId;
                        }
                    }
                    else if (customText.Type == CustomTextType.UIWidgetFieldValue3 && uiWidgetLanguage.FieldValue3.Equals(customText.Text, StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (!customTexts.Any(x => x.UIWidgetId == customText.UIWidgetId && x.Type == customText.Type && x.LanguageId == uiWidgetLanguage.LanguageId))
                        {
                            customText.LanguageId = uiWidgetLanguage.LanguageId;
                        }
                    }
                    else if (customText.Type == CustomTextType.UIWidgetFieldValue4 && uiWidgetLanguage.FieldValue4.Equals(customText.Text, StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (!customTexts.Any(x => x.UIWidgetId == customText.UIWidgetId && x.Type == customText.Type && x.LanguageId == uiWidgetLanguage.LanguageId))
                        {
                            customText.LanguageId = uiWidgetLanguage.LanguageId;
                        }
                    }
                    else if (customText.Type == CustomTextType.UIWidgetFieldValue5 && uiWidgetLanguage.FieldValue5.Equals(customText.Text, StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (!customTexts.Any(x => x.UIWidgetId == customText.UIWidgetId && x.Type == customText.Type && x.LanguageId == uiWidgetLanguage.LanguageId))
                        {
                            customText.LanguageId = uiWidgetLanguage.LanguageId;
                        }
                    }
                }                
            }

            return customTexts;
        }

        private CustomTextCollection GetUpdatedCustomTextsWithoutCultureCodeForVenueCategory()
        {
            PredicateExpression filter = new PredicateExpression(CustomTextFields.VenueCategoryId != DBNull.Value);

            PrefetchPath prefetch = new PrefetchPath(EntityType.CustomTextEntity);
            prefetch.Add(CustomTextEntityBase.PrefetchPathVenueCategoryEntity).SubPath.Add(VenueCategoryEntityBase.PrefetchPathVenueCategoryLanguageCollection);

            CustomTextCollection customTexts = this.GetCustomTextsWithCustomFilterAndPrefetch(filter, prefetch);

            foreach (CustomTextEntity customText in customTexts)
            {
                foreach (VenueCategoryLanguageEntity venueCategoryLanguage in customText.VenueCategoryEntity.VenueCategoryLanguageCollection)
                {
                    if (customText.Type == CustomTextType.VenueCategoryName && venueCategoryLanguage.Name.Equals(customText.Text, StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (!customTexts.Any(x => x.VenueCategoryId == customText.VenueCategoryId && x.Type == customText.Type && x.LanguageId == venueCategoryLanguage.LanguageId))
                        {
                            customText.LanguageId = venueCategoryLanguage.LanguageId;
                        }
                    }
                    else if (customText.Type == CustomTextType.VenueCategoryNamePlural && venueCategoryLanguage.NamePlural.Equals(customText.Text, StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (!customTexts.Any(x => x.VenueCategoryId == customText.VenueCategoryId && x.Type == customText.Type && x.LanguageId == venueCategoryLanguage.LanguageId))
                        {
                            customText.LanguageId = venueCategoryLanguage.LanguageId;
                        }
                    }
                }                
            }

            return customTexts;
        }

        private CustomTextCollection GetCustomTextsWithCustomFilterAndPrefetch(PredicateExpression customFilter, PrefetchPath prefetch)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(customFilter);
            //filter.Add(CustomTextFields.UIWidgetId != DBNull.Value);
            filter.AddWithAnd(CustomTextFields.CultureCode == DBNull.Value);

            File.AppendAllText(Server.MapPath("~/App_Data/Maintenance.txt"), string.Format("{0} - Start executing query\n", DateTimeOffset.Now));

            CustomTextCollection customTexts = new CustomTextCollection();
            customTexts.GetMulti(filter);

            File.AppendAllText(Server.MapPath("~/App_Data/Maintenance.txt"), string.Format("{0} - Done executing query\n", DateTimeOffset.Now));

            return customTexts;

        }

        private Dictionary<int, CustomTextType> GetMappedDictionaryForEntity(IEntity entity)
        {
            if (entity is CompanyLanguageEntity)
            {
                return this.mappedCompanyCustomTextTypes;            
            }
            else if (entity is DeliverypointgroupEntity)
            {
                return this.mappedDeliverypointgroupCustomTextTypes;
            }
            else if (entity is DeliverypointgroupLanguageEntity)
            {
                return this.mappedDeliverypointgroupLanguageCustomTextTypes;
            }
            else if (entity is CategoryLanguageEntity)
            {
                return this.mappedCategoryCustomTextTypes;
            }
            else if (entity is ProductLanguageEntity)
            {
                return this.mappedProductCustomTextTypes;
            }
            else if (entity is ActionButtonLanguageEntity)
            {
                return this.mappedActionButtonCustomTextTypes;
            }
            else if (entity is AdvertisementLanguageEntity)
            {
                return this.mappedAdvertisementCustomTextTypes;
            }
            else if (entity is AlterationLanguageEntity)
            {
                return this.mappedAlterationCustomTextTypes;
            }
            else if (entity is AlterationoptionLanguageEntity)
            {
                return this.mappedAlterationoptionCustomTextTypes;
            }
            else if (entity is AmenityLanguageEntity)
            {
                return this.mappedAmenityCustomTextTypes;
            }
            else if (entity is AnnouncementLanguageEntity)
            {
                return this.mappedAnnouncementCustomTextTypes;
            }
            else if (entity is AttachmentLanguageEntity)
            {
                return this.mappedAttachmentCustomTextTypes;
            }
            else if (entity is EntertainmentcategoryLanguageEntity)
            {
                return this.mappedEntertainmentcategoryCustomTextTypes;
            }
            else if (entity is GenericcategoryLanguageEntity)
            {
                return this.mappedGenericcategoryCustomTextTypes;
            }
            else if (entity is GenericproductLanguageEntity)
            {
                return this.mappedGenericproductCustomTextTypes;
            }
            else if (entity is PageLanguageEntity)
            {
                return this.mappedPageCustomTextTypes;
            }
            else if (entity is PageTemplateLanguageEntity)
            {
                return this.mappedPageTemplateCustomTextTypes;
            }
            else if (entity is PointOfInterestLanguageEntity)
            {
                return this.mappedPointOfInterestCustomTextTypes;
            }
            else if (entity is RoomControlAreaLanguageEntity)
            {
                return this.mappedRoomControlAreaCustomTextTypes;
            }
            else if (entity is RoomControlComponentLanguageEntity)
            {
                return this.mappedRoomControlComponentCustomTextTypes;
            }
            else if (entity is RoomControlSectionLanguageEntity)
            {
                return this.mappedRoomControlSectionCustomTextTypes;
            }
            else if (entity is RoomControlSectionItemLanguageEntity)
            {
                return this.mappedRoomControlSectionItemCustomTextTypes;
            }
            else if (entity is RoomControlWidgetLanguageEntity)
            {
                return this.mappedRoomControlWidgetCustomTextTypes;
            }
            else if (entity is SiteLanguageEntity)
            {
                return this.mappedSiteCustomTextTypes;
            }
            else if (entity is SiteTemplateLanguageEntity)
            {
                return this.mappedSiteTemplateCustomTextTypes;
            }
            else if (entity is StationLanguageEntity)
            {
                return this.mappedStationCustomTextTypes;
            }
            else if (entity is UIFooterItemLanguageEntity)
            {
                return this.mappedUIFooterItemCustomTextTypes;
            }
            else if (entity is UITabEntity)
            {
                return this.mappedUITabCustomTextTypes;
            }
            else if (entity is UITabLanguageEntity)
            {
                return this.mappedUITabLanguageCustomTextTypes;
            }
            else if (entity is UIWidgetLanguageEntity)
            {
                return this.mappedUIWidgetCustomTextTypes;
            }
            else if (entity is VenueCategoryLanguageEntity)
            {
                return this.mappedVenueCategoryCustomTextTypes;
            }
            return null;
        }

        private readonly Dictionary<int, CustomTextType> mappedCompanyCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)CompanyLanguageFieldIndex.Description, CustomTextType.CompanyDescription },
            { (int)CompanyLanguageFieldIndex.DescriptionSingleLine, CustomTextType.CompanyDescriptionShort },
            { (int)CompanyLanguageFieldIndex.VenuePageDescription, CustomTextType.CompanyVenuePageDescription }
        };

        private readonly Dictionary<int, CustomTextType> mappedDeliverypointgroupCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)DeliverypointgroupFieldIndex.OrderProcessingNotificationTitle, CustomTextType.DeliverypointgroupOrderProcessingNotificationTitle },
            { (int)DeliverypointgroupFieldIndex.OrderProcessingNotification, CustomTextType.DeliverypointgroupOrderProcessingNotificationText },
            { (int)DeliverypointgroupFieldIndex.ServiceProcessingNotificationTitle, CustomTextType.DeliverypointgroupServiceProcessingNotificationTitle },
            { (int)DeliverypointgroupFieldIndex.ServiceProcessingNotification, CustomTextType.DeliverypointgroupServiceProcessingNotificationText },
            { (int)DeliverypointgroupFieldIndex.OrderProcessedNotificationTitle, CustomTextType.DeliverypointgroupOrderProcessedNotificationTitle },
            { (int)DeliverypointgroupFieldIndex.OrderProcessedNotification, CustomTextType.DeliverypointgroupOrderProcessedNotificationText },
            { (int)DeliverypointgroupFieldIndex.ServiceProcessedNotificationTitle, CustomTextType.DeliverypointgroupServiceProcessedNotificationTitle },
            { (int)DeliverypointgroupFieldIndex.ServiceProcessedNotification, CustomTextType.DeliverypointgroupServiceProcessedNotificationText },
            { (int)DeliverypointgroupFieldIndex.FreeformMessageTitle, CustomTextType.DeliverypointgroupFreeformMessageTitle },
            { (int)DeliverypointgroupFieldIndex.OutOfChargeTitle, CustomTextType.DeliverypointgroupOutOfChargeTitle },
            { (int)DeliverypointgroupFieldIndex.OutOfChargeMessage, CustomTextType.DeliverypointgroupOutOfChargeText },
            { (int)DeliverypointgroupFieldIndex.OrderProcessedTitle, CustomTextType.DeliverypointgroupOrderProcessedTitle },
            { (int)DeliverypointgroupFieldIndex.OrderProcessedMessage, CustomTextType.DeliverypointgroupOrderProcessedText },
            { (int)DeliverypointgroupFieldIndex.OrderFailedTitle, CustomTextType.DeliverypointgroupOrderFailedTitle },
            { (int)DeliverypointgroupFieldIndex.OrderFailedMessage, CustomTextType.DeliverypointgroupOrderFailedText },
            { (int)DeliverypointgroupFieldIndex.OrderSavingTitle, CustomTextType.DeliverypointgroupOrderSavingTitle },
            { (int)DeliverypointgroupFieldIndex.OrderSavingMessage, CustomTextType.DeliverypointgroupOrderSavingText },
            { (int)DeliverypointgroupFieldIndex.OrderCompletedTitle, CustomTextType.DeliverypointgroupOrderCompletedTitle },
            { (int)DeliverypointgroupFieldIndex.OrderCompletedMessage, CustomTextType.DeliverypointgroupOrderCompletedText },
            { (int)DeliverypointgroupFieldIndex.ServiceSavingTitle, CustomTextType.DeliverypointgroupServiceSavingTitle },
            { (int)DeliverypointgroupFieldIndex.ServiceSavingMessage, CustomTextType.DeliverypointgroupServiceSavingText },
            { (int)DeliverypointgroupFieldIndex.ServiceProcessedTitle, CustomTextType.DeliverypointgroupServiceProcessedTitle },
            { (int)DeliverypointgroupFieldIndex.ServiceProcessedMessage, CustomTextType.DeliverypointgroupServiceProcessedText },
            { (int)DeliverypointgroupFieldIndex.ServiceFailedTitle, CustomTextType.DeliverypointgroupServiceFailedTitle },
            { (int)DeliverypointgroupFieldIndex.ServiceFailedMessage, CustomTextType.DeliverypointgroupServiceFailedText },
            { (int)DeliverypointgroupFieldIndex.RatingSavingTitle, CustomTextType.DeliverypointgroupRatingSavingTitle },
            { (int)DeliverypointgroupFieldIndex.RatingSavingMessage, CustomTextType.DeliverypointgroupRatingSavingText },
            { (int)DeliverypointgroupFieldIndex.RatingProcessedTitle, CustomTextType.DeliverypointgroupRatingProcessedTitle },
            { (int)DeliverypointgroupFieldIndex.RatingProcessedMessage, CustomTextType.DeliverypointgroupRatingProcessedText },
            { (int)DeliverypointgroupFieldIndex.OrderingNotAvailableMessage, CustomTextType.DeliverypointgroupOrderingNotAvailableText },
            { (int)DeliverypointgroupFieldIndex.PmsDeviceLockedTitle, CustomTextType.DeliverypointgroupPmsDeviceLockedTitle },
            { (int)DeliverypointgroupFieldIndex.PmsDeviceLockedText, CustomTextType.DeliverypointgroupPmsDeviceLockedText },
            { (int)DeliverypointgroupFieldIndex.PmsCheckinFailedTitle, CustomTextType.DeliverypointgroupPmsCheckinFailedTitle },
            { (int)DeliverypointgroupFieldIndex.PmsCheckinFailedText, CustomTextType.DeliverypointgroupPmsCheckinFailedText },
            { (int)DeliverypointgroupFieldIndex.PmsRestartTitle, CustomTextType.DeliverypointgroupPmsRestartTitle },
            { (int)DeliverypointgroupFieldIndex.PmsRestartText, CustomTextType.DeliverypointgroupPmsRestartText },
            { (int)DeliverypointgroupFieldIndex.PmsWelcomeTitle, CustomTextType.DeliverypointgroupPmsWelcomeTitle },
            { (int)DeliverypointgroupFieldIndex.PmsWelcomeText, CustomTextType.DeliverypointgroupPmsWelcomeText },
            { (int)DeliverypointgroupFieldIndex.PmsCheckoutCompleteTitle, CustomTextType.DeliverypointgroupPmsCheckoutCompleteTitle },
            { (int)DeliverypointgroupFieldIndex.PmsCheckoutCompleteText, CustomTextType.DeliverypointgroupPmsCheckoutCompleteText },
            { (int)DeliverypointgroupFieldIndex.PmsCheckoutApproveText, CustomTextType.DeliverypointgroupPmsCheckoutApproveText },
            { (int)DeliverypointgroupFieldIndex.ChargerRemovedDialogTitle, CustomTextType.DeliverypointgroupChargerRemovedTitle },
            { (int)DeliverypointgroupFieldIndex.ChargerRemovedDialogText, CustomTextType.DeliverypointgroupChargerRemovedText },
            { (int)DeliverypointgroupFieldIndex.ChargerRemovedReminderDialogTitle, CustomTextType.DeliverypointgroupChargerRemovedReminderTitle },
            { (int)DeliverypointgroupFieldIndex.ChargerRemovedReminderDialogText, CustomTextType.DeliverypointgroupChargerRemovedReminderText },
            { (int)DeliverypointgroupFieldIndex.TurnOffPrivacyTitle, CustomTextType.DeliverypointgroupTurnOffPrivacyTitle },
            { (int)DeliverypointgroupFieldIndex.TurnOffPrivacyText, CustomTextType.DeliverypointgroupTurnOffPrivacyText },
            { (int)DeliverypointgroupFieldIndex.ItemCurrentlyUnavailableText, CustomTextType.DeliverypointgroupItemCurrentlyUnavailableText },
            { (int)DeliverypointgroupFieldIndex.AlarmSetWhileNotChargingTitle, CustomTextType.DeliverypointgroupAlarmSetWhileNotChargingTitle },
            { (int)DeliverypointgroupFieldIndex.AlarmSetWhileNotChargingText, CustomTextType.DeliverypointgroupAlarmSetWhileNotChargingText },
            { (int)DeliverypointgroupFieldIndex.HotelUrl1, CustomTextType.DeliverypointgroupHotelUrl1 },
            { (int)DeliverypointgroupFieldIndex.HotelUrl1Caption, CustomTextType.DeliverypointgroupHotelUrl1Caption },
            { (int)DeliverypointgroupFieldIndex.HotelUrl2, CustomTextType.DeliverypointgroupHotelUrl2 },
            { (int)DeliverypointgroupFieldIndex.HotelUrl2Caption, CustomTextType.DeliverypointgroupHotelUrl2Caption },
            { (int)DeliverypointgroupFieldIndex.HotelUrl3, CustomTextType.DeliverypointgroupHotelUrl3 },
            { (int)DeliverypointgroupFieldIndex.HotelUrl3Caption, CustomTextType.DeliverypointgroupHotelUrl3Caption },
            { (int)DeliverypointgroupFieldIndex.DeliverypointCaption, CustomTextType.DeliverypointgroupDeliverypointCaption },            
            { (int)DeliverypointgroupFieldIndex.RoomserviceCharge, CustomTextType.DeliverypointgroupRoomserviceChargeText }
        };

        private readonly Dictionary<int, CustomTextType> mappedDeliverypointgroupLanguageCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)DeliverypointgroupLanguageFieldIndex.OutOfChargeTitle, CustomTextType.DeliverypointgroupOutOfChargeTitle },
            { (int)DeliverypointgroupLanguageFieldIndex.OutOfChargeMessage, CustomTextType.DeliverypointgroupOutOfChargeText },
            { (int)DeliverypointgroupLanguageFieldIndex.OrderProcessedTitle, CustomTextType.DeliverypointgroupOrderProcessedTitle },
            { (int)DeliverypointgroupLanguageFieldIndex.OrderProcessedMessage, CustomTextType.DeliverypointgroupOrderProcessedText },
            { (int)DeliverypointgroupLanguageFieldIndex.OrderFailedTitle, CustomTextType.DeliverypointgroupOrderFailedTitle },
            { (int)DeliverypointgroupLanguageFieldIndex.OrderFailedMessage, CustomTextType.DeliverypointgroupOrderFailedText },
            { (int)DeliverypointgroupLanguageFieldIndex.OrderSavingTitle, CustomTextType.DeliverypointgroupOrderSavingTitle },
            { (int)DeliverypointgroupLanguageFieldIndex.OrderSavingMessage, CustomTextType.DeliverypointgroupOrderSavingText },
            { (int)DeliverypointgroupLanguageFieldIndex.OrderCompletedTitle, CustomTextType.DeliverypointgroupOrderCompletedTitle },
            { (int)DeliverypointgroupLanguageFieldIndex.OrderCompletedMessage, CustomTextType.DeliverypointgroupOrderCompletedText },
            { (int)DeliverypointgroupLanguageFieldIndex.ServiceSavingTitle, CustomTextType.DeliverypointgroupServiceSavingTitle },
            { (int)DeliverypointgroupLanguageFieldIndex.ServiceSavingMessage, CustomTextType.DeliverypointgroupServiceSavingText },
            { (int)DeliverypointgroupLanguageFieldIndex.ServiceProcessedTitle, CustomTextType.DeliverypointgroupServiceProcessedTitle },
            { (int)DeliverypointgroupLanguageFieldIndex.ServiceProcessedMessage, CustomTextType.DeliverypointgroupServiceProcessedText },
            { (int)DeliverypointgroupLanguageFieldIndex.ServiceFailedTitle, CustomTextType.DeliverypointgroupServiceFailedTitle },
            { (int)DeliverypointgroupLanguageFieldIndex.ServiceFailedMessage, CustomTextType.DeliverypointgroupServiceFailedText },
            { (int)DeliverypointgroupLanguageFieldIndex.RatingSavingTitle, CustomTextType.DeliverypointgroupRatingSavingTitle },
            { (int)DeliverypointgroupLanguageFieldIndex.RatingSavingMessage, CustomTextType.DeliverypointgroupRatingSavingText },
            { (int)DeliverypointgroupLanguageFieldIndex.RatingProcessedTitle, CustomTextType.DeliverypointgroupRatingProcessedTitle },
            { (int)DeliverypointgroupLanguageFieldIndex.RatingProcessedMessage, CustomTextType.DeliverypointgroupRatingProcessedText },            
            { (int)DeliverypointgroupLanguageFieldIndex.PmsDeviceLockedTitle, CustomTextType.DeliverypointgroupPmsDeviceLockedTitle },
            { (int)DeliverypointgroupLanguageFieldIndex.PmsDeviceLockedText, CustomTextType.DeliverypointgroupPmsDeviceLockedText },
            { (int)DeliverypointgroupLanguageFieldIndex.PmsCheckinFailedTitle, CustomTextType.DeliverypointgroupPmsCheckinFailedTitle },
            { (int)DeliverypointgroupLanguageFieldIndex.PmsCheckinFailedText, CustomTextType.DeliverypointgroupPmsCheckinFailedText },
            { (int)DeliverypointgroupLanguageFieldIndex.PmsRestartTitle, CustomTextType.DeliverypointgroupPmsRestartTitle },
            { (int)DeliverypointgroupLanguageFieldIndex.PmsRestartText, CustomTextType.DeliverypointgroupPmsRestartText },
            { (int)DeliverypointgroupLanguageFieldIndex.PmsWelcomeTitle, CustomTextType.DeliverypointgroupPmsWelcomeTitle },
            { (int)DeliverypointgroupLanguageFieldIndex.PmsWelcomeText, CustomTextType.DeliverypointgroupPmsWelcomeText },
            { (int)DeliverypointgroupLanguageFieldIndex.PmsCheckoutCompleteTitle, CustomTextType.DeliverypointgroupPmsCheckoutCompleteTitle },
            { (int)DeliverypointgroupLanguageFieldIndex.PmsCheckoutCompleteText, CustomTextType.DeliverypointgroupPmsCheckoutCompleteText },
            { (int)DeliverypointgroupLanguageFieldIndex.PmsCheckoutApproveText, CustomTextType.DeliverypointgroupPmsCheckoutApproveText },
            { (int)DeliverypointgroupLanguageFieldIndex.ChargerRemovedDialogTitle, CustomTextType.DeliverypointgroupChargerRemovedTitle },
            { (int)DeliverypointgroupLanguageFieldIndex.ChargerRemovedDialogText, CustomTextType.DeliverypointgroupChargerRemovedText },
            { (int)DeliverypointgroupLanguageFieldIndex.ChargerRemovedReminderDialogTitle, CustomTextType.DeliverypointgroupChargerRemovedReminderTitle },
            { (int)DeliverypointgroupLanguageFieldIndex.ChargerRemovedReminderDialogText, CustomTextType.DeliverypointgroupChargerRemovedReminderText },
            { (int)DeliverypointgroupLanguageFieldIndex.TurnOffPrivacyTitle, CustomTextType.DeliverypointgroupTurnOffPrivacyTitle },
            { (int)DeliverypointgroupLanguageFieldIndex.TurnOffPrivacyText, CustomTextType.DeliverypointgroupTurnOffPrivacyText },
            { (int)DeliverypointgroupLanguageFieldIndex.ItemCurrentlyUnavailableText, CustomTextType.DeliverypointgroupItemCurrentlyUnavailableText },
            { (int)DeliverypointgroupLanguageFieldIndex.AlarmSetWhileNotChargingTitle, CustomTextType.DeliverypointgroupAlarmSetWhileNotChargingTitle },
            { (int)DeliverypointgroupLanguageFieldIndex.AlarmSetWhileNotChargingText, CustomTextType.DeliverypointgroupAlarmSetWhileNotChargingText },
            { (int)DeliverypointgroupLanguageFieldIndex.HotelUrl1, CustomTextType.DeliverypointgroupHotelUrl1 },
            { (int)DeliverypointgroupLanguageFieldIndex.HotelUrl1Caption, CustomTextType.DeliverypointgroupHotelUrl1Caption },
            { (int)DeliverypointgroupLanguageFieldIndex.HotelUrl2, CustomTextType.DeliverypointgroupHotelUrl2 },
            { (int)DeliverypointgroupLanguageFieldIndex.HotelUrl2Caption, CustomTextType.DeliverypointgroupHotelUrl2Caption },
            { (int)DeliverypointgroupLanguageFieldIndex.HotelUrl3, CustomTextType.DeliverypointgroupHotelUrl3 },
            { (int)DeliverypointgroupLanguageFieldIndex.HotelUrl3Caption, CustomTextType.DeliverypointgroupHotelUrl3Caption },
            { (int)DeliverypointgroupLanguageFieldIndex.DeliverypointCaption, CustomTextType.DeliverypointgroupDeliverypointCaption },
            { (int)DeliverypointgroupLanguageFieldIndex.PrintingConfirmationTitle, CustomTextType.DeliverypointgroupPrintingConfirmationTitle },
            { (int)DeliverypointgroupLanguageFieldIndex.PrintingConfirmationText, CustomTextType.DeliverypointgroupPrintingConfirmationText },
            { (int)DeliverypointgroupLanguageFieldIndex.PrintingSucceededTitle, CustomTextType.DeliverypointgroupPrintingSucceededTitle },
            { (int)DeliverypointgroupLanguageFieldIndex.PrintingSucceededText, CustomTextType.DeliverypointgroupPrintingSucceededText },
            { (int)DeliverypointgroupLanguageFieldIndex.SuggestionsCaption, CustomTextType.DeliverypointgroupSuggestionsCaption },
            { (int)DeliverypointgroupLanguageFieldIndex.PagesCaption, CustomTextType.DeliverypointgroupPagesCaption },
            { (int)DeliverypointgroupLanguageFieldIndex.RoomserviceChargeText, CustomTextType.DeliverypointgroupRoomserviceChargeText }
        };

        private readonly Dictionary<int, CustomTextType> mappedCategoryCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)CategoryLanguageFieldIndex.Name, CustomTextType.CategoryName },
            { (int)CategoryLanguageFieldIndex.Description, CustomTextType.CategoryDescription },
            { (int)CategoryLanguageFieldIndex.OrderProcessedTitle, CustomTextType.CategoryOrderProcessedTitle },
            { (int)CategoryLanguageFieldIndex.OrderProcessedMessage, CustomTextType.CategoryOrderProcessedText }            
        };

        private readonly Dictionary<int, CustomTextType> mappedProductCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)ProductLanguageFieldIndex.Name, CustomTextType.ProductName },
            { (int)ProductLanguageFieldIndex.Description, CustomTextType.ProductDescription },
            { (int)ProductLanguageFieldIndex.ButtonText, CustomTextType.ProductButtonText },
            { (int)ProductLanguageFieldIndex.OrderProcessedTitle, CustomTextType.ProductOrderProcessedTitle },
            { (int)ProductLanguageFieldIndex.OrderProcessedMessage, CustomTextType.ProductOrderProcessedText },
        };

        private readonly Dictionary<int, CustomTextType> mappedActionButtonCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)ActionButtonLanguageFieldIndex.Name, CustomTextType.ActionButtonName }
        };

        private readonly Dictionary<int, CustomTextType> mappedAdvertisementCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)AdvertisementLanguageFieldIndex.Name, CustomTextType.AdvertisementName },
            { (int)AdvertisementLanguageFieldIndex.Description, CustomTextType.AdvertisementDescription },
        };

        private readonly Dictionary<int, CustomTextType> mappedAlterationCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)AlterationLanguageFieldIndex.Name, CustomTextType.AlterationName },
            { (int)AlterationLanguageFieldIndex.Description, CustomTextType.AlterationDescription },
        };

        private readonly Dictionary<int, CustomTextType> mappedAlterationoptionCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)AlterationoptionLanguageFieldIndex.Name, CustomTextType.AlterationoptionName },
            { (int)AlterationoptionLanguageFieldIndex.Description, CustomTextType.AlterationoptionDescription },
        };

        private readonly Dictionary<int, CustomTextType> mappedAmenityCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)AmenityLanguageFieldIndex.Name, CustomTextType.AmenityName }
        };

        private readonly Dictionary<int, CustomTextType> mappedAnnouncementCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)AnnouncementLanguageFieldIndex.Title, CustomTextType.AnnouncementTitle },
            { (int)AnnouncementLanguageFieldIndex.Text, CustomTextType.AnnouncementText },
        };

        private readonly Dictionary<int, CustomTextType> mappedAttachmentCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)AttachmentLanguageFieldIndex.Name, CustomTextType.AttachmentName }
        };

        private readonly Dictionary<int, CustomTextType> mappedEntertainmentcategoryCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)EntertainmentcategoryLanguageFieldIndex.Name, CustomTextType.EntertainmentcategoryName }
        };

        private readonly Dictionary<int, CustomTextType> mappedGenericcategoryCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)GenericcategoryLanguageFieldIndex.Name, CustomTextType.GenericcategoryName }
        };

        private readonly Dictionary<int, CustomTextType> mappedGenericproductCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)GenericproductLanguageFieldIndex.Name, CustomTextType.GenericproductName },
            { (int)GenericproductLanguageFieldIndex.Description, CustomTextType.GenericproductDescription },
        };

        private readonly Dictionary<int, CustomTextType> mappedPageCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)PageLanguageFieldIndex.Name, CustomTextType.PageName }
        };

        private readonly Dictionary<int, CustomTextType> mappedPageTemplateCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)PageTemplateLanguageFieldIndex.Name, CustomTextType.PageTemplateName }
        };

        private readonly Dictionary<int, CustomTextType> mappedPointOfInterestCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)PointOfInterestLanguageFieldIndex.Description, CustomTextType.PointOfInterestDescription },
            { (int)PointOfInterestLanguageFieldIndex.DescriptionSingleLine, CustomTextType.PointOfInterestDescriptionSingleLine },
            { (int)PointOfInterestLanguageFieldIndex.VenuePageDescription, CustomTextType.PointOfInterestVenuePageDescription },
        };

        private readonly Dictionary<int, CustomTextType> mappedRoomControlAreaCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)RoomControlAreaLanguageFieldIndex.Name, CustomTextType.RoomControlAreaName },
        };

        private readonly Dictionary<int, CustomTextType> mappedRoomControlComponentCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)RoomControlComponentLanguageFieldIndex.Name, CustomTextType.RoomControlComponentName },
        };

        private readonly Dictionary<int, CustomTextType> mappedRoomControlSectionCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)RoomControlSectionLanguageFieldIndex.Name, CustomTextType.RoomControlSectionName },
        };

        private readonly Dictionary<int, CustomTextType> mappedRoomControlSectionItemCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)RoomControlSectionItemLanguageFieldIndex.Name, CustomTextType.RoomControlSectionItemName },
        };

        private readonly Dictionary<int, CustomTextType> mappedRoomControlWidgetCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)RoomControlWidgetLanguageFieldIndex.Caption, CustomTextType.RoomControlWidgetCaption },
        };

        private readonly Dictionary<int, CustomTextType> mappedSiteCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)SiteLanguageFieldIndex.Description, CustomTextType.SiteDescription },
        };

        private readonly Dictionary<int, CustomTextType> mappedSiteTemplateCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)SiteTemplateLanguageFieldIndex.Name, CustomTextType.SiteTemplateName },
        };

        private readonly Dictionary<int, CustomTextType> mappedStationCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)StationLanguageFieldIndex.Name, CustomTextType.StationName },
            { (int)StationLanguageFieldIndex.Description, CustomTextType.StationDescription },
            { (int)StationLanguageFieldIndex.SuccessMessage, CustomTextType.StationSuccessMessage },
        };

        private readonly Dictionary<int, CustomTextType> mappedUIFooterItemCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)UIFooterItemLanguageFieldIndex.Name, CustomTextType.UIFooterItemName },
        };

        private readonly Dictionary<int, CustomTextType> mappedUITabCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
             { (int)UITabFieldIndex.Caption, CustomTextType.UITabCaption },
             { (int)UITabFieldIndex.URL, CustomTextType.UITabUrl },
        };

        private readonly Dictionary<int, CustomTextType> mappedUITabLanguageCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
             { (int)UITabLanguageFieldIndex.Caption, CustomTextType.UITabCaption },
             { (int)UITabLanguageFieldIndex.URL, CustomTextType.UITabUrl },
        };

        private readonly Dictionary<int, CustomTextType> mappedUIWidgetCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)UIWidgetLanguageFieldIndex.Caption, CustomTextType.UIWidgetCaption },
            { (int)UIWidgetLanguageFieldIndex.FieldValue1, CustomTextType.UIWidgetFieldValue1 },
            { (int)UIWidgetLanguageFieldIndex.FieldValue2, CustomTextType.UIWidgetFieldValue2 },
            { (int)UIWidgetLanguageFieldIndex.FieldValue3, CustomTextType.UIWidgetFieldValue3 },
            { (int)UIWidgetLanguageFieldIndex.FieldValue4, CustomTextType.UIWidgetFieldValue4 },
            { (int)UIWidgetLanguageFieldIndex.FieldValue5, CustomTextType.UIWidgetFieldValue5 },
        };

        private readonly Dictionary<int, CustomTextType> mappedVenueCategoryCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)VenueCategoryLanguageFieldIndex.Name, CustomTextType.VenueCategoryName },
            { (int)VenueCategoryLanguageFieldIndex.NamePlural, CustomTextType.VenueCategoryNamePlural },
        };

        #endregion

        #endregion

        /// <summary>
        ///     Sets the Gui
        /// </summary>
        private void SetGui()
        {
            // Show when not in development database
            string catalog = Data.DbUtils.GetCurrentCatalogName().ToLower();

            if (TestUtil.IsPcDeveloper || CommonDaoBase.ActualConnectionString.Contains("localhost", StringComparison.InvariantCultureIgnoreCase))
            {
                lblConnectionString.Text = "Working on local machine!";
            }
            else if (catalog.Contains("dev"))
            {
                lblConnectionString.Text = "Working on Development environment!";
            }
            else if (catalog.Contains("test"))
            {
                lblConnectionString.Text = "Working on Test environment!";
            }
            else
            {
                lblConnectionString.Text = "Working in LIVE environment!";
                lblConnectionString.ForeColor = Color.Red;
            }

            if (CmsSessionHelper.CurrentRole < Role.GodMode)
            {
                foreach (ListItem item in this.cblLocal.Items)
                {
                    item.Selected = item.Value.Equals("DeleteCustomViews");
                    item.Enabled = item.Value.Equals("DeleteCustomViews");
                }

                tabsMain.GetTabPageByName("DataManagement").Visible = false;
                tabsMain.GetTabPageByName("Cache").Visible = false;
            }

            if (!IsPostBack)
            {
                DropDownListInt ddlMediaRatioTypeFrom = this.ddlMediaRatioTypeFrom;
                BindEnumToDDL(ddlMediaRatioTypeFrom, typeof(MediaType), true);

                DropDownListInt ddlMediaRatioTypeTo = this.ddlMediaRatioTypeTo;
                BindEnumToDDL(ddlMediaRatioTypeTo, typeof(MediaType), true);
            }

            // Translate listitems
            foreach (ListItem item in this.cblLocal.Items)
            {
                item.Text = this.Translate(item.Value, item.Text);
            }

            if (Request.IsLocal)
            {
                foreach (ListItem item in this.cblMaster.Items)
                {
                    item.Text = this.Translate(item.Value, item.Text);
                }
            }

            this.ddlMediaRefreshCompanyId.DataSource = CmsSessionHelper.CompanyCollectionForUser;
            this.ddlMediaRefreshCompanyId.DataBind();

            // -------------------------
            // Aria

            // Deliverypointgroups
            DeliverypointgroupCollection dpgs = new DeliverypointgroupCollection();
            PredicateExpression dgpFilter = new PredicateExpression();
            dgpFilter.Add(DeliverypointgroupFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            SortExpression sort = new SortExpression(DeliverypointgroupFields.Name | SortOperator.Ascending);
            dpgs.GetMulti(dgpFilter, 0, sort);

            this.ddlChangeDpgFromDpgId.DataSource = dpgs;
            this.ddlChangeDpgFromDpgId.DataBind();
            this.ddlChangeDpgToDpgId.DataSource = dpgs;
            this.ddlChangeDpgToDpgId.DataBind();

            this.cbRoomControlCopyDpgFrom.DataSource = dpgs;
            this.cbRoomControlCopyDpgFrom.DataBind();
            this.cbRoomControlCopyDpgTo.DataSource = dpgs;
            this.cbRoomControlCopyDpgTo.DataBind();

            this.cbSwitcherCompany.DataSource = CmsSessionHelper.CompanyCollectionForUser.OrderBy(x => x.Name);
            this.cbSwitcherCompany.DataBind();
        }

        public void BindEnumToDDL(DropDownListInt ddl, Type enumeration, bool sort = false)
        {
            string[] names = Enum.GetNames(enumeration);
            Array values = Enum.GetValues(enumeration);

            if (sort)
            {
                Dictionary<string, int> keyValuePairs = new Dictionary<string, int>();
                for (int i = 0; i < names.Length; i++)
                {
                    if (keyValuePairs.ContainsKey(names[i]))
                    {
                        throw new FunctionalException("Enum '{0}' contains a double value '{1}'", enumeration.Name, names[i]);
                    }
                    keyValuePairs.Add(names[i], Convert.ToInt32(values.GetValue(i)));
                }

                IOrderedEnumerable<KeyValuePair<string, int>> sorted = keyValuePairs.OrderBy(x => x.Key);

                foreach (KeyValuePair<string, int> pair in sorted)
                {
                    ddl.Items.Add(new ListItem(pair.Key, pair.Value.ToString()));
                }
            }
            else
            {
                for (int i = 0; i < names.Length; i++)
                {
                    ListItem item = new ListItem(names[i], Convert.ToInt32(values.GetValue(i)).ToString());
                    ddl.Items.Add(item);
                }
            }
        }

        #region Meta data methods

        /// <summary>
        ///     Refreshes the local and/or master data.
        /// </summary>
        public void Refresh()
        {
            TimeSpan masterRefresh = new TimeSpan(0);

            // Master data
            if (Request.IsLocal)
            {
                masterRefresh = RefreshMaster();
            }

            // Local data
            TimeSpan localRefresh = RefreshLocal();

            // Clear cache
            ClearCache();

            if (masterRefresh.Ticks > 0)
            {
                this.AddInformator(InformatorType.Information, "Master refreshed in: {0:g}", masterRefresh);
            }

            this.AddInformator(InformatorType.Information, "Local refreshed in: {0:g}", localRefresh);
        }

        /// <summary>
        ///     Refreshes the local data.
        /// </summary>
        private TimeSpan RefreshLocal()
        {
            Stopwatch sw = new Stopwatch();

            List<string> selectedValuesLocal = cblLocal.Items.Cast<ListItem>().Where(li => li.Selected).Select(li => li.Value).ToList();
            if (selectedValuesLocal.Count > 0)
            {
                sw.Start();

                try
                {
                    if (selectedValuesLocal.Contains("RefreshUiElements"))
                    {
                        UIElements.RefreshUIElements(true);
                    }

                    if (selectedValuesLocal.Contains("DeleteCustomUiElements"))
                    {
                        UIElementCustomCollection customUiElements = new UIElementCustomCollection();
                        customUiElements.DeleteMulti(null);
                    }

                    if (selectedValuesLocal.Contains("RefreshUiElementSubPanels"))
                    {
                        UIElementSubPanels.RefreshUIElementSubPanels(true);
                        UIElementSubPanels.RefreshUIElementSubPanelUIElements(true);
                    }

                    if (selectedValuesLocal.Contains("DeleteCustomUiElementSubPanels"))
                    {
                        UIElementSubPanelCustomCollection customUiElementSubPanels = new UIElementSubPanelCustomCollection();
                        customUiElementSubPanels.DeleteMulti(null);
                    }

                    if (selectedValuesLocal.Contains("RefreshModules"))
                    {
                        Modules.RefreshModules(true);
                    }

                    if (selectedValuesLocal.Contains("RefreshEntityInformation"))
                    {
                        EntityInformation.RefreshEntityInformationEntities(true);
                        EntityInformation.RefreshEntityFieldInformationEntities(true);
                    }

                    if (selectedValuesLocal.Contains("DeleteCustomEntityInformation"))
                    {
                        EntityFieldInformationCustomCollection customEntityFieldInformation = new EntityFieldInformationCustomCollection();
                        customEntityFieldInformation.DeleteMulti(null);
                        EntityInformationCustomCollection customEntityInformation = new EntityInformationCustomCollection();
                        customEntityInformation.DeleteMulti(null);
                    }

                    if (selectedValuesLocal.Contains("CreateDefaultViews"))
                    {
                        Views.CreateDefaultViews();
                        Views.CreateDefaultViewItems(true);
                        CreateDefaultClientViewItems();
                    }

                    if (selectedValuesLocal.Contains("DeleteCustomViews"))
                    {
                        ViewItemCustomCollection customViewItems = new ViewItemCustomCollection();
                        customViewItems.DeleteMulti(null);
                        ViewCustomCollection customViews = new ViewCustomCollection();
                        customViews.DeleteMulti(null);
                    }

                    if (selectedValuesLocal.Contains("RefreshReferentialContraints"))
                    {
                        try
                        {
                            ReferentialConstraints.RefreshReferentialConstraints(true);
                        }
                        catch
                        {
                            ReferentialConstraintCollection referentialContraints = new ReferentialConstraintCollection();
                            referentialContraints.DeleteMulti(null);
                            ReferentialConstraints.RefreshReferentialConstraints(true);
                        }
                    }

                    if (selectedValuesLocal.Contains("DeleteCustomReferentialContraints"))
                    {
                        ReferentialConstraintCustomCollection customReferentialContraints = new ReferentialConstraintCustomCollection();
                        customReferentialContraints.DeleteMulti(null);
                    }

                    if (selectedValuesLocal.Contains("RefreshGlobalizationData"))
                    {
                        new GlobalizationDataHelper().UpdateGlobalizationDataLocallyFromMaster("en");
                    }

                    if (selectedValuesLocal.Contains("RefreshRoleUiElementRights"))
                    {
                        RoleUIElementRights.RefreshRoleUIElementRights(true);
                    }

                    if (selectedValuesLocal.Contains("RefreshTimeZones"))
                    {
                        TimeZones.RefreshTimeZones();
                    }

                    if (selectedValuesLocal.Contains("RefreshCountryAndCurrency"))
                    {
                        Countries.Refresh();
                    }
                }
                finally
                {
                    sw.Stop();
                }
            }

            return sw.Elapsed;
        }

        public static void CreateDefaultClientViewItems()
        {
            ViewEntity viewEntity = GetViewEntity("Client");
            if (viewEntity == null)
                return;

            CreateViewItems(viewEntity, ClientFieldNames, 200, 100);
        }

        private static ViewEntity GetViewEntity(string entityName)
        {
            IEntityCollection entityCollection = LLBLGenEntityCollectionUtil.GetEntityCollectionUsingFieldCompareValuePredicateExpression("View", "EntityName", ComparisonOperator.Equal, entityName);

            if (entityCollection.Count > 0 && entityCollection[0] is ViewEntity)
            {
                return (ViewEntity)entityCollection[0];
            }

            return null;
        }

        private static void CreateViewItems(ViewEntity viewEntity, string[] fieldNames, int startPosition, int increment)
        {
            foreach (string fieldName in fieldNames)
            {
                ViewItemEntity viewItemEntity = new ViewItemEntity();
                viewItemEntity.ViewId = viewEntity.ViewId;
                viewItemEntity.EntityName = viewEntity.EntityName;
                viewItemEntity.FieldName = fieldName;
                viewItemEntity.ShowOnGridView = true;
                viewItemEntity.DisplayPosition = startPosition;
                viewItemEntity.Type = "System.String";
                viewItemEntity.UiElementTypeNameFull = viewEntity.UiElementTypeNameFull;
                viewItemEntity.Save();

                startPosition += increment;
            }
        }

        private static readonly string[] ClientFieldNames = new[]
        {
            "DeviceEntity.PrivateIpAddresses",
            "DeviceEntity.ApplicationVersion",
            "DeviceEntity.AgentVersion",
            "DeviceEntity.SupportToolsVersion",
            "ApiVersionAsString"
        };

        /// <summary>
        ///     Refreshes the master data.
        /// </summary>
        private TimeSpan RefreshMaster()
        {
            Stopwatch sw = new Stopwatch();

            List<string> selectedValuesMaster = cblMaster.Items.Cast<ListItem>().Where(li => li.Selected).Select(li => li.Value).ToList();
            if (selectedValuesMaster.Count > 0)
            {
                sw.Start();

                //try
                //{
                    if (selectedValuesMaster.Contains("RefreshUiElements"))
                    {
                        UIElements.CreateUIElements(Server.MapPath("~"));
                    }

                    if (selectedValuesMaster.Contains("RefreshUiElementSubPanels"))
                    {
                        UIElementSubPanels.CreateUIElementSubPanels(Server.MapPath("~"));
                    }

                    if (selectedValuesMaster.Contains("RefreshEntityInformation"))
                    {
                        EntityInformation.CreateEntityInformationEntities();
                        EntityInformation.CreateEntityFieldInformationEntities(true);
                    }

                    if (selectedValuesMaster.Contains("RefreshReferentialContraints"))
                    {
                        ReferentialConstraints.CheckReferentialConstraints();
                    }

                    if (selectedValuesMaster.Contains("RefreshGlobalizationData"))
                    {
                        new GlobalizationDataHelper().UpdateGlobalizationDataInMaster("en");
                    }

                    if (selectedValuesMaster.Contains("RefreshRoleUiElementRights"))
                    {
                        RoleUIElementRights.CreateRoleUIElementRights();
                    }
                //}
                //catch
                //{
                //}
                //finally
                //{
                    sw.Stop();
                //}
            }

            return sw.Elapsed;
        }

        /// <summary>
        ///     Clears the cache.
        /// </summary>
        public void ClearCache()
        {
            this.gvCache.Visible = false;
            CacheHelper.Clear();

            this.AddInformator(InformatorType.Information, "Cache cleared");
        }                

        #endregion

        #region Maintenance methods

        private void ConvertContactPagesToWebLinkPages()
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(new FieldLikePredicate(PageFields.Name, null, "%contact%"));
            filter.Add(PageFields.PageType != PageType.WebLink);
            filter.Add(new FieldLikePredicate(PageElementFields.StringValue1, null, "%website: %"));

            PrefetchPath prefetch = new PrefetchPath(EntityType.PageElementEntity);
            prefetch.Add(PageElementEntityBase.PrefetchPathPageEntity).SubPath.Add(PageEntityBase.PrefetchPathPageElementCollection);

            RelationCollection relations = new RelationCollection();
            relations.Add(PageElementEntityBase.Relations.PageEntityUsingPageId);

            PageElementCollection pageElements = new PageElementCollection();
            pageElements.GetMulti(filter, 0, null, relations, prefetch);

            int counter = 0;
            Regex linkParser = new Regex(@"\b(?:https?://|www\.)\S+\b", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            foreach (PageElementEntity pageElement in pageElements)
            {
                PageEntity page = pageElement.PageEntity;

                // Check if there isn't already a webview pageElement available for this page
                PageElementEntity webviewElement = page.PageElementCollection.SingleOrDefault(pe => pe.PageElementTypeAsEnum == PageElementType.WebView);
                if (webviewElement == null)
                {
                    // Try to find a website which we can set as url for the new button
                    MatchCollection matches = linkParser.Matches(pageElement.StringValue1);
                    if (matches.Count > 0)
                    {
                        // Take the last url, is probably the good one
                        Match website = matches[matches.Count - 1];

                        // Add url page element
                        PageElementEntity webviewPageElement = new PageElementEntity();
                        webviewPageElement.SystemName = "webViewelement";
                        webviewPageElement.ParentCompanyId = pageElement.ParentCompanyId;
                        webviewPageElement.PageElementType = (int)PageElementType.WebView;
                        webviewPageElement.LanguageId = pageElement.LanguageId;
                        webviewPageElement.StringValue1 = website.Value;
                        page.PageElementCollection.Add(webviewPageElement);

                        // Add button caption
                        PageElementEntity buttonTextPageElement = new PageElementEntity();
                        buttonTextPageElement.SystemName = "buttonText";
                        buttonTextPageElement.ParentCompanyId = pageElement.ParentCompanyId;
                        buttonTextPageElement.PageElementType = (int)PageElementType.SingleLineText;
                        buttonTextPageElement.StringValue1 = "Website";
                        page.PageElementCollection.Add(buttonTextPageElement);

                        // Remove the 'website: ' part from the text
                        pageElement.StringValue1 = pageElement.StringValue1.Replace("website: ", "", StringComparison.InvariantCultureIgnoreCase);
                        // Remove the actual website from the text
                        pageElement.StringValue1 = pageElement.StringValue1.Replace(website.Value + "/", "", StringComparison.InvariantCultureIgnoreCase);
                        pageElement.StringValue1 = pageElement.StringValue1.Replace(website.Value, "", StringComparison.InvariantCultureIgnoreCase);
                        // Remove white sheit at the end
                        pageElement.StringValue1 = pageElement.StringValue1.TrimEnd();

                        // Change page type
                        page.PageType = (int)PageType.WebLink;

                        // Save the changes for the page and its pageElements
                        page.Save(true);

                        // Increase the counter for feedback to the user
                        counter++;
                    }
                }
            }

            this.AddInformatorInfo(string.Format("A grand total of {0} pages were successfully converted to webLink pages. Enjoy!!", counter));
            this.Validate();
        }

        private void LinkCategorySuggestionsToProductCategories()
        {
            CategorySuggestionCollection categorySuggestions = this.GetCategorySuggestionsWithoutProductCategory();

            foreach (CategorySuggestionEntity categorySuggestion in categorySuggestions)
            {
                if (categorySuggestion.ProductId.HasValue && categorySuggestion.CategoryId.HasValue && categorySuggestion.CategoryEntity.MenuId.HasValue)
                {
                    int menuId = categorySuggestion.CategoryEntity.MenuId.Value;

                    foreach (ProductCategoryEntity productCategoryEntity in categorySuggestion.ProductEntity.ProductCategoryCollection)
                    {
                        CategoryEntity categoryEntity = productCategoryEntity.CategoryEntity;

                        if (categoryEntity.MenuId.Value == menuId)
                        {
                            categorySuggestion.ProductCategoryId = productCategoryEntity.ProductCategoryId;
                            categorySuggestion.ProductId = null;
                            categorySuggestion.Save();
                        }
                    }
                }
            }
        }

        private void GenerateCategorySuggestionsOverview()
        {
            DataTable table = new DataTable();

            DataColumn companyIdColumn = new DataColumn("CompanyId");
            DataColumn categoryIdColumn = new DataColumn("CategoryId");
            DataColumn categoryNameColumn = new DataColumn("CategoryName");
            DataColumn productIdColumn = new DataColumn("ProductId");
            DataColumn productNameColumn = new DataColumn("ProductName");
            DataColumn numberOfCategoriesColumn = new DataColumn("NumberOfCategories");

            table.Columns.Add(companyIdColumn);
            table.Columns.Add(categoryIdColumn);
            table.Columns.Add(categoryNameColumn);
            table.Columns.Add(productIdColumn);
            table.Columns.Add(productNameColumn);
            table.Columns.Add(numberOfCategoriesColumn);

            CategorySuggestionCollection categorySuggestions = this.GetCategorySuggestionsWithoutProductCategory();
            foreach (CategorySuggestionEntity categorySuggestion in categorySuggestions)
            {
                if (categorySuggestion.ProductId.HasValue)
                {
                    DataRow row = table.NewRow();

                    row["CompanyId"] = categorySuggestion.CategoryEntity.CompanyId;
                    row["CategoryId"] = categorySuggestion.CategoryId;
                    row["CategoryName"] = categorySuggestion.CategoryEntity.Name;
                    row["ProductId"] = categorySuggestion.ProductId;
                    row["ProductName"] = categorySuggestion.ProductEntity.Name;
                    row["NumberOfCategories"] = categorySuggestion.ProductEntity.ProductCategoryCollection.Count;

                    table.Rows.Add(row);
                }
            }

            if (table.Rows.Count > 0)
            {
                this.gvCategorySuggestions.DataSource = table;
                this.gvCategorySuggestions.DataBind();

                this.gvCategorySuggestions.Visible = true;
                this.plhCategorySuggestionsOverview.Visible = true;
            }
        }

        private CategorySuggestionCollection GetCategorySuggestionsWithoutProductCategory()
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(CategorySuggestionFields.ProductCategoryId == DBNull.Value);

            SortExpression sort = new SortExpression();
            sort.Add(new SortClause(CategoryFields.CompanyId, SortOperator.Descending));
            sort.Add(new SortClause(CategoryFields.CategoryId, SortOperator.Descending));

            PrefetchPath prefetch = new PrefetchPath(EntityType.CategorySuggestionEntity);
            IPrefetchPathElement prefetchCategory = prefetch.Add(CategorySuggestionEntityBase.PrefetchPathCategoryEntity);
            IPrefetchPathElement prefetchSuggestedProduct = prefetch.Add(CategorySuggestionEntityBase.PrefetchPathProductEntity);
            IPrefetchPathElement prefetchSuggestedProductProductCategories = prefetchSuggestedProduct.SubPath.Add(ProductEntityBase.PrefetchPathProductCategoryCollection);
            IPrefetchPathElement prefetchSuggestedProductProductCategoriesCategory = prefetchSuggestedProductProductCategories.SubPath.Add(ProductCategoryEntityBase.PrefetchPathCategoryEntity);

            RelationCollection relations = new RelationCollection();
            relations.Add(CategorySuggestionEntityBase.Relations.CategoryEntityUsingCategoryId);

            CategorySuggestionCollection categorySuggestions = new CategorySuggestionCollection();
            categorySuggestions.GetMulti(filter, 0, sort, relations, prefetch);

            return categorySuggestions;
        }

        private void CopyAllMediaRatioTypeMedia()
        {
            int companyId = CmsSessionHelper.CurrentCompanyId;

            new Thread(param =>
                       {
                           try
                           {
                               GeneralAuthorizer.SetThreadGodMode(true);

                               //CopyMediaRatioTypeMedia(MediaType.ProductButtoniPhoneRetina, MediaType.ProductButtoniPhoneRetinaHD, companyId);
                               //CopyMediaRatioTypeMedia(MediaType.ProductButtoniPhoneRetina, MediaType.ProductButtoniPhoneRetinaHDPlus, companyId);

                               //CopyMediaRatioTypeMedia(MediaType.GenericProductButtoniPhoneRetina, MediaType.GenericProductButtoniPhoneRetinaHD, companyId);
                               //CopyMediaRatioTypeMedia(MediaType.GenericProductButtoniPhoneRetina, MediaType.GenericProductButtoniPhoneRetinaHDPlus, companyId);

                               //CopyMediaRatioTypeMedia(MediaType.ProductBrandingiPhoneRetina, MediaType.ProductBrandingiPhoneRetinaHD, companyId);
                               //CopyMediaRatioTypeMedia(MediaType.ProductBrandingiPhoneRetina, MediaType.ProductBrandingiPhoneRetinaHDPlus, companyId);

                               //CopyMediaRatioTypeMedia(MediaType.GenericProductBrandingiPhoneRetina, MediaType.GenericProductBrandingiPhoneRetinaHD, companyId);
                               //CopyMediaRatioTypeMedia(MediaType.GenericProductBrandingiPhoneRetina, MediaType.GenericProductBrandingiPhoneRetinaHDPlus, companyId);

                               //CopyMediaRatioTypeMedia(MediaType.CategoryButtoniPhoneRetina, MediaType.CategoryButtoniPhoneRetinaHD, companyId);
                               //CopyMediaRatioTypeMedia(MediaType.CategoryButtoniPhoneRetina, MediaType.CategoryButtoniPhoneRetinaHDPlus, companyId);

                               //CopyMediaRatioTypeMedia(MediaType.GenericCategoryButtoniPhoneRetina, MediaType.GenericCategoryButtoniPhoneRetinaHD, companyId);
                               //CopyMediaRatioTypeMedia(MediaType.GenericCategoryButtoniPhoneRetina, MediaType.GenericCategoryButtoniPhoneRetinaHDPlus, companyId);

                               //CopyMediaRatioTypeMedia(MediaType.CategoryBrandingiPhoneRetina, MediaType.CategoryBrandingiPhoneRetinaHD, companyId);
                               //CopyMediaRatioTypeMedia(MediaType.CategoryBrandingiPhoneRetina, MediaType.CategoryBrandingiPhoneRetinaHDPlus, companyId);

                               //CopyMediaRatioTypeMedia(MediaType.GenericCategoryBrandingiPhoneRetina, MediaType.GenericCategoryBrandingiPhoneRetinaHD, companyId);
                               //CopyMediaRatioTypeMedia(MediaType.GenericCategoryBrandingiPhoneRetina, MediaType.GenericCategoryBrandingiPhoneRetinaHDPlus, companyId);

                               //CopyMediaRatioTypeMedia(MediaType.CompanyListiPhoneRetina, MediaType.CompanyListiPhoneRetinaHD, companyId);
                               //CopyMediaRatioTypeMedia(MediaType.CompanyListiPhoneRetina, MediaType.CompanyListiPhoneRetinaHDPlus, companyId);

                               //CopyMediaRatioTypeMedia(MediaType.HomepageiPhoneRetina, MediaType.HomepageiPhoneRetinaHD, companyId);
                               //CopyMediaRatioTypeMedia(MediaType.HomepageiPhoneRetina, MediaType.HomepageiPhoneRetinaHDPlus, companyId);

                               //CopyMediaRatioTypeMedia(MediaType.PointOfInterestListiPhoneRetina, MediaType.PointOfInterestListiPhoneRetinaHD, companyId);
                               //CopyMediaRatioTypeMedia(MediaType.PointOfInterestListiPhoneRetina, MediaType.PointOfInterestListiPhoneRetinaHDPlus, companyId);

                               //CopyMediaRatioTypeMedia(MediaType.PointOfInterestHomepageiPhoneRetina, MediaType.PointOfInterestHomepageiPhoneRetinaHD, companyId);
                               //CopyMediaRatioTypeMedia(MediaType.PointOfInterestHomepageiPhoneRetina, MediaType.PointOfInterestHomepageiPhoneRetinaHDPlus, companyId);

                               //CopyMediaRatioTypeMedia(MediaType.MapIconiPhoneRetina, MediaType.MapIconiPhoneRetinaHD, companyId);
                               //CopyMediaRatioTypeMedia(MediaType.MapIconiPhoneRetina, MediaType.MapIconiPhoneRetinaHDPlus, companyId);

                               //CopyMediaRatioTypeMedia(MediaType.PointOfInterestMapIconiPhoneRetina, MediaType.PointOfInterestMapIconiPhoneRetinaHD, companyId);
                               //CopyMediaRatioTypeMedia(MediaType.PointOfInterestMapIconiPhoneRetina, MediaType.PointOfInterestMapIconiPhoneRetinaHDPlus, companyId);

                               //CopyMediaRatioTypeMedia(MediaType.SitePageIconiPhoneRetina, MediaType.SitePageIconiPhoneRetinaHD, companyId);
                               //CopyMediaRatioTypeMedia(MediaType.SitePageIconiPhoneRetina, MediaType.SitePageIconiPhoneRetinaHDPlus, companyId);

                               //CopyMediaRatioTypeMedia(MediaType.SiteMobileHeaderImageiPhoneRetina, MediaType.SiteMobileHeaderImageiPhoneRetinaHD, companyId);
                               //CopyMediaRatioTypeMedia(MediaType.SiteMobileHeaderImageiPhoneRetina, MediaType.SiteMobileHeaderImageiPhoneRetinaHDPlus, companyId);

                               //CopyMediaRatioTypeMedia(MediaType.SitePointOfInterestSummaryPageiPhoneRetina, MediaType.SitePointOfInterestSummaryPageiPhoneRetinaHD, companyId);
                               //CopyMediaRatioTypeMedia(MediaType.SitePointOfInterestSummaryPageiPhoneRetina, MediaType.SitePointOfInterestSummaryPageiPhoneRetinaHDPlus, companyId);

                               //CopyMediaRatioTypeMedia(MediaType.SiteCompanySummaryPageiPhoneRetina, MediaType.SiteCompanySummaryPageiPhoneRetinaHD, companyId);
                               //CopyMediaRatioTypeMedia(MediaType.SiteCompanySummaryPageiPhoneRetina, MediaType.SiteCompanySummaryPageiPhoneRetinaHDPlus, companyId);

                               //CopyMediaRatioTypeMedia(MediaType.SiteBrandingiPhoneRetina, MediaType.SiteBrandingiPhoneRetinaHD, companyId);
                               //CopyMediaRatioTypeMedia(MediaType.SiteBrandingiPhoneRetina, MediaType.SiteBrandingiPhoneRetinaHDPlus, companyId);
                           }
                           catch
                           { }
                           finally
                           {
                               GeneralAuthorizer.SetThreadGodMode(false);
                           }
                       }).Start(HttpContext.Current);
        }

        private void CopyMediaRatioTypeMedia(MediaType mediaTypeFrom, MediaType mediaTypeTo)
        {
            List<MediaType> allTypes = new List<MediaType> { mediaTypeFrom, mediaTypeTo };

            PredicateExpression fromFilter = new PredicateExpression();
            fromFilter.Add(MediaRatioTypeMediaFields.MediaType == mediaTypeFrom);

            PredicateExpression fromAndToFilter = new PredicateExpression();
            fromAndToFilter.Add(MediaRatioTypeMediaFields.MediaType == allTypes);

            PrefetchPath prefetch = new PrefetchPath(EntityType.MediaRatioTypeMediaEntity);
            prefetch.Add(MediaRatioTypeMediaEntityBase.PrefetchPathMediaEntity);

            MediaRatioTypeMediaCollection mediaRatioTypeMediaCollection = new MediaRatioTypeMediaCollection();
            mediaRatioTypeMediaCollection.GetMulti(fromFilter, prefetch);

            MediaRatioTypeMediaCollection mediaRatioTypeMediaCollection2 = new MediaRatioTypeMediaCollection();
            mediaRatioTypeMediaCollection2.GetMulti(fromAndToFilter);

            MediaRatioType mediaRatioType = MediaRatioTypes.GetMediaRatioType(mediaTypeTo);

            EntityView<MediaRatioTypeMediaEntity> mediaRatioTypeMediaView = mediaRatioTypeMediaCollection2.DefaultView;

            for (int i = 0; i < mediaRatioTypeMediaCollection.Count; i++)
            {
                SaveMediaRatioTypeMedia(mediaRatioTypeMediaCollection[i], mediaRatioType, null, mediaRatioTypeMediaView);                
            }            
        }

        private void SaveMediaRatioTypeMedia(MediaRatioTypeMediaEntity source, MediaRatioType toMediaRatioType, Transaction transaction, EntityView<MediaRatioTypeMediaEntity> view)
        {
            try
            {
                PredicateExpression filter = new PredicateExpression();
                filter.Add(MediaRatioTypeMediaFields.MediaId == source.MediaEntity.MediaId);
                filter.Add(MediaRatioTypeMediaFields.MediaType == toMediaRatioType.MediaType);

                view.Filter = filter;

                if (view.Count == 0)
                {
                    MediaEntity mediaEntity = source.MediaEntity;

                    MediaRatioTypeMediaEntity mrtm = new MediaRatioTypeMediaEntity();
                    mrtm.MediaId = mediaEntity.MediaId;
                    mrtm.MediaTypeAsEnum = toMediaRatioType.MediaType;
                    mediaEntity.FileDownloadDelegate = () => mrtm.MediaEntity.Download();

                    MediaHelper.SetDefaultCutout(mediaEntity, mrtm);

                    if (mrtm.Save())
                    {
                        mrtm.ResizeAndPublishFile(mrtm.MediaEntity, true);
                        
                        // GC
                        mrtm.MediaEntity = null;
                        mrtm.MediaRatioTypeMediaFileEntity = null;
                        //view.RelatedCollection.Add(mrtm);
                    }
                }                
            }
            catch
            {
                // Do nothing
            }
            finally
            {
                
            }
        }

        private void CopyCompanyImages()
        {
            // Company ID is set
            if (CmsSessionHelper.CurrentCompanyId > 0)
            {
                PredicateExpression filter = null;
                RelationCollection relations = null;

                PredicateExpression mediaTypeFilter = new PredicateExpression();
                mediaTypeFilter.Add(MediaRatioTypeMediaFields.MediaType == MediaType.Homepage800x480);
                mediaTypeFilter.AddWithOr(MediaRatioTypeMediaFields.MediaType == MediaType.Homepage1280x800);
                mediaTypeFilter.AddWithOr(MediaRatioTypeMediaFields.MediaType == MediaType.Basket800x480);
                mediaTypeFilter.AddWithOr(MediaRatioTypeMediaFields.MediaType == MediaType.Basket1280x800);

                MediaCollection companyMedia = new MediaCollection();

                relations = new RelationCollection();
                relations.Add(MediaEntityBase.Relations.MediaRatioTypeMediaEntityUsingMediaId);

                filter = new PredicateExpression();
                filter.Add(MediaFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
                filter.Add(mediaTypeFilter);

                PrefetchPath prefetch = new PrefetchPath(EntityType.MediaEntity);
                prefetch.Add(MediaEntityBase.PrefetchPathMediaRatioTypeMediaCollection).SubPath.Add(MediaRatioTypeMediaEntityBase.PrefetchPathMediaRatioTypeMediaFileEntity);

                companyMedia.GetMulti(filter, 0, null, relations, prefetch);

                // This company has homepage images
                if (companyMedia.Count > 0)
                {
                    DeliverypointgroupCollection deliverypointgroups = new DeliverypointgroupCollection();

                    filter = new PredicateExpression();
                    filter.Add(DeliverypointgroupFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

                    deliverypointgroups.GetMulti(filter);

                    // Iterate through this company's deliverypointgroups
                    foreach (DeliverypointgroupEntity deliverypointgroup in deliverypointgroups)
                    {
                        MediaCollection media = new MediaCollection();

                        relations = new RelationCollection();
                        relations.Add(MediaEntityBase.Relations.MediaRatioTypeMediaEntityUsingMediaId);

                        filter = new PredicateExpression();
                        filter.Add(MediaFields.DeliverypointgroupId == deliverypointgroup.DeliverypointgroupId);

                        filter.Add(mediaTypeFilter);

                        media.GetMulti(filter, relations);

                        // This deliverypointgroup has no homepage/basket images yet
                        if (media.Count <= 0)
                        {
                            media = new MediaCollection();

                            relations = new RelationCollection();
                            relations.Add(MediaEntityBase.Relations.MediaRatioTypeMediaEntityUsingMediaId);

                            filter = new PredicateExpression();
                            filter.Add(MediaFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
                            filter.Add(mediaTypeFilter);

                            prefetch = new PrefetchPath(EntityType.MediaEntity);
                            prefetch.Add(MediaEntityBase.PrefetchPathMediaRatioTypeMediaCollection).SubPath.Add(MediaRatioTypeMediaEntityBase.PrefetchPathMediaRatioTypeMediaFileEntity);

                            media.GetMulti(filter, 0, null, relations, prefetch);

                            // Save all Media
                            foreach (MediaEntity mediaEntity in companyMedia)
                            {
                                MediaEntity newMediaEntity = new MediaEntity();
                                newMediaEntity.MediaType = -1;
                                newMediaEntity.Name = mediaEntity.Name;
                                newMediaEntity.FilePathRelativeToMediaPath = mediaEntity.FilePathRelativeToMediaPath;
                                newMediaEntity.DeliverypointgroupId = deliverypointgroup.DeliverypointgroupId;
                                newMediaEntity.Save();

                                // Save all MediaRatioTypeMedia
                                foreach (MediaRatioTypeMediaEntity mediaRatioTypeMediaEntity in mediaEntity.MediaRatioTypeMediaCollection)
                                {
                                    MediaRatioTypeMediaEntity newMediaRatioTypeMediaEntity = new MediaRatioTypeMediaEntity();
                                    newMediaRatioTypeMediaEntity.MediaId = newMediaEntity.MediaId;
                                    newMediaRatioTypeMediaEntity.MediaType = mediaRatioTypeMediaEntity.MediaType;
                                    newMediaRatioTypeMediaEntity.Top = mediaRatioTypeMediaEntity.Top;
                                    newMediaRatioTypeMediaEntity.Left = mediaRatioTypeMediaEntity.Left;
                                    newMediaRatioTypeMediaEntity.Width = mediaRatioTypeMediaEntity.Width;
                                    newMediaRatioTypeMediaEntity.Height = mediaRatioTypeMediaEntity.Height;
                                    newMediaRatioTypeMediaEntity.Save();

                                    // Save MediaRatioTypeMediaFile for this MediaRatioTypeMedia
                                    if (mediaRatioTypeMediaEntity.MediaRatioTypeMediaFileEntity != null && !mediaRatioTypeMediaEntity.MediaRatioTypeMediaFileEntity.IsNew)
                                    {
                                        MediaRatioTypeMediaFileEntity newMediaRatioTypeMediaFileEntity = new MediaRatioTypeMediaFileEntity();
                                        newMediaRatioTypeMediaFileEntity.MediaRatioTypeMediaId = newMediaRatioTypeMediaEntity.MediaRatioTypeMediaId;
                                        newMediaRatioTypeMediaFileEntity.Save();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void CopyEntertainment()
        {
            if (CmsSessionHelper.CurrentCompanyId > 0)
            {
                PredicateExpression filter = null;

                CompanyEntertainmentCollection companyEntertainment = new CompanyEntertainmentCollection();
                filter = new PredicateExpression();
                filter.Add(CompanyEntertainmentFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
                companyEntertainment.GetMulti(filter);

                if (companyEntertainment.Count > 0)
                {
                    DeliverypointgroupCollection deliverypointgroups = new DeliverypointgroupCollection();
                    filter = new PredicateExpression();
                    filter.Add(DeliverypointgroupFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
                    deliverypointgroups.GetMulti(filter);

                    foreach (DeliverypointgroupEntity deliverypointgroup in deliverypointgroups)
                    {
                        foreach (CompanyEntertainmentEntity companyEntertainmentEntity in companyEntertainment)
                        {
                            if (deliverypointgroup.CompanyId == companyEntertainmentEntity.EntertainmentEntity.CompanyId)
                            {
                                DeliverypointgroupEntertainmentCollection deliverypointgroupEntertainment = new DeliverypointgroupEntertainmentCollection();
                                filter = new PredicateExpression();
                                filter.Add(DeliverypointgroupEntertainmentFields.DeliverypointgroupId == deliverypointgroup.DeliverypointgroupId);
                                filter.Add(DeliverypointgroupEntertainmentFields.EntertainmentId == companyEntertainmentEntity.EntertainmentId);
                                deliverypointgroupEntertainment.GetMulti(filter);

                                if (deliverypointgroupEntertainment.Count == 0)
                                {
                                    DeliverypointgroupEntertainmentEntity newDeliverypointgroupEntertainmentEntity = new DeliverypointgroupEntertainmentEntity();
                                    newDeliverypointgroupEntertainmentEntity.DeliverypointgroupId = deliverypointgroup.DeliverypointgroupId;
                                    newDeliverypointgroupEntertainmentEntity.EntertainmentId = companyEntertainmentEntity.EntertainmentId;
                                    newDeliverypointgroupEntertainmentEntity.Save();
                                }
                            }
                        }
                    }
                }
            }
        }

        private void CopySiteImages()
        {
            new Thread(param =>
            {
                try
                {
                    GeneralAuthorizer.SetThreadGodMode(true);

                    this.CopyMediaRatioTypeMedia(MediaType.SiteBranding1280x800, MediaType.SiteMenuHeader1280x800);
                    this.CopyMediaRatioTypeMedia(MediaType.SitePageIcon1280x800, MediaType.SiteMenuIcon1280x800);
                    this.CopyMediaRatioTypeMedia(MediaType.SiteFullPageImage1280x800, MediaType.SitePageFull1280x800);
                    this.CopyMediaRatioTypeMedia(MediaType.SiteFullPageImageDirectory1280x800, MediaType.SitePageFull1280x800);
                    this.CopyMediaRatioTypeMedia(MediaType.SiteTwoThirdsPageImage1280x800, MediaType.SitePageTwoThirds1280x800);
                    this.CopyMediaRatioTypeMedia(MediaType.SiteTwoThirdsPageImageDirectory1280x800, MediaType.SitePageTwoThirds1280x800);
                    this.CopyMediaRatioTypeMedia(MediaType.SitePointOfInterestSummaryPage1280x800, MediaType.SitePageTwoThirdsPointOfInterest1280x800);
                    this.CopyMediaRatioTypeMedia(MediaType.SitePointOfInterestSummaryPageMicrosite1280x800, MediaType.SitePageTwoThirdsPointOfInterest1280x800);
                    this.CopyMediaRatioTypeMedia(MediaType.SiteCompanySummaryPage1280x800, MediaType.SitePageTwoThirdsCompany1280x800);
                }
                catch
                { }
                finally
                {
                    GeneralAuthorizer.SetThreadGodMode(false);
                }
            }).Start(HttpContext.Current);            
        }

        private void DecodeSites()
        {
            PredicateExpression filter = new PredicateExpression(PageElementFields.PageElementType == PageElementType.MultiLineText);

            PageElementCollection pageElements = new PageElementCollection();
            pageElements.GetMulti(filter);

            foreach (PageElementEntity pageElementEntity in pageElements)
            {
                pageElementEntity.StringValue1 = HttpUtility.HtmlDecode(pageElementEntity.StringValue1);
                pageElementEntity.Save();
            }
        }

        private void UploadDeliveryTime()
        {
            IncludeFieldsList includes = new IncludeFieldsList(DeliverypointgroupFields.DeliverypointgroupId, DeliverypointgroupFields.EstimatedDeliveryTime);

            DeliverypointgroupCollection deliverypointgroups = new DeliverypointgroupCollection();
            deliverypointgroups.GetMulti(null, includes, 0);

            foreach (DeliverypointgroupEntity deliverypointgroupEntity in deliverypointgroups)
            {
                CloudTaskHelper.UploadDeliveryTime(deliverypointgroupEntity.DeliverypointgroupId, deliverypointgroupEntity.EstimatedDeliveryTime);
            }
        }

        private void CreateNewSiteImagesCutouts(bool isTest)
        {
            List<MediaType> types = new List<MediaType>
                                    {
                                        MediaType.SiteMenuHeader1280x800,  
                                        MediaType.SiteMenuIcon1280x800,
                                        MediaType.SitePageFull1280x800,
                                        MediaType.SitePageTwoThirds1280x800,
                                        MediaType.SitePageTwoThirdsPointOfInterest1280x800,
                                        MediaType.SitePageTwoThirdsCompany1280x800
                                    };
        
            PredicateExpression filter = new PredicateExpression();
            filter.Add(MediaRatioTypeMediaFields.MediaType == types);
            filter.Add(MediaRatioTypeMediaFields.Width == 0);
            filter.Add(MediaRatioTypeMediaFields.Height == 0);

            PrefetchPath prefetch = new PrefetchPath(EntityType.MediaRatioTypeMediaEntity);
            prefetch.Add(MediaRatioTypeMediaEntity.PrefetchPathMediaEntity);

            RelationCollection relations = new RelationCollection();
            relations.Add(MediaRatioTypeMediaEntity.Relations.MediaEntityUsingMediaId);

            MediaRatioTypeMediaCollection mrtms = new MediaRatioTypeMediaCollection();
            
            if (isTest)
            {
                mrtms.GetMulti(filter, 0, null, relations, prefetch);
                this.lblNewSiteCutouts.Text = string.Format("{0} mrtms to cutout", mrtms.Count);
            }
            else
            {
                mrtms.GetMulti(filter, 500, null, relations, prefetch);

                foreach (MediaRatioTypeMediaEntity mrtm in mrtms)
                {
                    MediaHelper.SetDefaultCutout(mrtm.MediaEntity, mrtm);
                    mrtm.Save();
                }
            }            
        }

        private void ConvertMenuImages()
        {
            //this.CopyMediaRatioTypeMedia(MediaType.ProductButtoniPadRetina, MediaType.ProductIcon1280x800);
            //this.CopyMediaRatioTypeMedia(MediaType.GenericProductButtoniPadRetina, MediaType.GenericProductIcon1280x800);
            //this.CopyMediaRatioTypeMedia(MediaType.ProductBrandingiPadRetina, MediaType.ProductPagePhotoHorizontal1280x800);
            //this.CopyMediaRatioTypeMedia(MediaType.ProductBranding1280x800, MediaType.ProductPagePhotoVertical1280x800);
            //this.CopyMediaRatioTypeMedia(MediaType.GenericProductBrandingiPadRetina, MediaType.GenericProductPagePhotoHorizontal1280x800);
            //this.CopyMediaRatioTypeMedia(MediaType.GenericProductBranding1280x800, MediaType.GenericProductPagePhotoVertical1280x800);
            //this.CopyMediaRatioTypeMedia(MediaType.CategoryBrandingiPadRetina, MediaType.CategoryPagePhotoHorizontal1280x800);
            //this.CopyMediaRatioTypeMedia(MediaType.CategoryBranding1280x800, MediaType.CategoryPagePhotoVertical1280x800);
            //// Sneaking in a non-menu image, whatcha gonna do about it?
            //this.CopyMediaRatioTypeMedia(MediaType.SiteBrandingiPadRetina, MediaType.SiteBranding1280x800);
        }

        private void EncryptCompanyOwnerPasswords()
        {
            PredicateExpression filter = new PredicateExpression(CompanyOwnerFields.IsPasswordEncrypted == false);

            CompanyOwnerCollection companyOwnerCollection = new CompanyOwnerCollection();
            companyOwnerCollection.GetMulti(filter);

            foreach (CompanyOwnerEntity companyOwnerEntity in companyOwnerCollection)
            {
                if (!companyOwnerEntity.IsPasswordEncrypted) // Check, check, doublecheck
                {
                    companyOwnerEntity.Password = Cryptographer.EncryptStringUsingRijndael(companyOwnerEntity.Password);
                    companyOwnerEntity.IsPasswordEncrypted = true;
                    companyOwnerEntity.Save();
                }
            }
        }

        private void btnPrepareForAmazon_Click(object sender, EventArgs e)
        {
            try
            {
                // Create salts pms for all the companies
                this.CreateSaltPmsForAllCompanies();

                AmazonCloudStorageHelper.CreateAmazonGroup();
                AmazonCloudStorageHelper.CreateAmazonUserPerCompany();

                this.AddInformatorInfo("The Amazones are prepared. Good job!");
            }
            catch (Exception ex)
            {
                this.AddInformatorInfo("Something went wrong trying to prepare the Amazones: {0}", ex.Message);
            }

            this.Validate();
        }

        private void btnSetDefaultAlterationDialogColors_Click(object sender, EventArgs e)
        {
            UIThemeCollection uiThemes = new UIThemeCollection();
            uiThemes.GetMulti(null);

            foreach (UIThemeEntity uiTheme in uiThemes)
            {
                uiTheme.AlterationDialogBackgroundColor = -15066598;
                uiTheme.AlterationDialogBorderColor = -9211021;
                uiTheme.AlterationDialogPanelBackgroundColor = -13421773;
                uiTheme.AlterationDialogPanelBorderColor = -9211021;
                uiTheme.AlterationDialogListItemBackgroundColor = 0;
                uiTheme.AlterationDialogListItemSelectedBackgroundColor = -11776948;
                uiTheme.AlterationDialogListItemSelectedBorderColor = 0;
                uiTheme.AlterationDialogInputBackgroundColor = 0;
                uiTheme.AlterationDialogInputBorderColor = -10658467;
                uiTheme.AlterationDialogInputTextColor = -1;
                uiTheme.AlterationDialogInputHintTextColor = -8355712;
                uiTheme.AlterationDialogPrimaryTextColor = -1;
                uiTheme.AlterationDialogSecondaryTextColor = -9803160;
                uiTheme.AlterationDialogRadioButtonColor = -1;
                uiTheme.AlterationDialogCheckBoxColor = -1;
                uiTheme.AlterationDialogCloseButtonColor = -1;
                uiTheme.AlterationDialogListViewArrowColor = -1;
                uiTheme.AlterationDialogListViewDividerColor = -1;
                uiTheme.Save();
            }
        }

        private void ConvertProductSuggestions()
        {
            PrefetchPath productSuggestionPath = new PrefetchPath(EntityType.ProductSuggestionEntity);
            productSuggestionPath.Add(ProductSuggestionEntity.PrefetchPathProductEntity);

            RelationCollection productSuggestionRelations = new RelationCollection();
            productSuggestionRelations.Add(ProductSuggestionEntity.Relations.ProductEntityUsingProductId);

            ProductSuggestionCollection productSuggestionCollection = new ProductSuggestionCollection();
            productSuggestionCollection.GetMulti(null, 0, null, productSuggestionRelations, productSuggestionPath);

            foreach (ProductSuggestionEntity productSuggestion in productSuggestionCollection)
            {
                PredicateExpression categoriesFilter = new PredicateExpression();
                categoriesFilter.Add(ProductCategoryFields.ProductId == productSuggestion.ProductId);

                PrefetchPath categoriesPath = new PrefetchPath(EntityType.ProductCategoryEntity);
                categoriesPath.Add(ProductCategoryEntity.PrefetchPathCategoryEntity).SubPath.Add(CategoryEntity.PrefetchPathMenuEntity);

                ProductCategoryCollection categories = new ProductCategoryCollection();
                categories.GetMulti(categoriesFilter, categoriesPath);

                foreach (ProductCategoryEntity category in categories)
                {
                    PredicateExpression suggestedProductCategoryFilter = new PredicateExpression();
                    suggestedProductCategoryFilter.Add(ProductCategoryFields.ProductId == productSuggestion.SuggestedProductId);
                    suggestedProductCategoryFilter.Add(MenuFields.MenuId == category.CategoryEntity.MenuEntity.MenuId);

                    RelationCollection suggestedProductCategoryRelations = new RelationCollection();
                    suggestedProductCategoryRelations.Add(ProductCategoryEntity.Relations.CategoryEntityUsingCategoryId);
                    suggestedProductCategoryRelations.Add(CategoryEntity.Relations.MenuEntityUsingMenuId);

                    ProductCategoryCollection suggestedProductCategoryCollection = new ProductCategoryCollection();
                    suggestedProductCategoryCollection.GetMulti(suggestedProductCategoryFilter, 1, null, suggestedProductCategoryRelations);

                    foreach (ProductCategoryEntity suggestedProductCategory in suggestedProductCategoryCollection)
                    {
                        PredicateExpression existingProductCategorySuggestionFilter = new PredicateExpression();
                        existingProductCategorySuggestionFilter.Add(ProductCategorySuggestionFields.ProductCategoryId == category.ProductCategoryId);
                        existingProductCategorySuggestionFilter.Add(ProductCategorySuggestionFields.SuggestedProductCategoryId == suggestedProductCategory.ProductCategoryId);

                        ProductCategorySuggestionCollection productCategorySuggestionCollection = new ProductCategorySuggestionCollection();
                        productCategorySuggestionCollection.GetMulti(existingProductCategorySuggestionFilter);

                        if (productCategorySuggestionCollection.Count == 0)
                        {
                            object scalar = new ProductCategorySuggestionCollection().GetScalar(ProductCategorySuggestionFieldIndex.SortOrder, null, AggregateFunction.Max, ProductCategorySuggestionFields.ProductCategoryId == category.ProductCategoryId);
                            int sortValue = 0;
                            int.TryParse(scalar.ToString(), out sortValue);

                            ProductCategorySuggestionEntity productCategorySuggestionEntity = new ProductCategorySuggestionEntity();
                            productCategorySuggestionEntity.ProductCategoryId = category.ProductCategoryId;
                            productCategorySuggestionEntity.SuggestedProductCategoryId = suggestedProductCategory.ProductCategoryId;
                            productCategorySuggestionEntity.Checkout = productSuggestion.Checkout;
                            productCategorySuggestionEntity.SortOrder = sortValue + 1;
                            productCategorySuggestionEntity.Save();
                        }
                    }
                }
            }
        }

        private void CreateSaltPmsForAllCompanies()
        {
            PredicateExpression filter = new PredicateExpression(CompanyFields.SaltPms == DBNull.Value);

            CompanyCollection companiesWithoutSaltPms = new CompanyCollection();
            companiesWithoutSaltPms.GetMulti(filter);

            foreach (CompanyEntity company in companiesWithoutSaltPms)
            {
                string saltKey = RandomUtil.GetRandomLowerCaseString(16); // Must be 16 characters

                company.SaltPms = Cryptographer.EncryptUsingCBC(saltKey);
                company.Save();
            }
        }

        private void btnSetRoomControllerTypes_Click(object sender, EventArgs e)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(DeliverypointFields.RoomControllerType == DBNull.Value);

            DeliverypointCollection dpgCollection = new DeliverypointCollection();
            dpgCollection.GetMulti(filter);

            foreach (DeliverypointEntity deliverypointEntity in dpgCollection)
            {
                deliverypointEntity.RoomControllerType = deliverypointEntity.RoomControllerIp.IsNullOrWhiteSpace() ? RoomControlType.None : RoomControlType.Control4;

                deliverypointEntity.Save();
            }

            this.AddInformatorInfo("Deliverypoints without controller but with IP are set to Control4 room contoller type");
        }

        private void btnSetLoadedSuccessfully_Click(object sender, EventArgs e)
        {
            PredicateExpression filter = new PredicateExpression(new FieldLikePredicate(ClientFields.Notes, "%loaded:true%"));
            filter.Add(ClientFields.LoadedSuccessfully == false);

            ClientEntity valuesToSet = new ClientEntity();
            valuesToSet.LoadedSuccessfully = true;

            ClientCollection clients = new ClientCollection();
            clients.UpdateMulti(valuesToSet, filter);

            this.AddInformatorInfo("Clients are updated with the LoadSuccessfully flag");
        }

        private void btnSetVisibilityType_Click(object sender, EventArgs e)
        {
            // We only need the ones that have Visible set to false because VisibilityType default value is 0, which is Always
            PredicateExpression productFilter = new PredicateExpression();
            productFilter.Add(ProductFields.Visible == false);
            productFilter.Add(ProductFields.PosproductId == DBNull.Value);

            ProductCollection products = new ProductCollection();
            products.GetMulti(productFilter);

            foreach (ProductEntity product in products)
            {
                product.VisibilityType = VisibilityType.Never;
            }

            products.SaveMulti();

            // Same thing for the categories
            PredicateExpression categoryFilter = new PredicateExpression();
            categoryFilter.Add(CategoryFields.Visible == false);

            CategoryCollection categories = new CategoryCollection();
            categories.GetMulti(categoryFilter);

            foreach (CategoryEntity category in categories)
            {
                category.VisibilityType = VisibilityType.Never;
            }

            categories.SaveMulti();
        }

        private void btnSetMediaRelatedCompanyId_Click(object sender, EventArgs e)
        {
            PredicateExpression filter = new PredicateExpression(MediaFields.RelatedCompanyId == DBNull.Value);

            MediaCollection mediaCollection = new MediaCollection();
            mediaCollection.GetMulti(filter);

            foreach(MediaEntity media in mediaCollection)
            {
                media.RelatedCompanyId = this.GetRelatedCompanyId(media);
                media.Validator = null;
                media.Save();
            }

            //mediaCollection.SaveMulti();
        }
                
        private int? GetRelatedCompanyId(MediaEntity media)
        {
            int? toReturn = null;
            if (media.CompanyId.HasValue)
                toReturn = media.CompanyId;
            else if (media.ProductId.HasValue)
                toReturn = media.ProductEntity.CompanyId;
            else if (media.CategoryId.HasValue)
                toReturn = media.CategoryEntity.CompanyId;
            else if (media.AdvertisementId.HasValue)
                toReturn = media.AdvertisementEntity.CompanyId;
            else if (media.EntertainmentId.HasValue)
                toReturn = media.EntertainmentEntity.CompanyId;
            else if (media.AlterationoptionId.HasValue)
                toReturn = media.AlterationoptionEntity.CompanyId;
            else if (media.GenericproductId.HasValue)
                toReturn = null;
            else if (media.GenericcategoryId.HasValue)
                toReturn = null;
            else if (media.DeliverypointgroupId.HasValue)
                toReturn = media.DeliverypointgroupEntity.CompanyId;
            else if (media.SurveyId.HasValue)
                toReturn = media.SurveyEntity.CompanyId;
            else if (media.SurveyPageId.HasValue)
                toReturn = media.SurveyPageEntity.SurveyEntity.CompanyId;
            else if (media.ActionProductId.HasValue)
                toReturn = media.ActionProductEntity.CompanyId;
            else if (media.ActionCategoryId.HasValue)
                toReturn = media.ActionCategoryEntity.CompanyId;
            else if (media.ActionEntertainmentId.HasValue)
                toReturn = media.ActionEntertainmentEntity.CompanyId;
            else if (media.ActionEntertainmentcategoryId.HasValue)
                toReturn = null;
            else if (media.AlterationId.HasValue)
                toReturn = media.AlterationEntity.CompanyId;
            else if (media.PointOfInterestId.HasValue)
                toReturn = null;
            else if (media.RoutestephandlerId.HasValue)
                toReturn = media.RoutestephandlerEntity.RoutestepEntity.RouteEntity.CompanyId;
            else if (media.PageElementId.HasValue)
                toReturn = media.PageElementEntity.PageEntity.SiteEntity.CompanyId;
            else if (media.PageId.HasValue)
                toReturn = media.PageEntity.SiteEntity.CompanyId;
            else if (media.SiteId.HasValue)
                toReturn = media.SiteEntity.CompanyId;
            else if (media.AttachmentId.HasValue)
                toReturn = media.AttachmentEntity.ProductId.HasValue ? media.AttachmentEntity.ProductEntity.CompanyId : media.AttachmentEntity.PageEntity.SiteEntity.CompanyId;
            else if (media.PageTemplateElementId.HasValue)
                toReturn = null;
            else if (media.PageTemplateId.HasValue)
                toReturn = null;
            else if (media.UIWidgetId.HasValue)
                toReturn = media.UIWidgetEntity.UITabEntity.UIModeEntity.CompanyId;
            else if (media.UIThemeId.HasValue)
                toReturn = null;
            else if (media.RoomControlSectionId.HasValue)
                toReturn = media.RoomControlSectionEntity.RoomControlAreaEntity.RoomControlConfigurationEntity.CompanyId;
            else if (media.RoomControlSectionItemId.HasValue)
                toReturn = media.RoomControlSectionItemEntity.RoomControlSectionEntity.RoomControlAreaEntity.RoomControlConfigurationEntity.CompanyId;
            else if (media.StationId.HasValue)
                toReturn = media.StationEntity.StationListEntity.CompanyId;
            else if (media.UIFooterItemId.HasValue)
                toReturn = media.UIFooterItemEntity.UIModeEntity.CompanyId;

            return toReturn;
        }

        void btnSetCustomTextParentCompanyId_Click(object sender, EventArgs e)
        {
            CustomTextCollection customTextCollection = new CustomTextCollection();
            customTextCollection.GetMulti(null);

            foreach(CustomTextEntity customTextEntity in customTextCollection)
            {
                ICompanyRelatedChildEntityOrNullableCompanyRelatedChildEntity companyRelatedChildEntityOrNullableCompanyRelatedChildEntity = customTextEntity as ICompanyRelatedChildEntityOrNullableCompanyRelatedChildEntity;
                if (companyRelatedChildEntityOrNullableCompanyRelatedChildEntity.Parent != null)
                {
                    if (companyRelatedChildEntityOrNullableCompanyRelatedChildEntity.Parent is ICompanyRelatedEntity ||
                        companyRelatedChildEntityOrNullableCompanyRelatedChildEntity.Parent is ICompanyRelatedChildEntity)
                    {
                        ICompanyRelatedEntity parent = this.GetCompanyRelatedParent(companyRelatedChildEntityOrNullableCompanyRelatedChildEntity.Parent);
                        if (parent != null)
                        {
                            companyRelatedChildEntityOrNullableCompanyRelatedChildEntity.ParentCompanyId = parent.CompanyId;
                        }
                    }
                    else if (companyRelatedChildEntityOrNullableCompanyRelatedChildEntity.Parent is INullableCompanyRelatedEntity ||
                                companyRelatedChildEntityOrNullableCompanyRelatedChildEntity.Parent is INullableCompanyRelatedChildEntity)
                    {
                        INullableCompanyRelatedEntity parent = this.GetNullableCompanyRelatedParent(companyRelatedChildEntityOrNullableCompanyRelatedChildEntity.Parent);
                        if (parent != null)
                        {
                            companyRelatedChildEntityOrNullableCompanyRelatedChildEntity.ParentCompanyId = parent.CompanyId;
                        }
                    }
                    else if (companyRelatedChildEntityOrNullableCompanyRelatedChildEntity.Parent is ICompanyRelatedChildEntityOrNullableCompanyRelatedChildEntity)
                    {
                        IEntity parent = this.GetCompanyRelatedParentOrNullableCompanyRelatedParent(companyRelatedChildEntityOrNullableCompanyRelatedChildEntity.Parent);
                        if (parent != null)
                        {
                            object companyId = parent.Fields["CompanyId"].CurrentValue;
                            companyRelatedChildEntityOrNullableCompanyRelatedChildEntity.ParentCompanyId = (int?)companyId;
                        }
                    }
                }
                customTextEntity.Validator = null;
                customTextEntity.Save();
            }
            //customTextCollection.SaveMulti();
        }

        private ICompanyRelatedEntity GetCompanyRelatedParent(IEntityCore involvedEntity)
        {
            ICompanyRelatedEntity parent = null;

            if (involvedEntity is ICompanyRelatedEntity)
                parent = involvedEntity as ICompanyRelatedEntity;
            else if (involvedEntity is ICompanyRelatedChildEntity)
            {
                ICompanyRelatedChildEntity child = involvedEntity as ICompanyRelatedChildEntity;
                if (child != null && child.Parent != null)
                    parent = GetCompanyRelatedParent(child.Parent);
            }

            return parent;
        }

        private INullableCompanyRelatedEntity GetNullableCompanyRelatedParent(IEntityCore involvedEntity)
        {
            INullableCompanyRelatedEntity parent = null;

            if (involvedEntity is INullableCompanyRelatedEntity)
                parent = involvedEntity as INullableCompanyRelatedEntity;
            else if (involvedEntity is INullableCompanyRelatedChildEntity)
            {
                INullableCompanyRelatedChildEntity child = involvedEntity as INullableCompanyRelatedChildEntity;
                if (child != null && child.Parent != null)
                    parent = GetNullableCompanyRelatedParent(child.Parent);
            }

            return parent;
        }

        private IEntity GetCompanyRelatedParentOrNullableCompanyRelatedParent(IEntityCore involvedEntity)
        {
            IEntity parent = null;

            if (involvedEntity is INullableCompanyRelatedEntity)
            {
                parent = involvedEntity as IEntity;
            }
            else if (involvedEntity is ICompanyRelatedEntity)
            {
                parent = involvedEntity as IEntity;
            }
            else if (involvedEntity is INullableCompanyRelatedChildEntity)
            {
                INullableCompanyRelatedChildEntity child = involvedEntity as INullableCompanyRelatedChildEntity;
                if (child != null && child.Parent != null)
                    parent = GetNullableCompanyRelatedParent(child.Parent) as IEntity;
            }
            else if (involvedEntity is ICompanyRelatedChildEntity)
            {
                ICompanyRelatedChildEntity child = involvedEntity as ICompanyRelatedChildEntity;
                if (child != null && child.Parent != null)
                    parent = GetCompanyRelatedParent(child.Parent) as IEntity;
            }
            else if (involvedEntity is ICompanyRelatedChildEntityOrNullableCompanyRelatedChildEntity)
            {
                ICompanyRelatedChildEntityOrNullableCompanyRelatedChildEntity child = involvedEntity as ICompanyRelatedChildEntityOrNullableCompanyRelatedChildEntity;
                if (child != null && child.Parent != null)
                    parent = GetCompanyRelatedParentOrNullableCompanyRelatedParent(child.Parent) as IEntity;
            }

            return parent;
        }


        private void ConvertGenericProductsToBrandProductsMedia()
        {
            int uploadedMedia = 0;

            PredicateExpression filter = new PredicateExpression();
            filter.Add(ProductFields.BrandId != DBNull.Value);

            PrefetchPath prefetch = new PrefetchPath(EntityType.ProductEntity);
            prefetch.Add(ProductEntity.PrefetchPathMediaCollection).SubPath.Add(MediaEntity.PrefetchPathMediaRatioTypeMediaCollection);

            ProductCollection productCollection = new ProductCollection();
            productCollection.GetMulti(filter, prefetch);

            foreach (ProductEntity productEntity in productCollection)
            {
                foreach (MediaEntity mediaEntity in productEntity.MediaCollection)
                {
                    // Get download path and convert to old location
                    List<string> pathComponents = new List<string>();
                    pathComponents.Add(Obymobi.Web.CloudStorage.MediaExtensions.GenericFilesSubPath);
                    pathComponents.Add("original");
                    pathComponents.Add(mediaEntity.GetFileName(FileNameType.Cdn));
                    string oldUrl = Obymobi.Web.CloudStorage.MediaExtensions.GetCdnBaseUrl(CloudStorageProviderIdentifier.Amazon, string.Join("\\", pathComponents));

                    // Download old media
                    byte[] file = Obymobi.Web.CloudStorage.MediaExtensions.DownloadImageFromCdn(oldUrl);

                    bool uploaded = false;
                    if (file != null && file.Length > 0)
                    {
                        // Upload media to new location
                        mediaEntity.LastDistributedVersionTicks = DateTime.UtcNow.Ticks;
                        uploaded = mediaEntity.Upload(file);
                        uploadedMedia++;
                    }

                    foreach (MediaRatioTypeMediaEntity mediaRatioTypeEntity in mediaEntity.MediaRatioTypeMediaCollection)
                    {
                        MediaType type = (MediaType)mediaRatioTypeEntity.MediaType;
                        if (type == MediaType.GenericProductBranding1280x800)
                            type = MediaType.ProductBranding1280x800;
                        else if (type == MediaType.GenericProductPagePhotoHorizontal1280x800)
                            type = MediaType.ProductPagePhotoHorizontal1280x800;
                        else if (type == MediaType.GenericProductPagePhotoVertical1280x800)
                            type = MediaType.ProductPagePhotoVertical1280x800;
                        else if (type == MediaType.GenericProductButton1280x800)
                            type = MediaType.ProductButton1280x800;
                        else if (type == MediaType.GenericProductIcon1280x800)
                            type = MediaType.ProductIcon1280x800;

                        if (type != (MediaType)mediaRatioTypeEntity.MediaType)
                        {
                            mediaRatioTypeEntity.MediaType = (int)type;
                            mediaRatioTypeEntity.Save();

                            MediaHelper.QueueMediaRatioTypeMediaFileTask(mediaRatioTypeEntity, MediaProcessingTaskEntity.ProcessingAction.Upload, true, null, null);
                        }
                    }
                }
            }

            this.AddInformator(InformatorType.Information, "Uploaded {0} brand product media", uploadedMedia);
        }

        private void CreatePmsReportConfigurations()
        {
            PmsReportConfigurationCollection pmsReportConfigurationCollection = new PmsReportConfigurationCollection();
            if (pmsReportConfigurationCollection.GetDbCount() > 0)
            {
                return;
            }

            this.CreateSlsPmsConfiguration();
            this.CreateHyattPmsConfiguration();            
        }

        private PmsReportConfigurationEntity CreateSlsPmsConfiguration()
        {
            PmsReportConfigurationEntity configuration = new PmsReportConfigurationEntity();
            configuration.CompanyId = 441;
            configuration.Name = "SLS Bahamar Report Configuration";
            configuration.Email = "pms+441-1@crave-emenu.com";
            configuration.PmsReportType = PmsReportType.PlainText;
            configuration.ColumnSeparationType = 0;
            configuration.Save();
            
            PmsReportColumnEntity slsColumn1 = new PmsReportColumnEntity();
            slsColumn1.PmsReportConfigurationId = configuration.PmsReportConfigurationId;
            slsColumn1.ParentCompanyId = 441;
            slsColumn1.ColumnIndex = 0;
            slsColumn1.FriendlyName = "Check-in";
            slsColumn1.DataType = DataType.DateTime;
            slsColumn1.Format = "dd-MMM-yy";
            slsColumn1.Save();

            PmsReportColumnEntity slsColumn2 = new PmsReportColumnEntity();
            slsColumn2.PmsReportConfigurationId = configuration.PmsReportConfigurationId;
            slsColumn2.ParentCompanyId = 441;
            slsColumn2.ColumnIndex = 1;
            slsColumn2.FriendlyName = "Check-out";
            slsColumn2.DataType = DataType.DateTime;
            slsColumn2.Format = "dd-MMM-yy";
            slsColumn2.Save();

            PmsReportColumnEntity slsColumn3 = new PmsReportColumnEntity();
            slsColumn3.PmsReportConfigurationId = configuration.PmsReportConfigurationId;
            slsColumn3.ParentCompanyId = 441;
            slsColumn3.ColumnIndex = 2;
            slsColumn3.FriendlyName = "Room number";
            slsColumn3.DataType = DataType.Integer;
            slsColumn3.Format = "";
            slsColumn3.Save();

            PmsReportColumnEntity slsColumn4 = new PmsReportColumnEntity();
            slsColumn4.PmsReportConfigurationId = configuration.PmsReportConfigurationId;
            slsColumn4.ParentCompanyId = 441;
            slsColumn4.ColumnIndex = 3;
            slsColumn4.FriendlyName = "Checked in";
            slsColumn4.DataType = DataType.String;
            slsColumn4.Format = "";
            slsColumn4.Save();

            PmsActionRuleEntity slsActionRule1 = new PmsActionRuleEntity();
            slsActionRule1.PmsReportConfigurationId = configuration.PmsReportConfigurationId;
            slsActionRule1.ParentCompanyId = 441;
            slsActionRule1.Active = true;
            slsActionRule1.Type = PmsActionType.AddToMessagegroup;
            slsActionRule1.MessagegroupId = 593;
            slsActionRule1.Save();

            PmsRuleEntity slsRule1 = new PmsRuleEntity();
            slsRule1.PmsActionRuleId = slsActionRule1.PmsActionRuleId;
            slsRule1.ParentCompanyId = 441;
            slsRule1.PmsReportColumnEntity = slsColumn2;
            slsRule1.RuleType = RuleType.Equals;
            slsRule1.Value = "{today}";
            slsRule1.Save();

            PmsActionRuleEntity slsActionRule2 = new PmsActionRuleEntity();
            slsActionRule2.PmsReportConfigurationId = configuration.PmsReportConfigurationId;
            slsActionRule2.ParentCompanyId = 441;
            slsActionRule2.Active = true;
            slsActionRule2.Type = PmsActionType.AddToMessagegroup;
            slsActionRule2.MessagegroupId = 600;
            slsActionRule2.Save();

            PmsRuleEntity slsRule2 = new PmsRuleEntity();
            slsRule2.PmsActionRuleId = slsActionRule2.PmsActionRuleId;
            slsRule2.ParentCompanyId = 441;
            slsRule2.PmsReportColumnEntity = slsColumn2;
            slsRule2.RuleType = RuleType.Equals;
            slsRule2.Value = "{tomorrow}";
            slsRule2.Save();

            configuration.RoomNumberColumnId = slsColumn3.PmsReportColumnId;
            configuration.Save();

            return configuration;
        }

        private PmsReportConfigurationEntity CreateHyattPmsConfiguration()
        {
            PmsReportConfigurationEntity configuration = new PmsReportConfigurationEntity();
            configuration.CompanyId = 430;
            configuration.Name = "Hyatt Bahamar Report Configuration";
            configuration.Email = "pms+430-2@crave-emenu.com";
            configuration.PmsReportType = PmsReportType.PlainText;
            configuration.ColumnSeparationType = 0;
            configuration.Save();
            
            PmsReportColumnEntity slsColumn1 = new PmsReportColumnEntity();
            slsColumn1.PmsReportConfigurationId = configuration.PmsReportConfigurationId;
            slsColumn1.ParentCompanyId = 430;
            slsColumn1.ColumnIndex = 0;
            slsColumn1.FriendlyName = "Room number";
            slsColumn1.DataType = DataType.Integer;
            slsColumn1.Format = "";
            slsColumn1.Save();

            PmsReportColumnEntity slsColumn2 = new PmsReportColumnEntity();
            slsColumn2.PmsReportConfigurationId = configuration.PmsReportConfigurationId;
            slsColumn2.ParentCompanyId = 430;
            slsColumn2.ColumnIndex = 1;
            slsColumn2.FriendlyName = "Check-in";
            slsColumn2.DataType = DataType.DateTime;
            slsColumn2.Format = "dd-MMM-yy";
            slsColumn2.Save();

            PmsReportColumnEntity slsColumn3 = new PmsReportColumnEntity();
            slsColumn3.PmsReportConfigurationId = configuration.PmsReportConfigurationId;
            slsColumn3.ParentCompanyId = 430;
            slsColumn3.ColumnIndex = 2;
            slsColumn3.FriendlyName = "Check-out";
            slsColumn3.DataType = DataType.DateTime;
            slsColumn3.Format = "dd-MMM-yy";
            slsColumn3.Save();

            PmsReportColumnEntity slsColumn4 = new PmsReportColumnEntity();
            slsColumn4.PmsReportConfigurationId = configuration.PmsReportConfigurationId;
            slsColumn4.ParentCompanyId = 430;
            slsColumn4.ColumnIndex = 3;
            slsColumn4.FriendlyName = "Checked in";
            slsColumn4.DataType = DataType.String;
            slsColumn4.Format = "";
            slsColumn4.Save();

            PmsActionRuleEntity slsActionRule1 = new PmsActionRuleEntity();
            slsActionRule1.PmsReportConfigurationId = configuration.PmsReportConfigurationId;
            slsActionRule1.ParentCompanyId = 430;
            slsActionRule1.Active = true;
            slsActionRule1.Type = PmsActionType.AddToMessagegroup;
            slsActionRule1.MessagegroupId = 492;
            slsActionRule1.Save();

            PmsRuleEntity slsRule1 = new PmsRuleEntity();
            slsRule1.PmsActionRuleId = slsActionRule1.PmsActionRuleId;
            slsRule1.ParentCompanyId = 430;
            slsRule1.PmsReportColumnEntity = slsColumn3;
            slsRule1.RuleType = RuleType.Equals;
            slsRule1.Value = "{today}";
            slsRule1.Save();

            PmsActionRuleEntity slsActionRule2 = new PmsActionRuleEntity();
            slsActionRule2.PmsReportConfigurationId = configuration.PmsReportConfigurationId;
            slsActionRule2.ParentCompanyId = 430;
            slsActionRule2.Active = true;
            slsActionRule2.Type = PmsActionType.AddToMessagegroup;
            slsActionRule2.MessagegroupId = 482;
            slsActionRule2.Save();

            PmsRuleEntity slsRule2 = new PmsRuleEntity();
            slsRule2.PmsActionRuleId = slsActionRule2.PmsActionRuleId;
            slsRule2.ParentCompanyId = 430;
            slsRule2.PmsReportColumnEntity = slsColumn3;
            slsRule2.RuleType = RuleType.Equals;
            slsRule2.Value = "{tomorrow}";
            slsRule2.Save();

            configuration.RoomNumberColumnId = slsColumn1.PmsReportColumnId;
            configuration.Save();

            return configuration;
        }

        private void ConvertGenericProductsToBrandProducts()
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(GenericproductFields.BrandId != DBNull.Value);

            PrefetchPath prefetch = new PrefetchPath(EntityType.GenericproductEntity);
            prefetch.Add(GenericproductEntityBase.PrefetchPathAttachmentCollection);
            prefetch.Add(GenericproductEntityBase.PrefetchPathCustomTextCollection);
            prefetch.Add(GenericproductEntityBase.PrefetchPathMediaCollection);
            prefetch.Add(GenericproductEntityBase.PrefetchPathProductCollection);

            GenericproductCollection genericproductCollection = new GenericproductCollection();
            genericproductCollection.GetMulti(filter, prefetch);

            foreach (GenericproductEntity genericproductEntity in genericproductCollection)
            {
                ProductEntity productEntity = new ProductEntity();
                productEntity.Type = (int)ProductType.BrandProduct;
                productEntity.SubType = genericproductEntity.SubType;
                productEntity.BrandId = genericproductEntity.BrandId;
                productEntity.Name = genericproductEntity.Name;
                productEntity.Description = genericproductEntity.Description;
                productEntity.PriceIn = genericproductEntity.PriceIn;
                productEntity.VattariffId = genericproductEntity.VattariffId.GetValueOrDefault(3);
                productEntity.TextColor = genericproductEntity.TextColor;
                productEntity.BackgroundColor = genericproductEntity.BackgroundColor;
                productEntity.WebTypeTabletUrl = genericproductEntity.WebTypeTabletUrl;
                productEntity.WebTypeSmartphoneUrl = genericproductEntity.WebTypeSmartphoneUrl;
                productEntity.ViewLayoutType = (ViewLayoutType)genericproductEntity.ViewLayoutType;

                if (productEntity.Save())
                {
                    productEntity.Refetch();

                    // Relink CustomTexts
                    foreach (CustomTextEntity customTextEntity in genericproductEntity.CustomTextCollection.ToList())
                    {
                        CustomTextType type = customTextEntity.Type;
                        if (type == CustomTextType.GenericproductName)
                            type = CustomTextType.ProductName;
                        else if (type == CustomTextType.GenericproductDescription)
                            type = CustomTextType.ProductDescription;

                        customTextEntity.GenericproductId = null;
                        customTextEntity.ProductId = productEntity.ProductId;
                        customTextEntity.Type = type;
                        customTextEntity.Save();
                    }

                    // Relink attachments
                    foreach (AttachmentEntity attachmentEntity in genericproductEntity.AttachmentCollection.ToList())
                    {
                        attachmentEntity.GenericproductId = null;
                        attachmentEntity.ProductId = productEntity.ProductId;
                        attachmentEntity.Save();
                    }

                    // Relink images
                    foreach (MediaEntity mediaEntity in genericproductEntity.MediaCollection.ToList())
                    {
                        MediaType type = (MediaType)mediaEntity.MediaType;
                        if (type == MediaType.GenericProductBranding1280x800)
                            type = MediaType.ProductBranding1280x800;
                        else if (type == MediaType.GenericProductButton1280x800)
                            type = MediaType.ProductButton1280x800;

                        mediaEntity.GenericproductId = null;
                        mediaEntity.ProductId = productEntity.ProductId;
                        mediaEntity.MediaType = (int)type;
                        mediaEntity.Save();
                    }

                    // Relink existing products to new brand product
                    foreach (ProductEntity linkedProductEntity in genericproductEntity.ProductCollection.ToList())
                    {
                        if (!linkedProductEntity.BrandProductId.HasValue)
                        {
                            linkedProductEntity.GenericproductId = null;
                            linkedProductEntity.IsLinkedToGeneric = false;
                            linkedProductEntity.BrandProductId = productEntity.ProductId;
                            linkedProductEntity.Save();
                        }
                    }

                    genericproductEntity.Delete();
                }
            }

            this.AddInformator(InformatorType.Information, "Converted {0} generic products to brand products", genericproductCollection.Count);
        }

        private void SetUtcFields()
        {
            Dictionary<int, TimeZoneInfo> companiesWithTimeZone = this.GetCompaniesWithTimeZoneOlsonId();

            // Date & DateTime alterations
            AlterationCollection alterations = this.GetAlterationsWithDateTimeTypes();
            foreach (AlterationEntity alteration in alterations)
            {
                if (alteration.StartTime == null || alteration.EndTime == null || !alteration.CompanyId.HasValue || !companiesWithTimeZone.ContainsKey(alteration.CompanyId.Value))
                {
                    continue;
                }
                alteration.StartTimeUTC = alteration.StartTime.Value.ToTimeZone(companiesWithTimeZone[alteration.CompanyId.Value], TimeZoneInfo.Utc);
                alteration.EndTimeUTC = alteration.EndTime.Value.ToTimeZone(companiesWithTimeZone[alteration.CompanyId.Value], TimeZoneInfo.Utc);
                alteration.Save();
            }

            // Price schedule occurrences
            foreach (PriceScheduleItemOccurrenceEntity priceScheduleItemOccurrenceEntity in this.GetAllPriceScheduleItemOccurrences())
            {
                if (!companiesWithTimeZone.ContainsKey(priceScheduleItemOccurrenceEntity.ParentCompanyId.Value))
                {
                    continue;
                }

                TimeZoneInfo timeZoneInfo = companiesWithTimeZone[priceScheduleItemOccurrenceEntity.ParentCompanyId.Value];

                if (priceScheduleItemOccurrenceEntity.StartTime.HasValue && !priceScheduleItemOccurrenceEntity.StartTimeUTC.HasValue)
                {
                    priceScheduleItemOccurrenceEntity.StartTimeUTC = priceScheduleItemOccurrenceEntity.StartTime.LocalTimeToUtc(timeZoneInfo);
                }

                if (priceScheduleItemOccurrenceEntity.EndTime.HasValue && !priceScheduleItemOccurrenceEntity.EndTimeUTC.HasValue)
                {
                    priceScheduleItemOccurrenceEntity.EndTimeUTC = priceScheduleItemOccurrenceEntity.EndTime.LocalTimeToUtc(timeZoneInfo);
                }

                if (priceScheduleItemOccurrenceEntity.RecurrenceStart.HasValue && !priceScheduleItemOccurrenceEntity.RecurrenceStartUTC.HasValue)
                {
                    priceScheduleItemOccurrenceEntity.RecurrenceStartUTC = priceScheduleItemOccurrenceEntity.RecurrenceStart.LocalTimeToUtc(timeZoneInfo);
                }

                if (priceScheduleItemOccurrenceEntity.RecurrenceEnd.HasValue && !priceScheduleItemOccurrenceEntity.RecurrenceEndUTC.HasValue)
                {
                    priceScheduleItemOccurrenceEntity.RecurrenceEndUTC = priceScheduleItemOccurrenceEntity.RecurrenceEnd.LocalTimeToUtc(timeZoneInfo);
                }

                priceScheduleItemOccurrenceEntity.Save();
            }

            // Widget timers
            foreach (UIWidgetTimerEntity uiWidgetTimerEntity in this.GetUIWidgetTimerCollection())
            {
                if (!companiesWithTimeZone.ContainsKey(uiWidgetTimerEntity.ParentCompanyId.Value))
                {
                    continue;
                }

                TimeZoneInfo timeZoneInfo = companiesWithTimeZone[uiWidgetTimerEntity.ParentCompanyId.Value];

                uiWidgetTimerEntity.CountToTimeUTC = uiWidgetTimerEntity.CountToTime.LocalTimeToUtc(CmsSessionHelper.CompanyTimeZone);
                uiWidgetTimerEntity.CountToDateUTC = uiWidgetTimerEntity.CountToDate.LocalTimeToUtc(CmsSessionHelper.CompanyTimeZone);
                uiWidgetTimerEntity.Save();
            }
        }

        private UIWidgetTimerCollection GetUIWidgetTimerCollection()
        {
            UIWidgetTimerCollection uiWidgetTimerCollection = new UIWidgetTimerCollection();
            uiWidgetTimerCollection.GetMulti(null);

            return uiWidgetTimerCollection;
        }

        private AlterationCollection GetAlterationsWithDateTimeTypes()
        {
            PredicateExpression filter = new PredicateExpression();
            filter.AddWithOr(AlterationFields.Type == AlterationType.Date);
            filter.AddWithOr(AlterationFields.Type == AlterationType.DateTime);

            AlterationCollection alterationCollection = new AlterationCollection();
            alterationCollection.GetMulti(filter);

            return alterationCollection;
        }

        private void SetUtcFieldsOnOccurrenceEntities()
        {
            Dictionary<int, TimeZoneInfo> companiesWithTimeZone = this.GetCompaniesWithTimeZoneOlsonId();

            foreach (UIScheduleItemOccurrenceEntity uiScheduleItemOccurrenceEntity in this.GetAllUIScheduleItemOccurrences())
            {
                if (!companiesWithTimeZone.ContainsKey(uiScheduleItemOccurrenceEntity.ParentCompanyId.Value))
                {
                    continue;
                }

                TimeZoneInfo timeZoneInfo = companiesWithTimeZone[uiScheduleItemOccurrenceEntity.ParentCompanyId.Value];

                if (uiScheduleItemOccurrenceEntity.StartTime.HasValue && !uiScheduleItemOccurrenceEntity.StartTimeUTC.HasValue)
                {
                    uiScheduleItemOccurrenceEntity.StartTimeUTC = uiScheduleItemOccurrenceEntity.StartTime.LocalTimeToUtc(timeZoneInfo);
                }

                if (uiScheduleItemOccurrenceEntity.EndTime.HasValue && !uiScheduleItemOccurrenceEntity.EndTimeUTC.HasValue)
                {
                    uiScheduleItemOccurrenceEntity.EndTimeUTC = uiScheduleItemOccurrenceEntity.EndTime.LocalTimeToUtc(timeZoneInfo);
                }

                if (uiScheduleItemOccurrenceEntity.RecurrenceStart.HasValue && !uiScheduleItemOccurrenceEntity.RecurrenceStartUTC.HasValue)
                {
                    uiScheduleItemOccurrenceEntity.RecurrenceStartUTC = uiScheduleItemOccurrenceEntity.RecurrenceStart.LocalTimeToUtc(timeZoneInfo);
                }

                if (uiScheduleItemOccurrenceEntity.RecurrenceEnd.HasValue && !uiScheduleItemOccurrenceEntity.RecurrenceEndUTC.HasValue)
                {
                    uiScheduleItemOccurrenceEntity.RecurrenceEndUTC = uiScheduleItemOccurrenceEntity.RecurrenceEnd.LocalTimeToUtc(timeZoneInfo);
                }

                uiScheduleItemOccurrenceEntity.Save();
            }
        }

        private Dictionary<int, TimeZoneInfo> GetCompaniesWithTimeZoneOlsonId()
        {
            PredicateExpression filter = new PredicateExpression(CompanyFields.TimeZoneOlsonId != DBNull.Value);

            CompanyCollection companyCollection = new CompanyCollection();
            companyCollection.GetMulti(filter);
            
            return companyCollection.ToDictionary(x => x.CompanyId, x => TimeZoneInfo.FindSystemTimeZoneById(Obymobi.TimeZone.Mappings[x.TimeZoneOlsonId].WindowsTimeZoneId));
        }

        private UIScheduleItemOccurrenceCollection GetAllUIScheduleItemOccurrences()
        {
            UIScheduleItemOccurrenceCollection uiScheduleItemOccurrenceCollection = new UIScheduleItemOccurrenceCollection();
            uiScheduleItemOccurrenceCollection.GetMulti(null);

            return uiScheduleItemOccurrenceCollection;
        }

        private PriceScheduleItemOccurrenceCollection GetAllPriceScheduleItemOccurrences()
        {
            PriceScheduleItemOccurrenceCollection priceScheduleItemOccurrenceCollection = new PriceScheduleItemOccurrenceCollection();
            priceScheduleItemOccurrenceCollection.GetMulti(null);

            return priceScheduleItemOccurrenceCollection;
        }

        private void InitializeOutletOperationalStates()
        {
            TimeSpan defaultWaitTime = TimeSpan.FromMinutes(60);

            PredicateExpression outletFilter = new PredicateExpression();
            outletFilter.Add(OutletFields.OutletOperationalStateId == DBNull.Value);

            OutletCollection outletCollection = new OutletCollection();
            outletCollection.GetMulti(outletFilter);

            foreach (OutletEntity outletEntity in outletCollection)
            {
                OutletOperationalStateEntity operationalState = outletEntity.OutletOperationalStateEntity;
                operationalState.WaitTime = defaultWaitTime;
                operationalState.OrderIntakeDisabled = false;
                
                outletEntity.Save(true);
            }

            this.AddInformator(InformatorType.Information, "Generated operational state for {0} outlets", outletCollection.Count);
        }

        private void GenerateOutletSystemProducts(bool test = false)
        {
            OutletCollection outletCollection = this.GetOutletsWithMissingSystemProducts();

            if (test)
            {
                this.lblGenerateOutletSystemProductsOutput.Text += $"{outletCollection.Where(x => !x.TippingProductId.HasValue).Count()} outlets for which to create tipping products <br>";
                this.lblGenerateOutletSystemProductsOutput.Text += $"{outletCollection.Where(x => !x.ServiceChargeProductId.HasValue).Count()} outlets for which to create service charge products <br>";
                this.lblGenerateOutletSystemProductsOutput.Text += $"{outletCollection.Where(x => !x.DeliveryChargeProductId.HasValue).Count()} outlets for which to create delivery charge products";
                return;
            }

            foreach (OutletEntity outletEntity in outletCollection)
            {
                if (!outletEntity.TippingProductId.HasValue)
                {
                    outletEntity.TippingProductEntity = this.CreateSystemTypeProduct("Tipping", outletEntity.CompanyId);
                }

                if (!outletEntity.ServiceChargeProductId.HasValue)
                {
                    outletEntity.ServiceChargeProductEntity = this.CreateSystemTypeProduct("Service charge", outletEntity.CompanyId);
                }

                if (!outletEntity.DeliveryChargeProductId.HasValue)
                {
                    outletEntity.DeliveryChargeProductEntity = this.CreateSystemTypeProduct("Delivery charge", outletEntity.CompanyId);
                }
            }

            outletCollection.SaveMulti(true);
        }

        private void RestoreAllSystemSubTypes(bool test = false)
        {
            ProductCollection productCollection = this.GetMisconfiguredTippingSystemProducts();
            
            if (test)
            {
                this.lblRestoreAllSystemSubTypes.Text = $"{productCollection.Count()} tipping products which aren't configured properly";

                return;
            }

            foreach (ProductEntity productEntity in productCollection)
            {
                productEntity.SubType = (int)ProductSubType.SystemProduct;
            }

            productCollection.SaveMulti();
        }

        private void SetMinimumAmountForFreeServiceCharge(bool test = false)
        {
            ServiceMethodCollection serviceMethodCollection = this.GetServiceMethodsMissingMinimumAmountForFreeServiceCharge();

            if (test)
            {
                this.lblSetMinimumAmountForFreeServiceChargeOutput.Text = $"{serviceMethodCollection.Count} service methods for which to set the MinimumAmountForFreeServiceCharge";
                return;
            }

            foreach (ServiceMethodEntity serviceMethodEntity in serviceMethodCollection)
            {
                serviceMethodEntity.MinimumAmountForFreeServiceCharge = serviceMethodEntity.FreeDeliveryAmount;
            }

            serviceMethodCollection.SaveMulti();
        }

        private void GenerateDeliveryDistanceForDeliveryServiceMethods(bool test = false)
        {
            ServiceMethodCollection serviceMethodCollection = this.GetServiceMethodsWithTypeDelivery();
            ServiceMethodCollection serviceMethodsWithoutDeliveryDistance = serviceMethodCollection.Where(x => !x.DeliveryDistanceCollection.Any()).ToEntityCollection<ServiceMethodCollection>();

            if (test)
            {
                this.lblGenerateDeliveryDistanceForDeliveryServiceMethodsOutput.Text = $"{serviceMethodsWithoutDeliveryDistance.Count} service methods for which to create delivery distance";
                return;
            }

            foreach (ServiceMethodEntity serviceMethodEntity in serviceMethodsWithoutDeliveryDistance)
            {
                serviceMethodEntity.DeliveryDistanceCollection.Add(new DeliveryDistanceEntity
                {
                    MaximumInMetres = 100000,
                    Price = 0
                });
            }

            serviceMethodsWithoutDeliveryDistance.SaveMulti(true);
        }

        private void SetServiceMethodTypes(bool test = false)
        {
            ServiceMethodCollection serviceMethodCollection = this.GetServiceMethodWithFixedOrPercentageServiceCharge();

            if (test)
            {
                this.lblSetServiceMethodTypes.Text = $"{serviceMethodCollection.Count} service methods are going te be reevaluated.";

                return;
            }

            foreach (ServiceMethodEntity serviceMethodEntity in serviceMethodCollection)
            {
                if (serviceMethodEntity.ServiceChargeAmount.GetValueOrDefault(0) > 0)
                {
                    serviceMethodEntity.ServiceChargeType = ChargeType.Fixed;
                }
                else if (serviceMethodEntity.ServiceChargePercentage.GetValueOrDefault(0) > 0)
                {
                    serviceMethodEntity.ServiceChargeType = ChargeType.Percentage;
                }
            }

            serviceMethodCollection.SaveMulti();
        }
        
        private void SetMissingAppLessTranslations()
        {
            this.SetMissingOutletTranslations();
            this.SetMissingServiceMethodTranslations();
            this.SetMissingCheckoutMethodTranslations();
            this.SetMissingApplicationConfigurationTranslations();
        }

        protected void SetTestDeveloperUserFlags(object sender, EventArgs e)
        {
            string[] testers = new[]
            {
                "selina.ahmed",
                "louisa.fitzgerald",
                "stephen.penn",
                "petr.senkyr",
                "Automation",
                "AutoReseller",
                "AutoCustomer",
                "AutoAdmin"
            };

            foreach (string tester in testers)
            {
                UserCollection testersCollection = new UserCollection();
                testersCollection.GetMulti(new PredicateExpression(UserFields.Username.Like(tester)));

                foreach (UserEntity userEntity in testersCollection)
                {
                    userEntity.IsTester = true;
                    userEntity.Save();
                }
            }

            string[] developers = new[]
            {
                "floris.otto",
                "danny.kort",
                "robin.bijdeleij",
                "mathieu.bruning",
                "gabriel.vdkruijk",
                "matthew.vandertoornvrijthoff",
                "joost.vvelthoven",
                "robertjan.kuijvenhoven",
                "guido.devries"
            };

            foreach (string developer in developers)
            {
                UserCollection testersCollection = new UserCollection();
                testersCollection.GetMulti(new PredicateExpression(UserFields.Username.Like(developer)));

                foreach (UserEntity userEntity in testersCollection)
                {
                    userEntity.IsDeveloper = true;
                    userEntity.Save();
                }
            }
        }

        private void SetMissingOutletTranslations()
        {
            OutletCollection outletCollection = new OutletCollection();

            outletCollection.GetMulti(null);

            foreach (OutletEntity outletEntity in outletCollection)
            {
                TranslationHelper.SetDefaultTranslations(OutletDefaultTranslations.ResourceManager,
                    outletEntity,
                    outletEntity.CompanyEntity.GetDefaultCompanyCulture(),
                    outletEntity.CompanyEntity.GetCompanyCultures());
            }
        }

        private void SetMissingServiceMethodTranslations()
        {
            ServiceMethodCollection serviceMethodCollection = new ServiceMethodCollection();

            serviceMethodCollection.GetMulti(null);

            foreach (ServiceMethodEntity serviceMethodEntity in serviceMethodCollection)
            {
                TranslationHelper.SetDefaultTranslations(ServiceMethodDefaultTranslations.ResourceManager,
                    serviceMethodEntity,
                    serviceMethodEntity.CompanyEntity.GetDefaultCompanyCulture(),
                    serviceMethodEntity.CompanyEntity.GetCompanyCultures());
            }
        }

        private void SetMissingCheckoutMethodTranslations()
        {
            CheckoutMethodCollection checkoutMethodCollection = new CheckoutMethodCollection();

            checkoutMethodCollection.GetMulti(null);

            foreach (CheckoutMethodEntity checkoutMethodEntity in checkoutMethodCollection)
            {
                TranslationHelper.SetDefaultTranslations(ServiceMethodDefaultTranslations.ResourceManager,
                    checkoutMethodEntity,
                    checkoutMethodEntity.CompanyEntity.GetDefaultCompanyCulture(),
                    checkoutMethodEntity.CompanyEntity.GetCompanyCultures());
            }
        }

        private void SetMissingApplicationConfigurationTranslations()
        {
            ApplicationConfigurationCollection applicationConfigurationCollection = new ApplicationConfigurationCollection();

            applicationConfigurationCollection.GetMulti(null);

            foreach (ApplicationConfigurationEntity applicationConfigurationEntity in applicationConfigurationCollection)
            {
                TranslationHelper.SetDefaultTranslations(ServiceMethodDefaultTranslations.ResourceManager,
                    applicationConfigurationEntity,
                    applicationConfigurationEntity.CompanyEntity.GetDefaultCompanyCulture(),
                    applicationConfigurationEntity.CompanyEntity.GetCompanyCultures());
            }
		}

        private void PaymentIntegrationConfigurationConversion(bool test = false)
        {
            PaymentIntegrationConfigurationCollection paymentIntegrationConfigurationCollection = this.GetPaymentIntegrationConfigurationCollection();

            IEnumerable<PaymentIntegrationConfigurationEntity> applePayPaymentIntegrationConfigurations = paymentIntegrationConfigurationCollection
                .Where(pic => !string.IsNullOrWhiteSpace(pic.ApplePayMerchantIdentifier))
                .Where(pic => !pic.AdyenPaymentMethodCollection.Any(apm => apm.Type == AdyenPaymentMethod.ApplePay));

            IEnumerable<PaymentIntegrationConfigurationEntity> googlePayPaymentIntegrationConfigurations = paymentIntegrationConfigurationCollection
                .Where(pic => !string.IsNullOrWhiteSpace(pic.GooglePayMerchantIdentifier))
                .Where(pic => !pic.AdyenPaymentMethodCollection.Any(apm => apm.Type == AdyenPaymentMethod.GooglePay));

            IEnumerable<PaymentIntegrationConfigurationEntity> creditCardPaymentIntegrationConfigurations = paymentIntegrationConfigurationCollection
                .Where(pic => !pic.AdyenPaymentMethodCollection.Any());


            if (test)
            {
                this.lblPaymentIntegrationConfigurationConversion.Text = $@"{creditCardPaymentIntegrationConfigurations.Count()} Credit cards payment integration configurations are going to be converted.<br />
                        {applePayPaymentIntegrationConfigurations.Count()} Apple Pay payment integration configurations are going to be converted.<br />
                        {googlePayPaymentIntegrationConfigurations.Count()} Google Pay payment integration configurations are going to be converted.";

                return;
            }

            this.GenerateAdyenPaymentMethodsAndBrands(creditCardPaymentIntegrationConfigurations, AdyenPaymentMethod.CreditCard);
            this.GenerateAdyenPaymentMethodsAndBrands(applePayPaymentIntegrationConfigurations, AdyenPaymentMethod.ApplePay);
            this.GenerateAdyenPaymentMethodsAndBrands(googlePayPaymentIntegrationConfigurations, AdyenPaymentMethod.GooglePay);
        }

        private void ConvertTermsAndConditionsCustomText(bool test = default)
        {
            Dictionary<int, CustomTextType> mappings = new Dictionary<int, CustomTextType>
            {
                { 41036, CustomTextType.CheckoutMethodTermsAndConditionsRequiredError },
                { 41037, CustomTextType.CheckoutMethodTermsAndConditionsLabel }
            };

            IEnumerable<IGrouping<int, CustomTextEntity>> customTextGroups = this.GetCustomTextEntitiesByType(41036, 41037);

            if (test)
            {
                int records = customTextGroups.SelectMany(customTexts => customTexts).Count();
                this.lblConvertTermsAndConditionsCustomTextResult.Text = $"This method can convert {records} existing records.";

                return;
            }

            using (Transaction transaction = new Transaction(IsolationLevel.ReadUncommitted, nameof(ConvertTermsAndConditionsCustomText)))
            {
                try
                {
                    foreach (IGrouping<int, CustomTextEntity> customTextGroup in customTextGroups)
                    {
                        CustomTextType customTextType = mappings[customTextGroup.Key];

                        foreach(CustomTextEntity customTextEntity in customTextGroup)
                        {
                            OutletEntity outletEntity = customTextEntity?.OutletEntity;
                            if (outletEntity == null || outletEntity.IsNew)
                            {
                                continue;
                            }

                            foreach (CheckoutMethodEntity checkoutMethodEntity in outletEntity.CheckoutMethodCollection)
                            {
                                if (((int)customTextEntity.Type) == 41037 && customTextEntity.CultureCode.Equals(outletEntity.CompanyEntity.CultureCode, StringComparison.InvariantCultureIgnoreCase))
                                {
                                    transaction.Add(checkoutMethodEntity);

                                    checkoutMethodEntity.TermsAndConditionsLabel = customTextEntity.Text;
                                    checkoutMethodEntity.Save();
                                }

                                CustomTextEntity newCustomTextEntity = new CustomTextEntity()
                                {
                                    ParentCompanyId = customTextEntity.ParentCompanyId,
                                    CultureCode = customTextEntity.CultureCode,
                                    Text = customTextEntity.Text,
                                    Type = customTextType,
                                    CheckoutMethodId = checkoutMethodEntity.CheckoutMethodId
                                };

                                transaction.Add(newCustomTextEntity);
                                newCustomTextEntity.Save();
                            }

                            transaction.Add(customTextEntity);
                            customTextEntity.Delete();
                        }
                    }

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();

                    throw ex;
                }
            }
        }

        private void GenerateAdyenPaymentMethodsAndBrands(IEnumerable<PaymentIntegrationConfigurationEntity> paymentIntegrationConfigurationEntities, AdyenPaymentMethod adyenPaymentMethod,
            params AdyenPaymentMethodBrand[] adyenPaymentMethodBrands)
        {
            if (adyenPaymentMethodBrands.Length == 0)
            {
                adyenPaymentMethodBrands = new AdyenPaymentMethodBrand[] { AdyenPaymentMethodBrand.MasterCard, AdyenPaymentMethodBrand.Visa };
            }

            foreach (PaymentIntegrationConfigurationEntity paymentIntegrationConfigurationEntity in paymentIntegrationConfigurationEntities)
            {
                AdyenPaymentMethodEntity adyenPaymentMethodEntity = new AdyenPaymentMethodEntity()
                {
                    ParentCompanyId = paymentIntegrationConfigurationEntity.CompanyId,
                    Type = adyenPaymentMethod,
                    Active = true,
                    PaymentIntegrationConfigurationId = paymentIntegrationConfigurationEntity.PaymentIntegrationConfigurationId
                };

                adyenPaymentMethodEntity.Save();

                this.GenerateAdyenPaymentMethodBrands(adyenPaymentMethodEntity, adyenPaymentMethodBrands);
            }
        }

        private void GenerateAdyenPaymentMethodBrands(AdyenPaymentMethodEntity adyenPaymentMethodEntity, params AdyenPaymentMethodBrand[] adyenPaymentMethodBrands)
        {
            foreach (AdyenPaymentMethodBrand adyenPaymentMethodBrand in adyenPaymentMethodBrands)
            {
                AdyenPaymentMethodBrandEntity adyenPaymentMethodBrandEntity = new AdyenPaymentMethodBrandEntity
                {
                    Brand = adyenPaymentMethodBrand,
                    Active = true,
                    ParentCompanyId = adyenPaymentMethodEntity.ParentCompanyId,
                    AdyenPaymentMethodId = adyenPaymentMethodEntity.AdyenPaymentMethodId
                };

                adyenPaymentMethodBrandEntity.Save();
            }
        }

        private PaymentIntegrationConfigurationCollection GetPaymentIntegrationConfigurationCollection()
        {
            PrefetchPath prefetch = new PrefetchPath(EntityType.PaymentIntegrationConfigurationEntity);
            prefetch.Add(PaymentIntegrationConfigurationEntityBase.PrefetchPathAdyenPaymentMethodCollection)
                .SubPath.Add(AdyenPaymentMethodEntityBase.PrefetchPathAdyenPaymentMethodBrandCollection);

            PaymentIntegrationConfigurationCollection paymentIntegrationConfigurationCollection = new PaymentIntegrationConfigurationCollection();
            paymentIntegrationConfigurationCollection.GetMulti(null, prefetch);

            return paymentIntegrationConfigurationCollection;
        }

        private IEnumerable<IGrouping<int, CustomTextEntity>> GetCustomTextEntitiesByType(params int[] types)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(new FieldCompareRangePredicate(CustomTextFields.Type, types));

            PrefetchPath prefetch = new PrefetchPath(EntityType.CustomTextEntity);
            IPrefetchPathElement prefetchOutlet = prefetch.Add(CustomTextEntity.PrefetchPathOutletEntity);
            prefetchOutlet.SubPath.Add(OutletEntity.PrefetchPathCheckoutMethodCollection);
            prefetchOutlet.SubPath.Add(OutletEntity.PrefetchPathCompanyEntity, new IncludeFieldsList(CompanyFields.CultureCode));

            CustomTextCollection customTextCollection = new CustomTextCollection();
            customTextCollection.GetMulti(filter, prefetch);

            return customTextCollection.GroupBy(customText => (int)customText.Type);
        }

        private ServiceMethodCollection GetServiceMethodWithFixedOrPercentageServiceCharge()
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ServiceMethodFields.ServiceChargeAmount > 0);
            filter.AddWithOr(ServiceMethodFields.ServiceChargePercentage > 0);

            ServiceMethodCollection serviceMethodCollection = new ServiceMethodCollection();
            serviceMethodCollection.GetMulti(filter);

            return serviceMethodCollection;
        }

        private OutletCollection GetOutletsWithMissingSystemProducts()
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(OutletFields.TippingProductId == DBNull.Value);
            filter.AddWithOr(OutletFields.ServiceChargeProductId == DBNull.Value);
            filter.AddWithOr(OutletFields.DeliveryChargeProductId == DBNull.Value);

            OutletCollection outletCollection = new OutletCollection();
            outletCollection.GetMulti(filter);

            return outletCollection;
        }

        private ProductCollection GetMisconfiguredTippingSystemProducts()
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ProductFields.SubType != ProductSubType.SystemProduct);

            RelationCollection relation = new RelationCollection();
            relation.Add(OutletEntityBase.Relations.ProductEntityUsingTippingProductId);

            ProductCollection productCollection = new ProductCollection();
            productCollection.GetMulti(filter, relation);

            return productCollection;
        }

        private ServiceMethodCollection GetServiceMethodsMissingMinimumAmountForFreeServiceCharge()
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ServiceMethodFields.FreeDeliveryAmount > 0);
            filter.Add(ServiceMethodFields.MinimumAmountForFreeServiceCharge <= 0);

            ServiceMethodCollection serviceMethodCollection = new ServiceMethodCollection();
            serviceMethodCollection.GetMulti(filter);

            return serviceMethodCollection;
        }

        private ServiceMethodCollection GetServiceMethodsWithTypeDelivery()
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ServiceMethodFields.Type == ServiceMethodType.Delivery);

            PrefetchPath prefetch = new PrefetchPath(EntityType.ServiceMethodEntity);
            prefetch.Add(ServiceMethodEntity.PrefetchPathDeliveryDistanceCollection);

            ServiceMethodCollection serviceMethodCollection = new ServiceMethodCollection();
            serviceMethodCollection.GetMulti(filter, prefetch);

            return serviceMethodCollection;
        }

        private ProductEntity CreateSystemTypeProduct(string name, int companyId)
        {
            return new ProductEntity
            {
                Name = name,
                CompanyId = companyId,
                SubType = (int)ProductSubType.SystemProduct,
                VattariffId =  3
            };
        }

        #endregion


        private void BtnSetOverrideSubTypeOnProductsOnClick(object sender, EventArgs eventArgs)
        {
            ProductEntity product = new ProductEntity();
            product.OverrideSubType = true;

            PredicateExpression filter = new PredicateExpression();
            filter.Add(ProductFields.GenericproductId > 0);
            filter.Add(ProductFields.IsLinkedToGeneric == false);

            ProductCollection productCollection = new ProductCollection();
            productCollection.UpdateMulti(product, filter);
        }

        private void BtnFillReceiptTemplateEmailOnClick(object sender, EventArgs e)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ReceiptTemplateFields.SellerContactEmail == DBNull.Value);
            filter.AddWithOr(ReceiptTemplateFields.SellerContactEmail == string.Empty);

            PrefetchPath prefetch = new PrefetchPath(EntityType.ReceiptTemplateEntity);
            prefetch.Add(ReceiptTemplateEntityBase.PrefetchPathCompanyEntity, new IncludeFieldsList(CompanyFields.Name));

            ReceiptTemplateCollection templates = new ReceiptTemplateCollection();
            templates.GetMulti(filter, prefetch);

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Manual action required for the following templates:");
            foreach (ReceiptTemplateEntity templateEntity in templates)
            {
                string emailReceiver = templateEntity.Email.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                                                .Select(x => x.Trim())
                                                .Where(y => !y.IsNullOrWhiteSpace())
                                                .FirstOrDefault();

                if (emailReceiver.IsNullOrWhiteSpace())
                {
                    sb.AppendFormatLine("- {0} ({1})", templateEntity.Name, templateEntity.CompanyEntity.Name);
                    continue;
                }

                templateEntity.SellerContactEmail = emailReceiver;
                templateEntity.Save();
            }

            this.lblReceiptTemplateNoEmail.Text = sb.ToString().ReplaceLineBreakWithBR();
        }

        protected void btnUsersWithSimplePasswords_OnClick(object sender, EventArgs e)
        {
            List<string> passwordAdditions = new List<string> {"123", "1234", "12345", "test"};
            List<string> passwordMain = new List<string> { "test", "tester", "testing", "reseller", "customer", "crave", "ads", "asd", "asdf", "qawsed", "demo" };
            
            List<string> passwordList = new List<string>();
            foreach (string s in passwordMain)
            {
                passwordList.Add(s);

                foreach (string passwordAddition in passwordAdditions)
                {
                    passwordList.Add(passwordAddition + s);
                    passwordList.Add(s + passwordAddition);
                }
            }

            List<string> passwordsToCheck = passwordList.Distinct().ToList();

            UserCollection users = new UserCollection();
            users.GetMulti(null);

            List<string> usersWithSimplePassword = new List<string>();
            foreach (UserEntity user in users)
            {
                foreach (string s in passwordsToCheck)
                {
                    string encodedPassword = Dionysos.Web.Security.UserManager.Instance.EncodePassword(s, user);

                    if (encodedPassword.Equals(user.Password))
                    {
                        usersWithSimplePassword.Add(user.Username);
                    }
                }
            }

            this.lblUserOutput.Text = String.Join("<br/>", usersWithSimplePassword);
        }

        protected void btnCopyReceiptTemplateToOutletSeller_OnClick(object sender, EventArgs e)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(OutletFields.OutletSellerInformationId == DBNull.Value);

            PrefetchPath prefetch = new PrefetchPath(EntityType.OutletEntity);
            prefetch.Add(OutletEntity.PrefetchPathCheckoutMethodCollection).SubPath
                .Add(CheckoutMethodEntity.PrefetchPathReceiptTemplateEntity);
            prefetch.Add(OutletEntity.PrefetchPathCompanyEntity).SubPath
                .Add(CompanyEntity.PrefetchPathRouteCollection)
                .SubPath.Add(RouteEntity.PrefetchPathRoutestepCollection)
                .SubPath.Add(RoutestepEntity.PrefetchPathRoutestephandlerCollection);

            OutletCollection outletCollection = new OutletCollection();
            outletCollection.GetMulti(filter, prefetch);

            int saveCount = 0;
            foreach (OutletEntity outletEntity in outletCollection)
            {
                CheckoutMethodEntity checkoutMethod = outletEntity.CheckoutMethodCollection.FirstOrDefault(x => x.ReceiptTemplateId.HasValue);
                if (checkoutMethod == null) continue;
                
                RouteCollection routes = outletEntity.CompanyEntity.RouteCollection;
                RoutestephandlerEntity routestephandlerEntity = routes.SelectMany(r => r.RoutestepCollection)
                    .SelectMany(rs => rs.RoutestephandlerCollection)
                    .FirstOrDefault(rsh => rsh.HandlerType == 1000); // Send Receipt

                ReceiptTemplateEntity receiptTemplate = checkoutMethod.ReceiptTemplateEntity;

                OutletSellerInformationEntity sellerInfo = new OutletSellerInformationEntity();
                sellerInfo.SellerName = receiptTemplate.SellerName;
                sellerInfo.Address = receiptTemplate.SellerAddress;
                sellerInfo.Phonenumber = receiptTemplate.SellerContactInfo;
                sellerInfo.Email = receiptTemplate.SellerContactEmail;
                sellerInfo.VatNumber = receiptTemplate.VatNumber;
                sellerInfo.ReceiptReceiversEmail = receiptTemplate.Email;
                sellerInfo.ReceiptReceiversSms = receiptTemplate.Phonenumber;
                sellerInfo.CreatedUTC = DateTime.UtcNow;

                sellerInfo.ReceiveOrderReceiptNotifications = routestephandlerEntity != null && routestephandlerEntity.FieldValue1.Equals("1");
                
                outletEntity.OutletSellerInformationEntity = sellerInfo;
                outletEntity.Save(true);
                
                saveCount++;
            }

            this.AddInformator(InformatorType.Information, "Migrated {0} outlets", saveCount);
            this.Validate();
        }

        protected void btnConvertSendReceipt_OnClick(object sender, EventArgs e)
        {
            ConvertSendReceiptRoutestephandler(false);
        }

        protected void btnConvertSendReceiptTest_OnClick(object sender, EventArgs e)
        {
            ConvertSendReceiptRoutestephandler(true);
        }

        private void ConvertSendReceiptRoutestephandler(bool test)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(RoutestephandlerFields.HandlerType == 1000); // Send Receipt

            RelationCollection relation = new RelationCollection();
            relation.Add(RoutestepEntity.Relations.RoutestephandlerEntityUsingRoutestepId);

            PrefetchPath prefetch = new PrefetchPath(EntityType.RoutestepEntity);
            prefetch.Add(RoutestepEntity.PrefetchPathRoutestephandlerCollection);
            prefetch.Add(RoutestepEntity.PrefetchPathRouteEntity).SubPath.Add(RouteEntity.PrefetchPathCompanyEntity).SubPath.Add(CompanyEntity.PrefetchPathOutletCollection);

            RoutestepCollection routestepCollection = new RoutestepCollection();
            routestepCollection.GetMulti(filter, 0, null, relation, prefetch);

            StringBuilder sb = new StringBuilder();
            sb.Append("Result:<br/>");
            foreach (RoutestepEntity entity in routestepCollection)
            {
                RoutestephandlerEntity rshEntity = entity.RoutestephandlerCollection.FirstOrDefault(x => x.HandlerType == 1000);
                if (rshEntity != null)
                {
                    OutletEntity outletEntity = entity.RouteEntity.CompanyEntity.OutletCollection.FirstOrDefault();
                    string toEmailAddress = "";
                    if (outletEntity == null || outletEntity.OutletSellerInformationEntity.Email.IsNullOrWhiteSpace())
                    {
                        sb.AppendFormat("Routestep: {0}, Routestephandler: {1}, Company: {2} - Could not find outlet or has no Email configured, manual email input needed!<br/>", entity.RouteNameAndNumber, rshEntity.RoutestephandlerId, entity.ParentCompanyId);
                    }
                    else
                    {
                        toEmailAddress = outletEntity.OutletSellerInformationEntity.Email;
                        sb.AppendFormat("Routestep: {0}, Routestephandler: {1}, Company: {2} - OK! (Mail: {3})<br/>", entity.RouteNameAndNumber, rshEntity.RoutestephandlerId, entity.ParentCompanyId, toEmailAddress);
                    }
                    
                    if (!test)
                    {
                        rshEntity.HandlerTypeAsEnum = RoutestephandlerType.EmailOrder;
                        rshEntity.FieldValue1 = toEmailAddress; // To e-mail
                        rshEntity.FieldValue2 = "noreply@crave-emenu.com"; // From e-mail
                        rshEntity.FieldValue3 = null; // Title
                        rshEntity.FieldValue4 = null; // Message
                        rshEntity.FieldValue5 = "0"; // Don't use default template (false)
                        rshEntity.FieldValue6 = "0"; // To customer (false)

                        rshEntity.Validator = null; // Disable validator
                        rshEntity.Save();
                    }
                }
            }

            lblConvertSendReceiptResult.Text = sb.ToString();
        }

        protected void btnConvertEmailOrderTempalteVars_OnClick(object sender, EventArgs e)
        {
            Dictionary<string, string> templateVariableMapping = new Dictionary<string, string>
            {
                ["[[ORDERID]]"] = "[[ORDERNUMBER]]",
                ["[[EMAIL]]"] = "[[CUSTOMEREMAIL]]",
                ["[[PHONENUMBER]]"] = "[[CUSTOMERPHONE]]",
                ["[[LASTNAME]]"] = "[[CUSTOMERLASTNAME]]"
            };

            PredicateExpression filter = new PredicateExpression();
            filter.Add(RoutestephandlerFields.HandlerType == RoutestephandlerType.EmailOrder);
            filter.AddWithOr(RoutestephandlerFields.HandlerType == RoutestephandlerType.SMS);

            RoutestephandlerCollection routestephandlerCollection = new RoutestephandlerCollection();
            routestephandlerCollection.GetMulti(filter);

            StringBuilder sb = new StringBuilder();
            foreach (RoutestephandlerEntity routestephandlerEntity in routestephandlerCollection)
            {
                if (string.IsNullOrWhiteSpace(routestephandlerEntity.FieldValue4))
                {
                    continue;
                }

                foreach (KeyValuePair<string, string> templatePair in templateVariableMapping)
                {
                    routestephandlerEntity.FieldValue4 = routestephandlerEntity.FieldValue4.Replace(templatePair.Key, templatePair.Value);
                }

                if (routestephandlerEntity.IsDirty)
                {
                    sb.AppendFormat("* Routestephandler: {0}<br>", routestephandlerEntity.RoutestephandlerId);
                    routestephandlerEntity.Save();
                }
            }

            this.lblConvertEmailOrderTemplateReslut.Text = sb.ToString();
        }

        protected void btnConvertReportTemplateFilters_OnClick(object sender, EventArgs e)
        {
            PredicateExpression filter = new PredicateExpression(ReportProcessingTaskTemplateFields.ReportType == (int)AnalyticsReportType.TransactionsAppLess);

            ReportProcessingTaskTemplateCollection templateCollection = new ReportProcessingTaskTemplateCollection();
            templateCollection.GetMulti(filter);

            int count = 0;
            foreach (ReportProcessingTaskTemplateEntity templateEntity in templateCollection)
            {
                if (templateEntity.Filter.IsNullOrWhiteSpace())
                {
                    continue;
                }

                TransactionsAppLessRequest request = JsonConvert.DeserializeObject<TransactionsAppLessRequest>(templateEntity.Filter);
                if (request.SheetFilters.Count == 0)
                {
                    request.SheetFilters.Add(new TransactionsAppLessSheetFilter
                    {
                        Id = 0,
                        Name = "Report",
                        SortOrder = 1,
                        Active = true,
                        OutletId = request.OutletId,
                        ServiceMethods = request.ServiceMethods,
                        CheckoutMethods = request.CheckoutMethods,
                        ShowProducts = request.ShowProducts,
                        ShowProductQuantity = request.ShowProductQuantity,
                        ShowProductCategory = request.ShowProductCategory,
                        ShowProductPrice = request.ShowProductPrice,
                        ShowAlterations = request.ShowAlterations,
                        ShowOrderNotes = request.ShowOrderNotes,
                        ShowCustomerInfo = request.ShowCustomerInfo,
                        ShowSystemProducts = request.ShowSystemProducts,
                        IncludedSystemProductTypes = request.IncludedSystemProductTypes,
                        CategoryFilterType = request.CategoryFilterType,
                        CategoryFilterIds = request.CategoryFilterIds
                    });

                    templateEntity.Filter = JsonConvert.SerializeObject(request, Formatting.Indented);
                    templateEntity.Save();
                    count++;
                }
            }

            this.AddInformator(InformatorType.Information, "Converted {0} report templates", count);
        }

        protected void btnConvertNonConfiguredTaxTariffsForSysProducts_OnClick(object sender, EventArgs e)
        {
            PredicateExpression filter = new PredicateExpression(ProductFields.SubType == (int)ProductSubType.SystemProduct);
            filter.AddWithAnd(ProductFields.TaxTariffId == DBNull.Value);

            ProductCollection productCollection = new ProductCollection();
            productCollection.GetMulti(filter);

            (int createdTaxTariffs, IEnumerable<TaxTariffEntity> taxTariffs) = GetOrCreateZeroPercentageTaxTariffs(productCollection);
            int updatedProducts = UpdateTaxTariffForNonConfiguredProducts(productCollection, taxTariffs);
            
            this.lblConvertNonConfiguredTaxTariffsForSysProducts.Text = $@"{createdTaxTariffs} tax tariffs have been created.
                {Environment.NewLine}
                {updatedProducts} products have been updated.";

            return;
        }

        private ValueTuple<int, IEnumerable<TaxTariffEntity>> GetOrCreateZeroPercentageTaxTariffs(ProductCollection productCollection)
        {
            int createdTaxTariffRecords = 0;
            List<TaxTariffEntity> taxTariffs = new List<TaxTariffEntity>();

            using (Transaction transaction = new Transaction(IsolationLevel.ReadCommitted, nameof(btnConvertNonConfiguredTaxTariffsForSysProducts_OnClick)))
            {
                foreach (IGrouping<int, ProductEntity> groupedProducts in productCollection.Where(productEntity => productEntity.CompanyId.HasValue)
                    .GroupBy(productEntity => productEntity.CompanyId.Value))
                {
                    PredicateExpression taxTariffFilter = new PredicateExpression(TaxTariffFields.CompanyId == groupedProducts.Key);

                    TaxTariffCollection taxTariffCollection = new TaxTariffCollection();
                    taxTariffCollection.GetMulti(taxTariffFilter);

                    TaxTariffEntity taxTariffEntity = taxTariffCollection.FirstOrDefault(taxTariff => taxTariff.Percentage == 0);
                    if (taxTariffEntity == null)
                    {
                        taxTariffEntity = new TaxTariffEntity();
                        taxTariffEntity.AddToTransaction(transaction);
                        taxTariffEntity.CompanyId = groupedProducts.Key;
                        taxTariffEntity.Name = "Tax free";
                        taxTariffEntity.Percentage = 0;
                        taxTariffEntity.Save();

                        ++createdTaxTariffRecords;
                    }

                    taxTariffs.Add(taxTariffEntity);
                }

                try
                {
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    createdTaxTariffRecords = 0;
                    transaction.Rollback();

                    throw ex;
                }
            }

            return (createdTaxTariffRecords, taxTariffs);
        }

        private int UpdateTaxTariffForNonConfiguredProducts(ProductCollection productCollection, IEnumerable<TaxTariffEntity> taxTariffs)
        {
            int updatedProducts = 0;

            using (Transaction transaction = new Transaction(IsolationLevel.ReadCommitted, nameof(btnConvertNonConfiguredTaxTariffsForSysProducts_OnClick)))
            {
                foreach (ProductEntity productEntity in productCollection)
                {
                    int taxTariffId = taxTariffs.FirstOrDefault(taxTariffEntity => taxTariffEntity.CompanyId == productEntity.CompanyId).TaxTariffId;

                    productEntity.AddToTransaction(transaction);
                    productEntity.TaxTariffId = taxTariffId;
                    productEntity.Save();

                    ++updatedProducts;
                }

                try
                {
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    updatedProducts = 0;
                    transaction.Rollback();

                    throw ex;
                }
            }

            return updatedProducts;
        }

        protected void btnCreateZeroTaxTariffNonAppLess_OnClick(object sender, EventArgs e)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ApplicationConfigurationFields.ApplicationConfigurationId == DBNull.Value);
            filter.Add(TaxTariffFields.TaxTariffId == DBNull.Value);

            RelationCollection relations = new RelationCollection();
            relations.Add(CompanyEntityBase.Relations.ApplicationConfigurationEntityUsingCompanyId, JoinHint.Left);
            relations.Add(CompanyEntityBase.Relations.TaxTariffEntityUsingCompanyId, JoinHint.Left);

            CompanyCollection companyCollection = new CompanyCollection();
            companyCollection.GetMulti(filter, relations);

            foreach (CompanyEntity companyEntity in companyCollection)
            {
                TaxTariffEntity taxTariffEntity = new TaxTariffEntity();
                taxTariffEntity.CompanyId = companyEntity.CompanyId;
                taxTariffEntity.Name = "Tax free";
                taxTariffEntity.Percentage = 0.0;

                taxTariffEntity.Save();
            }

            lblCreateZeroTaxTariffNonAppLess.Text = $"Zero % tax tariff has been created for {companyCollection.Count} companies";
        }

        protected void btnCreateFreeOfChargeCheckoutMethod_OnClick(object sender, EventArgs e)
        {
            PrefetchPath prefetch = new PrefetchPath(EntityType.OutletEntity);
            prefetch.Add(OutletEntity.PrefetchPathCheckoutMethodCollection);

            OutletCollection outletCollection = new OutletCollection();
            outletCollection.GetMulti(null, prefetch);

            int numberOfCheckoutMethodsCreated = 0;

            foreach (OutletEntity outletEntity in outletCollection)
            {
                if (outletEntity.CheckoutMethodCollection.Any(x => x.CheckoutType == CheckoutType.FreeOfCharge))
                {
                    continue;
                }

                CheckoutMethodEntity checkoutMethod = new CheckoutMethodEntity();
                checkoutMethod.Active = true;
                checkoutMethod.Name = "Free Of Charge";
                checkoutMethod.Label = "Free Of Charge";
                checkoutMethod.TermsAndConditionsRequired = false;
                checkoutMethod.CheckoutType = CheckoutType.FreeOfCharge;
                checkoutMethod.CompanyId = outletEntity.CompanyId;
                checkoutMethod.OutletId = outletEntity.OutletId;

                if (checkoutMethod.Save())
                {
                    numberOfCheckoutMethodsCreated++;
                }
            }

            lblCreateFreeOfChargeCheckoutMethod.Text = $"Created {numberOfCheckoutMethodsCreated} free of charge checkout methods";
        }

        protected void btnRelease23RemoveDpgs_OnClick(object sender, EventArgs e)
        {
            RemoveServiceMethodPickupAndDeliveryDpgRelations(false);
        }

        protected void btnRelease23RemoveDpgsTest_OnClick(object sender, EventArgs e)
        {
            RemoveServiceMethodPickupAndDeliveryDpgRelations(true);
        }

        private void RemoveServiceMethodPickupAndDeliveryDpgRelations(bool test)
        {
            RelationCollection relations = new RelationCollection();
            relations.Add(ServiceMethodEntity.Relations.ServiceMethodDeliverypointgroupEntityUsingServiceMethodId);

            PredicateExpression filter = new PredicateExpression();
            filter.Add(ServiceMethodFields.Type.In(ServiceMethodType.PickUp, ServiceMethodType.Delivery));

            ServiceMethodDeliverypointgroupCollection serviceMethodDeliverypointgroupCollection = new ServiceMethodDeliverypointgroupCollection();
            serviceMethodDeliverypointgroupCollection.GetMulti(filter, 0, null, relations);

            int recordsCount = serviceMethodDeliverypointgroupCollection.Count();
            if (test)
            {
                this.lblRelease23RemoveDpgs.Text = $"{recordsCount} records are going to be updated.";

                return;
            }

            using (Transaction transaction = new Transaction(IsolationLevel.ReadUncommitted, nameof(RemoveServiceMethodPickupAndDeliveryDpgRelations)))
            {
                try
                {
                    foreach (var serviceMethodDeliverypointgroupEntity in serviceMethodDeliverypointgroupCollection)
                    {
                        transaction.Add(serviceMethodDeliverypointgroupEntity);
                        serviceMethodDeliverypointgroupEntity.Delete();
                    }

                    transaction.Commit();
                    this.lblRelease23RemoveDpgs.Text = $"{recordsCount} service method relations are removed.";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();

                    throw ex;
                }
            }
        }

        protected void btnConvertActRoutestephandlers_OnClick(object sender, EventArgs e)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(RoutestephandlerFields.HandlerType == 800); // Email Ack
            filter.AddWithOr(RoutestephandlerFields.HandlerType == 900); // SMS Ack

            RoutestephandlerCollection collection = new RoutestephandlerCollection();
            collection.GetMulti(filter);

            foreach (RoutestephandlerEntity entity in collection)
            {
                if (entity.HandlerType == 800)
                {
                    entity.HandlerTypeAsEnum = RoutestephandlerType.EmailOrder;
                }
                else if (entity.HandlerType == 900)
                {
                    entity.HandlerTypeAsEnum = RoutestephandlerType.SMS;
                }

                entity.FieldValue4 = entity.FieldValue4.Replace("[[ONTHECASELINK]]", string.Empty);
                entity.FieldValue4 = entity.FieldValue4.Replace("[[COMPLETELINK]]", string.Empty);

                entity.Save();
            }
            
            this.MultiValidatorDefault.AddInformation($"Successfully converted {collection.Count} routestephandlers");
            this.Validate();
        }

        protected void btnUpdateOptInsWithMissingCustomerName(object sender, EventArgs e)
        {
            bool isTest = ((Button)sender).ID.Contains("test", StringComparison.InvariantCultureIgnoreCase);

            UpdateOptInsWithMissingCustomerNameUseCase useCase = new UpdateOptInsWithMissingCustomerNameUseCase();
            int numberOfOptInsUpdated = useCase.Execute(isTest);

            if (isTest)
            {
                this.MultiValidatorDefault.AddInformation($"{numberOfOptInsUpdated} opt-ins are going the be updated");
            }
            else
            {
                this.MultiValidatorDefault.AddInformation($"Updated {numberOfOptInsUpdated} opt-ins with customer name");
            }

            this.Validate();
        }

        protected void btnUpdateDevTestCompanies(object sender, EventArgs e)
        {
            int[] developmentCompanyIds = new[] { 405 };
            int[] testCompanyIds = new[] { 222, 443, 550, 595 };

            CompanyEntity devCompany = new CompanyEntity { ReleaseGroup = ReleaseGroup.Dev };
            new CompanyCollection().UpdateMulti(devCompany, new PredicateExpression { CompanyFields.CompanyId.In(developmentCompanyIds) });

            CompanyEntity testCompany = new CompanyEntity { ReleaseGroup = ReleaseGroup.Test };
            new CompanyCollection().UpdateMulti(testCompany, new PredicateExpression { CompanyFields.CompanyId.In(testCompanyIds) });
        }

        protected void UpdateCategoryPath(object sender, EventArgs e)
        {
            bool isTest = ((Button)sender).ID.Contains("test", StringComparison.InvariantCultureIgnoreCase);

            UpdateOrderitemCategoryPathUseCase useCase = new UpdateOrderitemCategoryPathUseCase();
            int numberOfOrderitemsUpdated = useCase.Execute(isTest);

            if (isTest)
            {
                this.MultiValidatorDefault.AddInformation($"The category path of {numberOfOrderitemsUpdated} order items will be updated");
            }
            else
            {
                this.MultiValidatorDefault.AddInformation($"Updated the category path of {numberOfOrderitemsUpdated} order items");
            }

            this.Validate();
        }

        private void DisplayUnprocessableOrderCount()
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(CompanyFields.UseMonitoring == true);
            filter.Add(OrderFields.Status == OrderStatus.Unprocessable);
            filter.Add(OrderFields.CreatedUTC <= DateTime.UtcNow.AddDays(-1));

            RelationCollection relations = new RelationCollection(OrderEntityBase.Relations.CompanyEntityUsingCompanyId);

            int orderCount = new OrderCollection().GetDbCount(filter, relations);
            this.lblUnprocessableOrdersCount.Text = $"{orderCount} Orders";
        }

        protected void btnProcessOrders_OnClick(object sender, EventArgs e)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(CompanyFields.UseMonitoring == true);
            filter.Add(OrderFields.Status == OrderStatus.Unprocessable);
            filter.Add(OrderFields.CreatedUTC <= DateTime.UtcNow.AddDays(-1));

            RelationCollection relations = new RelationCollection(OrderEntityBase.Relations.CompanyEntityUsingCompanyId);

            OrderEntity updatedEntity = new OrderEntity();
            updatedEntity.Status = (int)OrderStatus.Processed;
            updatedEntity.ErrorCode = (int)OrderProcessingError.ManuallyProcessed;
            updatedEntity.ErrorText = "Auto-processed by CMS button";

            OrderCollection collection = new OrderCollection();
            collection.UpdateMulti(updatedEntity, filter, relations);

            // Update count
            DisplayUnprocessableOrderCount();
        }

        private void CleanInactiveClients()
        {
            if (CmsSessionHelper.CurrentRole < Role.GodMode)
            {
                MultiValidatorDefault.AddError("Only god mode users can delete inactive clients.");
                Validate();
                return;
            }
            else if (CmsSessionHelper.CurrentCompanyId <= 0)
            {
                MultiValidatorDefault.AddError("Please select a company before deleting inactive clients");
                Validate();
                return;
            }

            PredicateExpression clientFilter = new PredicateExpression();
            clientFilter.Add(CompanyFields.SystemType == SystemType.Crave); // Only Crave devices
            clientFilter.Add(CustomerFields.ClientId == DBNull.Value); // Only tablet devices
            clientFilter.Add(ClientFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

            PredicateExpression requestFilter = new PredicateExpression();
            requestFilter.Add(DeviceFields.LastRequestUTC <= DateTime.UtcNow.AddDays(-removeAfterDaysOfInactivity));
            requestFilter.AddWithOr(DeviceFields.LastRequestUTC == DBNull.Value);
            clientFilter.Add(requestFilter);

            RelationCollection relations = new RelationCollection();
            relations.Add(ClientEntityBase.Relations.CompanyEntityUsingCompanyId);
            relations.Add(ClientEntityBase.Relations.CustomerEntityUsingClientId, JoinHint.Left);
            relations.Add(ClientEntityBase.Relations.DeviceEntityUsingDeviceId);

            ClientCollection clientCollection = new ClientCollection();
            clientCollection.GetMulti(clientFilter, 0, null, relations);

            RemoveClients(clientCollection);
            plhDeleteInactiveClientsOfCompanyReponse.InsertHtml(clientCollection.Count + " devices deleted");
        }

        private void RemoveClients(ClientCollection clientsToRemove)
        {
            foreach (ClientEntity clientEntity in clientsToRemove)
            {
                Transaction transaction = new Transaction(IsolationLevel.ReadUncommitted, "ClientCleanupTransaction");
                clientEntity.AddToTransaction(transaction);
                
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("ID: {0} | CompanyId: {1}", clientEntity.ClientId, clientEntity.CompanyId);
                sb.AppendFormat(" | DPG: {0} | DP: {1}", clientEntity.DeliverypointGroupId.GetValueOrDefault(-1), clientEntity.DeliverypointId.GetValueOrDefault(-1));

                try
                {
                    clientEntity.Delete();
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    plhDeleteInactiveClientsOfCompanyReponse.InsertHtml("RemoveClients", "FAILED - {0}", sb.ToString());
                    plhDeleteInactiveClientsOfCompanyReponse.InsertHtml("RemoveClients", ex.ProcessStackTrace(true));
                    transaction.Rollback();
                }
                finally
                {
                    transaction.Dispose();
                }
            }
        }

        protected void MigrateAffiliateBookOnline(object sender, EventArgs e)
        {
            Transaction transaction = new Transaction(IsolationLevel.ReadUncommitted, "MigrationTransaction");
            List<int> attachmentIds = new List<int>();

            PredicateExpression filter = new PredicateExpression();
            filter.Add(AttachmentFields.Type == AttachmentType.AffiliateBookOnline);

            RelationCollection relations = new RelationCollection();
            relations.Add(ProductEntityBase.Relations.AttachmentEntityUsingProductId);

            ProductCollection collection = new ProductCollection();
            collection.AddToTransaction(transaction);
            collection.GetMulti(filter, relations);

            foreach (ProductEntity product in collection)
            {
                AttachmentEntity attachment = product.AttachmentCollection.FirstOrDefault(x => x.Type == AttachmentType.AffiliateBookOnline);
                product.SubType = (int)ProductSubType.WebLink;
                product.WebLinkType = WebLinkType.QrCode;
                product.WebTypeTabletUrl = attachment?.Url;
                product.OpenNewWindow = attachment?.OpenNewWindow;
                product.CustomizeButtonText = attachment?.Name;

                if (attachment?.AttachmentId != null)
                { attachmentIds.Add(attachment.AttachmentId); }
            }

            collection.SaveMulti();

            // Update existing Web Link products to have Button as default Web Link Type
            PredicateExpression webLinkFilter = new PredicateExpression();
            webLinkFilter.Add(ProductFields.WebLinkType == DBNull.Value);
            webLinkFilter.Add(ProductFields.SubType == ProductSubType.WebLink);

            ProductCollection webLinkProducts = new ProductCollection();
            webLinkProducts.AddToTransaction(transaction);
            webLinkProducts.GetMulti(webLinkFilter);

            foreach (ProductEntity product in webLinkProducts)
            { product.WebLinkType = WebLinkType.Button; }

            webLinkProducts.SaveMulti();

            // Delete Attachment Custom Texts
            PredicateExpression customTextFilter = new PredicateExpression();
            customTextFilter.Add(CustomTextFields.AttachmentId.In(attachmentIds));

            CustomTextCollection customTextEntities = new CustomTextCollection();
            customTextEntities.AddToTransaction(transaction);
            customTextEntities.DeleteMulti(customTextFilter);

            // Delete Attachments
            AttachmentCollection attachments = new AttachmentCollection();
            attachments.AddToTransaction(transaction);
            attachments.DeleteMulti(filter);

            try
            {
                transaction.Commit();
            }
            catch
            {
                transaction.Rollback();
                throw;
            }
        }

        protected void CreateAffiliatePartners(object sender, EventArgs e)
        {
            Transaction transaction = new Transaction(IsolationLevel.ReadUncommitted, "AffiliateTransaction");

            AffiliatePartnerCollection collection = new AffiliatePartnerCollection();

            List<AffiliatePartnerEntity> partners = new List<AffiliatePartnerEntity>()
            {
                new AffiliatePartnerEntity()
                {
                    Name = "Golden Tours",
                    DomainUrl = "https://allthingslondon.gttickets.com"
                },
                new AffiliatePartnerEntity()
                {
                    Name = "Tiqets",
                    DomainUrl = "https://www.tiqets.com"
                },
                new AffiliatePartnerEntity()
                {
                    Name = "Ingresso",
                    DomainUrl = "https://allthingslondon-{campaign}.ticketswitch.com"
                }
            };

            collection.AddToTransaction(transaction);
            collection.AddRange(partners);
            collection.SaveMulti();

            try
            {
                transaction.Commit();
            }
            catch
            {
                transaction.Rollback();
                throw;
            }
        }
    }
}
