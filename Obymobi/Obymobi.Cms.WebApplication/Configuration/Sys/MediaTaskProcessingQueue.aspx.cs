﻿using System;
using Dionysos.Web.UI;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Configuration.Sys
{
    public partial class MediaTaskProcessingQueue : PageQueryStringDataBinding
    {
        private enum FilterTypes
        {
            FilterRecentAndPending = 1,
            FilterPending = 2,
            FilterTwoOrMoreAttempts = 3,
            FilterLastHour = 4,
            FilterLastFourHours = 5
        }

        #region Fields


        #endregion

        #region Methods

        protected override void OnInit(EventArgs e)
        {            
            this.SetGui();
            base.OnInit(e);
        }

        void SetGui()
        {    
            // Set the filters
            this.ddlType.Items.Add(this.Translate(FilterTypes.FilterRecentAndPending.ToString(), "Recent en te verwerken"), (int)FilterTypes.FilterRecentAndPending);
            this.ddlType.Items.Add(this.Translate(FilterTypes.FilterPending.ToString(), "Te verwerken"), (int)FilterTypes.FilterPending);
            this.ddlType.Items.Add(this.Translate(FilterTypes.FilterTwoOrMoreAttempts.ToString(), "Twee of meer pogingen"), (int)FilterTypes.FilterTwoOrMoreAttempts);
            this.ddlType.Items.Add(this.Translate(FilterTypes.FilterLastHour.ToString(), "Afgelopen uur"), (int)FilterTypes.FilterLastHour);
            this.ddlType.Items.Add(this.Translate(FilterTypes.FilterLastFourHours.ToString(), "Afgelopen 4 uur"), (int)FilterTypes.FilterLastFourHours);
        }

        void HookUpEvents()
        {
            this.btChangeFilter.Click += btChangeFilter_Click;
        }

        void btChangeFilter_Click(object sender, EventArgs e)
        {
            this.RedirectWithControlValues();
        }

        void LoadData()
        {
            // GK -> Todo, make a real relation ship between 
            MediaProcessingTaskCollection tasks = new MediaProcessingTaskCollection();
            PrefetchPath path = new PrefetchPath(EntityType.MediaProcessingTaskEntity);

            // Prefetch the most used entities for Media
            var mediaPath = MediaRatioTypeMediaEntity.PrefetchPathMediaEntity;
            mediaPath.SubPath.Add(MediaEntity.PrefetchPathProductEntity);
            mediaPath.SubPath.Add(MediaEntity.PrefetchPathCompanyEntity);
            mediaPath.SubPath.Add(MediaEntity.PrefetchPathCategoryEntity);
            mediaPath.SubPath.Add(MediaEntity.PrefetchPathDeliverypointgroupEntity);
            mediaPath.SubPath.Add(MediaEntity.PrefetchPathPointOfInterestEntity);
            path.Add(MediaProcessingTaskEntity.PrefetchPathMediaRatioTypeMediaEntity).SubPath.Add(mediaPath);

            FilterTypes filterType = this.ddlType.ValidId.ToEnum<FilterTypes>();

            PredicateExpression filter = new PredicateExpression();
            switch (filterType)
            {
                case FilterTypes.FilterRecentAndPending:
                    filter.Add(MediaProcessingTaskFields.CreatedUTC >= DateTime.UtcNow.AddMinutes(-30));
                    filter.AddWithOr(MediaProcessingTaskFields.Attempts == 0);
                    break;
                case FilterTypes.FilterPending:
                    filter.Add(MediaProcessingTaskFields.Attempts == 0);
                    break;
                case FilterTypes.FilterTwoOrMoreAttempts:
                    filter.Add(MediaProcessingTaskFields.Attempts >= 2);
                    break;
                case FilterTypes.FilterLastHour:
                    filter.Add(MediaProcessingTaskFields.CreatedUTC >= DateTime.UtcNow.AddMinutes(-60));
                    break;
                case FilterTypes.FilterLastFourHours:
                    filter.Add(MediaProcessingTaskFields.CreatedUTC >= DateTime.UtcNow.AddMinutes(-240));
                    break;
                default:
                    break;
            }

            this.taskGrid.SettingsPager.PageSize = this.tbPageSize.Value ?? 30;

            SortExpression sort = new SortExpression();
            sort.Add(MediaProcessingTaskFields.MediaProcessingTaskId | SortOperator.Descending);

            tasks.GetMulti(filter, 0, sort, null, path);

            this.taskGrid.DataSource = tasks;
            this.taskGrid.DataBind();
        }
  
        #endregion

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!this.IsPostBack)
                this.BindQueryStringToControls();
            this.HookUpEvents();
            this.LoadData();
        }
      
        #endregion

        #region Properties
        #endregion


        protected override void SetDefaultValuesToControls()
        {
            this.ddlType.Value = (int)FilterTypes.FilterRecentAndPending;
            this.tbPageSize.Value = 30;
        }
    }
}


