﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using CraveOnsiteAgent.Configuration;
using Dionysos;
using Dionysos.Configuration;
using Dionysos.IO;
using Dionysos.Web.UI;
using Obymobi.Configuration;
using Obymobi.Constants;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Configuration.Sys
{
    public partial class GenerateConfig : PageDefault
    {
        #region Fields

        private const string TAB = "  ";
        private CloudEnvironment cloudEnvironment = CloudEnvironment.ProductionPrimary;
        private string url = string.Empty;

        #endregion

        #region Methods

        protected override void OnInit(EventArgs e)
        {
            this.SetGui();
            base.OnInit(e);
        }

        void SetGui()
        {
            this.ddlEnvironment.Items.Add("Live (https://app.*)", CloudEnvironment.ProductionPrimary.ToString());
            this.ddlEnvironment.Items.Add("Test (http://test.*)", CloudEnvironment.Test.ToString());
            this.ddlEnvironment.Items.Add("Development (http://dev.*)", CloudEnvironment.Development.ToString());
            this.ddlEnvironment.Items.Add("Staging (http://staging.*)", CloudEnvironment.Staging.ToString());
            this.ddlEnvironment.Items.Add("Manual", CloudEnvironment.Manual.ToString());

            if (TestUtil.IsPcDeveloper)
            {
                WebEnvironmentHelper.GetApiUrl();

                //this.ddlEnvironment.Items.Add("Localhost on /api (Manual)", "Manual-api");
                //this.ddlEnvironment.Items.Add("Localhost on /obymobi (Manual)", "Manual-obymobi");
                this.ddlEnvironment.Items.Add("Local: " + WebEnvironmentHelper.GetApiUrl(), "Manual-api");                
            }
        }

        void HookUpEvents()
        {
            this.btGenerateConfigFiles.Click += new EventHandler(btGenerateConfigFiles_Click);
        }

        void GenerateOnsiteServerConfig(string tempFolder, CompanyEntity company)
        {
            RelationCollection relations = new RelationCollection();
            relations.Add(ClientEntity.Relations.CustomerEntityUsingClientId);
            PredicateExpression filter = new PredicateExpression(ClientFields.CompanyId == company.CompanyId);
            ClientCollection clientCollection = new ClientCollection();
            clientCollection.GetMulti(filter, relations);

            foreach (TerminalEntity terminal in company.TerminalCollection)
            {                
                StringBuilder file = new StringBuilder();
                file.AppendLine("<configuration>");
                string currentSection = String.Empty;
                string itemtabs = TAB;

                // Cloud config
                AddCloudConfigSection(file);

                // Oss specific config
                CraveOnSiteServerConfigInfo onsiteServerConfiguration = new CraveOnSiteServerConfigInfo();
                for (int i = 0; i < onsiteServerConfiguration.Count; i++)
                {
                    ConfigurationItem item = onsiteServerConfiguration[i];
                    string section = item.Section;
                    if (!section.Equals(currentSection))
                    {
                        if (!currentSection.Equals(String.Empty))
                            file.AppendLine(TAB + "</section>");

                        file.AppendFormatLine(TAB + "<section name=\"{0}\">", section);

                        currentSection = section;
                        itemtabs = TAB + TAB;
                    }

                    file.AppendFormatLine("{0}<item name=\"{1}\">", itemtabs, item.Name);
                    file.AppendFormatLine("{0}<friendlyname>{1}</friendlyname>", itemtabs + TAB, item.FriendlyName);
                    file.AppendFormatLine("{0}<type>{1}</type>", itemtabs + TAB, item.Type);

                    object value = item.DefaultValue;

                    switch (item.Name)
                    {
                        case CraveOnSiteServerConfigConstants.CompanyOwner:
                            value = company.CompanyOwnerEntity.Username;
                            break;

                        case CraveOnSiteServerConfigConstants.CompanyPassword:
                            if (company.CompanyOwnerEntity.IsPasswordEncrypted)
                                value = company.CompanyOwnerEntity.Password;
                            else
                                value = Dionysos.Security.Cryptography.Cryptographer.EncryptStringUsingRijndael(company.CompanyOwnerEntity.Password);
                            break;

                        case CraveOnSiteServerConfigConstants.TerminalId:
                            value = terminal.TerminalId;
                            break;

                        case CraveOnSiteServerConfigConstants.CompanyId:
                            value = company.CompanyId;
                            break;

                        case CraveOnSiteServerConfigConstants.CustomerId:
                            if (clientCollection.Count > 0)
                            {
                                if (clientCollection[0].CustomerCollection != null && clientCollection[0].CustomerCollection.Count > 0)
                                {
                                    value = clientCollection[0].CustomerCollection[0].CustomerId;
                                }
                            }
                            break;
                        case CraveOnSiteServerConfigConstants.WebserviceUrl1:
                        case CraveOnSiteServerConfigConstants.WebserviceUrl2:                        
                            value = this.url;
                            break;

                        case CraveOnSiteServerConfigConstants.RequestInterval:
                            value = terminal.RequestInterval;
                            break;

                        case CraveOnSiteServerConfigConstants.Salt:
                            value = Dionysos.Security.Cryptography.Cryptographer.EncryptStringUsingRijndael(company.Salt);
                            break;

                    }

                    file.AppendFormatLine("{0}<value>{1}</value>", itemtabs + TAB, value);
                    file.AppendFormatLine("{0}</item>", itemtabs);
                }
                if (!currentSection.Equals(String.Empty))
                    file.AppendLine(TAB + "</section>");

                file.AppendLine("</configuration>");

                string derp = file.ToString();

                string subDirName = string.Format("OnsiteServer-{0}", terminal.Name);
                string subDirPath = Path.Combine(tempFolder, subDirName);
                if (!Directory.Exists(subDirPath))
                    Directory.CreateDirectory(subDirPath);

                File.WriteAllText(Path.Combine(subDirPath, "CraveOnsiteServer.config"), derp);
            }
        }

        private void AddCloudConfigSection(StringBuilder file)
        {
            string currentSection = String.Empty;
            string itemtabs = TAB;
            CraveCloudConfigInfo cloudConfiguration = new CraveCloudConfigInfo();
            foreach (var item in cloudConfiguration)
            {
                string section = item.Section;
                if (!section.Equals(currentSection))
                {
                    if (!currentSection.Equals(String.Empty))
                        file.AppendLine(TAB + "</section>");

                    file.AppendFormatLine(TAB + "<section name=\"{0}\">", section);

                    currentSection = section;
                    itemtabs = TAB + TAB;
                }

                file.AppendFormatLine("{0}<item name=\"{1}\">", itemtabs, item.Name);
                file.AppendFormatLine("{0}<friendlyname>{1}</friendlyname>", itemtabs + TAB, item.FriendlyName);
                file.AppendFormatLine("{0}<type>{1}</type>", itemtabs + TAB, item.Type);

                // Determine values
                string value = item.DefaultValue.ToString();
                switch (item.Name)
                {
                    case CraveCloudConfigConstants.CloudEnvironment:
                        value = this.cloudEnvironment.ToString();
                        break;
                    case CraveCloudConfigConstants.ManualWebserviceBaseUrl:
                        value = this.url;
                        break;
                }

                file.AppendFormatLine("{0}<value>{1}</value>", itemtabs + TAB, value);
                file.AppendFormatLine("{0}</item>", itemtabs);

            }
            if (!currentSection.Equals(String.Empty))
                file.AppendLine(TAB + "</section>");
        }

        void GenerateConfigFiles()
        {
            int companyId = this.ddlCompanyId.ValidId;
            string tempFolder = this.Server.MapPath(string.Format("~/Temp/{0}/", DateTime.Now.Ticks));
            Directory.CreateDirectory(tempFolder);
            string zipFile = this.Server.MapPath(string.Format("~/Temp/{0}.zip", DateTime.Now.Ticks));

            try
            {
                CompanyEntity company = new CompanyEntity(companyId);

                // Clients (only for Otoucho systems)
                if (company.SystemType == SystemType.Otoucho)
                    this.GenerateClientConfigFiles(tempFolder, company);

                // Onsite server
                this.GenerateOnsiteServerConfig(tempFolder, company);

                // Agent
                this.GenerateAgentConfigFiles(tempFolder, company);

                ZipFile zip = new ZipFile();
                zip.ZipFileFullPath = zipFile;
                zip.SourceDirectory = tempFolder;
                zip.IncludeSubFolders = true;
                zip.RemoveSourceDirectoryPath = true;
                zip.RemoveSourceDirectoryName = true;
                zip.ZipDirectory();

                this.Validate();

                if (this.IsValid)
                {
                    string fileNameForZip = string.Format("{0}-{1}.zip", company.Name.RemoveAllNoneSafeUrlCharacters(), this.cloudEnvironment.ToString());
                    this.Response.ContentType = MimeTypeHelper.GetMimeType(zipFile);
                    this.Response.AppendHeader("Content-Disposition", string.Format("attachment; filename={0}", fileNameForZip));
                    this.Response.TransmitFile(zipFile);
                    this.Response.End();
                }

                Directory.Delete(tempFolder, true);
                //File.Delete(zipFile);                
            }
            catch
            {
                try
                {
                    Directory.Delete(tempFolder, true);
                    //File.Delete(zipFile);
                }
                catch { }

                throw;
            }

        }

        private void GenerateAgentConfigFiles(string tempFolder, CompanyEntity company)
        {
            // Terminals
            foreach (var terminal in company.TerminalCollection)
            {
                StringBuilder file = new StringBuilder();
                file.AppendLine("<configuration>");
                string currentSection = String.Empty;
                string itemtabs = TAB;

                // Cloud config
                AddCloudConfigSection(file);

                // Oss specific config
                CraveOnSiteAgentConfigInfo agentConfiguration = new CraveOnSiteAgentConfigInfo();
                for (int i = 0; i < agentConfiguration.Count; i++)
                {
                    ConfigurationItem item = agentConfiguration[i];
                    string section = item.Section;
                    if (!section.Equals(currentSection))
                    {
                        if (!currentSection.Equals(String.Empty))
                            file.AppendLine(TAB + "</section>");

                        file.AppendFormatLine(TAB + "<section name=\"{0}\">", section);

                        currentSection = section;
                        itemtabs = TAB + TAB;
                    }

                    file.AppendFormatLine("{0}<item name=\"{1}\">", itemtabs, item.Name);
                    file.AppendFormatLine("{0}<friendlyname>{1}</friendlyname>", itemtabs + TAB, item.FriendlyName);
                    file.AppendFormatLine("{0}<type>{1}</type>", itemtabs + TAB, item.Type);

                    object value = item.DefaultValue;

                    switch (item.Name)
                    {
                        case CraveOnSiteAgentConfigConstants.Username:
                            value = company.CompanyOwnerEntity.Username;
                            break;

                        case CraveOnSiteAgentConfigConstants.Password:
                            if (company.CompanyOwnerEntity.IsPasswordEncrypted)
                                value = company.CompanyOwnerEntity.Password;
                            else
                                value = Dionysos.Security.Cryptography.Cryptographer.EncryptStringUsingRijndael(company.CompanyOwnerEntity.Password);
                            break;

                        case CraveOnSiteAgentConfigConstants.CompanyId:
                            value = company.CompanyId;
                            break;

                        case CraveOnSiteAgentConfigConstants.WebserviceBaseUrl1:
                        case CraveOnSiteAgentConfigConstants.WebserviceBaseUrl2:
                        case CraveOnSiteAgentConfigConstants.WebserviceBaseUrl3:
                            value = this.url;
                            break;
                    }

                    file.AppendFormatLine("{0}<value>{1}</value>", itemtabs + TAB, value);
                    file.AppendFormatLine("{0}</item>", itemtabs);
                }
                if (!currentSection.Equals(String.Empty))
                    file.AppendLine(TAB + "</section>");

                file.AppendLine("</configuration>");

                string derp = file.ToString();

                string subDirName = string.Format("Obymobi-Agent-{0}", terminal.Name.RemoveAllNoneSafeUrlCharacters());
                string subDirPath = Path.Combine(tempFolder, subDirName);
                if (!Directory.Exists(subDirPath))
                    Directory.CreateDirectory(subDirPath);

                File.WriteAllText(Path.Combine(subDirPath, "CraveOnsiteAgent.config"), derp);
            }
        }

        private void GenerateClientConfigFiles(string tempFolder, CompanyEntity company)
        {
            // Check if all clients are assigned to a deliverygroup 
            if (company.ClientCollection.Any(x => !x.DeliverypointGroupId.HasValue))
            {
                this.MultiValidatorDefault.AddError(this.Translate("TabletsWithoutDeliverypointgroup", "Er zijn één of meerdere schermen zonder Tafelgroup."));
                //this.MultiValidatorDefault.AddError("Er zijn Otouch-schermen zonder Tafelgroep, één van beide is vereist voor het download van een configuratie.");
            }

            // Pos Validation
            //PosHelperWrapper.VerifySettings(company);

            foreach (var client in company.ClientCollection)
            {
                string template = File.ReadAllText(this.Server.MapPath("~/Configuration/Sys/ClientConfigurationTemplate.txt"));

                var password = "";

                if (company.CompanyOwnerEntity.IsPasswordEncrypted)
                    password = Dionysos.Security.Cryptography.Cryptographer.DecryptStringUsingRijndael(company.CompanyOwnerEntity.Password);
                else
                    password = company.CompanyOwnerEntity.Password;

                // Replace variables
                template = template.Replace("[[Username]]", company.CompanyOwnerEntity.Username);
                template = template.Replace("[[Password]]", password);
                template = template.Replace("[[CompanyId]]", company.CompanyId.ToString());
                template = template.Replace("[[ClientId]]", client.ClientId.ToString());
                template = template.Replace("[[WebserviceBaseUrl1]]", this.url);
                template = template.Replace("[[WebserviceBaseUrl2]]", this.url);
                template = template.Replace("[[WebserviceBaseUrl3]]", this.url);

                if (client.DeliverypointGroupId.HasValue && client.DeliverypointgroupEntity.AddDeliverypointToOrder)
                {
                    template = template.Replace("[[AddDeliverypointToOrder]]", "true");

                    if (company.DeliverypointCollection.Count > 0)
                        template = template.Replace("[[DeliverypointNumber]]", company.DeliverypointCollection[0].Number);
                }
                else
                    template = template.Replace("[[AddDeliverypointToOrder]]", "false");

                string subDirName = string.Format("Client-{0}-{1}", client.ClientId, client.MacAddress);
                string subDirPath = Path.Combine(tempFolder, subDirName);
                if (!Directory.Exists(subDirPath))
                    Directory.CreateDirectory(subDirPath);

                File.WriteAllText(Path.Combine(subDirPath, "Settings.xml"), template);
            }
        }

        #endregion

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            this.HookUpEvents();

            if (!this.IsPostBack && CmsSessionHelper.CurrentCompanyId > 0)
            {
                this.ddlCompanyId.Value = CmsSessionHelper.CurrentCompanyId;

                if (this.Request.Url.ToString().Contains("app."))
                    this.ddlEnvironment.Value = CloudEnvironment.ProductionPrimary.ToString();
                else if (this.Request.Url.ToString().Contains("test."))
                    this.ddlEnvironment.Value = CloudEnvironment.Test.ToString();
                else if (this.Request.Url.ToString().Contains("dev."))
                    this.ddlEnvironment.Value = CloudEnvironment.Development.ToString();
                else if (this.Request.Url.ToString().Contains("staging."))
                    this.ddlEnvironment.Value = CloudEnvironment.Staging.ToString();
                else if (TestUtil.IsPcDeveloper)
                    this.ddlEnvironment.Value = "Manual-api";


                this.AddInformatorInfo(this.Translate("SelectedActiveCompany", "Het huidige bedrijf is automatisch geselecteerd."));
                this.Validate();
            }
        }

        void btGenerateConfigFiles_Click(object sender, EventArgs e)
        {
            bool standardCloudEnvironment = false;
            CloudEnvironment cloudEnvironment;

            if (EnumUtil.TryParse(this.ddlEnvironment.Value.ToString(), out cloudEnvironment))
            {
                standardCloudEnvironment = true;
            }


            if (this.ddlCompanyId.ValidId <= 0)
            {
                this.AddInformator(InformatorType.Warning, "U dient een bedrijf te kiezen waarvoor de configuratie moet worden gemaakt.");
                return;
            }
            else if (cloudEnvironment != CloudEnvironment.Manual && !this.tbManualUrl.Text.IsNullOrWhiteSpace())
            {
                this.AddInformator(InformatorType.Warning, this.Translate("CustomUrlsEnteredWithoutChoosingCustomConfig", "U heeft handmatige url's ingevuld, kies daarom 'Custom' als gewenste omgeving."));
                return;
            }

            if (!standardCloudEnvironment)
            {
                // Developer localhosts:
                if (this.ddlEnvironment.Value.Equals("Manual-api"))
                {
                    this.cloudEnvironment = CloudEnvironment.Manual;
                    this.url = WebEnvironmentHelper.GetApiUrl() + "/";
                }
            }
            else
            {
                this.cloudEnvironment = cloudEnvironment;

                switch (cloudEnvironment)
                {
                    case CloudEnvironment.ProductionPrimary:
                    case CloudEnvironment.ProductionSecondary:
                    case CloudEnvironment.ProductionOverwrite:
                        this.url = ObymobiConstants.PrimaryWebserviceUrl;
                        break;                    
                    case CloudEnvironment.Test:
                        this.url = ObymobiConstants.TestWebserviceUrl;
                        break;
                    case CloudEnvironment.Development:
                        this.url = ObymobiConstants.DevelopmentWebserviceUrl;
                        break;
                    case CloudEnvironment.Staging:
                        this.url = ObymobiConstants.StagingWebserviceUrl;
                        break;
                    case CloudEnvironment.Manual:
                        this.url = this.tbManualUrl.Text;
                        break;
                    default:
                        throw new NotImplementedException();                        
                }
            }

            this.GenerateConfigFiles();

            if (this.ddlCompanyId.ValidId > 0)
            {
                this.GenerateConfigFiles();
            }
            else
            {
                this.AddInformator(InformatorType.Warning, "U dient een bedrijf te kiezen waarvoor de configuratie moet worden gemaakt.");
            }
        }

        #endregion

        #region Properties
        #endregion

    }
}


