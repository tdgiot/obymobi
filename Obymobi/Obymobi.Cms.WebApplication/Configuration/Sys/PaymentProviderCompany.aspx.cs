using System;
using Obymobi.Enums;

namespace Obymobi.ObymobiCms.Configuration.Sys
{
    public partial class PaymentProviderCompany : Dionysos.Web.UI.PageLLBLGenEntity
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Redirect the user if it's not an administrator
            if (CmsSessionHelper.CurrentRole < Role.Reseller)
                Response.Redirect("~/401.aspx");
        }
    }
}
