﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dionysos.Web.UI;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.EntityClasses;
using System.Text;
using Dionysos;
using Dionysos.Web;

namespace Obymobi.ObymobiCms.Configuration.Sys
{
    public partial class Vectron : PageDefault
    {
        private void HookupEvents()
        {
            this.btnGenerateTestScript.Click += new EventHandler(btnGenerateTestScript_Click);
        }

        private bool CustomValidation()
        {
            bool success = true;

            if (this.ddlCompanyId.SelectedIndex == 0)
            {
                success = false;
                this.MultiValidatorDefault.AddWarning("Please select a company before generating the test script.");
            }

            base.Validate();

            return success;
        }

        private void GenerateTestScript()
        {
            StringBuilder script = new StringBuilder();
            script.AppendLine("dim Vectron");
            script.AppendLine("set Vectron = createobject(\"Flexipos.Order\")");
            script.AppendLine("Vectron.Show");
            script.AppendLine(string.Format("Vectron.ClerkOpen {0}", this.tbClerkNo.Value));
            script.AppendLine(string.Format("Vectron.GuestCOpen {0}", this.tbGuestCheckNo.Value));

            PredicateExpression filter = new PredicateExpression();
            filter.Add(ProductFields.CompanyId == this.ddlCompanyId.Value);

            ProductCollection products = new ProductCollection();
            products.GetMulti(filter);

            foreach (ProductEntity product in products)
            {
                if (product.PosproductId.HasValue && product.Visible && product.PosproductEntity.Visible)
                    script.AppendLine(string.Format("Vectron.PLU 1, {0}", product.PosproductEntity.ExternalId));
            }

            script.AppendLine(string.Format("Vectron.GuestCClose {0}", this.tbGuestCheckNo.Value));
            script.AppendLine(string.Format("Vectron.ClerkClose {0}", this.tbClerkNo.Value));
            script.AppendLine("Vectron.Hide");
            script.AppendLine("Set Vectron = Nothing");

            string filename = "Test.vbs";
            this.Response.Clear();
            Response.AppendHeader("content-disposition", "attachment; filename=" + filename);
            Response.ContentType = "text/vbs";
            Response.Charset = "UTF-8";
            Response.Write(script.ToString());
            Response.Flush();
            Response.End();
        }

        private void GenerateVectronTestScript(int companyId, int mode, int treshold)
        {
            StringBuilder sb = new StringBuilder();

            PredicateExpression productFilter = new PredicateExpression();
            productFilter.Add(ProductFields.CompanyId == companyId);
            productFilter.Add(ProductFields.PosproductId != DBNull.Value);

            RelationCollection productRelations = new RelationCollection();
            productRelations.Add(ProductEntity.Relations.PosproductEntityUsingPosproductId);

            PosproductCollection products = new PosproductCollection();
            products.GetMulti(productFilter, productRelations);

            PredicateExpression deliverypointFilter = new PredicateExpression();
            deliverypointFilter.Add(DeliverypointFields.CompanyId == companyId);
            deliverypointFilter.Add(DeliverypointFields.PosdeliverypointId != DBNull.Value);

            RelationCollection deliverypointRelations = new RelationCollection();
            deliverypointRelations.Add(DeliverypointEntity.Relations.PosdeliverypointEntityUsingPosdeliverypointId);

            PosdeliverypointCollection deliverypoints = new PosdeliverypointCollection();
            deliverypoints.GetMulti(deliverypointFilter, deliverypointRelations);

            List<string> deliverypointList = new List<string>();
            if (mode == 1)
            {
                int random = RandomUtil.GetRandomNumber(deliverypoints.Count);
                deliverypointList.Add(deliverypoints[random].ExternalId);
            }
            else if (mode == 2 || mode == 3)
            {
                foreach (PosdeliverypointEntity dpoint in deliverypoints)
                {
                    deliverypointList.Add(dpoint.ExternalId);
                }
            }

            sb.AppendLine("dim Vectron");
            sb.AppendLine("");
            sb.AppendLine("	set Vectron = createobject(\"Flexipos.Order\")");
            sb.AppendLine("	Vectron.Show");

            string deliverypoint = "0";

            if (mode == 1 || (mode == 3 && treshold > 0))
            {
                sb.AppendLine("	Vectron.ClerkOpen 80");

                int random = RandomUtil.GetRandomNumber(deliverypointList.Count);
                deliverypoint = deliverypointList[random];

                sb.AppendLine(string.Format("	Vectron.GuestCOpen {0}", deliverypoint));
            }

            int count = 0;
            int quantity = 1;

            for (int i = 0; i < products.Count; i++)
            {
                PosproductEntity product = products[i];

                if (mode == 2)
                {
                    sb.AppendLine("	Vectron.ClerkOpen 80");

                    int random = RandomUtil.GetRandomNumber(deliverypoints.Count);
                    deliverypoint = deliverypointList[random];

                    sb.AppendLine(string.Format("	Vectron.GuestCOpen {0}", deliverypoint));
                }

                quantity = RandomUtil.GetRandomNumber(10) + 1;
                sb.AppendLine(string.Format("	Vectron.PLU {1}, {0}", product.ExternalId, quantity));

                count++;

                if (mode == 2)
                {
                    sb.AppendLine(string.Format("	Vectron.GuestCClose {0}", deliverypoint));
                    sb.AppendLine("	Vectron.ClerkClose 80");
                }

                if (mode == 3 && count == treshold)
                {
                    sb.AppendLine(string.Format("	Vectron.GuestCClose {0}", deliverypoint));
                    sb.AppendLine("	Vectron.ClerkClose 80");
                    count = 0;

                    if (i != (products.Count - 1))
                    {
                        sb.AppendLine("	Vectron.ClerkOpen 80");

                        int random = RandomUtil.GetRandomNumber(deliverypoints.Count);
                        deliverypoint = deliverypointList[random];

                        sb.AppendLine(string.Format("	Vectron.GuestCOpen {0}", deliverypoint));
                    }
                }
            }

            if (mode == 1 || count > 0)
            {
                sb.AppendLine(string.Format("	Vectron.GuestCClose {0}", deliverypoint));
                sb.AppendLine("	Vectron.ClerkClose 80");
            }

            sb.AppendLine("	Vectron.Hide");
            sb.AppendLine("	Set Vectron = Nothing");

            string filename = string.Format("FlexiPos_company-{0}_mode-{1}_{2}.vbs", companyId, mode, DateTime.Now.ToString("yyyyMMddhhmmss"));

            this.Response.Clear();
            Response.AppendHeader("content-disposition", "attachment; filename=" + filename);
            Response.ContentType = "text/vbs";
            Response.Charset = "UTF-8";
            Response.Write(sb.ToString());
            Response.Flush();
            Response.End();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.HookupEvents();

            int companyId = 0;
            int mode = 0;
            int treshold = 0;

            QueryStringHelper.TryGetValue<int>("companyId", out companyId);
            QueryStringHelper.TryGetValue<int>("mode", out mode);
            QueryStringHelper.TryGetValue<int>("treshold", out treshold);

            if (companyId > 0 && mode > 0)
                this.GenerateVectronTestScript(companyId, mode, treshold);
        }

        private void btnGenerateTestScript_Click(object sender, EventArgs e)
        {
            if (this.CustomValidation())
                this.GenerateTestScript();
        }
    }
}