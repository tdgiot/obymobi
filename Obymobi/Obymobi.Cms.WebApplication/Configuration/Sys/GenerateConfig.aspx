﻿<%@ Page Title="Generate configuration files" Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Configuration.Sys.GenerateConfig" Codebehind="GenerateConfig.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitleHolder" Runat="Server"><D:Label runat="server" id="lblTitle">Configuratie bestand genereren</D:Label></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhToolbarHolder" Runat="Server">
<div>
    <D:Button runat="server" ID="btGenerateConfigFiles" Text="Configuratie Downloaden" />
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhContentHolder" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
					<table class="dataformV2">
						<tr>
							<td class="label">
							    <D:Label runat="server" id="lblCompanyId">Bedrijf</D:Label>
							</td>
							<td class="control">
							    <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlCompanyId" EntityName="Company" IncrementalFilteringMode="StartsWith" TextField="Name" ValueField="CompanyId"></X:ComboBoxLLBLGenEntityCollection>			
							</td>
							<td class="label">
								&nbsp;
							</td>
							<td class="control">
								&nbsp;
							</td>        
						</tr>
						<tr>
							<td class="label">
							    <D:Label runat="server" id="lblEnvironment">Omgeving</D:Label>
							</td>
							<td class="control">
							    <X:ComboBox runat="server" ID="ddlEnvironment" EntityName="Company">                                    
                                </X:ComboBox>			
							</td>
							<td class="label">
								&nbsp;
							</td>
							<td class="control">
								&nbsp;
							</td>        
						</tr>                        						
						<tr>
							<td class="label">
							    <D:Label runat="server" id="lblManualUrl">Handmatig</D:Label>
							</td>
							<td class="control">
							    <D:TextBoxString ID="tbManualUrl" runat="server"></D:TextBoxString>
							</td>
						</tr>																				
                    </table>                    
                </Controls>
            </X:TabPage>
        </TabPages>
    </X:PageControl>
</div>
</asp:Content>

