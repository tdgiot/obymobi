<%@ Page Async="true" Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Configuration.Sys.Maintenance" Title="Maintenance" CodeBehind="Maintenance.aspx.cs" %>

<%@ Register Src="~/Configuration/Sys/SubPanels/BulkConfigurationRoomControlConfiguration.ascx" TagName="BulkEditor" TagPrefix="RoomControlConfiguration" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cplhTitleHolder">
    <D:Label runat="server" ID="lblTitle">Master onderhoud</D:Label>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="cplhToolbarHolder">
    <span class="toolbar">
        <X:ToolBarButton runat="server" ID="tbbRefresh" CommandName="Refresh" Text="Verversen" PreSubtWarning="Zorg dat dat de database structuur actueel is, voordat er ververst wordt. Wil je doorgaan met verversen?" Image-Url="~/Images/Icons/arrow_refresh.png" />
        <X:ToolBarButton runat="server" ID="tbbClearCache" CommandName="ClearCache" Text="Cache legen" Image-Url="~/Images/Icons/delete.png" />
    </span>
</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="cplhContentHolder">
    <h1>
        <D:Label runat="server" ID="lblConnectionString" LocalizeText="false"></D:Label></h1>
    <br />
    <X:PageControl ID="tabsMain" runat="server" Width="100%">
        <TabPages>
            <X:TabPage Name="Local" Text="Lokaal">
                <controls>
					<D:CheckBoxList runat="server" ID="cblLocal" RepeatLayout="Flow"  CssClass="checkboxlist">
						<Items>
							<asp:ListItem Value="RefreshModules" Text="Modules verversen" Title="Module" Selected="True" />
							<asp:ListItem Value="RefreshUiElements" Text="Vensters verversen" Title="UIElement" Selected="True" />
							<asp:ListItem Value="DeleteCustomUiElements" Text="Aangepaste vensters verwijderen (!)" Title="UIElementCustom" />
							<asp:ListItem Value="RefreshUiElementSubPanels" Text="Subvensters verversen" Title="UIElementSubPanel" Selected="True" />
							<asp:ListItem Value="DeleteCustomUiElementSubPanels" Text="Aangepaste subvensters verwijderen (!)" Title="UIElementSubPanelCustom" />
							<asp:ListItem Value="RefreshEntityInformation" Text="Entity informatie verversen" Title="EntityInformation / EntityFieldInformation" Selected="True" />
							<asp:ListItem Value="DeleteCustomEntityInformation" Text="Aangepaste entity informatie verwijderen (!)" Title="EntityInformationCustom / EntityFieldInformationCustom" />
							<asp:ListItem Value="CreateDefaultViews" Text="Overzichten verversen" Title="View / ViewItem" Selected="True" />
							<asp:ListItem Value="DeleteCustomViews" Text="Aangepaste overzichten verwijderen" Title="CustomView / CustomViewItem" />
							<asp:ListItem Value="RefreshReferentialContraints" Text="Referentiele integriteit verversen" Title="ReferentialContraint" Selected="True" />
							<asp:ListItem Value="DeleteCustomReferentialContraints" Text="Aangepaste referentiele integriteit verwijderen (!)" Title="ReferentialContraintCustom" />
							<asp:ListItem Value="RefreshGlobalizationData" Text="Vertalingen verversen" Title="Translation" />
                            <asp:ListItem Value="RefreshRoleUiElementRights" Text="Pagina rechten verversen" Title="RoleUIElementRights" />
                            <asp:ListItem Value="RefreshTimeZones" Text="Refresh time zones" Title="TimeZones" />
                            <asp:ListItem Value="RefreshCountryAndCurrency" Text="Refresh countries and currencies" Title="CountriesCurrencies" />
						</Items>
					</D:CheckBoxList>
				</controls>
            </X:TabPage>
            <X:TabPage Name="Master" Text="Master">
                <controls>
					<D:CheckBoxList runat="server" ID="cblMaster" RepeatLayout="Flow"  CssClass="checkboxlist">
						<Items>
							<asp:ListItem Value="RefreshUiElements" Text="Vensters verversen" Title="UIElement" />
							<asp:ListItem Value="RefreshUiElementSubPanels" Text="Subvensters verversen" Title="UIElementSubPanel" />
							<asp:ListItem Value="RefreshEntityInformation" Text="Entity informatie verversen" Title="EntityInformation / EntityFieldInformation" />
							<asp:ListItem Value="RefreshReferentialContraints" Text="Referentiele integriteit verversen" Title="ReferentialContraint" />
							<asp:ListItem Value="RefreshGlobalizationData" Text="Vertalingen verversen" Title="Translation" />
                            <asp:ListItem Value="RefreshRoleUiElementRights" Text="Pagina rechten verversen" Title="RoleUIElementRights" />
						</Items>
					</D:CheckBoxList>
				</controls>
            </X:TabPage>
            <X:TabPage Name="Cache" Text="Cache">
                <controls>
					<D:Button runat="server" ID="btShowCache" Text="Toon cache inhoud" />
					<br />
					<X:GridView runat="server" ID="gvCache" Width="100%" Visible="False">
						<SettingsBehavior AllowSort="False" AllowDragDrop="False" />
						<SettingsPager Mode="ShowAllRecords" />
					</X:GridView>
				</controls>
            </X:TabPage>
            <X:TabPage Text="Gegevensbeheer" Name="DataManagement">
                <controls>
                    <table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelTextOnly runat="server" id="lblDeleteCompany">Bedrijf verwijderen</D:LabelTextOnly>
							</td>
							<td class="control">
                                <D:Button ID="btDeleteCompany" runat="server" LocalizeText="False" Text="Delete Current Company" PreSubmitWarning="Weet u zeker dat u dit bedrijf en alle bijbehorende gegevens wilt verwijderen? DIT KAN NIET ONGEDAAN WORDEN GEMAAKT." />                                    								
                                <D:Button ID="btDeleteInactiveClientsOfCompany" runat="server" LocalizeText="False" Text="Delete Inactive Clients Of Current Company" PreSubmitWarning="This will remove all clients being offline for 2 months or longer" />                                    								
                                <pre><D:PlaceHolder runat="server" ID="plhDeleteInactiveClientsOfCompanyReponse"></D:PlaceHolder></pre>
							</td>
                            <td></td>
                        </tr>
                    </table>
                    <hr />
                    <table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelTextOnly runat="server" id="lblConvertToUtc" LocalizeText="false">Convert datetimes to UTC</D:LabelTextOnly>
							</td>
							<td class="control">
                                <D:Button ID="btnConvertCreatedUpdatedToUtc" runat="server" Text="Convert Created/Updated to UTC" PreSubmitWarning="Are you sure?" />
                                <D:Button ID="btnConvertDateTimesToUtc" runat="server" Text="Convert DateTimes to UTC" PreSubmitWarning="Are you sure?" />
							</td>
                            <td></td>
                        </tr>
                    </table>
                    <hr />
                    <table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelTextOnly runat="server" id="lblMediaRatioTypeFrom">Media ratio type van</D:LabelTextOnly>
							</td>
							<td class="control">
                                <D:DropDownListInt ID="ddlMediaRatioTypeFrom" runat="server" UseDataBinding="true"></D:DropDownListInt>
							</td>
							<td class="label">
								<D:LabelTextOnly runat="server" id="lblMediaRatioTypeTo">Media ratio type naar</D:LabelTextOnly>
							</td>
							<td class="control">
                                <D:DropDownListInt ID="ddlMediaRatioTypeTo" runat="server" UseDataBinding="true"></D:DropDownListInt>
							</td>
                        </tr>
                        <tr>
							<td class="label">
							</td>
							<td class="control">
                                <D:Button ID="btnCopyMediaRatioTypeMedia" runat="server" Text="Kopieer!" />
							</td>                     
							<td class="label">
							</td>
							<td class="control">
                                <D:Button ID="btnCopyAllMediaRatioTypeMedia" runat="server" Text="Kopieer alles!" PreSubmitWarning="Are you sure?" />
							</td>                                                        
                        </tr>
                    </table>
                    <hr />
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:LabelTextOnly runat="server" ID="lblConvertOrderHours">Convert order hours</D:LabelTextOnly>
                            </td>
                            <td class="control">
                                <D:Button ID="btnConvertOrderHours" runat="server" Text="Convert order hours" />
                            </td>
                        </tr>
                    </table>
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:LabelTextOnly runat="server" ID="lblConvertMenuImages">Convert tablet menu images to emenu menu images</D:LabelTextOnly>
                            </td>
                            <td class="control">
                                <D:Button ID="btConvertMenuImages" runat="server" Text="Convert menu images" />
                            </td>
                        </tr>
                    </table>
                    <hr />
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:LabelTextOnly runat="server" ID="lblEncryptCompanyOwnerPasswords">Encrypt company owner passwords</D:LabelTextOnly>
                            </td>
                            <td class="control">
                                <D:Button ID="btnEncryptCompanyOwnerPasswords" runat="server" Text="Encrypt company owner passwords" />
                            </td>
                        </tr>
                    </table>
                    <hr />
                    <table class="dataformV2">
                        <tr>
		                    <td class="label">
                                <D:LabelTextOnly runat="server" id="lblMediaRefresh">Media naar v2 updaten</D:LabelTextOnly>
		                    </td>
		                    <td class="control"> 
                                <X:ComboBoxLLBLGenEntityCollection ID="ddlMediaRefreshCompanyId" runat="server" EntityName="Company" DataTextField="Name" DataValueField="CompanyId" PreventEntityCollectionInitialization="true"></X:ComboBoxLLBLGenEntityCollection>
		                    </td>          
                            <td class="label">
                                
                            </td>
                            <td class="control">
                                
                            </td>
                        </tr>
                    </table>
				    <hr/>
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:LabelTextOnly runat="server" ID="lblConvertContactPagesToWebLinkPages" Text="Convert contact pages to WebLink pages for native sites"></D:LabelTextOnly>
                            </td>
                            <td class="control">
                                <D:Button runat="server" ID="btnConvertContactPagesToWebLinkPages" Text="Convert contact pages"/>
                            </td>
                        </tr> 
                    </table>
                    <hr/>
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:LabelTextOnly runat="server" ID="lblClearEmptyPincodes" Text="Clear empty pincodes"></D:LabelTextOnly>
                            </td>
                            <td class="control">
                                <D:Button runat="server" ID="btnClearEmptyPincodes" Text="Clear empty pincodes"/>
                            </td>
                        </tr>
                    </table>
                    <hr/>
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:LabelTextOnly runat="server" ID="lblPrepareForAmazon" Text="Prepare for the Amazonezzz"></D:LabelTextOnly>
                            </td>
                            <td class="control">
                                <D:Button runat="server" ID="btnPrepareForAmazon" Text="Prepare for Amazon"/>
                            </td>
                        </tr>
                    </table>
                    <hr/>
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:LabelTextOnly runat="server" ID="lblSetDefaultAlterationDialogColors" Text="Set default alteration dialog colors"></D:LabelTextOnly>
                            </td>
                            <td class="control">
                                <D:Button runat="server" ID="btnSetDefaultAlterationDialogColors" Text="Set default colors"/>
                            </td>
                        </tr>
                    </table>
                    <hr/>
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                 <D:LabelTextOnly runat="server" id="lblGenerateCategorySuggestionsOverview">Generate overview of categorySuggestions without productCategory</D:LabelTextOnly>
                            </td>
                            <td class="control">
                                <D:Button ID="btnGenerateCategorySuggestionsOverview" runat="server" Text="Generate category suggestions overview" />
                                <D:Button ID="btnLinkCategorySuggestionsToProductCategories" runat="server" Text="Link CategorySuggestions to productCategories" />
                            </td>
                        </tr>                        
                    </table>       
                    <D:PlaceHolder ID="plhCategorySuggestionsOverview" runat="server" Visible="false">
                        <table class="dataformV2">
                        <tr>
                            <td colspan="4">                                
                                <X:GridView runat="server" ID="gvCategorySuggestions" Width="100%" Visible="False">
						            <SettingsBehavior AllowSort="True" AllowDragDrop="False" />
						            <SettingsPager Mode="ShowAllRecords" />
                                    <Columns>
                                        <dxwgv:GridViewDataHyperLinkColumn FieldName="CompanyId" VisibleIndex="0" SortIndex="0" SortOrder="Ascending">
								            <Settings AutoFilterCondition="Contains" />
                                            <PropertiesHyperLinkEdit NavigateUrlFormatString="~/Company/Installation.aspx?switchToCompany={0}" Target="_blank" TextField="CompanyId"  />
                                        </dxwgv:GridViewDataHyperLinkColumn>
                                        <dxwgv:GridViewDataHyperLinkColumn FieldName="CategoryId" VisibleIndex="1" SortIndex="0" SortOrder="Ascending">
								            <Settings AutoFilterCondition="Contains" />
                                            <PropertiesHyperLinkEdit NavigateUrlFormatString="~/Catalog/Category.aspx?id={0}" Target="_blank" TextField="CategoryId"  />
                                        </dxwgv:GridViewDataHyperLinkColumn>
                                        <dxwgv:GridViewDataColumn FieldName="CategoryName" VisibleIndex="2" >
								            <Settings AutoFilterCondition="Contains" />
                                        </dxwgv:GridViewDataColumn>
                                        <dxwgv:GridViewDataHyperLinkColumn FieldName="ProductId" VisibleIndex="3" >
								            <Settings AutoFilterCondition="Contains" />
                                            <PropertiesHyperLinkEdit NavigateUrlFormatString="~/Catalog/Product.aspx?id={0}" Target="_blank" TextField="ProductId"  />
                                        </dxwgv:GridViewDataHyperLinkColumn>
                                        <dxwgv:GridViewDataColumn FieldName="ProductName" VisibleIndex="4" >
								            <Settings AutoFilterCondition="Contains" />
                                        </dxwgv:GridViewDataColumn>
                                        <dxwgv:GridViewDataColumn FieldName="NumberOfCategories" VisibleIndex="5" >
								            <Settings AutoFilterCondition="Contains" />
                                        </dxwgv:GridViewDataColumn>
                                    </Columns>
					            </X:GridView>
                            </td>
                        </tr>
                        </table>
                    </D:PlaceHolder>
				    <hr/>
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                 <D:LabelTextOnly runat="server" id="btnConvertProductSuggestionsToProductCategorySuggestions">Convert ProductSuggestions to ProductCategorySuggestions</D:LabelTextOnly>
                            </td>
                            <td class="control">
                                <D:Button ID="btnConvertProductSuggestions" runat="server" Text="Convert product suggestions" />
                            </td>
                        </tr>    
                    </table>
                    <hr/>
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:LabelTextOnly runat="server" id="lblSetRoomControllerTypes">Set Room Controller Types</D:LabelTextOnly>
                            </td>
                            <td class="control">
                                <D:Button ID="btnSetRoomControllerTypes" runat="server" Text="Set Controllers" />
                            </td>
                        </tr>                    
                    </table>
                    <hr/>
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:LabelTextOnly runat="server" id="lblSetLoadedSuccessfully">Set LoadedSuccessfully flag on clients</D:LabelTextOnly>
                            </td>
                            <td class="control">
                                <D:Button ID="btnSetLoadedSuccessfully" runat="server" Text="Set Loaded Successfully flag" />
                            </td>
                        </tr>                    
                    </table>
                    <hr/>
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:LabelTextOnly runat="server" id="lblSetVisibilityType">Convert Visible flag to VisibilityType for Products and Categories</D:LabelTextOnly>
                            </td>
                            <td class="control">
                                <D:Button ID="btnSetVisibilityType" runat="server" Text="Set VisibilityType on Products and Categories" />
                            </td>
                        </tr>                    
                    </table>                    
                    <hr/>                    
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:LabelTextOnly runat="server" id="lblConvertUIThemes">Convert UI Themes</D:LabelTextOnly>
                            </td>
                            <td class="control">
                                <D:Button ID="btnConvertUIThemes" runat="server" Text="Convert UI Themes" />
                            </td>
                        </tr>                    
                    </table>
                    <hr/>
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:LabelTextOnly runat="server" id="lblConvertTimeZones">Convert Time Zones</D:LabelTextOnly>
                            </td>
                            <td class="control">
                                <D:Button ID="btnConvertTimeZones" runat="server" Text="Convert Time Zones" />
                            </td>
                        </tr>                    
                    </table>
                    <hr/>
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:LabelTextOnly runat="server" id="lblCreateDefaultCompanyCurrencies">Create default company currencies</D:LabelTextOnly>
                            </td>
                            <td class="control">
                                <D:Button ID="btnCreateDefaultCompanyCurrencies" runat="server" Text="Create currencies" />
                            </td>
                        </tr>                    
                    </table>
                    <hr/>
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:LabelTextOnly runat="server" id="lblSetCustomTextParentCompanyId">Set ParentCompanyId on CustomText entities</D:LabelTextOnly>
                            </td>
                            <td class="control">
                                <D:Button ID="btnSetCustomTextParentCompanyId" runat="server" Text="Set ParentCompanyId for all CustomTexts" />
                            </td>
                        </tr>                    
                    </table>
                    <hr/>                    
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:LabelTextOnly runat="server" id="lblToCustomTextEntities">Copy values to custom text entities</D:LabelTextOnly>
                            </td>
                            <td class="control">
                                <D:CheckBox ID="cbCompanyLanguages" Text="Company languages" runat="server"/><br />
                                <D:CheckBox ID="cbDeliverypointgroups" Text="Deliverypointgroups & deliverypointgroup languages" runat="server"/><br />
                                <D:CheckBox ID="cbCategoryLanguages" Text="Category languages" runat="server"/><br />
                                <D:CheckBox ID="cbProductLanguages" Text="Product languages" runat="server"/><br />
                                <D:CheckBox ID="cbGenericcategoryLanguages" Text="Generic category languages" runat="server" /><br />
                                <D:CheckBox ID="cbGenericproductLanguages" Text="Generic product languages" runat="server" /><br />
                                <D:CheckBox ID="cbAlterationAndOptionLanguages" Text="Alterations & alterationoption languages" runat="server" /><br />
                                <D:CheckBox ID="cbSiteAndPagesLanguages" Text="Site & pages languages" runat="server" /><br />
                                <D:CheckBox ID="cbSiteTemplateAndPageTemplateLanguages" Text="Site template & page template languages" runat="server" /><br />
                                <D:CheckBox ID="cbUITabWidgetLanguages" Text="UI tab & UI widget languages" runat="server" /><br />
                                <D:CheckBox ID="cbRoomControlLanguages" Text="Room control languages" runat="server" /><br />
                                <D:CheckBox ID="cbPointOfInterestLanguages" Text="Point of interest languages" runat="server" /><br />
                                <D:CheckBox ID="cbEntertainmentLanguages" Text="Entertainment languages" runat="server" /><br />
                                <D:CheckBox ID="cbAdvertisementLanguages" Text="Advertisement languages" runat="server" /><br />
                                <D:CheckBox ID="cbActionButtonLanguages" Text="ActionButton languages" runat="server" /><br />
                                <D:CheckBox ID="cbAmenityLanguages" Text="Amenity languages" runat="server" /><br />
                                <D:CheckBox ID="cbAnnouncementLanguages" Text="Announcement languages" runat="server" /><br />
                                <D:CheckBox ID="cbAttachmentLanguages" Text="Attachment languages" runat="server" /><br />
                                <D:CheckBox ID="cbUIFooterItemLanguages" Text="UI footer languages" runat="server" /><br />
                                <D:CheckBox ID="cbVenueCategoryLanguages" Text="Venue category languages" runat="server"/>
                                <br />
                                <D:Button ID="btnCopyValuesToCustomTextEntities" runat="server" Text="Copy to custom text entities" />
                                <D:Button ID="btnCopyValuesToCustomTextEntitiesTest" runat="server" Text="Test" />
                                <br />
                                <D:LabelTextOnly runat="server" ID="lblEntitiesToCopyToCustomText" LocalizeText="false"></D:LabelTextOnly>
                            </td>
                        </tr>
                    </table>                  
                    <hr/>
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:LabelTextOnly runat="server" id="lblCustomTextCleanupToDeleteDuplicates">Run custom text clean-up to delete duplicates</D:LabelTextOnly>
                            </td>
                            <td class="control">
                                <D:Button ID="btnCustomTextCleanupDelete" runat="server" Text="Clean up" />
                            </td>
                        </tr>                    
                    </table>  
                    <hr/>                    
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:LabelTextOnly runat="server" id="lblCreateDefaultCustomTexts">Create default custom texts</D:LabelTextOnly>
                            </td>
                            <td class="control">
                                <D:Button ID="btnCreateDefaultCustomTextsForRoomControl" runat="server" Text="Create default custom texts for: Room Control" />
                            </td>
                        </tr>                    
                        <tr>
                            <td class="label">
                            </td>
                            <td class="control">
                                <D:Button ID="btnCreateDefaultCustomTextsForSites" runat="server" Text="Create default custom texts for: Sites" />
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                            </td>
                            <td class="control">
                                <D:Button ID="btnCreateDefaultCustomTextsForMenu" runat="server" Text="Create default custom texts for: Menu" />
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                            </td>
                            <td class="control">
                                <D:Button ID="btnCreateDefaultCustomTextsForGeneric" runat="server" Text="Create default custom texts for: Generic entities" />
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                            </td>
                            <td class="control">
                                <D:Button ID="btnCreateDefaultCustomTextsForCompany" runat="server" Text="Create default custom texts for: Company related entities" />
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                            </td>
                            <td class="control">
                                <D:Button ID="btnCreateDefaultCustomTextsTest" runat="server" Text="Test" />
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                            </td>
                            <td class="control">                                
                                <D:LabelTextOnly runat="server" ID="lblCreateDefaultCustomTextsResult" LocalizeText="false"></D:LabelTextOnly>
                            </td>
                        </tr>
                    </table>                    
                    <hr/>  
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:LabelTextOnly runat="server" id="LabelTextOnly7">Run custom text clean-up to delete duplicates and remove language id</D:LabelTextOnly>
                            </td>
                            <td class="control">
                                <D:Button ID="btnCustomTextCleanupRemoveLanguage" runat="server" Text="Clean up" />
                            </td>
                        </tr>                    
                    </table>                    
                    <hr/>
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:LabelTextOnly runat="server" id="lblSetMediaRelatedCompanyId">Set RelatedCompanyId for all Media</D:LabelTextOnly>
                            </td>
                            <td class="control">
                                <D:Button ID="btnSetMediaRelatedCompanyId" runat="server" Text="Set RelatedCompanyId for all Media" />
                            </td>
                        </tr>                    
                    </table>
                    <hr/>
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:LabelTextOnly runat="server" id="lblConvertDeliverypointgroupToClientConfiguration">Convert deliverypoint group to client configuration</D:LabelTextOnly>
                            </td>
                            <td class="control">
                                <D:Button ID="btnClearClientConfigurations" runat="server" Text="Clear" />
                                <D:Button ID="btnConvertDeliverypointgroupToClientConfiguration" runat="server" Text="Convert" />
                            </td>
                        </tr>                    
                    </table>
                    <hr/>        
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:LabelTextOnly runat="server" id="lblConvertTerminalToTerminalConfiguration">Convert terminal to terminal configuration</D:LabelTextOnly>
                            </td>
                            <td class="control">
                                <D:Button ID="btnClearTerminalConfigurations" runat="server" Text="Clear" />
                                <D:Button ID="btnConvertTerminalToTerminalConfiguration" runat="server" Text="Convert" />
                            </td>
                        </tr>                    
                    </table>
                    <hr/>       
                     <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:LabelTextOnly runat="server" id="lblSetTerminalType">Set terminal type based on handling method for all terminals</D:LabelTextOnly>
                            </td>
                            <td class="control">
                                <D:Button ID="btnSetTerminalType" runat="server" Text="Convert" />
                            </td>
                        </tr>                    
                    </table>
                    <hr />
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:LabelTextOnly runat="server" id="lblSetCultureCodeForEmptyCustomTexts">Set culture codes for custom texts without one</D:LabelTextOnly>
                            </td>
                            <td class="control">
                                <D:CheckBox ID="cbSetCultureCodeForActionButton" Text="Action button" runat="server"/><br />
                                <D:CheckBox ID="cbSetCultureCodeForAdvertisement" Text="Advertisement" runat="server"/><br />
                                <D:CheckBox ID="cbSetCultureCodeForAlteration" Text="Alteration" runat="server"/><br />
                                <D:CheckBox ID="cbSetCultureCodeForAlterationoption" Text="Alteration option" runat="server"/><br />
                                <D:CheckBox ID="cbSetCultureCodeForAmenity" Text="Amenity" runat="server"/><br />
                                <D:CheckBox ID="cbSetCultureCodeForAnnouncement" Text="Announcement" runat="server"/><br />
                                <D:CheckBox ID="cbSetCultureCodeForAttachment" Text="Attachment" runat="server"/><br />                                
                                <D:CheckBox ID="cbSetCultureCodeForCategory" Text="Category" runat="server"/><br />                                                                                                
                                <D:CheckBox ID="cbSetCultureCodeForEntertainmentcategory" Text="Entertainment category" runat="server"/><br />
                                <D:CheckBox ID="cbSetCultureCodeForGenericcategory" Text="Generic category" runat="server"/><br />
                                <D:CheckBox ID="cbSetCultureCodeForGenericproduct" Text="Generic product" runat="server"/><br />
                                <D:CheckBox ID="cbSetCultureCodeForPage" Text="Page" runat="server"/><br />
                                <D:CheckBox ID="cbSetCultureCodeForPageTemplate" Text="Page template" runat="server"/><br />
                                <D:CheckBox ID="cbSetCultureCodeForPointOfInterest" Text="Point of interest" runat="server"/><br />
                                <D:CheckBox ID="cbSetCultureCodeForRoomControlArea" Text="Room control area" runat="server"/><br />
                                <D:CheckBox ID="cbSetCultureCodeForRoomControlComponent" Text="Room control component" runat="server"/><br />
                                <D:CheckBox ID="cbSetCultureCodeForRoomControlSection" Text="Room control section" runat="server"/><br />
                                <D:CheckBox ID="cbSetCultureCodeForRoomControlSectionItem" Text="Room control section item" runat="server"/><br />
                                <D:CheckBox ID="cbSetCultureCodeForRoomControlWidget" Text="Room control widget" runat="server"/><br />
                                <D:CheckBox ID="cbSetCultureCodeForProduct" Text="Product" runat="server"/><br />                                
                                <D:CheckBox ID="cbSetCultureCodeForSite" Text="Site" runat="server"/><br />
                                <D:CheckBox ID="cbSetCultureCodeForSiteTemplate" Text="SiteTemplate" runat="server"/><br />                                
                                <D:CheckBox ID="cbSetCultureCodeForUIFooterItem" Text="UI footer item" runat="server"/><br />
                                <D:CheckBox ID="cbSetCultureCodeForUITab" Text="UI tab" runat="server"/><br />
                                <D:CheckBox ID="cbSetCultureCodeForUIWidget" Text="UI widget" runat="server"/><br />
                                <D:CheckBox ID="cbSetCultureCodeForVenueCategory" Text="Venue category" runat="server"/><br />
                                <br />
                                <D:Button ID="btnSetCultureCodesForCustomTextsWithout" runat="server" Text="Do it" />
                                <D:Button ID="btnSetCultureCodesForCustomTextsWithoutTest" runat="server" Text="Test" />
                                <br />
                                <D:LabelTextOnly runat="server" ID="lblSetCultureCodeForEmptyCustomTextsResult" LocalizeText="false"></D:LabelTextOnly>
                            </td>
                        </tr>
                    </table> 
                    <hr />
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:LabelTextOnly runat="server" id="lblFixFormattingIssueForItalicAndBold">Fix formatting for italic/bold issue</D:LabelTextOnly>
                            </td>
                            <td class="control">                                
                                <D:Button ID="btnFixFormattingIssueForItalicAndBold" runat="server" Text="Do it" Visible="false" />
                                <D:Button ID="btnFixFormattingIssueForItalicAndBoldTest" runat="server" Text="Test" />
                                <br />
                                <D:LabelTextOnly runat="server" ID="lblFixFormattingIssueForItalicAndBoldResult" LocalizeText="false"></D:LabelTextOnly>
                            </td>
                        </tr>
                    </table>
                    <hr />
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:LabelTextOnly runat="server" ID="LabelTextOnly11">Decode native site texts</D:LabelTextOnly>
                            </td>
                            <td class="control">
                                <D:Button ID="btnDecodeSites" runat="server" Text="Decode" />
                            </td>
                        </tr>
                    </table>                    
                    <hr />
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:LabelTextOnly runat="server" ID="LabelTextOnly12" Text="Sync country codes"></D:LabelTextOnly>
                            </td>
                            <td class="control">
                                <D:Button runat="server" ID="btnSyncCountryCodes" Text="Sync"/>
                            </td>
                        </tr>
                    </table>
                    <hr/>
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:LabelTextOnly runat="server" ID="LabelTextOnly98">Upload delivery time to cloud</D:LabelTextOnly>
                            </td>
                            <td class="control">
                                <D:Button ID="btnUploadDeliveryTime" runat="server" Text="To the cloud and beyond!" />
                            </td>
                        </tr>
                    </table>                    
                    <hr />
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:LabelTextOnly runat="server" ID="LabelTextOnly9">Re-upload media linked to unknown device type</D:LabelTextOnly>
                            </td>
                            <td class="control">
                                <D:Button ID="btnReuploadMediaForUnknownDeviceType" runat="server" Text="Start Uploading"/>
                                <D:Button ID="btnReuploadMediaForUnknownDeviceTypeTest" runat="server" Text="Get media count to reupload"/>&nbsp;<D:LabelTextOnly runat="server" ID="lblReuploadMediaForUnknownDeviceType" LocalizeText="false"></D:LabelTextOnly>
                            </td>
                        </tr>
                    </table>                    
                    <hr />
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:LabelTextOnly runat="server" ID="LabelTextOnly99">Copy site images to new ratios</D:LabelTextOnly>
                            </td>
                            <td class="control">
                                <D:Button ID="btnCopySiteImages" runat="server" Text="Copy site images" />                                
                            </td>
                        </tr>
                    </table>                                        
                    <hr />
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:LabelTextOnly runat="server" ID="LabelTextOnly10">New site images cutouts</D:LabelTextOnly>
                            </td>
                            <td class="control">
                                <D:Button ID="btnNewSiteImagesCutoutsTest" runat="server" Text="Test" /><br />
                                <D:Button ID="btnNewSiteImagesCutouts" runat="server" Text="Cut it out!" />                                
                                <br />
                                <D:LabelTextOnly runat="server" ID="lblNewSiteCutouts" LocalizeText="false"></D:LabelTextOnly>
                            </td>
                        </tr>
                    </table> 
                    
                    <D:Panel ID="pnl201705" runat="server" GroupingText="Sprint 2017-5" Visible="False">
                        <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:LabelTextOnly runat="server" ID="LabelTextOnly13">Set override subtype on products with generic</D:LabelTextOnly>
                            </td>
                            <td class="control">
                                <D:Button ID="btnSetOverrideSubTypeOnProducts" runat="server" Text="Update" /> (RUN ONCE!!)
                            </td>
                        </tr>
                        </table> 
                        <tr>
                            <td class="label">
                                <D:LabelTextOnly runat="server" ID="LabelTextOnly14">Set company names in IR configurations</D:LabelTextOnly>
                            </td>
                            <td class="control">
                                <D:Button ID="btnSetCompanyNamesInIrConfigurations" runat="server" Text="Do it" /> (RUN ONCE!!)
                            </td>
                        </tr>
                        </table> 
                    </D:Panel>
                    
                    <D:Panel ID="pnl2018_01" runat="server" GroupingText="Sprint 2018-1" Visible="False">
                        <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:LabelTextOnly runat="server" ID="lblConvertGenericBrandProducts">Convert brand products to new situation</D:LabelTextOnly>
                            </td>
                            <td class="control">
                                <D:Button ID="btnConvertGenericBrandProducts" runat="server" Text="Convert the shizzle" />
                            </td>
                        </tr>
                            <tr>
                                <td class="label">
                                    <D:LabelTextOnly runat="server" ID="lblConvertGenericBrandProductsMedia">Convert old brand products media to new situation</D:LabelTextOnly>
                                </td>
                                <td class="control">
                                    <D:Button ID="btnConvertGenericBrandProductsMedia" runat="server" Text="Convert the shizzle" />
                                </td>
                            </tr>
                        </table> 
                        <hr />
                        <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:LabelTextOnly runat="server" ID="LabelTextOnly16">Set utc field on occurrence entities</D:LabelTextOnly>
                            </td>
                            <td class="control">
                                <D:Button ID="btnSetUtcFieldsOnOccurrenceEntities" runat="server" Text="Fill" />
                            </td>
                        </tr>
                        </table>
                    </D:Panel>
                    
				    <D:Panel ID="pnl2018_02" runat="server" GroupingText="Sprint 2018-2" Visible="False">
                        <table class="dataformV2">
				            <tr>
				                <td class="label">
				                    <D:LabelTextOnly runat="server" ID="LabelTextOnly88">Create pms report configurations</D:LabelTextOnly>
				                </td>
				                <td class="control">
				                    <D:Button ID="btnCreatePmsReportConfigurations" runat="server" Text="Create" />
				                </td>
				            </tr>
                        </table>

                        <hr />

                        <table class="dataformV2">
				            <tr>
				                <td class="label">
				                    <D:LabelTextOnly runat="server" ID="LabelTextOnly89">Set utc fields (new api)</D:LabelTextOnly>
				                </td>
				                <td class="control">
				                    <D:Button ID="btnSetUtcFields" runat="server" Text="Set" />
				                </td>
				            </tr>
                        </table>
                    </D:Panel>
                
                
				    <D:Panel ID="pnl2018_03" runat="server" GroupingText="Sprint 2018-3" Visible="False">
				        <table class="dataformV2">
				            <tr>
				                <td class="label">
				                    <D:LabelTextOnly runat="server" ID="lblMigrateDPGText">Migrate DPG translations to existing ClientConfiguration</D:LabelTextOnly>
				                </td>
				                <td class="control">
				                    <D:Button ID="btnMigrateDeliverypointgroupTranslationsToClientConfig" runat="server" Text="Migrate" />
				                </td>
				            </tr>
				        </table>

				        <hr />
                    </D:Panel>

				    <D:Panel ID="pnl2019_01" runat="server" GroupingText="Sprint 2019-1" Visible="False">
				        <table class="dataformV2">
				            <tr>
				                <td class="label">
				                    <D:LabelTextOnly runat="server" ID="lblSetAllowPublishingFlagsOnUsers">Set AllowPublishing flags on users</D:LabelTextOnly>
				                </td>
				                <td class="control">
				                    <D:Button ID="btnSetAllowPublishingFlagsOnUsers" runat="server" Text="Set" />
				                </td>
				            </tr>
				        </table>

				        <hr />
                    </D:Panel>
                
                    <D:Panel ID="pnl2019_10" runat="server" GroupingText="Sprint 2019-10" Visible="False">
                        <table class="dataformV2">
                            <tr>
                                <td class="label">
                                    <D:LabelTextOnly runat="server" ID="lblConvertChannels">Convert SortOrder to Channel for stations</D:LabelTextOnly>
                                </td>
                                <td class="control">
                                    <D:Button ID="btnConvertChannels" runat="server" Text="Convert" />
                                </td>
                            </tr>
                        </table>

                        <hr />
                    </D:Panel>
                
                <D:Panel ID="pnl2020_01" runat="server" GroupingText="Sprint 2020-01" Visible="False">
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:LabelTextOnly runat="server" ID="lblConvertBrandProducts">Convert brand products</D:LabelTextOnly>
                            </td>
                            <td class="control">
                                <D:Button ID="btnConvertBrandProducts" runat="server" Text="Convert" />
                            </td>
                        </tr>
                    </table>

                    <hr />
                </D:Panel>
                
                <D:Panel ID="pnl2020_03" runat="server" GroupingText="Sprint 2020-03" Visible="False">
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:LabelTextOnly runat="server" ID="lblInitOutletOperationalState">Create initial OutletOperationalState</D:LabelTextOnly>
                            </td>
                            <td class="control">
                                <D:Button ID="btnInitOutletOperationalState" runat="server" Text="Plug into the matrix" />
                            </td>
                        </tr>
                    </table>

                    <hr />
                </D:Panel>

                <D:Panel ID="pnl14_0" GroupingText="Sprint 2020-05 (1.4)" runat="server" LocalizeText="False">
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:LabelTextOnly runat="server" ID="lblFillReceiptTemplateEmail">Fill Receipt Template Vendor Email</D:LabelTextOnly>
                            </td>
                            <td class="control">
                                <D:Button ID="btnFillReceiptTemplateEmail" runat="server" Text="Copy from Company" />
                                <br />
                                <D:LabelTextOnly runat="server" ID="lblReceiptTemplateNoEmail" LocalizeText="false"></D:LabelTextOnly>
                            </td>
                        </tr>
                    </table>

                    <hr />

                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                Generate outlet system products
                            </td>
                            <td class="control">
                                <D:Button ID="btnGenerateOutletSystemProducts" Text="Generate" runat="server" />
                                <D:Button ID="btnGenerateOutletSystemProductsTest" runat="server" Text="Test" />
                                <br />
                                <D:LabelTextOnly runat="server" ID="lblGenerateOutletSystemProductsOutput" LocalizeText="false"></D:LabelTextOnly>
                            </td>
                        </tr>
                    </table>

                    <hr />

                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                Restore all system product their sub types
                            </td>
                            <td class="control">
                                <D:Button ID="btnRestoreOutOfSyncTippingSystemSubTypes" Text="Restore" runat="server" />
                                <D:Button ID="btnRestoreOutOfSyncTippingSystemSubTypesTest" Text="Test" runat="server" />
                                <br />
                                <D:LabelTextOnly runat="server" ID="lblRestoreAllSystemSubTypes" LocalizeText="false"></D:LabelTextOnly>
                            </td>
                        </tr>
                    </table>

                    <hr />
                    
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                Set MinimumAmountForFreeServiceCharge for service methods
                            </td>
                            <td class="control">
                                <D:Button ID="btnSetMinimumAmountForFreeServiceCharge" runat="server" Text="Set" />
                                <D:Button ID="btnSetMinimumAmountForFreeServiceChargeTest" runat="server" Text="Test" />
                                <br />
                                <D:LabelTextOnly runat="server" ID="lblSetMinimumAmountForFreeServiceChargeOutput" LocalizeText="false"></D:LabelTextOnly>
                            </td>
                        </tr>
                    </table>

                    <hr />
                    
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                Generate default delivery distance for delivery service methods
                            </td>
                            <td class="control">
                                <D:Button ID="btnGenerateDeliveryDistanceForDeliveryServiceMethods" runat="server" Text="Generate" />
                                <D:Button ID="btnGenerateDeliveryDistanceForDeliveryServiceMethodsTest" runat="server" Text="Test" />
                                <br />
                                <D:LabelTextOnly runat="server" ID="lblGenerateDeliveryDistanceForDeliveryServiceMethodsOutput" LocalizeText="false"></D:LabelTextOnly>
                            </td>
                        </tr>
                    </table>
                    
                    <hr />

                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                Set service method types based on the current data
                            </td>
                            <td class="control">
                                <D:Button ID="btnSetServiceMethodTypes" runat="server" Text="Set" />
                                <D:Button ID="btnTestServiceMethodTypes" runat="server" Text="Test" />
                                <br />
                                <D:LabelTextOnly runat="server" ID="lblSetServiceMethodTypes" LocalizeText="false"></D:LabelTextOnly>
                            </td>
                        </tr>
                    </table>
                </D:Panel>

                <D:Panel ID="pnlSprint202006_1_5_1" GroupingText="Sprint 2020-06 (1.5.1)" runat="server" LocalizeText="False">
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                Convert payment integration configuration to the new mechanism.
                            </td>
                            <td class="control">
                                <D:Button ID="btnPaymentIntegrationConfigurationConversion" Text="Convert" runat="server" />
                                <D:Button ID="btnTestPaymentIntegrationConfigurationConversion" Text="Test" runat="server" />
                                <br /><br />
                                <D:LabelTextOnly ID="lblPaymentIntegrationConfigurationConversion" LocalizeText="false" runat="server"></D:LabelTextOnly>
                            </td>
                        </tr>
                    </table>
                </D:Panel>
                
				<D:Panel ID="pnl_appless_1_6_0" runat="server" GroupingText="AppLess (1.6.0)" LocalizeText="false">
					<table class="dataformV2">
						<tr>
							<td class="label">
								Set missing default translations for appless
							</td>
							<td class="control">
								<D:Button ID="btnSetMissingTranslations" runat="server" Text="Do it" LocalizeText="false" />
							</td>
						</tr>
						<tr>
                            <td class="label">
                                <D:LabelTextOnly runat="server" ID="lblCopyReceiptTemplateInfo">Copy receipt template to outlet seller information</D:LabelTextOnly>
                            </td>
                            <td class="control">
                                <D:Button ID="btnCopyReceiptTemplateToOutletSeller" runat="server" Text="Copy Data" OnClick="btnCopyReceiptTemplateToOutletSeller_OnClick" />
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                <D:LabelTextOnly runat="server" ID="lblConvertSendReceipt" LocalizeText="False">Convert SendReceipt Routestephandlers</D:LabelTextOnly>
                            </td>
                            <td class="control">
                                <D:Button ID="btnConvertSendReceipt" runat="server" Text="Convert to EmailOrder" OnClick="btnConvertSendReceipt_OnClick" /> | <D:Button ID="btnConvertSendReceiptTest" runat="server" Text="Convert to EmailOrder (TEST)" OnClick="btnConvertSendReceiptTest_OnClick" /><br/>
                                <D:LabelTextOnly ID="lblConvertSendReceiptResult" LocalizeText="false" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                Convert EmailOrder routestep template vars
                            </td>
                            <td class="control">
                                <D:Button ID="btnConvertEmailOrderTempalteVars" runat="server" Text="Convert Template Variables" LocalizeText="false" OnClick="btnConvertEmailOrderTempalteVars_OnClick" /><br/>
                                <D:LabelTextOnly ID="lblConvertEmailOrderTemplateReslut" LocalizeText="false" runat="server" />
                            </td>
                        </tr>
						<tr>
							<td class="label">
								<D:LabelTextOnly runat="server" ID="lblSetDevsTesters">Set tester / developer flag for users</D:LabelTextOnly>
							</td>
							<td class="control">
								<D:Button ID="btnSetDevTestUserFlag" runat="server" Text="Set flag" OnClick="SetTestDeveloperUserFlags" />
							</td>
						</tr>
					</table>
                </D:Panel>

                    <D:Panel ID="pnl_appless_1_7_0" GroupingText="AppLess (1.7.0)" LocalizeText="false" runat="server">
                        <table class="dataformV2">
						    <tr>
							    <td class="label">
                                    <D:LabelTextOnly ID="lblConvertTermsAndConditionsCustomText" runat="server">Convert terms and conditions custom text values</D:LabelTextOnly>
							    </td>
                                <td class="control">
                                    <D:Button ID="btnConvertTermsAndConditionsCustomText" Text="Convert" runat="server" />
                                    <D:Button ID="btnTestConvertTermsAndConditionsCustomText" runat="server" Text="Test" />
                                    <br /><br />
                                    <D:LabelTextOnly ID="lblConvertTermsAndConditionsCustomTextResult" LocalizeText="false" runat="server"></D:LabelTextOnly>
                                </td>
                            </tr>
                        </table>
                    </D:Panel>
                
                <D:Panel ID="pnl_release_20" GroupingText="Release #20" LocalizeText="false" runat="server">
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                Convert report template filters
                            </td>
                            <td class="control">
                                <D:Button ID="btnConvertReportTemplateFilters" Text="Convert" runat="server" OnClick="btnConvertReportTemplateFilters_OnClick" />
                            </td>
                        </tr>
                    </table>
                </D:Panel>

                <D:Panel ID="pnlRelease21" GroupingText="Release #21" LocalizeText="false" runat="server">
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                Apply 0% tax tariff on unconfigured system products
                            </td>
                            <td class="control">
                                <D:Button ID="btnConvertNonConfiguredTaxTariffsForSysProducts" Text="Convert" runat="server" OnClick="btnConvertNonConfiguredTaxTariffsForSysProducts_OnClick" />
                                <br /><br />
                                <D:LabelTextOnly ID="lblConvertNonConfiguredTaxTariffsForSysProducts" LocalizeText="false" runat="server"></D:LabelTextOnly>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                Create 0% tax tariff for non-AppLess companies
                            </td>
                            <td class="control">
                                <D:Button ID="btnCreateZeroTaxTariffNonAppLess" Text="Convert" runat="server" OnClick="btnCreateZeroTaxTariffNonAppLess_OnClick" />
                                <br /><br />
                                <D:LabelTextOnly ID="lblCreateZeroTaxTariffNonAppLess" LocalizeText="false" runat="server" />
                            </td>
                        </tr>
                    </table>
                </D:Panel>

                <D:Panel ID="pnlRelease23" GroupingText="Release #23" LocalizeText="false" runat="server">
	                <table class="dataformV2">
		                <tr>
			                <td class="label">
				                Add a free of charge checkout method to all outlets
			                </td>
			                <td class="control">
				                <D:Button ID="btnCreateFreeOfChargeCheckoutMethod" Text="Create checkout methods" runat="server" OnClick="btnCreateFreeOfChargeCheckoutMethod_OnClick" />
				                <br /><br />
				                <D:LabelTextOnly ID="lblCreateFreeOfChargeCheckoutMethod" LocalizeText="false" runat="server" />
			                </td>
		                </tr>

                        <tr>
                            <td class="label">
                                Remove DPG relations from pick-up and delivery service methods
                            </td>
                            <td class="control">
                                <D:Button ID="btnRelease23RemoveDpgs" Text="Remove" LocalizeText="false" OnClick="btnRelease23RemoveDpgs_OnClick" runat="server" />
                                <D:Button ID="btnRelease23RemoveDpgsTest" Text="Test" LocalizeText="false" OnClick="btnRelease23RemoveDpgsTest_OnClick" runat="server" />
				                <br /><br />
				                <D:LabelTextOnly ID="lblRelease23RemoveDpgs" LocalizeText="false" runat="server" />
                            </td>
                        </tr>
	                </table>
                </D:Panel>

                <D:Panel ID="pnlRelease27" GroupingText="Release #27" LocalizeText="false" runat="server">
                    <table class="dataformV2">
                    <tr>
                        <td class="label">Convert Order/SMS Ack Routestephandlers</td>
                        <td class="control">
                            <D:Button ID="btnActRoutestephandlers" Text="Convert Act Routestephanlders" runat="server" OnClick="btnConvertActRoutestephandlers_OnClick" />
                        </td>
                    </tr>
                    </table>
                </D:Panel>
                
                <D:Panel ID="pnlRelease28" GroupingText="Release #28" LocalizeText="false" runat="server">
	                <table class="dataformV2">
		                <tr>
			                <td class="label">Update Opt-Ins with Customer Name</td>
			                <td class="control">
				                <D:Button ID="btnUpdateOptInsWithCustomerName" Text="Update" runat="server" OnClick="btnUpdateOptInsWithMissingCustomerName" />
				                <D:Button ID="btnUpdateOptInsWithCustomerNameTest" Text="Test" runat="server" OnClick="btnUpdateOptInsWithMissingCustomerName" />
			                </td>
		                </tr>
	                </table>
                </D:Panel>
                
                <D:Panel ID="pnlRelease29" GroupingText="Release #29" LocalizeText="false" runat="server">
                    <table class="dataformV2">
                        <tr>
                            <td class="label">Set dev/test companies</td>
                            <td class="control">
                                <D:Button ID="btnSetDevTestCompanies" Text="Update" runat="server" OnClick="btnUpdateDevTestCompanies" />
                            </td>
                        </tr>
	                    <tr> 
		                    <td class="label">Fix Category path of order items</td> 
		                    <td class="control"> 
			                    <D:Button ID="btnFixCategoryPath" Text="Update" runat="server" OnClick="UpdateCategoryPath" /> 
			                    <D:Button ID="btnFixCategoryPathTest" Text="Test" runat="server" OnClick="UpdateCategoryPath" /> 
		                    </td> 
	                    </tr> 
                    </table>
                </D:Panel>
                    
                 
                <D:Panel ID="pnlRelease46" GroupingText="Release #46" LocalizeText="false" runat="server">
                    <table class="dataformV2">
                        <tr>
                            <td class="label">Migrate Affiliate Book Online Attachments to Web Link Products</td>
                            <td class="control">
                                <D:Button ID="btnMigrateAffiliateBookOnline" Text="Migrate" runat="server" OnClick="MigrateAffiliateBookOnline" />
                            </td>
                        </tr> 
                    </table>
                </D:Panel>
                                    
                <D:Panel ID="pnlRelease48" GroupingText="Release #48" LocalizeText="false" runat="server">
                        <table class="dataformV2">
                            <tr>
                                <td class="label">Create Golden Tours, Tiqets and Ingresso Affiliate Partners</td>
                                <td class="control">
                                    <D:Button ID="btnCreateAffiliatePartners" Text="Create" runat="server" OnClick="CreateAffiliatePartners" />
                                </td>
                            </tr>
                        </table>
                </D:Panel>

                </controls>
            </X:TabPage>

            <X:TabPage Text="Aria" Name="Aria">
                <controls>
				    <table class="dataformV2">
				        <tr>
                            <td class="control" width="100%">
                                <strong>General:</strong>
				                <ul style="list-style-type: disc">
				                    <li>"Test" buttons will only return the affected clients without making any changes to clients or devices</li>
                                    <li>"Floor" fields can be used to select whole floors (eg. 3 or 3-), floor parts (eg. 3-1 or 3-21) or a room (eg. 3-247). Can be left empty for non Aria companies.</li>
				                </ul>
                            </td>
				        </tr>
				    </table>
                    
                    <hr />
                    
                    <table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelTextOnly runat="server" id="lblEmenuConnectionSwitcher" LocalizeText="false">Emenu connection switcher</D:LabelTextOnly>
							</td>
							<td class="control">
                                Result:<D:LabelTextOnly runat="server" ID="lblEmenuConnectionSwitcherResult" LocalizeText="false"></D:LabelTextOnly><br/>
                                <X:ComboBoxLLBLGenEntityCollection ID="cbSwitcherCompany" runat="server" EntityName="Company" PreventEntityCollectionInitialization="true"></X:ComboBoxLLBLGenEntityCollection><br />
                                <D:TextBoxInt ID="tbEmenuSwitcherAmount" runat="server" Text="50" /><br/>
                                <D:Button ID="btnGetEmenusRunningWebserviceMode" runat="server" Text="Get Emenu's running webservice mode" /><br/>
                                <D:Button ID="btnSwitchEmenuToWebservice" runat="server" Text="Switch to webservice" /><D:Button ID="btnSwitchEmenusToWebserviceTest" runat="server" Text="Test" /><br/>
                                <D:Button ID="btnSwitchEmenuToSignalR" runat="server" Text="Switch to SignalR" /><D:Button ID="btnSwitchEmenusToSignalRTest" runat="server" Text="Test" />
                            </td>
                            <td>
                            </td>
                            <td>                                
                            </td>
                        </tr>
                    </table>
                    
                    <hr />
                    
                    <table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelTextOnly runat="server" id="lblSupportToolsConnectionSwitcher" LocalizeText="false">SupportTools connection switcher</D:LabelTextOnly>
							</td>
							<td class="control">
                                Result:<D:LabelTextOnly runat="server" ID="lblSupportToolsConnectionSwitcherResult" LocalizeText="false"></D:LabelTextOnly><br/>                                
                                <D:TextBoxInt ID="tbSupportToolsSwitcherAmount" runat="server" Text="50" /><br/>
                                <D:Button ID="btnGetSupportToolsRunningWebserviceMode" runat="server" Text="Get SupportTools running webservice mode" /><br/>
                                <D:Button ID="btnSwitchSupportToolsToSignalR" runat="server" Text="Switch to signalR" /><D:Button ID="btnSwitchSupportToolsToSignalRTest" runat="server" Text="Test" /><br/>                                
                            </td>
                            <td>
                            </td>
                            <td>                                
                            </td>
                        </tr>
                    </table>

				    <hr/>

                    <table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelTextOnly runat="server" id="lblLinkRoomControlAreasToClients">Link room control areas to clients</D:LabelTextOnly>
							</td>
							<td class="control">
							    Company Id: <D:TextBoxString runat="server" ID="tbLinkRoomControlAreasCompanyId" Width="50" Text="343"/><br/>
                                <D:Button ID="btnLinkRoomControlAreasToClients" runat="server" Text="Link" PreSubmitWarning="Are you sure you want to link the room control areas to the corresponding clients?" />
                                <D:Button ID="btnLinkRoomControlAreasToClientsTest" runat="server" Text="Test"/><br/>
                                <br/>
                                <D:LabelTextOnly runat="server" ID="lblLinkRoomControlAreas" LocalizeText="false"></D:LabelTextOnly>
							</td>
                            <td></td>
                        </tr>                        
                    </table>
                    
                    <hr />
                    
                    <table class="dataformV2">
                        <tr>
							<td class="label">
								<D:LabelTextOnly runat="server" id="LabelTextOnly6">Change deliverypointgroup</D:LabelTextOnly>
							</td>
							<td class="control" colspan="3">
							    Change the deliverypointgroup to "To DPG" for all clients which are currently on "From DPG"
                            </td>                        
                        </tr>
                        <tr>
                            <td class="label"></td>
                            <td class="control">
                                Room numbers: <D:TextBoxString runat="server" ID="tbChangeDpgRoomNumbers" Width="100%"></D:TextBoxString>
                                <br /><br />
                                Floor: <D:TextBoxString runat="server" ID="tbChangeDpgFloor" Width="50"/>
                                &nbsp;
                                Amount: <D:TextBoxString runat="server" ID="tbChangeDpgAmount" Width="50" Text="150"/>
                                <br /><br />
                                Company: <b><%= string.Format("{0}", CmsSessionHelper.CurrentCompanyName) %></b><br/><br/>
							    From DPG: <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlChangeDpgFromDpgId" IncrementalFilteringMode="StartsWith" EntityName="Deliverypointgroup" TextField="Name" ValueField="DeliverypointgroupId" PreventEntityCollectionInitialization="true"/><br/>
                                To DPG: <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlChangeDpgToDpgId" IncrementalFilteringMode="StartsWith" EntityName="Deliverypointgroup" TextField="Name" ValueField="DeliverypointgroupId" PreventEntityCollectionInitialization="true"/><br/>
							    <br/>
                                <D:Button ID="btnChangeDpg" runat="server" Text="Change DPG" />&nbsp;<D:Button ID="btnChangeDpgTest" runat="server" Text="Test" /><br/>
                                <br />
                                <D:LabelTextOnly runat="server" ID="lblChangeDpgRoomsChanged" LocalizeText="false"></D:LabelTextOnly>
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>

				    <hr/>
                    
                    <table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelTextOnly runat="server" id="LabelTextOnly1" LocalizeText="false">Update to 20151126</D:LabelTextOnly>
							</td>
							<td class="control">
                                Floor: <D:TextBoxString runat="server" ID="tbFloorUpdate" Width="50"/>&nbsp;
                                Amount: <D:TextBoxString runat="server" ID="tbAmountUpdate" Width="50"/>&nbsp;
							    Company Id: <D:TextBoxString runat="server" ID="tbCompanyUpdate" Width="50" Text="343"/>
                                <br /><br />
							    Send command to clients to download the update (APK)<br/>
							    <D:Button ID="btnDownloadUpdateDevicesLive" runat="server" Text="Download update"/>&nbsp;
							    <D:Button ID="btnDownloadUpdateDevicesLiveTest" runat="server" Text="Test"/><br/>
							    <br/>
							    Send command to clients which are finished downloading to install the update<br/>
                                <D:Button ID="btnUpdateDevicesLive" runat="server" Text="Update   devices" />&nbsp;
							    <D:Button ID="btnUpdateDevicesLiveTest" runat="server" Text="Test"/><br/>
                                <br/>
							    Send command to clients which have started the installation<br/>
                                <D:Button ID="btnRestartSucceededLive" runat="server" Text="Restart succeeded" />&nbsp;
							    <D:Button ID="btnRestartSucceededLiveTest" runat="server" Text="Test"/><br/>
							    <br/>
							    Due to a bug in previous live version (150818xx), it's advised to do a complete floor reboot when you're done installing<br/>
							    <br/>
                                <D:LabelTextOnly runat="server" ID="lblUpdateRooms" LocalizeText="false"></D:LabelTextOnly>
							</td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>
                    
                    <hr />
                    
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:LabelTextOnly runat="server" id="LabelTextOnly2" LocalizeText="false">Reboot devices with inactive SupportTools</D:LabelTextOnly>
                            </td>
                            <td class="control" colspan="3">
                                This will send a reboot command to all clients which:
                                <ul style="list-style-type: disc">
                                    <li>Emenu last request was maximum 3 minutes ago</li>
                                    <li>SupportTools last request was longer than 3 minutes ago</li>
                                </ul>
                            </td>
                        </tr>
						<tr>
							<td class="label"></td>
							<td class="control">
							    Floor: <D:TextBoxString runat="server" ID="tbSupportToolsFloor" Width="50"/>&nbsp;
							    Company Id: <D:TextBoxString runat="server" ID="tbSupportToolsCompany" Width="50" Text="343"/><br/>
							    <br/>
                                <D:Button ID="btnSupportToolsReboot" runat="server" Text="Reboot devices" />
                                &nbsp;
							    <D:Button ID="btnSupportToolsRebootTest" runat="server" Text="Test"/>
							    <br/><br/>
                                <D:LabelTextOnly runat="server" ID="lblSupportToolsDevices" LocalizeText="false"></D:LabelTextOnly>
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>
                    
                    <hr />
                    
                    <table class="dataformV2">
						<tr>
							<td class="label">
							    <D:LabelTextOnly runat="server" id="LabelTextOnly4" LocalizeText="false">Restart Emenu</D:LabelTextOnly>
							</td>
							<td class="control">
							    Floor: <D:TextBoxString runat="server" ID="tbRestartFloor" Width="50"/>&nbsp;
							    Company Id: <D:TextBoxString runat="server" ID="tbRestartCompany" Width="50" Text="343"/><br/>
							    <br/>
                                This will send a restart Emenu command to all clients which last request was maximum 3 minutes ago.<br/>
                                <D:Button ID="btnRestartEmenu" runat="server" Text="Restart Emenu" />&nbsp;
							    <D:Button ID="btnRestartEmenuTest" runat="server" Text="Test"/>
							    <br/><br/>
							    When using the v26 button, an additional filter is added to only restart Emenu with version 15111026.<br/>
                                <D:Button ID="btnRestartEmenuNew" runat="server" Text="Restart Emenu (v26 only)" />&nbsp;
							    <D:Button ID="btnRestartEmenuNewTest" runat="server" Text="Test"/>
							    <br/><br/>
                                <D:LabelTextOnly runat="server" ID="lblRestartedEmenu" LocalizeText="false"></D:LabelTextOnly>
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>
                    
                    <hr />
                    
                    <table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelTextOnly runat="server" id="LabelTextOnly5" LocalizeText="false">Reboot Floor</D:LabelTextOnly>
							</td>
							<td class="control">
							    Floor: <D:TextBoxString runat="server" ID="tbRebootFloor" Width="50"/>&nbsp;
							    Company Id: <D:TextBoxString runat="server" ID="tbRebootCompany" Width="50" Text="343"/><br/>
							    <br/>
                                Send a reboot device command to all clients which last request (Emenu or SupportTools) was mamimum 3 minutes ago.<br/>
                                <D:Button ID="btnRebootFloor" runat="server" Text="Reboot Floor" />&nbsp;
							    <D:Button ID="btnREbootFloorTest" runat="server" Text="Test"/>
							    <br/><br/>
							    When using the non-v26 button, an additional filter is added to only reboot devices which have an older Emenu version installed then 15111026<br/>
                                <D:Button ID="btnRebootFloorNew" runat="server" Text="Reboot Floor (non v26 only)" />&nbsp;
							    <D:Button ID="btnRebootFloorNewTest" runat="server" Text="Test"/>
							    <br/><br/>
                                <D:LabelTextOnly runat="server" ID="lblRebootFloor" LocalizeText="false"></D:LabelTextOnly>
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>
                    
                    <hr />
                    
                    <table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelTextOnly runat="server" id="LabelTextOnly3" LocalizeText="false">Cleanup device storage</D:LabelTextOnly>
							</td>
							<td class="control">
							    Company Id: <D:TextBoxString runat="server" ID="tbCleanStorageCompany" Width="50" Text="343"/><br/>
							    <br/>
                                Send a cleanup command to remove all misplaced log files from storage. Send to all clients with active SupportTools for selected company.<br/>
							    <br/>
							    Can also manually run the following command from the device shell:<br/>
							    <pre width="30">shell rm -rf sdcard/Log/* && rm -rf sdcard/logs/*</pre><br/>
							    <strong>Note:</strong> Versions 2015111035 and up do this automatically on Emenu startup<br/>
                                <D:Button ID="btnCleanStorage" runat="server" Text="Clean Storage" />&nbsp;
							    <D:Button ID="btnCleanStorageTest" runat="server" Text="Test"/>
							    <br/><br/>
                                <D:LabelTextOnly runat="server" ID="lblCleanStorage" LocalizeText="false"></D:LabelTextOnly>
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>
                                        
                    <hr />
                    
                    <table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelTextOnly runat="server" id="lblRestartClientsStuckInLoading" LocalizeText="false">Restart clients stuck in loading</D:LabelTextOnly>
							</td>
							<td class="control">
							    Company Id: <D:TextBoxString runat="server" ID="tbRestartUnloadedClientsCompany" Width="50" Text="343"/><br /><br />
                                Send a restart command to all clients which are stuck in the loading procedure and have an active SupportTools connection.<br/><br/>
                                Restart <D:TextBoxInt ID="tbNumberOfClients" runat="server" Text="5" Width="20" Style="text-align: center" /> clients every <D:TextBoxInt ID="tbNumberOfSeconds" runat="server" Text="5" Width="20" Style="text-align: center"/> seconds.<br /><br />
							    <D:CheckBox ID="cbUnloadedClientsClearLogs" Text="Clear logs before restart" runat="server"/><br />
                                <D:CheckBox ID="cbUnloadedClientsClearCachedXmlFiles" Text="Clear cached .xml files before restart" runat="server"/><br />
							    <br/>
                                <D:Button ID="btnRestartUnloadedClients" runat="server" Text="Restart Emenu" />&nbsp;
							    <D:Button ID="btnRestartUnloadedClientsTest" runat="server" Text="Test"/>
							    <br/><br/>
                                <D:LabelTextOnly runat="server" ID="lblUnloadedClients" LocalizeText="false"></D:LabelTextOnly>
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>                                       

                </controls>
            </X:TabPage>
            <X:TabPage Text="Misc" Name="Misc">
                <controls>

                    <table class="dataformV2">
						<tr>
							<td class="label">
                                <D:LabelTextOnly runat="server" id="lblFixLeBonne" LocalizeText="false">Fix Le Bonne</D:LabelTextOnly>
                            </td>
                            <td class="control">
                                <D:Button ID="btnFixLeBonne" runat="server" Text="Fix it!" />&nbsp;
                            </td>
                        </tr>
                    </table>
                    
				    <hr />
                    
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                Unprocessable Orders (min. 24h old)
                            </td>
                            <td class="control">
                                <strong><D:LabelTextOnly runat="server" ID="lblUnprocessableOrdersCount" LocalizeText="False" ></D:LabelTextOnly></strong> &nbsp; <D:Button ID="btnProcessOrders" runat="server" Text="Manual Process All Orders" OnClick="btnProcessOrders_OnClick" />
                            </td>
                        </tr>
                    </table>
                    
                    <hr />
                    
				    <table class="dataformV2">
				    <tr>
				        <td class="label">
				            <D:LabelTextOnly runat="server" LocalizeText="false">Copy Room Control info between DPGs</D:LabelTextOnly>
				        </td>
				        <td class="control">
				            From DPG: <X:ComboBoxLLBLGenEntityCollection runat="server" ID="cbRoomControlCopyDpgFrom" IncrementalFilteringMode="StartsWith" EntityName="Deliverypointgroup" TextField="Name" ValueField="DeliverypointgroupId" PreventEntityCollectionInitialization="true"/><br/>
				            To DPG: <X:ComboBoxLLBLGenEntityCollection runat="server" ID="cbRoomControlCopyDpgTo" IncrementalFilteringMode="StartsWith" EntityName="Deliverypointgroup" TextField="Name" ValueField="DeliverypointgroupId" PreventEntityCollectionInitialization="true"/><br/>
                            <br/>
				            <D:Button ID="btnCopyRoomControlInfo" runat="server" Text="Copy" /> | <D:Button ID="btnCopyRoomControlInfoTest" runat="server" Text="Copy (Test)" /><br />
				            <D:LabelTextOnly runat="server" ID="lblCopyRoomControlInfo" LocalizeText="false"></D:LabelTextOnly>
				        </td>
				        <td></td>
				        <td></td>
				    </tr>
				    </table>
                    
                    <hr />
                    
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:LabelTextOnly runat="server" id="lblUsersWithSimplePasswords" LocalizeText="false">Users with test passwords</D:LabelTextOnly>
                            </td>
                            <td class="control">
                                <D:Button ID="btnUsersWithSimplePasswords" runat="server" Text="Show me what you've got" OnClick="btnUsersWithSimplePasswords_OnClick" /><br/>
                                <br/>
                                <D:LabelTextOnly runat="server" ID="lblUserOutput" LocalizeText="False" />
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>

                    <RoomControlConfiguration:BulkEditor ID="RoomControlConfigurationBulkEditor" runat="server" />
                </Controls>
            </X:TabPage>
        </TabPages>
    </X:PageControl>
</asp:Content>
