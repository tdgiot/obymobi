﻿using System;
using System.Web.UI.WebControls;
using Obymobi.Cms.Logic.Factories;
using Obymobi.Enums;

namespace Obymobi.ObymobiCms.Configuration.Sys
{
    public partial class PaymentProviders : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        protected override void OnInit(EventArgs e)
        {
            this.ApplyCompanyFilterForUserRole();
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.MainGridView.HtmlRowPrepared += this.MainGridView_HtmlRowPrepared;
            this.MainGridView.Styles.Cell.HorizontalAlign = HorizontalAlign.Left;

            ((MasterPages.MasterPageEntityCollection)this.Master).ToolBar.AddButton.Visible = CmsSessionHelper.CurrentRole == Role.GodMode;

            MainGridView.ShowDeleteHyperlinkColumn = CmsSessionHelper.CurrentRole == Role.GodMode;
        }

        private void MainGridView_HtmlRowPrepared(object sender, DevExpress.Web.ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType != DevExpress.Web.GridViewRowType.Data) return;

            // Convert the operation mode enum to text
            int type = (int)e.GetValue("Type");
            {
                PaymentProviderType typeAsEnum = type.ToEnum<PaymentProviderType>();
                this.SetCellValue(e.Row, "Type", typeAsEnum.ToString());
            }

            int environment = (int)e.GetValue("Environment");
            {
                PaymentProviderEnvironment typeAsEnum = environment.ToEnum<PaymentProviderEnvironment>();
                this.SetCellValue(e.Row, "Environment", typeAsEnum.ToString());
            }
        }

        private void SetCellValue(TableRow row, string fieldName, string value)
        {
            if (this.MainGridView.Columns[fieldName] != null)
            {
                int index = this.MainGridView.Columns[fieldName].Index;
                if (index < row.Cells.Count)
                {
                    row.Cells[index].Text = value;
                }
            }
        }

        private void ApplyCompanyFilterForUserRole()
        {
            this.Filter = PredicateFactory.CreatePaymentProviderCompanyIdFilter(CmsSessionHelper.CompanyIdsForUser, CmsSessionHelper.CurrentRole);
        }
    }
}