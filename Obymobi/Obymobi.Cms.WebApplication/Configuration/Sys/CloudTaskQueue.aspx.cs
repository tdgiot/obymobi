﻿using Dionysos.Web.UI;
using System;
using Obymobi.Constants;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System.Data;

namespace Obymobi.ObymobiCms.Configuration.Sys
{
    public partial class CloudTaskQueue : PageQueryStringDataBinding
    {
        private enum FilterTypes
        {
            All,
            Completed,
            Pending,
            Failed
        }

        readonly DateTime failedTime = new DateTime(2000, 1, 1);

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
                this.BindQueryStringToControls();

            LoadTasksData();
        }

        protected override void OnInit(EventArgs e)
        {
            this.SetGui();
            base.OnInit(e);
        }

        protected override void SetDefaultValuesToControls()
        {
            this.ddlFilter.Value = (int)FilterTypes.All;
        }

        void SetGui()
        {
            // Set the filters
            this.ddlFilter.Items.Add(this.Translate(FilterTypes.All.ToString(), "All tasks"), (int)FilterTypes.All);
            this.ddlFilter.Items.Add(this.Translate(FilterTypes.Completed.ToString(), "Only completed tasks"), (int)FilterTypes.Completed);
            this.ddlFilter.Items.Add(this.Translate(FilterTypes.Pending.ToString(), "Only pending tasks"), (int)FilterTypes.Pending);
            this.ddlFilter.Items.Add(this.Translate(FilterTypes.Failed.ToString(), "Only failed tasks"), (int)FilterTypes.Failed);

            this.taskGrid.Columns["AmazonUrl"].HeaderStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Center;
            this.taskGrid.Columns["AmazonUrl"].CellStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Center;
        }

        public void Refresh()
        {
            this.RedirectWithControlValues();    
        }

        void LoadTasksData()
        {
            var selectedTaskTypes = this.cblType.SelectedValuesAsIntList();
            var selectedActionTypes = this.cblAction.SelectedValuesAsIntList();

            var filter = new PredicateExpression();
            filter.Add(CloudProcessingTaskFields.Type == selectedTaskTypes);
            filter.Add(CloudProcessingTaskFields.Action == selectedActionTypes);

            var filterType = this.ddlFilter.ValidId.ToEnum<FilterTypes>();
            var subFilter = new PredicateExpression();
            if (filterType == FilterTypes.Completed)
            {
                subFilter.Add(CloudProcessingTaskFields.CompletedOnAmazonUTC > failedTime);
            }
            else if (filterType == FilterTypes.Pending)
            {
                subFilter.Add(CloudProcessingTaskFields.CompletedOnAmazonUTC == DBNull.Value);
            }
            else if (filterType == FilterTypes.Failed)
            {
                subFilter.Add(CloudProcessingTaskFields.CompletedOnAmazonUTC == failedTime);
            }

            if (filterType != FilterTypes.All)
            {
                filter.Add(subFilter);
            }

            var collection = new CloudProcessingTaskCollection();
            collection.GetMulti(filter);
            
            // Build custom DataSource
            var tasksDataSource = new DataTable();
            var createdColumn = new DataColumn("Created");
            tasksDataSource.Columns.Add(createdColumn);
            var typeUrlColumn = new DataColumn("TypeUrl");
            tasksDataSource.Columns.Add(typeUrlColumn);
            var typeColumn = new DataColumn("Type");
            tasksDataSource.Columns.Add(typeColumn);
            var filenameColumn = new DataColumn("Filename");
            tasksDataSource.Columns.Add(filenameColumn);
            var actionColumn = new DataColumn("Action");
            tasksDataSource.Columns.Add(actionColumn);
            var amazonColumn = new DataColumn("Amazon");
            tasksDataSource.Columns.Add(amazonColumn);
            var amazonUrlColumn = new DataColumn("AmazonUrl");
            tasksDataSource.Columns.Add(amazonUrlColumn);
            var attemptsColumn = new DataColumn("Attempts");
            tasksDataSource.Columns.Add(attemptsColumn);
            var nextAttemptColumn = new DataColumn("NextAttempt");
            tasksDataSource.Columns.Add(nextAttemptColumn);

            foreach (var entity in collection)
            {
                var row = tasksDataSource.NewRow();
                row[createdColumn] = entity.CreatedUTC;
                row[typeColumn] = entity.TypeAsEnum.ToString();

                row[typeUrlColumn] = "";
                switch (entity.TypeAsEnum)
                {
                    case Enums.CloudProcessingTaskType.Entertainment:
                        if (entity.EntertainmentId.HasValue)
                            row[typeUrlColumn] = "~/Generic/Entertainment.aspx?id=" + entity.EntertainmentId;
                        break;
                }

                row[filenameColumn] = entity.PathFormat;
                row[actionColumn] = entity.ActionAsEnum.ToString();

                row[amazonColumn] = GetImageForTask(entity.CompletedOnAmazonUTC);

                row[amazonUrlColumn] = "";

                if (entity.ActionAsEnum == CloudProcessingTaskEntity.ProcessingAction.Upload)
                {
                    string filePath = string.Format("{0}/{1}", entity.Container, entity.PathFormat);

                    if (entity.CompletedOnAmazonUTC.GetValueOrDefault() > failedTime)
                        row[amazonUrlColumn] = string.Format(ObymobiConstants.AmazonBlobStorageUrlFormat, WebEnvironmentHelper.GetAmazonRootContainer(), filePath);
                }

                row[attemptsColumn] = entity.Attempts;

                if (IsCompleted(entity))
                    row[nextAttemptColumn] = "";
                else
                    row[nextAttemptColumn] = entity.NextAttemptUTC;

                tasksDataSource.Rows.Add(row);
            }

            taskGrid.DataSource = tasksDataSource;
            taskGrid.DataBind();
        }

        string GetImageForTask(DateTime? dateTimeValue)
        {
            if (dateTimeValue.HasValue)
            {
                if (dateTimeValue.Value > failedTime)
                {
                    // Success
                    return "~/Images/Icons/true.png";
                }
                
                // Failed
                return "~/Images/Icons/false.png";
            }

            return "~/Images/Icons/time.png";
        }

        bool IsCompleted(CloudProcessingTaskEntity entity)
        {
            return (entity.CompletedOnAmazonUTC.GetValueOrDefault() > failedTime);
        }
    }
}