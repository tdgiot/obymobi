﻿<configuration>
  <section name="ObymobiRequestServiceOffline">
    <item name="Username">
      <friendlyname>Username</friendlyname>
      <type>System.String</type>
      <value>[[Username]]</value>
    </item>
    <item name="Password">
      <friendlyname>Password</friendlyname>
      <type>System.String</type>
      <value>[[Password]]</value>
    </item>  
    <item name="TerminalId">
      <friendlyname>Id of the Obymobi terminal</friendlyname>
      <type>System.Int32</type>
      <value>[[TerminalId]]</value>
    </item>
    <item name="WebserviceBaseUrl1">
      <friendlyname>Webservice Base Url 1</friendlyname>
      <type>System.String</type>
      <value>[[WebserviceBaseUrl1]]</value>
    </item>
    <item name="WebserviceBaseUrl2">
      <friendlyname>Webservice Base Url 2</friendlyname>
      <type>System.String</type>
      <value>[[WebserviceBaseUrl2]]</value>
    </item>
    <item name="WebserviceBaseUrl3">
      <friendlyname>Webservice Base Url 3</friendlyname>
      <type>System.String</type>
      <value>[[WebserviceBaseUrl3]]</value>
    </item>
    <item name="LastSuccessfulRequest">
      <friendlyname>Datetime of the last successful request to the Obymobi webservice</friendlyname>
      <type>System.DateTime</type>
      <value>1-1-0001 0:00:00</value>
    </item>
    <item name="LoggingLevel">
      <friendlyname>Level of logging (0 = normal, 1 = extended)</friendlyname>
      <type>System.Int32</type>
      <value>1</value>
    </item>
  </section>
</configuration>