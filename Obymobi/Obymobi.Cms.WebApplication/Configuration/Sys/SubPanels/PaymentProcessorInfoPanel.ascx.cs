﻿using System;
using System.Web.UI;
using Dionysos.Interfaces;
using Dionysos.Web.UI.WebControls;
using DionysosUI = Dionysos.Web.UI;

namespace Obymobi.Cms.WebApplication.Configuration.Sys.SubPanels
{
    public partial class PaymentProcessorInfoPanel : UserControl, ISaveableControl
    {
        private PaymentProcessorSettings paymentProcessorSettings;

        private MultiValidator MultiValidator => (this.Page as DionysosUI.PageDefault)?.MultiValidatorDefault;

        protected void Page_Load(object sender, EventArgs e)
        {
            this.LoadResources();
        }

        public new void Load(PaymentProcessorSettings paymentProcessorSettings)
        {
            this.paymentProcessorSettings = paymentProcessorSettings;
            this.SetGui();
        }

        public bool Save()
        {
            if (paymentProcessorSettings.IsRequired && string.IsNullOrWhiteSpace(tbPaymentProcessorInfo.Value))
            {
                MultiValidator.AddError("The payment processor info cannot be empty!");
            }

            if (paymentProcessorSettings.IsRequired && string.IsNullOrWhiteSpace(tbPaymentProcessorInfoFooter.Value))
            {
                MultiValidator.AddError("The payment processor info footer cannot be empty!");
            }

            return !MultiValidator.HasErrors;
        }

        private void LoadResources()
        {
            DionysosUI.MasterPage.LinkedJsFiles.Add("~/Js/Configuration/PaymentProvider/payment-provider-templates.js", 100);
        }

        private void SetGui()
        {
            tbPaymentProcessorInfo.IsRequired = paymentProcessorSettings.IsRequired;
            tbPaymentProcessorInfo.Text = paymentProcessorSettings.PaymentProcessorInfo;

            tbPaymentProcessorInfoFooter.IsRequired = paymentProcessorSettings.IsRequired;
            tbPaymentProcessorInfoFooter.Text = paymentProcessorSettings.PaymentProcessorInfoFooter;
        }

        public class PaymentProcessorSettings
        {
            public PaymentProcessorSettings(
                bool isRequired,
                string paymentProcessorInfo,
                string paymentProcessorInfoFooter)
            {
                IsRequired = isRequired;
                PaymentProcessorInfo = paymentProcessorInfo;
                PaymentProcessorInfoFooter = paymentProcessorInfoFooter;
            }

            public bool IsRequired { get; }
            public string PaymentProcessorInfo { get; }
            public string PaymentProcessorInfoFooter { get; }
        }
    }
}