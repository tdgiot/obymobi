﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BulkConfigurationRoomControlConfiguration.ascx.cs" Inherits="Obymobi.Cms.WebApplication.Configuration.Sys.SubPanels.BulkConfigurationRoomControlConfiguration" %>
<%@ Register TagPrefix="dxe" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

 <dxe:ASPxGlobalEvents ID="GlobalEvents" runat="server">
    <ClientSideEvents ControlsInitialized="function(s, e) { UpdateButtonState(); }" />
</dxe:ASPxGlobalEvents>

<D:Panel ID="BulkRoomControlConfigurationPanel" runat="server" GroupingText="Bulk apply room control configuration">
    <div style="height: 10px"></div>

    <table style="width: 65%">
        <thead>
            <tr>
                <th colspan="3" style="text-align: left;">
                    <label>Room control configuration</label>
                    <div style="height: 6px;"></div>
                    <X:ComboBoxLLBLGenEntityCollection ID="cbRoomControlConfiguration" IncrementalFilteringMode="StartsWith" EntityName="RoomControlConfiguration" TextField="Name" ValueField="RoomControlConfigurationId" PreventEntityCollectionInitialization="true" runat="server" />
                </th>
            </tr>
        </thead>

        <tbody>
            <tr>
                <td style="width: 40%">
                    <div class="BottomPadding">
                        <dxe:ASPxLabel ID="lblAvailable" runat="server" Text="Delivery points" />
                    </div>
                    <dxe:ASPxListBox ID="lbAvailable" runat="server" ClientInstanceName="lbAvailable" Width="100%" Height="240px" SelectionMode="CheckColumn">
                        <Items></Items>
                        <ClientSideEvents SelectedIndexChanged="function(s, e) { UpdateButtonState(); }" />
                    </dxe:ASPxListBox>
                </td>

                <td style="padding: 75px 60px;">
                    <div>
                        <dxe:ASPxButton ID="btnMoveSelectedItemsToRight" runat="server" ClientInstanceName="btnMoveSelectedItemsToRight"
                            AutoPostBack="False" Text="Add >" Width="130px" ClientEnabled="False"
                            ToolTip="Add selected items">
                            <ClientSideEvents Click="function(s, e) { AddSelectedItems(); }" />
                        </dxe:ASPxButton>
                    </div>

                    <div class="TopPadding">
                        <dxe:ASPxButton ID="btnMoveAllItemsToRight" runat="server" ClientInstanceName="btnMoveAllItemsToRight"
                            AutoPostBack="False" Text="Add All >>" Width="130px" ToolTip="Add all items">
                            <ClientSideEvents Click="function(s, e) { AddAllItems(); }" />
                        </dxe:ASPxButton>
                    </div>

                    <div style="height: 32px"></div>

                    <div>
                        <dxe:ASPxButton ID="btnMoveSelectedItemsToLeft" runat="server" ClientInstanceName="btnMoveSelectedItemsToLeft"
                            AutoPostBack="False" Text="< Remove" Width="130px" ClientEnabled="False"
                            ToolTip="Remove selected items">
                            <ClientSideEvents Click="function(s, e) { RemoveSelectedItems(); }" />
                        </dxe:ASPxButton>
                    </div>

                    <div class="TopPadding">
                        <dxe:ASPxButton ID="btnMoveAllItemsToLeft" runat="server" ClientInstanceName="btnMoveAllItemsToLeft"
                            AutoPostBack="False" Text="<< Remove All" Width="130px" ClientEnabled="False"
                            ToolTip="Remove all items">
                            <ClientSideEvents Click="function(s, e) { RemoveAllItems(); }" />
                        </dxe:ASPxButton>
                    </div>
                </td>

                <td style="width: 40%">
                    <div class="BottomPadding">
                        <dxe:ASPxLabel ID="lblChosen" runat="server" Text="Selection" />
                    </div>
                    <dxe:ASPxListBox ID="lbChosen" runat="server" ClientInstanceName="lbChosen" Width="100%"
                        Height="240px" SelectionMode="CheckColumn">
                        <ClientSideEvents SelectedIndexChanged="function(s, e) { UpdateButtonState(); }">
                        </ClientSideEvents>
                    </dxe:ASPxListBox>
                </td>
            </tr>
        </tbody>

        <tfoot>
            <tr>
                <td colspan="3" style="text-align: right;">
                    <D:Button ID="btnBulkUpdate" Text="Apply configuration" runat="server" />
                </td>
            </tr>
        </tfoot>
    </table>
</D:Panel>

<script type="text/javascript">
    function AddSelectedItems() {
        MoveSelectedItems(lbAvailable, lbChoosen);
        UpdateButtonState();
    }

    function AddAllItems() {
        MoveAllItems(lbAvailable, lbChoosen);
        UpdateButtonState();
    }

    function RemoveSelectedItems() {
        MoveSelectedItems(lbChoosen, lbAvailable);
        UpdateButtonState();
    }

    function RemoveAllItems() {
        MoveAllItems(lbChoosen, lbAvailable);
        UpdateButtonState();
    }

    function MoveSelectedItems(srcListBox, dstListBox) {
        srcListBox.BeginUpdate();
        dstListBox.BeginUpdate();

        let items = srcListBox.GetSelectedItems();
        for(let i = items.length - 1; i >= 0; i = i - 1) {
            dstListBox.AddItem(items[i].text, items[i].value);
            srcListBox.RemoveItem(items[i].index);
        }

        srcListBox.EndUpdate();
        dstListBox.EndUpdate();
    }

    function MoveAllItems(srcListBox, dstListBox) {
        srcListBox.BeginUpdate();

        let count = srcListBox.GetItemCount();
        for(let i = 0; i < count; i++) {
            let item = srcListBox.GetItem(i);
            dstListBox.AddItem(item.text, item.value);
        }

        srcListBox.EndUpdate();
        srcListBox.ClearItems();
    }

    function UpdateButtonState() {
        btnMoveAllItemsToRight.SetEnabled(lbAvailable.GetItemCount() > 0);
        btnMoveAllItemsToLeft.SetEnabled(lbChoosen.GetItemCount() > 0);
        btnMoveSelectedItemsToRight.SetEnabled(lbAvailable.GetSelectedItems().length > 0);
        btnMoveSelectedItemsToLeft.SetEnabled(lbChoosen.GetSelectedItems().length > 0);
    }
</script>
