﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OmisePaymentProvider.ascx.cs" Inherits="Obymobi.Cms.WebApplication.Configuration.Sys.SubPanels.OmisePaymentProvider" %>

<D:Panel ID="pnlOmiseInfo" GroupingText="Omise specific info" runat="server">
    <table class="dataformV2">
        <tr>
            <td class="label">
                <D:LabelEntityFieldInfo ID="lblOmisePublicKey" runat="server">Omise public key</D:LabelEntityFieldInfo>
            </td>
            <td class="control">
                <D:TextBoxString ID="tbOmisePublicKey" runat="server"></D:TextBoxString>
            </td>
            <td class="label">
                <D:LabelEntityFieldInfo ID="lblOmiseSecretKey" runat="server">Omise secret key</D:LabelEntityFieldInfo>
            </td>
            <td class="control">
                <D:TextBoxString ID="tbOmiseSecretKeyDecrypted" runat="server"></D:TextBoxString>
                <D:Button ID="btOmiseSecretKey" Text="Change" runat="server" CssClass="change-button" />
            </td>
        </tr>
    </table>
</D:Panel>