﻿using System;
using System.Data;
using DevExpress.Web;
using Dionysos.Web.UI;
using Dionysos.Web.UI.WebControls;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.QuerySpec;

namespace Obymobi.Cms.WebApplication.Configuration.Sys.SubPanels
{
    public partial class BulkConfigurationRoomControlConfiguration : SubPanelCustomDataSource
    {
        protected override void OnInit(EventArgs e)
        {
            LoadUserControls();
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
            HookupEvents();
        }

        private void HookupEvents()
        {
            btnBulkUpdate.Click += BulkUpdateDeliverypoints;
        }

        private void BulkUpdateDeliverypoints(object sender, EventArgs eventArgs)
        {
            if (lbChosen.Items.IsEmpty)
            {
                PageAsPageDefault.MultiValidatorDefault.AddError("No deliverypoints has been selected.");
                PageAsPageDefault.Validate();

                return;
            }

            if (cbRoomControlConfiguration.SelectedIndex <= 0)
            {
                PageAsPageDefault.MultiValidatorDefault.AddError("No room control has been selected or the selection is invalid.");
                PageAsPageDefault.Validate();

                return;
            }

            ListEditItemCollection listItems = lbChosen.Items;
            bool parsedRoomControlConfigurationId = int.TryParse(cbRoomControlConfiguration.SelectedItem.Value.ToString(), out int roomControlConfigurationId);
            if (!parsedRoomControlConfigurationId || roomControlConfigurationId <= 0)
            {
                PageAsPageDefault.MultiValidatorDefault.AddError("The room control configuration identity is invalid.");

                return;
            }

            using (Transaction transaction = new Transaction(IsolationLevel.ReadUncommitted, nameof(BulkUpdateDeliverypoints)))
            {
                try
                {
                    foreach (ListEditItem listItem in listItems)
                    {
                        bool parsedDeliverypointId = int.TryParse(listItem.Value.ToString(), out int deliverypointId);
                        if (!parsedDeliverypointId)
                        {
                            continue;
                        }

                        var deliverypointEntity = new DeliverypointEntity(deliverypointId);

                        deliverypointEntity.AddToTransaction(transaction);
                        deliverypointEntity.RoomControlConfigurationId = roomControlConfigurationId;
                        deliverypointEntity.Save();
                    }

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();

                    throw ex;
                }
            }

            PageAsPageDefault.AddInformator(InformatorType.Information, "All deliverypoins has been updated successfully.");
        }

        private ListEditItemCollection CreateListItemCollection(DeliverypointgroupCollection deliverypointgroupCollection)
        {
            ListEditItemCollection listItemCollection = new ListEditItemCollection();

            foreach (DeliverypointgroupEntity deliverypointgroupEntity in deliverypointgroupCollection)
            {
                foreach (DeliverypointEntity deliverypointEntity in deliverypointgroupEntity.DeliverypointCollection)
                {
                    string name = $"{deliverypointgroupEntity.Name} - {deliverypointEntity.Number}";
                    var listItem = new ListEditItem(name, deliverypointEntity.DeliverypointId);

                    listItemCollection.Add(listItem);
                }
            }

            return listItemCollection;
        }

        private DeliverypointgroupCollection GetDeliverypointgroups(PredicateExpression filter, RelationCollection relation)
        {
            IncludeFieldsList includeFieldList = new IncludeFieldsList { DeliverypointgroupFields.Name };

            SortExpression sortExpression = new SortExpression();
            sortExpression.Add(DeliverypointgroupFields.Name | SortOperator.Ascending);

            DbFunctionCall deliverypointNumberCall = new DbFunctionCall("CAST({0} AS int)", new object[] { DeliverypointFields.Number });
            ISortClause sortClause = deliverypointNumberCall.Ascending();
            sortExpression.Add(sortClause);

            DeliverypointgroupCollection deliverypointgroupCollection = new DeliverypointgroupCollection();
            deliverypointgroupCollection.GetMulti(filter, 0, sortExpression, relation, null, includeFieldList, 0, 0);

            return deliverypointgroupCollection;
        }

        private RoomControlConfigurationCollection GetRoomControlConfigurations(PredicateExpression filter)
        {
            IncludeFieldsList includeFiledList = new IncludeFieldsList
            {
                RoomControlConfigurationFields.RoomControlConfigurationId,
                RoomControlConfigurationFields.Name
            };

            RoomControlConfigurationCollection roomControlConfigurationCollection = new RoomControlConfigurationCollection();
            roomControlConfigurationCollection.GetMulti(filter, 0, null, null, null, includeFiledList, 0, 0);

            return roomControlConfigurationCollection;
        }

        private void LoadDeliverypointGroups()
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(DeliverypointFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

            RelationCollection relations = new RelationCollection();
            relations.Add(DeliverypointgroupEntityBase.Relations.DeliverypointEntityUsingDeliverypointgroupId);

            DeliverypointgroupCollection deliverypointgroupCollection = GetDeliverypointgroups(filter, relations);
            ListEditItemCollection listItemCollection = CreateListItemCollection(deliverypointgroupCollection);

            lbAvailable.DataSource = listItemCollection;
            lbAvailable.DataBind();
        }

        private void LoadRoomControlConfigurations()
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(RoomControlConfigurationFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

            RoomControlConfigurationCollection roomControlConfigurationCollection = GetRoomControlConfigurations(filter);

            cbRoomControlConfiguration.DataSource = roomControlConfigurationCollection;
            cbRoomControlConfiguration.DataBind();
        }

        private void LoadUserControls()
        {
            SetGui();
        }

        private void SetGui()
        {
            LoadRoomControlConfigurations();
            LoadDeliverypointGroups();
        }
    }
}
