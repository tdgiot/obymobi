﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdyenHppPaymentProvider.ascx.cs" Inherits="Obymobi.Cms.WebApplication.Configuration.Sys.SubPanels.AdyenHppPaymentProvider" %>

<D:Panel ID="pnlAdyenHppInfo" GroupingText="Adyen HPP specific info" runat="server">
    <table class="dataformV2">
        <tr>
            <td class="label">
                <D:LabelEntityFieldInfo runat="server" id="lblSkinCode">Skin code</D:LabelEntityFieldInfo>
            </td>
            <td class="control">
                <D:TextBoxString ID="tbSkinCode" runat="server" IsRequired="true"></D:TextBoxString>
            </td>        
            <td class="label">
                <D:LabelEntityFieldInfo runat="server" id="lblHmacKey">HMAC key</D:LabelEntityFieldInfo>
            </td>
            <td class="control">
                <D:TextBoxString ID="tbHmacKeyDecrypted" runat="server" IsRequired="true" />
                <D:Button runat="server" ID="btChangeHmacKey" Text="Change" CssClass="change-button" />
            </td>        
        </tr>
        <tr>
            <td class="label">
                <D:LabelEntityFieldInfo ID="lblAdyenUsername" runat="server">Adyen username</D:LabelEntityFieldInfo>
            </td>
            <td class="control">
                <D:TextBoxString ID="tbAdyenUsername" runat="server" IsRequired="true"></D:TextBoxString>
            </td>
            <td class="label">
                <D:LabelEntityFieldInfo ID="lblAdyenPassword" runat="server">Adyen password</D:LabelEntityFieldInfo>
            </td>
            <td class="control">
                <D:TextBoxString ID="tbAdyenPasswordDecrypted" runat="server"></D:TextBoxString>
                <D:Button ID="btChangeAdyenPassword" Text="Change" runat="server" CssClass="change-button" />
            </td>
        </tr>       
    </table>
</D:Panel>