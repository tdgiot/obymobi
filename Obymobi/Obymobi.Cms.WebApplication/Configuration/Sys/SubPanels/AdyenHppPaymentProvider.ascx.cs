﻿using Dionysos;
using Dionysos.Interfaces;
using Dionysos.Security.Cryptography;
using Dionysos.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using System;

namespace Obymobi.Cms.WebApplication.Configuration.Sys.SubPanels
{
    public partial class AdyenHppPaymentProvider : SubPanelCustomDataSource, ISaveableControl
    {
        public PaymentProviderEntity PaymentProviderEntity 
        { 
            get => DataSource as PaymentProviderEntity;
            set => DataSource = value; 
        }

        public bool Save()
        {
            if (!tbHmacKeyDecrypted.Text.IsNullOrWhiteSpace())
            {
                PaymentProviderEntity.HmacKey = Cryptographer.EncryptUsingCBC(tbHmacKeyDecrypted.Text);
            }

            if (!tbAdyenPasswordDecrypted.Text.IsNullOrWhiteSpace())
            {
                PaymentProviderEntity.AdyenPassword = Cryptographer.EncryptUsingCBC(tbAdyenPasswordDecrypted.Text);
            }

            return true;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            btChangeHmacKey.Click += BtChangeHmacKey_Click;
            btChangeAdyenPassword.Click += BtChangeAdyenPassword_Click;

            tbHmacKeyDecrypted.IsRequired = PaymentProviderEntity.HmacKey.IsNullOrWhiteSpace();
            tbHmacKeyDecrypted.Enabled = PaymentProviderEntity.HmacKey.IsNullOrWhiteSpace();
            btChangeHmacKey.Enabled = !PaymentProviderEntity.HmacKey.IsNullOrWhiteSpace();

            tbAdyenPasswordDecrypted.IsRequired = PaymentProviderEntity.AdyenPassword.IsNullOrWhiteSpace();

            tbAdyenPasswordDecrypted.Enabled = PaymentProviderEntity.AdyenPassword.IsNullOrWhiteSpace();
            btChangeAdyenPassword.Enabled = !PaymentProviderEntity.AdyenPassword.IsNullOrWhiteSpace();            
        }

        private void BtChangeHmacKey_Click(object sender, EventArgs e) => tbHmacKeyDecrypted.Enabled = true;

        private void BtChangeAdyenPassword_Click(object sender, EventArgs args) => tbAdyenPasswordDecrypted.Enabled = true;

    }
}