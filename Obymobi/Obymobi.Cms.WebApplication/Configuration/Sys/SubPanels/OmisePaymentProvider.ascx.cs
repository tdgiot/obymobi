﻿using Dionysos;
using Dionysos.Interfaces;
using Dionysos.Security.Cryptography;
using Dionysos.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using System;

namespace Obymobi.Cms.WebApplication.Configuration.Sys.SubPanels
{
    public partial class OmisePaymentProvider : SubPanelCustomDataSource, ISaveableControl
    {
        public PaymentProviderEntity PaymentProviderEntity
        {
            get => DataSource as PaymentProviderEntity;
            set => DataSource = value;
        }

        public bool Save()
        {
            if (!tbOmiseSecretKeyDecrypted.Text.IsNullOrWhiteSpace())
            {
                PaymentProviderEntity.OmiseSecretKey = Cryptographer.EncryptUsingCBC(tbOmiseSecretKeyDecrypted.Text);
            }

            return true;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            btOmiseSecretKey.Click += BtChangeOmiseSecretKey_Click;
            tbOmiseSecretKeyDecrypted.Enabled = PaymentProviderEntity.OmiseSecretKey.IsNullOrWhiteSpace();
            btOmiseSecretKey.Enabled = !PaymentProviderEntity.OmiseSecretKey.IsNullOrWhiteSpace();

        }
        private void BtChangeOmiseSecretKey_Click(object sender, EventArgs e) => tbOmiseSecretKeyDecrypted.Enabled = true;
    }
}