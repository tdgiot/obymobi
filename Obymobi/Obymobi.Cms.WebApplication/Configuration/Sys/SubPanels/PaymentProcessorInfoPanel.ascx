﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PaymentProcessorInfoPanel.ascx.cs" Inherits="Obymobi.Cms.WebApplication.Configuration.Sys.SubPanels.PaymentProcessorInfoPanel" %>

<table class="dataformV2">
    <tr>
        <td class="label">
            <D:LabelEntityFieldInfo runat="server" ID="lblPaymentProcessorInfoTemplate">Template</D:LabelEntityFieldInfo>
        </td>
        <td class="control">
            <D:DropDownList runat="server" ClientIDMode="Static" ID="cbTemplate" onchange="setTemplate(this)">
                <asp:ListItem Text="Use a template..." Value="0" Selected="True" />
                <asp:ListItem Text="Crave Interactive NL B.V." Value="1" />
                <asp:ListItem Text="Crave Interactive UK Ltd." Value="2" />
                <asp:ListItem Text="Crave Interactive US Inc." Value="3" />
            </D:DropDownList>
        </td>
        <td class="label"></td>
        <td class="control"></td>
    </tr>

    <tr>
        <td class="label">
            <D:LabelEntityFieldInfo runat="server" ID="lblPaymentProcessorInfo">Receipt</D:LabelEntityFieldInfo>
        </td>
        <td class="control" rowspan="3">
            <D:TextBoxMultiLine runat="server" ID="tbPaymentProcessorInfo" IsRequired="True" ClientIDMode="Static" Rows="5" /><br />        
        </td>
        <td class="label">
            <D:LabelEntityFieldInfo runat="server" ID="lblPaymentProcessorInfoFooter">Footer</D:LabelEntityFieldInfo>
        </td>
        <td class="control">
            <D:TextBoxMultiLine runat="server" ID="tbPaymentProcessorInfoFooter" IsRequired="True" ClientIDMode="Static" Rows="5" />
            <small style="float: left;">
                   <a href="https://guides.github.com/features/mastering-markdown/" target="_blank">Markdown</a>
                   <span style="display: inline-block; margin-left: 0; font-weight: normal">
                       can be used. (for external links within AppLess apply <strong>#external</strong> at the end of the URL)
                   </span>
            </small>
        </td>
    </tr>
</table>
