﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdyenPaymentProvider.ascx.cs" Inherits="Obymobi.Cms.WebApplication.Configuration.Sys.SubPanels.AdyenPaymentProvider" %>

<D:Panel ID="pnlAdyenInfo" GroupingText="Adyen specific info" runat="server">
	<table class="dataformV2">
		<tr>
			<td class="label">
				<D:LabelEntityFieldInfo runat="server" ID="lblApiKey">API Key</D:LabelEntityFieldInfo>
			</td>
			<td class="control">
				<D:TextBoxString ID="tbApiKeyDecrypted" runat="server" IsRequired="true" />
				<D:Button runat="server" ID="btChangeApiKey" Text="Change" CssClass="change-button" />
			</td>            
		</tr>
		<tr>
            <td class="label">
				<D:LabelEntityFieldInfo ID="lblAdyenUsername" runat="server">Adyen username</D:LabelEntityFieldInfo>
			</td>
			<td class="control">
				<D:TextBoxString ID="tbAdyenUsername" runat="server" IsRequired="true"></D:TextBoxString>
			</td>
			<td class="label">
				<D:LabelEntityFieldInfo ID="lblAdyenPassword" runat="server">Adyen password</D:LabelEntityFieldInfo>
			</td>
			<td class="control">
				<D:TextBoxString ID="tbAdyenPasswordDecrypted" runat="server"></D:TextBoxString>
				<D:Button ID="btChangeAdyenPassword" Text="Change" runat="server" CssClass="change-button" />
			</td>
        </tr>
        <tr>
			<td class="label">
				<D:LabelEntityFieldInfo runat="server" ID="lblLiveEndpointUrlPrefix">Live Endpoint Url Prefix</D:LabelEntityFieldInfo>
			</td>
			<td class="control">
				<D:TextBoxString ID="tbLiveEndpointUrlPrefix" runat="server"></D:TextBoxString>
			</td>
			<td class="label">
				<D:LabelEntityFieldInfo ID="lblClientKey" runat="server">Client Key</D:LabelEntityFieldInfo>
			</td>
			<td class="control">
				<D:TextBoxString ID="tbClientKey" runat="server" />
			</td>
		</tr>
		<D:PlaceHolder ID="plhMerchantIdentifiers" runat="server" Visible="false">
		<tr>
			<td class="label">
                <D:LabelEntityFieldInfo runat="server" id="lbGooglePayMerchantIdentifier">Google Pay merchant identifier</D:LabelEntityFieldInfo>
            </td>
            <td class="control">
                <D:TextBoxString ID="tbGooglePayMerchantIdentifier" runat="server"></D:TextBoxString>
            </td>
			<td class="label">
                <D:LabelEntityFieldInfo runat="server" id="lbApplePayMerchantIdentifier">Apple Pay merchant code</D:LabelEntityFieldInfo>
            </td>
            <td class="control">
                <D:TextBoxString ID="tbApplePayMerchantIdentifier" runat="server"></D:TextBoxString>
            </td>
		</tr>
		</D:PlaceHolder>
	</table>
</D:Panel>

<D:Panel ID="pnlLegacy" runat="server" GroupingText="Legacy (before drop-in 4.0.0)">
	<table class="dataformV2">
		<tr>
			<td class="label">
				<D:LabelEntityFieldInfo runat="server" ID="lblOriginDomain">Origin Domain</D:LabelEntityFieldInfo>
			</td>
			<td class="control">
				<D:TextBoxString ID="tbOriginDomain" runat="server"></D:TextBoxString>
			</td>
			<td class="label"></td>
			<td class="control"></td>
		</tr>

	</table>
</D:Panel>
