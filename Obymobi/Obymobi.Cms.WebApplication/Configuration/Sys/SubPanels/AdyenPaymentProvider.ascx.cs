﻿using Dionysos;
using Dionysos.Interfaces;
using Dionysos.Security.Cryptography;
using Dionysos.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using System;

namespace Obymobi.Cms.WebApplication.Configuration.Sys.SubPanels
{
    public partial class AdyenPaymentProvider : SubPanelCustomDataSource, ISaveableControl
    {
        public PaymentProviderEntity PaymentProviderEntity
        {
            get => DataSource as PaymentProviderEntity;
            set => DataSource = value;
        }

        public bool Save()
        {
            if (!tbApiKeyDecrypted.Text.IsNullOrWhiteSpace())
            {
                PaymentProviderEntity.ApiKey = Cryptographer.EncryptUsingCBC(tbApiKeyDecrypted.Text);
            }

            if (!tbAdyenPasswordDecrypted.Text.IsNullOrWhiteSpace())
            {
                PaymentProviderEntity.AdyenPassword = Cryptographer.EncryptUsingCBC(tbAdyenPasswordDecrypted.Text);
            }
            return true;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            btChangeApiKey.Click += BtChangeApiKey_Click;
            btChangeAdyenPassword.Click += BtChangeAdyenPassword_Click;

            tbApiKeyDecrypted.IsRequired = PaymentProviderEntity.ApiKey.IsNullOrWhiteSpace();
            tbApiKeyDecrypted.Enabled = PaymentProviderEntity.ApiKey.IsNullOrWhiteSpace();
            btChangeApiKey.Enabled = !PaymentProviderEntity.ApiKey.IsNullOrWhiteSpace();

            tbAdyenPasswordDecrypted.IsRequired = PaymentProviderEntity.AdyenPassword.IsNullOrWhiteSpace();

            tbAdyenPasswordDecrypted.Enabled = PaymentProviderEntity.AdyenPassword.IsNullOrWhiteSpace();
            btChangeAdyenPassword.Enabled = !PaymentProviderEntity.AdyenPassword.IsNullOrWhiteSpace();

            btChangeApiKey.Visible = PaymentProviderEntity.Type == PaymentProviderType.Adyen && CmsSessionHelper.CurrentRole == Role.GodMode;
            btChangeAdyenPassword.Visible = CmsSessionHelper.CurrentRole == Role.GodMode;
            tbOriginDomain.IsRequired = !PaymentProviderEntity.ClientKey.IsNullOrWhiteSpace();

            plhMerchantIdentifiers.Visible = PaymentProviderEntity.Type == PaymentProviderType.AdyenForPlatforms;

            if (PaymentProviderEntity.Environment == PaymentProviderEnvironment.Live)
            {
                tbLiveEndpointUrlPrefix.IsRequired = true;
            }
        }

        private void BtChangeApiKey_Click(object sender, EventArgs e) => tbApiKeyDecrypted.Enabled = true;

        private void BtChangeAdyenPassword_Click(object sender, EventArgs args) => tbAdyenPasswordDecrypted.Enabled = true;

    }
}