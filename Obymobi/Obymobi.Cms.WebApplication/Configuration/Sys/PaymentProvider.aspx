﻿<%@ Page Title="Payment Provider" Async="true" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" CodeBehind="PaymentProvider.aspx.cs" Inherits="Obymobi.ObymobiCms.Configuration.Sys.PaymentProvider" ValidateRequest="false" %>

<asp:Content ID="StylesheetContent" ContentPlaceHolderID="cphlStylesheets" runat="server">
	<style type="text/css">
		.change-button {
			float: right;
			margin-top: 4px;
		}
	</style>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" runat="server"></asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" runat="server">
	<div>
		<X:PageControl ID="tabsMain" runat="server" Width="100%">
			<TabPages>
				<X:TabPage Text="Algemeen" Name="Generic">
					<controls>
						<D:Panel ID="pnlGenericSettings" GroupingText="Generic settings" runat="server">
							<table class="dataformV2">
								<tr>
									<td class="label">
										<D:LabelEntityFieldInfo runat="server" ID="lblName">Name</D:LabelEntityFieldInfo>
									</td>
									<td class="control">
										<D:TextBoxString ID="tbName" runat="server" IsRequired="true" notdirty="true"></D:TextBoxString>
									</td>
									<td class="label">
										<D:LabelEntityFieldInfo runat="server" ID="tbType">Provider</D:LabelEntityFieldInfo>
									</td>
									<td class="control">
										<X:ComboBoxEnum ID="ddlType" runat="server" IsRequired="true" Type="Obymobi.Enums.PaymentProviderType, Obymobi" AutoPostBack="true" notdirty="true" />
									</td>
								</tr>
								<D:PlaceHolder runat="server" ID="plhEnvironment" Visible="false">
								<tr>
									<td class="label">
										<D:LabelEntityFieldInfo runat="server" ID="tbEnvironment">Environment</D:LabelEntityFieldInfo>
									</td>
									<td class="control">
										<X:ComboBoxEnum runat="server" ID="ddlEnvironment" IsRequired="True" Type="Obymobi.Enums.PaymentProviderEnvironment, Obymobi" />
									</td>
									<td class="label"></td>
									<td class="control"></td>
								</tr>
								</D:PlaceHolder>
							</table>
						</D:Panel>

						<D:PlaceHolder runat="server" ID="plhPaymentProviderInfo" Visible="false" />

						<D:Panel ID="pnlPaymentProcessorInfo" GroupingText="Payment processor info" runat="server"></D:Panel>
					</controls>
				</X:TabPage>
			</TabPages>
		</X:PageControl>
	</div>
</asp:Content>
