﻿<%@ Page Title="Vectron" Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Configuration.Sys.Vectron" Codebehind="Vectron.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitleHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhToolbarHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhContentHolder" Runat="Server">
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Name="Generic" Text="Tablet">
				<Controls>
					<table class="dataformV2">
						<tr>
							<td class="label">
							    <D:Label runat="server" id="lblCompanyId">Bedrijf</D:Label>
							</td>
							<td class="control">
							    <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlCompanyId" EntityName="Company" IncrementalFilteringMode="StartsWith" TextField="Name" ValueField="CompanyId" AutoPostBack="true"></X:ComboBoxLLBLGenEntityCollection>			
							</td>
							<td class="label">
							</td>
							<td class="control">
							</td>        
						</tr>
						<tr>
							<td class="label">
							    <D:Label runat="server" id="lblClerkNo">Bediende nummer</D:Label>
							</td>
							<td class="control">
							    <D:TextBoxInt ID="tbClerkNo" runat="server"></D:TextBoxInt>
							</td>
							<td class="label">
                                <D:Label runat="server" id="lblGuestCheckNo">Rekening nummer</D:Label>
							</td>
							<td class="control">
                                <D:TextBoxInt ID="tbGuestCheckNo" runat="server"></D:TextBoxInt>
							</td>        
						</tr>
						<tr>
							<td class="label">
							</td>
							<td class="control">
							    <D:Button ID="btnGenerateTestScript" runat="server" Text="Genereer test script" />							    
							</td>
							<td class="label">
							</td>
							<td class="control">
							</td>
						</tr>
                    </table>
                </Controls>
            </X:TabPage>
        </TabPages>
    </X:PageControl>
</asp:Content>

