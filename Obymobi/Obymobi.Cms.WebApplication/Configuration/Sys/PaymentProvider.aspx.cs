﻿using System;
using System.Web.UI.WebControls;
using Dionysos.Web.UI;
using Obymobi.Cms.WebApplication.Configuration.Sys.SubPanels;
using Obymobi.Cms.WebApplication.Strategies.PaymentProvider;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.ObymobiCms.MasterPages;

namespace Obymobi.ObymobiCms.Configuration.Sys
{
    public partial class PaymentProvider : PageLLBLGenEntity
    {
        private PaymentProviderPanelStrategy paymentProviderPanelStrategy;
        private PaymentProcessorInfoPanel paymentProcessorInfoPanel;

        public PaymentProviderEntity DataSourceAsPaymentProviderEntity => this.DataSource as PaymentProviderEntity;

        protected override void OnInit(EventArgs e)
        {
            DataSourceLoaded += PaymentProvider_DataSourceLoaded;

            tabsMain.AddTabPage("Companies", "PaymentProviderCompanyCollection", "~/Configuration/SubPanels/PaymentProviderCompanyCollection.ascx");
            
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Dionysos.Web.UI.DevExControls.ToolBarPageEntity toolbar = ((MasterPageEntity)Master).ToolBar;

            if (toolbar.SaveAndGoButton != null) toolbar.SaveAndGoButton.Visible = CmsSessionHelper.CurrentRole == Role.GodMode;
            if (toolbar.SaveAndNewButton != null) toolbar.SaveAndNewButton.Visible = CmsSessionHelper.CurrentRole == Role.GodMode;
            if (toolbar.SaveButton != null) toolbar.SaveButton.Visible = CmsSessionHelper.CurrentRole == Role.GodMode;
            if (toolbar.DeleteButton != null) toolbar.DeleteButton.Visible = CmsSessionHelper.CurrentRole == Role.GodMode;

            PaymentProviderType? paymentProviderType = ddlType.Value.GetValueOrDefault(0) > 0
                ? (PaymentProviderType)ddlType.Value.GetValueOrDefault(0)
                : (PaymentProviderType?)null;

            plhEnvironment.Visible = paymentProviderType.HasValue;

            if (paymentProviderType == PaymentProviderType.AdyenForPlatforms)
            {
                ddlEnvironment.Enabled = false;
                ddlEnvironment.Value = WebEnvironmentHelper.CloudEnvironmentIsProduction
                    ? (int)PaymentProviderEnvironment.Live
                    : (int)PaymentProviderEnvironment.Test;
            }
        }

        private void PaymentProvider_DataSourceLoaded(object sender)
        {
            if (!DataSourceAsPaymentProviderEntity.IsNew)
            {
                ddlType.Enabled = false;

                paymentProviderPanelStrategy = PaymentPanelStrategyFactory
                    .Get(DataSourceAsPaymentProviderEntity.Type)
                    .GetPaymentProviderPanelStrategy();

                paymentProviderPanelStrategy.AddPanel(this);
            }

            AddPaymentProcessorInfoPanel();
        }

        public override bool Save()
        {
            if (paymentProviderPanelStrategy != null && !paymentProviderPanelStrategy.Save(this))
            {
                return false;
            }            
           
            return base.Save();
        }                      

        private void AddPaymentProcessorInfoPanel()
        {
            pnlPaymentProcessorInfo.Visible = true;

            PaymentProcessorInfoPanel.PaymentProcessorSettings settings = new PaymentProcessorInfoPanel.PaymentProcessorSettings(true, DataSourceAsPaymentProviderEntity.PaymentProcessorInfo, DataSourceAsPaymentProviderEntity.PaymentProcessorInfoFooter);
            this.paymentProcessorInfoPanel = (PaymentProcessorInfoPanel)LoadControl("~/Configuration/Sys/SubPanels/PaymentProcessorInfoPanel.ascx");
            this.paymentProcessorInfoPanel.Load(settings);

            pnlPaymentProcessorInfo.Controls.Add(this.paymentProcessorInfoPanel);
        }                        

        public PlaceHolder PaymentProviderInfoPlaceHolder => plhPaymentProviderInfo;
    }
}
