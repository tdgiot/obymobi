﻿<%@ Page Title="Generate configuration files" Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Configuration.Sys.MediaTaskProcessingQueue" Codebehind="MediaTaskProcessingQueue.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitleHolder" Runat="Server"><D:Label runat="server" id="lblTitle">Media Task Queue</D:Label></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhToolbarHolder" Runat="Server">
<div>
    <D:Button runat="server" ID="btChangeFilter" Text="Filter veranderen" />
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhContentHolder" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
					<D:PlaceHolder runat="server" ID="plh">
	                    <table class="dataformV2">
						    <tr>
							    <td class="label">
								    <D:LabelTextOnly runat="server" id="lblFilter">Filter</D:LabelTextOnly>
							    </td>
							    <td class="control">
								    <X:ComboBoxInt ID="ddlType" runat="server" UseDataBinding="true"></X:ComboBoxInt>
							    </td>
                                <td class="label">
								    <D:LabelTextOnly runat="server" id="lblPageSize">Items per pagina</D:LabelTextOnly>
							    </td>
							    <td class="control">
                                    <D:TextBoxInt runat="server" ID="tbPageSize"></D:TextBoxInt>
							    </td>  
						    </tr>						       					                        										
					     </table>					                              
                          <X:GridView ID="taskGrid" ClientInstanceName="clientGrid" runat="server" Width="100%" KeyFieldName="ClientId">        
                            <SettingsBehavior AutoFilterRowInputDelay="350" />                            
                            <SettingsPager PageSize="30"></SettingsPager>
                            <SettingsBehavior AllowGroup="false" AllowDragDrop="false" />
                            <Columns>                                                                                                
                                <dxwgv:GridViewDataHyperLinkColumn FieldName="RelatedMediaId" VisibleIndex="1" >
                                    <CellStyle HorizontalAlign="Left">
                                    </CellStyle>
								    <Settings AutoFilterCondition="Contains" />
                                    <PropertiesHyperLinkEdit NavigateUrlFormatString="~/Generic/Media.aspx?id={0}" Target="_blank" TextField="RelatedMediaRelatedEntityNameAndShowFieldValue"  />
                                </dxwgv:GridViewDataHyperLinkColumn>
                                <dxwgv:GridViewDataColumn FieldName="ActionAsEnum" VisibleIndex="2" >
								    <Settings AutoFilterCondition="Contains" />
                                </dxwgv:GridViewDataColumn>
                                <dxwgv:GridViewDataColumn FieldName="MediaRatioTypeText" VisibleIndex="2" >
								    <Settings AutoFilterCondition="Contains" />
                                </dxwgv:GridViewDataColumn>
                                <dxwgv:GridViewDataColumn FieldName="CompletedOnAzureText" VisibleIndex="3" >								                                        
                                    <Settings AutoFilterCondition="Contains" />
                                </dxwgv:GridViewDataColumn>
                                <dxwgv:GridViewDataColumn FieldName="CompletedOnPrimaryServerText" VisibleIndex="4" >                                    
                                    <Settings AutoFilterCondition="Contains" />
                                </dxwgv:GridViewDataColumn>
                                <dxwgv:GridViewDataColumn FieldName="CompletedOnSecondaryServerText" VisibleIndex="5" >                                    
                                    <Settings AutoFilterCondition="Contains" />
                                </dxwgv:GridViewDataColumn>
                                <dxwgv:GridViewDataColumn FieldName="Attempts" VisibleIndex="6" >
								    <Settings AutoFilterCondition="Contains" />
                                </dxwgv:GridViewDataColumn>
                                <dxwgv:GridViewDataColumn FieldName="NextAttempt" VisibleIndex="7" >
								    <Settings AutoFilterCondition="Contains" />
                                </dxwgv:GridViewDataColumn>                               
                            </Columns>
                            <Settings ShowFilterRow="True" />
                        </X:GridView>         					    					
					</D:PlaceHolder>
                </Controls>
            </X:TabPage>
        </TabPages>
    </X:PageControl>
</div>
</asp:Content>

