﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Dionysos;
using Dionysos.Web;
using System.Drawing;

namespace Obymobi.ObymobiCms.Configuration
{
    public partial class Supportpools : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.MainGridView != null)
                this.MainGridView.HtmlRowPrepared += new DevExpress.Web.ASPxGridViewTableRowEventHandler(MainGridView_HtmlRowPrepared);
        }

        private void MainGridView_HtmlRowPrepared(object sender, DevExpress.Web.ASPxGridViewTableRowEventArgs e)
        {
            //if (e.RowType != DevExpress.Web.GridViewRowType.Data) return;
            //string stableVersion = (string)e.GetValue("StableVersion");
            //if (stableVersion.IsNullOrWhiteSpace())
            //    e.Row.BackColor = Color.FromArgb(249, 224, 224);
        }

        #endregion
    }
}
