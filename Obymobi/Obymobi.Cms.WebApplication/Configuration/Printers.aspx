﻿<%@ Page Title="Printers" Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Configuration.Printers" Codebehind="Printers.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitleHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhToolbarHolder" Runat="Server">
    <D:Button runat="server" ID="btnRefresh" Text="Verversen" CausesValidation="true"/>	
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhContentHolder" Runat="Server">
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Printers" Name="Printers">
				<Controls>
					<D:PlaceHolder runat="server" ID="plhOverview"></D:PlaceHolder>
                    <X:GridView ID="printersGrid" ClientInstanceName="printersGrid" runat="server" Width="100%">        
                        <SettingsBehavior AutoFilterRowInputDelay="350" />                            
                        <SettingsPager PageSize="50"></SettingsPager>
                        <SettingsBehavior AllowGroup="false" AllowDragDrop="false" />
                        <Columns>
                            <dxwgv:GridViewDataColumn FieldName="DisplayName" VisibleIndex="0">
                            </dxwgv:GridViewDataColumn>
                            <dxwgv:GridViewDataColumn FieldName="Name" VisibleIndex="1">
                            </dxwgv:GridViewDataColumn>
                            <dxwgv:GridViewDataColumn FieldName="Status" VisibleIndex="2">
                            </dxwgv:GridViewDataColumn>
                        </Columns>
                    </X:GridView>  
				</Controls>
			</X:TabPage>
            <X:TabPage Text="Opdrachten" Name="Jobs">
				<Controls>
                    <X:GridView ID="jobsGrid" ClientInstanceName="jobsGrid" runat="server" Width="100%">        
                        <SettingsBehavior AutoFilterRowInputDelay="350" />                            
                        <SettingsPager PageSize="50"></SettingsPager>
                        <SettingsBehavior AllowGroup="false" AllowDragDrop="false" />
                        <Columns>
                            <dxwgv:GridViewDataColumn FieldName="Title" VisibleIndex="0">
                            </dxwgv:GridViewDataColumn>
                            <dxwgv:GridViewDataColumn FieldName="Printer" VisibleIndex="1">
                            </dxwgv:GridViewDataColumn>
                            <dxwgv:GridViewDataColumn FieldName="Status" VisibleIndex="2">
                            </dxwgv:GridViewDataColumn>
                            <dxwgv:GridViewCommandColumn ButtonType="Image" VisibleIndex="3" Width="40">
                                <CustomButtons>
                                    <dxwgv:GridViewCommandColumnCustomButton ID="deleteJob" Text="Delete" >
                                        <Image Url="~/Images/Icons/delete.png" />
                                    </dxwgv:GridViewCommandColumnCustomButton>
                                </CustomButtons>
                            </dxwgv:GridViewCommandColumn>
                        </Columns>
                    </X:GridView>  		
				</Controls>
			</X:TabPage>
        </TabPages>
    </X:PageControl>
</asp:Content>

