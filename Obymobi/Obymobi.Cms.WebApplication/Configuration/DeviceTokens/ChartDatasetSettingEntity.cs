﻿using System.Collections.Generic;

namespace Obymobi.Cms.WebApplication.Configuration.DeviceTokens
{
    public class ChartDatasetSettingEntity
    {
        #region Constructors

        public ChartDatasetSettingEntity()
        { }

        #endregion

        #region Properties

        public int ChartDatasetSettingId { get; set; }

        public int ChartDatasetId { get; set; }

        public string BackgroundColor { get; set; }

        public string BorderColor { get; set; }

        public bool Hidden { get; set; }

        #endregion

        #region Statics

        public static IDictionary<int, ChartDatasetSettingEntity> GenerateDatasetSettings(int chartDatasetId)
        {
            switch (chartDatasetId)
            {
                case 1:
                    return new Dictionary<int, ChartDatasetSettingEntity>
                    {
                        { 0, new ChartDatasetSettingEntity { ChartDatasetSettingId = 1, ChartDatasetId = 1, Hidden = true, BackgroundColor = "rgba(10, 203, 16, 0.6)", BorderColor = "rgba(255, 255, 255, 1)" } },
                        { 1, new ChartDatasetSettingEntity { ChartDatasetSettingId = 2, ChartDatasetId = 1, BackgroundColor = "rgba(243, 135, 6, 0.6)", BorderColor = "rgba(255, 255, 255, 1)" } },
                        { 2, new ChartDatasetSettingEntity { ChartDatasetSettingId = 3, ChartDatasetId = 1, BackgroundColor = "rgba(54, 162, 235, 0.6)", BorderColor = "rgba(255, 255, 255, 1)" } },
                        { 3, new ChartDatasetSettingEntity { ChartDatasetSettingId = 4, ChartDatasetId = 1, BackgroundColor = "rgba(255, 99, 132, 0.6)", BorderColor = "rgba(255, 255, 255, 1)" } },
                    };
                case 2:
                    return new Dictionary<int, ChartDatasetSettingEntity>
                    {
                        { 0, new ChartDatasetSettingEntity { ChartDatasetSettingId = 5, ChartDatasetId = 2, BackgroundColor = "rgba(54, 162, 235, 0.6)", BorderColor = "rgba(54, 162, 235, 0.6)" } }
                    };
                case 3:
                    return new Dictionary<int, ChartDatasetSettingEntity>
                    {
                        { 0, new ChartDatasetSettingEntity { ChartDatasetSettingId = 6, ChartDatasetId = 3, BackgroundColor = "rgba(255, 99, 132, 0.6)", BorderColor = "rgba(255, 99, 132, 0.6)" } }
                    };
                case 4:
                    return new Dictionary<int, ChartDatasetSettingEntity>
                    {
                        { 0, new ChartDatasetSettingEntity { ChartDatasetSettingId = 7, ChartDatasetId = 4, BackgroundColor = "rgba(54, 162, 235, 0.6)", BorderColor = "rgba(54, 162, 235, 0.6)" } }
                    };
                case 5:
                    return new Dictionary<int, ChartDatasetSettingEntity>
                    {
                        { 0, new ChartDatasetSettingEntity { ChartDatasetSettingId = 8, ChartDatasetId = 5, BackgroundColor = "rgba(255, 99, 132, 0.6)", BorderColor = "rgba(255, 99, 132, 0.6)" } }
                    };
                default:
                    return null;
            }
        }

        #endregion

        #region Methods



        #endregion
    }
}