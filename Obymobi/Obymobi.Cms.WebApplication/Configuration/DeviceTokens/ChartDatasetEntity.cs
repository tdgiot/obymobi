﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using Obymobi.Charts.Dtos;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.Extensions;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Cms.WebApplication.Configuration.DeviceTokens
{
    public class ChartDatasetEntity
    {
        #region Constructors

        public ChartDatasetEntity()
        { }

        #endregion

        #region Properties

        /// <summary>
        /// Chart dataset database identity
        /// </summary>
        public int ChartDatasetId { get; private set; }

        /// <summary>
        /// Chart (parent) identity
        /// </summary>
        public int ChartId { get; private set; }

        /// <summary>
        /// The SQL query which should retrieve all data for this dataset
        /// </summary>
        public string SqlQuery { get; private set; }

        /// <summary>
        /// SQL parameters to avoid any posibility of SQL injection
        /// </summary>
        public object[] SqlParameters { get; private set; }

        /// <summary>
        /// Fills up all undefined background colors and/or border colors array with the value in the chart dataset settings array.
        /// </summary>
        public bool FillUpColors { get; private set; }

        /// <summary>
        /// All custom settings which will be applied to the provided datasets
        /// </summary>
        public IDictionary<int, ChartDatasetSettingEntity> ChartDatasetSettings { get; private set; }

        #region ChartJS model

        /// <summary>
        /// Defines the label name of the dataset.
        /// </summary>
        public string Label { get; private set; }

        /// <summary>
        /// Defines if all data results in this dataset should be filled with their configured background color.
        /// </summary>
        public bool Fill { get; private set; }

        /// <summary>
        /// Defines the border width which should be applied on the chart dataset.
        /// </summary>
        public int BorderWidth { get; private set; }

        #endregion

        #endregion

        #region Statics

        // ToDo: This should be replaced in the near future by database entities
        public static List<ChartDatasetEntity> GenerateChartDatasetEntities()
        {
            return new List<ChartDatasetEntity>()
            {
                new ChartDatasetEntity
                {
                    ChartDatasetId = 1,
                    ChartId = 1,
                    SqlQuery = @"SELECT
	                                'Active' AS [DataLabels],
	                                COUNT(DeviceId) AS [Data]
                                FROM Device WHERE [SandboxMode] = 0 AND [Token] IS NOT NULL AND [DeviceId] NOT IN (SELECT DISTINCT([DeviceId]) FROM DeviceTokenTask WHERE CAST([CreatedUTC] AS DATE) = CAST(GETDATE() AS  DATE)) AND [CustomerId] IS NULL
                                UNION ALL
                                SELECT
                                    'Sandboxed' AS [DataLabels],
                                    COUNT([DeviceId]) AS [Data]
                                FROM Device WHERE [SandboxMode] = 1 AND [DeviceId] NOT IN (SELECT DISTINCT([DeviceId]) FROM DeviceTokenTask WHERE CAST([CreatedUTC] AS DATE) = CAST(GETDATE() AS  DATE)) AND [CustomerId] IS NULL
                                UNION ALL
                                SELECT
	                                'Renew pending' AS [DataLabel],
	                                COUNT([DeviceTokenTaskId]) AS [Data]
                                FROM DeviceTokenTask WHERE CAST([CreatedUTC] AS DATE) = CAST(GETDATE() AS DATE) AND [Action] = 0 AND [State] < 3
                                UNION ALL
                                SELECT
	                                'Revoke pending' AS [DataLabel],
	                                COUNT([DeviceTokenTaskId]) AS [Data]
                                FROM DeviceTokenTask WHERE CAST([CreatedUTC] AS DATE) = CAST(GETDATE() AS DATE) AND [Action] = 1 AND [State] < 3",
                    Fill = true,
                    BorderWidth = 1,
                    ChartDatasetSettings = ChartDatasetSettingEntity.GenerateDatasetSettings(1)
                },
                new ChartDatasetEntity
                {
                    ChartDatasetId = 2,
                    ChartId = 2,
                    Label = "Renewing",
                    SqlQuery = @"SELECT 'Queued' AS [DataLabels], COUNT([DeviceTokenTaskId]) AS [Data] FROM DeviceTokenTask WHERE convert(DATE, [CreatedUtc]) = CONVERT(DATE, GETDATE()) AND [State] = 0 AND [Action] = 0
                                UNION ALL
                                SELECT 'Waiting to be retrieved' AS [DataLabels], COUNT([DeviceTokenTaskId]) AS [Data] FROM DeviceTokenTask WHERE convert(DATE, [CreatedUtc]) = CONVERT(DATE, GETDATE()) AND [State] = 1 AND [Action] = 0
                                UNION ALL
                                SELECT 'Acknowledged' AS [DataLabels], COUNT([DeviceTokenTaskId]) AS [Data] FROM DeviceTokenTask WHERE convert(DATE, [CreatedUtc]) = CONVERT(DATE, getdate()) AND [State] = 2 AND [Action] = 0",
                    Fill = true,
                    BorderWidth = 1,
                    FillUpColors = true,
                    ChartDatasetSettings = ChartDatasetSettingEntity.GenerateDatasetSettings(2)
                },
                new ChartDatasetEntity
                {
                    ChartDatasetId = 3,
                    ChartId = 2,
                    Label = "Revoking",
                    SqlQuery = @"SELECT 'Queued' AS [DataLabels], COUNT([DeviceTokenTaskId]) AS [Data] FROM DeviceTokenTask WHERE convert(DATE, [CreatedUtc]) = CONVERT(DATE, GETDATE()) AND [State] = 0 AND [Action] = 1
                                UNION ALL
                                SELECT 'Waiting to be retrieved' AS [DataLabels], COUNT([DeviceTokenTaskId]) AS [Data] FROM DeviceTokenTask WHERE convert(DATE, [CreatedUtc]) = CONVERT(DATE, GETDATE()) AND [State] = 1 AND [Action] = 1
                                UNION ALL
                                SELECT 'Acknowledged' AS [DataLabels], COUNT([DeviceTokenTaskId]) AS [Data] FROM DeviceTokenTask WHERE convert(DATE, [CreatedUtc]) = CONVERT(DATE, getdate()) AND [State] = 2 AND [Action] = 1",
                    Fill = true,
                    BorderWidth = 1,
                    FillUpColors = true,
                    ChartDatasetSettings = ChartDatasetSettingEntity.GenerateDatasetSettings(3)
                },
                new ChartDatasetEntity
                {
                    ChartDatasetId = 4,
                    ChartId = 3,
                    Label = "Renewed device tokens",
                    SqlQuery = @"DECLARE @mindate DATETIME
                                DECLARE @maxdate DATETIME
                                DECLARE @diff INT
                                SELECT @maxdate = GETDATE(), @mindate = MIN(DATEADD(DAY, -13, GETDATE())) SET @diff = DATEDIFF(DAY,  @mindate, @maxdate);
                                WITH CTE([DataLabels], [Level]) AS (
                                    SELECT CAST(@mindate AS DATE) AS [DataLabels], 0 AS [Level] 
                                    UNION ALL
                                    SELECT CAST(DATEADD(DAY, 1, CTE.[DataLabels]) AS DATE) AS [DataLabels], [Level] + 1 FROM CTE WHERE [Level] < @diff)
                                SELECT CAST([DataLabels] AS DATE) AS [DataLabels], ISNULL(DTH.RenewedTokens, 0) AS [Data] FROM CTE
                                LEFT JOIN (SELECT CAST([CreatedUTC] AS DATE) AS [Data], COUNT([DeviceTokenHistoryId]) AS [RenewedTokens] FROM [DeviceTokenHistory]
		                            WHERE [CreatedUTC] BETWEEN DATEADD(DAY, -14, GETDATE()) AND GETDATE() AND [Action] = 0 GROUP BY CAST([CreatedUTC] AS DATE)
                                ) DTH ON CTE.[DataLabels] = DTH.[Data]",
                    Fill = true,
                    BorderWidth = 1,
                    FillUpColors = true,
                    ChartDatasetSettings = ChartDatasetSettingEntity.GenerateDatasetSettings(4)
                },
                new ChartDatasetEntity
                {
                    ChartDatasetId = 5,
                    ChartId = 4,
                    Label = "Revoked device tokens",
                    SqlQuery = @"DECLARE @mindate DATETIME
                                DECLARE @maxdate DATETIME
                                DECLARE @diff INT
                                SELECT @maxdate = GETDATE(), @mindate = MIN(DATEADD(DAY, -13, GETDATE())) SET @diff = DATEDIFF(DAY,  @mindate, @maxdate);
                                WITH CTE([DataLabels], [Level]) AS (
                                    SELECT CAST(@mindate AS DATE) AS [DataLabels], 0 AS [Level] 
                                    UNION ALL
                                    SELECT CAST(DATEADD(DAY, 1, CTE.[DataLabels]) AS DATE) AS [DataLabels], [Level] + 1 FROM CTE WHERE [Level] < @diff
                                )
                                SELECT CAST([DataLabels] AS DATE) AS [DataLabels], ISNULL(DTH.RevokedTokens, 0) AS [Data] FROM CTE
                                LEFT JOIN (SELECT CAST([CreatedUTC] AS DATE) AS [Data], COUNT([DeviceTokenHistoryId]) AS [RevokedTokens] FROM [DeviceTokenHistory]
		                            WHERE [CreatedUTC] BETWEEN DATEADD(DAY, -14, GETDATE()) AND GETDATE() AND [Action] = 1 GROUP BY CAST([CreatedUTC] AS DATE)
                                ) DTH ON CTE.[DataLabels] = DTH.[Data];",
                    Fill = true,
                    BorderWidth = 1,
                    FillUpColors = true,
                    ChartDatasetSettings = ChartDatasetSettingEntity.GenerateDatasetSettings(5)
                }
            };
        }

        #endregion

        #region Methods

        /// <summary>
        /// Renders a data transfer object of this chart dataset.
        /// </summary>
        /// <returns>Data transfer object of this dataset.</returns>
        public ChartDatasetDto RenderChartDataset()
        {
            ChartDatasetDto chartDataset = new ChartDatasetDto { Label = this.Label };

            using (SqlCommand command = new SqlCommand(SqlQuery))
            {
                if (SqlParameters != null && SqlParameters.Length > 0)
                {
                    for (int i = 0; i < SqlParameters.Length; i++)
                    {
                        command.Parameters.AddWithValue($"@{i}", SqlParameters[i]);
                    }
                }

                using (RetrievalQuery query = new RetrievalQuery(command))
                {
                    DeviceTokenHistoryDAO dao = new DeviceTokenHistoryDAO();

                    using (Transaction transaction = new Transaction(IsolationLevel.ReadUncommitted, nameof(RenderChartDataset)))
                    {
                        using (SqlDataReader dataReader = (SqlDataReader)dao.GetAsDataReader(transaction, query, CommandBehavior.Default))
                        {
                            Dictionary<int, string> objectMapper = dataReader.GetDataReaderScheme();

                            foreach (DbDataRecord dbDataRecord in dataReader)
                            {
                                foreach (KeyValuePair<int, string> mapper in objectMapper)
                                {
                                    PropertyInfo property = chartDataset.GetType().GetProperty(mapper.Value);

                                    property.PropertyType.GetMethod("Add").Invoke(property.GetValue(chartDataset), new object[]
                                    {
                                        Convert.ChangeType(dataReader.GetValue(mapper.Key), property.PropertyType.GetGenericArguments()[0])
                                    });
                                }
                            }
                        }
                    }
                }
            }

            chartDataset.Fill = this.Fill;
            chartDataset.BackgroundColor = this.GenerateDtoBackgroundColors(chartDataset.Data.Count);
            chartDataset.BorderWidth = this.BorderWidth;
            chartDataset.BorderColor = this.GenerateDtoBorderColors(chartDataset.Data.Count);
            
            return chartDataset;
        }

        /// <summary>
        /// Generates the background colors for the data transfer object.
        /// If the FillUpColors property is true, all missing records will be filled up with the first value in the settings list.
        /// </summary>
        /// <param name="dataRecords">The number of records inside the chart data list.</param>
        /// <returns>List of string that contains a background color for each row.</returns>
        private IEnumerable<string> GenerateDtoBackgroundColors(int dataRecords)
        {
            if (this.ChartDatasetSettings.Any() && this.ChartDatasetSettings.Count < dataRecords && this.FillUpColors)
            {
                List<string> backgroundColors = this.ChartDatasetSettings.Values.Select(cds => cds.BackgroundColor).ToList();

                for (int i = this.ChartDatasetSettings.Count; i < dataRecords; i++)
                {
                    backgroundColors.Add(this.ChartDatasetSettings.Values.FirstOrDefault()?.BackgroundColor ?? string.Empty);
                }

                return backgroundColors;
            }
            else
            {
                return this.ChartDatasetSettings.Select(cds => cds.Value.BackgroundColor);
            }
        }

        /// <summary>
        /// Generates the border colors for the data transfer object.
        /// If the FillUpColors property is true, all missing records will be filled up with the first value in the settings list.
        /// </summary>
        /// <param name="dataRecords">The number of records inside the chart data list.</param>
        /// <returns>List of string that contains a border color for each row.</returns>
        public IEnumerable<string> GenerateDtoBorderColors(int dataRecords)
        {
            if (this.ChartDatasetSettings.Any() && this.ChartDatasetSettings.Count < dataRecords && this.BorderWidth > 0 && this.FillUpColors)
            {
                List<string> borderColors = this.ChartDatasetSettings.Values.Select(cds => cds.BorderColor).ToList();

                for (int i = this.ChartDatasetSettings.Count; i < dataRecords; i++)
                {
                    borderColors.Add(this.ChartDatasetSettings.Values.FirstOrDefault()?.BorderColor ?? string.Empty);
                }

                return borderColors;
            }
            else
            {
                return this.ChartDatasetSettings.Select(cds => cds.Value.BorderColor);
            }
        }

        #endregion
    }
}