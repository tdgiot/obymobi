﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Obymobi.Charts.Dtos;
using Obymobi.Charts.Enumerators;

namespace Obymobi.Cms.WebApplication.Configuration.DeviceTokens
{
    public class ChartEntity
    {
        #region Constructors

        public ChartEntity(int chartId)
        {
            ChartId = chartId;

            // ToDo: This should be some dynamically database called based on the current Chart identity.
            ChartDatasetEntities = ChartDatasetEntity.GenerateChartDatasetEntities().FindAll(cde => cde.ChartId == chartId);
        }

        #endregion

        #region Properties

        public int ChartId { get; set; }

        // ToDo: This should be some database property instead of this.
        public ChartType ChartType
        {
            get
            {
                switch (this.ChartId)
                {
                    case 1:
                        return ChartType.Pie;
                    case 2:
                        return ChartType.Bar;
                    case 3:
                    case 4:
                    default:
                        return ChartType.Line;
                }
            }
        }

        public List<ChartDatasetEntity> ChartDatasetEntities { get; private set; }

        public LegendPosition LegendPosition
        {
            get
            {
                switch (this.ChartId)
                {
                    case 2:
                        return LegendPosition.Top;
                    default:
                        return LegendPosition.Bottom;
                }
            }
        }

        public bool ReverseLegend
        {
            get
            {
                switch (this.ChartId)
                {
                    case 1:
                        return true;
                    default:
                        return false;
                }
            }
        }

        // ToDo: This should be some database property instead of this.
        public string Title
        {
            get
            {
                switch (this.ChartId)
                {
                    case 1:
                        return "Devices";
                    case 2:
                        return "Running tasks";
                    case 3:
                        return "Renewed device tokens";
                    case 4:
                        return "Revoked device tokens";
                    default:
                        return string.Empty;
                }
            }
        }

        // ToDo: This should be some database property instead of this.
        public Type LabelType
        {
            get
            {
                switch (ChartId)
                {
                    case 3:
                    case 4:
                        return typeof(DateTime);
                    default:
                        return typeof(string);
                }
            }
        }

        // ToDo: This should be some database property instead of this.
        public bool ApplyTotalsToLegend => ChartId == 1;

        #endregion

        #region Methods

        public ChartWrapperDto RenderChart(string cultureCode)
        {
            return this.RenderChart(!string.IsNullOrWhiteSpace(cultureCode) ? CultureInfo.GetCultureInfo(cultureCode) : CultureInfo.GetCultureInfo("en-GB"));
        }

        /// <summary>
        /// Converts the chart data into a chart data transfer object.
        /// This object is ready to use/inject into a JavaScript chart clients side.
        /// </summary>
        /// <returns>Data transfer object of the chart.</returns>
        public ChartWrapperDto RenderChart(CultureInfo cultureInfo)
        {
            List<ChartDatasetDto> chartDatasetDtos = new List<ChartDatasetDto>();

            foreach (ChartDatasetEntity chartDatasetEntity in ChartDatasetEntities)
            {
                chartDatasetDtos.Add(chartDatasetEntity.RenderChartDataset());
            }

            List<string> chartDataLabels = new List<string>();
            if (this.LabelType == typeof(DateTime))
            {
                foreach (string dataLabel in chartDatasetDtos.FirstOrDefault().DataLabels)
                {
                    bool isValidDateTime = DateTime.TryParse(dataLabel, out DateTime dateTime);
                    if (isValidDateTime)
                    {
                        chartDataLabels.Add(dateTime.ToString("dd MMMM", cultureInfo.DateTimeFormat));
                    }
                    else
                    {
                        chartDataLabels.Add(dataLabel);
                    }
                }
            }
            else
            {
                chartDataLabels.AddRange(chartDatasetDtos.FirstOrDefault().DataLabels);
            }

            IEnumerable<int> hiddenSlices = ChartDatasetEntities.FirstOrDefault().ChartDatasetSettings.Where(cds => cds.Value.Hidden).Select(kvp => kvp.Key);

            ChartDto chartDto = new ChartDto(
                ChartType,
                new ChartDataDto(chartDatasetDtos, chartDataLabels, hiddenSlices),
                new ChartOptionsDto
                {
                    Legend = new ChartOptionLegendDto(LegendPosition, ReverseLegend),
                    Title = new ChartOptionTitleDto(Title),
                    Scales = ChartType != ChartType.Pie ? new ChartOptionScalesDto(true) : null
                }
            );

            bool hideSlices = false;
            ChartWrapperDto chartWrapper = new ChartWrapperDto(chartDto, ApplyTotalsToLegend, hiddenSlices.Any());

            return chartWrapper;
        }

        #endregion
    }
}
