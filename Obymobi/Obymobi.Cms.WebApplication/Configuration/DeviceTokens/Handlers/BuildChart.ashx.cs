﻿using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Obymobi.Charts.Dtos;
using Obymobi.Enums;

namespace Obymobi.Cms.WebApplication.Configuration.DeviceTokens.Handlers
{
    /// <summary>
    /// Build the requested chart based on the (AJAX) request
    /// </summary>
    public class BuildChart : IHttpHandler
    {
        #region Properties

        public bool IsReusable => false;

        #endregion

        #region Methods

        /// <summary>
        /// Obtains the requested chart from the database and returns it as a JSON object.
        /// </summary>
        /// <param name="context">The context with the JSON object inside its response.</param>
        public void ProcessRequest(HttpContext context)
        {
            if (CmsSessionHelper.CurrentRole != Role.GodMode)
            {
                context.Response.StatusCode = 401;
                context.Response.Write("Not authorized");

                return;
            }

            bool isValidChartId = int.TryParse(context.Request["chartId"].ToString(), out int chartId);
            if (!isValidChartId)
            {
                context.Response.StatusCode = 404;
                context.Response.Write("Not found");

                return;
            }

            ChartEntity chartEntity = new ChartEntity(chartId);
            ChartWrapperDto chartWrapperDto = chartEntity.RenderChart(CmsSessionHelper.CurrentUser.CultureCode);

            context.Response.ContentType = "application/json";

            var serializerSettings = new JsonSerializerSettings();
            serializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            var json = JsonConvert.SerializeObject(chartWrapperDto, serializerSettings);

            context.Response.Write(json);
        }

        #endregion
    }
}
