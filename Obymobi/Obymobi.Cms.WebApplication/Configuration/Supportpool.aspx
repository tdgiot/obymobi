﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Configuration.Supportpool" Codebehind="Supportpool.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblName">Name</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
							</td> 
						</tr>
                        <tr>
                            <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblSystemSupportPool">Systeem Support Pool</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
                                <D:CheckBox runat="server" ID="cbSystemSupportPool" ClientIDMode="Static" onclick="checkedChanged(this)"></D:CheckBox>
							</td>
						</tr>
						<tr>
                            <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblContentSupportPool">Content Support Pool</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
                                <D:CheckBox runat="server" ID="cbContentSupportPool" />
							</td>
						</tr>
                        <tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblEmail">Email</D:LabelEntityFieldInfo>
							</td>
							<td class="control" rowspan="3">
								<D:TextBoxMultiLine ID="tbEmail" runat="server" IsRequired="false" UseDataBinding="false"></D:TextBoxMultiLine>
                                <br />
                                <D:Label ID="lblEmailComment" runat="server"><small>Meerdere e-mailadressen: Plaats elk e-mailadres op een nieuwe regel.</small></D:Label>
							</td>
                            <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblPhonenumber">Phonenumber</D:LabelEntityFieldInfo>
							</td>
							<td class="control" rowspan="3">
                                <D:TextBoxMultiLine ID="tbPhonenumber" runat="server" IsRequired="false" UseDataBinding="false"></D:TextBoxMultiLine>
                                <br />
                                <D:Label ID="lblPhoneComment" runat="server"><small>Meerdere telefoonnummers: Plaats elk telefoonnummer op een nieuwe regel.</small></D:Label>
							</td> 
						</tr>
					 </table>					
				</Controls>
			</X:TabPage>											
		</TabPages>
	</X:PageControl>
</div>
<script langguage="javascript" type="text/javascript">
function checkedChanged(checkbox) {
    if (checkbox.checked) {
        alert("Warning: enabling system on this support pool automatically disables the current system support pool.");
    }        
}
</script>
</asp:Content>
