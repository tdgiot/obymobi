using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.Injectables.Validators;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Web.UI.WebControls;
using Obymobi.Enums;

namespace Obymobi.ObymobiCms.Configuration
{
    public partial class Supportpool : Dionysos.Web.UI.PageLLBLGenEntity
    {
        #region Methods

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            this.DataSourceLoaded += new Dionysos.Delegates.Web.DataSourceLoadedHandler(Supportpool_DataSourceLoaded);
            base.OnInit(e);
        }

        void Supportpool_DataSourceLoaded(object sender)
        {
            string phonenumberString = this.DataSourceAsSupportpoolEntity.Phonenumber.Replace(", ", "\n");
            this.tbPhonenumber.Text = phonenumberString;
            string emailString = this.DataSourceAsSupportpoolEntity.Email.Replace(", ", "\n");
            this.tbEmail.Text = emailString;
            this.cbSystemSupportPool.Enabled = !this.DataSourceAsSupportpoolEntity.SystemSupportPool;            
        }

        private void LoadUserControls()
        {
            this.tabsMain.AddTabPage("Agents", "SupportpoolSupportagentCollection", "~/Configuration/Subpanels/SupportpoolSupportagentCollection.ascx");
            this.tabsMain.AddTabPage("Notification recipients", "SupportpoolNotificationRecipientCollection", "~/Configuration/Subpanels/SupportpoolNotificationRecipientCollection.ascx");
        }

        #endregion

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
        
        public override bool Save()
        {
            // Format the phonenumber and email fields
            this.FormatFields();

            bool isSaved = false;
            try
            {
                isSaved = base.Save();
            }
            catch (ObymobiException ex)
            {
                if (ex.ErrorEnumValue.Equals(SupportpoolValidator.SupportpoolValidatorResult.AnotherSupportpoolAlreadyIsSystemSupportPool))
                {
                    this.AddInformator(Dionysos.Web.UI.InformatorType.Warning, "Another support pool is already being used as system support pool."); 
                }
            }

            return isSaved;
        }

        private void FormatFields()
        {
            // Phonenumbers
            string[] phonenumbers = this.tbPhonenumber.Text.Split('\n');

            string phonenumberString = "";
            foreach (string phonenumber in phonenumbers)
            {
                if (phonenumber.Contains("\r"))
                    phonenumberString += phonenumber.Replace("\r", ", ");
                else
                    phonenumberString += phonenumber;
            }

            if (phonenumberString.EndsWith(", "))
                phonenumberString = phonenumberString.Substring(0, phonenumberString.LastIndexOf(", "));

            this.DataSourceAsSupportpoolEntity.Phonenumber = phonenumberString;

            // Emails
            string[] emails = this.tbEmail.Text.Split('\n');

            string emailString = "";
            foreach (string email in emails)
            {
                if (email.Contains("\r"))
                    emailString += email.Replace("\r", ", ");
                else
                    emailString += email;
            }
            if (emailString.EndsWith(", "))
                emailString = emailString.Substring(0, emailString.LastIndexOf(", "));

            this.DataSourceAsSupportpoolEntity.Email = emailString;
        }

        #endregion

        #region Properties

        public SupportpoolEntity DataSourceAsSupportpoolEntity
        {
            get
            {
                return this.DataSource as SupportpoolEntity;
            }
        }

        #endregion
    }
}