﻿using System;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Enums;

namespace Obymobi.ObymobiCms.Configuration
{
    public partial class Application : Dionysos.Web.UI.PageLLBLGenEntity
    {
		#region Methods

		protected override void OnInit(EventArgs e)
		{
            DataSourceLoaded += (Application_DataSourceLoaded);

            // Releases tab
            tabsMain.AddTabPage("Releases", "Release", "~/Configuration/SubPanels/ReleaseCollection.ascx");
            // Readme tab
            tabsMain.AddTabPage("Readme's", "Readme", "~/Configuration/SubPanels/ReleaseReadmeCollection.ascx");

			base.OnInit(e);
		}

        private void LoadUserControls()
        {
            var filter = new PredicateExpression(ReleaseFields.ApplicationId == EntityId);
            filter.Add(ReleaseFields.Intermediate == false);
            filter.Add(ReleaseFields.Filename != DBNull.Value);
            filter.Add(ReleaseFields.ReleaseGroup == ReleaseGroup.All);

            ReleaseCollection releases = new ReleaseCollection();
            releases.GetMulti(filter);

            ddlReleaseId.DataSource = releases;
            ddlReleaseId.DataBind();
        }

		#endregion

		#region Event Handlers


        protected void Page_Load(object sender, EventArgs e)
        {
            lblReleaseId.Visible = PageMode != Dionysos.Web.PageMode.Add;
            ddlReleaseId.Visible = PageMode != Dionysos.Web.PageMode.Add;
            tbCode.ReadOnly = PageMode != Dionysos.Web.PageMode.Add;

            if (PageMode != Dionysos.Web.PageMode.Add)
            {
                if (DataSourceAsApplicationEntity.ReleaseId == null)
                {
                    MultiValidatorDefault.AddWarning(Translate("WarningNoStable", "Deze applicatie heeft nog geen stable release."));
                }
                Validate();
            }
            
		}

        void Application_DataSourceLoaded(object sender) => LoadUserControls();

		#endregion

		#region Properties

        public ApplicationEntity DataSourceAsApplicationEntity => DataSource as ApplicationEntity;

		#endregion
    }
}
