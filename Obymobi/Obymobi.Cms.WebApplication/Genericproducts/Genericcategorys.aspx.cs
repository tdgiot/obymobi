﻿using System;
using System.Collections.Generic;
using System.Linq;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;

namespace Obymobi.ObymobiCms.Genericproducts
{
	public partial class Genericcategorys : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.MainGridView.LoadComplete += MainGridView_LoadComplete;
        }

        void MainGridView_LoadComplete(object sender, EventArgs e)
        {
            if (this.MainGridView.Columns["BrandName"] != null)
            {
                this.MainGridView.Columns["BrandName"].Visible = false;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            LLBLGenProDataSource datasource = this.DataSource as LLBLGenProDataSource;
            if (datasource != null)
            {
                datasource.FilterToUse = new PredicateExpression(GenericcategoryFields.BrandId == DBNull.Value);
            }
        }

        #endregion
    }
}

