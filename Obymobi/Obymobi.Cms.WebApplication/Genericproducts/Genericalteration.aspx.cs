﻿using System;
using System.Linq;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Web.UI.DevExControls;
using Dionysos;
using Obymobi.Logic.Cms;
using Obymobi.Enums;
using DevExpress.Web;
using Dionysos.Web;
using Dionysos.Web.UI;

namespace Obymobi.ObymobiCms.Genericproducts
{
    public partial class Genericalteration : Dionysos.Web.UI.PageLLBLGenEntity
    {
        #region Methods

        void HookUpEvents()
        {
            this.btRemoveAlterationFromProducts.Click += new EventHandler(btRemoveAlterationFromProducts_Click);
        }

        private void LoadUserControls()
        {
            // Add languages
            //foreach (LanguageEntity language in CmsSessionHelper.Languages)
            //{
                //AlterationLanguagePanel alterationLanguagePanel = this.tabsMain.AddTabPage(language.Code, String.Format("Language-{0}", language.LanguageId), "~/Catalog/SubPanels/AlterationLanguagePanel.ascx", "tabAdvanced", true) as AlterationLanguagePanel;
                //alterationLanguagePanel.LanguageId = language.LanguageId;
            //}

            this.tabsMain.AddTabPage("Opties", "Options", "~/Genericproducts/SubPanels/GenericalterationitemCollection.ascx", "tabAdvanced", true);
            //this.tabsMain.AddTabPage("Afbeeldingen", "Media", "~/Generic/UserControls/MediaCollection.ascx");
        }

        void SetGui()
        {
            if (this.DataSourceAsGenericalterationEntity.BrandId.HasValue)
            {
                this.ddlBrandId.Enabled = false;
            }
            else
            {
                this.ddlBrandId.DataSource = CmsSessionHelper.GetBrandCollectionForUser(BrandRole.Editor).Select(entity => entity.BrandId);
            }

            if (CmsSessionHelper.CurrentRole < Role.Administrator)
            {
                this.tabsMain.TabPages.FindByName("tabAdvanced").Visible = false;
            }
            
            DateTime midnight = DateTime.Now;
            midnight = midnight.AddHours(-midnight.Hour);
            midnight = midnight.AddMinutes(-midnight.Minute);
            if (this.DataSourceAsGenericalterationEntity.MinLeadMinutes > 0)
            {
                DateTime minLeadMinutes = midnight.AddMinutes(this.DataSourceAsGenericalterationEntity.MinLeadMinutes);
                this.teMinLeadMinutes.Value = minLeadMinutes;
            }

            if (this.DataSourceAsGenericalterationEntity.IntervalMinutes > 0)
            {
                DateTime intervalMinutes = midnight.AddMinutes(this.DataSourceAsGenericalterationEntity.IntervalMinutes);
                this.teIntervalMinutes.Value = intervalMinutes;
            }

            if (this.DataSourceAsGenericalterationEntity.Type == AlterationType.Date)
            {
                this.teMaxLeadDays.Value = this.DataSourceAsGenericalterationEntity.MaxLeadHours / 24;
            }
            else if (this.DataSourceAsGenericalterationEntity.Type == AlterationType.DateTime)
            {
                this.teMaxLeadHours.Value = this.DataSourceAsGenericalterationEntity.MaxLeadHours;
            }

            if (this.DataSourceAsGenericalterationEntity.Type == AlterationType.DateTime)
            {
                this.plhMinMaxOptions.Visible = false;
                this.plhTimeOptions.Visible = true;
                this.plhMaxDays.Visible = false;
            }
            else if (this.DataSourceAsGenericalterationEntity.Type == AlterationType.Date)
            {
                this.plhMinMaxOptions.Visible = false;
                this.plhTimeOptions.Visible = false;
                this.plhMaxDays.Visible = true;
            }
            else
            {
                this.plhMinMaxOptions.Visible = true;
                this.plhTimeOptions.Visible = false;
                this.plhMaxDays.Visible = false;
            }

            // Render all products that use this Alteration
            if (this.RenderProducts())
            {
                this.ddlBrandId.Enabled = false;
            }
        }

        bool RenderProducts()
        {
            var products = new GenericproductCollection();
            var filter = new PredicateExpression();
            filter.Add(GenericproductGenericalterationFields.GenericalterationId == this.EntityId);

            var joins = new RelationCollection();
            joins.Add(GenericproductEntityBase.Relations.GenericproductGenericalterationEntityUsingGenericproductId);

            var sort = new SortExpression();
            sort.Add(ProductFields.Name | SortOperator.Ascending);

            products.GetMulti(filter, 0, null, joins);

            foreach (var product in products)
            {
                this.plhProducts.AddHtml("<tr><td><a href=\"{0}\" target=\"_blank\">{1}</a></td></tr>", this.ResolveUrl("~/Genericproducts/Genericproduct.aspx?Id=" + product.GenericproductId), product.Name);
            }

            return products.Count > 0;
        }

        private void SetFilters()
        {

        }

        private void SetWarnings()
        {
            if ((this.DataSourceAsGenericalterationEntity.Type == AlterationType.Options && this.DataSourceAsGenericalterationEntity.GenericalterationitemCollection.Count == 0))
                this.AddInformator(Dionysos.Web.UI.InformatorType.Warning, this.Translate("NoAlterationoptions", "Deze productsamenstelling heeft nog geen opties."));


            this.Validate();
        }

        public override bool Save()
        {
            if (this.teStartTime.Value.HasValue)
                this.DataSourceAsGenericalterationEntity.StartTime = DateTimeUtil.CombineDateAndTime(this.teStartTime.Value.Value, DateTime.Now);

            if (this.teEndTime.Value.HasValue)
                this.DataSourceAsGenericalterationEntity.EndTime = DateTimeUtil.CombineDateAndTime(this.teEndTime.Value.Value, DateTime.Now);

            if (this.teMinLeadMinutes.Value.HasValue)
                this.DataSourceAsGenericalterationEntity.MinLeadMinutes = (this.teMinLeadMinutes.Value.Value.Hour * 60) + this.teMinLeadMinutes.Value.Value.Minute;

            if (this.teIntervalMinutes.Value.HasValue)
                this.DataSourceAsGenericalterationEntity.IntervalMinutes = (this.teIntervalMinutes.Value.Value.Hour * 60) + this.teIntervalMinutes.Value.Value.Minute;

            if (this.DataSourceAsGenericalterationEntity.Type == AlterationType.Date)
            {
                if (this.teMaxLeadDays.Value.HasValue)
                {
                    this.DataSourceAsGenericalterationEntity.MaxLeadHours = this.teMaxLeadDays.Value.Value * 24;
                }
            }
            else if (this.DataSourceAsGenericalterationEntity.Type == AlterationType.DateTime)
            {
                if (this.teMaxLeadHours.Value.HasValue)
                {
                    this.DataSourceAsGenericalterationEntity.MaxLeadHours = this.teMaxLeadHours.Value.Value;
                }
            }

            return base.Save();
        }

        public override bool Delete()
        {
            try
            {
                return base.Delete();
            }
            catch (Exception ex)
            {
                this.AddInformator(Dionysos.Web.UI.InformatorType.Warning, ex.Message);
            }

            return false;
        }

        #endregion


        #region Properties

        /// <summary>
        /// Return the page's datasource as a Entity
        /// </summary>
        public GenericalterationEntity DataSourceAsGenericalterationEntity
        {
            get
            {
                return this.DataSource as GenericalterationEntity;
            }
        }

        #endregion

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            this.DataSourceLoaded += (Genericalteration_DataSourceLoaded);
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.DataSourceAsGenericalterationEntity.BrandId.HasValue)
            {
                if (PageMode != PageMode.Add)
                {
                    if (CmsSessionHelper.GetUserRoleForBrand(this.DataSourceAsGenericalterationEntity.BrandId.Value).GetValueOrDefault(BrandRole.Viewer) == BrandRole.Viewer)
                    {
                        AddInformator(InformatorType.Warning, "You don't have the permissions to edit this product alteration");

                        this.tabsMain.ToggleAllControls(false);

                        MasterPages.MasterPageEntity masterPage = this.Master as MasterPages.MasterPageEntity;
                        if (masterPage != null)
                            masterPage.ToolBar.IsReadOnly = true;

                        Validate();
                    }
                }
            }

            if (!this.IsPostBack)
                this.SetWarnings();

            this.HookUpEvents();
        }

        void Genericalteration_DataSourceLoaded(object sender)
        {
            this.SetFilters();
            this.SetGui();
        }

        void btRemoveAlterationFromProducts_Click(object sender, EventArgs e)
        {
            //int removedFromXProducts = 0;
            //AlterationHelper.RemoveAlterationFromProducts(this.DataSourceAsGenericalterationEntity.AlterationId, out removedFromXProducts);

            //this.AddInformatorInfo(string.Format(this.Translate("OrderFailedProductNotSpecified", "Deze alteration is van {0} producten verwijderd."), removedFromXProducts));
        }

        #endregion
    }
}