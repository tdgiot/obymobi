using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Web.UI.DevExControls;
using Dionysos;
using Dionysos.Data.LLBLGen;
using Obymobi.Logic.Cms;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.ObymobiCms.Genericproducts
{
    public partial class Genericalterationitem : Dionysos.Web.UI.PageLLBLGenEntity
    {
        #region Methods

        protected override void OnInit(EventArgs e)
        {
            this.SetFilters();
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.SetGui();
        }

        private void SetFilters()
        {
            SortExpression sort2 = new SortExpression(new SortClause(GenericalterationoptionFields.Name, SortOperator.Ascending));

            GenericalterationoptionCollection alterationoptionCollection = new GenericalterationoptionCollection();
            alterationoptionCollection.GetMulti(null, 0, sort2);

            this.ddlGenericalterationoptionId.DataSource = alterationoptionCollection;
            this.ddlGenericalterationoptionId.DataBind();

            SortExpression sort = new SortExpression(new SortClause(GenericalterationFields.Name, SortOperator.Ascending));

            GenericalterationCollection alterationCollection = new GenericalterationCollection();
            alterationCollection.GetMulti(null, 0, sort);

            this.ddlGenericalterationId.DataSource = alterationCollection;
            this.ddlGenericalterationId.DataBind();
        }

        private void SetGui()
        {
            if (!this.DataSourceAsGenericalterationitemEntity.IsNew)
            {
                this.hlGenericalterationoptionId.Text = this.DataSourceAsGenericalterationitemEntity.GenericalterationoptionEntity.Name + " &#187;&#187;";
                this.hlGenericalterationoptionId.Target = "_Blank";
                this.hlGenericalterationoptionId.NavigateUrl = ResolveUrl(string.Format("~/Genericproducts/Genericalterationoption.aspx?id={0}", this.DataSourceAsGenericalterationitemEntity.GenericalterationoptionId));

                this.hlGenericalterationId.Text = this.DataSourceAsGenericalterationitemEntity.GenericalterationEntity.Name + " &#187;&#187;";
                this.hlGenericalterationId.Target = "_Blank";
                this.hlGenericalterationId.NavigateUrl = ResolveUrl(string.Format("~/Genericproducts/Genericalteration.aspx?id={0}", this.DataSourceAsGenericalterationitemEntity.GenericalterationId));
            }
        }

        #endregion
        
		#region Properties

		
		/// <summary>
		/// Return the page's datasource as a Entity
		/// </summary>
		public GenericalterationitemEntity	DataSourceAsGenericalterationitemEntity
		{
			get
			{
                return this.DataSource as GenericalterationitemEntity;
			}
		}

		#endregion        
    }
}
