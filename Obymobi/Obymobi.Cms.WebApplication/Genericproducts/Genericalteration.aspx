﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Genericproducts.Genericalteration" Codebehind="Genericalteration.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<controls>
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblName">Naam</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
							</td>	  
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblBrand">Brand</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlBrandId" IncrementalFilteringMode="StartsWith" EntityName="Brand" TextField="Name" ValueField="BrandId" />
                            </td>    
						</tr>
                        <tr>
                            <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblType">Type</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
                                <X:ComboBoxEnum ID="ddlType" runat="server" Type="Obymobi.Enums.AlterationType, Obymobi" ></X:ComboBoxEnum>
							</td>  
                            <td class="label"></td>
                            <td class="control"></td>
                        </tr>
                    </table>
                    <D:PlaceHolder ID="plhMinMaxOptions" runat="server">
                        <table class="dataformV2">
                            <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblMinOptions">Minimale opties</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <D:TextBoxInt runat="server" ID="tbMinOptions" AllowZero="true" AllowNegative="false"></D:TextBoxInt>
							    </td>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblMaxOptions">Maximale opties</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <D:TextBoxInt runat="server" ID="tbMaxOptions" AllowZero="true" AllowNegative="false"></D:TextBoxInt>
							    </td>
						    </tr>		
                    	</table>
                    </D:PlaceHolder>
					<D:PlaceHolder ID="plhTimeOptions" runat="server">
                        <table class="dataformV2">
                             <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblStartTime">Start time</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
                                    <X:TimeEdit runat="server" ID="teStartTime"></X:TimeEdit>
							    </td>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblEndTime">End time</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <X:TimeEdit runat="server" ID="teEndTime"></X:TimeEdit>
							    </td>
						    </tr>	
                            <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblMinLeadMinutes">Minimum lead time</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
                                    <X:TimeEdit runat="server" ID="teMinLeadMinutes" UseDataBinding="false"></X:TimeEdit>
							    </td>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblMaxLeadHours">Maximum lead hours</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <D:TextBoxInt runat="server" ID="teMaxLeadHours" UseDataBinding="false"></D:TextBoxInt>
							    </td>
						    </tr>	
                                <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblIntervalMinutes">Interval time</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
                                    <X:TimeEdit runat="server" ID="teIntervalMinutes" UseDataBinding="false"></X:TimeEdit>
							    </td>
							    <td class="label">
							    </td>
							    <td class="control">
							    </td>
						    </tr>
                            <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblShowDatePicker">Show date picker</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <D:CheckBox runat="server" ID="cbShowDatePicker" />
							    </td>
                                <td class="label">
                                </td>
                                <td class="control">
							    </td>
						    </tr>	
                        </table>
					</D:PlaceHolder>
					<D:PlaceHolder ID="plhMaxDays" runat="server">
                        <table class="dataformV2">
                             <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lvlMaxLeadDays">Maximum lead days</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
                                    <D:TextBoxInt runat="server" ID="teMaxLeadDays" UseDataBinding="false"></D:TextBoxInt>
							    </td>
							    <td class="label">
								</td>
							    <td class="control">
							    </td>
						    </tr>	
                        </table>
                    </D:PlaceHolder>		 
				</controls>
			</X:TabPage>														
            <X:TabPage Text="Technical" LocalizeText="false" Name="tabAdvanced">
                <controls>
				<D:PlaceHolder ID="PlaceHolder1" runat="server">
					<table class="dataformV2">
				        <tr>
                            <td class="label">
								<D:LabelTextOnly id="lblActions" LocalizeText="false" Text="Actions" runat="server" />
							</td>
							<td class="control">
							    <D:Button ID="btRemoveAlterationFromProducts" LocalizeText="false" Text="Remove this option from all products" PreSubmitWarning="Are you sure you want to remove this alteration from all products?" runat="server" /><br />                                
							</td>
							<td class="label">
							</td>
							<td class="control">
							</td>
						</tr>
                    </table>
                </D:PlaceHolder>
                </controls>
            </X:TabPage>
            <X:TabPage Text="Producten" Name="tabForProducts">
                <controls>
                    <D:PlaceHolder ID="PlaceHolder2" runat="server">
                        <D:LabelTextOnly ID="lblProductsUsingThisAlteration" LocalizeText="false" runat="server">The following products are using this generic alteration:</D:LabelTextOnly>
                        &nbsp;
                        <table>
                            <D:PlaceHolder runat="server" ID="plhProducts"></D:PlaceHolder>
                        </table>
                    </D:PlaceHolder>
                </controls>
            </X:TabPage>
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>

