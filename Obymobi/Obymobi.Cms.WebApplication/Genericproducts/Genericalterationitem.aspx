﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Genericproducts.Genericalterationitem" Codebehind="Genericalterationitem.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<controls>
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblGenericalterationoptionId">Alteration option</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlGenericalterationoptionId" EntityName="Genericalterationoption" IncrementalFilteringMode="StartsWith" TextField="Name" ValueField="GenericalterationoptionId"></X:ComboBoxLLBLGenEntityCollection>
							</td>
                            <td class="label">
                            </td>
                            <td class="control">
                                <D:HyperLink runat="server" LocalizeText="false" ID="hlGenericalterationoptionId" Target="_blank"></D:HyperLink>
                            </td>
						</tr>
                        <tr>
							<td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblGenericalterationId">Alteration</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlGenericalterationId" EntityName="Genericalteration" DisabledOnceSelectedFromQueryString="True" DisabledOnceSelectedAndSaved="True" IncrementalFilteringMode="StartsWith" TextField="Name" PreventEntityCollectionInitialization="True" ValueField="GenericalterationId"></X:ComboBoxLLBLGenEntityCollection>
							</td> 
                            <td class="label">
                            </td>
                            <td class="control">
                                <D:HyperLink runat="server" LocalizeText="false" ID="hlGenericalterationId"  Target="_blank"></D:HyperLink>
                            </td>
                        </tr>
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblSortOrder">Sort order</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
							    <D:TextBoxInt ID="tbSortOrder" runat="server"></D:TextBoxInt>
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblSelectedOnDefault">Selected on default</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:CheckBox runat="server" ID="cbSelectedOnDefault" />
							</td>
                        </tr>
					 </table>			
				</controls>
			</X:TabPage>	
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>

