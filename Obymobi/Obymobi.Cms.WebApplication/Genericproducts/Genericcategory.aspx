﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Genericproducts.Genericcategory" Codebehind="Genericcategory.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblName">Name</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
							</td>
							<td class="label">
								<D:Label runat="server" id="lblParentGenericcategoryId">Bovenliggende categorie</D:Label>
							</td>
							<td class="control">
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlParentGenericcategoryId" IncrementalFilteringMode="StartsWith" EntityName="Genericcategory" TextField="Name" ValueField="GenericcategoryId" />
							</td>	
					    </tr>					  
                        <tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblBrand">Brand</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlBrandId" IncrementalFilteringMode="StartsWith" EntityName="Brand" TextField="Name" ValueField="BrandId" />
							</td>
							<td class="label">

							</td>
							<td class="control">
							</td>	
					    </tr>		
					 </table>			
				</Controls>
			</X:TabPage>						
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>

