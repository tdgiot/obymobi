﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;

namespace Obymobi.ObymobiCms.Genericproducts
{
	public partial class Supplier : Dionysos.Web.UI.PageLLBLGenEntity
    {

		#region Fields
		#endregion

		#region Methods

		protected override void OnInit(EventArgs e)
		{
			this.DataSourceLoaded += new Dionysos.Delegates.Web.DataSourceLoadedHandler(Deliverypoint_DataSourceLoaded);
			base.OnInit(e);			
		}

		void SetGui()
		{

		}

		#endregion

		#region Event Handlers

		protected void Page_Load(object sender, EventArgs e)
		{

		}

		void Deliverypoint_DataSourceLoaded(object sender)
		{
			this.SetGui();
		}


		#endregion

		#region Properties
		#endregion

    }
}
