﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dionysos.Web.UI;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.ObymobiCms.Genericproducts
{
	public partial class Genericcategory : Dionysos.Web.UI.PageLLBLGenEntity
    {
		#region Methods

		protected override void OnInit(EventArgs e)
		{
            this.LoadUserControls();
			this.DataSourceLoaded += new Dionysos.Delegates.Web.DataSourceLoadedHandler(Deliverypoint_DataSourceLoaded);
			base.OnInit(e);			
		}

        private void LoadUserControls()
        {
            this.tabsMain.AddTabPage("Translations", "CustomTextCollection", "~/Generic/UserControls/CustomTextCollection.ascx");
            this.tabsMain.AddTabPage("Afbeeldingen", "Media", "~/Generic/UserControls/MediaCollection.ascx");
        }

		void SetGui()
		{
		    PredicateExpression filter = null;
            if (this.DataSourceAsGenericcategoryEntity.BrandId.HasValue)
            {
                this.ddlBrandId.Enabled = false;

                filter = new PredicateExpression(GenericcategoryFields.BrandId == this.DataSourceAsGenericcategoryEntity.BrandId.Value);
            }
            else
            {
                this.ddlBrandId.DataSource = CmsSessionHelper.GetBrandCollectionForUser(BrandRole.Editor).Select(entity => entity.BrandId);
            }

			GenericcategoryCollection gcs = new GenericcategoryCollection();
			SortExpression sortGcs = new SortExpression(GenericcategoryFields.Name | SortOperator.Ascending);
			gcs.GetMulti(filter, 0, sortGcs);

			this.ddlParentGenericcategoryId.DataSource = gcs;
			this.ddlParentGenericcategoryId.DataBind();
		}

		#endregion

		#region Event Handlers

		protected void Page_Load(object sender, EventArgs e)
		{

		}

		private void Deliverypoint_DataSourceLoaded(object sender)
		{
			this.SetGui();
		}

        public override bool Save()
        {
            if (this.ddlBrandId.Value.HasValue && this.ddlParentGenericcategoryId.Value.HasValue)
            {
                // Check if generic category is part of this brand
                GenericcategoryEntity genericcategoryEntity = new GenericcategoryEntity(this.ddlParentGenericcategoryId.Value.Value);
                if (!genericcategoryEntity.IsNew && genericcategoryEntity.BrandId != this.ddlBrandId.Value.Value)
                {
                    this.AddInformator(InformatorType.Warning, "The selected generic category is not part of the selected brand.");
                    return false;
                }
            }

            if (base.Save())
            {
                CustomTextHelper.UpdateCustomTexts(this.DataSource);
                return true;
            }
            else
                return false;
        }

		#endregion

        private GenericcategoryEntity DataSourceAsGenericcategoryEntity
        {
            get
            {
                return this.DataSource as GenericcategoryEntity;
            }
        }
    }
}
