﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using Dionysos;
using Dionysos.Web;
using Dionysos.Web.UI;
using Dionysos.Web.UI.DevExControls;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.ObymobiCms.Genericproducts
{
	public partial class Genericproduct : Dionysos.Web.UI.PageLLBLGenEntity
    {
        private string notSetText = "- " + Dionysos.Global.TranslationProvider.GetTranslation(ObymobiCmsTranslatables.NotSet) + " -";

		#region Methods

		protected override void OnInit(EventArgs e)
		{
            this.LoadUserControls();
			this.DataSourceLoaded += new Dionysos.Delegates.Web.DataSourceLoadedHandler(Deliverypoint_DataSourceLoaded);
			base.OnInit(e);			
		}

        private void LoadUserControls()
        {
            Generic.UserControls.CustomTextCollection translationsPanel = this.tabsMain.AddTabPage("Translations", "CustomTextCollection", "~/Generic/UserControls/CustomTextCollection.ascx") as Generic.UserControls.CustomTextCollection;
            translationsPanel.FullWidth = true;

            if (this.PageMode != Dionysos.Web.PageMode.Add)
                this.tabsMain.AddTabPage("Attachments", "Attachments", "~/Generic/SubPanels/AttachmentCollection.ascx");
            this.tabsMain.AddTabPage("Afbeeldingen", "Media", "~/Generic/UserControls/MediaCollection.ascx");
        }

		void SetGui()
		{
            if (this.DataSourceAsGenericproductEntity.BrandId.HasValue)
            {
                this.ddlBrandId.Enabled = false;

                IEntityCollection collection = this.ddlGenericcategoryId.DataSource as IEntityCollection;
                collection.DefaultView.Filter = new PredicateExpression(GenericcategoryFields.BrandId == this.DataSourceAsGenericproductEntity.BrandId.Value);
                this.ddlGenericcategoryId.DataBind();
            }
            else
            {
                this.ddlBrandId.DataSource = CmsSessionHelper.GetBrandCollectionForUser(BrandRole.Editor).Select(entity => entity.BrandId);
            }

            this.ddlSubType.Items.Add(this.Translate("InheritFromCategory", "Inherit from category"), (int)ProductSubType.Inherit);
            this.ddlSubType.Items.Add(this.Translate("Normal", "Normal"), (int)ProductSubType.Normal);
            this.ddlSubType.Items.Add(this.Translate("ServiceItem", "Service item"), (int)ProductSubType.ServiceItems);
            this.ddlSubType.Items.Add(this.Translate("DirectoryItem", "Directory item"), (int)ProductSubType.Directory);
            this.ddlSubType.Items.Add(this.Translate("WebLink", "Web link"), (int)ProductSubType.WebLink);

            if (this.DataSourceAsGenericproductEntity.IsNew && this.DataSourceAsGenericproductEntity.SubType > 0)
            {
                this.ddlSubType.Value = this.DataSourceAsGenericproductEntity.SubType;
            }

            this.plhWebLink.Visible = (ProductSubType)this.DataSourceAsGenericproductEntity.SubType == ProductSubType.WebLink;

            // Determine the need for WebType Urls
            this.tbWebTypeSmartphoneUrl.IsRequired = this.plhWebLink.Visible;
            this.tbWebTypeTabletUrl.IsRequired = this.plhWebLink.Visible;    
		}

        private void SetWarnings()
        {
            if (CmsSessionHelper.CurrentCompanyId > 0)
            {
                CompanyEntity currentCompany = new CompanyEntity(CmsSessionHelper.CurrentCompanyId);
                if (!currentCompany.IsNew)
                {
                    if (currentCompany.SystemType == SystemType.Otoucho)
                    {
                        if (!this.DataSourceAsGenericproductEntity.HasProductButton)
                            this.AddInformator(Dionysos.Web.UI.InformatorType.Warning, this.Translate("ButtonImageNotSet", "Dit product heeft nog geen knop afbeelding."));

                        if (!this.DataSourceAsGenericproductEntity.HasProductBranding)
                            this.AddInformator(Dionysos.Web.UI.InformatorType.Warning, this.Translate("BrandingImageNotSet", "Dit product heeft nog geen branding afbeelding."));

                        if (!this.DataSourceAsGenericproductEntity.HasSupplierId)
                            this.AddInformator(Dionysos.Web.UI.InformatorType.Warning, this.Translate("SupplierNotSet", "Dit product heeft nog geen leverancier."));

                        if (!this.DataSourceAsGenericproductEntity.HasTextColor)
                            this.AddInformator(Dionysos.Web.UI.InformatorType.Warning, this.Translate("TextColourNotSet", "Dit product heeft nog geen tekstkleur."));

                        if (!this.DataSourceAsGenericproductEntity.HasBackgroundColor)
                            this.AddInformator(Dionysos.Web.UI.InformatorType.Warning, this.Translate("BackgroundColourNotSet", "Dit product heeft nog geen achtergrondkleur."));

                        if (!this.DataSourceAsGenericproductEntity.HasDescription)
                            this.AddInformator(Dionysos.Web.UI.InformatorType.Warning, this.Translate("DescriptionNotSet", "Dit product heeft nog geen omschrijving."));
                    }
                    else if (currentCompany.SystemType == SystemType.Crave)
                    {
                        if (!this.DataSourceAsGenericproductEntity.HasProductButtonCrave)
                            this.AddInformator(Dionysos.Web.UI.InformatorType.Warning, this.Translate("ButtonImageNotSet", "Dit product heeft nog geen knop afbeelding."));

                        if (!this.DataSourceAsGenericproductEntity.HasProductBrandingCrave)
                            this.AddInformator(Dionysos.Web.UI.InformatorType.Warning, this.Translate("BrandingImageNotSet", "Dit product heeft nog geen branding afbeelding."));

                        if (!this.DataSourceAsGenericproductEntity.HasProductButton1280x800)
                            this.AddInformator(Dionysos.Web.UI.InformatorType.Warning, this.Translate("ButtonImageNotSet1280x800", "Dit product heeft nog geen knop afbeelding voor resolutie 1280x800."));

                        if (!this.DataSourceAsGenericproductEntity.HasProductBranding1280x800)
                            this.AddInformator(Dionysos.Web.UI.InformatorType.Warning, this.Translate("BrandingImageNotSet1280x800", "Dit product heeft nog geen branding afbeelding voor resolutie 1280x800."));

                        if (!this.DataSourceAsGenericproductEntity.HasSupplierId)
                            this.AddInformator(Dionysos.Web.UI.InformatorType.Warning, this.Translate("SupplierNotSet", "Dit product heeft nog geen leverancier."));

                        if (!this.DataSourceAsGenericproductEntity.HasDescription)
                            this.AddInformator(Dionysos.Web.UI.InformatorType.Warning, this.Translate("DescriptionNotSet", "Dit product heeft nog geen omschrijving."));
                    }

                    this.Validate();
                }
            }
        }

        private void SetProductAlterations()
        {
            var alterations = new GenericalterationCollection();
            PredicateExpression filter = new PredicateExpression();
            if (this.DataSourceAsGenericproductEntity.BrandId.HasValue)
            {
                filter.Add(GenericalterationFields.BrandId == this.DataSourceAsGenericproductEntity.BrandId);
            }
            else
            {
                filter.Add(GenericalterationFields.BrandId == DBNull.Value);
            }

            var sort = new SortExpression(GenericalterationFields.Name | SortOperator.Ascending);
            alterations.GetMulti(filter, 0, sort);

            EntityView<GenericproductGenericalterationEntity> paView = this.DataSourceAsGenericproductEntity.GenericproductGenericalterationCollection.DefaultView;

            // For backwards compatability, set SortOrders when not set			
            #region Fix for backwards compatibility
            bool fix = false;
            foreach (var pa in paView)
            {
                if (pa.SortOrder == 0)
                {
                    fix = true;
                    break;
                }
            }

            if (fix)
            {
                int sortOrder = 1;
                foreach (var pa in paView)
                {
                    pa.SortOrder = sortOrder;
                    pa.Save();
                    sortOrder++;
                }
            }
            #endregion

            foreach (var control in this.ControlList)
            {
                if (!control.ID.IsNullOrWhiteSpace() && control.ID.StartsWith("ddlProductAlteration"))
                {
                    var ddl = (ComboBoxInt)control;
                    ddl.DisplayEmptyItem = false;
                    ddl.DataSource = alterations;
                    ddl.DataBind();

                    ddl.Items.Insert(0, new DevExpress.Web.ListEditItem(this.notSetText, -1));
                    ddl.SelectedIndex = 0;

                    // Check if this category has a selected alteration for this position
                    var sortOrder = Convert.ToInt32(ddl.ID.GetAllAfterFirstOccurenceOf("ddlProductAlteration"));
                    paView.Filter = new PredicateExpression(GenericproductGenericalterationFields.SortOrder == sortOrder);

                    if (paView.Count == 1)
                    {
                        ddl.Value = paView[0].GenericalterationId;

                        var hlControl = this.ControlList.FindControl("hlProductAlteration" + sortOrder) as Dionysos.Web.UI.WebControls.HyperLink;
                        if (hlControl != null)
                        {
                            hlControl.Text = paView[0].GenericalterationEntity.Name + " &#187;&#187;";
                            hlControl.Target = "_Blank";
                            hlControl.NavigateUrl = this.ResolveUrl(string.Format("~/Genericproducts/Genericalteration.aspx?id={0}", paView[0].GenericalterationEntity.GenericalterationId));
                        }
                    }
                }
            }
        }

		#endregion

        #region Properties

        public GenericproductEntity DataSourceAsGenericproductEntity
        {
            get
            {
                return this.DataSource as GenericproductEntity;
            }
        }

        #endregion

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
		{
		    if (this.DataSourceAsGenericproductEntity.BrandId.HasValue)
		    {
		        if (PageMode != PageMode.Add)
		        {
		            if (CmsSessionHelper.GetUserRoleForBrand(this.DataSourceAsGenericproductEntity.BrandId.Value).GetValueOrDefault(BrandRole.Viewer) == BrandRole.Viewer)
		            {
		                AddInformator(InformatorType.Warning, "You don't have the permissions to edit this product");

		                this.tabsMain.ToggleAllControls(false);
		                this.tabsMain.GetTabPageByName("Attachments").Enabled = false;
                        this.tabsMain.GetTabPageByName("Media").Enabled = false;

		                MasterPages.MasterPageEntity masterPage = this.Master as MasterPages.MasterPageEntity;
		                if (masterPage != null)
		                    masterPage.ToolBar.IsReadOnly = true;

		                Validate();
		            }
		        }
		    }

		    if (!IsPostBack)
                this.SetWarnings();
		}

		private void Deliverypoint_DataSourceLoaded(object sender)
		{
            this.SetProductAlterations();

            this.ddlViewLayoutType.DataBindEnum<ViewLayoutType>();

			this.SetGui();
		}

        public override bool Save()
        {
            if (this.ddlBrandId.Value.HasValue && this.ddlGenericcategoryId.Value.HasValue)
            {
                // Check if generic category is part of this brand
                GenericcategoryEntity genericcategoryEntity = new GenericcategoryEntity(this.ddlGenericcategoryId.Value.Value);
                if (!genericcategoryEntity.IsNew && genericcategoryEntity.BrandId != this.ddlBrandId.Value.Value)
                {
                    this.AddInformator(InformatorType.Warning, "The selected generic category is not part of the selected brand.");
                    return false;
                }
            }

            if (base.Save())
            {
                CustomTextHelper.UpdateCustomTexts(this.DataSource);
                return true;
            }
            else
                return false;
        }

		#endregion
    }
}
