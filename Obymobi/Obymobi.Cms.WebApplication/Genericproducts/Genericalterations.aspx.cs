﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Cms;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Genericproducts
{
    public partial class Genericalterations : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "Genericalteration";
            this.EntityPageUrl = "~/Genericproducts/Genericalteration.aspx";
            base.OnInit(e);

            if (CmsSessionHelper.CurrentRole < Role.Reseller)
            {
                var masterPage = this.Master as MasterPages.MasterPageEntityCollection;
                if (masterPage != null)
                    masterPage.ToolBar.AddButton.Visible = false;
            }

            this.MainGridView.LoadComplete += MainGridView_LoadComplete;
        }

        void MainGridView_LoadComplete(object sender, EventArgs e)
        {
            if (this.MainGridView.Columns["BrandName"] != null)
            {
                this.MainGridView.Columns["BrandName"].Visible = false;
            }
        }   

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.MainGridView != null)
            {
                this.MainGridView.Styles.Cell.HorizontalAlign = HorizontalAlign.Left;
            }

            LLBLGenProDataSource datasource = this.DataSource as LLBLGenProDataSource;
            if (datasource != null)
            {
                datasource.FilterToUse = new PredicateExpression(GenericalterationFields.BrandId == DBNull.Value);
            }
        }

        #endregion
    }
}
