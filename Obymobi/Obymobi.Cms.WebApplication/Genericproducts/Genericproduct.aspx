﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Genericproducts.Genericproduct" Codebehind="Genericproduct.aspx.cs" %>
<%@ Reference VirtualPath="~/Generic/UserControls/CustomTextCollection.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
				    <D:PlaceHolder runat="server">
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblName">Name</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblBrandId">Brand</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlBrandId" CssClass="input" UseDataBinding="true" EntityName="Brand"></X:ComboBoxLLBLGenEntityCollection>  
							</td>							
						</tr>
                        <tr>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblGenericcategoryId">Categorie</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlGenericcategoryId" CssClass="input" UseDataBinding="true" EntityName="Genericcategory"></X:ComboBoxLLBLGenEntityCollection>            
                            </td>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblSubType">Subtype</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <X:ComboBoxInt runat="server" ID="ddlSubType"></X:ComboBoxInt>
                            </td>
                        </tr>
						<tr>
                            <td class="label">
								<D:Label runat="server" id="lblVattariffId">BTW-tarief</D:Label>
							</td>
							<td class="control">
								<X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlVattariffId" EntityName="Vattariff" IncrementalFilteringMode="StartsWith" TextField="Name" ValueField="VattariffId" ></X:ComboBoxLLBLGenEntityCollection>
							</td>     
							<td class="label">
							    <D:LabelEntityFieldInfo runat="server" id="lblSupplierId">Leverancier</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
							    <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlSupplierId" CssClass="input" UseDataBinding="true" EntityName="Supplier"></X:ComboBoxLLBLGenEntityCollection>  
							</td>
						</tr>
                        <tr>
      					    <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblPriceIn">Price in</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
							    <D:TextBoxDecimal ID="tbPriceIn" runat="server"></D:TextBoxDecimal>
							</td>
                            <td class="label">
                            </td>
                            <td class="control">
                            </td>
						</tr>
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblTextColor">Tekstkleur</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbTextColor" runat="server"></D:TextBoxString>
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblBackgroundColor">Achtergrondkleur</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbBackgroundColor" runat="server"></D:TextBoxString>
							</td>
						</tr>					
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblDescription">Omschrijving</D:LabelEntityFieldInfo>
							</td>
							<td class="control threeCol" colspan="3">
								<D:TextBoxMultiLine ID="tbDescription" runat="server" Rows="10" UseDataBinding="true"></D:TextBoxMultiLine>
							</td>       
						</tr>		
                                                <D:PlaceHolder ID="plhWebLink" runat="server" Visible="false">
                        <tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblWebTypeSmartphoneUrl">Web-type smartphone url</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxUri ID="tbWebTypeSmartphoneUrl" runat="server"></D:TextBoxUri>
							</td>
                        	<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblWebTypeTabletUrl">Web-type tablet url</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxUri ID="tbWebTypeTabletUrl" runat="server"></D:TextBoxUri>
							</td>
						</tr>
                        </D:PlaceHolder>   		  
						<tr>
                            <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblViewLayoutType">View layout</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<X:ComboBoxInt ID="ddlViewLayoutType" runat="server" UseDataBinding="true" notdirty="true"></X:ComboBoxInt>
							</td>
							<td class="label">
								
							</td>
							<td class="control">
								
							</td>
						</tr>                      												
					 </table>		
                    </D:PlaceHolder>			
				</Controls>
			</X:TabPage>	
            <X:TabPage Text="Productsamenstellingen" Name="tabAlterations">
                    <controls>					
                        <table class="dataformV2">
						<tr>
							<td class="label">
								<D:Label runat="server" id="lblProductAlteration1">Keuze 1</D:Label>
							</td>
							<td class="control">
								<X:ComboBoxInt runat="server" ID="ddlProductAlteration1" IncrementalFilteringMode="StartsWith" TextField="Name" ValueField="GenericalterationId" ></X:ComboBoxInt>
							</td>
							<td class="label">
                                <D:HyperLink runat="server" LocalizeText="false" ID="hlProductAlteration1"></D:HyperLink>
							</td>
							<td class="control">

							</td>
						</tr>
						<tr>
							<td class="label">
								<D:Label runat="server" id="lblProductAlteration2">Keuze 2</D:Label>
							</td>
							<td class="control">
								<X:ComboBoxInt runat="server" ID="ddlProductAlteration2" IncrementalFilteringMode="StartsWith" TextField="Name" ValueField="GenericalterationId" ></X:ComboBoxInt>
							</td>
							<td class="label">
                                <D:HyperLink runat="server" LocalizeText="false" ID="hlProductAlteration2"></D:HyperLink>
							</td>
							<td class="control">

							</td>
						</tr>
						<tr>
							<td class="label">
								<D:Label runat="server" id="lblProductAlteration3">Keuze 3</D:Label>
							</td>
							<td class="control">
								<X:ComboBoxInt runat="server" ID="ddlProductAlteration3" IncrementalFilteringMode="StartsWith" TextField="Name" ValueField="GenericalterationId" ></X:ComboBoxInt>
							</td>
							<td class="label">
                                <D:HyperLink runat="server" LocalizeText="false" ID="hlProductAlteration3"></D:HyperLink>
							</td>
							<td class="control">

							</td>
						</tr>
						<tr>
							<td class="label">
								<D:Label runat="server" id="lblProductAlteration4">Keuze 4</D:Label>
							</td>
							<td class="control">
								<X:ComboBoxInt runat="server" ID="ddlProductAlteration4" IncrementalFilteringMode="StartsWith" TextField="Name" ValueField="GenericalterationId" ></X:ComboBoxInt>
							</td>
							<td class="label">
                                <D:HyperLink runat="server" LocalizeText="false" ID="hlProductAlteration4"></D:HyperLink>
							</td>
							<td class="control">

							</td>
						</tr>
						<tr>
							<td class="label">
								<D:Label runat="server" id="lblProductAlteration5">Keuze 5</D:Label>
							</td>
							<td class="control">
								<X:ComboBoxInt runat="server" ID="ddlProductAlteration5" IncrementalFilteringMode="StartsWith" TextField="Name" ValueField="GenericalterationId" ></X:ComboBoxInt>
							</td>
							<td class="label">
                                <D:HyperLink runat="server" LocalizeText="false" ID="hlProductAlteration5"></D:HyperLink>
							</td>
							<td class="control">

							</td>
						</tr>
						<tr>
							<td class="label">
								<D:Label runat="server" id="lblProductAlteration6">Keuze 6</D:Label>
							</td>
							<td class="control">
								<X:ComboBoxInt runat="server" ID="ddlProductAlteration6" IncrementalFilteringMode="StartsWith" TextField="Name" ValueField="GenericalterationId" ></X:ComboBoxInt>
							</td>
							<td class="label">
                                <D:HyperLink runat="server" LocalizeText="false" ID="hlProductAlteration6"></D:HyperLink>
							</td>
							<td class="control">

							</td>
						</tr>
						<tr>
							<td class="label">
								<D:Label runat="server" id="lblProductAlteration7">Keuze 7</D:Label>
							</td>
							<td class="control">
								<X:ComboBoxInt runat="server" ID="ddlProductAlteration7" IncrementalFilteringMode="StartsWith" TextField="Name" ValueField="GenericalterationId" ></X:ComboBoxInt>
							</td>
							<td class="label">
                                <D:HyperLink runat="server" LocalizeText="false" ID="hlProductAlteration7"></D:HyperLink>
							</td>
							<td class="control">

							</td>
						</tr>
						<tr>
							<td class="label">
								<D:Label runat="server" id="lblProductAlteration8">Keuze 8</D:Label>
							</td>
							<td class="control">
								<X:ComboBoxInt runat="server" ID="ddlProductAlteration8" IncrementalFilteringMode="StartsWith" TextField="Name" ValueField="GenericalterationId" ></X:ComboBoxInt>
							</td>
							<td class="label">
                                <D:HyperLink runat="server" LocalizeText="false" ID="hlProductAlteration8"></D:HyperLink>
							</td>
							<td class="control">

							</td>
						</tr>
						<tr>
							<td class="label">
								<D:Label runat="server" id="lblProductAlteration9">Keuze 9</D:Label>
							</td>
							<td class="control">
								<X:ComboBoxInt runat="server" ID="ddlProductAlteration9" IncrementalFilteringMode="StartsWith" TextField="Name" ValueField="GenericalterationId" ></X:ComboBoxInt>
							</td>
							<td class="label">
                                <D:HyperLink runat="server" LocalizeText="false" ID="hlProductAlteration9"></D:HyperLink>
							</td>
							<td class="control">

							</td>
						</tr>
						<tr>
							<td class="label">
								<D:Label runat="server" id="lblProductAlteration10">Keuze 10</D:Label>
							</td>
							<td class="control">
								<X:ComboBoxInt runat="server" ID="ddlProductAlteration10" IncrementalFilteringMode="StartsWith" TextField="Name" ValueField="GenericalterationId" ></X:ComboBoxInt>
							</td>
							<td class="label">
                                <D:HyperLink runat="server" LocalizeText="false" ID="hlProductAlteration10"></D:HyperLink>
							</td>
							<td class="control">

							</td>
						</tr>
					</table>
				</controls>
            </X:TabPage>
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>

