﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dionysos.Data;
using Dionysos.Interfaces;
using Dionysos;
using Dionysos.Web;
using Dionysos.Web.Google;
using Obymobi.Logic.Analytics;
using Obymobi.Web.Google;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data;
using System.Text;
using System.IO;
using Obymobi.Logic.HelperClasses;
using System.Drawing;
using Dionysos.Drawing;
using Dionysos.Data.LLBLGen;
using System.Diagnostics;
using Obymobi.Logic.Cms;
using System.Threading.Tasks;
using SpreadsheetLight;
using System.Net;
using Amazon.IdentityManagement.Model;
using Dionysos.Web.UI;
using Obymobi.Web.CloudStorage;
using Role = Obymobi.Enums.Role;
using Obymobi.Enums;

namespace Obymobi.ObymobiCms
{
    public partial class _Default : Dionysos.Web.UI.PageDefault
    {
        protected override void OnInit(EventArgs e)
        {
            // Get the 10 recent companies for this user from his cookie if he got one
            this.RenderRecentCompanies();

            this.plhDatabaseClearingRequired.Visible = false;
            this.plhAmazonUsersToBeRemoved.Visible = false;
            if ((CmsSessionHelper.CurrentRole == Role.GodMode) &&
                (!WebEnvironmentHelper.CloudEnvironmentIsProduction || TestUtil.IsPcDeveloper))
            {
                if (!Dionysos.ConfigurationManager.GetBool(ObymobiDataConfigConstants.DatabaseClearedForNonLiveUsage))
                {
                    this.plhDatabaseClearingRequired.Visible = true;
                    this.btClearDatabase.Click += this.btClearDatabase_Click;
                }

                if (!Dionysos.ConfigurationManager.GetBool(ObymobiDataConfigConstants.AmazonUsersRemovedForNonLiveUsage))
                {
                    this.plhAmazonUsersToBeRemoved.Visible = true;
                    this.btRecheckAmazonUsers.Click += this.btRecheckAmazonUsers_Click;
                }
            }

            base.OnInit(e);
        }

        void btRecheckAmazonUsers_Click(object sender, EventArgs e)
        {
            List<User> users = AmazonCloudStorageHelper.GetAmazonUsersToBeRemoved().OrderBy(x => x.UserName).ToList();

            if (users.Count <= 0)
            {
                Dionysos.ConfigurationManager.SetValue(ObymobiDataConfigConstants.AmazonUsersRemovedForNonLiveUsage, true);
                this.plhAmazonUsersToBeRemoved.Visible = false;
            }
            else
            {
                this.plhAmazonUsersToBeRemoved.Visible = true;

                this.plhAmazonUsers.AddHtml("<ul>");
                foreach (User user in users)
                {
                    this.plhAmazonUsers.AddHtml("<li>{0}</li>", user.UserName);
                }
                this.plhAmazonUsers.AddHtml("</ul>");
            }
        }

        void btClearDatabase_Click(object sender, EventArgs e)
        {
            DatabaseClearer clearer = new DatabaseClearer();
            clearer.ClearDatabase();
            this.plhDatabaseClearingRequired.Visible = false;
        }

        private void GenerateVectronTestScript(int companyId, int mode, int treshold)
        {
            StringBuilder sb = new StringBuilder();

            PredicateExpression productFilter = new PredicateExpression();
            productFilter.Add(ProductFields.CompanyId == companyId);
            productFilter.Add(ProductFields.PosproductId != DBNull.Value);

            RelationCollection productRelations = new RelationCollection();
            productRelations.Add(ProductEntity.Relations.PosproductEntityUsingPosproductId);

            PosproductCollection products = new PosproductCollection();
            products.GetMulti(productFilter, productRelations);

            PredicateExpression deliverypointFilter = new PredicateExpression();
            deliverypointFilter.Add(DeliverypointFields.CompanyId == companyId);
            deliverypointFilter.Add(DeliverypointFields.PosdeliverypointId != DBNull.Value);

            RelationCollection deliverypointRelations = new RelationCollection();
            deliverypointRelations.Add(DeliverypointEntity.Relations.PosdeliverypointEntityUsingPosdeliverypointId);

            PosdeliverypointCollection deliverypoints = new PosdeliverypointCollection();
            deliverypoints.GetMulti(deliverypointFilter, deliverypointRelations);

            List<string> deliverypointList = new List<string>();
            if (mode == 1)
            {
                int random = RandomUtil.GetRandomNumber(deliverypoints.Count);
                deliverypointList.Add(deliverypoints[random].ExternalId);
            }
            else if (mode == 2 || mode == 3)
            {
                foreach (PosdeliverypointEntity dpoint in deliverypoints)
                {
                    deliverypointList.Add(dpoint.ExternalId);
                }
            }

            sb.AppendLine("dim Vectron");
            sb.AppendLine("");
            sb.AppendLine("	set Vectron = createobject(\"Flexipos.Order\")");
            sb.AppendLine("	Vectron.Show");

            string deliverypoint = "0";

            if (mode == 1 || (mode == 3 && treshold > 0))
            {
                sb.AppendLine("	Vectron.ClerkOpen 1");

                int random = RandomUtil.GetRandomNumber(deliverypoints.Count);
                deliverypoint = deliverypointList[random];

                sb.AppendLine(string.Format("	Vectron.GuestCOpen {0}", deliverypoint));
            }

            int count = 0;
            int quantity = 1;

            for (int i = 0; i < products.Count; i++)
            {
                PosproductEntity product = products[i];

                if (mode == 2)
                {
                    sb.AppendLine("	Vectron.ClerkOpen 1");

                    int random = RandomUtil.GetRandomNumber(deliverypoints.Count);
                    deliverypoint = deliverypointList[random];

                    sb.AppendLine(string.Format("	Vectron.GuestCOpen {0}", deliverypoint));
                }

                quantity = RandomUtil.GetRandomNumber(10) + 1;
                sb.AppendLine(string.Format("	Vectron.PLU {1}, {0}", product.ExternalId, quantity));

                count++;

                if (mode == 2)
                {
                    sb.AppendLine(string.Format("	Vectron.GuestCClose {0}", deliverypoint));
                    sb.AppendLine("	Vectron.ClerkClose 1");
                }

                if (mode == 3 && count == treshold)
                {
                    sb.AppendLine(string.Format("	Vectron.GuestCClose {0}", deliverypoint));
                    sb.AppendLine("	Vectron.ClerkClose 1");
                    count = 0;

                    if (i != (products.Count - 1))
                    {
                        sb.AppendLine("	Vectron.ClerkOpen 1");

                        int random = RandomUtil.GetRandomNumber(deliverypoints.Count);
                        deliverypoint = deliverypointList[random];

                        sb.AppendLine(string.Format("	Vectron.GuestCOpen {0}", deliverypoint));
                    }
                }
            }

            if (mode == 1 || count > 0)
            {
                sb.AppendLine(string.Format("	Vectron.GuestCClose {0}", deliverypoint));
                sb.AppendLine("	Vectron.ClerkClose 1");
            }

            sb.AppendLine("	Vectron.Hide");
            sb.AppendLine("	Set Vectron = Nothing");

            string filename = string.Format("FlexiPos_company-{0}_mode-{1}_{2}.vbs", companyId, mode, DateTime.Now.ToString("yyyyMMddhhmmss"));

            this.Response.Clear();
            Response.AppendHeader("content-disposition", "attachment; filename=" + filename);
            Response.ContentType = "text/vbs";
            Response.Charset = "UTF-8";
            Response.Write(sb.ToString());
            Response.Flush();
            Response.End();
        }

        private List<string> GetFilePaths(string directory)
        {
            List<string> filePaths = Directory.GetFiles(directory).ToList();

            foreach (string subDirectory in Directory.GetDirectories(directory))
            {
                filePaths.AddRange(this.GetFilePaths(subDirectory));
            }

            return filePaths;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (QueryStringHelper.HasValue("magic"))
            {
                this.DoTheMagic8();
            }            
            else if (QueryStringHelper.HasValue("task"))
            {
                Task.Factory.StartNew(() =>
                    {
                        Thread.Sleep(5000);
                        throw new Exception("HERP A DEPR MOTHERFUKCER");
                    }, TaskCreationOptions.LongRunning);
            }
            else if (TestUtil.IsPcGabriel && QueryStringHelper.HasValue("wand"))
            {
                OrderCollection orders = new OrderCollection();

                ExcludeFieldsList includedFieldsStatusOnly = new ExcludeFieldsList();
                includedFieldsStatusOnly.ExcludeContainedFields = false;
                includedFieldsStatusOnly.Add(OrderFields.OrderId);
                includedFieldsStatusOnly.Add(OrderFields.Status);

                ExcludeFieldsList includedFieldsAllNotes = new ExcludeFieldsList();
                includedFieldsAllNotes.ExcludeContainedFields = false;
                includedFieldsAllNotes.Add(OrderFields.OrderId);
                includedFieldsAllNotes.Add(OrderFields.Notes);
                includedFieldsAllNotes.Add(OrderFields.BenchmarkInformation);
                includedFieldsAllNotes.Add(OrderFields.CompanyName);

                ExcludeFieldsList includedFieldsPkOnly = new ExcludeFieldsList();
                includedFieldsPkOnly.ExcludeContainedFields = false;
                includedFieldsPkOnly.Add(OrderFields.OrderId);

                Stopwatch swAllFields = new Stopwatch();
                Stopwatch swStatusFields = new Stopwatch();
                Stopwatch swNotesFields = new Stopwatch();
                Stopwatch swPkField = new Stopwatch();

                for (int i = 0; i < 50; i++)
                {
                    Debug.WriteLine(i);

                    orders = new OrderCollection();
                    swStatusFields.Start();
                    orders.GetMulti(null, includedFieldsStatusOnly, 5000);
                    swStatusFields.Stop();

                    orders = new OrderCollection();
                    swPkField.Start();
                    orders.GetMulti(null, includedFieldsPkOnly, 5000);
                    swPkField.Stop();

                    orders = new OrderCollection();
                    swNotesFields.Start();
                    orders.GetMulti(null, includedFieldsAllNotes, 5000);
                    swNotesFields.Stop();

                    orders = new OrderCollection();
                    swAllFields.Start();
                    orders.GetMulti(null, null, 5000);
                    swAllFields.Stop();
                }

                Debug.WriteLine(string.Format("Order.Count: {0}", orders.GetDbCount()));
                Debug.WriteLine(string.Format("swAllFields: {0}, {1}%", swAllFields.ElapsedMilliseconds, "100"));

                decimal percentageStatusFields = Decimal.Divide(swStatusFields.ElapsedMilliseconds, swAllFields.ElapsedMilliseconds) * 100;
                Debug.WriteLine(string.Format("swStatusFields: {0}, {1}%", swStatusFields.ElapsedMilliseconds, Dionysos.Math.Round(percentageStatusFields, 2)));

                decimal percentageNotesFields = Decimal.Divide(swNotesFields.ElapsedMilliseconds, swAllFields.ElapsedMilliseconds) * 100;
                Debug.WriteLine(string.Format("swNotesFields: {0}, {1}%", swNotesFields.ElapsedMilliseconds, Dionysos.Math.Round(percentageNotesFields, 2)));

                decimal percentagePkField = Decimal.Divide(swPkField.ElapsedMilliseconds, swAllFields.ElapsedMilliseconds) * 100;
                Debug.WriteLine(string.Format("swPkField: {0}, {1}%", swPkField.ElapsedMilliseconds, Dionysos.Math.Round(percentagePkField, 2)));
            }
            else if (QueryStringHelper.HasValue("redis"))
            {
                this.plhOverview.AddHtml("Redis enabled: {0}", Obymobi.Web.Caching.RedisCacheHelper.Instance.RedisEnabled);
            }

            if (QueryStringHelper.HasValue("CompanyId"))
            {
                int companyId;
                if (QueryStringHelper.GetInt("CompanyId", out companyId))
                {
                    CmsSessionHelper.CurrentCompanyId = companyId;

                    string url = ResolveUrl("~/Company/Installation.aspx");
                    WebShortcuts.ResponseRedirect(url, true);
                }
            }
        }        

        private void RenderRecentCompanies()
        {
            UserEntity userEntity = CmsSessionHelper.CurrentUser;

            if (userEntity == null)
            {
                return;
            }

            IEnumerable<int> recentlyUsedCompanyIds = CmsSessionHelper.GetRecentlyUsedCompanies(userEntity.UserId).CompanyIds;
            if (!recentlyUsedCompanyIds.Any())
            {
                return;
            }

            CompanyCollection companyCollection;
            try
            {
                companyCollection = RetrieveCompanies(userEntity, recentlyUsedCompanyIds);
            }
            catch (AuthorizationException)
            {
                Dionysos.Web.Security.UserManager.RefreshCurrentUser();
                CmsSessionHelper.ClearCompaniesFromCacheForUser(userEntity.UserId);
                userEntity = CmsSessionHelper.CurrentUser;

                companyCollection = RetrieveCompanies(userEntity, recentlyUsedCompanyIds);
            }

            if (!companyCollection.Any())
            {
                return;
            }

            foreach (int recentlyUsedCompanyId in recentlyUsedCompanyIds)
            {
                CompanyEntity companyEntity = companyCollection.FirstOrDefault(x => x.CompanyId == recentlyUsedCompanyId);
                if (companyEntity != null)
                {
                    this.plhRecentCompanies.AddHtml("<li><a href=\"{1}\" >{0}</a></li>", companyEntity.Name, ResolveUrl(string.Format("~/default.aspx?CompanyId={0}", companyEntity.CompanyId)));
                }
            }
        }

        private static CompanyCollection RetrieveCompanies(UserEntity userEntity, IEnumerable<int> companyIds)
        {
            CompanyCollection companyCollection = new CompanyCollection();

            if (userEntity.IsAdministrator)
            {
                companyCollection = EntityCollection.GetMulti<CompanyCollection>(CompanyFields.CompanyId == companyIds.ToArray());
            }
            else if (userEntity.AccountId.HasValue)
            {
                companyCollection = EntityCollection.GetMulti<CompanyCollection>(AccountCompanyFields.AccountId == userEntity.AccountId.Value, CompanyEntity.Relations.AccountCompanyEntityUsingCompanyId, null);
            }

            return companyCollection;
        }

        private void DoTheMagic8()
        {
        }

        void UndoTheMagic()
        {
            for (int i = 0; i < 5; i++)
            {
                Stopwatch sw = new Stopwatch();

                // Preftech
                AlterationitemCollection alterationitems = new AlterationitemCollection();

                sw.Start();
                PrefetchPath path = new PrefetchPath(EntityType.AlterationitemEntity);
                path.Add(AlterationitemEntity.PrefetchPathAlterationEntity);
                path.Add(AlterationitemEntity.PrefetchPathAlterationoptionEntity);
                alterationitems.GetMulti(null, path);
                sw.Stop();

                Debug.WriteLine("Prefetched: " + sw.ElapsedTicks + "ticks - " + sw.ElapsedMilliseconds + "ms - Count: " + alterationitems.Count);
                sw.Reset();

                // Non prefecthed
                sw = new Stopwatch();
                sw.Start();
                alterationitems = new AlterationitemCollection();
                alterationitems.GetMulti(null);
                sw.Stop();
                Debug.WriteLine("Denormalised: " + sw.ElapsedTicks + "ticks - " + sw.ElapsedMilliseconds + "ms - Count: " + alterationitems.Count);
            }
        }

        private void LoadCategoriesWithoutProducts()
        {
            PredicateExpression filter = new PredicateExpression(CategoryFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            PrefetchPath prefetch = new PrefetchPath((int)EntityType.CategoryEntity);
            prefetch.Add(CategoryEntity.PrefetchPathProductCategoryCollection);

            CategoryCollection categories = new CategoryCollection();
            categories.GetMulti(filter, prefetch);

            this.plhOverview.AddHtml("<strong>Categorieen zonder producten</strong>");
            this.plhOverview.AddHtml("<ul>");

            foreach (CategoryEntity category in categories)
            {
                if (category.ProductCategoryCollection.Count == 0)
                {
                    this.plhOverview.AddHtml("<li><a href=\"{1}\" >{0}</a></li>", category.Name, ResolveUrl(string.Format("~/Catalog/Category.aspx?id={0}", category.CategoryId)));
                }
            }

            this.plhOverview.AddHtml("</ul><br />");
        }

        private void LoadCategoriesWithoutButtonImage()
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(CategoryFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

            PrefetchPath prefetch = new PrefetchPath((int)EntityType.CategoryEntity);
            prefetch.Add(CategoryEntity.PrefetchPathMediaCollection);

            CategoryCollection categories = new CategoryCollection();
            categories.GetMulti(filter, prefetch);

            this.plhOverview.AddHtml("<strong>Categorieen zonder afbeelding</strong>");
            this.plhOverview.AddHtml("<ul>");

            PredicateExpression mediaFilter = new PredicateExpression(MediaFields.MediaType == 400);

            foreach (CategoryEntity category in categories)
            {
                if (category.MediaCollection.FindMatches(mediaFilter).Count == 0)
                {
                    this.plhOverview.AddHtml("<li><a href=\"{1}\" >{0}</a></li>", category.Name, ResolveUrl(string.Format("~/Catalog/Category.aspx?id={0}", category.CategoryId)));
                }
            }

            this.plhOverview.AddHtml("</ul><br />");
        }

        private void LoadProductsWithoutCategory()
        {
            PredicateExpression filter = new PredicateExpression(ProductFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            PrefetchPath prefetch = new PrefetchPath((int)EntityType.ProductEntity);
            prefetch.Add(ProductEntity.PrefetchPathProductCategoryCollection);

            ProductCollection products = new ProductCollection();
            products.GetMulti(filter, prefetch);

            this.plhOverview.AddHtml("<strong>Producten zonder categorie</strong>");
            this.plhOverview.AddHtml("<ul>");

            foreach (ProductEntity product in products)
            {
                if (product.ProductCategoryCollection.Count == 0)
                {
                    this.plhOverview.AddHtml("<li><a href=\"{1}\" >{0}</a></li>", product.Name, ResolveUrl(string.Format("~/Catalog/Product.aspx?id={0}", product.ProductId)));
                }
            }

            this.plhOverview.AddHtml("</ul><br />");
        }

        private void LoadProductsWithoutButtonImage()
        {
            PredicateExpression filter = new PredicateExpression(ProductFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            PrefetchPath prefetch = new PrefetchPath((int)EntityType.ProductEntity);
            prefetch.Add(ProductEntity.PrefetchPathProductCategoryCollection);
            prefetch.Add(ProductEntity.PrefetchPathGenericproductEntity);

            ProductCollection products = new ProductCollection();
            products.GetMulti(filter, prefetch);

            this.plhOverview.AddHtml("<strong>Producten zonder knop afbeelding</strong>");
            this.plhOverview.AddHtml("<ul>");

            PredicateExpression mediaFilter = new PredicateExpression(MediaFields.MediaType == 300);

            foreach (ProductEntity product in products)
            {
                if (product.MediaCollection.FindMatches(mediaFilter).Count == 0)
                {
                    if (!product.GenericproductId.HasValue || product.GenericproductEntity.MediaCollection.FindMatches(mediaFilter).Count == 0)
                        this.plhOverview.AddHtml("<li><a href=\"{1}\" >{0}</a></li>", product.Name, ResolveUrl(string.Format("~/Catalog/Product.aspx?id={0}", product.ProductId)));
                }
            }

            this.plhOverview.AddHtml("</ul><br />");
        }

        private void LoadProductsWithoutPhoto()
        {
            PredicateExpression filter = new PredicateExpression(ProductFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            PrefetchPath prefetch = new PrefetchPath((int)EntityType.ProductEntity);
            prefetch.Add(ProductEntity.PrefetchPathMediaCollection);

            ProductCollection products = new ProductCollection();
            products.GetMulti(filter, prefetch);

            this.plhOverview.AddHtml("<strong>Producten zonder foto</strong>");
            this.plhOverview.AddHtml("<ul>");

            PredicateExpression mediaFilter = new PredicateExpression(MediaFields.MediaType == 200);

            foreach (ProductEntity product in products)
            {
                if (product.MediaCollection.FindMatches(mediaFilter).Count == 0 && (product.GenericproductId.HasValue && product.GenericproductEntity.MediaCollection.FindMatches(mediaFilter).Count == 0))
                {
                    this.plhOverview.AddHtml("<li><a href=\"{1}\" >{0}</a></li>", product.Name, ResolveUrl(string.Format("~/Catalog/Product.aspx?id={0}", product.ProductId)));
                }
            }

            this.plhOverview.AddHtml("</ul><br />");
        }

        private void LoadProductsInWrongCategory()
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ProductFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            filter.Add(CategoryFields.ParentCategoryId == DBNull.Value);

            RelationCollection relations = new RelationCollection();
            relations.Add(CategoryEntity.Relations.ProductCategoryEntityUsingCategoryId);
            relations.Add(ProductCategoryEntity.Relations.ProductEntityUsingProductId);

            ProductCollection products = new ProductCollection();
            products.GetMulti(filter, relations);

            this.plhOverview.AddHtml("<strong>Producten in verkeerde categorie</strong>");
            this.plhOverview.AddHtml("<ul>");

            foreach (ProductEntity product in products)
            {
                this.plhOverview.AddHtml("<li><a href=\"{1}\" >{0}</a></li>", product.Name, ResolveUrl(string.Format("~/Catalog/Product.aspx?id={0}", product.ProductId)));
            }

            this.plhOverview.AddHtml("</ul><br />");
        }

        private string GenerateMacAddress()
        {
            long ticksTime = DateTime.Now.Ticks;
            string ticksString = ticksTime.ToString("X");

            string physicalAddressString = "ABCD" + ticksString.Substring(6, 8);
            string address = string.Empty;
            for (int i = 0; i < physicalAddressString.Length; i++)
            {
                address += physicalAddressString[i];

                if (i % 2 == 1 && (i != physicalAddressString.Length - 1))
                    address += ":";
            }

            return address.ToLower();
        }
    }
}
