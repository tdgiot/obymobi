﻿namespace Obymobi.Cms.WebApplication.Validation.UserPasswords
{
    public enum PasswordValidationFailureType
    {
        CurrentPasswordNotEntered,
        CurrentPasswordInvalid,
        NewPasswordNotEntered,
        NewPasswordNotReentered,
        PasswordsDontMatch,
        PasswordTooShort,
        PasswordContainsNoCapitals,
        PasswordContainsNoNumbers
    }
}