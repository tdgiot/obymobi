﻿namespace Obymobi.Cms.WebApplication.Validation.UserPasswords.Interfaces
{
    public interface IUserPasswordValidator
    {
        string ValidateNewPassword(string password, string reenteredPassword);
        string ValidateChangesToExistingPassword(string password, string reenteredPassword);
        string ValidateNewAndCurrentPassword(string currentPassword, string newPassword, string newPasswordConfirm);
    }
}
