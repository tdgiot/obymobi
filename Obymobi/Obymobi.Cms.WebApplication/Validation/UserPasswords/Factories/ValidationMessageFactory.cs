﻿using Obymobi.Cms.WebApplication.Validation.UserPasswords.Factories.Interfaces;

namespace Obymobi.Cms.WebApplication.Validation.UserPasswords.Factories
{
    public class ValidationMessageFactory : IValidationMessageFactory
    {
        public string CreateValidationMessage(PasswordValidationFailureType validationFailureType)
        {
            switch (validationFailureType)
            {
                case PasswordValidationFailureType.NewPasswordNotEntered:
                    return "Please enter a password.";
                case PasswordValidationFailureType.NewPasswordNotReentered:
                    return "Please re-enter the desired password.";
                case PasswordValidationFailureType.PasswordsDontMatch:
                    return "The passwords do not match.";
                case PasswordValidationFailureType.PasswordTooShort:
                    return "Password needs to be at least 8 characters long.";
                case PasswordValidationFailureType.PasswordContainsNoCapitals:
                    return "Your password should contain at least 1 upper case character.";
                case PasswordValidationFailureType.PasswordContainsNoNumbers:
                    return "Your password should contain at least 1 number.";
                case PasswordValidationFailureType.CurrentPasswordInvalid:
                    return "The current password is invalid.";
                case PasswordValidationFailureType.CurrentPasswordNotEntered:
                    return "Please enter your current password.";
                default:
                    return string.Empty;
            }
        }
    }
}