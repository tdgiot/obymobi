﻿namespace Obymobi.Cms.WebApplication.Validation.UserPasswords.Factories.Interfaces
{
    public interface IValidationMessageFactory
    {
        string CreateValidationMessage(PasswordValidationFailureType validationFailureType);
    }
}
