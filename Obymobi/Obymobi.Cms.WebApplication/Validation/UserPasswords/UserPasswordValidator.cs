﻿using Dionysos;
using Dionysos.Web.Security;
using Obymobi.Cms.WebApplication.Validation.UserPasswords.Factories.Interfaces;
using Obymobi.Cms.WebApplication.Validation.UserPasswords.Interfaces;
using System.Linq;

namespace Obymobi.Cms.WebApplication.Validation.UserPasswords
{
    public class UserPasswordValidator : IUserPasswordValidator
    {
        private readonly IValidationMessageFactory validationMessageFactory;

        public UserPasswordValidator(IValidationMessageFactory validationMessageFactory)
        {
            this.validationMessageFactory = validationMessageFactory;
        }

        public string ValidateNewPassword(string newPassword, string newPasswordConfirm)
        {
            if (newPassword.IsNullOrWhiteSpace())
                return validationMessageFactory.CreateValidationMessage(PasswordValidationFailureType.NewPasswordNotEntered);

            return ValidatePasswordBase(newPassword, newPasswordConfirm);
        }

        public string ValidateChangesToExistingPassword(string newPassword, string newPasswordConfirm)
        {
            return ValidatePasswordBase(newPassword, newPasswordConfirm);
        }

        public string ValidateNewAndCurrentPassword(string currentPassword, string newPassword, string newPasswordConfirm)
        {
            if (currentPassword.IsNullOrWhiteSpace())
                return validationMessageFactory.CreateValidationMessage(PasswordValidationFailureType.CurrentPasswordNotEntered);
            else if (!UserManager.Instance.AuthenticateUser(UserManager.CurrentUser, currentPassword))
                return validationMessageFactory.CreateValidationMessage(PasswordValidationFailureType.CurrentPasswordInvalid);

            return ValidatePasswordBase(newPassword, newPasswordConfirm);
        }

        private string ValidatePasswordBase(string newPassword, string newPasswordConfirm)
        {
            if (newPasswordConfirm.IsNullOrWhiteSpace())
                return validationMessageFactory.CreateValidationMessage(PasswordValidationFailureType.NewPasswordNotReentered);
            else if (newPassword != newPasswordConfirm)
                return validationMessageFactory.CreateValidationMessage(PasswordValidationFailureType.PasswordsDontMatch);
            else if (newPassword.Trim().Length < 8)
                return validationMessageFactory.CreateValidationMessage(PasswordValidationFailureType.PasswordTooShort);
            else if (!newPassword.Any(char.IsUpper))
                return validationMessageFactory.CreateValidationMessage(PasswordValidationFailureType.PasswordContainsNoCapitals);
            else if (!newPassword.Any(char.IsNumber))
                return validationMessageFactory.CreateValidationMessage(PasswordValidationFailureType.PasswordContainsNoNumbers);

            return string.Empty;
        }
    }
}