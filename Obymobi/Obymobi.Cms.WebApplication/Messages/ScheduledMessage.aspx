﻿<%@ Page Title="Scheduled Message" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Messages.ScheduledMessage" Codebehind="ScheduledMessage.aspx.cs" %>
<%@ Register Assembly="DevExpress.Web.v20.1" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
    <script type="text/javascript">
        function SelectImage(id, value) {
            hfImageId.value = id;
            tbImageName.value = value;
            imgPreview.src = "../Generic/ThumbnailGenerator.ashx?mediaId=" + id + "&width=0&height=150&fitInDimensions=false";
            popup.Hide();
        }
        function ClearImage(id, value) {
            hfImageId.value = 0;
            tbImageName.value = "";
            imgPreview.src = "";
            popup.Hide();
        }
</script>
<div>
    <dx:ASPxPopupControl ClientInstanceName="popup" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" ID="pcImage" runat="server" ShowHeader="True"
        Modal="True" CssPostfix="1" EnableAnimation="false" AutoUpdatePosition="true" ShowShadow="false"
        HeaderText="Click on an image to select it">
        <ContentCollection>
            <dx:PopupControlContentControl runat="server">
                <div style="overflow:auto;height:450px;width:530px">
                <dx:ASPxDataView ID="dvGallery" runat="server" Layout="Flow" AllowPaging="false" CssPostfix="1" ShowVerticalScrollBar="true">
                    <ItemStyle Height="170px" Width="150px" />
                    <ItemTemplate>
                        <div align="center">
                            <table>
                                <tr> 
                                    <td class="thumbnailImageCell" onclick="SelectImage(<%# Eval("MediaId") %>,'<%# Eval("Name") %>')">
                                        <D:Image ID="imgPhoto" ImageUrl='<%# Eval("ImageUrl") %>' AlternateText='<%# Eval("Name") %>' Height='150px' Width='150px' runat="server"/>
                                    </td>
                                </tr>
                            </table>
                            <asp:Label Text='<%# Eval("Name") %>' runat="server" ID="lbName" Width='150px' />
                        </div>
                    </ItemTemplate>
                </dx:ASPxDataView>
                </div>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <ContentStyle VerticalAlign="Middle" HorizontalAlign="Center" />
        <ModalBackgroundStyle BackColor="Black" />
    </dx:ASPxPopupControl>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Generic" Name="Generic">
				<Controls>
                    <D:Panel ID="pnlMessageDetails" runat="server" GroupingText="Message details">
				        <table class="dataformV2">
                            <tr>
                                <td class="label">
                                    <D:LabelTextOnly runat="server" ID="lblMessageTemplateId">Message template</D:LabelTextOnly>
                                </td>
                                <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlMessageTemplateId" ClientIDMode="Static" CssClass="input" UseDataBinding="true" EntityName="MessageTemplate" PreventEntityCollectionInitialization="true" notdirty="true" AutoPostBack="true" />
                                </td>
                                <td class="label">
                                    <D:Label runat="server" id="lblImage">Image</D:Label>
                                </td>
                                <td class="control">
                                    <D:TextBox ID="tbImageName" ClientIDMode="Static" runat="server" ReadOnly="true" Width="90%"/>
                                    <D:CheckBox runat="server" ID="cbManualMediaEnabled" AutoPostBack="True" IsRequired="False"/>
                                </td>         
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:LabelTextOnly runat="server" ID="lblTitle">Title</D:LabelTextOnly>
                                </td>
                                <td class="control">
                                    <D:TextBoxString runat="server" ID="tbTitle" IsRequired="true" Width="90%" />
                                    <D:CheckBox runat="server" ID="cbManualTitleEnabled" AutoPostBack="True" IsRequired="False"/>
                                </td>
							    <td class="label">
							    </td>
							    <td class="control" >
                                    <D:Button ID="btSelectImage" runat="server" Text="Select image..." UseSubmitBehavior="false" OnClientClick="popup.Show(); return false;" />
                                    <D:Button ID="btClearImage" runat="server" Text="Clear" UseSubmitBehavior="false" OnClientClick="ClearImage(); return false;" />
							    </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:LabelTextOnly runat="server" ID="lblFriendlyName">Friendly Name</D:LabelTextOnly>
                                </td>
                                <td class="control">
                                    <D:TextBoxString runat="server" ID="tbFriendlyName" IsRequired="true" Width="90%" />
                                    <D:CheckBox runat="server" ID="cbManualFriendlyNameEnabled" AutoPostBack="True" IsRequired="False"/>
                                </td>
                                <td class="label"></td>
                                <td class="control"></td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:LabelTextOnly runat="server" ID="lblMessage">Message</D:LabelTextOnly>
                                </td>
                                <td class="control" style="vertical-align: top">
                                    <D:TextBoxMultiLine runat="server" ID="tbMessage" ClientIDMode="Static" Rows="12" MaxLength="750" UseValidation="true" Width="90%" />
                                    <D:CheckBox runat="server" ID="cbManualMessageEnabled" AutoPostBack="True" IsRequired="False"/><br/>
                                    <small>Note: Message length can be maximum 750 characters</small>
                                </td>
                                <td class="label">
                                </td>
                                <td class="control">
                                    <dxe:ASPxImage ID="imgPreview" ClientIDMode="Static" Height='150px' runat="server"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:LabelTextOnly runat="server" ID="lblMessageLayoutType">Message layout type</D:LabelTextOnly>
                                </td>
                                <td class="control">
                                    <X:ComboBoxInt ID="ddlMessageLayoutType" runat="server" UseDataBinding="true" DisplayEmptyItem="false" IsRequired="true" style="width: 92%; float: left;margin-right:2px"></X:ComboBoxInt>
                                    <D:CheckBox runat="server" ID="cbManualMessageLayoutTypeEnabled" AutoPostBack="True" IsRequired="False"/>
                                </td>
                                <td class="label"></td>
                                <td class="control"></td>
                            </tr>
                        </table>
                    </D:Panel>
                    <D:Panel ID="pnlAction" runat="server" GroupingText="Action">
                        <table class="dataformV2">
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblCategoryId">Category</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection ID="cbCategoryId" runat="server" IncrementalFilteringMode="StartsWith" EntityName="Category" ValueField="CategoryId" TextField="FullCategoryMenuName" PreventEntityCollectionInitialization="true" UseDataBinding="true">
                                        <ClientSideEvents SelectedIndexChanged="function(s, e) {cbProductCategoryId.PerformCallback('1');}" />
                                    </X:ComboBoxLLBLGenEntityCollection>
                                </td>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblProductCategoryId">Product</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection ID="cbProductCategoryId" ClientInstanceName="cbProductCategoryId" OnCallback="cbProductCategoryId_OnCallback" runat="server" IncrementalFilteringMode="StartsWith" EntityName="ProductCategory" ValueField="ProductCategoryId" TextField="ProductCategoryMenuName" PreventEntityCollectionInitialization="true" UseDataBinding="true"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblEntertainmentId">Entertainment</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection ID="cbEntertainmentId" runat="server" IncrementalFilteringMode="StartsWith" EntityName="Entertainment" ValueField="EntertainmentId" TextField="Name" PreventEntityCollectionInitialization="true" UseDataBinding="true"/>
                                </td>                                
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblSiteId">Site</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection ID="cbSiteId" runat="server" IncrementalFilteringMode="StartsWith" EntityName="Site" ValueField="SiteId" TextField="Name" PreventEntityCollectionInitialization="true" UseDataBinding="true">
                                        <ClientSideEvents SelectedIndexChanged="function(s, e) {cbPageId.PerformCallback('1');}" />
                                    </X:ComboBoxLLBLGenEntityCollection>
                                </td>
								<td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblPageId">Page</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection ID="cbPageId" ClientInstanceName="cbPageId" OnCallback="cbPageId_OnCallback" runat="server" IncrementalFilteringMode="StartsWith" EntityName="Page" ValueField="PageId" TextField="Name" PreventEntityCollectionInitialization="true" UseDataBinding="true"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblUrl">URL</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:TextBoxString ID="tbUrl" runat="server" IsRequired="false"></D:TextBoxString>
                                </td>
                            </tr>
				        </table> 
                    </D:Panel>
                </Controls>
            </X:TabPage>
        </TabPages>
    </X:PageControl>
    <asp:HiddenField ID="hfImageId" ClientIDMode="Static" runat="server" />
</asp:Content>