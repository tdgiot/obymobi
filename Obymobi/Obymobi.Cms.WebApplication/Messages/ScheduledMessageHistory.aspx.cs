﻿using System;
using System.Data;
using System.Linq;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data;
using Obymobi.ObymobiCms.MasterPages;

namespace Obymobi.ObymobiCms.Messages
{
    public partial class ScheduledMessageHistory : Dionysos.Web.UI.PageLLBLGenEntity
    {
        private void SetGui()
        {
            this.lblMessageValue.Text = this.DataSourceAsScheduledMessageHistoryEntity.Message;
            this.lblSentValue.Text = this.DataSourceAsScheduledMessageHistoryEntity.SentInCompanyTimeZone.ToString("g");

            if (this.DataSourceAsScheduledMessageHistoryEntity.ScheduledMessageEntity.IsNew)
            {
                this.lblTitleValue.Text = this.DataSourceAsScheduledMessageHistoryEntity.Title;
            }
            else
            {
                string url = this.ResolveUrl("~/Messages/ScheduledMessage.aspx?id=" + this.DataSourceAsScheduledMessageHistoryEntity.ScheduledMessageId);
                this.lblTitleValue.Text = string.Format("<a href='{0}' target='_blank'>{1}</a>", url, this.DataSourceAsScheduledMessageHistoryEntity.Title);
            }

            if (!this.DataSourceAsScheduledMessageHistoryEntity.MessagegroupEntity.IsNew)
            {
                string url = this.ResolveUrl("~/Company/Messagegroup.aspx?id=" + this.DataSourceAsScheduledMessageHistoryEntity.MessagegroupId);
                this.lblMessagegroupValue.Text = string.Format("<a href='{0}' target='_blank'>{1}</a>", url, this.DataSourceAsScheduledMessageHistoryEntity.MessagegroupEntity.Name);
            }

            if (this.DataSourceAsScheduledMessageHistoryEntity.UIScheduleItemOccurrenceEntity.IsNew)
            {
                this.lblMarketingScheduleValue.Text = this.DataSourceAsScheduledMessageHistoryEntity.UIScheduleName;
            }
            else
            {
                string url = this.ResolveUrl("~/Company/UISchedule.aspx?id=" + this.DataSourceAsScheduledMessageHistoryEntity.UIScheduleItemOccurrenceEntity.UIScheduleItemEntity.UIScheduleId);
                this.lblMarketingScheduleValue.Text = string.Format("<a href='{0}' target='_blank'>{1}</a>", url, this.DataSourceAsScheduledMessageHistoryEntity.UIScheduleName);
            }

            this.clientGrid.DataSource = this.CreateDataTable(this.GetRecipientClients());
            this.clientGrid.DataBind();
        }

        private ClientCollection GetRecipientClients()
        {
            string[] deliverypointIds = this.DataSourceAsScheduledMessageHistoryEntity.RecipientsAsString.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            ClientCollection allClients = new ClientCollection();

            while (deliverypointIds.Any())
            {
                string[] batched = deliverypointIds.Take(2000).ToArray();

                PredicateExpression filter = new PredicateExpression();
                filter.Add(ClientFields.DeliverypointId == batched);

                PrefetchPath prefetch = new PrefetchPath(EntityType.ClientEntity);

                ClientCollection clients = new ClientCollection();
                clients.GetMulti(filter, prefetch);

                allClients.AddRange(clients);

                deliverypointIds = deliverypointIds.Skip(2000).ToArray();
            }

            return allClients;
        }

        private DataTable CreateDataTable(ClientCollection clients)
        {
            DataColumn clientIdColumn = new DataColumn("ClientId");
            clientIdColumn.Caption = "Client Id";

            DataColumn deliverypointNumberColumn = new DataColumn("DeliverypointNumber");
            deliverypointNumberColumn.Caption = "DP Num";

            DataColumn deliverypointNameColumn = new DataColumn("DeliverypointName");
            deliverypointNameColumn.Caption = "DP Name";

            DataColumn deliverypointgroupIdColumn = new DataColumn("DeliverypointgroupId");
            deliverypointgroupIdColumn.Caption = "DPG Name";

            DataColumn deliverypointgroupNameColumn = new DataColumn("DeliverypointgroupName");
            deliverypointgroupNameColumn.Caption = "DPG Name";

            DataTable table = new DataTable();
            table.Columns.Add(clientIdColumn);
            table.Columns.Add(deliverypointNumberColumn);
            table.Columns.Add(deliverypointNameColumn);
            table.Columns.Add(deliverypointgroupIdColumn);
            table.Columns.Add(deliverypointgroupNameColumn);

            foreach (ClientEntity clientEntity in clients)
            {
                DataRow row = table.NewRow();
                row["ClientId"] = clientEntity.ClientId;

                if (!clientEntity.DeliverypointEntity.IsNew)
                {
                    row["DeliverypointNumber"] = clientEntity.DeliverypointEntity.Number;
                    row["DeliverypointName"] = clientEntity.DeliverypointEntity.Name;
                    row["DeliverypointgroupId"] = clientEntity.DeliverypointEntity.DeliverypointgroupId;
                    row["DeliverypointgroupName"] = clientEntity.DeliverypointEntity.DeliverypointgroupEntity.Name;
                }

                table.Rows.Add(row);
            }

            return table;
        }

        private void HookupEvents()
        {

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            MasterPageEntity masterPage = this.Master as MasterPages.MasterPageEntity;
            if (masterPage != null)
            {
                masterPage.ToolBar.SaveButton.Visible = false;
                masterPage.ToolBar.SaveAndGoButton.Visible = false;
                masterPage.ToolBar.SaveAndNewButton.Visible = false;
                masterPage.ToolBar.DeleteButton.Visible = false;
            }
            this.HookupEvents();
        }

        protected override void OnInit(EventArgs e)
        {
            this.DataSourceLoaded += ScheduledMessageHistory_DataSourceLoaded;
            base.OnInit(e);
        }

        private void ScheduledMessageHistory_DataSourceLoaded(object sender)
        {
            this.SetGui();
        }

        #region Properties

        public ScheduledMessageHistoryEntity DataSourceAsScheduledMessageHistoryEntity
        {
            get
            {
                return this.DataSource as ScheduledMessageHistoryEntity;
            }
        }

        #endregion
    }
}