﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using DevExpress.Web;
using Dionysos;
using Obymobi.Cms.Logic.HelperClasses;
using Obymobi.Cms.Logic.Sites;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Messages
{
    public partial class ScheduledMessage : Dionysos.Web.UI.PageLLBLGenEntity
    {
        private void SetGui()
        {
            DataTable table = new DataTable();
            table.Columns.Add(new DataColumn("ImageUrl"));
            table.Columns.Add(new DataColumn("MediaUrl"));
            table.Columns.Add(new DataColumn("MediaId"));
            table.Columns.Add(new DataColumn("Name"));

            PredicateExpression mediaFilter = new PredicateExpression();
            mediaFilter.Add(MediaRatioTypeMediaFields.MediaType == (int)MediaType.Gallery);
            mediaFilter.AddWithOr(MediaRatioTypeMediaFields.MediaType == (int)MediaType.GalleryLarge);
            mediaFilter.AddWithOr(MediaRatioTypeMediaFields.MediaType == (int)MediaType.GallerySmall);

            PredicateExpression filter = new PredicateExpression();
            filter.Add(MediaFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            filter.Add(mediaFilter);

            RelationCollection relations = new RelationCollection();
            relations.Add(MediaEntityBase.Relations.MediaRatioTypeMediaEntityUsingMediaId);

            MediaCollection mediaCollection = new MediaCollection();
            mediaCollection.GetMulti(filter, relations);

            foreach (MediaEntity media in mediaCollection)
            {
                DataRow row = table.NewRow();
                row["MediaUrl"] = ResolveUrl(string.Format("~/Generic/Media.aspx?id={0}&mediaType={1}", media.MediaId, MediaType.Gallery));
                string filePathRelative = "~/Generic/ThumbnailGenerator.ashx?mediaId={0}&width={1}&height={2}&fitInDimensions=false".FormatSafe(media.MediaId, 0, 150);
                row["ImageUrl"] = filePathRelative;
                row["MediaId"] = media.MediaId;
                row["Name"] = media.Name;
                table.Rows.Add(row);
            }

            this.dvGallery.DataSource = table;
            this.dvGallery.DataBind();

            if (!this.IsPostBack && this.DataSourceAsScheduledMessageEntity.MediaId.HasValue)
            {
                this.tbImageName.Text = this.DataSourceAsScheduledMessageEntity.MediaEntity.Name;
                this.hfImageId.Value = this.DataSourceAsScheduledMessageEntity.MediaEntity.MediaId.ToString();
                this.imgPreview.ImageUrl = "~/Generic/ThumbnailGenerator.ashx?mediaId={0}&width=0&height=150&fitInDimensions=false".FormatSafe(this.DataSourceAsScheduledMessageEntity.MediaEntity.MediaId);
            }

            if (this.DataSourceAsScheduledMessageEntity.MessageTemplateId.HasValue)
            {
                this.tbFriendlyName.Enabled = this.DataSourceAsScheduledMessageEntity.ManualFriendlyNameEnabled;
                this.tbTitle.Enabled = this.DataSourceAsScheduledMessageEntity.ManualTitleEnabled;
                this.tbMessage.Enabled = this.DataSourceAsScheduledMessageEntity.ManualMessageEnabled;
                this.ddlMessageLayoutType.Enabled = this.DataSourceAsScheduledMessageEntity.ManualMessageLayoutTypeEnabled;
                
                // Media
                this.btSelectImage.Visible = this.DataSourceAsScheduledMessageEntity.ManualMediaEnabled;
                this.btClearImage.Visible = this.DataSourceAsScheduledMessageEntity.ManualMediaEnabled;
            }
            else
            {
                this.DataSourceAsScheduledMessageEntity.ManualFriendlyNameEnabled = true;
                this.DataSourceAsScheduledMessageEntity.ManualTitleEnabled = true;
                this.DataSourceAsScheduledMessageEntity.ManualMessageEnabled = true;
                this.DataSourceAsScheduledMessageEntity.ManualMediaEnabled = true;
                this.DataSourceAsScheduledMessageEntity.ManualMessageLayoutTypeEnabled = true;

                this.cbManualFriendlyNameEnabled.Enabled = false;
                this.cbManualTitleEnabled.Enabled = false;
                this.cbManualMessageEnabled.Enabled = false;
                this.cbManualMediaEnabled.Enabled = false;
                this.cbManualMessageLayoutTypeEnabled.Enabled = false;
            }

            this.DataBindMessageTemplates();
            this.DataBindCategories();
            this.DataBindProducts();
            this.DataBindSites();
            this.DataBindPages();
            this.DataBindEntertainments();

            this.ddlMessageLayoutType.DataBindEnum<MessageLayoutType>();            
        }

        private void DataBindMessageTemplates()
        {
            // Filter
            PredicateExpression messageTemplateFilter = new PredicateExpression(MessageTemplateFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

            // Get results
            MessageTemplateCollection messageTemplateCollection = new MessageTemplateCollection();
            messageTemplateCollection.GetMulti(messageTemplateFilter);

            // Bind results
            this.ddlMessageTemplateId.DataSource = messageTemplateCollection;
            this.ddlMessageTemplateId.DataBind();
        }

        private void DataBindCategories()
        {
            this.cbCategoryId.DataSource = CategoryHelper.GetCategories(CmsSessionHelper.CurrentCompanyId, true);
            this.cbCategoryId.DataBind();
        }

        private void DataBindProducts()
        {
            int categoryId = 0;
            if (this.IsPostBack)
            {
                categoryId = this.cbCategoryId.ValidId;
            }
            else if (this.DataSourceAsScheduledMessageEntity.CategoryId.HasValue)
            {
                categoryId = this.DataSourceAsScheduledMessageEntity.CategoryId.Value;
            }

            this.cbProductCategoryId.DataSource = Obymobi.Logic.HelperClasses.ProductHelper.GetProductCategories(CmsSessionHelper.CurrentCompanyId, true, categoryId);
            this.cbProductCategoryId.DataBind();
        }

        private void DataBindSites()
        {
            this.cbSiteId.DataSource = SiteHelper.GetSites(CmsSessionHelper.CurrentCompanyId, true);
            this.cbSiteId.DataBind();
        }

        private void DataBindPages()
        {
            int siteId = 0;
            if (this.IsPostBack)
            {
                siteId = this.cbSiteId.ValidId;
            }
            else if (this.DataSourceAsScheduledMessageEntity.SiteId.HasValue)
            {
                siteId = this.DataSourceAsScheduledMessageEntity.SiteId.Value;
            }

            this.cbPageId.DataSource = PageHelper.GetPages(siteId, true);
            this.cbPageId.DataBind();
        }

        private void DataBindEntertainments()
        {
            this.cbEntertainmentId.DataSource = EntertainmentHelper.GetEntertainments(CmsSessionHelper.CurrentCompanyId, true, true);
            this.cbEntertainmentId.DataBind();
        }

        private void HookupEvents()
        {
            this.ddlMessageTemplateId.SelectedIndexChanged += ddlMessageTemplateId_SelectedIndexChanged;
            
            this.cbManualTitleEnabled.CheckedChanged += cbManualTitleEnabled_CheckedChanged;
            this.cbManualFriendlyNameEnabled.CheckedChanged += cbManualFriendlyNameEnabled_CheckedChanged;
            this.cbManualMessageEnabled.CheckedChanged += cbManualMessageEnabled_CheckedChanged;
            this.cbManualMediaEnabled.CheckedChanged += cbManualMediaEnabled_CheckedChanged;
            this.cbManualMessageLayoutTypeEnabled.CheckedChanged += cbManualMessageLayoutTypeEnabled_CheckedChanged;
        }

        void cbManualMessageEnabled_CheckedChanged(object sender, EventArgs e)
        {
            this.tbMessage.Enabled = ((CheckBox)sender).Checked;
        }

        void cbManualFriendlyNameEnabled_CheckedChanged(object sender, EventArgs e)
        {
            this.tbFriendlyName.Enabled = ((CheckBox)sender).Checked;
        }

        void cbManualMediaEnabled_CheckedChanged(object sender, EventArgs e)
        {
            this.btSelectImage.Visible = ((CheckBox)sender).Checked;
            this.btClearImage.Visible = ((CheckBox)sender).Checked;
        }

        void cbManualTitleEnabled_CheckedChanged(object sender, EventArgs e)
        {
            this.tbTitle.Enabled = ((CheckBox)sender).Checked;
        }

        void cbManualMessageLayoutTypeEnabled_CheckedChanged(object sender, EventArgs e)
        {
            this.ddlMessageLayoutType.Enabled = ((CheckBox)sender).Checked;
        }

        public override void Validate()
        {
            base.Validate();
            if (this.IsValid)
            {
                bool hasError = false;

                if (this.GetSelectedActions() > 1)
                {
                    this.MultiValidatorDefault.AddError("Multiple actions have been selected, please select only one action.");
                    hasError = true;
                }

                if (this.tbMessage.Text.IsNullOrWhiteSpace() && this.ddlMessageLayoutType.Value != (int)MessageLayoutType.AutoSize)
                {
                    this.MultiValidatorDefault.AddError("When no message has been specified, Message layout type needs to be 'Auto size'.");
                    hasError = true;
                }

                if (hasError)
                    base.Validate();
            }
        }

        private int GetSelectedActions()
        {
            int selectedActions = 0;

            if (this.cbCategoryId.ValidId > 0 || this.cbProductCategoryId.ValidId > 0)
                selectedActions++;
            if (this.cbEntertainmentId.ValidId > 0)
                selectedActions++;
            if (this.cbSiteId.ValidId > 0 || this.cbPageId.ValidId > 0)
                selectedActions++;
            if (!string.IsNullOrWhiteSpace(this.tbUrl.Value))
                selectedActions++;

            return selectedActions;
        }

        protected void cbProductCategoryId_OnCallback(object sender, CallbackEventArgsBase e)
        {
            this.DataBindProducts();
        }

        protected void cbPageId_OnCallback(object sender, CallbackEventArgsBase e)
        {
            this.DataBindPages();
        }

        private void ddlMessageTemplateId_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.cbManualFriendlyNameEnabled.Enabled = false;
            this.cbManualFriendlyNameEnabled.Checked = true;

            this.cbManualTitleEnabled.Enabled = false;
            this.cbManualTitleEnabled.Checked = true;

            this.cbManualMessageEnabled.Enabled = false;
            this.cbManualMessageEnabled.Checked = true;

            this.cbManualMediaEnabled.Enabled = false;
            this.cbManualMediaEnabled.Checked = true;

            this.cbManualMessageLayoutTypeEnabled.Enabled = false;
            this.cbManualMessageLayoutTypeEnabled.Checked = true;

            if (this.ddlMessageTemplateId.ValidId > 0)
            {
                MessageTemplateEntity messageTemplate = new MessageTemplateEntity(this.ddlMessageTemplateId.ValidId);
                if (!messageTemplate.IsNew)
                {
                    this.tbTitle.Text = messageTemplate.Title;
                    this.tbMessage.Text = messageTemplate.Message;
                    this.tbFriendlyName.Text = messageTemplate.Name;
                    if (messageTemplate.MediaId.HasValue)
                    {
                        this.hfImageId.Value = messageTemplate.MediaEntity.MediaId.ToString();
                        this.tbImageName.Text = messageTemplate.MediaEntity.Name;

                        this.cbManualMediaEnabled.Enabled = true;
                        this.cbManualMediaEnabled.Checked = false;
                    }

                    // Enable checkboxes and uncheck them
                    this.cbManualFriendlyNameEnabled.Enabled = true;
                    this.cbManualFriendlyNameEnabled.Checked = false;

                    this.cbManualTitleEnabled.Enabled = true;
                    this.cbManualTitleEnabled.Checked = false;

                    this.cbManualMessageEnabled.Enabled = true;
                    this.cbManualMessageEnabled.Checked = false;

                    this.cbManualMessageLayoutTypeEnabled.Enabled = true;
                    this.cbManualMessageLayoutTypeEnabled.Checked = false;

                    this.ddlMessageLayoutType.SelectedItem = this.ddlMessageLayoutType.Items.FindByValue((int)messageTemplate.MessageLayoutType);
                    this.ddlMessageLayoutType.Enabled = false;

                    this.cbCategoryId.SelectedIndex = 0;
                    this.cbProductCategoryId.SelectedIndex = 0;
                    this.cbEntertainmentId.SelectedIndex = 0;
                    this.cbSiteId.SelectedIndex = 0;
                    this.cbPageId.SelectedIndex = 0;
                    this.tbUrl.Text = string.Empty;

                    if (messageTemplate.CategoryId.HasValue)
                    {
                        this.cbCategoryId.SelectedItem = this.cbCategoryId.Items.FindByValue(messageTemplate.CategoryId.Value);
                    }

                    if (messageTemplate.ProductCategoryId.HasValue)
                    {
                        this.cbProductCategoryId.SelectedItem = this.cbProductCategoryId.Items.FindByValue(messageTemplate.ProductCategoryId.Value);
                    }

                    if (messageTemplate.EntertainmentId.HasValue)
                    {
                        this.cbEntertainmentId.SelectedItem = this.cbEntertainmentId.Items.FindByValue(messageTemplate.EntertainmentId.Value);
                    }
                }
            }

            this.ToggleFields();
        }

        private void ToggleFields()
        {
            this.tbTitle.Enabled = (!this.cbManualTitleEnabled.Enabled || (this.cbManualTitleEnabled.Enabled && this.cbManualTitleEnabled.Checked));
            this.tbFriendlyName.Enabled = (!this.cbManualFriendlyNameEnabled.Enabled || (this.cbManualFriendlyNameEnabled.Enabled && this.cbManualFriendlyNameEnabled.Checked));
            this.tbMessage.Enabled = (!this.cbManualMessageEnabled.Enabled || (this.cbManualMessageEnabled.Enabled && this.cbManualMessageEnabled.Checked));
            this.ddlMessageLayoutType.Enabled = (!this.cbManualMessageLayoutTypeEnabled.Enabled || (this.cbManualMessageLayoutTypeEnabled.Enabled && this.cbManualMessageLayoutTypeEnabled.Checked));

            this.btClearImage.Visible = (!this.cbManualMediaEnabled.Enabled || (this.cbManualMediaEnabled.Enabled && this.cbManualMediaEnabled.Checked));
            this.btSelectImage.Visible = (!this.cbManualMediaEnabled.Enabled || (this.cbManualMediaEnabled.Enabled && this.cbManualMediaEnabled.Checked));
        }

        public override bool Save()
        {
            if (this.PageMode == Dionysos.Web.PageMode.Add)
            {
                this.DataSourceAsScheduledMessageEntity.CompanyId = CmsSessionHelper.CurrentCompanyId;
            }

            int mediaId = 0;
            if (!string.IsNullOrWhiteSpace(this.hfImageId.Value))
                int.TryParse(this.hfImageId.Value, out mediaId);

            if (mediaId > 0)
            {
                this.DataSourceAsScheduledMessageEntity.MediaId = mediaId;
            }
            else
            {
                this.DataSourceAsScheduledMessageEntity.MediaId = null;
            }
            
            return base.Save();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.HookupEvents();            
        }

        protected override void OnInit(EventArgs e)
        {
            this.DataSourceLoaded += ScheduledMessage_DataSourceLoaded;
            base.OnInit(e);
        }

        void ScheduledMessage_DataSourceLoaded(object sender)
        {
            if (this.DataSourceAsScheduledMessageEntity.IsNew)
                this.DataSourceAsScheduledMessageEntity.MessageLayoutType = MessageLayoutType.Medium;

            this.SetGui();
        }

        #region Properties

        /// <summary>
        /// Return the page's datasource as a ScheduledMessageEntity
        /// </summary>
        public ScheduledMessageEntity DataSourceAsScheduledMessageEntity
        {
            get
            {
                return this.DataSource as ScheduledMessageEntity;
            }
        }

        #endregion
    }
}