﻿using Dionysos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dionysos.Web.UI;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using DevExpress.Web;
using System.Data;
using Obymobi.Enums;
using Obymobi.Data;
using Obymobi.Web.Azure.ServiceBus.Notifications;
using System.Text;
using Obymobi.Logic.HelperClasses;
using System.Threading.Tasks;

namespace Obymobi.ObymobiCms.Messages
{
    public partial class SendMessage : PageDefault
    {
        private void SetGui()
        {
            this.RenderMedia();
            this.RenderApps();            
        }

        private void RenderApps()
        {
            CompanyEntity companyEntity = new CompanyEntity();
            companyEntity.FetchUsingPK(CmsSessionHelper.CurrentCompanyId, null, null, new IncludeFieldsList(CompanyFields.Name));

            if (!companyEntity.IsNew)
            {
                string hubPathPrefix = companyEntity.Name.ToLowerInvariant().Replace(" ", "");
                if (companyEntity.CompanyId == 528) // Siam Kempinski Bangkok
                {
                    hubPathPrefix = hubPathPrefix.Replace("bangkok", "");
                }

                FieldLikePredicate filter = new FieldLikePredicate(AzureNotificationHubFields.HubPath, string.Format("{0}%", hubPathPrefix));

                AzureNotificationHubCollection hubs = new AzureNotificationHubCollection();
                hubs.GetMulti(filter);

                DataTable table = new DataTable();
                table.Columns.Add(new DataColumn("Id"));
                table.Columns.Add(new DataColumn("Name"));

                DataRow row = table.NewRow();
                row["Id"] = "-1";
                row["Name"] = "All apps";
                table.Rows.Add(row);

                foreach (AzureNotificationHubEntity entity in hubs)
                {
                    string appName = entity.HubPath.Split('-')[0];
                    appName = appName.Insert(hubPathPrefix.Length, " ");

                    row = table.NewRow();
                    row["Id"] = entity.NotificationHubId;
                    row["Name"] = appName.TurnFirstToUpper();
                    table.Rows.Add(row);
                }

                this.cbApp.DataSource = table;
                this.cbApp.DataBind();
            }
        }

        private void RenderMedia()
        {
            RelationCollection relations = new RelationCollection();
            relations.Add(MediaRatioTypeMediaEntity.Relations.MediaEntityUsingMediaId);

            PredicateExpression mediaFilter = new PredicateExpression();
            mediaFilter.Add(MediaRatioTypeMediaFields.MediaType == MediaType.Gallery);
            mediaFilter.AddWithOr(MediaRatioTypeMediaFields.MediaType == MediaType.GalleryLarge);
            mediaFilter.AddWithOr(MediaRatioTypeMediaFields.MediaType == MediaType.GallerySmall);

            PredicateExpression filter = new PredicateExpression();
            filter.Add(MediaFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            filter.Add(mediaFilter);

            PrefetchPath prefetch = new PrefetchPath(EntityType.MediaRatioTypeMediaEntity);
            prefetch.Add(MediaRatioTypeMediaEntity.PrefetchPathMediaEntity);
            prefetch.Add(MediaRatioTypeMediaEntity.PrefetchPathMediaRatioTypeMediaFileEntity);

            MediaRatioTypeMediaCollection mediaRatioTypeMediaCollection = new MediaRatioTypeMediaCollection();
            mediaRatioTypeMediaCollection.GetMulti(filter, 0, null, relations, prefetch);

            DataTable table = new DataTable();
            table.Columns.Add(new DataColumn("ImageUrl"));
            table.Columns.Add(new DataColumn("MediaUrl"));
            table.Columns.Add(new DataColumn("MediaId"));
            table.Columns.Add(new DataColumn("Name"));

            List<int> uniqueMediaIds = new List<int>();
            foreach (MediaRatioTypeMediaEntity mediaRatioTypeMedia in mediaRatioTypeMediaCollection)
            {
                if (!uniqueMediaIds.Contains(mediaRatioTypeMedia.MediaId))
                {
                    uniqueMediaIds.Add(mediaRatioTypeMedia.MediaId);

                    DataRow row = table.NewRow();
                    row["MediaUrl"] = ResolveUrl(string.Format("~/Generic/Media.aspx?id={0}&mediaType={1}", mediaRatioTypeMedia.MediaEntity.MediaId, mediaRatioTypeMedia.MediaType));
                    string filePathRelative = "~/Generic/ThumbnailGenerator.ashx?mediaId={0}&width={1}&height={2}&fitInDimensions=false".FormatSafe(mediaRatioTypeMedia.MediaId, 0, 150);
                    row["ImageUrl"] = filePathRelative;
                    row["MediaId"] = mediaRatioTypeMedia.MediaEntity.MediaId;
                    row["Name"] = mediaRatioTypeMedia.MediaEntity.Name;
                    table.Rows.Add(row);
                }
            }

            this.dvGallery.DataSource = table;
            this.dvGallery.DataBind();
        }

        private void SetFilters()
        {
            this.SetMessageTemplateFilter();

            this.SetProductsAndCategories(); // TODO Optimize
            this.SetSiteFilter();
            this.SetPageFilter();
        }

        private void SetMessageTemplateFilter()
        {
            // Filter
            PredicateExpression messageTemplateFilter = new PredicateExpression(MessageTemplateFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

            // Get results
            MessageTemplateCollection messageTemplateCollection = new MessageTemplateCollection();
            messageTemplateCollection.GetMulti(messageTemplateFilter);

            // Bind results
            this.ddlMessageTemplateId.DataSource = messageTemplateCollection;
            this.ddlMessageTemplateId.DataBind();
        }

        private void SetProductsAndCategories()
        {
            CompanyEntity companyEntity = new CompanyEntity(CmsSessionHelper.CurrentCompanyId);
            
            List<DeliverypointgroupEntity> deliverypointgroups = companyEntity.DeliverypointgroupCollection.Where(x => x.AvailableOnObymobi).ToList();
            if (deliverypointgroups.Count > 0)
            {
                PredicateExpression filter = new PredicateExpression();
                foreach(DeliverypointgroupEntity deliverypointgroup in deliverypointgroups)
                {
                    if(deliverypointgroup.MenuId.HasValue)
                    {
                        filter.AddWithOr(CategoryFields.MenuId == deliverypointgroup.MenuId.Value);
                    }
                }

                PrefetchPath path = new PrefetchPath(EntityType.CategoryEntity);
                path.Add(CategoryEntity.PrefetchPathProductCategoryCollection);

                CategoryCollection categoryCollection = new CategoryCollection();
                categoryCollection.GetMulti(filter, path);

                ProductCollection productCollection = new ProductCollection();
                foreach (CategoryEntity category in categoryCollection)
                {
                    foreach (ProductCategoryEntity productCategory in category.ProductCategoryCollection)
                    {
                        if(productCollection.FirstOrDefault(p => p.ProductId == productCategory.ProductId) == null)
                        {
                            productCollection.Add(productCategory.ProductEntity);
                        }
                    }
                }

                //TODO should probably be changed to ProductCategory some day

                // Bind the data
                this.ddlActionProductId.DataSource = productCollection;
                this.ddlActionProductId.DataBind();
                this.ddlActionCategoryId.DataSource = categoryCollection;
                this.ddlActionCategoryId.DataBind();
            }
        }

        private void SetSiteFilter()
        {
            // Filter
            PredicateExpression filter = new PredicateExpression();
            filter.Add(UIModeFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

            // Relations
            RelationCollection relations = new RelationCollection();
            relations.Add(SiteEntity.Relations.UITabEntityUsingSiteId);
            relations.Add(UITabEntity.Relations.UIModeEntityUsingUIModeId);

            // Sort
            SortExpression sort = new SortExpression(new SortClause(SiteFields.Name, SortOperator.Ascending));

            // Get results
            SiteCollection sites = new SiteCollection();
            sites.GetMulti(filter, 0, sort, relations);

            // Bind results
            this.ddlActionSiteId.DataSource = sites;
            this.ddlActionSiteId.DataBind();
        }

        private void SetPageFilter()
        {
            if (this.ddlActionSiteId.ValidId > 0)
            {
                var pages = this.LoadPages(this.ddlActionSiteId.ValidId);
                this.ddlActionPageId.DataSource = pages;
                this.ddlActionPageId.DataBind();
            }
        }

        private ProductCollection LoadProducts(int categoryId)
        {
            PredicateExpression filter = new PredicateExpression(ProductCategoryFields.CategoryId == categoryId);
            SortExpression sort = new SortExpression(new SortClause(ProductFields.SortOrder, SortOperator.Ascending));
            RelationCollection relations = new RelationCollection();
            relations.Add(ProductEntity.Relations.ProductCategoryEntityUsingProductId);

            // Get results
            ProductCollection products = new ProductCollection();
            products.GetMulti(filter, 0, sort, relations);

            // Return results
            return products;
        }

        private PageCollection LoadPages(int siteId)
        {
            // Filter
            PredicateExpression filter = new PredicateExpression(PageFields.SiteId == siteId);

            // Sort
            SortExpression sort = new SortExpression(new SortClause(PageFields.Name, SortOperator.Ascending));

            // Get results
            PageCollection pages = new PageCollection();
            pages.GetMulti(filter, 0, sort);

            // Return results
            return pages;
        }

        private void HookupEvents()
        {
            this.ddlMessageTemplateId.SelectedIndexChanged += ddlMessageTemplateId_SelectedIndexChanged;
        }

        public void Send()
        {
            this.Validate();
            if (this.IsValid)
            {
                string title = this.tbTitle.Text;
                string message = this.tbMessage.Text;

                int mediaId = -1;
                if (!string.IsNullOrWhiteSpace(this.tbImageId.Text))
                    int.TryParse(this.tbImageId.Text, out mediaId);

                int productId = this.ddlActionProductId.ValidId;
                int categoryId = this.ddlActionCategoryId.ValidId;
                int siteId = this.ddlActionSiteId.ValidId;
                int pageId = this.ddlActionPageId.ValidId;
                int companyId = CmsSessionHelper.CurrentCompanyId;

                var mediaCdnFilename = string.Empty;
                if (mediaId > 0)
                {
                    var mediaEntity = new MediaEntity(mediaId);
                    if (!mediaEntity.IsNew)
                    {
                        foreach (var mrt in mediaEntity.MediaRatioTypeMediaCollection)
                        {
                            if (mrt.MediaTypeAsEnum == MediaType.Gallery || mrt.MediaTypeAsEnum == MediaType.NotificationIconNormal || mrt.MediaTypeAsEnum == MediaType.GalleryLarge || mrt.MediaTypeAsEnum == MediaType.GallerySmall)
                            {
                                mediaCdnFilename = MediaHelper.GetMediaRatioTypeMediaPath(mrt);
                                if (mrt.MediaTypeAsEnum == MediaType.NotificationIconNormal)
                                    break;
                            }
                        }
                    }
                }

                // Get all access codes that contain this terminal's company
                PredicateExpression filter = new PredicateExpression();
                filter.Add(new FieldCompareSetPredicate(AccessCodeFields.AccessCodeId, AccessCodeCompanyFields.AccessCodeId, SetOperator.In, AccessCodeCompanyFields.CompanyId == companyId));

                AccessCodeCollection accessCodes = new AccessCodeCollection();
                accessCodes.GetMulti(filter);

                AzureNotificationHubCollection azureNotificationHubCollection = this.GetSelectedNotificationHubs();

                new TaskFactory().StartNew(() => { this.Send(title, message, mediaCdnFilename, productId, categoryId, siteId, pageId, companyId, accessCodes, azureNotificationHubCollection); }, TaskCreationOptions.LongRunning);

                this.AddInformatorInfo("Your message has been sent");
            }
        }

        private AzureNotificationHubCollection GetSelectedNotificationHubs()
        {
            AzureNotificationHubCollection azureNotificationHubCollection = new AzureNotificationHubCollection();

            if (this.cbApp.ValidId <= 0)
            {
                azureNotificationHubCollection.GetMulti(null);
            }
            else
            {
                azureNotificationHubCollection.GetMulti(AzureNotificationHubFields.NotificationHubId == this.cbApp.ValidId);
            }

            return azureNotificationHubCollection;
        }

        private void Send(string title, string message, string mediaCdnFilename, int productId, int categoryId, int siteId, int pageId, int companyId, AccessCodeCollection accessCodes, AzureNotificationHubCollection azureNotificationHubCollection)
        {
            // Only continue if this terminal's company is in one or more terminals
            if (accessCodes.Count > 0)
            {
                // Build a tag expression
                StringBuilder tagBuilder = new StringBuilder();
                foreach (AccessCodeEntity accessCode in accessCodes)
                {
                    if (tagBuilder.Length > 0)
                    {
                        tagBuilder.Append(" || ");
                    }
                    tagBuilder.Append(accessCode.Code);
                }

                var container = new AzureNotificationProvider.AzureNotificationContainer(tagBuilder.ToString(), title, message)
                                    {
                                        {"mediaOnCdn", mediaCdnFilename},
                                        {"duration", 0},
                                        {"urgent", false},
                                        {"companyId", companyId},
                                        {"productId", productId},
                                        {"categoryId", categoryId},
                                        {"siteId", siteId},
                                        {"pageId", pageId}
                                    };
                AzureNotificationProvider.SendNotification(container, azureNotificationHubCollection);
            }
        }

        public override void Validate()
        {
            base.Validate();
            if (this.IsValid)
            {
                int selectedActions = 0;

                if (this.ddlActionProductId.ValidId > 0)
                    selectedActions++;
                if (this.ddlActionCategoryId.ValidId > 0)
                    selectedActions++;
                if (this.ddlActionSiteId.ValidId > 0)
                    selectedActions++;
                if (this.ddlActionPageId.ValidId > 0)
                    selectedActions++;

                if (selectedActions == 2)
                {
                    if (this.ddlActionSiteId.ValidId > 0 && this.ddlActionPageId.ValidId > 0)
                    {
                        // OK
                    }
                    else if (this.ddlActionCategoryId.ValidId > 0 && this.ddlActionProductId.ValidId > 0)
                    {
                        // OK
                    }
                    else
                    {
                        // Error
                        this.MultiValidatorDefault.AddError("Multiple actions have been selected, please select only one action.");
                        base.Validate();
                    }
                }
                else if (selectedActions > 2)
                {
                    // Error
                    this.MultiValidatorDefault.AddError("Multiple actions have been selected, please select only one action.");
                    base.Validate();
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.lblCompanyName.Text = new CompanyEntity(CmsSessionHelper.CurrentCompanyId).Name;

            if (!this.IsPostBack)
                this.SetFilters();

            this.HookupEvents();
            this.SetGui();
        }


        protected void ddlActionProductId_OnCallback(object sender, CallbackEventArgsBase e)
        {
            if (this.ddlActionCategoryId.ValidId > 0)
            {
                var pages = this.LoadProducts(this.ddlActionCategoryId.ValidId);
                this.ddlActionProductId.DataSource = pages;
                this.ddlActionProductId.DataBind();
            }
        }

        protected void ddlActionPageId_OnCallback(object sender, CallbackEventArgsBase e)
        {
            if (this.ddlActionSiteId.ValidId > 0)
            {
                var pages = this.LoadPages(this.ddlActionSiteId.ValidId);
                this.ddlActionPageId.DataSource = pages;
                this.ddlActionPageId.DataBind();
            }
        }

        private void ddlMessageTemplateId_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.ddlMessageTemplateId.ValidId > 0)
            {
                MessageTemplateEntity messageTemplate = new MessageTemplateEntity(this.ddlMessageTemplateId.ValidId);
                if (!messageTemplate.IsNew)
                {
                    this.tbTitle.Text = messageTemplate.Title;
                    this.tbMessage.Text = messageTemplate.Message;
                    if(messageTemplate.MediaId.HasValue)
                    {
                        this.tbImageId.Text = messageTemplate.MediaEntity.MediaId.ToString();
                        this.tbImageName.Text = messageTemplate.MediaEntity.Name;
                    }
                }
            }
        }

    }
}