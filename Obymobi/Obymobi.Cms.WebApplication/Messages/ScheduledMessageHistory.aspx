﻿<%@ Page Title="Scheduled Message" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Messages.ScheduledMessageHistory" Codebehind="ScheduledMessageHistory.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">    
<X:PageControl Id="tabsMain" runat="server" Width="100%">
	<TabPages>
		<X:TabPage Text="Generic" Name="Generic">
			<Controls>  
                <D:Panel ID="pnlMessage" runat="server" GroupingText="Info">
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                <D:Label runat="server" id="lblTitle">Title</D:Label>
                            </td>
                            <td class="control">
                                <D:Label runat="server" id="lblTitleValue" LocalizeText="false"></D:Label>
                            </td>
                            <td class="label">
                                <D:Label runat="server" id="lblSent">Sent</D:Label>
                            </td>
                            <td class="control">
                                <D:Label runat="server" id="lblSentValue" LocalizeText="false"></D:Label>
                            </td>
                        </tr>                        
                        <tr>
                            <td class="label">                                
                                <D:Label runat="server" id="lblMessage">Message</D:Label>
                            </td>
                            <td class="control" colspan="3"> 
                                <D:Label runat="server" id="lblMessageValue" LocalizeText="false"></D:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                <D:Label runat="server" id="lblMessagegroup">Messagegroup</D:Label>
                            </td>
                            <td class="control">
                                <D:Label runat="server" id="lblMessagegroupValue" LocalizeText="false"></D:Label>
                            </td>                            
                            <td class="label">
                                <D:Label runat="server" id="lblMarketingSchedule">Marketing schedule</D:Label>
                            </td>
                            <td class="control">
                                <D:Label runat="server" id="lblMarketingScheduleValue" LocalizeText="false"></D:Label>
                            </td>                            
                        </tr>
                    </table>                
                </D:Panel>              
				<D:Panel ID="pnlRecipients" runat="server" GroupingText="Recipients">
				    <br />
                    <X:GridView ID="clientGrid" ClientInstanceName="clientGrid" runat="server" Width="100%" KeyFieldName="ClientId">        
                        <SettingsBehavior AutoFilterRowInputDelay="350" />                            
                        <SettingsPager PageSize="15"></SettingsPager>
                        <SettingsBehavior AllowGroup="false" AllowDragDrop="false" />
                        <Columns>
                            <dxwgv:GridViewDataHyperLinkColumn FieldName="ClientId" VisibleIndex="1" SortIndex="0" SortOrder="Ascending">
								<Settings AutoFilterCondition="Contains" />
                                <PropertiesHyperLinkEdit NavigateUrlFormatString="~/Company/Client.aspx?id={0}" Target="_blank" TextField="ClientId"  />
                            </dxwgv:GridViewDataHyperLinkColumn>                            
                            <dxwgv:GridViewDataColumn FieldName="DeliverypointNumber" VisibleIndex="2">
				                <Settings AutoFilterCondition="Contains" />
                            </dxwgv:GridViewDataColumn>
                            <dxwgv:GridViewDataColumn FieldName="DeliverypointName" VisibleIndex="2">
				                <Settings AutoFilterCondition="Contains" />
                            </dxwgv:GridViewDataColumn>                            
                            <dxwgv:GridViewDataHyperLinkColumn FieldName="DeliverypointgroupId" VisibleIndex="3">
								<Settings AutoFilterCondition="Contains" />
                                <PropertiesHyperLinkEdit NavigateUrlFormatString="~/Company/Deliverypointgroup.aspx?id={0}" Target="_blank" TextField="DeliverypointgroupName" />
                            </dxwgv:GridViewDataHyperLinkColumn>                            
                        </Columns>
                        <Settings ShowFilterRow="True" />
                    </X:GridView>
                </D:Panel>
            </Controls>
        </X:TabPage>
    </TabPages>
</X:PageControl>
</asp:Content>