﻿using System;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logic.HelperClasses;
using Obymobi.ObymobiCms.MasterPages;

namespace Obymobi.ObymobiCms.Messages
{
    public partial class ScheduledMessagesHistory : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "ScheduledMessageHistory";            
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            LLBLGenProDataSource datasource = this.DataSource as LLBLGenProDataSource;
            if (datasource != null)
            {
                datasource.FilterToUse = new PredicateExpression(ScheduledMessageHistoryFields.ParentCompanyId == CmsSessionHelper.CurrentCompanyId);
                datasource.SorterToUse = new SortExpression(new SortClause(ScheduledMessageHistoryFields.Sent, SortOperator.Ascending));
            }
            this.MainGridView.DataBound += MainGridView_DataBound;            

            MasterPageEntityCollection masterPage = this.Master as MasterPages.MasterPageEntityCollection;
            if (masterPage != null)
            {
                masterPage.ToolBar.AddButton.Visible = false;
            }
        }

        private void MainGridView_DataBound(object sender, EventArgs e)
        {
            if (this.MainGridView.Columns["Sent"] != null) this.MainGridView.Columns["Sent"].Visible = false;
        }

        #endregion
    }
}
