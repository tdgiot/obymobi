﻿<%@ Page Title="Send Message" Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Messages.SendMessage" Codebehind="SendMessage.aspx.cs" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitleHolder" Runat="Server">Send message</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhToolbarHolder" Runat="Server">
    <X:ToolBarButton runat="server" ID="btnSend" CommandName="Send" ClientInstanceName="exportButton" Text="Send message" Image-Url="~/images/icons/accept.png" style="margin-left: 3px;float:left"></X:ToolBarButton> 
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhContentHolder" Runat="Server">
<div>
    <script type="text/javascript">
        function SelectImage(id, value) {
            document.getElementById('<%= tbImageId.ClientID %>').value = id;
            document.getElementById('<%= tbImageName.ClientID %>').value = value;
            popup.Hide();
        }
        function ClearImage(id, value) {
            document.getElementById('<%= tbImageId.ClientID %>').value = 0;
            document.getElementById('<%= tbImageName.ClientID %>').value = "";
            popup.Hide();
        }
</script>
<div>
    <dx:ASPxPopupControl ClientInstanceName="popup" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" ID="pcImage" runat="server" ShowHeader="True"
        Modal="True" CssPostfix="1" EnableAnimation="false" AutoUpdatePosition="true" ShowShadow="false"
        HeaderText="Click on an image to select it">
        <ContentCollection>
            <dx:PopupControlContentControl runat="server">
                <div style="overflow:auto;height:450px;width:530px">
                <dx:ASPxDataView ID="dvGallery" runat="server" Layout="Flow" AllowPaging="false" CssPostfix="1" ShowVerticalScrollBar="true">
                    <ItemStyle Height="170px" Width="150px" />
                    <ItemTemplate>
                        <div align="center">
                            <table>
                                <tr> 
                                    <td class="thumbnailImageCell" onclick="SelectImage(<%# Eval("MediaId") %>,'<%# Eval("Name") %>')">
                                        <D:Image ID="imgPhoto" ImageUrl='<%# Eval("ImageUrl") %>' AlternateText='<%# Eval("Name") %>' Height='150px' Width='150px' runat="server"/>
                                    </td>
                                </tr>
                            </table>
                            <asp:Label Text='<%# Eval("Name") %>' runat="server" ID="lbName" Width='150px' />
                        </div>
                    </ItemTemplate>
                </dx:ASPxDataView>
                </div>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <ContentStyle VerticalAlign="Middle" HorizontalAlign="Center" />
        <ModalBackgroundStyle BackColor="Black" />
    </dx:ASPxPopupControl>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Generic" Name="Generic">
				<Controls>
                    <D:Panel ID="pnlGeneral" runat="server" GroupingText="General">
				        <table class="dataformV2">
                            <tr>
                                <td class="label">
                                    <D:LabelTextOnly runat="server" ID="lblCompanyId">Company</D:LabelTextOnly>
                                </td>
                                <td class="control">
                                    <D:Label runat="server" ID="lblCompanyName" LocalizeText="false">Company</D:Label>
                                </td>
                                <td class="label">
                                    <D:LabelTextOnly runat="server" ID="lblApp">App</D:LabelTextOnly>
                                </td>
                                <td class="control">
                                    <X:ComboBoxInt IsRequired="True" runat="server" ID="cbApp" ValueField="Id" TextField="Name" CssClass="input" UseDataBinding="true" DisplayEmptyItem="False"></X:ComboBoxInt>
                                </td>
                            </tr>
                        </table>
                    </D:Panel>
                    <D:Panel ID="pnlMessageDetails" runat="server" GroupingText="Message details">
				        <table class="dataformV2">
                            <tr>
                                <td class="label">
                                    <D:LabelTextOnly runat="server" ID="lblMessageTemplateId">Message template</D:LabelTextOnly>
                                </td>
                                <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlMessageTemplateId" CssClass="input" UseDataBinding="true" EntityName="MessageTemplate" PreventEntityCollectionInitialization="true" AutoPostBack="true" ></X:ComboBoxLLBLGenEntityCollection>
                                </td>
                                <td class="label">
                                    <D:Label runat="server" id="lblImage">Afbeelding</D:Label>
                                </td>
                                <td class="control">
								    <D:TextBox ID="tbImageName" runat="server" ReadOnly="true"></D:TextBox>
                                </td>         
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:LabelTextOnly runat="server" ID="lblTitle">Title</D:LabelTextOnly>
                                </td>
                                <td class="control">
                                    <D:TextBoxString runat="server" ID="tbTitle" IsRequired="true" />
                                </td>
							    <td class="label">
							    </td>
							    <td class="control" >
                                    <D:Button ID="btSelectImage" runat="server" Text="Afbeelding selecteren..." UseSubmitBehavior="false" OnClientClick="popup.Show(); return false;" />
                                    <D:Button ID="btClearImage" runat="server" Text="Clear" UseSubmitBehavior="false" OnClientClick="ClearImage(); return false;" />
							    </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:LabelTextOnly runat="server" ID="lblMessage">Message</D:LabelTextOnly>
                                </td>
                                <td class="control">
                                    <D:TextBoxMultiLine runat="server" ID="tbMessage" Rows="5" MaxLength="350" IsRequired="true" UseValidation="true" />
                                </td>
                                <td class="label">
                                </td>
                                <td class="control">
                                    <D:TextBox ID="tbImageId" runat="server" style="visibility:hidden;"></D:TextBox>
                                </td>
                            </tr>
                        </table>
                    </D:Panel>
                    <D:Panel ID="pnlAction" runat="server" GroupingText="Action">
                        <table class="dataformV2">
					        <tr>
                                <td class="label">
                                    <D:Label runat="server" ID="lblActionCategoryId">Category</D:Label>
                                </td>
                                <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlActionCategoryId" IncrementalFilteringMode="StartsWith" EntityName="Category" TextField="FullCategoryMenuName" ValueField="CategoryId" PreventEntityCollectionInitialization="true">
                                        <ClientSideEvents SelectedIndexChanged="function(s, e) {ddlActionProductId.PerformCallback('1');}" />
                                    </X:ComboBoxLLBLGenEntityCollection>
                                </td>
                                <td class="label">
                                    <D:Label runat="server" ID="lblActionProductId">Product</D:Label>
                                </td>
                                <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlActionProductId" ClientInstanceName="ddlActionProductId"  OnCallback="ddlActionProductId_OnCallback" IncrementalFilteringMode="StartsWith" EntityName="Product" TextField="Name" ValueField="ProductId" PreventEntityCollectionInitialization="true" />
                                </td>
					        </tr>
                            <tr>
                                <td class="label">
                                    <D:Label runat="server" ID="lblActionSiteId">Site</D:Label>
                                </td>
                                <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlActionSiteId" IncrementalFilteringMode="StartsWith" EntityName="Site" TextField="Name" ValueField="SiteId" PreventEntityCollectionInitialization="true">
                                        <ClientSideEvents SelectedIndexChanged="function(s, e) {ddlActionPageId.PerformCallback('1');}" />
                                    </X:ComboBoxLLBLGenEntityCollection>
                                </td>
                                <td class="label">
                                    <D:Label runat="server" ID="lblActionPageId">Site pagina</D:Label>
                                </td>
                                <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlActionPageId" ClientInstanceName="ddlActionPageId"  OnCallback="ddlActionPageId_OnCallback" IncrementalFilteringMode="StartsWith" EntityName="Page" TextField="Name" ValueField="PageId" PreventEntityCollectionInitialization="true" />
                                </td>
                            </tr>
				        </table> 
                    </D:Panel>
                </Controls>
            </X:TabPage>
        </TabPages>
    </X:PageControl>
</asp:Content>