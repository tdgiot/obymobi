﻿using System;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.ObymobiCms.Messages
{
    public partial class ScheduledMessages : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        #region Event handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            LLBLGenProDataSource datasource = this.DataSource as LLBLGenProDataSource;
            if (datasource != null)
            {
                PredicateExpression filter = new PredicateExpression(ScheduledMessageFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
                datasource.FilterToUse = filter;                
            }
        }

        #endregion
    }
}
