﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms._404" CodeBehind="404.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" id="HtmlHeader">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Crave Content Management System</title>
    <meta http-equiv="imagetoolbar" content="no" />
    <link href="~/favicon.ico" type="image/x-icon" link rel="shortcut icon" />

    <link href="~/css/default.css" type="text/css" link rel="Stylesheet" />
    <link href="~/css/default-hide-left-column.css" type="text/css" link rel="Stylesheet" />
    <link href="~/css/base.css" type="text/css" link rel="Stylesheet" />
    <link href="~/css/eWorldUiControls.css" type="text/css" link rel="Stylesheet" />
    <link href="~/css/mediacollection.css" type="text/css" link rel="Stylesheet" />
    <link href="~/css/mediabox.css" type="text/css" link rel="Stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div id="head" style="position:relative;">
            <div class="titles">
                <div class="pagetitle">
                    <div style="padding-left: 6px;">
                        The item that you are looking for does not exist.
                    </div>
                </div>
            </div>
        </div>
        <div id="content">
            <D:PlaceHolder ID="plhEmailSent" runat="server" Visible="false"><strong>Your e-mail has been sent to Crave.</strong></D:PlaceHolder>
            <br />
            <br />
            <D:PlaceHolder ID="plhMessage" runat="server"></D:PlaceHolder>
            <D:Button runat="server" ID="btnBack" Text="Go back" />
            <D:Button runat="server" ID="btnEmail" Text="E-mail to Crave" />
        </div>
    </form>
</body>
</html>
