using Dionysos;
using Dionysos.Interfaces.Data;
using Dionysos.Web;
using Dionysos.Web.Security;
using Dionysos.Web.UI;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Cms;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ServiceStack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.UI;

namespace Obymobi.ObymobiCms.MasterPages
{
    public partial class MasterPageBase : Obymobi.ObymobiCms.Web.UI.MasterPageBase
    {
        private const string CompanyCollectionCacheKey = "Obymobi.Cms.CompanyCollection";

        private bool PageTitleOverride = false;

        protected override void OnInit(EventArgs e)
        {
            int companyId;
            if (QueryStringHelper.TryGetValue("switchToCompany", out companyId))
            {
                if (CmsSessionHelper.CurrentCompanyId != companyId)
                {
                    if (CmsSessionHelper.CurrentRole >= Role.Administrator)
                    {
                        // Can show all.
                        CmsSessionHelper.CurrentCompanyId = companyId;
                        WebShortcuts.RedirectUsingRawUrl();
                    }
                    else
                    {
                        // Can't switch
                        this.Response.Redirect("~/SignOn.aspx?NoRightsToAccesCompanyData=true", true);
                    }
                }
            }

            base.OnInit(e);
        }

        public void SetPageTitle(string pageTitle)
        {
            Dionysos.Web.UI.Page dionysosPage = this.Page as Dionysos.Web.UI.Page;

            this.Page.Title = pageTitle;

            this.cplhTitleHolder.Controls.Clear();
            this.cplhTitleHolder.Controls.Add(new LiteralControl(pageTitle));

            if (dionysosPage != null)
                dionysosPage.CrumbleTitle = StringUtil.StripHtml(pageTitle);

            PageTitleOverride = true;
        }

        private void UpdateGuiForEntityPages()
        {
            this.hlPoweredBy.Text = "Crave Interactive";
            this.hlPoweredBy.NavigateUrl = "http://www.crave-emenu.com";

            if (this.PageTitleOverride)
            {
                return;
            }

            Dionysos.Web.UI.Page dionysosPage = this.Page as Dionysos.Web.UI.Page;

            List<string> excludedPageTitles = new List<string>();
            excludedPageTitles.Add("Feeds");
            excludedPageTitles.Add("Feed");
            excludedPageTitles.Add("Feed item");
            excludedPageTitles.Add("Feed synchronization");

            if (excludedPageTitles.Contains(this.Page.Title))
            {
                string pageTitle = this.Page.Title;
                string pageHtmlTitle = this.Page.Title;

                this.Page.Title = pageTitle;
                this.cplhTitleHolder.Controls.Clear();
                this.cplhTitleHolder.Controls.Add(new LiteralControl(pageHtmlTitle));

                if (dionysosPage != null)
                    dionysosPage.CrumbleTitle = StringUtil.StripHtml(pageHtmlTitle);
            }
            else if (this.PageAsPageEntity != null && this.PageAsPageEntity.UseEntityInformation)
            {
                string pageTitle = "";
                string pageHtmlTitle = "";

                // Add entitytype name
                IEntityInformation info = Dionysos.Data.EntityInformationUtil.GetEntityInformation(this.PageAsPageEntity.EntityName);

                if (info != null)
                {
                    // Add "Add" or displayname
                    if (this.PageAsPageEntity.PageMode == Dionysos.Web.PageMode.Add)
                    {
                        string breadCrumbHtml;
                        if (this.GetHtmlForBreadCrumbTitle(this.PageAsPageEntity.DataSource, info, out breadCrumbHtml))
                        {
                            pageHtmlTitle = breadCrumbHtml;
                        }
                        else
                        {
                            string strAdd = new PageDefault().Translate("Add", "Toevoegen");
                            pageTitle = string.Format("{0} | {1}", strAdd, info.FriendlyNamePlural);
                            pageHtmlTitle = string.Format("{0} <span style=\"font-weight:normal\">&gt; {1}</span>", info.FriendlyNamePlural, strAdd);
                        }
                    }
                    else if (this.PageAsPageEntity.PageMode == Dionysos.Web.PageMode.Edit)
                    {
                        string breadCrumbHtml;
                        if (this.GetHtmlForBreadCrumbTitle(this.PageAsPageEntity.DataSource, info, out breadCrumbHtml))
                        {
                            pageHtmlTitle = breadCrumbHtml;
                        }
                        else if (info.ShowFieldName.Length != 0 &&
                            this.PageAsPageEntity.DataSource.Fields[info.ShowFieldName] != null &&
                            this.PageAsPageEntity.DataSource.Fields[info.ShowFieldName].CurrentValue != null)
                        {
                            pageTitle = string.Format("{0} | {1}", this.PageAsPageEntity.DataSource.Fields[info.ShowFieldName].CurrentValue, info.FriendlyNamePlural);
                            pageHtmlTitle = string.Format("{0} <span style=\"font-weight:normal\">&gt; {1}</span>", info.FriendlyNamePlural, this.PageAsPageEntity.DataSource.Fields[info.ShowFieldName].CurrentValue);
                        }
                        else if (info.ShowFieldName.Length != 0 &&
                            Dionysos.Reflection.Member.HasProperty(this.PageAsPageEntity.DataSource, info.ShowFieldName))
                        {
                            string showFieldValue = Dionysos.Reflection.Member.InvokeProperty(this.PageAsPageEntity.DataSource, info.ShowFieldName).ToString();
                            pageTitle = string.Format("{0} | {1}", showFieldValue, info.FriendlyNamePlural);
                            pageHtmlTitle = string.Format("{0} <span style=\"font-weight:normal\">&gt; {1}</span>", info.FriendlyNamePlural, showFieldValue);
                        }
                        else
                        {
                            pageHtmlTitle = string.Format("{0} <span style=\"font-weight:normal\">&gt; {1}</span>", info.FriendlyNamePlural, "Edit");
                        }
                    }

                    this.Page.Title = pageTitle;

                    this.cplhTitleHolder.Controls.Clear();
                    this.cplhTitleHolder.Controls.Add(new LiteralControl(pageHtmlTitle));

                    if (dionysosPage != null)
                        dionysosPage.CrumbleTitle = StringUtil.StripHtml(pageHtmlTitle);
                }
            }
            else if (this.PageAsPageEntityCollection != null)
            {
                IEntityInformation info = Dionysos.Data.EntityInformationUtil.GetEntityInformation(this.PageAsPageEntityCollection.EntityName);
                if (info != null)
                {
                    string pageTitle = info.FriendlyNamePlural;
                    string pageHtmlTitle = info.FriendlyNamePlural;

                    this.Page.Title = pageTitle;
                    this.cplhTitleHolder.Controls.Clear();
                    this.cplhTitleHolder.Controls.Add(new LiteralControl(pageHtmlTitle));

                    if (dionysosPage != null)
                        dionysosPage.CrumbleTitle = StringUtil.StripHtml(pageHtmlTitle);
                }
            }
        }

        private void RegisterCssLinks()
        {
            Dionysos.Web.UI.MasterPage.LinkedCssFiles.Add("~/css/default.css", 10);
            Dionysos.Web.UI.MasterPage.LinkedCssFiles.Add("~/css/base.css", 20);
            Dionysos.Web.UI.MasterPage.LinkedCssFiles.Add("~/css/mediacollection.css", 40);
            Dionysos.Web.UI.MasterPage.LinkedCssFiles.Add("~/css/mediabox.css", 50);

            if (!PathHelper.FileName.Equals("CompanyManagement.aspx", StringComparison.InvariantCultureIgnoreCase))
            {
                Dionysos.Web.UI.MasterPage.LinkedJsFiles.Add("~/js/mootools-1.2.3-core-nc.js", 10);
                Dionysos.Web.UI.MasterPage.LinkedJsFiles.Add("~/js/mootools-1.2.3.1-more-nc.js", 11);
                Dionysos.Web.UI.MasterPage.LinkedJsFiles.Add("~/js/sortablePanels.js", 30);
            }

            if (PathHelper.FileName.Equals("CatalogProduct.aspx", StringComparison.InvariantCultureIgnoreCase) ||
                PathHelper.FileName.Equals("CatalogCategory.aspx", StringComparison.InvariantCultureIgnoreCase) ||
                PathHelper.FileName.Equals("CatalogAlteration.aspx", StringComparison.InvariantCultureIgnoreCase) ||
                PathHelper.FileName.Equals("CmsPage.aspx", StringComparison.InvariantCultureIgnoreCase) ||
                PathHelper.FileName.Equals("CmsPageTemplate.aspx", StringComparison.InvariantCultureIgnoreCase))
            {
                Dionysos.Web.UI.MasterPage.LinkedJsFiles.Add("~/js/markdown.js", 100);
                Dionysos.Web.UI.MasterPage.LinkedCssFiles.Add("~/css/markdown.css", 100);
            }

            if (this.PageAsPageEntity != null)
                Dionysos.Web.UI.MasterPage.LinkedJsFiles.Add("~/js/alert-to-save.js", 70);
        }

        private void BuildToolbar()
        {
        }

        private void BuildMenu()
        {
            var user = UserManager.CurrentUser as UserEntity;
            MenuItemCollection menuItems = null;
            string cacheKey = string.Empty;

            if (user != null)
            {
                cacheKey = string.Format("Obymobi.Cms.MainMenu-{0}-{1}-{2}", user.UserId, user.Role.ToIntString(), this.Page.GetType().Name);
                CacheHelper.TryGetValue(cacheKey, false, out menuItems);
            }

            if (menuItems == null)
            {
                menuItems = MenuHelper.GetMenuItemCollection(UIElementHelper.GetUIElementEntity(this.Page), true);
                if (user != null)
                    CacheHelper.AddSlidingExpire(false, cacheKey, menuItems, 600);
            }

            // Build the Main Menu
            this.BuildMainMenu(menuItems);
            this.BuildModuleMenu(menuItems);


            if (!WebEnvironmentHelper.CloudEnvironmentIsProduction)
            {
                string productionValue = WebEnvironmentHelper.CloudEnvironment.ToString();
                environment.InnerText = productionValue + " environment";
            }
            else
            {
                environment.Visible = false;
            }


        }

        private void BuildMainMenu(Dionysos.Web.UI.MenuItemCollection menuItems)
        {
            bool publishingAllowed = CmsSessionHelper.CurrentUser.AllowPublishing;
            bool hideNoAccessItems = true;
            for (int i = 0; i < menuItems.Count; i++)
            {
                MenuItem menuItemModule = menuItems[i] as MenuItem;
                bool accessAllowed = menuItemModule.Enabled;

                if (menuItemModule.Value.EqualsIgnoreCase("Publish") && !publishingAllowed)
                {
                    continue;
                }

                if ((accessAllowed || !hideNoAccessItems) && menuItemModule.ChildItems.Count > 0)
                {
                    this.plhMainMenu.AddHtml("<li>");
                    Dionysos.Web.UI.WebControls.HyperLink hlLink = new Dionysos.Web.UI.WebControls.HyperLink();
                    hlLink.ID = "hl" + menuItemModule.Value;
                    hlLink.NavigateUrl = menuItemModule.NavigateUrl;
                    hlLink.Text = menuItemModule.Text;
                    hlLink.LocalizeText = false;

                    // If last indicate so
                    if (i + 1 == menuItems.Count)
                        hlLink.CssClass = "last";

                    // Select active item
                    if (menuItemModule.Selected)
                        hlLink.CssClass = "active";

                    // Render it's submenu items as a submenu
                    if (menuItemModule.ChildItems.Count > 0)
                    {
                        // Render opening of submenu
                        this.plhMainMenu.AddHtml("<!--[if IE 7]><!--></a><!--<![endif]--><!--[if lte IE 6]><table><tr><td><![endif]--><ul>");

                        // Render the items
                        for (int j = 0; j < menuItemModule.ChildItems.Count; j++)
                        {
                            MenuItem current = menuItemModule.ChildItems[j];
                            this.plhMainMenu.AddHtml("<li><a href=\"{0}\">{1}</a></li>", this.ResolveUrl(current.NavigateUrl), current.Text);
                        }

                        // Rendre closing of submenu
                        this.plhMainMenu.AddHtml("</ul><!--[if lte IE 6]></td></tr></table></a><![endif]-->");
                    }

                    this.plhMainMenu.Controls.Add(hlLink);
                    this.plhMainMenu.AddHtml("</li>");
                }
            }
        }

        private void BuildModuleMenu(Dionysos.Web.UI.MenuItemCollection menuItems)
        {
            // Retrieve menu for active module
            MenuItem menuOfActiveModule = menuItems.GetSelectedItemAtDepth(0);

            // Set module title			
            this.lblModuleName.Text = menuOfActiveModule.Text;

            for (int i = 0; i < menuOfActiveModule.ChildItems.Count; i++)
            {
                MenuItem submenuItem = menuOfActiveModule.ChildItems[i];

                // Only non-configuration items			
                if (((UIElementEntity)submenuItem.DataItem).IsConfigurationItem)
                {
                    continue;
                }

                this.plhModuleMenu.AddHtml("<li>");

                Dionysos.Web.UI.WebControls.HyperLink hlSubMenuLink = new Dionysos.Web.UI.WebControls.HyperLink();
                hlSubMenuLink.ID = "hlSub" + submenuItem.Value;
                hlSubMenuLink.NavigateUrl = submenuItem.NavigateUrl;
                hlSubMenuLink.Text = submenuItem.Text;
                hlSubMenuLink.LocalizeText = false;

                if (submenuItem.Selected)
                    hlSubMenuLink.CssClass = "active";

                this.plhModuleMenu.Controls.Add(hlSubMenuLink);
                this.plhModuleMenu.AddHtml("</li>");
            }
        }

        private void SetCompanies()
        {
            UserEntity user = CmsSessionHelper.CurrentUser;
            if (!IsPostBack && user != null)
            {
                CompanyCollection companyCollection;
                try
                {
                    companyCollection = RetrieveAccessibleCompaniesForUser(user);
                }
                catch (AuthorizationException)
                {
                    UserManager.RefreshCurrentUser();
                    CmsSessionHelper.ClearCompaniesFromCacheForUser(user.UserId);
                    user = CmsSessionHelper.CurrentUser;

                    companyCollection = RetrieveAccessibleCompaniesForUser(user);
                }

                UpdateCompanySelector(companyCollection);
            }
        }

        private static CompanyCollection RetrieveAccessibleCompaniesForUser(UserEntity user)
        {
            if (user.Role >= Role.Crave)
            {
                return RetrieveOrderedCompanies();
            }
            else
            {
                return RetrieveOrderedCompaniesFromCache(user.AccountId);
            }
        }

        private static CompanyCollection RetrieveOrderedCompaniesFromCache(int? userAccountId = null)
        {
            CompanyCollection companyCollection = new CompanyCollection();

            if (userAccountId.HasValue)
            {
                // Only get the companies for this account
                string cacheKey = $"{CompanyCollectionCacheKey}-{userAccountId.Value}";

                // Try to get the companies from the cache
                companyCollection = RetrieveOrderedCompaniesFromCache(userAccountId, cacheKey);
            }
            else if (CmsSessionHelper.CurrentRole >= Role.Administrator)
            {
                // Fallback if no account was specified yet at the user
                companyCollection = RetrieveOrderedCompaniesFromCache(null, CompanyCollectionCacheKey);
            }

            return companyCollection;
        }

        private static CompanyCollection RetrieveOrderedCompaniesFromCache(int? userAccountId, string cacheKey)
        {
            if (!CacheHelper.TryGetValue(cacheKey, false, out CompanyCollection companyCollection))
            {
                companyCollection = RetrieveOrderedCompanies(userAccountId);
                CacheHelper.Add(false, cacheKey, companyCollection, null, DateTime.MaxValue, Cache.NoSlidingExpiration, CacheItemPriority.Normal, null);
            }

            return companyCollection;
        }

        public static void ClearCompaniesCacheForAccount(int userAccountId)
        {
            string cacheKey = $"{CompanyCollectionCacheKey}-{userAccountId}";
            CacheHelper.Remove(false, cacheKey);
        }

        private static CompanyCollection RetrieveOrderedCompanies(int? userAccountId = null)
        {
            SortExpression sort = new SortExpression(CompanyFields.Name | SortOperator.Ascending);
            CompanyCollection companyCollection = new CompanyCollection();


            PredicateExpression filter = new PredicateExpression(CompanyFields.Archived == false);
            if (userAccountId.HasValue)
            {
                RelationCollection relations = new RelationCollection(CompanyEntity.Relations.AccountCompanyEntityUsingCompanyId);
                filter.Add(AccountCompanyFields.AccountId == userAccountId.Value);
                companyCollection.GetMulti(filter, 0, sort, relations);
            }
            else
            {
                companyCollection.GetMulti(filter, 0, sort);
            }

            return companyCollection;
        }

        private void UpdateCompanySelector(CompanyCollection companyCollection)
        {
            if (companyCollection.Any())
            {
                if (companyCollection.Count > 1)
                {
                    this.plhCompanyList.Visible = true;
                }
                else
                {
                    this.plhCompanySingle.Visible = true;
                    this.lblCompanyValue.Text = companyCollection.First().Name;
                }

                this.ddlCompanies.Items.Clear();
                foreach (CompanyEntity company in companyCollection)
                {
                    if (!CmsSessionHelper.ShouldHideCompany(company))
                    {
                        this.ddlCompanies.Items.Add(company.Name, company.CompanyId);
                    }
                }

                this.ddlCompanies.Items.Insert(0, new DevExpress.Web.ListEditItem(new PageDefault().Translate("SelectCompanyItem", ""), -1));

                this.ddlCompanies.Value = CmsSessionHelper.CurrentCompanyId;
            }
            else
            {
                this.plhCompanySingle.Visible = true;
                this.lblCompanyValue.Text = "Not configured";
            }
        }


        private void HookupEvents()
        {
            this.PreRender += new EventHandler(Default_PreRender);
            this.ddlCompanies.SelectedIndexChanged += new EventHandler(ddlCompanies_SelectedIndexChanged);
        }

        /// <summary>
        /// Show the users navigation history
        /// </summary>
        void ShowHistory()
        {
            // Show current page			
            if (Page is Dionysos.Web.UI.Page dionysosPage && dionysosPage.CrumbleTitle?.Length > 0)
            { plhNavigationHistory.AddHtml("<li>{0}</li>", dionysosPage.CrumbleTitle); }
            else
            { plhNavigationHistory.AddHtml("<li>{0}</li>", Page.Title); }

            // Render last 10 crumbles
            Crumbles crumbles = Crumbles.CurrentCrumbles;

            int maxCrumbles = (crumbles.Count > 20) ? crumbles.Count - 21 : 0;

            for (int i = crumbles.Count - 1; i >= maxCrumbles; i--)
            {
                Crumble current = crumbles[i];

                plhNavigationHistory.AddHtml("<li><a href=\"{0}\">{1}</a></li>", ResolveUrl(current?.Url ?? string.Empty), current?.PageTitle);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // GK Moeten altijd opnieuw omdat ze verloren raken bij Postback, denk door ViewState niet beschikbaar voor Html.
            this.BuildMenu();
            this.BuildToolbar();
            this.RegisterCssLinks();
            this.SetCompanies();
            this.HookupEvents();

            // Set username
            this.lblUsername.Text = UserManager.CurrentUser.ShortDisplayName;

            if (CmsSessionHelper.CurrentCompanyId <= 0 && !this.Request.RawUrl.Contains("default.aspx", StringComparison.InvariantCultureIgnoreCase))
            {
                // Return to the home page when no company is selected
                Response.Redirect("~/default.aspx");
            }

            if (QueryStringHelper.HasValue("Console"))
                this.HideMasterControls();

            if (WebEnvironmentHelper.CloudEnvironmentIsProduction)
                this.plhGoogleAnalytics.Visible = true;
        }

        private void HideMasterControls()
        {
            Dionysos.Web.UI.MasterPage.LinkedCssFiles.Add("~/css/HiddenMaster.css", 99);
        }

        private void Default_PreRender(object sender, EventArgs e)
        {
            this.UpdateGuiForEntityPages();
            this.ShowHistory();

            if (DivToolbarCssClass.Length > 0)
            {
                this.plhDivToolbar.Controls.Clear();
                this.plhDivToolbar.AddHtml("<div id=\"toolbar\" class=\"{0}\">", DivToolbarCssClass);
            }

            // Add application name to the Title
            this.Page.Title += " | Crave Content Management System";

            // Show notification bar when needed
            this.plhNotifcationBar.Visible = false;
            try
            {
                if (!this.Page.IsValid)
                {
                    this.plhNotifcationBar.Visible = true;
                }
                else
                {
                    this.plhNotifcationBar.Visible = true;
                }
            }
            catch
            {
                // nothing, error occured: Page.IsValid cannot be called before validation has taken place. It should be queried in the event handler for a control that has CausesValidation=True and initiated the postback, or after a call to Page.Validate.
            }
        }

        private void ddlCompanies_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Set the current company id
            CmsSessionHelper.CurrentCompanyId = ddlCompanies.ValidId;

            // Clear Navigation History
            Crumbles.CurrentCrumbles.Clear();

            // Redirect to Installation page for new Company
            Response.Redirect("~/Company/Installation.aspx");
        }

        /// <summary>
        /// Gets or sets the DivToolbarCssClass
        /// </summary>
        public static string DivToolbarCssClass
        {
            get
            {
                if (HttpContext.Current.Items["DivToolbarCssClass"] != null)
                    return (string)HttpContext.Current.Items["DivToolbarCssClass"];
                else
                    return string.Empty;
            }
            set
            {
                HttpContext.Current.Items["DivToolbarCssClass"] = value;
            }
        }
    }
}
