﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Dionysos.Web.UI.DevExControls;

namespace Obymobi.ObymobiCms.MasterPages
{
    public partial class MasterPageEntityCollection : System.Web.UI.MasterPage
    {
		public MasterPageEntityCollection()
		{			
		}

		protected override void OnInit(EventArgs e)
		{
			
			base.OnInit(e);						
		}

		void gvMainGridView_PreRender(object sender, EventArgs e)
		{
        }

        public void SetPageTitle(string pageTitle)
        {
            MasterPageBase masterPage = this.Master as MasterPageBase;
            if (masterPage != null)
            {
                masterPage.SetPageTitle(pageTitle);
            }
        }

        #region Properties

        public ToolBarPageOverviewDataSourceCollection ToolBar
        {
            get { return this.ToolBarPageOverviewDataSourceCollection; }
        }

        #endregion
    }
}
