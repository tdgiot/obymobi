﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Dionysos.Web.UI;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dionysos.Web.UI.DevExControls;

namespace Obymobi.ObymobiCms.MasterPages
{
	public partial class MasterPageEntity : System.Web.UI.MasterPage
	{
		#region Methods

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);

            if (this.PageAsPageLLBLGenEntity != null)
			    this.PageAsPageLLBLGenEntity.Load += new EventHandler(PageAsPageLLBLGenEntity_Load);
		}


		void UpdateGui()
	    {
		    IEntity datasource = this.PageAsPageLLBLGenEntity.DataSource;

		    if (datasource.Fields["Archived"] != null && 
			    datasource.Fields["Archived"].CurrentValue is bool &&
			    (bool)datasource.Fields["Archived"].CurrentValue)
		    {
			    this.plhArchived.Visible = true;
		    }
	    }

        public void SetPageTitle(string pageTitle)
        {
            MasterPageBase masterPage = this.Master as MasterPageBase;
            if (masterPage != null)
            {
                masterPage.SetPageTitle(pageTitle);
            }
        }

        #endregion

        #region Event Handlers

        void PageAsPageLLBLGenEntity_Load(object sender, EventArgs e)
		{
			this.UpdateGui();

            if (this.ToolBarPageEntity.SaveButton != null) this.ToolBarPageEntity.SaveButton.UseSubmitBehavior = false;
            if (this.ToolBarPageEntity.SaveAndNewButton != null) this.ToolBarPageEntity.SaveAndNewButton.UseSubmitBehavior = false;
            if (this.ToolBarPageEntity.SaveAndGoButton != null) this.ToolBarPageEntity.SaveAndGoButton.UseSubmitBehavior = false;
            if (this.ToolBarPageEntity.DeleteButton != null) this.ToolBarPageEntity.DeleteButton.UseSubmitBehavior = false;
            if (this.ToolBarPageEntity.CancelButton != null) this.ToolBarPageEntity.CancelButton.UseSubmitBehavior = false;
		}

		#endregion

		#region Properties

		PageLLBLGenEntity PageAsPageLLBLGenEntity
		{
			get
			{
				return this.Page as PageLLBLGenEntity;
			}
		}

	    public ToolBarPageEntity ToolBar
	    {
	        get { return this.ToolBarPageEntity; }
	    }

	    public ContentPlaceHolder PageContent
	    {
            get { return this.cplhPageContent; }
	    }

		#endregion
	}
}