﻿using Dionysos.Web;
using Dionysos.Web.UI;
using Obymobi.Data.EntityClasses;
using Obymobi.ObymobiCms.Generic.SubPanels;
using System;

namespace Obymobi.ObymobiCms.Catalog
{
	public partial class ProductV2 : PageLLBLGenEntity
    {
        #region Fields

        private OptionPanel optionPanel = null;

        #endregion

        #region Methods

        private void LoadUserControls()
        {            
            this.optionPanel = (OptionPanel)this.LoadControl("~/Generic/SubPanels/OptionPanel.ascx");
            this.plhOptions.Controls.Add(this.optionPanel);
        }

        #endregion

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "Product";            
            this.LoadUserControls();            
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
		{
            int version = 2;
            QueryStringHelper.TryGetValue<int>("Version", out version);

            if (this.optionPanel != null && !this.DataSourceAsProductEntity.IsNew)
            {
                this.optionPanel.Load(version, this.DataSourceAsProductEntity.ProductId, -1, -1, -1, true);
            }
        }

        public override bool Save()
        {
            this.DataSourceAsProductEntity.ValidatorCreateOrUpdateDefaultCustomText = true;
            return base.Save();
        }

        #endregion

        #region Properties

        private ProductEntity DataSourceAsProductEntity
        {
            get { return this.DataSource as ProductEntity; }
        }

        #endregion
    }
}
