﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" EnableSessionState="ReadOnly" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Catalog.ProductV2" Title="Product V2" Codebehind="ProductV2.aspx.cs" %>
<%@ Reference Control="~/Generic/SubPanels/OptionPanel.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" runat="Server">
</asp:Content>
<asp:Content ID="Conent3" ContentPlaceHolderID="cplhAdditionalToolBarButtons" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhPageContent" runat="Server">
<div>
    <X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Generic" Name="Generic">
				<controls>
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblName">Name</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
							</td>	  
                            <td class="label">								
							</td>
							<td class="control">								
							</td>      
                        </tr>
                        <tr>
                            <td class="label">
                                <D:Label runat="server" id="lblComponents">Items</D:Label>
                            </td>
                            <td class="control" colspan="3">
                                <div style="width: 736px;">
                                    <D:PlaceHolder runat="server" ID="plhOptions"></D:PlaceHolder>
                                </div>
                            </td>
                        </tr>                        
                    </table>                    
                </controls>
            </X:TabPage>
        </TabPages>
    </X:PageControl>	
</div>
</asp:Content>