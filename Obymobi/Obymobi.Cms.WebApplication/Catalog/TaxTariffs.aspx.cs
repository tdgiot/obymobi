﻿using System;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;

namespace Obymobi.ObymobiCms.Catalog
{
    public partial class TaxTariffs : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        #region Event handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.DataSource is LLBLGenProDataSource dataSource)
            {
                dataSource.FilterToUse = new PredicateExpression(TaxTariffFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            }
        }

        #endregion
    }
}
