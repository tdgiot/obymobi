﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="PriceScheduledItemsPanel" Codebehind="PriceScheduledItemsPanel.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.ASPxScheduler.v20.1" Namespace="DevExpress.Web.ASPxScheduler" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.XtraScheduler.v20.1.Core" Namespace="DevExpress.XtraScheduler" TagPrefix="dx" %>

<table class="dataformV2">
    <tr>
		<td class="label">
		</td>
		<td class="control">
		    <D:Button runat="server" ID="btAddItem" Text="Add Item" OnClientClick="scheduler.RaiseCallback('MNUVIEW|NewAppointment'); return false;"  />
            <asp:HiddenField ID="hfFilter" ClientIDMode="Static" runat="server" />
		</td>	
        <td class="label">
		</td>
		<td class="control">
		</td>	    
	</tr>		
</table>
<small>Note: Hold 'Ctrl' while dragging an item to copy the item.</small>
<dx:ASPxScheduler ID="scheduler" runat="server" Width="100%" ActiveViewType="Timeline" ClientIDMode="static" AppointmentDataSourceID="priceScheduleDataSource">
    <OptionsToolTips AppointmentToolTipUrl="~/Company/SubPanels/ScheduleAppointmentTooltip.ascx" AppointmentToolTipCornerType="None" />
    <OptionsBehavior ShowViewSelector="true"/>
        <Storage>
        <Appointments>
            <Mappings AppointmentId="PriceScheduleItemId" Start="StartTime" End="EndTime" Subject="Subject"  AllDay="AllDay" Description="Description" Label="Label" Location="Location" RecurrenceInfo="RecurrenceInfo" ReminderInfo="ReminderInfo" Status="Status" Type="Type" />
            <CustomFieldMappings>                
                <dx:ASPxAppointmentCustomFieldMapping Member="PriceLevelId" Name="PriceLevelId" ValueType="Integer" />
                <dx:ASPxAppointmentCustomFieldMapping Member="BackgroundColor" Name="BackgroundColor" ValueType="Integer" />
                <dx:ASPxAppointmentCustomFieldMapping Member="TextColor" Name="TextColor" ValueType="Integer" />
            </CustomFieldMappings>
        </Appointments>
    </Storage>
    <OptionsForms AppointmentFormTemplateUrl="~/Company/SubPanels/PriceScheduleAppointment.ascx" />
</dx:ASPxScheduler>
<asp:ObjectDataSource ID="priceScheduleDataSource" runat="server" 
    DataObjectTypeName="PriceScheduleAppointment"
    DeleteMethod="DeleteMethodHandler"
    InsertMethod="InsertMethodHandler" 
    SelectMethod="SelectMethodHandler" 
    UpdateMethod="UpdateMethodHandler" 
    TypeName="PriceScheduleDataSource" 
    onobjectcreated="PriceScheduleDataSource_ObjectCreated"> 
</asp:ObjectDataSource> 
<script type="text/javascript">
    // <![CDATA[
    scheduler.menuManager.UpdateAppointmentMenuItems = function (appointmentViewInfo, menu)
    {
        var apt = this.scheduler.GetAppointment(appointmentViewInfo.appointmentId);
        if (apt == null)
            return;
        var aptType = apt.appointmentType;
        var isRecurring = aptType != ASPxAppointmentType.Normal;
        menu.GetItemByName("EditItem").SetVisible(!isRecurring);
        menu.GetItemByName("EditPattern").SetVisible(isRecurring);
        return this.constructor.prototype.UpdateAppointmentMenuItems.call(this, appointmentViewInfo, menu);
    }

    function DefaultAppointmentMenuHandler(control, s, args)
    {
        if (args.item.name == "AddItem")
        {
            control.RaiseCallback('MNUVIEW|NewAppointment');
        }
        else if (args.item.name == "EditItem")
        {
            control.RaiseCallback('MNUAPT|OpenAppointment');
        }
        else if (args.item.name == "EditPattern")
        {
            control.RaiseCallback('MNUAPT|EditSeries');
        }
        else if (args.item.GetItemCount() <= 0)
        {
            control.RaiseCallback("USRAPTMENU|" + args.item.name);
        }
    }
    // ]]> 
</script>