﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Catalog.SubPanels.AlterationComponentsPanel" Codebehind="AlterationComponentsPanel.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v20.1" Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dxwtl" %>
<%@ Register TagPrefix="dxwtl" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<script type="text/javascript">

    var lastItemType = null;

    var existingItemType = null;
    function OnItemTypeChanged(cmbItemType) {
        if (cmbItemType.InCallback())
            existingItemType = cmbItemType.GetValue().toString();
        else
            cmbItem.PerformCallback(cmbItemType.GetValue().toString());        
    }

    function OnItemEndCallback(s, e) {
        if (existingItemType) {
            cmbItem.PerformCallback(existingItemType);
            existingItemType = null;
        }
    }

    function OnInit(s, e) {        
        ASPxClientUtils.AttachEventToElement(s.GetMainElement(), "keypress",
        function (evt) {
            switch (evt.keyCode) {
                    //ENTER
                case 13:
                    if (s.IsEditing()) {
                        s.UpdateEdit();
                        evt.stopPropagation();
                    }
                    break;

                    //ESC               
                case 27:
                    if (s.IsEditing()) {
                        s.CancelEdit();
                        evt.stopPropagation();
                    }
                    break;
            }
        });
    }

</script>
<div style="width: 736px;">
    <span class="nobold" style="width: 100%; height: 24px; margin-left: 0;">
        <D:HyperLink runat="server" ID="hlExpandAll">Expand all</D:HyperLink> | 
        <D:HyperLink runat="server" ID="hlCollapseAll">Collapse all</D:HyperLink> | 
        <D:CheckBox runat="server" ID="cbSortingMode" ClientIDMode="Static" Text="Sort" notdirty="true" /> 
        <D:LabelTextOnly runat="server" ID="lblSortingWithShift">(or hold shift while dragging)</D:LabelTextOnly> |
        <D:Button runat="server" ID="btnSortSelected" Text="Auto-Sort Selected Category" />
        <D:Button runat="server" ID="btDeleteItemsTop" style="float:right;" Text="Delete" />
    </span>                                    
    <dxwtl:ASPxTreeList ID="tlComponents" ClientInstanceName="tlComponents" runat="server" EnableViewState="false">
        <SettingsSelection Enabled="true" Recursive="false" />        
        <ClientSideEvents Init="OnInit" />                
        <Templates>
            <EditForm>
                <dxwtl:ASPxPageControl ID="tabs" runat="server" ActiveTabIndex="0" Width="100%">
                    <TabPages>
                        <dxwtl:TabPage Text="New">
                             <ContentCollection>
                                 <dxwtl:ContentControl runat="server">
                                    <dxwtl:ASPxTreeListTemplateReplacement runat="server" ReplacementType="Editors" />
                                </dxwtl:ContentControl>
                            </ContentCollection>
                        </dxwtl:TabPage>
                        <dxwtl:TabPage Text="Existing">
                            <ContentCollection>
                                <dxwtl:ContentControl runat="server">   
                                    <table cellspacing="0" border="0" style="width:100%;border-collapse:collapse;">
                                        <tr>
                                            <td class="dxtlEditFormCaption_Glass" rowspan="1">
                                                <D:Label runat="server" id="lblItemType" Style="font-weight: normal;">Item Type:</D:Label>
                                            </td>
                                            <td class="dxtlEditFormEditCell_Glass" style="width: 50%;" colspan="1">
                                                <dxwtl:ASPxComboBox ID="cmbItemType" TextField="ItemType" runat="server" style="width: 100%" IncrementalFilteringMode="Contains">
                                                    <ClientSideEvents SelectedIndexChanged="function(s, e) { OnItemTypeChanged(s); }"></ClientSideEvents>
                                                </dxwtl:ASPxComboBox>
                                            </td>     
                                            <td class="dxtlEditFormCaption_Glass" rowspan="1"></td> 
                                            <td class="dxtlEditFormEditCell_Glass" style="width: 50%;" colspan="1"></td>
                                        </tr>
                                        <tr>
                                            <td class="dxtlEditFormCaption_Glass" rowspan="1">
                                                <D:Label runat="server" id="lblEntity" Style="font-weight: normal;">Entity:</D:Label>
                                            </td>
                                            <td class="dxtlEditFormEditCell_Glass" style="width: 50%;" colspan="3">
                                                <dxwtl:ASPxComboBox ID="cmbItem" ClientInstanceName="cmbItem" runat="server" style="width: 100%" OnCallback="cmbItem_Callback" IncrementalFilteringMode="Contains" EnableCallbackMode="True" CallbackPageSize="100">
                                                    <ClientSideEvents EndCallback="OnItemEndCallback"/>
                                                </dxwtl:ASPxComboBox>    
                                            </td>                                                                      
                                        </tr>                                        
                                    </table>                                                        
                                </dxwtl:ContentControl>
                            </ContentCollection>
                        </dxwtl:TabPage>
                    </TabPages>
                </dxwtl:ASPxPageControl>
                <div style="text-align: right; padding-top: 8px">
                    <dxwtl:ASPxTreeListTemplateReplacement runat="server" ReplacementType="UpdateButton"  />
                    <dxwtl:ASPxTreeListTemplateReplacement runat="server" ReplacementType="CancelButton" />
                </div>
            </EditForm>
        </Templates>
    </dxwtl:ASPxTreeList>               
    <div id="divDeletePages" style="margin-top: 4px;">
        <D:Button runat="server" ID="btDeleteItemsBottom" style="float:right;" Text="Delete" />        
    </div> 
</div>