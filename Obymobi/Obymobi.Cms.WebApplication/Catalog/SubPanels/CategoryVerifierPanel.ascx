﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Catalog.SubPanels.CategoryVerifierPanel" Codebehind="CategoryVerifierPanel.ascx.cs" %>
<%@ Reference VirtualPath="~/Catalog/SubPanels/ProductVerifierPanel.ascx" %>
<table class="dataformV2 categoryVerifier">
	<tr>
		<td class="label">
			<D:Label runat="server" ID="lblName">Naam</D:Label>
		</td>
		<td class="control">
			<strong><D:HyperLink runat="server" ID="lblNameValue"></D:HyperLink></strong>
		</td>
		<td class="label">
			<D:Label runat="server" ID="lblSortOrder">Sorteervolgorde</D:Label>
		</td>
		<td class="control">
			<D:Label runat="server" ID="lblSortOrderValue"></D:Label>
		</td>
	</tr>
	<tr>
		<td class="label">
			<D:Label runat="server" ID="lblHierarchy">Hierarchie</D:Label>
		</td>
		<td class="control">
			<D:Label runat="server" ID="lblHierachyValue"></D:Label>
		</td>
		<td class="label">
			<D:Label runat="server" ID="lblGenericCategoryName">Basiscategorie</D:Label>
		</td>
		<td class="control">
			<D:HyperLink runat="server" ID="lblGenericCategoryNameValue"></D:HyperLink>
		</td>
	</tr>
	<tr>
		<td class="label">
			<D:Label runat="server" ID="lblImages">Afbeeldingen</D:Label>
		</td>
		<td class="control">
			<D:HyperLink runat="server" ID="lblImagesButton">Knop</D:HyperLink>
		</td>
		<td class="label">
			<D:Label runat="server" ID="lblAdvertisementTags">Advertentie tags</D:Label>
		</td>
		<td class="control">
			<D:Label runat="server" ID="lblAdvertisementTagsValue"></D:Label>
		</td>
	</tr>
	<D:PlaceHolder runat="server" ID="plhSuggestionsAndAlterations">
	<tr>
		<td class="label">
			<D:Label runat="server" ID="lblSuggestions">Suggesties</D:Label>
		</td>
		<td class="control">
			<D:PlaceHolder runat="server" ID="plhSuggestions"></D:PlaceHolder>
		</td>
		<td class="label">
			<D:Label runat="server" ID="lblAlterations">Productsamenstellingen</D:Label>
		</td>
		<td class="control">
			<D:PlaceHolder runat="server" ID="plhAlterations"></D:PlaceHolder>
		</td>
	</tr>	
	</D:PlaceHolder>
	<D:PlaceHolder runat="server" ID="plhLayout">
	<tr>
		<td class="label">
			<D:Label runat="server" ID="Label1">Layout</D:Label>
		</td>
		<td class="control" colspan="3">
			<D:PlaceHolder runat="server" ID="plhLayoutHtml"></D:PlaceHolder>
		</td>
	</tr>		
	</D:PlaceHolder>	
	<tr>
		<td colspan="4" class="products">
			<D:PlaceHolder runat="server" ID="plhProducts"></D:PlaceHolder>
		</td>
	</tr>	
</table>
