﻿using System;
using Obymobi.Data.EntityClasses;
using Dionysos.Web.UI;
using Dionysos.Web.UI.DevExControls;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Web;
using Obymobi.Enums;

namespace Obymobi.ObymobiCms.Catalog.Subpanels
{
    public partial class AlterationitemCollection : SubPanelLLBLOverviewDataSourceCollection
    {
        #region Fields



        #endregion

        #region Methods

        private void LoadUserControls()
        {

        }

        private void HookUpEvents()
        {
            this.btnAddExisting.Click += btnAddExisting_Click;
            this.btnAddNew.Click += btnAddNew_Click;
        }

        private void AddNewAlteration()
        {
            int alterationId;
            decimal priceIn;
            if (!QueryStringHelper.GetInt("id", out alterationId))
            {
                // Show message
                string errorMessage = ((PageDefault)this.Page).Translate("AlterationitemCollection.ExistingAlterationOnly", "Er kunnen alleen opties toegevoegd worden voor een bestaande alteration");
                ((PageDefault)this.Page).AddInformator(InformatorType.Warning, errorMessage);
            }
            else if (string.IsNullOrEmpty(this.tbAlterationName.Text) || this.tbAlterationName.Text.Equals("Name"))
            {
                // Show message
                string errorMessage = ((PageDefault)this.Page).Translate("AlterationitemCollection.NoAlterationName", "Vul een naam voor de optie in");
                ((PageDefault)this.Page).AddInformator(InformatorType.Warning, errorMessage);
            }
            else if (!decimal.TryParse(this.tbPriceIn.Text, out priceIn))
            {
                // Show message
                string errorMessage = ((PageDefault)this.Page).Translate("AlterationitemCollection.NoAlterationPrice", "Vul een valide price voor de alteration in");
                ((PageDefault)this.Page).AddInformator(InformatorType.Warning, errorMessage);
            }
            else
            {
                // Create the new alterationoption
                AlterationoptionEntity alterationoptionEntity = new AlterationoptionEntity();
                alterationoptionEntity.ValidatorCreateOrUpdateDefaultCustomText = true;
                alterationoptionEntity.Name = this.tbAlterationName.Text;
                alterationoptionEntity.PriceIn = priceIn;
                alterationoptionEntity.CompanyId = this.AlterationEntity.BrandId.HasValue ? (int?)null : CmsSessionHelper.CurrentCompanyId;
                alterationoptionEntity.BrandId = this.AlterationEntity.BrandId;
                alterationoptionEntity.Save();

                // Create the relation between the alteration and the option
                AlterationitemEntity alterationitemEntity = new AlterationitemEntity();
                alterationitemEntity.AlterationId = alterationId;
                alterationitemEntity.AlterationoptionId = alterationoptionEntity.AlterationoptionId;
                alterationitemEntity.SortOrder = int.Parse(this.tbSortOrderNew.Text);
                if (alterationitemEntity.Save())
                {
                    this.tbPriceIn.Text = "0.00";
                    this.tbAlterationName.Text = string.Empty;
                    this.tbAlterationName.DefaultValue = "Name";

                    string message = ((PageDefault)this.Page).Translate("AlterationitemCollection.AlterationItemCreated", "De nieuwe alteration optie is succesvol aangemaakt en gekoppeld!");
                    ((PageDefault)this.Page).AddInformator(InformatorType.Information, message);
                }
            }            
        }

        private void AddExistingAlteration()
        {
            int alterationId;
            if (!QueryStringHelper.GetInt("id", out alterationId))
            {
                // Show message
                string errorMessage = ((PageDefault)this.Page).Translate("AlterationitemCollection.ExistingAlterationOnly", "Er kunnen alleen opties toegevoegd worden voor een bestaande alteration");
                ((PageDefault)this.Page).AddInformator(InformatorType.Warning, errorMessage);
            }
            else if (this.ddlAlterationoptionId.ValidId < 0)
            {
                // Show message
                string errorMessage = ((PageDefault)this.Page).Translate("AlterationitemCollection.NoAlterationOptionSelected", "Er is geen optie geselecteerd");
                ((PageDefault)this.Page).AddInformator(InformatorType.Warning, errorMessage);
            }
            else
            {
                // Create the relation between the alteration and the option
                AlterationitemEntity alterationitemEntity = new AlterationitemEntity();
                alterationitemEntity.AlterationId = alterationId;
                alterationitemEntity.AlterationoptionId = this.ddlAlterationoptionId.ValidId;
                alterationitemEntity.SortOrder = int.Parse(this.tbSortOrderExisting.Text);
                if (alterationitemEntity.Save())
                {
                    this.ddlAlterationoptionId.SelectedIndex = -1;

                    string message = ((PageDefault)this.Page).Translate("AlterationitemCollection.AlterationItemLinked", "De alteration optie is succesvol gekoppeld!");
                    ((PageDefault)this.Page).AddInformator(InformatorType.Information, message);
                }
            }
        }

        private void SetGui()
        {
            int alterationVersion = CmsSessionHelper.AlterationDialogMode == AlterationDialogMode.v3 ? 2 : 1;

            PredicateExpression filter;
            if (this.AlterationEntity != null && this.AlterationEntity.BrandId.HasValue)
            {
                filter = new PredicateExpression(AlterationoptionFields.BrandId == this.AlterationEntity.BrandId.Value);
            }
            else
            {
                filter = new PredicateExpression(AlterationoptionFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            }
            filter.Add(AlterationoptionFields.Version == alterationVersion);

            SortExpression sort = new SortExpression(new SortClause(AlterationoptionFields.Name, SortOperator.Ascending));

            IncludeFieldsList fields = new IncludeFieldsList();
            fields.Add(AlterationoptionFields.Name);
            fields.Add(AlterationoptionFields.FriendlyName);
            fields.Add(AlterationoptionFields.PriceIn);

            AlterationoptionCollection alterationoptionCollection = new AlterationoptionCollection();
            alterationoptionCollection.GetMulti(filter, 0, sort, null, null, fields, 0, 0);

            foreach (AlterationoptionEntity alterationoption in alterationoptionCollection)
            {
                this.ddlAlterationoptionId.Items.Add(alterationoption.NameAndPriceIn, alterationoption.AlterationoptionId);
            }

            int sortOrder = 1000;
            if (this.AlterationEntity != null && this.AlterationEntity.AlterationitemCollection.Count > 0)
            {
                // Calculate the new sort order
                sortOrder = (this.AlterationEntity.AlterationitemCollection.LastSortOrder + 100);
            }
            this.tbSortOrderExisting.Text = sortOrder.ToString();
            this.tbSortOrderNew.Text = sortOrder.ToString();
        }

        #endregion

        #region Events

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            this.EntityName = "Alterationitem";
            base.OnInit(e);            
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.HookUpEvents();
            if (!this.IsPostBack)
            {
                this.SetGui();
            }                       
        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            this.AddNewAlteration();

            // GK Refresh page because of the DevEx bug that all tabs get cleared
            QueryStringHelper qs = new QueryStringHelper();
            qs.AddItem(PageControl.queryStringElementToActiveTab, "Options");
            this.Response.Redirect(qs.MergeQuerystringWithRawUrl());
        }

        private void btnAddExisting_Click(object sender, EventArgs e)
        {
            this.AddExistingAlteration();

            // GK Refresh page because of the DevEx bug that all tabs get cleared
            QueryStringHelper qs = new QueryStringHelper();
            qs.AddItem(PageControl.queryStringElementToActiveTab, "Options");
            this.Response.Redirect(qs.MergeQuerystringWithRawUrl());
        }

        #endregion

        #region Properties

        public AlterationEntity AlterationEntity { get; set; }

        #endregion
    }
}