﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CategoryConfigurationAppLess.ascx.cs" Inherits="Obymobi.Cms.WebApplication.Catalog.SubPanels.CategoryConfigurationAppLess" %>

<div id="mainTabContent" runat="server">
    <Controls>
        <D:Panel ID="pnlSettings" runat="server" GroupingText="Display settings">
            <table class="dataformV2">
                <tr>
                    <td class="label">
                        <D:LabelEntityFieldInfo runat="server" ID="lblCatagoryViewType">Category view type</D:LabelEntityFieldInfo>
                    </td>
                    <td class="control">
                        <X:ComboBoxInt runat="server" ID="ddlCatagoryViewType"></X:ComboBoxInt>
                    </td>
                    <td class="label"></td>
                    <td class="control"></td>
                </tr>
                <tr>
                    <td class="label">
                        <D:LabelEntityFieldInfo runat="server" id="lbShowName">Show category title</D:LabelEntityFieldInfo>
                    </td>
                    <td class="control">
                        <D:CheckBox runat ="server" ID="cbShowName" />
                    </td>
                    <td class="label"></td>
                    <td class="control"></td>
                </tr>
            </table>
        </D:Panel>
    </Controls>
</div>
