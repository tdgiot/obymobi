﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Utils;
using DevExpress.Web;
using DevExpress.Web;
using DevExpress.Web.Internal;
using DevExpress.Web.ASPxTreeList;
using DevExpress.Web.ASPxTreeList.Internal;
using Dionysos;
using Dionysos.Web.UI;
using Dionysos.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic.Cms;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Logic.Model;
using Obymobi.Logic.Comet;

namespace Obymobi.ObymobiCms.Catalog.SubPanels
{
    public partial class CategoriesPanel : SubPanelCustomDataSource
    {
        #region  Fields

        private const string TRANSLATION_KEY_PREFIX = "Obymobi.ObymobiCms.Catalog.SubPanels.CategoriesPanel.";
        private MenuDataSource menuDataSource;
        private int Version = 1;

        #endregion

        #region Properties

        private string EndDragNodeMethod
        {
            get
            {
                return @"  function(s, e) {                                                                             
                                e.cancel = true;
		                        var key = s.GetNodeKeyByRow(e.targetElement);
		                        " + this.TreelistId + @".PerformCustomCallback(e.nodeKey + ':' + key);
                            }";
            }
        }

        private string NodeDblClickMethod
        {
            get { return @" function(s, e) {
	                            " + this.TreelistId + @".StartEdit(e.nodeKey);
		                        e.cancel = true;	                                                                    
                            }"; }
        }

        #endregion

        #region Methods

        private void RetrieveSelectedNodes(ref List<TreeListNode> selectedNodes, TreeListNode parent, bool includeChildren = true)
        {
            TreeListNodeCollection nodes = parent == null ? this.tlCategories.Nodes : parent.ChildNodes;
            foreach (TreeListNode node in nodes)
            {
                if (includeChildren)
                {
                    if (node.Selected || node.ParentNode.Selected)
                    {
                        if (node.ParentNode.Selected)
                        {
                            node.Selected = true;
                        }
                        selectedNodes.Add(node);
                    }
                }
                else
                {
                    if (node.Selected && !node.ParentNode.Selected)
                    {
                        selectedNodes.Add(node);
                    }
                }

                if (node.HasChildren)
                {
                    this.RetrieveSelectedNodes(ref selectedNodes, node, includeChildren);
                }
            }
        }

        private void CreateTreeList()
        {
            this.tlCategories.SettingsEditing.Mode = TreeListEditMode.Inline;
            this.tlCategories.SettingsPopupEditForm.Width = 600;
            this.tlCategories.SettingsBehavior.ColumnResizeMode = ColumnResizeMode.Disabled;
            this.tlCategories.SettingsBehavior.AllowSort = false;
            this.tlCategories.SettingsBehavior.AllowDragDrop = false;
            this.tlCategories.ClientInstanceName = this.TreelistId;
            this.tlCategories.AutoGenerateColumns = false;
            this.tlCategories.SettingsEditing.AllowNodeDragDrop = true;
            this.tlCategories.SettingsEditing.AllowRecursiveDelete = true;
            this.tlCategories.Settings.GridLines = GridLines.Both;
            this.tlCategories.ClientSideEvents.EndDragNode = this.EndDragNodeMethod;
            this.tlCategories.ClientSideEvents.NodeDblClick = this.NodeDblClickMethod;
            this.tlCategories.CustomErrorText += this.tlCategoriesStructure_CustomErrorText;

            this.tlCategories.KeyFieldName = "ItemId";
            this.tlCategories.ParentFieldName = "ParentItemId";

            TreeListTextColumn nameColumn = new TreeListTextColumn();
            nameColumn.Caption = "Name";
            nameColumn.Width = Unit.Pixel(167);
            nameColumn.VisibleIndex = 0;
            nameColumn.FieldName = "Name";
            nameColumn.CellStyle.Wrap = DefaultBoolean.True;
            this.tlCategories.Columns.Add(nameColumn);

            TreeListComboBoxColumn itemTypeColumn = new TreeListComboBoxColumn();
            itemTypeColumn.Caption = "Type";
            itemTypeColumn.Width = Unit.Pixel(70);
            itemTypeColumn.VisibleIndex = 1;
            itemTypeColumn.FieldName = "ItemType";
            itemTypeColumn.CellStyle.Wrap = DefaultBoolean.True;
            this.tlCategories.Columns.Add(itemTypeColumn);

            TreeListCommandColumn editCommandColumn = new TreeListCommandColumn();
            editCommandColumn.VisibleIndex = 4;
            editCommandColumn.Width = Unit.Pixel(60);
            editCommandColumn.ShowNewButtonInHeader = true;
            editCommandColumn.EditButton.Visible = true;
            editCommandColumn.NewButton.Visible = true;
            editCommandColumn.DeleteButton.Visible = CmsSessionHelper.CurrentRole >= Role.Reseller;
            editCommandColumn.ButtonType = ButtonType.Image;
            editCommandColumn.EditButton.Image.Url = this.ResolveUrl("~/images/icons/pencil.png");
            editCommandColumn.DeleteButton.Image.Url = this.ResolveUrl("~/images/icons/delete.png");
            editCommandColumn.CancelButton.Image.Url = this.ResolveUrl("~/images/icons/cross_grey.png");
            editCommandColumn.NewButton.Image.Url = this.ResolveUrl("~/images/icons/add2.png");
            editCommandColumn.UpdateButton.Image.Url = this.ResolveUrl("~/images/icons/disk.png");
            this.tlCategories.Columns.Add(editCommandColumn);

            TreeListHyperLinkColumn editPageV2Column = new TreeListHyperLinkColumn();
            editPageV2Column.Name = "EditPageV2";
            editPageV2Column.VisibleIndex = 5;
            editPageV2Column.Caption = "&nbsp;";
            editPageV2Column.Width = Unit.Pixel(15);
            editPageV2Column.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            editPageV2Column.PropertiesHyperLink.ImageUrl = "~/images/icons/page_gear.png";
            editPageV2Column.EditCellStyle.CssClass = "hidden";
            editPageV2Column.FieldName = "ItemId";
            this.tlCategories.Columns.Add(editPageV2Column);

            TreeListHyperLinkColumn editPageColumn = new TreeListHyperLinkColumn();
            editPageColumn.Name = "EditPage";
            editPageColumn.VisibleIndex = 5;
            editPageColumn.Caption = "&nbsp;";
            editPageColumn.Width = Unit.Pixel(15);
            editPageColumn.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            editPageColumn.PropertiesHyperLink.ImageUrl = "~/images/icons/page_edit.png";
            editPageColumn.EditCellStyle.CssClass = "hidden";
            editPageColumn.FieldName = "ItemId";
            this.tlCategories.Columns.Add(editPageColumn);

            this.tlCategories.CustomCallback += this.tlCategories_CustomCallback;
            this.tlCategories.NodeValidating += this.tlSiteStructure_NodeValidating;
            this.tlCategories.HtmlDataCellPrepared += this.tlCategories_HtmlDataCellPrepared;
            this.tlCategories.HtmlRowPrepared += this.tlCategories_HtmlRowPrepared;
            this.tlCategories.CommandColumnButtonInitialize += this.tlCategories_CommandColumnButtonInitialize;
            this.tlCategories.CellEditorInitialize += this.tlCategories_CellEditorInitialize;

            this.InitDataSource();

            this.tlCategories.Width = Unit.Pixel(736);
        }

        private void tlCategories_CustomCallback(object sender, TreeListCustomCallbackEventArgs e)
        {
            string[] nodes = e.Argument.Split(new char[] { ':' });

            string draggedItemId = nodes[0];
            string draggedUponItemId = nodes[1];

            this.menuDataSource.MenuManager.DraggedToSort(draggedItemId, draggedUponItemId);

            this.tlCategories.DataBind();            
        }

        private void tlCategories_HtmlRowPrepared(object sender, TreeListHtmlRowEventArgs e)
        {
            if (!e.NodeKey.IsNullOrWhiteSpace())
            {
                IMenuItem menuItem = this.menuDataSource.DataSource.SingleOrDefault(item => item.ItemId == e.NodeKey);
                if (menuItem != null)
                {
                    if (menuItem.VisibilityType == VisibilityType.Never)
                    {
                        e.Row.ForeColor = Color.Gray;
                        e.Row.Font.Strikeout = true;
                    }
                    else if (menuItem.IsLinkedToBrand)
                    {
                        e.Row.ForeColor = Color.Gray;
                    }
                }
            }
        }

        private void tlCategories_CommandColumnButtonInitialize(object sender, TreeListCommandColumnButtonEventArgs e)
        {
            if (e.NodeKey != null && e.ButtonType == TreeListCommandColumnButtonType.New)
            {
                string[] keyParts = e.NodeKey.Split(new[] {"-"}, StringSplitOptions.RemoveEmptyEntries);
                if (keyParts.Length > 0)
                {
                    string itemType = keyParts[0];
                    if (itemType == MenuItemType.Product)
                    {
                        e.Visible = DefaultBoolean.False;
                    }
                }
            }
        }

        private void tlCategories_HtmlDataCellPrepared(object sender, TreeListHtmlDataCellEventArgs e)
        {
            if (e.NodeKey == null)
            {
                return;
            }

            string[] keyParts = e.NodeKey.Split(new[] { "-" }, StringSplitOptions.RemoveEmptyEntries);
            string itemType = keyParts[0];
            string itemId = keyParts[1];

            bool editPage = e.Column.Name.Equals("EditPage", StringComparison.InvariantCultureIgnoreCase);
            bool editPageV2 = e.Column.Name.Equals("EditPageV2", StringComparison.InvariantCultureIgnoreCase);

            if (editPage || editPageV2)
            {
                string navigateUrl = string.Empty;

                if (keyParts.Length > 1)
                {
                    if (itemType == MenuItemType.Category)
                    {
                        navigateUrl = string.Format("~/Catalog/Category.aspx?id={0}", itemId);

                        if (editPageV2)
                        {
                            e.Cell.Text = string.Empty;
                        }                        
                    }
                    else if (itemType == MenuItemType.Product)
                    {
                        navigateUrl = editPage ? string.Format("~/Catalog/Product.aspx?id={0}", itemId) : string.Format("~/Catalog/ProductV2.aspx?id={0}&Version={1}", itemId, Version);
                    }
                }

                foreach (Control item in e.Cell.Controls)
                {
                    if (item is HyperLinkDisplayControl link)
                    {
                        link.NavigateUrl = navigateUrl;
                        break;
                    }
                }
            }

            if (e.Column.Caption.Equals("Type", StringComparison.InvariantCultureIgnoreCase))
            {
                e.Cell.Style["background"] = itemType == MenuItemType.Category ? "#d7f4f7" : string.Empty;
            }
        }

        private void tlCategories_CellEditorInitialize(object sender, TreeListColumnEditorEventArgs e)
        {
            if (e.Column.FieldName == "ItemType")
            {
                TreeListNode parentNode = null;
                if (e.NodeKey != null)
                {
                    TreeListNode currentNode = this.tlCategories.FindNodeByKeyValue(e.NodeKey);
                    if (currentNode != null)
                    {
                        parentNode = currentNode.ParentNode;
                    }
                }
                else
                {
                    string parentItemId = this.tlCategories.NewNodeParentKey;
                    parentNode = this.tlCategories.FindNodeByKeyValue(parentItemId);
                }

                ASPxComboBox cbType = e.Editor as ASPxComboBox;
                if (parentNode is TreeListRootNode)
                {
                    // Root node
                    this.DataBindEnumValuesToCombBox(cbType, MenuItemType.Category);
                    cbType.SelectedItem = cbType.Items.FindByValue(MenuItemType.Category);
                }
                else if (parentNode != null && parentNode.DataItem is CategoryEntity)
                {
                    CategoryEntity parentCategory = (CategoryEntity)parentNode.DataItem;
                    if (parentCategory.ProductCategoryCollection.Count > 0)
                    {
                        // Products as siblings
                        this.DataBindEnumValuesToCombBox(cbType, MenuItemType.Product);
                        cbType.SelectedItem = cbType.Items.FindByValue(MenuItemType.Product);
                    }
                    else if (parentCategory.ChildCategoryCollection.Count > 0)
                    {
                        // Categories as sibling
                        this.DataBindEnumValuesToCombBox(cbType, MenuItemType.Category);
                        cbType.SelectedItem = cbType.Items.FindByValue(MenuItemType.Category);
                    }
                    else
                    {
                        // No children today
                        this.DataBindEnumValuesToCombBox(cbType, MenuItemType.Category, MenuItemType.Product);
                        cbType.SelectedItem = cbType.Items.FindByValue(MenuItemType.Category);
                    }
                }
            }
        }

        private void DataBindEnumValuesToCombBox(ASPxComboBox combobox, params object[] values)
        {
            combobox.Items.Clear();
            foreach (object value in values)
            {
                combobox.Items.Add(value.ToString(), value);
            }
            combobox.DataBind();
        }

        private void InitDataSource()
        {
            if (this.DataSource != null)
            {
                this.menuDataSource = new MenuDataSource(this.DataSourceAsMenuEntity.MenuId);
                this.tlCategories.DataSource = this.menuDataSource;
            }
        }

        public void Refresh()
        {
            this.InitDataSource();
            this.tlCategories.DataBind();
        }

        private void ExpandCategoryNodes(TreeListNodeCollection nodes)
        {
            foreach (TreeListNode node in nodes)
            {
                if (node is TreeListRootNode)
                {
                    node.Expanded = true;
                }
                else if (node != null && node.DataItem is CategoryEntity)
                {
                    CategoryEntity category = (CategoryEntity)node.DataItem;
                    if (category.ChildCategoryCollection.Count > 0)
                    {
                        node.Expanded = true;
                    }
                }

                if (node.HasChildren)
                {
                    this.ExpandCategoryNodes(node.ChildNodes);
                }
            }
        }

        public void RefreshDataSource(IEntity datasource)
        {
            this.DataSource = datasource;
            if (this.DataSource != null && !((IEntity)this.DataSource).IsNew)
            {
                this.Refresh();
            }
        }

        private void HideProductmenuItems()
        {
            List<TreeListNode> selectedNodes = new List<TreeListNode>();
            this.RetrieveSelectedNodes(ref selectedNodes, null, false);

            if (selectedNodes.Count <= 0)
            {
                this.PageAsPageDefault.MultiValidatorDefault.AddError(this.PageAsPageDefault.Translate(CategoriesPanel.TRANSLATION_KEY_PREFIX + "Error.NoItemSelectedToHide", "No item has been selected to hide.", true));
                this.PageAsPageDefault.Validate();
            }
            else
            {
                this.HideItems(selectedNodes);
                this.tlCategories.DataBind();

                if (selectedNodes.Count == 1)
                {
                    this.PageAsPageDefault.AddInformatorInfo(this.PageAsPageDefault.Translate(CategoriesPanel.TRANSLATION_KEY_PREFIX + "Success.ItemHidden", "The selected item is hidden.", true));
                }
                else
                {
                    this.PageAsPageDefault.AddInformatorInfo(this.PageAsPageDefault.Translate(CategoriesPanel.TRANSLATION_KEY_PREFIX + "Success.ItemsHidden", "The selected items are hidden.", true));
                }

                this.PageAsPageDefault.Validate();
            }
        }

        private void HideItems(List<TreeListNode> nodes)
        {
            if (nodes.Count <= 0)
                return;

            int categoryIdAddition = 100000000;

            List<ProductmenuItem> productmenuItems = new List<ProductmenuItem>();
            foreach (TreeListNode node in nodes)
            {
                if (node.DataItem is CategoryEntity)
                {
                    CategoryEntity category = (CategoryEntity)node.DataItem;
                    if (category.VisibilityType != VisibilityType.Never)
                    {
                        category.VisibilityType = VisibilityType.Never;
                        category.Save();

                        int parentProductmenuItemId = category.ParentCategoryId + categoryIdAddition ?? -1;
                        productmenuItems.Add(this.CreateProductmenuItem(category.CategoryId + categoryIdAddition, parentProductmenuItemId, ProductmenuItemType.Category));
                    }                   
                }
                else if (node.DataItem is ProductCategoryEntity)
                {
                    ProductCategoryEntity productCategoryEntity = (ProductCategoryEntity)node.DataItem;

                    ProductEntity product = productCategoryEntity.ProductEntity;
                    if (product.VisibilityType != VisibilityType.Never)
                    {
                        product.VisibilityType = VisibilityType.Never;
                        product.Save();

                        productmenuItems.Add(this.CreateProductmenuItem(product.ProductId, productCategoryEntity.CategoryId + categoryIdAddition, ProductmenuItemType.Product));
                    }
                }

                node.Selected = false;
            }

            if (productmenuItems.Count > 0)
            {
                MenuUpdated menuUpdated = new MenuUpdated();
                menuUpdated.MenuUpdatedAction = (int)MenuUpdatedAction.Hide;
                menuUpdated.ProductmenuItems = productmenuItems.ToArray();

                CometHelper.MenuUpdated(CmsSessionHelper.CurrentCompanyId, menuUpdated);
            }            
        }

        private ProductmenuItem CreateProductmenuItem(int id, int parentProductmenuItemId, ProductmenuItemType type)
        {
            ProductmenuItem productmenuItem = new ProductmenuItem(id, parentProductmenuItemId);
            productmenuItem.VisibilityType = (int)VisibilityType.Never;
            productmenuItem.ItemType = (int)type;

            return productmenuItem;
        }

        private void DeleteSelected()
        {
            List<TreeListNode> selectedNodes = new List<TreeListNode>();
            this.RetrieveSelectedNodes(ref selectedNodes, null);

            if (selectedNodes.Count <= 0)
            {
                this.PageAsPageDefault.MultiValidatorDefault.AddError(this.PageAsPageDefault.Translate(CategoriesPanel.TRANSLATION_KEY_PREFIX + "Error.NoItemSelected", "No item has been selected to be deleted.", true));
                this.PageAsPageDefault.Validate();
            }
            else if (CmsSessionHelper.CurrentRole < Role.Reseller)
            {
                this.PageAsPageDefault.MultiValidatorDefault.AddError("Your role does not allow you to delete any items.");
                this.PageAsPageDefault.Validate();
            }
            else
            {
                this.DeleteItems(selectedNodes);
                this.tlCategories.DataBind();

                if (selectedNodes.Count == 1)
                {
                    this.PageAsPageDefault.AddInformatorInfo(this.PageAsPageDefault.Translate(CategoriesPanel.TRANSLATION_KEY_PREFIX + "Success.ItemsDeleted", "The selected item has been deleted.", true));
                }
                else
                {
                    this.PageAsPageDefault.AddInformatorInfo(this.PageAsPageDefault.Translate(CategoriesPanel.TRANSLATION_KEY_PREFIX + "Success.ItemsDeleted", "The selected items have been deleted.", true));
                }
                this.PageAsPageDefault.Validate();
            }
        }

        private void DeleteItems(List<TreeListNode> selectedNodes)
        {
            foreach (TreeListNode node in selectedNodes)
            {
                this.menuDataSource.DeleteParameters.Clear();
                this.menuDataSource.DeleteParameters.Add("ItemId", node.Key);
                this.menuDataSource.Delete();
            }

            this.DataSource = new MenuEntity(this.DataSourceAsMenuEntity.MenuId);
            this.InitDataSource();
        }

        #endregion

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            this.CreateTreeList();

            this.hlExpandAll.NavigateUrl = string.Format("javascript:{0}.ExpandAll()", this.TreelistId);
            this.hlCollapseAll.NavigateUrl = string.Format("javascript:{0}.CollapseAll()", this.TreelistId);
        }

        private void tlCategoriesStructure_CustomErrorText(object sender, TreeListCustomErrorTextEventArgs e)
        {
            if (e.Exception.InnerException != null)
            {
                ObymobiException exception = e.Exception.InnerException as ObymobiException;
                if (exception != null)
                {
                    e.ErrorText = exception.AdditionalMessage;
                }
                else
                {
                    e.ErrorText = e.Exception.InnerException.Message;
                }
            }
        }

        private void tlSiteStructure_NodeValidating(object sender, TreeListNodeValidationEventArgs e)
        {
            if (e.HasErrors)
            {
                e.NodeError = this.PageAsPageDefault.Translate(CategoriesPanel.TRANSLATION_KEY_PREFIX + "PleaseCorrectErrors", "Please correct the errors.", true);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Version = CmsSessionHelper.AlterationDialogMode == AlterationDialogMode.v3 ? 2 : 1;

            this.tlCategories.DataBind();
            this.ExpandCategoryNodes(this.tlCategories.Nodes);

            string deleteMessage = this.PageAsPageDefault.Translate(CategoriesPanel.TRANSLATION_KEY_PREFIX + "Warning.PreSubmit", "Are you sure you want to delete these items? Child items will be automatically removed if the parent item is selected.", true);
            string hideMessage = this.PageAsPageDefault.Translate(CategoriesPanel.TRANSLATION_KEY_PREFIX + "Warning.HidePreSubmit", "Are you sure you want to hide these items?", true);

            this.btDeleteItemsTop.PreSubmitWarning = deleteMessage;
            this.btDeleteItemsBottom.PreSubmitWarning = deleteMessage;

            this.btHideTop.PreSubmitWarning = hideMessage;
            this.btHideBottom.PreSubmitWarning = hideMessage;

            this.btDeleteItemsTop.Click += this.btDeleteSelected_Click;
            this.btDeleteItemsBottom.Click += this.btDeleteSelected_Click;

            this.btHideTop.Click += this.btHide_Click;
            this.btHideBottom.Click += this.btHide_Click;
        }

        private void btHide_Click(object sender, EventArgs e)
        {
            this.HideProductmenuItems();
        }

        private void btDeleteSelected_Click(object sender, EventArgs e)
        {
            this.DeleteSelected();
        }

        private string TreelistId
        {
            get { return string.Format("{0}_treelist", this.ID); }
        }

        private MenuEntity DataSourceAsMenuEntity
        {
            get { return (MenuEntity)this.DataSource; }
        }

        protected void cmbItem_Callback(object sender, CallbackEventArgsBase e)
        {
        }

        #endregion
    }
}