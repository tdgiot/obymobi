﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dionysos;
using Dionysos.Web.UI.DevExControls;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using DevExpress.Web;
using Obymobi.Cms.Logic.HelperClasses;

namespace Obymobi.ObymobiCms.Catalog.SubPanels
{
    public partial class ProductCategorySuggestionPanel : System.Web.UI.UserControl, Dionysos.Interfaces.ISaveableControl
    {
        private ProductCategoryEntity productCategory;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.productCategory != null)
            {
                ProductCategoryCollection productCategories = ProductHelper.GetProductCategoriesAvailableAsSuggestionForCompany(CmsSessionHelper.CurrentCompanyId, true, this.productCategory.CategoryEntity.MenuId);
                EntityView<ProductCategorySuggestionEntity> csView = this.productCategory.ProductCategorySuggestionCollection.DefaultView;

                for (int i = 1; i <= 4; i++)
                {
                    PlaceHolder plh = (PlaceHolder)this.FindControl(string.Format("plhProductSuggestion{0}", i));
                    if (plh != null)
                    {
                        ComboBoxInt combobox = new ComboBoxInt();
                        combobox.ID = string.Format("ddlProductSuggestion-{0}-{1}", this.productCategory.ProductCategoryId, i);
                        combobox.IncrementalFilteringMode = IncrementalFilteringMode.StartsWith;
                        combobox.TextField = "ProductCategoryName";
                        combobox.ValueField = "ProductCategoryId";
                        combobox.DataSource = productCategories;
                        combobox.DataBind();

                        if (!this.IsPostBack)
                        {
                            csView.Filter = new PredicateExpression(ProductSuggestionFields.SortOrder == i);
                            if (csView.Count == 1)
                            {
                                combobox.Value = csView[0].SuggestedProductCategoryId;

                                CheckBox cbCheckout = (CheckBox)this.FindControl(string.Format("cbSuggestionCheckout{0}", i));
                                if (cbCheckout != null)
                                {
                                    cbCheckout.Checked = csView[0].Checkout;
                                }
                            }
                        }
                        plh.Controls.Add(combobox);
                    }
                }

                // Get suggestions from parent categories                 
                CategoryCollection parentCategories = CategoryHelper.GetCategoriesWithPrefetchedParents(new List<int>() { this.productCategory.CategoryId });
                CategorySuggestionCollection parentCategorySuggestions = this.GetParentCategorySuggestions(parentCategories);
                if (parentCategorySuggestions.Count > 0)
                {
                    System.Text.StringBuilder builder = new System.Text.StringBuilder();
                    builder.AppendFormatLine("<table class=\"dataformV2\">");
                    builder.AppendFormatLine("<tr>");
                    builder.AppendFormatLine("   <td class='label'>");
                    builder.AppendFormatLine("   </td>");
                    builder.AppendFormatLine("   <td class='control'>");
                    builder.AppendFormatLine("       Suggestion");
                    builder.AppendFormatLine("   </td>");
                    builder.AppendFormatLine("   <td class='control'>");
                    builder.AppendFormatLine("      Category");
                    builder.AppendFormatLine("   </td>");
                    builder.AppendFormatLine("   <td class='label'>");
                    builder.AppendFormatLine("   </td>");
                    builder.AppendFormatLine("</tr>");

                    // Get the suggestions from a parent category
                    for (int i = 0; i < parentCategorySuggestions.Count; i++)
                    {
                        CategorySuggestionEntity categorySuggestion = parentCategorySuggestions[i];

                        string suggestionLink = string.Format("<a href=\"Product.aspx?id={0}\">{1}</a>", categorySuggestion.ProductCategoryEntity.ProductId, categorySuggestion.ProductCategoryEntity.ProductName);
                        string categoryLink = string.Format("<a href=\"Category.aspx?id={0}\">{1}</a>", categorySuggestion.CategoryId, categorySuggestion.CategoryEntity.Name);

                        // Create row for an suggestion
                        builder.AppendFormatLine("<tr>");
                        builder.AppendFormatLine("   <td class='label' style=\"padding-top:5px\">");
                        builder.AppendFormatLine("       {0}. ", (i + 1));
                        builder.AppendFormatLine("   </td>");
                        builder.AppendFormatLine("   <td class='control'>");
                        builder.AppendFormatLine("       {0}", suggestionLink);
                        builder.AppendFormatLine("   </td>");
                        builder.AppendFormatLine("   <td class='control'>");
                        builder.AppendFormatLine("      {0}", categoryLink);
                        builder.AppendFormatLine("   </td>");
                        builder.AppendFormatLine("   <td class='label'>");
                        builder.AppendFormatLine("   </td>");
                        builder.AppendFormatLine("</tr>");
                    }
                    builder.AppendFormatLine("</table>");

                    this.plhInheritedSuggestions.AddHtml(builder.ToString());
                }
            }
        }

        public void SetGui()
        {
            this.cbInheritSuggestions.Checked = this.productCategory.InheritSuggestions;
        }

        public void SetProductCategory(ProductCategoryEntity productCategory)
        {
            this.productCategory = productCategory;
        }

        private CategorySuggestionCollection GetParentCategorySuggestions(CategoryCollection parentCategories)
        {
            var categorySuggestions = new CategorySuggestionCollection();

            // Get parent categories with prefetched parents
            foreach (CategoryEntity parentCategory in parentCategories)
            {
                CategoryHelper.AddSuggestionsFromCategories(categorySuggestions, parentCategory);
            }

            return categorySuggestions;
        }

        public bool Save()
        {
            // Save Suggestions
            if (this.productCategory != null)
            {
                this.productCategory.InheritSuggestions = this.cbInheritSuggestions.Checked;
                this.productCategory.Save();

                EntityView <ProductCategorySuggestionEntity> psView = this.productCategory.ProductCategorySuggestionCollection.DefaultView;
                for (int i = 1; i <= 4; i++)
                {
                    ComboBoxInt ddl = (ComboBoxInt)this.FindControl(string.Format("ddlProductSuggestion-{0}-{1}", this.productCategory.ProductCategoryId, i));
                    if (ddl != null)
                    {
                        ProductCategorySuggestionEntity ps = null;
                        psView.Filter = new PredicateExpression(ProductSuggestionFields.SortOrder == i);
                        if (ddl.ValidId > 0)
                        {
                            // Update / Add
                            if (psView.Count == 0)
                            {
                                ps = new ProductCategorySuggestionEntity();
                                ps.ProductCategoryId = this.productCategory.ProductCategoryId;
                                ps.SortOrder = i;
                                ps.ParentCompanyId = this.productCategory.ParentCompanyId.GetValueOrDefault(0);
                            }
                            else
                            {
                                ps = psView[0];
                            }
                            ps.SuggestedProductCategoryId = ddl.ValidId;

                            CheckBox cbCheckout = (CheckBox)this.FindControl(string.Format("cbSuggestionCheckout{0}", i));
                            if (cbCheckout != null)
                            {
                                ps.Checkout = cbCheckout.Checked;
                            }
                            ps.Save();
                        }
                        else
                        {
                            // Delete if any
                            if (psView.Count == 1)
                            {
                                psView[0].Delete();
                                psView.RelatedCollection.Remove(psView[0]);
                            }
                        }
                    }
                }
            }
            return true;
        }
    }
}