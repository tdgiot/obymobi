﻿using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Utils;
using DevExpress.Web;
using DevExpress.Web;
using DevExpress.Web.Internal;
using DevExpress.Web.ASPxTreeList;
using DevExpress.Web.ASPxTreeList.Internal;
using Dionysos.Web.UI.WebControls;
using Obymobi.Cms.Logic.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Catalog.SubPanels
{
    public partial class PriceLevelItemsPanel : SubPanelCustomDataSource
    {
        #region Properties

        public int PriceLevelId { get; set; }

        private PriceLevelItemDataSource ItemsDataSource;

        private string NodeDblClickMethod
        {
            get { return @" function(s, e) {
	                            " + this.TreelistId + @".StartEdit(e.nodeKey);
		                        e.cancel = true;	                                                                    
                            }"; }
        }

        #endregion

        #region Methods

        private void CreateTreeList()
        {
            this.tlPriceLevelItems.SettingsEditing.Mode = TreeListEditMode.Inline;
            this.tlPriceLevelItems.SettingsBehavior.ColumnResizeMode = ColumnResizeMode.Disabled;
            this.tlPriceLevelItems.SettingsBehavior.AllowSort = false;
            this.tlPriceLevelItems.SettingsBehavior.AllowDragDrop = false;
            this.tlPriceLevelItems.ClientInstanceName = this.TreelistId;
            this.tlPriceLevelItems.AutoGenerateColumns = false;
            this.tlPriceLevelItems.SettingsEditing.AllowNodeDragDrop = false;
            this.tlPriceLevelItems.SettingsEditing.AllowRecursiveDelete = true;
            this.tlPriceLevelItems.Settings.GridLines = GridLines.Both;
            this.tlPriceLevelItems.ClientSideEvents.NodeDblClick = this.NodeDblClickMethod;
            this.tlPriceLevelItems.CustomErrorText += this.tlPriceLevelItemsStructure_CustomErrorText;

            this.tlPriceLevelItems.KeyFieldName = "ItemId";
            this.tlPriceLevelItems.ParentFieldName = "ParentItemId";

            TreeListComboBoxColumn nameColumn = new TreeListComboBoxColumn();
            nameColumn.Caption = "Name";
            nameColumn.Width = Unit.Pixel(300);
            nameColumn.VisibleIndex = 0;
            nameColumn.FieldName = "Name";
            nameColumn.CellStyle.Wrap = DefaultBoolean.True;
            this.tlPriceLevelItems.Columns.Add(nameColumn);

            TreeListTextColumn priceColumn = new TreeListTextColumn();
            priceColumn.Caption = "Price";
            priceColumn.Width = Unit.Pixel(70);
            priceColumn.VisibleIndex = 1;
            priceColumn.FieldName = "Price";
            priceColumn.CellStyle.Wrap = DefaultBoolean.True;
            this.tlPriceLevelItems.Columns.Add(priceColumn);

            TreeListCommandColumn editCommandColumn = new TreeListCommandColumn();
            editCommandColumn.VisibleIndex = 4;
            editCommandColumn.Width = Unit.Pixel(18);
            editCommandColumn.ShowNewButtonInHeader = true;
            editCommandColumn.EditButton.Visible = true;
            editCommandColumn.NewButton.Visible = false;
            editCommandColumn.DeleteButton.Visible = true;
            editCommandColumn.ButtonType = ButtonType.Image;
            editCommandColumn.EditButton.Image.Url = this.ResolveUrl("~/images/icons/pencil.png");
            editCommandColumn.DeleteButton.Image.Url = this.ResolveUrl("~/images/icons/delete.png");
            editCommandColumn.CancelButton.Image.Url = this.ResolveUrl("~/images/icons/cross_grey.png");
            editCommandColumn.NewButton.Image.Url = this.ResolveUrl("~/images/icons/add2.png");
            editCommandColumn.UpdateButton.Image.Url = this.ResolveUrl("~/images/icons/disk.png");
            this.tlPriceLevelItems.Columns.Add(editCommandColumn);

            TreeListHyperLinkColumn editPageColumn = new TreeListHyperLinkColumn();
            editPageColumn.Name = "EditPage";
            editPageColumn.VisibleIndex = 5;
            editPageColumn.Caption = "&nbsp;";
            editPageColumn.Width = Unit.Pixel(15);
            editPageColumn.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            editPageColumn.PropertiesHyperLink.ImageUrl = "~/images/icons/page_edit.png";
            editPageColumn.EditCellStyle.CssClass = "hidden";
            editPageColumn.FieldName = "ItemId";
            this.tlPriceLevelItems.Columns.Add(editPageColumn);
        
            this.tlPriceLevelItems.SettingsEditing.Mode = TreeListEditMode.Inline;
            this.tlPriceLevelItems.NodeValidating += this.tlPriceLevelItemsStructure_NodeValidating;
            this.tlPriceLevelItems.HtmlDataCellPrepared += this.tlPriceLevelItems_HtmlDataCellPrepared;
            this.tlPriceLevelItems.CommandColumnButtonInitialize += this.tlPriceLevelItems_CommandColumnButtonInitialize;
            this.tlPriceLevelItems.CellEditorInitialize += this.tlPriceLevelItems_CellEditorInitialize;

            this.InitDataSource();

            this.tlPriceLevelItems.Width = Unit.Pixel(736);
        }

        protected void tlPriceLevelItems_CellEditorInitialize(object sender, TreeListColumnEditorEventArgs e)
        {
            if (e.Column.FieldName == "Name")
            {
                if (e.NodeKey != null)
                {
                    e.Editor.ReadOnly = true;
                    e.Editor.BackColor = Color.LightGray;
                }
                else
                {
                    ASPxComboBox cbProducts = e.Editor as ASPxComboBox;
                    cbProducts.DropDownRows = 25;   
                    cbProducts.IncrementalFilteringMode = IncrementalFilteringMode.Contains;
                    cbProducts.IncrementalFilteringDelay = 1000;
                    cbProducts.EnableCallbackMode = true;
                    cbProducts.CallbackPageSize = 25;

                    string parentItemId = this.tlPriceLevelItems.NewNodeParentKey;
                    TreeListNode parentNode = this.tlPriceLevelItems.FindNodeByKeyValue(parentItemId);
                    if (parentNode is TreeListRootNode)
                    {
                        ListEditItemCollection items = new ListEditItemCollection();

                        CategoryCollection categories = ProductHelper.GetCategoriesWithProducts(CmsSessionHelper.CurrentCompanyId);
                        foreach (CategoryEntity category in categories)
                        {
                            if (category.ProductCategoryCollection.Count > 0)
                            {
                                string key = PriceLevelItemManager.GetItemId(category.CategoryId, PriceLevelItemManager.PriceLevelItemModelType.Category);
                                items.Add(new ListEditItem(category.MenuCategoryName, key));

                                foreach (ProductCategoryEntity productCategory in category.ProductCategoryCollection)
                                {
                                    if (productCategory.ProductEntity != null)
                                    {
                                        key = PriceLevelItemManager.GetItemId(productCategory.ProductCategoryId, PriceLevelItemManager.PriceLevelItemModelType.Product);
                                        items.Add(new ListEditItem(productCategory.MenuCategoryProductName, key));
                                    }                                    
                                }
                            }
                        }
                                                
                        cbProducts.DataSource = items;
                        cbProducts.DataBind();                        
                    }
                }
            }
            else if (e.Column.FieldName == "Price")
            {
                if (e.NodeKey != null && e.NodeKey.StartsWith("alteration") && !e.NodeKey.StartsWith("alterationoption"))
                {
                    e.Editor.ReadOnly = true;
                    e.Editor.BackColor = Color.LightGray;                   
                }
            }
        }        

        private void tlPriceLevelItems_CommandColumnButtonInitialize(object sender, TreeListCommandColumnButtonEventArgs e)
        {
            if (e.NodeKey != null )
            {
                if (e.ButtonType == TreeListCommandColumnButtonType.Delete || e.ButtonType == TreeListCommandColumnButtonType.Edit)
                {
                    string[] keyParts = e.NodeKey.Split(new[] { "_" }, StringSplitOptions.RemoveEmptyEntries);
                    if (keyParts.Length > 1)
                    {
                        string type = keyParts[0];
                        if (type.StartsWith("product", StringComparison.InvariantCultureIgnoreCase))
                        {
                            e.Visible = DefaultBoolean.True;
                        }
                        else if (type.StartsWith("alterationoption", StringComparison.InvariantCultureIgnoreCase))
                        {
                            e.Visible = e.ButtonType == TreeListCommandColumnButtonType.Edit ? e.Visible = DefaultBoolean.True : DefaultBoolean.False;
                        }
                        else if (type.StartsWith("alteration", StringComparison.InvariantCultureIgnoreCase))
                        {
                            e.Visible = DefaultBoolean.False;
                        }
                    }
                }
            }
        }

        private void tlPriceLevelItems_HtmlDataCellPrepared(object sender, TreeListHtmlDataCellEventArgs e)
        {
            if (e.Column.Name == "EditPage" && e.NodeKey != null)
            {
                string navigateUrl = string.Empty;
                string[] keyParts = e.NodeKey.Split(new[] {"_"}, StringSplitOptions.RemoveEmptyEntries);
                if (keyParts.Length > 1)
                {
                    string type = keyParts[0];
                    string[] keyValues = keyParts[1].Split(new[] { "-" }, StringSplitOptions.RemoveEmptyEntries);
                    if (type.StartsWith("product", StringComparison.InvariantCultureIgnoreCase))
                    {
                        navigateUrl = string.Format("~/Catalog/Product.aspx?id={0}", keyValues[0]);
                    }
                    else if (type.StartsWith("alterationoption", StringComparison.InvariantCultureIgnoreCase))
                    {
                        navigateUrl = string.Format("~/Catalog/Alterationoption.aspx?id={0}", keyValues[0]);
                    }                    
                    else if (type.StartsWith("alteration", StringComparison.InvariantCultureIgnoreCase))
                    {
                        navigateUrl = string.Format("~/Catalog/Alteration.aspx?id={0}", keyValues[0]);
                    }
                }

                foreach (Control item in e.Cell.Controls)
                {
                    HyperLinkDisplayControl link = item as HyperLinkDisplayControl;
                    if (link != null)
                    {
                        link.NavigateUrl = navigateUrl;
                        break;
                    }
                }
            }
        }

        private void InitDataSource()
        {
            if (this.PriceLevelId > 0)
            {
                this.ItemsDataSource = new PriceLevelItemDataSource(this.PriceLevelId);
                this.tlPriceLevelItems.DataSource = this.ItemsDataSource;
            }
        }

        public void Refresh()
        {
            this.InitDataSource();
            this.tlPriceLevelItems.DataBind();            
        }        

        public void RefreshDataSource(IEntity datasource)
        {
            this.DataSource = datasource;
            if (this.DataSource != null && !((IEntity)this.DataSource).IsNew)
            {
                this.Refresh();
            }
        }    

        private void ExpandNodes(TreeListNodeCollection nodes)
        {
            foreach (TreeListNode node in nodes)
            {
                if (node is TreeListRootNode)
                {
                    node.Expanded = true;
                }
                
                if (node.HasChildren)
                {
                    this.ExpandNodes(node.ChildNodes);
                }
            }
        }        

        #endregion

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            this.CreateTreeList();

            this.hlExpandAll.NavigateUrl = string.Format("javascript:{0}.ExpandAll()", this.TreelistId);
            this.hlCollapseAll.NavigateUrl = string.Format("javascript:{0}.CollapseAll()", this.TreelistId);
        }

        private void tlPriceLevelItemsStructure_CustomErrorText(object sender, TreeListCustomErrorTextEventArgs e)
        {
            if (e.Exception.InnerException != null)
            {
                ObymobiException exception = e.Exception.InnerException as ObymobiException;
                if (exception != null)
                {
                    e.ErrorText = exception.AdditionalMessage;
                }
                else
                {
                    e.ErrorText = e.Exception.InnerException.Message;
                }
            }
        }

        private void tlPriceLevelItemsStructure_NodeValidating(object sender, TreeListNodeValidationEventArgs e)
        {
            if (e.HasErrors)
            {
                e.NodeError = "Please correct the errors.";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.tlPriceLevelItems.DataBind();
            this.ExpandNodes(this.tlPriceLevelItems.Nodes);    
        }        

        private string TreelistId
        {
            get { return string.Format("{0}_treelist", this.ID); }
        }        

        #endregion
    }
}