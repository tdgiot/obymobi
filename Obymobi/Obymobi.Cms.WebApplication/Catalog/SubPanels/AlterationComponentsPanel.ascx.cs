﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Utils;
using DevExpress.Web;
using DevExpress.Web;
using DevExpress.Web.Internal;
using DevExpress.Web.ASPxTreeList;
using DevExpress.Web.ASPxTreeList.Internal;
using DevExpress.Web.Data;
using Dionysos.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dionysos;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logic.Cms;
using Obymobi.Logic.HelperClasses;
using System.Drawing;
using System.Text.RegularExpressions;
using Obymobi.Data;

namespace Obymobi.ObymobiCms.Catalog.SubPanels
{
    public partial class AlterationComponentsPanel : SubPanelCustomDataSource
    {
        #region  Fields

        private const string TRANSLATION_KEY_PREFIX = "Obymobi.ObymobiCms.Catalog.SubPanels.AlterationComponentsPanel.";
        private AlterationDataSource alterationDataSource;

        #endregion

        #region Properties

        private string EndDragNodeMethod
        {
            get
            {
                return @" function(s, e) {                                                                
                            if(e.htmlEvent.shiftKey || document.getElementById('" + this.cbSortingMode.ClientID + @"').checked){		                            
                                    e.cancel = true;
		                            var key = s.GetNodeKeyByRow(e.targetElement);
		                            " + this.TreelistId + @".PerformCustomCallback(e.nodeKey + ':' + key);
	                            }                                
                            }";
            }
        }

        private string NodeDblClickMethod
        {
            get { return @" function(s, e) {
	                            " + this.TreelistId + @".StartEdit(e.nodeKey);
		                        e.cancel = true;	                                                                    
                            }"; }
        }

        private string ItemTypeSelectedIndexChangedMethod {
            get { return @" function(s, e) {
                                var itemType = s.GetValue().toString();
                                if (" + this.TreelistId + @".GetEditor('Type').InCallback())
                                    lastItemType = itemType;
                                else
                                    " + this.TreelistId + @".GetEditor('Type').PerformCallback(itemType);
                            }"; }
        }

        private string TypeEndCallback
        {
            get { return @" function OnEndCallback(s,e){
                                if (lastItemType){
                                    " + this.TreelistId + @".GetEditor('Type').PerformCallback(lastItemType);
                                    lastItemType = null;
                                }
                            }"; }
        }

        #endregion

        #region Methods

        private void RetrieveSelectedNodes(ref List<TreeListNode> selectedNodes, TreeListNode parent, bool includeChildren = true)
        {
            TreeListNodeCollection nodes = parent == null ? this.tlComponents.Nodes : parent.ChildNodes;
            foreach (TreeListNode node in nodes)
            {
                if (includeChildren)
                {
                    if (node.Selected || node.ParentNode.Selected)
                    {
                        if (node.ParentNode.Selected)
                        {
                            node.Selected = true;
                        }
                        selectedNodes.Add(node);
                    }
                }
                else
                {
                    if (node.Selected && !node.ParentNode.Selected)
                    {
                        selectedNodes.Add(node);
                    }
                }

                if (node.HasChildren)
                {
                    this.RetrieveSelectedNodes(ref selectedNodes, node, includeChildren);
                }
            }
        }

        private void CreateTreeList()
        {
            this.tlComponents.SettingsEditing.Mode = TreeListEditMode.EditFormAndDisplayNode;
            this.tlComponents.SettingsPopupEditForm.Width = 600;
            this.tlComponents.SettingsBehavior.ColumnResizeMode = ColumnResizeMode.Disabled;
            this.tlComponents.SettingsBehavior.AllowSort = false;
            this.tlComponents.SettingsBehavior.AllowDragDrop = false;
            this.tlComponents.ClientInstanceName = this.TreelistId;
            this.tlComponents.AutoGenerateColumns = false;
            this.tlComponents.SettingsEditing.AllowNodeDragDrop = true;
            this.tlComponents.SettingsEditing.AllowRecursiveDelete = true;            
            this.tlComponents.Settings.GridLines = GridLines.Both;
            this.tlComponents.ClientSideEvents.EndDragNode = this.EndDragNodeMethod;
            this.tlComponents.ClientSideEvents.NodeDblClick = this.NodeDblClickMethod;
            this.tlComponents.CustomErrorText += this.tlComponents_CustomErrorText;

            this.tlComponents.KeyFieldName = "ItemId";
            this.tlComponents.ParentFieldName = "ParentItemId";

            TreeListTextColumn nameColumn = new TreeListTextColumn();
            nameColumn.Caption = "Name";
            nameColumn.Width = Unit.Pixel(167);
            nameColumn.VisibleIndex = 0;
            nameColumn.FieldName = "Name";
            nameColumn.CellStyle.Wrap = DefaultBoolean.True;
            nameColumn.EditFormSettings.VisibleIndex = 3;
            this.tlComponents.Columns.Add(nameColumn);

            TreeListComboBoxColumn itemTypeColumn = new TreeListComboBoxColumn();
            itemTypeColumn.Caption = "Item Type";
            itemTypeColumn.FieldName = "ItemType";
            itemTypeColumn.Width = Unit.Pixel(100);
            itemTypeColumn.VisibleIndex = 1;
            itemTypeColumn.CellStyle.Wrap = DefaultBoolean.True;
            itemTypeColumn.PropertiesComboBox.ClientSideEvents.SelectedIndexChanged = this.ItemTypeSelectedIndexChangedMethod;
            itemTypeColumn.EditFormSettings.VisibleIndex = 1;
            itemTypeColumn.EditFormSettings.ColumnSpan = 1;
            this.tlComponents.Columns.Add(itemTypeColumn);

            TreeListComboBoxColumn typeColumn = new TreeListComboBoxColumn();
            typeColumn.Caption = "Type";
            typeColumn.FieldName = "Type";
            typeColumn.Width = Unit.Pixel(100);
            typeColumn.VisibleIndex = 3;
            typeColumn.CellStyle.Wrap = DefaultBoolean.True;
            typeColumn.PropertiesComboBox.ClientSideEvents.EndCallback = this.TypeEndCallback;
            typeColumn.EditFormSettings.VisibleIndex = 2;
            this.tlComponents.Columns.Add(typeColumn);

            TreeListCommandColumn editCommandColumn = new TreeListCommandColumn();
            editCommandColumn.VisibleIndex = 4;
            editCommandColumn.Width = Unit.Pixel(60);
            editCommandColumn.ShowNewButtonInHeader = true;
            editCommandColumn.EditButton.Visible = true;
            editCommandColumn.NewButton.Visible = true;
            editCommandColumn.DeleteButton.Visible = true;
            editCommandColumn.ButtonType = ButtonType.Image;
            editCommandColumn.EditButton.Image.Url = this.ResolveUrl("~/images/icons/pencil.png");
            editCommandColumn.DeleteButton.Image.Url = this.ResolveUrl("~/images/icons/delete.png");
            editCommandColumn.CancelButton.Image.Url = this.ResolveUrl("~/images/icons/cross_grey.png");
            editCommandColumn.NewButton.Image.Url = this.ResolveUrl("~/images/icons/add2.png");
            editCommandColumn.UpdateButton.Image.Url = this.ResolveUrl("~/images/icons/disk.png");            
            this.tlComponents.Columns.Add(editCommandColumn);

            TreeListHyperLinkColumn editPageColumn = new TreeListHyperLinkColumn();
            editPageColumn.Name = "EditPage";
            editPageColumn.Caption = " ";
            editPageColumn.VisibleIndex = 5;            
            editPageColumn.Width = Unit.Pixel(15);
            editPageColumn.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            editPageColumn.PropertiesHyperLink.ImageUrl = "~/images/icons/page_edit.png";
            editPageColumn.EditCellStyle.CssClass = "hidden";
            editPageColumn.FieldName = "ItemId";            
            editPageColumn.EditFormSettings.VisibleIndex = 2;
            editPageColumn.EditFormCaptionStyle.CssClass = "hidden";
            this.tlComponents.Columns.Add(editPageColumn);
            
            this.tlComponents.NodeValidating += this.tlComponents_NodeValidating;
            this.tlComponents.HtmlDataCellPrepared += this.tlComponents_HtmlDataCellPrepared;
            this.tlComponents.HtmlRowPrepared += this.tlComponents_HtmlRowPrepared;
            this.tlComponents.CommandColumnButtonInitialize += this.tlComponents_CommandColumnButtonInitialize;
            this.tlComponents.CellEditorInitialize += this.tlComponents_CellEditorInitialize;
            this.tlComponents.CustomCallback += this.tlComponents_CustomCallback;
            this.tlComponents.NodeUpdating += this.tlComponents_OnNodeUpdating;
            this.tlComponents.NodeInserting += this.tlComponents_OnNodeInserting;

            this.InitDataSource();

            this.tlComponents.Width = Unit.Pixel(736);
        }

        private void tlComponents_HtmlRowPrepared(object sender, TreeListHtmlRowEventArgs e)
        {
            if (!e.NodeKey.IsNullOrWhiteSpace())
            {
                IAlterationComponent menuItem = this.alterationDataSource.DataSource.SingleOrDefault(item => item.ItemId == e.NodeKey);
                if (menuItem != null && !menuItem.Visible)
                {
                    e.Row.ForeColor = Color.Gray;
                }
            }
        }

        private void tlComponents_CommandColumnButtonInitialize(object sender, TreeListCommandColumnButtonEventArgs e)
        {
            if (e.NodeKey != null && e.ButtonType == TreeListCommandColumnButtonType.New)
            {
                string[] keyParts = e.NodeKey.Split(new[] {"-"}, StringSplitOptions.RemoveEmptyEntries);
                if (keyParts.Length > 0)
                {
                    string itemType = keyParts[0];
                    if (itemType == AlterationComponentType.Product)
                    {
                        e.Visible = DefaultBoolean.False;
                    }
                    else if (itemType == AlterationComponentType.Alteration)
                    {
                        TreeListNode currentNode = this.tlComponents.FindNodeByKeyValue(e.NodeKey);
                        if (currentNode != null && currentNode.DataItem != null)
                        {
                            if (currentNode.DataItem is AlterationEntity)
                            {
                                AlterationEntity alteration = (AlterationEntity)currentNode.DataItem;
                                e.Visible = this.CanHasChildren(alteration.Type) ? DefaultBoolean.True : DefaultBoolean.False;
                            }
                        }
                    }
                }
            }
        }

        private void tlComponents_HtmlDataCellPrepared(object sender, TreeListHtmlDataCellEventArgs e)
        {
            if (e.Column.Name == "EditPage" && e.NodeKey != null)
            {
                string navigateUrl = string.Empty;
                string[] keyParts = e.NodeKey.Split(new[] {"-"}, StringSplitOptions.RemoveEmptyEntries);
                if (keyParts.Length > 1)
                {
                    string itemType = keyParts[0];
                    string itemId = keyParts[1];
                    if (itemType == AlterationComponentType.Alteration)
                    {
                        navigateUrl = string.Format("~/Catalog/Alteration.aspx?id={0}", itemId);
                    }                    
                    else if (itemType == AlterationComponentType.Product)
                    {
                        navigateUrl = string.Format("~/Catalog/Product.aspx?id={0}", itemId);
                    }
                }

                foreach (Control item in e.Cell.Controls)
                {
                    HyperLinkDisplayControl link = item as HyperLinkDisplayControl;
                    if (link != null)
                    {
                        link.NavigateUrl = navigateUrl;
                        break;
                    }
                }
            }
        }

        protected void tlComponents_CellEditorInitialize(object sender, TreeListColumnEditorEventArgs e)
        {           
            if (e.Column.FieldName == "ItemType" || e.Column.FieldName == "Type")
            {
                TreeListNode parentNode = null;
                TreeListNode currentNode = null;
                if (e.NodeKey != null)
                {
                    currentNode = this.tlComponents.FindNodeByKeyValue(e.NodeKey);
                    if (currentNode != null)
                    {
                        parentNode = currentNode.ParentNode;
                    }
                }
                else
                {
                    string parentItemId = this.tlComponents.NewNodeParentKey;
                    parentNode = this.tlComponents.FindNodeByKeyValue(parentItemId);
                }
                
                if (e.Column.FieldName == "ItemType")
                {
                    ASPxComboBox cbItemType = e.Editor as ASPxComboBox;

                    if (parentNode is TreeListRootNode)
                    {
                        // Root node
                        this.FillItemTypes(cbItemType, AlterationComponentType.Alteration);

                        ASPxPageControl pageControl = this.tlComponents.FindEditFormTemplateControl("tabs") as ASPxPageControl;
                        if (pageControl != null && pageControl.TabPages.Count > 1)
                        {
                            pageControl.TabPages[1].Visible = false;
                        }
                    }
                    else if (currentNode != null)
                    {
                        ASPxComboBox existingItemTypeCb = this.GetExistingItemTypeComboBox();
                        if (existingItemTypeCb != null)
                        {
                            string itemType = DataBinder.Eval(currentNode.DataItem, "ItemType").ToString();
                            string type = DataBinder.Eval(currentNode.DataItem, "Type").ToString();

                            this.FillItemTypes(existingItemTypeCb, itemType, type);
                            existingItemTypeCb.ReadOnly = true;

                            ASPxComboBox existingEntitiesCb = this.GetExistingEntitiesComboBox();
                            if (existingEntitiesCb != null)
                            {
                                this.FillEntities(existingEntitiesCb, itemType);
                            }
                        }
                        cbItemType.ReadOnly = true;
                    }
                    else if (parentNode != null && parentNode.DataItem != null)
                    {
                        if (parentNode.DataItem is AlterationEntity)
                        {
                            AlterationEntity parentAlteration = (AlterationEntity)parentNode.DataItem;
                            if (parentAlteration.Type == AlterationType.Page)
                            {
                                this.FillItemTypes(cbItemType, AlterationComponentType.Alteration);                                
                            }
                            else if (parentAlteration.Type == AlterationType.Category)
                            {
                                if (parentAlteration.AlterationProductCollection.Count == 0 && parentAlteration.AlterationCollection.Count == 0)
                                {
                                    // No children yet
                                    this.FillItemTypes(cbItemType, AlterationComponentType.Alteration, AlterationComponentType.Product);
                                }
                                else if (parentAlteration.AlterationCollection.Count > 0)
                                {
                                    // Alteration as children
                                    this.FillItemTypes(cbItemType, AlterationComponentType.Alteration);
                                }
                                else if (parentAlteration.AlterationProductCollection.Count > 0)
                                {
                                    // Product as children
                                    this.FillItemTypes(cbItemType, AlterationComponentType.Product);
                                }
                            }
                            else if (parentAlteration.AlterationProductCollection.Count > 0)
                            {
                                // Products as siblings                                    
                                this.FillItemTypes(cbItemType, AlterationComponentType.Product);
                            }                                
                        }                            
                    }
                }
                else if (e.Column.FieldName == "Type")
                {
                    ASPxComboBox cbType = e.Editor as ASPxComboBox;
                    cbType.Callback += this.cbType_Callback;

                    if (parentNode is TreeListRootNode)
                    {
                        // Root node
                        this.DataBindAlterationTypeToCombobox(cbType, this.GetAlterationTypes(AlterationType.Package));
                    }
                    else if (parentNode != null && parentNode.DataItem != null) 
                    {
                        AlterationEntity parentAlteration = (AlterationEntity)parentNode.DataItem;
                        this.FillTypesWithParent(cbType, parentAlteration);
                    }

                    if (currentNode != null && currentNode.DataItem != null)
                    {
                        string type = DataBinder.Eval(currentNode.DataItem, "Type").ToString();
                        if (!type.IsNullOrWhiteSpace())
                        {
                            if (currentNode.DataItem is AlterationEntity)
                            {
                                AlterationEntity currentAlteration = (AlterationEntity)currentNode.DataItem;
                                cbType.SelectedItem = cbType.Items.FindByText(currentAlteration.Type.GetStringValue());
                            }
                            else
                            {
                                cbType.SelectedItem = cbType.Items.FindByText(type);
                            }
                        }
                    }
                }                
            }            
        }

        private void tlComponents_CustomCallback(object sender, TreeListCustomCallbackEventArgs e)
        {
            // Node dragged callback
            string[] nodes = e.Argument.Split(new char[] { ':' });

            string dragged = nodes[0];
            string draggedUpon = nodes[1];

            this.alterationDataSource.AlterationManager.DraggedToSort(dragged, draggedUpon);

            this.tlComponents.DataBind();
        }

        private void FillTypesWithParent(ASPxComboBox cbType, AlterationEntity parent)
        {
            if (parent.Type == AlterationType.Page)
            {
                this.DataBindAlterationTypeToCombobox(cbType, this.GetAlterationTypes(AlterationType.Page));
            }
            else if (parent.Type == AlterationType.Category)
            {
                if (parent.AlterationProductCollection.Count > 0)
                {
                    this.DataBindEnumValuesToCombBox(cbType, ProductType.Product);
                }
                else
                {
                    this.DataBindAlterationTypeToCombobox(cbType, this.GetAlterationTypes(AlterationType.Category));
                }
            }
        }

        private void FillItemTypes(ASPxComboBox combobox, params object[] values)
        {
            // Combobox on first tab
            this.DataBindStringValuesToCombBox(combobox, values);

            // Combobox on second tab
            ASPxComboBox existingItemTypeCombobox = this.GetExistingItemTypeComboBox();
            if (existingItemTypeCombobox != null)
            {
                this.DataBindStringValuesToCombBox(existingItemTypeCombobox, values);
            }

            if (values.Length > 0)
            {
                combobox.SelectedIndex = 0;
            }
        }        

        private ASPxComboBox GetExistingItemTypeComboBox()
        {
            ASPxComboBox cmbItemType = null;
            ASPxPageControl pageControl = this.tlComponents.FindEditFormTemplateControl("tabs") as ASPxPageControl;
            if (pageControl != null)
            {
                cmbItemType = pageControl.TabPages[1].FindControl("cmbItemType") as ASPxComboBox;
            }
            return cmbItemType;
        }

        private ASPxComboBox GetExistingEntitiesComboBox()
        {
            ASPxComboBox cmbItemType = null;
            ASPxPageControl pageControl = this.tlComponents.FindEditFormTemplateControl("tabs") as ASPxPageControl;
            if (pageControl != null)
            {
                cmbItemType = pageControl.TabPages[1].FindControl("cmbItem") as ASPxComboBox;
            }
            return cmbItemType;
        }

        private void FillTypes(ASPxComboBox cbType, string itemType, string type = "")
        {
            string parentItemId = this.tlComponents.NewNodeParentKey;
            TreeListNode parentNode = null;

            if (parentItemId != null)
            {
                parentNode = this.tlComponents.FindNodeByKeyValue(parentItemId);
            }

            if (itemType == AlterationComponentType.Alteration)
            {
                if (parentNode != null && parentNode.DataItem != null)
                {
                    AlterationEntity parentAlteration = parentNode.DataItem as AlterationEntity;
                    this.FillTypesWithParent(cbType, parentAlteration);
                }
                else
                {
                    this.DataBindEnumToComboBox(cbType, typeof(AlterationType));
                }                
            }
            else if (itemType == AlterationComponentType.Product)
            {
                this.DataBindEnumValuesToCombBox(cbType, ProductType.Product);
                cbType.SelectedIndex = 0;
            }

            if (!type.IsNullOrWhiteSpace())
            {
                cbType.SelectedItem = cbType.Items.FindByText(type);
            }
        }

        private void DataBindStringValuesToCombBox(ASPxComboBox combobox, params object[] values)
        {
            combobox.Items.Clear();
            foreach (object value in values)
            {
                combobox.Items.Add(value.ToString(), value);
            }            
        }

        private void DataBindAlterationTypeToCombobox(ASPxComboBox combobox, List<AlterationType> types)
        {
            combobox.Items.Clear();
            foreach (AlterationType value in types)
            {
                string enumText = EnumUtil.GetStringValue((Enum)value);
                int enumValue = Convert.ToInt32(value);

                combobox.Items.Add(enumText, enumValue);
            }

            if (types.Count > 0)
            {
                combobox.SelectedIndex = 0;
            }            
        }

        private void DataBindEnumValuesToCombBox(ASPxComboBox combobox, params object[] values)
        {
            combobox.Items.Clear();
            foreach (object value in values)
            {
                string enumText = EnumUtil.GetStringValue((Enum)value);
                int enumValue = Convert.ToInt32(value);

                combobox.Items.Add(enumText, enumValue);
            }

            if (values.Length > 0)
            {
                combobox.SelectedIndex = 0;
            }            
        }

        private void DataBindEnumToComboBox(ASPxComboBox combobox, Type enumType)
        {
            combobox.Items.Clear();

            string[] names = Enum.GetNames(enumType);
            Array values = Enum.GetValues(enumType);

            for (int i = 0; i < names.Length; i++)
            {
                string text = EnumUtil.GetStringValue((Enum)values.GetValue(i));
                int value = Convert.ToInt32(values.GetValue(i));
                combobox.Items.Add(text, value);
            }
            combobox.DataBind();
        }

        private void InitDataSource()
        {
            if (this.DataSource != null)
            {
                this.alterationDataSource = new AlterationDataSource(this.DataSourceAsAlterationEntity.AlterationId);
                this.tlComponents.DataSource = this.alterationDataSource;
            }
        }

        private void Refresh(bool expandAll = true)
        {
            this.InitDataSource();
            this.tlComponents.DataBind();
            if (expandAll)
            {
                this.tlComponents.ExpandAll();
            }
        }        

        public void RefreshDataSource(IEntity datasource)
        {
            this.DataSource = datasource;
            if (this.DataSource != null && !((IEntity)this.DataSource).IsNew)
            {
                this.Refresh();
            }
        }

        private void SortSelectedCategories()
        {
            List<TreeListNode> selectedNodes = new List<TreeListNode>();
            this.RetrieveSelectedNodes(ref selectedNodes, null, false);

            if (selectedNodes.Count != 1)
            {
                this.PageAsPageDefault.MultiValidatorDefault.AddError(this.PageAsPageDefault.Translate(AlterationComponentsPanel.TRANSLATION_KEY_PREFIX + "Error.SelectOneItem", "Only 1 category can be sorted at the time.", true));
                this.PageAsPageDefault.Validate();
            }
            else
            {
                string key = selectedNodes[0].Key;
                this.alterationDataSource.AlterationManager.SortCategory(key);

                this.tlComponents.DataBind();
            }
        }

        private void DeleteSelected()
        {
            List<TreeListNode> selectedNodes = new List<TreeListNode>();
            this.RetrieveSelectedNodes(ref selectedNodes, null);

            if (selectedNodes.Count <= 0)
            {
                this.PageAsPageDefault.MultiValidatorDefault.AddError(this.PageAsPageDefault.Translate(AlterationComponentsPanel.TRANSLATION_KEY_PREFIX + "Error.NoItemSelected", "No item has been selected to be deleted.", true));
                this.PageAsPageDefault.Validate();
            }
            else
            {
                this.DeleteItems(selectedNodes);
                this.tlComponents.DataBind();

                if (selectedNodes.Count == 1)
                {
                    this.PageAsPageDefault.AddInformatorInfo(this.PageAsPageDefault.Translate(AlterationComponentsPanel.TRANSLATION_KEY_PREFIX + "Success.ItemsDeleted", "The selected item has been deleted.", true));
                }
                else
                {
                    this.PageAsPageDefault.AddInformatorInfo(this.PageAsPageDefault.Translate(AlterationComponentsPanel.TRANSLATION_KEY_PREFIX + "Success.ItemsDeleted", "The selected items have been deleted.", true));
                }
                this.PageAsPageDefault.Validate();
            }
        }

        private void DeleteItems(List<TreeListNode> selectedNodes)
        {
            foreach (TreeListNode node in selectedNodes)
            {
                this.alterationDataSource.DeleteParameters.Clear();
                this.alterationDataSource.DeleteParameters.Add("ItemId", node.Key);
                this.alterationDataSource.Delete();                
            }

            this.DataSource = new AlterationEntity(this.DataSourceAsAlterationEntity.AlterationId);
            this.InitDataSource();
        }

        private void FillEntities(ASPxComboBox combobox, string value)
        {
            if (value == AlterationComponentType.Alteration)
            {
                int alterationVersion = CmsSessionHelper.AlterationDialogMode == AlterationDialogMode.v3 ? 2 : 1;

                PrefetchPath prefetch = new PrefetchPath(EntityType.AlterationEntity);
                prefetch.Add(AlterationEntityBase.PrefetchPathProductAlterationCollection);

                IncludeFieldsList includes = new IncludeFieldsList(AlterationFields.AlterationId, AlterationFields.Name);
                List<int> excludeAlteration = new List<int> { this.DataSourceAsAlterationEntity.AlterationId };
                
                PredicateExpression filter = new PredicateExpression();
                filter.Add(AlterationFields.ParentAlterationId == DBNull.Value);
                filter.Add(AlterationFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
                filter.Add(AlterationFields.AlterationId != excludeAlteration);
                filter.Add(AlterationFields.Version == alterationVersion);

                SortExpression sort = new SortExpression(new SortClause(AlterationFields.Name, SortOperator.Ascending));

                AlterationCollection alterations = new AlterationCollection();
                alterations.GetMulti(filter, 0, sort, null, prefetch, includes, 0, 0);

                List<AlterationEntity> filteredAlteration = alterations.Where(x => x.ProductAlterationCollection.Count <= 0).ToList();
                
                combobox.TextField = "Name";
                combobox.ValueField = "AlterationId";
                combobox.DataSource = filteredAlteration;
                combobox.DataBind();
            }
            else if (value == AlterationComponentType.Product)
            {
                IncludeFieldsList includes = new IncludeFieldsList(ProductFields.ProductId, ProductFields.Name);
                SortExpression sort = new SortExpression(new SortClause(ProductFields.Name, SortOperator.Ascending));
                ProductCollection products = ProductHelper.GetProductCollectionForCompany(CmsSessionHelper.CurrentCompanyId, null, ProductType.Product, sort, includes);
                
                combobox.TextField = "Name";
                combobox.ValueField = "ProductId";
                combobox.DataSource = products;
                combobox.DataBind();
            }
        }        

        private string GetExistingItemType()
        {
            string toReturn = "";
            ASPxComboBox cmb = this.GetExistingItemTypeComboBox();
            if (cmb != null)
            {
                toReturn = cmb.Value.ToString();
            }
            return toReturn;
        }

        private string GetExistingItemValue()
        {
            string toReturn = "";
            ASPxComboBox cmb = this.GetExistingEntitiesComboBox();
            if (cmb != null)
            {
                toReturn = cmb.Value.ToString();
            }
            return toReturn;
        }

        private int GetActiveEditTabIndex()
        {
            int index = 0;
            ASPxPageControl pageControl = this.tlComponents.FindEditFormTemplateControl("tabs") as ASPxPageControl;
            if (pageControl != null)
            {
                index = pageControl.ActiveTabIndex;                
            }
            return index;
        }

        private List<AlterationType> GetAlterationTypes(AlterationType type)
        {
            List<AlterationType> alterationTypes = new List<AlterationType>();

            switch (type)
            {
                case AlterationType.Package:
                    alterationTypes.Add(AlterationType.Page);
                    alterationTypes.Add(AlterationType.Summary);
                    break;
                case AlterationType.Page:
                    alterationTypes.Add(AlterationType.Options);
                    alterationTypes.Add(AlterationType.DateTime);
                    alterationTypes.Add(AlterationType.Date);
                    alterationTypes.Add(AlterationType.Email);
                    alterationTypes.Add(AlterationType.Text);
                    alterationTypes.Add(AlterationType.Numeric);
                    alterationTypes.Add(AlterationType.Category);
                    alterationTypes.Add(AlterationType.Notes);
                    alterationTypes.Add(AlterationType.Image);
                    alterationTypes.Add(AlterationType.Instruction);
                    break;
                case AlterationType.Category:
                    alterationTypes.Add(AlterationType.Category);
                    break;
            }

            return alterationTypes;
        }

        private bool CanHasChildren(AlterationType type)
        {
            switch (type)
            {
                case AlterationType.Package:
                case AlterationType.Page:
                case AlterationType.Category:
                    return true;
                default:
                    return false;
            }
        }

        private void LoadUserControls()
        {
            this.hlExpandAll.NavigateUrl = string.Format("javascript:{0}.ExpandAll()", this.TreelistId);
            this.hlCollapseAll.NavigateUrl = string.Format("javascript:{0}.CollapseAll()", this.TreelistId);
        }

        #endregion

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            base.OnInit(e);
            this.CreateTreeList();
        }

        protected void cbType_Callback(object sender, CallbackEventArgsBase e)
        {
            ASPxComboBox cbType = sender as ASPxComboBox;            
            this.FillTypes(cbType, e.Parameter);            
        }

        protected void cmbItem_Callback(object sender, CallbackEventArgsBase e)
        {
            this.FillEntities(sender as ASPxComboBox, e.Parameter);            
        }        

        private void tlComponents_CustomErrorText(object sender, TreeListCustomErrorTextEventArgs e)
        {
            if (e.Exception.InnerException != null)
            {
                ObymobiException exception = e.Exception.InnerException as ObymobiException;
                if (exception != null)
                {
                    e.ErrorText = exception.AdditionalMessage;
                }
                else
                {
                    e.ErrorText = e.Exception.InnerException.Message;
                }
            }
        }

        private void tlComponents_NodeValidating(object sender, TreeListNodeValidationEventArgs e)
        {
            if (e.HasErrors)
            {
                e.NodeError = this.PageAsPageDefault.Translate(AlterationComponentsPanel.TRANSLATION_KEY_PREFIX + "PleaseCorrectErrors", "Please correct the errors.", true);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.tlComponents.DataBind();

            string deleteMessage = this.PageAsPageDefault.Translate(AlterationComponentsPanel.TRANSLATION_KEY_PREFIX + "Warning.PreSubmit", "Are you sure you want to delete these items? Child items will be automatically removed if the parent item is selected.", true);

            this.btDeleteItemsTop.PreSubmitWarning = deleteMessage;
            this.btDeleteItemsBottom.PreSubmitWarning = deleteMessage;

            this.btDeleteItemsTop.Click += this.btDeleteSelected_Click;
            this.btDeleteItemsBottom.Click += this.btDeleteSelected_Click;

            this.btnSortSelected.Click += btnSortSelected_Click;
        }

        void btnSortSelected_Click(object sender, EventArgs e)
        {
            this.SortSelectedCategories();
        }

        private void btDeleteSelected_Click(object sender, EventArgs e)
        {
            this.DeleteSelected();
        }

        private string TreelistId
        {
            get { return string.Format("{0}_treelist", this.ID); }
        }

        private AlterationEntity DataSourceAsAlterationEntity
        {
            get { return (AlterationEntity)this.DataSource; }
        }

        #endregion

        protected void tlComponents_OnNodeUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {
            if (this.GetActiveEditTabIndex() == 1)
            {
                string existingItemType = this.GetExistingItemType();
                string existingItemValue = this.GetExistingItemValue();

                if (!existingItemType.IsNullOrWhiteSpace() && !existingItemValue.IsNullOrWhiteSpace())
                {
                    e.NewValues.Remove("Name");
                    e.NewValues.Remove("Type");
                    e.NewValues["ItemType"] = existingItemType;
                    e.NewValues["ForeignKey"] = existingItemValue;
                }
            }
        }

        protected void tlComponents_OnNodeInserting(object sender, ASPxDataInsertingEventArgs e)
        {
            if (this.GetActiveEditTabIndex() == 1)
            {
                string existingItemType = this.GetExistingItemType();
                string existingItemValue = this.GetExistingItemValue();
                
                if (!existingItemType.IsNullOrWhiteSpace() && !existingItemValue.IsNullOrWhiteSpace())
                {
                    e.NewValues.Remove("Name");
                    e.NewValues.Remove("Type");
                    e.NewValues["ItemType"] = existingItemType;
                    e.NewValues["ForeignKey"] = existingItemValue;
                }
            }
        }
    }
}