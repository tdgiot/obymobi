﻿using System;
using DevExpress.XtraScheduler;
using DevExpress.Web.ASPxScheduler;
using System.Web.UI.WebControls;
using System.Web.UI;

public partial class PriceScheduledItemsPanel : UserControl
{
    #region Fields

    public int PriceScheduleId { get; set; }    

    #endregion

    #region Methods

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        this.SetGui();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.scheduler.Storage.Appointments.AutoRetrieveId = true;
    }

    void SetGui()
    {
        this.scheduler.PopupMenuShowing += this.scheduler_PopupMenuShowing;

        this.scheduler.InitClientAppointment += this.scheduler_InitClientAppointment;
        this.scheduler.AppointmentFormShowing += this.scheduler_AppointmentFormShowing;
        this.scheduler.BeforeExecuteCallbackCommand += this.scheduler_BeforeExecuteCallbackCommand;
        this.scheduler.AfterExecuteCallbackCommand += this.scheduler_AfterExecuteCallbackCommand;
        this.scheduler.AppointmentViewInfoCustomizing += this.scheduler_AppointmentViewInfoCustomizing;        

        this.scheduler.TimelineView.IntervalCount = 24;
        this.scheduler.TimelineView.ShowMoreButtons = false;
        this.scheduler.TimelineView.NavigationButtonVisibility = NavigationButtonVisibility.Never;

        this.scheduler.WorkWeekView.ShowAllDayArea = false;
        this.scheduler.WorkWeekView.ShowFullWeek = true;
        this.scheduler.WorkWeekView.ShowMoreButtons = false;

        this.scheduler.DayView.ShowAllDayArea = false;
        this.scheduler.DayView.ShowMoreButtons = false;

        this.scheduler.MonthView.ShowWeekend = true;

        this.scheduler.OptionsBehavior.ShowViewNavigator = true;
        this.scheduler.OptionsBehavior.ShowViewVisibleInterval = true;
        this.scheduler.OptionsCustomization.AllowInplaceEditor = UsedAppointmentType.None;
        this.scheduler.DayView.TimeMarkerVisibility = TimeMarkerVisibility.Never;

        this.scheduler.WeekView.Enabled = true;
        this.scheduler.DayView.Enabled = true;
        this.scheduler.WeekView.Enabled = false;
        this.scheduler.WorkWeekView.Enabled = true;
        this.scheduler.MonthView.Enabled = true;

        this.scheduler.WorkWeekView.MenuCaption = "Full Week View";

        this.scheduler.Images.SmartTag.Url = this.ResolveUrl("~/images/icons/add2.png");

        this.scheduler.WorkDays.Add(WeekDays.Saturday);
        this.scheduler.WorkDays.Add(WeekDays.Sunday);
        this.scheduler.TimelineView.WorkTime = new TimeOfDayInterval(TimeSpan.FromHours(0), TimeSpan.FromHours(24));
        this.scheduler.DayView.WorkTime = new TimeOfDayInterval(TimeSpan.FromHours(0), TimeSpan.FromHours(24));
        this.scheduler.WorkWeekView.WorkTime = new TimeOfDayInterval(TimeSpan.FromHours(0), TimeSpan.FromHours(24));

        this.scheduler.Start = DateTime.Now.Date;
        this.scheduler.ActiveViewType = SchedulerViewType.WorkWeek;
    }

    void scheduler_AppointmentViewInfoCustomizing(object sender, DevExpress.Web.ASPxScheduler.AppointmentViewInfoCustomizingEventArgs e)
    {
        int backgroundColor = (int)e.ViewInfo.Appointment.CustomFields["BackgroundColor"];
        e.ViewInfo.AppointmentStyle.BackColor = System.Drawing.Color.FromArgb(backgroundColor);

        int textColor = (int)e.ViewInfo.Appointment.CustomFields["TextColor"];
        e.ViewInfo.AppointmentStyle.ForeColor = System.Drawing.Color.FromArgb(textColor);

        e.ViewInfo.StatusDisplayType = AppointmentStatusDisplayType.Never;
    }

    void scheduler_InitClientAppointment(object sender, InitClientAppointmentEventArgs args)
    {
        args.Properties["cpSubject"] = args.Appointment.Subject;
        args.Properties["cpLocation"] = args.Appointment.Location;
    }

    void scheduler_AfterExecuteCallbackCommand(object sender, SchedulerCallbackCommandEventArgs e)
    {
        this.scheduler.DataBind();
    }

    void scheduler_BeforeExecuteCallbackCommand(object sender, SchedulerCallbackCommandEventArgs e)
    {
        if (e.CommandId == SchedulerCallbackCommandId.AppointmentSave)
        {
            e.Command = new PriceScheduleAppointmentSaveCallbackCommand((ASPxScheduler)sender);
        }
        else if (e.CommandId == PriceScheduleAppointmentMenuCallbackCommand.CommandId)
        {
            e.Command = new PriceScheduleAppointmentMenuCallbackCommand((ASPxScheduler)sender);
        }
    }

    void scheduler_AppointmentFormShowing(object sender, DevExpress.Web.ASPxScheduler.AppointmentFormEventArgs e)
    {
        e.Container = new PriceScheduleAppointmentForm((ASPxScheduler)sender);
        e.Container.Caption = "Add Item";
    }

    void scheduler_PopupMenuShowing(object sender, DevExpress.Web.ASPxScheduler.PopupMenuShowingEventArgs e)
    {
        if (e.Menu.Id == SchedulerMenuItemId.DefaultMenu)
        {
            DevExpress.Web.MenuItem newAppointment = e.Menu.Items.FindByName("NewAppointment");
            newAppointment.Image.Url = this.ResolveUrl("~/images/icons/add2.png");
            newAppointment.Text = "Add Item";

            DevExpress.Web.MenuItem timeScaleEnable = e.Menu.Items.FindByName("TimeScaleEnable");
            DevExpress.Web.MenuItem switchViewMenu = e.Menu.Items.FindByName("SwitchViewMenu");
            
            e.Menu.Items.Clear();

            e.Menu.Items.Add(newAppointment);
            if (timeScaleEnable != null)
            {
                e.Menu.Items.Add(timeScaleEnable);
            }
            e.Menu.Items.Add(switchViewMenu);
        }
        else if (e.Menu.Id == SchedulerMenuItemId.AppointmentMenu)
        {
            e.Menu.ClientSideEvents.ItemClick = String.Format("function(s, e) {{ DefaultAppointmentMenuHandler({0}, s, e); }}", this.scheduler.ClientID);
            e.Menu.Items.Clear();

            DevExpress.Web.MenuItem editItem = new DevExpress.Web.MenuItem("Edit", "EditItem");
            editItem.BeginGroup = true;
            editItem.Image.Url = this.ResolveUrl("~/images/icons/pencil.png");
            e.Menu.Items.Add(editItem);

            DevExpress.Web.MenuItem editPattern = new DevExpress.Web.MenuItem("Edit", "EditPattern");
            editPattern.BeginGroup = true;
            editPattern.Image.Url = this.ResolveUrl("~/images/icons/pencil.png");
            e.Menu.Items.Add(editPattern);

            DevExpress.Web.MenuItem deleteItem = new DevExpress.Web.MenuItem("Delete", "DeleteId");
            deleteItem.BeginGroup = true;
            deleteItem.Image.Url = this.ResolveUrl("~/images/icons/delete.png");
            e.Menu.Items.Add(deleteItem);
        }
    }

    protected void PriceScheduleDataSource_ObjectCreated(object sender, ObjectDataSourceEventArgs e)
    {
        e.ObjectInstance = new PriceScheduleDataSource(this.PriceScheduleId);

    }

    #endregion
}