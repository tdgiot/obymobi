﻿using System;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Catalog.SubPanels
{
    public partial class CategoryCulturePanel : Dionysos.Web.UI.WebControls.SubPanelLLBLGenEntity
    {
        #region Properties

        public CategoryEntity ParentDataSourceAsCategoryEntity
        {
            get
            {
                return this.PageAsPageLLBLGenEntity.DataSource as CategoryEntity;
            }
        }

        public string CultureCode {get;set;}
        public bool IsDefaultCulture;

        #endregion

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "CustomText";
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.SetGui();
        }

        #endregion

        #region Methods

        protected void SetGui()
        {
            this.tbCultureName.Text = this.CultureCode;
            this.tbName.Enabled = !this.IsDefaultCulture;
            this.tbDescription.Enabled = !this.IsDefaultCulture;            
        }

        private void PopulateCustomTexts()
        {
            CustomTextRenderer renderer = new CustomTextRenderer(this.ParentDataSourceAsCategoryEntity, this.CultureCode);

            renderer.RenderCustomText(this.tbName, CustomTextType.CategoryName);
            renderer.RenderCustomText(this.tbDescription, CustomTextType.CategoryDescription);
            renderer.RenderCustomText(this.tbOrderProcessedTitle, CustomTextType.CategoryOrderProcessedTitle);
            renderer.RenderCustomText(this.tbOrderProcessedMessage, CustomTextType.CategoryOrderProcessedText);
            renderer.RenderCustomText(this.tbOrderConfirmationTitle, CustomTextType.CategoryOrderConfirmationTitle);
            renderer.RenderCustomText(this.tbOrderConfirmationText, CustomTextType.CategoryOrderConfirmationText);
            renderer.RenderCustomText(this.tbButtonText, CustomTextType.CategoryButtonText);
            renderer.RenderCustomText(this.tbCustomizeButtonText, CustomTextType.CategoryCustomizeButtonText);
        }

        protected override void OnParentDataSourceLoaded()
        {
            base.OnParentDataSourceLoaded();
            this.PopulateCustomTexts();
        }

        public override bool Save()
        {
            this.SaveCustomTexts();
            return false;
        }

        private void SaveCustomTexts()
        {
            CustomTextRenderer renderer = new CustomTextRenderer(this.ParentDataSourceAsCategoryEntity, this.CultureCode);

            renderer.SaveCustomText(this.tbName, CustomTextType.CategoryName);
            renderer.SaveCustomText(this.tbDescription, CustomTextType.CategoryDescription);
            renderer.SaveCustomText(this.tbOrderProcessedTitle, CustomTextType.CategoryOrderProcessedTitle);
            renderer.SaveCustomText(this.tbOrderProcessedMessage, CustomTextType.CategoryOrderProcessedText);
            renderer.SaveCustomText(this.tbOrderConfirmationTitle, CustomTextType.CategoryOrderConfirmationTitle);
            renderer.SaveCustomText(this.tbOrderConfirmationText, CustomTextType.CategoryOrderConfirmationText);
            renderer.SaveCustomText(this.tbButtonText, CustomTextType.CategoryButtonText);
            renderer.SaveCustomText(this.tbCustomizeButtonText, CustomTextType.CategoryCustomizeButtonText);
        }

        #endregion
    }
}