﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Catalog.SubPanels.CategoryProductPanel" Codebehind="CategoryProductPanel.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v20.1" Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dxwtl" %>
<%@ Register Assembly="Dionysos.Web" Namespace="Dionysos.Web.UI.WebControls" TagPrefix="cc1" %>
<table cellpadding="0" cellspacing="10">
    <tr>
        <td>
            <D:Label runat="server" ID="lblAddProduct">Add product</D:Label>
        </td>
        <td style="padding-left: 4px; width: 336px;">
            <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlAddProduct" EntityName="Product" notdirty="true" IncrementalFilteringMode="StartsWith" TextField="Name" ValueField="ProductId" PreventEntityCollectionInitialization="true"></X:ComboBoxLLBLGenEntityCollection>
        </td>
        <td>
            <D:Button runat="server" ID="btAddProduct" Text="Add" />
        </td>
    </tr>
</table>
<D:Button runat="server" ID="btOrderByName" Text="Sorteer op Naam" /><br /><br />
&nbsp;
<table>
    <D:Label ID="lblDragTip" runat="server"><small>Note: sleep met de producten om de sorteer volgorde te veranderen.</small></D:Label>
    <X:TreeList ID="tlProducts" ClientInstanceName="tlProducts" OnCustomCallback="Products_CustomCallback" runat="server" AutoGenerateColumns="False" KeyFieldName="ProductCategoryId" Width="100%">            
        <settingsbehavior autoexpandallnodes="True" />
        <Settings GridLines="Horizontal" />
        <SettingsEditing AllowNodeDragDrop="true"/>
        <SettingsSelection Enabled="true" />
        <ClientSideEvents EndDragNode="function(s, e){
            e.cancel = true;
            var key = s.GetNodeKeyByRow(e.targetElement);
            tlProducts.PerformCustomCallback('reorder' + ':' + e.nodeKey + ':' + key);
        }" />
        <Columns>                    
            <dxwtl:TreeListHyperLinkColumn caption="Naam" fieldname="ProductId" visibleindex="1" CellStyle-HorizontalAlign="Left">
                <PropertiesHyperLink TextField="ProductName" NavigateUrlFormatString="../Product.aspx?id={0}" >
                </PropertiesHyperLink>           
                <EditFormSettings VisibleIndex="0" />
            </dxwtl:TreeListHyperLinkColumn>            
            <dxwtl:TreeListDataColumn caption="Volgorde" fieldname="SortOrder" visibleindex="2" CellStyle-HorizontalAlign="Right">
                <EditFormSettings VisibleIndex="1" />
            </dxwtl:TreeListDataColumn>  
        </Columns>        
    </X:TreeList><br />
    <div id="divCategoryProductBottomButtons">
        <D:Button runat="server" ID="btDeleteProduct" Text="Verwijder geselecteerd(e) product(en)" />
    </div>
</table>


