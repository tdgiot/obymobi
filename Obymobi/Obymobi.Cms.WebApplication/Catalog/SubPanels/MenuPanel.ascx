﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Catalog.SubPanels.MenuPanel" Codebehind="MenuPanel.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v20.1" Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dxwtl" %>
<%@ Register Assembly="Dionysos.Web" Namespace="Dionysos.Web.UI.WebControls" TagPrefix="cc1" %>
 <X:TreeList ID="tlCategories" runat="server" AutoGenerateColumns="False" KeyFieldName="CategoryId" ParentFieldName="ParentCategoryId" Width="100%">            
    <settingsbehavior autoexpandallnodes="True" />
    <Settings GridLines="Horizontal" />
    <Columns>                    
        <dxwtl:TreeListHyperLinkColumn caption="Naam" fieldname="CategoryId" visibleindex="0" CellStyle-HorizontalAlign="Left">
            <PropertiesHyperLink TextField="Name" NavigateUrlFormatString="Category.aspx?id={0}" >
            </PropertiesHyperLink>           
        </dxwtl:TreeListHyperLinkColumn>            
        <dxwtl:TreeListDataColumn caption="Volgorde" fieldname="SortOrder" visibleindex="1" width="60px" CellStyle-HorizontalAlign="Left">
        </dxwtl:TreeListDataColumn>                        
    </Columns>        
</X:TreeList>    