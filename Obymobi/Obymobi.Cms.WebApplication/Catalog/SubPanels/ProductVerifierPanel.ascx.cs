﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Dionysos;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Dionysos.Web.UI.WebControls;

namespace Obymobi.ObymobiCms.Catalog.SubPanels
{
	public partial class ProductVerifierPanel : MenuVerifierPanel
	{
		#region Fields
		#endregion

		#region Methods

		public override void RenderInformation()
		{
			// Name
			if (this.ProductmenuItem.Name.IsNullOrWhiteSpace())
			{
				this.lblName.CssClass = "error";
				this.AddError("Naam ontbreekt");
			}
			this.lblNameValue.Text = this.ProductmenuItem.Name;
			this.lblNameValue.NavigateUrl = string.Format("~/Catalog/Product.aspx?id={0}", this.ProductmenuItem.Id);

			// Posproduct
			this.lblPosproduct.Visible = this.UsePos;
			this.lblPosproductValue.Visible = this.UsePos;

			if (this.UsePos)
			{
				if (!this.Product.PosproductId.HasValue)
				{
					this.lblPosproduct.CssClass = "error";
					this.AddError("Geen Kassaproduct gekoppeld");
				}
				else
				{
					this.lblPosproductValue.NavigateUrl = string.Format("~/Catalog/Posproduct.aspx?id={0}", this.Product.PosproductId);
					this.lblPosproductValue.Text = this.Product.PosproductEntity.Name;
				}
			}

			// Generic Product			
			if (this.Product.GenericproductId.HasValue)
			{
				this.lblGenericProductValue.Text = this.Product.GenericproductEntity.Name;
				this.lblGenericProductValue.NavigateUrl = string.Format("~/Genericproducts/Genericproduct.aspx?id={0}", this.Product.GenericproductId);
			}
			else
			{
				this.lblGenericProductValue.Text = "N/A";
				this.lblGenericProduct.CssClass = "warning";
				this.AddWarning("Geen basisproduct");
			}

			// Price
			if (this.Product.PriceIn == 0)
			{
				this.lblPrice.CssClass = "error";
				this.AddWarning("Prijs is € 0,00");
			}
			this.lblPriceValue.Text = (Convert.ToDecimal(this.ProductmenuItem.PriceIn) / Convert.ToDecimal(100.0)).ToString("N2");

			// Show on homepage
			this.lblShowOnHomepageValue.Text = this.ProductmenuItem.DisplayOnHomepage ? "Ja" : "Nee";

			// Colors
			this.lblForegrounColor.Text = this.ProductmenuItem.TextColor.IsNullOrWhiteSpace() ? "N/A" : this.ProductmenuItem.TextColor;
			this.lblBackgroundColor.Text = this.ProductmenuItem.BackgroundColor.IsNullOrWhiteSpace() ? "N/A" : this.ProductmenuItem.BackgroundColor;

			if (this.ProductmenuItem.TextColor.IsNullOrWhiteSpace() || this.ProductmenuItem.BackgroundColor.IsNullOrWhiteSpace())
			{
				this.lblColors.CssClass = "error";
				this.AddError("Kleuren niet ingesteld");
			}

			// Images
            var buttonImage = this.ProductmenuItem.Media.Any(m => (int)MediaType.ProductButton == m.MediaType);
            var brandingImage = this.ProductmenuItem.Media.Any(m => (int)MediaType.ProductBranding == m.MediaType);
            var largeImage = this.ProductmenuItem.Media.Any(m => (int)MediaType.ProductPhoto == m.MediaType);

			if (!buttonImage || !brandingImage || !largeImage)
			{
				this.lblImages.CssClass = "error";
				this.AddError("Er ontbreken 1 of meer afbeeldingen");
			}
			else
			{
				if (buttonImage)
                    this.lblImagesButton.NavigateUrl = string.Format("~/Generic/Media.aspx?id={0}", this.ProductmenuItem.Media.First(m => (int)MediaType.ProductButton == m.MediaType).MediaId);

				if (brandingImage)
                    this.lblImagesBranding.NavigateUrl = string.Format("~/Generic/Media.aspx?id={0}", this.ProductmenuItem.Media.First(m => (int)MediaType.ProductBranding == m.MediaType).MediaId);

				if (largeImage)
                    this.lblImagesLarge.NavigateUrl = string.Format("~/Generic/Media.aspx?id={0}", this.ProductmenuItem.Media.First(m => (int)MediaType.ProductPhoto == m.MediaType).MediaId);
			}

			this.lblImagesButton.CssClass = buttonImage ? string.Empty : "error";
			this.lblImagesBranding.CssClass = brandingImage ? string.Empty : "error";
			this.lblImagesLarge.CssClass = largeImage ? string.Empty : "error";

			// Suggestions & Alterations
			this.plhSuggestionsAndAlterations.Visible = this.ShowSuggestionAlterations;

			// Suggestions
			this.SetSuggestions();

			// Alterations		
			this.SetAlterations();

			// Description 
			this.plhDescription.Visible = this.ShowDescriptions;
			if (this.ProductmenuItem.Description.IsNullOrWhiteSpace())
			{
				this.lblDescriptionValue.Text = "N/A";
				this.lblDescription.CssClass = "error";
				this.AddWarning("Beschrijving ontbreekt");
			}
			else
				this.lblDescriptionValue.Text = this.ProductmenuItem.Description;

			// AdvertisementTags
			if (this.ProductmenuItem.AdvertisementTags.Count() == 0)
			{
				this.AddError("Er zijn geen advertentietags ingesteld");
			}

			foreach (var advertisementTag in this.ProductmenuItem.AdvertisementTags)
				this.lblAdvertisementTags.Text = StringUtil.CombineWithSeperator(", ", this.lblAdvertisementTags.Text, advertisementTag.Name);
		}

		#endregion

		#region Event Handlers

		protected void Page_Load(object sender, EventArgs e)
		{
		}

		#endregion

		#region Properties


		/// <summary>
		/// Gets or sets the product
		/// </summary>
		public ProductEntity Product
		{
			get
			{
				return this.GetProductById(this.ProductmenuItem.Id);
			}
		}

		public override Label lblSuggestionsControl { get { return this.lblSuggestions; } }
		public override Label lblAlterationsControl { get { return this.lblAlterations; } }
		public override PlaceHolder plhAlterationsControl { get { return this.plhAlterations; } }
		public override PlaceHolder plhSuggestionsControl { get { return this.plhSuggestions; } }


		#endregion

	}
}