﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Catalog.SubPanels.ProductCulturePanel" Codebehind="ProductCulturePanel.ascx.cs" %>
<table class="dataformV2">
	<tr>
	    <td class="label">
		    <D:LabelEntityFieldInfo runat="server" ID="lblName">Name</D:LabelEntityFieldInfo>
	    </td>
	    <td class="control">
		    <D:TextBoxString runat="server" ID="tbName" UseDataBinding="false"/>
	    </td>
	    <td class="label">
		    <D:Label runat="server" ID="lblCultureName">Culture</D:Label>
	    </td>
	    <td class="control">
		    <D:TextBoxString runat="server" ID="tbCultureName" LocalizeText="false" UseDataBinding="false" ReadOnly="true" />
	    </td>
	</tr>    
	<tr>
	    <td class="label">
	        <D:Label runat="server" id="lblAlterationButtonText">Alteration button Text</D:Label>
	    </td>
	    <td class="control">
	        <D:TextBoxString ID="tbCustomizeButtonText" runat="server" UseDataBinding="false"/>
	    </td>
		<td class="label">
			<D:LabelEntityFieldInfo runat="server" id="lblButtonText">Button Text</D:LabelEntityFieldInfo>
		</td>
		<td class="control">
		    <D:TextBoxString ID="tbButtonText" runat="server" UseDataBinding="false"/>
		</td>
	</tr>    
    <D:PlaceHolder ID="plhNonProductFields" runat="server">
    <tr>
        <td class="label">
		    <D:LabelEntityFieldInfo runat="server" id="lblDescription">Description</D:LabelEntityFieldInfo>
	    </td>
	    <td class="control threeCol" colspan="3">
	        <D:TextBoxMultiLine ID="tbDescription" runat="server" Rows="10" UseDataBinding="false" MaxLength="500"/>
	    </td>
    </tr>
	    <tr>
		    <td class="label">
			    <D:LabelEntityFieldInfo runat="server" id="lblShortDescription">Short Description</D:LabelEntityFieldInfo>
		    </td>
		    <td class="control threeCol" colspan="3">
			    <D:TextBoxMultiLine ID="tbShortDescription" runat="server" Rows="3" UseDataBinding="false" MaxLength="255"/>
		    </td>
	    </tr>
    </D:PlaceHolder>    
    <tr>
        <td class="label">
            <D:LabelEntityFieldInfo runat="server" ID="lblOrderProcessedTitle">Order processed title</D:LabelEntityFieldInfo>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbOrderProcessedTitle" UseDataBinding="false"/>
        </td>
        <td class="label">
            <D:LabelEntityFieldInfo runat="server" ID="lblOrderProcessedMessage">Order processed text</D:LabelEntityFieldInfo>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbOrderProcessedMessage" UseDataBinding="false"/>
        </td>
    </tr>
    <tr>
        <td class="label">
            <D:LabelEntityFieldInfo runat="server" ID="lblOrderConfirmationTitle">Order confirmation title</D:LabelEntityFieldInfo>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbOrderConfirmationTitle" UseDataBinding="false"/>
        </td>
        <td class="label">
            <D:LabelEntityFieldInfo runat="server" ID="lblOrderConfirmationText">Order confirmation text</D:LabelEntityFieldInfo>
        </td>
        <td class="control">
            <D:TextBoxString runat="server" ID="tbOrderConfirmationText" UseDataBinding="false"/>
        </td>
    </tr>
</table>