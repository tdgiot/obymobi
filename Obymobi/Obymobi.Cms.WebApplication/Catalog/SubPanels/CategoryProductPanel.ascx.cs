﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.EntityClasses;
using Dionysos.Web.UI;
using DevExpress.Web.ASPxTreeList;
using DevExpress.Web;
using Obymobi.Enums;

namespace Obymobi.ObymobiCms.Catalog.SubPanels
{
    public partial class CategoryProductPanel : UserControl, Dionysos.Interfaces.ISaveableControl
    {
        #region Fields

        private List<ProductCategoryEntity> orderedProductCategories = null;

        #endregion

        #region Enums

        public enum SortOrder
        {
            NameAsc,
            SortOrderAsc,
            SortOrderDesc
        }

        #endregion

        #region Methods

        private void HookUpEvents()
        {
            this.btOrderByName.Click += (btOrderByName_Click);
            this.btAddProduct.Click += (btAddProduct_Click);
            this.btDeleteProduct.Click += (btDeleteProduct_Click);
        }

        private void ChangeOrder(TreeListNode src, TreeListNode dst)
        {
            if (src == null || dst == null)
            { 
                // Do nothing
            }
            else if (src.DataItem == null || dst.DataItem == null)
            {
                // Still do nothing
            }
            else
            {
                var srcProduct = src.DataItem as ProductCategoryEntity;
                var dstProduct = dst.DataItem as ProductCategoryEntity;

                bool draggedDown = false;

                // Check for which direction the drag occurred
                if (srcProduct.SortOrder < dstProduct.SortOrder)
                    draggedDown = true;

                // Remove the source product
                this.orderedProductCategories.Remove(srcProduct);

                for (int i = 0; i < this.orderedProductCategories.Count; i++)
                {
                    if (this.orderedProductCategories[i] == dstProduct)
                    {
                        if (draggedDown)
                        {
                            // Place it below this item
                            this.orderedProductCategories.Insert(i + 1, srcProduct);
                        }
                        else
                        { 
                            // Place it in front of this item
                            this.orderedProductCategories.Insert(i, srcProduct);
                        }
                        break;
                    }
                }

                // Set the new sortorder for the product categories
                int orderNo = 1;
                foreach (var pc in orderedProductCategories)
                {
                    pc.SortOrder = orderNo;
                    pc.Save();
                    orderNo++;
                }                
            }
        }

        private void RenderProductCategories(SortOrder sortOrder)
        {
            List<ProductCategoryEntity> productCategories = this.GetSortedCategoryProducts(sortOrder);
            
            this.tlProducts.DataSource = productCategories;
            this.tlProducts.DataBind();
        }

        private List<ProductCategoryEntity> GetSortedCategoryProducts(SortOrder sortOrder)
        {
            EntityView<ProductCategoryEntity> productCategoryView = this.DataSourceAsCategoryEntity.ProductCategoryCollection.DefaultView;

            this.orderedProductCategories = new List<ProductCategoryEntity>();
            switch (sortOrder)
            {
                case SortOrder.NameAsc:
                    this.orderedProductCategories = productCategoryView.OrderBy(pc => pc.ProductName).ToList();
                    break;
                case SortOrder.SortOrderDesc:
                    this.orderedProductCategories = productCategoryView.OrderByDescending(pc => pc.SortOrder).ToList();
                    break;
                default:
                    this.orderedProductCategories = productCategoryView.OrderBy(pc => pc.SortOrder).ToList();
                    break;
            }
            return this.orderedProductCategories;
        }

        private void LoadUserControls(object sender)
        {
            if (IsParentCategory(DataSourceAsCategoryEntity.CategoryId))
            {
                string noProductsMessage = "Products cannot be added as this category has sub categories.";
                btAddProduct.Enabled = false;
                ddlAddProduct.Enabled = false;
                btAddProduct.ToolTip = noProductsMessage;
                ddlAddProduct.ToolTip = noProductsMessage;
                return;
            }

            var productCollection = new ProductCollection();
            IncludeFieldsList includeFields = new IncludeFieldsList();
            includeFields.Add(ProductFields.Name);

            IPredicateExpression filter = new PredicateExpression(ProductFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            filter.Add(ProductFields.SubType != ProductSubType.SystemProduct);
            filter.Add(new FieldCompareRangePredicate(ProductFields.ProductId, true, DataSourceAsCategoryEntity.ProductCategoryCollection.Select(x => x.ProductId).ToList()));
            productCollection.GetMulti(filter, 0, new SortExpression(ProductFields.Name | SortOperator.Ascending), null, null, includeFields, 0, 0);
            
            ddlAddProduct.Items.AddRange(productCollection.Select(x => new ListEditItem(x.Name, x.ProductId)).ToList());            
        }

        private bool IsParentCategory(int categoryId) => new CategoryCollection().GetDbCount(CategoryFields.ParentCategoryId == categoryId) > 0;

        private void AddProduct()
        {
            if (this.ddlAddProduct.ValidId > 0)
            {
                var productCategory = new ProductCategoryEntity();                
                productCategory.ProductId = this.ddlAddProduct.ValidId;

                // Check whats the biggest sort order is and 100 on top of that
                int sortOrder = 100;
                foreach (var pc in this.DataSourceAsCategoryEntity.ProductCategoryCollection)
                {
                    if (pc.SortOrder >= sortOrder)
                        sortOrder = pc.SortOrder + 100;
                }

                productCategory.SortOrder = sortOrder;
                this.DataSourceAsCategoryEntity.ProductCategoryCollection.Add(productCategory);
                productCategory.Save();

                // Rerender the treelist
                this.RenderProductCategories(SortOrder.SortOrderAsc);
            }
        }

        private void DeleteProduct()
        {
            // Create entityview
            EntityView<ProductCategoryEntity> pcs = this.DataSourceAsCategoryEntity.ProductCategoryCollection.DefaultView;

            foreach (TreeListNode node in this.tlProducts.Nodes)
            {
                if (node.Selected)
                {
                    int productCategoryId = Int32.Parse(node.Key);

                    // Get the ProductCategory entity
                    pcs.Filter = new PredicateExpression(ProductCategoryFields.ProductCategoryId == productCategoryId);

                    if (pcs.Count > 0)
                    {
                        ProductCategoryEntity productCategory = pcs[0];

                        pcs.RelatedCollection.Remove(productCategory);
                        productCategory.Delete();
                    }
                }
            }

            pcs.Filter = null;

            this.RenderProductCategories(SortOrder.SortOrderAsc);
        }

        private void OrderByName()
        {
            var categoryProducts = GetSortedCategoryProducts(SortOrder.NameAsc);
            var sortIndex = 100;
            foreach (var categoryProduct in categoryProducts)
            {
                categoryProduct.SortOrder = sortIndex;
                sortIndex += 100;

                categoryProduct.Save();
            }

            this.RenderProductCategories(SortOrder.SortOrderAsc);
        }

        #endregion

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            (Parent.Page as PageLLBLGenEntity).DataSourceLoaded += LoadUserControls;
            this.HookUpEvents();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        private void btOrderByName_Click(object sender, EventArgs e)
        {
            this.OrderByName();            
        }

        private void btAddProduct_Click(object sender, EventArgs e)
        {
            this.AddProduct();            
        }

        private void btDeleteProduct_Click(object sender, EventArgs e)
        {
            this.DeleteProduct();
        }

        public bool Save()
        {
            return true;
        }

        protected override void CreateChildControls()
        {
            this.RenderProductCategories(SortOrder.SortOrderAsc);
            base.CreateChildControls();
        }

        protected void Products_CustomCallback(object sender, TreeListCustomCallbackEventArgs e)
        {
            if (e.Argument.StartsWith("reorder:"))
            {
                string[] arg = e.Argument.Split(':');
                this.ChangeOrder(tlProducts.FindNodeByKeyValue(arg[1]), tlProducts.FindNodeByKeyValue(arg[2]));
                this.RenderProductCategories(SortOrder.SortOrderAsc);
            }
        }

        #endregion

        #region Properties

        public new PageLLBLGenEntity Page
        {
            get
            {
                return base.Page as PageLLBLGenEntity;
            }
        }

        public CategoryEntity DataSourceAsCategoryEntity
        {
            get
            {
                return this.Page.DataSource as CategoryEntity;
            }
        }

        #endregion

    }
}