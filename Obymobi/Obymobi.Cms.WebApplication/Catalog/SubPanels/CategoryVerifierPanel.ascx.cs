﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Obymobi.Logic.Model;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;
using Dionysos;
using Obymobi.Enums;
using Dionysos.Web.UI.WebControls;

namespace Obymobi.ObymobiCms.Catalog.SubPanels
{
	public partial class CategoryVerifierPanel : MenuVerifierPanel
	{
		#region Fields
		#endregion

		#region Methods

		public override void RenderInformation()
		{ 
			// Name
			if (this.ProductmenuItem.Name.IsNullOrWhiteSpace())
			{
				this.lblName.CssClass = "error";
				this.AddError("Naam ontbreekt");
			}			
			this.lblNameValue.Text = this.ProductmenuItem.Name;
			this.lblNameValue.NavigateUrl = string.Format("~/Catalog/Category.aspx?id={0}", this.Category.CategoryId);

			// SortOrder			
			this.lblSortOrderValue.Text = this.ProductmenuItem.SortOrder.ToString();

			// Hierarchy
			this.lblHierachyValue.Text = this.Category.FullCategoryName;

			if(this.Category.GenericcategoryId.HasValue)
			{
				this.lblGenericCategoryNameValue.Text = this.Category.GenericcategoryEntity.Name;
				this.lblGenericCategoryNameValue.NavigateUrl = "~/Genericproducts/Genericcategory.aspx?id=" + this.Category.GenericcategoryId.Value.ToString();
			}
			else
			{				
				this.lblGenericCategoryNameValue.Text = "N/A";
				this.lblGenericCategoryName.CssClass = "warning";
				this.AddWarning("Geen basiscategorie");			
			}

			// Image
			bool buttonImage = false;
			
			if(this.ProductmenuItem.Media != null)
                buttonImage = this.ProductmenuItem.Media.Any(m => (int)MediaType.CategoryButton == m.MediaType);

			if (!buttonImage)
			{
				this.lblImages.CssClass = "error";
				this.AddError("Er is geen afbeelding voor de knop");
			}
			else
			{
                var buttonImageMedia = this.ProductmenuItem.Media.First(m => (int)MediaType.CategoryButton == m.MediaType);
				this.lblImagesButton.NavigateUrl = "~/Generic/Media.aspx?id=" + buttonImageMedia.MediaId;
			}

			this.lblImagesButton.CssClass = buttonImage ? string.Empty : "error";

			// Suggestions & Alterations
			this.plhSuggestionsAndAlterations.Visible = this.ShowSuggestionAlterations;
			
			// Suggestions
			this.SetSuggestions();

			// Alterations		
			this.SetAlterations();			

			// Description > Not relevant for category

			// AdvertisementTags
			if (this.ProductmenuItem.AdvertisementTags.Count() == 0)
			{
				this.AddError("Er zijn geen advertentietags ingesteld");
			}

			foreach (var advertisementTag in this.ProductmenuItem.AdvertisementTags)			 
				this.lblAdvertisementTags.Text = StringUtil.CombineWithSeperator(", ", this.lblAdvertisementTags.Text, advertisementTag.Name);			

			// Product Layouts
			SetProductLayout();

			// Child products
			var products = this.ProductmenuItems.Where(pi => pi.ParentProductmenuItemId == this.ProductmenuItem.Id && pi.ItemType != 100).OrderBy(pi => pi.SortOrder);
			foreach (var product in products)
			{
				MenuVerifierPanel panel = this.Page.LoadControl("~/Catalog/SubPanels/ProductVerifierPanel.ascx") as MenuVerifierPanel;
				panel.ProductmenuItem = product;
				panel.CategoryView = this.CategoryView;
				panel.Errors = this.Errors;
				panel.OnlyWarnings = this.OnlyWarnings;
				panel.ProductmenuItems = this.ProductmenuItems;
				panel.ProductView = this.ProductView;
				panel.ShowAdvertisementTags = this.ShowAdvertisementTags;
				panel.ShowAlterationOptions = this.ShowAlterationOptions;
				panel.ShowCategoryLayouts = this.ShowCategoryLayouts;
				panel.ShowDescriptions = this.ShowDescriptions;
				panel.ShowProducts = this.ShowProducts;
				panel.ShowSuggestionAlterations = this.ShowSuggestionAlterations;
				panel.UsePos = this.UsePos;
				panel.Warnings = this.Warnings;
				panel.RenderInformation();

				this.plhProducts.Controls.Add(panel);
			}

			this.plhProducts.Visible = this.ShowProducts;
		}

		private void SetProductLayout()
		{
			var subItemsTemp = this.ProductmenuItems.Where(pi => pi.ParentProductmenuItemId == this.ProductmenuItem.Id);
			var subItems = subItemsTemp.ToArray();
			int subItemCount = subItems.Count();
			if (subItemCount == 0)
				this.AddError("Zijn geen producten of categorieën aanwezig");


			if (this.ShowCategoryLayouts)
			{
				// Get all items in this Category
				for (int iItems = 0; iItems < subItemCount; iItems++)
				{
					var subItem = subItems[iItems];

					if (iItems == 0 || iItems % 10 == 0)
					{
						// Start new table
						this.plhLayoutHtml.AddHtml("<table class=\"categoryLayout\">");
					}

					if (iItems == 0 || iItems % 2 == 0)
					{
						// Start new row
						this.plhLayoutHtml.AddHtml("<tr>");
					}

					// Render info
					if (subItem.ItemType == 100)
					{
						// Category
						int categoryId = subItem.Id - 100000000;
						string hrefToCategory = string.Format("<a href=\"Category.aspx?id={0}\">{1}</a>", categoryId, subItem.Name);
						this.plhLayoutHtml.AddHtml("<td class=\"category\"><span class=\"title\">{0}</span><br /><span class=\"type\">[[CATEGORIE]]</span></td>", hrefToCategory);
					}
					else
					{
						// Product
						string hrefToProduct = string.Format("<a href=\"Product.aspx?id={0}\">{1}</a>", subItem.Id, subItem.Name);
						this.plhLayoutHtml.AddHtml("<td class=\"product\"><span class=\"title\">{0}</span><br /><span class=\"type\">[[PRODUCT]]</span></td>", hrefToProduct);
					}

					if ((iItems + 1) % 2 == 0)
					{
						// Start new row
						this.plhLayoutHtml.AddHtml("</tr>");
					}

					if ((iItems + 1) % 10 == 0)
					{
						// End table
						this.plhLayoutHtml.AddHtml("</table>");
					}
				}

				// Close table if required				
				if (subItemCount > 0 && ((subItemCount) % 2 != 0 || ((subItemCount / 10 >= 1) && subItemCount % 10 != 0)))
					this.plhLayoutHtml.AddHtml("</tr></table>");
				else if(subItemCount < 10)
					this.plhLayoutHtml.AddHtml("</table>");
			}

			
		}

		#endregion

		#region Event Handlers

		protected void Page_Load(object sender, EventArgs e)
		{			
		}

		#endregion

		#region Properties

		public override Label lblSuggestionsControl { get { return this.lblSuggestions; } }
		public override Label lblAlterationsControl { get { return this.lblAlterations; } }
		public override PlaceHolder plhAlterationsControl { get { return this.plhAlterations; } }
		public override PlaceHolder plhSuggestionsControl { get { return this.plhSuggestions; } }

		/// <summary>
		/// Gets or sets the underlyingCategory
		/// </summary>
		public CategoryEntity Category
		{
			get
			{
				return this.GetCategoryById(this.ProductmenuItem.Id - 100000000);
			}		
		}		

		#endregion

	}
}