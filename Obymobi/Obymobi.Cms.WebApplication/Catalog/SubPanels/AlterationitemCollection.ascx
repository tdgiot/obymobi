﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Catalog.Subpanels.AlterationitemCollection" Codebehind="AlterationitemCollection.ascx.cs" %>
<div style="padding-bottom: 11px;">	
    <D:Panel ID="pnlName" runat="server" GroupingText="Naam" CssClass="alteration_panel">
        <table class="dataformV2">
	        <tr>
		        <td class="label">
                    <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlAlterationoptionId" PreventEntityCollectionInitialization="true" EntityName="Alterationoption" IncrementalFilteringMode="StartsWith" TextField="NameAndPriceIn" ValueField="AlterationoptionId" notdirty="true" TabIndex="0"></X:ComboBoxLLBLGenEntityCollection>
		        </td>            
	        </tr>
            <tr>
                <td class="label">
                    <D:TextBoxString ID="tbAlterationName" runat="server" LocalizeDefaultValue="false" DefaultValue="Name" notdirty="true" TabIndex="3"></D:TextBoxString>
                </td>
            </tr>
        </table>
    </D:Panel>
    <D:Panel ID="pnlSortOrder" runat="server" GroupingText="Sorteer volgorde" CssClass="alteration_panel">
        <table class="dataformV2">
	        <tr>
		        <td class="label">
                    <D:TextBoxInt ID="tbSortOrderExisting" runat="server" LocalizeDefaultValue="false" ThousandsSeperators="false" notdirty="true" TabIndex="1">1000</D:TextBoxInt>
		        </td>            
	        </tr>
            <tr>
                <td class="label">
                    <D:TextBoxInt ID="tbSortOrderNew" runat="server" LocalizeDefaultValue="false" ThousandsSeperators="false" notdirty="true" TabIndex="4">1000</D:TextBoxInt>
                </td>
            </tr>
        </table>
    </D:Panel>
    <D:Panel ID="pnlPrice" runat="server" GroupingText="Prijs" CssClass="alteration_panel">
        <table class="dataformV2" style="margin-top:23px;">
	        <tr>
		        <td class="label">
                    
		        </td>            
	        </tr>
            <tr>
                <td class="label">
                    <D:TextBoxInt ID="tbPriceIn" runat="server" LocalizeDefaultValue="false" ThousandsSeperators="false" notdirty="true" TabIndex="5">0.00</D:TextBoxInt>
                </td>
            </tr>
        </table>
    </D:Panel>
    <D:Panel ID="pnlActions" runat="server" GroupingText="" CssClass="alteration_panel">
        <table class="dataformV2" style="margin-top:8px;">
	        <tr>           
                <td class="label" style="text-align:left;">
                    <D:Button runat="server" ID="btnAddExisting" Text="Bestaande optie toevoegen" ToolTip="Toevoegen" Style="margin-top: 3px;" TabIndex="2" />
                </td>
	        </tr>
            <tr>                       
                <td class="label" style="text-align:left">
                    <D:Button runat="server" ID="btnAddNew" Text="Nieuwe optie toevoegen" ToolTip="Toevoegen" Style="margin-top: 3px;" TabIndex="6" />
                </td>            
            </tr>
        </table>    
    </D:Panel>    
</div>
<X:GridViewColumnSelector runat="server" ID="gvcsAlterationitem">
	<Columns />
</X:GridViewColumnSelector>

