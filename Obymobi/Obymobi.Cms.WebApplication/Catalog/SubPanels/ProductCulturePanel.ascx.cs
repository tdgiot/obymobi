﻿using System;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Catalog.SubPanels
{
    public partial class ProductCulturePanel : Dionysos.Web.UI.WebControls.SubPanelLLBLGenEntity
    {
        #region Properties        

        public ProductEntity ParentDataSourceAsProductEntity
        {
            get
            {
                return this.PageAsPageLLBLGenEntity.DataSource as ProductEntity;
            }
        }

        public string CultureCode {get;set;}
        public bool IsDefaultCulture;

        #endregion

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "CustomText";
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.SetGui();
        }

        #endregion

        #region Methods

        protected void SetGui()
        {
            this.tbCultureName.Text = this.CultureCode;
            this.tbName.Enabled = !this.IsDefaultCulture;
            this.tbButtonText.Enabled = !this.IsDefaultCulture;
            this.tbDescription.Enabled = !this.IsDefaultCulture;
            this.tbShortDescription.Enabled = !this.IsDefaultCulture;
            this.tbCustomizeButtonText.Enabled = !this.IsDefaultCulture;
        }

        private void PopulateCustomTexts()
        {
            CustomTextRenderer renderer = new CustomTextRenderer(this.ParentDataSourceAsProductEntity, this.CultureCode);

            renderer.RenderCustomText(this.tbName, CustomTextType.ProductName);
            renderer.RenderCustomText(this.tbButtonText, CustomTextType.ProductButtonText);
            renderer.RenderCustomText(this.tbDescription, CustomTextType.ProductDescription);
            renderer.RenderCustomText(this.tbShortDescription, CustomTextType.ProductShortDescription);
            renderer.RenderCustomText(this.tbOrderProcessedTitle, CustomTextType.ProductOrderProcessedTitle);
            renderer.RenderCustomText(this.tbOrderProcessedMessage, CustomTextType.ProductOrderProcessedText);
            renderer.RenderCustomText(this.tbOrderConfirmationTitle, CustomTextType.ProductOrderConfirmationTitle);
            renderer.RenderCustomText(this.tbOrderConfirmationText, CustomTextType.ProductOrderConfirmationText);
            renderer.RenderCustomText(this.tbCustomizeButtonText, CustomTextType.ProductCustomizeButtonText);
        }

        protected override void OnParentDataSourceLoaded()
        {
            base.OnParentDataSourceLoaded();
            this.PopulateCustomTexts();
        }

        public override bool Save()
        {
            this.SaveCustomTexts();
            return false;
        }

        private void SaveCustomTexts()
        {
            CustomTextRenderer renderer = new CustomTextRenderer(this.ParentDataSourceAsProductEntity, this.CultureCode);

            renderer.SaveCustomText(this.tbName, CustomTextType.ProductName);
            renderer.SaveCustomText(this.tbButtonText, CustomTextType.ProductButtonText);
            renderer.SaveCustomText(this.tbDescription, CustomTextType.ProductDescription);
            renderer.SaveCustomText(this.tbShortDescription, CustomTextType.ProductShortDescription);
            renderer.SaveCustomText(this.tbOrderProcessedTitle, CustomTextType.ProductOrderProcessedTitle);
            renderer.SaveCustomText(this.tbOrderProcessedMessage, CustomTextType.ProductOrderProcessedText);
            renderer.SaveCustomText(this.tbOrderConfirmationTitle, CustomTextType.ProductOrderConfirmationTitle);
            renderer.SaveCustomText(this.tbOrderConfirmationText, CustomTextType.ProductOrderConfirmationText);
            renderer.SaveCustomText(this.tbCustomizeButtonText, CustomTextType.ProductCustomizeButtonText);
        }

        #endregion
    }
}