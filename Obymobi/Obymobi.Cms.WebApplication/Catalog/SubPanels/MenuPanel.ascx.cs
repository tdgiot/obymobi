﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dionysos.Web.UI;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Web.UI.DevExControls;
using Obymobi.Logic.HelperClasses;
using DevExpress.Web.ASPxTreeList;

namespace Obymobi.ObymobiCms.Catalog.SubPanels
{
    public partial class MenuPanel : System.Web.UI.UserControl
    {
        #region Fields

        MenuEntity menuEntity = null;

        #endregion

        #region Methods

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);            
        }

        #endregion

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        /*void AddItemToLastItem(TreeListNode node)
        {
            if (node.HasChildren)
            {
                for (int i = 0; i < node.ChildNodes.Count; i++)
                {
                    this.AddItemToLastItem(node.ChildNodes[i]);
                }
            }

            TreeListNode nodeNew = tlCategories.AppendNode(node.Key + "_New", node);
            nodeNew["Name"] = string.Format("<a href=\"Category.aspx?mode=add&Entity=Category&ParentCategoryId={0}\">Item toevoegen...</a>", node.Key);
        }*/

        public void setMenu(MenuEntity menu)
        {
            menuEntity = menu;

            CategoryCollection categories = new CategoryCollection();
            categories.GetMulti(new PredicateExpression(CategoryFields.CompanyId == CmsSessionHelper.CurrentCompanyId));

            EntityView<CategoryEntity> navItemView = categories.DefaultView;
            SortExpression sort = new SortExpression(CategoryFields.ParentCategoryId | SortOperator.Ascending);
            navItemView.Sorter = sort;
            this.tlCategories.DataSource = navItemView;
            this.tlCategories.DataBind();
        }

        #endregion

        #region Properties

        public new PageLLBLGenEntity Page
        {
            get
            {
                return base.Page as PageLLBLGenEntity;
            }
        }

        #endregion

    }
}