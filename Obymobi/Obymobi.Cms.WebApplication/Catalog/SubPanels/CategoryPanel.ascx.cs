﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxTreeList;
using Dionysos.Web.UI;
using Dionysos.Web.UI.DevExControls;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Catalog.SubPanels
{
    public partial class CategoryPanel : System.Web.UI.UserControl, Dionysos.Interfaces.ISaveableControl
    {

        #region Fields

        List<int> categoryIds = null;

        #endregion

        #region Methods

        public void Initialize()
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ProductCategoryFields.ProductId == this.DataSourceAsProductEntity.ProductId);

            IncludeFieldsList includes = new IncludeFieldsList();
            includes.Add(ProductCategoryFields.CategoryId);
            includes.Add(ProductCategoryFields.ParentCompanyId);

            ProductCategoryCollection productCategories = new ProductCategoryCollection();
            productCategories.GetMulti(filter, includes, null);

            this.categoryIds = productCategories.Select(pc => pc.CategoryId).ToList();
        }

        #endregion

        #region Event Handlers

        private void createMenuTree()
        {
            MenuCollection menus = new MenuCollection();
            menus.GetMulti(new PredicateExpression(MenuFields.CompanyId == CmsSessionHelper.CurrentCompanyId));

            if (menus.Count > 0)
            {
                if (menus.Count > 1)
                {
                    PageControl tabsMenus = new PageControl();
                    tabsMenus.LocalizeTabPageTitles = false;
                    tabsMenus.Width = Unit.Percentage(100);
                    foreach (MenuEntity menu in menus)
                    {
                        TabPage page = new TabPage();
                        page.Text = menu.Name;
                        page.Name = string.Format("menu-{0}", menu.MenuId);

                        ASPxTreeList tlMenu = this.renderTreeMenu(menu);

                        page.Controls.Add(tlMenu);
                        tabsMenus.TabPages.Add(page);
                    }

                    this.plhMenus.Controls.Add(tabsMenus);
                }
                else
                {
                    ASPxTreeList tlMenu = this.renderTreeMenu(menus[0]);
                    this.plhMenus.Controls.Add(tlMenu);
                }
            }
        }

        protected override void CreateChildControls()
        {
            base.CreateChildControls();

            this.createMenuTree();
        }

        private ASPxTreeList renderTreeMenu(MenuEntity menu)
        {
            ASPxTreeList tlMenu = new ASPxTreeList();
            //this.plhMenus.Controls.Add(tlMenu);

            // Treelist settings
            tlMenu.KeyFieldName = "CategoryId";
            tlMenu.ParentFieldName = "ParentCategoryId";
            tlMenu.AutoGenerateColumns = false;
            tlMenu.SettingsBehavior.AutoExpandAllNodes = true;
            tlMenu.Settings.GridLines = GridLines.Horizontal;
            tlMenu.Width = Unit.Percentage(100);
            tlMenu.SettingsEditing.Mode = TreeListEditMode.EditFormAndDisplayNode;
            tlMenu.SettingsSelection.Enabled = true;
            tlMenu.ID = "tlMenu-" + menu.MenuId.ToString();
            tlMenu.EnableCallbacks = true;
            tlMenu.ClientInstanceName = "tlMenu-" + menu.MenuId.ToString();
            
            // Name Column
            TreeListHyperLinkColumn tlcName = new TreeListHyperLinkColumn();
            tlcName.Caption = "Name";
            tlcName.FieldName = "CategoryId";
            tlcName.CellStyle.HorizontalAlign = HorizontalAlign.Left;
            tlcName.PropertiesHyperLink.TextField = "Name";
            tlcName.PropertiesHyperLink.NavigateUrlFormatString = "~/Catalog/Category.aspx?id={0}";
            //string url = ResolveUrl(string.Format("~/Catalog/Category.aspx?id={0}", this.DataSourceAsSurveyEntity.SurveyId));
            tlMenu.Columns.Add(tlcName);
            
            // Bind the data
            tlMenu.DataBound += new EventHandler(Menu_DataBound);
            tlMenu.DataSource = menu.CategoryCollection;

            if (!IsPostBack)
                tlMenu.DataBind();

            return tlMenu;
        }

        void Menu_DataBound(object sender, EventArgs e)
        {
            ASPxTreeList tlMenu = (ASPxTreeList)sender;
            
            foreach (TreeListNode node in tlMenu.Nodes)
            {
                this.checkCategoryNode(node);
            }
        }

        private void checkCategoryNode(TreeListNode node)
        {
            int categoryId = int.Parse(node.Key);

            if (categoryId > 0 && categoryIds.Contains(categoryId))
            {
                // Found it!
                node.Selected = true;
            }

            // Check his child nodes
            foreach (TreeListNode childNode in node.ChildNodes)
                this.checkCategoryNode(childNode);
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private void SaveMenus(Control control)
        {
            if (control is ASPxTreeList)
            {
                ASPxTreeList tlMenu = (ASPxTreeList)control;

                string[] values = tlMenu.ID.Split('-');
                int menuId = int.Parse(values[1]);

                // Each node (category)
                foreach (TreeListNode node in tlMenu.Nodes)
                {
                    this.SaveCheckCategoryNodeCategory(node);
                }
            }
            else
            {
                foreach (Control childControl in control.Controls)
                {
                    this.SaveMenus(childControl);
                }
            }
        }

        public bool Save()
        {
            Transaction transaction = new Transaction(System.Data.IsolationLevel.ReadCommitted, "The Game");
            try
            {
                // Each treelist (Menu)
                foreach (Control control in plhMenus.Controls)
                {
                    SaveMenus(control);
                }
                
                transaction.Commit();
            }
            catch 
            {
                transaction.Rollback();
                transaction.Dispose();
            }
            finally
            {
                transaction.Dispose();
            }
            return true;
        }

        private void SaveCheckCategoryNodeCategory(TreeListNode node)
        {
            int categoryId = int.Parse(node.Key);
            ProductCategoryEntity productCategory = this.DataSourceAsProductEntity.ProductCategoryCollection.FirstOrDefault(pc => pc.CategoryId == categoryId);
            
            if (node.Selected)
            {
                // Category is selected

                // Check if the categoryId is in the list
                if (productCategory == null)
                {
                    // Add this category
                    ProductCategoryEntity newPc = new ProductCategoryEntity();
                    newPc.ProductId = this.DataSourceAsProductEntity.ProductId;
                    newPc.CategoryId = categoryId;
                    newPc.AddToTransaction(this.DataSourceAsProductEntity);
                    newPc.Save();
                }
            }
            else
            {
                // Category is not selected

                // Check if categoryId is in the list
                if (productCategory != null)
                {
                    // Delete this category
                    productCategory.AddToTransaction(this.DataSourceAsProductEntity);
                    productCategory.Delete();
                    this.DataSourceAsProductEntity.ProductCategoryCollection.Remove(productCategory);
                }
            }

            foreach (TreeListNode childNode in node.ChildNodes)
                this.SaveCheckCategoryNodeCategory(childNode);
        }

        #endregion

        #region Properties

        public new PageLLBLGenEntity Page
        {
            get
            {
                return base.Page as PageLLBLGenEntity;
            }
        }

        public ProductEntity DataSourceAsProductEntity
        {
            get
            {
                return this.Page.DataSource as ProductEntity;
            }
        }

        #endregion

    }
}