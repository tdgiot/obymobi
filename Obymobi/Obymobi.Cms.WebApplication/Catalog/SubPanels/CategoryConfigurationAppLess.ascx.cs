﻿using System;
using System.Linq;
using System.Web.UI;
using Dionysos.Interfaces;
using Dionysos.Web.UI;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;

namespace Obymobi.Cms.WebApplication.Catalog.SubPanels
{
    public partial class CategoryConfigurationAppLess : UserControl, ISaveableControl
    {
        #region Fields

        private CategoryEntity _categoryEntity;

        #endregion

        #region Events

        protected override void OnInit(EventArgs e) => base.OnInit(e);

        #endregion

        #region Properties

        public bool HasAppLessApplicationConfiguration => this._categoryEntity?.CompanyEntity?.ApplicationConfigurationCollection?.Any() ?? false;

        public new PageLLBLGenEntity Page => base.Page as PageLLBLGenEntity;

        #endregion

        #region Methods

        public new void Load(CategoryEntity categoryEntity)
        {
            this._categoryEntity = categoryEntity;

            this.SetGui();
        }

        public bool Save()
        {
            if (this._categoryEntity == null)
            {
                return true;
            }

            CategoryViewType categoryViewType = this.DetermineCategoryViewType();
            this._categoryEntity.ViewType = categoryViewType;

            return this._categoryEntity.Save();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.HasAppLessApplicationConfiguration)
            {
                UserControl uc = (UserControl)Page.LoadControl("~/AppLess/SubPanels/NoApplicationConfiguration.ascx");

                this.mainTabContent.Controls.Clear();
                this.mainTabContent.Controls.Add(uc);

                return;
            }
        }

        private CategoryViewType DetermineCategoryViewType()
        {
            bool categoryViewTypeParsed = Enum.TryParse(this.ddlCatagoryViewType.Value.ToString(), out CategoryViewType categoryViewType);

            return categoryViewTypeParsed ? categoryViewType : CategoryViewType.Standard;
        }

        private void InitializeCategoryViewType()
        {
            this.ddlCatagoryViewType.DataBindEnum<CategoryViewType>();

            if (!_categoryEntity.IsNew)
            {
                this.ddlCatagoryViewType.Value = (int)this._categoryEntity.ViewType;
            }
        }

        private void LoadUserControls()
        {
            this.InitializeCategoryViewType();
        }

        private void SetGui()
        {
            this.LoadUserControls();
        }

        #endregion
    }
}