﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Catalog.SubPanels.OrderHourPanel" Codebehind="OrderHourPanel.ascx.cs" %>
<table class="orderhour">
    <tr>
        <th>
            <D:Label runat="server" ID="lblDay">Dag</D:Label>
        </th>
        <th>
            <D:Label runat="server" ID="lblFromTo">Van / Tot</D:Label>
        </th>
        <th style="width:20px;">&nbsp;</th>
        <th>
            <D:Label runat="server" ID="lblFromTo2">Van / Tot</D:Label>
        </th>
        <th style="width:20px;">&nbsp;</th>
        <th>
            <D:Label runat="server" ID="lblFromTo3">Van / Tot</D:Label>
        </th>
        <th style="width:20px;">&nbsp;</th>
        <th>
            <D:Label runat="server" ID="lblAvailable">Beschikbaarheid</D:Label>
        </th>
    </tr>
    <tr>
        <td>
            <D:Label runat="server" ID="lblAllDays">Alle dagen</D:Label>
        </td>
        <td>                
            <X:TimeEdit runat="server" ID="teDayAllBegin1"></X:TimeEdit>
            <X:TimeEdit runat="server" ID="teDayAllEnd1"></X:TimeEdit>
        </td>
        <td class="empty_space"></td>
        <td>
            <X:TimeEdit runat="server" ID="teDayAllBegin2"></X:TimeEdit>
            <X:TimeEdit runat="server" ID="teDayAllEnd2"></X:TimeEdit>
        </td>
        <td class="empty_space"></td>
        <td>
            <X:TimeEdit runat="server" ID="teDayAllBegin3"></X:TimeEdit>
            <X:TimeEdit runat="server" ID="teDayAllEnd3"></X:TimeEdit>
        </td>        
        <td class="empty_space"></td>
        <td class="available_column">
            <D:Label runat="server" ID="lblDayAllAvailable" LocalizeText="false" CssClass="available"></D:Label>
        </td>
    </tr>
    <tr>
        <td>
            <%= Dionysos.Global.TranslationProvider.GetTranslation(Dionysos.Globalization.GenericTranslatables.Monday) %>
        </td>
        <td>
            <X:TimeEdit runat="server" ID="teDay1Begin1"></X:TimeEdit>
            <X:TimeEdit runat="server" ID="teDay1End1"></X:TimeEdit>
        </td>
        <td class="empty_space"></td>
        <td>
            <X:TimeEdit runat="server" ID="teDay1Begin2"></X:TimeEdit>
            <X:TimeEdit runat="server" ID="teDay1End2"></X:TimeEdit>
        </td>
        <td class="empty_space"></td>
        <td>
            <X:TimeEdit runat="server" ID="teDay1Begin3"></X:TimeEdit>
            <X:TimeEdit runat="server" ID="teDay1End3"></X:TimeEdit>
        </td>        
        <td class="empty_space"></td>
        <td class="available_column">
            <D:Label runat="server" ID="lblDay1Available" LocalizeText="false" CssClass="available"></D:Label>
        </td>
    </tr>
    <tr>
        <td>
            <%= Dionysos.Global.TranslationProvider.GetTranslation(Dionysos.Globalization.GenericTranslatables.Tuesday) %>
        </td>
        <td>
            <X:TimeEdit runat="server" ID="teDay2Begin1"></X:TimeEdit>
            <X:TimeEdit runat="server" ID="teDay2End1"></X:TimeEdit>
        </td>
        <td class="empty_space"></td>
        <td>
            <X:TimeEdit runat="server" ID="teDay2Begin2"></X:TimeEdit>
            <X:TimeEdit runat="server" ID="teDay2End2"></X:TimeEdit>
        </td>
        <td class="empty_space"></td>
        <td>
            <X:TimeEdit runat="server" ID="teDay2Begin3"></X:TimeEdit>
            <X:TimeEdit runat="server" ID="teDay2End3"></X:TimeEdit>
        </td>        
        <td class="empty_space"></td>
        <td class="available_column">
            <D:Label runat="server" ID="lblDay2Available" LocalizeText="false" CssClass="available"></D:Label>
        </td>
    </tr>
    <tr>
        <td>
            <%= Dionysos.Global.TranslationProvider.GetTranslation(Dionysos.Globalization.GenericTranslatables.Wednesday) %>
        </td>
        <td>
            <X:TimeEdit runat="server" ID="teDay3Begin1"></X:TimeEdit>
            <X:TimeEdit runat="server" ID="teDay3End1"></X:TimeEdit>
        </td>
        <td class="empty_space"></td>
        <td>
            <X:TimeEdit runat="server" ID="teDay3Begin2"></X:TimeEdit>
            <X:TimeEdit runat="server" ID="teDay3End2"></X:TimeEdit>
        </td>
        <td class="empty_space"></td>
        <td>
            <X:TimeEdit runat="server" ID="teDay3Begin3"></X:TimeEdit>
            <X:TimeEdit runat="server" ID="teDay3End3"></X:TimeEdit>
        </td>        
        <td class="empty_space"></td>
        <td class="available_column">
            <D:Label runat="server" ID="lblDay3Available" LocalizeText="false" CssClass="available"></D:Label>
        </td>
    </tr>
    <tr>
        <td>
            <%= Dionysos.Global.TranslationProvider.GetTranslation(Dionysos.Globalization.GenericTranslatables.Thursday) %>
        </td>
        <td>
            <X:TimeEdit runat="server" ID="teDay4Begin1"></X:TimeEdit>
            <X:TimeEdit runat="server" ID="teDay4End1"></X:TimeEdit>
        </td>
        <td class="empty_space"></td>
        <td>
            <X:TimeEdit runat="server" ID="teDay4Begin2"></X:TimeEdit>
            <X:TimeEdit runat="server" ID="teDay4End2"></X:TimeEdit>
        </td>
        <td class="empty_space"></td>
        <td>
            <X:TimeEdit runat="server" ID="teDay4Begin3"></X:TimeEdit>
            <X:TimeEdit runat="server" ID="teDay4End3"></X:TimeEdit>
        </td>        
        <td class="empty_space"></td>
        <td class="available_column">
            <D:Label runat="server" ID="lblDay4Available" LocalizeText="false" CssClass="available"></D:Label>
        </td>
    </tr>
    <tr>
        <td>
            <%= Dionysos.Global.TranslationProvider.GetTranslation(Dionysos.Globalization.GenericTranslatables.Friday) %>
        </td>
        <td>
            <X:TimeEdit runat="server" ID="teDay5Begin1"></X:TimeEdit>
            <X:TimeEdit runat="server" ID="teDay5End1"></X:TimeEdit>
        </td>
        <td class="empty_space"></td>
        <td>
            <X:TimeEdit runat="server" ID="teDay5Begin2"></X:TimeEdit>
            <X:TimeEdit runat="server" ID="teDay5End2"></X:TimeEdit>
        </td>
        <td class="empty_space"></td>
        <td>
            <X:TimeEdit runat="server" ID="teDay5Begin3"></X:TimeEdit>
            <X:TimeEdit runat="server" ID="teDay5End3"></X:TimeEdit>
        </td>        
        <td class="empty_space"></td>
        <td class="available_column">
            <D:Label runat="server" ID="lblDay5Available" LocalizeText="false" CssClass="available"></D:Label>
        </td>
    </tr>
    <tr>
        <td>
            <%= Dionysos.Global.TranslationProvider.GetTranslation(Dionysos.Globalization.GenericTranslatables.Saturday) %>
        </td>
        <td>
            <X:TimeEdit runat="server" ID="teDay6Begin1"></X:TimeEdit>
            <X:TimeEdit runat="server" ID="teDay6End1"></X:TimeEdit>
        </td>
        <td class="empty_space"></td>
        <td>
            <X:TimeEdit runat="server" ID="teDay6Begin2"></X:TimeEdit>
            <X:TimeEdit runat="server" ID="teDay6End2"></X:TimeEdit>
        </td>
        <td class="empty_space"></td>
        <td>
            <X:TimeEdit runat="server" ID="teDay6Begin3"></X:TimeEdit>
            <X:TimeEdit runat="server" ID="teDay6End3"></X:TimeEdit>
        </td>        
        <td class="empty_space"></td>
        <td class="available_column">
            <D:Label runat="server" ID="lblDay6Available" LocalizeText="false" CssClass="available"></D:Label>
        </td>
    </tr>
    <tr>
        <td>
            <%= Dionysos.Global.TranslationProvider.GetTranslation(Dionysos.Globalization.GenericTranslatables.Sunday) %>
        </td>
        <td>
            <X:TimeEdit runat="server" ID="teDay0Begin1"></X:TimeEdit>
            <X:TimeEdit runat="server" ID="teDay0End1"></X:TimeEdit>
        </td>
        <td class="empty_space"></td>
        <td>
            <X:TimeEdit runat="server" ID="teDay0Begin2"></X:TimeEdit>
            <X:TimeEdit runat="server" ID="teDay0End2"></X:TimeEdit>
        </td>
        <td class="empty_space"></td>
        <td>
            <X:TimeEdit runat="server" ID="teDay0Begin3"></X:TimeEdit>
            <X:TimeEdit runat="server" ID="teDay0End3"></X:TimeEdit>
        </td>        
        <td class="empty_space"></td>
        <td class="available_column">
            <D:Label runat="server" ID="lblDay0Available" LocalizeText="false" CssClass="available"></D:Label>
        </td>
    </tr>

</table>