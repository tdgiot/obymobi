﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Catalog.SubPanels.ProductVerifierPanel" Codebehind="ProductVerifierPanel.ascx.cs" %>
<table class="dataformV2 productVerifier">
	<tr>
		<td class="label">
			<D:Label runat="server" ID="lblName">Naam</D:Label>
		</td>
		<td class="control">
			<D:HyperLink runat="server" ID="lblNameValue"></D:HyperLink>
		</td>
		<td class="label">
			<D:Label runat="server" ID="lblPosproduct">Kassaproduct</D:Label>
		</td>
		<td class="control">
			<D:Hyperlink runat="server" ID="lblPosproductValue"></D:Hyperlink>
		</td>
	</tr>
	<tr>
		<td class="label">
			<D:Label runat="server" ID="lblGenericProduct">Basisproduct</D:Label>
		</td>
		<td class="control">
			<D:HyperLink runat="server" ID="lblGenericProductValue"></D:HyperLink>
		</td>
		<td class="label">
			<D:Label runat="server" ID="lblPrice">Prijs</D:Label>
		</td>
		<td class="control">
			<D:Label runat="server" ID="lblPriceValue"></D:Label>
		</td>
	</tr>
	<tr>
		<td class="label">
			<D:Label runat="server" ID="lblShowOnHomepage">Weergeven op homepage</D:Label>
		</td>
		<td class="control">
			<D:Label runat="server" ID="lblShowOnHomepageValue"></D:Label>
		</td>
		<td class="label">
			&nbsp;
		</td>
		<td class="control">
			&nbsp;
		</td>
	</tr>
	<D:PlaceHolder runat="server" ID="plhDescription">
	<tr>
		<td class="label">
			<D:Label runat="server" ID="lblDescription">Beschrijving</D:Label>
		</td>
		<td class="control">
			<D:Label runat="server" ID="lblDescriptionValue"></D:Label>
		</td>
		<td class="label">
			&nbsp;
		</td>
		<td class="control">
			&nbsp;
		</td>
	</tr>
	</D:PlaceHolder>
	<tr>
		<td class="label">
			<D:Label runat="server" ID="lblColors">Kleuren</D:Label>
		</td>
		<td class="control">
			<D:Label runat="server" ID="lblForegrounColor"></D:Label>
			|
			<D:Label runat="server" ID="lblBackgroundColor"></D:Label>
		</td>
		<td class="label">
			<D:Label runat="server" ID="lblImages">Afbeeldingen</D:Label>
		</td>
		<td class="control">
			<D:HyperLink runat="server" ID="lblImagesButton">Knop</D:HyperLink> | <D:HyperLink runat="server" ID="lblImagesBranding">Branding</D:HyperLink> | <D:HyperLink runat="server" ID="lblImagesLarge">Groot</D:HyperLink>
		</td>
	</tr>
	<tr>
		<td class="label">
			<D:Label runat="server" ID="lblAdvertisementTags">Advertentie tags</D:Label>
		</td>
		<td class="control">
			<D:Label runat="server" ID="lblAdvertisementTagsValue"></D:Label>
		</td>
	</tr>
	<D:PlaceHolder runat="server" ID="plhSuggestionsAndAlterations">
		<tr>
			<td class="label">
				<D:Label runat="server" ID="lblSuggestions">Suggesties</D:Label>
			</td>
			<td class="control">
				<D:PlaceHolder runat="server" ID="plhSuggestions">
				</D:PlaceHolder>
			</td>
			<td class="label">
				<D:Label runat="server" ID="lblAlterations">Productsamenstellingen</D:Label>
			</td>
			<td class="control">
				<D:PlaceHolder runat="server" ID="plhAlterations">
				</D:PlaceHolder>
			</td>
		</tr>
	</D:PlaceHolder>
</table>
