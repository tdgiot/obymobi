﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Catalog.SubPanels.ProductCategorySuggestionPanel" Codebehind="ProductCategorySuggestionPanel.ascx.cs" %>
<div class="divPageLeft" style="max-width: 800px">
    <D:Panel ID="pnlProductSuggestions" runat="server" GroupingText="Suggestions">	
        <table class="dataformV2">
            <tr>
	            <td class="label">
		            <D:Label runat="server" id="lblProductSuggestion1">Suggestion 1</D:Label>
	            </td>
	            <td class="control" colspan="2">
	                 <D:PlaceHolder runat="server" ID="plhProductSuggestion1"></D:PlaceHolder>
	            </td>
            </tr>
            <tr>
	            <td class="label">
		            <D:Label runat="server" id="lblSuggestionCheckout1">Checkout</D:Label>
	            </td>
	            <td class="control" colspan="2">
                    <D:CheckBox runat="server" ID="cbSuggestionCheckout1" />
	            </td>
            </tr>
            <tr>
	            <td class="label">
		            <D:Label runat="server" id="lblProductSuggestion2">Suggestion 2</D:Label>
	            </td>
	            <td class="control" colspan="2">
	                <D:PlaceHolder runat="server" ID="plhProductSuggestion2"></D:PlaceHolder>
	            </td>
            </tr>
            <tr>
	            <td class="label">
		            <D:Label runat="server" id="lblSuggestionCheckout2">Checkout</D:Label>
	            </td>
	            <td class="control" colspan="2">
                    <D:CheckBox runat="server" ID="cbSuggestionCheckout2" />
	            </td>
            </tr>
            <tr>
                <td class="label">
		            <D:Label runat="server" id="lblProductSuggestion3">Suggestion 3</D:Label>
	            </td>
	            <td class="control" colspan="2">
	                <D:PlaceHolder runat="server" ID="plhProductSuggestion3"></D:PlaceHolder>
	            </td>
            </tr>
            <tr>
	            <td class="label">
		            <D:Label runat="server" id="lblSuggestionCheckout3">Checkout</D:Label>
	            </td>
	            <td class="control" colspan="2">
                    <D:CheckBox runat="server" ID="cbSuggestionCheckout3" />
	            </td>
            </tr>
            <tr>
                <td class="label">
		            <D:Label runat="server" id="lblProductSuggestion4">Suggestion 4</D:Label>
	            </td>
	            <td class="control" colspan="2">
	                <D:PlaceHolder runat="server" ID="plhProductSuggestion4"></D:PlaceHolder>
	            </td>
            </tr>
            <tr>
	            <td class="label">
		            <D:Label runat="server" id="lblSuggestionCheckout4">Checkout</D:Label>
	            </td>
	            <td class="control" colspan="2">
                    <D:CheckBox runat="server" ID="cbSuggestionCheckout4" />
	            </td>
            </tr>
        </table>
    </D:Panel>
</div>
<div class="divPageRight" style="max-width: 800px">
    <D:Panel ID="pnlInheritedSuggestions" runat="server" GroupingText="Inherited suggestions">
        <table class="dataformV2">
            <tr>
                <td class="control"><D:CheckBox ID="cbInheritSuggestions" runat="server" Text="Inherit from categories (new api)" LocalizeText="False" /></td>
            </tr>
        </table>
        <D:PlaceHolder runat="server" ID="plhInheritedSuggestions"></D:PlaceHolder>
    </D:Panel>
</div>