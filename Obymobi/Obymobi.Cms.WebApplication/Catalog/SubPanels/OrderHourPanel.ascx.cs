﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dionysos.Web.UI;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Web.UI.DevExControls;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.ObymobiCms.Catalog.SubPanels
{
    public partial class OrderHourPanel : System.Web.UI.UserControl, Dionysos.Interfaces.ISaveableControl
    {
        #region Fields

        private int? productId = null;
        private int? categoryId = null;

        #endregion

        #region Methods

        protected override void OnInit(EventArgs e)
        {
            Dionysos.Web.UI.MasterPage.LinkedCssFiles.Add("~/css/catalog.css", 1000);
            base.OnInit(e);            
            this.Page.DataSourceLoaded += new Dionysos.Delegates.Web.DataSourceLoadedHandler(Page_DataSourceLoaded);
        }

        OrderHourCollection GetRelatedOrderHours()
        {
            PredicateExpression filter = new PredicateExpression();

            if (this.ProductId.HasValue)
                filter.Add(OrderHourFields.ProductId == this.ProductId);
            else if (this.CategoryId.HasValue)
                filter.Add(OrderHourFields.CategoryId == this.CategoryId);

            SortExpression sort = new SortExpression();
            sort.Add(OrderHourFields.DayOfWeek | SortOperator.Ascending);

            OrderHourCollection orderHours = new OrderHourCollection();
            orderHours.GetMulti(filter, 0, sort);

            return orderHours;
        }

        public bool Save()
        {
            var orderHoursView = this.GetRelatedOrderHours().DefaultView;

            // 7 + 1 (All) days.
            for (int iDay = 0; iDay < 8; iDay++)
            {
                for (int iInterval = 1; iInterval < 4; iInterval++)
                {
                    string controlNameBegin = string.Empty;
                    string controlNameEnd = string.Empty;
                    PredicateExpression filter = new PredicateExpression();
                    if (iDay <= 6)
                    {
                        controlNameBegin = string.Format("teDay{0}Begin{1}", iDay, iInterval);
                        controlNameEnd = string.Format("teDay{0}End{1}", iDay, iInterval);
                        filter.Add(OrderHourFields.DayOfWeek == iDay);
                    }
                    else
                    {
                        controlNameBegin = string.Format("teDayAllBegin{0}", iInterval);
                        controlNameEnd = string.Format("teDayAllEnd{0}", iInterval);
                        filter.Add(OrderHourFields.DayOfWeek == DBNull.Value);
                    }

                    filter.Add(OrderHourFields.SortOrder == iInterval);

                    orderHoursView.Filter = filter;

                    // Find OrderHour in View
                    OrderHourEntity orderHour;
                    if (orderHoursView.Count > 1)
                        throw new NotImplementedException();
                    else if (orderHoursView.Count == 1)
                        orderHour = orderHoursView[0];
                    else
                        orderHour = null;

                    // Get the controls
                    TimeEdit teBegin = this.FindControl(controlNameBegin) as TimeEdit;
                    TimeEdit teEnd = this.FindControl(controlNameEnd) as TimeEdit;

                    // Sync the years
                    if (teBegin.Value.HasValue && teEnd.Value.HasValue)
                    {
                        if(teBegin.Value.Value.Year > teEnd.Value.Value.Year)
                        {
                            teEnd.Value = teEnd.Value.Value.AddYears(teBegin.Value.Value.Year - teEnd.Value.Value.Year);
                        }
                        else if(teEnd.Value.Value.Year > teBegin.Value.Value.Year)
                        {
                            teBegin.Value = teBegin.Value.Value.AddYears(teEnd.Value.Value.Year - teBegin.Value.Value.Year);
                        }
                    }

                    if (teBegin.Value.HasValue && teEnd.Value.HasValue && (teBegin.Value != teEnd.Value) && (teBegin.Value < teEnd.Value))
                    {
                        // Got a value
                        if (orderHour == null)
                        {
                            orderHour = new OrderHourEntity();
                            orderHour.ProductId = this.ProductId;
                            orderHour.CategoryId = this.CategoryId;
                            orderHour.DayOfWeek = iDay <= 6 ? (int?)iDay : null;
                            orderHour.SortOrder = iInterval;
                        }

                        orderHour.TimeStartFromDateTime(teBegin.Value.Value);
                        orderHour.TimeEndFromDateTime(teEnd.Value.Value);

                        orderHour.Save();
                    }
                    else
                    {
                        // No value
                        if (orderHour != null)
                        {
                            orderHoursView.RelatedCollection.Remove(orderHour);
                            orderHour.Delete();
                        }
                    }

                }
            }

            return true;
        }

        void LoadValues()
        {
            var orderHours = this.GetRelatedOrderHours().DefaultView;
            var orderHourView = this.GetRelatedOrderHours().DefaultView;

            Dictionary<int, string> weekAvailability = new Dictionary<int, string>();
            for (int i = 0; i < orderHours.Count; i++)
            {
                var orderHour = orderHours[i];

                if (orderHour.SortOrder > 3 || orderHour.SortOrder < 1)
                    throw new NotImplementedException(string.Format("The CMS currently only supports SortOrder > 1 && < 4  per day, DayOfWeek '{0}' has different.", orderHour.DayOfWeek));

                // Get the controlname
                string controlNameBegin = string.Empty;
                string controlNameEnd = string.Empty;

                if (orderHour.DayOfWeek.HasValue)
                {
                    controlNameBegin = string.Format("teDay{0}Begin{1}", orderHour.DayOfWeek, orderHour.SortOrder);
                    controlNameEnd = string.Format("teDay{0}End{1}", orderHour.DayOfWeek, orderHour.SortOrder);
                }
                else
                {
                    controlNameBegin = string.Format("teDayAllBegin{0}", orderHour.SortOrder);
                    controlNameEnd = string.Format("teDayAllEnd{0}", orderHour.SortOrder);
                }

                // Find the controls
                TimeEdit teBegin = this.FindControl(controlNameBegin) as TimeEdit;
                TimeEdit teEnd = this.FindControl(controlNameEnd) as TimeEdit;

                teBegin.Value = orderHour.TimeStartAsDateTime();
                teEnd.Value = orderHour.TimeEndAsDateTime();

                // Get the day of the week, 999 for all days
                int day = 0;
                if (orderHour.DayOfWeek.HasValue)
                    day = orderHour.DayOfWeek.Value;
                else
                    day = 999;
                
                string newTime = string.Format("from {0} to {1}", orderHour.TimeStartAsDateTime().ToString("t"), orderHour.TimeEndAsDateTime().ToString("t"));

                string dayTimes;
                if (weekAvailability.TryGetValue(day, out dayTimes))
                    weekAvailability[day] = dayTimes += ", " + newTime;
                else
                    weekAvailability.Add(day, newTime);
            }

            // Check if there's a schedule for "allDays"
            string defaultAvailability;
            bool allDaysSet = false;
            if (weekAvailability.TryGetValue(999, out defaultAvailability))
            {
                allDaysSet = true;
            }
            else
            {
                defaultAvailability = "Not available";
            }
                
                
            // Loop through weekdays, 0 = sunday, 6 = saterday, all days = 999
            for (int i = 0; i <= 6; i++)
            {
                string controlAvailable = string.Format("lblDay{0}Available", i);
                Label lblAvailable = this.FindControl(controlAvailable) as Label;

                if (lblAvailable != null)
                {
                    // Check if there's a schedule for this day
                    string dayAvailability;
                    if (weekAvailability.TryGetValue(i, out dayAvailability))
                    {
                        // We got a schedule
                        lblAvailable.Text = dayAvailability;
                    }
                    else
                    {
                        // Use the default availability
                        lblAvailable.Text = defaultAvailability;
                    }
                }
            }

            // Last but not least, set the availability for "allDays"
            string controlAllAvailable = string.Format("lblDayAllAvailable");
            Label lblAllAvailable = this.FindControl(controlAllAvailable) as Label;
            if (lblAllAvailable != null)
            {
                if (allDaysSet)
                    lblAllAvailable.Text = "";
                else
                    lblAllAvailable.Text = defaultAvailability;
            }
                
        }

        #endregion

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        void Page_DataSourceLoaded(object sender)
        {
            if (this.Page.DataSource is ProductEntity)
                this.ProductId = ((ProductEntity)this.Page.DataSource).ProductId;
            else if (this.Page.DataSource is CategoryEntity)
                this.CategoryId = ((CategoryEntity)this.Page.DataSource).CategoryId;
            else
                throw new NotImplementedException();

            this.LoadValues();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the categoryId
        /// </summary>
        public int? CategoryId
        {
            get
            {
                return this.categoryId;
            }
            set
            {
                this.categoryId = value;
            }
        }


        /// <summary>
        /// Gets or sets the productId
        /// </summary>
        public int? ProductId
        {
            get
            {
                return this.productId;
            }
            set
            {
                this.productId = value;
            }
        }

        public new PageLLBLGenEntity Page
        {
            get
            {
                return base.Page as PageLLBLGenEntity;
            }
        }

        #endregion

    }
}