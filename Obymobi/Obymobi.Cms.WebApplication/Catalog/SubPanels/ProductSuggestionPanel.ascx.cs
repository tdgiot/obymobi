﻿using Dionysos.Web.UI;
using Dionysos.Web.UI.DevExControls;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Obymobi.ObymobiCms.Catalog.SubPanels
{
    public partial class ProductSuggestionPanel : System.Web.UI.UserControl, Dionysos.Interfaces.ISaveableControl
    {
        #region Methods

        private List<ProductCategorySuggestionPanel> panels = new List<ProductCategorySuggestionPanel>();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void CreateChildControls()
        {
            base.CreateChildControls();

            PredicateExpression filter = new PredicateExpression();
            filter.Add(ProductCategoryFields.ProductId == this.DataSourceAsProductEntity.ProductId);

            PrefetchPath path = new PrefetchPath(EntityType.ProductCategoryEntity);
            path.Add(ProductCategoryEntity.PrefetchPathCategoryEntity).SubPath.Add(CategoryEntity.PrefetchPathMenuEntity);

            SortExpression sort = new SortExpression(new SortClause(MenuFields.Name, SortOperator.Ascending));
            sort.Add(new SortClause(CategoryFields.Name, SortOperator.Ascending));

            RelationCollection relations = new RelationCollection();
            relations.Add(ProductCategoryEntity.Relations.CategoryEntityUsingCategoryId);
            relations.Add(CategoryEntity.Relations.MenuEntityUsingMenuId);

            ProductCategoryCollection productCategoryCollection = new ProductCategoryCollection();
            productCategoryCollection.GetMulti(filter, 0, sort, relations, path);

            if (productCategoryCollection.Count > 0)
            {
                if (productCategoryCollection.Count > 1)
                {
                    PageControl tabsMenus = new PageControl();
                    tabsMenus.ID = string.Format("suggestion-control-{0}", this.DataSourceAsProductEntity.ProductId);
                    tabsMenus.LocalizeTabPageTitles = false;
                    tabsMenus.Width = Unit.Percentage(100);
                    this.plhCategories.Controls.Add(tabsMenus);

                    foreach (ProductCategoryEntity productCategory in productCategoryCollection)
                    {
                        string pageText = string.Format("{0} - {1}", productCategory.CategoryEntity.MenuEntity.Name, productCategory.CategoryEntity.Name);
                        string pageName = string.Format("productcategory-{0}", productCategory.ProductCategoryId);

                        ProductCategorySuggestionPanel panel = tabsMenus.AddTabPage(pageText, pageName, "~/Catalog/SubPanels/ProductCategorySuggestionPanel.ascx") as ProductCategorySuggestionPanel;
                        panel.ID = pageName;
                        panel.SetProductCategory(productCategory);

                        panels.Add(panel);
                    }
                }
                else
                {
                    ProductCategoryEntity productCategory = productCategoryCollection[0];

                    string pageText = string.Format("{0} - {1}", productCategory.CategoryEntity.MenuEntity.Name, productCategory.CategoryEntity.Name);
                    string pageName = string.Format("productcategory-{0}", productCategory.ProductCategoryId);

                    ProductCategorySuggestionPanel panel = LoadControl("~/Catalog/SubPanels/ProductCategorySuggestionPanel.ascx") as ProductCategorySuggestionPanel;
                    panel.ID = pageName;
                    panel.SetProductCategory(productCategory);

                    panels.Add(panel);
                    this.plhCategories.Controls.Add(panel);
                }
            }

            this.SetGui();
        }

        public bool Save()
        {
            foreach (ProductCategorySuggestionPanel panel in this.panels)
            {
                panel.Save();
            }
            return true;
        }

        public void SetGui()
        {
            foreach (ProductCategorySuggestionPanel panel in this.panels)
            {
                panel.SetGui();
            }
        }

        #endregion

        #region Properties

        public new PageLLBLGenEntity Page
        {
            get
            {
                return base.Page as PageLLBLGenEntity;
            }
        }

        public ProductEntity DataSourceAsProductEntity
        {
            get
            {
                return this.Page.DataSource as ProductEntity;
            }
        }

        #endregion
    }
}