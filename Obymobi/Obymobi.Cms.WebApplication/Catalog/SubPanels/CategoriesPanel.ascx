﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Catalog.SubPanels.CategoriesPanel" Codebehind="CategoriesPanel.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v20.1" Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dxwtl" %>
<script type="text/javascript">    
    var lastItemType = null;

    var existingItemType = null;
    function OnItemTypeChanged(cmbItemType) {
        if (cmbItemType.InCallback())
            existingItemType = cmbItemType.GetValue().toString();
        else
            cmbItem.PerformCallback(cmbItemType.GetValue().toString());
    }

    function OnItemEndCallback(s, e) {
        if (existingItemType) {
            cmbItem.PerformCallback(existingItemType);
            existingItemType = null;
        }
    }

    function OnInit(s, e) {        
        ASPxClientUtils.AttachEventToElement(s.GetMainElement(), "keypress",
        function (evt) {
            switch (evt.keyCode) {
                    //ENTER
                case 13:
                    if (s.IsEditing()) {
                        s.UpdateEdit();
                        evt.stopPropagation();
                    }
                    break;

                    //ESC               
                case 27:
                    if (s.IsEditing()) {
                        s.CancelEdit();
                        evt.stopPropagation();
                    }
                    break;
            }
        });
    }
</script>
<div style="width: 736px;">
    <span class="nobold" style="width: 100%; height: 24px; margin-left: 0;">
        <D:HyperLink runat="server" ID="hlExpandAll">Expand all</D:HyperLink> | <D:HyperLink runat="server" ID="hlCollapseAll">Collapse all</D:HyperLink>
        <D:Button runat="server" ID="btDeleteItemsTop" style="float:right;" Text="Delete" />
        <D:Button runat="server" ID="btHideTop" style="float:right; margin-right: 4px;" Text="Hide" />        
    </span>                                    
    <dxwtl:ASPxTreeList ID="tlCategories" ClientInstanceName="tlCategories" runat="server" EnableViewState="false">
        <SettingsSelection Enabled="true" Recursive="false" />        
        <ClientSideEvents Init="OnInit" />                
    </dxwtl:ASPxTreeList>               
    <div id="divDeletePages" style="margin-top: 4px;">
        <D:Button runat="server" ID="btDeleteItemsBottom" style="float:right;" Text="Delete" />
        <D:Button runat="server" ID="btHideBottom" style="float:right; margin-right: 4px;" Text="Hide" />        
    </div> 
</div>