﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Catalog.SubPanels.PriceLevelItemsPanel" Codebehind="PriceLevelItemsPanel.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v20.1" Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dxwtl" %>
<script type="text/javascript">    

    function OnInit(s, e) {        
        ASPxClientUtils.AttachEventToElement(s.GetMainElement(), "keypress",
        function (evt) {
            switch (evt.keyCode) {
                    //ENTER
                case 13:
                    if (s.IsEditing()) {
                        s.UpdateEdit();
                        evt.stopPropagation();
                    }
                    break;

                    //ESC               
                case 27:
                    if (s.IsEditing()) {
                        s.CancelEdit();
                        evt.stopPropagation();
                    }
                    break;
            }
        });
    }
</script>
<div style="width: 736px;">
    <span class="nobold" style="width: 100%; height: 24px; margin-left: 0;">
        <D:HyperLink runat="server" ID="hlExpandAll">Expand all</D:HyperLink> | <D:HyperLink runat="server" ID="hlCollapseAll">Collapse all</D:HyperLink>        
    </span>                                    
    <dxwtl:ASPxTreeList ID="tlPriceLevelItems" ClientInstanceName="tlPriceLevelItems" runat="server" EnableViewState="false">        
        <ClientSideEvents Init="OnInit" />                
    </dxwtl:ASPxTreeList>
</div>