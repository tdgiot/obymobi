﻿using Dionysos.Web.UI;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.ObymobiCms.MasterPages;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Web.UI.WebControls;

namespace Obymobi.ObymobiCms.Catalog
{
    public partial class Alterations : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "Alteration";
            this.EntityPageUrl = "~/Catalog/Alteration.aspx";
            base.OnInit(e);

            MasterPageEntityCollection masterPage = this.Master as MasterPages.MasterPageEntityCollection;
            if (masterPage != null)
            {
                masterPage.SetPageTitle("Alterations V1 & V2");
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            CompanyEntity companyEntity = new CompanyEntity(CmsSessionHelper.CurrentCompanyId);
            if (companyEntity.AlterationDialogMode > AlterationDialogMode.v2)
            {
                this.AddInformator(InformatorType.Warning, "This company is not configured to use V1/V2 alterations");
                this.Validate();
            }

            LLBLGenProDataSource datasource = this.DataSource as LLBLGenProDataSource;
            if (datasource != null)
            {
                PredicateExpression filter = new PredicateExpression(AlterationFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
                filter.Add(AlterationFields.ParentAlterationId == DBNull.Value);
                filter.Add(AlterationFields.Type != AlterationType.Page);
                filter.Add(AlterationFields.Type != AlterationType.Category);
                filter.Add(AlterationFields.Version == 1);

                datasource.FilterToUse = filter;                
            }

            if (this.MainGridView != null)
            {
                this.MainGridView.Styles.Cell.HorizontalAlign = HorizontalAlign.Left;
            }            
        }
    }
}
