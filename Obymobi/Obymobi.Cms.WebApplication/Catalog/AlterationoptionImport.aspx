﻿<%@ Page Title="Alteration option import" Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Catalog.AlterationoptionImport" Codebehind="AlterationoptionImport.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitleHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhToolbarHolder" Runat="Server">
    <D:Button runat="server" ID="btImport" Text="Importeren" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhContentHolder" Runat="Server">
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Productsamenstelling opties importeren" Name="ImportProductAlterationOptions">
				<Controls>
					<D:PlaceHolder ID="PlaceHolder1" runat="server">
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:Label runat="server" id="lblCategoryId">Categorie</D:Label>
							</td>
							<td class="control">
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlCategoryId" IncrementalFilteringMode="StartsWith" EntityName="Category" TextField="Name" ValueField="CategoryId" />
							</td>
						</tr>
					 </table>			
					 </D:PlaceHolder>
				</Controls>
			</X:TabPage>			
		</TabPages>
	</X:PageControl>
</asp:Content>

