﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Catalog.Posalterationoption" Codebehind="Posalterationoption.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblName">Naam</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblExternalId">Kassa id</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbExternalId" runat="server" IsRequired="true"></D:TextBoxString>
							</td>	
					    </tr>
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblPriceAddition">Prijs toevoeging</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxDecimal runat="server" ID="tbPriceAddition" AllowZero="true" AllowNegative="false"></D:TextBoxDecimal>
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblPriceIn">Price</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxDecimal runat="server" ID="tbPriceIn" AllowZero="true" AllowNegative="false"></D:TextBoxDecimal>
							</td>
						</tr>
                        <tr>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblDescription">Description</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control" colspan="3">
                                <D:TextBoxMultiLine runat="server" ID="tbDescription"></D:TextBoxMultiLine>
                            </td>
                        </tr>
					    <tr>						
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblFieldValue1">Waarde 1</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
							    <D:TextBoxString ID="tbFieldValue1" runat="server"></D:TextBoxString>
							</td>        
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblFieldValue2">Waarde 2</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbFieldValue2" runat="server"></D:TextBoxString>
							</td>							
						</tr>
					    <tr>						
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblFieldValue3">Waarde 3</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
							    <D:TextBoxString ID="tbFieldValue3" runat="server"></D:TextBoxString>
							</td>        
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblFieldValue4">Waarde 4</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbFieldValue4" runat="server"></D:TextBoxString>
							</td>							
						</tr>
					    <tr>						
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblFieldValue5">Waarde 5</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
							    <D:TextBoxString ID="tbFieldValue5" runat="server"></D:TextBoxString>
							</td>        
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblFieldValue6">Waarde 6</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbFieldValue6" runat="server"></D:TextBoxString>
							</td>							
						</tr>
					    <tr>						
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblFieldValue7">Waarde 7</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
							    <D:TextBoxString ID="tbFieldValue7" runat="server"></D:TextBoxString>
							</td>        
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblFieldValue8">Waarde 8</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbFieldValue8" runat="server"></D:TextBoxString>
							</td>							
						</tr>
					    <tr>						
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblFieldValue9">Waarde 9</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
							    <D:TextBoxString ID="tbFieldValue9" runat="server"></D:TextBoxString>
							</td>        
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblFieldValue10">Waarde 10</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbFieldValue10" runat="server"></D:TextBoxString>
							</td>							
						</tr>
					 </table>			
				</Controls>
			</X:TabPage>						
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>

