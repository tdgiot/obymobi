﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.StoredProcedureCallerClasses;
using Obymobi.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;
using DevExpress.Web.ASPxTreeList;

namespace Obymobi.ObymobiCms.Catalog
{
    public partial class Categories : Dionysos.Web.UI.PageLLBLGenEntityCollection
    {
        #region Fields

        #endregion

        #region Methods

        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "Category";
            this.EntityPageUrl = "~/Catalog/Category.aspx";
            this.DataSortField = "SortOrder";
            this.PagingPageSize = 1000000;

            PredicateExpression filter = new PredicateExpression(CategoryFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            this.DataSourceFilter = filter;       
        }

        public override bool InitializeDataBindings()
        {
            this.LoadData();

            return true;
        }

        void LoadData()
        {
            EntityView<CategoryEntity> navItemView = this.DataSourceAsCategoryCollection.DefaultView;
            SortExpression sort = new SortExpression(CategoryFields.ParentCategoryId | SortOperator.Ascending);
            navItemView.Sorter = sort;
            this.tlCategories.DataSource = navItemView;
            this.tlCategories.DataBind();
        }

        void AddItemToLastItem(TreeListNode node)
        {
            if (node.HasChildren)
            {
                for (int i = 0; i < node.ChildNodes.Count; i++)
                {
                    this.AddItemToLastItem(node.ChildNodes[i]);
                }
            }

            TreeListNode nodeNew = tlCategories.AppendNode(node.Key + "_New", node);
            nodeNew["Name"] = string.Format("<a href=\"Category.aspx?mode=add&Entity=Category&ParentCategoryId={0}\">Item toevoegen...</a>", node.Key);
        }

        void SetGui()
        {

        }

        #endregion

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.IsPostBack)
            {
                this.InitializeEntityCollection();
                this.InitializeDataBindings();
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Return the page's datasource as a CategoryCollection
        /// </summary>
        public CategoryCollection DataSourceAsCategoryCollection
        {
            get
            {
                return this.DataSource as CategoryCollection;
            }
        }

        #endregion

    }
}
