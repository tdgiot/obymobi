﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Catalog.GenericProductMatching" Codebehind="GenericProductMatching.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitleHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhToolbarHolder" Runat="Server">	
		<D:Button runat="server" ID="btMatch" Text="Koppelen" />	
		<D:Button runat="server" ID="btChangeCategory" Text="Categorie wisselen" />	
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhContentHolder" Runat="Server">
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Producten koppelen" Name="ProductMatching">
				<Controls>
					<D:PlaceHolder ID="PlaceHolder1" runat="server">
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:Label runat="server" id="lblCategoryId">Categorie</D:Label>
							</td>
							<td class="control">
								<X:ComboBoxInt runat="server" ID="ddlCategoryId" IncrementalFilteringMode="StartsWith" TextField="FullCategoryName" ValueField="CategoryId" ></X:ComboBoxInt>
							</td>
						</tr>
						<tr>
							<td class="label">
								&nbsp;
							</td>
							<td class="control">
								<D:CheckBox runat="server" ID="cbImport" AutoPostBack="true" Visible="false" />
							</td>							
						</tr>
						<tr>
							<td class="label">
								<D:Label runat="server" id="lblProductTitle">Menukaart product</D:Label>
							</td>
							<td class="control">
								<D:Label runat="server" id="lblGenericProductTitle">Basis product</D:Label>
							</td>							
							<td class="label">
								&nbsp;
							</td>
							<td class="control">
								&nbsp;
							</td>				
						</tr>	
						<D:PlaceHolder runat="server" ID="plhProducts"></D:PlaceHolder>
					 </table>			
					 </D:PlaceHolder>
				</Controls>
			</X:TabPage>			
		</TabPages>
	</X:PageControl>
</asp:Content>

