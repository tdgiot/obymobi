﻿using System;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;

namespace Obymobi.ObymobiCms.Catalog
{
    public partial class Posproducts : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "Posproduct";
            this.EntityPageUrl = "~/Catalog/Posproduct.aspx";
            base.OnInit(e);

            if (CmsSessionHelper.CurrentRole < Role.Administrator)
            {
                var masterPage = this.Master as MasterPages.MasterPageEntityCollection;
                if (masterPage != null)
                    masterPage.ToolBar.AddButton.Visible = false;
            }
        }

        protected override void  OnLoadComplete(EventArgs e)
        {
 	         base.OnLoadComplete(e);

            DevExpress.Web.GridViewDataColumn column = this.MainGridView.Columns["ExternalId"] as DevExpress.Web.GridViewDataColumn;
            if (column != null)
            {
                column.Settings.SortMode = DevExpress.XtraGrid.ColumnSortMode.Custom;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            LLBLGenProDataSource datasource = this.DataSource as LLBLGenProDataSource;
            if (datasource != null)
            {
                PredicateExpression filter = new PredicateExpression();
                filter.Add(PosproductFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

                datasource.FilterToUse = filter;
            }
            
            this.MainGridView.CustomColumnSort += MainGridView_CustomColumnSort;  
        }

        void MainGridView_CustomColumnSort(object sender, DevExpress.Web.CustomColumnSortEventArgs e)
        {
            if (e.Column.FieldName == "ExternalId")
            {
                try
                {
                    var numVal1 = Convert.ToInt64(e.Value1);
                    var numVal2 = Convert.ToInt64(e.Value2);

                    if (numVal1 < numVal2)
                        e.Result = -1;
                    else if (numVal1 > numVal2)
                        e.Result = 1;
                    else
                        e.Result = 0;
                }
                catch
                {
                    e.Result = 0;
                }

                e.Handled = true;
            }
        }

        #endregion
    }
}
