﻿using Dionysos.Web.UI;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Web.UI.WebControls;

namespace Obymobi.ObymobiCms.Catalog
{
    public partial class AlterationoptionsV3 : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "Alterationoption";
            this.EntityPageUrl = "~/Catalog/Alterationoption.aspx";
            base.OnInit(e);

            MasterPages.MasterPageEntityCollection masterPage = this.Master as MasterPages.MasterPageEntityCollection;
            if (masterPage != null)
            {
                masterPage.SetPageTitle("Alteration options V3");
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            CompanyEntity companyEntity = new CompanyEntity(CmsSessionHelper.CurrentCompanyId);
            if (companyEntity.AlterationDialogMode != AlterationDialogMode.v3)
            {
                this.AddInformator(InformatorType.Warning, "This company is not configured to use V3 alterations");
                this.Validate();
            }

            LLBLGenProDataSource datasource = this.DataSource as LLBLGenProDataSource;
            if (datasource != null)
            {
                PredicateExpression filter = new PredicateExpression(AlterationoptionFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
                filter.Add(AlterationoptionFields.Version == 2);
                datasource.FilterToUse = filter;
            }            

            if (this.MainGridView != null)
            {
                this.MainGridView.Styles.Cell.HorizontalAlign = HorizontalAlign.Left;
            }
        }

        public new void Add()
        {
            this.Response.Redirect("~/Catalog/Alterationoption.aspx?mode=add&entity=Alterationoption&id=&version=2");
        }
    }
}
