﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using Dionysos.Web;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Dionysos.Web.UI.DevExControls;
using Obymobi.Enums;

namespace Obymobi.ObymobiCms.Catalog
{
    public partial class AlterationoptionImport : Dionysos.Web.UI.PageQueryStringDataBinding
    {
        #region Methods

        protected override void OnInit(EventArgs e)
        {
            this.SetGui();
            base.OnInit(e);
        }

        protected override void SetDefaultValuesToControls()
        {
            this.ddlCategoryId.Value = -1;
        }

        private void SetGui()
        {
            CategoryCollection cats = new CategoryCollection();

            PredicateExpression filter = new PredicateExpression();
            filter.Add(CategoryFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

            SortExpression sort = new SortExpression();
            sort.Add(CategoryFields.Name | SortOperator.Ascending);

            cats.GetMulti(filter, 0, sort);

            // Load Categories of Company
            this.ddlCategoryId.DataSource = cats;
            this.ddlCategoryId.DataBind();
        }

        private void HookupEvents()
        {
            this.btImport.Click += new EventHandler(btImport_Click);
        }

        private void ImportAlterationoptions()
        {
            if (this.ddlCategoryId.ValidId > 0)
            {
                PredicateExpression filter = new PredicateExpression();
                filter.Add(ProductCategoryFields.CategoryId == this.ddlCategoryId.ValidId);
                filter.Add(ProductFields.VisibilityType != VisibilityType.Never);

                RelationCollection relations = new RelationCollection();
                relations.Add(ProductCategoryEntity.Relations.ProductEntityUsingProductId);

                ProductCollection products = new ProductCollection();
                products.GetMulti(filter, relations);

                PredicateExpression filter2 = new PredicateExpression();
                filter2.Add(AlterationoptionFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
                filter2.Add(AlterationoptionFields.Version == 1);

                AlterationoptionCollection alterationoptions = new AlterationoptionCollection();
                alterationoptions.GetMulti(filter2);

                EntityView<AlterationoptionEntity> alterationoptionView = alterationoptions.DefaultView;

                foreach (ProductEntity product in products)
                {
                    if (product.PosproductId.HasValue)
                    {
                        PredicateExpression filter3 = new PredicateExpression();
                        filter3.Add(AlterationoptionFields.PosproductId == product.PosproductId);

                        alterationoptionView.Filter = filter3;

                        if (alterationoptionView.Count == 0)
                        {
                            AlterationoptionEntity alterationoption = new AlterationoptionEntity();
                            alterationoption.Name = product.Name;                            
                            alterationoption.PriceIn = product.PriceIn;
                            alterationoption.PosproductId = product.PosproductId;
                            alterationoption.IsProductRelated = true;
                            alterationoption.CompanyId = CmsSessionHelper.CurrentCompanyId;
                            if (alterationoption.Save())
                            {
                                alterationoption.Refetch();
                                alterationoptionView.RelatedCollection.Add(alterationoption);
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            this.HookupEvents();
        }

        private void btImport_Click(object sender, EventArgs e)
        {
            this.ImportAlterationoptions();
        }


        #endregion
    }
}