﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Catalog.Menu" ValidateRequest="false" Codebehind="Menu.aspx.cs" %>
<%@ Reference VirtualPath="~/Catalog/SubPanels/CategoriesPanel.ascx" %>
<%@ Reference Control="~/Catalog/SubPanels/CategoriesPanel.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
    <X:ToolBarButton runat="server" ID="btUpload" CommandName="Upload" Text="Upload" Visible="false" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true"/>
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
	            <Controls>
                    <D:PlaceHolder runat="server" ID="plhGeneric">
		            <table class="dataformV2">
			            <tr>
				            <td class="label">
					            <D:LabelEntityFieldInfo runat="server" id="lblName">Name</D:LabelEntityFieldInfo>
				            </td>
				            <td class="control">
					            <D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
				            </td>
				            <td class="label">
				            </td>
				            <td class="control">
                                <D:Button ID="btnAddBatch" runat="server" Text="Batch categorieen toevoegen" notdirty="true"/>
				            </td>
			            </tr>    
                        <D:PlaceHolder runat="server" ID="plhCategories">
                        <tr>
                             <td class="label">
                                 <D:Label runat="server" id="lblCategories">Categories</D:Label>
                             </td>
                            <td class="control" colspan="3">
                                 <div style="width: 736px;">
                                    <D:PlaceHolder runat="server" ID="plhCategoriesPanel"></D:PlaceHolder>
                                </div>
                             </td>
                        </tr>
                        </D:PlaceHolder>                    
			        </table>	
                    </D:PlaceHolder>
	            </Controls>
            </X:TabPage>
            <X:TabPage Text="Brands" Name="Brands">
	            <Controls>
		            <table class="dataformV2">          
                        <tr>
                            <td class="label"> 
                                <D:LabelTextOnly ID="lblBrand" runat="server">Brand</D:LabelTextOnly>
                            </td>
                             <td class="control">   
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlBrands" EntityName="Brand" IncrementalFilteringMode="StartsWith" TextField="Name" ValueField="BrandId" PreventEntityCollectionInitialization="True" AutoPostBack="True" notdirty="True" />
                            </td>
                             <td class="label">
                             </td>
                            <td class="control">
                            </td>
                        </tr>  
                        <tr>
                            <td class="label"> 
                                <D:LabelTextOnly ID="lblProduct" runat="server">Product</D:LabelTextOnly>
                            </td>
                             <td class="control">   
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlBrandProducts" EntityName="Product" IncrementalFilteringMode="StartsWith" TextField="Name" ValueField="ProductId" PreventEntityCollectionInitialization="True"  notdirty="True" />
                            </td>
                             <td class="label">
                             </td>
                            <td class="control">
                            </td>
                        </tr>  
                        <tr>
                            <td class="label"> 
                                <D:LabelTextOnly ID="LabelTextOnly1" runat="server">In Category</D:LabelTextOnly>
                            </td>
                             <td class="control">   
                                <X:ComboBox runat="server" ID="ddlCategories" IncrementalFilteringMode="Contains" notdirty="True" />
                            </td>
                             <td class="label">
                             </td>
                            <td class="control">
                            </td>
                        </tr>  
                        <tr>
                            <td class="label">        
                            </td>
                            <td class="control">   
                                <D:Button runat="server" ID="btnLinkBrandproduct" Text="Create new product in menu and link brand product" LocalizeText="False"/>
                            </td>
                            <td class="label">
                            </td>
                            <td class="control">
                            </td>
                        </tr>  
                    </table>
                </Controls>
            </X:TabPage> 
            <X:TabPage Text="Copy" Name="Copy">
	            <Controls>
                    <D:Panel runat="server" ID="pnlCopy">
		            <table class="dataformV2">                        
                        <tr>
		                    <td class="label"> 
                                <D:LabelTextOnly ID="lblNewMenuName" runat="server">Menu naam</D:LabelTextOnly>
                            </td>
                             <td class="control">   
                                <D:TextBoxString ID="tbNewMenuName" runat="server" notdirty="true"></D:TextBoxString>
                            </td>
                             <td class="label">
                                 
                             </td>
                            <td class="control">
                                <D:Button ID="btnCopyMenu" runat="server" Text="Kopiëren" ClientIDMode="Static"/>
                            </td>
                        </tr>        
                        <D:PlaceHolder runat="server" ID="plhProgress" ClientIDMode="Static">
                        <tr>
                            <td class="label">
                                
                            </td>
                            <td class="control">
                                <strong id="strProgress" style="display:none;"><D:LabelTextOnly runat="server" id="lblProgress">Progress</D:LabelTextOnly></strong>
                            </td>
                        </tr>                                        
                        <tr>
                            <td class="label">
                                
                            </td>
                            <td class="control" colspan="3">
                                <D:TextBoxMultiLine runat="server" ID="tbProgress" Rows="40" ClientIDMode="Static" ReadOnly="true" Style="display:none;"></D:TextBoxMultiLine>   
                            </td>
                        </tr>
                        </D:PlaceHolder>
                    </table>
                    </D:Panel>
                </Controls>
            </X:TabPage>
            <X:TabPage Text="Download" Name="Download">
                <Controls>
                    <table class="dataformV2">
                        <tr>
                            <td class="label">
                                
                            </td>
                            <td class="control">
                                <D:CheckBox ID="cbIncludeAlterations" runat="server" Text="Include product alterations" notdirty="true" style="font-weight:normal"/>
                            </td>
                            <td class="label">
                                
                            </td>
                            <td class="control">
                                
                            </td>
                        </tr>
                        <tr>
                            <td class="label">

                            </td>
                            <td class="control">
                                <D:CheckBox ID="cbIncludeMedia" runat="server" Text="Include product media" notdirty="true" style="font-weight:normal"/>
                            </td>
                            <td class="label">

                            </td>
                            <td class="control">

                            </td>
                        </tr>
                        <tr>
                            <td class="label">

                            </td>
                            <td class="control">
                                <D:CheckBox ID="cbIncludeCategoryMedia" runat="server" Text="Include category media" notdirty="true" style="font-weight:normal"/>
                            </td>
                            <td class="label">

                            </td>
                            <td class="control">

                            </td>
                        </tr>
                        <tr>
                            <td class="label">

                            </td>
                            <td class="control">
                                <D:Button ID="btnDownloadMenu" runat="server" Text="Download menu" />
                            </td>
                            <td class="label">

                            </td>
                            <td class="control">

                            </td>
                        </tr>
                    </table>
                </Controls>
            </X:TabPage>
            <X:TabPage Text="Push" Name="Push">
                <Controls>
                    <table class="dataformV2">
                        <tr>
                            <td class="label"></td>
                            <td class="control">
                                <p>This will push the menu for all DPGs it's linked to and can take up to 10 minutes to finish.</p>
                                <p>Once you press this button all your changes will be available for all clients!</p>
                            </td>
                            <td class="label"></td>
                            <td class="control"></td>
                        </tr>
                        <tr>
                            <td class="label"></td>
                            <td class="control">
                                <D:Button runat="server" ID="btnPush" LocalizeText="False" Text="Push to WebService"/><br/>
                                <D:CheckBox runat="server" ID="btnDumpEt" LocalizeText="False" Text="He panik sold, pump it"/><br/>
                                Last Push:<D:Label runat="server" ID="lblTimestampMenu" LocalizeText="False"/>
                            </td>
                            <td class="label"></td>
                            <td class="control">
                            </td>
                        </tr>
                    </table>
                </Controls>
            </X:TabPage>
        </TabPages>
    </X:PageControl>
</div>
 <script type="text/javascript">
     var progressVisible = false;

     InitProgress();

     function InitProgress() {
         PageMethods.SessionExists(function (exists) {
             if (exists) {
                  UpdateProgress();
             }
         });
     }

     function UpdateProgress() {
         PageMethods.SessionExists(function (exists) {
             if (exists) {
                 progressVisible = true;
             } else {
                 progressVisible = false;
             }

             if (progressVisible) {
                 document.getElementById("btnCopyMenu").style.display = 'none';
                 document.getElementById("strProgress").style.display = 'block';
                 document.getElementById("tbProgress").style.display = 'block';

                 PageMethods.GetLog(function (log) {
                     document.getElementById("tbProgress").innerHTML = log;
                     document.getElementById("tbProgress").scrollTop = 99999;
                 });
             }
             else {
                 document.getElementById("btnCopyMenu").style.display = 'block';                 
             }
         });
         setTimeout(function () { UpdateProgress(); }, 500);
     }
 </script>
</asp:Content>

