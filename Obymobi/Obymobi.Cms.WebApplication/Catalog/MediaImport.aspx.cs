﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dionysos;
using Dionysos.Web;
using Dionysos.Web.UI.DevExControls;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dionysos.Web.UI.WebControls;
using Obymobi.Data;
using System.IO;
using Dionysos.IO;
using Obymobi.Enums;


namespace Obymobi.ObymobiCms.Catalog
{
    public partial class MediaImport : Dionysos.Web.UI.PageQueryStringDataBinding
    {
        #region Methods

        protected override void SetDefaultValuesToControls()
        {
        }

        private void SetGui()
        {
        }

        private void LoadPhotos()
        {
            string fileDir = Server.MapPath("~/Temp/Photo-import");
            string fileName = "Media.zip";
            string filePath = Path.Combine(fileDir, fileName);

            if (!Directory.Exists(fileDir))
                Directory.CreateDirectory(fileDir);

            if (File.Exists(filePath))
            {
                // Unzip the photos
                ZipUtil.Unzip(filePath, fileDir, true);

                // Get the paths to the files
                string[] filePaths = Directory.GetFiles(fileDir);

                // Get the product collection
                PredicateExpression filter = new PredicateExpression();
                filter.Add(ProductFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
                filter.Add(ProductFields.VisibilityType != VisibilityType.Never);

                RelationCollection joins = new RelationCollection();
                joins.Add(ProductEntity.Relations.ProductCategoryEntityUsingProductId, JoinHint.Left);

                PrefetchPath path = new PrefetchPath(EntityType.ProductEntity);
                path.Add(ProductEntity.PrefetchPathCategoryCollectionViaProductCategory).SubPath.Add(CategoryEntity.PrefetchPathParentCategoryEntity).SubPath.Add(CategoryEntity.PrefetchPathParentCategoryEntity).SubPath.Add(CategoryEntity.PrefetchPathParentCategoryEntity).SubPath.Add(CategoryEntity.PrefetchPathParentCategoryEntity);

                SortExpression sort = new SortExpression();
                sort.Add(ProductFields.Name | SortOperator.Ascending);

                ProductCollection products = new ProductCollection();
                products.GetMulti(filter, 0, sort, joins, path);

                ListItemCollection listItems = new ListItemCollection();
                listItems.Add(new ListItem("Geen - Maak uw keuze"));

                foreach (string file in filePaths)
                {
                    if (file != filePath)
                    {
                        // Get the filename
                        string name = Path.GetFileName(file);
                        string nameWithoutExtension = Path.GetFileNameWithoutExtension(file);

                        string productName = nameWithoutExtension.Substring(nameWithoutExtension.LastIndexOf("_") + 1);
                        string productNameFormatted = productName.Replace("-", " ");

                        listItems.Add(new ListItem(productNameFormatted, file));
                    }
                }

                int productCount = 0;
                int treshold = 5;

                foreach (ProductEntity product in products)
                {
                    if (product.MediaCollection.Count == 0)
                    {
                        bool isFood = false;
                        foreach (CategoryEntity category in product.CategoryCollectionViaProductCategory)
                        {
                            if (category.Name.Equals("Eten", StringComparison.InvariantCultureIgnoreCase))
                                isFood = true;
                            else if (
                                        category.ParentCategoryId.HasValue &&
                                        category.ParentCategoryEntity.Name.Equals("Eten", StringComparison.InvariantCultureIgnoreCase)
                                    )
                                isFood = true;
                            else if (
                                        category.ParentCategoryId.HasValue &&
                                        category.ParentCategoryEntity.ParentCategoryId.HasValue &&
                                        category.ParentCategoryEntity.ParentCategoryEntity.Name.Equals("Eten", StringComparison.InvariantCultureIgnoreCase)
                                    )
                                isFood = true;
                            else if (
                                        category.ParentCategoryId.HasValue &&
                                        category.ParentCategoryEntity.ParentCategoryId.HasValue &&
                                        category.ParentCategoryEntity.ParentCategoryEntity.ParentCategoryId.HasValue &&
                                        category.ParentCategoryEntity.ParentCategoryEntity.ParentCategoryEntity.Name.Equals("Eten", StringComparison.InvariantCultureIgnoreCase)
                                    )
                                isFood = true;
                            else if (
                                        category.ParentCategoryId.HasValue &&
                                        category.ParentCategoryEntity.ParentCategoryId.HasValue &&
                                        category.ParentCategoryEntity.ParentCategoryEntity.ParentCategoryId.HasValue &&
                                        category.ParentCategoryEntity.ParentCategoryEntity.ParentCategoryEntity.ParentCategoryId.HasValue &&
                                        category.ParentCategoryEntity.ParentCategoryEntity.ParentCategoryEntity.ParentCategoryEntity.Name.Equals("Eten", StringComparison.InvariantCultureIgnoreCase)
                                    )
                                isFood = true;
                        }

                        if (!isFood)
                        {
                            // Only food
                        }
                        else
                        {
                            productCount++;
                            // Add the filename
                            this.plhProducts.AddHtml("<tr><td>{0}</td><td>", product.Name);

                            Dionysos.Web.UI.WebControls.DropDownList ddlFilePath = new Dionysos.Web.UI.WebControls.DropDownList();
                            ddlFilePath.ID = "ddlProductId-" + product.ProductId;
                            ddlFilePath.UseDataBinding = false;
                            //ddlProductId.DataTextField = ProductFields.Name.Name;
                            //ddlProductId.DataValueField = ProductFields.ProductId.Name;
                            ddlFilePath.DataSource = listItems;
                            ddlFilePath.DataBind();

                            if (product.MediaCollection.Count > 0)
                            {
                                ddlFilePath.CssClass += " valid";
                            }
                            else
                            {
                                bool found = false;
                                string file = string.Empty;

                                int distance = -1;
                                int minDistance = -1;
                                foreach (ListItem item in listItems)
                                {
                                    distance = item.Text.ComputeLevenshteinDistance(product.Name);
                                    if (distance < treshold && (distance < minDistance || minDistance == -1))
                                    {
                                        file = item.Value;
                                        minDistance = distance;
                                        found = true;
                                        break;
                                    }
                                }

                                if (found)
                                {
                                    ddlFilePath.SelectedIndex = listItems.IndexOf(listItems.FindByValue(file));
                                    ddlFilePath.CssClass += " suggestion";
                                }
                                else
                                {
                                    ddlFilePath.SelectedIndex = 0;
                                    ddlFilePath.CssClass += " empty";
                                }
                            }

                            this.plhProducts.Controls.Add(ddlFilePath);

                            this.plhProducts.AddHtml("</td></tr>");
                        }

                        this.lblProductCount.Text = string.Format("{0} product{1}", productCount, (productCount > 1 ? "en" : ""));
                    }
                }
            }
        }

        //private void RenderProductsToMatch()
        //{
        //    this.plhProducts.Controls.Clear();

        //    // Check what to render
        //    ProductCollection products = new ProductCollection();
        //    PredicateExpression productFilter = new PredicateExpression();
        //    productFilter.Add(ProductFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

        //    if (this.cbOnlyVisible.Checked)
        //        productFilter.Add(ProductFields.Visible == true);

        //    if (this.cbOnlyNoGenericproduct.Checked)
        //        productFilter.Add(ProductFields.GenericproductId == DBNull.Value);

        //    if (this.cbOnlyNoPosproduct.Checked)
        //        productFilter.Add(ProductFields.PosproductId == DBNull.Value);

        //    RelationCollection joins = new RelationCollection();
        //    joins.Add(ProductEntity.Relations.ProductCategoryEntityUsingProductId, JoinHint.Left);

        //    PrefetchPath path = new PrefetchPath(EntityType.ProductEntity);
        //    path.Add(ProductEntity.PrefetchPathCategoryCollectionViaProductCategory).SubPath.Add(CategoryEntity.PrefetchPathParentCategoryEntity).SubPath.Add(CategoryEntity.PrefetchPathParentCategoryEntity).SubPath.Add(CategoryEntity.PrefetchPathParentCategoryEntity).SubPath.Add(CategoryEntity.PrefetchPathParentCategoryEntity);

        //    SortExpression sort = new SortExpression();
        //    sort.Add(ProductFields.Name | SortOperator.Ascending);

        //    products.GetMulti(productFilter, 0, sort, joins, path);

        //    // Load the generic products
        //    GenericproductCollection genericProducts = new GenericproductCollection();

        //    SortExpression sortGeneric = new SortExpression();
        //    sortGeneric.Add(GenericproductFields.Name | SortOperator.Ascending);
        //    genericProducts.GetMulti(null, 0, sortGeneric);

        //    // Load the POS products
        //    PosproductCollection posProducts = new PosproductCollection();

        //    PredicateExpression posFilter = new PredicateExpression();
        //    posFilter.Add(PosproductFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

        //    SortExpression sortPos = new SortExpression();
        //    sortPos.Add(PosproductFields.Name | SortOperator.Ascending);
        //    posProducts.GetMulti(posFilter, 0, sortPos);

        //    int productCount = 0;
        //    int treshold = 5;

        //    for (int i = 0; i < products.Count; i++)
        //    {
        //        ProductEntity current = products[i];

        //        bool isFood = false;
        //        bool isDrink = false;
        //        foreach (CategoryEntity category in current.CategoryCollectionViaProductCategory)
        //        {
        //            if (category.Name.Equals("Eten", StringComparison.InvariantCultureIgnoreCase))
        //                isFood = true;
        //            else if (
        //                        category.ParentCategoryId.HasValue &&
        //                        category.ParentCategoryEntity.Name.Equals("Eten", StringComparison.InvariantCultureIgnoreCase)
        //                    )
        //                isFood = true;
        //            else if (
        //                        category.ParentCategoryId.HasValue &&
        //                        category.ParentCategoryEntity.ParentCategoryId.HasValue &&
        //                        category.ParentCategoryEntity.ParentCategoryEntity.Name.Equals("Eten", StringComparison.InvariantCultureIgnoreCase)
        //                    )
        //                isFood = true;
        //            else if (
        //                        category.ParentCategoryId.HasValue &&
        //                        category.ParentCategoryEntity.ParentCategoryId.HasValue &&
        //                        category.ParentCategoryEntity.ParentCategoryEntity.ParentCategoryId.HasValue &&
        //                        category.ParentCategoryEntity.ParentCategoryEntity.ParentCategoryEntity.Name.Equals("Eten", StringComparison.InvariantCultureIgnoreCase)
        //                    )
        //                isFood = true;
        //            else if (
        //                        category.ParentCategoryId.HasValue &&
        //                        category.ParentCategoryEntity.ParentCategoryId.HasValue &&
        //                        category.ParentCategoryEntity.ParentCategoryEntity.ParentCategoryId.HasValue &&
        //                        category.ParentCategoryEntity.ParentCategoryEntity.ParentCategoryEntity.ParentCategoryId.HasValue &&
        //                        category.ParentCategoryEntity.ParentCategoryEntity.ParentCategoryEntity.ParentCategoryEntity.Name.Equals("Eten", StringComparison.InvariantCultureIgnoreCase)
        //                    )
        //                isFood = true;
        //            if (category.Name.Equals("Drinken", StringComparison.InvariantCultureIgnoreCase))
        //                isDrink = true;
        //            else if (
        //                        category.ParentCategoryId.HasValue &&
        //                        category.ParentCategoryEntity.Name.Equals("Drinken", StringComparison.InvariantCultureIgnoreCase)
        //                    )
        //                isDrink = true;
        //            else if (
        //                        category.ParentCategoryId.HasValue &&
        //                        category.ParentCategoryEntity.ParentCategoryId.HasValue &&
        //                        category.ParentCategoryEntity.ParentCategoryEntity.Name.Equals("Drinken", StringComparison.InvariantCultureIgnoreCase)
        //                    )
        //                isDrink = true;
        //            else if (
        //                        category.ParentCategoryId.HasValue &&
        //                        category.ParentCategoryEntity.ParentCategoryId.HasValue &&
        //                        category.ParentCategoryEntity.ParentCategoryEntity.ParentCategoryId.HasValue &&
        //                        category.ParentCategoryEntity.ParentCategoryEntity.ParentCategoryEntity.Name.Equals("Drinken", StringComparison.InvariantCultureIgnoreCase)
        //                    )
        //                isDrink = true;
        //            else if (
        //                        category.ParentCategoryId.HasValue &&
        //                        category.ParentCategoryEntity.ParentCategoryId.HasValue &&
        //                        category.ParentCategoryEntity.ParentCategoryEntity.ParentCategoryId.HasValue &&
        //                        category.ParentCategoryEntity.ParentCategoryEntity.ParentCategoryEntity.ParentCategoryId.HasValue &&
        //                        category.ParentCategoryEntity.ParentCategoryEntity.ParentCategoryEntity.ParentCategoryEntity.Name.Equals("Drinken", StringComparison.InvariantCultureIgnoreCase)
        //                    )
        //                isDrink = true;
        //        }

        //        if (this.cbOnlyFood.Checked && !isFood)
        //        {
        //            // Only food
        //        }
        //        else if (this.cbOnlyDrinks.Checked && !isDrink)
        //        {
        //            // Only drinks
        //        }
        //        else
        //        {
        //            productCount++;

        //            // Render row per product
        //            this.plhProducts.AddHtml("<tr>");
        //            this.plhProducts.AddHtml("<td class=\"control\">");

        //            TextBoxString tbName = new TextBoxString();
        //            tbName.ID = "tb-" + current.ProductId.ToString();
        //            tbName.UseDataBinding = false;
        //            tbName.Value = current.Name;

        //            this.plhProducts.Controls.Add(tbName);
        //            this.plhProducts.AddHtml("</td>");

        //            this.plhProducts.AddHtml("<td class=\"control\">");

        //            Dionysos.Web.UI.WebControls.DropDownListInt ddlGenericproductId = new Dionysos.Web.UI.WebControls.DropDownListInt();
        //            ddlGenericproductId.ID = "ddlGenericproductId-" + current.ProductId.ToString();
        //            ddlGenericproductId.UseDataBinding = false;
        //            ddlGenericproductId.DataTextField = GenericproductFields.Name.Name;
        //            ddlGenericproductId.DataValueField = GenericproductFields.GenericproductId.Name;
        //            ddlGenericproductId.DataSource = genericProducts;
        //            ddlGenericproductId.DataBind();

        //            ddlGenericproductId.Items.Insert(0, new ListItem("Geen - Maak uw keuze"));

        //            if (current.GenericproductId.HasValue)
        //            {
        //                ddlGenericproductId.Value = current.GenericproductId.Value;
        //                ddlGenericproductId.CssClass += " valid";
        //            }
        //            else
        //            {
        //                bool found = false;
        //                int genericProductId = -1;

        //                if (this.cbSuggestGenericproducts.Checked)
        //                {

        //                    int distance = -1;
        //                    int minDistance = -1;
        //                    foreach (var genericProduct in genericProducts)
        //                    {
        //                        distance = genericProduct.Name.ComputeLevenshteinDistance(current.Name);
        //                        if (distance < treshold && (distance < minDistance || minDistance == -1))
        //                        {
        //                            genericProductId = genericProduct.GenericproductId;
        //                            minDistance = distance;
        //                            found = true;
        //                            break;
        //                        }
        //                    }
        //                }

        //                if (found)
        //                {
        //                    ddlGenericproductId.Value = genericProductId;
        //                    ddlGenericproductId.CssClass += " suggestion";
        //                }
        //                else
        //                {
        //                    ddlGenericproductId.SelectedIndex = 0;
        //                    ddlGenericproductId.CssClass += " empty";
        //                }
        //            }

        //            this.plhProducts.Controls.Add(ddlGenericproductId);

        //            this.plhProducts.AddHtml("</td>");
        //            this.plhProducts.AddHtml("<td class=\"control\">");

        //            Dionysos.Web.UI.WebControls.DropDownListInt ddlPosproductId = new Dionysos.Web.UI.WebControls.DropDownListInt();
        //            ddlPosproductId.ID = "ddlPosproductId-" + current.ProductId.ToString();
        //            ddlPosproductId.UseDataBinding = false;
        //            ddlPosproductId.DataTextField = PosproductFields.Name.Name;
        //            ddlPosproductId.DataValueField = PosproductFields.PosproductId.Name;
        //            ddlPosproductId.DataSource = posProducts;
        //            ddlPosproductId.DataBind();

        //            ddlPosproductId.Items.Insert(0, new ListItem("Geen - Maak uw keuze"));

        //            if (current.PosproductId.HasValue)
        //            {
        //                ddlPosproductId.Value = current.PosproductId.Value;
        //                ddlPosproductId.CssClass += " valid";
        //            }
        //            else
        //            {
        //                ddlPosproductId.SelectedIndex = 0;
        //                ddlPosproductId.CssClass += " empty";
        //            }

        //            this.plhProducts.Controls.Add(ddlPosproductId);

        //            this.plhProducts.AddHtml("</td>");
        //            this.plhProducts.AddHtml("<td>");
        //            this.plhProducts.AddHtml("</td>");
        //            this.plhProducts.AddHtml("</tr>");
        //        }
        //    }

        //    this.lblProductCount.Text = string.Format("{0} product{1}", productCount, (productCount > 1 ? "en" : ""));
        //}

        private void SaveMatches()
        {
            MediaCollection mediaCollection = new MediaCollection();
            mediaCollection.GetMulti(null);

            EntityView<MediaEntity> mediaView = mediaCollection.DefaultView;
            PredicateExpression mediaFilter = null;

            // Match
            foreach (var Control in this.plhProducts.Controls)
            {
                if (Control is Dionysos.Web.UI.WebControls.DropDownList)
                {
                    var ddl = Control as Dionysos.Web.UI.WebControls.DropDownList;
                    int productId = Convert.ToInt32(Dionysos.StringUtil.GetAllAfterFirstOccurenceOf(ddl.ID, '-'));

                    // Get the product
                    ProductEntity product = new ProductEntity(productId);

                    // verify company
                    if (product.CompanyId != CmsSessionHelper.CurrentCompanyId)
                        throw new Exception("Ongeldigde product selectie in producten");

                    if (ddl.SelectedIndex > 0)
                    {
                        string fileName = ddl.SelectedValue;

                        #region Media

                        mediaFilter = new PredicateExpression();
                        mediaFilter.Add(MediaFields.ProductId == productId);
                        mediaFilter.Add(MediaFields.Name == fileName);
                        mediaView.Filter = mediaFilter;

                        MediaEntity mediaEntity = null;

                        if (mediaView.Count == 0)
                        {
                            mediaEntity = new MediaEntity();
                            mediaEntity.ProductId = productId;
                            mediaEntity.MediaType = -1;
                            mediaEntity.Name = fileName;
                            mediaEntity.FilePathRelativeToMediaPath = fileName;
                            if (mediaEntity.Save())
                            {
                                mediaEntity.Refetch();
                                mediaView.RelatedCollection.Add(mediaEntity);
                            }
                        }
                        else if (mediaView.Count == 1)
                        {
                            mediaEntity = mediaView[0];
                        }
                        else
                            throw new ApplicationException(string.Format("Multiple media items found for product '{0}'", product.Name));

                        #endregion
                    }

                    product.Save();
                }
            }
        }

        void HookupEvents()
        {
            this.btLoadPhotos.Click += new EventHandler(btLoadPhotos_Click);
            this.btMatch.Click += new EventHandler(btMatch_Click);
        }

        #endregion

        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            this.SetGui();
            base.OnInit(e);
            this.LoadPhotos();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.HookupEvents();
        }

        private void btLoadPhotos_Click(object sender, EventArgs e)
        {
            QueryStringHelper qs = this.GetQueryStringInstanceWithValues();
            WebShortcuts.Redirect(qs.MergeQuerystringWithRawUrl());
        }

        private void btMatch_Click(object sender, EventArgs e)
        {
            this.SaveMatches();

            QueryStringHelper qs = this.GetQueryStringInstanceWithValues();
            WebShortcuts.Redirect(qs.MergeQuerystringWithRawUrl());
        }

        #endregion
    }
}